# ATSyRA 2 Studio code

## Directory Structure

- plugins : all the plugins that constitute the ATSyRA specific code
- releng : artifacts related to release engineering, ie. Eclipse features, Eclipse 
   update site, Eclipse product and branding plugin. It also contains script 
   useful to mirror external update sites.
- tests : test plugins for ATSyRA

## Setting up development environment


### Local development

- Use a previous version of ATSyRA studio (preferably the latest build in the CI).
- Checkout the git repository
- Import in Eclipse the plugins you want to contribute to.  (you can also import all of them :wink: )
- Launch a second Eclipse from the first one in order to test your changes.

Special case:
- changes on the grammar. Due to limitation of xtext, if you modify the base ecore 
and then generate its code, the xtext grammar is not able to work directly on it 
in the same workspace.
You have to launch the second eclipse, them import the xtext project. In this 
second eclipse, you'll be able to generate the code for the grammar. 

### How to compile the full version

You can compile the full Studio with a single command (the same command is used in the continuous integration)
```
mvn clean package
```

### Continuous Integration

![Build Status](https://gitlab.inria.fr/inria.fr/atsyra/atsyra-dev/badges/master/pipeline.svg)

The `gitlab-ci.yml` defines the following jobs:

```mermaid
flowchart LR
    
    

    Start((.))-- "commit(!master)" --> test_only  

    subgraph build
        test_only
    end

    Start((.))-- "MR" --> test_and_package
    Start((.))-- "commit(master)" -->  buid_and_publish_master

    subgraph package
        test_and_package
        buid_and_publish_master
    end

    Start((.))-- "tag" -->deploy_pomfirst
    Start((.))-- "tag" -->build_deploy_eclipse_release
    Start((.))-- "commit(📁docs)" -->publish_doc
    buid_and_publish_master-- "commit" -->publish_benchmarks

    subgraph publish
        deploy_pomfirst
        build_deploy_eclipse_release
        publish_doc
        publish_benchmarks
    end
    publish_doc -- "ssh" --> www
    build_deploy_eclipse_release -- "ssh" --> www
    buid_and_publish_master -- "ssh" --> www
    
    deploy_pomfirst-- "tag" -->release_job
    build_deploy_eclipse_release-- "tag" -->release_job
    
    Start((.))-- "tag" --> update_changelog 

    subgraph release
        release_job
        update_changelog
    end

    subgraph atsyra.irisa.fr
        www
    end
    publish_benchmarks -- "commit" --> pages
    subgraph atsyra-benchmark-results/.gitlab-ci.yml
        pages
    end
    
```

