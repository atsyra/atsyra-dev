package fr.irisa.atsyra.transfo.atg.ctl.test;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.junit.BeforeClass;
import org.junit.Test;
import com.google.inject.Injector;
import atsyragoal.AtsyraGoalModel;
import atsyragoal.AtsyraTree;
import fr.irisa.atsyra.transfo.atg.ctl.AbstractAtsyraTreeAspects;

public class AbstractAtsyraTreeAspectTest {

	private static AtsyraTree test_or_trees;
	private static AtsyraTree test_sand_trees;

	@BeforeClass
	public static void setUpBeforeClass() {
		Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
				.getInjector(fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
		XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class); 
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		
		URI uri = URI.createFileURI("testfiles/AbstractAtsyraTreeAspects_1/test.atg");
		Resource goalRes = resourceSet.getResource(uri, true);
		AtsyraGoalModel goalModel = (AtsyraGoalModel) goalRes.getContents().get(0);
		test_or_trees = (AtsyraTree) goalModel.getTrees().get(0);
		test_sand_trees = (AtsyraTree) goalModel.getTrees().get(1);
	}
	
	@Test
	public void testgetNotEmptyFormula() {
		assertEquals("var1=val * EF var1=1 + var2=val * EF var2=1 + var3=val * EF var3=1;", AbstractAtsyraTreeAspects.getNotEmptyFormula(test_or_trees));
		assertEquals("var1=val * EF (var1=1 * var2=val * EF (var2=1 * var3=val * EF (var3=1)));", AbstractAtsyraTreeAspects.getNotEmptyFormula(test_sand_trees));
	}
	
	@Test
	public void testgetMeetFormula() {
		assertEquals("TRUE * (var1=val * EF (var1=1 * FALSE) + var2=val * EF (var2=1 * FALSE) + var3=val * EF (var3=1 * FALSE));", AbstractAtsyraTreeAspects.getMeetFormula(test_or_trees));
		assertEquals("TRUE * var1=val * EF (var1=1 * var2=val * EF (var2=1 * var3=val * EF (var3=1 * FALSE)));", AbstractAtsyraTreeAspects.getMeetFormula(test_sand_trees));
	}
	
	@Test
	public void testgetUnderMatchFormula() {
		assertEquals("!TRUE * var1=val * EF var1=1 + var1=val * EF (var1=1 * !FALSE) + !TRUE * var2=val * EF var2=1 + var2=val * EF (var2=1 * !FALSE) + !TRUE * var3=val * EF var3=1 + var3=val * EF (var3=1 * !FALSE);", AbstractAtsyraTreeAspects.getUnderMatchFormula(test_or_trees));
		assertEquals("!TRUE * var1=val * EF (var1=1 * var2=val * EF (var2=1 * var3=val * EF (var3=1))) + var1=val * EF (var1=1 * var2=val * EF (var2=1 * var3=val * EF (var3=1 * !FALSE)));", AbstractAtsyraTreeAspects.getUnderMatchFormula(test_sand_trees));
	}

}
