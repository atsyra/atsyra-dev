package fr.irisa.atsyra.gal.test;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Injector;

import atsyragoal.AtsyraGoalModel;
import atsyragoal.AtsyraTree;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.gal.CTLanswer;
import fr.irisa.atsyra.gal.GalCTL;
import fr.irisa.atsyra.transfo.atg.ctl.AbstractAtsyraTreeAspects;
import fr.irisa.atsyra.transfo.building.gal.Building2GALForCTL;
import fr.lip6.move.gal.GALTypeDeclaration;

public class TreePropertiesCheckingTest {

	private static Building2GALForCTL b2g;
	private static XtextResourceSet resourceSet;
	private static AtsyraGoalModel goal_test;
	private static BuildingModel buildingModel_test;
	private static IWorkspace workspace; 
	
	private void LoadTestCase(String caseName) {
		URI buildinguri = URI.createFileURI("testfiles/" + caseName + "/test.building");
		Resource buildingRes = resourceSet.getResource(buildinguri, true);
		buildingModel_test = (BuildingModel) buildingRes.getContents().get(0);
		URI goaluri = URI.createFileURI("testfiles/" + caseName + "/test.atg");
		Resource goalRes = resourceSet.getResource(goaluri, true);
		goal_test = (AtsyraGoalModel) goalRes.getContents().get(0);
	}
	
	@BeforeClass
	public static void SetUpBeforeClass() {
		workspace= ResourcesPlugin.getWorkspace(); 
		b2g = new Building2GALForCTL();
		
		Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
				.getInjector(fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
		resourceSet = inj.getInstance(XtextResourceSet.class); 
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
	}
	
	@Test
	public void testORMeet() {
		LoadTestCase("OR_1"); 
		//look for a gal file
		
		IPath path = Path.fromOSString("fr.irisa.atsyra.gal.test/testfiles/OR_1/test.gal") ;
		IFile gal_file = workspace.getRoot().getFileForLocation(path);
		// compute the gal model
		GALTypeDeclaration gal = b2g.transformToGAL(buildingModel_test);
		b2g.flattenGAL(gal);
		// write the gal model in the file
		b2g.writeToFile(gal, gal_file, null);
		try {
			AtsyraTree tree = goal_test.getTrees().get(0);
			String formula = AbstractAtsyraTreeAspects.getMeetFormula(tree);
			InputStream stream = new ByteArrayInputStream(formula.getBytes(StandardCharsets.UTF_8));
			IFile meet_formula_file = gal_file.getProject().getFile("testfiles/OR_1/testmeet.ctl");
			if(!meet_formula_file.exists()) {
				meet_formula_file.create(stream, true, null);
			}
			else {
				meet_formula_file.setContents(stream, true, false, null);
			}
			GalCTL ctlmc = new GalCTL(gal_file, meet_formula_file);
			CTLanswer result = ctlmc.computeCTL(null);
			assertEquals(CTLanswer.True, result);
		} catch (CoreException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void testORUM() {
		LoadTestCase("OR_1"); 
		//look for a gal file
		
		IPath path = Path.fromOSString("fr.irisa.atsyra.gal.test/testfiles/OR_1/test.gal") ;
		IFile gal_file = workspace.getRoot().getFileForLocation(path);
		// compute the gal model
		GALTypeDeclaration gal = b2g.transformToGAL(buildingModel_test);
		b2g.flattenGAL(gal);
		// write the gal model in the file
		b2g.writeToFile(gal, gal_file, null);
		try {
			AtsyraTree tree = goal_test.getTrees().get(0);
			String formula = AbstractAtsyraTreeAspects.getUnderMatchFormula(tree);
			InputStream stream = new ByteArrayInputStream(formula.getBytes(StandardCharsets.UTF_8));
			IFile um_formula_file = gal_file.getProject().getFile("testfiles/OR_1/testum.ctl");
			if(!um_formula_file.exists()) {
				um_formula_file.create(stream, true, null);
			}
			else {
				um_formula_file.setContents(stream, true, false, null);
			}
			GalCTL ctlmc = new GalCTL(gal_file, um_formula_file);
			CTLanswer result = ctlmc.computeCTL(null);
			assertEquals(CTLanswer.False, result);
		} catch (CoreException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void testOROM() {

		fail("Not yet implemented");
	}
	
	@Test
	public void testORMatch() {

		fail("Not yet implemented");
	}
	
	@Test
	public void testSANDMeet() {
		LoadTestCase("SAND_1"); 
		//look for a gal file
		
		IPath path = Path.fromOSString("fr.irisa.atsyra.gal.test/testfiles/SAND_1/test.gal") ;
		IFile gal_file = workspace.getRoot().getFileForLocation(path);
		// compute the gal model
		GALTypeDeclaration gal = b2g.transformToGAL(buildingModel_test);
		b2g.flattenGAL(gal);
		// write the gal model in the file
		b2g.writeToFile(gal, gal_file, null);
		try {
			AtsyraTree tree = goal_test.getTrees().get(0);
			String formula = AbstractAtsyraTreeAspects.getMeetFormula(tree);
			InputStream stream = new ByteArrayInputStream(formula.getBytes(StandardCharsets.UTF_8));
			IFile meet_formula_file = gal_file.getProject().getFile("testfiles/SAND_1/testmeet.ctl");
			if(!meet_formula_file.exists()) {
				meet_formula_file.create(stream, true, null);
			}
			else {
				meet_formula_file.setContents(stream, true, false, null);
			}
			GalCTL ctlmc = new GalCTL(gal_file, meet_formula_file);
			CTLanswer result = ctlmc.computeCTL(null);
			assertEquals(CTLanswer.True, result);
		} catch (CoreException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void testSANDUM() {
		LoadTestCase("SAND_1"); 
		//look for a gal file
		
		IPath path = Path.fromOSString("fr.irisa.atsyra.gal.test/testfiles/SAND_1/test.gal") ;
		IFile gal_file = workspace.getRoot().getFileForLocation(path);
		// compute the gal model
		GALTypeDeclaration gal = b2g.transformToGAL(buildingModel_test);
		b2g.flattenGAL(gal);
		// write the gal model in the file
		b2g.writeToFile(gal, gal_file, null);
		try {
			AtsyraTree tree = goal_test.getTrees().get(0);
			String formula = AbstractAtsyraTreeAspects.getUnderMatchFormula(tree);
			InputStream stream = new ByteArrayInputStream(formula.getBytes(StandardCharsets.UTF_8));
			IFile um_formula_file = gal_file.getProject().getFile("testfiles/SAND_1/testum.ctl");
			if(!um_formula_file.exists()) {
				um_formula_file.create(stream, true, null);
			}
			else {
				um_formula_file.setContents(stream, true, false, null);
			}
			GalCTL ctlmc = new GalCTL(gal_file, um_formula_file);
			CTLanswer result = ctlmc.computeCTL(null);
			assertEquals(CTLanswer.False, result);
		} catch (CoreException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void testSANDOM() {

		fail("Not yet implemented");
	}
	
	@Test
	public void testSANDMatch() {

		fail("Not yet implemented");
	}
	
	@Test
	public void testANDMeet() {

		fail("Not yet implemented");
	}
	
	@Test
	public void testANDUM() {

		fail("Not yet implemented");
	}
	
	@Test
	public void testANDOM() {

		fail("Not yet implemented");
	}
	
	@Test
	public void testANDMatch() {
		fail("Not yet implemented");
	}
}
