/**
 * 
 */
package fr.irisa.atsyra.transfo.building.gal.test;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.irisa.atsyra.transfo.building.gal.GALBuildHelper;
import fr.lip6.move.gal.And;
import fr.lip6.move.gal.AssignType;
import fr.lip6.move.gal.Assignment;
import fr.lip6.move.gal.BooleanExpression;
import fr.lip6.move.gal.Comparison;
import fr.lip6.move.gal.ComparisonOperators;
import fr.lip6.move.gal.Constant;
import fr.lip6.move.gal.GalFactory;
import fr.lip6.move.gal.Ite;
import fr.lip6.move.gal.Label;
import fr.lip6.move.gal.Not;
import fr.lip6.move.gal.Or;
import fr.lip6.move.gal.ParamRef;
import fr.lip6.move.gal.Parameter;
import fr.lip6.move.gal.SelfCall;
import fr.lip6.move.gal.Statement;
import fr.lip6.move.gal.Transition;
import fr.lip6.move.gal.TypedefDeclaration;
import fr.lip6.move.gal.Variable;
import fr.lip6.move.gal.VariableReference;

/**
 * @author Maxime Audinot
 *
 */
public class GALBuildHelperTest {
	
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createVariable(String, int)}.
	 */
	@Test
	public void testcreateVariable (){
		String name = "test_variable";
		int value = 0;
		Variable variable = GALBuildHelper.createVariable(name,value);
		assertEquals(name,variable.getName());
		assertTrue("variable initial value should be a constant", variable.getValue() instanceof Constant);
		Constant cst = (Constant) variable.getValue();
		assertEquals(value,cst.getValue());
		assertFalse(variable.isHotbit());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createHotbitVariable(String, int)}.
	 */
	@Test
	public void testcreateHotbitVariable (){
		String name = "test_variable";
		int value = 0;
		Variable variable = GALBuildHelper.createHotbitVariable(name,value);
		assertEquals(name,variable.getName());
		assertTrue("variable initial value should be a constant", variable.getValue() instanceof Constant);
		Constant cst = (Constant) variable.getValue();
		assertEquals(value,cst.getValue());
		assertTrue(variable.isHotbit());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createConstant(int)}.
	 */
	@Test
	public void testcreateConstant() {
		int value = 0;
		Constant constant = GALBuildHelper.createConstant(value);
		assertEquals(value,constant.getValue());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createTransition(String)}.
	 */
	@Test
	public void testcreateTransition() {
		String name = "test_transition";
		Transition transition = GALBuildHelper.createTransition(name);
		assertEquals(name,transition.getName());
		assertEquals(null, transition.getGuard());
		assertTrue(transition.getActions().isEmpty());
		assertEquals(null, transition.getLabel());
		assertTrue(transition.getParams().isEmpty());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createBoolExprEqVarCst(Variable, int)}.
	 */
	@Test
	public void testcreateBoolExprEqVarCst() {
		Variable variable = GALBuildHelper.createVariable("test_variable", 1);
		int constant = 0;
		BooleanExpression expression = GALBuildHelper.createBoolExprEqVarCst(variable, constant);
		assertTrue(expression instanceof Comparison);
		Comparison cmp = (Comparison) expression;
		assertEquals(ComparisonOperators.EQ, cmp.getOperator());
		assertTrue(cmp.getLeft() instanceof VariableReference);
		assertEquals(variable, ((VariableReference)cmp.getLeft()).getRef());
		assertTrue(cmp.getRight() instanceof Constant);
		assertEquals(constant, ((Constant)cmp.getRight()).getValue());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createBoolExprEqVarVar(Variable, Variable)}.
	 */
	@Test
	public void testcreateBoolExprEqVarVar() {
		Variable variable1 = GALBuildHelper.createVariable("test_variable_one", 1);
		Variable variable2 = GALBuildHelper.createVariable("test_variable_two", 0);
		BooleanExpression expression = GALBuildHelper.createBoolExprEqVarVar(variable1, variable2);
		assertTrue(expression instanceof Comparison);
		Comparison cmp = (Comparison) expression;
		assertEquals(ComparisonOperators.EQ, cmp.getOperator());
		assertTrue(cmp.getLeft() instanceof VariableReference);
		assertEquals(variable1, ((VariableReference)cmp.getLeft()).getRef());
		assertTrue(cmp.getRight() instanceof VariableReference);
		assertEquals(variable2, ((VariableReference)cmp.getRight()).getRef());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createBoolExprEqCstCst(int, int)}.
	 */
	@Test
	public void testcreateBoolExprEqCstCst() {
		int constant1 = 0;
		int constant2 = 0;
		BooleanExpression expression = GALBuildHelper.createBoolExprEqCstCst(constant1, constant2);
		assertTrue(expression instanceof Comparison);
		Comparison cmp = (Comparison) expression;
		assertEquals(ComparisonOperators.EQ, cmp.getOperator());
		assertTrue(cmp.getLeft() instanceof Constant);
		assertEquals(constant1, ((Constant)cmp.getLeft()).getValue());
		assertTrue(cmp.getRight() instanceof Constant);
		assertEquals(constant2,  ((Constant)cmp.getRight()).getValue());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createBoolExprEqPrmCst(Parameter, int)}.
	 */
	@Test
	public void testcreateBoolExprEqPrmCst() {
		TypedefDeclaration type = GALBuildHelper.createTypeDefDeclaration("test_type", 0, 4);
		Parameter param = GALBuildHelper.createParam("test_param", type);
		int constant = 0;
		BooleanExpression expression = GALBuildHelper.createBoolExprEqPrmCst(param, constant);
		assertTrue(expression instanceof Comparison);
		Comparison cmp = (Comparison) expression;
		assertEquals(ComparisonOperators.EQ, cmp.getOperator());
		assertTrue(cmp.getLeft() instanceof ParamRef);
		assertEquals(param, ((ParamRef)cmp.getLeft()).getRefParam());
		assertTrue(cmp.getRight() instanceof Constant);
		assertEquals(constant, ((Constant)cmp.getRight()).getValue());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createBoolExprAnd(BooleanExpression, BooleanExpression)}.
	 */
	@Test
	public void testcreateBoolExprAnd() {
		BooleanExpression left = GALBuildHelper.createBoolExprEqCstCst(0, 0);
		BooleanExpression right = GALBuildHelper.createBoolExprEqCstCst(1, 2);
		BooleanExpression expression = GALBuildHelper.createBoolExprAnd(left, right);
		assertTrue(expression instanceof And);
		And and = (And) expression;
		assertEquals(left,and.getLeft());
		assertEquals(right,and.getRight());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createBoolExprOr(BooleanExpression, BooleanExpression)}.
	 */
	@Test
	public void testcreateBoolExprOr() {
		BooleanExpression left = GALBuildHelper.createBoolExprEqCstCst(0, 0);
		BooleanExpression right = GALBuildHelper.createBoolExprEqCstCst(1, 2);
		BooleanExpression expression = GALBuildHelper.createBoolExprOr(left, right);
		assertTrue(expression instanceof Or);
		Or or = (Or) expression;
		assertEquals(left,or.getLeft());
		assertEquals(right,or.getRight());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createBoolExprNot(BooleanExpression)}.
	 */
	@Test
	public void testcreateBoolExprNot() {
		BooleanExpression expr = GALBuildHelper.createBoolExprEqCstCst(0, 0);
		BooleanExpression expression = GALBuildHelper.createBoolExprNot(expr);
		assertTrue(expression instanceof Not);
		Not not = (Not) expression;
		assertEquals(expr,not.getValue());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createVarAssignConst(Variable, int)}.
	 */
	@Test
	public void testcreateVarAssignConst() {
		Variable variable = GALBuildHelper.createVariable("test_variable", 1);
		int constant = 0;
		Statement statement = GALBuildHelper.createVarAssignConst(variable, constant);
		assertTrue(statement instanceof Assignment);
		Assignment assignment = (Assignment) statement;
		assertEquals(assignment.getType(), AssignType.ASSIGN);
		assertTrue(assignment.getLeft() instanceof VariableReference);
		assertEquals(variable,((VariableReference)assignment.getLeft()).getRef());
		assertTrue(assignment.getRight() instanceof Constant);
		assertEquals(constant,((Constant)assignment.getRight()).getValue());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createVarAssignVar(Variable, Variable)}.
	 */
	@Test
	public void testcreateVarAssignVar() {
		Variable variable1 = GALBuildHelper.createVariable("test_variable_one", 1);
		Variable variable2 = GALBuildHelper.createVariable("test_variable_two", 0);
		Statement statement = GALBuildHelper.createVarAssignVar(variable1, variable2);
		assertTrue(statement instanceof Assignment);
		Assignment assignment = (Assignment) statement;
		assertEquals(assignment.getType(), AssignType.ASSIGN);
		assertTrue(assignment.getLeft() instanceof VariableReference);
		assertEquals(variable1, ((VariableReference)assignment.getLeft()).getRef());
		assertTrue(assignment.getRight() instanceof VariableReference);
		assertEquals(variable2, ((VariableReference)assignment.getRight()).getRef());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createVarAssignParam(Variable, Parameter)}.
	 */
	@Test
	public void testcreateVarAssignParam() {
		TypedefDeclaration type = GALBuildHelper.createTypeDefDeclaration("test_type", 0, 4);
		Variable variable = GALBuildHelper.createVariable("test_variable", 1);
		Parameter param = GALBuildHelper.createParam("test_param", type);
		Statement statement = GALBuildHelper.createVarAssignParam(variable, param);
		assertTrue(statement instanceof Assignment);
		Assignment assignment = (Assignment) statement;
		assertEquals(assignment.getType(), AssignType.ASSIGN);
		assertTrue(assignment.getLeft() instanceof VariableReference);
		assertEquals(variable,((VariableReference)assignment.getLeft()).getRef());
		assertTrue(assignment.getRight() instanceof ParamRef);
		assertEquals(param,((ParamRef)assignment.getRight()).getRefParam());
		}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createParamRef(Parameter)}.
	 */
	@Test
	public void testcreateParamRef() {
		TypedefDeclaration type = GALBuildHelper.createTypeDefDeclaration("test_type", 0, 4);
		Parameter param = GALBuildHelper.createParam("test_param", type);
		ParamRef paramref = GALBuildHelper.createParamRef(param);
		assertEquals(param,paramref.getRefParam());
		}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createVarRef(Variable)}.
	 */
	@Test
	public void testcreateVarRef() {
		Variable variable = GALBuildHelper.createVariable("test_variable", 1);
		VariableReference varref = GALBuildHelper.createVarRef(variable);
		assertEquals(variable,varref.getRef());
		assertEquals(null,varref.getIndex());
		}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createIfThen(BooleanExpression, Statement)}.
	 */
	@Test
	public void testcreateIfThen() {
		BooleanExpression cond = GALBuildHelper.createBoolExprEqCstCst(0, 0);
		Statement thenSt = GalFactory.eINSTANCE.createStatement();
		Statement statement = GALBuildHelper.createIfThen(cond, thenSt);
		assertTrue(statement instanceof Ite);
		Ite ite = (Ite) statement;
		assertEquals(cond,ite.getCond());
		assertTrue(ite.getIfTrue().size() == 1);
		assertEquals(thenSt,ite.getIfTrue().get(0));
		assertTrue(ite.getIfFalse().isEmpty());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createIfThenElse(BooleanExpression, Statement, Statement)}.
	 */
	@Test
	public void testcreateIfThenElse() {
		BooleanExpression cond = GALBuildHelper.createBoolExprEqCstCst(0, 0);
		Statement thenSt = GalFactory.eINSTANCE.createStatement();
		Statement elseSt = GalFactory.eINSTANCE.createStatement();
		Statement statement = GALBuildHelper.createIfThenElse(cond, thenSt, elseSt);
		assertTrue(statement instanceof Ite);
		Ite ite = (Ite) statement;
		assertEquals(cond,ite.getCond());
		assertTrue(ite.getIfTrue().size() == 1);
		assertEquals(thenSt,ite.getIfTrue().get(0));
		assertTrue(ite.getIfFalse().size() == 1);
		assertEquals(elseSt,ite.getIfFalse().get(0));
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createCall(Label)}.
	 */
	@Test
	public void testcreateCall() {
		Label called = GalFactory.eINSTANCE.createLabel();
		Statement statement = GALBuildHelper.createCall(called);
		assertTrue(statement instanceof SelfCall);
		SelfCall call = (SelfCall) statement;
		assertEquals(called,call.getLabel());
		assertTrue(call.getParams().isEmpty());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createTypeDefDeclaration(String, int, int)}.
	 */
	@Test
	public void testcreateTypeDefDeclaration() {
		String name = "test_type";
		int min = 1;
		int max = 4;
		TypedefDeclaration typedef = GALBuildHelper.createTypeDefDeclaration(name, min, max);
		assertEquals(name,typedef.getName());
		assertTrue(typedef.getMin() instanceof Constant);
		assertEquals(min,((Constant)typedef.getMin()).getValue());
		assertTrue(typedef.getMax() instanceof Constant);
		assertEquals(max,((Constant)typedef.getMax()).getValue());
	}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createParam(String, TypedefDeclaration)}.
	 */
	@Test
	public void testcreateParam() {
		String name = "test_param";
		TypedefDeclaration type = GalFactory.eINSTANCE.createTypedefDeclaration();
		Parameter param = GALBuildHelper.createParam(name, type);
		assertEquals("$"+name,param.getName());
		assertEquals(type,param.getType());
		}
		
	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.GALBuildHelper#createLabel(String)}.
	 */
	@Test
	public void testcreateLabel() {
		String name = "test_label";	
		Label label = GALBuildHelper.createLabel(name);
		assertEquals(name,label.getName());
		}

}
