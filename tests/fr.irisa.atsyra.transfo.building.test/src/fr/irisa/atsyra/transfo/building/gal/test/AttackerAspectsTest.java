package fr.irisa.atsyra.transfo.building.gal.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;
import java.util.TreeSet;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Injector;

import fr.irisa.atsyra.building.Attacker;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.transfo.building.gal.AttackerAspects;

public class AttackerAspectsTest {

	static BuildingModel test_building = null;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
				.getInjector(fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
		XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class); 
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		
		URI uri = URI.createFileURI("testfiles/AttackerAspects_1/test.building");
		Resource buildingRes = resourceSet.getResource(uri, true);
		test_building = (BuildingModel) buildingRes.getContents().get(0);
	}

	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.AttackerAspects#getNumber(fr.irisa.atsyra.transfo.building.gal.Zone)}.
	 */
	@Test
	public void testgetNumber() {
		// we need a dummy attacker to call init numbers (which is a static aspect, but xtend generate a function taking an attacker as argument
		Attacker dummy = test_building.getBuildings().get(0).getAttackers().get(0);
		AttackerAspects.initNumbers(dummy, test_building);
		// get the list of attackers
		EList<Attacker> attackers = test_building.getBuildings().get(0).getAttackers();
		// values are uniques
		for(Attacker attacker1 : attackers) {
			for(Attacker attacker2 : attackers) {
				assertTrue(attacker1.equals(attacker2) || AttackerAspects.getNumber(attacker1) != AttackerAspects.getNumber(attacker2));
			}
		}
		// get the set of all vales
		Set<Integer> values = new TreeSet<Integer>();
		for(Attacker attacker : attackers) {
			values.add(AttackerAspects.getNumber(attacker));
		}
		// all values should be exactly the interval [1, number of attackers]
		assertFalse(values.contains(0));
		for(int i=1; i <= attackers.size(); i++) {
			assertTrue(values.contains(i));
		}
	}
}
