/**
 * 
 */
package fr.irisa.atsyra.transfo.building.gal.test;

import static org.junit.Assert.assertEquals;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Injector;

import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraGoalModel;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.gal.GalTypeDeclarationAspects;
import fr.irisa.atsyra.transfo.building.gal.Building2GALForReachability;
import fr.lip6.move.gal.GALTypeDeclaration;
import fr.lip6.move.gal.Specification;
import fr.lip6.move.serialization.SerializationUtil;

/**
 * @author Maxime Audinot
 *
 */
public class Building2GALTest {
	
	private static Building2GALForReachability b2g;
	private static XtextResourceSet resourceSet;
	private static AtsyraGoal goal_test;
	private static BuildingModel buildingModel_test;
	private static GALTypeDeclaration gal_test;
	
	private void LoadTestCase(String caseName) {
		URI buildinguri = URI.createFileURI("testfiles/" + caseName + "/test.building");
		Resource buildingRes = resourceSet.getResource(buildinguri, true);
		buildingModel_test = (BuildingModel) buildingRes.getContents().get(0);
		URI goaluri = URI.createFileURI("testfiles/" + caseName + "/test.atg");
		Resource goalRes = resourceSet.getResource(goaluri, true);
		AtsyraGoalModel goalModel = (AtsyraGoalModel) goalRes.getContents().get(0);
		goal_test = (AtsyraGoal) goalModel.getAtsyragoals().get(0);
		URI galuri = URI.createFileURI("testfiles/" + caseName + "/test.gal");
		Resource galRes = resourceSet.getResource(galuri, true);
		Specification galSpec = (Specification) galRes.getContents().get(0);
		gal_test = (GALTypeDeclaration) galSpec.getTypes().get(0);
	}
	
	@BeforeClass
	public static void SetUpBeforeClass() {
		b2g = new Building2GALForReachability();
		
		Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
				.getInjector(fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
		resourceSet = inj.getInstance(XtextResourceSet.class); 
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
	}

	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.Building2GAL#transformToGAL(fr.irisa.atsyra.transfo.building.gal.BuildingModel, fr.irisa.atsyra.transfo.building.gal.AtsyraGoal)}.
	 */
	@Test
	public void testtransformToGAL() {
		GALTypeDeclaration galType;
		for(int i=1;i<5;i++) {
			// test case transformToGAL_i
			LoadTestCase("transformToGAL_" + i);
			galType = b2g.transformToGAL(buildingModel_test, goal_test);
			System.err.println(GalTypeDeclarationAspects.getString(galType));
			assertEquals(SerializationUtil.getText(gal_test, true),SerializationUtil.getText(galType, true));
		}
	}

}
