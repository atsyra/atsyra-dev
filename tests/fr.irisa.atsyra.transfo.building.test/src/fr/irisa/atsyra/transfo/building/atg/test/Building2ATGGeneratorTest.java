package fr.irisa.atsyra.transfo.building.atg.test;

import static org.junit.Assert.assertEquals;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.util.Files;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Injector;

import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.transfo.building.atg.Building2ATGGenerator;

public class Building2ATGGeneratorTest {
	
	private static Building2ATGGenerator b2atg;
	private static XtextResourceSet resourceSet;
	private static String atg_expected;
	private static BuildingModel buildingModel_test;

	private void LoadTestCase(String caseName) {
		URI buildinguri = URI.createFileURI("testfiles/" + caseName + "/test.building");
		Resource buildingRes = resourceSet.getResource(buildinguri, true);
		buildingModel_test = (BuildingModel) buildingRes.getContents().get(0);
		atg_expected = Files.readFileIntoString("testfiles/" + caseName + "/test.atg");
	}
	
	@BeforeClass
	public static void SetUpBeforeClass() {
		b2atg = new Building2ATGGenerator();
		
		Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
				.getInjector(fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
		resourceSet = inj.getInstance(XtextResourceSet.class); 
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
	}
	
	@Test
	public void testgenerateATG() {
		for(int i=1;i<4;i++) {
			// test case generateATG_i
			LoadTestCase("generateATG_" + i);
			assertEquals(atg_expected, b2atg.generateATG(buildingModel_test));
		}
	}

}
