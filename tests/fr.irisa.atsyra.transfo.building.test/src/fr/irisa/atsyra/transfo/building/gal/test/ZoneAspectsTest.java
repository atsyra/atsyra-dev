package fr.irisa.atsyra.transfo.building.gal.test;

import static org.junit.Assert.*;

import java.util.Set;
import java.util.TreeSet;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Injector;

import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.building.Zone;
import fr.irisa.atsyra.transfo.building.gal.ZoneAspects;

public class ZoneAspectsTest {
	
	static BuildingModel test_building = null;
	
	@BeforeClass
	static public void setUp() {
		Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
				.getInjector(fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
		XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class); 
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		
		URI uri = URI.createFileURI("testfiles/ZoneAspects_1/test.building");
		Resource buildingRes = resourceSet.getResource(uri, true);
		test_building = (BuildingModel) buildingRes.getContents().get(0);
	}

	/**
	 * Test method for {@link fr.irisa.atsyra.transfo.building.gal.ZoneAspects#getNumber(fr.irisa.atsyra.transfo.building.gal.Zone)}.
	 */
	@Test
	public void testgetNumber() {
		// we need a dummy zone to call init numbers (which is a static aspect, but xtend generate a function taking a zone as argument
		Zone dummy = test_building.getBuildings().get(0).getZones().get(0);
		ZoneAspects.initNumbers(dummy, test_building);
		// get the list of zones
		EList<Zone> zones = test_building.getBuildings().get(0).getZones();
		// values are uniques
		for(Zone zone1 : zones) {
			for(Zone zone2 : zones) {
				assertTrue(zone1.equals(zone2) || ZoneAspects.getNumber(zone1) != ZoneAspects.getNumber(zone2));
			}
		}
		// get the set of all vales
		Set<Integer> values = new TreeSet<Integer>();
		for(Zone zone : zones) {
			values.add(ZoneAspects.getNumber(zone));
		}
		// all values should be exactly the interval [1, number of zones]
		assertFalse(values.contains(0));
		for(int i=1; i <= zones.size(); i++) {
			assertTrue(values.contains(i));
		}
	}

}
