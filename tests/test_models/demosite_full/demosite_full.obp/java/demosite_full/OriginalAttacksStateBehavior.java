package demosite_full;

import org.xid.basics.serializer.Boost;
import obp.explorer.runtime.obs.InformalAction;
import obp.explorer.runtime.obs.AtomicAction;
import obp.explorer.runtime.core.Transition;
import obp.explorer.runtime.core.Channel;
import obp.explorer.runtime.SymbolsTable;
import obp.explorer.runtime.ProcessBehavior;
import obp.explorer.runtime.ExplorationContext;
import obp.explorer.runtime.Configuration;
import obp.explorer.runtime.BehaviorConfiguration;
import obp.explorer.runtime.AbstractCallable;
import java.util.List;

@SuppressWarnings("all")
public class OriginalAttacksStateBehavior extends ProcessBehavior {

	private static short idSeed = 1;

	public static final short S0 = 1;

	private final short informal0;

	private final short informal1;

	private final short informal2;

	private final short informal3;

	private final short informal4;

	private final short informal5;

	private final short informal6;

	private final short informal7;

	private final short informal8;

	private final short informal9;

	private final short informal10;

	private final short informal11;

	private final short informal12;

	private final short informal13;

	private final short informal14;

	private final short informal15;

	private final short informal16;

	private final short informal17;

	private final short informal18;

	private final short informal19;

	private final short informal20;

	private final short informal21;

	private final short informal22;

	private final short informal23;

	private final short informal24;

	private final short informal25;

	private final short informal26;

	private final short informal27;

	private final short informal28;

	private final short informal29;

	private final short informal30;

	private final short informal31;

	private final short informal32;

	private final short informal33;

	private final short informal34;

	private final short informal35;

	private final short informal36;

	private final short informal37;

	private final short informal38;

	private final short informal39;

	private final short informal40;

	private final short informal41;

	private final short informal42;

	private final short informal43;

	private final short informal44;

	private final short informal45;

	private final short informal46;

	private final short informal47;

	private final short informal48;

	public int level_of_alcapone;

	public int position_of_alarm1;

	public int position_of_alarm2;

	public OriginalAttacksStateBehavior(SymbolsTable symbols) {
		super("OriginalAttacksState", idSeed++, symbols);
		this.level_of_alcapone = 1;
		this.position_of_alarm1 = 1;
		this.position_of_alarm2 = 1;
		informal0 = symbols.getInformalId("go_from_Out_to_z1_by_door1");
		informal1 = symbols.getInformalId("go_from_z1_to_Out_by_door1");
		informal2 = symbols.getInformalId("open_door1");
		informal3 = symbols.getInformalId("close_door1");
		informal4 = symbols.getInformalId("go_from_z1_to_z2_by_door2");
		informal5 = symbols.getInformalId("go_from_z2_to_z1_by_door2");
		informal6 = symbols.getInformalId("open_door2");
		informal7 = symbols.getInformalId("close_door2");
		informal8 = symbols.getInformalId("go_from_z1_to_z7_by_door3");
		informal9 = symbols.getInformalId("go_from_z7_to_z1_by_door3");
		informal10 = symbols.getInformalId("open_door3");
		informal11 = symbols.getInformalId("close_door3");
		informal12 = symbols.getInformalId("go_from_z1_to_z8_by_door4");
		informal13 = symbols.getInformalId("go_from_z8_to_z1_by_door4");
		informal14 = symbols.getInformalId("open_door4");
		informal15 = symbols.getInformalId("close_door4");
		informal16 = symbols.getInformalId("go_from_z6_to_z5_by_door5");
		informal17 = symbols.getInformalId("go_from_z5_to_z6_by_door5");
		informal18 = symbols.getInformalId("open_door5");
		informal19 = symbols.getInformalId("close_door5");
		informal20 = symbols.getInformalId("go_from_z5_to_z4_by_door6");
		informal21 = symbols.getInformalId("go_from_z4_to_z5_by_door6");
		informal22 = symbols.getInformalId("open_door6");
		informal23 = symbols.getInformalId("close_door6");
		informal24 = symbols.getInformalId("go_from_z2_to_z3_by_lockedDoor1");
		informal25 = symbols.getInformalId("go_from_z3_to_z2_by_lockedDoor1");
		informal26 = symbols.getInformalId("open_lockedDoor1");
		informal27 = symbols.getInformalId("close_lockedDoor1");
		informal28 = symbols.getInformalId("unlock_lockedDoor1");
		informal29 = symbols.getInformalId("lock_lockedDoor1");
		informal30 = symbols.getInformalId("go_from_z3_to_z4_by_lockedDoor2");
		informal31 = symbols.getInformalId("go_from_z4_to_z3_by_lockedDoor2");
		informal32 = symbols.getInformalId("open_lockedDoor2");
		informal33 = symbols.getInformalId("close_lockedDoor2");
		informal34 = symbols.getInformalId("unlock_lockedDoor2");
		informal35 = symbols.getInformalId("lock_lockedDoor2");
		informal36 = symbols.getInformalId("go_from_z6_to_z7");
		informal37 = symbols.getInformalId("go_from_z7_to_z6");
		informal38 = symbols.getInformalId("go_from_z6_to_z8");
		informal39 = symbols.getInformalId("go_from_z8_to_z6");
		informal40 = symbols.getInformalId("deactivate_alarm1");
		informal41 = symbols.getInformalId("activate_alarm1");
		informal42 = symbols.getInformalId("deactivate_alarm2");
		informal43 = symbols.getInformalId("activate_alarm2");
		informal44 = symbols.getInformalId("take_key");
		informal45 = symbols.getInformalId("drop_key");
		informal46 = symbols.getInformalId("take_document");
		informal47 = symbols.getInformalId("drop_document");
		informal48 = symbols.getInformalId("reach_goal");
	}

	private final String[] stateNames = new String[] {"s0"};

	public int getStateId(String name) {
		for (short i=0; i<stateNames.length; i++) {
			if ( stateNames[i].equals(name) ) return (i+1);
		}
		throw new IllegalArgumentException("State '"+ name +"' doesn't exist in process '"+ this.name +"'.");
	}

	public String getStateName(short id) {
		return stateNames[id-1];
	}

	private final Transition transition0 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal0));
			if ((me.position_of_alcapone == 0) && (me.state_of_door1 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition1 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal1));
			if ((me.position_of_alcapone == 1) && (me.state_of_door1 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition2 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal2));
			if (((me.state_of_door1 == 0) && ((me.position_of_alcapone == 0) || (me.position_of_alcapone == 1))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door1 = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition3 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal3));
			if (((me.state_of_door1 == 1) && ((me.position_of_alcapone == 0) || (me.position_of_alcapone == 1))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door1 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition4 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal4));
			if ((me.position_of_alcapone == 1) && (me.state_of_door2 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 2;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition5 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal5));
			if ((me.position_of_alcapone == 2) && (me.state_of_door2 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition6 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal6));
			if (((me.state_of_door2 == 0) && ((me.position_of_alcapone == 1) || (me.position_of_alcapone == 2))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door2 = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition7 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal7));
			if (((me.state_of_door2 == 1) && ((me.position_of_alcapone == 1) || (me.position_of_alcapone == 2))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door2 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition8 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal8));
			if ((me.position_of_alcapone == 1) && (me.state_of_door3 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 7;
			if (me.state_of_alarm1 == 1) {
				me.alarm1_triggered = 1;
			}
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition9 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal9));
			if ((me.position_of_alcapone == 7) && (me.state_of_door3 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition10 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal10));
			if (((me.state_of_door3 == 0) && ((me.position_of_alcapone == 1) || (me.position_of_alcapone == 7))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door3 = 1;
			if (me.state_of_alarm1 == 1) {
				me.alarm1_triggered = 1;
			}
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition11 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal11));
			if (((me.state_of_door3 == 1) && ((me.position_of_alcapone == 1) || (me.position_of_alcapone == 7))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door3 = 0;
			if (me.state_of_alarm1 == 1) {
				me.alarm1_triggered = 1;
			}
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition12 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal12));
			if ((me.position_of_alcapone == 1) && (me.state_of_door4 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 8;
			if (me.state_of_alarm2 == 1) {
				me.alarm2_triggered = 1;
			}
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition13 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal13));
			if ((me.position_of_alcapone == 8) && (me.state_of_door4 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition14 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal14));
			if (((me.state_of_door4 == 0) && ((me.position_of_alcapone == 1) || (me.position_of_alcapone == 8))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door4 = 1;
			if (me.state_of_alarm2 == 1) {
				me.alarm2_triggered = 1;
			}
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition15 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal15));
			if (((me.state_of_door4 == 1) && ((me.position_of_alcapone == 1) || (me.position_of_alcapone == 8))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door4 = 0;
			if (me.state_of_alarm2 == 1) {
				me.alarm2_triggered = 1;
			}
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition16 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal16));
			if ((me.position_of_alcapone == 6) && (me.state_of_door5 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 5;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition17 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal17));
			if ((me.position_of_alcapone == 5) && (me.state_of_door5 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 6;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition18 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal18));
			if (((me.state_of_door5 == 0) && ((me.position_of_alcapone == 6) || (me.position_of_alcapone == 5))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door5 = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition19 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal19));
			if (((me.state_of_door5 == 1) && ((me.position_of_alcapone == 6) || (me.position_of_alcapone == 5))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door5 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition20 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal20));
			if ((me.position_of_alcapone == 5) && (me.state_of_door6 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 4;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition21 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal21));
			if ((me.position_of_alcapone == 4) && (me.state_of_door6 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 5;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition22 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal22));
			if (((me.state_of_door6 == 0) && ((me.position_of_alcapone == 5) || (me.position_of_alcapone == 4))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door6 = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition23 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal23));
			if (((me.state_of_door6 == 1) && ((me.position_of_alcapone == 5) || (me.position_of_alcapone == 4))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_door6 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition24 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal24));
			if ((me.position_of_alcapone == 2) && (me.state_of_lockedDoor1 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 3;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition25 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal25));
			if ((me.position_of_alcapone == 3) && (me.state_of_lockedDoor1 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 2;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition26 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal26));
			if (((me.state_of_lockedDoor1 == 0) && ((me.position_of_alcapone == 2) || (me.position_of_alcapone == 3))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_lockedDoor1 = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition27 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal27));
			if (((me.state_of_lockedDoor1 == 1) && ((me.position_of_alcapone == 2) || (me.position_of_alcapone == 3))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_lockedDoor1 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition28 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal28));
			if ((((me.state_of_lockedDoor1 == 2) && (me.position_of_key == 9)) && ((me.position_of_alcapone == 2) || (me.position_of_alcapone == 3))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_lockedDoor1 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition29 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal29));
			if ((((me.state_of_lockedDoor1 == 0) && (me.position_of_key == 9)) && ((me.position_of_alcapone == 2) || (me.position_of_alcapone == 3))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_lockedDoor1 = 2;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition30 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal30));
			if ((me.position_of_alcapone == 3) && (me.state_of_lockedDoor2 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 4;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition31 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal31));
			if ((me.position_of_alcapone == 4) && (me.state_of_lockedDoor2 == 1) == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 3;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition32 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal32));
			if (((me.state_of_lockedDoor2 == 0) && ((me.position_of_alcapone == 3) || (me.position_of_alcapone == 4))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_lockedDoor2 = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition33 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal33));
			if (((me.state_of_lockedDoor2 == 1) && ((me.position_of_alcapone == 3) || (me.position_of_alcapone == 4))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_lockedDoor2 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition34 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal34));
			if ((((me.state_of_lockedDoor2 == 2) && (me.position_of_key == 9)) && ((me.position_of_alcapone == 3) || (me.position_of_alcapone == 4))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_lockedDoor2 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition35 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal35));
			if ((((me.state_of_lockedDoor2 == 0) && (me.position_of_key == 9)) && ((me.position_of_alcapone == 3) || (me.position_of_alcapone == 4))) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_lockedDoor2 = 2;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition36 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal36));
			if (me.position_of_alcapone == 6 == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 7;
			if (me.state_of_alarm1 == 1) {
				me.alarm1_triggered = 1;
			}
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition37 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal37));
			if (me.position_of_alcapone == 7 == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 6;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition38 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal38));
			if (me.position_of_alcapone == 6 == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 8;
			if (me.state_of_alarm2 == 1) {
				me.alarm2_triggered = 1;
			}
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition39 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal39));
			if (me.position_of_alcapone == 8 == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_alcapone = 6;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition40 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal40));
			if (((me.state_of_alarm1 == 1) && (me.position_of_alcapone == position_of_alarm1)) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_alarm1 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition41 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal41));
			if (((me.state_of_alarm1 == 0) && (me.position_of_alcapone == position_of_alarm1)) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_alarm1 = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition42 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal42));
			if (((me.state_of_alarm2 == 1) && (me.position_of_alcapone == position_of_alarm2)) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_alarm2 = 0;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition43 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal43));
			if (((me.state_of_alarm2 == 0) && (me.position_of_alcapone == position_of_alarm2)) && ((level_of_alcapone) >= (0)) == true) {
			} else {
				if (true) { return false; }
			}
			me.state_of_alarm2 = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition44 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal44));
			if (me.position_of_alcapone == me.position_of_key == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_key = 9;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition45 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal45));
			if (me.position_of_key == 9 == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_key = me.position_of_alcapone;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition46 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal46));
			if (me.position_of_alcapone == me.position_of_document == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_document = 9;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition47 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal47));
			if (me.position_of_document == 9 == true) {
			} else {
				if (true) { return false; }
			}
			me.position_of_document = me.position_of_alcapone;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition transition48 = 	new Transition(id) {
		@Override
		public boolean guard(Configuration configuration, ExplorationContext context) {
			OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			if (me.state != S0) return false;
			return true;
		}

		@Override
		public boolean action(final ExplorationContext context, List<AtomicAction> actions, Configuration configuration) {
			final OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) configuration.behaviorConfigurations[id];
			actions.add(new InformalAction(id, informal48));
			if ((((me.position_of_alcapone == 0) && (me.position_of_document == 9)) && (me.alarm1_triggered == 0)) && (me.alarm2_triggered == 0) == true) {
			} else {
				if (true) { return false; }
			}
			me.goalReached = 1;
			me.state = S0;
			if (true) return true;
			if (true) { return true; } else { return true; }
		}

	}

;

	private final Transition[] transitions = 	new Transition[] {
		transition0,
		transition1,
		transition2,
		transition3,
		transition4,
		transition5,
		transition6,
		transition7,
		transition8,
		transition9,
		transition10,
		transition11,
		transition12,
		transition13,
		transition14,
		transition15,
		transition16,
		transition17,
		transition18,
		transition19,
		transition20,
		transition21,
		transition22,
		transition23,
		transition24,
		transition25,
		transition26,
		transition27,
		transition28,
		transition29,
		transition30,
		transition31,
		transition32,
		transition33,
		transition34,
		transition35,
		transition36,
		transition37,
		transition38,
		transition39,
		transition40,
		transition41,
		transition42,
		transition43,
		transition44,
		transition45,
		transition46,
		transition47,
		transition48,
	}
;

	@Override
	public Transition[] getTransitions(Configuration configuration) {
		return transitions;
	}

	@Override
	public BehaviorConfiguration createInitialConfiguration(Configuration conf) {
		OriginalAttacksStateConfiguration me = new OriginalAttacksStateConfiguration();
		//Initial action code.
		me.state = S0;
		return me;
	}

	public String toStringConfiguration(BehaviorConfiguration behaviorConfiguration) {
		OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) behaviorConfiguration;
		StringBuilder text = new StringBuilder();
		text.append("proc: ");
		text.append("'");
		text.append(name);
		text.append("' [");
		switch (me.state) {
		case OriginalAttacksStateBehavior.S0: 
			text.append("@s0,");
			break;
		}
		text.append("\n\t- position_of_alcapone=");
		text.append(Integer.toString(me.position_of_alcapone));
		text.append("\n\t- state_of_door1=");
		text.append(Integer.toString(me.state_of_door1));
		text.append("\n\t- state_of_door2=");
		text.append(Integer.toString(me.state_of_door2));
		text.append("\n\t- state_of_door3=");
		text.append(Integer.toString(me.state_of_door3));
		text.append("\n\t- state_of_door4=");
		text.append(Integer.toString(me.state_of_door4));
		text.append("\n\t- state_of_door5=");
		text.append(Integer.toString(me.state_of_door5));
		text.append("\n\t- state_of_door6=");
		text.append(Integer.toString(me.state_of_door6));
		text.append("\n\t- state_of_lockedDoor1=");
		text.append(Integer.toString(me.state_of_lockedDoor1));
		text.append("\n\t- state_of_lockedDoor2=");
		text.append(Integer.toString(me.state_of_lockedDoor2));
		text.append("\n\t- state_of_alarm1=");
		text.append(Integer.toString(me.state_of_alarm1));
		text.append("\n\t- alarm1_triggered=");
		text.append(Integer.toString(me.alarm1_triggered));
		text.append("\n\t- state_of_alarm2=");
		text.append(Integer.toString(me.state_of_alarm2));
		text.append("\n\t- alarm2_triggered=");
		text.append(Integer.toString(me.alarm2_triggered));
		text.append("\n\t- position_of_key=");
		text.append(Integer.toString(me.position_of_key));
		text.append("\n\t- position_of_document=");
		text.append(Integer.toString(me.position_of_document));
		text.append("\n\t- goalReached=");
		text.append(Integer.toString(me.goalReached));
		text.append("\n]");
		return text.toString();
	}

	public String toDotStringConfiguration(BehaviorConfiguration behaviorConfiguration) {
		OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) behaviorConfiguration;
		StringBuilder text = new StringBuilder();
		text.append("'");
		text.append(name);
		text.append("' [");
		switch (me.state) {
		case OriginalAttacksStateBehavior.S0: 
			text.append("@s0,");
			break;
		}
		text.append(" position_of_alcapone=");
		text.append(Integer.toString(me.position_of_alcapone));
		text.append(" state_of_door1=");
		text.append(Integer.toString(me.state_of_door1));
		text.append(" state_of_door2=");
		text.append(Integer.toString(me.state_of_door2));
		text.append(" state_of_door3=");
		text.append(Integer.toString(me.state_of_door3));
		text.append(" state_of_door4=");
		text.append(Integer.toString(me.state_of_door4));
		text.append(" state_of_door5=");
		text.append(Integer.toString(me.state_of_door5));
		text.append(" state_of_door6=");
		text.append(Integer.toString(me.state_of_door6));
		text.append(" state_of_lockedDoor1=");
		text.append(Integer.toString(me.state_of_lockedDoor1));
		text.append(" state_of_lockedDoor2=");
		text.append(Integer.toString(me.state_of_lockedDoor2));
		text.append(" state_of_alarm1=");
		text.append(Integer.toString(me.state_of_alarm1));
		text.append(" alarm1_triggered=");
		text.append(Integer.toString(me.alarm1_triggered));
		text.append(" state_of_alarm2=");
		text.append(Integer.toString(me.state_of_alarm2));
		text.append(" alarm2_triggered=");
		text.append(Integer.toString(me.alarm2_triggered));
		text.append(" position_of_key=");
		text.append(Integer.toString(me.position_of_key));
		text.append(" position_of_document=");
		text.append(Integer.toString(me.position_of_document));
		text.append(" goalReached=");
		text.append(Integer.toString(me.goalReached));
		text.append("]");
		return text.toString();
	}

	@Override
	public BehaviorConfiguration readConfiguration(Boost boost) {
		OriginalAttacksStateConfiguration me = new OriginalAttacksStateConfiguration();
		me.state = boost.readShort();
		me.position_of_alcapone = boost.readInt();
		me.state_of_door1 = boost.readInt();
		me.state_of_door2 = boost.readInt();
		me.state_of_door3 = boost.readInt();
		me.state_of_door4 = boost.readInt();
		me.state_of_door5 = boost.readInt();
		me.state_of_door6 = boost.readInt();
		me.state_of_lockedDoor1 = boost.readInt();
		me.state_of_lockedDoor2 = boost.readInt();
		me.state_of_alarm1 = boost.readInt();
		me.alarm1_triggered = boost.readInt();
		me.state_of_alarm2 = boost.readInt();
		me.alarm2_triggered = boost.readInt();
		me.position_of_key = boost.readInt();
		me.position_of_document = boost.readInt();
		me.goalReached = boost.readInt();
		return me;
	}

	@Override
	public void writeConfiguration(Boost boost, BehaviorConfiguration processConfiguration) {
		OriginalAttacksStateConfiguration me = (OriginalAttacksStateConfiguration) processConfiguration;
		boost.writeShort(me.state);
		boost.writeInt(me.position_of_alcapone);
		boost.writeInt(me.state_of_door1);
		boost.writeInt(me.state_of_door2);
		boost.writeInt(me.state_of_door3);
		boost.writeInt(me.state_of_door4);
		boost.writeInt(me.state_of_door5);
		boost.writeInt(me.state_of_door6);
		boost.writeInt(me.state_of_lockedDoor1);
		boost.writeInt(me.state_of_lockedDoor2);
		boost.writeInt(me.state_of_alarm1);
		boost.writeInt(me.alarm1_triggered);
		boost.writeInt(me.state_of_alarm2);
		boost.writeInt(me.alarm2_triggered);
		boost.writeInt(me.position_of_key);
		boost.writeInt(me.position_of_document);
		boost.writeInt(me.goalReached);
	}

}

