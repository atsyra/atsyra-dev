package demosite_full;

import obp.explorer.runtime.BehaviorConfiguration;

@SuppressWarnings("all")
public class OriginalAttacksStateConfiguration extends BehaviorConfiguration {

	public short state;

	public int position_of_alcapone;

	public int state_of_door1;

	public int state_of_door2;

	public int state_of_door3;

	public int state_of_door4;

	public int state_of_door5;

	public int state_of_door6;

	public int state_of_lockedDoor1;

	public int state_of_lockedDoor2;

	public int state_of_alarm1;

	public int alarm1_triggered;

	public int state_of_alarm2;

	public int alarm2_triggered;

	public int position_of_key;

	public int position_of_document;

	public int goalReached;

	//Create initial configuration
	public OriginalAttacksStateConfiguration() {
		this.position_of_alcapone = 0;
		this.state_of_door1 = 0;
		this.state_of_door2 = 0;
		this.state_of_door3 = 0;
		this.state_of_door4 = 0;
		this.state_of_door5 = 0;
		this.state_of_door6 = 0;
		this.state_of_lockedDoor1 = 2;
		this.state_of_lockedDoor2 = 2;
		this.state_of_alarm1 = 1;
		this.alarm1_triggered = 0;
		this.state_of_alarm2 = 1;
		this.alarm2_triggered = 0;
		this.position_of_key = 2;
		this.position_of_document = 4;
		this.goalReached = 0;
	}

	//Create configuration as a copy
	public OriginalAttacksStateConfiguration(OriginalAttacksStateConfiguration other) {
		this.state = other.state;
		this.position_of_alcapone = other.position_of_alcapone;
		this.state_of_door1 = other.state_of_door1;
		this.state_of_door2 = other.state_of_door2;
		this.state_of_door3 = other.state_of_door3;
		this.state_of_door4 = other.state_of_door4;
		this.state_of_door5 = other.state_of_door5;
		this.state_of_door6 = other.state_of_door6;
		this.state_of_lockedDoor1 = other.state_of_lockedDoor1;
		this.state_of_lockedDoor2 = other.state_of_lockedDoor2;
		this.state_of_alarm1 = other.state_of_alarm1;
		this.alarm1_triggered = other.alarm1_triggered;
		this.state_of_alarm2 = other.state_of_alarm2;
		this.alarm2_triggered = other.alarm2_triggered;
		this.position_of_key = other.position_of_key;
		this.position_of_document = other.position_of_document;
		this.goalReached = other.goalReached;
	}

	@Override
	public BehaviorConfiguration createCopy() {
		return new OriginalAttacksStateConfiguration(this);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof OriginalAttacksStateConfiguration) {
			OriginalAttacksStateConfiguration other = (OriginalAttacksStateConfiguration) object;
			if (state != other.state) return false;
			if (position_of_alcapone != other.position_of_alcapone) return false;
			if (state_of_door1 != other.state_of_door1) return false;
			if (state_of_door2 != other.state_of_door2) return false;
			if (state_of_door3 != other.state_of_door3) return false;
			if (state_of_door4 != other.state_of_door4) return false;
			if (state_of_door5 != other.state_of_door5) return false;
			if (state_of_door6 != other.state_of_door6) return false;
			if (state_of_lockedDoor1 != other.state_of_lockedDoor1) return false;
			if (state_of_lockedDoor2 != other.state_of_lockedDoor2) return false;
			if (state_of_alarm1 != other.state_of_alarm1) return false;
			if (alarm1_triggered != other.alarm1_triggered) return false;
			if (state_of_alarm2 != other.state_of_alarm2) return false;
			if (alarm2_triggered != other.alarm2_triggered) return false;
			if (position_of_key != other.position_of_key) return false;
			if (position_of_document != other.position_of_document) return false;
			if (goalReached != other.goalReached) return false;
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int total = 17;
		total = total * 37 + state;
		total = total * 37 + position_of_alcapone;
		total = total * 37 + state_of_door1;
		total = total * 37 + state_of_door2;
		total = total * 37 + state_of_door3;
		total = total * 37 + state_of_door4;
		total = total * 37 + state_of_door5;
		total = total * 37 + state_of_door6;
		total = total * 37 + state_of_lockedDoor1;
		total = total * 37 + state_of_lockedDoor2;
		total = total * 37 + state_of_alarm1;
		total = total * 37 + alarm1_triggered;
		total = total * 37 + state_of_alarm2;
		total = total * 37 + alarm2_triggered;
		total = total * 37 + position_of_key;
		total = total * 37 + position_of_document;
		total = total * 37 + goalReached;
		return total;
	}

}

