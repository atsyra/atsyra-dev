package demosite_full;

import obp.explorer.runtime.SymbolsTable;
import obp.explorer.runtime.Program;
import obp.explorer.runtime.Component;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

public class Demosite_fullRoot implements Program {

	public Map<String, String> getMetaInformations() {
		return Collections.emptyMap();
	}

	private Map<String, Integer> ioQueueSizeMap;

	public int getIoQueueSize(String name) {
		if ( ioQueueSizeMap == null ) {
			ioQueueSizeMap = new HashMap<String, Integer>();
		}
		Integer size = ioQueueSizeMap.get(name);
		if ( size == null ) {
			throw new IllegalArgumentException("Queue named '"+ name +"' doesn't exists.");
		}
		return size.intValue();
	}

	@Override
	public void unloadExternalLibrary() { }
	private Component root;

	@Override
	public Component getRoot(SymbolsTable symbols) {
		if (root == null) {
			symbols.newInformalId("go_from_Out_to_z1_by_door1");
			symbols.newInformalId("go_from_z1_to_Out_by_door1");
			symbols.newInformalId("open_door1");
			symbols.newInformalId("close_door1");
			symbols.newInformalId("go_from_z1_to_z2_by_door2");
			symbols.newInformalId("go_from_z2_to_z1_by_door2");
			symbols.newInformalId("open_door2");
			symbols.newInformalId("close_door2");
			symbols.newInformalId("go_from_z1_to_z7_by_door3");
			symbols.newInformalId("go_from_z7_to_z1_by_door3");
			symbols.newInformalId("open_door3");
			symbols.newInformalId("close_door3");
			symbols.newInformalId("go_from_z1_to_z8_by_door4");
			symbols.newInformalId("go_from_z8_to_z1_by_door4");
			symbols.newInformalId("open_door4");
			symbols.newInformalId("close_door4");
			symbols.newInformalId("go_from_z6_to_z5_by_door5");
			symbols.newInformalId("go_from_z5_to_z6_by_door5");
			symbols.newInformalId("open_door5");
			symbols.newInformalId("close_door5");
			symbols.newInformalId("go_from_z5_to_z4_by_door6");
			symbols.newInformalId("go_from_z4_to_z5_by_door6");
			symbols.newInformalId("open_door6");
			symbols.newInformalId("close_door6");
			symbols.newInformalId("go_from_z2_to_z3_by_lockedDoor1");
			symbols.newInformalId("go_from_z3_to_z2_by_lockedDoor1");
			symbols.newInformalId("open_lockedDoor1");
			symbols.newInformalId("close_lockedDoor1");
			symbols.newInformalId("unlock_lockedDoor1");
			symbols.newInformalId("lock_lockedDoor1");
			symbols.newInformalId("go_from_z3_to_z4_by_lockedDoor2");
			symbols.newInformalId("go_from_z4_to_z3_by_lockedDoor2");
			symbols.newInformalId("open_lockedDoor2");
			symbols.newInformalId("close_lockedDoor2");
			symbols.newInformalId("unlock_lockedDoor2");
			symbols.newInformalId("lock_lockedDoor2");
			symbols.newInformalId("go_from_z6_to_z7");
			symbols.newInformalId("go_from_z7_to_z6");
			symbols.newInformalId("go_from_z6_to_z8");
			symbols.newInformalId("go_from_z8_to_z6");
			symbols.newInformalId("deactivate_alarm1");
			symbols.newInformalId("activate_alarm1");
			symbols.newInformalId("deactivate_alarm2");
			symbols.newInformalId("activate_alarm2");
			symbols.newInformalId("take_key");
			symbols.newInformalId("drop_key");
			symbols.newInformalId("take_document");
			symbols.newInformalId("drop_document");
			symbols.newInformalId("reach_goal");
			root = new OriginalAttacksStateBehavior(symbols);
		}
		return root;
	}

}

