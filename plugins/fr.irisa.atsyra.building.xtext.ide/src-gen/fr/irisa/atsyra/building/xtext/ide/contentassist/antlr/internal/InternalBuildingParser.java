/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.xtext.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.irisa.atsyra.building.xtext.services.BuildingGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalBuildingParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'import'", "'Building'", "'{'", "'}'", "'Zone'", "'alarms'", "'('", "')'", "','", "'Alarm'", "'location'", "'Virtual'", "'access'", "'zone1'", "'zone2'", "'Door'", "'level'", "'keys'", "'Window'", "'inside'", "'outside'", "'BadgedDoor'", "'badges'", "'Item'", "'Attacker'", "'-'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalBuildingParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalBuildingParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalBuildingParser.tokenNames; }
    public String getGrammarFileName() { return "InternalBuilding.g"; }


    	private BuildingGrammarAccess grammarAccess;

    	public void setGrammarAccess(BuildingGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleBuildingModel"
    // InternalBuilding.g:53:1: entryRuleBuildingModel : ruleBuildingModel EOF ;
    public final void entryRuleBuildingModel() throws RecognitionException {
        try {
            // InternalBuilding.g:54:1: ( ruleBuildingModel EOF )
            // InternalBuilding.g:55:1: ruleBuildingModel EOF
            {
             before(grammarAccess.getBuildingModelRule()); 
            pushFollow(FOLLOW_1);
            ruleBuildingModel();

            state._fsp--;

             after(grammarAccess.getBuildingModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBuildingModel"


    // $ANTLR start "ruleBuildingModel"
    // InternalBuilding.g:62:1: ruleBuildingModel : ( ( rule__BuildingModel__Group__0 ) ) ;
    public final void ruleBuildingModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:66:2: ( ( ( rule__BuildingModel__Group__0 ) ) )
            // InternalBuilding.g:67:2: ( ( rule__BuildingModel__Group__0 ) )
            {
            // InternalBuilding.g:67:2: ( ( rule__BuildingModel__Group__0 ) )
            // InternalBuilding.g:68:3: ( rule__BuildingModel__Group__0 )
            {
             before(grammarAccess.getBuildingModelAccess().getGroup()); 
            // InternalBuilding.g:69:3: ( rule__BuildingModel__Group__0 )
            // InternalBuilding.g:69:4: rule__BuildingModel__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BuildingModel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBuildingModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBuildingModel"


    // $ANTLR start "entryRuleImport"
    // InternalBuilding.g:78:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalBuilding.g:79:1: ( ruleImport EOF )
            // InternalBuilding.g:80:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalBuilding.g:87:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:91:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalBuilding.g:92:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalBuilding.g:92:2: ( ( rule__Import__Group__0 ) )
            // InternalBuilding.g:93:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalBuilding.g:94:3: ( rule__Import__Group__0 )
            // InternalBuilding.g:94:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleBuilding"
    // InternalBuilding.g:103:1: entryRuleBuilding : ruleBuilding EOF ;
    public final void entryRuleBuilding() throws RecognitionException {
        try {
            // InternalBuilding.g:104:1: ( ruleBuilding EOF )
            // InternalBuilding.g:105:1: ruleBuilding EOF
            {
             before(grammarAccess.getBuildingRule()); 
            pushFollow(FOLLOW_1);
            ruleBuilding();

            state._fsp--;

             after(grammarAccess.getBuildingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBuilding"


    // $ANTLR start "ruleBuilding"
    // InternalBuilding.g:112:1: ruleBuilding : ( ( rule__Building__Group__0 ) ) ;
    public final void ruleBuilding() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:116:2: ( ( ( rule__Building__Group__0 ) ) )
            // InternalBuilding.g:117:2: ( ( rule__Building__Group__0 ) )
            {
            // InternalBuilding.g:117:2: ( ( rule__Building__Group__0 ) )
            // InternalBuilding.g:118:3: ( rule__Building__Group__0 )
            {
             before(grammarAccess.getBuildingAccess().getGroup()); 
            // InternalBuilding.g:119:3: ( rule__Building__Group__0 )
            // InternalBuilding.g:119:4: rule__Building__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Building__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBuildingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBuilding"


    // $ANTLR start "entryRuleEString"
    // InternalBuilding.g:128:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalBuilding.g:129:1: ( ruleEString EOF )
            // InternalBuilding.g:130:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalBuilding.g:137:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:141:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalBuilding.g:142:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalBuilding.g:142:2: ( ( rule__EString__Alternatives ) )
            // InternalBuilding.g:143:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalBuilding.g:144:3: ( rule__EString__Alternatives )
            // InternalBuilding.g:144:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleZone"
    // InternalBuilding.g:153:1: entryRuleZone : ruleZone EOF ;
    public final void entryRuleZone() throws RecognitionException {
        try {
            // InternalBuilding.g:154:1: ( ruleZone EOF )
            // InternalBuilding.g:155:1: ruleZone EOF
            {
             before(grammarAccess.getZoneRule()); 
            pushFollow(FOLLOW_1);
            ruleZone();

            state._fsp--;

             after(grammarAccess.getZoneRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleZone"


    // $ANTLR start "ruleZone"
    // InternalBuilding.g:162:1: ruleZone : ( ( rule__Zone__Group__0 ) ) ;
    public final void ruleZone() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:166:2: ( ( ( rule__Zone__Group__0 ) ) )
            // InternalBuilding.g:167:2: ( ( rule__Zone__Group__0 ) )
            {
            // InternalBuilding.g:167:2: ( ( rule__Zone__Group__0 ) )
            // InternalBuilding.g:168:3: ( rule__Zone__Group__0 )
            {
             before(grammarAccess.getZoneAccess().getGroup()); 
            // InternalBuilding.g:169:3: ( rule__Zone__Group__0 )
            // InternalBuilding.g:169:4: rule__Zone__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Zone__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getZoneAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleZone"


    // $ANTLR start "entryRuleAlarm"
    // InternalBuilding.g:178:1: entryRuleAlarm : ruleAlarm EOF ;
    public final void entryRuleAlarm() throws RecognitionException {
        try {
            // InternalBuilding.g:179:1: ( ruleAlarm EOF )
            // InternalBuilding.g:180:1: ruleAlarm EOF
            {
             before(grammarAccess.getAlarmRule()); 
            pushFollow(FOLLOW_1);
            ruleAlarm();

            state._fsp--;

             after(grammarAccess.getAlarmRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAlarm"


    // $ANTLR start "ruleAlarm"
    // InternalBuilding.g:187:1: ruleAlarm : ( ( rule__Alarm__Group__0 ) ) ;
    public final void ruleAlarm() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:191:2: ( ( ( rule__Alarm__Group__0 ) ) )
            // InternalBuilding.g:192:2: ( ( rule__Alarm__Group__0 ) )
            {
            // InternalBuilding.g:192:2: ( ( rule__Alarm__Group__0 ) )
            // InternalBuilding.g:193:3: ( rule__Alarm__Group__0 )
            {
             before(grammarAccess.getAlarmAccess().getGroup()); 
            // InternalBuilding.g:194:3: ( rule__Alarm__Group__0 )
            // InternalBuilding.g:194:4: rule__Alarm__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Alarm__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAlarmAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAlarm"


    // $ANTLR start "entryRuleAccess"
    // InternalBuilding.g:203:1: entryRuleAccess : ruleAccess EOF ;
    public final void entryRuleAccess() throws RecognitionException {
        try {
            // InternalBuilding.g:204:1: ( ruleAccess EOF )
            // InternalBuilding.g:205:1: ruleAccess EOF
            {
             before(grammarAccess.getAccessRule()); 
            pushFollow(FOLLOW_1);
            ruleAccess();

            state._fsp--;

             after(grammarAccess.getAccessRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAccess"


    // $ANTLR start "ruleAccess"
    // InternalBuilding.g:212:1: ruleAccess : ( ( rule__Access__Alternatives ) ) ;
    public final void ruleAccess() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:216:2: ( ( ( rule__Access__Alternatives ) ) )
            // InternalBuilding.g:217:2: ( ( rule__Access__Alternatives ) )
            {
            // InternalBuilding.g:217:2: ( ( rule__Access__Alternatives ) )
            // InternalBuilding.g:218:3: ( rule__Access__Alternatives )
            {
             before(grammarAccess.getAccessAccess().getAlternatives()); 
            // InternalBuilding.g:219:3: ( rule__Access__Alternatives )
            // InternalBuilding.g:219:4: rule__Access__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Access__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAccessAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAccess"


    // $ANTLR start "entryRuleVirtual"
    // InternalBuilding.g:228:1: entryRuleVirtual : ruleVirtual EOF ;
    public final void entryRuleVirtual() throws RecognitionException {
        try {
            // InternalBuilding.g:229:1: ( ruleVirtual EOF )
            // InternalBuilding.g:230:1: ruleVirtual EOF
            {
             before(grammarAccess.getVirtualRule()); 
            pushFollow(FOLLOW_1);
            ruleVirtual();

            state._fsp--;

             after(grammarAccess.getVirtualRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVirtual"


    // $ANTLR start "ruleVirtual"
    // InternalBuilding.g:237:1: ruleVirtual : ( ( rule__Virtual__Group__0 ) ) ;
    public final void ruleVirtual() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:241:2: ( ( ( rule__Virtual__Group__0 ) ) )
            // InternalBuilding.g:242:2: ( ( rule__Virtual__Group__0 ) )
            {
            // InternalBuilding.g:242:2: ( ( rule__Virtual__Group__0 ) )
            // InternalBuilding.g:243:3: ( rule__Virtual__Group__0 )
            {
             before(grammarAccess.getVirtualAccess().getGroup()); 
            // InternalBuilding.g:244:3: ( rule__Virtual__Group__0 )
            // InternalBuilding.g:244:4: rule__Virtual__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Virtual__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVirtualAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVirtual"


    // $ANTLR start "entryRuleDoor"
    // InternalBuilding.g:253:1: entryRuleDoor : ruleDoor EOF ;
    public final void entryRuleDoor() throws RecognitionException {
        try {
            // InternalBuilding.g:254:1: ( ruleDoor EOF )
            // InternalBuilding.g:255:1: ruleDoor EOF
            {
             before(grammarAccess.getDoorRule()); 
            pushFollow(FOLLOW_1);
            ruleDoor();

            state._fsp--;

             after(grammarAccess.getDoorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDoor"


    // $ANTLR start "ruleDoor"
    // InternalBuilding.g:262:1: ruleDoor : ( ( rule__Door__Group__0 ) ) ;
    public final void ruleDoor() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:266:2: ( ( ( rule__Door__Group__0 ) ) )
            // InternalBuilding.g:267:2: ( ( rule__Door__Group__0 ) )
            {
            // InternalBuilding.g:267:2: ( ( rule__Door__Group__0 ) )
            // InternalBuilding.g:268:3: ( rule__Door__Group__0 )
            {
             before(grammarAccess.getDoorAccess().getGroup()); 
            // InternalBuilding.g:269:3: ( rule__Door__Group__0 )
            // InternalBuilding.g:269:4: rule__Door__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Door__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDoorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDoor"


    // $ANTLR start "entryRuleWindow"
    // InternalBuilding.g:278:1: entryRuleWindow : ruleWindow EOF ;
    public final void entryRuleWindow() throws RecognitionException {
        try {
            // InternalBuilding.g:279:1: ( ruleWindow EOF )
            // InternalBuilding.g:280:1: ruleWindow EOF
            {
             before(grammarAccess.getWindowRule()); 
            pushFollow(FOLLOW_1);
            ruleWindow();

            state._fsp--;

             after(grammarAccess.getWindowRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWindow"


    // $ANTLR start "ruleWindow"
    // InternalBuilding.g:287:1: ruleWindow : ( ( rule__Window__Group__0 ) ) ;
    public final void ruleWindow() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:291:2: ( ( ( rule__Window__Group__0 ) ) )
            // InternalBuilding.g:292:2: ( ( rule__Window__Group__0 ) )
            {
            // InternalBuilding.g:292:2: ( ( rule__Window__Group__0 ) )
            // InternalBuilding.g:293:3: ( rule__Window__Group__0 )
            {
             before(grammarAccess.getWindowAccess().getGroup()); 
            // InternalBuilding.g:294:3: ( rule__Window__Group__0 )
            // InternalBuilding.g:294:4: rule__Window__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Window__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWindowAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWindow"


    // $ANTLR start "entryRuleBadged"
    // InternalBuilding.g:303:1: entryRuleBadged : ruleBadged EOF ;
    public final void entryRuleBadged() throws RecognitionException {
        try {
            // InternalBuilding.g:304:1: ( ruleBadged EOF )
            // InternalBuilding.g:305:1: ruleBadged EOF
            {
             before(grammarAccess.getBadgedRule()); 
            pushFollow(FOLLOW_1);
            ruleBadged();

            state._fsp--;

             after(grammarAccess.getBadgedRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBadged"


    // $ANTLR start "ruleBadged"
    // InternalBuilding.g:312:1: ruleBadged : ( ( rule__Badged__Group__0 ) ) ;
    public final void ruleBadged() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:316:2: ( ( ( rule__Badged__Group__0 ) ) )
            // InternalBuilding.g:317:2: ( ( rule__Badged__Group__0 ) )
            {
            // InternalBuilding.g:317:2: ( ( rule__Badged__Group__0 ) )
            // InternalBuilding.g:318:3: ( rule__Badged__Group__0 )
            {
             before(grammarAccess.getBadgedAccess().getGroup()); 
            // InternalBuilding.g:319:3: ( rule__Badged__Group__0 )
            // InternalBuilding.g:319:4: rule__Badged__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Badged__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBadgedAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBadged"


    // $ANTLR start "entryRuleItem"
    // InternalBuilding.g:328:1: entryRuleItem : ruleItem EOF ;
    public final void entryRuleItem() throws RecognitionException {
        try {
            // InternalBuilding.g:329:1: ( ruleItem EOF )
            // InternalBuilding.g:330:1: ruleItem EOF
            {
             before(grammarAccess.getItemRule()); 
            pushFollow(FOLLOW_1);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getItemRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleItem"


    // $ANTLR start "ruleItem"
    // InternalBuilding.g:337:1: ruleItem : ( ( rule__Item__Group__0 ) ) ;
    public final void ruleItem() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:341:2: ( ( ( rule__Item__Group__0 ) ) )
            // InternalBuilding.g:342:2: ( ( rule__Item__Group__0 ) )
            {
            // InternalBuilding.g:342:2: ( ( rule__Item__Group__0 ) )
            // InternalBuilding.g:343:3: ( rule__Item__Group__0 )
            {
             before(grammarAccess.getItemAccess().getGroup()); 
            // InternalBuilding.g:344:3: ( rule__Item__Group__0 )
            // InternalBuilding.g:344:4: rule__Item__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Item__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getItemAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleItem"


    // $ANTLR start "entryRuleAttacker"
    // InternalBuilding.g:353:1: entryRuleAttacker : ruleAttacker EOF ;
    public final void entryRuleAttacker() throws RecognitionException {
        try {
            // InternalBuilding.g:354:1: ( ruleAttacker EOF )
            // InternalBuilding.g:355:1: ruleAttacker EOF
            {
             before(grammarAccess.getAttackerRule()); 
            pushFollow(FOLLOW_1);
            ruleAttacker();

            state._fsp--;

             after(grammarAccess.getAttackerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttacker"


    // $ANTLR start "ruleAttacker"
    // InternalBuilding.g:362:1: ruleAttacker : ( ( rule__Attacker__Group__0 ) ) ;
    public final void ruleAttacker() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:366:2: ( ( ( rule__Attacker__Group__0 ) ) )
            // InternalBuilding.g:367:2: ( ( rule__Attacker__Group__0 ) )
            {
            // InternalBuilding.g:367:2: ( ( rule__Attacker__Group__0 ) )
            // InternalBuilding.g:368:3: ( rule__Attacker__Group__0 )
            {
             before(grammarAccess.getAttackerAccess().getGroup()); 
            // InternalBuilding.g:369:3: ( rule__Attacker__Group__0 )
            // InternalBuilding.g:369:4: rule__Attacker__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttacker"


    // $ANTLR start "entryRuleEInt"
    // InternalBuilding.g:378:1: entryRuleEInt : ruleEInt EOF ;
    public final void entryRuleEInt() throws RecognitionException {
        try {
            // InternalBuilding.g:379:1: ( ruleEInt EOF )
            // InternalBuilding.g:380:1: ruleEInt EOF
            {
             before(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getEIntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalBuilding.g:387:1: ruleEInt : ( ( rule__EInt__Group__0 ) ) ;
    public final void ruleEInt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:391:2: ( ( ( rule__EInt__Group__0 ) ) )
            // InternalBuilding.g:392:2: ( ( rule__EInt__Group__0 ) )
            {
            // InternalBuilding.g:392:2: ( ( rule__EInt__Group__0 ) )
            // InternalBuilding.g:393:3: ( rule__EInt__Group__0 )
            {
             before(grammarAccess.getEIntAccess().getGroup()); 
            // InternalBuilding.g:394:3: ( rule__EInt__Group__0 )
            // InternalBuilding.g:394:4: rule__EInt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEIntAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "rule__Building__Alternatives_4"
    // InternalBuilding.g:402:1: rule__Building__Alternatives_4 : ( ( ( rule__Building__AccessesAssignment_4_0 ) ) | ( ( rule__Building__AlarmsAssignment_4_1 ) ) | ( ( rule__Building__AttackersAssignment_4_2 ) ) | ( ( rule__Building__ItemsAssignment_4_3 ) ) | ( ( rule__Building__ZonesAssignment_4_4 ) ) );
    public final void rule__Building__Alternatives_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:406:1: ( ( ( rule__Building__AccessesAssignment_4_0 ) ) | ( ( rule__Building__AlarmsAssignment_4_1 ) ) | ( ( rule__Building__AttackersAssignment_4_2 ) ) | ( ( rule__Building__ItemsAssignment_4_3 ) ) | ( ( rule__Building__ZonesAssignment_4_4 ) ) )
            int alt1=5;
            switch ( input.LA(1) ) {
            case 22:
            case 26:
            case 29:
            case 32:
                {
                alt1=1;
                }
                break;
            case 20:
                {
                alt1=2;
                }
                break;
            case 35:
                {
                alt1=3;
                }
                break;
            case 34:
                {
                alt1=4;
                }
                break;
            case 15:
                {
                alt1=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalBuilding.g:407:2: ( ( rule__Building__AccessesAssignment_4_0 ) )
                    {
                    // InternalBuilding.g:407:2: ( ( rule__Building__AccessesAssignment_4_0 ) )
                    // InternalBuilding.g:408:3: ( rule__Building__AccessesAssignment_4_0 )
                    {
                     before(grammarAccess.getBuildingAccess().getAccessesAssignment_4_0()); 
                    // InternalBuilding.g:409:3: ( rule__Building__AccessesAssignment_4_0 )
                    // InternalBuilding.g:409:4: rule__Building__AccessesAssignment_4_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Building__AccessesAssignment_4_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBuildingAccess().getAccessesAssignment_4_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalBuilding.g:413:2: ( ( rule__Building__AlarmsAssignment_4_1 ) )
                    {
                    // InternalBuilding.g:413:2: ( ( rule__Building__AlarmsAssignment_4_1 ) )
                    // InternalBuilding.g:414:3: ( rule__Building__AlarmsAssignment_4_1 )
                    {
                     before(grammarAccess.getBuildingAccess().getAlarmsAssignment_4_1()); 
                    // InternalBuilding.g:415:3: ( rule__Building__AlarmsAssignment_4_1 )
                    // InternalBuilding.g:415:4: rule__Building__AlarmsAssignment_4_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Building__AlarmsAssignment_4_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getBuildingAccess().getAlarmsAssignment_4_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalBuilding.g:419:2: ( ( rule__Building__AttackersAssignment_4_2 ) )
                    {
                    // InternalBuilding.g:419:2: ( ( rule__Building__AttackersAssignment_4_2 ) )
                    // InternalBuilding.g:420:3: ( rule__Building__AttackersAssignment_4_2 )
                    {
                     before(grammarAccess.getBuildingAccess().getAttackersAssignment_4_2()); 
                    // InternalBuilding.g:421:3: ( rule__Building__AttackersAssignment_4_2 )
                    // InternalBuilding.g:421:4: rule__Building__AttackersAssignment_4_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Building__AttackersAssignment_4_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getBuildingAccess().getAttackersAssignment_4_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalBuilding.g:425:2: ( ( rule__Building__ItemsAssignment_4_3 ) )
                    {
                    // InternalBuilding.g:425:2: ( ( rule__Building__ItemsAssignment_4_3 ) )
                    // InternalBuilding.g:426:3: ( rule__Building__ItemsAssignment_4_3 )
                    {
                     before(grammarAccess.getBuildingAccess().getItemsAssignment_4_3()); 
                    // InternalBuilding.g:427:3: ( rule__Building__ItemsAssignment_4_3 )
                    // InternalBuilding.g:427:4: rule__Building__ItemsAssignment_4_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Building__ItemsAssignment_4_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getBuildingAccess().getItemsAssignment_4_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalBuilding.g:431:2: ( ( rule__Building__ZonesAssignment_4_4 ) )
                    {
                    // InternalBuilding.g:431:2: ( ( rule__Building__ZonesAssignment_4_4 ) )
                    // InternalBuilding.g:432:3: ( rule__Building__ZonesAssignment_4_4 )
                    {
                     before(grammarAccess.getBuildingAccess().getZonesAssignment_4_4()); 
                    // InternalBuilding.g:433:3: ( rule__Building__ZonesAssignment_4_4 )
                    // InternalBuilding.g:433:4: rule__Building__ZonesAssignment_4_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Building__ZonesAssignment_4_4();

                    state._fsp--;


                    }

                     after(grammarAccess.getBuildingAccess().getZonesAssignment_4_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Alternatives_4"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalBuilding.g:441:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:445:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalBuilding.g:446:2: ( RULE_STRING )
                    {
                    // InternalBuilding.g:446:2: ( RULE_STRING )
                    // InternalBuilding.g:447:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalBuilding.g:452:2: ( RULE_ID )
                    {
                    // InternalBuilding.g:452:2: ( RULE_ID )
                    // InternalBuilding.g:453:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__Access__Alternatives"
    // InternalBuilding.g:462:1: rule__Access__Alternatives : ( ( ruleVirtual ) | ( ruleDoor ) | ( ruleWindow ) | ( ruleBadged ) );
    public final void rule__Access__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:466:1: ( ( ruleVirtual ) | ( ruleDoor ) | ( ruleWindow ) | ( ruleBadged ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt3=1;
                }
                break;
            case 26:
                {
                alt3=2;
                }
                break;
            case 29:
                {
                alt3=3;
                }
                break;
            case 32:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalBuilding.g:467:2: ( ruleVirtual )
                    {
                    // InternalBuilding.g:467:2: ( ruleVirtual )
                    // InternalBuilding.g:468:3: ruleVirtual
                    {
                     before(grammarAccess.getAccessAccess().getVirtualParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleVirtual();

                    state._fsp--;

                     after(grammarAccess.getAccessAccess().getVirtualParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalBuilding.g:473:2: ( ruleDoor )
                    {
                    // InternalBuilding.g:473:2: ( ruleDoor )
                    // InternalBuilding.g:474:3: ruleDoor
                    {
                     before(grammarAccess.getAccessAccess().getDoorParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleDoor();

                    state._fsp--;

                     after(grammarAccess.getAccessAccess().getDoorParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalBuilding.g:479:2: ( ruleWindow )
                    {
                    // InternalBuilding.g:479:2: ( ruleWindow )
                    // InternalBuilding.g:480:3: ruleWindow
                    {
                     before(grammarAccess.getAccessAccess().getWindowParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleWindow();

                    state._fsp--;

                     after(grammarAccess.getAccessAccess().getWindowParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalBuilding.g:485:2: ( ruleBadged )
                    {
                    // InternalBuilding.g:485:2: ( ruleBadged )
                    // InternalBuilding.g:486:3: ruleBadged
                    {
                     before(grammarAccess.getAccessAccess().getBadgedParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleBadged();

                    state._fsp--;

                     after(grammarAccess.getAccessAccess().getBadgedParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Access__Alternatives"


    // $ANTLR start "rule__BuildingModel__Group__0"
    // InternalBuilding.g:495:1: rule__BuildingModel__Group__0 : rule__BuildingModel__Group__0__Impl rule__BuildingModel__Group__1 ;
    public final void rule__BuildingModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:499:1: ( rule__BuildingModel__Group__0__Impl rule__BuildingModel__Group__1 )
            // InternalBuilding.g:500:2: rule__BuildingModel__Group__0__Impl rule__BuildingModel__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__BuildingModel__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BuildingModel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingModel__Group__0"


    // $ANTLR start "rule__BuildingModel__Group__0__Impl"
    // InternalBuilding.g:507:1: rule__BuildingModel__Group__0__Impl : ( () ) ;
    public final void rule__BuildingModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:511:1: ( ( () ) )
            // InternalBuilding.g:512:1: ( () )
            {
            // InternalBuilding.g:512:1: ( () )
            // InternalBuilding.g:513:2: ()
            {
             before(grammarAccess.getBuildingModelAccess().getBuildingModelAction_0()); 
            // InternalBuilding.g:514:2: ()
            // InternalBuilding.g:514:3: 
            {
            }

             after(grammarAccess.getBuildingModelAccess().getBuildingModelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingModel__Group__0__Impl"


    // $ANTLR start "rule__BuildingModel__Group__1"
    // InternalBuilding.g:522:1: rule__BuildingModel__Group__1 : rule__BuildingModel__Group__1__Impl rule__BuildingModel__Group__2 ;
    public final void rule__BuildingModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:526:1: ( rule__BuildingModel__Group__1__Impl rule__BuildingModel__Group__2 )
            // InternalBuilding.g:527:2: rule__BuildingModel__Group__1__Impl rule__BuildingModel__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__BuildingModel__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BuildingModel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingModel__Group__1"


    // $ANTLR start "rule__BuildingModel__Group__1__Impl"
    // InternalBuilding.g:534:1: rule__BuildingModel__Group__1__Impl : ( ( rule__BuildingModel__ImportsAssignment_1 )* ) ;
    public final void rule__BuildingModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:538:1: ( ( ( rule__BuildingModel__ImportsAssignment_1 )* ) )
            // InternalBuilding.g:539:1: ( ( rule__BuildingModel__ImportsAssignment_1 )* )
            {
            // InternalBuilding.g:539:1: ( ( rule__BuildingModel__ImportsAssignment_1 )* )
            // InternalBuilding.g:540:2: ( rule__BuildingModel__ImportsAssignment_1 )*
            {
             before(grammarAccess.getBuildingModelAccess().getImportsAssignment_1()); 
            // InternalBuilding.g:541:2: ( rule__BuildingModel__ImportsAssignment_1 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==11) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalBuilding.g:541:3: rule__BuildingModel__ImportsAssignment_1
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__BuildingModel__ImportsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getBuildingModelAccess().getImportsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingModel__Group__1__Impl"


    // $ANTLR start "rule__BuildingModel__Group__2"
    // InternalBuilding.g:549:1: rule__BuildingModel__Group__2 : rule__BuildingModel__Group__2__Impl ;
    public final void rule__BuildingModel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:553:1: ( rule__BuildingModel__Group__2__Impl )
            // InternalBuilding.g:554:2: rule__BuildingModel__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BuildingModel__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingModel__Group__2"


    // $ANTLR start "rule__BuildingModel__Group__2__Impl"
    // InternalBuilding.g:560:1: rule__BuildingModel__Group__2__Impl : ( ( rule__BuildingModel__BuildingsAssignment_2 )* ) ;
    public final void rule__BuildingModel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:564:1: ( ( ( rule__BuildingModel__BuildingsAssignment_2 )* ) )
            // InternalBuilding.g:565:1: ( ( rule__BuildingModel__BuildingsAssignment_2 )* )
            {
            // InternalBuilding.g:565:1: ( ( rule__BuildingModel__BuildingsAssignment_2 )* )
            // InternalBuilding.g:566:2: ( rule__BuildingModel__BuildingsAssignment_2 )*
            {
             before(grammarAccess.getBuildingModelAccess().getBuildingsAssignment_2()); 
            // InternalBuilding.g:567:2: ( rule__BuildingModel__BuildingsAssignment_2 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==12) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalBuilding.g:567:3: rule__BuildingModel__BuildingsAssignment_2
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__BuildingModel__BuildingsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getBuildingModelAccess().getBuildingsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingModel__Group__2__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalBuilding.g:576:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:580:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalBuilding.g:581:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalBuilding.g:588:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:592:1: ( ( 'import' ) )
            // InternalBuilding.g:593:1: ( 'import' )
            {
            // InternalBuilding.g:593:1: ( 'import' )
            // InternalBuilding.g:594:2: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalBuilding.g:603:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:607:1: ( rule__Import__Group__1__Impl )
            // InternalBuilding.g:608:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalBuilding.g:614:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportURIAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:618:1: ( ( ( rule__Import__ImportURIAssignment_1 ) ) )
            // InternalBuilding.g:619:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            {
            // InternalBuilding.g:619:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            // InternalBuilding.g:620:2: ( rule__Import__ImportURIAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            // InternalBuilding.g:621:2: ( rule__Import__ImportURIAssignment_1 )
            // InternalBuilding.g:621:3: rule__Import__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__Building__Group__0"
    // InternalBuilding.g:630:1: rule__Building__Group__0 : rule__Building__Group__0__Impl rule__Building__Group__1 ;
    public final void rule__Building__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:634:1: ( rule__Building__Group__0__Impl rule__Building__Group__1 )
            // InternalBuilding.g:635:2: rule__Building__Group__0__Impl rule__Building__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Building__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Building__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__0"


    // $ANTLR start "rule__Building__Group__0__Impl"
    // InternalBuilding.g:642:1: rule__Building__Group__0__Impl : ( () ) ;
    public final void rule__Building__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:646:1: ( ( () ) )
            // InternalBuilding.g:647:1: ( () )
            {
            // InternalBuilding.g:647:1: ( () )
            // InternalBuilding.g:648:2: ()
            {
             before(grammarAccess.getBuildingAccess().getBuildingAction_0()); 
            // InternalBuilding.g:649:2: ()
            // InternalBuilding.g:649:3: 
            {
            }

             after(grammarAccess.getBuildingAccess().getBuildingAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__0__Impl"


    // $ANTLR start "rule__Building__Group__1"
    // InternalBuilding.g:657:1: rule__Building__Group__1 : rule__Building__Group__1__Impl rule__Building__Group__2 ;
    public final void rule__Building__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:661:1: ( rule__Building__Group__1__Impl rule__Building__Group__2 )
            // InternalBuilding.g:662:2: rule__Building__Group__1__Impl rule__Building__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Building__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Building__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__1"


    // $ANTLR start "rule__Building__Group__1__Impl"
    // InternalBuilding.g:669:1: rule__Building__Group__1__Impl : ( 'Building' ) ;
    public final void rule__Building__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:673:1: ( ( 'Building' ) )
            // InternalBuilding.g:674:1: ( 'Building' )
            {
            // InternalBuilding.g:674:1: ( 'Building' )
            // InternalBuilding.g:675:2: 'Building'
            {
             before(grammarAccess.getBuildingAccess().getBuildingKeyword_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getBuildingAccess().getBuildingKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__1__Impl"


    // $ANTLR start "rule__Building__Group__2"
    // InternalBuilding.g:684:1: rule__Building__Group__2 : rule__Building__Group__2__Impl rule__Building__Group__3 ;
    public final void rule__Building__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:688:1: ( rule__Building__Group__2__Impl rule__Building__Group__3 )
            // InternalBuilding.g:689:2: rule__Building__Group__2__Impl rule__Building__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Building__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Building__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__2"


    // $ANTLR start "rule__Building__Group__2__Impl"
    // InternalBuilding.g:696:1: rule__Building__Group__2__Impl : ( ( rule__Building__NameAssignment_2 ) ) ;
    public final void rule__Building__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:700:1: ( ( ( rule__Building__NameAssignment_2 ) ) )
            // InternalBuilding.g:701:1: ( ( rule__Building__NameAssignment_2 ) )
            {
            // InternalBuilding.g:701:1: ( ( rule__Building__NameAssignment_2 ) )
            // InternalBuilding.g:702:2: ( rule__Building__NameAssignment_2 )
            {
             before(grammarAccess.getBuildingAccess().getNameAssignment_2()); 
            // InternalBuilding.g:703:2: ( rule__Building__NameAssignment_2 )
            // InternalBuilding.g:703:3: rule__Building__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Building__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getBuildingAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__2__Impl"


    // $ANTLR start "rule__Building__Group__3"
    // InternalBuilding.g:711:1: rule__Building__Group__3 : rule__Building__Group__3__Impl rule__Building__Group__4 ;
    public final void rule__Building__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:715:1: ( rule__Building__Group__3__Impl rule__Building__Group__4 )
            // InternalBuilding.g:716:2: rule__Building__Group__3__Impl rule__Building__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Building__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Building__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__3"


    // $ANTLR start "rule__Building__Group__3__Impl"
    // InternalBuilding.g:723:1: rule__Building__Group__3__Impl : ( '{' ) ;
    public final void rule__Building__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:727:1: ( ( '{' ) )
            // InternalBuilding.g:728:1: ( '{' )
            {
            // InternalBuilding.g:728:1: ( '{' )
            // InternalBuilding.g:729:2: '{'
            {
             before(grammarAccess.getBuildingAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getBuildingAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__3__Impl"


    // $ANTLR start "rule__Building__Group__4"
    // InternalBuilding.g:738:1: rule__Building__Group__4 : rule__Building__Group__4__Impl rule__Building__Group__5 ;
    public final void rule__Building__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:742:1: ( rule__Building__Group__4__Impl rule__Building__Group__5 )
            // InternalBuilding.g:743:2: rule__Building__Group__4__Impl rule__Building__Group__5
            {
            pushFollow(FOLLOW_9);
            rule__Building__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Building__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__4"


    // $ANTLR start "rule__Building__Group__4__Impl"
    // InternalBuilding.g:750:1: rule__Building__Group__4__Impl : ( ( rule__Building__Alternatives_4 )* ) ;
    public final void rule__Building__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:754:1: ( ( ( rule__Building__Alternatives_4 )* ) )
            // InternalBuilding.g:755:1: ( ( rule__Building__Alternatives_4 )* )
            {
            // InternalBuilding.g:755:1: ( ( rule__Building__Alternatives_4 )* )
            // InternalBuilding.g:756:2: ( rule__Building__Alternatives_4 )*
            {
             before(grammarAccess.getBuildingAccess().getAlternatives_4()); 
            // InternalBuilding.g:757:2: ( rule__Building__Alternatives_4 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==15||LA6_0==20||LA6_0==22||LA6_0==26||LA6_0==29||LA6_0==32||(LA6_0>=34 && LA6_0<=35)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalBuilding.g:757:3: rule__Building__Alternatives_4
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Building__Alternatives_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getBuildingAccess().getAlternatives_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__4__Impl"


    // $ANTLR start "rule__Building__Group__5"
    // InternalBuilding.g:765:1: rule__Building__Group__5 : rule__Building__Group__5__Impl ;
    public final void rule__Building__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:769:1: ( rule__Building__Group__5__Impl )
            // InternalBuilding.g:770:2: rule__Building__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Building__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__5"


    // $ANTLR start "rule__Building__Group__5__Impl"
    // InternalBuilding.g:776:1: rule__Building__Group__5__Impl : ( '}' ) ;
    public final void rule__Building__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:780:1: ( ( '}' ) )
            // InternalBuilding.g:781:1: ( '}' )
            {
            // InternalBuilding.g:781:1: ( '}' )
            // InternalBuilding.g:782:2: '}'
            {
             before(grammarAccess.getBuildingAccess().getRightCurlyBracketKeyword_5()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getBuildingAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__Group__5__Impl"


    // $ANTLR start "rule__Zone__Group__0"
    // InternalBuilding.g:792:1: rule__Zone__Group__0 : rule__Zone__Group__0__Impl rule__Zone__Group__1 ;
    public final void rule__Zone__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:796:1: ( rule__Zone__Group__0__Impl rule__Zone__Group__1 )
            // InternalBuilding.g:797:2: rule__Zone__Group__0__Impl rule__Zone__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Zone__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__0"


    // $ANTLR start "rule__Zone__Group__0__Impl"
    // InternalBuilding.g:804:1: rule__Zone__Group__0__Impl : ( () ) ;
    public final void rule__Zone__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:808:1: ( ( () ) )
            // InternalBuilding.g:809:1: ( () )
            {
            // InternalBuilding.g:809:1: ( () )
            // InternalBuilding.g:810:2: ()
            {
             before(grammarAccess.getZoneAccess().getZoneAction_0()); 
            // InternalBuilding.g:811:2: ()
            // InternalBuilding.g:811:3: 
            {
            }

             after(grammarAccess.getZoneAccess().getZoneAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__0__Impl"


    // $ANTLR start "rule__Zone__Group__1"
    // InternalBuilding.g:819:1: rule__Zone__Group__1 : rule__Zone__Group__1__Impl rule__Zone__Group__2 ;
    public final void rule__Zone__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:823:1: ( rule__Zone__Group__1__Impl rule__Zone__Group__2 )
            // InternalBuilding.g:824:2: rule__Zone__Group__1__Impl rule__Zone__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Zone__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__1"


    // $ANTLR start "rule__Zone__Group__1__Impl"
    // InternalBuilding.g:831:1: rule__Zone__Group__1__Impl : ( 'Zone' ) ;
    public final void rule__Zone__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:835:1: ( ( 'Zone' ) )
            // InternalBuilding.g:836:1: ( 'Zone' )
            {
            // InternalBuilding.g:836:1: ( 'Zone' )
            // InternalBuilding.g:837:2: 'Zone'
            {
             before(grammarAccess.getZoneAccess().getZoneKeyword_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getZoneAccess().getZoneKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__1__Impl"


    // $ANTLR start "rule__Zone__Group__2"
    // InternalBuilding.g:846:1: rule__Zone__Group__2 : rule__Zone__Group__2__Impl rule__Zone__Group__3 ;
    public final void rule__Zone__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:850:1: ( rule__Zone__Group__2__Impl rule__Zone__Group__3 )
            // InternalBuilding.g:851:2: rule__Zone__Group__2__Impl rule__Zone__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Zone__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__2"


    // $ANTLR start "rule__Zone__Group__2__Impl"
    // InternalBuilding.g:858:1: rule__Zone__Group__2__Impl : ( ( rule__Zone__NameAssignment_2 ) ) ;
    public final void rule__Zone__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:862:1: ( ( ( rule__Zone__NameAssignment_2 ) ) )
            // InternalBuilding.g:863:1: ( ( rule__Zone__NameAssignment_2 ) )
            {
            // InternalBuilding.g:863:1: ( ( rule__Zone__NameAssignment_2 ) )
            // InternalBuilding.g:864:2: ( rule__Zone__NameAssignment_2 )
            {
             before(grammarAccess.getZoneAccess().getNameAssignment_2()); 
            // InternalBuilding.g:865:2: ( rule__Zone__NameAssignment_2 )
            // InternalBuilding.g:865:3: rule__Zone__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Zone__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getZoneAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__2__Impl"


    // $ANTLR start "rule__Zone__Group__3"
    // InternalBuilding.g:873:1: rule__Zone__Group__3 : rule__Zone__Group__3__Impl rule__Zone__Group__4 ;
    public final void rule__Zone__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:877:1: ( rule__Zone__Group__3__Impl rule__Zone__Group__4 )
            // InternalBuilding.g:878:2: rule__Zone__Group__3__Impl rule__Zone__Group__4
            {
            pushFollow(FOLLOW_12);
            rule__Zone__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__3"


    // $ANTLR start "rule__Zone__Group__3__Impl"
    // InternalBuilding.g:885:1: rule__Zone__Group__3__Impl : ( '{' ) ;
    public final void rule__Zone__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:889:1: ( ( '{' ) )
            // InternalBuilding.g:890:1: ( '{' )
            {
            // InternalBuilding.g:890:1: ( '{' )
            // InternalBuilding.g:891:2: '{'
            {
             before(grammarAccess.getZoneAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getZoneAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__3__Impl"


    // $ANTLR start "rule__Zone__Group__4"
    // InternalBuilding.g:900:1: rule__Zone__Group__4 : rule__Zone__Group__4__Impl rule__Zone__Group__5 ;
    public final void rule__Zone__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:904:1: ( rule__Zone__Group__4__Impl rule__Zone__Group__5 )
            // InternalBuilding.g:905:2: rule__Zone__Group__4__Impl rule__Zone__Group__5
            {
            pushFollow(FOLLOW_12);
            rule__Zone__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__4"


    // $ANTLR start "rule__Zone__Group__4__Impl"
    // InternalBuilding.g:912:1: rule__Zone__Group__4__Impl : ( ( rule__Zone__Group_4__0 )? ) ;
    public final void rule__Zone__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:916:1: ( ( ( rule__Zone__Group_4__0 )? ) )
            // InternalBuilding.g:917:1: ( ( rule__Zone__Group_4__0 )? )
            {
            // InternalBuilding.g:917:1: ( ( rule__Zone__Group_4__0 )? )
            // InternalBuilding.g:918:2: ( rule__Zone__Group_4__0 )?
            {
             before(grammarAccess.getZoneAccess().getGroup_4()); 
            // InternalBuilding.g:919:2: ( rule__Zone__Group_4__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==16) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalBuilding.g:919:3: rule__Zone__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Zone__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getZoneAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__4__Impl"


    // $ANTLR start "rule__Zone__Group__5"
    // InternalBuilding.g:927:1: rule__Zone__Group__5 : rule__Zone__Group__5__Impl ;
    public final void rule__Zone__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:931:1: ( rule__Zone__Group__5__Impl )
            // InternalBuilding.g:932:2: rule__Zone__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Zone__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__5"


    // $ANTLR start "rule__Zone__Group__5__Impl"
    // InternalBuilding.g:938:1: rule__Zone__Group__5__Impl : ( '}' ) ;
    public final void rule__Zone__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:942:1: ( ( '}' ) )
            // InternalBuilding.g:943:1: ( '}' )
            {
            // InternalBuilding.g:943:1: ( '}' )
            // InternalBuilding.g:944:2: '}'
            {
             before(grammarAccess.getZoneAccess().getRightCurlyBracketKeyword_5()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getZoneAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group__5__Impl"


    // $ANTLR start "rule__Zone__Group_4__0"
    // InternalBuilding.g:954:1: rule__Zone__Group_4__0 : rule__Zone__Group_4__0__Impl rule__Zone__Group_4__1 ;
    public final void rule__Zone__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:958:1: ( rule__Zone__Group_4__0__Impl rule__Zone__Group_4__1 )
            // InternalBuilding.g:959:2: rule__Zone__Group_4__0__Impl rule__Zone__Group_4__1
            {
            pushFollow(FOLLOW_13);
            rule__Zone__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__0"


    // $ANTLR start "rule__Zone__Group_4__0__Impl"
    // InternalBuilding.g:966:1: rule__Zone__Group_4__0__Impl : ( 'alarms' ) ;
    public final void rule__Zone__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:970:1: ( ( 'alarms' ) )
            // InternalBuilding.g:971:1: ( 'alarms' )
            {
            // InternalBuilding.g:971:1: ( 'alarms' )
            // InternalBuilding.g:972:2: 'alarms'
            {
             before(grammarAccess.getZoneAccess().getAlarmsKeyword_4_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getZoneAccess().getAlarmsKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__0__Impl"


    // $ANTLR start "rule__Zone__Group_4__1"
    // InternalBuilding.g:981:1: rule__Zone__Group_4__1 : rule__Zone__Group_4__1__Impl rule__Zone__Group_4__2 ;
    public final void rule__Zone__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:985:1: ( rule__Zone__Group_4__1__Impl rule__Zone__Group_4__2 )
            // InternalBuilding.g:986:2: rule__Zone__Group_4__1__Impl rule__Zone__Group_4__2
            {
            pushFollow(FOLLOW_6);
            rule__Zone__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__1"


    // $ANTLR start "rule__Zone__Group_4__1__Impl"
    // InternalBuilding.g:993:1: rule__Zone__Group_4__1__Impl : ( '(' ) ;
    public final void rule__Zone__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:997:1: ( ( '(' ) )
            // InternalBuilding.g:998:1: ( '(' )
            {
            // InternalBuilding.g:998:1: ( '(' )
            // InternalBuilding.g:999:2: '('
            {
             before(grammarAccess.getZoneAccess().getLeftParenthesisKeyword_4_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getZoneAccess().getLeftParenthesisKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__1__Impl"


    // $ANTLR start "rule__Zone__Group_4__2"
    // InternalBuilding.g:1008:1: rule__Zone__Group_4__2 : rule__Zone__Group_4__2__Impl rule__Zone__Group_4__3 ;
    public final void rule__Zone__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1012:1: ( rule__Zone__Group_4__2__Impl rule__Zone__Group_4__3 )
            // InternalBuilding.g:1013:2: rule__Zone__Group_4__2__Impl rule__Zone__Group_4__3
            {
            pushFollow(FOLLOW_14);
            rule__Zone__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__2"


    // $ANTLR start "rule__Zone__Group_4__2__Impl"
    // InternalBuilding.g:1020:1: rule__Zone__Group_4__2__Impl : ( ( rule__Zone__AlarmsAssignment_4_2 ) ) ;
    public final void rule__Zone__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1024:1: ( ( ( rule__Zone__AlarmsAssignment_4_2 ) ) )
            // InternalBuilding.g:1025:1: ( ( rule__Zone__AlarmsAssignment_4_2 ) )
            {
            // InternalBuilding.g:1025:1: ( ( rule__Zone__AlarmsAssignment_4_2 ) )
            // InternalBuilding.g:1026:2: ( rule__Zone__AlarmsAssignment_4_2 )
            {
             before(grammarAccess.getZoneAccess().getAlarmsAssignment_4_2()); 
            // InternalBuilding.g:1027:2: ( rule__Zone__AlarmsAssignment_4_2 )
            // InternalBuilding.g:1027:3: rule__Zone__AlarmsAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__Zone__AlarmsAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getZoneAccess().getAlarmsAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__2__Impl"


    // $ANTLR start "rule__Zone__Group_4__3"
    // InternalBuilding.g:1035:1: rule__Zone__Group_4__3 : rule__Zone__Group_4__3__Impl rule__Zone__Group_4__4 ;
    public final void rule__Zone__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1039:1: ( rule__Zone__Group_4__3__Impl rule__Zone__Group_4__4 )
            // InternalBuilding.g:1040:2: rule__Zone__Group_4__3__Impl rule__Zone__Group_4__4
            {
            pushFollow(FOLLOW_14);
            rule__Zone__Group_4__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group_4__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__3"


    // $ANTLR start "rule__Zone__Group_4__3__Impl"
    // InternalBuilding.g:1047:1: rule__Zone__Group_4__3__Impl : ( ( rule__Zone__Group_4_3__0 )* ) ;
    public final void rule__Zone__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1051:1: ( ( ( rule__Zone__Group_4_3__0 )* ) )
            // InternalBuilding.g:1052:1: ( ( rule__Zone__Group_4_3__0 )* )
            {
            // InternalBuilding.g:1052:1: ( ( rule__Zone__Group_4_3__0 )* )
            // InternalBuilding.g:1053:2: ( rule__Zone__Group_4_3__0 )*
            {
             before(grammarAccess.getZoneAccess().getGroup_4_3()); 
            // InternalBuilding.g:1054:2: ( rule__Zone__Group_4_3__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==19) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalBuilding.g:1054:3: rule__Zone__Group_4_3__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Zone__Group_4_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getZoneAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__3__Impl"


    // $ANTLR start "rule__Zone__Group_4__4"
    // InternalBuilding.g:1062:1: rule__Zone__Group_4__4 : rule__Zone__Group_4__4__Impl ;
    public final void rule__Zone__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1066:1: ( rule__Zone__Group_4__4__Impl )
            // InternalBuilding.g:1067:2: rule__Zone__Group_4__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Zone__Group_4__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__4"


    // $ANTLR start "rule__Zone__Group_4__4__Impl"
    // InternalBuilding.g:1073:1: rule__Zone__Group_4__4__Impl : ( ')' ) ;
    public final void rule__Zone__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1077:1: ( ( ')' ) )
            // InternalBuilding.g:1078:1: ( ')' )
            {
            // InternalBuilding.g:1078:1: ( ')' )
            // InternalBuilding.g:1079:2: ')'
            {
             before(grammarAccess.getZoneAccess().getRightParenthesisKeyword_4_4()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getZoneAccess().getRightParenthesisKeyword_4_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4__4__Impl"


    // $ANTLR start "rule__Zone__Group_4_3__0"
    // InternalBuilding.g:1089:1: rule__Zone__Group_4_3__0 : rule__Zone__Group_4_3__0__Impl rule__Zone__Group_4_3__1 ;
    public final void rule__Zone__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1093:1: ( rule__Zone__Group_4_3__0__Impl rule__Zone__Group_4_3__1 )
            // InternalBuilding.g:1094:2: rule__Zone__Group_4_3__0__Impl rule__Zone__Group_4_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Zone__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Zone__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4_3__0"


    // $ANTLR start "rule__Zone__Group_4_3__0__Impl"
    // InternalBuilding.g:1101:1: rule__Zone__Group_4_3__0__Impl : ( ',' ) ;
    public final void rule__Zone__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1105:1: ( ( ',' ) )
            // InternalBuilding.g:1106:1: ( ',' )
            {
            // InternalBuilding.g:1106:1: ( ',' )
            // InternalBuilding.g:1107:2: ','
            {
             before(grammarAccess.getZoneAccess().getCommaKeyword_4_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getZoneAccess().getCommaKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4_3__0__Impl"


    // $ANTLR start "rule__Zone__Group_4_3__1"
    // InternalBuilding.g:1116:1: rule__Zone__Group_4_3__1 : rule__Zone__Group_4_3__1__Impl ;
    public final void rule__Zone__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1120:1: ( rule__Zone__Group_4_3__1__Impl )
            // InternalBuilding.g:1121:2: rule__Zone__Group_4_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Zone__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4_3__1"


    // $ANTLR start "rule__Zone__Group_4_3__1__Impl"
    // InternalBuilding.g:1127:1: rule__Zone__Group_4_3__1__Impl : ( ( rule__Zone__AlarmsAssignment_4_3_1 ) ) ;
    public final void rule__Zone__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1131:1: ( ( ( rule__Zone__AlarmsAssignment_4_3_1 ) ) )
            // InternalBuilding.g:1132:1: ( ( rule__Zone__AlarmsAssignment_4_3_1 ) )
            {
            // InternalBuilding.g:1132:1: ( ( rule__Zone__AlarmsAssignment_4_3_1 ) )
            // InternalBuilding.g:1133:2: ( rule__Zone__AlarmsAssignment_4_3_1 )
            {
             before(grammarAccess.getZoneAccess().getAlarmsAssignment_4_3_1()); 
            // InternalBuilding.g:1134:2: ( rule__Zone__AlarmsAssignment_4_3_1 )
            // InternalBuilding.g:1134:3: rule__Zone__AlarmsAssignment_4_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Zone__AlarmsAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getZoneAccess().getAlarmsAssignment_4_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__Group_4_3__1__Impl"


    // $ANTLR start "rule__Alarm__Group__0"
    // InternalBuilding.g:1143:1: rule__Alarm__Group__0 : rule__Alarm__Group__0__Impl rule__Alarm__Group__1 ;
    public final void rule__Alarm__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1147:1: ( rule__Alarm__Group__0__Impl rule__Alarm__Group__1 )
            // InternalBuilding.g:1148:2: rule__Alarm__Group__0__Impl rule__Alarm__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Alarm__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Alarm__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__0"


    // $ANTLR start "rule__Alarm__Group__0__Impl"
    // InternalBuilding.g:1155:1: rule__Alarm__Group__0__Impl : ( () ) ;
    public final void rule__Alarm__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1159:1: ( ( () ) )
            // InternalBuilding.g:1160:1: ( () )
            {
            // InternalBuilding.g:1160:1: ( () )
            // InternalBuilding.g:1161:2: ()
            {
             before(grammarAccess.getAlarmAccess().getAlarmAction_0()); 
            // InternalBuilding.g:1162:2: ()
            // InternalBuilding.g:1162:3: 
            {
            }

             after(grammarAccess.getAlarmAccess().getAlarmAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__0__Impl"


    // $ANTLR start "rule__Alarm__Group__1"
    // InternalBuilding.g:1170:1: rule__Alarm__Group__1 : rule__Alarm__Group__1__Impl rule__Alarm__Group__2 ;
    public final void rule__Alarm__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1174:1: ( rule__Alarm__Group__1__Impl rule__Alarm__Group__2 )
            // InternalBuilding.g:1175:2: rule__Alarm__Group__1__Impl rule__Alarm__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Alarm__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Alarm__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__1"


    // $ANTLR start "rule__Alarm__Group__1__Impl"
    // InternalBuilding.g:1182:1: rule__Alarm__Group__1__Impl : ( 'Alarm' ) ;
    public final void rule__Alarm__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1186:1: ( ( 'Alarm' ) )
            // InternalBuilding.g:1187:1: ( 'Alarm' )
            {
            // InternalBuilding.g:1187:1: ( 'Alarm' )
            // InternalBuilding.g:1188:2: 'Alarm'
            {
             before(grammarAccess.getAlarmAccess().getAlarmKeyword_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getAlarmAccess().getAlarmKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__1__Impl"


    // $ANTLR start "rule__Alarm__Group__2"
    // InternalBuilding.g:1197:1: rule__Alarm__Group__2 : rule__Alarm__Group__2__Impl rule__Alarm__Group__3 ;
    public final void rule__Alarm__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1201:1: ( rule__Alarm__Group__2__Impl rule__Alarm__Group__3 )
            // InternalBuilding.g:1202:2: rule__Alarm__Group__2__Impl rule__Alarm__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Alarm__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Alarm__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__2"


    // $ANTLR start "rule__Alarm__Group__2__Impl"
    // InternalBuilding.g:1209:1: rule__Alarm__Group__2__Impl : ( ( rule__Alarm__NameAssignment_2 ) ) ;
    public final void rule__Alarm__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1213:1: ( ( ( rule__Alarm__NameAssignment_2 ) ) )
            // InternalBuilding.g:1214:1: ( ( rule__Alarm__NameAssignment_2 ) )
            {
            // InternalBuilding.g:1214:1: ( ( rule__Alarm__NameAssignment_2 ) )
            // InternalBuilding.g:1215:2: ( rule__Alarm__NameAssignment_2 )
            {
             before(grammarAccess.getAlarmAccess().getNameAssignment_2()); 
            // InternalBuilding.g:1216:2: ( rule__Alarm__NameAssignment_2 )
            // InternalBuilding.g:1216:3: rule__Alarm__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Alarm__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAlarmAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__2__Impl"


    // $ANTLR start "rule__Alarm__Group__3"
    // InternalBuilding.g:1224:1: rule__Alarm__Group__3 : rule__Alarm__Group__3__Impl rule__Alarm__Group__4 ;
    public final void rule__Alarm__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1228:1: ( rule__Alarm__Group__3__Impl rule__Alarm__Group__4 )
            // InternalBuilding.g:1229:2: rule__Alarm__Group__3__Impl rule__Alarm__Group__4
            {
            pushFollow(FOLLOW_17);
            rule__Alarm__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Alarm__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__3"


    // $ANTLR start "rule__Alarm__Group__3__Impl"
    // InternalBuilding.g:1236:1: rule__Alarm__Group__3__Impl : ( '{' ) ;
    public final void rule__Alarm__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1240:1: ( ( '{' ) )
            // InternalBuilding.g:1241:1: ( '{' )
            {
            // InternalBuilding.g:1241:1: ( '{' )
            // InternalBuilding.g:1242:2: '{'
            {
             before(grammarAccess.getAlarmAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getAlarmAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__3__Impl"


    // $ANTLR start "rule__Alarm__Group__4"
    // InternalBuilding.g:1251:1: rule__Alarm__Group__4 : rule__Alarm__Group__4__Impl rule__Alarm__Group__5 ;
    public final void rule__Alarm__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1255:1: ( rule__Alarm__Group__4__Impl rule__Alarm__Group__5 )
            // InternalBuilding.g:1256:2: rule__Alarm__Group__4__Impl rule__Alarm__Group__5
            {
            pushFollow(FOLLOW_17);
            rule__Alarm__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Alarm__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__4"


    // $ANTLR start "rule__Alarm__Group__4__Impl"
    // InternalBuilding.g:1263:1: rule__Alarm__Group__4__Impl : ( ( rule__Alarm__Group_4__0 )? ) ;
    public final void rule__Alarm__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1267:1: ( ( ( rule__Alarm__Group_4__0 )? ) )
            // InternalBuilding.g:1268:1: ( ( rule__Alarm__Group_4__0 )? )
            {
            // InternalBuilding.g:1268:1: ( ( rule__Alarm__Group_4__0 )? )
            // InternalBuilding.g:1269:2: ( rule__Alarm__Group_4__0 )?
            {
             before(grammarAccess.getAlarmAccess().getGroup_4()); 
            // InternalBuilding.g:1270:2: ( rule__Alarm__Group_4__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==21) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalBuilding.g:1270:3: rule__Alarm__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Alarm__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAlarmAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__4__Impl"


    // $ANTLR start "rule__Alarm__Group__5"
    // InternalBuilding.g:1278:1: rule__Alarm__Group__5 : rule__Alarm__Group__5__Impl ;
    public final void rule__Alarm__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1282:1: ( rule__Alarm__Group__5__Impl )
            // InternalBuilding.g:1283:2: rule__Alarm__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Alarm__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__5"


    // $ANTLR start "rule__Alarm__Group__5__Impl"
    // InternalBuilding.g:1289:1: rule__Alarm__Group__5__Impl : ( '}' ) ;
    public final void rule__Alarm__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1293:1: ( ( '}' ) )
            // InternalBuilding.g:1294:1: ( '}' )
            {
            // InternalBuilding.g:1294:1: ( '}' )
            // InternalBuilding.g:1295:2: '}'
            {
             before(grammarAccess.getAlarmAccess().getRightCurlyBracketKeyword_5()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getAlarmAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group__5__Impl"


    // $ANTLR start "rule__Alarm__Group_4__0"
    // InternalBuilding.g:1305:1: rule__Alarm__Group_4__0 : rule__Alarm__Group_4__0__Impl rule__Alarm__Group_4__1 ;
    public final void rule__Alarm__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1309:1: ( rule__Alarm__Group_4__0__Impl rule__Alarm__Group_4__1 )
            // InternalBuilding.g:1310:2: rule__Alarm__Group_4__0__Impl rule__Alarm__Group_4__1
            {
            pushFollow(FOLLOW_6);
            rule__Alarm__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Alarm__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group_4__0"


    // $ANTLR start "rule__Alarm__Group_4__0__Impl"
    // InternalBuilding.g:1317:1: rule__Alarm__Group_4__0__Impl : ( 'location' ) ;
    public final void rule__Alarm__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1321:1: ( ( 'location' ) )
            // InternalBuilding.g:1322:1: ( 'location' )
            {
            // InternalBuilding.g:1322:1: ( 'location' )
            // InternalBuilding.g:1323:2: 'location'
            {
             before(grammarAccess.getAlarmAccess().getLocationKeyword_4_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getAlarmAccess().getLocationKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group_4__0__Impl"


    // $ANTLR start "rule__Alarm__Group_4__1"
    // InternalBuilding.g:1332:1: rule__Alarm__Group_4__1 : rule__Alarm__Group_4__1__Impl ;
    public final void rule__Alarm__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1336:1: ( rule__Alarm__Group_4__1__Impl )
            // InternalBuilding.g:1337:2: rule__Alarm__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Alarm__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group_4__1"


    // $ANTLR start "rule__Alarm__Group_4__1__Impl"
    // InternalBuilding.g:1343:1: rule__Alarm__Group_4__1__Impl : ( ( rule__Alarm__LocationAssignment_4_1 ) ) ;
    public final void rule__Alarm__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1347:1: ( ( ( rule__Alarm__LocationAssignment_4_1 ) ) )
            // InternalBuilding.g:1348:1: ( ( rule__Alarm__LocationAssignment_4_1 ) )
            {
            // InternalBuilding.g:1348:1: ( ( rule__Alarm__LocationAssignment_4_1 ) )
            // InternalBuilding.g:1349:2: ( rule__Alarm__LocationAssignment_4_1 )
            {
             before(grammarAccess.getAlarmAccess().getLocationAssignment_4_1()); 
            // InternalBuilding.g:1350:2: ( rule__Alarm__LocationAssignment_4_1 )
            // InternalBuilding.g:1350:3: rule__Alarm__LocationAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Alarm__LocationAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getAlarmAccess().getLocationAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__Group_4__1__Impl"


    // $ANTLR start "rule__Virtual__Group__0"
    // InternalBuilding.g:1359:1: rule__Virtual__Group__0 : rule__Virtual__Group__0__Impl rule__Virtual__Group__1 ;
    public final void rule__Virtual__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1363:1: ( rule__Virtual__Group__0__Impl rule__Virtual__Group__1 )
            // InternalBuilding.g:1364:2: rule__Virtual__Group__0__Impl rule__Virtual__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__Virtual__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Virtual__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__0"


    // $ANTLR start "rule__Virtual__Group__0__Impl"
    // InternalBuilding.g:1371:1: rule__Virtual__Group__0__Impl : ( 'Virtual' ) ;
    public final void rule__Virtual__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1375:1: ( ( 'Virtual' ) )
            // InternalBuilding.g:1376:1: ( 'Virtual' )
            {
            // InternalBuilding.g:1376:1: ( 'Virtual' )
            // InternalBuilding.g:1377:2: 'Virtual'
            {
             before(grammarAccess.getVirtualAccess().getVirtualKeyword_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getVirtualAccess().getVirtualKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__0__Impl"


    // $ANTLR start "rule__Virtual__Group__1"
    // InternalBuilding.g:1386:1: rule__Virtual__Group__1 : rule__Virtual__Group__1__Impl rule__Virtual__Group__2 ;
    public final void rule__Virtual__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1390:1: ( rule__Virtual__Group__1__Impl rule__Virtual__Group__2 )
            // InternalBuilding.g:1391:2: rule__Virtual__Group__1__Impl rule__Virtual__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Virtual__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Virtual__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__1"


    // $ANTLR start "rule__Virtual__Group__1__Impl"
    // InternalBuilding.g:1398:1: rule__Virtual__Group__1__Impl : ( 'access' ) ;
    public final void rule__Virtual__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1402:1: ( ( 'access' ) )
            // InternalBuilding.g:1403:1: ( 'access' )
            {
            // InternalBuilding.g:1403:1: ( 'access' )
            // InternalBuilding.g:1404:2: 'access'
            {
             before(grammarAccess.getVirtualAccess().getAccessKeyword_1()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getVirtualAccess().getAccessKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__1__Impl"


    // $ANTLR start "rule__Virtual__Group__2"
    // InternalBuilding.g:1413:1: rule__Virtual__Group__2 : rule__Virtual__Group__2__Impl rule__Virtual__Group__3 ;
    public final void rule__Virtual__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1417:1: ( rule__Virtual__Group__2__Impl rule__Virtual__Group__3 )
            // InternalBuilding.g:1418:2: rule__Virtual__Group__2__Impl rule__Virtual__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Virtual__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Virtual__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__2"


    // $ANTLR start "rule__Virtual__Group__2__Impl"
    // InternalBuilding.g:1425:1: rule__Virtual__Group__2__Impl : ( ( rule__Virtual__NameAssignment_2 ) ) ;
    public final void rule__Virtual__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1429:1: ( ( ( rule__Virtual__NameAssignment_2 ) ) )
            // InternalBuilding.g:1430:1: ( ( rule__Virtual__NameAssignment_2 ) )
            {
            // InternalBuilding.g:1430:1: ( ( rule__Virtual__NameAssignment_2 ) )
            // InternalBuilding.g:1431:2: ( rule__Virtual__NameAssignment_2 )
            {
             before(grammarAccess.getVirtualAccess().getNameAssignment_2()); 
            // InternalBuilding.g:1432:2: ( rule__Virtual__NameAssignment_2 )
            // InternalBuilding.g:1432:3: rule__Virtual__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Virtual__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVirtualAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__2__Impl"


    // $ANTLR start "rule__Virtual__Group__3"
    // InternalBuilding.g:1440:1: rule__Virtual__Group__3 : rule__Virtual__Group__3__Impl rule__Virtual__Group__4 ;
    public final void rule__Virtual__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1444:1: ( rule__Virtual__Group__3__Impl rule__Virtual__Group__4 )
            // InternalBuilding.g:1445:2: rule__Virtual__Group__3__Impl rule__Virtual__Group__4
            {
            pushFollow(FOLLOW_19);
            rule__Virtual__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Virtual__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__3"


    // $ANTLR start "rule__Virtual__Group__3__Impl"
    // InternalBuilding.g:1452:1: rule__Virtual__Group__3__Impl : ( '{' ) ;
    public final void rule__Virtual__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1456:1: ( ( '{' ) )
            // InternalBuilding.g:1457:1: ( '{' )
            {
            // InternalBuilding.g:1457:1: ( '{' )
            // InternalBuilding.g:1458:2: '{'
            {
             before(grammarAccess.getVirtualAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getVirtualAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__3__Impl"


    // $ANTLR start "rule__Virtual__Group__4"
    // InternalBuilding.g:1467:1: rule__Virtual__Group__4 : rule__Virtual__Group__4__Impl rule__Virtual__Group__5 ;
    public final void rule__Virtual__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1471:1: ( rule__Virtual__Group__4__Impl rule__Virtual__Group__5 )
            // InternalBuilding.g:1472:2: rule__Virtual__Group__4__Impl rule__Virtual__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Virtual__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Virtual__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__4"


    // $ANTLR start "rule__Virtual__Group__4__Impl"
    // InternalBuilding.g:1479:1: rule__Virtual__Group__4__Impl : ( 'zone1' ) ;
    public final void rule__Virtual__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1483:1: ( ( 'zone1' ) )
            // InternalBuilding.g:1484:1: ( 'zone1' )
            {
            // InternalBuilding.g:1484:1: ( 'zone1' )
            // InternalBuilding.g:1485:2: 'zone1'
            {
             before(grammarAccess.getVirtualAccess().getZone1Keyword_4()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getVirtualAccess().getZone1Keyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__4__Impl"


    // $ANTLR start "rule__Virtual__Group__5"
    // InternalBuilding.g:1494:1: rule__Virtual__Group__5 : rule__Virtual__Group__5__Impl rule__Virtual__Group__6 ;
    public final void rule__Virtual__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1498:1: ( rule__Virtual__Group__5__Impl rule__Virtual__Group__6 )
            // InternalBuilding.g:1499:2: rule__Virtual__Group__5__Impl rule__Virtual__Group__6
            {
            pushFollow(FOLLOW_20);
            rule__Virtual__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Virtual__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__5"


    // $ANTLR start "rule__Virtual__Group__5__Impl"
    // InternalBuilding.g:1506:1: rule__Virtual__Group__5__Impl : ( ( rule__Virtual__Zone1Assignment_5 ) ) ;
    public final void rule__Virtual__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1510:1: ( ( ( rule__Virtual__Zone1Assignment_5 ) ) )
            // InternalBuilding.g:1511:1: ( ( rule__Virtual__Zone1Assignment_5 ) )
            {
            // InternalBuilding.g:1511:1: ( ( rule__Virtual__Zone1Assignment_5 ) )
            // InternalBuilding.g:1512:2: ( rule__Virtual__Zone1Assignment_5 )
            {
             before(grammarAccess.getVirtualAccess().getZone1Assignment_5()); 
            // InternalBuilding.g:1513:2: ( rule__Virtual__Zone1Assignment_5 )
            // InternalBuilding.g:1513:3: rule__Virtual__Zone1Assignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Virtual__Zone1Assignment_5();

            state._fsp--;


            }

             after(grammarAccess.getVirtualAccess().getZone1Assignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__5__Impl"


    // $ANTLR start "rule__Virtual__Group__6"
    // InternalBuilding.g:1521:1: rule__Virtual__Group__6 : rule__Virtual__Group__6__Impl rule__Virtual__Group__7 ;
    public final void rule__Virtual__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1525:1: ( rule__Virtual__Group__6__Impl rule__Virtual__Group__7 )
            // InternalBuilding.g:1526:2: rule__Virtual__Group__6__Impl rule__Virtual__Group__7
            {
            pushFollow(FOLLOW_6);
            rule__Virtual__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Virtual__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__6"


    // $ANTLR start "rule__Virtual__Group__6__Impl"
    // InternalBuilding.g:1533:1: rule__Virtual__Group__6__Impl : ( 'zone2' ) ;
    public final void rule__Virtual__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1537:1: ( ( 'zone2' ) )
            // InternalBuilding.g:1538:1: ( 'zone2' )
            {
            // InternalBuilding.g:1538:1: ( 'zone2' )
            // InternalBuilding.g:1539:2: 'zone2'
            {
             before(grammarAccess.getVirtualAccess().getZone2Keyword_6()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getVirtualAccess().getZone2Keyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__6__Impl"


    // $ANTLR start "rule__Virtual__Group__7"
    // InternalBuilding.g:1548:1: rule__Virtual__Group__7 : rule__Virtual__Group__7__Impl rule__Virtual__Group__8 ;
    public final void rule__Virtual__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1552:1: ( rule__Virtual__Group__7__Impl rule__Virtual__Group__8 )
            // InternalBuilding.g:1553:2: rule__Virtual__Group__7__Impl rule__Virtual__Group__8
            {
            pushFollow(FOLLOW_21);
            rule__Virtual__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Virtual__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__7"


    // $ANTLR start "rule__Virtual__Group__7__Impl"
    // InternalBuilding.g:1560:1: rule__Virtual__Group__7__Impl : ( ( rule__Virtual__Zone2Assignment_7 ) ) ;
    public final void rule__Virtual__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1564:1: ( ( ( rule__Virtual__Zone2Assignment_7 ) ) )
            // InternalBuilding.g:1565:1: ( ( rule__Virtual__Zone2Assignment_7 ) )
            {
            // InternalBuilding.g:1565:1: ( ( rule__Virtual__Zone2Assignment_7 ) )
            // InternalBuilding.g:1566:2: ( rule__Virtual__Zone2Assignment_7 )
            {
             before(grammarAccess.getVirtualAccess().getZone2Assignment_7()); 
            // InternalBuilding.g:1567:2: ( rule__Virtual__Zone2Assignment_7 )
            // InternalBuilding.g:1567:3: rule__Virtual__Zone2Assignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Virtual__Zone2Assignment_7();

            state._fsp--;


            }

             after(grammarAccess.getVirtualAccess().getZone2Assignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__7__Impl"


    // $ANTLR start "rule__Virtual__Group__8"
    // InternalBuilding.g:1575:1: rule__Virtual__Group__8 : rule__Virtual__Group__8__Impl ;
    public final void rule__Virtual__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1579:1: ( rule__Virtual__Group__8__Impl )
            // InternalBuilding.g:1580:2: rule__Virtual__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Virtual__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__8"


    // $ANTLR start "rule__Virtual__Group__8__Impl"
    // InternalBuilding.g:1586:1: rule__Virtual__Group__8__Impl : ( '}' ) ;
    public final void rule__Virtual__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1590:1: ( ( '}' ) )
            // InternalBuilding.g:1591:1: ( '}' )
            {
            // InternalBuilding.g:1591:1: ( '}' )
            // InternalBuilding.g:1592:2: '}'
            {
             before(grammarAccess.getVirtualAccess().getRightCurlyBracketKeyword_8()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getVirtualAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Group__8__Impl"


    // $ANTLR start "rule__Door__Group__0"
    // InternalBuilding.g:1602:1: rule__Door__Group__0 : rule__Door__Group__0__Impl rule__Door__Group__1 ;
    public final void rule__Door__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1606:1: ( rule__Door__Group__0__Impl rule__Door__Group__1 )
            // InternalBuilding.g:1607:2: rule__Door__Group__0__Impl rule__Door__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Door__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__0"


    // $ANTLR start "rule__Door__Group__0__Impl"
    // InternalBuilding.g:1614:1: rule__Door__Group__0__Impl : ( 'Door' ) ;
    public final void rule__Door__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1618:1: ( ( 'Door' ) )
            // InternalBuilding.g:1619:1: ( 'Door' )
            {
            // InternalBuilding.g:1619:1: ( 'Door' )
            // InternalBuilding.g:1620:2: 'Door'
            {
             before(grammarAccess.getDoorAccess().getDoorKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getDoorKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__0__Impl"


    // $ANTLR start "rule__Door__Group__1"
    // InternalBuilding.g:1629:1: rule__Door__Group__1 : rule__Door__Group__1__Impl rule__Door__Group__2 ;
    public final void rule__Door__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1633:1: ( rule__Door__Group__1__Impl rule__Door__Group__2 )
            // InternalBuilding.g:1634:2: rule__Door__Group__1__Impl rule__Door__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Door__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__1"


    // $ANTLR start "rule__Door__Group__1__Impl"
    // InternalBuilding.g:1641:1: rule__Door__Group__1__Impl : ( ( rule__Door__NameAssignment_1 ) ) ;
    public final void rule__Door__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1645:1: ( ( ( rule__Door__NameAssignment_1 ) ) )
            // InternalBuilding.g:1646:1: ( ( rule__Door__NameAssignment_1 ) )
            {
            // InternalBuilding.g:1646:1: ( ( rule__Door__NameAssignment_1 ) )
            // InternalBuilding.g:1647:2: ( rule__Door__NameAssignment_1 )
            {
             before(grammarAccess.getDoorAccess().getNameAssignment_1()); 
            // InternalBuilding.g:1648:2: ( rule__Door__NameAssignment_1 )
            // InternalBuilding.g:1648:3: rule__Door__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Door__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDoorAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__1__Impl"


    // $ANTLR start "rule__Door__Group__2"
    // InternalBuilding.g:1656:1: rule__Door__Group__2 : rule__Door__Group__2__Impl rule__Door__Group__3 ;
    public final void rule__Door__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1660:1: ( rule__Door__Group__2__Impl rule__Door__Group__3 )
            // InternalBuilding.g:1661:2: rule__Door__Group__2__Impl rule__Door__Group__3
            {
            pushFollow(FOLLOW_22);
            rule__Door__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__2"


    // $ANTLR start "rule__Door__Group__2__Impl"
    // InternalBuilding.g:1668:1: rule__Door__Group__2__Impl : ( '{' ) ;
    public final void rule__Door__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1672:1: ( ( '{' ) )
            // InternalBuilding.g:1673:1: ( '{' )
            {
            // InternalBuilding.g:1673:1: ( '{' )
            // InternalBuilding.g:1674:2: '{'
            {
             before(grammarAccess.getDoorAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__2__Impl"


    // $ANTLR start "rule__Door__Group__3"
    // InternalBuilding.g:1683:1: rule__Door__Group__3 : rule__Door__Group__3__Impl rule__Door__Group__4 ;
    public final void rule__Door__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1687:1: ( rule__Door__Group__3__Impl rule__Door__Group__4 )
            // InternalBuilding.g:1688:2: rule__Door__Group__3__Impl rule__Door__Group__4
            {
            pushFollow(FOLLOW_22);
            rule__Door__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__3"


    // $ANTLR start "rule__Door__Group__3__Impl"
    // InternalBuilding.g:1695:1: rule__Door__Group__3__Impl : ( ( rule__Door__Group_3__0 )? ) ;
    public final void rule__Door__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1699:1: ( ( ( rule__Door__Group_3__0 )? ) )
            // InternalBuilding.g:1700:1: ( ( rule__Door__Group_3__0 )? )
            {
            // InternalBuilding.g:1700:1: ( ( rule__Door__Group_3__0 )? )
            // InternalBuilding.g:1701:2: ( rule__Door__Group_3__0 )?
            {
             before(grammarAccess.getDoorAccess().getGroup_3()); 
            // InternalBuilding.g:1702:2: ( rule__Door__Group_3__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==27) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalBuilding.g:1702:3: rule__Door__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Door__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDoorAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__3__Impl"


    // $ANTLR start "rule__Door__Group__4"
    // InternalBuilding.g:1710:1: rule__Door__Group__4 : rule__Door__Group__4__Impl rule__Door__Group__5 ;
    public final void rule__Door__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1714:1: ( rule__Door__Group__4__Impl rule__Door__Group__5 )
            // InternalBuilding.g:1715:2: rule__Door__Group__4__Impl rule__Door__Group__5
            {
            pushFollow(FOLLOW_22);
            rule__Door__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__4"


    // $ANTLR start "rule__Door__Group__4__Impl"
    // InternalBuilding.g:1722:1: rule__Door__Group__4__Impl : ( ( rule__Door__Group_4__0 )? ) ;
    public final void rule__Door__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1726:1: ( ( ( rule__Door__Group_4__0 )? ) )
            // InternalBuilding.g:1727:1: ( ( rule__Door__Group_4__0 )? )
            {
            // InternalBuilding.g:1727:1: ( ( rule__Door__Group_4__0 )? )
            // InternalBuilding.g:1728:2: ( rule__Door__Group_4__0 )?
            {
             before(grammarAccess.getDoorAccess().getGroup_4()); 
            // InternalBuilding.g:1729:2: ( rule__Door__Group_4__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==24) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalBuilding.g:1729:3: rule__Door__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Door__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDoorAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__4__Impl"


    // $ANTLR start "rule__Door__Group__5"
    // InternalBuilding.g:1737:1: rule__Door__Group__5 : rule__Door__Group__5__Impl rule__Door__Group__6 ;
    public final void rule__Door__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1741:1: ( rule__Door__Group__5__Impl rule__Door__Group__6 )
            // InternalBuilding.g:1742:2: rule__Door__Group__5__Impl rule__Door__Group__6
            {
            pushFollow(FOLLOW_22);
            rule__Door__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__5"


    // $ANTLR start "rule__Door__Group__5__Impl"
    // InternalBuilding.g:1749:1: rule__Door__Group__5__Impl : ( ( rule__Door__Group_5__0 )? ) ;
    public final void rule__Door__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1753:1: ( ( ( rule__Door__Group_5__0 )? ) )
            // InternalBuilding.g:1754:1: ( ( rule__Door__Group_5__0 )? )
            {
            // InternalBuilding.g:1754:1: ( ( rule__Door__Group_5__0 )? )
            // InternalBuilding.g:1755:2: ( rule__Door__Group_5__0 )?
            {
             before(grammarAccess.getDoorAccess().getGroup_5()); 
            // InternalBuilding.g:1756:2: ( rule__Door__Group_5__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==25) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalBuilding.g:1756:3: rule__Door__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Door__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDoorAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__5__Impl"


    // $ANTLR start "rule__Door__Group__6"
    // InternalBuilding.g:1764:1: rule__Door__Group__6 : rule__Door__Group__6__Impl rule__Door__Group__7 ;
    public final void rule__Door__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1768:1: ( rule__Door__Group__6__Impl rule__Door__Group__7 )
            // InternalBuilding.g:1769:2: rule__Door__Group__6__Impl rule__Door__Group__7
            {
            pushFollow(FOLLOW_22);
            rule__Door__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__6"


    // $ANTLR start "rule__Door__Group__6__Impl"
    // InternalBuilding.g:1776:1: rule__Door__Group__6__Impl : ( ( rule__Door__Group_6__0 )? ) ;
    public final void rule__Door__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1780:1: ( ( ( rule__Door__Group_6__0 )? ) )
            // InternalBuilding.g:1781:1: ( ( rule__Door__Group_6__0 )? )
            {
            // InternalBuilding.g:1781:1: ( ( rule__Door__Group_6__0 )? )
            // InternalBuilding.g:1782:2: ( rule__Door__Group_6__0 )?
            {
             before(grammarAccess.getDoorAccess().getGroup_6()); 
            // InternalBuilding.g:1783:2: ( rule__Door__Group_6__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==28) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalBuilding.g:1783:3: rule__Door__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Door__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDoorAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__6__Impl"


    // $ANTLR start "rule__Door__Group__7"
    // InternalBuilding.g:1791:1: rule__Door__Group__7 : rule__Door__Group__7__Impl rule__Door__Group__8 ;
    public final void rule__Door__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1795:1: ( rule__Door__Group__7__Impl rule__Door__Group__8 )
            // InternalBuilding.g:1796:2: rule__Door__Group__7__Impl rule__Door__Group__8
            {
            pushFollow(FOLLOW_22);
            rule__Door__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__7"


    // $ANTLR start "rule__Door__Group__7__Impl"
    // InternalBuilding.g:1803:1: rule__Door__Group__7__Impl : ( ( rule__Door__Group_7__0 )? ) ;
    public final void rule__Door__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1807:1: ( ( ( rule__Door__Group_7__0 )? ) )
            // InternalBuilding.g:1808:1: ( ( rule__Door__Group_7__0 )? )
            {
            // InternalBuilding.g:1808:1: ( ( rule__Door__Group_7__0 )? )
            // InternalBuilding.g:1809:2: ( rule__Door__Group_7__0 )?
            {
             before(grammarAccess.getDoorAccess().getGroup_7()); 
            // InternalBuilding.g:1810:2: ( rule__Door__Group_7__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==16) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalBuilding.g:1810:3: rule__Door__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Door__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDoorAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__7__Impl"


    // $ANTLR start "rule__Door__Group__8"
    // InternalBuilding.g:1818:1: rule__Door__Group__8 : rule__Door__Group__8__Impl ;
    public final void rule__Door__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1822:1: ( rule__Door__Group__8__Impl )
            // InternalBuilding.g:1823:2: rule__Door__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Door__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__8"


    // $ANTLR start "rule__Door__Group__8__Impl"
    // InternalBuilding.g:1829:1: rule__Door__Group__8__Impl : ( '}' ) ;
    public final void rule__Door__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1833:1: ( ( '}' ) )
            // InternalBuilding.g:1834:1: ( '}' )
            {
            // InternalBuilding.g:1834:1: ( '}' )
            // InternalBuilding.g:1835:2: '}'
            {
             before(grammarAccess.getDoorAccess().getRightCurlyBracketKeyword_8()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group__8__Impl"


    // $ANTLR start "rule__Door__Group_3__0"
    // InternalBuilding.g:1845:1: rule__Door__Group_3__0 : rule__Door__Group_3__0__Impl rule__Door__Group_3__1 ;
    public final void rule__Door__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1849:1: ( rule__Door__Group_3__0__Impl rule__Door__Group_3__1 )
            // InternalBuilding.g:1850:2: rule__Door__Group_3__0__Impl rule__Door__Group_3__1
            {
            pushFollow(FOLLOW_23);
            rule__Door__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_3__0"


    // $ANTLR start "rule__Door__Group_3__0__Impl"
    // InternalBuilding.g:1857:1: rule__Door__Group_3__0__Impl : ( 'level' ) ;
    public final void rule__Door__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1861:1: ( ( 'level' ) )
            // InternalBuilding.g:1862:1: ( 'level' )
            {
            // InternalBuilding.g:1862:1: ( 'level' )
            // InternalBuilding.g:1863:2: 'level'
            {
             before(grammarAccess.getDoorAccess().getLevelKeyword_3_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getLevelKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_3__0__Impl"


    // $ANTLR start "rule__Door__Group_3__1"
    // InternalBuilding.g:1872:1: rule__Door__Group_3__1 : rule__Door__Group_3__1__Impl ;
    public final void rule__Door__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1876:1: ( rule__Door__Group_3__1__Impl )
            // InternalBuilding.g:1877:2: rule__Door__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Door__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_3__1"


    // $ANTLR start "rule__Door__Group_3__1__Impl"
    // InternalBuilding.g:1883:1: rule__Door__Group_3__1__Impl : ( ( rule__Door__LevelAssignment_3_1 ) ) ;
    public final void rule__Door__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1887:1: ( ( ( rule__Door__LevelAssignment_3_1 ) ) )
            // InternalBuilding.g:1888:1: ( ( rule__Door__LevelAssignment_3_1 ) )
            {
            // InternalBuilding.g:1888:1: ( ( rule__Door__LevelAssignment_3_1 ) )
            // InternalBuilding.g:1889:2: ( rule__Door__LevelAssignment_3_1 )
            {
             before(grammarAccess.getDoorAccess().getLevelAssignment_3_1()); 
            // InternalBuilding.g:1890:2: ( rule__Door__LevelAssignment_3_1 )
            // InternalBuilding.g:1890:3: rule__Door__LevelAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Door__LevelAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getDoorAccess().getLevelAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_3__1__Impl"


    // $ANTLR start "rule__Door__Group_4__0"
    // InternalBuilding.g:1899:1: rule__Door__Group_4__0 : rule__Door__Group_4__0__Impl rule__Door__Group_4__1 ;
    public final void rule__Door__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1903:1: ( rule__Door__Group_4__0__Impl rule__Door__Group_4__1 )
            // InternalBuilding.g:1904:2: rule__Door__Group_4__0__Impl rule__Door__Group_4__1
            {
            pushFollow(FOLLOW_6);
            rule__Door__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_4__0"


    // $ANTLR start "rule__Door__Group_4__0__Impl"
    // InternalBuilding.g:1911:1: rule__Door__Group_4__0__Impl : ( 'zone1' ) ;
    public final void rule__Door__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1915:1: ( ( 'zone1' ) )
            // InternalBuilding.g:1916:1: ( 'zone1' )
            {
            // InternalBuilding.g:1916:1: ( 'zone1' )
            // InternalBuilding.g:1917:2: 'zone1'
            {
             before(grammarAccess.getDoorAccess().getZone1Keyword_4_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getZone1Keyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_4__0__Impl"


    // $ANTLR start "rule__Door__Group_4__1"
    // InternalBuilding.g:1926:1: rule__Door__Group_4__1 : rule__Door__Group_4__1__Impl ;
    public final void rule__Door__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1930:1: ( rule__Door__Group_4__1__Impl )
            // InternalBuilding.g:1931:2: rule__Door__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Door__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_4__1"


    // $ANTLR start "rule__Door__Group_4__1__Impl"
    // InternalBuilding.g:1937:1: rule__Door__Group_4__1__Impl : ( ( rule__Door__Zone1Assignment_4_1 ) ) ;
    public final void rule__Door__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1941:1: ( ( ( rule__Door__Zone1Assignment_4_1 ) ) )
            // InternalBuilding.g:1942:1: ( ( rule__Door__Zone1Assignment_4_1 ) )
            {
            // InternalBuilding.g:1942:1: ( ( rule__Door__Zone1Assignment_4_1 ) )
            // InternalBuilding.g:1943:2: ( rule__Door__Zone1Assignment_4_1 )
            {
             before(grammarAccess.getDoorAccess().getZone1Assignment_4_1()); 
            // InternalBuilding.g:1944:2: ( rule__Door__Zone1Assignment_4_1 )
            // InternalBuilding.g:1944:3: rule__Door__Zone1Assignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Door__Zone1Assignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getDoorAccess().getZone1Assignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_4__1__Impl"


    // $ANTLR start "rule__Door__Group_5__0"
    // InternalBuilding.g:1953:1: rule__Door__Group_5__0 : rule__Door__Group_5__0__Impl rule__Door__Group_5__1 ;
    public final void rule__Door__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1957:1: ( rule__Door__Group_5__0__Impl rule__Door__Group_5__1 )
            // InternalBuilding.g:1958:2: rule__Door__Group_5__0__Impl rule__Door__Group_5__1
            {
            pushFollow(FOLLOW_6);
            rule__Door__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_5__0"


    // $ANTLR start "rule__Door__Group_5__0__Impl"
    // InternalBuilding.g:1965:1: rule__Door__Group_5__0__Impl : ( 'zone2' ) ;
    public final void rule__Door__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1969:1: ( ( 'zone2' ) )
            // InternalBuilding.g:1970:1: ( 'zone2' )
            {
            // InternalBuilding.g:1970:1: ( 'zone2' )
            // InternalBuilding.g:1971:2: 'zone2'
            {
             before(grammarAccess.getDoorAccess().getZone2Keyword_5_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getZone2Keyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_5__0__Impl"


    // $ANTLR start "rule__Door__Group_5__1"
    // InternalBuilding.g:1980:1: rule__Door__Group_5__1 : rule__Door__Group_5__1__Impl ;
    public final void rule__Door__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1984:1: ( rule__Door__Group_5__1__Impl )
            // InternalBuilding.g:1985:2: rule__Door__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Door__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_5__1"


    // $ANTLR start "rule__Door__Group_5__1__Impl"
    // InternalBuilding.g:1991:1: rule__Door__Group_5__1__Impl : ( ( rule__Door__Zone2Assignment_5_1 ) ) ;
    public final void rule__Door__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:1995:1: ( ( ( rule__Door__Zone2Assignment_5_1 ) ) )
            // InternalBuilding.g:1996:1: ( ( rule__Door__Zone2Assignment_5_1 ) )
            {
            // InternalBuilding.g:1996:1: ( ( rule__Door__Zone2Assignment_5_1 ) )
            // InternalBuilding.g:1997:2: ( rule__Door__Zone2Assignment_5_1 )
            {
             before(grammarAccess.getDoorAccess().getZone2Assignment_5_1()); 
            // InternalBuilding.g:1998:2: ( rule__Door__Zone2Assignment_5_1 )
            // InternalBuilding.g:1998:3: rule__Door__Zone2Assignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Door__Zone2Assignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getDoorAccess().getZone2Assignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_5__1__Impl"


    // $ANTLR start "rule__Door__Group_6__0"
    // InternalBuilding.g:2007:1: rule__Door__Group_6__0 : rule__Door__Group_6__0__Impl rule__Door__Group_6__1 ;
    public final void rule__Door__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2011:1: ( rule__Door__Group_6__0__Impl rule__Door__Group_6__1 )
            // InternalBuilding.g:2012:2: rule__Door__Group_6__0__Impl rule__Door__Group_6__1
            {
            pushFollow(FOLLOW_13);
            rule__Door__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__0"


    // $ANTLR start "rule__Door__Group_6__0__Impl"
    // InternalBuilding.g:2019:1: rule__Door__Group_6__0__Impl : ( 'keys' ) ;
    public final void rule__Door__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2023:1: ( ( 'keys' ) )
            // InternalBuilding.g:2024:1: ( 'keys' )
            {
            // InternalBuilding.g:2024:1: ( 'keys' )
            // InternalBuilding.g:2025:2: 'keys'
            {
             before(grammarAccess.getDoorAccess().getKeysKeyword_6_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getKeysKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__0__Impl"


    // $ANTLR start "rule__Door__Group_6__1"
    // InternalBuilding.g:2034:1: rule__Door__Group_6__1 : rule__Door__Group_6__1__Impl rule__Door__Group_6__2 ;
    public final void rule__Door__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2038:1: ( rule__Door__Group_6__1__Impl rule__Door__Group_6__2 )
            // InternalBuilding.g:2039:2: rule__Door__Group_6__1__Impl rule__Door__Group_6__2
            {
            pushFollow(FOLLOW_6);
            rule__Door__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__1"


    // $ANTLR start "rule__Door__Group_6__1__Impl"
    // InternalBuilding.g:2046:1: rule__Door__Group_6__1__Impl : ( '(' ) ;
    public final void rule__Door__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2050:1: ( ( '(' ) )
            // InternalBuilding.g:2051:1: ( '(' )
            {
            // InternalBuilding.g:2051:1: ( '(' )
            // InternalBuilding.g:2052:2: '('
            {
             before(grammarAccess.getDoorAccess().getLeftParenthesisKeyword_6_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getLeftParenthesisKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__1__Impl"


    // $ANTLR start "rule__Door__Group_6__2"
    // InternalBuilding.g:2061:1: rule__Door__Group_6__2 : rule__Door__Group_6__2__Impl rule__Door__Group_6__3 ;
    public final void rule__Door__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2065:1: ( rule__Door__Group_6__2__Impl rule__Door__Group_6__3 )
            // InternalBuilding.g:2066:2: rule__Door__Group_6__2__Impl rule__Door__Group_6__3
            {
            pushFollow(FOLLOW_14);
            rule__Door__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__2"


    // $ANTLR start "rule__Door__Group_6__2__Impl"
    // InternalBuilding.g:2073:1: rule__Door__Group_6__2__Impl : ( ( rule__Door__KeysAssignment_6_2 ) ) ;
    public final void rule__Door__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2077:1: ( ( ( rule__Door__KeysAssignment_6_2 ) ) )
            // InternalBuilding.g:2078:1: ( ( rule__Door__KeysAssignment_6_2 ) )
            {
            // InternalBuilding.g:2078:1: ( ( rule__Door__KeysAssignment_6_2 ) )
            // InternalBuilding.g:2079:2: ( rule__Door__KeysAssignment_6_2 )
            {
             before(grammarAccess.getDoorAccess().getKeysAssignment_6_2()); 
            // InternalBuilding.g:2080:2: ( rule__Door__KeysAssignment_6_2 )
            // InternalBuilding.g:2080:3: rule__Door__KeysAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__Door__KeysAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getDoorAccess().getKeysAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__2__Impl"


    // $ANTLR start "rule__Door__Group_6__3"
    // InternalBuilding.g:2088:1: rule__Door__Group_6__3 : rule__Door__Group_6__3__Impl rule__Door__Group_6__4 ;
    public final void rule__Door__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2092:1: ( rule__Door__Group_6__3__Impl rule__Door__Group_6__4 )
            // InternalBuilding.g:2093:2: rule__Door__Group_6__3__Impl rule__Door__Group_6__4
            {
            pushFollow(FOLLOW_14);
            rule__Door__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__3"


    // $ANTLR start "rule__Door__Group_6__3__Impl"
    // InternalBuilding.g:2100:1: rule__Door__Group_6__3__Impl : ( ( rule__Door__Group_6_3__0 )* ) ;
    public final void rule__Door__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2104:1: ( ( ( rule__Door__Group_6_3__0 )* ) )
            // InternalBuilding.g:2105:1: ( ( rule__Door__Group_6_3__0 )* )
            {
            // InternalBuilding.g:2105:1: ( ( rule__Door__Group_6_3__0 )* )
            // InternalBuilding.g:2106:2: ( rule__Door__Group_6_3__0 )*
            {
             before(grammarAccess.getDoorAccess().getGroup_6_3()); 
            // InternalBuilding.g:2107:2: ( rule__Door__Group_6_3__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==19) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalBuilding.g:2107:3: rule__Door__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Door__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getDoorAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__3__Impl"


    // $ANTLR start "rule__Door__Group_6__4"
    // InternalBuilding.g:2115:1: rule__Door__Group_6__4 : rule__Door__Group_6__4__Impl ;
    public final void rule__Door__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2119:1: ( rule__Door__Group_6__4__Impl )
            // InternalBuilding.g:2120:2: rule__Door__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Door__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__4"


    // $ANTLR start "rule__Door__Group_6__4__Impl"
    // InternalBuilding.g:2126:1: rule__Door__Group_6__4__Impl : ( ')' ) ;
    public final void rule__Door__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2130:1: ( ( ')' ) )
            // InternalBuilding.g:2131:1: ( ')' )
            {
            // InternalBuilding.g:2131:1: ( ')' )
            // InternalBuilding.g:2132:2: ')'
            {
             before(grammarAccess.getDoorAccess().getRightParenthesisKeyword_6_4()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getRightParenthesisKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6__4__Impl"


    // $ANTLR start "rule__Door__Group_6_3__0"
    // InternalBuilding.g:2142:1: rule__Door__Group_6_3__0 : rule__Door__Group_6_3__0__Impl rule__Door__Group_6_3__1 ;
    public final void rule__Door__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2146:1: ( rule__Door__Group_6_3__0__Impl rule__Door__Group_6_3__1 )
            // InternalBuilding.g:2147:2: rule__Door__Group_6_3__0__Impl rule__Door__Group_6_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Door__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6_3__0"


    // $ANTLR start "rule__Door__Group_6_3__0__Impl"
    // InternalBuilding.g:2154:1: rule__Door__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__Door__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2158:1: ( ( ',' ) )
            // InternalBuilding.g:2159:1: ( ',' )
            {
            // InternalBuilding.g:2159:1: ( ',' )
            // InternalBuilding.g:2160:2: ','
            {
             before(grammarAccess.getDoorAccess().getCommaKeyword_6_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6_3__0__Impl"


    // $ANTLR start "rule__Door__Group_6_3__1"
    // InternalBuilding.g:2169:1: rule__Door__Group_6_3__1 : rule__Door__Group_6_3__1__Impl ;
    public final void rule__Door__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2173:1: ( rule__Door__Group_6_3__1__Impl )
            // InternalBuilding.g:2174:2: rule__Door__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Door__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6_3__1"


    // $ANTLR start "rule__Door__Group_6_3__1__Impl"
    // InternalBuilding.g:2180:1: rule__Door__Group_6_3__1__Impl : ( ( rule__Door__KeysAssignment_6_3_1 ) ) ;
    public final void rule__Door__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2184:1: ( ( ( rule__Door__KeysAssignment_6_3_1 ) ) )
            // InternalBuilding.g:2185:1: ( ( rule__Door__KeysAssignment_6_3_1 ) )
            {
            // InternalBuilding.g:2185:1: ( ( rule__Door__KeysAssignment_6_3_1 ) )
            // InternalBuilding.g:2186:2: ( rule__Door__KeysAssignment_6_3_1 )
            {
             before(grammarAccess.getDoorAccess().getKeysAssignment_6_3_1()); 
            // InternalBuilding.g:2187:2: ( rule__Door__KeysAssignment_6_3_1 )
            // InternalBuilding.g:2187:3: rule__Door__KeysAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Door__KeysAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getDoorAccess().getKeysAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_6_3__1__Impl"


    // $ANTLR start "rule__Door__Group_7__0"
    // InternalBuilding.g:2196:1: rule__Door__Group_7__0 : rule__Door__Group_7__0__Impl rule__Door__Group_7__1 ;
    public final void rule__Door__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2200:1: ( rule__Door__Group_7__0__Impl rule__Door__Group_7__1 )
            // InternalBuilding.g:2201:2: rule__Door__Group_7__0__Impl rule__Door__Group_7__1
            {
            pushFollow(FOLLOW_13);
            rule__Door__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__0"


    // $ANTLR start "rule__Door__Group_7__0__Impl"
    // InternalBuilding.g:2208:1: rule__Door__Group_7__0__Impl : ( 'alarms' ) ;
    public final void rule__Door__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2212:1: ( ( 'alarms' ) )
            // InternalBuilding.g:2213:1: ( 'alarms' )
            {
            // InternalBuilding.g:2213:1: ( 'alarms' )
            // InternalBuilding.g:2214:2: 'alarms'
            {
             before(grammarAccess.getDoorAccess().getAlarmsKeyword_7_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getAlarmsKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__0__Impl"


    // $ANTLR start "rule__Door__Group_7__1"
    // InternalBuilding.g:2223:1: rule__Door__Group_7__1 : rule__Door__Group_7__1__Impl rule__Door__Group_7__2 ;
    public final void rule__Door__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2227:1: ( rule__Door__Group_7__1__Impl rule__Door__Group_7__2 )
            // InternalBuilding.g:2228:2: rule__Door__Group_7__1__Impl rule__Door__Group_7__2
            {
            pushFollow(FOLLOW_6);
            rule__Door__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__1"


    // $ANTLR start "rule__Door__Group_7__1__Impl"
    // InternalBuilding.g:2235:1: rule__Door__Group_7__1__Impl : ( '(' ) ;
    public final void rule__Door__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2239:1: ( ( '(' ) )
            // InternalBuilding.g:2240:1: ( '(' )
            {
            // InternalBuilding.g:2240:1: ( '(' )
            // InternalBuilding.g:2241:2: '('
            {
             before(grammarAccess.getDoorAccess().getLeftParenthesisKeyword_7_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getLeftParenthesisKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__1__Impl"


    // $ANTLR start "rule__Door__Group_7__2"
    // InternalBuilding.g:2250:1: rule__Door__Group_7__2 : rule__Door__Group_7__2__Impl rule__Door__Group_7__3 ;
    public final void rule__Door__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2254:1: ( rule__Door__Group_7__2__Impl rule__Door__Group_7__3 )
            // InternalBuilding.g:2255:2: rule__Door__Group_7__2__Impl rule__Door__Group_7__3
            {
            pushFollow(FOLLOW_14);
            rule__Door__Group_7__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_7__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__2"


    // $ANTLR start "rule__Door__Group_7__2__Impl"
    // InternalBuilding.g:2262:1: rule__Door__Group_7__2__Impl : ( ( rule__Door__AlarmsAssignment_7_2 ) ) ;
    public final void rule__Door__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2266:1: ( ( ( rule__Door__AlarmsAssignment_7_2 ) ) )
            // InternalBuilding.g:2267:1: ( ( rule__Door__AlarmsAssignment_7_2 ) )
            {
            // InternalBuilding.g:2267:1: ( ( rule__Door__AlarmsAssignment_7_2 ) )
            // InternalBuilding.g:2268:2: ( rule__Door__AlarmsAssignment_7_2 )
            {
             before(grammarAccess.getDoorAccess().getAlarmsAssignment_7_2()); 
            // InternalBuilding.g:2269:2: ( rule__Door__AlarmsAssignment_7_2 )
            // InternalBuilding.g:2269:3: rule__Door__AlarmsAssignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__Door__AlarmsAssignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getDoorAccess().getAlarmsAssignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__2__Impl"


    // $ANTLR start "rule__Door__Group_7__3"
    // InternalBuilding.g:2277:1: rule__Door__Group_7__3 : rule__Door__Group_7__3__Impl rule__Door__Group_7__4 ;
    public final void rule__Door__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2281:1: ( rule__Door__Group_7__3__Impl rule__Door__Group_7__4 )
            // InternalBuilding.g:2282:2: rule__Door__Group_7__3__Impl rule__Door__Group_7__4
            {
            pushFollow(FOLLOW_14);
            rule__Door__Group_7__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_7__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__3"


    // $ANTLR start "rule__Door__Group_7__3__Impl"
    // InternalBuilding.g:2289:1: rule__Door__Group_7__3__Impl : ( ( rule__Door__Group_7_3__0 )* ) ;
    public final void rule__Door__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2293:1: ( ( ( rule__Door__Group_7_3__0 )* ) )
            // InternalBuilding.g:2294:1: ( ( rule__Door__Group_7_3__0 )* )
            {
            // InternalBuilding.g:2294:1: ( ( rule__Door__Group_7_3__0 )* )
            // InternalBuilding.g:2295:2: ( rule__Door__Group_7_3__0 )*
            {
             before(grammarAccess.getDoorAccess().getGroup_7_3()); 
            // InternalBuilding.g:2296:2: ( rule__Door__Group_7_3__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==19) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalBuilding.g:2296:3: rule__Door__Group_7_3__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Door__Group_7_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getDoorAccess().getGroup_7_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__3__Impl"


    // $ANTLR start "rule__Door__Group_7__4"
    // InternalBuilding.g:2304:1: rule__Door__Group_7__4 : rule__Door__Group_7__4__Impl ;
    public final void rule__Door__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2308:1: ( rule__Door__Group_7__4__Impl )
            // InternalBuilding.g:2309:2: rule__Door__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Door__Group_7__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__4"


    // $ANTLR start "rule__Door__Group_7__4__Impl"
    // InternalBuilding.g:2315:1: rule__Door__Group_7__4__Impl : ( ')' ) ;
    public final void rule__Door__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2319:1: ( ( ')' ) )
            // InternalBuilding.g:2320:1: ( ')' )
            {
            // InternalBuilding.g:2320:1: ( ')' )
            // InternalBuilding.g:2321:2: ')'
            {
             before(grammarAccess.getDoorAccess().getRightParenthesisKeyword_7_4()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getRightParenthesisKeyword_7_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7__4__Impl"


    // $ANTLR start "rule__Door__Group_7_3__0"
    // InternalBuilding.g:2331:1: rule__Door__Group_7_3__0 : rule__Door__Group_7_3__0__Impl rule__Door__Group_7_3__1 ;
    public final void rule__Door__Group_7_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2335:1: ( rule__Door__Group_7_3__0__Impl rule__Door__Group_7_3__1 )
            // InternalBuilding.g:2336:2: rule__Door__Group_7_3__0__Impl rule__Door__Group_7_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Door__Group_7_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Door__Group_7_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7_3__0"


    // $ANTLR start "rule__Door__Group_7_3__0__Impl"
    // InternalBuilding.g:2343:1: rule__Door__Group_7_3__0__Impl : ( ',' ) ;
    public final void rule__Door__Group_7_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2347:1: ( ( ',' ) )
            // InternalBuilding.g:2348:1: ( ',' )
            {
            // InternalBuilding.g:2348:1: ( ',' )
            // InternalBuilding.g:2349:2: ','
            {
             before(grammarAccess.getDoorAccess().getCommaKeyword_7_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getDoorAccess().getCommaKeyword_7_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7_3__0__Impl"


    // $ANTLR start "rule__Door__Group_7_3__1"
    // InternalBuilding.g:2358:1: rule__Door__Group_7_3__1 : rule__Door__Group_7_3__1__Impl ;
    public final void rule__Door__Group_7_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2362:1: ( rule__Door__Group_7_3__1__Impl )
            // InternalBuilding.g:2363:2: rule__Door__Group_7_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Door__Group_7_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7_3__1"


    // $ANTLR start "rule__Door__Group_7_3__1__Impl"
    // InternalBuilding.g:2369:1: rule__Door__Group_7_3__1__Impl : ( ( rule__Door__AlarmsAssignment_7_3_1 ) ) ;
    public final void rule__Door__Group_7_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2373:1: ( ( ( rule__Door__AlarmsAssignment_7_3_1 ) ) )
            // InternalBuilding.g:2374:1: ( ( rule__Door__AlarmsAssignment_7_3_1 ) )
            {
            // InternalBuilding.g:2374:1: ( ( rule__Door__AlarmsAssignment_7_3_1 ) )
            // InternalBuilding.g:2375:2: ( rule__Door__AlarmsAssignment_7_3_1 )
            {
             before(grammarAccess.getDoorAccess().getAlarmsAssignment_7_3_1()); 
            // InternalBuilding.g:2376:2: ( rule__Door__AlarmsAssignment_7_3_1 )
            // InternalBuilding.g:2376:3: rule__Door__AlarmsAssignment_7_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Door__AlarmsAssignment_7_3_1();

            state._fsp--;


            }

             after(grammarAccess.getDoorAccess().getAlarmsAssignment_7_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Group_7_3__1__Impl"


    // $ANTLR start "rule__Window__Group__0"
    // InternalBuilding.g:2385:1: rule__Window__Group__0 : rule__Window__Group__0__Impl rule__Window__Group__1 ;
    public final void rule__Window__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2389:1: ( rule__Window__Group__0__Impl rule__Window__Group__1 )
            // InternalBuilding.g:2390:2: rule__Window__Group__0__Impl rule__Window__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Window__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__0"


    // $ANTLR start "rule__Window__Group__0__Impl"
    // InternalBuilding.g:2397:1: rule__Window__Group__0__Impl : ( 'Window' ) ;
    public final void rule__Window__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2401:1: ( ( 'Window' ) )
            // InternalBuilding.g:2402:1: ( 'Window' )
            {
            // InternalBuilding.g:2402:1: ( 'Window' )
            // InternalBuilding.g:2403:2: 'Window'
            {
             before(grammarAccess.getWindowAccess().getWindowKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getWindowKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__0__Impl"


    // $ANTLR start "rule__Window__Group__1"
    // InternalBuilding.g:2412:1: rule__Window__Group__1 : rule__Window__Group__1__Impl rule__Window__Group__2 ;
    public final void rule__Window__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2416:1: ( rule__Window__Group__1__Impl rule__Window__Group__2 )
            // InternalBuilding.g:2417:2: rule__Window__Group__1__Impl rule__Window__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Window__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__1"


    // $ANTLR start "rule__Window__Group__1__Impl"
    // InternalBuilding.g:2424:1: rule__Window__Group__1__Impl : ( ( rule__Window__NameAssignment_1 ) ) ;
    public final void rule__Window__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2428:1: ( ( ( rule__Window__NameAssignment_1 ) ) )
            // InternalBuilding.g:2429:1: ( ( rule__Window__NameAssignment_1 ) )
            {
            // InternalBuilding.g:2429:1: ( ( rule__Window__NameAssignment_1 ) )
            // InternalBuilding.g:2430:2: ( rule__Window__NameAssignment_1 )
            {
             before(grammarAccess.getWindowAccess().getNameAssignment_1()); 
            // InternalBuilding.g:2431:2: ( rule__Window__NameAssignment_1 )
            // InternalBuilding.g:2431:3: rule__Window__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Window__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWindowAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__1__Impl"


    // $ANTLR start "rule__Window__Group__2"
    // InternalBuilding.g:2439:1: rule__Window__Group__2 : rule__Window__Group__2__Impl rule__Window__Group__3 ;
    public final void rule__Window__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2443:1: ( rule__Window__Group__2__Impl rule__Window__Group__3 )
            // InternalBuilding.g:2444:2: rule__Window__Group__2__Impl rule__Window__Group__3
            {
            pushFollow(FOLLOW_24);
            rule__Window__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__2"


    // $ANTLR start "rule__Window__Group__2__Impl"
    // InternalBuilding.g:2451:1: rule__Window__Group__2__Impl : ( '{' ) ;
    public final void rule__Window__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2455:1: ( ( '{' ) )
            // InternalBuilding.g:2456:1: ( '{' )
            {
            // InternalBuilding.g:2456:1: ( '{' )
            // InternalBuilding.g:2457:2: '{'
            {
             before(grammarAccess.getWindowAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__2__Impl"


    // $ANTLR start "rule__Window__Group__3"
    // InternalBuilding.g:2466:1: rule__Window__Group__3 : rule__Window__Group__3__Impl rule__Window__Group__4 ;
    public final void rule__Window__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2470:1: ( rule__Window__Group__3__Impl rule__Window__Group__4 )
            // InternalBuilding.g:2471:2: rule__Window__Group__3__Impl rule__Window__Group__4
            {
            pushFollow(FOLLOW_24);
            rule__Window__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__3"


    // $ANTLR start "rule__Window__Group__3__Impl"
    // InternalBuilding.g:2478:1: rule__Window__Group__3__Impl : ( ( rule__Window__Group_3__0 )? ) ;
    public final void rule__Window__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2482:1: ( ( ( rule__Window__Group_3__0 )? ) )
            // InternalBuilding.g:2483:1: ( ( rule__Window__Group_3__0 )? )
            {
            // InternalBuilding.g:2483:1: ( ( rule__Window__Group_3__0 )? )
            // InternalBuilding.g:2484:2: ( rule__Window__Group_3__0 )?
            {
             before(grammarAccess.getWindowAccess().getGroup_3()); 
            // InternalBuilding.g:2485:2: ( rule__Window__Group_3__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==27) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalBuilding.g:2485:3: rule__Window__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Window__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getWindowAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__3__Impl"


    // $ANTLR start "rule__Window__Group__4"
    // InternalBuilding.g:2493:1: rule__Window__Group__4 : rule__Window__Group__4__Impl rule__Window__Group__5 ;
    public final void rule__Window__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2497:1: ( rule__Window__Group__4__Impl rule__Window__Group__5 )
            // InternalBuilding.g:2498:2: rule__Window__Group__4__Impl rule__Window__Group__5
            {
            pushFollow(FOLLOW_24);
            rule__Window__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__4"


    // $ANTLR start "rule__Window__Group__4__Impl"
    // InternalBuilding.g:2505:1: rule__Window__Group__4__Impl : ( ( rule__Window__Group_4__0 )? ) ;
    public final void rule__Window__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2509:1: ( ( ( rule__Window__Group_4__0 )? ) )
            // InternalBuilding.g:2510:1: ( ( rule__Window__Group_4__0 )? )
            {
            // InternalBuilding.g:2510:1: ( ( rule__Window__Group_4__0 )? )
            // InternalBuilding.g:2511:2: ( rule__Window__Group_4__0 )?
            {
             before(grammarAccess.getWindowAccess().getGroup_4()); 
            // InternalBuilding.g:2512:2: ( rule__Window__Group_4__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==30) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalBuilding.g:2512:3: rule__Window__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Window__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getWindowAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__4__Impl"


    // $ANTLR start "rule__Window__Group__5"
    // InternalBuilding.g:2520:1: rule__Window__Group__5 : rule__Window__Group__5__Impl rule__Window__Group__6 ;
    public final void rule__Window__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2524:1: ( rule__Window__Group__5__Impl rule__Window__Group__6 )
            // InternalBuilding.g:2525:2: rule__Window__Group__5__Impl rule__Window__Group__6
            {
            pushFollow(FOLLOW_24);
            rule__Window__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__5"


    // $ANTLR start "rule__Window__Group__5__Impl"
    // InternalBuilding.g:2532:1: rule__Window__Group__5__Impl : ( ( rule__Window__Group_5__0 )? ) ;
    public final void rule__Window__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2536:1: ( ( ( rule__Window__Group_5__0 )? ) )
            // InternalBuilding.g:2537:1: ( ( rule__Window__Group_5__0 )? )
            {
            // InternalBuilding.g:2537:1: ( ( rule__Window__Group_5__0 )? )
            // InternalBuilding.g:2538:2: ( rule__Window__Group_5__0 )?
            {
             before(grammarAccess.getWindowAccess().getGroup_5()); 
            // InternalBuilding.g:2539:2: ( rule__Window__Group_5__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==31) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalBuilding.g:2539:3: rule__Window__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Window__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getWindowAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__5__Impl"


    // $ANTLR start "rule__Window__Group__6"
    // InternalBuilding.g:2547:1: rule__Window__Group__6 : rule__Window__Group__6__Impl rule__Window__Group__7 ;
    public final void rule__Window__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2551:1: ( rule__Window__Group__6__Impl rule__Window__Group__7 )
            // InternalBuilding.g:2552:2: rule__Window__Group__6__Impl rule__Window__Group__7
            {
            pushFollow(FOLLOW_24);
            rule__Window__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__6"


    // $ANTLR start "rule__Window__Group__6__Impl"
    // InternalBuilding.g:2559:1: rule__Window__Group__6__Impl : ( ( rule__Window__Group_6__0 )? ) ;
    public final void rule__Window__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2563:1: ( ( ( rule__Window__Group_6__0 )? ) )
            // InternalBuilding.g:2564:1: ( ( rule__Window__Group_6__0 )? )
            {
            // InternalBuilding.g:2564:1: ( ( rule__Window__Group_6__0 )? )
            // InternalBuilding.g:2565:2: ( rule__Window__Group_6__0 )?
            {
             before(grammarAccess.getWindowAccess().getGroup_6()); 
            // InternalBuilding.g:2566:2: ( rule__Window__Group_6__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==16) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalBuilding.g:2566:3: rule__Window__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Window__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getWindowAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__6__Impl"


    // $ANTLR start "rule__Window__Group__7"
    // InternalBuilding.g:2574:1: rule__Window__Group__7 : rule__Window__Group__7__Impl ;
    public final void rule__Window__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2578:1: ( rule__Window__Group__7__Impl )
            // InternalBuilding.g:2579:2: rule__Window__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Window__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__7"


    // $ANTLR start "rule__Window__Group__7__Impl"
    // InternalBuilding.g:2585:1: rule__Window__Group__7__Impl : ( '}' ) ;
    public final void rule__Window__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2589:1: ( ( '}' ) )
            // InternalBuilding.g:2590:1: ( '}' )
            {
            // InternalBuilding.g:2590:1: ( '}' )
            // InternalBuilding.g:2591:2: '}'
            {
             before(grammarAccess.getWindowAccess().getRightCurlyBracketKeyword_7()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group__7__Impl"


    // $ANTLR start "rule__Window__Group_3__0"
    // InternalBuilding.g:2601:1: rule__Window__Group_3__0 : rule__Window__Group_3__0__Impl rule__Window__Group_3__1 ;
    public final void rule__Window__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2605:1: ( rule__Window__Group_3__0__Impl rule__Window__Group_3__1 )
            // InternalBuilding.g:2606:2: rule__Window__Group_3__0__Impl rule__Window__Group_3__1
            {
            pushFollow(FOLLOW_23);
            rule__Window__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_3__0"


    // $ANTLR start "rule__Window__Group_3__0__Impl"
    // InternalBuilding.g:2613:1: rule__Window__Group_3__0__Impl : ( 'level' ) ;
    public final void rule__Window__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2617:1: ( ( 'level' ) )
            // InternalBuilding.g:2618:1: ( 'level' )
            {
            // InternalBuilding.g:2618:1: ( 'level' )
            // InternalBuilding.g:2619:2: 'level'
            {
             before(grammarAccess.getWindowAccess().getLevelKeyword_3_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getLevelKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_3__0__Impl"


    // $ANTLR start "rule__Window__Group_3__1"
    // InternalBuilding.g:2628:1: rule__Window__Group_3__1 : rule__Window__Group_3__1__Impl ;
    public final void rule__Window__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2632:1: ( rule__Window__Group_3__1__Impl )
            // InternalBuilding.g:2633:2: rule__Window__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Window__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_3__1"


    // $ANTLR start "rule__Window__Group_3__1__Impl"
    // InternalBuilding.g:2639:1: rule__Window__Group_3__1__Impl : ( ( rule__Window__LevelAssignment_3_1 ) ) ;
    public final void rule__Window__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2643:1: ( ( ( rule__Window__LevelAssignment_3_1 ) ) )
            // InternalBuilding.g:2644:1: ( ( rule__Window__LevelAssignment_3_1 ) )
            {
            // InternalBuilding.g:2644:1: ( ( rule__Window__LevelAssignment_3_1 ) )
            // InternalBuilding.g:2645:2: ( rule__Window__LevelAssignment_3_1 )
            {
             before(grammarAccess.getWindowAccess().getLevelAssignment_3_1()); 
            // InternalBuilding.g:2646:2: ( rule__Window__LevelAssignment_3_1 )
            // InternalBuilding.g:2646:3: rule__Window__LevelAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Window__LevelAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getWindowAccess().getLevelAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_3__1__Impl"


    // $ANTLR start "rule__Window__Group_4__0"
    // InternalBuilding.g:2655:1: rule__Window__Group_4__0 : rule__Window__Group_4__0__Impl rule__Window__Group_4__1 ;
    public final void rule__Window__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2659:1: ( rule__Window__Group_4__0__Impl rule__Window__Group_4__1 )
            // InternalBuilding.g:2660:2: rule__Window__Group_4__0__Impl rule__Window__Group_4__1
            {
            pushFollow(FOLLOW_6);
            rule__Window__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_4__0"


    // $ANTLR start "rule__Window__Group_4__0__Impl"
    // InternalBuilding.g:2667:1: rule__Window__Group_4__0__Impl : ( 'inside' ) ;
    public final void rule__Window__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2671:1: ( ( 'inside' ) )
            // InternalBuilding.g:2672:1: ( 'inside' )
            {
            // InternalBuilding.g:2672:1: ( 'inside' )
            // InternalBuilding.g:2673:2: 'inside'
            {
             before(grammarAccess.getWindowAccess().getInsideKeyword_4_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getInsideKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_4__0__Impl"


    // $ANTLR start "rule__Window__Group_4__1"
    // InternalBuilding.g:2682:1: rule__Window__Group_4__1 : rule__Window__Group_4__1__Impl ;
    public final void rule__Window__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2686:1: ( rule__Window__Group_4__1__Impl )
            // InternalBuilding.g:2687:2: rule__Window__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Window__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_4__1"


    // $ANTLR start "rule__Window__Group_4__1__Impl"
    // InternalBuilding.g:2693:1: rule__Window__Group_4__1__Impl : ( ( rule__Window__InsideAssignment_4_1 ) ) ;
    public final void rule__Window__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2697:1: ( ( ( rule__Window__InsideAssignment_4_1 ) ) )
            // InternalBuilding.g:2698:1: ( ( rule__Window__InsideAssignment_4_1 ) )
            {
            // InternalBuilding.g:2698:1: ( ( rule__Window__InsideAssignment_4_1 ) )
            // InternalBuilding.g:2699:2: ( rule__Window__InsideAssignment_4_1 )
            {
             before(grammarAccess.getWindowAccess().getInsideAssignment_4_1()); 
            // InternalBuilding.g:2700:2: ( rule__Window__InsideAssignment_4_1 )
            // InternalBuilding.g:2700:3: rule__Window__InsideAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Window__InsideAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getWindowAccess().getInsideAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_4__1__Impl"


    // $ANTLR start "rule__Window__Group_5__0"
    // InternalBuilding.g:2709:1: rule__Window__Group_5__0 : rule__Window__Group_5__0__Impl rule__Window__Group_5__1 ;
    public final void rule__Window__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2713:1: ( rule__Window__Group_5__0__Impl rule__Window__Group_5__1 )
            // InternalBuilding.g:2714:2: rule__Window__Group_5__0__Impl rule__Window__Group_5__1
            {
            pushFollow(FOLLOW_6);
            rule__Window__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_5__0"


    // $ANTLR start "rule__Window__Group_5__0__Impl"
    // InternalBuilding.g:2721:1: rule__Window__Group_5__0__Impl : ( 'outside' ) ;
    public final void rule__Window__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2725:1: ( ( 'outside' ) )
            // InternalBuilding.g:2726:1: ( 'outside' )
            {
            // InternalBuilding.g:2726:1: ( 'outside' )
            // InternalBuilding.g:2727:2: 'outside'
            {
             before(grammarAccess.getWindowAccess().getOutsideKeyword_5_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getOutsideKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_5__0__Impl"


    // $ANTLR start "rule__Window__Group_5__1"
    // InternalBuilding.g:2736:1: rule__Window__Group_5__1 : rule__Window__Group_5__1__Impl ;
    public final void rule__Window__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2740:1: ( rule__Window__Group_5__1__Impl )
            // InternalBuilding.g:2741:2: rule__Window__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Window__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_5__1"


    // $ANTLR start "rule__Window__Group_5__1__Impl"
    // InternalBuilding.g:2747:1: rule__Window__Group_5__1__Impl : ( ( rule__Window__OutsideAssignment_5_1 ) ) ;
    public final void rule__Window__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2751:1: ( ( ( rule__Window__OutsideAssignment_5_1 ) ) )
            // InternalBuilding.g:2752:1: ( ( rule__Window__OutsideAssignment_5_1 ) )
            {
            // InternalBuilding.g:2752:1: ( ( rule__Window__OutsideAssignment_5_1 ) )
            // InternalBuilding.g:2753:2: ( rule__Window__OutsideAssignment_5_1 )
            {
             before(grammarAccess.getWindowAccess().getOutsideAssignment_5_1()); 
            // InternalBuilding.g:2754:2: ( rule__Window__OutsideAssignment_5_1 )
            // InternalBuilding.g:2754:3: rule__Window__OutsideAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Window__OutsideAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getWindowAccess().getOutsideAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_5__1__Impl"


    // $ANTLR start "rule__Window__Group_6__0"
    // InternalBuilding.g:2763:1: rule__Window__Group_6__0 : rule__Window__Group_6__0__Impl rule__Window__Group_6__1 ;
    public final void rule__Window__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2767:1: ( rule__Window__Group_6__0__Impl rule__Window__Group_6__1 )
            // InternalBuilding.g:2768:2: rule__Window__Group_6__0__Impl rule__Window__Group_6__1
            {
            pushFollow(FOLLOW_13);
            rule__Window__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__0"


    // $ANTLR start "rule__Window__Group_6__0__Impl"
    // InternalBuilding.g:2775:1: rule__Window__Group_6__0__Impl : ( 'alarms' ) ;
    public final void rule__Window__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2779:1: ( ( 'alarms' ) )
            // InternalBuilding.g:2780:1: ( 'alarms' )
            {
            // InternalBuilding.g:2780:1: ( 'alarms' )
            // InternalBuilding.g:2781:2: 'alarms'
            {
             before(grammarAccess.getWindowAccess().getAlarmsKeyword_6_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getAlarmsKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__0__Impl"


    // $ANTLR start "rule__Window__Group_6__1"
    // InternalBuilding.g:2790:1: rule__Window__Group_6__1 : rule__Window__Group_6__1__Impl rule__Window__Group_6__2 ;
    public final void rule__Window__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2794:1: ( rule__Window__Group_6__1__Impl rule__Window__Group_6__2 )
            // InternalBuilding.g:2795:2: rule__Window__Group_6__1__Impl rule__Window__Group_6__2
            {
            pushFollow(FOLLOW_6);
            rule__Window__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__1"


    // $ANTLR start "rule__Window__Group_6__1__Impl"
    // InternalBuilding.g:2802:1: rule__Window__Group_6__1__Impl : ( '(' ) ;
    public final void rule__Window__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2806:1: ( ( '(' ) )
            // InternalBuilding.g:2807:1: ( '(' )
            {
            // InternalBuilding.g:2807:1: ( '(' )
            // InternalBuilding.g:2808:2: '('
            {
             before(grammarAccess.getWindowAccess().getLeftParenthesisKeyword_6_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getLeftParenthesisKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__1__Impl"


    // $ANTLR start "rule__Window__Group_6__2"
    // InternalBuilding.g:2817:1: rule__Window__Group_6__2 : rule__Window__Group_6__2__Impl rule__Window__Group_6__3 ;
    public final void rule__Window__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2821:1: ( rule__Window__Group_6__2__Impl rule__Window__Group_6__3 )
            // InternalBuilding.g:2822:2: rule__Window__Group_6__2__Impl rule__Window__Group_6__3
            {
            pushFollow(FOLLOW_14);
            rule__Window__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__2"


    // $ANTLR start "rule__Window__Group_6__2__Impl"
    // InternalBuilding.g:2829:1: rule__Window__Group_6__2__Impl : ( ( rule__Window__AlarmsAssignment_6_2 ) ) ;
    public final void rule__Window__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2833:1: ( ( ( rule__Window__AlarmsAssignment_6_2 ) ) )
            // InternalBuilding.g:2834:1: ( ( rule__Window__AlarmsAssignment_6_2 ) )
            {
            // InternalBuilding.g:2834:1: ( ( rule__Window__AlarmsAssignment_6_2 ) )
            // InternalBuilding.g:2835:2: ( rule__Window__AlarmsAssignment_6_2 )
            {
             before(grammarAccess.getWindowAccess().getAlarmsAssignment_6_2()); 
            // InternalBuilding.g:2836:2: ( rule__Window__AlarmsAssignment_6_2 )
            // InternalBuilding.g:2836:3: rule__Window__AlarmsAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__Window__AlarmsAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getWindowAccess().getAlarmsAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__2__Impl"


    // $ANTLR start "rule__Window__Group_6__3"
    // InternalBuilding.g:2844:1: rule__Window__Group_6__3 : rule__Window__Group_6__3__Impl rule__Window__Group_6__4 ;
    public final void rule__Window__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2848:1: ( rule__Window__Group_6__3__Impl rule__Window__Group_6__4 )
            // InternalBuilding.g:2849:2: rule__Window__Group_6__3__Impl rule__Window__Group_6__4
            {
            pushFollow(FOLLOW_14);
            rule__Window__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__3"


    // $ANTLR start "rule__Window__Group_6__3__Impl"
    // InternalBuilding.g:2856:1: rule__Window__Group_6__3__Impl : ( ( rule__Window__Group_6_3__0 )* ) ;
    public final void rule__Window__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2860:1: ( ( ( rule__Window__Group_6_3__0 )* ) )
            // InternalBuilding.g:2861:1: ( ( rule__Window__Group_6_3__0 )* )
            {
            // InternalBuilding.g:2861:1: ( ( rule__Window__Group_6_3__0 )* )
            // InternalBuilding.g:2862:2: ( rule__Window__Group_6_3__0 )*
            {
             before(grammarAccess.getWindowAccess().getGroup_6_3()); 
            // InternalBuilding.g:2863:2: ( rule__Window__Group_6_3__0 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==19) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalBuilding.g:2863:3: rule__Window__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Window__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getWindowAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__3__Impl"


    // $ANTLR start "rule__Window__Group_6__4"
    // InternalBuilding.g:2871:1: rule__Window__Group_6__4 : rule__Window__Group_6__4__Impl ;
    public final void rule__Window__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2875:1: ( rule__Window__Group_6__4__Impl )
            // InternalBuilding.g:2876:2: rule__Window__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Window__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__4"


    // $ANTLR start "rule__Window__Group_6__4__Impl"
    // InternalBuilding.g:2882:1: rule__Window__Group_6__4__Impl : ( ')' ) ;
    public final void rule__Window__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2886:1: ( ( ')' ) )
            // InternalBuilding.g:2887:1: ( ')' )
            {
            // InternalBuilding.g:2887:1: ( ')' )
            // InternalBuilding.g:2888:2: ')'
            {
             before(grammarAccess.getWindowAccess().getRightParenthesisKeyword_6_4()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getRightParenthesisKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6__4__Impl"


    // $ANTLR start "rule__Window__Group_6_3__0"
    // InternalBuilding.g:2898:1: rule__Window__Group_6_3__0 : rule__Window__Group_6_3__0__Impl rule__Window__Group_6_3__1 ;
    public final void rule__Window__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2902:1: ( rule__Window__Group_6_3__0__Impl rule__Window__Group_6_3__1 )
            // InternalBuilding.g:2903:2: rule__Window__Group_6_3__0__Impl rule__Window__Group_6_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Window__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Window__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6_3__0"


    // $ANTLR start "rule__Window__Group_6_3__0__Impl"
    // InternalBuilding.g:2910:1: rule__Window__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__Window__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2914:1: ( ( ',' ) )
            // InternalBuilding.g:2915:1: ( ',' )
            {
            // InternalBuilding.g:2915:1: ( ',' )
            // InternalBuilding.g:2916:2: ','
            {
             before(grammarAccess.getWindowAccess().getCommaKeyword_6_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getWindowAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6_3__0__Impl"


    // $ANTLR start "rule__Window__Group_6_3__1"
    // InternalBuilding.g:2925:1: rule__Window__Group_6_3__1 : rule__Window__Group_6_3__1__Impl ;
    public final void rule__Window__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2929:1: ( rule__Window__Group_6_3__1__Impl )
            // InternalBuilding.g:2930:2: rule__Window__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Window__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6_3__1"


    // $ANTLR start "rule__Window__Group_6_3__1__Impl"
    // InternalBuilding.g:2936:1: rule__Window__Group_6_3__1__Impl : ( ( rule__Window__AlarmsAssignment_6_3_1 ) ) ;
    public final void rule__Window__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2940:1: ( ( ( rule__Window__AlarmsAssignment_6_3_1 ) ) )
            // InternalBuilding.g:2941:1: ( ( rule__Window__AlarmsAssignment_6_3_1 ) )
            {
            // InternalBuilding.g:2941:1: ( ( rule__Window__AlarmsAssignment_6_3_1 ) )
            // InternalBuilding.g:2942:2: ( rule__Window__AlarmsAssignment_6_3_1 )
            {
             before(grammarAccess.getWindowAccess().getAlarmsAssignment_6_3_1()); 
            // InternalBuilding.g:2943:2: ( rule__Window__AlarmsAssignment_6_3_1 )
            // InternalBuilding.g:2943:3: rule__Window__AlarmsAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Window__AlarmsAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getWindowAccess().getAlarmsAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__Group_6_3__1__Impl"


    // $ANTLR start "rule__Badged__Group__0"
    // InternalBuilding.g:2952:1: rule__Badged__Group__0 : rule__Badged__Group__0__Impl rule__Badged__Group__1 ;
    public final void rule__Badged__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2956:1: ( rule__Badged__Group__0__Impl rule__Badged__Group__1 )
            // InternalBuilding.g:2957:2: rule__Badged__Group__0__Impl rule__Badged__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Badged__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__0"


    // $ANTLR start "rule__Badged__Group__0__Impl"
    // InternalBuilding.g:2964:1: rule__Badged__Group__0__Impl : ( 'BadgedDoor' ) ;
    public final void rule__Badged__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2968:1: ( ( 'BadgedDoor' ) )
            // InternalBuilding.g:2969:1: ( 'BadgedDoor' )
            {
            // InternalBuilding.g:2969:1: ( 'BadgedDoor' )
            // InternalBuilding.g:2970:2: 'BadgedDoor'
            {
             before(grammarAccess.getBadgedAccess().getBadgedDoorKeyword_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getBadgedDoorKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__0__Impl"


    // $ANTLR start "rule__Badged__Group__1"
    // InternalBuilding.g:2979:1: rule__Badged__Group__1 : rule__Badged__Group__1__Impl rule__Badged__Group__2 ;
    public final void rule__Badged__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2983:1: ( rule__Badged__Group__1__Impl rule__Badged__Group__2 )
            // InternalBuilding.g:2984:2: rule__Badged__Group__1__Impl rule__Badged__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Badged__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__1"


    // $ANTLR start "rule__Badged__Group__1__Impl"
    // InternalBuilding.g:2991:1: rule__Badged__Group__1__Impl : ( ( rule__Badged__NameAssignment_1 ) ) ;
    public final void rule__Badged__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:2995:1: ( ( ( rule__Badged__NameAssignment_1 ) ) )
            // InternalBuilding.g:2996:1: ( ( rule__Badged__NameAssignment_1 ) )
            {
            // InternalBuilding.g:2996:1: ( ( rule__Badged__NameAssignment_1 ) )
            // InternalBuilding.g:2997:2: ( rule__Badged__NameAssignment_1 )
            {
             before(grammarAccess.getBadgedAccess().getNameAssignment_1()); 
            // InternalBuilding.g:2998:2: ( rule__Badged__NameAssignment_1 )
            // InternalBuilding.g:2998:3: rule__Badged__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Badged__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgedAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__1__Impl"


    // $ANTLR start "rule__Badged__Group__2"
    // InternalBuilding.g:3006:1: rule__Badged__Group__2 : rule__Badged__Group__2__Impl rule__Badged__Group__3 ;
    public final void rule__Badged__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3010:1: ( rule__Badged__Group__2__Impl rule__Badged__Group__3 )
            // InternalBuilding.g:3011:2: rule__Badged__Group__2__Impl rule__Badged__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__Badged__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__2"


    // $ANTLR start "rule__Badged__Group__2__Impl"
    // InternalBuilding.g:3018:1: rule__Badged__Group__2__Impl : ( '{' ) ;
    public final void rule__Badged__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3022:1: ( ( '{' ) )
            // InternalBuilding.g:3023:1: ( '{' )
            {
            // InternalBuilding.g:3023:1: ( '{' )
            // InternalBuilding.g:3024:2: '{'
            {
             before(grammarAccess.getBadgedAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__2__Impl"


    // $ANTLR start "rule__Badged__Group__3"
    // InternalBuilding.g:3033:1: rule__Badged__Group__3 : rule__Badged__Group__3__Impl rule__Badged__Group__4 ;
    public final void rule__Badged__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3037:1: ( rule__Badged__Group__3__Impl rule__Badged__Group__4 )
            // InternalBuilding.g:3038:2: rule__Badged__Group__3__Impl rule__Badged__Group__4
            {
            pushFollow(FOLLOW_25);
            rule__Badged__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__3"


    // $ANTLR start "rule__Badged__Group__3__Impl"
    // InternalBuilding.g:3045:1: rule__Badged__Group__3__Impl : ( ( rule__Badged__Group_3__0 )? ) ;
    public final void rule__Badged__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3049:1: ( ( ( rule__Badged__Group_3__0 )? ) )
            // InternalBuilding.g:3050:1: ( ( rule__Badged__Group_3__0 )? )
            {
            // InternalBuilding.g:3050:1: ( ( rule__Badged__Group_3__0 )? )
            // InternalBuilding.g:3051:2: ( rule__Badged__Group_3__0 )?
            {
             before(grammarAccess.getBadgedAccess().getGroup_3()); 
            // InternalBuilding.g:3052:2: ( rule__Badged__Group_3__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==27) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalBuilding.g:3052:3: rule__Badged__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badged__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgedAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__3__Impl"


    // $ANTLR start "rule__Badged__Group__4"
    // InternalBuilding.g:3060:1: rule__Badged__Group__4 : rule__Badged__Group__4__Impl rule__Badged__Group__5 ;
    public final void rule__Badged__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3064:1: ( rule__Badged__Group__4__Impl rule__Badged__Group__5 )
            // InternalBuilding.g:3065:2: rule__Badged__Group__4__Impl rule__Badged__Group__5
            {
            pushFollow(FOLLOW_25);
            rule__Badged__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__4"


    // $ANTLR start "rule__Badged__Group__4__Impl"
    // InternalBuilding.g:3072:1: rule__Badged__Group__4__Impl : ( ( rule__Badged__Group_4__0 )? ) ;
    public final void rule__Badged__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3076:1: ( ( ( rule__Badged__Group_4__0 )? ) )
            // InternalBuilding.g:3077:1: ( ( rule__Badged__Group_4__0 )? )
            {
            // InternalBuilding.g:3077:1: ( ( rule__Badged__Group_4__0 )? )
            // InternalBuilding.g:3078:2: ( rule__Badged__Group_4__0 )?
            {
             before(grammarAccess.getBadgedAccess().getGroup_4()); 
            // InternalBuilding.g:3079:2: ( rule__Badged__Group_4__0 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==30) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalBuilding.g:3079:3: rule__Badged__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badged__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgedAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__4__Impl"


    // $ANTLR start "rule__Badged__Group__5"
    // InternalBuilding.g:3087:1: rule__Badged__Group__5 : rule__Badged__Group__5__Impl rule__Badged__Group__6 ;
    public final void rule__Badged__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3091:1: ( rule__Badged__Group__5__Impl rule__Badged__Group__6 )
            // InternalBuilding.g:3092:2: rule__Badged__Group__5__Impl rule__Badged__Group__6
            {
            pushFollow(FOLLOW_25);
            rule__Badged__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__5"


    // $ANTLR start "rule__Badged__Group__5__Impl"
    // InternalBuilding.g:3099:1: rule__Badged__Group__5__Impl : ( ( rule__Badged__Group_5__0 )? ) ;
    public final void rule__Badged__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3103:1: ( ( ( rule__Badged__Group_5__0 )? ) )
            // InternalBuilding.g:3104:1: ( ( rule__Badged__Group_5__0 )? )
            {
            // InternalBuilding.g:3104:1: ( ( rule__Badged__Group_5__0 )? )
            // InternalBuilding.g:3105:2: ( rule__Badged__Group_5__0 )?
            {
             before(grammarAccess.getBadgedAccess().getGroup_5()); 
            // InternalBuilding.g:3106:2: ( rule__Badged__Group_5__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==31) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalBuilding.g:3106:3: rule__Badged__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badged__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgedAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__5__Impl"


    // $ANTLR start "rule__Badged__Group__6"
    // InternalBuilding.g:3114:1: rule__Badged__Group__6 : rule__Badged__Group__6__Impl rule__Badged__Group__7 ;
    public final void rule__Badged__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3118:1: ( rule__Badged__Group__6__Impl rule__Badged__Group__7 )
            // InternalBuilding.g:3119:2: rule__Badged__Group__6__Impl rule__Badged__Group__7
            {
            pushFollow(FOLLOW_25);
            rule__Badged__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__6"


    // $ANTLR start "rule__Badged__Group__6__Impl"
    // InternalBuilding.g:3126:1: rule__Badged__Group__6__Impl : ( ( rule__Badged__Group_6__0 )? ) ;
    public final void rule__Badged__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3130:1: ( ( ( rule__Badged__Group_6__0 )? ) )
            // InternalBuilding.g:3131:1: ( ( rule__Badged__Group_6__0 )? )
            {
            // InternalBuilding.g:3131:1: ( ( rule__Badged__Group_6__0 )? )
            // InternalBuilding.g:3132:2: ( rule__Badged__Group_6__0 )?
            {
             before(grammarAccess.getBadgedAccess().getGroup_6()); 
            // InternalBuilding.g:3133:2: ( rule__Badged__Group_6__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==33) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalBuilding.g:3133:3: rule__Badged__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badged__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgedAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__6__Impl"


    // $ANTLR start "rule__Badged__Group__7"
    // InternalBuilding.g:3141:1: rule__Badged__Group__7 : rule__Badged__Group__7__Impl rule__Badged__Group__8 ;
    public final void rule__Badged__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3145:1: ( rule__Badged__Group__7__Impl rule__Badged__Group__8 )
            // InternalBuilding.g:3146:2: rule__Badged__Group__7__Impl rule__Badged__Group__8
            {
            pushFollow(FOLLOW_25);
            rule__Badged__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__7"


    // $ANTLR start "rule__Badged__Group__7__Impl"
    // InternalBuilding.g:3153:1: rule__Badged__Group__7__Impl : ( ( rule__Badged__Group_7__0 )? ) ;
    public final void rule__Badged__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3157:1: ( ( ( rule__Badged__Group_7__0 )? ) )
            // InternalBuilding.g:3158:1: ( ( rule__Badged__Group_7__0 )? )
            {
            // InternalBuilding.g:3158:1: ( ( rule__Badged__Group_7__0 )? )
            // InternalBuilding.g:3159:2: ( rule__Badged__Group_7__0 )?
            {
             before(grammarAccess.getBadgedAccess().getGroup_7()); 
            // InternalBuilding.g:3160:2: ( rule__Badged__Group_7__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==16) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalBuilding.g:3160:3: rule__Badged__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badged__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgedAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__7__Impl"


    // $ANTLR start "rule__Badged__Group__8"
    // InternalBuilding.g:3168:1: rule__Badged__Group__8 : rule__Badged__Group__8__Impl ;
    public final void rule__Badged__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3172:1: ( rule__Badged__Group__8__Impl )
            // InternalBuilding.g:3173:2: rule__Badged__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badged__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__8"


    // $ANTLR start "rule__Badged__Group__8__Impl"
    // InternalBuilding.g:3179:1: rule__Badged__Group__8__Impl : ( '}' ) ;
    public final void rule__Badged__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3183:1: ( ( '}' ) )
            // InternalBuilding.g:3184:1: ( '}' )
            {
            // InternalBuilding.g:3184:1: ( '}' )
            // InternalBuilding.g:3185:2: '}'
            {
             before(grammarAccess.getBadgedAccess().getRightCurlyBracketKeyword_8()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group__8__Impl"


    // $ANTLR start "rule__Badged__Group_3__0"
    // InternalBuilding.g:3195:1: rule__Badged__Group_3__0 : rule__Badged__Group_3__0__Impl rule__Badged__Group_3__1 ;
    public final void rule__Badged__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3199:1: ( rule__Badged__Group_3__0__Impl rule__Badged__Group_3__1 )
            // InternalBuilding.g:3200:2: rule__Badged__Group_3__0__Impl rule__Badged__Group_3__1
            {
            pushFollow(FOLLOW_23);
            rule__Badged__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_3__0"


    // $ANTLR start "rule__Badged__Group_3__0__Impl"
    // InternalBuilding.g:3207:1: rule__Badged__Group_3__0__Impl : ( 'level' ) ;
    public final void rule__Badged__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3211:1: ( ( 'level' ) )
            // InternalBuilding.g:3212:1: ( 'level' )
            {
            // InternalBuilding.g:3212:1: ( 'level' )
            // InternalBuilding.g:3213:2: 'level'
            {
             before(grammarAccess.getBadgedAccess().getLevelKeyword_3_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getLevelKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_3__0__Impl"


    // $ANTLR start "rule__Badged__Group_3__1"
    // InternalBuilding.g:3222:1: rule__Badged__Group_3__1 : rule__Badged__Group_3__1__Impl ;
    public final void rule__Badged__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3226:1: ( rule__Badged__Group_3__1__Impl )
            // InternalBuilding.g:3227:2: rule__Badged__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badged__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_3__1"


    // $ANTLR start "rule__Badged__Group_3__1__Impl"
    // InternalBuilding.g:3233:1: rule__Badged__Group_3__1__Impl : ( ( rule__Badged__LevelAssignment_3_1 ) ) ;
    public final void rule__Badged__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3237:1: ( ( ( rule__Badged__LevelAssignment_3_1 ) ) )
            // InternalBuilding.g:3238:1: ( ( rule__Badged__LevelAssignment_3_1 ) )
            {
            // InternalBuilding.g:3238:1: ( ( rule__Badged__LevelAssignment_3_1 ) )
            // InternalBuilding.g:3239:2: ( rule__Badged__LevelAssignment_3_1 )
            {
             before(grammarAccess.getBadgedAccess().getLevelAssignment_3_1()); 
            // InternalBuilding.g:3240:2: ( rule__Badged__LevelAssignment_3_1 )
            // InternalBuilding.g:3240:3: rule__Badged__LevelAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Badged__LevelAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgedAccess().getLevelAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_3__1__Impl"


    // $ANTLR start "rule__Badged__Group_4__0"
    // InternalBuilding.g:3249:1: rule__Badged__Group_4__0 : rule__Badged__Group_4__0__Impl rule__Badged__Group_4__1 ;
    public final void rule__Badged__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3253:1: ( rule__Badged__Group_4__0__Impl rule__Badged__Group_4__1 )
            // InternalBuilding.g:3254:2: rule__Badged__Group_4__0__Impl rule__Badged__Group_4__1
            {
            pushFollow(FOLLOW_6);
            rule__Badged__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_4__0"


    // $ANTLR start "rule__Badged__Group_4__0__Impl"
    // InternalBuilding.g:3261:1: rule__Badged__Group_4__0__Impl : ( 'inside' ) ;
    public final void rule__Badged__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3265:1: ( ( 'inside' ) )
            // InternalBuilding.g:3266:1: ( 'inside' )
            {
            // InternalBuilding.g:3266:1: ( 'inside' )
            // InternalBuilding.g:3267:2: 'inside'
            {
             before(grammarAccess.getBadgedAccess().getInsideKeyword_4_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getInsideKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_4__0__Impl"


    // $ANTLR start "rule__Badged__Group_4__1"
    // InternalBuilding.g:3276:1: rule__Badged__Group_4__1 : rule__Badged__Group_4__1__Impl ;
    public final void rule__Badged__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3280:1: ( rule__Badged__Group_4__1__Impl )
            // InternalBuilding.g:3281:2: rule__Badged__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badged__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_4__1"


    // $ANTLR start "rule__Badged__Group_4__1__Impl"
    // InternalBuilding.g:3287:1: rule__Badged__Group_4__1__Impl : ( ( rule__Badged__InsideAssignment_4_1 ) ) ;
    public final void rule__Badged__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3291:1: ( ( ( rule__Badged__InsideAssignment_4_1 ) ) )
            // InternalBuilding.g:3292:1: ( ( rule__Badged__InsideAssignment_4_1 ) )
            {
            // InternalBuilding.g:3292:1: ( ( rule__Badged__InsideAssignment_4_1 ) )
            // InternalBuilding.g:3293:2: ( rule__Badged__InsideAssignment_4_1 )
            {
             before(grammarAccess.getBadgedAccess().getInsideAssignment_4_1()); 
            // InternalBuilding.g:3294:2: ( rule__Badged__InsideAssignment_4_1 )
            // InternalBuilding.g:3294:3: rule__Badged__InsideAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Badged__InsideAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgedAccess().getInsideAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_4__1__Impl"


    // $ANTLR start "rule__Badged__Group_5__0"
    // InternalBuilding.g:3303:1: rule__Badged__Group_5__0 : rule__Badged__Group_5__0__Impl rule__Badged__Group_5__1 ;
    public final void rule__Badged__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3307:1: ( rule__Badged__Group_5__0__Impl rule__Badged__Group_5__1 )
            // InternalBuilding.g:3308:2: rule__Badged__Group_5__0__Impl rule__Badged__Group_5__1
            {
            pushFollow(FOLLOW_6);
            rule__Badged__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_5__0"


    // $ANTLR start "rule__Badged__Group_5__0__Impl"
    // InternalBuilding.g:3315:1: rule__Badged__Group_5__0__Impl : ( 'outside' ) ;
    public final void rule__Badged__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3319:1: ( ( 'outside' ) )
            // InternalBuilding.g:3320:1: ( 'outside' )
            {
            // InternalBuilding.g:3320:1: ( 'outside' )
            // InternalBuilding.g:3321:2: 'outside'
            {
             before(grammarAccess.getBadgedAccess().getOutsideKeyword_5_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getOutsideKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_5__0__Impl"


    // $ANTLR start "rule__Badged__Group_5__1"
    // InternalBuilding.g:3330:1: rule__Badged__Group_5__1 : rule__Badged__Group_5__1__Impl ;
    public final void rule__Badged__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3334:1: ( rule__Badged__Group_5__1__Impl )
            // InternalBuilding.g:3335:2: rule__Badged__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badged__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_5__1"


    // $ANTLR start "rule__Badged__Group_5__1__Impl"
    // InternalBuilding.g:3341:1: rule__Badged__Group_5__1__Impl : ( ( rule__Badged__OutsideAssignment_5_1 ) ) ;
    public final void rule__Badged__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3345:1: ( ( ( rule__Badged__OutsideAssignment_5_1 ) ) )
            // InternalBuilding.g:3346:1: ( ( rule__Badged__OutsideAssignment_5_1 ) )
            {
            // InternalBuilding.g:3346:1: ( ( rule__Badged__OutsideAssignment_5_1 ) )
            // InternalBuilding.g:3347:2: ( rule__Badged__OutsideAssignment_5_1 )
            {
             before(grammarAccess.getBadgedAccess().getOutsideAssignment_5_1()); 
            // InternalBuilding.g:3348:2: ( rule__Badged__OutsideAssignment_5_1 )
            // InternalBuilding.g:3348:3: rule__Badged__OutsideAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Badged__OutsideAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgedAccess().getOutsideAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_5__1__Impl"


    // $ANTLR start "rule__Badged__Group_6__0"
    // InternalBuilding.g:3357:1: rule__Badged__Group_6__0 : rule__Badged__Group_6__0__Impl rule__Badged__Group_6__1 ;
    public final void rule__Badged__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3361:1: ( rule__Badged__Group_6__0__Impl rule__Badged__Group_6__1 )
            // InternalBuilding.g:3362:2: rule__Badged__Group_6__0__Impl rule__Badged__Group_6__1
            {
            pushFollow(FOLLOW_13);
            rule__Badged__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__0"


    // $ANTLR start "rule__Badged__Group_6__0__Impl"
    // InternalBuilding.g:3369:1: rule__Badged__Group_6__0__Impl : ( 'badges' ) ;
    public final void rule__Badged__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3373:1: ( ( 'badges' ) )
            // InternalBuilding.g:3374:1: ( 'badges' )
            {
            // InternalBuilding.g:3374:1: ( 'badges' )
            // InternalBuilding.g:3375:2: 'badges'
            {
             before(grammarAccess.getBadgedAccess().getBadgesKeyword_6_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getBadgesKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__0__Impl"


    // $ANTLR start "rule__Badged__Group_6__1"
    // InternalBuilding.g:3384:1: rule__Badged__Group_6__1 : rule__Badged__Group_6__1__Impl rule__Badged__Group_6__2 ;
    public final void rule__Badged__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3388:1: ( rule__Badged__Group_6__1__Impl rule__Badged__Group_6__2 )
            // InternalBuilding.g:3389:2: rule__Badged__Group_6__1__Impl rule__Badged__Group_6__2
            {
            pushFollow(FOLLOW_6);
            rule__Badged__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__1"


    // $ANTLR start "rule__Badged__Group_6__1__Impl"
    // InternalBuilding.g:3396:1: rule__Badged__Group_6__1__Impl : ( '(' ) ;
    public final void rule__Badged__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3400:1: ( ( '(' ) )
            // InternalBuilding.g:3401:1: ( '(' )
            {
            // InternalBuilding.g:3401:1: ( '(' )
            // InternalBuilding.g:3402:2: '('
            {
             before(grammarAccess.getBadgedAccess().getLeftParenthesisKeyword_6_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getLeftParenthesisKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__1__Impl"


    // $ANTLR start "rule__Badged__Group_6__2"
    // InternalBuilding.g:3411:1: rule__Badged__Group_6__2 : rule__Badged__Group_6__2__Impl rule__Badged__Group_6__3 ;
    public final void rule__Badged__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3415:1: ( rule__Badged__Group_6__2__Impl rule__Badged__Group_6__3 )
            // InternalBuilding.g:3416:2: rule__Badged__Group_6__2__Impl rule__Badged__Group_6__3
            {
            pushFollow(FOLLOW_14);
            rule__Badged__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__2"


    // $ANTLR start "rule__Badged__Group_6__2__Impl"
    // InternalBuilding.g:3423:1: rule__Badged__Group_6__2__Impl : ( ( rule__Badged__BadgesAssignment_6_2 ) ) ;
    public final void rule__Badged__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3427:1: ( ( ( rule__Badged__BadgesAssignment_6_2 ) ) )
            // InternalBuilding.g:3428:1: ( ( rule__Badged__BadgesAssignment_6_2 ) )
            {
            // InternalBuilding.g:3428:1: ( ( rule__Badged__BadgesAssignment_6_2 ) )
            // InternalBuilding.g:3429:2: ( rule__Badged__BadgesAssignment_6_2 )
            {
             before(grammarAccess.getBadgedAccess().getBadgesAssignment_6_2()); 
            // InternalBuilding.g:3430:2: ( rule__Badged__BadgesAssignment_6_2 )
            // InternalBuilding.g:3430:3: rule__Badged__BadgesAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__Badged__BadgesAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getBadgedAccess().getBadgesAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__2__Impl"


    // $ANTLR start "rule__Badged__Group_6__3"
    // InternalBuilding.g:3438:1: rule__Badged__Group_6__3 : rule__Badged__Group_6__3__Impl rule__Badged__Group_6__4 ;
    public final void rule__Badged__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3442:1: ( rule__Badged__Group_6__3__Impl rule__Badged__Group_6__4 )
            // InternalBuilding.g:3443:2: rule__Badged__Group_6__3__Impl rule__Badged__Group_6__4
            {
            pushFollow(FOLLOW_14);
            rule__Badged__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__3"


    // $ANTLR start "rule__Badged__Group_6__3__Impl"
    // InternalBuilding.g:3450:1: rule__Badged__Group_6__3__Impl : ( ( rule__Badged__Group_6_3__0 )* ) ;
    public final void rule__Badged__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3454:1: ( ( ( rule__Badged__Group_6_3__0 )* ) )
            // InternalBuilding.g:3455:1: ( ( rule__Badged__Group_6_3__0 )* )
            {
            // InternalBuilding.g:3455:1: ( ( rule__Badged__Group_6_3__0 )* )
            // InternalBuilding.g:3456:2: ( rule__Badged__Group_6_3__0 )*
            {
             before(grammarAccess.getBadgedAccess().getGroup_6_3()); 
            // InternalBuilding.g:3457:2: ( rule__Badged__Group_6_3__0 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==19) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalBuilding.g:3457:3: rule__Badged__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Badged__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getBadgedAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__3__Impl"


    // $ANTLR start "rule__Badged__Group_6__4"
    // InternalBuilding.g:3465:1: rule__Badged__Group_6__4 : rule__Badged__Group_6__4__Impl ;
    public final void rule__Badged__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3469:1: ( rule__Badged__Group_6__4__Impl )
            // InternalBuilding.g:3470:2: rule__Badged__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badged__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__4"


    // $ANTLR start "rule__Badged__Group_6__4__Impl"
    // InternalBuilding.g:3476:1: rule__Badged__Group_6__4__Impl : ( ')' ) ;
    public final void rule__Badged__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3480:1: ( ( ')' ) )
            // InternalBuilding.g:3481:1: ( ')' )
            {
            // InternalBuilding.g:3481:1: ( ')' )
            // InternalBuilding.g:3482:2: ')'
            {
             before(grammarAccess.getBadgedAccess().getRightParenthesisKeyword_6_4()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getRightParenthesisKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6__4__Impl"


    // $ANTLR start "rule__Badged__Group_6_3__0"
    // InternalBuilding.g:3492:1: rule__Badged__Group_6_3__0 : rule__Badged__Group_6_3__0__Impl rule__Badged__Group_6_3__1 ;
    public final void rule__Badged__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3496:1: ( rule__Badged__Group_6_3__0__Impl rule__Badged__Group_6_3__1 )
            // InternalBuilding.g:3497:2: rule__Badged__Group_6_3__0__Impl rule__Badged__Group_6_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Badged__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6_3__0"


    // $ANTLR start "rule__Badged__Group_6_3__0__Impl"
    // InternalBuilding.g:3504:1: rule__Badged__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__Badged__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3508:1: ( ( ',' ) )
            // InternalBuilding.g:3509:1: ( ',' )
            {
            // InternalBuilding.g:3509:1: ( ',' )
            // InternalBuilding.g:3510:2: ','
            {
             before(grammarAccess.getBadgedAccess().getCommaKeyword_6_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6_3__0__Impl"


    // $ANTLR start "rule__Badged__Group_6_3__1"
    // InternalBuilding.g:3519:1: rule__Badged__Group_6_3__1 : rule__Badged__Group_6_3__1__Impl ;
    public final void rule__Badged__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3523:1: ( rule__Badged__Group_6_3__1__Impl )
            // InternalBuilding.g:3524:2: rule__Badged__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badged__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6_3__1"


    // $ANTLR start "rule__Badged__Group_6_3__1__Impl"
    // InternalBuilding.g:3530:1: rule__Badged__Group_6_3__1__Impl : ( ( rule__Badged__BadgesAssignment_6_3_1 ) ) ;
    public final void rule__Badged__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3534:1: ( ( ( rule__Badged__BadgesAssignment_6_3_1 ) ) )
            // InternalBuilding.g:3535:1: ( ( rule__Badged__BadgesAssignment_6_3_1 ) )
            {
            // InternalBuilding.g:3535:1: ( ( rule__Badged__BadgesAssignment_6_3_1 ) )
            // InternalBuilding.g:3536:2: ( rule__Badged__BadgesAssignment_6_3_1 )
            {
             before(grammarAccess.getBadgedAccess().getBadgesAssignment_6_3_1()); 
            // InternalBuilding.g:3537:2: ( rule__Badged__BadgesAssignment_6_3_1 )
            // InternalBuilding.g:3537:3: rule__Badged__BadgesAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Badged__BadgesAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgedAccess().getBadgesAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_6_3__1__Impl"


    // $ANTLR start "rule__Badged__Group_7__0"
    // InternalBuilding.g:3546:1: rule__Badged__Group_7__0 : rule__Badged__Group_7__0__Impl rule__Badged__Group_7__1 ;
    public final void rule__Badged__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3550:1: ( rule__Badged__Group_7__0__Impl rule__Badged__Group_7__1 )
            // InternalBuilding.g:3551:2: rule__Badged__Group_7__0__Impl rule__Badged__Group_7__1
            {
            pushFollow(FOLLOW_13);
            rule__Badged__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__0"


    // $ANTLR start "rule__Badged__Group_7__0__Impl"
    // InternalBuilding.g:3558:1: rule__Badged__Group_7__0__Impl : ( 'alarms' ) ;
    public final void rule__Badged__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3562:1: ( ( 'alarms' ) )
            // InternalBuilding.g:3563:1: ( 'alarms' )
            {
            // InternalBuilding.g:3563:1: ( 'alarms' )
            // InternalBuilding.g:3564:2: 'alarms'
            {
             before(grammarAccess.getBadgedAccess().getAlarmsKeyword_7_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getAlarmsKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__0__Impl"


    // $ANTLR start "rule__Badged__Group_7__1"
    // InternalBuilding.g:3573:1: rule__Badged__Group_7__1 : rule__Badged__Group_7__1__Impl rule__Badged__Group_7__2 ;
    public final void rule__Badged__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3577:1: ( rule__Badged__Group_7__1__Impl rule__Badged__Group_7__2 )
            // InternalBuilding.g:3578:2: rule__Badged__Group_7__1__Impl rule__Badged__Group_7__2
            {
            pushFollow(FOLLOW_6);
            rule__Badged__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__1"


    // $ANTLR start "rule__Badged__Group_7__1__Impl"
    // InternalBuilding.g:3585:1: rule__Badged__Group_7__1__Impl : ( '(' ) ;
    public final void rule__Badged__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3589:1: ( ( '(' ) )
            // InternalBuilding.g:3590:1: ( '(' )
            {
            // InternalBuilding.g:3590:1: ( '(' )
            // InternalBuilding.g:3591:2: '('
            {
             before(grammarAccess.getBadgedAccess().getLeftParenthesisKeyword_7_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getLeftParenthesisKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__1__Impl"


    // $ANTLR start "rule__Badged__Group_7__2"
    // InternalBuilding.g:3600:1: rule__Badged__Group_7__2 : rule__Badged__Group_7__2__Impl rule__Badged__Group_7__3 ;
    public final void rule__Badged__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3604:1: ( rule__Badged__Group_7__2__Impl rule__Badged__Group_7__3 )
            // InternalBuilding.g:3605:2: rule__Badged__Group_7__2__Impl rule__Badged__Group_7__3
            {
            pushFollow(FOLLOW_14);
            rule__Badged__Group_7__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_7__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__2"


    // $ANTLR start "rule__Badged__Group_7__2__Impl"
    // InternalBuilding.g:3612:1: rule__Badged__Group_7__2__Impl : ( ( rule__Badged__AlarmsAssignment_7_2 ) ) ;
    public final void rule__Badged__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3616:1: ( ( ( rule__Badged__AlarmsAssignment_7_2 ) ) )
            // InternalBuilding.g:3617:1: ( ( rule__Badged__AlarmsAssignment_7_2 ) )
            {
            // InternalBuilding.g:3617:1: ( ( rule__Badged__AlarmsAssignment_7_2 ) )
            // InternalBuilding.g:3618:2: ( rule__Badged__AlarmsAssignment_7_2 )
            {
             before(grammarAccess.getBadgedAccess().getAlarmsAssignment_7_2()); 
            // InternalBuilding.g:3619:2: ( rule__Badged__AlarmsAssignment_7_2 )
            // InternalBuilding.g:3619:3: rule__Badged__AlarmsAssignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__Badged__AlarmsAssignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getBadgedAccess().getAlarmsAssignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__2__Impl"


    // $ANTLR start "rule__Badged__Group_7__3"
    // InternalBuilding.g:3627:1: rule__Badged__Group_7__3 : rule__Badged__Group_7__3__Impl rule__Badged__Group_7__4 ;
    public final void rule__Badged__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3631:1: ( rule__Badged__Group_7__3__Impl rule__Badged__Group_7__4 )
            // InternalBuilding.g:3632:2: rule__Badged__Group_7__3__Impl rule__Badged__Group_7__4
            {
            pushFollow(FOLLOW_14);
            rule__Badged__Group_7__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_7__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__3"


    // $ANTLR start "rule__Badged__Group_7__3__Impl"
    // InternalBuilding.g:3639:1: rule__Badged__Group_7__3__Impl : ( ( rule__Badged__Group_7_3__0 )* ) ;
    public final void rule__Badged__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3643:1: ( ( ( rule__Badged__Group_7_3__0 )* ) )
            // InternalBuilding.g:3644:1: ( ( rule__Badged__Group_7_3__0 )* )
            {
            // InternalBuilding.g:3644:1: ( ( rule__Badged__Group_7_3__0 )* )
            // InternalBuilding.g:3645:2: ( rule__Badged__Group_7_3__0 )*
            {
             before(grammarAccess.getBadgedAccess().getGroup_7_3()); 
            // InternalBuilding.g:3646:2: ( rule__Badged__Group_7_3__0 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==19) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalBuilding.g:3646:3: rule__Badged__Group_7_3__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Badged__Group_7_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getBadgedAccess().getGroup_7_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__3__Impl"


    // $ANTLR start "rule__Badged__Group_7__4"
    // InternalBuilding.g:3654:1: rule__Badged__Group_7__4 : rule__Badged__Group_7__4__Impl ;
    public final void rule__Badged__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3658:1: ( rule__Badged__Group_7__4__Impl )
            // InternalBuilding.g:3659:2: rule__Badged__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badged__Group_7__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__4"


    // $ANTLR start "rule__Badged__Group_7__4__Impl"
    // InternalBuilding.g:3665:1: rule__Badged__Group_7__4__Impl : ( ')' ) ;
    public final void rule__Badged__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3669:1: ( ( ')' ) )
            // InternalBuilding.g:3670:1: ( ')' )
            {
            // InternalBuilding.g:3670:1: ( ')' )
            // InternalBuilding.g:3671:2: ')'
            {
             before(grammarAccess.getBadgedAccess().getRightParenthesisKeyword_7_4()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getRightParenthesisKeyword_7_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7__4__Impl"


    // $ANTLR start "rule__Badged__Group_7_3__0"
    // InternalBuilding.g:3681:1: rule__Badged__Group_7_3__0 : rule__Badged__Group_7_3__0__Impl rule__Badged__Group_7_3__1 ;
    public final void rule__Badged__Group_7_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3685:1: ( rule__Badged__Group_7_3__0__Impl rule__Badged__Group_7_3__1 )
            // InternalBuilding.g:3686:2: rule__Badged__Group_7_3__0__Impl rule__Badged__Group_7_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Badged__Group_7_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badged__Group_7_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7_3__0"


    // $ANTLR start "rule__Badged__Group_7_3__0__Impl"
    // InternalBuilding.g:3693:1: rule__Badged__Group_7_3__0__Impl : ( ',' ) ;
    public final void rule__Badged__Group_7_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3697:1: ( ( ',' ) )
            // InternalBuilding.g:3698:1: ( ',' )
            {
            // InternalBuilding.g:3698:1: ( ',' )
            // InternalBuilding.g:3699:2: ','
            {
             before(grammarAccess.getBadgedAccess().getCommaKeyword_7_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getBadgedAccess().getCommaKeyword_7_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7_3__0__Impl"


    // $ANTLR start "rule__Badged__Group_7_3__1"
    // InternalBuilding.g:3708:1: rule__Badged__Group_7_3__1 : rule__Badged__Group_7_3__1__Impl ;
    public final void rule__Badged__Group_7_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3712:1: ( rule__Badged__Group_7_3__1__Impl )
            // InternalBuilding.g:3713:2: rule__Badged__Group_7_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badged__Group_7_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7_3__1"


    // $ANTLR start "rule__Badged__Group_7_3__1__Impl"
    // InternalBuilding.g:3719:1: rule__Badged__Group_7_3__1__Impl : ( ( rule__Badged__AlarmsAssignment_7_3_1 ) ) ;
    public final void rule__Badged__Group_7_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3723:1: ( ( ( rule__Badged__AlarmsAssignment_7_3_1 ) ) )
            // InternalBuilding.g:3724:1: ( ( rule__Badged__AlarmsAssignment_7_3_1 ) )
            {
            // InternalBuilding.g:3724:1: ( ( rule__Badged__AlarmsAssignment_7_3_1 ) )
            // InternalBuilding.g:3725:2: ( rule__Badged__AlarmsAssignment_7_3_1 )
            {
             before(grammarAccess.getBadgedAccess().getAlarmsAssignment_7_3_1()); 
            // InternalBuilding.g:3726:2: ( rule__Badged__AlarmsAssignment_7_3_1 )
            // InternalBuilding.g:3726:3: rule__Badged__AlarmsAssignment_7_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Badged__AlarmsAssignment_7_3_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgedAccess().getAlarmsAssignment_7_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__Group_7_3__1__Impl"


    // $ANTLR start "rule__Item__Group__0"
    // InternalBuilding.g:3735:1: rule__Item__Group__0 : rule__Item__Group__0__Impl rule__Item__Group__1 ;
    public final void rule__Item__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3739:1: ( rule__Item__Group__0__Impl rule__Item__Group__1 )
            // InternalBuilding.g:3740:2: rule__Item__Group__0__Impl rule__Item__Group__1
            {
            pushFollow(FOLLOW_26);
            rule__Item__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Item__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__Group__0"


    // $ANTLR start "rule__Item__Group__0__Impl"
    // InternalBuilding.g:3747:1: rule__Item__Group__0__Impl : ( () ) ;
    public final void rule__Item__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3751:1: ( ( () ) )
            // InternalBuilding.g:3752:1: ( () )
            {
            // InternalBuilding.g:3752:1: ( () )
            // InternalBuilding.g:3753:2: ()
            {
             before(grammarAccess.getItemAccess().getItemAction_0()); 
            // InternalBuilding.g:3754:2: ()
            // InternalBuilding.g:3754:3: 
            {
            }

             after(grammarAccess.getItemAccess().getItemAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__Group__0__Impl"


    // $ANTLR start "rule__Item__Group__1"
    // InternalBuilding.g:3762:1: rule__Item__Group__1 : rule__Item__Group__1__Impl rule__Item__Group__2 ;
    public final void rule__Item__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3766:1: ( rule__Item__Group__1__Impl rule__Item__Group__2 )
            // InternalBuilding.g:3767:2: rule__Item__Group__1__Impl rule__Item__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Item__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Item__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__Group__1"


    // $ANTLR start "rule__Item__Group__1__Impl"
    // InternalBuilding.g:3774:1: rule__Item__Group__1__Impl : ( 'Item' ) ;
    public final void rule__Item__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3778:1: ( ( 'Item' ) )
            // InternalBuilding.g:3779:1: ( 'Item' )
            {
            // InternalBuilding.g:3779:1: ( 'Item' )
            // InternalBuilding.g:3780:2: 'Item'
            {
             before(grammarAccess.getItemAccess().getItemKeyword_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getItemAccess().getItemKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__Group__1__Impl"


    // $ANTLR start "rule__Item__Group__2"
    // InternalBuilding.g:3789:1: rule__Item__Group__2 : rule__Item__Group__2__Impl ;
    public final void rule__Item__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3793:1: ( rule__Item__Group__2__Impl )
            // InternalBuilding.g:3794:2: rule__Item__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Item__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__Group__2"


    // $ANTLR start "rule__Item__Group__2__Impl"
    // InternalBuilding.g:3800:1: rule__Item__Group__2__Impl : ( ( rule__Item__NameAssignment_2 ) ) ;
    public final void rule__Item__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3804:1: ( ( ( rule__Item__NameAssignment_2 ) ) )
            // InternalBuilding.g:3805:1: ( ( rule__Item__NameAssignment_2 ) )
            {
            // InternalBuilding.g:3805:1: ( ( rule__Item__NameAssignment_2 ) )
            // InternalBuilding.g:3806:2: ( rule__Item__NameAssignment_2 )
            {
             before(grammarAccess.getItemAccess().getNameAssignment_2()); 
            // InternalBuilding.g:3807:2: ( rule__Item__NameAssignment_2 )
            // InternalBuilding.g:3807:3: rule__Item__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Item__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getItemAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__Group__2__Impl"


    // $ANTLR start "rule__Attacker__Group__0"
    // InternalBuilding.g:3816:1: rule__Attacker__Group__0 : rule__Attacker__Group__0__Impl rule__Attacker__Group__1 ;
    public final void rule__Attacker__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3820:1: ( rule__Attacker__Group__0__Impl rule__Attacker__Group__1 )
            // InternalBuilding.g:3821:2: rule__Attacker__Group__0__Impl rule__Attacker__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__Attacker__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__0"


    // $ANTLR start "rule__Attacker__Group__0__Impl"
    // InternalBuilding.g:3828:1: rule__Attacker__Group__0__Impl : ( () ) ;
    public final void rule__Attacker__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3832:1: ( ( () ) )
            // InternalBuilding.g:3833:1: ( () )
            {
            // InternalBuilding.g:3833:1: ( () )
            // InternalBuilding.g:3834:2: ()
            {
             before(grammarAccess.getAttackerAccess().getAttackerAction_0()); 
            // InternalBuilding.g:3835:2: ()
            // InternalBuilding.g:3835:3: 
            {
            }

             after(grammarAccess.getAttackerAccess().getAttackerAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__0__Impl"


    // $ANTLR start "rule__Attacker__Group__1"
    // InternalBuilding.g:3843:1: rule__Attacker__Group__1 : rule__Attacker__Group__1__Impl rule__Attacker__Group__2 ;
    public final void rule__Attacker__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3847:1: ( rule__Attacker__Group__1__Impl rule__Attacker__Group__2 )
            // InternalBuilding.g:3848:2: rule__Attacker__Group__1__Impl rule__Attacker__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Attacker__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__1"


    // $ANTLR start "rule__Attacker__Group__1__Impl"
    // InternalBuilding.g:3855:1: rule__Attacker__Group__1__Impl : ( 'Attacker' ) ;
    public final void rule__Attacker__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3859:1: ( ( 'Attacker' ) )
            // InternalBuilding.g:3860:1: ( 'Attacker' )
            {
            // InternalBuilding.g:3860:1: ( 'Attacker' )
            // InternalBuilding.g:3861:2: 'Attacker'
            {
             before(grammarAccess.getAttackerAccess().getAttackerKeyword_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getAttackerKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__1__Impl"


    // $ANTLR start "rule__Attacker__Group__2"
    // InternalBuilding.g:3870:1: rule__Attacker__Group__2 : rule__Attacker__Group__2__Impl rule__Attacker__Group__3 ;
    public final void rule__Attacker__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3874:1: ( rule__Attacker__Group__2__Impl rule__Attacker__Group__3 )
            // InternalBuilding.g:3875:2: rule__Attacker__Group__2__Impl rule__Attacker__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Attacker__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__2"


    // $ANTLR start "rule__Attacker__Group__2__Impl"
    // InternalBuilding.g:3882:1: rule__Attacker__Group__2__Impl : ( ( rule__Attacker__NameAssignment_2 ) ) ;
    public final void rule__Attacker__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3886:1: ( ( ( rule__Attacker__NameAssignment_2 ) ) )
            // InternalBuilding.g:3887:1: ( ( rule__Attacker__NameAssignment_2 ) )
            {
            // InternalBuilding.g:3887:1: ( ( rule__Attacker__NameAssignment_2 ) )
            // InternalBuilding.g:3888:2: ( rule__Attacker__NameAssignment_2 )
            {
             before(grammarAccess.getAttackerAccess().getNameAssignment_2()); 
            // InternalBuilding.g:3889:2: ( rule__Attacker__NameAssignment_2 )
            // InternalBuilding.g:3889:3: rule__Attacker__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__2__Impl"


    // $ANTLR start "rule__Attacker__Group__3"
    // InternalBuilding.g:3897:1: rule__Attacker__Group__3 : rule__Attacker__Group__3__Impl rule__Attacker__Group__4 ;
    public final void rule__Attacker__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3901:1: ( rule__Attacker__Group__3__Impl rule__Attacker__Group__4 )
            // InternalBuilding.g:3902:2: rule__Attacker__Group__3__Impl rule__Attacker__Group__4
            {
            pushFollow(FOLLOW_28);
            rule__Attacker__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__3"


    // $ANTLR start "rule__Attacker__Group__3__Impl"
    // InternalBuilding.g:3909:1: rule__Attacker__Group__3__Impl : ( '{' ) ;
    public final void rule__Attacker__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3913:1: ( ( '{' ) )
            // InternalBuilding.g:3914:1: ( '{' )
            {
            // InternalBuilding.g:3914:1: ( '{' )
            // InternalBuilding.g:3915:2: '{'
            {
             before(grammarAccess.getAttackerAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__3__Impl"


    // $ANTLR start "rule__Attacker__Group__4"
    // InternalBuilding.g:3924:1: rule__Attacker__Group__4 : rule__Attacker__Group__4__Impl rule__Attacker__Group__5 ;
    public final void rule__Attacker__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3928:1: ( rule__Attacker__Group__4__Impl rule__Attacker__Group__5 )
            // InternalBuilding.g:3929:2: rule__Attacker__Group__4__Impl rule__Attacker__Group__5
            {
            pushFollow(FOLLOW_28);
            rule__Attacker__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__4"


    // $ANTLR start "rule__Attacker__Group__4__Impl"
    // InternalBuilding.g:3936:1: rule__Attacker__Group__4__Impl : ( ( rule__Attacker__Group_4__0 )? ) ;
    public final void rule__Attacker__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3940:1: ( ( ( rule__Attacker__Group_4__0 )? ) )
            // InternalBuilding.g:3941:1: ( ( rule__Attacker__Group_4__0 )? )
            {
            // InternalBuilding.g:3941:1: ( ( rule__Attacker__Group_4__0 )? )
            // InternalBuilding.g:3942:2: ( rule__Attacker__Group_4__0 )?
            {
             before(grammarAccess.getAttackerAccess().getGroup_4()); 
            // InternalBuilding.g:3943:2: ( rule__Attacker__Group_4__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==27) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalBuilding.g:3943:3: rule__Attacker__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Attacker__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttackerAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__4__Impl"


    // $ANTLR start "rule__Attacker__Group__5"
    // InternalBuilding.g:3951:1: rule__Attacker__Group__5 : rule__Attacker__Group__5__Impl ;
    public final void rule__Attacker__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3955:1: ( rule__Attacker__Group__5__Impl )
            // InternalBuilding.g:3956:2: rule__Attacker__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__5"


    // $ANTLR start "rule__Attacker__Group__5__Impl"
    // InternalBuilding.g:3962:1: rule__Attacker__Group__5__Impl : ( '}' ) ;
    public final void rule__Attacker__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3966:1: ( ( '}' ) )
            // InternalBuilding.g:3967:1: ( '}' )
            {
            // InternalBuilding.g:3967:1: ( '}' )
            // InternalBuilding.g:3968:2: '}'
            {
             before(grammarAccess.getAttackerAccess().getRightCurlyBracketKeyword_5()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__5__Impl"


    // $ANTLR start "rule__Attacker__Group_4__0"
    // InternalBuilding.g:3978:1: rule__Attacker__Group_4__0 : rule__Attacker__Group_4__0__Impl rule__Attacker__Group_4__1 ;
    public final void rule__Attacker__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3982:1: ( rule__Attacker__Group_4__0__Impl rule__Attacker__Group_4__1 )
            // InternalBuilding.g:3983:2: rule__Attacker__Group_4__0__Impl rule__Attacker__Group_4__1
            {
            pushFollow(FOLLOW_23);
            rule__Attacker__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_4__0"


    // $ANTLR start "rule__Attacker__Group_4__0__Impl"
    // InternalBuilding.g:3990:1: rule__Attacker__Group_4__0__Impl : ( 'level' ) ;
    public final void rule__Attacker__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:3994:1: ( ( 'level' ) )
            // InternalBuilding.g:3995:1: ( 'level' )
            {
            // InternalBuilding.g:3995:1: ( 'level' )
            // InternalBuilding.g:3996:2: 'level'
            {
             before(grammarAccess.getAttackerAccess().getLevelKeyword_4_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getLevelKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_4__0__Impl"


    // $ANTLR start "rule__Attacker__Group_4__1"
    // InternalBuilding.g:4005:1: rule__Attacker__Group_4__1 : rule__Attacker__Group_4__1__Impl ;
    public final void rule__Attacker__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4009:1: ( rule__Attacker__Group_4__1__Impl )
            // InternalBuilding.g:4010:2: rule__Attacker__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_4__1"


    // $ANTLR start "rule__Attacker__Group_4__1__Impl"
    // InternalBuilding.g:4016:1: rule__Attacker__Group_4__1__Impl : ( ( rule__Attacker__LevelAssignment_4_1 ) ) ;
    public final void rule__Attacker__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4020:1: ( ( ( rule__Attacker__LevelAssignment_4_1 ) ) )
            // InternalBuilding.g:4021:1: ( ( rule__Attacker__LevelAssignment_4_1 ) )
            {
            // InternalBuilding.g:4021:1: ( ( rule__Attacker__LevelAssignment_4_1 ) )
            // InternalBuilding.g:4022:2: ( rule__Attacker__LevelAssignment_4_1 )
            {
             before(grammarAccess.getAttackerAccess().getLevelAssignment_4_1()); 
            // InternalBuilding.g:4023:2: ( rule__Attacker__LevelAssignment_4_1 )
            // InternalBuilding.g:4023:3: rule__Attacker__LevelAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__LevelAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getLevelAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_4__1__Impl"


    // $ANTLR start "rule__EInt__Group__0"
    // InternalBuilding.g:4032:1: rule__EInt__Group__0 : rule__EInt__Group__0__Impl rule__EInt__Group__1 ;
    public final void rule__EInt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4036:1: ( rule__EInt__Group__0__Impl rule__EInt__Group__1 )
            // InternalBuilding.g:4037:2: rule__EInt__Group__0__Impl rule__EInt__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__EInt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EInt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0"


    // $ANTLR start "rule__EInt__Group__0__Impl"
    // InternalBuilding.g:4044:1: rule__EInt__Group__0__Impl : ( ( '-' )? ) ;
    public final void rule__EInt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4048:1: ( ( ( '-' )? ) )
            // InternalBuilding.g:4049:1: ( ( '-' )? )
            {
            // InternalBuilding.g:4049:1: ( ( '-' )? )
            // InternalBuilding.g:4050:2: ( '-' )?
            {
             before(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 
            // InternalBuilding.g:4051:2: ( '-' )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==36) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalBuilding.g:4051:3: '-'
                    {
                    match(input,36,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0__Impl"


    // $ANTLR start "rule__EInt__Group__1"
    // InternalBuilding.g:4059:1: rule__EInt__Group__1 : rule__EInt__Group__1__Impl ;
    public final void rule__EInt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4063:1: ( rule__EInt__Group__1__Impl )
            // InternalBuilding.g:4064:2: rule__EInt__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1"


    // $ANTLR start "rule__EInt__Group__1__Impl"
    // InternalBuilding.g:4070:1: rule__EInt__Group__1__Impl : ( RULE_INT ) ;
    public final void rule__EInt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4074:1: ( ( RULE_INT ) )
            // InternalBuilding.g:4075:1: ( RULE_INT )
            {
            // InternalBuilding.g:4075:1: ( RULE_INT )
            // InternalBuilding.g:4076:2: RULE_INT
            {
             before(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1__Impl"


    // $ANTLR start "rule__BuildingModel__ImportsAssignment_1"
    // InternalBuilding.g:4086:1: rule__BuildingModel__ImportsAssignment_1 : ( ruleImport ) ;
    public final void rule__BuildingModel__ImportsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4090:1: ( ( ruleImport ) )
            // InternalBuilding.g:4091:2: ( ruleImport )
            {
            // InternalBuilding.g:4091:2: ( ruleImport )
            // InternalBuilding.g:4092:3: ruleImport
            {
             before(grammarAccess.getBuildingModelAccess().getImportsImportParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getBuildingModelAccess().getImportsImportParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingModel__ImportsAssignment_1"


    // $ANTLR start "rule__BuildingModel__BuildingsAssignment_2"
    // InternalBuilding.g:4101:1: rule__BuildingModel__BuildingsAssignment_2 : ( ruleBuilding ) ;
    public final void rule__BuildingModel__BuildingsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4105:1: ( ( ruleBuilding ) )
            // InternalBuilding.g:4106:2: ( ruleBuilding )
            {
            // InternalBuilding.g:4106:2: ( ruleBuilding )
            // InternalBuilding.g:4107:3: ruleBuilding
            {
             before(grammarAccess.getBuildingModelAccess().getBuildingsBuildingParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleBuilding();

            state._fsp--;

             after(grammarAccess.getBuildingModelAccess().getBuildingsBuildingParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingModel__BuildingsAssignment_2"


    // $ANTLR start "rule__Import__ImportURIAssignment_1"
    // InternalBuilding.g:4116:1: rule__Import__ImportURIAssignment_1 : ( ruleEString ) ;
    public final void rule__Import__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4120:1: ( ( ruleEString ) )
            // InternalBuilding.g:4121:2: ( ruleEString )
            {
            // InternalBuilding.g:4121:2: ( ruleEString )
            // InternalBuilding.g:4122:3: ruleEString
            {
             before(grammarAccess.getImportAccess().getImportURIEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getImportAccess().getImportURIEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportURIAssignment_1"


    // $ANTLR start "rule__Building__NameAssignment_2"
    // InternalBuilding.g:4131:1: rule__Building__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Building__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4135:1: ( ( ruleEString ) )
            // InternalBuilding.g:4136:2: ( ruleEString )
            {
            // InternalBuilding.g:4136:2: ( ruleEString )
            // InternalBuilding.g:4137:3: ruleEString
            {
             before(grammarAccess.getBuildingAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBuildingAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__NameAssignment_2"


    // $ANTLR start "rule__Building__AccessesAssignment_4_0"
    // InternalBuilding.g:4146:1: rule__Building__AccessesAssignment_4_0 : ( ruleAccess ) ;
    public final void rule__Building__AccessesAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4150:1: ( ( ruleAccess ) )
            // InternalBuilding.g:4151:2: ( ruleAccess )
            {
            // InternalBuilding.g:4151:2: ( ruleAccess )
            // InternalBuilding.g:4152:3: ruleAccess
            {
             before(grammarAccess.getBuildingAccess().getAccessesAccessParserRuleCall_4_0_0()); 
            pushFollow(FOLLOW_2);
            ruleAccess();

            state._fsp--;

             after(grammarAccess.getBuildingAccess().getAccessesAccessParserRuleCall_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__AccessesAssignment_4_0"


    // $ANTLR start "rule__Building__AlarmsAssignment_4_1"
    // InternalBuilding.g:4161:1: rule__Building__AlarmsAssignment_4_1 : ( ruleAlarm ) ;
    public final void rule__Building__AlarmsAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4165:1: ( ( ruleAlarm ) )
            // InternalBuilding.g:4166:2: ( ruleAlarm )
            {
            // InternalBuilding.g:4166:2: ( ruleAlarm )
            // InternalBuilding.g:4167:3: ruleAlarm
            {
             before(grammarAccess.getBuildingAccess().getAlarmsAlarmParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAlarm();

            state._fsp--;

             after(grammarAccess.getBuildingAccess().getAlarmsAlarmParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__AlarmsAssignment_4_1"


    // $ANTLR start "rule__Building__AttackersAssignment_4_2"
    // InternalBuilding.g:4176:1: rule__Building__AttackersAssignment_4_2 : ( ruleAttacker ) ;
    public final void rule__Building__AttackersAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4180:1: ( ( ruleAttacker ) )
            // InternalBuilding.g:4181:2: ( ruleAttacker )
            {
            // InternalBuilding.g:4181:2: ( ruleAttacker )
            // InternalBuilding.g:4182:3: ruleAttacker
            {
             before(grammarAccess.getBuildingAccess().getAttackersAttackerParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAttacker();

            state._fsp--;

             after(grammarAccess.getBuildingAccess().getAttackersAttackerParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__AttackersAssignment_4_2"


    // $ANTLR start "rule__Building__ItemsAssignment_4_3"
    // InternalBuilding.g:4191:1: rule__Building__ItemsAssignment_4_3 : ( ruleItem ) ;
    public final void rule__Building__ItemsAssignment_4_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4195:1: ( ( ruleItem ) )
            // InternalBuilding.g:4196:2: ( ruleItem )
            {
            // InternalBuilding.g:4196:2: ( ruleItem )
            // InternalBuilding.g:4197:3: ruleItem
            {
             before(grammarAccess.getBuildingAccess().getItemsItemParserRuleCall_4_3_0()); 
            pushFollow(FOLLOW_2);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getBuildingAccess().getItemsItemParserRuleCall_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__ItemsAssignment_4_3"


    // $ANTLR start "rule__Building__ZonesAssignment_4_4"
    // InternalBuilding.g:4206:1: rule__Building__ZonesAssignment_4_4 : ( ruleZone ) ;
    public final void rule__Building__ZonesAssignment_4_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4210:1: ( ( ruleZone ) )
            // InternalBuilding.g:4211:2: ( ruleZone )
            {
            // InternalBuilding.g:4211:2: ( ruleZone )
            // InternalBuilding.g:4212:3: ruleZone
            {
             before(grammarAccess.getBuildingAccess().getZonesZoneParserRuleCall_4_4_0()); 
            pushFollow(FOLLOW_2);
            ruleZone();

            state._fsp--;

             after(grammarAccess.getBuildingAccess().getZonesZoneParserRuleCall_4_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Building__ZonesAssignment_4_4"


    // $ANTLR start "rule__Zone__NameAssignment_2"
    // InternalBuilding.g:4221:1: rule__Zone__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Zone__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4225:1: ( ( ruleEString ) )
            // InternalBuilding.g:4226:2: ( ruleEString )
            {
            // InternalBuilding.g:4226:2: ( ruleEString )
            // InternalBuilding.g:4227:3: ruleEString
            {
             before(grammarAccess.getZoneAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getZoneAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__NameAssignment_2"


    // $ANTLR start "rule__Zone__AlarmsAssignment_4_2"
    // InternalBuilding.g:4236:1: rule__Zone__AlarmsAssignment_4_2 : ( ( ruleEString ) ) ;
    public final void rule__Zone__AlarmsAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4240:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4241:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4241:2: ( ( ruleEString ) )
            // InternalBuilding.g:4242:3: ( ruleEString )
            {
             before(grammarAccess.getZoneAccess().getAlarmsAlarmCrossReference_4_2_0()); 
            // InternalBuilding.g:4243:3: ( ruleEString )
            // InternalBuilding.g:4244:4: ruleEString
            {
             before(grammarAccess.getZoneAccess().getAlarmsAlarmEStringParserRuleCall_4_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getZoneAccess().getAlarmsAlarmEStringParserRuleCall_4_2_0_1()); 

            }

             after(grammarAccess.getZoneAccess().getAlarmsAlarmCrossReference_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__AlarmsAssignment_4_2"


    // $ANTLR start "rule__Zone__AlarmsAssignment_4_3_1"
    // InternalBuilding.g:4255:1: rule__Zone__AlarmsAssignment_4_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Zone__AlarmsAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4259:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4260:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4260:2: ( ( ruleEString ) )
            // InternalBuilding.g:4261:3: ( ruleEString )
            {
             before(grammarAccess.getZoneAccess().getAlarmsAlarmCrossReference_4_3_1_0()); 
            // InternalBuilding.g:4262:3: ( ruleEString )
            // InternalBuilding.g:4263:4: ruleEString
            {
             before(grammarAccess.getZoneAccess().getAlarmsAlarmEStringParserRuleCall_4_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getZoneAccess().getAlarmsAlarmEStringParserRuleCall_4_3_1_0_1()); 

            }

             after(grammarAccess.getZoneAccess().getAlarmsAlarmCrossReference_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zone__AlarmsAssignment_4_3_1"


    // $ANTLR start "rule__Alarm__NameAssignment_2"
    // InternalBuilding.g:4274:1: rule__Alarm__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Alarm__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4278:1: ( ( ruleEString ) )
            // InternalBuilding.g:4279:2: ( ruleEString )
            {
            // InternalBuilding.g:4279:2: ( ruleEString )
            // InternalBuilding.g:4280:3: ruleEString
            {
             before(grammarAccess.getAlarmAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAlarmAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__NameAssignment_2"


    // $ANTLR start "rule__Alarm__LocationAssignment_4_1"
    // InternalBuilding.g:4289:1: rule__Alarm__LocationAssignment_4_1 : ( ( ruleEString ) ) ;
    public final void rule__Alarm__LocationAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4293:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4294:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4294:2: ( ( ruleEString ) )
            // InternalBuilding.g:4295:3: ( ruleEString )
            {
             before(grammarAccess.getAlarmAccess().getLocationZoneCrossReference_4_1_0()); 
            // InternalBuilding.g:4296:3: ( ruleEString )
            // InternalBuilding.g:4297:4: ruleEString
            {
             before(grammarAccess.getAlarmAccess().getLocationZoneEStringParserRuleCall_4_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAlarmAccess().getLocationZoneEStringParserRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getAlarmAccess().getLocationZoneCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alarm__LocationAssignment_4_1"


    // $ANTLR start "rule__Virtual__NameAssignment_2"
    // InternalBuilding.g:4308:1: rule__Virtual__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Virtual__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4312:1: ( ( ruleEString ) )
            // InternalBuilding.g:4313:2: ( ruleEString )
            {
            // InternalBuilding.g:4313:2: ( ruleEString )
            // InternalBuilding.g:4314:3: ruleEString
            {
             before(grammarAccess.getVirtualAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getVirtualAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__NameAssignment_2"


    // $ANTLR start "rule__Virtual__Zone1Assignment_5"
    // InternalBuilding.g:4323:1: rule__Virtual__Zone1Assignment_5 : ( ( ruleEString ) ) ;
    public final void rule__Virtual__Zone1Assignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4327:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4328:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4328:2: ( ( ruleEString ) )
            // InternalBuilding.g:4329:3: ( ruleEString )
            {
             before(grammarAccess.getVirtualAccess().getZone1ZoneCrossReference_5_0()); 
            // InternalBuilding.g:4330:3: ( ruleEString )
            // InternalBuilding.g:4331:4: ruleEString
            {
             before(grammarAccess.getVirtualAccess().getZone1ZoneEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getVirtualAccess().getZone1ZoneEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getVirtualAccess().getZone1ZoneCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Zone1Assignment_5"


    // $ANTLR start "rule__Virtual__Zone2Assignment_7"
    // InternalBuilding.g:4342:1: rule__Virtual__Zone2Assignment_7 : ( ( ruleEString ) ) ;
    public final void rule__Virtual__Zone2Assignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4346:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4347:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4347:2: ( ( ruleEString ) )
            // InternalBuilding.g:4348:3: ( ruleEString )
            {
             before(grammarAccess.getVirtualAccess().getZone2ZoneCrossReference_7_0()); 
            // InternalBuilding.g:4349:3: ( ruleEString )
            // InternalBuilding.g:4350:4: ruleEString
            {
             before(grammarAccess.getVirtualAccess().getZone2ZoneEStringParserRuleCall_7_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getVirtualAccess().getZone2ZoneEStringParserRuleCall_7_0_1()); 

            }

             after(grammarAccess.getVirtualAccess().getZone2ZoneCrossReference_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Virtual__Zone2Assignment_7"


    // $ANTLR start "rule__Door__NameAssignment_1"
    // InternalBuilding.g:4361:1: rule__Door__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Door__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4365:1: ( ( ruleEString ) )
            // InternalBuilding.g:4366:2: ( ruleEString )
            {
            // InternalBuilding.g:4366:2: ( ruleEString )
            // InternalBuilding.g:4367:3: ruleEString
            {
             before(grammarAccess.getDoorAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDoorAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__NameAssignment_1"


    // $ANTLR start "rule__Door__LevelAssignment_3_1"
    // InternalBuilding.g:4376:1: rule__Door__LevelAssignment_3_1 : ( ruleEInt ) ;
    public final void rule__Door__LevelAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4380:1: ( ( ruleEInt ) )
            // InternalBuilding.g:4381:2: ( ruleEInt )
            {
            // InternalBuilding.g:4381:2: ( ruleEInt )
            // InternalBuilding.g:4382:3: ruleEInt
            {
             before(grammarAccess.getDoorAccess().getLevelEIntParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getDoorAccess().getLevelEIntParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__LevelAssignment_3_1"


    // $ANTLR start "rule__Door__Zone1Assignment_4_1"
    // InternalBuilding.g:4391:1: rule__Door__Zone1Assignment_4_1 : ( ( ruleEString ) ) ;
    public final void rule__Door__Zone1Assignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4395:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4396:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4396:2: ( ( ruleEString ) )
            // InternalBuilding.g:4397:3: ( ruleEString )
            {
             before(grammarAccess.getDoorAccess().getZone1ZoneCrossReference_4_1_0()); 
            // InternalBuilding.g:4398:3: ( ruleEString )
            // InternalBuilding.g:4399:4: ruleEString
            {
             before(grammarAccess.getDoorAccess().getZone1ZoneEStringParserRuleCall_4_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDoorAccess().getZone1ZoneEStringParserRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getDoorAccess().getZone1ZoneCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Zone1Assignment_4_1"


    // $ANTLR start "rule__Door__Zone2Assignment_5_1"
    // InternalBuilding.g:4410:1: rule__Door__Zone2Assignment_5_1 : ( ( ruleEString ) ) ;
    public final void rule__Door__Zone2Assignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4414:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4415:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4415:2: ( ( ruleEString ) )
            // InternalBuilding.g:4416:3: ( ruleEString )
            {
             before(grammarAccess.getDoorAccess().getZone2ZoneCrossReference_5_1_0()); 
            // InternalBuilding.g:4417:3: ( ruleEString )
            // InternalBuilding.g:4418:4: ruleEString
            {
             before(grammarAccess.getDoorAccess().getZone2ZoneEStringParserRuleCall_5_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDoorAccess().getZone2ZoneEStringParserRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getDoorAccess().getZone2ZoneCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__Zone2Assignment_5_1"


    // $ANTLR start "rule__Door__KeysAssignment_6_2"
    // InternalBuilding.g:4429:1: rule__Door__KeysAssignment_6_2 : ( ( ruleEString ) ) ;
    public final void rule__Door__KeysAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4433:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4434:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4434:2: ( ( ruleEString ) )
            // InternalBuilding.g:4435:3: ( ruleEString )
            {
             before(grammarAccess.getDoorAccess().getKeysItemCrossReference_6_2_0()); 
            // InternalBuilding.g:4436:3: ( ruleEString )
            // InternalBuilding.g:4437:4: ruleEString
            {
             before(grammarAccess.getDoorAccess().getKeysItemEStringParserRuleCall_6_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDoorAccess().getKeysItemEStringParserRuleCall_6_2_0_1()); 

            }

             after(grammarAccess.getDoorAccess().getKeysItemCrossReference_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__KeysAssignment_6_2"


    // $ANTLR start "rule__Door__KeysAssignment_6_3_1"
    // InternalBuilding.g:4448:1: rule__Door__KeysAssignment_6_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Door__KeysAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4452:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4453:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4453:2: ( ( ruleEString ) )
            // InternalBuilding.g:4454:3: ( ruleEString )
            {
             before(grammarAccess.getDoorAccess().getKeysItemCrossReference_6_3_1_0()); 
            // InternalBuilding.g:4455:3: ( ruleEString )
            // InternalBuilding.g:4456:4: ruleEString
            {
             before(grammarAccess.getDoorAccess().getKeysItemEStringParserRuleCall_6_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDoorAccess().getKeysItemEStringParserRuleCall_6_3_1_0_1()); 

            }

             after(grammarAccess.getDoorAccess().getKeysItemCrossReference_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__KeysAssignment_6_3_1"


    // $ANTLR start "rule__Door__AlarmsAssignment_7_2"
    // InternalBuilding.g:4467:1: rule__Door__AlarmsAssignment_7_2 : ( ( ruleEString ) ) ;
    public final void rule__Door__AlarmsAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4471:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4472:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4472:2: ( ( ruleEString ) )
            // InternalBuilding.g:4473:3: ( ruleEString )
            {
             before(grammarAccess.getDoorAccess().getAlarmsAlarmCrossReference_7_2_0()); 
            // InternalBuilding.g:4474:3: ( ruleEString )
            // InternalBuilding.g:4475:4: ruleEString
            {
             before(grammarAccess.getDoorAccess().getAlarmsAlarmEStringParserRuleCall_7_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDoorAccess().getAlarmsAlarmEStringParserRuleCall_7_2_0_1()); 

            }

             after(grammarAccess.getDoorAccess().getAlarmsAlarmCrossReference_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__AlarmsAssignment_7_2"


    // $ANTLR start "rule__Door__AlarmsAssignment_7_3_1"
    // InternalBuilding.g:4486:1: rule__Door__AlarmsAssignment_7_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Door__AlarmsAssignment_7_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4490:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4491:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4491:2: ( ( ruleEString ) )
            // InternalBuilding.g:4492:3: ( ruleEString )
            {
             before(grammarAccess.getDoorAccess().getAlarmsAlarmCrossReference_7_3_1_0()); 
            // InternalBuilding.g:4493:3: ( ruleEString )
            // InternalBuilding.g:4494:4: ruleEString
            {
             before(grammarAccess.getDoorAccess().getAlarmsAlarmEStringParserRuleCall_7_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDoorAccess().getAlarmsAlarmEStringParserRuleCall_7_3_1_0_1()); 

            }

             after(grammarAccess.getDoorAccess().getAlarmsAlarmCrossReference_7_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Door__AlarmsAssignment_7_3_1"


    // $ANTLR start "rule__Window__NameAssignment_1"
    // InternalBuilding.g:4505:1: rule__Window__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Window__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4509:1: ( ( ruleEString ) )
            // InternalBuilding.g:4510:2: ( ruleEString )
            {
            // InternalBuilding.g:4510:2: ( ruleEString )
            // InternalBuilding.g:4511:3: ruleEString
            {
             before(grammarAccess.getWindowAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getWindowAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__NameAssignment_1"


    // $ANTLR start "rule__Window__LevelAssignment_3_1"
    // InternalBuilding.g:4520:1: rule__Window__LevelAssignment_3_1 : ( ruleEInt ) ;
    public final void rule__Window__LevelAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4524:1: ( ( ruleEInt ) )
            // InternalBuilding.g:4525:2: ( ruleEInt )
            {
            // InternalBuilding.g:4525:2: ( ruleEInt )
            // InternalBuilding.g:4526:3: ruleEInt
            {
             before(grammarAccess.getWindowAccess().getLevelEIntParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getWindowAccess().getLevelEIntParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__LevelAssignment_3_1"


    // $ANTLR start "rule__Window__InsideAssignment_4_1"
    // InternalBuilding.g:4535:1: rule__Window__InsideAssignment_4_1 : ( ( ruleEString ) ) ;
    public final void rule__Window__InsideAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4539:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4540:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4540:2: ( ( ruleEString ) )
            // InternalBuilding.g:4541:3: ( ruleEString )
            {
             before(grammarAccess.getWindowAccess().getInsideZoneCrossReference_4_1_0()); 
            // InternalBuilding.g:4542:3: ( ruleEString )
            // InternalBuilding.g:4543:4: ruleEString
            {
             before(grammarAccess.getWindowAccess().getInsideZoneEStringParserRuleCall_4_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getWindowAccess().getInsideZoneEStringParserRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getWindowAccess().getInsideZoneCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__InsideAssignment_4_1"


    // $ANTLR start "rule__Window__OutsideAssignment_5_1"
    // InternalBuilding.g:4554:1: rule__Window__OutsideAssignment_5_1 : ( ( ruleEString ) ) ;
    public final void rule__Window__OutsideAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4558:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4559:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4559:2: ( ( ruleEString ) )
            // InternalBuilding.g:4560:3: ( ruleEString )
            {
             before(grammarAccess.getWindowAccess().getOutsideZoneCrossReference_5_1_0()); 
            // InternalBuilding.g:4561:3: ( ruleEString )
            // InternalBuilding.g:4562:4: ruleEString
            {
             before(grammarAccess.getWindowAccess().getOutsideZoneEStringParserRuleCall_5_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getWindowAccess().getOutsideZoneEStringParserRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getWindowAccess().getOutsideZoneCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__OutsideAssignment_5_1"


    // $ANTLR start "rule__Window__AlarmsAssignment_6_2"
    // InternalBuilding.g:4573:1: rule__Window__AlarmsAssignment_6_2 : ( ( ruleEString ) ) ;
    public final void rule__Window__AlarmsAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4577:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4578:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4578:2: ( ( ruleEString ) )
            // InternalBuilding.g:4579:3: ( ruleEString )
            {
             before(grammarAccess.getWindowAccess().getAlarmsAlarmCrossReference_6_2_0()); 
            // InternalBuilding.g:4580:3: ( ruleEString )
            // InternalBuilding.g:4581:4: ruleEString
            {
             before(grammarAccess.getWindowAccess().getAlarmsAlarmEStringParserRuleCall_6_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getWindowAccess().getAlarmsAlarmEStringParserRuleCall_6_2_0_1()); 

            }

             after(grammarAccess.getWindowAccess().getAlarmsAlarmCrossReference_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__AlarmsAssignment_6_2"


    // $ANTLR start "rule__Window__AlarmsAssignment_6_3_1"
    // InternalBuilding.g:4592:1: rule__Window__AlarmsAssignment_6_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Window__AlarmsAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4596:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4597:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4597:2: ( ( ruleEString ) )
            // InternalBuilding.g:4598:3: ( ruleEString )
            {
             before(grammarAccess.getWindowAccess().getAlarmsAlarmCrossReference_6_3_1_0()); 
            // InternalBuilding.g:4599:3: ( ruleEString )
            // InternalBuilding.g:4600:4: ruleEString
            {
             before(grammarAccess.getWindowAccess().getAlarmsAlarmEStringParserRuleCall_6_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getWindowAccess().getAlarmsAlarmEStringParserRuleCall_6_3_1_0_1()); 

            }

             after(grammarAccess.getWindowAccess().getAlarmsAlarmCrossReference_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Window__AlarmsAssignment_6_3_1"


    // $ANTLR start "rule__Badged__NameAssignment_1"
    // InternalBuilding.g:4611:1: rule__Badged__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Badged__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4615:1: ( ( ruleEString ) )
            // InternalBuilding.g:4616:2: ( ruleEString )
            {
            // InternalBuilding.g:4616:2: ( ruleEString )
            // InternalBuilding.g:4617:3: ruleEString
            {
             before(grammarAccess.getBadgedAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBadgedAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__NameAssignment_1"


    // $ANTLR start "rule__Badged__LevelAssignment_3_1"
    // InternalBuilding.g:4626:1: rule__Badged__LevelAssignment_3_1 : ( ruleEInt ) ;
    public final void rule__Badged__LevelAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4630:1: ( ( ruleEInt ) )
            // InternalBuilding.g:4631:2: ( ruleEInt )
            {
            // InternalBuilding.g:4631:2: ( ruleEInt )
            // InternalBuilding.g:4632:3: ruleEInt
            {
             before(grammarAccess.getBadgedAccess().getLevelEIntParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getBadgedAccess().getLevelEIntParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__LevelAssignment_3_1"


    // $ANTLR start "rule__Badged__InsideAssignment_4_1"
    // InternalBuilding.g:4641:1: rule__Badged__InsideAssignment_4_1 : ( ( ruleEString ) ) ;
    public final void rule__Badged__InsideAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4645:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4646:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4646:2: ( ( ruleEString ) )
            // InternalBuilding.g:4647:3: ( ruleEString )
            {
             before(grammarAccess.getBadgedAccess().getInsideZoneCrossReference_4_1_0()); 
            // InternalBuilding.g:4648:3: ( ruleEString )
            // InternalBuilding.g:4649:4: ruleEString
            {
             before(grammarAccess.getBadgedAccess().getInsideZoneEStringParserRuleCall_4_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBadgedAccess().getInsideZoneEStringParserRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getBadgedAccess().getInsideZoneCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__InsideAssignment_4_1"


    // $ANTLR start "rule__Badged__OutsideAssignment_5_1"
    // InternalBuilding.g:4660:1: rule__Badged__OutsideAssignment_5_1 : ( ( ruleEString ) ) ;
    public final void rule__Badged__OutsideAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4664:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4665:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4665:2: ( ( ruleEString ) )
            // InternalBuilding.g:4666:3: ( ruleEString )
            {
             before(grammarAccess.getBadgedAccess().getOutsideZoneCrossReference_5_1_0()); 
            // InternalBuilding.g:4667:3: ( ruleEString )
            // InternalBuilding.g:4668:4: ruleEString
            {
             before(grammarAccess.getBadgedAccess().getOutsideZoneEStringParserRuleCall_5_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBadgedAccess().getOutsideZoneEStringParserRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getBadgedAccess().getOutsideZoneCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__OutsideAssignment_5_1"


    // $ANTLR start "rule__Badged__BadgesAssignment_6_2"
    // InternalBuilding.g:4679:1: rule__Badged__BadgesAssignment_6_2 : ( ( ruleEString ) ) ;
    public final void rule__Badged__BadgesAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4683:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4684:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4684:2: ( ( ruleEString ) )
            // InternalBuilding.g:4685:3: ( ruleEString )
            {
             before(grammarAccess.getBadgedAccess().getBadgesItemCrossReference_6_2_0()); 
            // InternalBuilding.g:4686:3: ( ruleEString )
            // InternalBuilding.g:4687:4: ruleEString
            {
             before(grammarAccess.getBadgedAccess().getBadgesItemEStringParserRuleCall_6_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBadgedAccess().getBadgesItemEStringParserRuleCall_6_2_0_1()); 

            }

             after(grammarAccess.getBadgedAccess().getBadgesItemCrossReference_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__BadgesAssignment_6_2"


    // $ANTLR start "rule__Badged__BadgesAssignment_6_3_1"
    // InternalBuilding.g:4698:1: rule__Badged__BadgesAssignment_6_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Badged__BadgesAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4702:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4703:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4703:2: ( ( ruleEString ) )
            // InternalBuilding.g:4704:3: ( ruleEString )
            {
             before(grammarAccess.getBadgedAccess().getBadgesItemCrossReference_6_3_1_0()); 
            // InternalBuilding.g:4705:3: ( ruleEString )
            // InternalBuilding.g:4706:4: ruleEString
            {
             before(grammarAccess.getBadgedAccess().getBadgesItemEStringParserRuleCall_6_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBadgedAccess().getBadgesItemEStringParserRuleCall_6_3_1_0_1()); 

            }

             after(grammarAccess.getBadgedAccess().getBadgesItemCrossReference_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__BadgesAssignment_6_3_1"


    // $ANTLR start "rule__Badged__AlarmsAssignment_7_2"
    // InternalBuilding.g:4717:1: rule__Badged__AlarmsAssignment_7_2 : ( ( ruleEString ) ) ;
    public final void rule__Badged__AlarmsAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4721:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4722:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4722:2: ( ( ruleEString ) )
            // InternalBuilding.g:4723:3: ( ruleEString )
            {
             before(grammarAccess.getBadgedAccess().getAlarmsAlarmCrossReference_7_2_0()); 
            // InternalBuilding.g:4724:3: ( ruleEString )
            // InternalBuilding.g:4725:4: ruleEString
            {
             before(grammarAccess.getBadgedAccess().getAlarmsAlarmEStringParserRuleCall_7_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBadgedAccess().getAlarmsAlarmEStringParserRuleCall_7_2_0_1()); 

            }

             after(grammarAccess.getBadgedAccess().getAlarmsAlarmCrossReference_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__AlarmsAssignment_7_2"


    // $ANTLR start "rule__Badged__AlarmsAssignment_7_3_1"
    // InternalBuilding.g:4736:1: rule__Badged__AlarmsAssignment_7_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Badged__AlarmsAssignment_7_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4740:1: ( ( ( ruleEString ) ) )
            // InternalBuilding.g:4741:2: ( ( ruleEString ) )
            {
            // InternalBuilding.g:4741:2: ( ( ruleEString ) )
            // InternalBuilding.g:4742:3: ( ruleEString )
            {
             before(grammarAccess.getBadgedAccess().getAlarmsAlarmCrossReference_7_3_1_0()); 
            // InternalBuilding.g:4743:3: ( ruleEString )
            // InternalBuilding.g:4744:4: ruleEString
            {
             before(grammarAccess.getBadgedAccess().getAlarmsAlarmEStringParserRuleCall_7_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBadgedAccess().getAlarmsAlarmEStringParserRuleCall_7_3_1_0_1()); 

            }

             after(grammarAccess.getBadgedAccess().getAlarmsAlarmCrossReference_7_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badged__AlarmsAssignment_7_3_1"


    // $ANTLR start "rule__Item__NameAssignment_2"
    // InternalBuilding.g:4755:1: rule__Item__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Item__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4759:1: ( ( ruleEString ) )
            // InternalBuilding.g:4760:2: ( ruleEString )
            {
            // InternalBuilding.g:4760:2: ( ruleEString )
            // InternalBuilding.g:4761:3: ruleEString
            {
             before(grammarAccess.getItemAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getItemAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__NameAssignment_2"


    // $ANTLR start "rule__Attacker__NameAssignment_2"
    // InternalBuilding.g:4770:1: rule__Attacker__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Attacker__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4774:1: ( ( ruleEString ) )
            // InternalBuilding.g:4775:2: ( ruleEString )
            {
            // InternalBuilding.g:4775:2: ( ruleEString )
            // InternalBuilding.g:4776:3: ruleEString
            {
             before(grammarAccess.getAttackerAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAttackerAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__NameAssignment_2"


    // $ANTLR start "rule__Attacker__LevelAssignment_4_1"
    // InternalBuilding.g:4785:1: rule__Attacker__LevelAssignment_4_1 : ( ruleEInt ) ;
    public final void rule__Attacker__LevelAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalBuilding.g:4789:1: ( ( ruleEInt ) )
            // InternalBuilding.g:4790:2: ( ruleEInt )
            {
            // InternalBuilding.g:4790:2: ( ruleEInt )
            // InternalBuilding.g:4791:3: ruleEInt
            {
             before(grammarAccess.getAttackerAccess().getLevelEIntParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getAttackerAccess().getLevelEIntParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__LevelAssignment_4_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000D2450C000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000D24508002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000D24508000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000014000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000204000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x000000001B014000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000001000000040L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x00000000C8014000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x00000002C8014000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000008004000L});

}