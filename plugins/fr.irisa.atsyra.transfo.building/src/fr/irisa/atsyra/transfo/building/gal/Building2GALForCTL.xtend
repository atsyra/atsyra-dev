
package fr.irisa.atsyra.transfo.building.gal

import fr.irisa.atsyra.building.BuildingModel
import fr.lip6.move.gal.GALTypeDeclaration
import fr.lip6.move.gal.Label
import fr.lip6.move.gal.Transition
import fr.lip6.move.gal.TypedefDeclaration
import fr.lip6.move.gal.Variable

import static extension fr.irisa.atsyra.transfo.building.gal.BuildingModelAspects.*
import fr.irisa.atsyra.transfo.building.gal.ZoneAspects
import fr.irisa.atsyra.transfo.building.gal.AttackerAspects
import fr.irisa.atsyra.transfo.building.gal.Context
import fr.irisa.atsyra.transfo.building.gal.GALBuildHelper
import fr.irisa.atsyra.transfo.building.gal.GalNamer

class Building2GALForCTL extends AbstractBuilding2GAL {
	def GALTypeDeclaration transformToGAL(BuildingModel buildingModel){
		//context initialization
		val Context context = new Context()
		context.gal.name = "Building"
		//initialization of the constants
		ZoneAspects.initNumbers(buildingModel.buildings.get(0).zones.get(0), buildingModel) // the method is static, not using the Zone parameter. But the static keyword is not enough
		AttackerAspects.initNumbers(buildingModel.buildings.get(0).attackers.get(0), buildingModel) // the method is static, not using the Attacker parameter. But the static keyword is not enough
		// creating the init variable, to use in the building guards
		init = GALBuildHelper.createVariable("init",0)
		context.gal.variables.add(init)
		context.init = init;
		//creation of the GAL part from Building
		buildingModel.createGalElement(context)
		buildingModel.linkGalElement(context)
		//creation of the GAL part for the initial state
		updateGalWithInit(context)		
		return context.gal
	}
	
	Variable init
	TypedefDeclaration Bools
	TypedefDeclaration Attackers
	TypedefDeclaration Zones
	Transition initVars
	
	def void updateGalWithInit(Context context) {
		Bools = GALBuildHelper.createTypeDefDeclaration("bools",0,1)
		context.gal.typedefs.add(Bools)
		Attackers = GALBuildHelper.createTypeDefDeclaration("attackers",1,AttackerAspects.numberOfAttackers)
		context.gal.typedefs.add(Attackers)
		Zones = GALBuildHelper.createTypeDefDeclaration("zones",1,ZoneAspects.numberOfZones)
		context.gal.typedefs.add(Zones)
		initVars = GALBuildHelper.createTransition("assign_initial_values")
		context.gal.transitions.add(initVars)
		initVars.guard = GALBuildHelper.createBoolExprEqVarCst(init,0)
		context.AttackerLocation.forEach[att, variable| 
			val param = GALBuildHelper.createParam(GalNamer.getParamName(variable.name), Zones)
			initVars.params.add(param)
			initVars.actions.add(GALBuildHelper.createVarAssignParam(variable,param))
		]
		context.AlarmEnabled.forEach[alarm, variable| 
			val param = GALBuildHelper.createParam(GalNamer.getParamName(variable.name), Bools)
			initVars.params.add(param)
			initVars.actions.add(GALBuildHelper.createVarAssignParam(variable,param))
		]
		context.AlarmTriggered.forEach[alarm, variable| 
			val param = GALBuildHelper.createParam(GalNamer.getParamName(variable.name), Bools)
			initVars.params.add(param)
			initVars.actions.add(GALBuildHelper.createVarAssignParam(variable,param))
		]
		/* for each item, the two variables from ItemLocalization and ItemOwnedBy  are not independent. 
		 * we need to help GAL to handle their common initialization */ 
		context.ItemLocation.forEach[item, variableLoc| 
			// First, get the owner variable corresponding to the same item
			val variableOwner = context.ItemOwnedBy.get(item)
			// Create a parameter for choosing whether the item should be initially owned by an attacker, or located in a zone
			val paramChoice = GALBuildHelper.createParam("param_" + item + "_is_owned" , Bools)
			initVars.params.add(paramChoice)
			val choice = GALBuildHelper.createBoolExprEqPrmCst(paramChoice,GALBuildHelper.galFalse)
			// Create the transition initializing a location for the item. labLoc is the label of this transition.
			val labLoc = createTransitionLocForItem(context,item,variableLoc,variableOwner)
			// Create the transition initializing a location for the item. labLoc is the label of this transition.
			val labOwn = createTransitionOwnForItem(context,item,variableOwner,variableLoc)
			// Create the call instructions
			val callLoc = GALBuildHelper.createCall(labLoc)
			val callOwner = GALBuildHelper.createCall(labOwn)
			initVars.actions.add(GALBuildHelper.createIfThenElse(choice,callLoc,callOwner))
		]
		context.AccessOpen.forEach[acc, variable| 
			val param = GALBuildHelper.createParam(GalNamer.getParamName(variable.name), Bools)
			initVars.params.add(param)
			initVars.actions.add(GALBuildHelper.createVarAssignParam(variable,param))
		]
		context.AccessLocked.forEach[acc, variable| 
			val param = GALBuildHelper.createParam(GalNamer.getParamName(variable.name), Bools)
			initVars.params.add(param)
			initVars.actions.add(GALBuildHelper.createVarAssignParam(variable,param))
		]
		initVars.actions.add(GALBuildHelper.createVarAssignConst(init,GALBuildHelper.galTrue))
	}
	
	def private Label createTransitionLocForItem(Context context, String itemName, Variable variableLoc, Variable variableOwn) {
		val transitionLoc = GALBuildHelper.createTransition("assign_initial_value_of_" + itemName + "_location")
		context.gal.transitions.add(transitionLoc)
		val paramLoc = GALBuildHelper.createParam(GalNamer.getParamName(variableLoc.name), Zones)
		transitionLoc.params.add(paramLoc)
		transitionLoc.guard = GALBuildHelper.boolTrue()
		val label = GALBuildHelper.createLabel("assign_initial_value_of_" + itemName + "_location")
		transitionLoc.label = label
		val chooseLoc = GALBuildHelper.createVarAssignParam(variableLoc,paramLoc)
		transitionLoc.actions.add(chooseLoc)
		val ownerIsNone = GALBuildHelper.createVarAssignConst(variableOwn,0)
		transitionLoc.actions.add(ownerIsNone)
		return label
	}
	
	def private Label createTransitionOwnForItem(Context context, String itemName, Variable variableOwn, Variable variableLoc) {
		val transitionOwn = GALBuildHelper.createTransition("assign_initial_value_of_" + itemName + "_owner")
		context.gal.transitions.add(transitionOwn)
		val paramOwner = GALBuildHelper.createParam(GalNamer.getParamName(variableOwn.name), Attackers)
		transitionOwn.params.add(paramOwner)
		transitionOwn.guard = GALBuildHelper.boolTrue()
		val label = GALBuildHelper.createLabel("assign_initial_value_of_" + itemName + "_owner")
		transitionOwn.label = label
		val chooseOwner = GALBuildHelper.createVarAssignParam(variableOwn,paramOwner)
		transitionOwn.actions.add(chooseOwner)
		val locIsNone = GALBuildHelper.createVarAssignConst(variableLoc,0)
		transitionOwn.actions.add(locIsNone)
		return label 
	}
	
}