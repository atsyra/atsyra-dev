package fr.irisa.atsyra.transfo.building.gal

import atsyragoal.AndCondition
import atsyragoal.AtsyraGoal
import atsyragoal.AtsyraGoalModel
import atsyragoal.BooleanLiteral
import atsyragoal.BooleanSystemCondition
import atsyragoal.DefaultAssignment
import atsyragoal.EqualsCondition
import atsyragoal.GoalCondition
import atsyragoal.NotCondition
import atsyragoal.OrCondition
import atsyragoal.SystemConstFeature
import atsyragoal.SystemFeature
import atsyragoal.TypedElement
import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.lip6.move.gal.BooleanExpression
import fr.lip6.move.gal.Constant
import fr.lip6.move.gal.Variable
import java.util.List
import java.util.Optional
import java.util.function.BiConsumer

import static fr.irisa.atsyra.transfo.building.gal.AttackerAspects.*
import static fr.irisa.atsyra.transfo.building.gal.ZoneAspects.*

import static extension fr.irisa.atsyra.transfo.building.gal.DefaultAssignmentAspect.*
import static extension fr.irisa.atsyra.transfo.building.gal.GoalConditionAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.TypedElementAspects.*

@Aspect (className=GoalCondition)
abstract class GoalConditionAspects {
	def public BooleanExpression getTestStateExpression(Context context)
		
		
	/** 
	 * returns the list of default assignment that remain applicable for the given condition
	 * typically, it removes any default assignment that has been involved in one of the condition operand
	 */
	def public List<DefaultAssignment> getApplicableAssignements(List<DefaultAssignment> defaultValueAssignments)
	
	def public void forEachAssignment(BiConsumer<Variable, Constant> doConsume, Context context)
}

@Aspect (className=AndCondition)
class AndConditionAspects extends GoalConditionAspects {
	def public BooleanExpression getTestStateExpression(Context context){
		val seed = _self.operands.head.getTestStateExpression(context)
		return _self.operands.tail.fold(seed,[r,t | GALBuildHelper.createBoolExprAnd(r,t.getTestStateExpression(context))]) 
	}

	def public List<DefaultAssignment> getApplicableAssignements(List<DefaultAssignment> defaultValueAssignments){
		val List<DefaultAssignment> remainingDefaultValueAssignments = newArrayList(defaultValueAssignments)
		_self.operands.forEach[op|
			val t = op.getApplicableAssignements(remainingDefaultValueAssignments) 
			remainingDefaultValueAssignments.clear 
			remainingDefaultValueAssignments.addAll(t)
		]
		return remainingDefaultValueAssignments
	}
	
	def public void forEachAssignment(BiConsumer<Variable, Constant> doConsume, Context context) {
		_self.operands.forEach[op|
			op.forEachAssignment(doConsume,context)
		]
	}
}

@Aspect (className=OrCondition)
class OrConditionAspects extends GoalConditionAspects {
	def public BooleanExpression getTestStateExpression(Context context){
		val seed = _self.operands.head.getTestStateExpression(context)
		return _self.operands.tail.fold(seed,[r,t | GALBuildHelper.createBoolExprOr(r,t.getTestStateExpression(context))]) 
	}
	
	def public List<DefaultAssignment> getApplicableAssignements(List<DefaultAssignment> defaultValueAssignments){
		val List<DefaultAssignment> remainingDefaultValueAssignments = newArrayList(defaultValueAssignments)
		_self.operands.forEach[op|
			val t = op.getApplicableAssignements(remainingDefaultValueAssignments) 
			remainingDefaultValueAssignments.clear 
			remainingDefaultValueAssignments.addAll(t)
		]
		return remainingDefaultValueAssignments
	}
	def public void forEachAssignment(BiConsumer<Variable, Constant> doConsume, Context context) {
		// ingnore Or
	}
}

@Aspect (className=NotCondition)
class NotConditionAspects extends GoalConditionAspects {
	def public BooleanExpression getTestStateExpression(Context context){
		return GALBuildHelper.createBoolExprNot(_self.operands.head.getTestStateExpression(context)) 
	}

	def public List<DefaultAssignment> getApplicableAssignements(List<DefaultAssignment> defaultValueAssignments){
		_self.operands.head.getApplicableAssignements(defaultValueAssignments)
	}
	
	def public void forEachAssignment(BiConsumer<Variable, Constant> doConsume, Context context) {
		// ignore Not
	}
}

@Aspect (className=EqualsCondition)
class EqualsConditionAspects extends GoalConditionAspects {
	def public BooleanExpression getTestStateExpression(Context context){
		if(_self.source.isaConstant() && _self.target.isaConstant() ) {
			return GALBuildHelper.createBoolExprEqCstCst(_self.source.getConstant(context), _self.target.getConstant(context))
		}
		else if(_self.source.isaConstant() && !_self.target.isaConstant() ) {
			return GALBuildHelper.createBoolExprEqVarCst(_self.target.getVariable(context), _self.source.getConstant(context))
		}
		else if(!_self.source.isaConstant() && _self.target.isaConstant() ) {
			return GALBuildHelper.createBoolExprEqVarCst(_self.source.getVariable(context), _self.target.getConstant(context))
		}
		else {
			return GALBuildHelper.createBoolExprEqVarVar(_self.source.getVariable(context), _self.target.getVariable(context))
		}
	}
	
	def public List<DefaultAssignment> getApplicableAssignements(List<DefaultAssignment> defaultValueAssignments){
		// removes defaults whose target is involved in the equals condition
		defaultValueAssignments.filter[assignment|
			!(_self.source == assignment.target || _self.target == assignment.target)
		].toList
	}
	
	def public void forEachAssignment(BiConsumer<Variable, Constant> doConsume, Context context) {
		if(_self.source.isaConstant() && !_self.target.isaConstant() ) {
			doConsume.accept(_self.target.getVariable(context),GALBuildHelper.createConstant(_self.source.getConstant(context)))
		}
		else if(!_self.source.isaConstant() && _self.target.isaConstant() ) {
			doConsume.accept(_self.source.getVariable(context),GALBuildHelper.createConstant(_self.target.getConstant(context)))
		}
	}
}

@Aspect (className=BooleanSystemCondition)
class BooleanSystemConditionAspects extends GoalConditionAspects {
	def public BooleanExpression getTestStateExpression(Context context){
		if(_self.source.isaConstant()) {
			return GALBuildHelper.createBoolExprEqCstCst(_self.source.getConstant(context), GALBuildHelper.galTrue)
		}
		else {
			return GALBuildHelper.createBoolExprEqVarCst(_self.source.getVariable(context), GALBuildHelper.galTrue)
		}
	}
	
	/** returns only applicable DefaultAssignment */
	def public List<DefaultAssignment> getApplicableAssignements(List<DefaultAssignment> defaultValueAssignments){
		// removes defaults whose target is involved in the boolean condition
		defaultValueAssignments.filter[assignment|
			!(/*_self.source instanceof SystemFeature && */_self.source == assignment.target)
		].toList
	}
	
	def public void forEachAssignment(BiConsumer<Variable, Constant> doConsume, Context context) {
		if(!_self.source.isaConstant()) {
			doConsume.accept(_self.source.getVariable(context),GALBuildHelper.createConstant(GALBuildHelper.galTrue))
		}
	}
}

@Aspect (className=TypedElement)
class TypedElementAspects {
	def public boolean isaConstant(){
		return _self instanceof SystemConstFeature || _self instanceof BooleanLiteral
	}
	
	def public Variable getVariable(Context context){
		switch _self {
			SystemFeature: _self.getSystVariable(context)
			default: throw new Exception("AtsyraGoal TypedElement getVariable : Not defined")
			}
	}
	
	def public Variable getSystVariable(Context context){
		val object = _self.name.split("\\.",2).get(0)
		val feature_name = _self.name.split("\\.",2).get(1)
		switch feature_name  {
			case BuildingFeature.ENABLED:
			return context.AlarmEnabled.get(object)
			case BuildingFeature.TRIGGERED:
			return context.AlarmTriggered.get(object)
			case BuildingFeature.OPEN:
			return context.AccessOpen.get(object)
			case BuildingFeature.LOCKED:
			return context.AccessLocked.get(object)
			case BuildingFeature.LOCATION:
			switch _self.getObjectType(){
				case "Attacker": 
				return context.AttackerLocation.get(object)
				case "Item":
				return context.ItemLocation.get(object)
				default:
				throw new Exception("AtsyraGoal TypedElement getSystVariable : Undefined object type \"" + _self.getObjectType() + "\" for feature \"" + feature_name +"\"")
			}
			case BuildingFeature.LEVEL:
			return context.AttackerLevel.get(object)
			case BuildingFeature.OWNER:
			return context.ItemOwnedBy.get(object)
			default:
			throw new Exception("AtsyraGoal TypedElement getSystVariable : Undefined Feature \"" + feature_name +"\"")
		}
	}
	
	def public String getObjectType() {
		val object = _self.name.split("\\.",2).get(0)
		val goalmodel = _self.eContainer as AtsyraGoalModel
		val decl = goalmodel.typedElements.findFirst[typedElem | typedElem.name == object]
		return decl.type.name
	}
	
	def public int getConstant(Context context){
		switch _self {
			BooleanLiteral: _self.getBoolConstant(context)
			SystemConstFeature: _self.getSystConstant(context)
			default: throw new Exception("AtsyraGoal TypedElement getConstant : Not defined for "+_self)
		}
	}
	
	def public int getBoolConstant(Context context){
		switch _self.name {
			case "true", case "^true": return GALBuildHelper.galTrue
			case "false", case "^false": return GALBuildHelper.galFalse
			default: throw new Exception("AtsyraGoal TypedElement getBoolConstant : Not defined for "+_self)
		}
	}
	
	def public int getSystConstant(Context context){
		if (zoneGalValues.containsKey(_self.name)){
			return zoneGalValues.get(_self.name)
		}
		if (attackerGalValues.containsKey(_self.name)){
			return attackerGalValues.get(_self.name)
		}
		else throw new Exception("AtsyraGoal TypedElement getSystConstant : Not defined")
	}
}

@Aspect (className=AtsyraGoal)
class AtsyraGoalAspect {
	
	/** Returns the expression for the precondition
	 * it takes care of default values if they are used
	 */
	def public BooleanExpression getPreconditionTestStateExpression(Context context){		
		val defaultTestStateExp = _self.getApplicableDefault4PreconditionTestStateExpression(context)
		val conditionTestStateExp = _self.precondition.getTestStateExpression(context)
		if(defaultTestStateExp.present) {
			return GALBuildHelper.createBoolExprAnd(defaultTestStateExp.get, conditionTestStateExp)
		} else {
			return conditionTestStateExp
		}
	}
	
	/** Returns the expression for the precondition
	 * it takes care of default values if they are used
	 */
	def public BooleanExpression getPostconditionTestStateExpression(Context context){
		val defaultTestStateExp = _self.getApplicableDefault4PostconditionTestStateExpression(context)
		val conditionTestStateExp = _self.postcondition.getTestStateExpression(context)
		if(defaultTestStateExp.present) {
			return GALBuildHelper.createBoolExprAnd(defaultTestStateExp.get, conditionTestStateExp)
		} else {
			return conditionTestStateExp
		}
	}
	
	/** compute the applicable testStateExpression complement based on default value to be used on precondition */
	def public Optional<BooleanExpression> getApplicableDefault4PreconditionTestStateExpression(Context context){
		if(_self.defaultUsedInPre !== null){
			val List<DefaultAssignment> applicableAssignments = _self.precondition.getApplicableAssignements(_self.defaultUsedInPre.defaultValueAssignments)
			if(applicableAssignments.empty){
				return Optional.empty
			}
			val seed = applicableAssignments.head.getTestStateExpression(context)
			return Optional.of(applicableAssignments.tail.fold(seed,[r,t | GALBuildHelper.createBoolExprAnd(r,
						t.getTestStateExpression(context)
					)]) )
			
		}		
		return Optional.empty
	}
	/** compute the applicable testStateExpression complement based on default value to be used on poscondition */
	def public Optional<BooleanExpression> getApplicableDefault4PostconditionTestStateExpression(Context context){
		if(_self.defaultUsedInPost !== null){
			val List<DefaultAssignment> applicableAssignments = _self.postcondition.getApplicableAssignements(_self.defaultUsedInPost.defaultValueAssignments)
			if(applicableAssignments.empty){
				return Optional.empty
			}
			val seed = applicableAssignments.head.getTestStateExpression(context)
			return Optional.of(applicableAssignments.tail.fold(seed,[r,t | GALBuildHelper.createBoolExprAnd(r,
						t.getTestStateExpression(context)
					)]) )
			
		}		
		return Optional.empty
	}	
}

@Aspect (className=DefaultAssignment)
class DefaultAssignmentAspect {
	def public BooleanExpression getTestStateExpression(Context context){
		return GALBuildHelper.createBoolExprEqVarCst(_self.target.getVariable(context), _self.assignedValue.getConstant(context))
	}
}
