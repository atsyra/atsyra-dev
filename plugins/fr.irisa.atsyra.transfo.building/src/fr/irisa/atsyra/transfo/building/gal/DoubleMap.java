/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.transfo.building.gal;

import java.util.HashMap;

public class DoubleMap<X,Y,Z> {
	protected HashMap<X,HashMap<Y,Z>> map;
	
	public DoubleMap() {
		map = new HashMap<>();
	}
	
	public Z get(X x, Y y) {
		return map.get(x).get(y);
	}
	
	public void put(X x, Y y, Z z) {
		if(!map.containsKey(x)) {
			map.put(x, new HashMap<>());
		}
		HashMap<Y,Z> tmp = map.get(x);
		tmp.put(y, z);
	}
}
