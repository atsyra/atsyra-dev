package fr.irisa.atsyra.transfo.building.gal


import fr.lip6.move.gal.Variable
import fr.lip6.move.gal.GALTypeDeclaration
import java.util.HashMap
import fr.lip6.move.gal.GalFactory

public class Context {
	// our gal file
	public GALTypeDeclaration gal	
	// variables for attackers
	public HashMap<String, Variable> AttackerLocation
	public HashMap<String, Variable> AttackerLevel
	// variables for alarms
	public HashMap<String, Variable> AlarmEnabled
	public HashMap<String, Variable> AlarmTriggered
	// variables for items
	public HashMap<String, Variable> ItemLocation
	public HashMap<String, Variable> ItemOwnedBy
	// variables for accesses
	public HashMap<String, Variable> AccessOpen
	public HashMap<String, Variable> AccessLocked
	
	public Variable init;
	public Variable goalReached;
	
	public new() {
		this.gal = GalFactory.eINSTANCE.createGALTypeDeclaration
		this.AttackerLocation = new HashMap<String, Variable>();
		this.AttackerLevel = new HashMap<String, Variable>();
		this.AlarmEnabled = new HashMap<String, Variable>();
		this.AlarmTriggered = new HashMap<String, Variable>();
		this.ItemLocation = new HashMap<String, Variable>();
		this.ItemOwnedBy = new HashMap<String, Variable>();
		this.AccessOpen = new HashMap<String, Variable>();
		this.AccessLocked = new HashMap<String, Variable>();
	}
	
	def public GetFeatureMap(String typeName, String featureName) {
		switch(typeName) {
			case "access":
			switch{featureName} {
				case BuildingFeature.OPEN:
				AccessOpen
				case BuildingFeature.LOCKED:
				AccessLocked
			}
			case "attacker":
			switch{featureName} {
				case BuildingFeature.LOCATION:
				AttackerLocation
				case BuildingFeature.LEVEL:
				AttackerLevel
			}
			case "alarm":
			switch{featureName} {
				case BuildingFeature.ENABLED:
				AlarmEnabled
				case BuildingFeature.TRIGGERED:
				AlarmTriggered
			}
			case "item":
			switch{featureName} {
				case BuildingFeature.LOCATION:
				ItemLocation
				case BuildingFeature.OWNER:
				ItemOwnedBy
			}
		}
	}
}