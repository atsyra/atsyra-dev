package fr.irisa.atsyra.transfo.building.gal

import fr.lip6.move.gal.GALTypeDeclaration
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.IProgressMonitor
import java.io.FileOutputStream
import java.io.File
import fr.lip6.move.serialization.SerializationUtil
import fr.lip6.move.gal.GalFactory
import fr.lip6.move.gal.instantiate.GALRewriter
import org.eclipse.core.runtime.SubMonitor
import fr.irisa.atsyra.building.BuildingModel

import static extension fr.irisa.atsyra.transfo.building.gal.AttackerAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.BuildingModelAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.ZoneAspects.*

class AbstractBuilding2GAL {
	public def void writeToFile(GALTypeDeclaration gal, IFile file, IProgressMonitor monitor) {
		val SubMonitor subMonitor = SubMonitor.convert(monitor,  100);
		subMonitor.setTaskName("Saving "+file.getName());
		val out = new FileOutputStream(new File(file.location.toOSString));
		out.write(0);
		out.close();
		SerializationUtil.systemToFile(gal,file.location.toOSString);

		
		subMonitor.done
	}
	
	public def flattenGAL(GALTypeDeclaration gal) {
		val spec = GalFactory.eINSTANCE.createSpecification
		spec.types.add(gal);
		GALRewriter.flatten(spec,true);
		return spec.types.get(0);
	}
	
		
	/**
	 * collect numbers of zones and attackers mappings and produce a java property like string for it
	 * Note that it supposes that initNumbers has been called before
	 */
	def String getGalVariableMapping(BuildingModel buildingModel){
		return '''«FOR building : buildingModel.allBuildings»
		«FOR zone : building.zones»
		zone.«zone.name» = «zone.number»
		«ENDFOR»
		«ENDFOR»«FOR building : buildingModel.allBuildings»
		«FOR att : building.attackers»
		attacker.«att.name» = «att.number»
		«ENDFOR»
		«ENDFOR»
		'''
	}
}