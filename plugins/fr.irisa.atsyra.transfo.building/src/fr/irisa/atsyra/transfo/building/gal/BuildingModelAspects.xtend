package fr.irisa.atsyra.transfo.building.gal


import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.building.BuildingModel
import fr.irisa.atsyra.building.Building
import fr.irisa.atsyra.building.Access
import fr.irisa.atsyra.building.Attacker
import fr.irisa.atsyra.building.Alarm
import fr.irisa.atsyra.building.Item
import static extension fr.irisa.atsyra.transfo.building.gal.BuildingModelAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.BuildingAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.AccessAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.AttackerAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.AlarmAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.ItemAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.ZoneAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.ImportAspects.*
import fr.lip6.move.gal.Variable
import fr.lip6.move.gal.Transition
import fr.irisa.atsyra.building.Zone
import fr.irisa.atsyra.building.Import
import org.eclipse.emf.common.util.URI
import java.util.List
import java.util.ArrayList
import java.util.HashMap
import fr.irisa.atsyra.building.Virtual
import fr.irisa.atsyra.building.Door
import fr.irisa.atsyra.building.Window
import fr.irisa.atsyra.building.BadgedDoor

@Aspect (className=BuildingModel)
class BuildingModelAspects {
	List<Building> allBuildingsCache = null
	
	def List<Building> getImportedBuildings(){
		val List<Building> res = new ArrayList<Building>
		for ( imp : _self.imports) {
			res.addAll(imp.buildings)
		}
		return res
	}
	def List<Building> getAllBuildings(){
		if(_self.allBuildingsCache === null){
			_self.allBuildingsCache = new ArrayList<Building>(_self.buildings)
			_self.allBuildingsCache.addAll(_self.importedBuildings)
		}
		return _self.allBuildingsCache
	}
	def public void createGalElement (Context context) {
		_self.buildings.forEach[building | building.createGalElement(context) ]
	}
	
	def public void linkGalElement (Context context) {
		_self.buildings.forEach[building | building.linkGalElement(context) ]
	}
	
	public static final boolean CLOSE_ACTION_ENABLED = true;
	public static final boolean ENABLE_ACTION_ENABLED = true;
	public static final boolean DROP_ACTION_ENABLED = false;
}

@Aspect (className=Building)
class BuildingAspects {
	def public void createGalElement (Context context) {
		_self.accesses.forEach[access | access.createGalElementForStatic(context) ]
		_self.alarms.forEach[alarm | alarm.createGalElementForStatic(context) ]
		_self.items.forEach[item | item.createGalElementForStatic(context) ]
		_self.attackers.forEach[attacker | attacker.createGalElement(context, _self)]
	}
	
	def public void linkGalElement (Context context) {
		_self.attackers.forEach[attacker | attacker.linkGalElement(context, _self)]
	}
}

@Aspect (className=Attacker)
class AttackerAspects {
	public static HashMap<String,Integer> attackerGalValues = new HashMap<String,Integer>()
	public static int numberOfAttackers;
	/* Initialisation of the values of the attackers of all buildings in GAL
	 * 0 is kept for the null value
	 */
	 public def static void initNumbers(BuildingModel buildingModel){
		var cpt = 1;
		for(building : buildingModel.allBuildings){
			for(attacker : building.attackers){
				attackerGalValues.put(attacker.name,cpt);
				cpt++;
			}
		}
		numberOfAttackers = cpt-1
	}
	
	//getter for attacker number. null is mapped to 0
	def public int getNumber(){
		if(_self === null) {
			return 0;
		}
		else {
			return attackerGalValues.get(_self.name);
		}
	}
	
	public Variable createdVarLocation
	public Variable createdVarLevel
	
	def public void createGalElement (Context context, Building building) {
		//Variables
		_self.createdVarLocation = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.LOCATION) ,GALBuildHelper.galUninit)
		context.AttackerLocation.put(_self.name, _self.createdVarLocation)
		context.gal.variables.add(_self.createdVarLocation)
		_self.createdVarLevel = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.LEVEL),0)
		context.AttackerLevel.put(_self.name, _self.createdVarLevel)
		context.gal.variables.add(_self.createdVarLevel)
		//creating GAL for the elements the attacker interacts with		
		building.accesses.forEach[access | access.createGalElementForInteractions(context, _self) ]
		building.alarms.forEach[alarm | alarm.createGalElementForInteractions(context, _self) ]
		building.items.forEach[item | item.createGalElementForInteractions(context, _self) ]
	}
	
	def public void linkGalElement (Context context, Building building) {
		building.accesses.forEach[access | access.linkGalElement(context, _self) ]
		building.alarms.forEach[alarm | alarm.linkGalElement(context, _self) ]
		building.items.forEach[item | item.linkGalElement(context, _self) ]
	}
}

@Aspect (className=Access)
abstract class AccessAspects {

	def public void createGalElementForStatic (Context context)
	
	def public void createGalElementForInteractions (Context context, Attacker attacker)
	
	def public void linkGalElement (Context context, Attacker attacker)
	
}

@Aspect (className=Virtual)
class VirtualAspects extends AccessAspects {
	HashMap<Attacker,Transition> createdTranGoFromZ1ToZ2 = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranGoFromZ2ToZ1 = new HashMap<Attacker,Transition>()
	
	def public void createGalElementForStatic (Context context) {
		//No variables for virtual accesses
	}
	def public void createGalElementForInteractions (Context context, Attacker attacker) {
		// Transition go_from_z1_to_z2
		val TranGoFromZ1ToZ2 = GALBuildHelper.createTransition(attacker.name + "_goes_from_" +_self.zone1.name + "_to_" + _self.zone2.name + "_by_" + _self.name)
		_self.createdTranGoFromZ1ToZ2.put(attacker, TranGoFromZ1ToZ2)
		context.gal.transitions.add(TranGoFromZ1ToZ2)
		// Transition go_from_z2_to_z1
		val TranGoFromZ2ToZ1 = GALBuildHelper.createTransition(attacker.name + "_goes_from_" +_self.zone2.name + "_to_" + _self.zone1.name + "_by_" + _self.name)
		_self.createdTranGoFromZ2ToZ1.put(attacker, TranGoFromZ2ToZ1)
		context.gal.transitions.add(TranGoFromZ2ToZ1)
	}
	
	def public void linkGalElement (Context context, Attacker attacker) {
		//Transition go_from_z1_to_z2
		_self.createdTranGoFromZ1ToZ2.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone1.number),context)
		_self.createdTranGoFromZ1ToZ2.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(attacker.createdVarLocation, _self.zone2.number))
		_self.zone2.alarms.forEach[alarm | _self.createdTranGoFromZ1ToZ2.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprIsVarTrue(alarm.createdVarEnabled),GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered, GALBuildHelper.galTrue)))]
		//Transition go_from_z2_to_z1 
		_self.createdTranGoFromZ2ToZ1.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone2.number),context)
		_self.createdTranGoFromZ2ToZ1.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(attacker.createdVarLocation, _self.zone1.number))			
		_self.zone1.alarms.forEach[alarm | _self.createdTranGoFromZ2ToZ1.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprIsVarTrue(alarm.createdVarEnabled),GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered, GALBuildHelper.galTrue)))]
	}
}

@Aspect (className=Door)
class DoorAspects extends AccessAspects {
	public Variable createdVarOpenOfAccess
	public Variable createdVarLockedOfAccess
	HashMap<Attacker,Transition> createdTranGoFromZ1ToZ2 = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranGoFromZ2ToZ1 = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranOpenAccess = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranCloseAccess = new HashMap<Attacker,Transition>()
	DoubleMap<Attacker,Item,Transition> createdTranUnlockAccess = new DoubleMap<Attacker,Item,Transition>()
	DoubleMap<Attacker,Item,Transition> createdTranLockAccess = new DoubleMap<Attacker,Item,Transition>()
	
	def public void createGalElementForStatic (Context context) {
		//Variables
		_self.createdVarOpenOfAccess = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.OPEN) ,GALBuildHelper.galUninit)
		context.AccessOpen.put(_self.name, _self.createdVarOpenOfAccess)
		context.gal.variables.add(_self.createdVarOpenOfAccess)
		if(!_self.keys.empty){
			_self.createdVarLockedOfAccess = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.LOCKED),GALBuildHelper.galUninit)
			context.AccessLocked.put(_self.name, _self.createdVarLockedOfAccess)
			context.gal.variables.add(_self.createdVarLockedOfAccess)
		}
	}
	def public void createGalElementForInteractions (Context context, Attacker attacker) {
		// Transition go_from_z1_to_z2
		val TranGoFromZ1ToZ2 = GALBuildHelper.createTransition(attacker.name + "_goes_from_" +_self.zone1.name + "_to_" + _self.zone2.name + "_by_" + _self.name)
		_self.createdTranGoFromZ1ToZ2.put(attacker, TranGoFromZ1ToZ2)
		context.gal.transitions.add(TranGoFromZ1ToZ2)
		// Transition go_from_z2_to_z1
		val TranGoFromZ2ToZ1 = GALBuildHelper.createTransition(attacker.name + "_goes_from_" +_self.zone2.name + "_to_" + _self.zone1.name + "_by_" + _self.name)
		_self.createdTranGoFromZ2ToZ1.put(attacker, TranGoFromZ2ToZ1)
		context.gal.transitions.add(TranGoFromZ2ToZ1)
		// Transition openAccess
		val TranOpenAccess = GALBuildHelper.createTransition(attacker.name + "_opens_" + _self.name)
		_self.createdTranOpenAccess.put(attacker,TranOpenAccess)
		context.gal.transitions.add(TranOpenAccess)
		// Transition closeAccess
		val TranCloseAccess = GALBuildHelper.createTransition(attacker.name + "_closes_" + _self.name)
		_self.createdTranCloseAccess.put(attacker,TranCloseAccess)
		if(CLOSE_ACTION_ENABLED) {
			context.gal.transitions.add(TranCloseAccess)
		}
		// Transition unlockAccess
		_self.keys.forEach[key | 
			val TranUnlockAccess = GALBuildHelper.createTransition(attacker.name + "_unlocks_" + _self.name + "_with_" + key.name)
			_self.createdTranUnlockAccess.put(attacker,key,TranUnlockAccess)
			context.gal.transitions.add(TranUnlockAccess)
		]
		// Transition LockAccess
		_self.keys.forEach[key | 
			val TranLockAccess = GALBuildHelper.createTransition(attacker.name + "_locks_" + _self.name + "_with_" + key.name)
			_self.createdTranLockAccess.put(attacker,key,TranLockAccess)
			context.gal.transitions.add(TranLockAccess)
		]
	}
	
	def public void linkGalElement (Context context, Attacker attacker) {
		//Transition go_from_z1_to_z2
		val from12_attackerInZ1 = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone1.number)
		val from12_accessOpen = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue)
		_self.createdTranGoFromZ1ToZ2.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(from12_attackerInZ1,from12_accessOpen),context)
		_self.createdTranGoFromZ1ToZ2.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(attacker.createdVarLocation, _self.zone2.number))
		_self.alarms.forEach[alarm | 
			_self.createdTranGoFromZ1ToZ2.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		_self.zone2.alarms.forEach[alarm | _self.createdTranGoFromZ1ToZ2.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprIsVarTrue(alarm.createdVarEnabled),GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered, GALBuildHelper.galTrue)))]
		val from21_attackerInZ2 = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone2.number)
		val from21_accessOpen = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue)
		_self.createdTranGoFromZ2ToZ1.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(from21_attackerInZ2,from21_accessOpen),context)
		_self.createdTranGoFromZ2ToZ1.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(attacker.createdVarLocation, _self.zone1.number))
		_self.alarms.forEach[alarm | 
			_self.createdTranGoFromZ2ToZ1.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		_self.zone1.alarms.forEach[alarm | _self.createdTranGoFromZ2ToZ1.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprIsVarTrue(alarm.createdVarEnabled),GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered, GALBuildHelper.galTrue)))]
		
		//Transition open
		val open_attackerInZ1orZ2 = GALBuildHelper.createBoolExprOr(GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone1.number),GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone2.number))
		val open_closed = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galFalse)
		if(_self.createdVarLockedOfAccess !== null) {
			val open_unlocked = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarLockedOfAccess, GALBuildHelper.galFalse)
			_self.createdTranOpenAccess.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(GALBuildHelper.createBoolExprAnd(open_attackerInZ1orZ2, open_unlocked),open_closed),context)			
		} else {
			// no key for this access
			_self.createdTranOpenAccess.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(open_attackerInZ1orZ2, open_closed), context)			
		}
		_self.createdTranOpenAccess.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue))
		_self.alarms.forEach[alarm | 
			_self.createdTranOpenAccess.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		//Transition close
		val close_attackerInZ1orZ2 = GALBuildHelper.createBoolExprOr(GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone1.number),GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone2.number))
		val close_open = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue)
		if(_self.createdVarLockedOfAccess !== null){
			val close_unlocked = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarLockedOfAccess, GALBuildHelper.galFalse)
			_self.createdTranCloseAccess.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(GALBuildHelper.createBoolExprAnd(close_attackerInZ1orZ2, close_unlocked),close_open),context)
			
		} else {
			_self.createdTranCloseAccess.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(close_attackerInZ1orZ2, close_open), context)
			
		}
		_self.createdTranCloseAccess.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarOpenOfAccess, GALBuildHelper.galFalse))
		_self.alarms.forEach[alarm | 
			_self.createdTranCloseAccess.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		//Transition unlock
		
		_self.keys.forEach[ key |
			val unlock_attackerInZ1orZ2 = GALBuildHelper.createBoolExprOr(GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone1.number),GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone2.number))
			val unlock_locked = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarLockedOfAccess, GALBuildHelper.galTrue)
			val unlock_closed = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galFalse)
			val unlock_gotKey = GALBuildHelper.createBoolExprEqVarCst(key.createdVarOwnedby,attacker.number)
			_self.createdTranUnlockAccess.get(attacker,key).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(GALBuildHelper.createBoolExprAnd(GALBuildHelper.createBoolExprAnd(unlock_attackerInZ1orZ2,unlock_gotKey), unlock_locked),unlock_closed),context)
		
			_self.createdTranUnlockAccess.get(attacker,key).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarLockedOfAccess, GALBuildHelper.galFalse))
			_self.alarms.forEach[alarm | 
				_self.createdTranUnlockAccess.get(attacker,key).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																						GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
			]
		]
		//Transition lock
		_self.keys.forEach[ key |
			val lock_attackerInZ1orZ2 = GALBuildHelper.createBoolExprOr(GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone1.number),GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.zone2.number))
			val lock_unlocked = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarLockedOfAccess, GALBuildHelper.galFalse)
			val lock_closed = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galFalse)
			val lock_gotKey = GALBuildHelper.createBoolExprEqVarCst(key.createdVarOwnedby,attacker.number)
			_self.createdTranLockAccess.get(attacker,key).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(GALBuildHelper.createBoolExprAnd(GALBuildHelper.createBoolExprAnd(lock_attackerInZ1orZ2,lock_gotKey), lock_unlocked),lock_closed),context)
			_self.createdTranLockAccess.get(attacker,key).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarLockedOfAccess, GALBuildHelper.galTrue))
			_self.alarms.forEach[alarm | 
				_self.createdTranLockAccess.get(attacker,key).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																						GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
				))
			]
		]
		}
}

@Aspect (className=Window)
class WindowAspects extends AccessAspects {
	public Variable createdVarOpenOfAccess
	HashMap<Attacker,Transition> createdTranGoFromIToO = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranGoFromOToI = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranOpenAccess = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranCloseAccess = new HashMap<Attacker,Transition>()
	
	def public void createGalElementForStatic (Context context) {
		//Variables
		_self.createdVarOpenOfAccess = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.OPEN) ,GALBuildHelper.galUninit)
		context.AccessOpen.put(_self.name, _self.createdVarOpenOfAccess)
		context.gal.variables.add(_self.createdVarOpenOfAccess)
	}
	def public void createGalElementForInteractions (Context context, Attacker attacker) {
		// Transition go_from_inside_to_outside
		val TranGoFromIToO = GALBuildHelper.createTransition(attacker.name + "_goes_from_" +_self.inside.name + "_to_" + _self.outside.name + "_by_" + _self.name)
		_self.createdTranGoFromIToO.put(attacker, TranGoFromIToO)
		context.gal.transitions.add(TranGoFromIToO)
		// Transition go_from_outside_to_inside
		val TranGoFromOToI = GALBuildHelper.createTransition(attacker.name + "_goes_from_" +_self.outside.name + "_to_" + _self.inside.name + "_by_" + _self.name)
		_self.createdTranGoFromOToI.put(attacker, TranGoFromOToI)
		context.gal.transitions.add(TranGoFromOToI)
		// Transition openAccess
		val TranOpenAccess = GALBuildHelper.createTransition(attacker.name + "_opens_" + _self.name)
		_self.createdTranOpenAccess.put(attacker,TranOpenAccess)
		context.gal.transitions.add(TranOpenAccess)
		// Transition closeAccess
		val TranCloseAccess = GALBuildHelper.createTransition(attacker.name + "_closes_" + _self.name)
		_self.createdTranCloseAccess.put(attacker,TranCloseAccess)
		if(CLOSE_ACTION_ENABLED) {
			context.gal.transitions.add(TranCloseAccess)
		}
	}
	
	def public void linkGalElement (Context context, Attacker attacker) {
		//Transition go_from_inside_to_outside
		val from12_attackerInZ1 = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.inside.number)
		val from12_accessOpen = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue)
		_self.createdTranGoFromIToO.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(from12_attackerInZ1,from12_accessOpen),context)
		_self.createdTranGoFromIToO.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(attacker.createdVarLocation, _self.outside.number))
		_self.alarms.forEach[alarm | 
			_self.createdTranGoFromIToO.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		_self.outside.alarms.forEach[alarm | _self.createdTranGoFromIToO.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprIsVarTrue(alarm.createdVarEnabled),GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered, GALBuildHelper.galTrue)))]
		// Transition go_from_outside_to_inside
		val from21_attackerInZ2 = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.outside.number)
		val from21_accessOpen = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue)
		_self.createdTranGoFromOToI.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(from21_attackerInZ2,from21_accessOpen),context)
		_self.createdTranGoFromOToI.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(attacker.createdVarLocation, _self.inside.number))
		_self.alarms.forEach[alarm | 
			_self.createdTranGoFromOToI.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		_self.inside.alarms.forEach[alarm | _self.createdTranGoFromOToI.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprIsVarTrue(alarm.createdVarEnabled),GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered, GALBuildHelper.galTrue)))]
		//Transition open
		val open_attackerInside = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.inside.number)
		val open_closed = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galFalse)
		_self.createdTranOpenAccess.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(open_attackerInside,open_closed),context)
		_self.createdTranOpenAccess.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue))
		_self.alarms.forEach[alarm | 
			_self.createdTranOpenAccess.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		//Transition close
		val close_attackerInZ1orZ2 = GALBuildHelper.createBoolExprOr(GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.inside.number),GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.outside.number))
		val close_open = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue)
		_self.createdTranCloseAccess.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(close_attackerInZ1orZ2,close_open),context)
		_self.createdTranCloseAccess.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarOpenOfAccess, GALBuildHelper.galFalse))
		_self.alarms.forEach[alarm | 
			_self.createdTranCloseAccess.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		}
}

@Aspect (className=BadgedDoor)
class BadgedDoorAspects extends AccessAspects {
	public Variable createdVarOpenOfAccess
	HashMap<Attacker,Transition> createdTranGoFromIToO = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranGoFromOToI = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranOpenAccessFromI = new HashMap<Attacker,Transition>()
	DoubleMap<Attacker,Item,Transition> createdTranOpenAccessWith = new DoubleMap<Attacker,Item,Transition>()
	HashMap<Attacker,Transition> createdTranCloseAccess = new HashMap<Attacker,Transition>()
	
	def public void createGalElementForStatic (Context context) {
		//Variables
		_self.createdVarOpenOfAccess = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.OPEN) ,GALBuildHelper.galUninit)
		context.AccessOpen.put(_self.name, _self.createdVarOpenOfAccess)
		context.gal.variables.add(_self.createdVarOpenOfAccess)
	}
	def public void createGalElementForInteractions (Context context, Attacker attacker) {
		// Transition go_from_inside_to_outside
		val TranGoFromIToO = GALBuildHelper.createTransition(attacker.name + "_goes_from_" +_self.inside.name + "_to_" + _self.outside.name + "_by_" + _self.name)
		_self.createdTranGoFromIToO.put(attacker, TranGoFromIToO)
		context.gal.transitions.add(TranGoFromIToO)
		// Transition go_from_outside_to_inside
		val TranGoFromOToI = GALBuildHelper.createTransition(attacker.name + "_goes_from_" +_self.outside.name + "_to_" + _self.inside.name + "_by_" + _self.name)
		_self.createdTranGoFromOToI.put(attacker, TranGoFromOToI)
		context.gal.transitions.add(TranGoFromOToI)
		// Transition openAccessFromI
		val TranOpenAccessFromI = GALBuildHelper.createTransition(attacker.name + "_opens_" + _self.name + "_from_inside")
		_self.createdTranOpenAccessFromI.put(attacker,TranOpenAccessFromI)
		context.gal.transitions.add(TranOpenAccessFromI)
		// Transition openAccessWith
		_self.badges.forEach[badge | 
			val TranOpenAccessWith = GALBuildHelper.createTransition(attacker.name + "_opens_" + _self.name + "_with_" + badge.name)
			_self.createdTranOpenAccessWith.put(attacker,badge,TranOpenAccessWith)
			context.gal.transitions.add(TranOpenAccessWith)
		]
		// Transition closeAccess
		val TranCloseAccess = GALBuildHelper.createTransition(attacker.name + "_closes_" + _self.name)
		_self.createdTranCloseAccess.put(attacker,TranCloseAccess)
		if(CLOSE_ACTION_ENABLED) {
			context.gal.transitions.add(TranCloseAccess)
		}
	}
	
	def public void linkGalElement (Context context, Attacker attacker) {
		//Transition go_from_inside_to_outside
		val from12_attackerInZ1 = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.inside.number)
		val from12_accessOpen = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue)
		_self.createdTranGoFromIToO.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(from12_attackerInZ1,from12_accessOpen),context)
		_self.createdTranGoFromIToO.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(attacker.createdVarLocation, _self.outside.number))
		_self.alarms.forEach[alarm | 
			_self.createdTranGoFromIToO.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		_self.inside.alarms.forEach[alarm | _self.createdTranGoFromOToI.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprIsVarTrue(alarm.createdVarEnabled),GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered, GALBuildHelper.galTrue)))]
		// Transition go_from_outside_to_inside
		val from21_attackerInZ2 = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.outside.number)
		val from21_accessOpen = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue)
		_self.createdTranGoFromOToI.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(from21_attackerInZ2,from21_accessOpen),context)
		_self.createdTranGoFromOToI.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(attacker.createdVarLocation, _self.inside.number))
		_self.alarms.forEach[alarm | 
			_self.createdTranGoFromOToI.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		_self.outside.alarms.forEach[alarm | _self.createdTranGoFromIToO.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprIsVarTrue(alarm.createdVarEnabled),GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered, GALBuildHelper.galTrue)))]
		//Transition openFromI
		val open_attackerInside = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.inside.number)
		val openFromI_closed = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galFalse)
		_self.createdTranOpenAccessFromI.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(open_attackerInside,openFromI_closed),context)
		_self.createdTranOpenAccessFromI.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue))
		_self.alarms.forEach[alarm | 
			_self.createdTranOpenAccessFromI.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		//Transition openWith
		_self.badges.forEach[ badge | 
			val openWith_gotBadge = GALBuildHelper.createBoolExprEqVarCst(badge.createdVarOwnedby,attacker.number)
			val openWith_attackerOutsideWithBadge = GALBuildHelper.createBoolExprAnd(GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.outside.number),openWith_gotBadge)
			val openWith_closed = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galFalse)
			_self.createdTranOpenAccessWith.get(attacker,badge).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(openWith_attackerOutsideWithBadge,openWith_closed),context)
			_self.createdTranOpenAccessWith.get(attacker,badge).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue))
			_self.alarms.forEach[alarm | 
				_self.createdTranOpenAccessWith.get(attacker,badge).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																						GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
				))
			]
		]
		
		//Transition close
		val close_attackerInZ1orZ2 = GALBuildHelper.createBoolExprOr(GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.inside.number),GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation, _self.outside.number))
		val close_open = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOpenOfAccess, GALBuildHelper.galTrue)
		_self.createdTranCloseAccess.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(close_attackerInZ1orZ2,close_open),context)
		_self.createdTranCloseAccess.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarOpenOfAccess, GALBuildHelper.galFalse))
		_self.alarms.forEach[alarm | 
			_self.createdTranCloseAccess.get(attacker).actions.add(GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprEqVarCst(alarm.createdVarEnabled,GALBuildHelper.galTrue),
																					GALBuildHelper.createVarAssignConst(alarm.createdVarTriggered,GALBuildHelper.galTrue)
			))
		]
		}
}

@Aspect (className=Alarm)
class AlarmAspects {
	public Variable createdVarEnabled
	public Variable createdVarTriggered
	HashMap<Attacker,Transition> createdTranEnable = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTranDisable = new HashMap<Attacker,Transition>()
	
	def public void createGalElementForStatic (Context context) {
		//variables
		_self.createdVarEnabled = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.ENABLED) ,GALBuildHelper.galUninit)
		context.AlarmEnabled.put(_self.name, _self.createdVarEnabled)
		context.gal.variables.add(_self.createdVarEnabled)
		_self.createdVarTriggered = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.TRIGGERED),GALBuildHelper.galUninit)
		context.AlarmTriggered.put(_self.name, _self.createdVarTriggered)
		context.gal.variables.add(_self.createdVarTriggered)
	}
	
	def public void createGalElementForInteractions (Context context, Attacker attacker) {
		if(_self.location !== null) {
			// only alarms with location may be enabled/disabled by attacker
			// Transition enableAlarm
			val TranEnable = GALBuildHelper.createTransition(attacker.name + "_enables_" + _self.name)
			_self.createdTranEnable.put(attacker,TranEnable)
			if(ENABLE_ACTION_ENABLED) {
				context.gal.transitions.add(TranEnable)
			}
			// Transition disableAlarm
			val TranDisable = GALBuildHelper.createTransition(attacker.name + "_disables_" + _self.name)
			_self.createdTranDisable.put(attacker,TranDisable)
			context.gal.transitions.add(TranDisable)
			
		}
	}
	
	def public void linkGalElement (Context context, Attacker attacker) {
		if(_self.location !== null) {
			// only alarms with location may be enabled/disabled by attacker
			//Transition enable
			val enable_disabled = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarEnabled,GALBuildHelper.galFalse)
			val enable_attackerInZ = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation,_self.location.number)
			_self.createdTranEnable.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(enable_attackerInZ, enable_disabled),context)
			_self.createdTranEnable.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarEnabled,GALBuildHelper.galTrue))
			//Transition disable
			val disable_enabled = GALBuildHelper.createBoolExprEqVarCst(_self.createdVarEnabled,GALBuildHelper.galTrue)
			val disable_attackerInZ = GALBuildHelper.createBoolExprEqVarCst(attacker.createdVarLocation,_self.location.number)
			_self.createdTranDisable.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprAnd(disable_attackerInZ, disable_enabled),context)
			_self.createdTranDisable.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarEnabled,GALBuildHelper.galFalse))
		}
	}
}

@Aspect (className=Item)
class ItemAspects {
	public Variable createdVarLocation
	public Variable createdVarOwnedby
	HashMap<Attacker,Transition> createdTransPick = new HashMap<Attacker,Transition>()
	HashMap<Attacker,Transition> createdTransDrop = new HashMap<Attacker,Transition>()
	
	def public void createGalElementForStatic (Context context) {
		//variables
		_self.createdVarLocation = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.LOCATION) ,GALBuildHelper.galUninit)
		context.ItemLocation.put(_self.name, _self.createdVarLocation)
		context.gal.variables.add(_self.createdVarLocation)
		_self.createdVarOwnedby = GALBuildHelper.createVariable(GalNamer.getVarName(_self.name,BuildingFeature.OWNER),GALBuildHelper.galUninit)
		context.ItemOwnedBy.put(_self.name, _self.createdVarOwnedby)
		context.gal.variables.add(_self.createdVarOwnedby)
	}
	
	def public void createGalElementForInteractions (Context context, Attacker attacker) {
		// Transition pick item
		val TransPick = GALBuildHelper.createTransition(attacker.name + "_picks_" + _self.name)
		_self.createdTransPick.put(attacker,TransPick)
		context.gal.transitions.add(TransPick)
		// Transition drop item
		val TransDrop = GALBuildHelper.createTransition(attacker.name + "_drops_" + _self.name)
		_self.createdTransDrop.put(attacker,TransDrop)
		if(DROP_ACTION_ENABLED) {
			context.gal.transitions.add(TransDrop)
		}
	}
	
	def public void linkGalElement (Context context, Attacker attacker) {
		//Transition pick
		_self.createdTransPick.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprEqVarVar(attacker.createdVarLocation, _self.createdVarLocation),context)
		_self.createdTransPick.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarOwnedby, attacker.number))
		_self.createdTransPick.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarLocation,0))
		//Transition drop
		_self.createdTransDrop.get(attacker).guard = GALBuildHelper.createSystemGuard(GALBuildHelper.createBoolExprEqVarCst(_self.createdVarOwnedby , attacker.number),context)
		_self.createdTransDrop.get(attacker).actions.add(GALBuildHelper.createVarAssignVar(_self.createdVarLocation, attacker.createdVarLocation))
		_self.createdTransDrop.get(attacker).actions.add(GALBuildHelper.createVarAssignConst(_self.createdVarOwnedby,0))
	}
}

@Aspect (className=Zone)
class ZoneAspects {
	public static HashMap<String,Integer> zoneGalValues = new HashMap<String,Integer>()
	public static int numberOfZones;
	private static boolean initialized = false;
	/* Initialisation of the values of the zones of all buildings in GAL
	 * 0 is kept for the null value
	 */
	 static def public void initNumbers(BuildingModel buildingModel){
	 	//if(initialized) throw new RuntimeException("Attempted to initialize twice"); // exception if the values were already initialized
	 	initialized = true;
		var cpt = 1;
		for(building : buildingModel.allBuildings){
			for(zone : building.zones){
				zoneGalValues.put(zone.name,cpt);
				cpt++;
			}
		}
		numberOfZones = cpt-1
	}
	
	//getter for zone number. null is mapped to 0
	def public int getNumber(){
		if(_self === null) {
			return 0;
		}
		else {
			return zoneGalValues.get(_self.name);
		}
	}
	
	def	public static String getZoneNameFromValue(int value) {
		// first, test that value where previously initialized
		if(!initialized) throw new RuntimeException("Attempted to get zone names without initialization them");
		// If the value is out of bounds, return null
		if(!zoneGalValues.containsValue(value)) return null;
		// look for a key that maps to "value". The look return as soon as a zone is found
		for (entry : zoneGalValues.entrySet) {
			if(entry.value == value) return entry.key
		}
		// no key found, which contradicts that zoneGalValues contains value
		return null;
	}
	
}

@Aspect (className=Import)
class ImportAspects {
	def URI getEMFURI(){
		return URI.createURI(_self.getImportURI()).resolve(_self.eResource().getURI())
	}
	
	def List<Building> getBuildings(){
		val importRes = _self.eResource.resourceSet.getResource(_self.EMFURI, true)
		val bmodel = importRes.contents.get(0) as BuildingModel
		return bmodel.allBuildings
	}
}