package fr.irisa.atsyra.transfo.building.gal

import atsyragoal.GoalCondition
import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.transfo.building.gal.Context
import fr.irisa.atsyra.transfo.building.gal.GALBuildHelper
import fr.lip6.move.gal.Transition
import fr.lip6.move.gal.TypedefDeclaration
import fr.lip6.move.gal.Variable


import static extension fr.irisa.atsyra.transfo.building.gal.AtsyraGoalAspect.*
import static extension fr.irisa.atsyra.transfo.building.gal.AttackerAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.ZoneAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.GoalConditionAspects.*
import atsyragoal.AtsyraGoal
import fr.lip6.move.gal.BooleanExpression

@Aspect (className=GoalCondition)
class Condition2GAL_GoalConditionAspect {
	Variable initStep
	Variable goalReached
	TypedefDeclaration Bools
	TypedefDeclaration Attackers
	TypedefDeclaration Zones
	Transition initVars
	Transition initFinish
	Transition reachGoal
	Integer initialisationStepCounter

	def public void updateGalWithInitializationSearch (Context context) {
	
		_self.initStep = GALBuildHelper.createVariable("initStep",0)
		_self.initStep.comment = "/** step in the initialization phase, this make sure to initialize the variables in a fixed order */"
		context.gal.variables.add(_self.initStep)
		_self.goalReached = GALBuildHelper.createVariable("goal_reached",0)
		context.gal.variables.add(_self.goalReached)
		/* _self.Bools = GALBuildHelper.createTypeDefDeclaration("bools",0,1)
		context.gal.typedefs.add(_self.Bools)
		_self.Attackers = GALBuildHelper.createTypeDefDeclaration("attackers",1,numberOfAttackers)
		context.gal.typedefs.add(_self.Attackers)
		_self.Zones = GALBuildHelper.createTypeDefDeclaration("zones",1,numberOfZones)
		context.gal.typedefs.add(_self.Zones)
		*/
		
		// create some phase, where only one valid action is done for initializing
		_self.initialisationStepCounter = 0;
			
		/* for each item, the two variables from ItemLocalization and ItemOwnedBy  are not independent. 
		 * we need to help GAL to handle their common initialization */ 
 	 	context.ItemLocation.forEach[item, variableLoc| 
			// First, get the owner variable corresponding to the same item
			val variableOwner = context.ItemOwnedBy.get(item)
			
			(1..numberOfZones).forEach[zoneValue | 
				// Create the transition initializing a location for the item. 
				val labLoc = _self.createStepTransitionForVariableValue(context, item+"_location",variableLoc, zoneValue)
				context.gal.transitions.add(labLoc)
			]
			(1..numberOfAttackers).forEach[attackerValue | 
				// Create the transition initializing a location for the item. labLoc is the label of this transition.
				val labOwn = _self.createStepTransitionForVariableValue(context, item+"_owner", variableOwner, attackerValue)
				context.gal.transitions.add(labOwn)
			]
			
			_self.initialisationStepCounter = _self.initialisationStepCounter +1
		] 
		
		
		// TODO deal with opened door which cannot be locked !? must do something like 
		
		context.AccessOpen.forEach[acc, variable|
			(0..1).forEach[boolValue |
				val traAccessClosed = _self.createStepTransitionForVariableValue(context, acc+"_open", variable, boolValue)
				context.gal.transitions.add(traAccessClosed)
				]
			_self.initialisationStepCounter = _self.initialisationStepCounter +1
		]
		context.AccessLocked.forEach[acc, variable|
			(0..1).forEach[boolValue |
				val traAccessClosed = _self.createStepTransitionForVariableValue(context, acc+"_locked", variable, boolValue)
				context.gal.transitions.add(traAccessClosed)
				
				]
			
			_self.initialisationStepCounter = _self.initialisationStepCounter +1
		]
		
		context.AttackerLocation.forEach[att, variable|
			(1..numberOfZones).forEach[zoneValue |
				val traAccessClosed = _self.createStepTransitionForVariableValue(context, att+"_location", variable, zoneValue)
				context.gal.transitions.add(traAccessClosed)
				]
			
			_self.initialisationStepCounter = _self.initialisationStepCounter +1
		]
		
		
		context.AlarmEnabled.forEach[alm, variable|
			(0..1).forEach[boolValue |
				val traAccessClosed = _self.createStepTransitionForVariableValue(context, alm+"_enabled", variable, boolValue)
				context.gal.transitions.add(traAccessClosed)
				]
			
			_self.initialisationStepCounter = _self.initialisationStepCounter +1
		]
		
		
		
		context.AlarmTriggered.forEach[alm, variable|
			(0..1).forEach[boolValue |
				val traAccessClosed = _self.createStepTransitionForVariableValue(context, alm+"_triggered", variable, boolValue)
				context.gal.transitions.add(traAccessClosed)
				]
			
			_self.initialisationStepCounter = _self.initialisationStepCounter +1
		]
		
		// Reach goal = all initialization steps done + condition
		_self.reachGoal = GALBuildHelper.createTransition("reach_goal")
		context.gal.transitions.add(_self.reachGoal)
		
		var BooleanExpression conditionTestStateExpression
		if(_self.eContainer instanceof AtsyraGoal) {
			val goal = _self.eContainer as AtsyraGoal
			if(_self.eContainingFeature.name == "precondition"){
				// is in pre of a goal
				conditionTestStateExpression = goal.getPreconditionTestStateExpression(context)
			} else {
				//  in post of a goal
				conditionTestStateExpression = goal.getPostconditionTestStateExpression(context)
			}
		} else {
			// not in a pre/post
			conditionTestStateExpression = _self.getTestStateExpression(context)
		}
		
		_self.reachGoal.guard = GALBuildHelper.createBoolExprAnd(
			GALBuildHelper.createBoolExprEqVarCst(_self.initStep,_self.initialisationStepCounter),
			conditionTestStateExpression
			)
		_self.reachGoal.actions.add(GALBuildHelper.createVarAssignConst(_self.goalReached,GALBuildHelper.galTrue))
	}

	/*
	def private Transition createTransitionLocForItem(Context context, String itemName, Variable variableLoc, int locationValue) {
		val transitionLoc = GALBuildHelper.createTransition("assign_initial_value_of_" + itemName + "_location_"+locationValue)
		
		transitionLoc.guard = GALBuildHelper.createBoolExprAnd(
			GALBuildHelper.createBoolExprEqVarCst(_self.initStep,_self.initialisationStepCounter), 
			GALBuildHelper.createBoolExprEqVarCst(variableLoc,0)
		)
		
		
		val chooseLoc = GALBuildHelper.createVarAssignConst(variableLoc, locationValue)
		transitionLoc.actions.add(chooseLoc)
		val incrementInitStep = GALBuildHelper.createVarAssignConst(_self.initStep, _self.initialisationStepCounter+1)
		transitionLoc.actions.add(incrementInitStep)
		return transitionLoc
	}
	
	def private Transition createTransitionOwnForItem(Context context, String itemName, Variable variableOwn, int ownerValue) {
		val transitionOwn = GALBuildHelper.createTransition("assign_initial_value_of_" + itemName + "_owner_"+ownerValue)
		
		transitionOwn.guard = GALBuildHelper.createBoolExprAnd(
			GALBuildHelper.createBoolExprEqVarCst(_self.initStep,_self.initialisationStepCounter), 
			GALBuildHelper.createBoolExprEqVarCst(variableOwn, 0)
		)
		
		
		val chooseOwner = GALBuildHelper.createVarAssignConst(variableOwn, ownerValue)
		transitionOwn.actions.add(chooseOwner)
		val incrementInitStep = GALBuildHelper.createVarAssignConst(_self.initStep, _self.initialisationStepCounter+1)
		transitionOwn.actions.add(incrementInitStep)
		
		return transitionOwn
	}*/
	
	def private Transition createStepTransitionForVariableValue(Context context, String variableName, Variable variable, int varValue) {
		val transitionLoc = GALBuildHelper.createTransition("assign_" + variableName + "_"+varValue)
		
		transitionLoc.guard = GALBuildHelper.createBoolExprAnd(
			GALBuildHelper.createBoolExprEqVarCst(_self.initStep,_self.initialisationStepCounter), 
			GALBuildHelper.createBoolExprEqVarCst(variable,0)
		)
		val chooseLoc = GALBuildHelper.createVarAssignConst(variable, varValue)
		transitionLoc.actions.add(chooseLoc)
		val incrementInitStep = GALBuildHelper.createVarAssignConst(_self.initStep, _self.initialisationStepCounter+1)
		transitionLoc.actions.add(incrementInitStep)
		return transitionLoc
	}
}
