package fr.irisa.atsyra.transfo.building.gal


class GalNamer {
	def static String getVarName(String object, String feature) {
		object + "." + feature
	}
	def static String getParamName(String variableName) {
		"param_" + variableName.replace('.','_')
	}
}

class BuildingFeature {
	static public final String LOCATION = "location"
	static public final String LEVEL = "level"
	static public final String OWNER = "owner"
	static public final String OPEN = "open"
	static public final String LOCKED = "locked"
	static public final String ENABLED = "enabled"
	static public final String TRIGGERED = "triggered"
}