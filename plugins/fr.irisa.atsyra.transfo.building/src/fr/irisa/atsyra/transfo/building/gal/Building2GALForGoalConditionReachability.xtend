
package fr.irisa.atsyra.transfo.building.gal

import fr.irisa.atsyra.building.BuildingModel
import fr.lip6.move.gal.GALTypeDeclaration

import static extension fr.irisa.atsyra.transfo.building.gal.Condition2GAL_GoalConditionAspect.*
import static extension fr.irisa.atsyra.transfo.building.gal.BuildingModelAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.ZoneAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.AttackerAspects.*
import atsyragoal.GoalCondition
import fr.irisa.atsyra.transfo.building.gal.Context

class Building2GALForGoalConditionReachability extends AbstractBuilding2GAL {
	def GALTypeDeclaration transformToGAL(BuildingModel buildingModel, GoalCondition condition){
		//context initialization
		val Context context = new Context()
		context.gal.name = "Building"
		//initialization of the constants
		buildingModel.buildings.get(0).zones.get(0).initNumbers(buildingModel)
		buildingModel.buildings.get(0).attackers.get(0).initNumbers(buildingModel)
		//creation of the GAL part from Building
		buildingModel.createGalElement(context)
		buildingModel.linkGalElement(context)
		
		// actually we don't care about non initialization transition created by the previous step,
		// clear them
		context.gal.transitions.clear
		
		//creation of the GAL part from GoalCondition used to search initialization steps
		condition.updateGalWithInitializationSearch(context)		
		return context.gal
	}
}