/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.transfo.building.graph;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;

import fr.irisa.atsyra.building.BuildingModel;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;

public class LocationGraph {
	
	public void doGenerate(Resource resource, IResource iRes, IFolder genFolder, IProgressMonitor monitor) throws CoreException, IOException {
		
		BuildingModel buildingModel = (BuildingModel) resource.getContents().get(0);
		if(!buildingModel.getBuildings().isEmpty()){
			IFile destIFile = genFolder.getFile(resource.getURI().lastSegment().substring(0, resource.getURI().lastSegment().lastIndexOf("."))+"_definition.atg");
			IFileUtils.writeInFileIfDifferent(destIFile, generateGraph(buildingModel), monitor);
		}
	}
	
	private String generateGraph(BuildingModel buildingModel) {
		return null;
		
		
	}
}
