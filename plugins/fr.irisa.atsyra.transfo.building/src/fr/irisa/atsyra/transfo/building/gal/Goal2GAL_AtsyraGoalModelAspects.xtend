package fr.irisa.atsyra.transfo.building.gal

import atsyragoal.AtsyraGoal
import fr.inria.diverse.k3.al.annotationprocessor.Aspect

import fr.irisa.atsyra.transfo.building.gal.Context
import fr.irisa.atsyra.transfo.building.gal.GALBuildHelper
import static extension fr.irisa.atsyra.transfo.building.gal.GALBuildHelper.*
import fr.irisa.atsyra.transfo.building.gal.GalNamer
import fr.lip6.move.gal.BooleanExpression
import fr.lip6.move.gal.Transition
import fr.lip6.move.gal.TypedefDeclaration
import fr.lip6.move.gal.Variable

import static fr.irisa.atsyra.transfo.building.gal.AttackerAspects.*
import static fr.irisa.atsyra.transfo.building.gal.ZoneAspects.*

import static extension fr.irisa.atsyra.transfo.building.gal.AtsyraGoalAspect.*
import static extension fr.irisa.atsyra.transfo.building.gal.GoalConditionAspects.*

import static extension fr.irisa.atsyra.transfo.building.gal.TypedElementAspects.*
import fr.lip6.move.gal.SelfCall

@Aspect (className=AtsyraGoal)
class Goal2GAL_AtsyraGoalAspects {
	TypedefDeclaration Bools
	TypedefDeclaration Attackers
	TypedefDeclaration Zones
	Transition initVars
	Transition reachGoal
	
	def public void createGlobalVars(Context context) {
		context.init = GALBuildHelper.createVariable("init",0)
		context.gal.variables.add(context.init)
		//_self.init.comment = "/** my wonderful comment\n"
		context.goalReached = GALBuildHelper.createVariable("goal_reached",0)
		context.gal.variables.add(context.goalReached)
	}
	
	/** This function apply the strict assignment from the precondition to the GAL model
	 * If there is contradictory assignments in the precondition, only the last one processed by forEachAssignement will be used
	 */
	def private void updateAssignedVars(Context context) {
		_self.precondition.forEachAssignment([variable, constant | 
			val name = variable.name.split("\\.",2).get(0)
			if(context.ItemLocation.containsKey(name) && context.ItemLocation.get(name).equals(variable) && constant.value != GALBuildHelper.galNone) {
				// If an item is located (not assigned to 0), it is not owned
				context.ItemOwnedBy.get(name).value = GALBuildHelper.createConstant(GALBuildHelper.galNone)
			}
			else if(context.ItemOwnedBy.containsKey(name) && context.ItemOwnedBy.get(name).equals(variable) && constant.value != GALBuildHelper.galNone) {
				// If an item is owned(not assigned to 0), it is not located
				context.ItemLocation.get(name).value = GALBuildHelper.createConstant(GALBuildHelper.galNone)
			}
			else if(context.AccessLocked.containsKey(name) && context.AccessLocked.get(name).equals(variable) && constant.value == 1) {
				// If an access is locked, then it is not open
				// assumption: any ccess with a locked variable has a open variable
				context.AccessOpen.get(name).value = GALBuildHelper.createConstant(GALBuildHelper.galFalse)
			}
			else if(context.AccessOpen.containsKey(name) && context.AccessOpen.get(name).equals(variable) && constant.value == 1 && context.AccessOpen.containsKey(name)) {
				// If an access is open, then it is not locked
				// warning : and access can be open, but not have a locked variable
				context.AccessOpen.get(name).value = GALBuildHelper.createConstant(GALBuildHelper.galFalse)
			}
			variable.value = constant
		]
			, context)
		// think of the dependend variables: if owner is fixed, then so is location. same for locked or open to true.
	} 
	
	def public void updateGalElement (Context context) {
		// definition of the building domains
		_self.createTypedefs(context)
		// transition assign_initial_values
		_self.initVars = GALBuildHelper.createTransition("assign_initial_values")
		context.gal.transitions.add(_self.initVars)
		_self.initVars.guard = GALBuildHelper.createBoolExprEqVarCst(context.init,GALBuildHelper.galFalse)//_self.allVariableAreZero(context)
		//First, initialize the cariables that are strictly assigned in the precondition, to lower the number of possible initialization
		_self.updateAssignedVars(context)
		// for each attacker, create a transition to initialize their position
		context.AttackerLocation.forEach[att, variable| 
			if(GALBuildHelper.isVarUninitialized(variable)) {
			 _self.createTranstionForInitVar(att + "_location", variable, context, _self.Zones)
			}
		]
		context.AlarmEnabled.forEach[alarm, variable| 
			if(GALBuildHelper.isVarUninitialized(variable)) {
				_self.createTranstionForInitVar(alarm + "_enabled", variable, context, _self.Bools)
			}
		]
		context.AlarmTriggered.forEach[alarm, variable| 
			if(GALBuildHelper.isVarUninitialized(variable)) {
				_self.createTranstionForInitVar(alarm + "_triggered", variable, context, _self.Bools)
			}
		]
		context.ItemLocation.forEach[item, variableLoc| 
			// First, get the owner variable corresponding to the same item
			val variableOwner = context.ItemOwnedBy.get(item)
			// opti: test if the vraiable are already initialized. We assume that either both or none of the two are initialized, so we test only one
			if(GALBuildHelper.isVarUninitialized(variableLoc)) {
				// Create the transition initializing a location for the item. labLoc is the label of this transition.
				_self.createTranstionForInitVar(item + "_location", variableLoc, context, _self.Zones)
				//additionally, a item location can be left uninitialized
				_self.createTranstionUninitialized(item + "_location", variableLoc, context)
				// Create the transition initializing a location for the item. labLoc is the label of this transition.
				_self.createTranstionForInitVar(item + "_owner", variableOwner, context, _self.Attackers)
				_self.createTranstionUninitialized(item + "_owner", variableOwner, context)
			}
			// Now add a test for the consistency: an item cannot be owned and located (if the variable is initialized, this is only useful for debug purpose)
			val inconsitent = GALBuildHelper.createBoolExprAnd(GALBuildHelper.createBoolExprDiffVarCst(variableLoc,GALBuildHelper.galNone),GALBuildHelper.createBoolExprDiffVarCst(variableOwner,GALBuildHelper.galNone))
			_self.initVars.actions.add(GALBuildHelper.createIfThen(inconsitent,GALBuildHelper.createAbort()))
		]
		context.AccessOpen.forEach[acc, variable| 
			if(GALBuildHelper.isVarUninitialized(variable)) {
				_self.createTranstionForInitVar(acc + "_open", variable, context, _self.Bools)
			}
		]
		// note that it is important that open is initialized before locked
		context.AccessLocked.forEach[acc, variable| 
			if(GALBuildHelper.isVarUninitialized(variable)) {
				_self.createTranstionForInitVar(acc + "_locked", variable, context, _self.Bools)
			}
			// Now add a test for the consistency: an door cannot be locked if it is open
			if(!context.AccessOpen.containsKey(acc)) throw new Exception("Assumption error: an access has a lock variable, but no open variable")
			val varOpen = context.AccessOpen.get(acc)
			val inconsitent = GALBuildHelper.createBoolExprAnd(GALBuildHelper.createBoolExprIsVarTrue(variable),GALBuildHelper.createBoolExprIsVarTrue(varOpen))
			_self.initVars.actions.add(GALBuildHelper.createIfThen(inconsitent,GALBuildHelper.createAbort()))
		]		
		// Add the test to see if the initialization satisfies the precondition of the goal. abort otherwise
		val isTheStateInit = _self.getInitStateExpression(context)
		_self.initVars.actions.add(GALBuildHelper.createIfThenElse(isTheStateInit,GALBuildHelper.createVarAssignConst(context.init,GALBuildHelper.galTrue),GALBuildHelper.createAbort()))
		// transition reach_goal
		_self.reachGoal = GALBuildHelper.createTransition("reach_goal")
		context.gal.transitions.add(_self.reachGoal)
		val finalStateExpr = _self.getFinalStateExpression(context)
		val initGood = GALBuildHelper.createBoolExprEqVarCst(context.init, GALBuildHelper.galTrue)
		_self.reachGoal.guard = GALBuildHelper.createBoolExprAnd(finalStateExpr,initGood)
		_self.reachGoal.actions.add(GALBuildHelper.createVarAssignConst(context.goalReached,GALBuildHelper.galTrue))
		
		// modify the var initial values according to precondition assignment 
		// TODO generalize to assignment defined in precondition too
		if(_self.defaultUsedInPre !== null) {
			val defaultInPre = _self.precondition.getApplicableAssignements(_self.defaultUsedInPre.defaultValueAssignments)
			defaultInPre.forEach[d |
				// find the variable for this assignment
				val variable = context.gal.variables.findFirst[variable | variable.name == d.target.name]
				if( variable !== null && d.assignedValue.isaConstant){
					variable.value = createConstant(d.assignedValue.getConstant(context))
					// remove the call in assign_initial_values
					val searchedAction = "assign_initial_values_"+(d.target.name.replace(".","_"))
					val action = _self.initVars.actions.filter(SelfCall).findFirst[ action | action.label.name == searchedAction]
					if(action !== null){
						_self.initVars.actions.remove(action)
					}
					// TODO remove the transition too
				}
			]
		}
	}
	
	def private void createTypedefs(Context context) {
		_self.Bools = GALBuildHelper.createTypeDefDeclaration("bools",0,1)
		context.gal.typedefs.add(_self.Bools)
		_self.Attackers = GALBuildHelper.createTypeDefDeclaration("attackers",1,numberOfAttackers)
		context.gal.typedefs.add(_self.Attackers)
		_self.Zones = GALBuildHelper.createTypeDefDeclaration("zones",1,numberOfZones)
		context.gal.typedefs.add(_self.Zones)
	}
	
	def static private BooleanExpression allVariableAreZero (Context context) {
		val seed = GALBuildHelper.createBoolExprEqVarCst(context.gal.variables.head,0)
		context.gal.variables.tail.fold(seed,[r,t| GALBuildHelper.createBoolExprAnd(r,GALBuildHelper.createBoolExprEqVarCst(t,0))])
	}
	
	def private BooleanExpression getInitStateExpression (Context context) {
		return _self.getPreconditionTestStateExpression(context)
	}
	
	def private BooleanExpression getFinalStateExpression (Context context) {
		return _self.getPostconditionTestStateExpression(context)
	}
	
	def private void createTranstionForInitVar(String transitionName, Variable variable, Context context, TypedefDeclaration domain) {
		val transition = GALBuildHelper.createTransition("assign_initial_values_" + transitionName)
		context.gal.transitions.add(transition)
		transition.guard = GALBuildHelper.boolTrue
		val param = GALBuildHelper.createParam(GalNamer.getParamName(variable.name), domain)
		transition.params.add(param)
		transition.actions.add(GALBuildHelper.createVarAssignParam(variable,param))
		val label = GALBuildHelper.createLabel("assign_initial_values_" + transitionName)
		transition.label = label
		// add a call instruction to the main assign_initial_values transition
		val call = GALBuildHelper.createCall(label)
		_self.initVars.actions.add(call)
	}
	
	def private void createTranstionUninitialized(String transitionName, Variable variable, Context context) {
		val transition = GALBuildHelper.createTransition("assign_initial_values_" + transitionName + "_none")
		context.gal.transitions.add(transition)
		transition.guard = GALBuildHelper.boolTrue
		transition.actions.add(GALBuildHelper.createVarAssignConst(variable,GALBuildHelper.galNone))
		transition.label = GALBuildHelper.createLabel("assign_initial_values_" + transitionName)
	}

}

