
package fr.irisa.atsyra.transfo.building.gal

import atsyragoal.AtsyraGoal
import fr.irisa.atsyra.building.BuildingModel
import fr.lip6.move.gal.GALTypeDeclaration
import fr.irisa.atsyra.transfo.building.gal.Context

import static extension fr.irisa.atsyra.transfo.building.gal.Goal2GAL_AtsyraGoalAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.BuildingModelAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.ZoneAspects.*
import static extension fr.irisa.atsyra.transfo.building.gal.AttackerAspects.*

class Building2GALForReachability extends AbstractBuilding2GAL {
	def GALTypeDeclaration transformToGAL(BuildingModel buildingModel, AtsyraGoal goal){
		//context initialization
		val Context context = new Context()
		context.gal.name = "Building"
		//initialization of the constants
		buildingModel.buildings.get(0).zones.get(0).initNumbers(buildingModel)
		buildingModel.buildings.get(0).attackers.get(0).initNumbers(buildingModel)
		//creation of the global variable
		goal.createGlobalVars(context)
		//creation of the GAL part from Building
		buildingModel.createGalElement(context)
		buildingModel.linkGalElement(context)
		//creation of the GAL part from AtsyraGoal
		goal.updateGalElement(context)		
		return context.gal
	}

}