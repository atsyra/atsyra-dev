package fr.irisa.atsyra.transfo.building.gal

import fr.lip6.move.gal.Variable
import fr.lip6.move.gal.GalFactory
import fr.lip6.move.gal.Constant
import fr.lip6.move.gal.Transition
import fr.lip6.move.gal.BooleanExpression
import fr.lip6.move.gal.Statement
import fr.lip6.move.gal.ComparisonOperators
import fr.lip6.move.gal.VariableReference
import fr.lip6.move.gal.TypedefDeclaration
import fr.lip6.move.gal.Parameter
import fr.lip6.move.gal.ParamRef
import fr.lip6.move.gal.Label
import java.util.List
import fr.lip6.move.gal.Abort

final class GALBuildHelper {
	
	// create a variable
	def static Variable createVariable (String name, int value){
		var variable = GalFactory.eINSTANCE.createVariable()
		variable.setName(name)
		variable.setValue(createConstant(value))
		return variable
	}
	
	// create a hotbit variable (for enumerated types)
	def static Variable createHotbitVariable (String name, int value){
		var variable = GalFactory.eINSTANCE.createVariable()
		variable.setName(name)
		variable.setValue(createConstant(value))
		variable.hotbit = true
		return variable
	}
	
	public static int galTrue = 1
	public static int galFalse = 0
	public static int galUninit = -1
	public static int galNone = 0
	
	def static BooleanExpression boolTrue() {
		GalFactory.eINSTANCE.createTrue()
	}
	
	def static BooleanExpression boolFalse() {
		GalFactory.eINSTANCE.createFalse()
	}
	
	def static Abort createAbort() {
		GalFactory.eINSTANCE.createAbort()
	}
	
	// create a GAL constant
	def static Constant createConstant(int value) {
		val c = GalFactory.eINSTANCE.createConstant()
		c.setValue(value)
		return c
	}
	
	// create a GAL transition
	def static Transition createTransition(String name) {
		var transition = GalFactory.eINSTANCE.createTransition()
		transition.name = name
		return transition
	}
	
	// create boolean expression : variable == constant
	def static BooleanExpression createBoolExprEqVarCst(Variable variable, int constant) {
		val eq = GalFactory.eINSTANCE.createComparison()
		eq.setLeft(createVarRef(variable))
		eq.setRight(createConstant(constant))
		eq.setOperator(ComparisonOperators.EQ)
		return eq
	}
	
	// create boolean expression : variable != constant
	def static BooleanExpression createBoolExprDiffVarCst(Variable variable, int constant) {
		val eq = GalFactory.eINSTANCE.createComparison()
		eq.setLeft(createVarRef(variable))
		eq.setRight(createConstant(constant))
		eq.setOperator(ComparisonOperators.NE)
		return eq
	}
	
	// create boolean expression : variable1 == variable2
	def static BooleanExpression createBoolExprEqVarVar(Variable variable1, Variable variable2) {
		val eq = GalFactory.eINSTANCE.createComparison()
		eq.setLeft(createVarRef(variable1))
		eq.setRight(createVarRef(variable2))
		eq.setOperator(ComparisonOperators.EQ)
		return eq
	}
	
	// create boolean expression : constant1 == constant2
	def static BooleanExpression createBoolExprEqCstCst(int constant1, int constant2) {
		val eq = GalFactory.eINSTANCE.createComparison()
		eq.setLeft(createConstant(constant1))
		eq.setRight(createConstant(constant2))
		eq.setOperator(ComparisonOperators.EQ)
		return eq
	}
	
	// create boolean expression : param == constant
	def static BooleanExpression createBoolExprEqPrmCst(Parameter param, int constant) {
		val eq = GalFactory.eINSTANCE.createComparison()
		eq.setLeft(createParamRef(param))
		eq.setRight(createConstant(constant))
		eq.setOperator(ComparisonOperators.EQ)
		return eq
	}
	
	// create boolean expression : variable == 1 
	def static BooleanExpression createBoolExprIsVarTrue(Variable variable) {
		return createBoolExprEqVarCst(variable,galTrue)
	}
	
	// create boolean expression : variable == 0
	def static BooleanExpression createBoolExprIsVarFalse(Variable variable) {
		return createBoolExprEqVarCst(variable,galFalse)
	}
	
	// create boolean expression : left & right
	def static BooleanExpression createBoolExprAnd(BooleanExpression left, BooleanExpression right) {
		val exp = GalFactory.eINSTANCE.createAnd()
		exp.setLeft(left)
		exp.setRight(right)
		return exp
	}
	
	// create boolean expression : left || right
	def static BooleanExpression createBoolExprOr(BooleanExpression left, BooleanExpression right) {
		val exp = GalFactory.eINSTANCE.createOr()
		exp.setLeft(left)
		exp.setRight(right)
		return exp
	}
	
	// create boolean expression : ~ expr
	def static BooleanExpression createBoolExprNot(BooleanExpression expr) {
		val exp = GalFactory.eINSTANCE.createNot()
		exp.value = expr
		return exp
	}
	
	// create statement : variable = constant
	def static Statement createVarAssignConst(Variable variable, int constant) {
		val ass = GalFactory.eINSTANCE.createAssignment()
		ass.setLeft(createVarRef(variable))
		ass.setRight(createConstant(constant))
		return ass
	}
	
	// create statement : variable1 = variable2
	def static Statement createVarAssignVar(Variable variable1, Variable variable2) {
		val ass = GalFactory.eINSTANCE.createAssignment()
		ass.setLeft(createVarRef(variable1))
		ass.setRight(createVarRef(variable2))
		return ass
	}
	
	// create statement : variable = param
	def static Statement createVarAssignParam(Variable variable, Parameter param) {
		val ass = GalFactory.eINSTANCE.createAssignment()
		ass.setLeft(createVarRef(variable))
		ass.setRight(createParamRef(param))
		return ass
	}
	
	def static ParamRef createParamRef(Parameter parameter) {
		val ref = GalFactory.eINSTANCE.createParamRef()
		ref.refParam = parameter
		return ref
	}
	
	def static VariableReference createVarRef(Variable variable) {
		val ref = GalFactory.eINSTANCE.createVariableReference()
		ref.ref = variable
		return ref
	}
	
	// create the statement If(cond) {thenSt};
	def static Statement createIfThen(BooleanExpression cond, Statement thenSt) {
		val ite = GalFactory.eINSTANCE.createIte()
		ite.cond = cond
		ite.ifTrue.add(thenSt)
		return ite
	}
	
	// create the statement If(cond) {thenSt} else {elseSt};
	def static Statement createIfThenElse(BooleanExpression cond, Statement thenSt, Statement elseSt) {
		val ite = GalFactory.eINSTANCE.createIte()
		ite.cond = cond
		ite.ifTrue.add(thenSt)
		ite.ifFalse.add(elseSt)
		return ite
	}
	
	// create the statement self."called";
	def static Statement createCall(Label called) {
		val call = GalFactory.eINSTANCE.createSelfCall()
		call.label = called
		return call
	}
	
	// create range: typedef name = min..max
	def static TypedefDeclaration createTypeDefDeclaration(String name, int min, int max) {
		val typedef = GalFactory.eINSTANCE.createTypedefDeclaration()
		typedef.name = name
		typedef.min = createConstant(min)
		typedef.max = createConstant(max)
		return typedef
	}
	
	def static Parameter createParam(String name, TypedefDeclaration type) {
		val param = GalFactory.eINSTANCE.createParameter()
		param.name = '$' + name
		param.type = type
		return param
	}
	
	def static Label createLabel(String name) {
		val lab = GalFactory.eINSTANCE.createLabel()
		lab.name = name
		return lab
	}
	
	def static createBigOr(List<BooleanExpression> expressions) {
		expressions.reduce([r,t| GALBuildHelper.createBoolExprOr(r,t)])
	}
	
	def static createBigAnd(List<BooleanExpression> expressions) {
		expressions.reduce([r,t| GALBuildHelper.createBoolExprAnd(r,t)])
	}
	
	def static createSystemGuard(BooleanExpression guard, Context context) {
		if(context.init === null) {
			guard
		} else {
			if(context.goalReached === null){
				createBoolExprAnd(GALBuildHelper.createBoolExprIsVarTrue(context.init),guard)
			} else {
				createBoolExprAnd(GALBuildHelper.createBoolExprIsVarTrue(context.init),createBoolExprAnd(GALBuildHelper.createBoolExprIsVarFalse(context.goalReached),guard))
			}
		}
	}
	
	def static isVarUninitialized(Variable variable) {
		return variable.value instanceof Constant && (variable.value as Constant).value == galUninit
	}
}