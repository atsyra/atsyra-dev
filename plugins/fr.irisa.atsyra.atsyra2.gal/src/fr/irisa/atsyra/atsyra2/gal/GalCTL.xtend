package fr.irisa.atsyra.atsyra2.gal

import fr.irisa.atsyra.gal.process.InterruptibleProcessController
import fr.irisa.atsyra.gal.process.InterruptibleProcessController.KilledByUserException
import fr.irisa.atsyra.resultstore.Result
import fr.irisa.atsyra.resultstore.ResultValue
import fr.lip6.move.gal.itstools.preference.PreferenceConstants
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.util.ArrayList
import java.util.Optional
import java.util.concurrent.TimeoutException
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Platform

class GalCTL {
	public long timeout = 60
	protected IFile gal_file
	protected IFile formula_file
	public long executionDuration = 0
	public Result resultHandler
	
	new(IFile galFile, IFile formulaFile, Result resultHandler) {
		super()
		gal_file = galFile
		formula_file = formulaFile
		timeout = getDefaultTimeout()
		this.resultHandler = resultHandler
	}
	
	def ResultValue computeCTL(IProgressMonitor monitor) {
		if (gal_file !== null) {
			try {
				return runCTLmc(monitor);			
			} catch (Exception e) {
				Activator.error(
						"Compute CTL raised an exception "
								+ e.getMessage(), e);	
					return ResultValue.ABORTED
			}
		}
	}
	
	def ResultValue runCTLmc(IProgressMonitor monitor) {
		
		if (getITSToolPath().nullOrEmpty){
			resultHandler.value = ResultValue.ABORTED
			return ResultValue.ABORTED;
		}
		val cmd = buildCommandArguments();
		val errorOutput = new ByteArrayOutputStream();
		val stdOutput = new ByteArrayOutputStream();

		//Activator.debug("Timeout="	+ timeout);
		
		var long startTime = System.currentTimeMillis
		val controller = new InterruptibleProcessController(
				timeout * 1000, cmd.toArray(newArrayOfSize(cmd.size())), null,
				new File(gal_file.getParent().getLocationURI()), monitor, "ITSCTL "+gal_file.name+" ", true); // workdir
		controller.forwardErrorOutput(errorOutput);
		controller.forwardOutput(stdOutput);
		var int exitCode
		try {
			exitCode = controller.execute();
			val stdOutString = stdOutput.toString();
			
			//Activator.debug(stdOutString);
			if (exitCode != 0) {
				if (errorOutput.size() > 0) {	
					Activator.error("CTL model-checking reported some errors :"+ errorOutput.toString);
					resultHandler.value = ResultValue.ABORTED
					return ResultValue.ABORTED;
				}
			} else {
				
				
				
				//resSet = resourceSetProvider.get(gtb_file.getProject());
				//resSet = new ResourceSetImpl();
				
				//updateActionDefinitionFromReachabilityTrace(stdOutput.toString());
				//GalOutput2AttackScenario.buildScenarioModelFromReachabilityTrace(stdOutput.toString(), scenario_file, lla_building_action_file, resSet);
				processGALOutpout(stdOutString, false);
			}
		} catch (IOException e) {
			//Activator.debug(stdOutput.toString());
			Activator.error(
							"Unexpected exception while running CTL model-checking."
									+ errorOutput, e);
			resultHandler.value = ResultValue.ABORTED
			return ResultValue.ABORTED;
		} catch (TimeoutException e) {

			val stdOutString = stdOutput.toString();
			//if (errorOutput.size() > 0) {
			//	Activator.error(errorOutput.toString(), null);
			//}
			
			//Activator.debug(stdOutString);
			Activator.error(
							"CTL model-checking did not finish in a timely way."
									+ errorOutput, e);
			processGALOutpout(stdOutString, true);
		} catch (KilledByUserException e) {
			val stdOutString = stdOutput.toString();
			//Activator.error(errorOutput.toString(), null);
			processGALOutpout(stdOutString, true);
			//Activator.debug(stdOutString);
			Activator.error("CTL model-checking interrupted by user.", e);	
			resultHandler.value = ResultValue.ABORTED					
			return ResultValue.ABORTED;
		} finally {
			executionDuration = System.currentTimeMillis - startTime
			resultHandler.duration = executionDuration
		}
	}
	
	def ResultValue processGALOutpout(String stdOutString, boolean timeoutTrigerred){
		resultHandler.log = stdOutString
		//Activator.debug(stdOutString)
		if(stdOutString.contains("Formula is TRUE")){
			Activator.important("Property is true!")
			val example = GetCounterExampleForOutput(stdOutString)
				if(example.isPresent) 
					if(example.get.empty) {
						resultHandler.details = "An example: the empty path"
						Activator.important("An example: the empty path")
					} else {
						resultHandler.details = "An example: " + example.get
						Activator.important("An example: " + example.get)
					}
				else Activator.important("Unable to compute an example!")
			resultHandler.value = ResultValue.TRUE
			return ResultValue.TRUE;
		}
		else{
			if(timeoutTrigerred){
				Activator.error("Unable to compute CTL property : timeout.")
				resultHandler.value = ResultValue.TIMEOUT
				return ResultValue.TIMEOUT;
			} else {
				Activator.important("Property is false.")
				val counterexample = GetCounterExampleForOutput(stdOutString)
				if(counterexample.isPresent) 
					if(counterexample.get.empty){
						resultHandler.details = "An example: the empty path"
						Activator.important("A counterexample: the empty path")
						
					} else {
						resultHandler.details = "A counterexample: " + counterexample.get
						Activator.important("A counterexample: " + counterexample.get)	
					}
				else Activator.important("Unable to compute a counterexample!")
				resultHandler.value = ResultValue.FALSE
				return ResultValue.FALSE;
			}
		}
	}
	
	def Optional<String> GetCounterExampleForOutput(String stdOutString) {
		var index = 0;
		// if there is no match, then remember it. It will be used to decide in there is an empty counterexampelo r not.
		val someEmptyPath = stdOutString.indexOf("Shortest path is empty transition sequence") != -1
		// otherwise, there is one; we iterate only onver 
		var noinfinite = 50; // just to be sure
		var list = new ArrayList<String[]>()
		while(index != -1 && noinfinite >0) {
			noinfinite--
			index = stdOutString.indexOf("This shortest transition sequence", index +10)
			//Activator.debug(Integer.toString(index))
			if(index != -1) {
				val localindex = stdOutString.indexOf(":",index) + 2;
				//Activator.debug(stdOutString.substring(localindex,stdOutString.indexOf('\n',localindex+5)))
				//stdOutString.substring(localindex,stdOutString.indexOf('\n',localindex+5)).split(", ").forEach[s | Activator.debug(s)]
				list.add(stdOutString.substring(localindex,stdOutString.indexOf('\n',localindex+5)).split(", "))
			}
		}
		if(list.empty && !someEmptyPath) return Optional.empty
		else {
			list.reverse
			return Optional.of('''«FOR sub : list SEPARATOR ', '»«FOR s : sub SEPARATOR ', '»«s»«ENDFOR»«ENDFOR»''')
		}
	}
	
	def String getITSToolPath() {
		val toolFullPath = Platform.preferencesService.getString("fr.lip6.move.gal.itstools.preference", PreferenceConstants.ITSCTL_EXE, "", null);
		if (toolFullPath.nullOrEmpty) {
			Activator
					.error("Please specify the absolute path to the tool in the preferences page Coloane->ITS Path.");
			return null;
		}
		if (!new File(toolFullPath).isFile()) {
			Activator.error("Could not find ITS tool at \""+ toolFullPath + "\"");
			return null;
		}
		return toolFullPath;
	}
	
	def getDefaultTimeout() {
		return Platform.preferencesService.getInt("fr.irisa.atsyra.ide.ui", "maxExecTimePreference", 60, null)
	}
	
	def buildCommandArguments() {
		// should build something like :
		// its-ctl -i building.gal -t GAL -formula form.ctl

		val cmd = new ArrayList<String>();
		cmd.add(getITSToolPath());
		cmd.add("--quiet");

		cmd.add("-i");
		cmd.add(gal_file.getName());
		cmd.add("-t");
		cmd.add("GAL");
		cmd.add("-ctl");
		cmd.add(formula_file.location.toOSString);
		cmd.add("--witness");
		return cmd;
	}
}
