/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyra2.gal;

import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystemManager;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}
	
	public static void debug(String msg){
		getMessagingSystem().debug(msg, "ATSyRA");
	}
	public static void error(String msg){
		getMessagingSystem().error(msg, "ATSyRA");
	}
	public static void error(String msg,  Throwable e){
		getMessagingSystem().error(msg, "ATSyRA", e);
	}
	public static void info(String str) {
		getMessagingSystem().info(str, "ATSyRA");
	}
	public static void important(String str) {
		getMessagingSystem().important(str, "ATSyRA");
	}
	
	public static final String msgGroup = "fr.irisa.atsyra";
	protected static MessagingSystem messagingSystem = null;
	public static MessagingSystem getMessagingSystem() {
		if(messagingSystem == null) {
			MessagingSystemManager msm = new MessagingSystemManager();
			messagingSystem = msm.createBestPlatformMessagingSystem(msgGroup, "ATSyRA");
		}
		return messagingSystem;
	}

}
