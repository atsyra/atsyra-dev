package fr.irisa.atsyra.atsyra2.gal

import fr.irisa.atsyra.resultstore.Result
import fr.irisa.atsyra.resultstore.ResultValue
import java.util.ArrayList
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Status
import java.io.File

class GalReach extends fr.irisa.atsyra.gal.AbstractGalReachability {

	protected Result resultHandler
	
	new(IFile galFile, Result resultHandler) {
		super(new File(galFile.location.toOSString))
		this.resultHandler = resultHandler
	}
	
	def computeReachability(IProgressMonitor monitor) {
		var long startTime
		if (gal_file !== null) {
			try {
				startTime = System.currentTimeMillis
				runReachability(monitor);
				return Status.OK_STATUS;						
			} catch (Exception e) {
				Activator.error(
						"Compute reachability raised an exception "
								+ e.getMessage(), e);	
			} finally {
				resultHandler.duration = System.currentTimeMillis - startTime
			}
		}
	}
	
	override processGALOutpout(String stdOutString, boolean timeoutTrigerred){
		//Activator.debug(stdOutString)
		resultHandler.log = stdOutString
		if(stdOutString.contains("Reachability property goal_reached==1 is true.")){
			Activator.important(resultHandler.name+" is reachable!")
			resultHandler.value = ResultValue.TRUE
		}
		else{
			if(timeoutTrigerred){
				Activator.error(resultHandler.name+" Unable to compute reachability : timeout.")
				resultHandler.value = ResultValue.TIMEOUT
			} else {
				Activator.important(resultHandler.name+"  is not reachable.")
				resultHandler.value = ResultValue.FALSE
			}
		}
	}	
	
	override buildCommandArguments() {
		// should build something like :
		// its-reach -i building.gal -t GAL -reachable 'goalReached==1'
		// --nowitness

		val cmd = new ArrayList<String>();
		cmd.add(getITSToolPath());
		cmd.add("--quiet");

		cmd.add("-i");
		cmd.add(gal_file.getName());
		cmd.add("-t");
		cmd.add("GAL");
		cmd.add("-reachable");
		//cmd.add("objectif_atteint==1");
		cmd.add("goal_reached==1");
		cmd.add("--nowitness");
		// Handle order input
		/*
		 * if (buildOrderFile()) { cmd.add("--load-order");
		 * cmd.add(getOrderFileName()); } if
		 * (getParameters().getBoolParameterValue(DDDSDD_PARAMETER)) {
		 * cmd.add("--sdd"); } else { cmd.add("--ddd"); }
		 * 
		 * String strat =
		 * getParameters().getEnumParameterValue(SCALAR_PARAMETER); if
		 * (strat.equals(DEPTH2)) { cmd.add("-ssD2"); } else if
		 * (strat.equals(DEPTHREC)) { cmd.add("-ssDR"); } else if
		 * (strat.equals(DEPTHSHALLOW)) { cmd.add("-ssDS"); }
		 * cmd.add(getParameters().getParameterValue(BLOCK_SIZE_PARAMETER));
		 * 
		 * cmd.add("--gc-threshold");
		 * cmd.add(getParameters().getParameterValue(GCTHRESHOLD));
		 */
		return cmd;
	}
}
