/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyragoal.xtext.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.irisa.atsyra.atsyragoal.xtext.services.AtsyRAGoalGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalAtsyRAGoalParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'or'", "'OR'", "'and'", "'AND'", "'not'", "'NOT'", "'true'", "'false'", "'SAND'", "'AtsyraGoalModel'", "'{'", "'}'", "'defaults'", "'types'", "'features'", "'atsyragoals'", "'trees'", "'import'", "'('", "')'", "'='", "'const'", "'Goal'", "'pre'", "':'", "'with'", "'post'", "','", "'['", "']'", "'InternalType'", "'SystemType'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalAtsyRAGoalParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalAtsyRAGoalParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalAtsyRAGoalParser.tokenNames; }
    public String getGrammarFileName() { return "InternalAtsyRAGoal.g"; }


    	private AtsyRAGoalGrammarAccess grammarAccess;

    	public void setGrammarAccess(AtsyRAGoalGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleAtsyraGoalModel"
    // InternalAtsyRAGoal.g:54:1: entryRuleAtsyraGoalModel : ruleAtsyraGoalModel EOF ;
    public final void entryRuleAtsyraGoalModel() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:55:1: ( ruleAtsyraGoalModel EOF )
            // InternalAtsyRAGoal.g:56:1: ruleAtsyraGoalModel EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAtsyraGoalModel();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtsyraGoalModel"


    // $ANTLR start "ruleAtsyraGoalModel"
    // InternalAtsyRAGoal.g:63:1: ruleAtsyraGoalModel : ( ( rule__AtsyraGoalModel__Group__0 ) ) ;
    public final void ruleAtsyraGoalModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:67:2: ( ( ( rule__AtsyraGoalModel__Group__0 ) ) )
            // InternalAtsyRAGoal.g:68:2: ( ( rule__AtsyraGoalModel__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:68:2: ( ( rule__AtsyraGoalModel__Group__0 ) )
            // InternalAtsyRAGoal.g:69:3: ( rule__AtsyraGoalModel__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:70:3: ( rule__AtsyraGoalModel__Group__0 )
            // InternalAtsyRAGoal.g:70:4: rule__AtsyraGoalModel__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtsyraGoalModel"


    // $ANTLR start "entryRuleImport"
    // InternalAtsyRAGoal.g:79:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:80:1: ( ruleImport EOF )
            // InternalAtsyRAGoal.g:81:1: ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalAtsyRAGoal.g:88:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:92:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalAtsyRAGoal.g:93:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:93:2: ( ( rule__Import__Group__0 ) )
            // InternalAtsyRAGoal.g:94:3: ( rule__Import__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:95:3: ( rule__Import__Group__0 )
            // InternalAtsyRAGoal.g:95:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleType"
    // InternalAtsyRAGoal.g:104:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:105:1: ( ruleType EOF )
            // InternalAtsyRAGoal.g:106:1: ruleType EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypeRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalAtsyRAGoal.g:113:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:117:2: ( ( ( rule__Type__Alternatives ) ) )
            // InternalAtsyRAGoal.g:118:2: ( ( rule__Type__Alternatives ) )
            {
            // InternalAtsyRAGoal.g:118:2: ( ( rule__Type__Alternatives ) )
            // InternalAtsyRAGoal.g:119:3: ( rule__Type__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypeAccess().getAlternatives()); 
            }
            // InternalAtsyRAGoal.g:120:3: ( rule__Type__Alternatives )
            // InternalAtsyRAGoal.g:120:4: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Type__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypeAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleGoalCondition"
    // InternalAtsyRAGoal.g:129:1: entryRuleGoalCondition : ruleGoalCondition EOF ;
    public final void entryRuleGoalCondition() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:130:1: ( ruleGoalCondition EOF )
            // InternalAtsyRAGoal.g:131:1: ruleGoalCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoalConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGoalCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoalConditionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGoalCondition"


    // $ANTLR start "ruleGoalCondition"
    // InternalAtsyRAGoal.g:138:1: ruleGoalCondition : ( ruleOrCondition ) ;
    public final void ruleGoalCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:142:2: ( ( ruleOrCondition ) )
            // InternalAtsyRAGoal.g:143:2: ( ruleOrCondition )
            {
            // InternalAtsyRAGoal.g:143:2: ( ruleOrCondition )
            // InternalAtsyRAGoal.g:144:3: ruleOrCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoalConditionAccess().getOrConditionParserRuleCall()); 
            }
            pushFollow(FOLLOW_2);
            ruleOrCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoalConditionAccess().getOrConditionParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGoalCondition"


    // $ANTLR start "entryRuleOrCondition"
    // InternalAtsyRAGoal.g:154:1: entryRuleOrCondition : ruleOrCondition EOF ;
    public final void entryRuleOrCondition() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:155:1: ( ruleOrCondition EOF )
            // InternalAtsyRAGoal.g:156:1: ruleOrCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleOrCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrConditionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrCondition"


    // $ANTLR start "ruleOrCondition"
    // InternalAtsyRAGoal.g:163:1: ruleOrCondition : ( ( rule__OrCondition__Group__0 ) ) ;
    public final void ruleOrCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:167:2: ( ( ( rule__OrCondition__Group__0 ) ) )
            // InternalAtsyRAGoal.g:168:2: ( ( rule__OrCondition__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:168:2: ( ( rule__OrCondition__Group__0 ) )
            // InternalAtsyRAGoal.g:169:3: ( rule__OrCondition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrConditionAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:170:3: ( rule__OrCondition__Group__0 )
            // InternalAtsyRAGoal.g:170:4: rule__OrCondition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OrCondition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrConditionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrCondition"


    // $ANTLR start "entryRuleAndCondition"
    // InternalAtsyRAGoal.g:179:1: entryRuleAndCondition : ruleAndCondition EOF ;
    public final void entryRuleAndCondition() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:180:1: ( ruleAndCondition EOF )
            // InternalAtsyRAGoal.g:181:1: ruleAndCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAndCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndConditionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAndCondition"


    // $ANTLR start "ruleAndCondition"
    // InternalAtsyRAGoal.g:188:1: ruleAndCondition : ( ( rule__AndCondition__Group__0 ) ) ;
    public final void ruleAndCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:192:2: ( ( ( rule__AndCondition__Group__0 ) ) )
            // InternalAtsyRAGoal.g:193:2: ( ( rule__AndCondition__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:193:2: ( ( rule__AndCondition__Group__0 ) )
            // InternalAtsyRAGoal.g:194:3: ( rule__AndCondition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndConditionAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:195:3: ( rule__AndCondition__Group__0 )
            // InternalAtsyRAGoal.g:195:4: rule__AndCondition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AndCondition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndConditionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAndCondition"


    // $ANTLR start "entryRuleTerminalCondition"
    // InternalAtsyRAGoal.g:204:1: entryRuleTerminalCondition : ruleTerminalCondition EOF ;
    public final void entryRuleTerminalCondition() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:205:1: ( ruleTerminalCondition EOF )
            // InternalAtsyRAGoal.g:206:1: ruleTerminalCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleTerminalCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalConditionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTerminalCondition"


    // $ANTLR start "ruleTerminalCondition"
    // InternalAtsyRAGoal.g:213:1: ruleTerminalCondition : ( ( rule__TerminalCondition__Alternatives ) ) ;
    public final void ruleTerminalCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:217:2: ( ( ( rule__TerminalCondition__Alternatives ) ) )
            // InternalAtsyRAGoal.g:218:2: ( ( rule__TerminalCondition__Alternatives ) )
            {
            // InternalAtsyRAGoal.g:218:2: ( ( rule__TerminalCondition__Alternatives ) )
            // InternalAtsyRAGoal.g:219:3: ( rule__TerminalCondition__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalConditionAccess().getAlternatives()); 
            }
            // InternalAtsyRAGoal.g:220:3: ( rule__TerminalCondition__Alternatives )
            // InternalAtsyRAGoal.g:220:4: rule__TerminalCondition__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TerminalCondition__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalConditionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTerminalCondition"


    // $ANTLR start "entryRuleEqualOrBooleanCond"
    // InternalAtsyRAGoal.g:229:1: entryRuleEqualOrBooleanCond : ruleEqualOrBooleanCond EOF ;
    public final void entryRuleEqualOrBooleanCond() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:230:1: ( ruleEqualOrBooleanCond EOF )
            // InternalAtsyRAGoal.g:231:1: ruleEqualOrBooleanCond EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleEqualOrBooleanCond();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEqualOrBooleanCond"


    // $ANTLR start "ruleEqualOrBooleanCond"
    // InternalAtsyRAGoal.g:238:1: ruleEqualOrBooleanCond : ( ( rule__EqualOrBooleanCond__Alternatives ) ) ;
    public final void ruleEqualOrBooleanCond() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:242:2: ( ( ( rule__EqualOrBooleanCond__Alternatives ) ) )
            // InternalAtsyRAGoal.g:243:2: ( ( rule__EqualOrBooleanCond__Alternatives ) )
            {
            // InternalAtsyRAGoal.g:243:2: ( ( rule__EqualOrBooleanCond__Alternatives ) )
            // InternalAtsyRAGoal.g:244:3: ( rule__EqualOrBooleanCond__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getAlternatives()); 
            }
            // InternalAtsyRAGoal.g:245:3: ( rule__EqualOrBooleanCond__Alternatives )
            // InternalAtsyRAGoal.g:245:4: rule__EqualOrBooleanCond__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEqualOrBooleanCond"


    // $ANTLR start "entryRuleTypedElementDecl"
    // InternalAtsyRAGoal.g:254:1: entryRuleTypedElementDecl : ruleTypedElementDecl EOF ;
    public final void entryRuleTypedElementDecl() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:255:1: ( ruleTypedElementDecl EOF )
            // InternalAtsyRAGoal.g:256:1: ruleTypedElementDecl EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedElementDeclRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleTypedElementDecl();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedElementDeclRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypedElementDecl"


    // $ANTLR start "ruleTypedElementDecl"
    // InternalAtsyRAGoal.g:263:1: ruleTypedElementDecl : ( ( rule__TypedElementDecl__Alternatives ) ) ;
    public final void ruleTypedElementDecl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:267:2: ( ( ( rule__TypedElementDecl__Alternatives ) ) )
            // InternalAtsyRAGoal.g:268:2: ( ( rule__TypedElementDecl__Alternatives ) )
            {
            // InternalAtsyRAGoal.g:268:2: ( ( rule__TypedElementDecl__Alternatives ) )
            // InternalAtsyRAGoal.g:269:3: ( rule__TypedElementDecl__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedElementDeclAccess().getAlternatives()); 
            }
            // InternalAtsyRAGoal.g:270:3: ( rule__TypedElementDecl__Alternatives )
            // InternalAtsyRAGoal.g:270:4: rule__TypedElementDecl__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TypedElementDecl__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedElementDeclAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypedElementDecl"


    // $ANTLR start "entryRuleEString"
    // InternalAtsyRAGoal.g:279:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:280:1: ( ruleEString EOF )
            // InternalAtsyRAGoal.g:281:1: ruleEString EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEStringRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEStringRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalAtsyRAGoal.g:288:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:292:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalAtsyRAGoal.g:293:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalAtsyRAGoal.g:293:2: ( ( rule__EString__Alternatives ) )
            // InternalAtsyRAGoal.g:294:3: ( rule__EString__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEStringAccess().getAlternatives()); 
            }
            // InternalAtsyRAGoal.g:295:3: ( rule__EString__Alternatives )
            // InternalAtsyRAGoal.g:295:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEStringAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleTypedElementString"
    // InternalAtsyRAGoal.g:304:1: entryRuleTypedElementString : ruleTypedElementString EOF ;
    public final void entryRuleTypedElementString() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:305:1: ( ruleTypedElementString EOF )
            // InternalAtsyRAGoal.g:306:1: ruleTypedElementString EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedElementStringRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleTypedElementString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedElementStringRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypedElementString"


    // $ANTLR start "ruleTypedElementString"
    // InternalAtsyRAGoal.g:313:1: ruleTypedElementString : ( ( rule__TypedElementString__Alternatives ) ) ;
    public final void ruleTypedElementString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:317:2: ( ( ( rule__TypedElementString__Alternatives ) ) )
            // InternalAtsyRAGoal.g:318:2: ( ( rule__TypedElementString__Alternatives ) )
            {
            // InternalAtsyRAGoal.g:318:2: ( ( rule__TypedElementString__Alternatives ) )
            // InternalAtsyRAGoal.g:319:3: ( rule__TypedElementString__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedElementStringAccess().getAlternatives()); 
            }
            // InternalAtsyRAGoal.g:320:3: ( rule__TypedElementString__Alternatives )
            // InternalAtsyRAGoal.g:320:4: rule__TypedElementString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TypedElementString__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedElementStringAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypedElementString"


    // $ANTLR start "entryRuleAtsyraGoal"
    // InternalAtsyRAGoal.g:329:1: entryRuleAtsyraGoal : ruleAtsyraGoal EOF ;
    public final void entryRuleAtsyraGoal() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:330:1: ( ruleAtsyraGoal EOF )
            // InternalAtsyRAGoal.g:331:1: ruleAtsyraGoal EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAtsyraGoal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtsyraGoal"


    // $ANTLR start "ruleAtsyraGoal"
    // InternalAtsyRAGoal.g:338:1: ruleAtsyraGoal : ( ( rule__AtsyraGoal__UnorderedGroup ) ) ;
    public final void ruleAtsyraGoal() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:342:2: ( ( ( rule__AtsyraGoal__UnorderedGroup ) ) )
            // InternalAtsyRAGoal.g:343:2: ( ( rule__AtsyraGoal__UnorderedGroup ) )
            {
            // InternalAtsyRAGoal.g:343:2: ( ( rule__AtsyraGoal__UnorderedGroup ) )
            // InternalAtsyRAGoal.g:344:3: ( rule__AtsyraGoal__UnorderedGroup )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup()); 
            }
            // InternalAtsyRAGoal.g:345:3: ( rule__AtsyraGoal__UnorderedGroup )
            // InternalAtsyRAGoal.g:345:4: rule__AtsyraGoal__UnorderedGroup
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__UnorderedGroup();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtsyraGoal"


    // $ANTLR start "entryRuleDefaultValues"
    // InternalAtsyRAGoal.g:354:1: entryRuleDefaultValues : ruleDefaultValues EOF ;
    public final void entryRuleDefaultValues() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:355:1: ( ruleDefaultValues EOF )
            // InternalAtsyRAGoal.g:356:1: ruleDefaultValues EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleDefaultValues();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDefaultValues"


    // $ANTLR start "ruleDefaultValues"
    // InternalAtsyRAGoal.g:363:1: ruleDefaultValues : ( ( rule__DefaultValues__Group__0 ) ) ;
    public final void ruleDefaultValues() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:367:2: ( ( ( rule__DefaultValues__Group__0 ) ) )
            // InternalAtsyRAGoal.g:368:2: ( ( rule__DefaultValues__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:368:2: ( ( rule__DefaultValues__Group__0 ) )
            // InternalAtsyRAGoal.g:369:3: ( rule__DefaultValues__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:370:3: ( rule__DefaultValues__Group__0 )
            // InternalAtsyRAGoal.g:370:4: rule__DefaultValues__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDefaultValues"


    // $ANTLR start "entryRuleDefaultValueAssignments"
    // InternalAtsyRAGoal.g:379:1: entryRuleDefaultValueAssignments : ruleDefaultValueAssignments EOF ;
    public final void entryRuleDefaultValueAssignments() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:380:1: ( ruleDefaultValueAssignments EOF )
            // InternalAtsyRAGoal.g:381:1: ruleDefaultValueAssignments EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleDefaultValueAssignments();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDefaultValueAssignments"


    // $ANTLR start "ruleDefaultValueAssignments"
    // InternalAtsyRAGoal.g:388:1: ruleDefaultValueAssignments : ( ( rule__DefaultValueAssignments__Group__0 ) ) ;
    public final void ruleDefaultValueAssignments() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:392:2: ( ( ( rule__DefaultValueAssignments__Group__0 ) ) )
            // InternalAtsyRAGoal.g:393:2: ( ( rule__DefaultValueAssignments__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:393:2: ( ( rule__DefaultValueAssignments__Group__0 ) )
            // InternalAtsyRAGoal.g:394:3: ( rule__DefaultValueAssignments__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:395:3: ( rule__DefaultValueAssignments__Group__0 )
            // InternalAtsyRAGoal.g:395:4: rule__DefaultValueAssignments__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValueAssignments__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDefaultValueAssignments"


    // $ANTLR start "entryRuleAbstractAtsyraTree"
    // InternalAtsyRAGoal.g:404:1: entryRuleAbstractAtsyraTree : ruleAbstractAtsyraTree EOF ;
    public final void entryRuleAbstractAtsyraTree() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:405:1: ( ruleAbstractAtsyraTree EOF )
            // InternalAtsyRAGoal.g:406:1: ruleAbstractAtsyraTree EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAbstractAtsyraTreeRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAbstractAtsyraTree();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAbstractAtsyraTreeRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractAtsyraTree"


    // $ANTLR start "ruleAbstractAtsyraTree"
    // InternalAtsyRAGoal.g:413:1: ruleAbstractAtsyraTree : ( ( rule__AbstractAtsyraTree__Alternatives ) ) ;
    public final void ruleAbstractAtsyraTree() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:417:2: ( ( ( rule__AbstractAtsyraTree__Alternatives ) ) )
            // InternalAtsyRAGoal.g:418:2: ( ( rule__AbstractAtsyraTree__Alternatives ) )
            {
            // InternalAtsyRAGoal.g:418:2: ( ( rule__AbstractAtsyraTree__Alternatives ) )
            // InternalAtsyRAGoal.g:419:3: ( rule__AbstractAtsyraTree__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAbstractAtsyraTreeAccess().getAlternatives()); 
            }
            // InternalAtsyRAGoal.g:420:3: ( rule__AbstractAtsyraTree__Alternatives )
            // InternalAtsyRAGoal.g:420:4: rule__AbstractAtsyraTree__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractAtsyraTree__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAbstractAtsyraTreeAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractAtsyraTree"


    // $ANTLR start "entryRuleAtsyraTreeReference"
    // InternalAtsyRAGoal.g:429:1: entryRuleAtsyraTreeReference : ruleAtsyraTreeReference EOF ;
    public final void entryRuleAtsyraTreeReference() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:430:1: ( ruleAtsyraTreeReference EOF )
            // InternalAtsyRAGoal.g:431:1: ruleAtsyraTreeReference EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAtsyraTreeReference();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeReferenceRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtsyraTreeReference"


    // $ANTLR start "ruleAtsyraTreeReference"
    // InternalAtsyRAGoal.g:438:1: ruleAtsyraTreeReference : ( ( rule__AtsyraTreeReference__ReferencedTreeAssignment ) ) ;
    public final void ruleAtsyraTreeReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:442:2: ( ( ( rule__AtsyraTreeReference__ReferencedTreeAssignment ) ) )
            // InternalAtsyRAGoal.g:443:2: ( ( rule__AtsyraTreeReference__ReferencedTreeAssignment ) )
            {
            // InternalAtsyRAGoal.g:443:2: ( ( rule__AtsyraTreeReference__ReferencedTreeAssignment ) )
            // InternalAtsyRAGoal.g:444:3: ( rule__AtsyraTreeReference__ReferencedTreeAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeReferenceAccess().getReferencedTreeAssignment()); 
            }
            // InternalAtsyRAGoal.g:445:3: ( rule__AtsyraTreeReference__ReferencedTreeAssignment )
            // InternalAtsyRAGoal.g:445:4: rule__AtsyraTreeReference__ReferencedTreeAssignment
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTreeReference__ReferencedTreeAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeReferenceAccess().getReferencedTreeAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtsyraTreeReference"


    // $ANTLR start "entryRuleAtsyraTree"
    // InternalAtsyRAGoal.g:454:1: entryRuleAtsyraTree : ruleAtsyraTree EOF ;
    public final void entryRuleAtsyraTree() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:455:1: ( ruleAtsyraTree EOF )
            // InternalAtsyRAGoal.g:456:1: ruleAtsyraTree EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAtsyraTree();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtsyraTree"


    // $ANTLR start "ruleAtsyraTree"
    // InternalAtsyRAGoal.g:463:1: ruleAtsyraTree : ( ( rule__AtsyraTree__Group__0 ) ) ;
    public final void ruleAtsyraTree() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:467:2: ( ( ( rule__AtsyraTree__Group__0 ) ) )
            // InternalAtsyRAGoal.g:468:2: ( ( rule__AtsyraTree__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:468:2: ( ( rule__AtsyraTree__Group__0 ) )
            // InternalAtsyRAGoal.g:469:3: ( rule__AtsyraTree__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:470:3: ( rule__AtsyraTree__Group__0 )
            // InternalAtsyRAGoal.g:470:4: rule__AtsyraTree__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtsyraTree"


    // $ANTLR start "entryRuleBooleanLiteral"
    // InternalAtsyRAGoal.g:479:1: entryRuleBooleanLiteral : ruleBooleanLiteral EOF ;
    public final void entryRuleBooleanLiteral() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:480:1: ( ruleBooleanLiteral EOF )
            // InternalAtsyRAGoal.g:481:1: ruleBooleanLiteral EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleBooleanLiteral();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanLiteral"


    // $ANTLR start "ruleBooleanLiteral"
    // InternalAtsyRAGoal.g:488:1: ruleBooleanLiteral : ( ( rule__BooleanLiteral__Group__0 ) ) ;
    public final void ruleBooleanLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:492:2: ( ( ( rule__BooleanLiteral__Group__0 ) ) )
            // InternalAtsyRAGoal.g:493:2: ( ( rule__BooleanLiteral__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:493:2: ( ( rule__BooleanLiteral__Group__0 ) )
            // InternalAtsyRAGoal.g:494:3: ( rule__BooleanLiteral__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:495:3: ( rule__BooleanLiteral__Group__0 )
            // InternalAtsyRAGoal.g:495:4: rule__BooleanLiteral__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanLiteral"


    // $ANTLR start "entryRuleBuildingSystemFeature"
    // InternalAtsyRAGoal.g:504:1: entryRuleBuildingSystemFeature : ruleBuildingSystemFeature EOF ;
    public final void entryRuleBuildingSystemFeature() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:505:1: ( ruleBuildingSystemFeature EOF )
            // InternalAtsyRAGoal.g:506:1: ruleBuildingSystemFeature EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemFeatureRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleBuildingSystemFeature();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemFeatureRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBuildingSystemFeature"


    // $ANTLR start "ruleBuildingSystemFeature"
    // InternalAtsyRAGoal.g:513:1: ruleBuildingSystemFeature : ( ( rule__BuildingSystemFeature__Group__0 ) ) ;
    public final void ruleBuildingSystemFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:517:2: ( ( ( rule__BuildingSystemFeature__Group__0 ) ) )
            // InternalAtsyRAGoal.g:518:2: ( ( rule__BuildingSystemFeature__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:518:2: ( ( rule__BuildingSystemFeature__Group__0 ) )
            // InternalAtsyRAGoal.g:519:3: ( rule__BuildingSystemFeature__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemFeatureAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:520:3: ( rule__BuildingSystemFeature__Group__0 )
            // InternalAtsyRAGoal.g:520:4: rule__BuildingSystemFeature__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BuildingSystemFeature__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemFeatureAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBuildingSystemFeature"


    // $ANTLR start "entryRuleBuildingSystemConstFeature"
    // InternalAtsyRAGoal.g:529:1: entryRuleBuildingSystemConstFeature : ruleBuildingSystemConstFeature EOF ;
    public final void entryRuleBuildingSystemConstFeature() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:530:1: ( ruleBuildingSystemConstFeature EOF )
            // InternalAtsyRAGoal.g:531:1: ruleBuildingSystemConstFeature EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemConstFeatureRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleBuildingSystemConstFeature();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemConstFeatureRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBuildingSystemConstFeature"


    // $ANTLR start "ruleBuildingSystemConstFeature"
    // InternalAtsyRAGoal.g:538:1: ruleBuildingSystemConstFeature : ( ( rule__BuildingSystemConstFeature__Group__0 ) ) ;
    public final void ruleBuildingSystemConstFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:542:2: ( ( ( rule__BuildingSystemConstFeature__Group__0 ) ) )
            // InternalAtsyRAGoal.g:543:2: ( ( rule__BuildingSystemConstFeature__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:543:2: ( ( rule__BuildingSystemConstFeature__Group__0 ) )
            // InternalAtsyRAGoal.g:544:3: ( rule__BuildingSystemConstFeature__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemConstFeatureAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:545:3: ( rule__BuildingSystemConstFeature__Group__0 )
            // InternalAtsyRAGoal.g:545:4: rule__BuildingSystemConstFeature__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BuildingSystemConstFeature__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemConstFeatureAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBuildingSystemConstFeature"


    // $ANTLR start "entryRuleInternalType"
    // InternalAtsyRAGoal.g:554:1: entryRuleInternalType : ruleInternalType EOF ;
    public final void entryRuleInternalType() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:555:1: ( ruleInternalType EOF )
            // InternalAtsyRAGoal.g:556:1: ruleInternalType EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInternalTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleInternalType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInternalTypeRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInternalType"


    // $ANTLR start "ruleInternalType"
    // InternalAtsyRAGoal.g:563:1: ruleInternalType : ( ( rule__InternalType__Group__0 ) ) ;
    public final void ruleInternalType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:567:2: ( ( ( rule__InternalType__Group__0 ) ) )
            // InternalAtsyRAGoal.g:568:2: ( ( rule__InternalType__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:568:2: ( ( rule__InternalType__Group__0 ) )
            // InternalAtsyRAGoal.g:569:3: ( rule__InternalType__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInternalTypeAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:570:3: ( rule__InternalType__Group__0 )
            // InternalAtsyRAGoal.g:570:4: rule__InternalType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InternalType__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInternalTypeAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInternalType"


    // $ANTLR start "entryRuleSystemType"
    // InternalAtsyRAGoal.g:579:1: entryRuleSystemType : ruleSystemType EOF ;
    public final void entryRuleSystemType() throws RecognitionException {
        try {
            // InternalAtsyRAGoal.g:580:1: ( ruleSystemType EOF )
            // InternalAtsyRAGoal.g:581:1: ruleSystemType EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSystemTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleSystemType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSystemTypeRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSystemType"


    // $ANTLR start "ruleSystemType"
    // InternalAtsyRAGoal.g:588:1: ruleSystemType : ( ( rule__SystemType__Group__0 ) ) ;
    public final void ruleSystemType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:592:2: ( ( ( rule__SystemType__Group__0 ) ) )
            // InternalAtsyRAGoal.g:593:2: ( ( rule__SystemType__Group__0 ) )
            {
            // InternalAtsyRAGoal.g:593:2: ( ( rule__SystemType__Group__0 ) )
            // InternalAtsyRAGoal.g:594:3: ( rule__SystemType__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSystemTypeAccess().getGroup()); 
            }
            // InternalAtsyRAGoal.g:595:3: ( rule__SystemType__Group__0 )
            // InternalAtsyRAGoal.g:595:4: rule__SystemType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SystemType__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSystemTypeAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSystemType"


    // $ANTLR start "ruleAtsyraTreeOperator"
    // InternalAtsyRAGoal.g:604:1: ruleAtsyraTreeOperator : ( ( rule__AtsyraTreeOperator__Alternatives ) ) ;
    public final void ruleAtsyraTreeOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:608:1: ( ( ( rule__AtsyraTreeOperator__Alternatives ) ) )
            // InternalAtsyRAGoal.g:609:2: ( ( rule__AtsyraTreeOperator__Alternatives ) )
            {
            // InternalAtsyRAGoal.g:609:2: ( ( rule__AtsyraTreeOperator__Alternatives ) )
            // InternalAtsyRAGoal.g:610:3: ( rule__AtsyraTreeOperator__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeOperatorAccess().getAlternatives()); 
            }
            // InternalAtsyRAGoal.g:611:3: ( rule__AtsyraTreeOperator__Alternatives )
            // InternalAtsyRAGoal.g:611:4: rule__AtsyraTreeOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTreeOperator__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeOperatorAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtsyraTreeOperator"


    // $ANTLR start "rule__Type__Alternatives"
    // InternalAtsyRAGoal.g:619:1: rule__Type__Alternatives : ( ( ruleInternalType ) | ( ruleSystemType ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:623:1: ( ( ruleInternalType ) | ( ruleSystemType ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==41) ) {
                alt1=1;
            }
            else if ( (LA1_0==42) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalAtsyRAGoal.g:624:2: ( ruleInternalType )
                    {
                    // InternalAtsyRAGoal.g:624:2: ( ruleInternalType )
                    // InternalAtsyRAGoal.g:625:3: ruleInternalType
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypeAccess().getInternalTypeParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleInternalType();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypeAccess().getInternalTypeParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:630:2: ( ruleSystemType )
                    {
                    // InternalAtsyRAGoal.g:630:2: ( ruleSystemType )
                    // InternalAtsyRAGoal.g:631:3: ruleSystemType
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypeAccess().getSystemTypeParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleSystemType();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypeAccess().getSystemTypeParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__OrCondition__Alternatives_1_1"
    // InternalAtsyRAGoal.g:640:1: rule__OrCondition__Alternatives_1_1 : ( ( 'or' ) | ( 'OR' ) );
    public final void rule__OrCondition__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:644:1: ( ( 'or' ) | ( 'OR' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalAtsyRAGoal.g:645:2: ( 'or' )
                    {
                    // InternalAtsyRAGoal.g:645:2: ( 'or' )
                    // InternalAtsyRAGoal.g:646:3: 'or'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getOrConditionAccess().getOrKeyword_1_1_0()); 
                    }
                    match(input,11,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getOrConditionAccess().getOrKeyword_1_1_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:651:2: ( 'OR' )
                    {
                    // InternalAtsyRAGoal.g:651:2: ( 'OR' )
                    // InternalAtsyRAGoal.g:652:3: 'OR'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getOrConditionAccess().getORKeyword_1_1_1()); 
                    }
                    match(input,12,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getOrConditionAccess().getORKeyword_1_1_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Alternatives_1_1"


    // $ANTLR start "rule__AndCondition__Alternatives_1_1"
    // InternalAtsyRAGoal.g:661:1: rule__AndCondition__Alternatives_1_1 : ( ( 'and' ) | ( 'AND' ) );
    public final void rule__AndCondition__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:665:1: ( ( 'and' ) | ( 'AND' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            else if ( (LA3_0==14) ) {
                alt3=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalAtsyRAGoal.g:666:2: ( 'and' )
                    {
                    // InternalAtsyRAGoal.g:666:2: ( 'and' )
                    // InternalAtsyRAGoal.g:667:3: 'and'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAndConditionAccess().getAndKeyword_1_1_0()); 
                    }
                    match(input,13,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAndConditionAccess().getAndKeyword_1_1_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:672:2: ( 'AND' )
                    {
                    // InternalAtsyRAGoal.g:672:2: ( 'AND' )
                    // InternalAtsyRAGoal.g:673:3: 'AND'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAndConditionAccess().getANDKeyword_1_1_1()); 
                    }
                    match(input,14,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAndConditionAccess().getANDKeyword_1_1_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Alternatives_1_1"


    // $ANTLR start "rule__TerminalCondition__Alternatives"
    // InternalAtsyRAGoal.g:682:1: rule__TerminalCondition__Alternatives : ( ( ( rule__TerminalCondition__Group_0__0 ) ) | ( ( rule__TerminalCondition__Group_1__0 ) ) | ( ruleEqualOrBooleanCond ) );
    public final void rule__TerminalCondition__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:686:1: ( ( ( rule__TerminalCondition__Group_0__0 ) ) | ( ( rule__TerminalCondition__Group_1__0 ) ) | ( ruleEqualOrBooleanCond ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 29:
                {
                alt4=1;
                }
                break;
            case 15:
            case 16:
                {
                alt4=2;
                }
                break;
            case RULE_STRING:
            case RULE_ID:
            case 17:
            case 18:
                {
                alt4=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalAtsyRAGoal.g:687:2: ( ( rule__TerminalCondition__Group_0__0 ) )
                    {
                    // InternalAtsyRAGoal.g:687:2: ( ( rule__TerminalCondition__Group_0__0 ) )
                    // InternalAtsyRAGoal.g:688:3: ( rule__TerminalCondition__Group_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalConditionAccess().getGroup_0()); 
                    }
                    // InternalAtsyRAGoal.g:689:3: ( rule__TerminalCondition__Group_0__0 )
                    // InternalAtsyRAGoal.g:689:4: rule__TerminalCondition__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TerminalCondition__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalConditionAccess().getGroup_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:693:2: ( ( rule__TerminalCondition__Group_1__0 ) )
                    {
                    // InternalAtsyRAGoal.g:693:2: ( ( rule__TerminalCondition__Group_1__0 ) )
                    // InternalAtsyRAGoal.g:694:3: ( rule__TerminalCondition__Group_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalConditionAccess().getGroup_1()); 
                    }
                    // InternalAtsyRAGoal.g:695:3: ( rule__TerminalCondition__Group_1__0 )
                    // InternalAtsyRAGoal.g:695:4: rule__TerminalCondition__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TerminalCondition__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalConditionAccess().getGroup_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalAtsyRAGoal.g:699:2: ( ruleEqualOrBooleanCond )
                    {
                    // InternalAtsyRAGoal.g:699:2: ( ruleEqualOrBooleanCond )
                    // InternalAtsyRAGoal.g:700:3: ruleEqualOrBooleanCond
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalConditionAccess().getEqualOrBooleanCondParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleEqualOrBooleanCond();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalConditionAccess().getEqualOrBooleanCondParserRuleCall_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Alternatives"


    // $ANTLR start "rule__TerminalCondition__Alternatives_1_1"
    // InternalAtsyRAGoal.g:709:1: rule__TerminalCondition__Alternatives_1_1 : ( ( 'not' ) | ( 'NOT' ) );
    public final void rule__TerminalCondition__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:713:1: ( ( 'not' ) | ( 'NOT' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            else if ( (LA5_0==16) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalAtsyRAGoal.g:714:2: ( 'not' )
                    {
                    // InternalAtsyRAGoal.g:714:2: ( 'not' )
                    // InternalAtsyRAGoal.g:715:3: 'not'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalConditionAccess().getNotKeyword_1_1_0()); 
                    }
                    match(input,15,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalConditionAccess().getNotKeyword_1_1_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:720:2: ( 'NOT' )
                    {
                    // InternalAtsyRAGoal.g:720:2: ( 'NOT' )
                    // InternalAtsyRAGoal.g:721:3: 'NOT'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalConditionAccess().getNOTKeyword_1_1_1()); 
                    }
                    match(input,16,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalConditionAccess().getNOTKeyword_1_1_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Alternatives_1_1"


    // $ANTLR start "rule__EqualOrBooleanCond__Alternatives"
    // InternalAtsyRAGoal.g:730:1: rule__EqualOrBooleanCond__Alternatives : ( ( ( rule__EqualOrBooleanCond__Group_0__0 ) ) | ( ( rule__EqualOrBooleanCond__Group_1__0 ) ) );
    public final void rule__EqualOrBooleanCond__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:734:1: ( ( ( rule__EqualOrBooleanCond__Group_0__0 ) ) | ( ( rule__EqualOrBooleanCond__Group_1__0 ) ) )
            int alt6=2;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==31) ) {
                    alt6=2;
                }
                else if ( (LA6_1==EOF||(LA6_1>=11 && LA6_1<=14)||LA6_1==22||LA6_1==30||LA6_1==33||LA6_1==37) ) {
                    alt6=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
                }
                break;
            case RULE_ID:
                {
                int LA6_2 = input.LA(2);

                if ( (LA6_2==EOF||(LA6_2>=11 && LA6_2<=14)||LA6_2==22||LA6_2==30||LA6_2==33||LA6_2==37) ) {
                    alt6=1;
                }
                else if ( (LA6_2==31) ) {
                    alt6=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 2, input);

                    throw nvae;
                }
                }
                break;
            case 17:
                {
                int LA6_3 = input.LA(2);

                if ( (LA6_3==31) ) {
                    alt6=2;
                }
                else if ( (LA6_3==EOF||(LA6_3>=11 && LA6_3<=14)||LA6_3==22||LA6_3==30||LA6_3==33||LA6_3==37) ) {
                    alt6=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 3, input);

                    throw nvae;
                }
                }
                break;
            case 18:
                {
                int LA6_4 = input.LA(2);

                if ( (LA6_4==EOF||(LA6_4>=11 && LA6_4<=14)||LA6_4==22||LA6_4==30||LA6_4==33||LA6_4==37) ) {
                    alt6=1;
                }
                else if ( (LA6_4==31) ) {
                    alt6=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 4, input);

                    throw nvae;
                }
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalAtsyRAGoal.g:735:2: ( ( rule__EqualOrBooleanCond__Group_0__0 ) )
                    {
                    // InternalAtsyRAGoal.g:735:2: ( ( rule__EqualOrBooleanCond__Group_0__0 ) )
                    // InternalAtsyRAGoal.g:736:3: ( rule__EqualOrBooleanCond__Group_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getEqualOrBooleanCondAccess().getGroup_0()); 
                    }
                    // InternalAtsyRAGoal.g:737:3: ( rule__EqualOrBooleanCond__Group_0__0 )
                    // InternalAtsyRAGoal.g:737:4: rule__EqualOrBooleanCond__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__EqualOrBooleanCond__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getEqualOrBooleanCondAccess().getGroup_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:741:2: ( ( rule__EqualOrBooleanCond__Group_1__0 ) )
                    {
                    // InternalAtsyRAGoal.g:741:2: ( ( rule__EqualOrBooleanCond__Group_1__0 ) )
                    // InternalAtsyRAGoal.g:742:3: ( rule__EqualOrBooleanCond__Group_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getEqualOrBooleanCondAccess().getGroup_1()); 
                    }
                    // InternalAtsyRAGoal.g:743:3: ( rule__EqualOrBooleanCond__Group_1__0 )
                    // InternalAtsyRAGoal.g:743:4: rule__EqualOrBooleanCond__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__EqualOrBooleanCond__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getEqualOrBooleanCondAccess().getGroup_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Alternatives"


    // $ANTLR start "rule__TypedElementDecl__Alternatives"
    // InternalAtsyRAGoal.g:751:1: rule__TypedElementDecl__Alternatives : ( ( ( rule__TypedElementDecl__Group_0__0 ) ) | ( ruleBuildingSystemFeature ) | ( ( rule__TypedElementDecl__Group_2__0 ) ) );
    public final void rule__TypedElementDecl__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:755:1: ( ( ( rule__TypedElementDecl__Group_0__0 ) ) | ( ruleBuildingSystemFeature ) | ( ( rule__TypedElementDecl__Group_2__0 ) ) )
            int alt7=3;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==32) ) {
                int LA7_1 = input.LA(2);

                if ( ((LA7_1>=RULE_STRING && LA7_1<=RULE_ID)) ) {
                    alt7=3;
                }
                else if ( ((LA7_1>=17 && LA7_1<=18)) ) {
                    alt7=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 7, 1, input);

                    throw nvae;
                }
            }
            else if ( ((LA7_0>=RULE_STRING && LA7_0<=RULE_ID)) ) {
                alt7=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalAtsyRAGoal.g:756:2: ( ( rule__TypedElementDecl__Group_0__0 ) )
                    {
                    // InternalAtsyRAGoal.g:756:2: ( ( rule__TypedElementDecl__Group_0__0 ) )
                    // InternalAtsyRAGoal.g:757:3: ( rule__TypedElementDecl__Group_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypedElementDeclAccess().getGroup_0()); 
                    }
                    // InternalAtsyRAGoal.g:758:3: ( rule__TypedElementDecl__Group_0__0 )
                    // InternalAtsyRAGoal.g:758:4: rule__TypedElementDecl__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypedElementDecl__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypedElementDeclAccess().getGroup_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:762:2: ( ruleBuildingSystemFeature )
                    {
                    // InternalAtsyRAGoal.g:762:2: ( ruleBuildingSystemFeature )
                    // InternalAtsyRAGoal.g:763:3: ruleBuildingSystemFeature
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypedElementDeclAccess().getBuildingSystemFeatureParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleBuildingSystemFeature();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypedElementDeclAccess().getBuildingSystemFeatureParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalAtsyRAGoal.g:768:2: ( ( rule__TypedElementDecl__Group_2__0 ) )
                    {
                    // InternalAtsyRAGoal.g:768:2: ( ( rule__TypedElementDecl__Group_2__0 ) )
                    // InternalAtsyRAGoal.g:769:3: ( rule__TypedElementDecl__Group_2__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypedElementDeclAccess().getGroup_2()); 
                    }
                    // InternalAtsyRAGoal.g:770:3: ( rule__TypedElementDecl__Group_2__0 )
                    // InternalAtsyRAGoal.g:770:4: rule__TypedElementDecl__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypedElementDecl__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypedElementDeclAccess().getGroup_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementDecl__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalAtsyRAGoal.g:778:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:782:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_STRING) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_ID) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalAtsyRAGoal.g:783:2: ( RULE_STRING )
                    {
                    // InternalAtsyRAGoal.g:783:2: ( RULE_STRING )
                    // InternalAtsyRAGoal.g:784:3: RULE_STRING
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    }
                    match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:789:2: ( RULE_ID )
                    {
                    // InternalAtsyRAGoal.g:789:2: ( RULE_ID )
                    // InternalAtsyRAGoal.g:790:3: RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    }
                    match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__TypedElementString__Alternatives"
    // InternalAtsyRAGoal.g:799:1: rule__TypedElementString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) | ( 'true' ) | ( 'false' ) );
    public final void rule__TypedElementString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:803:1: ( ( RULE_STRING ) | ( RULE_ID ) | ( 'true' ) | ( 'false' ) )
            int alt9=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt9=1;
                }
                break;
            case RULE_ID:
                {
                alt9=2;
                }
                break;
            case 17:
                {
                alt9=3;
                }
                break;
            case 18:
                {
                alt9=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalAtsyRAGoal.g:804:2: ( RULE_STRING )
                    {
                    // InternalAtsyRAGoal.g:804:2: ( RULE_STRING )
                    // InternalAtsyRAGoal.g:805:3: RULE_STRING
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypedElementStringAccess().getSTRINGTerminalRuleCall_0()); 
                    }
                    match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypedElementStringAccess().getSTRINGTerminalRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:810:2: ( RULE_ID )
                    {
                    // InternalAtsyRAGoal.g:810:2: ( RULE_ID )
                    // InternalAtsyRAGoal.g:811:3: RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypedElementStringAccess().getIDTerminalRuleCall_1()); 
                    }
                    match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypedElementStringAccess().getIDTerminalRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalAtsyRAGoal.g:816:2: ( 'true' )
                    {
                    // InternalAtsyRAGoal.g:816:2: ( 'true' )
                    // InternalAtsyRAGoal.g:817:3: 'true'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypedElementStringAccess().getTrueKeyword_2()); 
                    }
                    match(input,17,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypedElementStringAccess().getTrueKeyword_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalAtsyRAGoal.g:822:2: ( 'false' )
                    {
                    // InternalAtsyRAGoal.g:822:2: ( 'false' )
                    // InternalAtsyRAGoal.g:823:3: 'false'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypedElementStringAccess().getFalseKeyword_3()); 
                    }
                    match(input,18,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypedElementStringAccess().getFalseKeyword_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementString__Alternatives"


    // $ANTLR start "rule__AbstractAtsyraTree__Alternatives"
    // InternalAtsyRAGoal.g:832:1: rule__AbstractAtsyraTree__Alternatives : ( ( ruleAtsyraTree ) | ( ruleAtsyraTreeReference ) );
    public final void rule__AbstractAtsyraTree__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:836:1: ( ( ruleAtsyraTree ) | ( ruleAtsyraTreeReference ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_STRING) ) {
                int LA10_1 = input.LA(2);

                if ( (LA10_1==39) ) {
                    alt10=1;
                }
                else if ( (LA10_1==EOF||LA10_1==30||LA10_1==38) ) {
                    alt10=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA10_0==RULE_ID) ) {
                int LA10_2 = input.LA(2);

                if ( (LA10_2==EOF||LA10_2==30||LA10_2==38) ) {
                    alt10=2;
                }
                else if ( (LA10_2==39) ) {
                    alt10=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 2, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalAtsyRAGoal.g:837:2: ( ruleAtsyraTree )
                    {
                    // InternalAtsyRAGoal.g:837:2: ( ruleAtsyraTree )
                    // InternalAtsyRAGoal.g:838:3: ruleAtsyraTree
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAbstractAtsyraTreeAccess().getAtsyraTreeParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleAtsyraTree();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAbstractAtsyraTreeAccess().getAtsyraTreeParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:843:2: ( ruleAtsyraTreeReference )
                    {
                    // InternalAtsyRAGoal.g:843:2: ( ruleAtsyraTreeReference )
                    // InternalAtsyRAGoal.g:844:3: ruleAtsyraTreeReference
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAbstractAtsyraTreeAccess().getAtsyraTreeReferenceParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleAtsyraTreeReference();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAbstractAtsyraTreeAccess().getAtsyraTreeReferenceParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractAtsyraTree__Alternatives"


    // $ANTLR start "rule__BooleanLiteral__NameAlternatives_1_0"
    // InternalAtsyRAGoal.g:853:1: rule__BooleanLiteral__NameAlternatives_1_0 : ( ( 'true' ) | ( 'false' ) );
    public final void rule__BooleanLiteral__NameAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:857:1: ( ( 'true' ) | ( 'false' ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==17) ) {
                alt11=1;
            }
            else if ( (LA11_0==18) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalAtsyRAGoal.g:858:2: ( 'true' )
                    {
                    // InternalAtsyRAGoal.g:858:2: ( 'true' )
                    // InternalAtsyRAGoal.g:859:3: 'true'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBooleanLiteralAccess().getNameTrueKeyword_1_0_0()); 
                    }
                    match(input,17,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBooleanLiteralAccess().getNameTrueKeyword_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:864:2: ( 'false' )
                    {
                    // InternalAtsyRAGoal.g:864:2: ( 'false' )
                    // InternalAtsyRAGoal.g:865:3: 'false'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBooleanLiteralAccess().getNameFalseKeyword_1_0_1()); 
                    }
                    match(input,18,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBooleanLiteralAccess().getNameFalseKeyword_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__NameAlternatives_1_0"


    // $ANTLR start "rule__AtsyraTreeOperator__Alternatives"
    // InternalAtsyRAGoal.g:874:1: rule__AtsyraTreeOperator__Alternatives : ( ( ( 'OR' ) ) | ( ( 'SAND' ) ) | ( ( 'AND' ) ) );
    public final void rule__AtsyraTreeOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:878:1: ( ( ( 'OR' ) ) | ( ( 'SAND' ) ) | ( ( 'AND' ) ) )
            int alt12=3;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt12=1;
                }
                break;
            case 19:
                {
                alt12=2;
                }
                break;
            case 14:
                {
                alt12=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalAtsyRAGoal.g:879:2: ( ( 'OR' ) )
                    {
                    // InternalAtsyRAGoal.g:879:2: ( ( 'OR' ) )
                    // InternalAtsyRAGoal.g:880:3: ( 'OR' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAtsyraTreeOperatorAccess().getOREnumLiteralDeclaration_0()); 
                    }
                    // InternalAtsyRAGoal.g:881:3: ( 'OR' )
                    // InternalAtsyRAGoal.g:881:4: 'OR'
                    {
                    match(input,12,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAtsyraTreeOperatorAccess().getOREnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:885:2: ( ( 'SAND' ) )
                    {
                    // InternalAtsyRAGoal.g:885:2: ( ( 'SAND' ) )
                    // InternalAtsyRAGoal.g:886:3: ( 'SAND' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAtsyraTreeOperatorAccess().getSANDEnumLiteralDeclaration_1()); 
                    }
                    // InternalAtsyRAGoal.g:887:3: ( 'SAND' )
                    // InternalAtsyRAGoal.g:887:4: 'SAND'
                    {
                    match(input,19,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAtsyraTreeOperatorAccess().getSANDEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalAtsyRAGoal.g:891:2: ( ( 'AND' ) )
                    {
                    // InternalAtsyRAGoal.g:891:2: ( ( 'AND' ) )
                    // InternalAtsyRAGoal.g:892:3: ( 'AND' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAtsyraTreeOperatorAccess().getANDEnumLiteralDeclaration_2()); 
                    }
                    // InternalAtsyRAGoal.g:893:3: ( 'AND' )
                    // InternalAtsyRAGoal.g:893:4: 'AND'
                    {
                    match(input,14,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAtsyraTreeOperatorAccess().getANDEnumLiteralDeclaration_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTreeOperator__Alternatives"


    // $ANTLR start "rule__AtsyraGoalModel__Group__0"
    // InternalAtsyRAGoal.g:901:1: rule__AtsyraGoalModel__Group__0 : rule__AtsyraGoalModel__Group__0__Impl rule__AtsyraGoalModel__Group__1 ;
    public final void rule__AtsyraGoalModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:905:1: ( rule__AtsyraGoalModel__Group__0__Impl rule__AtsyraGoalModel__Group__1 )
            // InternalAtsyRAGoal.g:906:2: rule__AtsyraGoalModel__Group__0__Impl rule__AtsyraGoalModel__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__AtsyraGoalModel__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__0"


    // $ANTLR start "rule__AtsyraGoalModel__Group__0__Impl"
    // InternalAtsyRAGoal.g:913:1: rule__AtsyraGoalModel__Group__0__Impl : ( () ) ;
    public final void rule__AtsyraGoalModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:917:1: ( ( () ) )
            // InternalAtsyRAGoal.g:918:1: ( () )
            {
            // InternalAtsyRAGoal.g:918:1: ( () )
            // InternalAtsyRAGoal.g:919:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getAtsyraGoalModelAction_0()); 
            }
            // InternalAtsyRAGoal.g:920:2: ()
            // InternalAtsyRAGoal.g:920:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getAtsyraGoalModelAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__0__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group__1"
    // InternalAtsyRAGoal.g:928:1: rule__AtsyraGoalModel__Group__1 : rule__AtsyraGoalModel__Group__1__Impl rule__AtsyraGoalModel__Group__2 ;
    public final void rule__AtsyraGoalModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:932:1: ( rule__AtsyraGoalModel__Group__1__Impl rule__AtsyraGoalModel__Group__2 )
            // InternalAtsyRAGoal.g:933:2: rule__AtsyraGoalModel__Group__1__Impl rule__AtsyraGoalModel__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__AtsyraGoalModel__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__1"


    // $ANTLR start "rule__AtsyraGoalModel__Group__1__Impl"
    // InternalAtsyRAGoal.g:940:1: rule__AtsyraGoalModel__Group__1__Impl : ( ( rule__AtsyraGoalModel__ImportsAssignment_1 )* ) ;
    public final void rule__AtsyraGoalModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:944:1: ( ( ( rule__AtsyraGoalModel__ImportsAssignment_1 )* ) )
            // InternalAtsyRAGoal.g:945:1: ( ( rule__AtsyraGoalModel__ImportsAssignment_1 )* )
            {
            // InternalAtsyRAGoal.g:945:1: ( ( rule__AtsyraGoalModel__ImportsAssignment_1 )* )
            // InternalAtsyRAGoal.g:946:2: ( rule__AtsyraGoalModel__ImportsAssignment_1 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getImportsAssignment_1()); 
            }
            // InternalAtsyRAGoal.g:947:2: ( rule__AtsyraGoalModel__ImportsAssignment_1 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==28) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:947:3: rule__AtsyraGoalModel__ImportsAssignment_1
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__AtsyraGoalModel__ImportsAssignment_1();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getImportsAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__1__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group__2"
    // InternalAtsyRAGoal.g:955:1: rule__AtsyraGoalModel__Group__2 : rule__AtsyraGoalModel__Group__2__Impl rule__AtsyraGoalModel__Group__3 ;
    public final void rule__AtsyraGoalModel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:959:1: ( rule__AtsyraGoalModel__Group__2__Impl rule__AtsyraGoalModel__Group__3 )
            // InternalAtsyRAGoal.g:960:2: rule__AtsyraGoalModel__Group__2__Impl rule__AtsyraGoalModel__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__AtsyraGoalModel__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__2"


    // $ANTLR start "rule__AtsyraGoalModel__Group__2__Impl"
    // InternalAtsyRAGoal.g:967:1: rule__AtsyraGoalModel__Group__2__Impl : ( 'AtsyraGoalModel' ) ;
    public final void rule__AtsyraGoalModel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:971:1: ( ( 'AtsyraGoalModel' ) )
            // InternalAtsyRAGoal.g:972:1: ( 'AtsyraGoalModel' )
            {
            // InternalAtsyRAGoal.g:972:1: ( 'AtsyraGoalModel' )
            // InternalAtsyRAGoal.g:973:2: 'AtsyraGoalModel'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getAtsyraGoalModelKeyword_2()); 
            }
            match(input,20,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getAtsyraGoalModelKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__2__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group__3"
    // InternalAtsyRAGoal.g:982:1: rule__AtsyraGoalModel__Group__3 : rule__AtsyraGoalModel__Group__3__Impl rule__AtsyraGoalModel__Group__4 ;
    public final void rule__AtsyraGoalModel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:986:1: ( rule__AtsyraGoalModel__Group__3__Impl rule__AtsyraGoalModel__Group__4 )
            // InternalAtsyRAGoal.g:987:2: rule__AtsyraGoalModel__Group__3__Impl rule__AtsyraGoalModel__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__AtsyraGoalModel__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__3"


    // $ANTLR start "rule__AtsyraGoalModel__Group__3__Impl"
    // InternalAtsyRAGoal.g:994:1: rule__AtsyraGoalModel__Group__3__Impl : ( '{' ) ;
    public final void rule__AtsyraGoalModel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:998:1: ( ( '{' ) )
            // InternalAtsyRAGoal.g:999:1: ( '{' )
            {
            // InternalAtsyRAGoal.g:999:1: ( '{' )
            // InternalAtsyRAGoal.g:1000:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,21,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__3__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group__4"
    // InternalAtsyRAGoal.g:1009:1: rule__AtsyraGoalModel__Group__4 : rule__AtsyraGoalModel__Group__4__Impl rule__AtsyraGoalModel__Group__5 ;
    public final void rule__AtsyraGoalModel__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1013:1: ( rule__AtsyraGoalModel__Group__4__Impl rule__AtsyraGoalModel__Group__5 )
            // InternalAtsyRAGoal.g:1014:2: rule__AtsyraGoalModel__Group__4__Impl rule__AtsyraGoalModel__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__AtsyraGoalModel__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__4"


    // $ANTLR start "rule__AtsyraGoalModel__Group__4__Impl"
    // InternalAtsyRAGoal.g:1021:1: rule__AtsyraGoalModel__Group__4__Impl : ( ( rule__AtsyraGoalModel__Group_4__0 )? ) ;
    public final void rule__AtsyraGoalModel__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1025:1: ( ( ( rule__AtsyraGoalModel__Group_4__0 )? ) )
            // InternalAtsyRAGoal.g:1026:1: ( ( rule__AtsyraGoalModel__Group_4__0 )? )
            {
            // InternalAtsyRAGoal.g:1026:1: ( ( rule__AtsyraGoalModel__Group_4__0 )? )
            // InternalAtsyRAGoal.g:1027:2: ( rule__AtsyraGoalModel__Group_4__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getGroup_4()); 
            }
            // InternalAtsyRAGoal.g:1028:2: ( rule__AtsyraGoalModel__Group_4__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==23) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalAtsyRAGoal.g:1028:3: rule__AtsyraGoalModel__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoalModel__Group_4__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getGroup_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__4__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group__5"
    // InternalAtsyRAGoal.g:1036:1: rule__AtsyraGoalModel__Group__5 : rule__AtsyraGoalModel__Group__5__Impl rule__AtsyraGoalModel__Group__6 ;
    public final void rule__AtsyraGoalModel__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1040:1: ( rule__AtsyraGoalModel__Group__5__Impl rule__AtsyraGoalModel__Group__6 )
            // InternalAtsyRAGoal.g:1041:2: rule__AtsyraGoalModel__Group__5__Impl rule__AtsyraGoalModel__Group__6
            {
            pushFollow(FOLLOW_6);
            rule__AtsyraGoalModel__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__5"


    // $ANTLR start "rule__AtsyraGoalModel__Group__5__Impl"
    // InternalAtsyRAGoal.g:1048:1: rule__AtsyraGoalModel__Group__5__Impl : ( ( rule__AtsyraGoalModel__Group_5__0 )? ) ;
    public final void rule__AtsyraGoalModel__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1052:1: ( ( ( rule__AtsyraGoalModel__Group_5__0 )? ) )
            // InternalAtsyRAGoal.g:1053:1: ( ( rule__AtsyraGoalModel__Group_5__0 )? )
            {
            // InternalAtsyRAGoal.g:1053:1: ( ( rule__AtsyraGoalModel__Group_5__0 )? )
            // InternalAtsyRAGoal.g:1054:2: ( rule__AtsyraGoalModel__Group_5__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getGroup_5()); 
            }
            // InternalAtsyRAGoal.g:1055:2: ( rule__AtsyraGoalModel__Group_5__0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==24) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalAtsyRAGoal.g:1055:3: rule__AtsyraGoalModel__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoalModel__Group_5__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getGroup_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__5__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group__6"
    // InternalAtsyRAGoal.g:1063:1: rule__AtsyraGoalModel__Group__6 : rule__AtsyraGoalModel__Group__6__Impl rule__AtsyraGoalModel__Group__7 ;
    public final void rule__AtsyraGoalModel__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1067:1: ( rule__AtsyraGoalModel__Group__6__Impl rule__AtsyraGoalModel__Group__7 )
            // InternalAtsyRAGoal.g:1068:2: rule__AtsyraGoalModel__Group__6__Impl rule__AtsyraGoalModel__Group__7
            {
            pushFollow(FOLLOW_6);
            rule__AtsyraGoalModel__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__7();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__6"


    // $ANTLR start "rule__AtsyraGoalModel__Group__6__Impl"
    // InternalAtsyRAGoal.g:1075:1: rule__AtsyraGoalModel__Group__6__Impl : ( ( rule__AtsyraGoalModel__Group_6__0 )? ) ;
    public final void rule__AtsyraGoalModel__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1079:1: ( ( ( rule__AtsyraGoalModel__Group_6__0 )? ) )
            // InternalAtsyRAGoal.g:1080:1: ( ( rule__AtsyraGoalModel__Group_6__0 )? )
            {
            // InternalAtsyRAGoal.g:1080:1: ( ( rule__AtsyraGoalModel__Group_6__0 )? )
            // InternalAtsyRAGoal.g:1081:2: ( rule__AtsyraGoalModel__Group_6__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getGroup_6()); 
            }
            // InternalAtsyRAGoal.g:1082:2: ( rule__AtsyraGoalModel__Group_6__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==25) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalAtsyRAGoal.g:1082:3: rule__AtsyraGoalModel__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoalModel__Group_6__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getGroup_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__6__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group__7"
    // InternalAtsyRAGoal.g:1090:1: rule__AtsyraGoalModel__Group__7 : rule__AtsyraGoalModel__Group__7__Impl rule__AtsyraGoalModel__Group__8 ;
    public final void rule__AtsyraGoalModel__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1094:1: ( rule__AtsyraGoalModel__Group__7__Impl rule__AtsyraGoalModel__Group__8 )
            // InternalAtsyRAGoal.g:1095:2: rule__AtsyraGoalModel__Group__7__Impl rule__AtsyraGoalModel__Group__8
            {
            pushFollow(FOLLOW_6);
            rule__AtsyraGoalModel__Group__7__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__8();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__7"


    // $ANTLR start "rule__AtsyraGoalModel__Group__7__Impl"
    // InternalAtsyRAGoal.g:1102:1: rule__AtsyraGoalModel__Group__7__Impl : ( ( rule__AtsyraGoalModel__Group_7__0 )? ) ;
    public final void rule__AtsyraGoalModel__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1106:1: ( ( ( rule__AtsyraGoalModel__Group_7__0 )? ) )
            // InternalAtsyRAGoal.g:1107:1: ( ( rule__AtsyraGoalModel__Group_7__0 )? )
            {
            // InternalAtsyRAGoal.g:1107:1: ( ( rule__AtsyraGoalModel__Group_7__0 )? )
            // InternalAtsyRAGoal.g:1108:2: ( rule__AtsyraGoalModel__Group_7__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getGroup_7()); 
            }
            // InternalAtsyRAGoal.g:1109:2: ( rule__AtsyraGoalModel__Group_7__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==26) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalAtsyRAGoal.g:1109:3: rule__AtsyraGoalModel__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoalModel__Group_7__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getGroup_7()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__7__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group__8"
    // InternalAtsyRAGoal.g:1117:1: rule__AtsyraGoalModel__Group__8 : rule__AtsyraGoalModel__Group__8__Impl rule__AtsyraGoalModel__Group__9 ;
    public final void rule__AtsyraGoalModel__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1121:1: ( rule__AtsyraGoalModel__Group__8__Impl rule__AtsyraGoalModel__Group__9 )
            // InternalAtsyRAGoal.g:1122:2: rule__AtsyraGoalModel__Group__8__Impl rule__AtsyraGoalModel__Group__9
            {
            pushFollow(FOLLOW_6);
            rule__AtsyraGoalModel__Group__8__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__9();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__8"


    // $ANTLR start "rule__AtsyraGoalModel__Group__8__Impl"
    // InternalAtsyRAGoal.g:1129:1: rule__AtsyraGoalModel__Group__8__Impl : ( ( rule__AtsyraGoalModel__Group_8__0 )? ) ;
    public final void rule__AtsyraGoalModel__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1133:1: ( ( ( rule__AtsyraGoalModel__Group_8__0 )? ) )
            // InternalAtsyRAGoal.g:1134:1: ( ( rule__AtsyraGoalModel__Group_8__0 )? )
            {
            // InternalAtsyRAGoal.g:1134:1: ( ( rule__AtsyraGoalModel__Group_8__0 )? )
            // InternalAtsyRAGoal.g:1135:2: ( rule__AtsyraGoalModel__Group_8__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getGroup_8()); 
            }
            // InternalAtsyRAGoal.g:1136:2: ( rule__AtsyraGoalModel__Group_8__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==27) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalAtsyRAGoal.g:1136:3: rule__AtsyraGoalModel__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoalModel__Group_8__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getGroup_8()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__8__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group__9"
    // InternalAtsyRAGoal.g:1144:1: rule__AtsyraGoalModel__Group__9 : rule__AtsyraGoalModel__Group__9__Impl ;
    public final void rule__AtsyraGoalModel__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1148:1: ( rule__AtsyraGoalModel__Group__9__Impl )
            // InternalAtsyRAGoal.g:1149:2: rule__AtsyraGoalModel__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group__9__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__9"


    // $ANTLR start "rule__AtsyraGoalModel__Group__9__Impl"
    // InternalAtsyRAGoal.g:1155:1: rule__AtsyraGoalModel__Group__9__Impl : ( '}' ) ;
    public final void rule__AtsyraGoalModel__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1159:1: ( ( '}' ) )
            // InternalAtsyRAGoal.g:1160:1: ( '}' )
            {
            // InternalAtsyRAGoal.g:1160:1: ( '}' )
            // InternalAtsyRAGoal.g:1161:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_9()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_9()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group__9__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__0"
    // InternalAtsyRAGoal.g:1171:1: rule__AtsyraGoalModel__Group_4__0 : rule__AtsyraGoalModel__Group_4__0__Impl rule__AtsyraGoalModel__Group_4__1 ;
    public final void rule__AtsyraGoalModel__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1175:1: ( rule__AtsyraGoalModel__Group_4__0__Impl rule__AtsyraGoalModel__Group_4__1 )
            // InternalAtsyRAGoal.g:1176:2: rule__AtsyraGoalModel__Group_4__0__Impl rule__AtsyraGoalModel__Group_4__1
            {
            pushFollow(FOLLOW_5);
            rule__AtsyraGoalModel__Group_4__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_4__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__0"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__0__Impl"
    // InternalAtsyRAGoal.g:1183:1: rule__AtsyraGoalModel__Group_4__0__Impl : ( 'defaults' ) ;
    public final void rule__AtsyraGoalModel__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1187:1: ( ( 'defaults' ) )
            // InternalAtsyRAGoal.g:1188:1: ( 'defaults' )
            {
            // InternalAtsyRAGoal.g:1188:1: ( 'defaults' )
            // InternalAtsyRAGoal.g:1189:2: 'defaults'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getDefaultsKeyword_4_0()); 
            }
            match(input,23,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getDefaultsKeyword_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__0__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__1"
    // InternalAtsyRAGoal.g:1198:1: rule__AtsyraGoalModel__Group_4__1 : rule__AtsyraGoalModel__Group_4__1__Impl rule__AtsyraGoalModel__Group_4__2 ;
    public final void rule__AtsyraGoalModel__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1202:1: ( rule__AtsyraGoalModel__Group_4__1__Impl rule__AtsyraGoalModel__Group_4__2 )
            // InternalAtsyRAGoal.g:1203:2: rule__AtsyraGoalModel__Group_4__1__Impl rule__AtsyraGoalModel__Group_4__2
            {
            pushFollow(FOLLOW_7);
            rule__AtsyraGoalModel__Group_4__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_4__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__1"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__1__Impl"
    // InternalAtsyRAGoal.g:1210:1: rule__AtsyraGoalModel__Group_4__1__Impl : ( '{' ) ;
    public final void rule__AtsyraGoalModel__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1214:1: ( ( '{' ) )
            // InternalAtsyRAGoal.g:1215:1: ( '{' )
            {
            // InternalAtsyRAGoal.g:1215:1: ( '{' )
            // InternalAtsyRAGoal.g:1216:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_4_1()); 
            }
            match(input,21,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_4_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__1__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__2"
    // InternalAtsyRAGoal.g:1225:1: rule__AtsyraGoalModel__Group_4__2 : rule__AtsyraGoalModel__Group_4__2__Impl rule__AtsyraGoalModel__Group_4__3 ;
    public final void rule__AtsyraGoalModel__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1229:1: ( rule__AtsyraGoalModel__Group_4__2__Impl rule__AtsyraGoalModel__Group_4__3 )
            // InternalAtsyRAGoal.g:1230:2: rule__AtsyraGoalModel__Group_4__2__Impl rule__AtsyraGoalModel__Group_4__3
            {
            pushFollow(FOLLOW_8);
            rule__AtsyraGoalModel__Group_4__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_4__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__2"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__2__Impl"
    // InternalAtsyRAGoal.g:1237:1: rule__AtsyraGoalModel__Group_4__2__Impl : ( ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_2 ) ) ;
    public final void rule__AtsyraGoalModel__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1241:1: ( ( ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_2 ) ) )
            // InternalAtsyRAGoal.g:1242:1: ( ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_2 ) )
            {
            // InternalAtsyRAGoal.g:1242:1: ( ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_2 ) )
            // InternalAtsyRAGoal.g:1243:2: ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesAssignment_4_2()); 
            }
            // InternalAtsyRAGoal.g:1244:2: ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_2 )
            // InternalAtsyRAGoal.g:1244:3: rule__AtsyraGoalModel__DefaultValuesAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__DefaultValuesAssignment_4_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesAssignment_4_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__2__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__3"
    // InternalAtsyRAGoal.g:1252:1: rule__AtsyraGoalModel__Group_4__3 : rule__AtsyraGoalModel__Group_4__3__Impl rule__AtsyraGoalModel__Group_4__4 ;
    public final void rule__AtsyraGoalModel__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1256:1: ( rule__AtsyraGoalModel__Group_4__3__Impl rule__AtsyraGoalModel__Group_4__4 )
            // InternalAtsyRAGoal.g:1257:2: rule__AtsyraGoalModel__Group_4__3__Impl rule__AtsyraGoalModel__Group_4__4
            {
            pushFollow(FOLLOW_8);
            rule__AtsyraGoalModel__Group_4__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_4__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__3"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__3__Impl"
    // InternalAtsyRAGoal.g:1264:1: rule__AtsyraGoalModel__Group_4__3__Impl : ( ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_3 )* ) ;
    public final void rule__AtsyraGoalModel__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1268:1: ( ( ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_3 )* ) )
            // InternalAtsyRAGoal.g:1269:1: ( ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_3 )* )
            {
            // InternalAtsyRAGoal.g:1269:1: ( ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_3 )* )
            // InternalAtsyRAGoal.g:1270:2: ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_3 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesAssignment_4_3()); 
            }
            // InternalAtsyRAGoal.g:1271:2: ( rule__AtsyraGoalModel__DefaultValuesAssignment_4_3 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=RULE_STRING && LA19_0<=RULE_ID)) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:1271:3: rule__AtsyraGoalModel__DefaultValuesAssignment_4_3
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__AtsyraGoalModel__DefaultValuesAssignment_4_3();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesAssignment_4_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__3__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__4"
    // InternalAtsyRAGoal.g:1279:1: rule__AtsyraGoalModel__Group_4__4 : rule__AtsyraGoalModel__Group_4__4__Impl ;
    public final void rule__AtsyraGoalModel__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1283:1: ( rule__AtsyraGoalModel__Group_4__4__Impl )
            // InternalAtsyRAGoal.g:1284:2: rule__AtsyraGoalModel__Group_4__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_4__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__4"


    // $ANTLR start "rule__AtsyraGoalModel__Group_4__4__Impl"
    // InternalAtsyRAGoal.g:1290:1: rule__AtsyraGoalModel__Group_4__4__Impl : ( '}' ) ;
    public final void rule__AtsyraGoalModel__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1294:1: ( ( '}' ) )
            // InternalAtsyRAGoal.g:1295:1: ( '}' )
            {
            // InternalAtsyRAGoal.g:1295:1: ( '}' )
            // InternalAtsyRAGoal.g:1296:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_4_4()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_4_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_4__4__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__0"
    // InternalAtsyRAGoal.g:1306:1: rule__AtsyraGoalModel__Group_5__0 : rule__AtsyraGoalModel__Group_5__0__Impl rule__AtsyraGoalModel__Group_5__1 ;
    public final void rule__AtsyraGoalModel__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1310:1: ( rule__AtsyraGoalModel__Group_5__0__Impl rule__AtsyraGoalModel__Group_5__1 )
            // InternalAtsyRAGoal.g:1311:2: rule__AtsyraGoalModel__Group_5__0__Impl rule__AtsyraGoalModel__Group_5__1
            {
            pushFollow(FOLLOW_5);
            rule__AtsyraGoalModel__Group_5__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_5__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__0"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__0__Impl"
    // InternalAtsyRAGoal.g:1318:1: rule__AtsyraGoalModel__Group_5__0__Impl : ( 'types' ) ;
    public final void rule__AtsyraGoalModel__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1322:1: ( ( 'types' ) )
            // InternalAtsyRAGoal.g:1323:1: ( 'types' )
            {
            // InternalAtsyRAGoal.g:1323:1: ( 'types' )
            // InternalAtsyRAGoal.g:1324:2: 'types'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTypesKeyword_5_0()); 
            }
            match(input,24,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTypesKeyword_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__0__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__1"
    // InternalAtsyRAGoal.g:1333:1: rule__AtsyraGoalModel__Group_5__1 : rule__AtsyraGoalModel__Group_5__1__Impl rule__AtsyraGoalModel__Group_5__2 ;
    public final void rule__AtsyraGoalModel__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1337:1: ( rule__AtsyraGoalModel__Group_5__1__Impl rule__AtsyraGoalModel__Group_5__2 )
            // InternalAtsyRAGoal.g:1338:2: rule__AtsyraGoalModel__Group_5__1__Impl rule__AtsyraGoalModel__Group_5__2
            {
            pushFollow(FOLLOW_10);
            rule__AtsyraGoalModel__Group_5__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_5__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__1"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__1__Impl"
    // InternalAtsyRAGoal.g:1345:1: rule__AtsyraGoalModel__Group_5__1__Impl : ( '{' ) ;
    public final void rule__AtsyraGoalModel__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1349:1: ( ( '{' ) )
            // InternalAtsyRAGoal.g:1350:1: ( '{' )
            {
            // InternalAtsyRAGoal.g:1350:1: ( '{' )
            // InternalAtsyRAGoal.g:1351:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_5_1()); 
            }
            match(input,21,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_5_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__1__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__2"
    // InternalAtsyRAGoal.g:1360:1: rule__AtsyraGoalModel__Group_5__2 : rule__AtsyraGoalModel__Group_5__2__Impl rule__AtsyraGoalModel__Group_5__3 ;
    public final void rule__AtsyraGoalModel__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1364:1: ( rule__AtsyraGoalModel__Group_5__2__Impl rule__AtsyraGoalModel__Group_5__3 )
            // InternalAtsyRAGoal.g:1365:2: rule__AtsyraGoalModel__Group_5__2__Impl rule__AtsyraGoalModel__Group_5__3
            {
            pushFollow(FOLLOW_11);
            rule__AtsyraGoalModel__Group_5__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_5__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__2"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__2__Impl"
    // InternalAtsyRAGoal.g:1372:1: rule__AtsyraGoalModel__Group_5__2__Impl : ( ( rule__AtsyraGoalModel__TypesAssignment_5_2 ) ) ;
    public final void rule__AtsyraGoalModel__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1376:1: ( ( ( rule__AtsyraGoalModel__TypesAssignment_5_2 ) ) )
            // InternalAtsyRAGoal.g:1377:1: ( ( rule__AtsyraGoalModel__TypesAssignment_5_2 ) )
            {
            // InternalAtsyRAGoal.g:1377:1: ( ( rule__AtsyraGoalModel__TypesAssignment_5_2 ) )
            // InternalAtsyRAGoal.g:1378:2: ( rule__AtsyraGoalModel__TypesAssignment_5_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTypesAssignment_5_2()); 
            }
            // InternalAtsyRAGoal.g:1379:2: ( rule__AtsyraGoalModel__TypesAssignment_5_2 )
            // InternalAtsyRAGoal.g:1379:3: rule__AtsyraGoalModel__TypesAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__TypesAssignment_5_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTypesAssignment_5_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__2__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__3"
    // InternalAtsyRAGoal.g:1387:1: rule__AtsyraGoalModel__Group_5__3 : rule__AtsyraGoalModel__Group_5__3__Impl rule__AtsyraGoalModel__Group_5__4 ;
    public final void rule__AtsyraGoalModel__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1391:1: ( rule__AtsyraGoalModel__Group_5__3__Impl rule__AtsyraGoalModel__Group_5__4 )
            // InternalAtsyRAGoal.g:1392:2: rule__AtsyraGoalModel__Group_5__3__Impl rule__AtsyraGoalModel__Group_5__4
            {
            pushFollow(FOLLOW_11);
            rule__AtsyraGoalModel__Group_5__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_5__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__3"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__3__Impl"
    // InternalAtsyRAGoal.g:1399:1: rule__AtsyraGoalModel__Group_5__3__Impl : ( ( rule__AtsyraGoalModel__TypesAssignment_5_3 )* ) ;
    public final void rule__AtsyraGoalModel__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1403:1: ( ( ( rule__AtsyraGoalModel__TypesAssignment_5_3 )* ) )
            // InternalAtsyRAGoal.g:1404:1: ( ( rule__AtsyraGoalModel__TypesAssignment_5_3 )* )
            {
            // InternalAtsyRAGoal.g:1404:1: ( ( rule__AtsyraGoalModel__TypesAssignment_5_3 )* )
            // InternalAtsyRAGoal.g:1405:2: ( rule__AtsyraGoalModel__TypesAssignment_5_3 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTypesAssignment_5_3()); 
            }
            // InternalAtsyRAGoal.g:1406:2: ( rule__AtsyraGoalModel__TypesAssignment_5_3 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( ((LA20_0>=41 && LA20_0<=42)) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:1406:3: rule__AtsyraGoalModel__TypesAssignment_5_3
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__AtsyraGoalModel__TypesAssignment_5_3();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTypesAssignment_5_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__3__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__4"
    // InternalAtsyRAGoal.g:1414:1: rule__AtsyraGoalModel__Group_5__4 : rule__AtsyraGoalModel__Group_5__4__Impl ;
    public final void rule__AtsyraGoalModel__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1418:1: ( rule__AtsyraGoalModel__Group_5__4__Impl )
            // InternalAtsyRAGoal.g:1419:2: rule__AtsyraGoalModel__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_5__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__4"


    // $ANTLR start "rule__AtsyraGoalModel__Group_5__4__Impl"
    // InternalAtsyRAGoal.g:1425:1: rule__AtsyraGoalModel__Group_5__4__Impl : ( '}' ) ;
    public final void rule__AtsyraGoalModel__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1429:1: ( ( '}' ) )
            // InternalAtsyRAGoal.g:1430:1: ( '}' )
            {
            // InternalAtsyRAGoal.g:1430:1: ( '}' )
            // InternalAtsyRAGoal.g:1431:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_5_4()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_5_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_5__4__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__0"
    // InternalAtsyRAGoal.g:1441:1: rule__AtsyraGoalModel__Group_6__0 : rule__AtsyraGoalModel__Group_6__0__Impl rule__AtsyraGoalModel__Group_6__1 ;
    public final void rule__AtsyraGoalModel__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1445:1: ( rule__AtsyraGoalModel__Group_6__0__Impl rule__AtsyraGoalModel__Group_6__1 )
            // InternalAtsyRAGoal.g:1446:2: rule__AtsyraGoalModel__Group_6__0__Impl rule__AtsyraGoalModel__Group_6__1
            {
            pushFollow(FOLLOW_5);
            rule__AtsyraGoalModel__Group_6__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_6__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__0"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__0__Impl"
    // InternalAtsyRAGoal.g:1453:1: rule__AtsyraGoalModel__Group_6__0__Impl : ( 'features' ) ;
    public final void rule__AtsyraGoalModel__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1457:1: ( ( 'features' ) )
            // InternalAtsyRAGoal.g:1458:1: ( 'features' )
            {
            // InternalAtsyRAGoal.g:1458:1: ( 'features' )
            // InternalAtsyRAGoal.g:1459:2: 'features'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getFeaturesKeyword_6_0()); 
            }
            match(input,25,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getFeaturesKeyword_6_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__0__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__1"
    // InternalAtsyRAGoal.g:1468:1: rule__AtsyraGoalModel__Group_6__1 : rule__AtsyraGoalModel__Group_6__1__Impl rule__AtsyraGoalModel__Group_6__2 ;
    public final void rule__AtsyraGoalModel__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1472:1: ( rule__AtsyraGoalModel__Group_6__1__Impl rule__AtsyraGoalModel__Group_6__2 )
            // InternalAtsyRAGoal.g:1473:2: rule__AtsyraGoalModel__Group_6__1__Impl rule__AtsyraGoalModel__Group_6__2
            {
            pushFollow(FOLLOW_13);
            rule__AtsyraGoalModel__Group_6__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_6__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__1"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__1__Impl"
    // InternalAtsyRAGoal.g:1480:1: rule__AtsyraGoalModel__Group_6__1__Impl : ( '{' ) ;
    public final void rule__AtsyraGoalModel__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1484:1: ( ( '{' ) )
            // InternalAtsyRAGoal.g:1485:1: ( '{' )
            {
            // InternalAtsyRAGoal.g:1485:1: ( '{' )
            // InternalAtsyRAGoal.g:1486:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_6_1()); 
            }
            match(input,21,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_6_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__1__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__2"
    // InternalAtsyRAGoal.g:1495:1: rule__AtsyraGoalModel__Group_6__2 : rule__AtsyraGoalModel__Group_6__2__Impl rule__AtsyraGoalModel__Group_6__3 ;
    public final void rule__AtsyraGoalModel__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1499:1: ( rule__AtsyraGoalModel__Group_6__2__Impl rule__AtsyraGoalModel__Group_6__3 )
            // InternalAtsyRAGoal.g:1500:2: rule__AtsyraGoalModel__Group_6__2__Impl rule__AtsyraGoalModel__Group_6__3
            {
            pushFollow(FOLLOW_14);
            rule__AtsyraGoalModel__Group_6__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_6__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__2"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__2__Impl"
    // InternalAtsyRAGoal.g:1507:1: rule__AtsyraGoalModel__Group_6__2__Impl : ( ( rule__AtsyraGoalModel__TypedElementsAssignment_6_2 ) ) ;
    public final void rule__AtsyraGoalModel__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1511:1: ( ( ( rule__AtsyraGoalModel__TypedElementsAssignment_6_2 ) ) )
            // InternalAtsyRAGoal.g:1512:1: ( ( rule__AtsyraGoalModel__TypedElementsAssignment_6_2 ) )
            {
            // InternalAtsyRAGoal.g:1512:1: ( ( rule__AtsyraGoalModel__TypedElementsAssignment_6_2 ) )
            // InternalAtsyRAGoal.g:1513:2: ( rule__AtsyraGoalModel__TypedElementsAssignment_6_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsAssignment_6_2()); 
            }
            // InternalAtsyRAGoal.g:1514:2: ( rule__AtsyraGoalModel__TypedElementsAssignment_6_2 )
            // InternalAtsyRAGoal.g:1514:3: rule__AtsyraGoalModel__TypedElementsAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__TypedElementsAssignment_6_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsAssignment_6_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__2__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__3"
    // InternalAtsyRAGoal.g:1522:1: rule__AtsyraGoalModel__Group_6__3 : rule__AtsyraGoalModel__Group_6__3__Impl rule__AtsyraGoalModel__Group_6__4 ;
    public final void rule__AtsyraGoalModel__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1526:1: ( rule__AtsyraGoalModel__Group_6__3__Impl rule__AtsyraGoalModel__Group_6__4 )
            // InternalAtsyRAGoal.g:1527:2: rule__AtsyraGoalModel__Group_6__3__Impl rule__AtsyraGoalModel__Group_6__4
            {
            pushFollow(FOLLOW_14);
            rule__AtsyraGoalModel__Group_6__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_6__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__3"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__3__Impl"
    // InternalAtsyRAGoal.g:1534:1: rule__AtsyraGoalModel__Group_6__3__Impl : ( ( rule__AtsyraGoalModel__TypedElementsAssignment_6_3 )* ) ;
    public final void rule__AtsyraGoalModel__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1538:1: ( ( ( rule__AtsyraGoalModel__TypedElementsAssignment_6_3 )* ) )
            // InternalAtsyRAGoal.g:1539:1: ( ( rule__AtsyraGoalModel__TypedElementsAssignment_6_3 )* )
            {
            // InternalAtsyRAGoal.g:1539:1: ( ( rule__AtsyraGoalModel__TypedElementsAssignment_6_3 )* )
            // InternalAtsyRAGoal.g:1540:2: ( rule__AtsyraGoalModel__TypedElementsAssignment_6_3 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsAssignment_6_3()); 
            }
            // InternalAtsyRAGoal.g:1541:2: ( rule__AtsyraGoalModel__TypedElementsAssignment_6_3 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( ((LA21_0>=RULE_STRING && LA21_0<=RULE_ID)||LA21_0==32) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:1541:3: rule__AtsyraGoalModel__TypedElementsAssignment_6_3
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__AtsyraGoalModel__TypedElementsAssignment_6_3();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsAssignment_6_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__3__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__4"
    // InternalAtsyRAGoal.g:1549:1: rule__AtsyraGoalModel__Group_6__4 : rule__AtsyraGoalModel__Group_6__4__Impl ;
    public final void rule__AtsyraGoalModel__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1553:1: ( rule__AtsyraGoalModel__Group_6__4__Impl )
            // InternalAtsyRAGoal.g:1554:2: rule__AtsyraGoalModel__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_6__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__4"


    // $ANTLR start "rule__AtsyraGoalModel__Group_6__4__Impl"
    // InternalAtsyRAGoal.g:1560:1: rule__AtsyraGoalModel__Group_6__4__Impl : ( '}' ) ;
    public final void rule__AtsyraGoalModel__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1564:1: ( ( '}' ) )
            // InternalAtsyRAGoal.g:1565:1: ( '}' )
            {
            // InternalAtsyRAGoal.g:1565:1: ( '}' )
            // InternalAtsyRAGoal.g:1566:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_6_4()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_6_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_6__4__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__0"
    // InternalAtsyRAGoal.g:1576:1: rule__AtsyraGoalModel__Group_7__0 : rule__AtsyraGoalModel__Group_7__0__Impl rule__AtsyraGoalModel__Group_7__1 ;
    public final void rule__AtsyraGoalModel__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1580:1: ( rule__AtsyraGoalModel__Group_7__0__Impl rule__AtsyraGoalModel__Group_7__1 )
            // InternalAtsyRAGoal.g:1581:2: rule__AtsyraGoalModel__Group_7__0__Impl rule__AtsyraGoalModel__Group_7__1
            {
            pushFollow(FOLLOW_5);
            rule__AtsyraGoalModel__Group_7__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_7__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__0"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__0__Impl"
    // InternalAtsyRAGoal.g:1588:1: rule__AtsyraGoalModel__Group_7__0__Impl : ( 'atsyragoals' ) ;
    public final void rule__AtsyraGoalModel__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1592:1: ( ( 'atsyragoals' ) )
            // InternalAtsyRAGoal.g:1593:1: ( 'atsyragoals' )
            {
            // InternalAtsyRAGoal.g:1593:1: ( 'atsyragoals' )
            // InternalAtsyRAGoal.g:1594:2: 'atsyragoals'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsKeyword_7_0()); 
            }
            match(input,26,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsKeyword_7_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__0__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__1"
    // InternalAtsyRAGoal.g:1603:1: rule__AtsyraGoalModel__Group_7__1 : rule__AtsyraGoalModel__Group_7__1__Impl rule__AtsyraGoalModel__Group_7__2 ;
    public final void rule__AtsyraGoalModel__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1607:1: ( rule__AtsyraGoalModel__Group_7__1__Impl rule__AtsyraGoalModel__Group_7__2 )
            // InternalAtsyRAGoal.g:1608:2: rule__AtsyraGoalModel__Group_7__1__Impl rule__AtsyraGoalModel__Group_7__2
            {
            pushFollow(FOLLOW_16);
            rule__AtsyraGoalModel__Group_7__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_7__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__1"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__1__Impl"
    // InternalAtsyRAGoal.g:1615:1: rule__AtsyraGoalModel__Group_7__1__Impl : ( '{' ) ;
    public final void rule__AtsyraGoalModel__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1619:1: ( ( '{' ) )
            // InternalAtsyRAGoal.g:1620:1: ( '{' )
            {
            // InternalAtsyRAGoal.g:1620:1: ( '{' )
            // InternalAtsyRAGoal.g:1621:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_7_1()); 
            }
            match(input,21,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_7_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__1__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__2"
    // InternalAtsyRAGoal.g:1630:1: rule__AtsyraGoalModel__Group_7__2 : rule__AtsyraGoalModel__Group_7__2__Impl rule__AtsyraGoalModel__Group_7__3 ;
    public final void rule__AtsyraGoalModel__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1634:1: ( rule__AtsyraGoalModel__Group_7__2__Impl rule__AtsyraGoalModel__Group_7__3 )
            // InternalAtsyRAGoal.g:1635:2: rule__AtsyraGoalModel__Group_7__2__Impl rule__AtsyraGoalModel__Group_7__3
            {
            pushFollow(FOLLOW_16);
            rule__AtsyraGoalModel__Group_7__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_7__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__2"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__2__Impl"
    // InternalAtsyRAGoal.g:1642:1: rule__AtsyraGoalModel__Group_7__2__Impl : ( ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2 ) ) ;
    public final void rule__AtsyraGoalModel__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1646:1: ( ( ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2 ) ) )
            // InternalAtsyRAGoal.g:1647:1: ( ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2 ) )
            {
            // InternalAtsyRAGoal.g:1647:1: ( ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2 ) )
            // InternalAtsyRAGoal.g:1648:2: ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAssignment_7_2()); 
            }
            // InternalAtsyRAGoal.g:1649:2: ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2 )
            // InternalAtsyRAGoal.g:1649:3: rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAssignment_7_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__2__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__3"
    // InternalAtsyRAGoal.g:1657:1: rule__AtsyraGoalModel__Group_7__3 : rule__AtsyraGoalModel__Group_7__3__Impl rule__AtsyraGoalModel__Group_7__4 ;
    public final void rule__AtsyraGoalModel__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1661:1: ( rule__AtsyraGoalModel__Group_7__3__Impl rule__AtsyraGoalModel__Group_7__4 )
            // InternalAtsyRAGoal.g:1662:2: rule__AtsyraGoalModel__Group_7__3__Impl rule__AtsyraGoalModel__Group_7__4
            {
            pushFollow(FOLLOW_16);
            rule__AtsyraGoalModel__Group_7__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_7__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__3"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__3__Impl"
    // InternalAtsyRAGoal.g:1669:1: rule__AtsyraGoalModel__Group_7__3__Impl : ( ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3 )* ) ;
    public final void rule__AtsyraGoalModel__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1673:1: ( ( ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3 )* ) )
            // InternalAtsyRAGoal.g:1674:1: ( ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3 )* )
            {
            // InternalAtsyRAGoal.g:1674:1: ( ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3 )* )
            // InternalAtsyRAGoal.g:1675:2: ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAssignment_7_3()); 
            }
            // InternalAtsyRAGoal.g:1676:2: ( rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==22) ) {
                    int LA22_1 = input.LA(2);

                    if ( (LA22_1==22) ) {
                        int LA22_4 = input.LA(3);

                        if ( (LA22_4==EOF) ) {
                            int LA22_5 = input.LA(4);




                        }
                        else if ( (LA22_4==22||LA22_4==27||LA22_4==33||LA22_4==37) ) {
                            alt22=1;
                        }


                    }
                    else if ( (LA22_1==33||LA22_1==37) ) {
                        alt22=1;
                    }


                }
                else if ( (LA22_0==33||LA22_0==37) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:1676:3: rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3
            	    {
            	    pushFollow(FOLLOW_17);
            	    rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAssignment_7_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__3__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__4"
    // InternalAtsyRAGoal.g:1684:1: rule__AtsyraGoalModel__Group_7__4 : rule__AtsyraGoalModel__Group_7__4__Impl ;
    public final void rule__AtsyraGoalModel__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1688:1: ( rule__AtsyraGoalModel__Group_7__4__Impl )
            // InternalAtsyRAGoal.g:1689:2: rule__AtsyraGoalModel__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_7__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__4"


    // $ANTLR start "rule__AtsyraGoalModel__Group_7__4__Impl"
    // InternalAtsyRAGoal.g:1695:1: rule__AtsyraGoalModel__Group_7__4__Impl : ( '}' ) ;
    public final void rule__AtsyraGoalModel__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1699:1: ( ( '}' ) )
            // InternalAtsyRAGoal.g:1700:1: ( '}' )
            {
            // InternalAtsyRAGoal.g:1700:1: ( '}' )
            // InternalAtsyRAGoal.g:1701:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_7_4()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_7_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_7__4__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__0"
    // InternalAtsyRAGoal.g:1711:1: rule__AtsyraGoalModel__Group_8__0 : rule__AtsyraGoalModel__Group_8__0__Impl rule__AtsyraGoalModel__Group_8__1 ;
    public final void rule__AtsyraGoalModel__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1715:1: ( rule__AtsyraGoalModel__Group_8__0__Impl rule__AtsyraGoalModel__Group_8__1 )
            // InternalAtsyRAGoal.g:1716:2: rule__AtsyraGoalModel__Group_8__0__Impl rule__AtsyraGoalModel__Group_8__1
            {
            pushFollow(FOLLOW_5);
            rule__AtsyraGoalModel__Group_8__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_8__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__0"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__0__Impl"
    // InternalAtsyRAGoal.g:1723:1: rule__AtsyraGoalModel__Group_8__0__Impl : ( 'trees' ) ;
    public final void rule__AtsyraGoalModel__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1727:1: ( ( 'trees' ) )
            // InternalAtsyRAGoal.g:1728:1: ( 'trees' )
            {
            // InternalAtsyRAGoal.g:1728:1: ( 'trees' )
            // InternalAtsyRAGoal.g:1729:2: 'trees'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTreesKeyword_8_0()); 
            }
            match(input,27,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTreesKeyword_8_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__0__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__1"
    // InternalAtsyRAGoal.g:1738:1: rule__AtsyraGoalModel__Group_8__1 : rule__AtsyraGoalModel__Group_8__1__Impl rule__AtsyraGoalModel__Group_8__2 ;
    public final void rule__AtsyraGoalModel__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1742:1: ( rule__AtsyraGoalModel__Group_8__1__Impl rule__AtsyraGoalModel__Group_8__2 )
            // InternalAtsyRAGoal.g:1743:2: rule__AtsyraGoalModel__Group_8__1__Impl rule__AtsyraGoalModel__Group_8__2
            {
            pushFollow(FOLLOW_7);
            rule__AtsyraGoalModel__Group_8__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_8__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__1"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__1__Impl"
    // InternalAtsyRAGoal.g:1750:1: rule__AtsyraGoalModel__Group_8__1__Impl : ( '{' ) ;
    public final void rule__AtsyraGoalModel__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1754:1: ( ( '{' ) )
            // InternalAtsyRAGoal.g:1755:1: ( '{' )
            {
            // InternalAtsyRAGoal.g:1755:1: ( '{' )
            // InternalAtsyRAGoal.g:1756:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_8_1()); 
            }
            match(input,21,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_8_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__1__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__2"
    // InternalAtsyRAGoal.g:1765:1: rule__AtsyraGoalModel__Group_8__2 : rule__AtsyraGoalModel__Group_8__2__Impl rule__AtsyraGoalModel__Group_8__3 ;
    public final void rule__AtsyraGoalModel__Group_8__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1769:1: ( rule__AtsyraGoalModel__Group_8__2__Impl rule__AtsyraGoalModel__Group_8__3 )
            // InternalAtsyRAGoal.g:1770:2: rule__AtsyraGoalModel__Group_8__2__Impl rule__AtsyraGoalModel__Group_8__3
            {
            pushFollow(FOLLOW_8);
            rule__AtsyraGoalModel__Group_8__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_8__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__2"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__2__Impl"
    // InternalAtsyRAGoal.g:1777:1: rule__AtsyraGoalModel__Group_8__2__Impl : ( ( rule__AtsyraGoalModel__TreesAssignment_8_2 ) ) ;
    public final void rule__AtsyraGoalModel__Group_8__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1781:1: ( ( ( rule__AtsyraGoalModel__TreesAssignment_8_2 ) ) )
            // InternalAtsyRAGoal.g:1782:1: ( ( rule__AtsyraGoalModel__TreesAssignment_8_2 ) )
            {
            // InternalAtsyRAGoal.g:1782:1: ( ( rule__AtsyraGoalModel__TreesAssignment_8_2 ) )
            // InternalAtsyRAGoal.g:1783:2: ( rule__AtsyraGoalModel__TreesAssignment_8_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTreesAssignment_8_2()); 
            }
            // InternalAtsyRAGoal.g:1784:2: ( rule__AtsyraGoalModel__TreesAssignment_8_2 )
            // InternalAtsyRAGoal.g:1784:3: rule__AtsyraGoalModel__TreesAssignment_8_2
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__TreesAssignment_8_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTreesAssignment_8_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__2__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__3"
    // InternalAtsyRAGoal.g:1792:1: rule__AtsyraGoalModel__Group_8__3 : rule__AtsyraGoalModel__Group_8__3__Impl rule__AtsyraGoalModel__Group_8__4 ;
    public final void rule__AtsyraGoalModel__Group_8__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1796:1: ( rule__AtsyraGoalModel__Group_8__3__Impl rule__AtsyraGoalModel__Group_8__4 )
            // InternalAtsyRAGoal.g:1797:2: rule__AtsyraGoalModel__Group_8__3__Impl rule__AtsyraGoalModel__Group_8__4
            {
            pushFollow(FOLLOW_8);
            rule__AtsyraGoalModel__Group_8__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_8__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__3"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__3__Impl"
    // InternalAtsyRAGoal.g:1804:1: rule__AtsyraGoalModel__Group_8__3__Impl : ( ( rule__AtsyraGoalModel__TreesAssignment_8_3 )* ) ;
    public final void rule__AtsyraGoalModel__Group_8__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1808:1: ( ( ( rule__AtsyraGoalModel__TreesAssignment_8_3 )* ) )
            // InternalAtsyRAGoal.g:1809:1: ( ( rule__AtsyraGoalModel__TreesAssignment_8_3 )* )
            {
            // InternalAtsyRAGoal.g:1809:1: ( ( rule__AtsyraGoalModel__TreesAssignment_8_3 )* )
            // InternalAtsyRAGoal.g:1810:2: ( rule__AtsyraGoalModel__TreesAssignment_8_3 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTreesAssignment_8_3()); 
            }
            // InternalAtsyRAGoal.g:1811:2: ( rule__AtsyraGoalModel__TreesAssignment_8_3 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( ((LA23_0>=RULE_STRING && LA23_0<=RULE_ID)) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:1811:3: rule__AtsyraGoalModel__TreesAssignment_8_3
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__AtsyraGoalModel__TreesAssignment_8_3();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTreesAssignment_8_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__3__Impl"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__4"
    // InternalAtsyRAGoal.g:1819:1: rule__AtsyraGoalModel__Group_8__4 : rule__AtsyraGoalModel__Group_8__4__Impl ;
    public final void rule__AtsyraGoalModel__Group_8__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1823:1: ( rule__AtsyraGoalModel__Group_8__4__Impl )
            // InternalAtsyRAGoal.g:1824:2: rule__AtsyraGoalModel__Group_8__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoalModel__Group_8__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__4"


    // $ANTLR start "rule__AtsyraGoalModel__Group_8__4__Impl"
    // InternalAtsyRAGoal.g:1830:1: rule__AtsyraGoalModel__Group_8__4__Impl : ( '}' ) ;
    public final void rule__AtsyraGoalModel__Group_8__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1834:1: ( ( '}' ) )
            // InternalAtsyRAGoal.g:1835:1: ( '}' )
            {
            // InternalAtsyRAGoal.g:1835:1: ( '}' )
            // InternalAtsyRAGoal.g:1836:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_8_4()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_8_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__Group_8__4__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalAtsyRAGoal.g:1846:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1850:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalAtsyRAGoal.g:1851:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Import__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalAtsyRAGoal.g:1858:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1862:1: ( ( 'import' ) )
            // InternalAtsyRAGoal.g:1863:1: ( 'import' )
            {
            // InternalAtsyRAGoal.g:1863:1: ( 'import' )
            // InternalAtsyRAGoal.g:1864:2: 'import'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }
            match(input,28,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalAtsyRAGoal.g:1873:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1877:1: ( rule__Import__Group__1__Impl )
            // InternalAtsyRAGoal.g:1878:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalAtsyRAGoal.g:1884:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportURIAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1888:1: ( ( ( rule__Import__ImportURIAssignment_1 ) ) )
            // InternalAtsyRAGoal.g:1889:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            {
            // InternalAtsyRAGoal.g:1889:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            // InternalAtsyRAGoal.g:1890:2: ( rule__Import__ImportURIAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }
            // InternalAtsyRAGoal.g:1891:2: ( rule__Import__ImportURIAssignment_1 )
            // InternalAtsyRAGoal.g:1891:3: rule__Import__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportURIAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__OrCondition__Group__0"
    // InternalAtsyRAGoal.g:1900:1: rule__OrCondition__Group__0 : rule__OrCondition__Group__0__Impl rule__OrCondition__Group__1 ;
    public final void rule__OrCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1904:1: ( rule__OrCondition__Group__0__Impl rule__OrCondition__Group__1 )
            // InternalAtsyRAGoal.g:1905:2: rule__OrCondition__Group__0__Impl rule__OrCondition__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__OrCondition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__OrCondition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group__0"


    // $ANTLR start "rule__OrCondition__Group__0__Impl"
    // InternalAtsyRAGoal.g:1912:1: rule__OrCondition__Group__0__Impl : ( ruleAndCondition ) ;
    public final void rule__OrCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1916:1: ( ( ruleAndCondition ) )
            // InternalAtsyRAGoal.g:1917:1: ( ruleAndCondition )
            {
            // InternalAtsyRAGoal.g:1917:1: ( ruleAndCondition )
            // InternalAtsyRAGoal.g:1918:2: ruleAndCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrConditionAccess().getAndConditionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAndCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrConditionAccess().getAndConditionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group__0__Impl"


    // $ANTLR start "rule__OrCondition__Group__1"
    // InternalAtsyRAGoal.g:1927:1: rule__OrCondition__Group__1 : rule__OrCondition__Group__1__Impl ;
    public final void rule__OrCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1931:1: ( rule__OrCondition__Group__1__Impl )
            // InternalAtsyRAGoal.g:1932:2: rule__OrCondition__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrCondition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group__1"


    // $ANTLR start "rule__OrCondition__Group__1__Impl"
    // InternalAtsyRAGoal.g:1938:1: rule__OrCondition__Group__1__Impl : ( ( rule__OrCondition__Group_1__0 )* ) ;
    public final void rule__OrCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1942:1: ( ( ( rule__OrCondition__Group_1__0 )* ) )
            // InternalAtsyRAGoal.g:1943:1: ( ( rule__OrCondition__Group_1__0 )* )
            {
            // InternalAtsyRAGoal.g:1943:1: ( ( rule__OrCondition__Group_1__0 )* )
            // InternalAtsyRAGoal.g:1944:2: ( rule__OrCondition__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrConditionAccess().getGroup_1()); 
            }
            // InternalAtsyRAGoal.g:1945:2: ( rule__OrCondition__Group_1__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( ((LA24_0>=11 && LA24_0<=12)) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:1945:3: rule__OrCondition__Group_1__0
            	    {
            	    pushFollow(FOLLOW_19);
            	    rule__OrCondition__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrConditionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group__1__Impl"


    // $ANTLR start "rule__OrCondition__Group_1__0"
    // InternalAtsyRAGoal.g:1954:1: rule__OrCondition__Group_1__0 : rule__OrCondition__Group_1__0__Impl rule__OrCondition__Group_1__1 ;
    public final void rule__OrCondition__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1958:1: ( rule__OrCondition__Group_1__0__Impl rule__OrCondition__Group_1__1 )
            // InternalAtsyRAGoal.g:1959:2: rule__OrCondition__Group_1__0__Impl rule__OrCondition__Group_1__1
            {
            pushFollow(FOLLOW_18);
            rule__OrCondition__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__OrCondition__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group_1__0"


    // $ANTLR start "rule__OrCondition__Group_1__0__Impl"
    // InternalAtsyRAGoal.g:1966:1: rule__OrCondition__Group_1__0__Impl : ( () ) ;
    public final void rule__OrCondition__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1970:1: ( ( () ) )
            // InternalAtsyRAGoal.g:1971:1: ( () )
            {
            // InternalAtsyRAGoal.g:1971:1: ( () )
            // InternalAtsyRAGoal.g:1972:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrConditionAccess().getOrConditionOperandsAction_1_0()); 
            }
            // InternalAtsyRAGoal.g:1973:2: ()
            // InternalAtsyRAGoal.g:1973:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrConditionAccess().getOrConditionOperandsAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group_1__0__Impl"


    // $ANTLR start "rule__OrCondition__Group_1__1"
    // InternalAtsyRAGoal.g:1981:1: rule__OrCondition__Group_1__1 : rule__OrCondition__Group_1__1__Impl rule__OrCondition__Group_1__2 ;
    public final void rule__OrCondition__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1985:1: ( rule__OrCondition__Group_1__1__Impl rule__OrCondition__Group_1__2 )
            // InternalAtsyRAGoal.g:1986:2: rule__OrCondition__Group_1__1__Impl rule__OrCondition__Group_1__2
            {
            pushFollow(FOLLOW_20);
            rule__OrCondition__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__OrCondition__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group_1__1"


    // $ANTLR start "rule__OrCondition__Group_1__1__Impl"
    // InternalAtsyRAGoal.g:1993:1: rule__OrCondition__Group_1__1__Impl : ( ( rule__OrCondition__Alternatives_1_1 ) ) ;
    public final void rule__OrCondition__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:1997:1: ( ( ( rule__OrCondition__Alternatives_1_1 ) ) )
            // InternalAtsyRAGoal.g:1998:1: ( ( rule__OrCondition__Alternatives_1_1 ) )
            {
            // InternalAtsyRAGoal.g:1998:1: ( ( rule__OrCondition__Alternatives_1_1 ) )
            // InternalAtsyRAGoal.g:1999:2: ( rule__OrCondition__Alternatives_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrConditionAccess().getAlternatives_1_1()); 
            }
            // InternalAtsyRAGoal.g:2000:2: ( rule__OrCondition__Alternatives_1_1 )
            // InternalAtsyRAGoal.g:2000:3: rule__OrCondition__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__OrCondition__Alternatives_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrConditionAccess().getAlternatives_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group_1__1__Impl"


    // $ANTLR start "rule__OrCondition__Group_1__2"
    // InternalAtsyRAGoal.g:2008:1: rule__OrCondition__Group_1__2 : rule__OrCondition__Group_1__2__Impl ;
    public final void rule__OrCondition__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2012:1: ( rule__OrCondition__Group_1__2__Impl )
            // InternalAtsyRAGoal.g:2013:2: rule__OrCondition__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrCondition__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group_1__2"


    // $ANTLR start "rule__OrCondition__Group_1__2__Impl"
    // InternalAtsyRAGoal.g:2019:1: rule__OrCondition__Group_1__2__Impl : ( ( rule__OrCondition__OperandsAssignment_1_2 ) ) ;
    public final void rule__OrCondition__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2023:1: ( ( ( rule__OrCondition__OperandsAssignment_1_2 ) ) )
            // InternalAtsyRAGoal.g:2024:1: ( ( rule__OrCondition__OperandsAssignment_1_2 ) )
            {
            // InternalAtsyRAGoal.g:2024:1: ( ( rule__OrCondition__OperandsAssignment_1_2 ) )
            // InternalAtsyRAGoal.g:2025:2: ( rule__OrCondition__OperandsAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrConditionAccess().getOperandsAssignment_1_2()); 
            }
            // InternalAtsyRAGoal.g:2026:2: ( rule__OrCondition__OperandsAssignment_1_2 )
            // InternalAtsyRAGoal.g:2026:3: rule__OrCondition__OperandsAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__OrCondition__OperandsAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrConditionAccess().getOperandsAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__Group_1__2__Impl"


    // $ANTLR start "rule__AndCondition__Group__0"
    // InternalAtsyRAGoal.g:2035:1: rule__AndCondition__Group__0 : rule__AndCondition__Group__0__Impl rule__AndCondition__Group__1 ;
    public final void rule__AndCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2039:1: ( rule__AndCondition__Group__0__Impl rule__AndCondition__Group__1 )
            // InternalAtsyRAGoal.g:2040:2: rule__AndCondition__Group__0__Impl rule__AndCondition__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__AndCondition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AndCondition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group__0"


    // $ANTLR start "rule__AndCondition__Group__0__Impl"
    // InternalAtsyRAGoal.g:2047:1: rule__AndCondition__Group__0__Impl : ( ruleTerminalCondition ) ;
    public final void rule__AndCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2051:1: ( ( ruleTerminalCondition ) )
            // InternalAtsyRAGoal.g:2052:1: ( ruleTerminalCondition )
            {
            // InternalAtsyRAGoal.g:2052:1: ( ruleTerminalCondition )
            // InternalAtsyRAGoal.g:2053:2: ruleTerminalCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndConditionAccess().getTerminalConditionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleTerminalCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndConditionAccess().getTerminalConditionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group__0__Impl"


    // $ANTLR start "rule__AndCondition__Group__1"
    // InternalAtsyRAGoal.g:2062:1: rule__AndCondition__Group__1 : rule__AndCondition__Group__1__Impl ;
    public final void rule__AndCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2066:1: ( rule__AndCondition__Group__1__Impl )
            // InternalAtsyRAGoal.g:2067:2: rule__AndCondition__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndCondition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group__1"


    // $ANTLR start "rule__AndCondition__Group__1__Impl"
    // InternalAtsyRAGoal.g:2073:1: rule__AndCondition__Group__1__Impl : ( ( rule__AndCondition__Group_1__0 )* ) ;
    public final void rule__AndCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2077:1: ( ( ( rule__AndCondition__Group_1__0 )* ) )
            // InternalAtsyRAGoal.g:2078:1: ( ( rule__AndCondition__Group_1__0 )* )
            {
            // InternalAtsyRAGoal.g:2078:1: ( ( rule__AndCondition__Group_1__0 )* )
            // InternalAtsyRAGoal.g:2079:2: ( rule__AndCondition__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndConditionAccess().getGroup_1()); 
            }
            // InternalAtsyRAGoal.g:2080:2: ( rule__AndCondition__Group_1__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( ((LA25_0>=13 && LA25_0<=14)) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:2080:3: rule__AndCondition__Group_1__0
            	    {
            	    pushFollow(FOLLOW_22);
            	    rule__AndCondition__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndConditionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group__1__Impl"


    // $ANTLR start "rule__AndCondition__Group_1__0"
    // InternalAtsyRAGoal.g:2089:1: rule__AndCondition__Group_1__0 : rule__AndCondition__Group_1__0__Impl rule__AndCondition__Group_1__1 ;
    public final void rule__AndCondition__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2093:1: ( rule__AndCondition__Group_1__0__Impl rule__AndCondition__Group_1__1 )
            // InternalAtsyRAGoal.g:2094:2: rule__AndCondition__Group_1__0__Impl rule__AndCondition__Group_1__1
            {
            pushFollow(FOLLOW_21);
            rule__AndCondition__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AndCondition__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group_1__0"


    // $ANTLR start "rule__AndCondition__Group_1__0__Impl"
    // InternalAtsyRAGoal.g:2101:1: rule__AndCondition__Group_1__0__Impl : ( () ) ;
    public final void rule__AndCondition__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2105:1: ( ( () ) )
            // InternalAtsyRAGoal.g:2106:1: ( () )
            {
            // InternalAtsyRAGoal.g:2106:1: ( () )
            // InternalAtsyRAGoal.g:2107:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndConditionAccess().getAndConditionOperandsAction_1_0()); 
            }
            // InternalAtsyRAGoal.g:2108:2: ()
            // InternalAtsyRAGoal.g:2108:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndConditionAccess().getAndConditionOperandsAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group_1__0__Impl"


    // $ANTLR start "rule__AndCondition__Group_1__1"
    // InternalAtsyRAGoal.g:2116:1: rule__AndCondition__Group_1__1 : rule__AndCondition__Group_1__1__Impl rule__AndCondition__Group_1__2 ;
    public final void rule__AndCondition__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2120:1: ( rule__AndCondition__Group_1__1__Impl rule__AndCondition__Group_1__2 )
            // InternalAtsyRAGoal.g:2121:2: rule__AndCondition__Group_1__1__Impl rule__AndCondition__Group_1__2
            {
            pushFollow(FOLLOW_20);
            rule__AndCondition__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AndCondition__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group_1__1"


    // $ANTLR start "rule__AndCondition__Group_1__1__Impl"
    // InternalAtsyRAGoal.g:2128:1: rule__AndCondition__Group_1__1__Impl : ( ( rule__AndCondition__Alternatives_1_1 ) ) ;
    public final void rule__AndCondition__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2132:1: ( ( ( rule__AndCondition__Alternatives_1_1 ) ) )
            // InternalAtsyRAGoal.g:2133:1: ( ( rule__AndCondition__Alternatives_1_1 ) )
            {
            // InternalAtsyRAGoal.g:2133:1: ( ( rule__AndCondition__Alternatives_1_1 ) )
            // InternalAtsyRAGoal.g:2134:2: ( rule__AndCondition__Alternatives_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndConditionAccess().getAlternatives_1_1()); 
            }
            // InternalAtsyRAGoal.g:2135:2: ( rule__AndCondition__Alternatives_1_1 )
            // InternalAtsyRAGoal.g:2135:3: rule__AndCondition__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__AndCondition__Alternatives_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndConditionAccess().getAlternatives_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group_1__1__Impl"


    // $ANTLR start "rule__AndCondition__Group_1__2"
    // InternalAtsyRAGoal.g:2143:1: rule__AndCondition__Group_1__2 : rule__AndCondition__Group_1__2__Impl ;
    public final void rule__AndCondition__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2147:1: ( rule__AndCondition__Group_1__2__Impl )
            // InternalAtsyRAGoal.g:2148:2: rule__AndCondition__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndCondition__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group_1__2"


    // $ANTLR start "rule__AndCondition__Group_1__2__Impl"
    // InternalAtsyRAGoal.g:2154:1: rule__AndCondition__Group_1__2__Impl : ( ( rule__AndCondition__OperandsAssignment_1_2 ) ) ;
    public final void rule__AndCondition__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2158:1: ( ( ( rule__AndCondition__OperandsAssignment_1_2 ) ) )
            // InternalAtsyRAGoal.g:2159:1: ( ( rule__AndCondition__OperandsAssignment_1_2 ) )
            {
            // InternalAtsyRAGoal.g:2159:1: ( ( rule__AndCondition__OperandsAssignment_1_2 ) )
            // InternalAtsyRAGoal.g:2160:2: ( rule__AndCondition__OperandsAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndConditionAccess().getOperandsAssignment_1_2()); 
            }
            // InternalAtsyRAGoal.g:2161:2: ( rule__AndCondition__OperandsAssignment_1_2 )
            // InternalAtsyRAGoal.g:2161:3: rule__AndCondition__OperandsAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__AndCondition__OperandsAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndConditionAccess().getOperandsAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__Group_1__2__Impl"


    // $ANTLR start "rule__TerminalCondition__Group_0__0"
    // InternalAtsyRAGoal.g:2170:1: rule__TerminalCondition__Group_0__0 : rule__TerminalCondition__Group_0__0__Impl rule__TerminalCondition__Group_0__1 ;
    public final void rule__TerminalCondition__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2174:1: ( rule__TerminalCondition__Group_0__0__Impl rule__TerminalCondition__Group_0__1 )
            // InternalAtsyRAGoal.g:2175:2: rule__TerminalCondition__Group_0__0__Impl rule__TerminalCondition__Group_0__1
            {
            pushFollow(FOLLOW_20);
            rule__TerminalCondition__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__TerminalCondition__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_0__0"


    // $ANTLR start "rule__TerminalCondition__Group_0__0__Impl"
    // InternalAtsyRAGoal.g:2182:1: rule__TerminalCondition__Group_0__0__Impl : ( '(' ) ;
    public final void rule__TerminalCondition__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2186:1: ( ( '(' ) )
            // InternalAtsyRAGoal.g:2187:1: ( '(' )
            {
            // InternalAtsyRAGoal.g:2187:1: ( '(' )
            // InternalAtsyRAGoal.g:2188:2: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalConditionAccess().getLeftParenthesisKeyword_0_0()); 
            }
            match(input,29,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalConditionAccess().getLeftParenthesisKeyword_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_0__0__Impl"


    // $ANTLR start "rule__TerminalCondition__Group_0__1"
    // InternalAtsyRAGoal.g:2197:1: rule__TerminalCondition__Group_0__1 : rule__TerminalCondition__Group_0__1__Impl rule__TerminalCondition__Group_0__2 ;
    public final void rule__TerminalCondition__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2201:1: ( rule__TerminalCondition__Group_0__1__Impl rule__TerminalCondition__Group_0__2 )
            // InternalAtsyRAGoal.g:2202:2: rule__TerminalCondition__Group_0__1__Impl rule__TerminalCondition__Group_0__2
            {
            pushFollow(FOLLOW_23);
            rule__TerminalCondition__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__TerminalCondition__Group_0__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_0__1"


    // $ANTLR start "rule__TerminalCondition__Group_0__1__Impl"
    // InternalAtsyRAGoal.g:2209:1: rule__TerminalCondition__Group_0__1__Impl : ( ruleGoalCondition ) ;
    public final void rule__TerminalCondition__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2213:1: ( ( ruleGoalCondition ) )
            // InternalAtsyRAGoal.g:2214:1: ( ruleGoalCondition )
            {
            // InternalAtsyRAGoal.g:2214:1: ( ruleGoalCondition )
            // InternalAtsyRAGoal.g:2215:2: ruleGoalCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalConditionAccess().getGoalConditionParserRuleCall_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleGoalCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalConditionAccess().getGoalConditionParserRuleCall_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_0__1__Impl"


    // $ANTLR start "rule__TerminalCondition__Group_0__2"
    // InternalAtsyRAGoal.g:2224:1: rule__TerminalCondition__Group_0__2 : rule__TerminalCondition__Group_0__2__Impl ;
    public final void rule__TerminalCondition__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2228:1: ( rule__TerminalCondition__Group_0__2__Impl )
            // InternalAtsyRAGoal.g:2229:2: rule__TerminalCondition__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TerminalCondition__Group_0__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_0__2"


    // $ANTLR start "rule__TerminalCondition__Group_0__2__Impl"
    // InternalAtsyRAGoal.g:2235:1: rule__TerminalCondition__Group_0__2__Impl : ( ')' ) ;
    public final void rule__TerminalCondition__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2239:1: ( ( ')' ) )
            // InternalAtsyRAGoal.g:2240:1: ( ')' )
            {
            // InternalAtsyRAGoal.g:2240:1: ( ')' )
            // InternalAtsyRAGoal.g:2241:2: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalConditionAccess().getRightParenthesisKeyword_0_2()); 
            }
            match(input,30,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalConditionAccess().getRightParenthesisKeyword_0_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_0__2__Impl"


    // $ANTLR start "rule__TerminalCondition__Group_1__0"
    // InternalAtsyRAGoal.g:2251:1: rule__TerminalCondition__Group_1__0 : rule__TerminalCondition__Group_1__0__Impl rule__TerminalCondition__Group_1__1 ;
    public final void rule__TerminalCondition__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2255:1: ( rule__TerminalCondition__Group_1__0__Impl rule__TerminalCondition__Group_1__1 )
            // InternalAtsyRAGoal.g:2256:2: rule__TerminalCondition__Group_1__0__Impl rule__TerminalCondition__Group_1__1
            {
            pushFollow(FOLLOW_24);
            rule__TerminalCondition__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__TerminalCondition__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_1__0"


    // $ANTLR start "rule__TerminalCondition__Group_1__0__Impl"
    // InternalAtsyRAGoal.g:2263:1: rule__TerminalCondition__Group_1__0__Impl : ( () ) ;
    public final void rule__TerminalCondition__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2267:1: ( ( () ) )
            // InternalAtsyRAGoal.g:2268:1: ( () )
            {
            // InternalAtsyRAGoal.g:2268:1: ( () )
            // InternalAtsyRAGoal.g:2269:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalConditionAccess().getNotConditionAction_1_0()); 
            }
            // InternalAtsyRAGoal.g:2270:2: ()
            // InternalAtsyRAGoal.g:2270:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalConditionAccess().getNotConditionAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_1__0__Impl"


    // $ANTLR start "rule__TerminalCondition__Group_1__1"
    // InternalAtsyRAGoal.g:2278:1: rule__TerminalCondition__Group_1__1 : rule__TerminalCondition__Group_1__1__Impl rule__TerminalCondition__Group_1__2 ;
    public final void rule__TerminalCondition__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2282:1: ( rule__TerminalCondition__Group_1__1__Impl rule__TerminalCondition__Group_1__2 )
            // InternalAtsyRAGoal.g:2283:2: rule__TerminalCondition__Group_1__1__Impl rule__TerminalCondition__Group_1__2
            {
            pushFollow(FOLLOW_20);
            rule__TerminalCondition__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__TerminalCondition__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_1__1"


    // $ANTLR start "rule__TerminalCondition__Group_1__1__Impl"
    // InternalAtsyRAGoal.g:2290:1: rule__TerminalCondition__Group_1__1__Impl : ( ( rule__TerminalCondition__Alternatives_1_1 ) ) ;
    public final void rule__TerminalCondition__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2294:1: ( ( ( rule__TerminalCondition__Alternatives_1_1 ) ) )
            // InternalAtsyRAGoal.g:2295:1: ( ( rule__TerminalCondition__Alternatives_1_1 ) )
            {
            // InternalAtsyRAGoal.g:2295:1: ( ( rule__TerminalCondition__Alternatives_1_1 ) )
            // InternalAtsyRAGoal.g:2296:2: ( rule__TerminalCondition__Alternatives_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalConditionAccess().getAlternatives_1_1()); 
            }
            // InternalAtsyRAGoal.g:2297:2: ( rule__TerminalCondition__Alternatives_1_1 )
            // InternalAtsyRAGoal.g:2297:3: rule__TerminalCondition__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__TerminalCondition__Alternatives_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalConditionAccess().getAlternatives_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_1__1__Impl"


    // $ANTLR start "rule__TerminalCondition__Group_1__2"
    // InternalAtsyRAGoal.g:2305:1: rule__TerminalCondition__Group_1__2 : rule__TerminalCondition__Group_1__2__Impl ;
    public final void rule__TerminalCondition__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2309:1: ( rule__TerminalCondition__Group_1__2__Impl )
            // InternalAtsyRAGoal.g:2310:2: rule__TerminalCondition__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TerminalCondition__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_1__2"


    // $ANTLR start "rule__TerminalCondition__Group_1__2__Impl"
    // InternalAtsyRAGoal.g:2316:1: rule__TerminalCondition__Group_1__2__Impl : ( ( rule__TerminalCondition__OperandsAssignment_1_2 ) ) ;
    public final void rule__TerminalCondition__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2320:1: ( ( ( rule__TerminalCondition__OperandsAssignment_1_2 ) ) )
            // InternalAtsyRAGoal.g:2321:1: ( ( rule__TerminalCondition__OperandsAssignment_1_2 ) )
            {
            // InternalAtsyRAGoal.g:2321:1: ( ( rule__TerminalCondition__OperandsAssignment_1_2 ) )
            // InternalAtsyRAGoal.g:2322:2: ( rule__TerminalCondition__OperandsAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalConditionAccess().getOperandsAssignment_1_2()); 
            }
            // InternalAtsyRAGoal.g:2323:2: ( rule__TerminalCondition__OperandsAssignment_1_2 )
            // InternalAtsyRAGoal.g:2323:3: rule__TerminalCondition__OperandsAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__TerminalCondition__OperandsAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalConditionAccess().getOperandsAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__Group_1__2__Impl"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_0__0"
    // InternalAtsyRAGoal.g:2332:1: rule__EqualOrBooleanCond__Group_0__0 : rule__EqualOrBooleanCond__Group_0__0__Impl rule__EqualOrBooleanCond__Group_0__1 ;
    public final void rule__EqualOrBooleanCond__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2336:1: ( rule__EqualOrBooleanCond__Group_0__0__Impl rule__EqualOrBooleanCond__Group_0__1 )
            // InternalAtsyRAGoal.g:2337:2: rule__EqualOrBooleanCond__Group_0__0__Impl rule__EqualOrBooleanCond__Group_0__1
            {
            pushFollow(FOLLOW_25);
            rule__EqualOrBooleanCond__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_0__0"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_0__0__Impl"
    // InternalAtsyRAGoal.g:2344:1: rule__EqualOrBooleanCond__Group_0__0__Impl : ( () ) ;
    public final void rule__EqualOrBooleanCond__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2348:1: ( ( () ) )
            // InternalAtsyRAGoal.g:2349:1: ( () )
            {
            // InternalAtsyRAGoal.g:2349:1: ( () )
            // InternalAtsyRAGoal.g:2350:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getBooleanSystemConditionAction_0_0()); 
            }
            // InternalAtsyRAGoal.g:2351:2: ()
            // InternalAtsyRAGoal.g:2351:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getBooleanSystemConditionAction_0_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_0__0__Impl"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_0__1"
    // InternalAtsyRAGoal.g:2359:1: rule__EqualOrBooleanCond__Group_0__1 : rule__EqualOrBooleanCond__Group_0__1__Impl ;
    public final void rule__EqualOrBooleanCond__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2363:1: ( rule__EqualOrBooleanCond__Group_0__1__Impl )
            // InternalAtsyRAGoal.g:2364:2: rule__EqualOrBooleanCond__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_0__1"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_0__1__Impl"
    // InternalAtsyRAGoal.g:2370:1: rule__EqualOrBooleanCond__Group_0__1__Impl : ( ( rule__EqualOrBooleanCond__SourceAssignment_0_1 ) ) ;
    public final void rule__EqualOrBooleanCond__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2374:1: ( ( ( rule__EqualOrBooleanCond__SourceAssignment_0_1 ) ) )
            // InternalAtsyRAGoal.g:2375:1: ( ( rule__EqualOrBooleanCond__SourceAssignment_0_1 ) )
            {
            // InternalAtsyRAGoal.g:2375:1: ( ( rule__EqualOrBooleanCond__SourceAssignment_0_1 ) )
            // InternalAtsyRAGoal.g:2376:2: ( rule__EqualOrBooleanCond__SourceAssignment_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getSourceAssignment_0_1()); 
            }
            // InternalAtsyRAGoal.g:2377:2: ( rule__EqualOrBooleanCond__SourceAssignment_0_1 )
            // InternalAtsyRAGoal.g:2377:3: rule__EqualOrBooleanCond__SourceAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__SourceAssignment_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getSourceAssignment_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_0__1__Impl"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1__0"
    // InternalAtsyRAGoal.g:2386:1: rule__EqualOrBooleanCond__Group_1__0 : rule__EqualOrBooleanCond__Group_1__0__Impl rule__EqualOrBooleanCond__Group_1__1 ;
    public final void rule__EqualOrBooleanCond__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2390:1: ( rule__EqualOrBooleanCond__Group_1__0__Impl rule__EqualOrBooleanCond__Group_1__1 )
            // InternalAtsyRAGoal.g:2391:2: rule__EqualOrBooleanCond__Group_1__0__Impl rule__EqualOrBooleanCond__Group_1__1
            {
            pushFollow(FOLLOW_20);
            rule__EqualOrBooleanCond__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1__0"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1__0__Impl"
    // InternalAtsyRAGoal.g:2398:1: rule__EqualOrBooleanCond__Group_1__0__Impl : ( () ) ;
    public final void rule__EqualOrBooleanCond__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2402:1: ( ( () ) )
            // InternalAtsyRAGoal.g:2403:1: ( () )
            {
            // InternalAtsyRAGoal.g:2403:1: ( () )
            // InternalAtsyRAGoal.g:2404:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getEqualsConditionAction_1_0()); 
            }
            // InternalAtsyRAGoal.g:2405:2: ()
            // InternalAtsyRAGoal.g:2405:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getEqualsConditionAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1__0__Impl"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1__1"
    // InternalAtsyRAGoal.g:2413:1: rule__EqualOrBooleanCond__Group_1__1 : rule__EqualOrBooleanCond__Group_1__1__Impl rule__EqualOrBooleanCond__Group_1__2 ;
    public final void rule__EqualOrBooleanCond__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2417:1: ( rule__EqualOrBooleanCond__Group_1__1__Impl rule__EqualOrBooleanCond__Group_1__2 )
            // InternalAtsyRAGoal.g:2418:2: rule__EqualOrBooleanCond__Group_1__1__Impl rule__EqualOrBooleanCond__Group_1__2
            {
            pushFollow(FOLLOW_26);
            rule__EqualOrBooleanCond__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1__1"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1__1__Impl"
    // InternalAtsyRAGoal.g:2425:1: rule__EqualOrBooleanCond__Group_1__1__Impl : ( ( rule__EqualOrBooleanCond__SourceAssignment_1_1 ) ) ;
    public final void rule__EqualOrBooleanCond__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2429:1: ( ( ( rule__EqualOrBooleanCond__SourceAssignment_1_1 ) ) )
            // InternalAtsyRAGoal.g:2430:1: ( ( rule__EqualOrBooleanCond__SourceAssignment_1_1 ) )
            {
            // InternalAtsyRAGoal.g:2430:1: ( ( rule__EqualOrBooleanCond__SourceAssignment_1_1 ) )
            // InternalAtsyRAGoal.g:2431:2: ( rule__EqualOrBooleanCond__SourceAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getSourceAssignment_1_1()); 
            }
            // InternalAtsyRAGoal.g:2432:2: ( rule__EqualOrBooleanCond__SourceAssignment_1_1 )
            // InternalAtsyRAGoal.g:2432:3: rule__EqualOrBooleanCond__SourceAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__SourceAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getSourceAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1__1__Impl"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1__2"
    // InternalAtsyRAGoal.g:2440:1: rule__EqualOrBooleanCond__Group_1__2 : rule__EqualOrBooleanCond__Group_1__2__Impl ;
    public final void rule__EqualOrBooleanCond__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2444:1: ( rule__EqualOrBooleanCond__Group_1__2__Impl )
            // InternalAtsyRAGoal.g:2445:2: rule__EqualOrBooleanCond__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1__2"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1__2__Impl"
    // InternalAtsyRAGoal.g:2451:1: rule__EqualOrBooleanCond__Group_1__2__Impl : ( ( rule__EqualOrBooleanCond__Group_1_2__0 ) ) ;
    public final void rule__EqualOrBooleanCond__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2455:1: ( ( ( rule__EqualOrBooleanCond__Group_1_2__0 ) ) )
            // InternalAtsyRAGoal.g:2456:1: ( ( rule__EqualOrBooleanCond__Group_1_2__0 ) )
            {
            // InternalAtsyRAGoal.g:2456:1: ( ( rule__EqualOrBooleanCond__Group_1_2__0 ) )
            // InternalAtsyRAGoal.g:2457:2: ( rule__EqualOrBooleanCond__Group_1_2__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getGroup_1_2()); 
            }
            // InternalAtsyRAGoal.g:2458:2: ( rule__EqualOrBooleanCond__Group_1_2__0 )
            // InternalAtsyRAGoal.g:2458:3: rule__EqualOrBooleanCond__Group_1_2__0
            {
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__Group_1_2__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getGroup_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1__2__Impl"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1_2__0"
    // InternalAtsyRAGoal.g:2467:1: rule__EqualOrBooleanCond__Group_1_2__0 : rule__EqualOrBooleanCond__Group_1_2__0__Impl rule__EqualOrBooleanCond__Group_1_2__1 ;
    public final void rule__EqualOrBooleanCond__Group_1_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2471:1: ( rule__EqualOrBooleanCond__Group_1_2__0__Impl rule__EqualOrBooleanCond__Group_1_2__1 )
            // InternalAtsyRAGoal.g:2472:2: rule__EqualOrBooleanCond__Group_1_2__0__Impl rule__EqualOrBooleanCond__Group_1_2__1
            {
            pushFollow(FOLLOW_25);
            rule__EqualOrBooleanCond__Group_1_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__Group_1_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1_2__0"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1_2__0__Impl"
    // InternalAtsyRAGoal.g:2479:1: rule__EqualOrBooleanCond__Group_1_2__0__Impl : ( ( '=' ) ) ;
    public final void rule__EqualOrBooleanCond__Group_1_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2483:1: ( ( ( '=' ) ) )
            // InternalAtsyRAGoal.g:2484:1: ( ( '=' ) )
            {
            // InternalAtsyRAGoal.g:2484:1: ( ( '=' ) )
            // InternalAtsyRAGoal.g:2485:2: ( '=' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getEqualsSignKeyword_1_2_0()); 
            }
            // InternalAtsyRAGoal.g:2486:2: ( '=' )
            // InternalAtsyRAGoal.g:2486:3: '='
            {
            match(input,31,FOLLOW_2); if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getEqualsSignKeyword_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1_2__0__Impl"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1_2__1"
    // InternalAtsyRAGoal.g:2494:1: rule__EqualOrBooleanCond__Group_1_2__1 : rule__EqualOrBooleanCond__Group_1_2__1__Impl ;
    public final void rule__EqualOrBooleanCond__Group_1_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2498:1: ( rule__EqualOrBooleanCond__Group_1_2__1__Impl )
            // InternalAtsyRAGoal.g:2499:2: rule__EqualOrBooleanCond__Group_1_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__Group_1_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1_2__1"


    // $ANTLR start "rule__EqualOrBooleanCond__Group_1_2__1__Impl"
    // InternalAtsyRAGoal.g:2505:1: rule__EqualOrBooleanCond__Group_1_2__1__Impl : ( ( rule__EqualOrBooleanCond__TargetAssignment_1_2_1 ) ) ;
    public final void rule__EqualOrBooleanCond__Group_1_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2509:1: ( ( ( rule__EqualOrBooleanCond__TargetAssignment_1_2_1 ) ) )
            // InternalAtsyRAGoal.g:2510:1: ( ( rule__EqualOrBooleanCond__TargetAssignment_1_2_1 ) )
            {
            // InternalAtsyRAGoal.g:2510:1: ( ( rule__EqualOrBooleanCond__TargetAssignment_1_2_1 ) )
            // InternalAtsyRAGoal.g:2511:2: ( rule__EqualOrBooleanCond__TargetAssignment_1_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getTargetAssignment_1_2_1()); 
            }
            // InternalAtsyRAGoal.g:2512:2: ( rule__EqualOrBooleanCond__TargetAssignment_1_2_1 )
            // InternalAtsyRAGoal.g:2512:3: rule__EqualOrBooleanCond__TargetAssignment_1_2_1
            {
            pushFollow(FOLLOW_2);
            rule__EqualOrBooleanCond__TargetAssignment_1_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getTargetAssignment_1_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__Group_1_2__1__Impl"


    // $ANTLR start "rule__TypedElementDecl__Group_0__0"
    // InternalAtsyRAGoal.g:2521:1: rule__TypedElementDecl__Group_0__0 : rule__TypedElementDecl__Group_0__0__Impl rule__TypedElementDecl__Group_0__1 ;
    public final void rule__TypedElementDecl__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2525:1: ( rule__TypedElementDecl__Group_0__0__Impl rule__TypedElementDecl__Group_0__1 )
            // InternalAtsyRAGoal.g:2526:2: rule__TypedElementDecl__Group_0__0__Impl rule__TypedElementDecl__Group_0__1
            {
            pushFollow(FOLLOW_27);
            rule__TypedElementDecl__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__TypedElementDecl__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementDecl__Group_0__0"


    // $ANTLR start "rule__TypedElementDecl__Group_0__0__Impl"
    // InternalAtsyRAGoal.g:2533:1: rule__TypedElementDecl__Group_0__0__Impl : ( 'const' ) ;
    public final void rule__TypedElementDecl__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2537:1: ( ( 'const' ) )
            // InternalAtsyRAGoal.g:2538:1: ( 'const' )
            {
            // InternalAtsyRAGoal.g:2538:1: ( 'const' )
            // InternalAtsyRAGoal.g:2539:2: 'const'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedElementDeclAccess().getConstKeyword_0_0()); 
            }
            match(input,32,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedElementDeclAccess().getConstKeyword_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementDecl__Group_0__0__Impl"


    // $ANTLR start "rule__TypedElementDecl__Group_0__1"
    // InternalAtsyRAGoal.g:2548:1: rule__TypedElementDecl__Group_0__1 : rule__TypedElementDecl__Group_0__1__Impl ;
    public final void rule__TypedElementDecl__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2552:1: ( rule__TypedElementDecl__Group_0__1__Impl )
            // InternalAtsyRAGoal.g:2553:2: rule__TypedElementDecl__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypedElementDecl__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementDecl__Group_0__1"


    // $ANTLR start "rule__TypedElementDecl__Group_0__1__Impl"
    // InternalAtsyRAGoal.g:2559:1: rule__TypedElementDecl__Group_0__1__Impl : ( ruleBooleanLiteral ) ;
    public final void rule__TypedElementDecl__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2563:1: ( ( ruleBooleanLiteral ) )
            // InternalAtsyRAGoal.g:2564:1: ( ruleBooleanLiteral )
            {
            // InternalAtsyRAGoal.g:2564:1: ( ruleBooleanLiteral )
            // InternalAtsyRAGoal.g:2565:2: ruleBooleanLiteral
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedElementDeclAccess().getBooleanLiteralParserRuleCall_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleBooleanLiteral();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedElementDeclAccess().getBooleanLiteralParserRuleCall_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementDecl__Group_0__1__Impl"


    // $ANTLR start "rule__TypedElementDecl__Group_2__0"
    // InternalAtsyRAGoal.g:2575:1: rule__TypedElementDecl__Group_2__0 : rule__TypedElementDecl__Group_2__0__Impl rule__TypedElementDecl__Group_2__1 ;
    public final void rule__TypedElementDecl__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2579:1: ( rule__TypedElementDecl__Group_2__0__Impl rule__TypedElementDecl__Group_2__1 )
            // InternalAtsyRAGoal.g:2580:2: rule__TypedElementDecl__Group_2__0__Impl rule__TypedElementDecl__Group_2__1
            {
            pushFollow(FOLLOW_7);
            rule__TypedElementDecl__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__TypedElementDecl__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementDecl__Group_2__0"


    // $ANTLR start "rule__TypedElementDecl__Group_2__0__Impl"
    // InternalAtsyRAGoal.g:2587:1: rule__TypedElementDecl__Group_2__0__Impl : ( 'const' ) ;
    public final void rule__TypedElementDecl__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2591:1: ( ( 'const' ) )
            // InternalAtsyRAGoal.g:2592:1: ( 'const' )
            {
            // InternalAtsyRAGoal.g:2592:1: ( 'const' )
            // InternalAtsyRAGoal.g:2593:2: 'const'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedElementDeclAccess().getConstKeyword_2_0()); 
            }
            match(input,32,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedElementDeclAccess().getConstKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementDecl__Group_2__0__Impl"


    // $ANTLR start "rule__TypedElementDecl__Group_2__1"
    // InternalAtsyRAGoal.g:2602:1: rule__TypedElementDecl__Group_2__1 : rule__TypedElementDecl__Group_2__1__Impl ;
    public final void rule__TypedElementDecl__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2606:1: ( rule__TypedElementDecl__Group_2__1__Impl )
            // InternalAtsyRAGoal.g:2607:2: rule__TypedElementDecl__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypedElementDecl__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementDecl__Group_2__1"


    // $ANTLR start "rule__TypedElementDecl__Group_2__1__Impl"
    // InternalAtsyRAGoal.g:2613:1: rule__TypedElementDecl__Group_2__1__Impl : ( ruleBuildingSystemConstFeature ) ;
    public final void rule__TypedElementDecl__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2617:1: ( ( ruleBuildingSystemConstFeature ) )
            // InternalAtsyRAGoal.g:2618:1: ( ruleBuildingSystemConstFeature )
            {
            // InternalAtsyRAGoal.g:2618:1: ( ruleBuildingSystemConstFeature )
            // InternalAtsyRAGoal.g:2619:2: ruleBuildingSystemConstFeature
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedElementDeclAccess().getBuildingSystemConstFeatureParserRuleCall_2_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleBuildingSystemConstFeature();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedElementDeclAccess().getBuildingSystemConstFeatureParserRuleCall_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedElementDecl__Group_2__1__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0__0"
    // InternalAtsyRAGoal.g:2629:1: rule__AtsyraGoal__Group_0__0 : rule__AtsyraGoal__Group_0__0__Impl rule__AtsyraGoal__Group_0__1 ;
    public final void rule__AtsyraGoal__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2633:1: ( rule__AtsyraGoal__Group_0__0__Impl rule__AtsyraGoal__Group_0__1 )
            // InternalAtsyRAGoal.g:2634:2: rule__AtsyraGoal__Group_0__0__Impl rule__AtsyraGoal__Group_0__1
            {
            pushFollow(FOLLOW_7);
            rule__AtsyraGoal__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0__0"


    // $ANTLR start "rule__AtsyraGoal__Group_0__0__Impl"
    // InternalAtsyRAGoal.g:2641:1: rule__AtsyraGoal__Group_0__0__Impl : ( 'Goal' ) ;
    public final void rule__AtsyraGoal__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2645:1: ( ( 'Goal' ) )
            // InternalAtsyRAGoal.g:2646:1: ( 'Goal' )
            {
            // InternalAtsyRAGoal.g:2646:1: ( 'Goal' )
            // InternalAtsyRAGoal.g:2647:2: 'Goal'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getGoalKeyword_0_0()); 
            }
            match(input,33,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getGoalKeyword_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0__0__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0__1"
    // InternalAtsyRAGoal.g:2656:1: rule__AtsyraGoal__Group_0__1 : rule__AtsyraGoal__Group_0__1__Impl rule__AtsyraGoal__Group_0__2 ;
    public final void rule__AtsyraGoal__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2660:1: ( rule__AtsyraGoal__Group_0__1__Impl rule__AtsyraGoal__Group_0__2 )
            // InternalAtsyRAGoal.g:2661:2: rule__AtsyraGoal__Group_0__1__Impl rule__AtsyraGoal__Group_0__2
            {
            pushFollow(FOLLOW_5);
            rule__AtsyraGoal__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0__1"


    // $ANTLR start "rule__AtsyraGoal__Group_0__1__Impl"
    // InternalAtsyRAGoal.g:2668:1: rule__AtsyraGoal__Group_0__1__Impl : ( ( rule__AtsyraGoal__NameAssignment_0_1 ) ) ;
    public final void rule__AtsyraGoal__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2672:1: ( ( ( rule__AtsyraGoal__NameAssignment_0_1 ) ) )
            // InternalAtsyRAGoal.g:2673:1: ( ( rule__AtsyraGoal__NameAssignment_0_1 ) )
            {
            // InternalAtsyRAGoal.g:2673:1: ( ( rule__AtsyraGoal__NameAssignment_0_1 ) )
            // InternalAtsyRAGoal.g:2674:2: ( rule__AtsyraGoal__NameAssignment_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getNameAssignment_0_1()); 
            }
            // InternalAtsyRAGoal.g:2675:2: ( rule__AtsyraGoal__NameAssignment_0_1 )
            // InternalAtsyRAGoal.g:2675:3: rule__AtsyraGoal__NameAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__NameAssignment_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getNameAssignment_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0__1__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0__2"
    // InternalAtsyRAGoal.g:2683:1: rule__AtsyraGoal__Group_0__2 : rule__AtsyraGoal__Group_0__2__Impl rule__AtsyraGoal__Group_0__3 ;
    public final void rule__AtsyraGoal__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2687:1: ( rule__AtsyraGoal__Group_0__2__Impl rule__AtsyraGoal__Group_0__3 )
            // InternalAtsyRAGoal.g:2688:2: rule__AtsyraGoal__Group_0__2__Impl rule__AtsyraGoal__Group_0__3
            {
            pushFollow(FOLLOW_28);
            rule__AtsyraGoal__Group_0__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0__2"


    // $ANTLR start "rule__AtsyraGoal__Group_0__2__Impl"
    // InternalAtsyRAGoal.g:2695:1: rule__AtsyraGoal__Group_0__2__Impl : ( '{' ) ;
    public final void rule__AtsyraGoal__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2699:1: ( ( '{' ) )
            // InternalAtsyRAGoal.g:2700:1: ( '{' )
            {
            // InternalAtsyRAGoal.g:2700:1: ( '{' )
            // InternalAtsyRAGoal.g:2701:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getLeftCurlyBracketKeyword_0_2()); 
            }
            match(input,21,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getLeftCurlyBracketKeyword_0_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0__2__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0__3"
    // InternalAtsyRAGoal.g:2710:1: rule__AtsyraGoal__Group_0__3 : rule__AtsyraGoal__Group_0__3__Impl ;
    public final void rule__AtsyraGoal__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2714:1: ( rule__AtsyraGoal__Group_0__3__Impl )
            // InternalAtsyRAGoal.g:2715:2: rule__AtsyraGoal__Group_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0__3"


    // $ANTLR start "rule__AtsyraGoal__Group_0__3__Impl"
    // InternalAtsyRAGoal.g:2721:1: rule__AtsyraGoal__Group_0__3__Impl : ( ( rule__AtsyraGoal__Group_0_3__0 )? ) ;
    public final void rule__AtsyraGoal__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2725:1: ( ( ( rule__AtsyraGoal__Group_0_3__0 )? ) )
            // InternalAtsyRAGoal.g:2726:1: ( ( rule__AtsyraGoal__Group_0_3__0 )? )
            {
            // InternalAtsyRAGoal.g:2726:1: ( ( rule__AtsyraGoal__Group_0_3__0 )? )
            // InternalAtsyRAGoal.g:2727:2: ( rule__AtsyraGoal__Group_0_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getGroup_0_3()); 
            }
            // InternalAtsyRAGoal.g:2728:2: ( rule__AtsyraGoal__Group_0_3__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==34) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalAtsyRAGoal.g:2728:3: rule__AtsyraGoal__Group_0_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoal__Group_0_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getGroup_0_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0__3__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3__0"
    // InternalAtsyRAGoal.g:2737:1: rule__AtsyraGoal__Group_0_3__0 : rule__AtsyraGoal__Group_0_3__0__Impl rule__AtsyraGoal__Group_0_3__1 ;
    public final void rule__AtsyraGoal__Group_0_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2741:1: ( rule__AtsyraGoal__Group_0_3__0__Impl rule__AtsyraGoal__Group_0_3__1 )
            // InternalAtsyRAGoal.g:2742:2: rule__AtsyraGoal__Group_0_3__0__Impl rule__AtsyraGoal__Group_0_3__1
            {
            pushFollow(FOLLOW_29);
            rule__AtsyraGoal__Group_0_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3__0"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3__0__Impl"
    // InternalAtsyRAGoal.g:2749:1: rule__AtsyraGoal__Group_0_3__0__Impl : ( 'pre' ) ;
    public final void rule__AtsyraGoal__Group_0_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2753:1: ( ( 'pre' ) )
            // InternalAtsyRAGoal.g:2754:1: ( 'pre' )
            {
            // InternalAtsyRAGoal.g:2754:1: ( 'pre' )
            // InternalAtsyRAGoal.g:2755:2: 'pre'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getPreKeyword_0_3_0()); 
            }
            match(input,34,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getPreKeyword_0_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3__0__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3__1"
    // InternalAtsyRAGoal.g:2764:1: rule__AtsyraGoal__Group_0_3__1 : rule__AtsyraGoal__Group_0_3__1__Impl rule__AtsyraGoal__Group_0_3__2 ;
    public final void rule__AtsyraGoal__Group_0_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2768:1: ( rule__AtsyraGoal__Group_0_3__1__Impl rule__AtsyraGoal__Group_0_3__2 )
            // InternalAtsyRAGoal.g:2769:2: rule__AtsyraGoal__Group_0_3__1__Impl rule__AtsyraGoal__Group_0_3__2
            {
            pushFollow(FOLLOW_29);
            rule__AtsyraGoal__Group_0_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0_3__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3__1"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3__1__Impl"
    // InternalAtsyRAGoal.g:2776:1: rule__AtsyraGoal__Group_0_3__1__Impl : ( ( rule__AtsyraGoal__Group_0_3_1__0 )? ) ;
    public final void rule__AtsyraGoal__Group_0_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2780:1: ( ( ( rule__AtsyraGoal__Group_0_3_1__0 )? ) )
            // InternalAtsyRAGoal.g:2781:1: ( ( rule__AtsyraGoal__Group_0_3_1__0 )? )
            {
            // InternalAtsyRAGoal.g:2781:1: ( ( rule__AtsyraGoal__Group_0_3_1__0 )? )
            // InternalAtsyRAGoal.g:2782:2: ( rule__AtsyraGoal__Group_0_3_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getGroup_0_3_1()); 
            }
            // InternalAtsyRAGoal.g:2783:2: ( rule__AtsyraGoal__Group_0_3_1__0 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==36) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalAtsyRAGoal.g:2783:3: rule__AtsyraGoal__Group_0_3_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoal__Group_0_3_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getGroup_0_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3__1__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3__2"
    // InternalAtsyRAGoal.g:2791:1: rule__AtsyraGoal__Group_0_3__2 : rule__AtsyraGoal__Group_0_3__2__Impl rule__AtsyraGoal__Group_0_3__3 ;
    public final void rule__AtsyraGoal__Group_0_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2795:1: ( rule__AtsyraGoal__Group_0_3__2__Impl rule__AtsyraGoal__Group_0_3__3 )
            // InternalAtsyRAGoal.g:2796:2: rule__AtsyraGoal__Group_0_3__2__Impl rule__AtsyraGoal__Group_0_3__3
            {
            pushFollow(FOLLOW_20);
            rule__AtsyraGoal__Group_0_3__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0_3__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3__2"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3__2__Impl"
    // InternalAtsyRAGoal.g:2803:1: rule__AtsyraGoal__Group_0_3__2__Impl : ( ':' ) ;
    public final void rule__AtsyraGoal__Group_0_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2807:1: ( ( ':' ) )
            // InternalAtsyRAGoal.g:2808:1: ( ':' )
            {
            // InternalAtsyRAGoal.g:2808:1: ( ':' )
            // InternalAtsyRAGoal.g:2809:2: ':'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getColonKeyword_0_3_2()); 
            }
            match(input,35,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getColonKeyword_0_3_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3__2__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3__3"
    // InternalAtsyRAGoal.g:2818:1: rule__AtsyraGoal__Group_0_3__3 : rule__AtsyraGoal__Group_0_3__3__Impl ;
    public final void rule__AtsyraGoal__Group_0_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2822:1: ( rule__AtsyraGoal__Group_0_3__3__Impl )
            // InternalAtsyRAGoal.g:2823:2: rule__AtsyraGoal__Group_0_3__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0_3__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3__3"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3__3__Impl"
    // InternalAtsyRAGoal.g:2829:1: rule__AtsyraGoal__Group_0_3__3__Impl : ( ( rule__AtsyraGoal__PreconditionAssignment_0_3_3 ) ) ;
    public final void rule__AtsyraGoal__Group_0_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2833:1: ( ( ( rule__AtsyraGoal__PreconditionAssignment_0_3_3 ) ) )
            // InternalAtsyRAGoal.g:2834:1: ( ( rule__AtsyraGoal__PreconditionAssignment_0_3_3 ) )
            {
            // InternalAtsyRAGoal.g:2834:1: ( ( rule__AtsyraGoal__PreconditionAssignment_0_3_3 ) )
            // InternalAtsyRAGoal.g:2835:2: ( rule__AtsyraGoal__PreconditionAssignment_0_3_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getPreconditionAssignment_0_3_3()); 
            }
            // InternalAtsyRAGoal.g:2836:2: ( rule__AtsyraGoal__PreconditionAssignment_0_3_3 )
            // InternalAtsyRAGoal.g:2836:3: rule__AtsyraGoal__PreconditionAssignment_0_3_3
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__PreconditionAssignment_0_3_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getPreconditionAssignment_0_3_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3__3__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3_1__0"
    // InternalAtsyRAGoal.g:2845:1: rule__AtsyraGoal__Group_0_3_1__0 : rule__AtsyraGoal__Group_0_3_1__0__Impl rule__AtsyraGoal__Group_0_3_1__1 ;
    public final void rule__AtsyraGoal__Group_0_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2849:1: ( rule__AtsyraGoal__Group_0_3_1__0__Impl rule__AtsyraGoal__Group_0_3_1__1 )
            // InternalAtsyRAGoal.g:2850:2: rule__AtsyraGoal__Group_0_3_1__0__Impl rule__AtsyraGoal__Group_0_3_1__1
            {
            pushFollow(FOLLOW_7);
            rule__AtsyraGoal__Group_0_3_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0_3_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3_1__0"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3_1__0__Impl"
    // InternalAtsyRAGoal.g:2857:1: rule__AtsyraGoal__Group_0_3_1__0__Impl : ( 'with' ) ;
    public final void rule__AtsyraGoal__Group_0_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2861:1: ( ( 'with' ) )
            // InternalAtsyRAGoal.g:2862:1: ( 'with' )
            {
            // InternalAtsyRAGoal.g:2862:1: ( 'with' )
            // InternalAtsyRAGoal.g:2863:2: 'with'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getWithKeyword_0_3_1_0()); 
            }
            match(input,36,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getWithKeyword_0_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3_1__0__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3_1__1"
    // InternalAtsyRAGoal.g:2872:1: rule__AtsyraGoal__Group_0_3_1__1 : rule__AtsyraGoal__Group_0_3_1__1__Impl ;
    public final void rule__AtsyraGoal__Group_0_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2876:1: ( rule__AtsyraGoal__Group_0_3_1__1__Impl )
            // InternalAtsyRAGoal.g:2877:2: rule__AtsyraGoal__Group_0_3_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_0_3_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3_1__1"


    // $ANTLR start "rule__AtsyraGoal__Group_0_3_1__1__Impl"
    // InternalAtsyRAGoal.g:2883:1: rule__AtsyraGoal__Group_0_3_1__1__Impl : ( ( rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1 ) ) ;
    public final void rule__AtsyraGoal__Group_0_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2887:1: ( ( ( rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1 ) ) )
            // InternalAtsyRAGoal.g:2888:1: ( ( rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1 ) )
            {
            // InternalAtsyRAGoal.g:2888:1: ( ( rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1 ) )
            // InternalAtsyRAGoal.g:2889:2: ( rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPreAssignment_0_3_1_1()); 
            }
            // InternalAtsyRAGoal.g:2890:2: ( rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1 )
            // InternalAtsyRAGoal.g:2890:3: rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPreAssignment_0_3_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_0_3_1__1__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_1__0"
    // InternalAtsyRAGoal.g:2899:1: rule__AtsyraGoal__Group_1__0 : rule__AtsyraGoal__Group_1__0__Impl rule__AtsyraGoal__Group_1__1 ;
    public final void rule__AtsyraGoal__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2903:1: ( rule__AtsyraGoal__Group_1__0__Impl rule__AtsyraGoal__Group_1__1 )
            // InternalAtsyRAGoal.g:2904:2: rule__AtsyraGoal__Group_1__0__Impl rule__AtsyraGoal__Group_1__1
            {
            pushFollow(FOLLOW_16);
            rule__AtsyraGoal__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1__0"


    // $ANTLR start "rule__AtsyraGoal__Group_1__0__Impl"
    // InternalAtsyRAGoal.g:2911:1: rule__AtsyraGoal__Group_1__0__Impl : ( ( rule__AtsyraGoal__Group_1_0__0 )? ) ;
    public final void rule__AtsyraGoal__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2915:1: ( ( ( rule__AtsyraGoal__Group_1_0__0 )? ) )
            // InternalAtsyRAGoal.g:2916:1: ( ( rule__AtsyraGoal__Group_1_0__0 )? )
            {
            // InternalAtsyRAGoal.g:2916:1: ( ( rule__AtsyraGoal__Group_1_0__0 )? )
            // InternalAtsyRAGoal.g:2917:2: ( rule__AtsyraGoal__Group_1_0__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getGroup_1_0()); 
            }
            // InternalAtsyRAGoal.g:2918:2: ( rule__AtsyraGoal__Group_1_0__0 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==37) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalAtsyRAGoal.g:2918:3: rule__AtsyraGoal__Group_1_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoal__Group_1_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getGroup_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1__0__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_1__1"
    // InternalAtsyRAGoal.g:2926:1: rule__AtsyraGoal__Group_1__1 : rule__AtsyraGoal__Group_1__1__Impl ;
    public final void rule__AtsyraGoal__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2930:1: ( rule__AtsyraGoal__Group_1__1__Impl )
            // InternalAtsyRAGoal.g:2931:2: rule__AtsyraGoal__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1__1"


    // $ANTLR start "rule__AtsyraGoal__Group_1__1__Impl"
    // InternalAtsyRAGoal.g:2937:1: rule__AtsyraGoal__Group_1__1__Impl : ( '}' ) ;
    public final void rule__AtsyraGoal__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2941:1: ( ( '}' ) )
            // InternalAtsyRAGoal.g:2942:1: ( '}' )
            {
            // InternalAtsyRAGoal.g:2942:1: ( '}' )
            // InternalAtsyRAGoal.g:2943:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getRightCurlyBracketKeyword_1_1()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getRightCurlyBracketKeyword_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1__1__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0__0"
    // InternalAtsyRAGoal.g:2953:1: rule__AtsyraGoal__Group_1_0__0 : rule__AtsyraGoal__Group_1_0__0__Impl rule__AtsyraGoal__Group_1_0__1 ;
    public final void rule__AtsyraGoal__Group_1_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2957:1: ( rule__AtsyraGoal__Group_1_0__0__Impl rule__AtsyraGoal__Group_1_0__1 )
            // InternalAtsyRAGoal.g:2958:2: rule__AtsyraGoal__Group_1_0__0__Impl rule__AtsyraGoal__Group_1_0__1
            {
            pushFollow(FOLLOW_29);
            rule__AtsyraGoal__Group_1_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_1_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0__0"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0__0__Impl"
    // InternalAtsyRAGoal.g:2965:1: rule__AtsyraGoal__Group_1_0__0__Impl : ( 'post' ) ;
    public final void rule__AtsyraGoal__Group_1_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2969:1: ( ( 'post' ) )
            // InternalAtsyRAGoal.g:2970:1: ( 'post' )
            {
            // InternalAtsyRAGoal.g:2970:1: ( 'post' )
            // InternalAtsyRAGoal.g:2971:2: 'post'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getPostKeyword_1_0_0()); 
            }
            match(input,37,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getPostKeyword_1_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0__0__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0__1"
    // InternalAtsyRAGoal.g:2980:1: rule__AtsyraGoal__Group_1_0__1 : rule__AtsyraGoal__Group_1_0__1__Impl rule__AtsyraGoal__Group_1_0__2 ;
    public final void rule__AtsyraGoal__Group_1_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2984:1: ( rule__AtsyraGoal__Group_1_0__1__Impl rule__AtsyraGoal__Group_1_0__2 )
            // InternalAtsyRAGoal.g:2985:2: rule__AtsyraGoal__Group_1_0__1__Impl rule__AtsyraGoal__Group_1_0__2
            {
            pushFollow(FOLLOW_29);
            rule__AtsyraGoal__Group_1_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_1_0__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0__1"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0__1__Impl"
    // InternalAtsyRAGoal.g:2992:1: rule__AtsyraGoal__Group_1_0__1__Impl : ( ( rule__AtsyraGoal__Group_1_0_1__0 )? ) ;
    public final void rule__AtsyraGoal__Group_1_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:2996:1: ( ( ( rule__AtsyraGoal__Group_1_0_1__0 )? ) )
            // InternalAtsyRAGoal.g:2997:1: ( ( rule__AtsyraGoal__Group_1_0_1__0 )? )
            {
            // InternalAtsyRAGoal.g:2997:1: ( ( rule__AtsyraGoal__Group_1_0_1__0 )? )
            // InternalAtsyRAGoal.g:2998:2: ( rule__AtsyraGoal__Group_1_0_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getGroup_1_0_1()); 
            }
            // InternalAtsyRAGoal.g:2999:2: ( rule__AtsyraGoal__Group_1_0_1__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==36) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalAtsyRAGoal.g:2999:3: rule__AtsyraGoal__Group_1_0_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoal__Group_1_0_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getGroup_1_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0__1__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0__2"
    // InternalAtsyRAGoal.g:3007:1: rule__AtsyraGoal__Group_1_0__2 : rule__AtsyraGoal__Group_1_0__2__Impl rule__AtsyraGoal__Group_1_0__3 ;
    public final void rule__AtsyraGoal__Group_1_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3011:1: ( rule__AtsyraGoal__Group_1_0__2__Impl rule__AtsyraGoal__Group_1_0__3 )
            // InternalAtsyRAGoal.g:3012:2: rule__AtsyraGoal__Group_1_0__2__Impl rule__AtsyraGoal__Group_1_0__3
            {
            pushFollow(FOLLOW_20);
            rule__AtsyraGoal__Group_1_0__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_1_0__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0__2"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0__2__Impl"
    // InternalAtsyRAGoal.g:3019:1: rule__AtsyraGoal__Group_1_0__2__Impl : ( ':' ) ;
    public final void rule__AtsyraGoal__Group_1_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3023:1: ( ( ':' ) )
            // InternalAtsyRAGoal.g:3024:1: ( ':' )
            {
            // InternalAtsyRAGoal.g:3024:1: ( ':' )
            // InternalAtsyRAGoal.g:3025:2: ':'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getColonKeyword_1_0_2()); 
            }
            match(input,35,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getColonKeyword_1_0_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0__2__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0__3"
    // InternalAtsyRAGoal.g:3034:1: rule__AtsyraGoal__Group_1_0__3 : rule__AtsyraGoal__Group_1_0__3__Impl ;
    public final void rule__AtsyraGoal__Group_1_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3038:1: ( rule__AtsyraGoal__Group_1_0__3__Impl )
            // InternalAtsyRAGoal.g:3039:2: rule__AtsyraGoal__Group_1_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_1_0__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0__3"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0__3__Impl"
    // InternalAtsyRAGoal.g:3045:1: rule__AtsyraGoal__Group_1_0__3__Impl : ( ( rule__AtsyraGoal__PostconditionAssignment_1_0_3 ) ) ;
    public final void rule__AtsyraGoal__Group_1_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3049:1: ( ( ( rule__AtsyraGoal__PostconditionAssignment_1_0_3 ) ) )
            // InternalAtsyRAGoal.g:3050:1: ( ( rule__AtsyraGoal__PostconditionAssignment_1_0_3 ) )
            {
            // InternalAtsyRAGoal.g:3050:1: ( ( rule__AtsyraGoal__PostconditionAssignment_1_0_3 ) )
            // InternalAtsyRAGoal.g:3051:2: ( rule__AtsyraGoal__PostconditionAssignment_1_0_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getPostconditionAssignment_1_0_3()); 
            }
            // InternalAtsyRAGoal.g:3052:2: ( rule__AtsyraGoal__PostconditionAssignment_1_0_3 )
            // InternalAtsyRAGoal.g:3052:3: rule__AtsyraGoal__PostconditionAssignment_1_0_3
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__PostconditionAssignment_1_0_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getPostconditionAssignment_1_0_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0__3__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0_1__0"
    // InternalAtsyRAGoal.g:3061:1: rule__AtsyraGoal__Group_1_0_1__0 : rule__AtsyraGoal__Group_1_0_1__0__Impl rule__AtsyraGoal__Group_1_0_1__1 ;
    public final void rule__AtsyraGoal__Group_1_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3065:1: ( rule__AtsyraGoal__Group_1_0_1__0__Impl rule__AtsyraGoal__Group_1_0_1__1 )
            // InternalAtsyRAGoal.g:3066:2: rule__AtsyraGoal__Group_1_0_1__0__Impl rule__AtsyraGoal__Group_1_0_1__1
            {
            pushFollow(FOLLOW_7);
            rule__AtsyraGoal__Group_1_0_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_1_0_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0_1__0"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0_1__0__Impl"
    // InternalAtsyRAGoal.g:3073:1: rule__AtsyraGoal__Group_1_0_1__0__Impl : ( 'with' ) ;
    public final void rule__AtsyraGoal__Group_1_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3077:1: ( ( 'with' ) )
            // InternalAtsyRAGoal.g:3078:1: ( 'with' )
            {
            // InternalAtsyRAGoal.g:3078:1: ( 'with' )
            // InternalAtsyRAGoal.g:3079:2: 'with'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getWithKeyword_1_0_1_0()); 
            }
            match(input,36,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getWithKeyword_1_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0_1__0__Impl"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0_1__1"
    // InternalAtsyRAGoal.g:3088:1: rule__AtsyraGoal__Group_1_0_1__1 : rule__AtsyraGoal__Group_1_0_1__1__Impl ;
    public final void rule__AtsyraGoal__Group_1_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3092:1: ( rule__AtsyraGoal__Group_1_0_1__1__Impl )
            // InternalAtsyRAGoal.g:3093:2: rule__AtsyraGoal__Group_1_0_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__Group_1_0_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0_1__1"


    // $ANTLR start "rule__AtsyraGoal__Group_1_0_1__1__Impl"
    // InternalAtsyRAGoal.g:3099:1: rule__AtsyraGoal__Group_1_0_1__1__Impl : ( ( rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1 ) ) ;
    public final void rule__AtsyraGoal__Group_1_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3103:1: ( ( ( rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1 ) ) )
            // InternalAtsyRAGoal.g:3104:1: ( ( rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1 ) )
            {
            // InternalAtsyRAGoal.g:3104:1: ( ( rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1 ) )
            // InternalAtsyRAGoal.g:3105:2: ( rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPostAssignment_1_0_1_1()); 
            }
            // InternalAtsyRAGoal.g:3106:2: ( rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1 )
            // InternalAtsyRAGoal.g:3106:3: rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPostAssignment_1_0_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__Group_1_0_1__1__Impl"


    // $ANTLR start "rule__DefaultValues__Group__0"
    // InternalAtsyRAGoal.g:3115:1: rule__DefaultValues__Group__0 : rule__DefaultValues__Group__0__Impl rule__DefaultValues__Group__1 ;
    public final void rule__DefaultValues__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3119:1: ( rule__DefaultValues__Group__0__Impl rule__DefaultValues__Group__1 )
            // InternalAtsyRAGoal.g:3120:2: rule__DefaultValues__Group__0__Impl rule__DefaultValues__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__DefaultValues__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__0"


    // $ANTLR start "rule__DefaultValues__Group__0__Impl"
    // InternalAtsyRAGoal.g:3127:1: rule__DefaultValues__Group__0__Impl : ( () ) ;
    public final void rule__DefaultValues__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3131:1: ( ( () ) )
            // InternalAtsyRAGoal.g:3132:1: ( () )
            {
            // InternalAtsyRAGoal.g:3132:1: ( () )
            // InternalAtsyRAGoal.g:3133:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getDefaultValuesAction_0()); 
            }
            // InternalAtsyRAGoal.g:3134:2: ()
            // InternalAtsyRAGoal.g:3134:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getDefaultValuesAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__0__Impl"


    // $ANTLR start "rule__DefaultValues__Group__1"
    // InternalAtsyRAGoal.g:3142:1: rule__DefaultValues__Group__1 : rule__DefaultValues__Group__1__Impl rule__DefaultValues__Group__2 ;
    public final void rule__DefaultValues__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3146:1: ( rule__DefaultValues__Group__1__Impl rule__DefaultValues__Group__2 )
            // InternalAtsyRAGoal.g:3147:2: rule__DefaultValues__Group__1__Impl rule__DefaultValues__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__DefaultValues__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__1"


    // $ANTLR start "rule__DefaultValues__Group__1__Impl"
    // InternalAtsyRAGoal.g:3154:1: rule__DefaultValues__Group__1__Impl : ( ( rule__DefaultValues__NameAssignment_1 ) ) ;
    public final void rule__DefaultValues__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3158:1: ( ( ( rule__DefaultValues__NameAssignment_1 ) ) )
            // InternalAtsyRAGoal.g:3159:1: ( ( rule__DefaultValues__NameAssignment_1 ) )
            {
            // InternalAtsyRAGoal.g:3159:1: ( ( rule__DefaultValues__NameAssignment_1 ) )
            // InternalAtsyRAGoal.g:3160:2: ( rule__DefaultValues__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getNameAssignment_1()); 
            }
            // InternalAtsyRAGoal.g:3161:2: ( rule__DefaultValues__NameAssignment_1 )
            // InternalAtsyRAGoal.g:3161:3: rule__DefaultValues__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValues__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__1__Impl"


    // $ANTLR start "rule__DefaultValues__Group__2"
    // InternalAtsyRAGoal.g:3169:1: rule__DefaultValues__Group__2 : rule__DefaultValues__Group__2__Impl rule__DefaultValues__Group__3 ;
    public final void rule__DefaultValues__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3173:1: ( rule__DefaultValues__Group__2__Impl rule__DefaultValues__Group__3 )
            // InternalAtsyRAGoal.g:3174:2: rule__DefaultValues__Group__2__Impl rule__DefaultValues__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__DefaultValues__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__2"


    // $ANTLR start "rule__DefaultValues__Group__2__Impl"
    // InternalAtsyRAGoal.g:3181:1: rule__DefaultValues__Group__2__Impl : ( '{' ) ;
    public final void rule__DefaultValues__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3185:1: ( ( '{' ) )
            // InternalAtsyRAGoal.g:3186:1: ( '{' )
            {
            // InternalAtsyRAGoal.g:3186:1: ( '{' )
            // InternalAtsyRAGoal.g:3187:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getLeftCurlyBracketKeyword_2()); 
            }
            match(input,21,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getLeftCurlyBracketKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__2__Impl"


    // $ANTLR start "rule__DefaultValues__Group__3"
    // InternalAtsyRAGoal.g:3196:1: rule__DefaultValues__Group__3 : rule__DefaultValues__Group__3__Impl rule__DefaultValues__Group__4 ;
    public final void rule__DefaultValues__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3200:1: ( rule__DefaultValues__Group__3__Impl rule__DefaultValues__Group__4 )
            // InternalAtsyRAGoal.g:3201:2: rule__DefaultValues__Group__3__Impl rule__DefaultValues__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__DefaultValues__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__3"


    // $ANTLR start "rule__DefaultValues__Group__3__Impl"
    // InternalAtsyRAGoal.g:3208:1: rule__DefaultValues__Group__3__Impl : ( ( rule__DefaultValues__Group_3__0 )? ) ;
    public final void rule__DefaultValues__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3212:1: ( ( ( rule__DefaultValues__Group_3__0 )? ) )
            // InternalAtsyRAGoal.g:3213:1: ( ( rule__DefaultValues__Group_3__0 )? )
            {
            // InternalAtsyRAGoal.g:3213:1: ( ( rule__DefaultValues__Group_3__0 )? )
            // InternalAtsyRAGoal.g:3214:2: ( rule__DefaultValues__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getGroup_3()); 
            }
            // InternalAtsyRAGoal.g:3215:2: ( rule__DefaultValues__Group_3__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( ((LA30_0>=RULE_STRING && LA30_0<=RULE_ID)) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalAtsyRAGoal.g:3215:3: rule__DefaultValues__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DefaultValues__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__3__Impl"


    // $ANTLR start "rule__DefaultValues__Group__4"
    // InternalAtsyRAGoal.g:3223:1: rule__DefaultValues__Group__4 : rule__DefaultValues__Group__4__Impl ;
    public final void rule__DefaultValues__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3227:1: ( rule__DefaultValues__Group__4__Impl )
            // InternalAtsyRAGoal.g:3228:2: rule__DefaultValues__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__4"


    // $ANTLR start "rule__DefaultValues__Group__4__Impl"
    // InternalAtsyRAGoal.g:3234:1: rule__DefaultValues__Group__4__Impl : ( '}' ) ;
    public final void rule__DefaultValues__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3238:1: ( ( '}' ) )
            // InternalAtsyRAGoal.g:3239:1: ( '}' )
            {
            // InternalAtsyRAGoal.g:3239:1: ( '}' )
            // InternalAtsyRAGoal.g:3240:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getRightCurlyBracketKeyword_4()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getRightCurlyBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group__4__Impl"


    // $ANTLR start "rule__DefaultValues__Group_3__0"
    // InternalAtsyRAGoal.g:3250:1: rule__DefaultValues__Group_3__0 : rule__DefaultValues__Group_3__0__Impl rule__DefaultValues__Group_3__1 ;
    public final void rule__DefaultValues__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3254:1: ( rule__DefaultValues__Group_3__0__Impl rule__DefaultValues__Group_3__1 )
            // InternalAtsyRAGoal.g:3255:2: rule__DefaultValues__Group_3__0__Impl rule__DefaultValues__Group_3__1
            {
            pushFollow(FOLLOW_30);
            rule__DefaultValues__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group_3__0"


    // $ANTLR start "rule__DefaultValues__Group_3__0__Impl"
    // InternalAtsyRAGoal.g:3262:1: rule__DefaultValues__Group_3__0__Impl : ( ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0 ) ) ;
    public final void rule__DefaultValues__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3266:1: ( ( ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0 ) ) )
            // InternalAtsyRAGoal.g:3267:1: ( ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0 ) )
            {
            // InternalAtsyRAGoal.g:3267:1: ( ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0 ) )
            // InternalAtsyRAGoal.g:3268:2: ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsAssignment_3_0()); 
            }
            // InternalAtsyRAGoal.g:3269:2: ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0 )
            // InternalAtsyRAGoal.g:3269:3: rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsAssignment_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group_3__0__Impl"


    // $ANTLR start "rule__DefaultValues__Group_3__1"
    // InternalAtsyRAGoal.g:3277:1: rule__DefaultValues__Group_3__1 : rule__DefaultValues__Group_3__1__Impl ;
    public final void rule__DefaultValues__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3281:1: ( rule__DefaultValues__Group_3__1__Impl )
            // InternalAtsyRAGoal.g:3282:2: rule__DefaultValues__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group_3__1"


    // $ANTLR start "rule__DefaultValues__Group_3__1__Impl"
    // InternalAtsyRAGoal.g:3288:1: rule__DefaultValues__Group_3__1__Impl : ( ( rule__DefaultValues__Group_3_1__0 )* ) ;
    public final void rule__DefaultValues__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3292:1: ( ( ( rule__DefaultValues__Group_3_1__0 )* ) )
            // InternalAtsyRAGoal.g:3293:1: ( ( rule__DefaultValues__Group_3_1__0 )* )
            {
            // InternalAtsyRAGoal.g:3293:1: ( ( rule__DefaultValues__Group_3_1__0 )* )
            // InternalAtsyRAGoal.g:3294:2: ( rule__DefaultValues__Group_3_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getGroup_3_1()); 
            }
            // InternalAtsyRAGoal.g:3295:2: ( rule__DefaultValues__Group_3_1__0 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==38) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:3295:3: rule__DefaultValues__Group_3_1__0
            	    {
            	    pushFollow(FOLLOW_31);
            	    rule__DefaultValues__Group_3_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getGroup_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group_3__1__Impl"


    // $ANTLR start "rule__DefaultValues__Group_3_1__0"
    // InternalAtsyRAGoal.g:3304:1: rule__DefaultValues__Group_3_1__0 : rule__DefaultValues__Group_3_1__0__Impl rule__DefaultValues__Group_3_1__1 ;
    public final void rule__DefaultValues__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3308:1: ( rule__DefaultValues__Group_3_1__0__Impl rule__DefaultValues__Group_3_1__1 )
            // InternalAtsyRAGoal.g:3309:2: rule__DefaultValues__Group_3_1__0__Impl rule__DefaultValues__Group_3_1__1
            {
            pushFollow(FOLLOW_7);
            rule__DefaultValues__Group_3_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group_3_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group_3_1__0"


    // $ANTLR start "rule__DefaultValues__Group_3_1__0__Impl"
    // InternalAtsyRAGoal.g:3316:1: rule__DefaultValues__Group_3_1__0__Impl : ( ',' ) ;
    public final void rule__DefaultValues__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3320:1: ( ( ',' ) )
            // InternalAtsyRAGoal.g:3321:1: ( ',' )
            {
            // InternalAtsyRAGoal.g:3321:1: ( ',' )
            // InternalAtsyRAGoal.g:3322:2: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getCommaKeyword_3_1_0()); 
            }
            match(input,38,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getCommaKeyword_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group_3_1__0__Impl"


    // $ANTLR start "rule__DefaultValues__Group_3_1__1"
    // InternalAtsyRAGoal.g:3331:1: rule__DefaultValues__Group_3_1__1 : rule__DefaultValues__Group_3_1__1__Impl ;
    public final void rule__DefaultValues__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3335:1: ( rule__DefaultValues__Group_3_1__1__Impl )
            // InternalAtsyRAGoal.g:3336:2: rule__DefaultValues__Group_3_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValues__Group_3_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group_3_1__1"


    // $ANTLR start "rule__DefaultValues__Group_3_1__1__Impl"
    // InternalAtsyRAGoal.g:3342:1: rule__DefaultValues__Group_3_1__1__Impl : ( ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1 ) ) ;
    public final void rule__DefaultValues__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3346:1: ( ( ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1 ) ) )
            // InternalAtsyRAGoal.g:3347:1: ( ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1 ) )
            {
            // InternalAtsyRAGoal.g:3347:1: ( ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1 ) )
            // InternalAtsyRAGoal.g:3348:2: ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsAssignment_3_1_1()); 
            }
            // InternalAtsyRAGoal.g:3349:2: ( rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1 )
            // InternalAtsyRAGoal.g:3349:3: rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsAssignment_3_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__Group_3_1__1__Impl"


    // $ANTLR start "rule__DefaultValueAssignments__Group__0"
    // InternalAtsyRAGoal.g:3358:1: rule__DefaultValueAssignments__Group__0 : rule__DefaultValueAssignments__Group__0__Impl rule__DefaultValueAssignments__Group__1 ;
    public final void rule__DefaultValueAssignments__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3362:1: ( rule__DefaultValueAssignments__Group__0__Impl rule__DefaultValueAssignments__Group__1 )
            // InternalAtsyRAGoal.g:3363:2: rule__DefaultValueAssignments__Group__0__Impl rule__DefaultValueAssignments__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__DefaultValueAssignments__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DefaultValueAssignments__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__Group__0"


    // $ANTLR start "rule__DefaultValueAssignments__Group__0__Impl"
    // InternalAtsyRAGoal.g:3370:1: rule__DefaultValueAssignments__Group__0__Impl : ( () ) ;
    public final void rule__DefaultValueAssignments__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3374:1: ( ( () ) )
            // InternalAtsyRAGoal.g:3375:1: ( () )
            {
            // InternalAtsyRAGoal.g:3375:1: ( () )
            // InternalAtsyRAGoal.g:3376:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsAccess().getDefaultAssignmentAction_0()); 
            }
            // InternalAtsyRAGoal.g:3377:2: ()
            // InternalAtsyRAGoal.g:3377:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsAccess().getDefaultAssignmentAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__Group__0__Impl"


    // $ANTLR start "rule__DefaultValueAssignments__Group__1"
    // InternalAtsyRAGoal.g:3385:1: rule__DefaultValueAssignments__Group__1 : rule__DefaultValueAssignments__Group__1__Impl rule__DefaultValueAssignments__Group__2 ;
    public final void rule__DefaultValueAssignments__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3389:1: ( rule__DefaultValueAssignments__Group__1__Impl rule__DefaultValueAssignments__Group__2 )
            // InternalAtsyRAGoal.g:3390:2: rule__DefaultValueAssignments__Group__1__Impl rule__DefaultValueAssignments__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__DefaultValueAssignments__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DefaultValueAssignments__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__Group__1"


    // $ANTLR start "rule__DefaultValueAssignments__Group__1__Impl"
    // InternalAtsyRAGoal.g:3397:1: rule__DefaultValueAssignments__Group__1__Impl : ( ( rule__DefaultValueAssignments__TargetAssignment_1 ) ) ;
    public final void rule__DefaultValueAssignments__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3401:1: ( ( ( rule__DefaultValueAssignments__TargetAssignment_1 ) ) )
            // InternalAtsyRAGoal.g:3402:1: ( ( rule__DefaultValueAssignments__TargetAssignment_1 ) )
            {
            // InternalAtsyRAGoal.g:3402:1: ( ( rule__DefaultValueAssignments__TargetAssignment_1 ) )
            // InternalAtsyRAGoal.g:3403:2: ( rule__DefaultValueAssignments__TargetAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsAccess().getTargetAssignment_1()); 
            }
            // InternalAtsyRAGoal.g:3404:2: ( rule__DefaultValueAssignments__TargetAssignment_1 )
            // InternalAtsyRAGoal.g:3404:3: rule__DefaultValueAssignments__TargetAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValueAssignments__TargetAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsAccess().getTargetAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__Group__1__Impl"


    // $ANTLR start "rule__DefaultValueAssignments__Group__2"
    // InternalAtsyRAGoal.g:3412:1: rule__DefaultValueAssignments__Group__2 : rule__DefaultValueAssignments__Group__2__Impl rule__DefaultValueAssignments__Group__3 ;
    public final void rule__DefaultValueAssignments__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3416:1: ( rule__DefaultValueAssignments__Group__2__Impl rule__DefaultValueAssignments__Group__3 )
            // InternalAtsyRAGoal.g:3417:2: rule__DefaultValueAssignments__Group__2__Impl rule__DefaultValueAssignments__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__DefaultValueAssignments__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DefaultValueAssignments__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__Group__2"


    // $ANTLR start "rule__DefaultValueAssignments__Group__2__Impl"
    // InternalAtsyRAGoal.g:3424:1: rule__DefaultValueAssignments__Group__2__Impl : ( '=' ) ;
    public final void rule__DefaultValueAssignments__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3428:1: ( ( '=' ) )
            // InternalAtsyRAGoal.g:3429:1: ( '=' )
            {
            // InternalAtsyRAGoal.g:3429:1: ( '=' )
            // InternalAtsyRAGoal.g:3430:2: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsAccess().getEqualsSignKeyword_2()); 
            }
            match(input,31,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsAccess().getEqualsSignKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__Group__2__Impl"


    // $ANTLR start "rule__DefaultValueAssignments__Group__3"
    // InternalAtsyRAGoal.g:3439:1: rule__DefaultValueAssignments__Group__3 : rule__DefaultValueAssignments__Group__3__Impl ;
    public final void rule__DefaultValueAssignments__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3443:1: ( rule__DefaultValueAssignments__Group__3__Impl )
            // InternalAtsyRAGoal.g:3444:2: rule__DefaultValueAssignments__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValueAssignments__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__Group__3"


    // $ANTLR start "rule__DefaultValueAssignments__Group__3__Impl"
    // InternalAtsyRAGoal.g:3450:1: rule__DefaultValueAssignments__Group__3__Impl : ( ( rule__DefaultValueAssignments__AssignedValueAssignment_3 ) ) ;
    public final void rule__DefaultValueAssignments__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3454:1: ( ( ( rule__DefaultValueAssignments__AssignedValueAssignment_3 ) ) )
            // InternalAtsyRAGoal.g:3455:1: ( ( rule__DefaultValueAssignments__AssignedValueAssignment_3 ) )
            {
            // InternalAtsyRAGoal.g:3455:1: ( ( rule__DefaultValueAssignments__AssignedValueAssignment_3 ) )
            // InternalAtsyRAGoal.g:3456:2: ( rule__DefaultValueAssignments__AssignedValueAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsAccess().getAssignedValueAssignment_3()); 
            }
            // InternalAtsyRAGoal.g:3457:2: ( rule__DefaultValueAssignments__AssignedValueAssignment_3 )
            // InternalAtsyRAGoal.g:3457:3: rule__DefaultValueAssignments__AssignedValueAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__DefaultValueAssignments__AssignedValueAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsAccess().getAssignedValueAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__Group__3__Impl"


    // $ANTLR start "rule__AtsyraTree__Group__0"
    // InternalAtsyRAGoal.g:3466:1: rule__AtsyraTree__Group__0 : rule__AtsyraTree__Group__0__Impl rule__AtsyraTree__Group__1 ;
    public final void rule__AtsyraTree__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3470:1: ( rule__AtsyraTree__Group__0__Impl rule__AtsyraTree__Group__1 )
            // InternalAtsyRAGoal.g:3471:2: rule__AtsyraTree__Group__0__Impl rule__AtsyraTree__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__AtsyraTree__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__0"


    // $ANTLR start "rule__AtsyraTree__Group__0__Impl"
    // InternalAtsyRAGoal.g:3478:1: rule__AtsyraTree__Group__0__Impl : ( () ) ;
    public final void rule__AtsyraTree__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3482:1: ( ( () ) )
            // InternalAtsyRAGoal.g:3483:1: ( () )
            {
            // InternalAtsyRAGoal.g:3483:1: ( () )
            // InternalAtsyRAGoal.g:3484:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getAtsyraTreeAction_0()); 
            }
            // InternalAtsyRAGoal.g:3485:2: ()
            // InternalAtsyRAGoal.g:3485:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getAtsyraTreeAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__0__Impl"


    // $ANTLR start "rule__AtsyraTree__Group__1"
    // InternalAtsyRAGoal.g:3493:1: rule__AtsyraTree__Group__1 : rule__AtsyraTree__Group__1__Impl rule__AtsyraTree__Group__2 ;
    public final void rule__AtsyraTree__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3497:1: ( rule__AtsyraTree__Group__1__Impl rule__AtsyraTree__Group__2 )
            // InternalAtsyRAGoal.g:3498:2: rule__AtsyraTree__Group__1__Impl rule__AtsyraTree__Group__2
            {
            pushFollow(FOLLOW_32);
            rule__AtsyraTree__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__1"


    // $ANTLR start "rule__AtsyraTree__Group__1__Impl"
    // InternalAtsyRAGoal.g:3505:1: rule__AtsyraTree__Group__1__Impl : ( ( rule__AtsyraTree__NameAssignment_1 ) ) ;
    public final void rule__AtsyraTree__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3509:1: ( ( ( rule__AtsyraTree__NameAssignment_1 ) ) )
            // InternalAtsyRAGoal.g:3510:1: ( ( rule__AtsyraTree__NameAssignment_1 ) )
            {
            // InternalAtsyRAGoal.g:3510:1: ( ( rule__AtsyraTree__NameAssignment_1 ) )
            // InternalAtsyRAGoal.g:3511:2: ( rule__AtsyraTree__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getNameAssignment_1()); 
            }
            // InternalAtsyRAGoal.g:3512:2: ( rule__AtsyraTree__NameAssignment_1 )
            // InternalAtsyRAGoal.g:3512:3: rule__AtsyraTree__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__1__Impl"


    // $ANTLR start "rule__AtsyraTree__Group__2"
    // InternalAtsyRAGoal.g:3520:1: rule__AtsyraTree__Group__2 : rule__AtsyraTree__Group__2__Impl rule__AtsyraTree__Group__3 ;
    public final void rule__AtsyraTree__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3524:1: ( rule__AtsyraTree__Group__2__Impl rule__AtsyraTree__Group__3 )
            // InternalAtsyRAGoal.g:3525:2: rule__AtsyraTree__Group__2__Impl rule__AtsyraTree__Group__3
            {
            pushFollow(FOLLOW_33);
            rule__AtsyraTree__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__2"


    // $ANTLR start "rule__AtsyraTree__Group__2__Impl"
    // InternalAtsyRAGoal.g:3532:1: rule__AtsyraTree__Group__2__Impl : ( '[' ) ;
    public final void rule__AtsyraTree__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3536:1: ( ( '[' ) )
            // InternalAtsyRAGoal.g:3537:1: ( '[' )
            {
            // InternalAtsyRAGoal.g:3537:1: ( '[' )
            // InternalAtsyRAGoal.g:3538:2: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getLeftSquareBracketKeyword_2()); 
            }
            match(input,39,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getLeftSquareBracketKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__2__Impl"


    // $ANTLR start "rule__AtsyraTree__Group__3"
    // InternalAtsyRAGoal.g:3547:1: rule__AtsyraTree__Group__3 : rule__AtsyraTree__Group__3__Impl rule__AtsyraTree__Group__4 ;
    public final void rule__AtsyraTree__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3551:1: ( rule__AtsyraTree__Group__3__Impl rule__AtsyraTree__Group__4 )
            // InternalAtsyRAGoal.g:3552:2: rule__AtsyraTree__Group__3__Impl rule__AtsyraTree__Group__4
            {
            pushFollow(FOLLOW_33);
            rule__AtsyraTree__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__3"


    // $ANTLR start "rule__AtsyraTree__Group__3__Impl"
    // InternalAtsyRAGoal.g:3559:1: rule__AtsyraTree__Group__3__Impl : ( ( rule__AtsyraTree__MainGoalAssignment_3 )? ) ;
    public final void rule__AtsyraTree__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3563:1: ( ( ( rule__AtsyraTree__MainGoalAssignment_3 )? ) )
            // InternalAtsyRAGoal.g:3564:1: ( ( rule__AtsyraTree__MainGoalAssignment_3 )? )
            {
            // InternalAtsyRAGoal.g:3564:1: ( ( rule__AtsyraTree__MainGoalAssignment_3 )? )
            // InternalAtsyRAGoal.g:3565:2: ( rule__AtsyraTree__MainGoalAssignment_3 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getMainGoalAssignment_3()); 
            }
            // InternalAtsyRAGoal.g:3566:2: ( rule__AtsyraTree__MainGoalAssignment_3 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==RULE_ID) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalAtsyRAGoal.g:3566:3: rule__AtsyraTree__MainGoalAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraTree__MainGoalAssignment_3();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getMainGoalAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__3__Impl"


    // $ANTLR start "rule__AtsyraTree__Group__4"
    // InternalAtsyRAGoal.g:3574:1: rule__AtsyraTree__Group__4 : rule__AtsyraTree__Group__4__Impl rule__AtsyraTree__Group__5 ;
    public final void rule__AtsyraTree__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3578:1: ( rule__AtsyraTree__Group__4__Impl rule__AtsyraTree__Group__5 )
            // InternalAtsyRAGoal.g:3579:2: rule__AtsyraTree__Group__4__Impl rule__AtsyraTree__Group__5
            {
            pushFollow(FOLLOW_34);
            rule__AtsyraTree__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__4"


    // $ANTLR start "rule__AtsyraTree__Group__4__Impl"
    // InternalAtsyRAGoal.g:3586:1: rule__AtsyraTree__Group__4__Impl : ( ']' ) ;
    public final void rule__AtsyraTree__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3590:1: ( ( ']' ) )
            // InternalAtsyRAGoal.g:3591:1: ( ']' )
            {
            // InternalAtsyRAGoal.g:3591:1: ( ']' )
            // InternalAtsyRAGoal.g:3592:2: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getRightSquareBracketKeyword_4()); 
            }
            match(input,40,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getRightSquareBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__4__Impl"


    // $ANTLR start "rule__AtsyraTree__Group__5"
    // InternalAtsyRAGoal.g:3601:1: rule__AtsyraTree__Group__5 : rule__AtsyraTree__Group__5__Impl ;
    public final void rule__AtsyraTree__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3605:1: ( rule__AtsyraTree__Group__5__Impl )
            // InternalAtsyRAGoal.g:3606:2: rule__AtsyraTree__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__5"


    // $ANTLR start "rule__AtsyraTree__Group__5__Impl"
    // InternalAtsyRAGoal.g:3612:1: rule__AtsyraTree__Group__5__Impl : ( ( rule__AtsyraTree__Group_5__0 )? ) ;
    public final void rule__AtsyraTree__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3616:1: ( ( ( rule__AtsyraTree__Group_5__0 )? ) )
            // InternalAtsyRAGoal.g:3617:1: ( ( rule__AtsyraTree__Group_5__0 )? )
            {
            // InternalAtsyRAGoal.g:3617:1: ( ( rule__AtsyraTree__Group_5__0 )? )
            // InternalAtsyRAGoal.g:3618:2: ( rule__AtsyraTree__Group_5__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getGroup_5()); 
            }
            // InternalAtsyRAGoal.g:3619:2: ( rule__AtsyraTree__Group_5__0 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==12||LA33_0==14||LA33_0==19) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalAtsyRAGoal.g:3619:3: rule__AtsyraTree__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraTree__Group_5__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getGroup_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group__5__Impl"


    // $ANTLR start "rule__AtsyraTree__Group_5__0"
    // InternalAtsyRAGoal.g:3628:1: rule__AtsyraTree__Group_5__0 : rule__AtsyraTree__Group_5__0__Impl rule__AtsyraTree__Group_5__1 ;
    public final void rule__AtsyraTree__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3632:1: ( rule__AtsyraTree__Group_5__0__Impl rule__AtsyraTree__Group_5__1 )
            // InternalAtsyRAGoal.g:3633:2: rule__AtsyraTree__Group_5__0__Impl rule__AtsyraTree__Group_5__1
            {
            pushFollow(FOLLOW_35);
            rule__AtsyraTree__Group_5__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group_5__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__0"


    // $ANTLR start "rule__AtsyraTree__Group_5__0__Impl"
    // InternalAtsyRAGoal.g:3640:1: rule__AtsyraTree__Group_5__0__Impl : ( ( rule__AtsyraTree__OperatorAssignment_5_0 ) ) ;
    public final void rule__AtsyraTree__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3644:1: ( ( ( rule__AtsyraTree__OperatorAssignment_5_0 ) ) )
            // InternalAtsyRAGoal.g:3645:1: ( ( rule__AtsyraTree__OperatorAssignment_5_0 ) )
            {
            // InternalAtsyRAGoal.g:3645:1: ( ( rule__AtsyraTree__OperatorAssignment_5_0 ) )
            // InternalAtsyRAGoal.g:3646:2: ( rule__AtsyraTree__OperatorAssignment_5_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getOperatorAssignment_5_0()); 
            }
            // InternalAtsyRAGoal.g:3647:2: ( rule__AtsyraTree__OperatorAssignment_5_0 )
            // InternalAtsyRAGoal.g:3647:3: rule__AtsyraTree__OperatorAssignment_5_0
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__OperatorAssignment_5_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getOperatorAssignment_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__0__Impl"


    // $ANTLR start "rule__AtsyraTree__Group_5__1"
    // InternalAtsyRAGoal.g:3655:1: rule__AtsyraTree__Group_5__1 : rule__AtsyraTree__Group_5__1__Impl rule__AtsyraTree__Group_5__2 ;
    public final void rule__AtsyraTree__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3659:1: ( rule__AtsyraTree__Group_5__1__Impl rule__AtsyraTree__Group_5__2 )
            // InternalAtsyRAGoal.g:3660:2: rule__AtsyraTree__Group_5__1__Impl rule__AtsyraTree__Group_5__2
            {
            pushFollow(FOLLOW_7);
            rule__AtsyraTree__Group_5__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group_5__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__1"


    // $ANTLR start "rule__AtsyraTree__Group_5__1__Impl"
    // InternalAtsyRAGoal.g:3667:1: rule__AtsyraTree__Group_5__1__Impl : ( '(' ) ;
    public final void rule__AtsyraTree__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3671:1: ( ( '(' ) )
            // InternalAtsyRAGoal.g:3672:1: ( '(' )
            {
            // InternalAtsyRAGoal.g:3672:1: ( '(' )
            // InternalAtsyRAGoal.g:3673:2: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getLeftParenthesisKeyword_5_1()); 
            }
            match(input,29,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getLeftParenthesisKeyword_5_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__1__Impl"


    // $ANTLR start "rule__AtsyraTree__Group_5__2"
    // InternalAtsyRAGoal.g:3682:1: rule__AtsyraTree__Group_5__2 : rule__AtsyraTree__Group_5__2__Impl rule__AtsyraTree__Group_5__3 ;
    public final void rule__AtsyraTree__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3686:1: ( rule__AtsyraTree__Group_5__2__Impl rule__AtsyraTree__Group_5__3 )
            // InternalAtsyRAGoal.g:3687:2: rule__AtsyraTree__Group_5__2__Impl rule__AtsyraTree__Group_5__3
            {
            pushFollow(FOLLOW_36);
            rule__AtsyraTree__Group_5__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group_5__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__2"


    // $ANTLR start "rule__AtsyraTree__Group_5__2__Impl"
    // InternalAtsyRAGoal.g:3694:1: rule__AtsyraTree__Group_5__2__Impl : ( ( rule__AtsyraTree__OperandsAssignment_5_2 ) ) ;
    public final void rule__AtsyraTree__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3698:1: ( ( ( rule__AtsyraTree__OperandsAssignment_5_2 ) ) )
            // InternalAtsyRAGoal.g:3699:1: ( ( rule__AtsyraTree__OperandsAssignment_5_2 ) )
            {
            // InternalAtsyRAGoal.g:3699:1: ( ( rule__AtsyraTree__OperandsAssignment_5_2 ) )
            // InternalAtsyRAGoal.g:3700:2: ( rule__AtsyraTree__OperandsAssignment_5_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getOperandsAssignment_5_2()); 
            }
            // InternalAtsyRAGoal.g:3701:2: ( rule__AtsyraTree__OperandsAssignment_5_2 )
            // InternalAtsyRAGoal.g:3701:3: rule__AtsyraTree__OperandsAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__OperandsAssignment_5_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getOperandsAssignment_5_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__2__Impl"


    // $ANTLR start "rule__AtsyraTree__Group_5__3"
    // InternalAtsyRAGoal.g:3709:1: rule__AtsyraTree__Group_5__3 : rule__AtsyraTree__Group_5__3__Impl rule__AtsyraTree__Group_5__4 ;
    public final void rule__AtsyraTree__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3713:1: ( rule__AtsyraTree__Group_5__3__Impl rule__AtsyraTree__Group_5__4 )
            // InternalAtsyRAGoal.g:3714:2: rule__AtsyraTree__Group_5__3__Impl rule__AtsyraTree__Group_5__4
            {
            pushFollow(FOLLOW_36);
            rule__AtsyraTree__Group_5__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group_5__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__3"


    // $ANTLR start "rule__AtsyraTree__Group_5__3__Impl"
    // InternalAtsyRAGoal.g:3721:1: rule__AtsyraTree__Group_5__3__Impl : ( ( rule__AtsyraTree__Group_5_3__0 )* ) ;
    public final void rule__AtsyraTree__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3725:1: ( ( ( rule__AtsyraTree__Group_5_3__0 )* ) )
            // InternalAtsyRAGoal.g:3726:1: ( ( rule__AtsyraTree__Group_5_3__0 )* )
            {
            // InternalAtsyRAGoal.g:3726:1: ( ( rule__AtsyraTree__Group_5_3__0 )* )
            // InternalAtsyRAGoal.g:3727:2: ( rule__AtsyraTree__Group_5_3__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getGroup_5_3()); 
            }
            // InternalAtsyRAGoal.g:3728:2: ( rule__AtsyraTree__Group_5_3__0 )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==38) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:3728:3: rule__AtsyraTree__Group_5_3__0
            	    {
            	    pushFollow(FOLLOW_31);
            	    rule__AtsyraTree__Group_5_3__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getGroup_5_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__3__Impl"


    // $ANTLR start "rule__AtsyraTree__Group_5__4"
    // InternalAtsyRAGoal.g:3736:1: rule__AtsyraTree__Group_5__4 : rule__AtsyraTree__Group_5__4__Impl ;
    public final void rule__AtsyraTree__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3740:1: ( rule__AtsyraTree__Group_5__4__Impl )
            // InternalAtsyRAGoal.g:3741:2: rule__AtsyraTree__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group_5__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__4"


    // $ANTLR start "rule__AtsyraTree__Group_5__4__Impl"
    // InternalAtsyRAGoal.g:3747:1: rule__AtsyraTree__Group_5__4__Impl : ( ')' ) ;
    public final void rule__AtsyraTree__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3751:1: ( ( ')' ) )
            // InternalAtsyRAGoal.g:3752:1: ( ')' )
            {
            // InternalAtsyRAGoal.g:3752:1: ( ')' )
            // InternalAtsyRAGoal.g:3753:2: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getRightParenthesisKeyword_5_4()); 
            }
            match(input,30,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getRightParenthesisKeyword_5_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5__4__Impl"


    // $ANTLR start "rule__AtsyraTree__Group_5_3__0"
    // InternalAtsyRAGoal.g:3763:1: rule__AtsyraTree__Group_5_3__0 : rule__AtsyraTree__Group_5_3__0__Impl rule__AtsyraTree__Group_5_3__1 ;
    public final void rule__AtsyraTree__Group_5_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3767:1: ( rule__AtsyraTree__Group_5_3__0__Impl rule__AtsyraTree__Group_5_3__1 )
            // InternalAtsyRAGoal.g:3768:2: rule__AtsyraTree__Group_5_3__0__Impl rule__AtsyraTree__Group_5_3__1
            {
            pushFollow(FOLLOW_7);
            rule__AtsyraTree__Group_5_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group_5_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5_3__0"


    // $ANTLR start "rule__AtsyraTree__Group_5_3__0__Impl"
    // InternalAtsyRAGoal.g:3775:1: rule__AtsyraTree__Group_5_3__0__Impl : ( ',' ) ;
    public final void rule__AtsyraTree__Group_5_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3779:1: ( ( ',' ) )
            // InternalAtsyRAGoal.g:3780:1: ( ',' )
            {
            // InternalAtsyRAGoal.g:3780:1: ( ',' )
            // InternalAtsyRAGoal.g:3781:2: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getCommaKeyword_5_3_0()); 
            }
            match(input,38,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getCommaKeyword_5_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5_3__0__Impl"


    // $ANTLR start "rule__AtsyraTree__Group_5_3__1"
    // InternalAtsyRAGoal.g:3790:1: rule__AtsyraTree__Group_5_3__1 : rule__AtsyraTree__Group_5_3__1__Impl ;
    public final void rule__AtsyraTree__Group_5_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3794:1: ( rule__AtsyraTree__Group_5_3__1__Impl )
            // InternalAtsyRAGoal.g:3795:2: rule__AtsyraTree__Group_5_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__Group_5_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5_3__1"


    // $ANTLR start "rule__AtsyraTree__Group_5_3__1__Impl"
    // InternalAtsyRAGoal.g:3801:1: rule__AtsyraTree__Group_5_3__1__Impl : ( ( rule__AtsyraTree__OperandsAssignment_5_3_1 ) ) ;
    public final void rule__AtsyraTree__Group_5_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3805:1: ( ( ( rule__AtsyraTree__OperandsAssignment_5_3_1 ) ) )
            // InternalAtsyRAGoal.g:3806:1: ( ( rule__AtsyraTree__OperandsAssignment_5_3_1 ) )
            {
            // InternalAtsyRAGoal.g:3806:1: ( ( rule__AtsyraTree__OperandsAssignment_5_3_1 ) )
            // InternalAtsyRAGoal.g:3807:2: ( rule__AtsyraTree__OperandsAssignment_5_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getOperandsAssignment_5_3_1()); 
            }
            // InternalAtsyRAGoal.g:3808:2: ( rule__AtsyraTree__OperandsAssignment_5_3_1 )
            // InternalAtsyRAGoal.g:3808:3: rule__AtsyraTree__OperandsAssignment_5_3_1
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraTree__OperandsAssignment_5_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getOperandsAssignment_5_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__Group_5_3__1__Impl"


    // $ANTLR start "rule__BooleanLiteral__Group__0"
    // InternalAtsyRAGoal.g:3817:1: rule__BooleanLiteral__Group__0 : rule__BooleanLiteral__Group__0__Impl rule__BooleanLiteral__Group__1 ;
    public final void rule__BooleanLiteral__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3821:1: ( rule__BooleanLiteral__Group__0__Impl rule__BooleanLiteral__Group__1 )
            // InternalAtsyRAGoal.g:3822:2: rule__BooleanLiteral__Group__0__Impl rule__BooleanLiteral__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__BooleanLiteral__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__0"


    // $ANTLR start "rule__BooleanLiteral__Group__0__Impl"
    // InternalAtsyRAGoal.g:3829:1: rule__BooleanLiteral__Group__0__Impl : ( () ) ;
    public final void rule__BooleanLiteral__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3833:1: ( ( () ) )
            // InternalAtsyRAGoal.g:3834:1: ( () )
            {
            // InternalAtsyRAGoal.g:3834:1: ( () )
            // InternalAtsyRAGoal.g:3835:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralAccess().getBooleanLiteralAction_0()); 
            }
            // InternalAtsyRAGoal.g:3836:2: ()
            // InternalAtsyRAGoal.g:3836:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralAccess().getBooleanLiteralAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__0__Impl"


    // $ANTLR start "rule__BooleanLiteral__Group__1"
    // InternalAtsyRAGoal.g:3844:1: rule__BooleanLiteral__Group__1 : rule__BooleanLiteral__Group__1__Impl rule__BooleanLiteral__Group__2 ;
    public final void rule__BooleanLiteral__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3848:1: ( rule__BooleanLiteral__Group__1__Impl rule__BooleanLiteral__Group__2 )
            // InternalAtsyRAGoal.g:3849:2: rule__BooleanLiteral__Group__1__Impl rule__BooleanLiteral__Group__2
            {
            pushFollow(FOLLOW_35);
            rule__BooleanLiteral__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__1"


    // $ANTLR start "rule__BooleanLiteral__Group__1__Impl"
    // InternalAtsyRAGoal.g:3856:1: rule__BooleanLiteral__Group__1__Impl : ( ( rule__BooleanLiteral__NameAssignment_1 ) ) ;
    public final void rule__BooleanLiteral__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3860:1: ( ( ( rule__BooleanLiteral__NameAssignment_1 ) ) )
            // InternalAtsyRAGoal.g:3861:1: ( ( rule__BooleanLiteral__NameAssignment_1 ) )
            {
            // InternalAtsyRAGoal.g:3861:1: ( ( rule__BooleanLiteral__NameAssignment_1 ) )
            // InternalAtsyRAGoal.g:3862:2: ( rule__BooleanLiteral__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralAccess().getNameAssignment_1()); 
            }
            // InternalAtsyRAGoal.g:3863:2: ( rule__BooleanLiteral__NameAssignment_1 )
            // InternalAtsyRAGoal.g:3863:3: rule__BooleanLiteral__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__1__Impl"


    // $ANTLR start "rule__BooleanLiteral__Group__2"
    // InternalAtsyRAGoal.g:3871:1: rule__BooleanLiteral__Group__2 : rule__BooleanLiteral__Group__2__Impl rule__BooleanLiteral__Group__3 ;
    public final void rule__BooleanLiteral__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3875:1: ( rule__BooleanLiteral__Group__2__Impl rule__BooleanLiteral__Group__3 )
            // InternalAtsyRAGoal.g:3876:2: rule__BooleanLiteral__Group__2__Impl rule__BooleanLiteral__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__BooleanLiteral__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__2"


    // $ANTLR start "rule__BooleanLiteral__Group__2__Impl"
    // InternalAtsyRAGoal.g:3883:1: rule__BooleanLiteral__Group__2__Impl : ( '(' ) ;
    public final void rule__BooleanLiteral__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3887:1: ( ( '(' ) )
            // InternalAtsyRAGoal.g:3888:1: ( '(' )
            {
            // InternalAtsyRAGoal.g:3888:1: ( '(' )
            // InternalAtsyRAGoal.g:3889:2: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralAccess().getLeftParenthesisKeyword_2()); 
            }
            match(input,29,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralAccess().getLeftParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__2__Impl"


    // $ANTLR start "rule__BooleanLiteral__Group__3"
    // InternalAtsyRAGoal.g:3898:1: rule__BooleanLiteral__Group__3 : rule__BooleanLiteral__Group__3__Impl rule__BooleanLiteral__Group__4 ;
    public final void rule__BooleanLiteral__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3902:1: ( rule__BooleanLiteral__Group__3__Impl rule__BooleanLiteral__Group__4 )
            // InternalAtsyRAGoal.g:3903:2: rule__BooleanLiteral__Group__3__Impl rule__BooleanLiteral__Group__4
            {
            pushFollow(FOLLOW_23);
            rule__BooleanLiteral__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__3"


    // $ANTLR start "rule__BooleanLiteral__Group__3__Impl"
    // InternalAtsyRAGoal.g:3910:1: rule__BooleanLiteral__Group__3__Impl : ( ( rule__BooleanLiteral__TypeAssignment_3 ) ) ;
    public final void rule__BooleanLiteral__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3914:1: ( ( ( rule__BooleanLiteral__TypeAssignment_3 ) ) )
            // InternalAtsyRAGoal.g:3915:1: ( ( rule__BooleanLiteral__TypeAssignment_3 ) )
            {
            // InternalAtsyRAGoal.g:3915:1: ( ( rule__BooleanLiteral__TypeAssignment_3 ) )
            // InternalAtsyRAGoal.g:3916:2: ( rule__BooleanLiteral__TypeAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralAccess().getTypeAssignment_3()); 
            }
            // InternalAtsyRAGoal.g:3917:2: ( rule__BooleanLiteral__TypeAssignment_3 )
            // InternalAtsyRAGoal.g:3917:3: rule__BooleanLiteral__TypeAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__TypeAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralAccess().getTypeAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__3__Impl"


    // $ANTLR start "rule__BooleanLiteral__Group__4"
    // InternalAtsyRAGoal.g:3925:1: rule__BooleanLiteral__Group__4 : rule__BooleanLiteral__Group__4__Impl ;
    public final void rule__BooleanLiteral__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3929:1: ( rule__BooleanLiteral__Group__4__Impl )
            // InternalAtsyRAGoal.g:3930:2: rule__BooleanLiteral__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__4"


    // $ANTLR start "rule__BooleanLiteral__Group__4__Impl"
    // InternalAtsyRAGoal.g:3936:1: rule__BooleanLiteral__Group__4__Impl : ( ')' ) ;
    public final void rule__BooleanLiteral__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3940:1: ( ( ')' ) )
            // InternalAtsyRAGoal.g:3941:1: ( ')' )
            {
            // InternalAtsyRAGoal.g:3941:1: ( ')' )
            // InternalAtsyRAGoal.g:3942:2: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralAccess().getRightParenthesisKeyword_4()); 
            }
            match(input,30,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralAccess().getRightParenthesisKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group__4__Impl"


    // $ANTLR start "rule__BuildingSystemFeature__Group__0"
    // InternalAtsyRAGoal.g:3952:1: rule__BuildingSystemFeature__Group__0 : rule__BuildingSystemFeature__Group__0__Impl rule__BuildingSystemFeature__Group__1 ;
    public final void rule__BuildingSystemFeature__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3956:1: ( rule__BuildingSystemFeature__Group__0__Impl rule__BuildingSystemFeature__Group__1 )
            // InternalAtsyRAGoal.g:3957:2: rule__BuildingSystemFeature__Group__0__Impl rule__BuildingSystemFeature__Group__1
            {
            pushFollow(FOLLOW_35);
            rule__BuildingSystemFeature__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BuildingSystemFeature__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__Group__0"


    // $ANTLR start "rule__BuildingSystemFeature__Group__0__Impl"
    // InternalAtsyRAGoal.g:3964:1: rule__BuildingSystemFeature__Group__0__Impl : ( ( rule__BuildingSystemFeature__NameAssignment_0 ) ) ;
    public final void rule__BuildingSystemFeature__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3968:1: ( ( ( rule__BuildingSystemFeature__NameAssignment_0 ) ) )
            // InternalAtsyRAGoal.g:3969:1: ( ( rule__BuildingSystemFeature__NameAssignment_0 ) )
            {
            // InternalAtsyRAGoal.g:3969:1: ( ( rule__BuildingSystemFeature__NameAssignment_0 ) )
            // InternalAtsyRAGoal.g:3970:2: ( rule__BuildingSystemFeature__NameAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemFeatureAccess().getNameAssignment_0()); 
            }
            // InternalAtsyRAGoal.g:3971:2: ( rule__BuildingSystemFeature__NameAssignment_0 )
            // InternalAtsyRAGoal.g:3971:3: rule__BuildingSystemFeature__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__BuildingSystemFeature__NameAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemFeatureAccess().getNameAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__Group__0__Impl"


    // $ANTLR start "rule__BuildingSystemFeature__Group__1"
    // InternalAtsyRAGoal.g:3979:1: rule__BuildingSystemFeature__Group__1 : rule__BuildingSystemFeature__Group__1__Impl rule__BuildingSystemFeature__Group__2 ;
    public final void rule__BuildingSystemFeature__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3983:1: ( rule__BuildingSystemFeature__Group__1__Impl rule__BuildingSystemFeature__Group__2 )
            // InternalAtsyRAGoal.g:3984:2: rule__BuildingSystemFeature__Group__1__Impl rule__BuildingSystemFeature__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__BuildingSystemFeature__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BuildingSystemFeature__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__Group__1"


    // $ANTLR start "rule__BuildingSystemFeature__Group__1__Impl"
    // InternalAtsyRAGoal.g:3991:1: rule__BuildingSystemFeature__Group__1__Impl : ( '(' ) ;
    public final void rule__BuildingSystemFeature__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:3995:1: ( ( '(' ) )
            // InternalAtsyRAGoal.g:3996:1: ( '(' )
            {
            // InternalAtsyRAGoal.g:3996:1: ( '(' )
            // InternalAtsyRAGoal.g:3997:2: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemFeatureAccess().getLeftParenthesisKeyword_1()); 
            }
            match(input,29,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemFeatureAccess().getLeftParenthesisKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__Group__1__Impl"


    // $ANTLR start "rule__BuildingSystemFeature__Group__2"
    // InternalAtsyRAGoal.g:4006:1: rule__BuildingSystemFeature__Group__2 : rule__BuildingSystemFeature__Group__2__Impl rule__BuildingSystemFeature__Group__3 ;
    public final void rule__BuildingSystemFeature__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4010:1: ( rule__BuildingSystemFeature__Group__2__Impl rule__BuildingSystemFeature__Group__3 )
            // InternalAtsyRAGoal.g:4011:2: rule__BuildingSystemFeature__Group__2__Impl rule__BuildingSystemFeature__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__BuildingSystemFeature__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BuildingSystemFeature__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__Group__2"


    // $ANTLR start "rule__BuildingSystemFeature__Group__2__Impl"
    // InternalAtsyRAGoal.g:4018:1: rule__BuildingSystemFeature__Group__2__Impl : ( ( rule__BuildingSystemFeature__TypeAssignment_2 ) ) ;
    public final void rule__BuildingSystemFeature__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4022:1: ( ( ( rule__BuildingSystemFeature__TypeAssignment_2 ) ) )
            // InternalAtsyRAGoal.g:4023:1: ( ( rule__BuildingSystemFeature__TypeAssignment_2 ) )
            {
            // InternalAtsyRAGoal.g:4023:1: ( ( rule__BuildingSystemFeature__TypeAssignment_2 ) )
            // InternalAtsyRAGoal.g:4024:2: ( rule__BuildingSystemFeature__TypeAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemFeatureAccess().getTypeAssignment_2()); 
            }
            // InternalAtsyRAGoal.g:4025:2: ( rule__BuildingSystemFeature__TypeAssignment_2 )
            // InternalAtsyRAGoal.g:4025:3: rule__BuildingSystemFeature__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__BuildingSystemFeature__TypeAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemFeatureAccess().getTypeAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__Group__2__Impl"


    // $ANTLR start "rule__BuildingSystemFeature__Group__3"
    // InternalAtsyRAGoal.g:4033:1: rule__BuildingSystemFeature__Group__3 : rule__BuildingSystemFeature__Group__3__Impl ;
    public final void rule__BuildingSystemFeature__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4037:1: ( rule__BuildingSystemFeature__Group__3__Impl )
            // InternalAtsyRAGoal.g:4038:2: rule__BuildingSystemFeature__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BuildingSystemFeature__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__Group__3"


    // $ANTLR start "rule__BuildingSystemFeature__Group__3__Impl"
    // InternalAtsyRAGoal.g:4044:1: rule__BuildingSystemFeature__Group__3__Impl : ( ')' ) ;
    public final void rule__BuildingSystemFeature__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4048:1: ( ( ')' ) )
            // InternalAtsyRAGoal.g:4049:1: ( ')' )
            {
            // InternalAtsyRAGoal.g:4049:1: ( ')' )
            // InternalAtsyRAGoal.g:4050:2: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemFeatureAccess().getRightParenthesisKeyword_3()); 
            }
            match(input,30,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemFeatureAccess().getRightParenthesisKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__Group__3__Impl"


    // $ANTLR start "rule__BuildingSystemConstFeature__Group__0"
    // InternalAtsyRAGoal.g:4060:1: rule__BuildingSystemConstFeature__Group__0 : rule__BuildingSystemConstFeature__Group__0__Impl rule__BuildingSystemConstFeature__Group__1 ;
    public final void rule__BuildingSystemConstFeature__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4064:1: ( rule__BuildingSystemConstFeature__Group__0__Impl rule__BuildingSystemConstFeature__Group__1 )
            // InternalAtsyRAGoal.g:4065:2: rule__BuildingSystemConstFeature__Group__0__Impl rule__BuildingSystemConstFeature__Group__1
            {
            pushFollow(FOLLOW_35);
            rule__BuildingSystemConstFeature__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BuildingSystemConstFeature__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__Group__0"


    // $ANTLR start "rule__BuildingSystemConstFeature__Group__0__Impl"
    // InternalAtsyRAGoal.g:4072:1: rule__BuildingSystemConstFeature__Group__0__Impl : ( ( rule__BuildingSystemConstFeature__NameAssignment_0 ) ) ;
    public final void rule__BuildingSystemConstFeature__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4076:1: ( ( ( rule__BuildingSystemConstFeature__NameAssignment_0 ) ) )
            // InternalAtsyRAGoal.g:4077:1: ( ( rule__BuildingSystemConstFeature__NameAssignment_0 ) )
            {
            // InternalAtsyRAGoal.g:4077:1: ( ( rule__BuildingSystemConstFeature__NameAssignment_0 ) )
            // InternalAtsyRAGoal.g:4078:2: ( rule__BuildingSystemConstFeature__NameAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemConstFeatureAccess().getNameAssignment_0()); 
            }
            // InternalAtsyRAGoal.g:4079:2: ( rule__BuildingSystemConstFeature__NameAssignment_0 )
            // InternalAtsyRAGoal.g:4079:3: rule__BuildingSystemConstFeature__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__BuildingSystemConstFeature__NameAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemConstFeatureAccess().getNameAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__Group__0__Impl"


    // $ANTLR start "rule__BuildingSystemConstFeature__Group__1"
    // InternalAtsyRAGoal.g:4087:1: rule__BuildingSystemConstFeature__Group__1 : rule__BuildingSystemConstFeature__Group__1__Impl rule__BuildingSystemConstFeature__Group__2 ;
    public final void rule__BuildingSystemConstFeature__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4091:1: ( rule__BuildingSystemConstFeature__Group__1__Impl rule__BuildingSystemConstFeature__Group__2 )
            // InternalAtsyRAGoal.g:4092:2: rule__BuildingSystemConstFeature__Group__1__Impl rule__BuildingSystemConstFeature__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__BuildingSystemConstFeature__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BuildingSystemConstFeature__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__Group__1"


    // $ANTLR start "rule__BuildingSystemConstFeature__Group__1__Impl"
    // InternalAtsyRAGoal.g:4099:1: rule__BuildingSystemConstFeature__Group__1__Impl : ( '(' ) ;
    public final void rule__BuildingSystemConstFeature__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4103:1: ( ( '(' ) )
            // InternalAtsyRAGoal.g:4104:1: ( '(' )
            {
            // InternalAtsyRAGoal.g:4104:1: ( '(' )
            // InternalAtsyRAGoal.g:4105:2: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemConstFeatureAccess().getLeftParenthesisKeyword_1()); 
            }
            match(input,29,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemConstFeatureAccess().getLeftParenthesisKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__Group__1__Impl"


    // $ANTLR start "rule__BuildingSystemConstFeature__Group__2"
    // InternalAtsyRAGoal.g:4114:1: rule__BuildingSystemConstFeature__Group__2 : rule__BuildingSystemConstFeature__Group__2__Impl rule__BuildingSystemConstFeature__Group__3 ;
    public final void rule__BuildingSystemConstFeature__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4118:1: ( rule__BuildingSystemConstFeature__Group__2__Impl rule__BuildingSystemConstFeature__Group__3 )
            // InternalAtsyRAGoal.g:4119:2: rule__BuildingSystemConstFeature__Group__2__Impl rule__BuildingSystemConstFeature__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__BuildingSystemConstFeature__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BuildingSystemConstFeature__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__Group__2"


    // $ANTLR start "rule__BuildingSystemConstFeature__Group__2__Impl"
    // InternalAtsyRAGoal.g:4126:1: rule__BuildingSystemConstFeature__Group__2__Impl : ( ( rule__BuildingSystemConstFeature__TypeAssignment_2 ) ) ;
    public final void rule__BuildingSystemConstFeature__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4130:1: ( ( ( rule__BuildingSystemConstFeature__TypeAssignment_2 ) ) )
            // InternalAtsyRAGoal.g:4131:1: ( ( rule__BuildingSystemConstFeature__TypeAssignment_2 ) )
            {
            // InternalAtsyRAGoal.g:4131:1: ( ( rule__BuildingSystemConstFeature__TypeAssignment_2 ) )
            // InternalAtsyRAGoal.g:4132:2: ( rule__BuildingSystemConstFeature__TypeAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemConstFeatureAccess().getTypeAssignment_2()); 
            }
            // InternalAtsyRAGoal.g:4133:2: ( rule__BuildingSystemConstFeature__TypeAssignment_2 )
            // InternalAtsyRAGoal.g:4133:3: rule__BuildingSystemConstFeature__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__BuildingSystemConstFeature__TypeAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemConstFeatureAccess().getTypeAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__Group__2__Impl"


    // $ANTLR start "rule__BuildingSystemConstFeature__Group__3"
    // InternalAtsyRAGoal.g:4141:1: rule__BuildingSystemConstFeature__Group__3 : rule__BuildingSystemConstFeature__Group__3__Impl ;
    public final void rule__BuildingSystemConstFeature__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4145:1: ( rule__BuildingSystemConstFeature__Group__3__Impl )
            // InternalAtsyRAGoal.g:4146:2: rule__BuildingSystemConstFeature__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BuildingSystemConstFeature__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__Group__3"


    // $ANTLR start "rule__BuildingSystemConstFeature__Group__3__Impl"
    // InternalAtsyRAGoal.g:4152:1: rule__BuildingSystemConstFeature__Group__3__Impl : ( ')' ) ;
    public final void rule__BuildingSystemConstFeature__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4156:1: ( ( ')' ) )
            // InternalAtsyRAGoal.g:4157:1: ( ')' )
            {
            // InternalAtsyRAGoal.g:4157:1: ( ')' )
            // InternalAtsyRAGoal.g:4158:2: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemConstFeatureAccess().getRightParenthesisKeyword_3()); 
            }
            match(input,30,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemConstFeatureAccess().getRightParenthesisKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__Group__3__Impl"


    // $ANTLR start "rule__InternalType__Group__0"
    // InternalAtsyRAGoal.g:4168:1: rule__InternalType__Group__0 : rule__InternalType__Group__0__Impl rule__InternalType__Group__1 ;
    public final void rule__InternalType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4172:1: ( rule__InternalType__Group__0__Impl rule__InternalType__Group__1 )
            // InternalAtsyRAGoal.g:4173:2: rule__InternalType__Group__0__Impl rule__InternalType__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__InternalType__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__InternalType__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InternalType__Group__0"


    // $ANTLR start "rule__InternalType__Group__0__Impl"
    // InternalAtsyRAGoal.g:4180:1: rule__InternalType__Group__0__Impl : ( () ) ;
    public final void rule__InternalType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4184:1: ( ( () ) )
            // InternalAtsyRAGoal.g:4185:1: ( () )
            {
            // InternalAtsyRAGoal.g:4185:1: ( () )
            // InternalAtsyRAGoal.g:4186:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInternalTypeAccess().getInternalTypeAction_0()); 
            }
            // InternalAtsyRAGoal.g:4187:2: ()
            // InternalAtsyRAGoal.g:4187:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInternalTypeAccess().getInternalTypeAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InternalType__Group__0__Impl"


    // $ANTLR start "rule__InternalType__Group__1"
    // InternalAtsyRAGoal.g:4195:1: rule__InternalType__Group__1 : rule__InternalType__Group__1__Impl rule__InternalType__Group__2 ;
    public final void rule__InternalType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4199:1: ( rule__InternalType__Group__1__Impl rule__InternalType__Group__2 )
            // InternalAtsyRAGoal.g:4200:2: rule__InternalType__Group__1__Impl rule__InternalType__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__InternalType__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__InternalType__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InternalType__Group__1"


    // $ANTLR start "rule__InternalType__Group__1__Impl"
    // InternalAtsyRAGoal.g:4207:1: rule__InternalType__Group__1__Impl : ( 'InternalType' ) ;
    public final void rule__InternalType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4211:1: ( ( 'InternalType' ) )
            // InternalAtsyRAGoal.g:4212:1: ( 'InternalType' )
            {
            // InternalAtsyRAGoal.g:4212:1: ( 'InternalType' )
            // InternalAtsyRAGoal.g:4213:2: 'InternalType'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInternalTypeAccess().getInternalTypeKeyword_1()); 
            }
            match(input,41,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInternalTypeAccess().getInternalTypeKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InternalType__Group__1__Impl"


    // $ANTLR start "rule__InternalType__Group__2"
    // InternalAtsyRAGoal.g:4222:1: rule__InternalType__Group__2 : rule__InternalType__Group__2__Impl ;
    public final void rule__InternalType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4226:1: ( rule__InternalType__Group__2__Impl )
            // InternalAtsyRAGoal.g:4227:2: rule__InternalType__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InternalType__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InternalType__Group__2"


    // $ANTLR start "rule__InternalType__Group__2__Impl"
    // InternalAtsyRAGoal.g:4233:1: rule__InternalType__Group__2__Impl : ( ( rule__InternalType__NameAssignment_2 ) ) ;
    public final void rule__InternalType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4237:1: ( ( ( rule__InternalType__NameAssignment_2 ) ) )
            // InternalAtsyRAGoal.g:4238:1: ( ( rule__InternalType__NameAssignment_2 ) )
            {
            // InternalAtsyRAGoal.g:4238:1: ( ( rule__InternalType__NameAssignment_2 ) )
            // InternalAtsyRAGoal.g:4239:2: ( rule__InternalType__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInternalTypeAccess().getNameAssignment_2()); 
            }
            // InternalAtsyRAGoal.g:4240:2: ( rule__InternalType__NameAssignment_2 )
            // InternalAtsyRAGoal.g:4240:3: rule__InternalType__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__InternalType__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInternalTypeAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InternalType__Group__2__Impl"


    // $ANTLR start "rule__SystemType__Group__0"
    // InternalAtsyRAGoal.g:4249:1: rule__SystemType__Group__0 : rule__SystemType__Group__0__Impl rule__SystemType__Group__1 ;
    public final void rule__SystemType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4253:1: ( rule__SystemType__Group__0__Impl rule__SystemType__Group__1 )
            // InternalAtsyRAGoal.g:4254:2: rule__SystemType__Group__0__Impl rule__SystemType__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__SystemType__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__SystemType__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SystemType__Group__0"


    // $ANTLR start "rule__SystemType__Group__0__Impl"
    // InternalAtsyRAGoal.g:4261:1: rule__SystemType__Group__0__Impl : ( () ) ;
    public final void rule__SystemType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4265:1: ( ( () ) )
            // InternalAtsyRAGoal.g:4266:1: ( () )
            {
            // InternalAtsyRAGoal.g:4266:1: ( () )
            // InternalAtsyRAGoal.g:4267:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSystemTypeAccess().getSystemTypeAction_0()); 
            }
            // InternalAtsyRAGoal.g:4268:2: ()
            // InternalAtsyRAGoal.g:4268:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSystemTypeAccess().getSystemTypeAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SystemType__Group__0__Impl"


    // $ANTLR start "rule__SystemType__Group__1"
    // InternalAtsyRAGoal.g:4276:1: rule__SystemType__Group__1 : rule__SystemType__Group__1__Impl rule__SystemType__Group__2 ;
    public final void rule__SystemType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4280:1: ( rule__SystemType__Group__1__Impl rule__SystemType__Group__2 )
            // InternalAtsyRAGoal.g:4281:2: rule__SystemType__Group__1__Impl rule__SystemType__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__SystemType__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__SystemType__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SystemType__Group__1"


    // $ANTLR start "rule__SystemType__Group__1__Impl"
    // InternalAtsyRAGoal.g:4288:1: rule__SystemType__Group__1__Impl : ( 'SystemType' ) ;
    public final void rule__SystemType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4292:1: ( ( 'SystemType' ) )
            // InternalAtsyRAGoal.g:4293:1: ( 'SystemType' )
            {
            // InternalAtsyRAGoal.g:4293:1: ( 'SystemType' )
            // InternalAtsyRAGoal.g:4294:2: 'SystemType'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSystemTypeAccess().getSystemTypeKeyword_1()); 
            }
            match(input,42,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSystemTypeAccess().getSystemTypeKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SystemType__Group__1__Impl"


    // $ANTLR start "rule__SystemType__Group__2"
    // InternalAtsyRAGoal.g:4303:1: rule__SystemType__Group__2 : rule__SystemType__Group__2__Impl ;
    public final void rule__SystemType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4307:1: ( rule__SystemType__Group__2__Impl )
            // InternalAtsyRAGoal.g:4308:2: rule__SystemType__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SystemType__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SystemType__Group__2"


    // $ANTLR start "rule__SystemType__Group__2__Impl"
    // InternalAtsyRAGoal.g:4314:1: rule__SystemType__Group__2__Impl : ( ( rule__SystemType__NameAssignment_2 ) ) ;
    public final void rule__SystemType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4318:1: ( ( ( rule__SystemType__NameAssignment_2 ) ) )
            // InternalAtsyRAGoal.g:4319:1: ( ( rule__SystemType__NameAssignment_2 ) )
            {
            // InternalAtsyRAGoal.g:4319:1: ( ( rule__SystemType__NameAssignment_2 ) )
            // InternalAtsyRAGoal.g:4320:2: ( rule__SystemType__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSystemTypeAccess().getNameAssignment_2()); 
            }
            // InternalAtsyRAGoal.g:4321:2: ( rule__SystemType__NameAssignment_2 )
            // InternalAtsyRAGoal.g:4321:3: rule__SystemType__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SystemType__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSystemTypeAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SystemType__Group__2__Impl"


    // $ANTLR start "rule__AtsyraGoal__UnorderedGroup"
    // InternalAtsyRAGoal.g:4330:1: rule__AtsyraGoal__UnorderedGroup : rule__AtsyraGoal__UnorderedGroup__0 {...}?;
    public final void rule__AtsyraGoal__UnorderedGroup() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup());
        	
        try {
            // InternalAtsyRAGoal.g:4335:1: ( rule__AtsyraGoal__UnorderedGroup__0 {...}?)
            // InternalAtsyRAGoal.g:4336:2: rule__AtsyraGoal__UnorderedGroup__0 {...}?
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__UnorderedGroup__0();

            state._fsp--;
            if (state.failed) return ;
            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup()) ) {
                if (state.backtracking>0) {state.failed=true; return ;}
                throw new FailedPredicateException(input, "rule__AtsyraGoal__UnorderedGroup", "getUnorderedGroupHelper().canLeave(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup())");
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__UnorderedGroup"


    // $ANTLR start "rule__AtsyraGoal__UnorderedGroup__Impl"
    // InternalAtsyRAGoal.g:4344:1: rule__AtsyraGoal__UnorderedGroup__Impl : ( ({...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) ) ) | ({...}? => ( ( ( rule__AtsyraGoal__Group_1__0 ) ) ) ) ) ;
    public final void rule__AtsyraGoal__UnorderedGroup__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalAtsyRAGoal.g:4349:1: ( ( ({...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) ) ) | ({...}? => ( ( ( rule__AtsyraGoal__Group_1__0 ) ) ) ) ) )
            // InternalAtsyRAGoal.g:4350:3: ( ({...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) ) ) | ({...}? => ( ( ( rule__AtsyraGoal__Group_1__0 ) ) ) ) )
            {
            // InternalAtsyRAGoal.g:4350:3: ( ({...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) ) ) | ({...}? => ( ( ( rule__AtsyraGoal__Group_1__0 ) ) ) ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( LA35_0 == 33 && getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0) ) {
                alt35=1;
            }
            else if ( ( LA35_0 == 22 || LA35_0 == 37 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1) ) {
                alt35=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // InternalAtsyRAGoal.g:4351:3: ({...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) ) )
                    {
                    // InternalAtsyRAGoal.g:4351:3: ({...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) ) )
                    // InternalAtsyRAGoal.g:4352:4: {...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__AtsyraGoal__UnorderedGroup__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0)");
                    }
                    // InternalAtsyRAGoal.g:4352:104: ( ( ( rule__AtsyraGoal__Group_0__0 ) ) )
                    // InternalAtsyRAGoal.g:4353:5: ( ( rule__AtsyraGoal__Group_0__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0);
                    selected = true;
                    // InternalAtsyRAGoal.g:4359:5: ( ( rule__AtsyraGoal__Group_0__0 ) )
                    // InternalAtsyRAGoal.g:4360:6: ( rule__AtsyraGoal__Group_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAtsyraGoalAccess().getGroup_0()); 
                    }
                    // InternalAtsyRAGoal.g:4361:6: ( rule__AtsyraGoal__Group_0__0 )
                    // InternalAtsyRAGoal.g:4361:7: rule__AtsyraGoal__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoal__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAtsyraGoalAccess().getGroup_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:4366:3: ({...}? => ( ( ( rule__AtsyraGoal__Group_1__0 ) ) ) )
                    {
                    // InternalAtsyRAGoal.g:4366:3: ({...}? => ( ( ( rule__AtsyraGoal__Group_1__0 ) ) ) )
                    // InternalAtsyRAGoal.g:4367:4: {...}? => ( ( ( rule__AtsyraGoal__Group_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__AtsyraGoal__UnorderedGroup__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1)");
                    }
                    // InternalAtsyRAGoal.g:4367:104: ( ( ( rule__AtsyraGoal__Group_1__0 ) ) )
                    // InternalAtsyRAGoal.g:4368:5: ( ( rule__AtsyraGoal__Group_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1);
                    selected = true;
                    // InternalAtsyRAGoal.g:4374:5: ( ( rule__AtsyraGoal__Group_1__0 ) )
                    // InternalAtsyRAGoal.g:4375:6: ( rule__AtsyraGoal__Group_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAtsyraGoalAccess().getGroup_1()); 
                    }
                    // InternalAtsyRAGoal.g:4376:6: ( rule__AtsyraGoal__Group_1__0 )
                    // InternalAtsyRAGoal.g:4376:7: rule__AtsyraGoal__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoal__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAtsyraGoalAccess().getGroup_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__UnorderedGroup__Impl"


    // $ANTLR start "rule__AtsyraGoal__UnorderedGroup__0"
    // InternalAtsyRAGoal.g:4389:1: rule__AtsyraGoal__UnorderedGroup__0 : rule__AtsyraGoal__UnorderedGroup__Impl ( rule__AtsyraGoal__UnorderedGroup__1 )? ;
    public final void rule__AtsyraGoal__UnorderedGroup__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4393:1: ( rule__AtsyraGoal__UnorderedGroup__Impl ( rule__AtsyraGoal__UnorderedGroup__1 )? )
            // InternalAtsyRAGoal.g:4394:2: rule__AtsyraGoal__UnorderedGroup__Impl ( rule__AtsyraGoal__UnorderedGroup__1 )?
            {
            pushFollow(FOLLOW_17);
            rule__AtsyraGoal__UnorderedGroup__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalAtsyRAGoal.g:4395:2: ( rule__AtsyraGoal__UnorderedGroup__1 )?
            int alt36=2;
            switch ( input.LA(1) ) {
                case 33:
                    {
                    int LA36_1 = input.LA(2);

                    if ( synpred41_InternalAtsyRAGoal() && getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0) ) {
                        alt36=1;
                    }
                    }
                    break;
                case 37:
                    {
                    int LA36_2 = input.LA(2);

                    if ( synpred41_InternalAtsyRAGoal() && getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1) ) {
                        alt36=1;
                    }
                    }
                    break;
                case 22:
                    {
                    int LA36_3 = input.LA(2);

                    if ( synpred41_InternalAtsyRAGoal() && getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1) ) {
                        alt36=1;
                    }
                    }
                    break;
            }

            switch (alt36) {
                case 1 :
                    // InternalAtsyRAGoal.g:0:0: rule__AtsyraGoal__UnorderedGroup__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtsyraGoal__UnorderedGroup__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__UnorderedGroup__0"


    // $ANTLR start "rule__AtsyraGoal__UnorderedGroup__1"
    // InternalAtsyRAGoal.g:4401:1: rule__AtsyraGoal__UnorderedGroup__1 : rule__AtsyraGoal__UnorderedGroup__Impl ;
    public final void rule__AtsyraGoal__UnorderedGroup__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4405:1: ( rule__AtsyraGoal__UnorderedGroup__Impl )
            // InternalAtsyRAGoal.g:4406:2: rule__AtsyraGoal__UnorderedGroup__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtsyraGoal__UnorderedGroup__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__UnorderedGroup__1"


    // $ANTLR start "rule__AtsyraGoalModel__ImportsAssignment_1"
    // InternalAtsyRAGoal.g:4413:1: rule__AtsyraGoalModel__ImportsAssignment_1 : ( ruleImport ) ;
    public final void rule__AtsyraGoalModel__ImportsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4417:1: ( ( ruleImport ) )
            // InternalAtsyRAGoal.g:4418:2: ( ruleImport )
            {
            // InternalAtsyRAGoal.g:4418:2: ( ruleImport )
            // InternalAtsyRAGoal.g:4419:3: ruleImport
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getImportsImportParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getImportsImportParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__ImportsAssignment_1"


    // $ANTLR start "rule__AtsyraGoalModel__DefaultValuesAssignment_4_2"
    // InternalAtsyRAGoal.g:4428:1: rule__AtsyraGoalModel__DefaultValuesAssignment_4_2 : ( ruleDefaultValues ) ;
    public final void rule__AtsyraGoalModel__DefaultValuesAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4432:1: ( ( ruleDefaultValues ) )
            // InternalAtsyRAGoal.g:4433:2: ( ruleDefaultValues )
            {
            // InternalAtsyRAGoal.g:4433:2: ( ruleDefaultValues )
            // InternalAtsyRAGoal.g:4434:3: ruleDefaultValues
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesDefaultValuesParserRuleCall_4_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDefaultValues();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesDefaultValuesParserRuleCall_4_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__DefaultValuesAssignment_4_2"


    // $ANTLR start "rule__AtsyraGoalModel__DefaultValuesAssignment_4_3"
    // InternalAtsyRAGoal.g:4443:1: rule__AtsyraGoalModel__DefaultValuesAssignment_4_3 : ( ruleDefaultValues ) ;
    public final void rule__AtsyraGoalModel__DefaultValuesAssignment_4_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4447:1: ( ( ruleDefaultValues ) )
            // InternalAtsyRAGoal.g:4448:2: ( ruleDefaultValues )
            {
            // InternalAtsyRAGoal.g:4448:2: ( ruleDefaultValues )
            // InternalAtsyRAGoal.g:4449:3: ruleDefaultValues
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesDefaultValuesParserRuleCall_4_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDefaultValues();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesDefaultValuesParserRuleCall_4_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__DefaultValuesAssignment_4_3"


    // $ANTLR start "rule__AtsyraGoalModel__TypesAssignment_5_2"
    // InternalAtsyRAGoal.g:4458:1: rule__AtsyraGoalModel__TypesAssignment_5_2 : ( ruleType ) ;
    public final void rule__AtsyraGoalModel__TypesAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4462:1: ( ( ruleType ) )
            // InternalAtsyRAGoal.g:4463:2: ( ruleType )
            {
            // InternalAtsyRAGoal.g:4463:2: ( ruleType )
            // InternalAtsyRAGoal.g:4464:3: ruleType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTypesTypeParserRuleCall_5_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTypesTypeParserRuleCall_5_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__TypesAssignment_5_2"


    // $ANTLR start "rule__AtsyraGoalModel__TypesAssignment_5_3"
    // InternalAtsyRAGoal.g:4473:1: rule__AtsyraGoalModel__TypesAssignment_5_3 : ( ruleType ) ;
    public final void rule__AtsyraGoalModel__TypesAssignment_5_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4477:1: ( ( ruleType ) )
            // InternalAtsyRAGoal.g:4478:2: ( ruleType )
            {
            // InternalAtsyRAGoal.g:4478:2: ( ruleType )
            // InternalAtsyRAGoal.g:4479:3: ruleType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTypesTypeParserRuleCall_5_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTypesTypeParserRuleCall_5_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__TypesAssignment_5_3"


    // $ANTLR start "rule__AtsyraGoalModel__TypedElementsAssignment_6_2"
    // InternalAtsyRAGoal.g:4488:1: rule__AtsyraGoalModel__TypedElementsAssignment_6_2 : ( ruleTypedElementDecl ) ;
    public final void rule__AtsyraGoalModel__TypedElementsAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4492:1: ( ( ruleTypedElementDecl ) )
            // InternalAtsyRAGoal.g:4493:2: ( ruleTypedElementDecl )
            {
            // InternalAtsyRAGoal.g:4493:2: ( ruleTypedElementDecl )
            // InternalAtsyRAGoal.g:4494:3: ruleTypedElementDecl
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsTypedElementDeclParserRuleCall_6_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleTypedElementDecl();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsTypedElementDeclParserRuleCall_6_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__TypedElementsAssignment_6_2"


    // $ANTLR start "rule__AtsyraGoalModel__TypedElementsAssignment_6_3"
    // InternalAtsyRAGoal.g:4503:1: rule__AtsyraGoalModel__TypedElementsAssignment_6_3 : ( ruleTypedElementDecl ) ;
    public final void rule__AtsyraGoalModel__TypedElementsAssignment_6_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4507:1: ( ( ruleTypedElementDecl ) )
            // InternalAtsyRAGoal.g:4508:2: ( ruleTypedElementDecl )
            {
            // InternalAtsyRAGoal.g:4508:2: ( ruleTypedElementDecl )
            // InternalAtsyRAGoal.g:4509:3: ruleTypedElementDecl
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsTypedElementDeclParserRuleCall_6_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleTypedElementDecl();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsTypedElementDeclParserRuleCall_6_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__TypedElementsAssignment_6_3"


    // $ANTLR start "rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2"
    // InternalAtsyRAGoal.g:4518:1: rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2 : ( ruleAtsyraGoal ) ;
    public final void rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4522:1: ( ( ruleAtsyraGoal ) )
            // InternalAtsyRAGoal.g:4523:2: ( ruleAtsyraGoal )
            {
            // InternalAtsyRAGoal.g:4523:2: ( ruleAtsyraGoal )
            // InternalAtsyRAGoal.g:4524:3: ruleAtsyraGoal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAtsyraGoalParserRuleCall_7_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAtsyraGoal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAtsyraGoalParserRuleCall_7_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__AtsyragoalsAssignment_7_2"


    // $ANTLR start "rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3"
    // InternalAtsyRAGoal.g:4533:1: rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3 : ( ruleAtsyraGoal ) ;
    public final void rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4537:1: ( ( ruleAtsyraGoal ) )
            // InternalAtsyRAGoal.g:4538:2: ( ruleAtsyraGoal )
            {
            // InternalAtsyRAGoal.g:4538:2: ( ruleAtsyraGoal )
            // InternalAtsyRAGoal.g:4539:3: ruleAtsyraGoal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAtsyraGoalParserRuleCall_7_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAtsyraGoal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAtsyraGoalParserRuleCall_7_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__AtsyragoalsAssignment_7_3"


    // $ANTLR start "rule__AtsyraGoalModel__TreesAssignment_8_2"
    // InternalAtsyRAGoal.g:4548:1: rule__AtsyraGoalModel__TreesAssignment_8_2 : ( ruleAtsyraTree ) ;
    public final void rule__AtsyraGoalModel__TreesAssignment_8_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4552:1: ( ( ruleAtsyraTree ) )
            // InternalAtsyRAGoal.g:4553:2: ( ruleAtsyraTree )
            {
            // InternalAtsyRAGoal.g:4553:2: ( ruleAtsyraTree )
            // InternalAtsyRAGoal.g:4554:3: ruleAtsyraTree
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTreesAtsyraTreeParserRuleCall_8_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAtsyraTree();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTreesAtsyraTreeParserRuleCall_8_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__TreesAssignment_8_2"


    // $ANTLR start "rule__AtsyraGoalModel__TreesAssignment_8_3"
    // InternalAtsyRAGoal.g:4563:1: rule__AtsyraGoalModel__TreesAssignment_8_3 : ( ruleAtsyraTree ) ;
    public final void rule__AtsyraGoalModel__TreesAssignment_8_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4567:1: ( ( ruleAtsyraTree ) )
            // InternalAtsyRAGoal.g:4568:2: ( ruleAtsyraTree )
            {
            // InternalAtsyRAGoal.g:4568:2: ( ruleAtsyraTree )
            // InternalAtsyRAGoal.g:4569:3: ruleAtsyraTree
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalModelAccess().getTreesAtsyraTreeParserRuleCall_8_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAtsyraTree();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalModelAccess().getTreesAtsyraTreeParserRuleCall_8_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoalModel__TreesAssignment_8_3"


    // $ANTLR start "rule__Import__ImportURIAssignment_1"
    // InternalAtsyRAGoal.g:4578:1: rule__Import__ImportURIAssignment_1 : ( ruleEString ) ;
    public final void rule__Import__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4582:1: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4583:2: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:4583:2: ( ruleEString )
            // InternalAtsyRAGoal.g:4584:3: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURIEStringParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURIEStringParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportURIAssignment_1"


    // $ANTLR start "rule__OrCondition__OperandsAssignment_1_2"
    // InternalAtsyRAGoal.g:4593:1: rule__OrCondition__OperandsAssignment_1_2 : ( ruleAndCondition ) ;
    public final void rule__OrCondition__OperandsAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4597:1: ( ( ruleAndCondition ) )
            // InternalAtsyRAGoal.g:4598:2: ( ruleAndCondition )
            {
            // InternalAtsyRAGoal.g:4598:2: ( ruleAndCondition )
            // InternalAtsyRAGoal.g:4599:3: ruleAndCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrConditionAccess().getOperandsAndConditionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAndCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrConditionAccess().getOperandsAndConditionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrCondition__OperandsAssignment_1_2"


    // $ANTLR start "rule__AndCondition__OperandsAssignment_1_2"
    // InternalAtsyRAGoal.g:4608:1: rule__AndCondition__OperandsAssignment_1_2 : ( ruleTerminalCondition ) ;
    public final void rule__AndCondition__OperandsAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4612:1: ( ( ruleTerminalCondition ) )
            // InternalAtsyRAGoal.g:4613:2: ( ruleTerminalCondition )
            {
            // InternalAtsyRAGoal.g:4613:2: ( ruleTerminalCondition )
            // InternalAtsyRAGoal.g:4614:3: ruleTerminalCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndConditionAccess().getOperandsTerminalConditionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleTerminalCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndConditionAccess().getOperandsTerminalConditionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndCondition__OperandsAssignment_1_2"


    // $ANTLR start "rule__TerminalCondition__OperandsAssignment_1_2"
    // InternalAtsyRAGoal.g:4623:1: rule__TerminalCondition__OperandsAssignment_1_2 : ( ruleTerminalCondition ) ;
    public final void rule__TerminalCondition__OperandsAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4627:1: ( ( ruleTerminalCondition ) )
            // InternalAtsyRAGoal.g:4628:2: ( ruleTerminalCondition )
            {
            // InternalAtsyRAGoal.g:4628:2: ( ruleTerminalCondition )
            // InternalAtsyRAGoal.g:4629:3: ruleTerminalCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalConditionAccess().getOperandsTerminalConditionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleTerminalCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalConditionAccess().getOperandsTerminalConditionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalCondition__OperandsAssignment_1_2"


    // $ANTLR start "rule__EqualOrBooleanCond__SourceAssignment_0_1"
    // InternalAtsyRAGoal.g:4638:1: rule__EqualOrBooleanCond__SourceAssignment_0_1 : ( ( ruleTypedElementString ) ) ;
    public final void rule__EqualOrBooleanCond__SourceAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4642:1: ( ( ( ruleTypedElementString ) ) )
            // InternalAtsyRAGoal.g:4643:2: ( ( ruleTypedElementString ) )
            {
            // InternalAtsyRAGoal.g:4643:2: ( ( ruleTypedElementString ) )
            // InternalAtsyRAGoal.g:4644:3: ( ruleTypedElementString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementCrossReference_0_1_0()); 
            }
            // InternalAtsyRAGoal.g:4645:3: ( ruleTypedElementString )
            // InternalAtsyRAGoal.g:4646:4: ruleTypedElementString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementTypedElementStringParserRuleCall_0_1_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleTypedElementString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementTypedElementStringParserRuleCall_0_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementCrossReference_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__SourceAssignment_0_1"


    // $ANTLR start "rule__EqualOrBooleanCond__SourceAssignment_1_1"
    // InternalAtsyRAGoal.g:4657:1: rule__EqualOrBooleanCond__SourceAssignment_1_1 : ( ( ruleTypedElementString ) ) ;
    public final void rule__EqualOrBooleanCond__SourceAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4661:1: ( ( ( ruleTypedElementString ) ) )
            // InternalAtsyRAGoal.g:4662:2: ( ( ruleTypedElementString ) )
            {
            // InternalAtsyRAGoal.g:4662:2: ( ( ruleTypedElementString ) )
            // InternalAtsyRAGoal.g:4663:3: ( ruleTypedElementString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementCrossReference_1_1_0()); 
            }
            // InternalAtsyRAGoal.g:4664:3: ( ruleTypedElementString )
            // InternalAtsyRAGoal.g:4665:4: ruleTypedElementString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementTypedElementStringParserRuleCall_1_1_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleTypedElementString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementTypedElementStringParserRuleCall_1_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementCrossReference_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__SourceAssignment_1_1"


    // $ANTLR start "rule__EqualOrBooleanCond__TargetAssignment_1_2_1"
    // InternalAtsyRAGoal.g:4676:1: rule__EqualOrBooleanCond__TargetAssignment_1_2_1 : ( ( ruleTypedElementString ) ) ;
    public final void rule__EqualOrBooleanCond__TargetAssignment_1_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4680:1: ( ( ( ruleTypedElementString ) ) )
            // InternalAtsyRAGoal.g:4681:2: ( ( ruleTypedElementString ) )
            {
            // InternalAtsyRAGoal.g:4681:2: ( ( ruleTypedElementString ) )
            // InternalAtsyRAGoal.g:4682:3: ( ruleTypedElementString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getTargetTypedElementCrossReference_1_2_1_0()); 
            }
            // InternalAtsyRAGoal.g:4683:3: ( ruleTypedElementString )
            // InternalAtsyRAGoal.g:4684:4: ruleTypedElementString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqualOrBooleanCondAccess().getTargetTypedElementTypedElementStringParserRuleCall_1_2_1_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleTypedElementString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getTargetTypedElementTypedElementStringParserRuleCall_1_2_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqualOrBooleanCondAccess().getTargetTypedElementCrossReference_1_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOrBooleanCond__TargetAssignment_1_2_1"


    // $ANTLR start "rule__AtsyraGoal__NameAssignment_0_1"
    // InternalAtsyRAGoal.g:4695:1: rule__AtsyraGoal__NameAssignment_0_1 : ( ruleEString ) ;
    public final void rule__AtsyraGoal__NameAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4699:1: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4700:2: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:4700:2: ( ruleEString )
            // InternalAtsyRAGoal.g:4701:3: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getNameEStringParserRuleCall_0_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getNameEStringParserRuleCall_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__NameAssignment_0_1"


    // $ANTLR start "rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1"
    // InternalAtsyRAGoal.g:4710:1: rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1 : ( ( ruleEString ) ) ;
    public final void rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4714:1: ( ( ( ruleEString ) ) )
            // InternalAtsyRAGoal.g:4715:2: ( ( ruleEString ) )
            {
            // InternalAtsyRAGoal.g:4715:2: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4716:3: ( ruleEString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPreDefaultValuesCrossReference_0_3_1_1_0()); 
            }
            // InternalAtsyRAGoal.g:4717:3: ( ruleEString )
            // InternalAtsyRAGoal.g:4718:4: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPreDefaultValuesEStringParserRuleCall_0_3_1_1_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPreDefaultValuesEStringParserRuleCall_0_3_1_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPreDefaultValuesCrossReference_0_3_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__DefaultUsedInPreAssignment_0_3_1_1"


    // $ANTLR start "rule__AtsyraGoal__PreconditionAssignment_0_3_3"
    // InternalAtsyRAGoal.g:4729:1: rule__AtsyraGoal__PreconditionAssignment_0_3_3 : ( ruleGoalCondition ) ;
    public final void rule__AtsyraGoal__PreconditionAssignment_0_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4733:1: ( ( ruleGoalCondition ) )
            // InternalAtsyRAGoal.g:4734:2: ( ruleGoalCondition )
            {
            // InternalAtsyRAGoal.g:4734:2: ( ruleGoalCondition )
            // InternalAtsyRAGoal.g:4735:3: ruleGoalCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getPreconditionGoalConditionParserRuleCall_0_3_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGoalCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getPreconditionGoalConditionParserRuleCall_0_3_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__PreconditionAssignment_0_3_3"


    // $ANTLR start "rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1"
    // InternalAtsyRAGoal.g:4744:1: rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1 : ( ( ruleEString ) ) ;
    public final void rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4748:1: ( ( ( ruleEString ) ) )
            // InternalAtsyRAGoal.g:4749:2: ( ( ruleEString ) )
            {
            // InternalAtsyRAGoal.g:4749:2: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4750:3: ( ruleEString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPostDefaultValuesCrossReference_1_0_1_1_0()); 
            }
            // InternalAtsyRAGoal.g:4751:3: ( ruleEString )
            // InternalAtsyRAGoal.g:4752:4: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPostDefaultValuesEStringParserRuleCall_1_0_1_1_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPostDefaultValuesEStringParserRuleCall_1_0_1_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPostDefaultValuesCrossReference_1_0_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__DefaultUsedInPostAssignment_1_0_1_1"


    // $ANTLR start "rule__AtsyraGoal__PostconditionAssignment_1_0_3"
    // InternalAtsyRAGoal.g:4763:1: rule__AtsyraGoal__PostconditionAssignment_1_0_3 : ( ruleGoalCondition ) ;
    public final void rule__AtsyraGoal__PostconditionAssignment_1_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4767:1: ( ( ruleGoalCondition ) )
            // InternalAtsyRAGoal.g:4768:2: ( ruleGoalCondition )
            {
            // InternalAtsyRAGoal.g:4768:2: ( ruleGoalCondition )
            // InternalAtsyRAGoal.g:4769:3: ruleGoalCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraGoalAccess().getPostconditionGoalConditionParserRuleCall_1_0_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGoalCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraGoalAccess().getPostconditionGoalConditionParserRuleCall_1_0_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraGoal__PostconditionAssignment_1_0_3"


    // $ANTLR start "rule__DefaultValues__NameAssignment_1"
    // InternalAtsyRAGoal.g:4778:1: rule__DefaultValues__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__DefaultValues__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4782:1: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4783:2: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:4783:2: ( ruleEString )
            // InternalAtsyRAGoal.g:4784:3: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getNameEStringParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getNameEStringParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__NameAssignment_1"


    // $ANTLR start "rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0"
    // InternalAtsyRAGoal.g:4793:1: rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0 : ( ruleDefaultValueAssignments ) ;
    public final void rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4797:1: ( ( ruleDefaultValueAssignments ) )
            // InternalAtsyRAGoal.g:4798:2: ( ruleDefaultValueAssignments )
            {
            // InternalAtsyRAGoal.g:4798:2: ( ruleDefaultValueAssignments )
            // InternalAtsyRAGoal.g:4799:3: ruleDefaultValueAssignments
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsDefaultValueAssignmentsParserRuleCall_3_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDefaultValueAssignments();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsDefaultValueAssignmentsParserRuleCall_3_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__DefaultValueAssignmentsAssignment_3_0"


    // $ANTLR start "rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1"
    // InternalAtsyRAGoal.g:4808:1: rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1 : ( ruleDefaultValueAssignments ) ;
    public final void rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4812:1: ( ( ruleDefaultValueAssignments ) )
            // InternalAtsyRAGoal.g:4813:2: ( ruleDefaultValueAssignments )
            {
            // InternalAtsyRAGoal.g:4813:2: ( ruleDefaultValueAssignments )
            // InternalAtsyRAGoal.g:4814:3: ruleDefaultValueAssignments
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsDefaultValueAssignmentsParserRuleCall_3_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDefaultValueAssignments();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsDefaultValueAssignmentsParserRuleCall_3_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValues__DefaultValueAssignmentsAssignment_3_1_1"


    // $ANTLR start "rule__DefaultValueAssignments__TargetAssignment_1"
    // InternalAtsyRAGoal.g:4823:1: rule__DefaultValueAssignments__TargetAssignment_1 : ( ( ruleEString ) ) ;
    public final void rule__DefaultValueAssignments__TargetAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4827:1: ( ( ( ruleEString ) ) )
            // InternalAtsyRAGoal.g:4828:2: ( ( ruleEString ) )
            {
            // InternalAtsyRAGoal.g:4828:2: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4829:3: ( ruleEString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsAccess().getTargetSystemFeatureCrossReference_1_0()); 
            }
            // InternalAtsyRAGoal.g:4830:3: ( ruleEString )
            // InternalAtsyRAGoal.g:4831:4: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsAccess().getTargetSystemFeatureEStringParserRuleCall_1_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsAccess().getTargetSystemFeatureEStringParserRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsAccess().getTargetSystemFeatureCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__TargetAssignment_1"


    // $ANTLR start "rule__DefaultValueAssignments__AssignedValueAssignment_3"
    // InternalAtsyRAGoal.g:4842:1: rule__DefaultValueAssignments__AssignedValueAssignment_3 : ( ( ruleTypedElementString ) ) ;
    public final void rule__DefaultValueAssignments__AssignedValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4846:1: ( ( ( ruleTypedElementString ) ) )
            // InternalAtsyRAGoal.g:4847:2: ( ( ruleTypedElementString ) )
            {
            // InternalAtsyRAGoal.g:4847:2: ( ( ruleTypedElementString ) )
            // InternalAtsyRAGoal.g:4848:3: ( ruleTypedElementString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsAccess().getAssignedValueTypedElementCrossReference_3_0()); 
            }
            // InternalAtsyRAGoal.g:4849:3: ( ruleTypedElementString )
            // InternalAtsyRAGoal.g:4850:4: ruleTypedElementString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDefaultValueAssignmentsAccess().getAssignedValueTypedElementTypedElementStringParserRuleCall_3_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleTypedElementString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsAccess().getAssignedValueTypedElementTypedElementStringParserRuleCall_3_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDefaultValueAssignmentsAccess().getAssignedValueTypedElementCrossReference_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultValueAssignments__AssignedValueAssignment_3"


    // $ANTLR start "rule__AtsyraTreeReference__ReferencedTreeAssignment"
    // InternalAtsyRAGoal.g:4861:1: rule__AtsyraTreeReference__ReferencedTreeAssignment : ( ( ruleEString ) ) ;
    public final void rule__AtsyraTreeReference__ReferencedTreeAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4865:1: ( ( ( ruleEString ) ) )
            // InternalAtsyRAGoal.g:4866:2: ( ( ruleEString ) )
            {
            // InternalAtsyRAGoal.g:4866:2: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4867:3: ( ruleEString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeReferenceAccess().getReferencedTreeAtsyraTreeCrossReference_0()); 
            }
            // InternalAtsyRAGoal.g:4868:3: ( ruleEString )
            // InternalAtsyRAGoal.g:4869:4: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeReferenceAccess().getReferencedTreeAtsyraTreeEStringParserRuleCall_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeReferenceAccess().getReferencedTreeAtsyraTreeEStringParserRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeReferenceAccess().getReferencedTreeAtsyraTreeCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTreeReference__ReferencedTreeAssignment"


    // $ANTLR start "rule__AtsyraTree__NameAssignment_1"
    // InternalAtsyRAGoal.g:4880:1: rule__AtsyraTree__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__AtsyraTree__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4884:1: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4885:2: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:4885:2: ( ruleEString )
            // InternalAtsyRAGoal.g:4886:3: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getNameEStringParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getNameEStringParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__NameAssignment_1"


    // $ANTLR start "rule__AtsyraTree__MainGoalAssignment_3"
    // InternalAtsyRAGoal.g:4895:1: rule__AtsyraTree__MainGoalAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__AtsyraTree__MainGoalAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4899:1: ( ( ( RULE_ID ) ) )
            // InternalAtsyRAGoal.g:4900:2: ( ( RULE_ID ) )
            {
            // InternalAtsyRAGoal.g:4900:2: ( ( RULE_ID ) )
            // InternalAtsyRAGoal.g:4901:3: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getMainGoalAtsyraGoalCrossReference_3_0()); 
            }
            // InternalAtsyRAGoal.g:4902:3: ( RULE_ID )
            // InternalAtsyRAGoal.g:4903:4: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getMainGoalAtsyraGoalIDTerminalRuleCall_3_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getMainGoalAtsyraGoalIDTerminalRuleCall_3_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getMainGoalAtsyraGoalCrossReference_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__MainGoalAssignment_3"


    // $ANTLR start "rule__AtsyraTree__OperatorAssignment_5_0"
    // InternalAtsyRAGoal.g:4914:1: rule__AtsyraTree__OperatorAssignment_5_0 : ( ruleAtsyraTreeOperator ) ;
    public final void rule__AtsyraTree__OperatorAssignment_5_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4918:1: ( ( ruleAtsyraTreeOperator ) )
            // InternalAtsyRAGoal.g:4919:2: ( ruleAtsyraTreeOperator )
            {
            // InternalAtsyRAGoal.g:4919:2: ( ruleAtsyraTreeOperator )
            // InternalAtsyRAGoal.g:4920:3: ruleAtsyraTreeOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getOperatorAtsyraTreeOperatorEnumRuleCall_5_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAtsyraTreeOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getOperatorAtsyraTreeOperatorEnumRuleCall_5_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__OperatorAssignment_5_0"


    // $ANTLR start "rule__AtsyraTree__OperandsAssignment_5_2"
    // InternalAtsyRAGoal.g:4929:1: rule__AtsyraTree__OperandsAssignment_5_2 : ( ruleAbstractAtsyraTree ) ;
    public final void rule__AtsyraTree__OperandsAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4933:1: ( ( ruleAbstractAtsyraTree ) )
            // InternalAtsyRAGoal.g:4934:2: ( ruleAbstractAtsyraTree )
            {
            // InternalAtsyRAGoal.g:4934:2: ( ruleAbstractAtsyraTree )
            // InternalAtsyRAGoal.g:4935:3: ruleAbstractAtsyraTree
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getOperandsAbstractAtsyraTreeParserRuleCall_5_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAbstractAtsyraTree();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getOperandsAbstractAtsyraTreeParserRuleCall_5_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__OperandsAssignment_5_2"


    // $ANTLR start "rule__AtsyraTree__OperandsAssignment_5_3_1"
    // InternalAtsyRAGoal.g:4944:1: rule__AtsyraTree__OperandsAssignment_5_3_1 : ( ruleAbstractAtsyraTree ) ;
    public final void rule__AtsyraTree__OperandsAssignment_5_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4948:1: ( ( ruleAbstractAtsyraTree ) )
            // InternalAtsyRAGoal.g:4949:2: ( ruleAbstractAtsyraTree )
            {
            // InternalAtsyRAGoal.g:4949:2: ( ruleAbstractAtsyraTree )
            // InternalAtsyRAGoal.g:4950:3: ruleAbstractAtsyraTree
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtsyraTreeAccess().getOperandsAbstractAtsyraTreeParserRuleCall_5_3_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAbstractAtsyraTree();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtsyraTreeAccess().getOperandsAbstractAtsyraTreeParserRuleCall_5_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtsyraTree__OperandsAssignment_5_3_1"


    // $ANTLR start "rule__BooleanLiteral__NameAssignment_1"
    // InternalAtsyRAGoal.g:4959:1: rule__BooleanLiteral__NameAssignment_1 : ( ( rule__BooleanLiteral__NameAlternatives_1_0 ) ) ;
    public final void rule__BooleanLiteral__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4963:1: ( ( ( rule__BooleanLiteral__NameAlternatives_1_0 ) ) )
            // InternalAtsyRAGoal.g:4964:2: ( ( rule__BooleanLiteral__NameAlternatives_1_0 ) )
            {
            // InternalAtsyRAGoal.g:4964:2: ( ( rule__BooleanLiteral__NameAlternatives_1_0 ) )
            // InternalAtsyRAGoal.g:4965:3: ( rule__BooleanLiteral__NameAlternatives_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralAccess().getNameAlternatives_1_0()); 
            }
            // InternalAtsyRAGoal.g:4966:3: ( rule__BooleanLiteral__NameAlternatives_1_0 )
            // InternalAtsyRAGoal.g:4966:4: rule__BooleanLiteral__NameAlternatives_1_0
            {
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__NameAlternatives_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralAccess().getNameAlternatives_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__NameAssignment_1"


    // $ANTLR start "rule__BooleanLiteral__TypeAssignment_3"
    // InternalAtsyRAGoal.g:4974:1: rule__BooleanLiteral__TypeAssignment_3 : ( ( ruleEString ) ) ;
    public final void rule__BooleanLiteral__TypeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4978:1: ( ( ( ruleEString ) ) )
            // InternalAtsyRAGoal.g:4979:2: ( ( ruleEString ) )
            {
            // InternalAtsyRAGoal.g:4979:2: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4980:3: ( ruleEString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralAccess().getTypeTypeCrossReference_3_0()); 
            }
            // InternalAtsyRAGoal.g:4981:3: ( ruleEString )
            // InternalAtsyRAGoal.g:4982:4: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanLiteralAccess().getTypeTypeEStringParserRuleCall_3_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralAccess().getTypeTypeEStringParserRuleCall_3_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanLiteralAccess().getTypeTypeCrossReference_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__TypeAssignment_3"


    // $ANTLR start "rule__BuildingSystemFeature__NameAssignment_0"
    // InternalAtsyRAGoal.g:4993:1: rule__BuildingSystemFeature__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__BuildingSystemFeature__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:4997:1: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:4998:2: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:4998:2: ( ruleEString )
            // InternalAtsyRAGoal.g:4999:3: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemFeatureAccess().getNameEStringParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemFeatureAccess().getNameEStringParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__NameAssignment_0"


    // $ANTLR start "rule__BuildingSystemFeature__TypeAssignment_2"
    // InternalAtsyRAGoal.g:5008:1: rule__BuildingSystemFeature__TypeAssignment_2 : ( ( ruleEString ) ) ;
    public final void rule__BuildingSystemFeature__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:5012:1: ( ( ( ruleEString ) ) )
            // InternalAtsyRAGoal.g:5013:2: ( ( ruleEString ) )
            {
            // InternalAtsyRAGoal.g:5013:2: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:5014:3: ( ruleEString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemFeatureAccess().getTypeTypeCrossReference_2_0()); 
            }
            // InternalAtsyRAGoal.g:5015:3: ( ruleEString )
            // InternalAtsyRAGoal.g:5016:4: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemFeatureAccess().getTypeTypeEStringParserRuleCall_2_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemFeatureAccess().getTypeTypeEStringParserRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemFeatureAccess().getTypeTypeCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemFeature__TypeAssignment_2"


    // $ANTLR start "rule__BuildingSystemConstFeature__NameAssignment_0"
    // InternalAtsyRAGoal.g:5027:1: rule__BuildingSystemConstFeature__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__BuildingSystemConstFeature__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:5031:1: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:5032:2: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:5032:2: ( ruleEString )
            // InternalAtsyRAGoal.g:5033:3: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemConstFeatureAccess().getNameEStringParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemConstFeatureAccess().getNameEStringParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__NameAssignment_0"


    // $ANTLR start "rule__BuildingSystemConstFeature__TypeAssignment_2"
    // InternalAtsyRAGoal.g:5042:1: rule__BuildingSystemConstFeature__TypeAssignment_2 : ( ( ruleEString ) ) ;
    public final void rule__BuildingSystemConstFeature__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:5046:1: ( ( ( ruleEString ) ) )
            // InternalAtsyRAGoal.g:5047:2: ( ( ruleEString ) )
            {
            // InternalAtsyRAGoal.g:5047:2: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:5048:3: ( ruleEString )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemConstFeatureAccess().getTypeTypeCrossReference_2_0()); 
            }
            // InternalAtsyRAGoal.g:5049:3: ( ruleEString )
            // InternalAtsyRAGoal.g:5050:4: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBuildingSystemConstFeatureAccess().getTypeTypeEStringParserRuleCall_2_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemConstFeatureAccess().getTypeTypeEStringParserRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBuildingSystemConstFeatureAccess().getTypeTypeCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuildingSystemConstFeature__TypeAssignment_2"


    // $ANTLR start "rule__InternalType__NameAssignment_2"
    // InternalAtsyRAGoal.g:5061:1: rule__InternalType__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__InternalType__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:5065:1: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:5066:2: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:5066:2: ( ruleEString )
            // InternalAtsyRAGoal.g:5067:3: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInternalTypeAccess().getNameEStringParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInternalTypeAccess().getNameEStringParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InternalType__NameAssignment_2"


    // $ANTLR start "rule__SystemType__NameAssignment_2"
    // InternalAtsyRAGoal.g:5076:1: rule__SystemType__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__SystemType__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalAtsyRAGoal.g:5080:1: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:5081:2: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:5081:2: ( ruleEString )
            // InternalAtsyRAGoal.g:5082:3: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSystemTypeAccess().getNameEStringParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSystemTypeAccess().getNameEStringParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SystemType__NameAssignment_2"

    // $ANTLR start synpred40_InternalAtsyRAGoal
    public final void synpred40_InternalAtsyRAGoal_fragment() throws RecognitionException {   
        // InternalAtsyRAGoal.g:4351:3: ( ({...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) ) ) )
        // InternalAtsyRAGoal.g:4351:3: ({...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) ) )
        {
        // InternalAtsyRAGoal.g:4351:3: ({...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) ) )
        // InternalAtsyRAGoal.g:4352:4: {...}? => ( ( ( rule__AtsyraGoal__Group_0__0 ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred40_InternalAtsyRAGoal", "getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0)");
        }
        // InternalAtsyRAGoal.g:4352:104: ( ( ( rule__AtsyraGoal__Group_0__0 ) ) )
        // InternalAtsyRAGoal.g:4353:5: ( ( rule__AtsyraGoal__Group_0__0 ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0);
        // InternalAtsyRAGoal.g:4359:5: ( ( rule__AtsyraGoal__Group_0__0 ) )
        // InternalAtsyRAGoal.g:4360:6: ( rule__AtsyraGoal__Group_0__0 )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getAtsyraGoalAccess().getGroup_0()); 
        }
        // InternalAtsyRAGoal.g:4361:6: ( rule__AtsyraGoal__Group_0__0 )
        // InternalAtsyRAGoal.g:4361:7: rule__AtsyraGoal__Group_0__0
        {
        pushFollow(FOLLOW_2);
        rule__AtsyraGoal__Group_0__0();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred40_InternalAtsyRAGoal

    // $ANTLR start synpred41_InternalAtsyRAGoal
    public final void synpred41_InternalAtsyRAGoal_fragment() throws RecognitionException {   
        // InternalAtsyRAGoal.g:4395:2: ( rule__AtsyraGoal__UnorderedGroup__1 )
        // InternalAtsyRAGoal.g:4395:2: rule__AtsyraGoal__UnorderedGroup__1
        {
        pushFollow(FOLLOW_2);
        rule__AtsyraGoal__UnorderedGroup__1();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred41_InternalAtsyRAGoal

    // Delegated rules

    public final boolean synpred41_InternalAtsyRAGoal() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred41_InternalAtsyRAGoal_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred40_InternalAtsyRAGoal() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred40_InternalAtsyRAGoal_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000010100000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000FC00000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000400030L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000060000000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000060000400000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000060000000002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000100000030L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000100400030L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000100000032L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000002200400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000002200400002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000001802L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000020078030L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000006002L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000060030L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000001800000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000010000000020L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000085000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000004040000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000020000000000L});

}
