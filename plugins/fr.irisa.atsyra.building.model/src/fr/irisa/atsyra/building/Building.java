/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Building</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.Building#getZones <em>Zones</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.Building#getAlarms <em>Alarms</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.Building#getAccesses <em>Accesses</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.Building#getItems <em>Items</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.Building#getAttackers <em>Attackers</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getBuilding()
 * @model
 * @generated
 */
public interface Building extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Zones</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Zone}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Zones</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zones</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuilding_Zones()
	 * @model containment="true" keys="name"
	 * @generated
	 */
	EList<Zone> getZones();

	/**
	 * Returns the value of the '<em><b>Alarms</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Alarm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarms</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuilding_Alarms()
	 * @model containment="true" keys="name"
	 * @generated
	 */
	EList<Alarm> getAlarms();

	/**
	 * Returns the value of the '<em><b>Accesses</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Access}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accesses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accesses</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuilding_Accesses()
	 * @model containment="true" keys="name"
	 * @generated
	 */
	EList<Access> getAccesses();

	/**
	 * Returns the value of the '<em><b>Items</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Item}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Items</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Items</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuilding_Items()
	 * @model containment="true" keys="name"
	 * @generated
	 */
	EList<Item> getItems();

	/**
	 * Returns the value of the '<em><b>Attackers</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Attacker}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attackers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attackers</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuilding_Attackers()
	 * @model containment="true" keys="name"
	 * @generated
	 */
	EList<Attacker> getAttackers();

} // Building
