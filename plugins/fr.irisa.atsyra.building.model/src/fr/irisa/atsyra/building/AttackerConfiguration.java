/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attacker Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.AttackerConfiguration#getAttacker <em>Attacker</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.AttackerConfiguration#getLocation <em>Location</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getAttackerConfiguration()
 * @model
 * @generated
 */
public interface AttackerConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Attacker</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attacker</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attacker</em>' reference.
	 * @see #setAttacker(Attacker)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getAttackerConfiguration_Attacker()
	 * @model keys="name" required="true"
	 * @generated
	 */
	Attacker getAttacker();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.AttackerConfiguration#getAttacker <em>Attacker</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attacker</em>' reference.
	 * @see #getAttacker()
	 * @generated
	 */
	void setAttacker(Attacker value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' reference.
	 * @see #setLocation(Zone)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getAttackerConfiguration_Location()
	 * @model keys="name"
	 * @generated
	 */
	Zone getLocation();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.AttackerConfiguration#getLocation <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(Zone value);

} // AttackerConfiguration
