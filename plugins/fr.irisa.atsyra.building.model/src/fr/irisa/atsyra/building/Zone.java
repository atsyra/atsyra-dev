/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Zone</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.Zone#getAlarms <em>Alarms</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getZone()
 * @model
 * @generated
 */
public interface Zone extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Alarms</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Alarm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarms</em>' reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getZone_Alarms()
	 * @model keys="name"
	 * @generated
	 */
	EList<Alarm> getAlarms();

} // Zone
