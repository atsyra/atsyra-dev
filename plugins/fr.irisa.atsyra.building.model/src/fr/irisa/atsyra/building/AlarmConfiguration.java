/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.AlarmConfiguration#getAlarm <em>Alarm</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.AlarmConfiguration#isWasTriggered <em>Was Triggered</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.AlarmConfiguration#isIsEnabled <em>Is Enabled</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getAlarmConfiguration()
 * @model
 * @generated
 */
public interface AlarmConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Alarm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm</em>' reference.
	 * @see #setAlarm(Alarm)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getAlarmConfiguration_Alarm()
	 * @model keys="name" required="true"
	 * @generated
	 */
	Alarm getAlarm();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.AlarmConfiguration#getAlarm <em>Alarm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alarm</em>' reference.
	 * @see #getAlarm()
	 * @generated
	 */
	void setAlarm(Alarm value);

	/**
	 * Returns the value of the '<em><b>Was Triggered</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Was Triggered</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Was Triggered</em>' attribute.
	 * @see #setWasTriggered(boolean)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getAlarmConfiguration_WasTriggered()
	 * @model
	 * @generated
	 */
	boolean isWasTriggered();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.AlarmConfiguration#isWasTriggered <em>Was Triggered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Was Triggered</em>' attribute.
	 * @see #isWasTriggered()
	 * @generated
	 */
	void setWasTriggered(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Enabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Enabled</em>' attribute.
	 * @see #setIsEnabled(boolean)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getAlarmConfiguration_IsEnabled()
	 * @model
	 * @generated
	 */
	boolean isIsEnabled();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.AlarmConfiguration#isIsEnabled <em>Is Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Enabled</em>' attribute.
	 * @see #isIsEnabled()
	 * @generated
	 */
	void setIsEnabled(boolean value);

} // AlarmConfiguration
