/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.atsyra.building.BuildingFactory
 * @model kind="package"
 * @generated
 */
public interface BuildingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "building";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/atsyra/building";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "building";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BuildingPackage eINSTANCE = fr.irisa.atsyra.building.impl.BuildingPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.NamedElementImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.BuildingImpl <em>Building</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.BuildingImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getBuilding()
	 * @generated
	 */
	int BUILDING = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Zones</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING__ZONES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Alarms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING__ALARMS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING__ACCESSES = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Items</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING__ITEMS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Attackers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING__ATTACKERS = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Building</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Building</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.ZoneImpl <em>Zone</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.ZoneImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getZone()
	 * @generated
	 */
	int ZONE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Alarms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE__ALARMS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Zone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Zone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.AccessImpl <em>Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.AccessImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAccess()
	 * @generated
	 */
	int ACCESS = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.VirtualImpl <em>Virtual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.VirtualImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getVirtual()
	 * @generated
	 */
	int VIRTUAL = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL__NAME = ACCESS__NAME;

	/**
	 * The feature id for the '<em><b>Zone1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL__ZONE1 = ACCESS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Zone2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL__ZONE2 = ACCESS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Virtual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_FEATURE_COUNT = ACCESS_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Virtual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_OPERATION_COUNT = ACCESS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.DoorImpl <em>Door</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.DoorImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getDoor()
	 * @generated
	 */
	int DOOR = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR__NAME = ACCESS__NAME;

	/**
	 * The feature id for the '<em><b>Zone1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR__ZONE1 = ACCESS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Zone2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR__ZONE2 = ACCESS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Keys</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR__KEYS = ACCESS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR__LEVEL = ACCESS_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Alarms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR__ALARMS = ACCESS_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Door</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR_FEATURE_COUNT = ACCESS_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Door</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR_OPERATION_COUNT = ACCESS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.WindowImpl <em>Window</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.WindowImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getWindow()
	 * @generated
	 */
	int WINDOW = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__NAME = ACCESS__NAME;

	/**
	 * The feature id for the '<em><b>Inside</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__INSIDE = ACCESS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outside</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__OUTSIDE = ACCESS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__LEVEL = ACCESS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Alarms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__ALARMS = ACCESS_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW_FEATURE_COUNT = ACCESS_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW_OPERATION_COUNT = ACCESS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.BadgedDoorImpl <em>Badged Door</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.BadgedDoorImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getBadgedDoor()
	 * @generated
	 */
	int BADGED_DOOR = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGED_DOOR__NAME = ACCESS__NAME;

	/**
	 * The feature id for the '<em><b>Inside</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGED_DOOR__INSIDE = ACCESS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outside</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGED_DOOR__OUTSIDE = ACCESS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Badges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGED_DOOR__BADGES = ACCESS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGED_DOOR__LEVEL = ACCESS_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Alarms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGED_DOOR__ALARMS = ACCESS_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Badged Door</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGED_DOOR_FEATURE_COUNT = ACCESS_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Badged Door</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGED_DOOR_OPERATION_COUNT = ACCESS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.AlarmImpl <em>Alarm</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.AlarmImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAlarm()
	 * @generated
	 */
	int ALARM = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM__LOCATION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Alarm</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Alarm</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.ItemImpl <em>Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.ItemImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getItem()
	 * @generated
	 */
	int ITEM = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.AttackerImpl <em>Attacker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.AttackerImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAttacker()
	 * @generated
	 */
	int ATTACKER = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER__LEVEL = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attacker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Attacker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.BuildingConfigurationImpl <em>Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.BuildingConfigurationImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getBuildingConfiguration()
	 * @generated
	 */
	int BUILDING_CONFIGURATION = 11;

	/**
	 * The feature id for the '<em><b>Attacker Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_CONFIGURATION__ATTACKER_CONFIGURATIONS = 0;

	/**
	 * The feature id for the '<em><b>Alarm Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_CONFIGURATION__ALARM_CONFIGURATIONS = 1;

	/**
	 * The feature id for the '<em><b>Access Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_CONFIGURATION__ACCESS_CONFIGURATIONS = 2;

	/**
	 * The feature id for the '<em><b>Item Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_CONFIGURATION__ITEM_CONFIGURATIONS = 3;

	/**
	 * The number of structural features of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_CONFIGURATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_CONFIGURATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.BuildingModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.BuildingModelImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getBuildingModel()
	 * @generated
	 */
	int BUILDING_MODEL = 12;

	/**
	 * The feature id for the '<em><b>Buildings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_MODEL__BUILDINGS = 0;

	/**
	 * The feature id for the '<em><b>Building Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_MODEL__BUILDING_CONFIGURATIONS = 1;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_MODEL__IMPORTS = 2;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_MODEL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.AttackerConfigurationImpl <em>Attacker Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.AttackerConfigurationImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAttackerConfiguration()
	 * @generated
	 */
	int ATTACKER_CONFIGURATION = 13;

	/**
	 * The feature id for the '<em><b>Attacker</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER_CONFIGURATION__ATTACKER = 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER_CONFIGURATION__LOCATION = 1;

	/**
	 * The number of structural features of the '<em>Attacker Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER_CONFIGURATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Attacker Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER_CONFIGURATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.ItemConfigurationImpl <em>Item Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.ItemConfigurationImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getItemConfiguration()
	 * @generated
	 */
	int ITEM_CONFIGURATION = 14;

	/**
	 * The feature id for the '<em><b>Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_CONFIGURATION__ITEM = 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_CONFIGURATION__LOCATION = 1;

	/**
	 * The feature id for the '<em><b>Owned By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_CONFIGURATION__OWNED_BY = 2;

	/**
	 * The number of structural features of the '<em>Item Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_CONFIGURATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Item Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_CONFIGURATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.AccessConfigurationImpl <em>Access Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.AccessConfigurationImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAccessConfiguration()
	 * @generated
	 */
	int ACCESS_CONFIGURATION = 15;

	/**
	 * The feature id for the '<em><b>Access</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONFIGURATION__ACCESS = 0;

	/**
	 * The feature id for the '<em><b>Is Open</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONFIGURATION__IS_OPEN = 1;

	/**
	 * The feature id for the '<em><b>Is Locked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONFIGURATION__IS_LOCKED = 2;

	/**
	 * The number of structural features of the '<em>Access Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONFIGURATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Access Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONFIGURATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.AlarmConfigurationImpl <em>Alarm Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.AlarmConfigurationImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAlarmConfiguration()
	 * @generated
	 */
	int ALARM_CONFIGURATION = 16;

	/**
	 * The feature id for the '<em><b>Alarm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CONFIGURATION__ALARM = 0;

	/**
	 * The feature id for the '<em><b>Was Triggered</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CONFIGURATION__WAS_TRIGGERED = 1;

	/**
	 * The feature id for the '<em><b>Is Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CONFIGURATION__IS_ENABLED = 2;

	/**
	 * The number of structural features of the '<em>Alarm Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CONFIGURATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Alarm Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CONFIGURATION_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.building.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.building.impl.ImportImpl
	 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 17;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__IMPORT_URI = 0;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Building <em>Building</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Building</em>'.
	 * @see fr.irisa.atsyra.building.Building
	 * @generated
	 */
	EClass getBuilding();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.Building#getZones <em>Zones</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Zones</em>'.
	 * @see fr.irisa.atsyra.building.Building#getZones()
	 * @see #getBuilding()
	 * @generated
	 */
	EReference getBuilding_Zones();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.Building#getAlarms <em>Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alarms</em>'.
	 * @see fr.irisa.atsyra.building.Building#getAlarms()
	 * @see #getBuilding()
	 * @generated
	 */
	EReference getBuilding_Alarms();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.Building#getAccesses <em>Accesses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Accesses</em>'.
	 * @see fr.irisa.atsyra.building.Building#getAccesses()
	 * @see #getBuilding()
	 * @generated
	 */
	EReference getBuilding_Accesses();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.Building#getItems <em>Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Items</em>'.
	 * @see fr.irisa.atsyra.building.Building#getItems()
	 * @see #getBuilding()
	 * @generated
	 */
	EReference getBuilding_Items();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.Building#getAttackers <em>Attackers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attackers</em>'.
	 * @see fr.irisa.atsyra.building.Building#getAttackers()
	 * @see #getBuilding()
	 * @generated
	 */
	EReference getBuilding_Attackers();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Zone <em>Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Zone</em>'.
	 * @see fr.irisa.atsyra.building.Zone
	 * @generated
	 */
	EClass getZone();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.building.Zone#getAlarms <em>Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Alarms</em>'.
	 * @see fr.irisa.atsyra.building.Zone#getAlarms()
	 * @see #getZone()
	 * @generated
	 */
	EReference getZone_Alarms();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Access <em>Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access</em>'.
	 * @see fr.irisa.atsyra.building.Access
	 * @generated
	 */
	EClass getAccess();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Virtual <em>Virtual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Virtual</em>'.
	 * @see fr.irisa.atsyra.building.Virtual
	 * @generated
	 */
	EClass getVirtual();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.Virtual#getZone1 <em>Zone1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Zone1</em>'.
	 * @see fr.irisa.atsyra.building.Virtual#getZone1()
	 * @see #getVirtual()
	 * @generated
	 */
	EReference getVirtual_Zone1();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.Virtual#getZone2 <em>Zone2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Zone2</em>'.
	 * @see fr.irisa.atsyra.building.Virtual#getZone2()
	 * @see #getVirtual()
	 * @generated
	 */
	EReference getVirtual_Zone2();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Door <em>Door</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Door</em>'.
	 * @see fr.irisa.atsyra.building.Door
	 * @generated
	 */
	EClass getDoor();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.Door#getZone1 <em>Zone1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Zone1</em>'.
	 * @see fr.irisa.atsyra.building.Door#getZone1()
	 * @see #getDoor()
	 * @generated
	 */
	EReference getDoor_Zone1();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.Door#getZone2 <em>Zone2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Zone2</em>'.
	 * @see fr.irisa.atsyra.building.Door#getZone2()
	 * @see #getDoor()
	 * @generated
	 */
	EReference getDoor_Zone2();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.building.Door#getKeys <em>Keys</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Keys</em>'.
	 * @see fr.irisa.atsyra.building.Door#getKeys()
	 * @see #getDoor()
	 * @generated
	 */
	EReference getDoor_Keys();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.Door#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see fr.irisa.atsyra.building.Door#getLevel()
	 * @see #getDoor()
	 * @generated
	 */
	EAttribute getDoor_Level();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.building.Door#getAlarms <em>Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Alarms</em>'.
	 * @see fr.irisa.atsyra.building.Door#getAlarms()
	 * @see #getDoor()
	 * @generated
	 */
	EReference getDoor_Alarms();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Window <em>Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Window</em>'.
	 * @see fr.irisa.atsyra.building.Window
	 * @generated
	 */
	EClass getWindow();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.Window#getInside <em>Inside</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Inside</em>'.
	 * @see fr.irisa.atsyra.building.Window#getInside()
	 * @see #getWindow()
	 * @generated
	 */
	EReference getWindow_Inside();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.Window#getOutside <em>Outside</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Outside</em>'.
	 * @see fr.irisa.atsyra.building.Window#getOutside()
	 * @see #getWindow()
	 * @generated
	 */
	EReference getWindow_Outside();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.Window#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see fr.irisa.atsyra.building.Window#getLevel()
	 * @see #getWindow()
	 * @generated
	 */
	EAttribute getWindow_Level();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.building.Window#getAlarms <em>Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Alarms</em>'.
	 * @see fr.irisa.atsyra.building.Window#getAlarms()
	 * @see #getWindow()
	 * @generated
	 */
	EReference getWindow_Alarms();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.BadgedDoor <em>Badged Door</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Badged Door</em>'.
	 * @see fr.irisa.atsyra.building.BadgedDoor
	 * @generated
	 */
	EClass getBadgedDoor();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.BadgedDoor#getInside <em>Inside</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Inside</em>'.
	 * @see fr.irisa.atsyra.building.BadgedDoor#getInside()
	 * @see #getBadgedDoor()
	 * @generated
	 */
	EReference getBadgedDoor_Inside();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.BadgedDoor#getOutside <em>Outside</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Outside</em>'.
	 * @see fr.irisa.atsyra.building.BadgedDoor#getOutside()
	 * @see #getBadgedDoor()
	 * @generated
	 */
	EReference getBadgedDoor_Outside();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.building.BadgedDoor#getBadges <em>Badges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Badges</em>'.
	 * @see fr.irisa.atsyra.building.BadgedDoor#getBadges()
	 * @see #getBadgedDoor()
	 * @generated
	 */
	EReference getBadgedDoor_Badges();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.BadgedDoor#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see fr.irisa.atsyra.building.BadgedDoor#getLevel()
	 * @see #getBadgedDoor()
	 * @generated
	 */
	EAttribute getBadgedDoor_Level();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.building.BadgedDoor#getAlarms <em>Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Alarms</em>'.
	 * @see fr.irisa.atsyra.building.BadgedDoor#getAlarms()
	 * @see #getBadgedDoor()
	 * @generated
	 */
	EReference getBadgedDoor_Alarms();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Alarm <em>Alarm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm</em>'.
	 * @see fr.irisa.atsyra.building.Alarm
	 * @generated
	 */
	EClass getAlarm();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.Alarm#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see fr.irisa.atsyra.building.Alarm#getLocation()
	 * @see #getAlarm()
	 * @generated
	 */
	EReference getAlarm_Location();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Item <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Item</em>'.
	 * @see fr.irisa.atsyra.building.Item
	 * @generated
	 */
	EClass getItem();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.irisa.atsyra.building.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.building.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Attacker <em>Attacker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attacker</em>'.
	 * @see fr.irisa.atsyra.building.Attacker
	 * @generated
	 */
	EClass getAttacker();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.Attacker#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see fr.irisa.atsyra.building.Attacker#getLevel()
	 * @see #getAttacker()
	 * @generated
	 */
	EAttribute getAttacker_Level();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.BuildingConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration</em>'.
	 * @see fr.irisa.atsyra.building.BuildingConfiguration
	 * @generated
	 */
	EClass getBuildingConfiguration();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.BuildingConfiguration#getAttackerConfigurations <em>Attacker Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attacker Configurations</em>'.
	 * @see fr.irisa.atsyra.building.BuildingConfiguration#getAttackerConfigurations()
	 * @see #getBuildingConfiguration()
	 * @generated
	 */
	EReference getBuildingConfiguration_AttackerConfigurations();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.BuildingConfiguration#getAlarmConfigurations <em>Alarm Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alarm Configurations</em>'.
	 * @see fr.irisa.atsyra.building.BuildingConfiguration#getAlarmConfigurations()
	 * @see #getBuildingConfiguration()
	 * @generated
	 */
	EReference getBuildingConfiguration_AlarmConfigurations();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.BuildingConfiguration#getAccessConfigurations <em>Access Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Access Configurations</em>'.
	 * @see fr.irisa.atsyra.building.BuildingConfiguration#getAccessConfigurations()
	 * @see #getBuildingConfiguration()
	 * @generated
	 */
	EReference getBuildingConfiguration_AccessConfigurations();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.BuildingConfiguration#getItemConfigurations <em>Item Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Item Configurations</em>'.
	 * @see fr.irisa.atsyra.building.BuildingConfiguration#getItemConfigurations()
	 * @see #getBuildingConfiguration()
	 * @generated
	 */
	EReference getBuildingConfiguration_ItemConfigurations();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.BuildingModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see fr.irisa.atsyra.building.BuildingModel
	 * @generated
	 */
	EClass getBuildingModel();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.BuildingModel#getBuildings <em>Buildings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Buildings</em>'.
	 * @see fr.irisa.atsyra.building.BuildingModel#getBuildings()
	 * @see #getBuildingModel()
	 * @generated
	 */
	EReference getBuildingModel_Buildings();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.BuildingModel#getBuildingConfigurations <em>Building Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Building Configurations</em>'.
	 * @see fr.irisa.atsyra.building.BuildingModel#getBuildingConfigurations()
	 * @see #getBuildingModel()
	 * @generated
	 */
	EReference getBuildingModel_BuildingConfigurations();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.building.BuildingModel#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see fr.irisa.atsyra.building.BuildingModel#getImports()
	 * @see #getBuildingModel()
	 * @generated
	 */
	EReference getBuildingModel_Imports();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.AttackerConfiguration <em>Attacker Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attacker Configuration</em>'.
	 * @see fr.irisa.atsyra.building.AttackerConfiguration
	 * @generated
	 */
	EClass getAttackerConfiguration();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.AttackerConfiguration#getAttacker <em>Attacker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attacker</em>'.
	 * @see fr.irisa.atsyra.building.AttackerConfiguration#getAttacker()
	 * @see #getAttackerConfiguration()
	 * @generated
	 */
	EReference getAttackerConfiguration_Attacker();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.AttackerConfiguration#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see fr.irisa.atsyra.building.AttackerConfiguration#getLocation()
	 * @see #getAttackerConfiguration()
	 * @generated
	 */
	EReference getAttackerConfiguration_Location();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.ItemConfiguration <em>Item Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Item Configuration</em>'.
	 * @see fr.irisa.atsyra.building.ItemConfiguration
	 * @generated
	 */
	EClass getItemConfiguration();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.ItemConfiguration#getItem <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Item</em>'.
	 * @see fr.irisa.atsyra.building.ItemConfiguration#getItem()
	 * @see #getItemConfiguration()
	 * @generated
	 */
	EReference getItemConfiguration_Item();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.ItemConfiguration#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see fr.irisa.atsyra.building.ItemConfiguration#getLocation()
	 * @see #getItemConfiguration()
	 * @generated
	 */
	EReference getItemConfiguration_Location();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.ItemConfiguration#getOwnedBy <em>Owned By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Owned By</em>'.
	 * @see fr.irisa.atsyra.building.ItemConfiguration#getOwnedBy()
	 * @see #getItemConfiguration()
	 * @generated
	 */
	EReference getItemConfiguration_OwnedBy();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.AccessConfiguration <em>Access Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Configuration</em>'.
	 * @see fr.irisa.atsyra.building.AccessConfiguration
	 * @generated
	 */
	EClass getAccessConfiguration();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.AccessConfiguration#getAccess <em>Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Access</em>'.
	 * @see fr.irisa.atsyra.building.AccessConfiguration#getAccess()
	 * @see #getAccessConfiguration()
	 * @generated
	 */
	EReference getAccessConfiguration_Access();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.AccessConfiguration#isIsOpen <em>Is Open</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Open</em>'.
	 * @see fr.irisa.atsyra.building.AccessConfiguration#isIsOpen()
	 * @see #getAccessConfiguration()
	 * @generated
	 */
	EAttribute getAccessConfiguration_IsOpen();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.AccessConfiguration#isIsLocked <em>Is Locked</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Locked</em>'.
	 * @see fr.irisa.atsyra.building.AccessConfiguration#isIsLocked()
	 * @see #getAccessConfiguration()
	 * @generated
	 */
	EAttribute getAccessConfiguration_IsLocked();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.AlarmConfiguration <em>Alarm Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Configuration</em>'.
	 * @see fr.irisa.atsyra.building.AlarmConfiguration
	 * @generated
	 */
	EClass getAlarmConfiguration();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.building.AlarmConfiguration#getAlarm <em>Alarm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Alarm</em>'.
	 * @see fr.irisa.atsyra.building.AlarmConfiguration#getAlarm()
	 * @see #getAlarmConfiguration()
	 * @generated
	 */
	EReference getAlarmConfiguration_Alarm();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.AlarmConfiguration#isWasTriggered <em>Was Triggered</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Was Triggered</em>'.
	 * @see fr.irisa.atsyra.building.AlarmConfiguration#isWasTriggered()
	 * @see #getAlarmConfiguration()
	 * @generated
	 */
	EAttribute getAlarmConfiguration_WasTriggered();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.AlarmConfiguration#isIsEnabled <em>Is Enabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Enabled</em>'.
	 * @see fr.irisa.atsyra.building.AlarmConfiguration#isIsEnabled()
	 * @see #getAlarmConfiguration()
	 * @generated
	 */
	EAttribute getAlarmConfiguration_IsEnabled();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.building.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see fr.irisa.atsyra.building.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.building.Import#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see fr.irisa.atsyra.building.Import#getImportURI()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_ImportURI();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BuildingFactory getBuildingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.BuildingImpl <em>Building</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.BuildingImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getBuilding()
		 * @generated
		 */
		EClass BUILDING = eINSTANCE.getBuilding();

		/**
		 * The meta object literal for the '<em><b>Zones</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING__ZONES = eINSTANCE.getBuilding_Zones();

		/**
		 * The meta object literal for the '<em><b>Alarms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING__ALARMS = eINSTANCE.getBuilding_Alarms();

		/**
		 * The meta object literal for the '<em><b>Accesses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING__ACCESSES = eINSTANCE.getBuilding_Accesses();

		/**
		 * The meta object literal for the '<em><b>Items</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING__ITEMS = eINSTANCE.getBuilding_Items();

		/**
		 * The meta object literal for the '<em><b>Attackers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING__ATTACKERS = eINSTANCE.getBuilding_Attackers();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.ZoneImpl <em>Zone</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.ZoneImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getZone()
		 * @generated
		 */
		EClass ZONE = eINSTANCE.getZone();

		/**
		 * The meta object literal for the '<em><b>Alarms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ZONE__ALARMS = eINSTANCE.getZone_Alarms();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.AccessImpl <em>Access</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.AccessImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAccess()
		 * @generated
		 */
		EClass ACCESS = eINSTANCE.getAccess();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.VirtualImpl <em>Virtual</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.VirtualImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getVirtual()
		 * @generated
		 */
		EClass VIRTUAL = eINSTANCE.getVirtual();

		/**
		 * The meta object literal for the '<em><b>Zone1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIRTUAL__ZONE1 = eINSTANCE.getVirtual_Zone1();

		/**
		 * The meta object literal for the '<em><b>Zone2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIRTUAL__ZONE2 = eINSTANCE.getVirtual_Zone2();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.DoorImpl <em>Door</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.DoorImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getDoor()
		 * @generated
		 */
		EClass DOOR = eINSTANCE.getDoor();

		/**
		 * The meta object literal for the '<em><b>Zone1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOOR__ZONE1 = eINSTANCE.getDoor_Zone1();

		/**
		 * The meta object literal for the '<em><b>Zone2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOOR__ZONE2 = eINSTANCE.getDoor_Zone2();

		/**
		 * The meta object literal for the '<em><b>Keys</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOOR__KEYS = eINSTANCE.getDoor_Keys();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOOR__LEVEL = eINSTANCE.getDoor_Level();

		/**
		 * The meta object literal for the '<em><b>Alarms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOOR__ALARMS = eINSTANCE.getDoor_Alarms();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.WindowImpl <em>Window</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.WindowImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getWindow()
		 * @generated
		 */
		EClass WINDOW = eINSTANCE.getWindow();

		/**
		 * The meta object literal for the '<em><b>Inside</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WINDOW__INSIDE = eINSTANCE.getWindow_Inside();

		/**
		 * The meta object literal for the '<em><b>Outside</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WINDOW__OUTSIDE = eINSTANCE.getWindow_Outside();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WINDOW__LEVEL = eINSTANCE.getWindow_Level();

		/**
		 * The meta object literal for the '<em><b>Alarms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WINDOW__ALARMS = eINSTANCE.getWindow_Alarms();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.BadgedDoorImpl <em>Badged Door</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.BadgedDoorImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getBadgedDoor()
		 * @generated
		 */
		EClass BADGED_DOOR = eINSTANCE.getBadgedDoor();

		/**
		 * The meta object literal for the '<em><b>Inside</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BADGED_DOOR__INSIDE = eINSTANCE.getBadgedDoor_Inside();

		/**
		 * The meta object literal for the '<em><b>Outside</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BADGED_DOOR__OUTSIDE = eINSTANCE.getBadgedDoor_Outside();

		/**
		 * The meta object literal for the '<em><b>Badges</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BADGED_DOOR__BADGES = eINSTANCE.getBadgedDoor_Badges();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BADGED_DOOR__LEVEL = eINSTANCE.getBadgedDoor_Level();

		/**
		 * The meta object literal for the '<em><b>Alarms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BADGED_DOOR__ALARMS = eINSTANCE.getBadgedDoor_Alarms();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.AlarmImpl <em>Alarm</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.AlarmImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAlarm()
		 * @generated
		 */
		EClass ALARM = eINSTANCE.getAlarm();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM__LOCATION = eINSTANCE.getAlarm_Location();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.ItemImpl <em>Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.ItemImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getItem()
		 * @generated
		 */
		EClass ITEM = eINSTANCE.getItem();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.NamedElementImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.AttackerImpl <em>Attacker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.AttackerImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAttacker()
		 * @generated
		 */
		EClass ATTACKER = eINSTANCE.getAttacker();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTACKER__LEVEL = eINSTANCE.getAttacker_Level();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.BuildingConfigurationImpl <em>Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.BuildingConfigurationImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getBuildingConfiguration()
		 * @generated
		 */
		EClass BUILDING_CONFIGURATION = eINSTANCE.getBuildingConfiguration();

		/**
		 * The meta object literal for the '<em><b>Attacker Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING_CONFIGURATION__ATTACKER_CONFIGURATIONS = eINSTANCE.getBuildingConfiguration_AttackerConfigurations();

		/**
		 * The meta object literal for the '<em><b>Alarm Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING_CONFIGURATION__ALARM_CONFIGURATIONS = eINSTANCE.getBuildingConfiguration_AlarmConfigurations();

		/**
		 * The meta object literal for the '<em><b>Access Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING_CONFIGURATION__ACCESS_CONFIGURATIONS = eINSTANCE.getBuildingConfiguration_AccessConfigurations();

		/**
		 * The meta object literal for the '<em><b>Item Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING_CONFIGURATION__ITEM_CONFIGURATIONS = eINSTANCE.getBuildingConfiguration_ItemConfigurations();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.BuildingModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.BuildingModelImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getBuildingModel()
		 * @generated
		 */
		EClass BUILDING_MODEL = eINSTANCE.getBuildingModel();

		/**
		 * The meta object literal for the '<em><b>Buildings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING_MODEL__BUILDINGS = eINSTANCE.getBuildingModel_Buildings();

		/**
		 * The meta object literal for the '<em><b>Building Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING_MODEL__BUILDING_CONFIGURATIONS = eINSTANCE.getBuildingModel_BuildingConfigurations();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDING_MODEL__IMPORTS = eINSTANCE.getBuildingModel_Imports();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.AttackerConfigurationImpl <em>Attacker Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.AttackerConfigurationImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAttackerConfiguration()
		 * @generated
		 */
		EClass ATTACKER_CONFIGURATION = eINSTANCE.getAttackerConfiguration();

		/**
		 * The meta object literal for the '<em><b>Attacker</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACKER_CONFIGURATION__ATTACKER = eINSTANCE.getAttackerConfiguration_Attacker();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACKER_CONFIGURATION__LOCATION = eINSTANCE.getAttackerConfiguration_Location();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.ItemConfigurationImpl <em>Item Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.ItemConfigurationImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getItemConfiguration()
		 * @generated
		 */
		EClass ITEM_CONFIGURATION = eINSTANCE.getItemConfiguration();

		/**
		 * The meta object literal for the '<em><b>Item</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEM_CONFIGURATION__ITEM = eINSTANCE.getItemConfiguration_Item();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEM_CONFIGURATION__LOCATION = eINSTANCE.getItemConfiguration_Location();

		/**
		 * The meta object literal for the '<em><b>Owned By</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEM_CONFIGURATION__OWNED_BY = eINSTANCE.getItemConfiguration_OwnedBy();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.AccessConfigurationImpl <em>Access Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.AccessConfigurationImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAccessConfiguration()
		 * @generated
		 */
		EClass ACCESS_CONFIGURATION = eINSTANCE.getAccessConfiguration();

		/**
		 * The meta object literal for the '<em><b>Access</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONFIGURATION__ACCESS = eINSTANCE.getAccessConfiguration_Access();

		/**
		 * The meta object literal for the '<em><b>Is Open</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESS_CONFIGURATION__IS_OPEN = eINSTANCE.getAccessConfiguration_IsOpen();

		/**
		 * The meta object literal for the '<em><b>Is Locked</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESS_CONFIGURATION__IS_LOCKED = eINSTANCE.getAccessConfiguration_IsLocked();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.AlarmConfigurationImpl <em>Alarm Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.AlarmConfigurationImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getAlarmConfiguration()
		 * @generated
		 */
		EClass ALARM_CONFIGURATION = eINSTANCE.getAlarmConfiguration();

		/**
		 * The meta object literal for the '<em><b>Alarm</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_CONFIGURATION__ALARM = eINSTANCE.getAlarmConfiguration_Alarm();

		/**
		 * The meta object literal for the '<em><b>Was Triggered</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALARM_CONFIGURATION__WAS_TRIGGERED = eINSTANCE.getAlarmConfiguration_WasTriggered();

		/**
		 * The meta object literal for the '<em><b>Is Enabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALARM_CONFIGURATION__IS_ENABLED = eINSTANCE.getAlarmConfiguration_IsEnabled();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.building.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.building.impl.ImportImpl
		 * @see fr.irisa.atsyra.building.impl.BuildingPackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__IMPORT_URI = eINSTANCE.getImport_ImportURI();

	}

} //BuildingPackage
