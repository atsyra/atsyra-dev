/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Badged Door</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.BadgedDoor#getInside <em>Inside</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.BadgedDoor#getOutside <em>Outside</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.BadgedDoor#getBadges <em>Badges</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.BadgedDoor#getLevel <em>Level</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.BadgedDoor#getAlarms <em>Alarms</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getBadgedDoor()
 * @model
 * @generated
 */
public interface BadgedDoor extends Access {
	/**
	 * Returns the value of the '<em><b>Inside</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inside</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inside</em>' reference.
	 * @see #setInside(Zone)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBadgedDoor_Inside()
	 * @model keys="name"
	 * @generated
	 */
	Zone getInside();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.BadgedDoor#getInside <em>Inside</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inside</em>' reference.
	 * @see #getInside()
	 * @generated
	 */
	void setInside(Zone value);

	/**
	 * Returns the value of the '<em><b>Outside</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outside</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outside</em>' reference.
	 * @see #setOutside(Zone)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBadgedDoor_Outside()
	 * @model keys="name"
	 * @generated
	 */
	Zone getOutside();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.BadgedDoor#getOutside <em>Outside</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outside</em>' reference.
	 * @see #getOutside()
	 * @generated
	 */
	void setOutside(Zone value);

	/**
	 * Returns the value of the '<em><b>Badges</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Item}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Badges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Badges</em>' reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBadgedDoor_Badges()
	 * @model keys="name"
	 * @generated
	 */
	EList<Item> getBadges();

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see #setLevel(int)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBadgedDoor_Level()
	 * @model
	 * @generated
	 */
	int getLevel();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.BadgedDoor#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(int value);

	/**
	 * Returns the value of the '<em><b>Alarms</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Alarm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarms</em>' reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBadgedDoor_Alarms()
	 * @model keys="name"
	 * @generated
	 */
	EList<Alarm> getAlarms();

} // BadgedDoor
