/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.BuildingModel#getBuildings <em>Buildings</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.BuildingModel#getBuildingConfigurations <em>Building Configurations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.BuildingModel#getImports <em>Imports</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getBuildingModel()
 * @model
 * @generated
 */
public interface BuildingModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Buildings</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Building}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Buildings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Buildings</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuildingModel_Buildings()
	 * @model containment="true" keys="name"
	 * @generated
	 */
	EList<Building> getBuildings();

	/**
	 * Returns the value of the '<em><b>Building Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.BuildingConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Building Configurations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Building Configurations</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuildingModel_BuildingConfigurations()
	 * @model containment="true"
	 * @generated
	 */
	EList<BuildingConfiguration> getBuildingConfigurations();

	/**
	 * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuildingModel_Imports()
	 * @model containment="true" keys="importURI"
	 * @generated
	 */
	EList<Import> getImports();

} // BuildingModel
