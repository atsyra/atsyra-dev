/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Item Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.ItemConfiguration#getItem <em>Item</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.ItemConfiguration#getLocation <em>Location</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.ItemConfiguration#getOwnedBy <em>Owned By</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getItemConfiguration()
 * @model
 * @generated
 */
public interface ItemConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item</em>' reference.
	 * @see #setItem(Item)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getItemConfiguration_Item()
	 * @model keys="name" required="true"
	 * @generated
	 */
	Item getItem();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.ItemConfiguration#getItem <em>Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Item</em>' reference.
	 * @see #getItem()
	 * @generated
	 */
	void setItem(Item value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' reference.
	 * @see #setLocation(Zone)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getItemConfiguration_Location()
	 * @model keys="name"
	 * @generated
	 */
	Zone getLocation();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.ItemConfiguration#getLocation <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(Zone value);

	/**
	 * Returns the value of the '<em><b>Owned By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned By</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned By</em>' reference.
	 * @see #setOwnedBy(Attacker)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getItemConfiguration_OwnedBy()
	 * @model keys="name"
	 * @generated
	 */
	Attacker getOwnedBy();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.ItemConfiguration#getOwnedBy <em>Owned By</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owned By</em>' reference.
	 * @see #getOwnedBy()
	 * @generated
	 */
	void setOwnedBy(Attacker value);

} // ItemConfiguration
