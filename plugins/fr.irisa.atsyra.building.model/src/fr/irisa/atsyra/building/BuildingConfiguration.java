/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.BuildingConfiguration#getAttackerConfigurations <em>Attacker Configurations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.BuildingConfiguration#getAlarmConfigurations <em>Alarm Configurations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.BuildingConfiguration#getAccessConfigurations <em>Access Configurations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.BuildingConfiguration#getItemConfigurations <em>Item Configurations</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getBuildingConfiguration()
 * @model
 * @generated
 */
public interface BuildingConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Attacker Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.AttackerConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attacker Configurations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attacker Configurations</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuildingConfiguration_AttackerConfigurations()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttackerConfiguration> getAttackerConfigurations();

	/**
	 * Returns the value of the '<em><b>Alarm Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.AlarmConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Configurations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Configurations</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuildingConfiguration_AlarmConfigurations()
	 * @model containment="true"
	 * @generated
	 */
	EList<AlarmConfiguration> getAlarmConfigurations();

	/**
	 * Returns the value of the '<em><b>Access Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.AccessConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Configurations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Configurations</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuildingConfiguration_AccessConfigurations()
	 * @model containment="true"
	 * @generated
	 */
	EList<AccessConfiguration> getAccessConfigurations();

	/**
	 * Returns the value of the '<em><b>Item Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.ItemConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item Configurations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item Configurations</em>' containment reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getBuildingConfiguration_ItemConfigurations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ItemConfiguration> getItemConfigurations();

} // BuildingConfiguration
