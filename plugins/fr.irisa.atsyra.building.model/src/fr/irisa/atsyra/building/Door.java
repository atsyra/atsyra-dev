/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Door</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.Door#getZone1 <em>Zone1</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.Door#getZone2 <em>Zone2</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.Door#getKeys <em>Keys</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.Door#getLevel <em>Level</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.Door#getAlarms <em>Alarms</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getDoor()
 * @model
 * @generated
 */
public interface Door extends Access {
	/**
	 * Returns the value of the '<em><b>Zone1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Zone1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zone1</em>' reference.
	 * @see #setZone1(Zone)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getDoor_Zone1()
	 * @model keys="name"
	 * @generated
	 */
	Zone getZone1();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.Door#getZone1 <em>Zone1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Zone1</em>' reference.
	 * @see #getZone1()
	 * @generated
	 */
	void setZone1(Zone value);

	/**
	 * Returns the value of the '<em><b>Zone2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Zone2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zone2</em>' reference.
	 * @see #setZone2(Zone)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getDoor_Zone2()
	 * @model keys="name"
	 * @generated
	 */
	Zone getZone2();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.Door#getZone2 <em>Zone2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Zone2</em>' reference.
	 * @see #getZone2()
	 * @generated
	 */
	void setZone2(Zone value);

	/**
	 * Returns the value of the '<em><b>Keys</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Item}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Keys</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keys</em>' reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getDoor_Keys()
	 * @model
	 * @generated
	 */
	EList<Item> getKeys();

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see #setLevel(int)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getDoor_Level()
	 * @model
	 * @generated
	 */
	int getLevel();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.Door#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(int value);

	/**
	 * Returns the value of the '<em><b>Alarms</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.building.Alarm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarms</em>' reference list.
	 * @see fr.irisa.atsyra.building.BuildingPackage#getDoor_Alarms()
	 * @model keys="name"
	 * @generated
	 */
	EList<Alarm> getAlarms();

} // Door
