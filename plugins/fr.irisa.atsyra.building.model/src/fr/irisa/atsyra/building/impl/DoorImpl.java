/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.Alarm;
import fr.irisa.atsyra.building.BuildingPackage;
import fr.irisa.atsyra.building.Door;
import fr.irisa.atsyra.building.Item;
import fr.irisa.atsyra.building.Zone;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Door</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.impl.DoorImpl#getZone1 <em>Zone1</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.DoorImpl#getZone2 <em>Zone2</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.DoorImpl#getKeys <em>Keys</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.DoorImpl#getLevel <em>Level</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.DoorImpl#getAlarms <em>Alarms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DoorImpl extends AccessImpl implements Door {
	/**
	 * The cached value of the '{@link #getZone1() <em>Zone1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZone1()
	 * @generated
	 * @ordered
	 */
	protected Zone zone1;

	/**
	 * The cached value of the '{@link #getZone2() <em>Zone2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZone2()
	 * @generated
	 * @ordered
	 */
	protected Zone zone2;

	/**
	 * The cached value of the '{@link #getKeys() <em>Keys</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeys()
	 * @generated
	 * @ordered
	 */
	protected EList<Item> keys;

	/**
	 * The default value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected static final int LEVEL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected int level = LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAlarms() <em>Alarms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarms()
	 * @generated
	 * @ordered
	 */
	protected EList<Alarm> alarms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DoorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingPackage.Literals.DOOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone getZone1() {
		if (zone1 != null && zone1.eIsProxy()) {
			InternalEObject oldZone1 = (InternalEObject)zone1;
			zone1 = (Zone)eResolveProxy(oldZone1);
			if (zone1 != oldZone1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.DOOR__ZONE1, oldZone1, zone1));
			}
		}
		return zone1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone basicGetZone1() {
		return zone1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setZone1(Zone newZone1) {
		Zone oldZone1 = zone1;
		zone1 = newZone1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.DOOR__ZONE1, oldZone1, zone1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone getZone2() {
		if (zone2 != null && zone2.eIsProxy()) {
			InternalEObject oldZone2 = (InternalEObject)zone2;
			zone2 = (Zone)eResolveProxy(oldZone2);
			if (zone2 != oldZone2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.DOOR__ZONE2, oldZone2, zone2));
			}
		}
		return zone2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone basicGetZone2() {
		return zone2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setZone2(Zone newZone2) {
		Zone oldZone2 = zone2;
		zone2 = newZone2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.DOOR__ZONE2, oldZone2, zone2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Item> getKeys() {
		if (keys == null) {
			keys = new EObjectResolvingEList<Item>(Item.class, this, BuildingPackage.DOOR__KEYS);
		}
		return keys;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevel(int newLevel) {
		int oldLevel = level;
		level = newLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.DOOR__LEVEL, oldLevel, level));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Alarm> getAlarms() {
		if (alarms == null) {
			alarms = new EObjectResolvingEList<Alarm>(Alarm.class, this, BuildingPackage.DOOR__ALARMS);
		}
		return alarms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BuildingPackage.DOOR__ZONE1:
				if (resolve) return getZone1();
				return basicGetZone1();
			case BuildingPackage.DOOR__ZONE2:
				if (resolve) return getZone2();
				return basicGetZone2();
			case BuildingPackage.DOOR__KEYS:
				return getKeys();
			case BuildingPackage.DOOR__LEVEL:
				return getLevel();
			case BuildingPackage.DOOR__ALARMS:
				return getAlarms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BuildingPackage.DOOR__ZONE1:
				setZone1((Zone)newValue);
				return;
			case BuildingPackage.DOOR__ZONE2:
				setZone2((Zone)newValue);
				return;
			case BuildingPackage.DOOR__KEYS:
				getKeys().clear();
				getKeys().addAll((Collection<? extends Item>)newValue);
				return;
			case BuildingPackage.DOOR__LEVEL:
				setLevel((Integer)newValue);
				return;
			case BuildingPackage.DOOR__ALARMS:
				getAlarms().clear();
				getAlarms().addAll((Collection<? extends Alarm>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BuildingPackage.DOOR__ZONE1:
				setZone1((Zone)null);
				return;
			case BuildingPackage.DOOR__ZONE2:
				setZone2((Zone)null);
				return;
			case BuildingPackage.DOOR__KEYS:
				getKeys().clear();
				return;
			case BuildingPackage.DOOR__LEVEL:
				setLevel(LEVEL_EDEFAULT);
				return;
			case BuildingPackage.DOOR__ALARMS:
				getAlarms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BuildingPackage.DOOR__ZONE1:
				return zone1 != null;
			case BuildingPackage.DOOR__ZONE2:
				return zone2 != null;
			case BuildingPackage.DOOR__KEYS:
				return keys != null && !keys.isEmpty();
			case BuildingPackage.DOOR__LEVEL:
				return level != LEVEL_EDEFAULT;
			case BuildingPackage.DOOR__ALARMS:
				return alarms != null && !alarms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (level: ");
		result.append(level);
		result.append(')');
		return result.toString();
	}

} //DoorImpl
