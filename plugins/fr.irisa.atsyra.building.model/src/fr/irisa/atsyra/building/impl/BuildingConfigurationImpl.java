/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.AccessConfiguration;
import fr.irisa.atsyra.building.AlarmConfiguration;
import fr.irisa.atsyra.building.AttackerConfiguration;
import fr.irisa.atsyra.building.BuildingConfiguration;
import fr.irisa.atsyra.building.BuildingPackage;
import fr.irisa.atsyra.building.ItemConfiguration;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingConfigurationImpl#getAttackerConfigurations <em>Attacker Configurations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingConfigurationImpl#getAlarmConfigurations <em>Alarm Configurations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingConfigurationImpl#getAccessConfigurations <em>Access Configurations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingConfigurationImpl#getItemConfigurations <em>Item Configurations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BuildingConfigurationImpl extends MinimalEObjectImpl.Container implements BuildingConfiguration {
	/**
	 * The cached value of the '{@link #getAttackerConfigurations() <em>Attacker Configurations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttackerConfigurations()
	 * @generated
	 * @ordered
	 */
	protected EList<AttackerConfiguration> attackerConfigurations;

	/**
	 * The cached value of the '{@link #getAlarmConfigurations() <em>Alarm Configurations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarmConfigurations()
	 * @generated
	 * @ordered
	 */
	protected EList<AlarmConfiguration> alarmConfigurations;

	/**
	 * The cached value of the '{@link #getAccessConfigurations() <em>Access Configurations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessConfigurations()
	 * @generated
	 * @ordered
	 */
	protected EList<AccessConfiguration> accessConfigurations;

	/**
	 * The cached value of the '{@link #getItemConfigurations() <em>Item Configurations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItemConfigurations()
	 * @generated
	 * @ordered
	 */
	protected EList<ItemConfiguration> itemConfigurations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BuildingConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingPackage.Literals.BUILDING_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttackerConfiguration> getAttackerConfigurations() {
		if (attackerConfigurations == null) {
			attackerConfigurations = new EObjectContainmentEList<AttackerConfiguration>(AttackerConfiguration.class, this, BuildingPackage.BUILDING_CONFIGURATION__ATTACKER_CONFIGURATIONS);
		}
		return attackerConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AlarmConfiguration> getAlarmConfigurations() {
		if (alarmConfigurations == null) {
			alarmConfigurations = new EObjectContainmentEList<AlarmConfiguration>(AlarmConfiguration.class, this, BuildingPackage.BUILDING_CONFIGURATION__ALARM_CONFIGURATIONS);
		}
		return alarmConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessConfiguration> getAccessConfigurations() {
		if (accessConfigurations == null) {
			accessConfigurations = new EObjectContainmentEList<AccessConfiguration>(AccessConfiguration.class, this, BuildingPackage.BUILDING_CONFIGURATION__ACCESS_CONFIGURATIONS);
		}
		return accessConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ItemConfiguration> getItemConfigurations() {
		if (itemConfigurations == null) {
			itemConfigurations = new EObjectContainmentEList<ItemConfiguration>(ItemConfiguration.class, this, BuildingPackage.BUILDING_CONFIGURATION__ITEM_CONFIGURATIONS);
		}
		return itemConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BuildingPackage.BUILDING_CONFIGURATION__ATTACKER_CONFIGURATIONS:
				return ((InternalEList<?>)getAttackerConfigurations()).basicRemove(otherEnd, msgs);
			case BuildingPackage.BUILDING_CONFIGURATION__ALARM_CONFIGURATIONS:
				return ((InternalEList<?>)getAlarmConfigurations()).basicRemove(otherEnd, msgs);
			case BuildingPackage.BUILDING_CONFIGURATION__ACCESS_CONFIGURATIONS:
				return ((InternalEList<?>)getAccessConfigurations()).basicRemove(otherEnd, msgs);
			case BuildingPackage.BUILDING_CONFIGURATION__ITEM_CONFIGURATIONS:
				return ((InternalEList<?>)getItemConfigurations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BuildingPackage.BUILDING_CONFIGURATION__ATTACKER_CONFIGURATIONS:
				return getAttackerConfigurations();
			case BuildingPackage.BUILDING_CONFIGURATION__ALARM_CONFIGURATIONS:
				return getAlarmConfigurations();
			case BuildingPackage.BUILDING_CONFIGURATION__ACCESS_CONFIGURATIONS:
				return getAccessConfigurations();
			case BuildingPackage.BUILDING_CONFIGURATION__ITEM_CONFIGURATIONS:
				return getItemConfigurations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BuildingPackage.BUILDING_CONFIGURATION__ATTACKER_CONFIGURATIONS:
				getAttackerConfigurations().clear();
				getAttackerConfigurations().addAll((Collection<? extends AttackerConfiguration>)newValue);
				return;
			case BuildingPackage.BUILDING_CONFIGURATION__ALARM_CONFIGURATIONS:
				getAlarmConfigurations().clear();
				getAlarmConfigurations().addAll((Collection<? extends AlarmConfiguration>)newValue);
				return;
			case BuildingPackage.BUILDING_CONFIGURATION__ACCESS_CONFIGURATIONS:
				getAccessConfigurations().clear();
				getAccessConfigurations().addAll((Collection<? extends AccessConfiguration>)newValue);
				return;
			case BuildingPackage.BUILDING_CONFIGURATION__ITEM_CONFIGURATIONS:
				getItemConfigurations().clear();
				getItemConfigurations().addAll((Collection<? extends ItemConfiguration>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BuildingPackage.BUILDING_CONFIGURATION__ATTACKER_CONFIGURATIONS:
				getAttackerConfigurations().clear();
				return;
			case BuildingPackage.BUILDING_CONFIGURATION__ALARM_CONFIGURATIONS:
				getAlarmConfigurations().clear();
				return;
			case BuildingPackage.BUILDING_CONFIGURATION__ACCESS_CONFIGURATIONS:
				getAccessConfigurations().clear();
				return;
			case BuildingPackage.BUILDING_CONFIGURATION__ITEM_CONFIGURATIONS:
				getItemConfigurations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BuildingPackage.BUILDING_CONFIGURATION__ATTACKER_CONFIGURATIONS:
				return attackerConfigurations != null && !attackerConfigurations.isEmpty();
			case BuildingPackage.BUILDING_CONFIGURATION__ALARM_CONFIGURATIONS:
				return alarmConfigurations != null && !alarmConfigurations.isEmpty();
			case BuildingPackage.BUILDING_CONFIGURATION__ACCESS_CONFIGURATIONS:
				return accessConfigurations != null && !accessConfigurations.isEmpty();
			case BuildingPackage.BUILDING_CONFIGURATION__ITEM_CONFIGURATIONS:
				return itemConfigurations != null && !itemConfigurations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BuildingConfigurationImpl
