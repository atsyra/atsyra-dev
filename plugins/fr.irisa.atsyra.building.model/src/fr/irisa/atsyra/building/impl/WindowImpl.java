/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.Alarm;
import fr.irisa.atsyra.building.BuildingPackage;
import fr.irisa.atsyra.building.Window;
import fr.irisa.atsyra.building.Zone;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Window</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.impl.WindowImpl#getInside <em>Inside</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.WindowImpl#getOutside <em>Outside</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.WindowImpl#getLevel <em>Level</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.WindowImpl#getAlarms <em>Alarms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WindowImpl extends AccessImpl implements Window {
	/**
	 * The cached value of the '{@link #getInside() <em>Inside</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInside()
	 * @generated
	 * @ordered
	 */
	protected Zone inside;

	/**
	 * The cached value of the '{@link #getOutside() <em>Outside</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutside()
	 * @generated
	 * @ordered
	 */
	protected Zone outside;

	/**
	 * The default value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected static final int LEVEL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected int level = LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAlarms() <em>Alarms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarms()
	 * @generated
	 * @ordered
	 */
	protected EList<Alarm> alarms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WindowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingPackage.Literals.WINDOW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone getInside() {
		if (inside != null && inside.eIsProxy()) {
			InternalEObject oldInside = (InternalEObject)inside;
			inside = (Zone)eResolveProxy(oldInside);
			if (inside != oldInside) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.WINDOW__INSIDE, oldInside, inside));
			}
		}
		return inside;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone basicGetInside() {
		return inside;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInside(Zone newInside) {
		Zone oldInside = inside;
		inside = newInside;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.WINDOW__INSIDE, oldInside, inside));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone getOutside() {
		if (outside != null && outside.eIsProxy()) {
			InternalEObject oldOutside = (InternalEObject)outside;
			outside = (Zone)eResolveProxy(oldOutside);
			if (outside != oldOutside) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.WINDOW__OUTSIDE, oldOutside, outside));
			}
		}
		return outside;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone basicGetOutside() {
		return outside;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutside(Zone newOutside) {
		Zone oldOutside = outside;
		outside = newOutside;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.WINDOW__OUTSIDE, oldOutside, outside));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevel(int newLevel) {
		int oldLevel = level;
		level = newLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.WINDOW__LEVEL, oldLevel, level));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Alarm> getAlarms() {
		if (alarms == null) {
			alarms = new EObjectResolvingEList<Alarm>(Alarm.class, this, BuildingPackage.WINDOW__ALARMS);
		}
		return alarms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BuildingPackage.WINDOW__INSIDE:
				if (resolve) return getInside();
				return basicGetInside();
			case BuildingPackage.WINDOW__OUTSIDE:
				if (resolve) return getOutside();
				return basicGetOutside();
			case BuildingPackage.WINDOW__LEVEL:
				return getLevel();
			case BuildingPackage.WINDOW__ALARMS:
				return getAlarms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BuildingPackage.WINDOW__INSIDE:
				setInside((Zone)newValue);
				return;
			case BuildingPackage.WINDOW__OUTSIDE:
				setOutside((Zone)newValue);
				return;
			case BuildingPackage.WINDOW__LEVEL:
				setLevel((Integer)newValue);
				return;
			case BuildingPackage.WINDOW__ALARMS:
				getAlarms().clear();
				getAlarms().addAll((Collection<? extends Alarm>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BuildingPackage.WINDOW__INSIDE:
				setInside((Zone)null);
				return;
			case BuildingPackage.WINDOW__OUTSIDE:
				setOutside((Zone)null);
				return;
			case BuildingPackage.WINDOW__LEVEL:
				setLevel(LEVEL_EDEFAULT);
				return;
			case BuildingPackage.WINDOW__ALARMS:
				getAlarms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BuildingPackage.WINDOW__INSIDE:
				return inside != null;
			case BuildingPackage.WINDOW__OUTSIDE:
				return outside != null;
			case BuildingPackage.WINDOW__LEVEL:
				return level != LEVEL_EDEFAULT;
			case BuildingPackage.WINDOW__ALARMS:
				return alarms != null && !alarms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (level: ");
		result.append(level);
		result.append(')');
		return result.toString();
	}

} //WindowImpl
