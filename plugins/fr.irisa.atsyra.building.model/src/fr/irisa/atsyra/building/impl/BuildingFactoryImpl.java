/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BuildingFactoryImpl extends EFactoryImpl implements BuildingFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BuildingFactory init() {
		try {
			BuildingFactory theBuildingFactory = (BuildingFactory)EPackage.Registry.INSTANCE.getEFactory(BuildingPackage.eNS_URI);
			if (theBuildingFactory != null) {
				return theBuildingFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BuildingFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildingFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BuildingPackage.BUILDING: return createBuilding();
			case BuildingPackage.ZONE: return createZone();
			case BuildingPackage.VIRTUAL: return createVirtual();
			case BuildingPackage.DOOR: return createDoor();
			case BuildingPackage.WINDOW: return createWindow();
			case BuildingPackage.BADGED_DOOR: return createBadgedDoor();
			case BuildingPackage.ALARM: return createAlarm();
			case BuildingPackage.ITEM: return createItem();
			case BuildingPackage.NAMED_ELEMENT: return createNamedElement();
			case BuildingPackage.ATTACKER: return createAttacker();
			case BuildingPackage.BUILDING_CONFIGURATION: return createBuildingConfiguration();
			case BuildingPackage.BUILDING_MODEL: return createBuildingModel();
			case BuildingPackage.ATTACKER_CONFIGURATION: return createAttackerConfiguration();
			case BuildingPackage.ITEM_CONFIGURATION: return createItemConfiguration();
			case BuildingPackage.ACCESS_CONFIGURATION: return createAccessConfiguration();
			case BuildingPackage.ALARM_CONFIGURATION: return createAlarmConfiguration();
			case BuildingPackage.IMPORT: return createImport();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Building createBuilding() {
		BuildingImpl building = new BuildingImpl();
		return building;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone createZone() {
		ZoneImpl zone = new ZoneImpl();
		return zone;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Virtual createVirtual() {
		VirtualImpl virtual = new VirtualImpl();
		return virtual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Door createDoor() {
		DoorImpl door = new DoorImpl();
		return door;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Window createWindow() {
		WindowImpl window = new WindowImpl();
		return window;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BadgedDoor createBadgedDoor() {
		BadgedDoorImpl badgedDoor = new BadgedDoorImpl();
		return badgedDoor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alarm createAlarm() {
		AlarmImpl alarm = new AlarmImpl();
		return alarm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Item createItem() {
		ItemImpl item = new ItemImpl();
		return item;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedElement createNamedElement() {
		NamedElementImpl namedElement = new NamedElementImpl();
		return namedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attacker createAttacker() {
		AttackerImpl attacker = new AttackerImpl();
		return attacker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildingConfiguration createBuildingConfiguration() {
		BuildingConfigurationImpl buildingConfiguration = new BuildingConfigurationImpl();
		return buildingConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildingModel createBuildingModel() {
		BuildingModelImpl buildingModel = new BuildingModelImpl();
		return buildingModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackerConfiguration createAttackerConfiguration() {
		AttackerConfigurationImpl attackerConfiguration = new AttackerConfigurationImpl();
		return attackerConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ItemConfiguration createItemConfiguration() {
		ItemConfigurationImpl itemConfiguration = new ItemConfigurationImpl();
		return itemConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessConfiguration createAccessConfiguration() {
		AccessConfigurationImpl accessConfiguration = new AccessConfigurationImpl();
		return accessConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmConfiguration createAlarmConfiguration() {
		AlarmConfigurationImpl alarmConfiguration = new AlarmConfigurationImpl();
		return alarmConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Import createImport() {
		ImportImpl import_ = new ImportImpl();
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildingPackage getBuildingPackage() {
		return (BuildingPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BuildingPackage getPackage() {
		return BuildingPackage.eINSTANCE;
	}

} //BuildingFactoryImpl
