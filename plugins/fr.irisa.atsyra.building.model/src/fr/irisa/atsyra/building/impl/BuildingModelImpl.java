/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.Building;
import fr.irisa.atsyra.building.BuildingConfiguration;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.building.BuildingPackage;

import fr.irisa.atsyra.building.Import;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingModelImpl#getBuildings <em>Buildings</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingModelImpl#getBuildingConfigurations <em>Building Configurations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingModelImpl#getImports <em>Imports</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BuildingModelImpl extends MinimalEObjectImpl.Container implements BuildingModel {
	/**
	 * The cached value of the '{@link #getBuildings() <em>Buildings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBuildings()
	 * @generated
	 * @ordered
	 */
	protected EList<Building> buildings;

	/**
	 * The cached value of the '{@link #getBuildingConfigurations() <em>Building Configurations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBuildingConfigurations()
	 * @generated
	 * @ordered
	 */
	protected EList<BuildingConfiguration> buildingConfigurations;

	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> imports;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BuildingModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingPackage.Literals.BUILDING_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Building> getBuildings() {
		if (buildings == null) {
			buildings = new EObjectContainmentEList<Building>(Building.class, this, BuildingPackage.BUILDING_MODEL__BUILDINGS);
		}
		return buildings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BuildingConfiguration> getBuildingConfigurations() {
		if (buildingConfigurations == null) {
			buildingConfigurations = new EObjectContainmentEList<BuildingConfiguration>(BuildingConfiguration.class, this, BuildingPackage.BUILDING_MODEL__BUILDING_CONFIGURATIONS);
		}
		return buildingConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Import> getImports() {
		if (imports == null) {
			imports = new EObjectContainmentEList<Import>(Import.class, this, BuildingPackage.BUILDING_MODEL__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BuildingPackage.BUILDING_MODEL__BUILDINGS:
				return ((InternalEList<?>)getBuildings()).basicRemove(otherEnd, msgs);
			case BuildingPackage.BUILDING_MODEL__BUILDING_CONFIGURATIONS:
				return ((InternalEList<?>)getBuildingConfigurations()).basicRemove(otherEnd, msgs);
			case BuildingPackage.BUILDING_MODEL__IMPORTS:
				return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BuildingPackage.BUILDING_MODEL__BUILDINGS:
				return getBuildings();
			case BuildingPackage.BUILDING_MODEL__BUILDING_CONFIGURATIONS:
				return getBuildingConfigurations();
			case BuildingPackage.BUILDING_MODEL__IMPORTS:
				return getImports();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BuildingPackage.BUILDING_MODEL__BUILDINGS:
				getBuildings().clear();
				getBuildings().addAll((Collection<? extends Building>)newValue);
				return;
			case BuildingPackage.BUILDING_MODEL__BUILDING_CONFIGURATIONS:
				getBuildingConfigurations().clear();
				getBuildingConfigurations().addAll((Collection<? extends BuildingConfiguration>)newValue);
				return;
			case BuildingPackage.BUILDING_MODEL__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends Import>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BuildingPackage.BUILDING_MODEL__BUILDINGS:
				getBuildings().clear();
				return;
			case BuildingPackage.BUILDING_MODEL__BUILDING_CONFIGURATIONS:
				getBuildingConfigurations().clear();
				return;
			case BuildingPackage.BUILDING_MODEL__IMPORTS:
				getImports().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BuildingPackage.BUILDING_MODEL__BUILDINGS:
				return buildings != null && !buildings.isEmpty();
			case BuildingPackage.BUILDING_MODEL__BUILDING_CONFIGURATIONS:
				return buildingConfigurations != null && !buildingConfigurations.isEmpty();
			case BuildingPackage.BUILDING_MODEL__IMPORTS:
				return imports != null && !imports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BuildingModelImpl
