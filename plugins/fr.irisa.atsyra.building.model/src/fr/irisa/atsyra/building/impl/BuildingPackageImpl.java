/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.Access;
import fr.irisa.atsyra.building.AccessConfiguration;
import fr.irisa.atsyra.building.Alarm;
import fr.irisa.atsyra.building.AlarmConfiguration;
import fr.irisa.atsyra.building.Attacker;
import fr.irisa.atsyra.building.AttackerConfiguration;
import fr.irisa.atsyra.building.BadgedDoor;
import fr.irisa.atsyra.building.Building;
import fr.irisa.atsyra.building.BuildingConfiguration;
import fr.irisa.atsyra.building.BuildingFactory;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.building.BuildingPackage;
import fr.irisa.atsyra.building.Door;
import fr.irisa.atsyra.building.Import;
import fr.irisa.atsyra.building.Item;
import fr.irisa.atsyra.building.ItemConfiguration;
import fr.irisa.atsyra.building.NamedElement;
import fr.irisa.atsyra.building.Virtual;
import fr.irisa.atsyra.building.Window;
import fr.irisa.atsyra.building.Zone;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BuildingPackageImpl extends EPackageImpl implements BuildingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass buildingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass zoneEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass virtualEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass windowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass badgedDoorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alarmEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass itemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attackerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass buildingConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass buildingModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attackerConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass itemConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alarmConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.atsyra.building.BuildingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BuildingPackageImpl() {
		super(eNS_URI, BuildingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BuildingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BuildingPackage init() {
		if (isInited) return (BuildingPackage)EPackage.Registry.INSTANCE.getEPackage(BuildingPackage.eNS_URI);

		// Obtain or create and register package
		BuildingPackageImpl theBuildingPackage = (BuildingPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BuildingPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BuildingPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theBuildingPackage.createPackageContents();

		// Initialize created meta-data
		theBuildingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBuildingPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BuildingPackage.eNS_URI, theBuildingPackage);
		return theBuildingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBuilding() {
		return buildingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuilding_Zones() {
		return (EReference)buildingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuilding_Alarms() {
		return (EReference)buildingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuilding_Accesses() {
		return (EReference)buildingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuilding_Items() {
		return (EReference)buildingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuilding_Attackers() {
		return (EReference)buildingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getZone() {
		return zoneEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getZone_Alarms() {
		return (EReference)zoneEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccess() {
		return accessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVirtual() {
		return virtualEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVirtual_Zone1() {
		return (EReference)virtualEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVirtual_Zone2() {
		return (EReference)virtualEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDoor() {
		return doorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoor_Zone1() {
		return (EReference)doorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoor_Zone2() {
		return (EReference)doorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoor_Keys() {
		return (EReference)doorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDoor_Level() {
		return (EAttribute)doorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoor_Alarms() {
		return (EReference)doorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWindow() {
		return windowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWindow_Inside() {
		return (EReference)windowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWindow_Outside() {
		return (EReference)windowEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWindow_Level() {
		return (EAttribute)windowEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWindow_Alarms() {
		return (EReference)windowEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBadgedDoor() {
		return badgedDoorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBadgedDoor_Inside() {
		return (EReference)badgedDoorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBadgedDoor_Outside() {
		return (EReference)badgedDoorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBadgedDoor_Badges() {
		return (EReference)badgedDoorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBadgedDoor_Level() {
		return (EAttribute)badgedDoorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBadgedDoor_Alarms() {
		return (EReference)badgedDoorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlarm() {
		return alarmEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAlarm_Location() {
		return (EReference)alarmEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getItem() {
		return itemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttacker() {
		return attackerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttacker_Level() {
		return (EAttribute)attackerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBuildingConfiguration() {
		return buildingConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuildingConfiguration_AttackerConfigurations() {
		return (EReference)buildingConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuildingConfiguration_AlarmConfigurations() {
		return (EReference)buildingConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuildingConfiguration_AccessConfigurations() {
		return (EReference)buildingConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuildingConfiguration_ItemConfigurations() {
		return (EReference)buildingConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBuildingModel() {
		return buildingModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuildingModel_Buildings() {
		return (EReference)buildingModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuildingModel_BuildingConfigurations() {
		return (EReference)buildingModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuildingModel_Imports() {
		return (EReference)buildingModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttackerConfiguration() {
		return attackerConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttackerConfiguration_Attacker() {
		return (EReference)attackerConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttackerConfiguration_Location() {
		return (EReference)attackerConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getItemConfiguration() {
		return itemConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getItemConfiguration_Item() {
		return (EReference)itemConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getItemConfiguration_Location() {
		return (EReference)itemConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getItemConfiguration_OwnedBy() {
		return (EReference)itemConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessConfiguration() {
		return accessConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessConfiguration_Access() {
		return (EReference)accessConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAccessConfiguration_IsOpen() {
		return (EAttribute)accessConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAccessConfiguration_IsLocked() {
		return (EAttribute)accessConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlarmConfiguration() {
		return alarmConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAlarmConfiguration_Alarm() {
		return (EReference)alarmConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAlarmConfiguration_WasTriggered() {
		return (EAttribute)alarmConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAlarmConfiguration_IsEnabled() {
		return (EAttribute)alarmConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImport() {
		return importEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImport_ImportURI() {
		return (EAttribute)importEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildingFactory getBuildingFactory() {
		return (BuildingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		buildingEClass = createEClass(BUILDING);
		createEReference(buildingEClass, BUILDING__ZONES);
		createEReference(buildingEClass, BUILDING__ALARMS);
		createEReference(buildingEClass, BUILDING__ACCESSES);
		createEReference(buildingEClass, BUILDING__ITEMS);
		createEReference(buildingEClass, BUILDING__ATTACKERS);

		zoneEClass = createEClass(ZONE);
		createEReference(zoneEClass, ZONE__ALARMS);

		accessEClass = createEClass(ACCESS);

		virtualEClass = createEClass(VIRTUAL);
		createEReference(virtualEClass, VIRTUAL__ZONE1);
		createEReference(virtualEClass, VIRTUAL__ZONE2);

		doorEClass = createEClass(DOOR);
		createEReference(doorEClass, DOOR__ZONE1);
		createEReference(doorEClass, DOOR__ZONE2);
		createEReference(doorEClass, DOOR__KEYS);
		createEAttribute(doorEClass, DOOR__LEVEL);
		createEReference(doorEClass, DOOR__ALARMS);

		windowEClass = createEClass(WINDOW);
		createEReference(windowEClass, WINDOW__INSIDE);
		createEReference(windowEClass, WINDOW__OUTSIDE);
		createEAttribute(windowEClass, WINDOW__LEVEL);
		createEReference(windowEClass, WINDOW__ALARMS);

		badgedDoorEClass = createEClass(BADGED_DOOR);
		createEReference(badgedDoorEClass, BADGED_DOOR__INSIDE);
		createEReference(badgedDoorEClass, BADGED_DOOR__OUTSIDE);
		createEReference(badgedDoorEClass, BADGED_DOOR__BADGES);
		createEAttribute(badgedDoorEClass, BADGED_DOOR__LEVEL);
		createEReference(badgedDoorEClass, BADGED_DOOR__ALARMS);

		alarmEClass = createEClass(ALARM);
		createEReference(alarmEClass, ALARM__LOCATION);

		itemEClass = createEClass(ITEM);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		attackerEClass = createEClass(ATTACKER);
		createEAttribute(attackerEClass, ATTACKER__LEVEL);

		buildingConfigurationEClass = createEClass(BUILDING_CONFIGURATION);
		createEReference(buildingConfigurationEClass, BUILDING_CONFIGURATION__ATTACKER_CONFIGURATIONS);
		createEReference(buildingConfigurationEClass, BUILDING_CONFIGURATION__ALARM_CONFIGURATIONS);
		createEReference(buildingConfigurationEClass, BUILDING_CONFIGURATION__ACCESS_CONFIGURATIONS);
		createEReference(buildingConfigurationEClass, BUILDING_CONFIGURATION__ITEM_CONFIGURATIONS);

		buildingModelEClass = createEClass(BUILDING_MODEL);
		createEReference(buildingModelEClass, BUILDING_MODEL__BUILDINGS);
		createEReference(buildingModelEClass, BUILDING_MODEL__BUILDING_CONFIGURATIONS);
		createEReference(buildingModelEClass, BUILDING_MODEL__IMPORTS);

		attackerConfigurationEClass = createEClass(ATTACKER_CONFIGURATION);
		createEReference(attackerConfigurationEClass, ATTACKER_CONFIGURATION__ATTACKER);
		createEReference(attackerConfigurationEClass, ATTACKER_CONFIGURATION__LOCATION);

		itemConfigurationEClass = createEClass(ITEM_CONFIGURATION);
		createEReference(itemConfigurationEClass, ITEM_CONFIGURATION__ITEM);
		createEReference(itemConfigurationEClass, ITEM_CONFIGURATION__LOCATION);
		createEReference(itemConfigurationEClass, ITEM_CONFIGURATION__OWNED_BY);

		accessConfigurationEClass = createEClass(ACCESS_CONFIGURATION);
		createEReference(accessConfigurationEClass, ACCESS_CONFIGURATION__ACCESS);
		createEAttribute(accessConfigurationEClass, ACCESS_CONFIGURATION__IS_OPEN);
		createEAttribute(accessConfigurationEClass, ACCESS_CONFIGURATION__IS_LOCKED);

		alarmConfigurationEClass = createEClass(ALARM_CONFIGURATION);
		createEReference(alarmConfigurationEClass, ALARM_CONFIGURATION__ALARM);
		createEAttribute(alarmConfigurationEClass, ALARM_CONFIGURATION__WAS_TRIGGERED);
		createEAttribute(alarmConfigurationEClass, ALARM_CONFIGURATION__IS_ENABLED);

		importEClass = createEClass(IMPORT);
		createEAttribute(importEClass, IMPORT__IMPORT_URI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		buildingEClass.getESuperTypes().add(this.getNamedElement());
		zoneEClass.getESuperTypes().add(this.getNamedElement());
		accessEClass.getESuperTypes().add(this.getNamedElement());
		virtualEClass.getESuperTypes().add(this.getAccess());
		doorEClass.getESuperTypes().add(this.getAccess());
		windowEClass.getESuperTypes().add(this.getAccess());
		badgedDoorEClass.getESuperTypes().add(this.getAccess());
		alarmEClass.getESuperTypes().add(this.getNamedElement());
		itemEClass.getESuperTypes().add(this.getNamedElement());
		attackerEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(buildingEClass, Building.class, "Building", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBuilding_Zones(), this.getZone(), null, "zones", null, 0, -1, Building.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBuilding_Zones().getEKeys().add(this.getNamedElement_Name());
		initEReference(getBuilding_Alarms(), this.getAlarm(), null, "alarms", null, 0, -1, Building.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBuilding_Alarms().getEKeys().add(this.getNamedElement_Name());
		initEReference(getBuilding_Accesses(), this.getAccess(), null, "accesses", null, 0, -1, Building.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBuilding_Accesses().getEKeys().add(this.getNamedElement_Name());
		initEReference(getBuilding_Items(), this.getItem(), null, "items", null, 0, -1, Building.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBuilding_Items().getEKeys().add(this.getNamedElement_Name());
		initEReference(getBuilding_Attackers(), this.getAttacker(), null, "attackers", null, 0, -1, Building.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBuilding_Attackers().getEKeys().add(this.getNamedElement_Name());

		initEClass(zoneEClass, Zone.class, "Zone", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getZone_Alarms(), this.getAlarm(), null, "alarms", null, 0, -1, Zone.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getZone_Alarms().getEKeys().add(this.getNamedElement_Name());

		initEClass(accessEClass, Access.class, "Access", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(virtualEClass, Virtual.class, "Virtual", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVirtual_Zone1(), this.getZone(), null, "zone1", null, 0, 1, Virtual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getVirtual_Zone1().getEKeys().add(this.getNamedElement_Name());
		initEReference(getVirtual_Zone2(), this.getZone(), null, "zone2", null, 0, 1, Virtual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getVirtual_Zone2().getEKeys().add(this.getNamedElement_Name());

		initEClass(doorEClass, Door.class, "Door", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDoor_Zone1(), this.getZone(), null, "zone1", null, 0, 1, Door.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getDoor_Zone1().getEKeys().add(this.getNamedElement_Name());
		initEReference(getDoor_Zone2(), this.getZone(), null, "zone2", null, 0, 1, Door.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getDoor_Zone2().getEKeys().add(this.getNamedElement_Name());
		initEReference(getDoor_Keys(), this.getItem(), null, "keys", null, 0, -1, Door.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDoor_Level(), ecorePackage.getEInt(), "level", null, 0, 1, Door.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDoor_Alarms(), this.getAlarm(), null, "alarms", null, 0, -1, Door.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getDoor_Alarms().getEKeys().add(this.getNamedElement_Name());

		initEClass(windowEClass, Window.class, "Window", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWindow_Inside(), this.getZone(), null, "inside", null, 0, 1, Window.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getWindow_Inside().getEKeys().add(this.getNamedElement_Name());
		initEReference(getWindow_Outside(), this.getZone(), null, "outside", null, 0, 1, Window.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getWindow_Outside().getEKeys().add(this.getNamedElement_Name());
		initEAttribute(getWindow_Level(), ecorePackage.getEInt(), "level", null, 0, 1, Window.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWindow_Alarms(), this.getAlarm(), null, "alarms", null, 0, -1, Window.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getWindow_Alarms().getEKeys().add(this.getNamedElement_Name());

		initEClass(badgedDoorEClass, BadgedDoor.class, "BadgedDoor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBadgedDoor_Inside(), this.getZone(), null, "inside", null, 0, 1, BadgedDoor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBadgedDoor_Inside().getEKeys().add(this.getNamedElement_Name());
		initEReference(getBadgedDoor_Outside(), this.getZone(), null, "outside", null, 0, 1, BadgedDoor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBadgedDoor_Outside().getEKeys().add(this.getNamedElement_Name());
		initEReference(getBadgedDoor_Badges(), this.getItem(), null, "badges", null, 0, -1, BadgedDoor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBadgedDoor_Badges().getEKeys().add(this.getNamedElement_Name());
		initEAttribute(getBadgedDoor_Level(), ecorePackage.getEInt(), "level", null, 0, 1, BadgedDoor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBadgedDoor_Alarms(), this.getAlarm(), null, "alarms", null, 0, -1, BadgedDoor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBadgedDoor_Alarms().getEKeys().add(this.getNamedElement_Name());

		initEClass(alarmEClass, Alarm.class, "Alarm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAlarm_Location(), this.getZone(), null, "location", null, 0, 1, Alarm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAlarm_Location().getEKeys().add(this.getNamedElement_Name());

		initEClass(itemEClass, Item.class, "Item", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attackerEClass, Attacker.class, "Attacker", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAttacker_Level(), ecorePackage.getEInt(), "level", null, 0, 1, Attacker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(buildingConfigurationEClass, BuildingConfiguration.class, "BuildingConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBuildingConfiguration_AttackerConfigurations(), this.getAttackerConfiguration(), null, "attackerConfigurations", null, 0, -1, BuildingConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBuildingConfiguration_AlarmConfigurations(), this.getAlarmConfiguration(), null, "alarmConfigurations", null, 0, -1, BuildingConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBuildingConfiguration_AccessConfigurations(), this.getAccessConfiguration(), null, "accessConfigurations", null, 0, -1, BuildingConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBuildingConfiguration_ItemConfigurations(), this.getItemConfiguration(), null, "itemConfigurations", null, 0, -1, BuildingConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(buildingModelEClass, BuildingModel.class, "BuildingModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBuildingModel_Buildings(), this.getBuilding(), null, "buildings", null, 0, -1, BuildingModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBuildingModel_Buildings().getEKeys().add(this.getNamedElement_Name());
		initEReference(getBuildingModel_BuildingConfigurations(), this.getBuildingConfiguration(), null, "buildingConfigurations", null, 0, -1, BuildingModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBuildingModel_Imports(), this.getImport(), null, "imports", null, 0, -1, BuildingModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getBuildingModel_Imports().getEKeys().add(this.getImport_ImportURI());

		initEClass(attackerConfigurationEClass, AttackerConfiguration.class, "AttackerConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttackerConfiguration_Attacker(), this.getAttacker(), null, "attacker", null, 1, 1, AttackerConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAttackerConfiguration_Attacker().getEKeys().add(this.getNamedElement_Name());
		initEReference(getAttackerConfiguration_Location(), this.getZone(), null, "location", null, 0, 1, AttackerConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAttackerConfiguration_Location().getEKeys().add(this.getNamedElement_Name());

		initEClass(itemConfigurationEClass, ItemConfiguration.class, "ItemConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getItemConfiguration_Item(), this.getItem(), null, "item", null, 1, 1, ItemConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getItemConfiguration_Item().getEKeys().add(this.getNamedElement_Name());
		initEReference(getItemConfiguration_Location(), this.getZone(), null, "location", null, 0, 1, ItemConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getItemConfiguration_Location().getEKeys().add(this.getNamedElement_Name());
		initEReference(getItemConfiguration_OwnedBy(), this.getAttacker(), null, "ownedBy", null, 0, 1, ItemConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getItemConfiguration_OwnedBy().getEKeys().add(this.getNamedElement_Name());

		initEClass(accessConfigurationEClass, AccessConfiguration.class, "AccessConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccessConfiguration_Access(), this.getAccess(), null, "access", null, 1, 1, AccessConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAccessConfiguration_Access().getEKeys().add(this.getNamedElement_Name());
		initEAttribute(getAccessConfiguration_IsOpen(), ecorePackage.getEBoolean(), "isOpen", null, 0, 1, AccessConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAccessConfiguration_IsLocked(), ecorePackage.getEBoolean(), "isLocked", null, 0, 1, AccessConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(alarmConfigurationEClass, AlarmConfiguration.class, "AlarmConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAlarmConfiguration_Alarm(), this.getAlarm(), null, "alarm", null, 1, 1, AlarmConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAlarmConfiguration_Alarm().getEKeys().add(this.getNamedElement_Name());
		initEAttribute(getAlarmConfiguration_WasTriggered(), ecorePackage.getEBoolean(), "wasTriggered", null, 0, 1, AlarmConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAlarmConfiguration_IsEnabled(), ecorePackage.getEBoolean(), "isEnabled", null, 0, 1, AlarmConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(importEClass, Import.class, "Import", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImport_ImportURI(), ecorePackage.getEString(), "importURI", null, 0, 1, Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //BuildingPackageImpl
