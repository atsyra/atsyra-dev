/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.Attacker;
import fr.irisa.atsyra.building.BuildingPackage;
import fr.irisa.atsyra.building.Item;
import fr.irisa.atsyra.building.ItemConfiguration;
import fr.irisa.atsyra.building.Zone;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Item Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.impl.ItemConfigurationImpl#getItem <em>Item</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.ItemConfigurationImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.ItemConfigurationImpl#getOwnedBy <em>Owned By</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ItemConfigurationImpl extends MinimalEObjectImpl.Container implements ItemConfiguration {
	/**
	 * The cached value of the '{@link #getItem() <em>Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItem()
	 * @generated
	 * @ordered
	 */
	protected Item item;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected Zone location;

	/**
	 * The cached value of the '{@link #getOwnedBy() <em>Owned By</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedBy()
	 * @generated
	 * @ordered
	 */
	protected Attacker ownedBy;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ItemConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingPackage.Literals.ITEM_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Item getItem() {
		if (item != null && item.eIsProxy()) {
			InternalEObject oldItem = (InternalEObject)item;
			item = (Item)eResolveProxy(oldItem);
			if (item != oldItem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.ITEM_CONFIGURATION__ITEM, oldItem, item));
			}
		}
		return item;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Item basicGetItem() {
		return item;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setItem(Item newItem) {
		Item oldItem = item;
		item = newItem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.ITEM_CONFIGURATION__ITEM, oldItem, item));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone getLocation() {
		if (location != null && location.eIsProxy()) {
			InternalEObject oldLocation = (InternalEObject)location;
			location = (Zone)eResolveProxy(oldLocation);
			if (location != oldLocation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.ITEM_CONFIGURATION__LOCATION, oldLocation, location));
			}
		}
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone basicGetLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(Zone newLocation) {
		Zone oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.ITEM_CONFIGURATION__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attacker getOwnedBy() {
		if (ownedBy != null && ownedBy.eIsProxy()) {
			InternalEObject oldOwnedBy = (InternalEObject)ownedBy;
			ownedBy = (Attacker)eResolveProxy(oldOwnedBy);
			if (ownedBy != oldOwnedBy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.ITEM_CONFIGURATION__OWNED_BY, oldOwnedBy, ownedBy));
			}
		}
		return ownedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attacker basicGetOwnedBy() {
		return ownedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwnedBy(Attacker newOwnedBy) {
		Attacker oldOwnedBy = ownedBy;
		ownedBy = newOwnedBy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.ITEM_CONFIGURATION__OWNED_BY, oldOwnedBy, ownedBy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BuildingPackage.ITEM_CONFIGURATION__ITEM:
				if (resolve) return getItem();
				return basicGetItem();
			case BuildingPackage.ITEM_CONFIGURATION__LOCATION:
				if (resolve) return getLocation();
				return basicGetLocation();
			case BuildingPackage.ITEM_CONFIGURATION__OWNED_BY:
				if (resolve) return getOwnedBy();
				return basicGetOwnedBy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BuildingPackage.ITEM_CONFIGURATION__ITEM:
				setItem((Item)newValue);
				return;
			case BuildingPackage.ITEM_CONFIGURATION__LOCATION:
				setLocation((Zone)newValue);
				return;
			case BuildingPackage.ITEM_CONFIGURATION__OWNED_BY:
				setOwnedBy((Attacker)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BuildingPackage.ITEM_CONFIGURATION__ITEM:
				setItem((Item)null);
				return;
			case BuildingPackage.ITEM_CONFIGURATION__LOCATION:
				setLocation((Zone)null);
				return;
			case BuildingPackage.ITEM_CONFIGURATION__OWNED_BY:
				setOwnedBy((Attacker)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BuildingPackage.ITEM_CONFIGURATION__ITEM:
				return item != null;
			case BuildingPackage.ITEM_CONFIGURATION__LOCATION:
				return location != null;
			case BuildingPackage.ITEM_CONFIGURATION__OWNED_BY:
				return ownedBy != null;
		}
		return super.eIsSet(featureID);
	}

} //ItemConfigurationImpl
