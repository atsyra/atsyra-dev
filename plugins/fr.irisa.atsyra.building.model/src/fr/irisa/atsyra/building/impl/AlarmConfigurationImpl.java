/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.Alarm;
import fr.irisa.atsyra.building.AlarmConfiguration;
import fr.irisa.atsyra.building.BuildingPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alarm Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.impl.AlarmConfigurationImpl#getAlarm <em>Alarm</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.AlarmConfigurationImpl#isWasTriggered <em>Was Triggered</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.AlarmConfigurationImpl#isIsEnabled <em>Is Enabled</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlarmConfigurationImpl extends MinimalEObjectImpl.Container implements AlarmConfiguration {
	/**
	 * The cached value of the '{@link #getAlarm() <em>Alarm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarm()
	 * @generated
	 * @ordered
	 */
	protected Alarm alarm;

	/**
	 * The default value of the '{@link #isWasTriggered() <em>Was Triggered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWasTriggered()
	 * @generated
	 * @ordered
	 */
	protected static final boolean WAS_TRIGGERED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isWasTriggered() <em>Was Triggered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWasTriggered()
	 * @generated
	 * @ordered
	 */
	protected boolean wasTriggered = WAS_TRIGGERED_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsEnabled() <em>Is Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsEnabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ENABLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsEnabled() <em>Is Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsEnabled()
	 * @generated
	 * @ordered
	 */
	protected boolean isEnabled = IS_ENABLED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlarmConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingPackage.Literals.ALARM_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alarm getAlarm() {
		if (alarm != null && alarm.eIsProxy()) {
			InternalEObject oldAlarm = (InternalEObject)alarm;
			alarm = (Alarm)eResolveProxy(oldAlarm);
			if (alarm != oldAlarm) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.ALARM_CONFIGURATION__ALARM, oldAlarm, alarm));
			}
		}
		return alarm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alarm basicGetAlarm() {
		return alarm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlarm(Alarm newAlarm) {
		Alarm oldAlarm = alarm;
		alarm = newAlarm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.ALARM_CONFIGURATION__ALARM, oldAlarm, alarm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isWasTriggered() {
		return wasTriggered;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWasTriggered(boolean newWasTriggered) {
		boolean oldWasTriggered = wasTriggered;
		wasTriggered = newWasTriggered;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.ALARM_CONFIGURATION__WAS_TRIGGERED, oldWasTriggered, wasTriggered));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsEnabled() {
		return isEnabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsEnabled(boolean newIsEnabled) {
		boolean oldIsEnabled = isEnabled;
		isEnabled = newIsEnabled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.ALARM_CONFIGURATION__IS_ENABLED, oldIsEnabled, isEnabled));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BuildingPackage.ALARM_CONFIGURATION__ALARM:
				if (resolve) return getAlarm();
				return basicGetAlarm();
			case BuildingPackage.ALARM_CONFIGURATION__WAS_TRIGGERED:
				return isWasTriggered();
			case BuildingPackage.ALARM_CONFIGURATION__IS_ENABLED:
				return isIsEnabled();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BuildingPackage.ALARM_CONFIGURATION__ALARM:
				setAlarm((Alarm)newValue);
				return;
			case BuildingPackage.ALARM_CONFIGURATION__WAS_TRIGGERED:
				setWasTriggered((Boolean)newValue);
				return;
			case BuildingPackage.ALARM_CONFIGURATION__IS_ENABLED:
				setIsEnabled((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BuildingPackage.ALARM_CONFIGURATION__ALARM:
				setAlarm((Alarm)null);
				return;
			case BuildingPackage.ALARM_CONFIGURATION__WAS_TRIGGERED:
				setWasTriggered(WAS_TRIGGERED_EDEFAULT);
				return;
			case BuildingPackage.ALARM_CONFIGURATION__IS_ENABLED:
				setIsEnabled(IS_ENABLED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BuildingPackage.ALARM_CONFIGURATION__ALARM:
				return alarm != null;
			case BuildingPackage.ALARM_CONFIGURATION__WAS_TRIGGERED:
				return wasTriggered != WAS_TRIGGERED_EDEFAULT;
			case BuildingPackage.ALARM_CONFIGURATION__IS_ENABLED:
				return isEnabled != IS_ENABLED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (wasTriggered: ");
		result.append(wasTriggered);
		result.append(", isEnabled: ");
		result.append(isEnabled);
		result.append(')');
		return result.toString();
	}

} //AlarmConfigurationImpl
