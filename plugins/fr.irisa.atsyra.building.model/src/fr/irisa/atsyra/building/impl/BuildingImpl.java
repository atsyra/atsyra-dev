/*******************************************************************************
 * Copyright (c) 2014, 2016 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.Access;
import fr.irisa.atsyra.building.Alarm;
import fr.irisa.atsyra.building.Attacker;
import fr.irisa.atsyra.building.Building;
import fr.irisa.atsyra.building.BuildingPackage;
import fr.irisa.atsyra.building.Item;
import fr.irisa.atsyra.building.Zone;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Building</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingImpl#getZones <em>Zones</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingImpl#getAlarms <em>Alarms</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingImpl#getAccesses <em>Accesses</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingImpl#getItems <em>Items</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.BuildingImpl#getAttackers <em>Attackers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BuildingImpl extends NamedElementImpl implements Building {
	/**
	 * The cached value of the '{@link #getZones() <em>Zones</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZones()
	 * @generated
	 * @ordered
	 */
	protected EList<Zone> zones;

	/**
	 * The cached value of the '{@link #getAlarms() <em>Alarms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarms()
	 * @generated
	 * @ordered
	 */
	protected EList<Alarm> alarms;

	/**
	 * The cached value of the '{@link #getAccesses() <em>Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<Access> accesses;

	/**
	 * The cached value of the '{@link #getItems() <em>Items</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItems()
	 * @generated
	 * @ordered
	 */
	protected EList<Item> items;

	/**
	 * The cached value of the '{@link #getAttackers() <em>Attackers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttackers()
	 * @generated
	 * @ordered
	 */
	protected EList<Attacker> attackers;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BuildingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingPackage.Literals.BUILDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Zone> getZones() {
		if (zones == null) {
			zones = new EObjectContainmentEList<Zone>(Zone.class, this, BuildingPackage.BUILDING__ZONES);
		}
		return zones;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Alarm> getAlarms() {
		if (alarms == null) {
			alarms = new EObjectContainmentEList<Alarm>(Alarm.class, this, BuildingPackage.BUILDING__ALARMS);
		}
		return alarms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Access> getAccesses() {
		if (accesses == null) {
			accesses = new EObjectContainmentEList<Access>(Access.class, this, BuildingPackage.BUILDING__ACCESSES);
		}
		return accesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Item> getItems() {
		if (items == null) {
			items = new EObjectContainmentEList<Item>(Item.class, this, BuildingPackage.BUILDING__ITEMS);
		}
		return items;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attacker> getAttackers() {
		if (attackers == null) {
			attackers = new EObjectContainmentEList<Attacker>(Attacker.class, this, BuildingPackage.BUILDING__ATTACKERS);
		}
		return attackers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BuildingPackage.BUILDING__ZONES:
				return ((InternalEList<?>)getZones()).basicRemove(otherEnd, msgs);
			case BuildingPackage.BUILDING__ALARMS:
				return ((InternalEList<?>)getAlarms()).basicRemove(otherEnd, msgs);
			case BuildingPackage.BUILDING__ACCESSES:
				return ((InternalEList<?>)getAccesses()).basicRemove(otherEnd, msgs);
			case BuildingPackage.BUILDING__ITEMS:
				return ((InternalEList<?>)getItems()).basicRemove(otherEnd, msgs);
			case BuildingPackage.BUILDING__ATTACKERS:
				return ((InternalEList<?>)getAttackers()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BuildingPackage.BUILDING__ZONES:
				return getZones();
			case BuildingPackage.BUILDING__ALARMS:
				return getAlarms();
			case BuildingPackage.BUILDING__ACCESSES:
				return getAccesses();
			case BuildingPackage.BUILDING__ITEMS:
				return getItems();
			case BuildingPackage.BUILDING__ATTACKERS:
				return getAttackers();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BuildingPackage.BUILDING__ZONES:
				getZones().clear();
				getZones().addAll((Collection<? extends Zone>)newValue);
				return;
			case BuildingPackage.BUILDING__ALARMS:
				getAlarms().clear();
				getAlarms().addAll((Collection<? extends Alarm>)newValue);
				return;
			case BuildingPackage.BUILDING__ACCESSES:
				getAccesses().clear();
				getAccesses().addAll((Collection<? extends Access>)newValue);
				return;
			case BuildingPackage.BUILDING__ITEMS:
				getItems().clear();
				getItems().addAll((Collection<? extends Item>)newValue);
				return;
			case BuildingPackage.BUILDING__ATTACKERS:
				getAttackers().clear();
				getAttackers().addAll((Collection<? extends Attacker>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BuildingPackage.BUILDING__ZONES:
				getZones().clear();
				return;
			case BuildingPackage.BUILDING__ALARMS:
				getAlarms().clear();
				return;
			case BuildingPackage.BUILDING__ACCESSES:
				getAccesses().clear();
				return;
			case BuildingPackage.BUILDING__ITEMS:
				getItems().clear();
				return;
			case BuildingPackage.BUILDING__ATTACKERS:
				getAttackers().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BuildingPackage.BUILDING__ZONES:
				return zones != null && !zones.isEmpty();
			case BuildingPackage.BUILDING__ALARMS:
				return alarms != null && !alarms.isEmpty();
			case BuildingPackage.BUILDING__ACCESSES:
				return accesses != null && !accesses.isEmpty();
			case BuildingPackage.BUILDING__ITEMS:
				return items != null && !items.isEmpty();
			case BuildingPackage.BUILDING__ATTACKERS:
				return attackers != null && !attackers.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BuildingImpl
