/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.impl;

import fr.irisa.atsyra.building.BuildingPackage;
import fr.irisa.atsyra.building.Virtual;
import fr.irisa.atsyra.building.Zone;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Virtual</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.impl.VirtualImpl#getZone1 <em>Zone1</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.impl.VirtualImpl#getZone2 <em>Zone2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VirtualImpl extends AccessImpl implements Virtual {
	/**
	 * The cached value of the '{@link #getZone1() <em>Zone1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZone1()
	 * @generated
	 * @ordered
	 */
	protected Zone zone1;

	/**
	 * The cached value of the '{@link #getZone2() <em>Zone2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZone2()
	 * @generated
	 * @ordered
	 */
	protected Zone zone2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VirtualImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingPackage.Literals.VIRTUAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone getZone1() {
		if (zone1 != null && zone1.eIsProxy()) {
			InternalEObject oldZone1 = (InternalEObject)zone1;
			zone1 = (Zone)eResolveProxy(oldZone1);
			if (zone1 != oldZone1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.VIRTUAL__ZONE1, oldZone1, zone1));
			}
		}
		return zone1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone basicGetZone1() {
		return zone1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setZone1(Zone newZone1) {
		Zone oldZone1 = zone1;
		zone1 = newZone1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.VIRTUAL__ZONE1, oldZone1, zone1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone getZone2() {
		if (zone2 != null && zone2.eIsProxy()) {
			InternalEObject oldZone2 = (InternalEObject)zone2;
			zone2 = (Zone)eResolveProxy(oldZone2);
			if (zone2 != oldZone2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BuildingPackage.VIRTUAL__ZONE2, oldZone2, zone2));
			}
		}
		return zone2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone basicGetZone2() {
		return zone2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setZone2(Zone newZone2) {
		Zone oldZone2 = zone2;
		zone2 = newZone2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BuildingPackage.VIRTUAL__ZONE2, oldZone2, zone2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BuildingPackage.VIRTUAL__ZONE1:
				if (resolve) return getZone1();
				return basicGetZone1();
			case BuildingPackage.VIRTUAL__ZONE2:
				if (resolve) return getZone2();
				return basicGetZone2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BuildingPackage.VIRTUAL__ZONE1:
				setZone1((Zone)newValue);
				return;
			case BuildingPackage.VIRTUAL__ZONE2:
				setZone2((Zone)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BuildingPackage.VIRTUAL__ZONE1:
				setZone1((Zone)null);
				return;
			case BuildingPackage.VIRTUAL__ZONE2:
				setZone2((Zone)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BuildingPackage.VIRTUAL__ZONE1:
				return zone1 != null;
			case BuildingPackage.VIRTUAL__ZONE2:
				return zone2 != null;
		}
		return super.eIsSet(featureID);
	}

} //VirtualImpl
