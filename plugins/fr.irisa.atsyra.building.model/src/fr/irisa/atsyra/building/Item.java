/*******************************************************************************
 * Copyright (c) 2014, 2016 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Item</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getItem()
 * @model
 * @generated
 */
public interface Item extends NamedElement {
} // Item
