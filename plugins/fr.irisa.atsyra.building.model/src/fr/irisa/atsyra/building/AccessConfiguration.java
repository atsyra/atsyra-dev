/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.AccessConfiguration#getAccess <em>Access</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.AccessConfiguration#isIsOpen <em>Is Open</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.AccessConfiguration#isIsLocked <em>Is Locked</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getAccessConfiguration()
 * @model
 * @generated
 */
public interface AccessConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Access</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access</em>' reference.
	 * @see #setAccess(Access)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getAccessConfiguration_Access()
	 * @model keys="name" required="true"
	 * @generated
	 */
	Access getAccess();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.AccessConfiguration#getAccess <em>Access</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access</em>' reference.
	 * @see #getAccess()
	 * @generated
	 */
	void setAccess(Access value);

	/**
	 * Returns the value of the '<em><b>Is Open</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Open</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Open</em>' attribute.
	 * @see #setIsOpen(boolean)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getAccessConfiguration_IsOpen()
	 * @model
	 * @generated
	 */
	boolean isIsOpen();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.AccessConfiguration#isIsOpen <em>Is Open</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Open</em>' attribute.
	 * @see #isIsOpen()
	 * @generated
	 */
	void setIsOpen(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Locked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Locked</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Locked</em>' attribute.
	 * @see #setIsLocked(boolean)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getAccessConfiguration_IsLocked()
	 * @model
	 * @generated
	 */
	boolean isIsLocked();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.AccessConfiguration#isIsLocked <em>Is Locked</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Locked</em>' attribute.
	 * @see #isIsLocked()
	 * @generated
	 */
	void setIsLocked(boolean value);

} // AccessConfiguration
