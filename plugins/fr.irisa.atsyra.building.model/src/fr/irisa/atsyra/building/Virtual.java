/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Virtual</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.building.Virtual#getZone1 <em>Zone1</em>}</li>
 *   <li>{@link fr.irisa.atsyra.building.Virtual#getZone2 <em>Zone2</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.building.BuildingPackage#getVirtual()
 * @model
 * @generated
 */
public interface Virtual extends Access {
	/**
	 * Returns the value of the '<em><b>Zone1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Zone1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zone1</em>' reference.
	 * @see #setZone1(Zone)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getVirtual_Zone1()
	 * @model keys="name"
	 * @generated
	 */
	Zone getZone1();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.Virtual#getZone1 <em>Zone1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Zone1</em>' reference.
	 * @see #getZone1()
	 * @generated
	 */
	void setZone1(Zone value);

	/**
	 * Returns the value of the '<em><b>Zone2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Zone2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zone2</em>' reference.
	 * @see #setZone2(Zone)
	 * @see fr.irisa.atsyra.building.BuildingPackage#getVirtual_Zone2()
	 * @model keys="name"
	 * @generated
	 */
	Zone getZone2();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.building.Virtual#getZone2 <em>Zone2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Zone2</em>' reference.
	 * @see #getZone2()
	 * @generated
	 */
	void setZone2(Zone value);

} // Virtual
