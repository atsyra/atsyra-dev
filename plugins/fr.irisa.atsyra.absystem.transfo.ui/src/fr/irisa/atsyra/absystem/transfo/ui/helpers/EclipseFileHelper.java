/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.ui.helpers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

public class EclipseFileHelper {
	public static final String DEFAULT_GEN_FOLDER_NAME = "gen";
	
	private  EclipseFileHelper() {
	}
	
	public static void setupGenFolder(IFolder genFolder, IProgressMonitor monitor) throws CoreException, IOException {
		createFolderIfNeeded(genFolder, monitor.slice(1));
		addGitignore(genFolder, monitor.slice(1));
	}
	
	public static void createFolderIfNeeded(IFolder folder, IProgressMonitor monitor) throws CoreException {
		if (!folder.exists()) {
			folder.create(true, true, monitor);
		}
		monitor.done();
	}

	public static void createFileIfNeeded(IFile file, IProgressMonitor monitor) throws CoreException {
		if (!file.exists()) {
			file.create(InputStream.nullInputStream(), true, monitor);
		}
		monitor.done();
	}
	
	public static void addGitignore(IFolder folder, IProgressMonitor monitor) throws IOException {
		monitor.setTaskName(".gitignore");
		File file = new File(folder.getLocation().append(".gitignore").toOSString());
		FileUtils.writeStringToFile(file, "*", StandardCharsets.UTF_8);
		monitor.done();
	}
}
