/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.ui.handlers;

import java.util.logging.Logger;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;

import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.transfo.ui.helpers.ABS2ABSWorker;

public class ABS2ABSHandler extends AbstractABSGoalSelectHandler {
	static Logger log = Logger.getLogger(ABS2ABSHandler.class.getName());

	@Override
	public Object executeForSelectedGoal(ExecutionEvent event, IProject updatedGemocGoalProject, Goal goal)
			throws ExecutionException {
		IFile goalFile = getGoalIFile(event, goal);
		ABS2ABSWorker.abs2abs(goalFile, goal);
		return null;
	}

	@Override
	public String getSelectionMessage() {
		return "Select a goal";
	}
}
