/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.ui.helpers;

import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.transfo.ABS2ABS;
import fr.irisa.atsyra.absystem.transfo.ABS2ABSDefault;
import fr.irisa.atsyra.absystem.transfo.ABS2ABS.ABS2ABScontext;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandler;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandlerImpl;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotraceFactory;
import fr.irisa.atsyra.absystem.transfo.ui.Activator;

public class ABS2ABSWorker {
	public static Job abs2abs(IFile resource, Goal goal) {
		if (resource.getName().endsWith(".abs")) {
			final IFile goalFile = resource;
			try {

				Job job = new Job("Transforming ABS for file " + goalFile.getName() + " and goal " + goal.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						SubMonitor submonitor = SubMonitor.convert(monitor, "ABS2ABS", 100);
						Clock clock = Clock.systemDefaultZone();
						Instant start = clock.instant();
						IFolder genFolder = resource.getProject().getFolder(EclipseFileHelper.DEFAULT_GEN_FOLDER_NAME);
						try {
							EclipseFileHelper.setupGenFolder(genFolder, submonitor.split(1));
							GeneratedFilesHandler generatedFilesHandler = new GeneratedFilesHandlerImpl(genFolder, goalFile.getLocation().removeFileExtension().lastSegment() + "_" + goal.getName());
							ABS2ABScontext context = new ABS2ABScontext(generatedFilesHandler, goal,
									start, goal.eResource().getResourceSet());
							IFile traceabilityFile = genFolder
									.getFile(goalFile.getFullPath().removeFileExtension().lastSegment() + "_"
											+ goal.getName() + ".traceabilityMap.xmi");
							EclipseFileHelper.createFileIfNeeded(traceabilityFile, submonitor.split(1));
							ResourceSet rs = goal.eResource().getResourceSet();
							Resource traceResource = rs.createResource(URI.createPlatformResourceURI(traceabilityFile.getFullPath().toString(), false));
							TransfoTraceModel tracemodel = TransfotraceFactory.eINSTANCE.createTransfoTraceModel();
							traceResource.getContents().add(tracemodel);
							traceResource.save(null);
							ABS2ABS abs2abs = new ABS2ABSDefault();
							abs2abs.abs2abs(context, tracemodel, submonitor.split(98));
							Activator.eclipseImportant("ABS transformation completed.");
						} catch (IOException | CoreException e) {
							Activator.eclipseError("Error during abs2abs." , e);
						}
						Instant end = clock.instant();
						Duration duration = Duration.between(start, end);
						Activator.eclipseInfo("ABS2ABS execution time: "
										+ Long.toString(duration.getSeconds()) + " seconds " + duration.getNano() + " nanos.");
						monitor.done();
						return Status.OK_STATUS;
					}
				};
				job.setPriority(Job.LONG);
				// prevent concurrent job
				job.setRule(goalFile.getProject());
				job.schedule();
				return job;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
