/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.ui.handlers;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.gemoc.commons.eclipse.core.resources.FileFinderVisitor;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.ISources;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.outline.impl.EObjectNode;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;

import fr.irisa.atsyra.absreport.ABSReportRuntimeModule;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.transfo.ui.Activator;
import fr.irisa.atsyra.absystem.transfo.ui.dialogs.SelectAnyGoalDialog;

public abstract class AbstractABSGoalSelectHandler extends AbstractHandler {

	public abstract Object executeForSelectedGoal(ExecutionEvent event, IProject updatedGemocGoalProject, Goal goal)
			throws ExecutionException;

	public abstract String getSelectionMessage();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// get the optional selection and eventually project data to preset the
		// wizard
		final IProject updatedGemocGoalProject = getUpdatedGoalProjectFromSelection(event);

		Goal lang = getGoalFromSelection(event);
		if (lang != null && updatedGemocGoalProject != null) {
			// go for it, we have everything in the selection
			executeForSelectedGoal(event, updatedGemocGoalProject, lang);
			return null;
		}

		final IFile goalFile = getGoalFileFromSelection(event);
		Injector injector = Guice.createInjector(new ABSReportRuntimeModule());
		Provider<ResourceSet> resourceSetProvider = injector.getProvider(ResourceSet.class);
		final ResourceSet rs = resourceSetProvider.get();
		if (goalFile != null) {
			// we have only one Goal file in the selection
			final URI uri = URI.createPlatformResourceURI(goalFile.getFullPath().toString(), true);
			rs.getResource(uri, true);
		} else {
			// we will search for all .atg files in the project
			FileFinderVisitor atsyraProjectVisitor = new FileFinderVisitor("abs");
			try {
				if (updatedGemocGoalProject != null) {
					updatedGemocGoalProject.accept(atsyraProjectVisitor);
					for (IFile projectGoalIFile : atsyraProjectVisitor.getFiles()) {
						// consider all melange files in the project, get them in
						// the ResourceSet
						if (!(projectGoalIFile.getFullPath().toString().contains("/bin/")
								| projectGoalIFile.getFullPath().toString().contains("/target/"))) {
							// FIXME stupid check to remove some typical duplicates,
							// a better impl should look into java output

							final URI uri = URI.createPlatformResourceURI(projectGoalIFile.getFullPath().toString(),
									true);
							rs.getResource(uri, true);
						}
					}
				}
			} catch (CoreException e) {
				Activator.eclipseError(e.getMessage(), e);
			}
		}

		final LabelProvider labelProvider = new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Goal) {
					return ((Goal) element).getName();
				} else
					return super.getText(element);
			}
		};
		final SelectAnyGoalDialog dialog = new SelectAnyGoalDialog(rs, labelProvider);
		dialog.setTitle("Select a Goal");
		dialog.setMessage(getSelectionMessage());
		final int res = dialog.open();
		if (res == WizardDialog.OK) {
			lang = (Goal) dialog.getFirstResult();
			executeForSelectedGoal(event, updatedGemocGoalProject, lang);
		}

		return null;
	}

	/**
	 * Search for the IFile of the goal either via the event or directly from the
	 * eResource
	 */
	protected IFile getGoalIFile(ExecutionEvent event, Goal goal) {
		IFile atsyraGoalFile = getGoalFileFromSelection(event);
		if (atsyraGoalFile == null) {
			// this means that we have to retrieve the IFile from the goal instance (either
			// because
			// it comes from an editor of because we have selected one goal among other in
			// the project)
			atsyraGoalFile = EMFResource.getIFile(goal);
		}
		return atsyraGoalFile;
	}

	protected IFile getGoalFileFromSelection(ExecutionEvent event) {
		IFile selectedGoalIFile = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
			Iterator<Object> iterator = strucSelection.iterator(); iterator.hasNext();) {

				Object element = iterator.next();

				if (element instanceof IFile) {
					selectedGoalIFile = (IFile) element;

				}
				if (element instanceof IAdaptable) {
					IFile res = (IFile) ((IAdaptable) element).getAdapter(IFile.class);
					if (res != null) {
						selectedGoalIFile = res;
					}
				}
			}
		}
		return selectedGoalIFile;
	}

	protected Goal getGoalFromSelection(ExecutionEvent event) {
		Goal selectedGoal = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
			Iterator<Object> iterator = strucSelection.iterator(); iterator.hasNext();) {

				Object element = iterator.next();

				if (element instanceof Goal) {
					selectedGoal = (Goal) element;
				}
//				if (element instanceof GoalResult) {
//					selectedGoal = ((GoalResult) element).getGoal();
//				}
				if (element instanceof EObjectNode) {
					// try selection from xtexteditor outline
					final XtextEditor editor = org.eclipse.xtext.ui.editor.utils.EditorUtils
							.getActiveXtextEditor(event);
					final IUnitOfWork<Goal, XtextResource> _function = (XtextResource it) -> {
						if (((EObjectNode) element).getEObject(it) instanceof Goal) {
							return (Goal) ((EObjectNode) element).getEObject(it);
						} else {
							return null;
						}
					};
					final Goal goal = editor.getDocument().readOnly(_function);
					if (goal != null) {
						return goal;
					}
				}
				if (element instanceof IAdaptable) {
					Goal res = (Goal) ((IAdaptable) element).getAdapter(Goal.class);
					if (res != null) {
						selectedGoal = res;
					}
				}
			}
		} else {
			// try selection from xtexteditor
			final XtextEditor editor = org.eclipse.xtext.ui.editor.utils.EditorUtils.getActiveXtextEditor(event);
			if (editor != null) {
				final ITextSelection textSelection = (ITextSelection) editor.getSelectionProvider().getSelection();
				final IUnitOfWork<Goal, XtextResource> _function = (XtextResource it) -> {
					int _offset = textSelection.getOffset();
					return this.getSelectedGoal(it, _offset);
				};

				final Goal lang = editor.getDocument().readOnly(_function);
				if (lang != null) {
					return lang;
				}
			}
		}
		return selectedGoal;
	}

	protected Goal getSelectedGoal(XtextResource resource, int offset) {
		final EObjectAtOffsetHelper eObjectAtOffsetHelper = resource.getResourceServiceProvider()
				.get(EObjectAtOffsetHelper.class);
		EObject selectedElement = eObjectAtOffsetHelper.resolveContainedElementAt(resource, offset);
		if (selectedElement != null) {
			EObject currentElem = selectedElement;
			while (currentElem != null) {
				if (currentElem instanceof Goal) {
					return (Goal) currentElem;
				}
				currentElem = currentElem.eContainer();
			}
		}
		return null;
	}

	protected IProject getUpdatedGoalProjectFromSelection(ExecutionEvent event) {
		IProject updatedGemocLanguageProject = null;
		
		Object appContextObj = event.getApplicationContext( );
		if( appContextObj instanceof IEvaluationContext )
		{
			IEvaluationContext appContext = (IEvaluationContext)appContextObj;
			appContext.getVariable( ISources.ACTIVE_CURRENT_SELECTION_NAME);
			
		}
		
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
			Iterator<Object> iterator = strucSelection.iterator(); iterator.hasNext();) {

				Object element = iterator.next();

				if (element instanceof IResource) {
					updatedGemocLanguageProject = ((IResource) element).getProject();

				}
				if (element instanceof Goal) {
					IFile goalFile = EMFResource.getIFile((Goal) element);
					if (goalFile != null) {
						updatedGemocLanguageProject = goalFile.getProject();
					}
				}
//				if (element instanceof GoalResult ){
//					IFile goalFile = EMFResource.getIFile(((GoalResult)element).getGoal());
//					if(goalFile != null){
//						updatedGemocLanguageProject  = goalFile.getProject();
//					}
//				}
				if (element instanceof EObjectNode) {
					// this is an outline element of an xtext editor
					final XtextEditor editor = org.eclipse.xtext.ui.editor.utils.EditorUtils
							.getActiveXtextEditor(event);
					updatedGemocLanguageProject = editor.getResource().getProject();
				}
				if (element instanceof IAdaptable) {
					IResource res = (IResource) ((IAdaptable) element).getAdapter(IResource.class);
					if (res != null) {
						updatedGemocLanguageProject = res.getProject();
					}
				}
			}
		} else if (selection != null & selection instanceof ITextSelection) {
			IResource res = (IResource) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActiveEditor()
					.getEditorInput().getAdapter(IResource.class);
			if (res != null) {
				updatedGemocLanguageProject = res.getProject();
			}
		}
		return updatedGemocLanguageProject;
	}

	/**
	 * look for a resultstore file with the same name, in genFolder
	 * 
	 * @param URI
	 * @return
	 */
	public URI getResultStoreURI(IFile file) {
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder("gen");
		IFile resultStoreFile = file.getProject()
				.getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension + ".resultstore");

		URI resultStoreUri = URI.createPlatformResourceURI(resultStoreFile.getFullPath().toString(), true);
		if (!resultStoreFile.exists()) {
			// make sure the file exist,
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
			Map<String, Object> m = reg.getExtensionToFactoryMap();
			m.put("resultstore", new XMIResourceFactoryImpl());
			ResourceSet resSet = new ResourceSetImpl();
			Resource res = resSet.createResource(resultStoreUri);
			try {
				res.save(null);
			} catch (IOException e) {
				Activator.eclipseError("failed to save result in: " + resultStoreUri, e);
			}
		}
		return resultStoreUri;
	}

}
