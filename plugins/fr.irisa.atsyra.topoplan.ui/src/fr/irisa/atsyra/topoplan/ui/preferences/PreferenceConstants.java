/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.topoplan.ui.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_TOPOPLAN_EXE_PATH = "topoPlanExepathPreference";

	//public static final String P_BOOLEAN = "booleanPreference";

	//public static final String P_CHOICE = "choicePreference";

	//public static final String P_STRING = "stringPreference";
	
}
