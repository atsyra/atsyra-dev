/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.topoplan.ui.preferences;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import fr.irisa.atsyra.topoplan.ui.Activator;


/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		if(Platform.getBundle("fr.irisa.atsyra.topoplan.win32") != null){
				// TODO this code should be exposed by the fragment itself ...
				File topowin32exe = null;
				try {
					topowin32exe = new File(Platform.getBundle("fr.irisa.atsyra.topoplan.win32").getEntry("/binaries/win32-x86_67/TopoPlan4ATSyRA.exe").toURI());
				} catch (URISyntaxException e) {
					Activator.eclipseWarn("failed to find topoplan executable in fragment, falling back to default", e);
				}
				if(topowin32exe !=  null && topowin32exe.exists()){
					store.setDefault(PreferenceConstants.P_TOPOPLAN_EXE_PATH, topowin32exe.getAbsolutePath());
				} else {
					Activator.eclipseWarn("failed to find topoplan executable (/binaries/win32-x86_67/TopoPlan4ATSyRA.exe) in fragment (fr.irisa.atsyra.topoplan.win32), falling back to default", null);
					// use Env variable to get topoplan default path
					String topoplanPath = System.getenv().get("TopoPlanExe");
					store.setDefault(PreferenceConstants.P_TOPOPLAN_EXE_PATH, topoplanPath != null ? topoplanPath : "");
				}
		} else {
			// use Env variable to get topoplan default path
			String topoplanPath = System.getenv().get("TopoPlanExe");
			store.setDefault(PreferenceConstants.P_TOPOPLAN_EXE_PATH, topoplanPath != null ? topoplanPath : "");
		}
		//store.setDefault(PreferenceConstants.P_BOOLEAN, true);
		//store.setDefault(PreferenceConstants.P_CHOICE, "choice2");
		//store.setDefault(PreferenceConstants.P_STRING,
		//		"Default value");
	}

}
