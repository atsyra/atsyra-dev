/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package topoplanwizard;


import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

public class Page extends WizardPage {
	
    private FileChooser pathOgre;
    private FileChooser pathInfo;
    private Composite container;
    private Text nomProjet;

	protected Page() {
		super("Select your mesh.xml file");
        setTitle("Select your mesh.xml file");
        setDescription("");
        setControl(pathOgre);
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 1;
        Label labelP = new Label(container, SWT.NONE);
        labelP.setText("Project's name");
        nomProjet = new Text(container, SWT.BORDER);
        nomProjet.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (!nomProjet.getText().isEmpty() && pathOgre.isOk() && pathInfo.isOk()) {
                    setPageComplete(true);
                }
                else {
                	setPageComplete(false);
                }
            }
        });
        
        GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);
		gd2.grabExcessHorizontalSpace = true;
		gd2.horizontalAlignment = GridData.FILL;
		nomProjet.setLayoutData(gd2);
        
        
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("Select the mesh.xml file");

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
     		
     	pathOgre = new FileChooser(container);
     	pathOgre.setLayoutData(gd);
     
     	
     	
     	 Label label2 = new Label(container, SWT.NONE);
         label2.setText("Select the info file (optional)");

         GridData gd1 = new GridData(GridData.FILL_HORIZONTAL);
      		
      	pathInfo = new FileChooser(container);
      	pathInfo.setLayoutData(gd1);
      	pathInfo.ok = true;

     	Label labelError = new Label(container, SWT.NONE);
        labelError.setText("");
     		
     	pathOgre.mButton.addListener(SWT.Selection,  new Listener() {
		      public void handleEvent(Event event) {
		    	  
		    	  String extension = "";
		    	  String s = pathOgre.mText.getText();

		    	  int i = (s.substring(0, s.lastIndexOf('.'))).lastIndexOf('.');
		    	  if (i > 0) {
		    	      extension = s.substring(i+1);
		    	  }
		    	  
		    	  if(extension.equals("mesh.xml")) {
		    		  labelError.setText("");
		    		  if(nomProjet.getText().equals("") || nomProjet.getText() == null) {
		    			  nomProjet.setText(s.substring((s.replace("\\", "/")).lastIndexOf("/")+1, i));
		    		  }
			    	  if(pathInfo.isOk()) {
			    		  setPageComplete(true);
			    	  }
			    	  pathOgre.ok = true;
		    	  }
		    	  else {
		    		  labelError.setText("Le fichier sélectionné est incorrect. Sélectionnez un fichier ayant l'extension <<mesh.xml>>.");
		    		  container.layout();
		    		  pathOgre.ok = false;
		    		  setPageComplete(false);
		    	  }
		      }
		});
     	
     	
     	

        pathOgre.mText.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                setPageComplete(false);
                pathOgre.ok = false;
            }
        });
        
        
        
     	
     	
     	
     	pathInfo.mButton.addListener(SWT.Selection,  new Listener() {
		      public void handleEvent(Event event) {
		    	  
		    	  String extension = "";
		    	  String s = pathInfo.mText.getText();
		    	  if(s.equals("")) {
		    		  labelError.setText("");
			    	  if(!nomProjet.getText().equals("") && pathOgre.isOk()) {
			    		  setPageComplete(true);
			    	  }
			    	  pathInfo.ok = true;
		    	  }
		    	  else {
		    		  int i = s.lastIndexOf('.');
			    	  if (i > 0) {
			    	      extension = s.substring(i+1);
			    	  }
			    	  
			    	  if(extension.equals("info")) {
			    		  labelError.setText("");
				    	  if(!nomProjet.getText().equals("") && pathOgre.isOk()) {
				    		  setPageComplete(true);
				    	  }
				    	  pathInfo.ok = true;
			    	  }
			    	  else {
			    		  labelError.setText("Le fichier sélectionné est incorrect. Sélectionnez un fichier ayant l'extension <<info>>.");
			    		  container.layout();
			    		  pathInfo.ok = false;
			    		  setPageComplete(false);
			    	  }
		    	  }

		      }
		});
     	
     	
        pathInfo.mText.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (!nomProjet.getText().isEmpty() && pathOgre.isOk() && pathInfo.mText.getText().equals("")) {
                    setPageComplete(true);
                    pathInfo.ok = true;
                }
                else {
                	setPageComplete(false);
                }
            }
        });
     	
     	
     	
        setControl(container);
        setPageComplete(false);
	}
	
	public String getOgreFilePath(){
		return pathOgre.getText();
	}
	
	public String getInfoFilePath(){
		return pathInfo.getText();
	}
	
	public String getProjectName() {
		return nomProjet.getText();
	}

}
