/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package topoplanwizard;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import fr.irisa.atsyra.topoplan.ui.Activator;

public class TopoPlan extends Wizard implements INewWizard {
	
	protected Page p;

	public TopoPlan() {
		super();
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {

	}
	
    @Override
    public String getWindowTitle() {
        return "Import From TopoPlan";
    }

	
	 @Override
	 public void addPages() {
	     p = new Page();
	     addPage(p);
	 }

	@Override
	public boolean performFinish() {
		String inOgrePath = p.getOgreFilePath();
		inOgrePath = inOgrePath.replace("\\", "/");
		

		IProject proj = TopoPlanProject.createProject(p.getProjectName());

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		String s = root.getLocation().toString();

		String outPath = s + proj.getFullPath() + "/";
		String outOgrePath = outPath + inOgrePath.substring(inOgrePath.lastIndexOf("/")+1);
		
		
		
		String inInfoPath = p.getInfoFilePath();
		inInfoPath = inInfoPath.replace("\\", "/");
		String outInfoPath = "";
		
		if(!inInfoPath.equals("")) {
			outInfoPath = outPath + "/src-gen/";
		}
		
		
		try {
			Files.copy(Paths.get(inOgrePath), Paths.get(outOgrePath), StandardCopyOption.REPLACE_EXISTING);
			
			if(!inInfoPath.equals("")) {
				Files.createDirectories(Paths.get(outInfoPath));
				outInfoPath = outInfoPath + inInfoPath.substring(inInfoPath.lastIndexOf("/")+1);
				Files.copy(Paths.get(inInfoPath), Paths.get(outInfoPath));
			}
			else {
				Files.createFile(Paths.get(outPath + "/" + p.getProjectName() + ".tpi"));
				
				//On ouvre un flux d'écriture dans un fichier
		        FileOutputStream fos = new FileOutputStream(new File(outPath + "/" + p.getProjectName() + ".tpi"));
		         
		        //On stocke chaque caractére de la chaéne nom par case du tableau nomAEcrire
		        byte[] stringAMettre = "Infos for ".getBytes();
		         
		        String nomOgre = inOgrePath.substring(inOgrePath.lastIndexOf("/")+1); // nom+extension
		        nomOgre = nomOgre.substring(0, nomOgre.indexOf("."));
		        //Méme démarche que ci-dessus
		        byte[] nomDufichier = nomOgre.getBytes();
		         
		        //On écrit tout éa dans notre fichier
		        fos.write(stringAMettre);
		        fos.write(nomDufichier);
		 
		        fos.close() ;
			}
			
			
		} catch (IOException e) {
			Activator.eclipseError(e.getMessage(), e);
		}

		
		
		try {
			proj.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			Activator.eclipseError(e.getMessage(), e);
		}
	 
	    return true;

	}
}
