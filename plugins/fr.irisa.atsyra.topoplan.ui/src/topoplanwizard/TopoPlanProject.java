/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package topoplanwizard;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;

import fr.irisa.atsyra.topoplan.ui.Activator;


 
public class TopoPlanProject {

    public static IProject createProject(String projectName) {
 
        IProject project = createBaseProject(projectName);

        return project;
    }
 

    private static IProject createBaseProject(String projectName) {
        IProject newProject = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
       
 
        if (!newProject.exists()) {
            IProjectDescription desc = newProject.getWorkspace().newProjectDescription(newProject.getName());
 
            desc.setLocationURI(null);
            try {
                newProject.create(desc, null);
                if (!newProject.isOpen()) {
                    newProject.open(null);
                }
            } catch (CoreException e) {
            	Activator.eclipseError(e.getMessage(), e);
            }
        }
 
        return newProject;
    }
 
}