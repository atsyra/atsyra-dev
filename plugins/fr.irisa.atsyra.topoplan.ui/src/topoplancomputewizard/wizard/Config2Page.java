/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package topoplancomputewizard.wizard;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.irisa.atsyra.topoplan.ui.Activator;

public class Config2Page extends WizardPage {
	
    private Composite container;
    public Text humanoRadius;
    public Text flatHeight;
    public Text height;
    public Text stepHeight;
    public Text stepLength;
    public Text angle;
    public String tpcfile;

	protected Config2Page(String tpc) {
		super("Configuration");
        setTitle("Configuration (2/2)");
        setDescription("");
        tpcfile = tpc;
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		setControl(container);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 2;
        
        
        //precision
        Label labelP = new Label(container, SWT.NONE);
        labelP.setText("humanoRadius");
        humanoRadius = new Text(container, SWT.BORDER);
        
        GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);
		gd2.grabExcessHorizontalSpace = true;
		gd2.horizontalAlignment = GridData.FILL;
		humanoRadius.setLayoutData(gd2);
		humanoRadius.setText("0.35");
		
		
		humanoRadius.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkComplete();
            }
        });
        
        
        
        
        //zprecision
        Label labelz = new Label(container, SWT.NONE);
        labelz.setText("flat height");
        flatHeight = new Text(container, SWT.BORDER);
        
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;
		flatHeight.setLayoutData(gd);
		flatHeight.setText("0.01");
		
		
		flatHeight.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkComplete();
            }
        });
        
        
        
        
        
        
      //epsilonWallProjection
        Label labelew = new Label(container, SWT.NONE);
        labelew.setText("height");
        height = new Text(container, SWT.BORDER);
        
        GridData gd1 = new GridData(GridData.FILL_HORIZONTAL);
		gd1.grabExcessHorizontalSpace = true;
		gd1.horizontalAlignment = GridData.FILL;
		height.setLayoutData(gd1);
		height.setText("1.75");
		
		
		height.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkComplete();
            }
        });
		
		
		
		
		
		
        //precision
        Label labelsh = new Label(container, SWT.NONE);
        labelsh.setText("stepHeight");
        stepHeight = new Text(container, SWT.BORDER);
        
        GridData gd3 = new GridData(GridData.FILL_HORIZONTAL);
		gd3.grabExcessHorizontalSpace = true;
		gd3.horizontalAlignment = GridData.FILL;
		stepHeight.setLayoutData(gd3);
		stepHeight.setText("0.41");
		
		
		stepHeight.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkComplete();
            }
        });
		
		
		
        //precision
        Label labelsl = new Label(container, SWT.NONE);
        labelsl.setText("steplength");
        stepLength = new Text(container, SWT.BORDER);
        
        GridData gd4 = new GridData(GridData.FILL_HORIZONTAL);
		gd4.grabExcessHorizontalSpace = true;
		gd4.horizontalAlignment = GridData.FILL;
		stepLength.setLayoutData(gd4);
		stepLength.setText("0.8");
		
		
		stepLength.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkComplete();
            }
        });
		
		
		
        //precision
        Label labela = new Label(container, SWT.NONE);
        labela.setText("angle");
        angle = new Text(container, SWT.BORDER);
        
        GridData gd5 = new GridData(GridData.FILL_HORIZONTAL);
		gd5.grabExcessHorizontalSpace = true;
		gd5.horizontalAlignment = GridData.FILL;
		angle.setLayoutData(gd5);
		angle.setText("45");
		
		
		angle.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkComplete();
            }
        });
		
		
		
		
		//bouton pour reset les valeurs
		Button reset = new Button(container, SWT.PUSH);
		reset.setText("Reset configurations");
		reset.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Reset on page 1");
			    humanoRadius.setText("0.35");
			    flatHeight.setText("0.01");
			    height.setText("1.75");
			    stepHeight.setText("0.41");
			    stepLength.setText("0.8");
			    angle.setText("45");
				
			}});
		
		
		
		if(tpcfile != "" && new File(tpcfile).exists()) {
			BufferedReader br;
			try {
				br = new BufferedReader(new FileReader(tpcfile));
				String line;
				int cmp = 0;
				while ((line = br.readLine()) != null && cmp < 9) {
				   switch (cmp) {
				   		case 3:
				   			humanoRadius.setText(line);
				   			break;
				   		case 4:
				   			flatHeight.setText(line);
				   			break;
				   		case 5:
				   			height.setText(line);
				   			break;
				   		case 6:
				   			stepHeight.setText(line);
				   			break;
				   		case 7:
				   			stepLength.setText(line);
				   			break;
				   		case 8:
				   			angle.setText(line);
				   			break;
				   		default:
				   			break;
					   
				   }
				   cmp++;
				}
				br.close();
			} catch (FileNotFoundException e) {
				Activator.eclipseError(e.getMessage(), e);
			} catch (IOException e) {
				Activator.eclipseError(e.getMessage(), e);
			}
		}
		
		
		
	}
		
		
	
	public void checkComplete() {
		if(humanoRadius.getText() != "" && flatHeight.getText() != "" && height.getText() != "" && stepHeight.getText() != "" && stepLength.getText() != "" && angle.getText() != "") {
			try {
				Float.parseFloat(humanoRadius.getText());
				Float.parseFloat(flatHeight.getText());
				Float.parseFloat(height.getText());
				Float.parseFloat(stepHeight.getText());
				Float.parseFloat(stepLength.getText());
				Float.parseFloat(angle.getText());
				setPageComplete(true);
			}
			catch(Exception e) {
				setPageComplete(false);
			}
		}
		else {
			setPageComplete(false);
		}
	}

}
