/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package topoplancomputewizard.wizard;


import java.io.File;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class Page extends WizardPage {
	
    private Composite container;
    private boolean hasInfo = false;
    private String infoFileName = "";

	protected Page() {
		super("Choose your info file");
        setTitle("Which info file do you want to use ?");
        setDescription("");
	}

	@Override
	public void createControl(Composite parent) {
		((WizardDialog) this.getWizard().getContainer()).setMinimumPageSize(300, 400);
		container = new Composite(parent, SWT.NONE);
		setControl(container);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 2;
        
        //radio "aucun fichier info"
        Label labelP = new Label(container, SWT.NONE);
        labelP.setText("No info file");
        Button radioButtonNI = new Button( container, SWT.RADIO );
        radioButtonNI.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseUp(MouseEvent e) {
				hasInfo = false;
				infoFileName = "";
			}

        });
        
        
        
        
        
        
        
      //radios pour chaque fichier info


		String outPath = ((PickInfoFileWizard)getWizard()).ogrefile.getLocation().removeLastSegments(1) + "/src-gen/";
        System.out.println(outPath);
        
        File folder = new File(outPath);
        File[] listOfFiles = folder.listFiles();
        if(listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length; i++) {
                
                if (listOfFiles[i].isFile() && listOfFiles[i].getName().substring(listOfFiles[i].getName().indexOf(".")+1).equals("info")) {
                  System.out.println("File " + listOfFiles[i].getName());
                  String filename = listOfFiles[i].getName();
                  
                  
                  Label label = new Label(container, SWT.NONE);
                  label.setText(listOfFiles[i].getName());
                  Button radioButton = new Button( container, SWT.RADIO );
                  radioButton.addMouseListener(new MouseListener() {

          			@Override
          			public void mouseDoubleClick(MouseEvent e) {
          				// TODO Auto-generated method stub
          				
          			}

          			@Override
          			public void mouseDown(MouseEvent e) {
          				// TODO Auto-generated method stub
          				
          			}

          			@Override
          			public void mouseUp(MouseEvent e) {
          				hasInfo = true;
          				infoFileName = outPath + filename;
          			}

                  });
                  
                  
                  
                  
                  
                  
                }
              }
        }



	}

	public String getInfoFileName() {
		return infoFileName;
	}
	
	public boolean getHasInfo() {
		return hasInfo;
	}
}
