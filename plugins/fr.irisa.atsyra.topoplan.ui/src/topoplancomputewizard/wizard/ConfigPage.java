/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package topoplancomputewizard.wizard;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.irisa.atsyra.topoplan.ui.Activator;

public class ConfigPage extends WizardPage {
	
    private Composite container;
    public Text precision;
    public Text zprecision;
    public Text epsilonWallProjection;
    public String tpcfile;

	protected ConfigPage(String tpc) {
		super("Configuration");
        setTitle("Configuration (1/2)");
        setDescription("");
        tpcfile = tpc;
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		setControl(container);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 2;
        
        
        //precision
        Label labelP = new Label(container, SWT.NONE);
        labelP.setText("Précision");
        precision = new Text(container, SWT.BORDER);
        
        GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);
		gd2.grabExcessHorizontalSpace = true;
		gd2.horizontalAlignment = GridData.FILL;
		precision.setLayoutData(gd2);
		precision.setText("0.01");
		
		
        precision.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkComplete();
            }
        });
        
        
        
        
        //zprecision
        Label labelz = new Label(container, SWT.NONE);
        labelz.setText("Précision Z");
        zprecision = new Text(container, SWT.BORDER);
        
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;
		zprecision.setLayoutData(gd);
		zprecision.setText("0.005");
		
		
        zprecision.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkComplete();
            }
        });
        
        
        
        
        
        
      //epsilonWallProjection
        Label labelew = new Label(container, SWT.NONE);
        labelew.setText("epsilonWallProjection");
        epsilonWallProjection = new Text(container, SWT.BORDER);
        
        GridData gd1 = new GridData(GridData.FILL_HORIZONTAL);
		gd1.grabExcessHorizontalSpace = true;
		gd1.horizontalAlignment = GridData.FILL;
		epsilonWallProjection.setLayoutData(gd1);
		epsilonWallProjection.setText("0.001");
		
		
		epsilonWallProjection.addKeyListener(new KeyListener() {
        	

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkComplete();
            }
        });
		
		
		//bouton pour reset les valeurs
		Button reset = new Button(container, SWT.PUSH);
		reset.setText("Reset configurations");
		reset.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Reset on page 1");
			   precision.setText("0.01");
			   zprecision.setText("0.005");
			   epsilonWallProjection.setText("0.001");
				
			}});		
		if(tpcfile != ""  && new File(tpcfile).exists()) {
			BufferedReader br;
			try {
				br = new BufferedReader(new FileReader(tpcfile));
				String line;
				int cmp = 0;
				while ((line = br.readLine()) != null && cmp < 3) {
				   switch (cmp) {
				   		case 0:
				   			precision.setText(line);
				   			break;
				   		case 1:
				   			zprecision.setText(line);
				   			break;
				   		case 2:
				   			epsilonWallProjection.setText(line);
				   			break;
				   		default:
				   			break;
					   
				   }
				   cmp++;
				}
				br.close();
			} catch (FileNotFoundException e) {
				Activator.eclipseError(e.getMessage(), e);
			} catch (IOException e) {
				Activator.eclipseError(e.getMessage(), e);
			}
		}
	}
	
	public void checkComplete() {
		if(precision.getText() != "" && zprecision.getText() != "" && epsilonWallProjection.getText() != "") {
			try {
				Float.parseFloat(precision.getText());
				Float.parseFloat(zprecision.getText());
				Float.parseFloat(epsilonWallProjection.getText());
				setPageComplete(true);
			}
			catch(Exception e) {
				setPageComplete(false);
			}
		}
		else {
			setPageComplete(false);
		}
	}

}
