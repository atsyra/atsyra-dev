/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package topoplancomputewizard.wizard;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import fr.irisa.atsyra.topoplan.ui.Activator;
import fr.irisa.atsyra.topoplan.ui.preferences.PreferenceConstants;

public class PickInfoFileWizard extends Wizard implements INewWizard {


    private Page p;
    private ConfigPage cp;
    private Config2Page c2p;
    public IFile ogrefile;


    // constructor
    public PickInfoFileWizard() {
        super();
        setWindowTitle("Choose your info file");
    }

    @Override
    public boolean performCancel() {

        return true;
    }

    @Override
    public void addPages() {
        super.addPages();
        p = new Page();
        addPage(p);
        cp = new ConfigPage(ogrefile.getLocation().toString().substring(0, ogrefile.getLocation().toString().lastIndexOf(".mesh.xml")) + ".tpc");
        addPage(cp);
        c2p = new Config2Page(ogrefile.getLocation().toString().substring(0, ogrefile.getLocation().toString().lastIndexOf(".mesh.xml")) + ".tpc");
        addPage(c2p);
    }

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean performFinish() {
		String ogrePath = ogrefile.getLocation().toString();
		String infoPath = p.getInfoFileName();
		String buildingPath = ogrefile.getLocation().toString().substring(0, ogrefile.getLocation().toString().indexOf(".mesh.xml")) + ".building";		
		String topoplanPath = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.P_TOPOPLAN_EXE_PATH);
		
		
		//sauvegarde des configurations utilisées
		 BufferedWriter bw;
		try {
			 bw = new BufferedWriter(new FileWriter(ogrefile.getLocation().toString().substring(0, ogrefile.getLocation().toString().lastIndexOf(".mesh.xml")) + ".tpc"));
			 PrintWriter pWriter = new PrintWriter(bw);
		     pWriter.println(cp.precision.getText());
		     pWriter.println(cp.zprecision.getText());
		     pWriter.println(cp.epsilonWallProjection.getText());
		     pWriter.println(c2p.humanoRadius.getText());
		     pWriter.println(c2p.flatHeight.getText());
		     pWriter.println(c2p.height.getText());
		     pWriter.println(c2p.stepHeight.getText());
		     pWriter.println(c2p.stepLength.getText());
		     pWriter.println(c2p.angle.getText());
		     pWriter.close() ;
		} catch (IOException e) {
			Activator.eclipseError("Echec de l'enregistrement des configurations", e);
			System.err.println("Echec de l'enregistrement des configurations");
		}
	    
		//appel de topoplan		
		Runtime runtime = Runtime.getRuntime();
		 String [] fcommand;
		if(!infoPath.equals("")) {
			String[] command = {topoplanPath, "-o", ogrePath, "-i", infoPath, "-b", buildingPath, "-p", cp.precision.getText(), "-zp", cp.zprecision.getText(), "-ewp", cp.epsilonWallProjection.getText(), "-hr", c2p.humanoRadius.getText(), "-fh", c2p.flatHeight.getText(), "-h", c2p.height.getText(), "-sh", c2p.stepHeight.getText(), "-sl", c2p.stepLength.getText(), "-a", c2p.angle.getText()};
			fcommand = command;
			System.out.println(infoPath);
		}
		else {
			String[] command = {topoplanPath, "-o", ogrePath, "-ni", "-b", buildingPath, "-p", cp.precision.getText(), "-zp", cp.zprecision.getText(), "-ewp", cp.epsilonWallProjection.getText(), "-hr", c2p.humanoRadius.getText(), "-fh", c2p.flatHeight.getText(), "-h", c2p.height.getText(), "-sh", c2p.stepHeight.getText(), "-sl", c2p.stepLength.getText(), "-a", c2p.angle.getText()};
			fcommand = command;
		}
		Activator.debug("TopoPlan called with the following command");
		Activator.debug(Arrays.toString(fcommand).replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", ""));
		
		
		try {
			final Process process = runtime.exec(fcommand);
			
			new Thread() {
				public void run() {
					try {
						BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
						String line = "";
						try {
							while((line = reader.readLine()) != null) {
								// Traitement du flux de sortie de l'application si besoin est
								Activator.debug(line);
							}
						} finally {
							reader.close();
						}
					} catch(IOException ioe) {
						Activator.eclipseError(ioe.getMessage(), ioe);						
					}
				}
			}.start();

			// Consommation de la sortie d'erreur de l'application externe dans un Thread separe
			new Thread() {
				public void run() {
					try {
						BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
						String line = "";
						try {
							while((line = reader.readLine()) != null) {
								// Traitement du flux d'erreur de l'application si besoin est
								Activator.debug(line);
							}
						} finally {
							reader.close();
						}
					} catch(IOException ioe) {
						Activator.eclipseError(ioe.getMessage(), ioe);
					}
				}
			}.start();
			
			
			process.waitFor();
			int exitStatus = process.exitValue();
			if(exitStatus != 0) {
				MessageDialog.openError(new Shell(), "TopoPlanError", "Erreur lors de l'appel à TopoPlan. Vérifiez vos configurations. \nError code: "+exitStatus+
						"\nTry to run the topoplan command directly in the system console to have better messages.");
			}
			
		} catch (IOException e) {
			Activator.eclipseError(e.getMessage(), e);
			MessageDialog.openError(new Shell(), "TopoPlanError", "Erreur lors de l'appel à TopoPlan. Vérifiez vos configurations.");
		} catch (InterruptedException e) {
			Activator.eclipseError(e.getMessage(), e);
		}
		
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IWorkbenchPage activePage = window.getActivePage();
		IProject project;
		IEditorPart activeEditor = activePage.getActiveEditor();

		if (activeEditor != null) {
		   IEditorInput input = activeEditor.getEditorInput();

		   project = input.getAdapter(IProject.class);
		   if (project == null) {
		      IResource resource = input.getAdapter(IResource.class);
		      if (resource != null) {
		         project = resource.getProject();
		      }
		   }
		   try {
				project.refreshLocal(IResource.DEPTH_INFINITE, null);
			} catch (CoreException e) {
				Activator.eclipseError(e.getMessage(), e);
			}
		}
		else {
			ISelection iselection = window.getSelectionService().getSelection();
	        IStructuredSelection selection = (IStructuredSelection) iselection;

	        Object firstElement = selection.getFirstElement();
	        if (firstElement instanceof IResource) {
	            project = ((IResource) firstElement).getProject();
				   try {
						project.refreshLocal(IResource.DEPTH_INFINITE, null);
					} catch (CoreException e) {
						Activator.eclipseError(e.getMessage(), e);
					}
	        } 
	        /*else if (firstElement instanceof PackageFragmentRoot) {
	            IJavaProject jProject = ((PackageFragmentRoot) firstElement)
	                    .getJavaProject();
	            project = jProject.getProject();
	        } else if (firstElement instanceof IJavaElement) {
	            IJavaProject jProject = ((IJavaElement) firstElement)
	                    .getJavaProject();
	            project = jProject.getProject();
	        }*/

		}
		
		
	 
	    return true;
	}

}