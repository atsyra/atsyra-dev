/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package replacewizard;


	import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.swing.JOptionPane;

import org.eclipse.core.resources.IFile;
	import org.eclipse.core.resources.IProject;
	import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
	import org.eclipse.jface.viewers.ISelection;
	import org.eclipse.jface.viewers.IStructuredSelection;
	import org.eclipse.jface.wizard.Wizard;
	import org.eclipse.ui.IEditorInput;
	import org.eclipse.ui.IEditorPart;
	import org.eclipse.ui.INewWizard;
	import org.eclipse.ui.IWorkbench;
	import org.eclipse.ui.IWorkbenchPage;
	import org.eclipse.ui.IWorkbenchWindow;
	import org.eclipse.ui.PlatformUI;

import fr.irisa.atsyra.topoplan.ui.Activator;

	public class PickOgreFileWizard extends Wizard implements INewWizard {


	    private Page p;
	    public IFile ogrefile;


	    // constructor
	    public PickOgreFileWizard() {
	        super();
	        setWindowTitle("Choose your mesh.xml file");
	    }

	    @Override
	    public boolean performCancel() {

	        return true;
	    }

	    @Override
	    public void addPages() {
	        super.addPages();
	        p = new Page();
	        addPage(p);
	    }

		@Override
		public void init(IWorkbench workbench, IStructuredSelection selection) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean performFinish() {
			String ogrePath = ogrefile.getLocation().toString();
			String newOgrePath = p.getOgreFilePath();
			
			//remplacer ogrefile par newOgrefile
			try {
				Files.copy(Paths.get(newOgrePath), Paths.get(ogrePath), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null,"Erreur lors du changement de fichier.");
			}
			
			
			
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			IWorkbenchPage activePage = window.getActivePage();
			IProject project;
			IEditorPart activeEditor = activePage.getActiveEditor();

			if (activeEditor != null) {
			   IEditorInput input = activeEditor.getEditorInput();

			   project = input.getAdapter(IProject.class);
			   if (project == null) {
			      IResource resource = input.getAdapter(IResource.class);
			      if (resource != null) {
			         project = resource.getProject();
			      }
			   }
			   try {
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
				} catch (CoreException e) {
					Activator.eclipseError(e.getMessage(), e);
				}
			}
			else {
				ISelection iselection = window.getSelectionService().getSelection();
		        IStructuredSelection selection = (IStructuredSelection) iselection;

		        Object firstElement = selection.getFirstElement();
		        if (firstElement instanceof IResource) {
		            project = ((IResource) firstElement).getProject();
					   try {
							project.refreshLocal(IResource.DEPTH_INFINITE, null);
						} catch (CoreException e) {
							Activator.eclipseError(e.getMessage(), e);
						}
		        } 
			}
			
			
		 
		    return true;
		}

	}