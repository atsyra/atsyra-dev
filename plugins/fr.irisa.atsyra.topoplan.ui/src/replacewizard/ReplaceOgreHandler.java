/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package replacewizard;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ReplaceOgreHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
	    if (window != null)
	    {
	        IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
	        Object firstElement = selection.getFirstElement();
	        IFile file = (IFile) Platform.getAdapterManager().getAdapter(firstElement,
                    IFile.class);
            if (file == null) {
                if (firstElement instanceof IAdaptable) {
                    file = (IFile) ((IAdaptable) firstElement).getAdapter(IFile.class);
                }
            }
            if (file != null) {
               System.out.println(file.getLocation());
            }
	   
            Shell activeShell = HandlerUtil.getActiveShell(event);

  		  PickOgreFileWizard wizard = new PickOgreFileWizard();
  		 wizard.ogrefile = file;

  		  WizardDialog dialog = new WizardDialog(activeShell, wizard);

  		  dialog.open();
	    }
	    
	    
	    
	    
		
		  
		return null;
	}
}
