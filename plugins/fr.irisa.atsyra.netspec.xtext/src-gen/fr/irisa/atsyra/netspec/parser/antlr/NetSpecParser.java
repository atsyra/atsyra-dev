/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/*
 * generated by Xtext 2.10.0
 */
package fr.irisa.atsyra.netspec.parser.antlr;

import com.google.inject.Inject;
import fr.irisa.atsyra.netspec.parser.antlr.internal.InternalNetSpecParser;
import fr.irisa.atsyra.netspec.services.NetSpecGrammarAccess;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;

public class NetSpecParser extends AbstractAntlrParser {

	@Inject
	private NetSpecGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalNetSpecParser createParser(XtextTokenStream stream) {
		return new InternalNetSpecParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "Model";
	}

	public NetSpecGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(NetSpecGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
