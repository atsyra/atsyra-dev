/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.netspec.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.atsyra.netspec.services.NetSpecGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalNetSpecParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Attacker'", "'{'", "'hackLevel'", "'None'", "'Low'", "'Medium'", "'High'", "'cryptoLevel'", "'virusLevel'", "'loginLevel'", "'LoginsKnown'", "'KeysKnown'", "'HostsControlled'", "'}'", "'Goal'", "'Datas'", "'Files'", "'Hosts'", "'CanTriggerIds'", "'Host'", "'Sessions'", "'Root'", "'IDS'", "'Firewall'", "'Antivirus'", "'Session'", "'Login'", "'Level'", "'Perfect'", "'File'", "'Key'", "'ContainsLogins'", "'ContainsKeys'", "'Data'", "'Active'", "'Link'", "'H1'", "'H2'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalNetSpecParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalNetSpecParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalNetSpecParser.tokenNames; }
    public String getGrammarFileName() { return "InternalNetSpec.g"; }



     	private NetSpecGrammarAccess grammarAccess;

        public InternalNetSpecParser(TokenStream input, NetSpecGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected NetSpecGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalNetSpec.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalNetSpec.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalNetSpec.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalNetSpec.g:71:1: ruleModel returns [EObject current=null] : ( ( (lv_attacker_0_0= ruleAttacker ) ) ( (lv_goal_1_0= ruleGoal ) ) ( ( (lv_hosts_2_0= ruleHost ) ) | ( (lv_sessions_3_0= ruleSession ) ) | ( (lv_files_4_0= ruleFile ) ) | ( (lv_logins_5_0= ruleLogin ) ) | ( (lv_keys_6_0= ruleKey ) ) | ( (lv_firewalls_7_0= ruleFirewall ) ) | ( (lv_ids_8_0= ruleIds ) ) | ( (lv_links_9_0= ruleLink ) ) | ( (lv_antivirus_10_0= ruleAntivirus ) ) | ( (lv_datas_11_0= ruleData ) ) )* ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_attacker_0_0 = null;

        EObject lv_goal_1_0 = null;

        EObject lv_hosts_2_0 = null;

        EObject lv_sessions_3_0 = null;

        EObject lv_files_4_0 = null;

        EObject lv_logins_5_0 = null;

        EObject lv_keys_6_0 = null;

        EObject lv_firewalls_7_0 = null;

        EObject lv_ids_8_0 = null;

        EObject lv_links_9_0 = null;

        EObject lv_antivirus_10_0 = null;

        EObject lv_datas_11_0 = null;



        	enterRule();

        try {
            // InternalNetSpec.g:77:2: ( ( ( (lv_attacker_0_0= ruleAttacker ) ) ( (lv_goal_1_0= ruleGoal ) ) ( ( (lv_hosts_2_0= ruleHost ) ) | ( (lv_sessions_3_0= ruleSession ) ) | ( (lv_files_4_0= ruleFile ) ) | ( (lv_logins_5_0= ruleLogin ) ) | ( (lv_keys_6_0= ruleKey ) ) | ( (lv_firewalls_7_0= ruleFirewall ) ) | ( (lv_ids_8_0= ruleIds ) ) | ( (lv_links_9_0= ruleLink ) ) | ( (lv_antivirus_10_0= ruleAntivirus ) ) | ( (lv_datas_11_0= ruleData ) ) )* ) )
            // InternalNetSpec.g:78:2: ( ( (lv_attacker_0_0= ruleAttacker ) ) ( (lv_goal_1_0= ruleGoal ) ) ( ( (lv_hosts_2_0= ruleHost ) ) | ( (lv_sessions_3_0= ruleSession ) ) | ( (lv_files_4_0= ruleFile ) ) | ( (lv_logins_5_0= ruleLogin ) ) | ( (lv_keys_6_0= ruleKey ) ) | ( (lv_firewalls_7_0= ruleFirewall ) ) | ( (lv_ids_8_0= ruleIds ) ) | ( (lv_links_9_0= ruleLink ) ) | ( (lv_antivirus_10_0= ruleAntivirus ) ) | ( (lv_datas_11_0= ruleData ) ) )* )
            {
            // InternalNetSpec.g:78:2: ( ( (lv_attacker_0_0= ruleAttacker ) ) ( (lv_goal_1_0= ruleGoal ) ) ( ( (lv_hosts_2_0= ruleHost ) ) | ( (lv_sessions_3_0= ruleSession ) ) | ( (lv_files_4_0= ruleFile ) ) | ( (lv_logins_5_0= ruleLogin ) ) | ( (lv_keys_6_0= ruleKey ) ) | ( (lv_firewalls_7_0= ruleFirewall ) ) | ( (lv_ids_8_0= ruleIds ) ) | ( (lv_links_9_0= ruleLink ) ) | ( (lv_antivirus_10_0= ruleAntivirus ) ) | ( (lv_datas_11_0= ruleData ) ) )* )
            // InternalNetSpec.g:79:3: ( (lv_attacker_0_0= ruleAttacker ) ) ( (lv_goal_1_0= ruleGoal ) ) ( ( (lv_hosts_2_0= ruleHost ) ) | ( (lv_sessions_3_0= ruleSession ) ) | ( (lv_files_4_0= ruleFile ) ) | ( (lv_logins_5_0= ruleLogin ) ) | ( (lv_keys_6_0= ruleKey ) ) | ( (lv_firewalls_7_0= ruleFirewall ) ) | ( (lv_ids_8_0= ruleIds ) ) | ( (lv_links_9_0= ruleLink ) ) | ( (lv_antivirus_10_0= ruleAntivirus ) ) | ( (lv_datas_11_0= ruleData ) ) )*
            {
            // InternalNetSpec.g:79:3: ( (lv_attacker_0_0= ruleAttacker ) )
            // InternalNetSpec.g:80:4: (lv_attacker_0_0= ruleAttacker )
            {
            // InternalNetSpec.g:80:4: (lv_attacker_0_0= ruleAttacker )
            // InternalNetSpec.g:81:5: lv_attacker_0_0= ruleAttacker
            {

            					newCompositeNode(grammarAccess.getModelAccess().getAttackerAttackerParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_3);
            lv_attacker_0_0=ruleAttacker();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModelRule());
            					}
            					set(
            						current,
            						"attacker",
            						lv_attacker_0_0,
            						"fr.irisa.atsyra.netspec.NetSpec.Attacker");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalNetSpec.g:98:3: ( (lv_goal_1_0= ruleGoal ) )
            // InternalNetSpec.g:99:4: (lv_goal_1_0= ruleGoal )
            {
            // InternalNetSpec.g:99:4: (lv_goal_1_0= ruleGoal )
            // InternalNetSpec.g:100:5: lv_goal_1_0= ruleGoal
            {

            					newCompositeNode(grammarAccess.getModelAccess().getGoalGoalParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_goal_1_0=ruleGoal();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModelRule());
            					}
            					set(
            						current,
            						"goal",
            						lv_goal_1_0,
            						"fr.irisa.atsyra.netspec.NetSpec.Goal");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalNetSpec.g:117:3: ( ( (lv_hosts_2_0= ruleHost ) ) | ( (lv_sessions_3_0= ruleSession ) ) | ( (lv_files_4_0= ruleFile ) ) | ( (lv_logins_5_0= ruleLogin ) ) | ( (lv_keys_6_0= ruleKey ) ) | ( (lv_firewalls_7_0= ruleFirewall ) ) | ( (lv_ids_8_0= ruleIds ) ) | ( (lv_links_9_0= ruleLink ) ) | ( (lv_antivirus_10_0= ruleAntivirus ) ) | ( (lv_datas_11_0= ruleData ) ) )*
            loop1:
            do {
                int alt1=11;
                switch ( input.LA(1) ) {
                case 30:
                    {
                    alt1=1;
                    }
                    break;
                case 36:
                    {
                    alt1=2;
                    }
                    break;
                case 40:
                    {
                    alt1=3;
                    }
                    break;
                case 37:
                    {
                    alt1=4;
                    }
                    break;
                case 41:
                    {
                    alt1=5;
                    }
                    break;
                case 34:
                    {
                    alt1=6;
                    }
                    break;
                case 33:
                    {
                    alt1=7;
                    }
                    break;
                case 46:
                    {
                    alt1=8;
                    }
                    break;
                case 35:
                    {
                    alt1=9;
                    }
                    break;
                case 44:
                    {
                    alt1=10;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // InternalNetSpec.g:118:4: ( (lv_hosts_2_0= ruleHost ) )
            	    {
            	    // InternalNetSpec.g:118:4: ( (lv_hosts_2_0= ruleHost ) )
            	    // InternalNetSpec.g:119:5: (lv_hosts_2_0= ruleHost )
            	    {
            	    // InternalNetSpec.g:119:5: (lv_hosts_2_0= ruleHost )
            	    // InternalNetSpec.g:120:6: lv_hosts_2_0= ruleHost
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getHostsHostParserRuleCall_2_0_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_hosts_2_0=ruleHost();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"hosts",
            	    							lv_hosts_2_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.Host");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalNetSpec.g:138:4: ( (lv_sessions_3_0= ruleSession ) )
            	    {
            	    // InternalNetSpec.g:138:4: ( (lv_sessions_3_0= ruleSession ) )
            	    // InternalNetSpec.g:139:5: (lv_sessions_3_0= ruleSession )
            	    {
            	    // InternalNetSpec.g:139:5: (lv_sessions_3_0= ruleSession )
            	    // InternalNetSpec.g:140:6: lv_sessions_3_0= ruleSession
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getSessionsSessionParserRuleCall_2_1_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_sessions_3_0=ruleSession();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"sessions",
            	    							lv_sessions_3_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.Session");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalNetSpec.g:158:4: ( (lv_files_4_0= ruleFile ) )
            	    {
            	    // InternalNetSpec.g:158:4: ( (lv_files_4_0= ruleFile ) )
            	    // InternalNetSpec.g:159:5: (lv_files_4_0= ruleFile )
            	    {
            	    // InternalNetSpec.g:159:5: (lv_files_4_0= ruleFile )
            	    // InternalNetSpec.g:160:6: lv_files_4_0= ruleFile
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getFilesFileParserRuleCall_2_2_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_files_4_0=ruleFile();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"files",
            	    							lv_files_4_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.File");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalNetSpec.g:178:4: ( (lv_logins_5_0= ruleLogin ) )
            	    {
            	    // InternalNetSpec.g:178:4: ( (lv_logins_5_0= ruleLogin ) )
            	    // InternalNetSpec.g:179:5: (lv_logins_5_0= ruleLogin )
            	    {
            	    // InternalNetSpec.g:179:5: (lv_logins_5_0= ruleLogin )
            	    // InternalNetSpec.g:180:6: lv_logins_5_0= ruleLogin
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getLoginsLoginParserRuleCall_2_3_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_logins_5_0=ruleLogin();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"logins",
            	    							lv_logins_5_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.Login");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalNetSpec.g:198:4: ( (lv_keys_6_0= ruleKey ) )
            	    {
            	    // InternalNetSpec.g:198:4: ( (lv_keys_6_0= ruleKey ) )
            	    // InternalNetSpec.g:199:5: (lv_keys_6_0= ruleKey )
            	    {
            	    // InternalNetSpec.g:199:5: (lv_keys_6_0= ruleKey )
            	    // InternalNetSpec.g:200:6: lv_keys_6_0= ruleKey
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getKeysKeyParserRuleCall_2_4_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_keys_6_0=ruleKey();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"keys",
            	    							lv_keys_6_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.Key");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 6 :
            	    // InternalNetSpec.g:218:4: ( (lv_firewalls_7_0= ruleFirewall ) )
            	    {
            	    // InternalNetSpec.g:218:4: ( (lv_firewalls_7_0= ruleFirewall ) )
            	    // InternalNetSpec.g:219:5: (lv_firewalls_7_0= ruleFirewall )
            	    {
            	    // InternalNetSpec.g:219:5: (lv_firewalls_7_0= ruleFirewall )
            	    // InternalNetSpec.g:220:6: lv_firewalls_7_0= ruleFirewall
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getFirewallsFirewallParserRuleCall_2_5_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_firewalls_7_0=ruleFirewall();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"firewalls",
            	    							lv_firewalls_7_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.Firewall");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 7 :
            	    // InternalNetSpec.g:238:4: ( (lv_ids_8_0= ruleIds ) )
            	    {
            	    // InternalNetSpec.g:238:4: ( (lv_ids_8_0= ruleIds ) )
            	    // InternalNetSpec.g:239:5: (lv_ids_8_0= ruleIds )
            	    {
            	    // InternalNetSpec.g:239:5: (lv_ids_8_0= ruleIds )
            	    // InternalNetSpec.g:240:6: lv_ids_8_0= ruleIds
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getIdsIdsParserRuleCall_2_6_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_ids_8_0=ruleIds();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"ids",
            	    							lv_ids_8_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.Ids");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 8 :
            	    // InternalNetSpec.g:258:4: ( (lv_links_9_0= ruleLink ) )
            	    {
            	    // InternalNetSpec.g:258:4: ( (lv_links_9_0= ruleLink ) )
            	    // InternalNetSpec.g:259:5: (lv_links_9_0= ruleLink )
            	    {
            	    // InternalNetSpec.g:259:5: (lv_links_9_0= ruleLink )
            	    // InternalNetSpec.g:260:6: lv_links_9_0= ruleLink
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getLinksLinkParserRuleCall_2_7_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_links_9_0=ruleLink();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"links",
            	    							lv_links_9_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.Link");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 9 :
            	    // InternalNetSpec.g:278:4: ( (lv_antivirus_10_0= ruleAntivirus ) )
            	    {
            	    // InternalNetSpec.g:278:4: ( (lv_antivirus_10_0= ruleAntivirus ) )
            	    // InternalNetSpec.g:279:5: (lv_antivirus_10_0= ruleAntivirus )
            	    {
            	    // InternalNetSpec.g:279:5: (lv_antivirus_10_0= ruleAntivirus )
            	    // InternalNetSpec.g:280:6: lv_antivirus_10_0= ruleAntivirus
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getAntivirusAntivirusParserRuleCall_2_8_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_antivirus_10_0=ruleAntivirus();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"antivirus",
            	    							lv_antivirus_10_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.Antivirus");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 10 :
            	    // InternalNetSpec.g:298:4: ( (lv_datas_11_0= ruleData ) )
            	    {
            	    // InternalNetSpec.g:298:4: ( (lv_datas_11_0= ruleData ) )
            	    // InternalNetSpec.g:299:5: (lv_datas_11_0= ruleData )
            	    {
            	    // InternalNetSpec.g:299:5: (lv_datas_11_0= ruleData )
            	    // InternalNetSpec.g:300:6: lv_datas_11_0= ruleData
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getDatasDataParserRuleCall_2_9_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_datas_11_0=ruleData();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"datas",
            	    							lv_datas_11_0,
            	    							"fr.irisa.atsyra.netspec.NetSpec.Data");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleAttacker"
    // InternalNetSpec.g:322:1: entryRuleAttacker returns [EObject current=null] : iv_ruleAttacker= ruleAttacker EOF ;
    public final EObject entryRuleAttacker() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttacker = null;


        try {
            // InternalNetSpec.g:322:49: (iv_ruleAttacker= ruleAttacker EOF )
            // InternalNetSpec.g:323:2: iv_ruleAttacker= ruleAttacker EOF
            {
             newCompositeNode(grammarAccess.getAttackerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttacker=ruleAttacker();

            state._fsp--;

             current =iv_ruleAttacker; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttacker"


    // $ANTLR start "ruleAttacker"
    // InternalNetSpec.g:329:1: ruleAttacker returns [EObject current=null] : ( () otherlv_1= 'Attacker' otherlv_2= '{' otherlv_3= 'hackLevel' ( ( (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' ) ) ) otherlv_5= 'cryptoLevel' ( ( (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' ) ) ) otherlv_7= 'virusLevel' ( ( (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' ) ) ) otherlv_9= 'loginLevel' ( ( (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' ) ) ) (otherlv_11= 'LoginsKnown' ( (otherlv_12= RULE_ID ) )+ )? (otherlv_13= 'KeysKnown' ( (otherlv_14= RULE_ID ) )+ )? (otherlv_15= 'HostsControlled' ( (otherlv_16= RULE_ID ) )+ )? otherlv_17= '}' ) ;
    public final EObject ruleAttacker() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_hack_4_1=null;
        Token lv_hack_4_2=null;
        Token lv_hack_4_3=null;
        Token lv_hack_4_4=null;
        Token otherlv_5=null;
        Token lv_crypto_6_1=null;
        Token lv_crypto_6_2=null;
        Token lv_crypto_6_3=null;
        Token lv_crypto_6_4=null;
        Token otherlv_7=null;
        Token lv_virus_8_1=null;
        Token lv_virus_8_2=null;
        Token lv_virus_8_3=null;
        Token lv_virus_8_4=null;
        Token otherlv_9=null;
        Token lv_login_10_1=null;
        Token lv_login_10_2=null;
        Token lv_login_10_3=null;
        Token lv_login_10_4=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;


        	enterRule();

        try {
            // InternalNetSpec.g:335:2: ( ( () otherlv_1= 'Attacker' otherlv_2= '{' otherlv_3= 'hackLevel' ( ( (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' ) ) ) otherlv_5= 'cryptoLevel' ( ( (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' ) ) ) otherlv_7= 'virusLevel' ( ( (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' ) ) ) otherlv_9= 'loginLevel' ( ( (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' ) ) ) (otherlv_11= 'LoginsKnown' ( (otherlv_12= RULE_ID ) )+ )? (otherlv_13= 'KeysKnown' ( (otherlv_14= RULE_ID ) )+ )? (otherlv_15= 'HostsControlled' ( (otherlv_16= RULE_ID ) )+ )? otherlv_17= '}' ) )
            // InternalNetSpec.g:336:2: ( () otherlv_1= 'Attacker' otherlv_2= '{' otherlv_3= 'hackLevel' ( ( (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' ) ) ) otherlv_5= 'cryptoLevel' ( ( (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' ) ) ) otherlv_7= 'virusLevel' ( ( (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' ) ) ) otherlv_9= 'loginLevel' ( ( (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' ) ) ) (otherlv_11= 'LoginsKnown' ( (otherlv_12= RULE_ID ) )+ )? (otherlv_13= 'KeysKnown' ( (otherlv_14= RULE_ID ) )+ )? (otherlv_15= 'HostsControlled' ( (otherlv_16= RULE_ID ) )+ )? otherlv_17= '}' )
            {
            // InternalNetSpec.g:336:2: ( () otherlv_1= 'Attacker' otherlv_2= '{' otherlv_3= 'hackLevel' ( ( (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' ) ) ) otherlv_5= 'cryptoLevel' ( ( (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' ) ) ) otherlv_7= 'virusLevel' ( ( (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' ) ) ) otherlv_9= 'loginLevel' ( ( (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' ) ) ) (otherlv_11= 'LoginsKnown' ( (otherlv_12= RULE_ID ) )+ )? (otherlv_13= 'KeysKnown' ( (otherlv_14= RULE_ID ) )+ )? (otherlv_15= 'HostsControlled' ( (otherlv_16= RULE_ID ) )+ )? otherlv_17= '}' )
            // InternalNetSpec.g:337:3: () otherlv_1= 'Attacker' otherlv_2= '{' otherlv_3= 'hackLevel' ( ( (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' ) ) ) otherlv_5= 'cryptoLevel' ( ( (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' ) ) ) otherlv_7= 'virusLevel' ( ( (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' ) ) ) otherlv_9= 'loginLevel' ( ( (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' ) ) ) (otherlv_11= 'LoginsKnown' ( (otherlv_12= RULE_ID ) )+ )? (otherlv_13= 'KeysKnown' ( (otherlv_14= RULE_ID ) )+ )? (otherlv_15= 'HostsControlled' ( (otherlv_16= RULE_ID ) )+ )? otherlv_17= '}'
            {
            // InternalNetSpec.g:337:3: ()
            // InternalNetSpec.g:338:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAttackerAccess().getAttackerAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getAttackerAccess().getAttackerKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getAttackerAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_3, grammarAccess.getAttackerAccess().getHackLevelKeyword_3());
            		
            // InternalNetSpec.g:356:3: ( ( (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' ) ) )
            // InternalNetSpec.g:357:4: ( (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' ) )
            {
            // InternalNetSpec.g:357:4: ( (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' ) )
            // InternalNetSpec.g:358:5: (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' )
            {
            // InternalNetSpec.g:358:5: (lv_hack_4_1= 'None' | lv_hack_4_2= 'Low' | lv_hack_4_3= 'Medium' | lv_hack_4_4= 'High' )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt2=1;
                }
                break;
            case 15:
                {
                alt2=2;
                }
                break;
            case 16:
                {
                alt2=3;
                }
                break;
            case 17:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalNetSpec.g:359:6: lv_hack_4_1= 'None'
                    {
                    lv_hack_4_1=(Token)match(input,14,FOLLOW_8); 

                    						newLeafNode(lv_hack_4_1, grammarAccess.getAttackerAccess().getHackNoneKeyword_4_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "hack", lv_hack_4_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:370:6: lv_hack_4_2= 'Low'
                    {
                    lv_hack_4_2=(Token)match(input,15,FOLLOW_8); 

                    						newLeafNode(lv_hack_4_2, grammarAccess.getAttackerAccess().getHackLowKeyword_4_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "hack", lv_hack_4_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:381:6: lv_hack_4_3= 'Medium'
                    {
                    lv_hack_4_3=(Token)match(input,16,FOLLOW_8); 

                    						newLeafNode(lv_hack_4_3, grammarAccess.getAttackerAccess().getHackMediumKeyword_4_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "hack", lv_hack_4_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:392:6: lv_hack_4_4= 'High'
                    {
                    lv_hack_4_4=(Token)match(input,17,FOLLOW_8); 

                    						newLeafNode(lv_hack_4_4, grammarAccess.getAttackerAccess().getHackHighKeyword_4_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "hack", lv_hack_4_4, null);
                    					

                    }
                    break;

            }


            }


            }

            otherlv_5=(Token)match(input,18,FOLLOW_7); 

            			newLeafNode(otherlv_5, grammarAccess.getAttackerAccess().getCryptoLevelKeyword_5());
            		
            // InternalNetSpec.g:409:3: ( ( (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' ) ) )
            // InternalNetSpec.g:410:4: ( (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' ) )
            {
            // InternalNetSpec.g:410:4: ( (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' ) )
            // InternalNetSpec.g:411:5: (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' )
            {
            // InternalNetSpec.g:411:5: (lv_crypto_6_1= 'None' | lv_crypto_6_2= 'Low' | lv_crypto_6_3= 'Medium' | lv_crypto_6_4= 'High' )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt3=1;
                }
                break;
            case 15:
                {
                alt3=2;
                }
                break;
            case 16:
                {
                alt3=3;
                }
                break;
            case 17:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalNetSpec.g:412:6: lv_crypto_6_1= 'None'
                    {
                    lv_crypto_6_1=(Token)match(input,14,FOLLOW_9); 

                    						newLeafNode(lv_crypto_6_1, grammarAccess.getAttackerAccess().getCryptoNoneKeyword_6_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "crypto", lv_crypto_6_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:423:6: lv_crypto_6_2= 'Low'
                    {
                    lv_crypto_6_2=(Token)match(input,15,FOLLOW_9); 

                    						newLeafNode(lv_crypto_6_2, grammarAccess.getAttackerAccess().getCryptoLowKeyword_6_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "crypto", lv_crypto_6_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:434:6: lv_crypto_6_3= 'Medium'
                    {
                    lv_crypto_6_3=(Token)match(input,16,FOLLOW_9); 

                    						newLeafNode(lv_crypto_6_3, grammarAccess.getAttackerAccess().getCryptoMediumKeyword_6_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "crypto", lv_crypto_6_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:445:6: lv_crypto_6_4= 'High'
                    {
                    lv_crypto_6_4=(Token)match(input,17,FOLLOW_9); 

                    						newLeafNode(lv_crypto_6_4, grammarAccess.getAttackerAccess().getCryptoHighKeyword_6_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "crypto", lv_crypto_6_4, null);
                    					

                    }
                    break;

            }


            }


            }

            otherlv_7=(Token)match(input,19,FOLLOW_7); 

            			newLeafNode(otherlv_7, grammarAccess.getAttackerAccess().getVirusLevelKeyword_7());
            		
            // InternalNetSpec.g:462:3: ( ( (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' ) ) )
            // InternalNetSpec.g:463:4: ( (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' ) )
            {
            // InternalNetSpec.g:463:4: ( (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' ) )
            // InternalNetSpec.g:464:5: (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' )
            {
            // InternalNetSpec.g:464:5: (lv_virus_8_1= 'None' | lv_virus_8_2= 'Low' | lv_virus_8_3= 'Medium' | lv_virus_8_4= 'High' )
            int alt4=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt4=1;
                }
                break;
            case 15:
                {
                alt4=2;
                }
                break;
            case 16:
                {
                alt4=3;
                }
                break;
            case 17:
                {
                alt4=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalNetSpec.g:465:6: lv_virus_8_1= 'None'
                    {
                    lv_virus_8_1=(Token)match(input,14,FOLLOW_10); 

                    						newLeafNode(lv_virus_8_1, grammarAccess.getAttackerAccess().getVirusNoneKeyword_8_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "virus", lv_virus_8_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:476:6: lv_virus_8_2= 'Low'
                    {
                    lv_virus_8_2=(Token)match(input,15,FOLLOW_10); 

                    						newLeafNode(lv_virus_8_2, grammarAccess.getAttackerAccess().getVirusLowKeyword_8_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "virus", lv_virus_8_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:487:6: lv_virus_8_3= 'Medium'
                    {
                    lv_virus_8_3=(Token)match(input,16,FOLLOW_10); 

                    						newLeafNode(lv_virus_8_3, grammarAccess.getAttackerAccess().getVirusMediumKeyword_8_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "virus", lv_virus_8_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:498:6: lv_virus_8_4= 'High'
                    {
                    lv_virus_8_4=(Token)match(input,17,FOLLOW_10); 

                    						newLeafNode(lv_virus_8_4, grammarAccess.getAttackerAccess().getVirusHighKeyword_8_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "virus", lv_virus_8_4, null);
                    					

                    }
                    break;

            }


            }


            }

            otherlv_9=(Token)match(input,20,FOLLOW_7); 

            			newLeafNode(otherlv_9, grammarAccess.getAttackerAccess().getLoginLevelKeyword_9());
            		
            // InternalNetSpec.g:515:3: ( ( (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' ) ) )
            // InternalNetSpec.g:516:4: ( (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' ) )
            {
            // InternalNetSpec.g:516:4: ( (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' ) )
            // InternalNetSpec.g:517:5: (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' )
            {
            // InternalNetSpec.g:517:5: (lv_login_10_1= 'None' | lv_login_10_2= 'Low' | lv_login_10_3= 'Medium' | lv_login_10_4= 'High' )
            int alt5=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt5=1;
                }
                break;
            case 15:
                {
                alt5=2;
                }
                break;
            case 16:
                {
                alt5=3;
                }
                break;
            case 17:
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalNetSpec.g:518:6: lv_login_10_1= 'None'
                    {
                    lv_login_10_1=(Token)match(input,14,FOLLOW_11); 

                    						newLeafNode(lv_login_10_1, grammarAccess.getAttackerAccess().getLoginNoneKeyword_10_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "login", lv_login_10_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:529:6: lv_login_10_2= 'Low'
                    {
                    lv_login_10_2=(Token)match(input,15,FOLLOW_11); 

                    						newLeafNode(lv_login_10_2, grammarAccess.getAttackerAccess().getLoginLowKeyword_10_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "login", lv_login_10_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:540:6: lv_login_10_3= 'Medium'
                    {
                    lv_login_10_3=(Token)match(input,16,FOLLOW_11); 

                    						newLeafNode(lv_login_10_3, grammarAccess.getAttackerAccess().getLoginMediumKeyword_10_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "login", lv_login_10_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:551:6: lv_login_10_4= 'High'
                    {
                    lv_login_10_4=(Token)match(input,17,FOLLOW_11); 

                    						newLeafNode(lv_login_10_4, grammarAccess.getAttackerAccess().getLoginHighKeyword_10_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttackerRule());
                    						}
                    						setWithLastConsumed(current, "login", lv_login_10_4, null);
                    					

                    }
                    break;

            }


            }


            }

            // InternalNetSpec.g:564:3: (otherlv_11= 'LoginsKnown' ( (otherlv_12= RULE_ID ) )+ )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalNetSpec.g:565:4: otherlv_11= 'LoginsKnown' ( (otherlv_12= RULE_ID ) )+
                    {
                    otherlv_11=(Token)match(input,21,FOLLOW_12); 

                    				newLeafNode(otherlv_11, grammarAccess.getAttackerAccess().getLoginsKnownKeyword_11_0());
                    			
                    // InternalNetSpec.g:569:4: ( (otherlv_12= RULE_ID ) )+
                    int cnt6=0;
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==RULE_ID) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalNetSpec.g:570:5: (otherlv_12= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:570:5: (otherlv_12= RULE_ID )
                    	    // InternalNetSpec.g:571:6: otherlv_12= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getAttackerRule());
                    	    						}
                    	    					
                    	    otherlv_12=(Token)match(input,RULE_ID,FOLLOW_13); 

                    	    						newLeafNode(otherlv_12, grammarAccess.getAttackerAccess().getLoginsKLoginCrossReference_11_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt6 >= 1 ) break loop6;
                                EarlyExitException eee =
                                    new EarlyExitException(6, input);
                                throw eee;
                        }
                        cnt6++;
                    } while (true);


                    }
                    break;

            }

            // InternalNetSpec.g:583:3: (otherlv_13= 'KeysKnown' ( (otherlv_14= RULE_ID ) )+ )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==22) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalNetSpec.g:584:4: otherlv_13= 'KeysKnown' ( (otherlv_14= RULE_ID ) )+
                    {
                    otherlv_13=(Token)match(input,22,FOLLOW_12); 

                    				newLeafNode(otherlv_13, grammarAccess.getAttackerAccess().getKeysKnownKeyword_12_0());
                    			
                    // InternalNetSpec.g:588:4: ( (otherlv_14= RULE_ID ) )+
                    int cnt8=0;
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==RULE_ID) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalNetSpec.g:589:5: (otherlv_14= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:589:5: (otherlv_14= RULE_ID )
                    	    // InternalNetSpec.g:590:6: otherlv_14= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getAttackerRule());
                    	    						}
                    	    					
                    	    otherlv_14=(Token)match(input,RULE_ID,FOLLOW_14); 

                    	    						newLeafNode(otherlv_14, grammarAccess.getAttackerAccess().getKeysKKeyCrossReference_12_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt8 >= 1 ) break loop8;
                                EarlyExitException eee =
                                    new EarlyExitException(8, input);
                                throw eee;
                        }
                        cnt8++;
                    } while (true);


                    }
                    break;

            }

            // InternalNetSpec.g:602:3: (otherlv_15= 'HostsControlled' ( (otherlv_16= RULE_ID ) )+ )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalNetSpec.g:603:4: otherlv_15= 'HostsControlled' ( (otherlv_16= RULE_ID ) )+
                    {
                    otherlv_15=(Token)match(input,23,FOLLOW_12); 

                    				newLeafNode(otherlv_15, grammarAccess.getAttackerAccess().getHostsControlledKeyword_13_0());
                    			
                    // InternalNetSpec.g:607:4: ( (otherlv_16= RULE_ID ) )+
                    int cnt10=0;
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==RULE_ID) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalNetSpec.g:608:5: (otherlv_16= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:608:5: (otherlv_16= RULE_ID )
                    	    // InternalNetSpec.g:609:6: otherlv_16= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getAttackerRule());
                    	    						}
                    	    					
                    	    otherlv_16=(Token)match(input,RULE_ID,FOLLOW_15); 

                    	    						newLeafNode(otherlv_16, grammarAccess.getAttackerAccess().getHostsHostCrossReference_13_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt10 >= 1 ) break loop10;
                                EarlyExitException eee =
                                    new EarlyExitException(10, input);
                                throw eee;
                        }
                        cnt10++;
                    } while (true);


                    }
                    break;

            }

            otherlv_17=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_17, grammarAccess.getAttackerAccess().getRightCurlyBracketKeyword_14());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttacker"


    // $ANTLR start "entryRuleGoal"
    // InternalNetSpec.g:629:1: entryRuleGoal returns [EObject current=null] : iv_ruleGoal= ruleGoal EOF ;
    public final EObject entryRuleGoal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGoal = null;


        try {
            // InternalNetSpec.g:629:45: (iv_ruleGoal= ruleGoal EOF )
            // InternalNetSpec.g:630:2: iv_ruleGoal= ruleGoal EOF
            {
             newCompositeNode(grammarAccess.getGoalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGoal=ruleGoal();

            state._fsp--;

             current =iv_ruleGoal; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGoal"


    // $ANTLR start "ruleGoal"
    // InternalNetSpec.g:636:1: ruleGoal returns [EObject current=null] : ( () otherlv_1= 'Goal' otherlv_2= '{' (otherlv_3= 'Datas' ( (otherlv_4= RULE_ID ) )+ )? (otherlv_5= 'Files' ( (otherlv_6= RULE_ID ) )+ )? (otherlv_7= 'Hosts' ( (otherlv_8= RULE_ID ) )+ )? ( (lv_ids_9_0= 'CanTriggerIds' ) )? otherlv_10= '}' ) ;
    public final EObject ruleGoal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token lv_ids_9_0=null;
        Token otherlv_10=null;


        	enterRule();

        try {
            // InternalNetSpec.g:642:2: ( ( () otherlv_1= 'Goal' otherlv_2= '{' (otherlv_3= 'Datas' ( (otherlv_4= RULE_ID ) )+ )? (otherlv_5= 'Files' ( (otherlv_6= RULE_ID ) )+ )? (otherlv_7= 'Hosts' ( (otherlv_8= RULE_ID ) )+ )? ( (lv_ids_9_0= 'CanTriggerIds' ) )? otherlv_10= '}' ) )
            // InternalNetSpec.g:643:2: ( () otherlv_1= 'Goal' otherlv_2= '{' (otherlv_3= 'Datas' ( (otherlv_4= RULE_ID ) )+ )? (otherlv_5= 'Files' ( (otherlv_6= RULE_ID ) )+ )? (otherlv_7= 'Hosts' ( (otherlv_8= RULE_ID ) )+ )? ( (lv_ids_9_0= 'CanTriggerIds' ) )? otherlv_10= '}' )
            {
            // InternalNetSpec.g:643:2: ( () otherlv_1= 'Goal' otherlv_2= '{' (otherlv_3= 'Datas' ( (otherlv_4= RULE_ID ) )+ )? (otherlv_5= 'Files' ( (otherlv_6= RULE_ID ) )+ )? (otherlv_7= 'Hosts' ( (otherlv_8= RULE_ID ) )+ )? ( (lv_ids_9_0= 'CanTriggerIds' ) )? otherlv_10= '}' )
            // InternalNetSpec.g:644:3: () otherlv_1= 'Goal' otherlv_2= '{' (otherlv_3= 'Datas' ( (otherlv_4= RULE_ID ) )+ )? (otherlv_5= 'Files' ( (otherlv_6= RULE_ID ) )+ )? (otherlv_7= 'Hosts' ( (otherlv_8= RULE_ID ) )+ )? ( (lv_ids_9_0= 'CanTriggerIds' ) )? otherlv_10= '}'
            {
            // InternalNetSpec.g:644:3: ()
            // InternalNetSpec.g:645:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGoalAccess().getGoalAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,25,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getGoalAccess().getGoalKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getGoalAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalNetSpec.g:659:3: (otherlv_3= 'Datas' ( (otherlv_4= RULE_ID ) )+ )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==26) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalNetSpec.g:660:4: otherlv_3= 'Datas' ( (otherlv_4= RULE_ID ) )+
                    {
                    otherlv_3=(Token)match(input,26,FOLLOW_12); 

                    				newLeafNode(otherlv_3, grammarAccess.getGoalAccess().getDatasKeyword_3_0());
                    			
                    // InternalNetSpec.g:664:4: ( (otherlv_4= RULE_ID ) )+
                    int cnt12=0;
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==RULE_ID) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalNetSpec.g:665:5: (otherlv_4= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:665:5: (otherlv_4= RULE_ID )
                    	    // InternalNetSpec.g:666:6: otherlv_4= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getGoalRule());
                    	    						}
                    	    					
                    	    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_17); 

                    	    						newLeafNode(otherlv_4, grammarAccess.getGoalAccess().getDatasDataCrossReference_3_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt12 >= 1 ) break loop12;
                                EarlyExitException eee =
                                    new EarlyExitException(12, input);
                                throw eee;
                        }
                        cnt12++;
                    } while (true);


                    }
                    break;

            }

            // InternalNetSpec.g:678:3: (otherlv_5= 'Files' ( (otherlv_6= RULE_ID ) )+ )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==27) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalNetSpec.g:679:4: otherlv_5= 'Files' ( (otherlv_6= RULE_ID ) )+
                    {
                    otherlv_5=(Token)match(input,27,FOLLOW_12); 

                    				newLeafNode(otherlv_5, grammarAccess.getGoalAccess().getFilesKeyword_4_0());
                    			
                    // InternalNetSpec.g:683:4: ( (otherlv_6= RULE_ID ) )+
                    int cnt14=0;
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==RULE_ID) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalNetSpec.g:684:5: (otherlv_6= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:684:5: (otherlv_6= RULE_ID )
                    	    // InternalNetSpec.g:685:6: otherlv_6= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getGoalRule());
                    	    						}
                    	    					
                    	    otherlv_6=(Token)match(input,RULE_ID,FOLLOW_18); 

                    	    						newLeafNode(otherlv_6, grammarAccess.getGoalAccess().getFilesFileCrossReference_4_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt14 >= 1 ) break loop14;
                                EarlyExitException eee =
                                    new EarlyExitException(14, input);
                                throw eee;
                        }
                        cnt14++;
                    } while (true);


                    }
                    break;

            }

            // InternalNetSpec.g:697:3: (otherlv_7= 'Hosts' ( (otherlv_8= RULE_ID ) )+ )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==28) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalNetSpec.g:698:4: otherlv_7= 'Hosts' ( (otherlv_8= RULE_ID ) )+
                    {
                    otherlv_7=(Token)match(input,28,FOLLOW_12); 

                    				newLeafNode(otherlv_7, grammarAccess.getGoalAccess().getHostsKeyword_5_0());
                    			
                    // InternalNetSpec.g:702:4: ( (otherlv_8= RULE_ID ) )+
                    int cnt16=0;
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==RULE_ID) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // InternalNetSpec.g:703:5: (otherlv_8= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:703:5: (otherlv_8= RULE_ID )
                    	    // InternalNetSpec.g:704:6: otherlv_8= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getGoalRule());
                    	    						}
                    	    					
                    	    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_19); 

                    	    						newLeafNode(otherlv_8, grammarAccess.getGoalAccess().getHostsHostCrossReference_5_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt16 >= 1 ) break loop16;
                                EarlyExitException eee =
                                    new EarlyExitException(16, input);
                                throw eee;
                        }
                        cnt16++;
                    } while (true);


                    }
                    break;

            }

            // InternalNetSpec.g:716:3: ( (lv_ids_9_0= 'CanTriggerIds' ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==29) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalNetSpec.g:717:4: (lv_ids_9_0= 'CanTriggerIds' )
                    {
                    // InternalNetSpec.g:717:4: (lv_ids_9_0= 'CanTriggerIds' )
                    // InternalNetSpec.g:718:5: lv_ids_9_0= 'CanTriggerIds'
                    {
                    lv_ids_9_0=(Token)match(input,29,FOLLOW_20); 

                    					newLeafNode(lv_ids_9_0, grammarAccess.getGoalAccess().getIdsCanTriggerIdsKeyword_6_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getGoalRule());
                    					}
                    					setWithLastConsumed(current, "ids", true, "CanTriggerIds");
                    				

                    }


                    }
                    break;

            }

            otherlv_10=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getGoalAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGoal"


    // $ANTLR start "entryRuleHost"
    // InternalNetSpec.g:738:1: entryRuleHost returns [EObject current=null] : iv_ruleHost= ruleHost EOF ;
    public final EObject entryRuleHost() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHost = null;


        try {
            // InternalNetSpec.g:738:45: (iv_ruleHost= ruleHost EOF )
            // InternalNetSpec.g:739:2: iv_ruleHost= ruleHost EOF
            {
             newCompositeNode(grammarAccess.getHostRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHost=ruleHost();

            state._fsp--;

             current =iv_ruleHost; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHost"


    // $ANTLR start "ruleHost"
    // InternalNetSpec.g:745:1: ruleHost returns [EObject current=null] : ( () otherlv_1= 'Host' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Sessions' ( (otherlv_5= RULE_ID ) )+ )? (otherlv_6= 'Files' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'Root' ( (otherlv_9= RULE_ID ) ) )? (otherlv_10= 'IDS' ( (otherlv_11= RULE_ID ) ) )? (otherlv_12= 'Firewall' ( (otherlv_13= RULE_ID ) ) )? (otherlv_14= 'Antivirus' ( (otherlv_15= RULE_ID ) ) )? otherlv_16= '}' ) ;
    public final EObject ruleHost() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;


        	enterRule();

        try {
            // InternalNetSpec.g:751:2: ( ( () otherlv_1= 'Host' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Sessions' ( (otherlv_5= RULE_ID ) )+ )? (otherlv_6= 'Files' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'Root' ( (otherlv_9= RULE_ID ) ) )? (otherlv_10= 'IDS' ( (otherlv_11= RULE_ID ) ) )? (otherlv_12= 'Firewall' ( (otherlv_13= RULE_ID ) ) )? (otherlv_14= 'Antivirus' ( (otherlv_15= RULE_ID ) ) )? otherlv_16= '}' ) )
            // InternalNetSpec.g:752:2: ( () otherlv_1= 'Host' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Sessions' ( (otherlv_5= RULE_ID ) )+ )? (otherlv_6= 'Files' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'Root' ( (otherlv_9= RULE_ID ) ) )? (otherlv_10= 'IDS' ( (otherlv_11= RULE_ID ) ) )? (otherlv_12= 'Firewall' ( (otherlv_13= RULE_ID ) ) )? (otherlv_14= 'Antivirus' ( (otherlv_15= RULE_ID ) ) )? otherlv_16= '}' )
            {
            // InternalNetSpec.g:752:2: ( () otherlv_1= 'Host' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Sessions' ( (otherlv_5= RULE_ID ) )+ )? (otherlv_6= 'Files' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'Root' ( (otherlv_9= RULE_ID ) ) )? (otherlv_10= 'IDS' ( (otherlv_11= RULE_ID ) ) )? (otherlv_12= 'Firewall' ( (otherlv_13= RULE_ID ) ) )? (otherlv_14= 'Antivirus' ( (otherlv_15= RULE_ID ) ) )? otherlv_16= '}' )
            // InternalNetSpec.g:753:3: () otherlv_1= 'Host' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Sessions' ( (otherlv_5= RULE_ID ) )+ )? (otherlv_6= 'Files' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'Root' ( (otherlv_9= RULE_ID ) ) )? (otherlv_10= 'IDS' ( (otherlv_11= RULE_ID ) ) )? (otherlv_12= 'Firewall' ( (otherlv_13= RULE_ID ) ) )? (otherlv_14= 'Antivirus' ( (otherlv_15= RULE_ID ) ) )? otherlv_16= '}'
            {
            // InternalNetSpec.g:753:3: ()
            // InternalNetSpec.g:754:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getHostAccess().getHostAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,30,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getHostAccess().getHostKeyword_1());
            		
            // InternalNetSpec.g:764:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalNetSpec.g:765:4: (lv_name_2_0= RULE_ID )
            {
            // InternalNetSpec.g:765:4: (lv_name_2_0= RULE_ID )
            // InternalNetSpec.g:766:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getHostAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getHostRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_21); 

            			newLeafNode(otherlv_3, grammarAccess.getHostAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalNetSpec.g:786:3: (otherlv_4= 'Sessions' ( (otherlv_5= RULE_ID ) )+ )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==31) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalNetSpec.g:787:4: otherlv_4= 'Sessions' ( (otherlv_5= RULE_ID ) )+
                    {
                    otherlv_4=(Token)match(input,31,FOLLOW_12); 

                    				newLeafNode(otherlv_4, grammarAccess.getHostAccess().getSessionsKeyword_4_0());
                    			
                    // InternalNetSpec.g:791:4: ( (otherlv_5= RULE_ID ) )+
                    int cnt19=0;
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==RULE_ID) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // InternalNetSpec.g:792:5: (otherlv_5= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:792:5: (otherlv_5= RULE_ID )
                    	    // InternalNetSpec.g:793:6: otherlv_5= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getHostRule());
                    	    						}
                    	    					
                    	    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_22); 

                    	    						newLeafNode(otherlv_5, grammarAccess.getHostAccess().getSessionsSessionCrossReference_4_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt19 >= 1 ) break loop19;
                                EarlyExitException eee =
                                    new EarlyExitException(19, input);
                                throw eee;
                        }
                        cnt19++;
                    } while (true);


                    }
                    break;

            }

            // InternalNetSpec.g:805:3: (otherlv_6= 'Files' ( (otherlv_7= RULE_ID ) )+ )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==27) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalNetSpec.g:806:4: otherlv_6= 'Files' ( (otherlv_7= RULE_ID ) )+
                    {
                    otherlv_6=(Token)match(input,27,FOLLOW_12); 

                    				newLeafNode(otherlv_6, grammarAccess.getHostAccess().getFilesKeyword_5_0());
                    			
                    // InternalNetSpec.g:810:4: ( (otherlv_7= RULE_ID ) )+
                    int cnt21=0;
                    loop21:
                    do {
                        int alt21=2;
                        int LA21_0 = input.LA(1);

                        if ( (LA21_0==RULE_ID) ) {
                            alt21=1;
                        }


                        switch (alt21) {
                    	case 1 :
                    	    // InternalNetSpec.g:811:5: (otherlv_7= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:811:5: (otherlv_7= RULE_ID )
                    	    // InternalNetSpec.g:812:6: otherlv_7= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getHostRule());
                    	    						}
                    	    					
                    	    otherlv_7=(Token)match(input,RULE_ID,FOLLOW_23); 

                    	    						newLeafNode(otherlv_7, grammarAccess.getHostAccess().getFilesFileCrossReference_5_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt21 >= 1 ) break loop21;
                                EarlyExitException eee =
                                    new EarlyExitException(21, input);
                                throw eee;
                        }
                        cnt21++;
                    } while (true);


                    }
                    break;

            }

            // InternalNetSpec.g:824:3: (otherlv_8= 'Root' ( (otherlv_9= RULE_ID ) ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==32) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalNetSpec.g:825:4: otherlv_8= 'Root' ( (otherlv_9= RULE_ID ) )
                    {
                    otherlv_8=(Token)match(input,32,FOLLOW_12); 

                    				newLeafNode(otherlv_8, grammarAccess.getHostAccess().getRootKeyword_6_0());
                    			
                    // InternalNetSpec.g:829:4: ( (otherlv_9= RULE_ID ) )
                    // InternalNetSpec.g:830:5: (otherlv_9= RULE_ID )
                    {
                    // InternalNetSpec.g:830:5: (otherlv_9= RULE_ID )
                    // InternalNetSpec.g:831:6: otherlv_9= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getHostRule());
                    						}
                    					
                    otherlv_9=(Token)match(input,RULE_ID,FOLLOW_24); 

                    						newLeafNode(otherlv_9, grammarAccess.getHostAccess().getRootPwdLoginCrossReference_6_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalNetSpec.g:843:3: (otherlv_10= 'IDS' ( (otherlv_11= RULE_ID ) ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==33) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalNetSpec.g:844:4: otherlv_10= 'IDS' ( (otherlv_11= RULE_ID ) )
                    {
                    otherlv_10=(Token)match(input,33,FOLLOW_12); 

                    				newLeafNode(otherlv_10, grammarAccess.getHostAccess().getIDSKeyword_7_0());
                    			
                    // InternalNetSpec.g:848:4: ( (otherlv_11= RULE_ID ) )
                    // InternalNetSpec.g:849:5: (otherlv_11= RULE_ID )
                    {
                    // InternalNetSpec.g:849:5: (otherlv_11= RULE_ID )
                    // InternalNetSpec.g:850:6: otherlv_11= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getHostRule());
                    						}
                    					
                    otherlv_11=(Token)match(input,RULE_ID,FOLLOW_25); 

                    						newLeafNode(otherlv_11, grammarAccess.getHostAccess().getIdsIdsCrossReference_7_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalNetSpec.g:862:3: (otherlv_12= 'Firewall' ( (otherlv_13= RULE_ID ) ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==34) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalNetSpec.g:863:4: otherlv_12= 'Firewall' ( (otherlv_13= RULE_ID ) )
                    {
                    otherlv_12=(Token)match(input,34,FOLLOW_12); 

                    				newLeafNode(otherlv_12, grammarAccess.getHostAccess().getFirewallKeyword_8_0());
                    			
                    // InternalNetSpec.g:867:4: ( (otherlv_13= RULE_ID ) )
                    // InternalNetSpec.g:868:5: (otherlv_13= RULE_ID )
                    {
                    // InternalNetSpec.g:868:5: (otherlv_13= RULE_ID )
                    // InternalNetSpec.g:869:6: otherlv_13= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getHostRule());
                    						}
                    					
                    otherlv_13=(Token)match(input,RULE_ID,FOLLOW_26); 

                    						newLeafNode(otherlv_13, grammarAccess.getHostAccess().getFirewallFirewallCrossReference_8_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalNetSpec.g:881:3: (otherlv_14= 'Antivirus' ( (otherlv_15= RULE_ID ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==35) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalNetSpec.g:882:4: otherlv_14= 'Antivirus' ( (otherlv_15= RULE_ID ) )
                    {
                    otherlv_14=(Token)match(input,35,FOLLOW_12); 

                    				newLeafNode(otherlv_14, grammarAccess.getHostAccess().getAntivirusKeyword_9_0());
                    			
                    // InternalNetSpec.g:886:4: ( (otherlv_15= RULE_ID ) )
                    // InternalNetSpec.g:887:5: (otherlv_15= RULE_ID )
                    {
                    // InternalNetSpec.g:887:5: (otherlv_15= RULE_ID )
                    // InternalNetSpec.g:888:6: otherlv_15= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getHostRule());
                    						}
                    					
                    otherlv_15=(Token)match(input,RULE_ID,FOLLOW_20); 

                    						newLeafNode(otherlv_15, grammarAccess.getHostAccess().getAntivirusAntivirusCrossReference_9_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_16=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_16, grammarAccess.getHostAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHost"


    // $ANTLR start "entryRuleSession"
    // InternalNetSpec.g:908:1: entryRuleSession returns [EObject current=null] : iv_ruleSession= ruleSession EOF ;
    public final EObject entryRuleSession() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSession = null;


        try {
            // InternalNetSpec.g:908:48: (iv_ruleSession= ruleSession EOF )
            // InternalNetSpec.g:909:2: iv_ruleSession= ruleSession EOF
            {
             newCompositeNode(grammarAccess.getSessionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSession=ruleSession();

            state._fsp--;

             current =iv_ruleSession; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSession"


    // $ANTLR start "ruleSession"
    // InternalNetSpec.g:915:1: ruleSession returns [EObject current=null] : ( () otherlv_1= 'Session' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Login' ( (otherlv_5= RULE_ID ) ) )? ( (lv_root_6_0= 'Root' ) )? (otherlv_7= 'Files' ( (otherlv_8= RULE_ID ) )+ )? otherlv_9= '}' ) ;
    public final EObject ruleSession() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_root_6_0=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;


        	enterRule();

        try {
            // InternalNetSpec.g:921:2: ( ( () otherlv_1= 'Session' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Login' ( (otherlv_5= RULE_ID ) ) )? ( (lv_root_6_0= 'Root' ) )? (otherlv_7= 'Files' ( (otherlv_8= RULE_ID ) )+ )? otherlv_9= '}' ) )
            // InternalNetSpec.g:922:2: ( () otherlv_1= 'Session' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Login' ( (otherlv_5= RULE_ID ) ) )? ( (lv_root_6_0= 'Root' ) )? (otherlv_7= 'Files' ( (otherlv_8= RULE_ID ) )+ )? otherlv_9= '}' )
            {
            // InternalNetSpec.g:922:2: ( () otherlv_1= 'Session' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Login' ( (otherlv_5= RULE_ID ) ) )? ( (lv_root_6_0= 'Root' ) )? (otherlv_7= 'Files' ( (otherlv_8= RULE_ID ) )+ )? otherlv_9= '}' )
            // InternalNetSpec.g:923:3: () otherlv_1= 'Session' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Login' ( (otherlv_5= RULE_ID ) ) )? ( (lv_root_6_0= 'Root' ) )? (otherlv_7= 'Files' ( (otherlv_8= RULE_ID ) )+ )? otherlv_9= '}'
            {
            // InternalNetSpec.g:923:3: ()
            // InternalNetSpec.g:924:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSessionAccess().getSessionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,36,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getSessionAccess().getSessionKeyword_1());
            		
            // InternalNetSpec.g:934:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalNetSpec.g:935:4: (lv_name_2_0= RULE_ID )
            {
            // InternalNetSpec.g:935:4: (lv_name_2_0= RULE_ID )
            // InternalNetSpec.g:936:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getSessionAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSessionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_27); 

            			newLeafNode(otherlv_3, grammarAccess.getSessionAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalNetSpec.g:956:3: (otherlv_4= 'Login' ( (otherlv_5= RULE_ID ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==37) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalNetSpec.g:957:4: otherlv_4= 'Login' ( (otherlv_5= RULE_ID ) )
                    {
                    otherlv_4=(Token)match(input,37,FOLLOW_12); 

                    				newLeafNode(otherlv_4, grammarAccess.getSessionAccess().getLoginKeyword_4_0());
                    			
                    // InternalNetSpec.g:961:4: ( (otherlv_5= RULE_ID ) )
                    // InternalNetSpec.g:962:5: (otherlv_5= RULE_ID )
                    {
                    // InternalNetSpec.g:962:5: (otherlv_5= RULE_ID )
                    // InternalNetSpec.g:963:6: otherlv_5= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSessionRule());
                    						}
                    					
                    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_28); 

                    						newLeafNode(otherlv_5, grammarAccess.getSessionAccess().getLoginLoginCrossReference_4_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalNetSpec.g:975:3: ( (lv_root_6_0= 'Root' ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==32) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalNetSpec.g:976:4: (lv_root_6_0= 'Root' )
                    {
                    // InternalNetSpec.g:976:4: (lv_root_6_0= 'Root' )
                    // InternalNetSpec.g:977:5: lv_root_6_0= 'Root'
                    {
                    lv_root_6_0=(Token)match(input,32,FOLLOW_29); 

                    					newLeafNode(lv_root_6_0, grammarAccess.getSessionAccess().getRootRootKeyword_5_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSessionRule());
                    					}
                    					setWithLastConsumed(current, "root", true, "Root");
                    				

                    }


                    }
                    break;

            }

            // InternalNetSpec.g:989:3: (otherlv_7= 'Files' ( (otherlv_8= RULE_ID ) )+ )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==27) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalNetSpec.g:990:4: otherlv_7= 'Files' ( (otherlv_8= RULE_ID ) )+
                    {
                    otherlv_7=(Token)match(input,27,FOLLOW_12); 

                    				newLeafNode(otherlv_7, grammarAccess.getSessionAccess().getFilesKeyword_6_0());
                    			
                    // InternalNetSpec.g:994:4: ( (otherlv_8= RULE_ID ) )+
                    int cnt29=0;
                    loop29:
                    do {
                        int alt29=2;
                        int LA29_0 = input.LA(1);

                        if ( (LA29_0==RULE_ID) ) {
                            alt29=1;
                        }


                        switch (alt29) {
                    	case 1 :
                    	    // InternalNetSpec.g:995:5: (otherlv_8= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:995:5: (otherlv_8= RULE_ID )
                    	    // InternalNetSpec.g:996:6: otherlv_8= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getSessionRule());
                    	    						}
                    	    					
                    	    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_15); 

                    	    						newLeafNode(otherlv_8, grammarAccess.getSessionAccess().getFilesFileCrossReference_6_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt29 >= 1 ) break loop29;
                                EarlyExitException eee =
                                    new EarlyExitException(29, input);
                                throw eee;
                        }
                        cnt29++;
                    } while (true);


                    }
                    break;

            }

            otherlv_9=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getSessionAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSession"


    // $ANTLR start "entryRuleAntivirus"
    // InternalNetSpec.g:1016:1: entryRuleAntivirus returns [EObject current=null] : iv_ruleAntivirus= ruleAntivirus EOF ;
    public final EObject entryRuleAntivirus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAntivirus = null;


        try {
            // InternalNetSpec.g:1016:50: (iv_ruleAntivirus= ruleAntivirus EOF )
            // InternalNetSpec.g:1017:2: iv_ruleAntivirus= ruleAntivirus EOF
            {
             newCompositeNode(grammarAccess.getAntivirusRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAntivirus=ruleAntivirus();

            state._fsp--;

             current =iv_ruleAntivirus; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAntivirus"


    // $ANTLR start "ruleAntivirus"
    // InternalNetSpec.g:1023:1: ruleAntivirus returns [EObject current=null] : ( () otherlv_1= 'Antivirus' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}' ) ;
    public final EObject ruleAntivirus() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_level_5_1=null;
        Token lv_level_5_2=null;
        Token lv_level_5_3=null;
        Token lv_level_5_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalNetSpec.g:1029:2: ( ( () otherlv_1= 'Antivirus' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}' ) )
            // InternalNetSpec.g:1030:2: ( () otherlv_1= 'Antivirus' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}' )
            {
            // InternalNetSpec.g:1030:2: ( () otherlv_1= 'Antivirus' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}' )
            // InternalNetSpec.g:1031:3: () otherlv_1= 'Antivirus' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}'
            {
            // InternalNetSpec.g:1031:3: ()
            // InternalNetSpec.g:1032:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAntivirusAccess().getAntivirusAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,35,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getAntivirusAccess().getAntivirusKeyword_1());
            		
            // InternalNetSpec.g:1042:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalNetSpec.g:1043:4: (lv_name_2_0= RULE_ID )
            {
            // InternalNetSpec.g:1043:4: (lv_name_2_0= RULE_ID )
            // InternalNetSpec.g:1044:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getAntivirusAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAntivirusRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_30); 

            			newLeafNode(otherlv_3, grammarAccess.getAntivirusAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,38,FOLLOW_31); 

            			newLeafNode(otherlv_4, grammarAccess.getAntivirusAccess().getLevelKeyword_4());
            		
            // InternalNetSpec.g:1068:3: ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) )
            // InternalNetSpec.g:1069:4: ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) )
            {
            // InternalNetSpec.g:1069:4: ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) )
            // InternalNetSpec.g:1070:5: (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' )
            {
            // InternalNetSpec.g:1070:5: (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' )
            int alt31=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt31=1;
                }
                break;
            case 16:
                {
                alt31=2;
                }
                break;
            case 17:
                {
                alt31=3;
                }
                break;
            case 39:
                {
                alt31=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // InternalNetSpec.g:1071:6: lv_level_5_1= 'Low'
                    {
                    lv_level_5_1=(Token)match(input,15,FOLLOW_20); 

                    						newLeafNode(lv_level_5_1, grammarAccess.getAntivirusAccess().getLevelLowKeyword_5_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAntivirusRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:1082:6: lv_level_5_2= 'Medium'
                    {
                    lv_level_5_2=(Token)match(input,16,FOLLOW_20); 

                    						newLeafNode(lv_level_5_2, grammarAccess.getAntivirusAccess().getLevelMediumKeyword_5_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAntivirusRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:1093:6: lv_level_5_3= 'High'
                    {
                    lv_level_5_3=(Token)match(input,17,FOLLOW_20); 

                    						newLeafNode(lv_level_5_3, grammarAccess.getAntivirusAccess().getLevelHighKeyword_5_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAntivirusRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:1104:6: lv_level_5_4= 'Perfect'
                    {
                    lv_level_5_4=(Token)match(input,39,FOLLOW_20); 

                    						newLeafNode(lv_level_5_4, grammarAccess.getAntivirusAccess().getLevelPerfectKeyword_5_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAntivirusRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_4, null);
                    					

                    }
                    break;

            }


            }


            }

            otherlv_6=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getAntivirusAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAntivirus"


    // $ANTLR start "entryRuleFile"
    // InternalNetSpec.g:1125:1: entryRuleFile returns [EObject current=null] : iv_ruleFile= ruleFile EOF ;
    public final EObject entryRuleFile() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFile = null;


        try {
            // InternalNetSpec.g:1125:45: (iv_ruleFile= ruleFile EOF )
            // InternalNetSpec.g:1126:2: iv_ruleFile= ruleFile EOF
            {
             newCompositeNode(grammarAccess.getFileRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFile=ruleFile();

            state._fsp--;

             current =iv_ruleFile; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFile"


    // $ANTLR start "ruleFile"
    // InternalNetSpec.g:1132:1: ruleFile returns [EObject current=null] : ( () otherlv_1= 'File' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Key' ( (otherlv_5= RULE_ID ) ) )? (otherlv_6= 'ContainsLogins' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'ContainsKeys' ( (otherlv_9= RULE_ID ) )+ )? (otherlv_10= 'Datas' ( (otherlv_11= RULE_ID ) )+ )? otherlv_12= '}' ) ;
    public final EObject ruleFile() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;


        	enterRule();

        try {
            // InternalNetSpec.g:1138:2: ( ( () otherlv_1= 'File' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Key' ( (otherlv_5= RULE_ID ) ) )? (otherlv_6= 'ContainsLogins' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'ContainsKeys' ( (otherlv_9= RULE_ID ) )+ )? (otherlv_10= 'Datas' ( (otherlv_11= RULE_ID ) )+ )? otherlv_12= '}' ) )
            // InternalNetSpec.g:1139:2: ( () otherlv_1= 'File' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Key' ( (otherlv_5= RULE_ID ) ) )? (otherlv_6= 'ContainsLogins' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'ContainsKeys' ( (otherlv_9= RULE_ID ) )+ )? (otherlv_10= 'Datas' ( (otherlv_11= RULE_ID ) )+ )? otherlv_12= '}' )
            {
            // InternalNetSpec.g:1139:2: ( () otherlv_1= 'File' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Key' ( (otherlv_5= RULE_ID ) ) )? (otherlv_6= 'ContainsLogins' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'ContainsKeys' ( (otherlv_9= RULE_ID ) )+ )? (otherlv_10= 'Datas' ( (otherlv_11= RULE_ID ) )+ )? otherlv_12= '}' )
            // InternalNetSpec.g:1140:3: () otherlv_1= 'File' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'Key' ( (otherlv_5= RULE_ID ) ) )? (otherlv_6= 'ContainsLogins' ( (otherlv_7= RULE_ID ) )+ )? (otherlv_8= 'ContainsKeys' ( (otherlv_9= RULE_ID ) )+ )? (otherlv_10= 'Datas' ( (otherlv_11= RULE_ID ) )+ )? otherlv_12= '}'
            {
            // InternalNetSpec.g:1140:3: ()
            // InternalNetSpec.g:1141:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFileAccess().getFileAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,40,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getFileAccess().getFileKeyword_1());
            		
            // InternalNetSpec.g:1151:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalNetSpec.g:1152:4: (lv_name_2_0= RULE_ID )
            {
            // InternalNetSpec.g:1152:4: (lv_name_2_0= RULE_ID )
            // InternalNetSpec.g:1153:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getFileAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFileRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_32); 

            			newLeafNode(otherlv_3, grammarAccess.getFileAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalNetSpec.g:1173:3: (otherlv_4= 'Key' ( (otherlv_5= RULE_ID ) ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==41) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalNetSpec.g:1174:4: otherlv_4= 'Key' ( (otherlv_5= RULE_ID ) )
                    {
                    otherlv_4=(Token)match(input,41,FOLLOW_12); 

                    				newLeafNode(otherlv_4, grammarAccess.getFileAccess().getKeyKeyword_4_0());
                    			
                    // InternalNetSpec.g:1178:4: ( (otherlv_5= RULE_ID ) )
                    // InternalNetSpec.g:1179:5: (otherlv_5= RULE_ID )
                    {
                    // InternalNetSpec.g:1179:5: (otherlv_5= RULE_ID )
                    // InternalNetSpec.g:1180:6: otherlv_5= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFileRule());
                    						}
                    					
                    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_33); 

                    						newLeafNode(otherlv_5, grammarAccess.getFileAccess().getKeyKeyCrossReference_4_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalNetSpec.g:1192:3: (otherlv_6= 'ContainsLogins' ( (otherlv_7= RULE_ID ) )+ )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==42) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalNetSpec.g:1193:4: otherlv_6= 'ContainsLogins' ( (otherlv_7= RULE_ID ) )+
                    {
                    otherlv_6=(Token)match(input,42,FOLLOW_12); 

                    				newLeafNode(otherlv_6, grammarAccess.getFileAccess().getContainsLoginsKeyword_5_0());
                    			
                    // InternalNetSpec.g:1197:4: ( (otherlv_7= RULE_ID ) )+
                    int cnt33=0;
                    loop33:
                    do {
                        int alt33=2;
                        int LA33_0 = input.LA(1);

                        if ( (LA33_0==RULE_ID) ) {
                            alt33=1;
                        }


                        switch (alt33) {
                    	case 1 :
                    	    // InternalNetSpec.g:1198:5: (otherlv_7= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:1198:5: (otherlv_7= RULE_ID )
                    	    // InternalNetSpec.g:1199:6: otherlv_7= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getFileRule());
                    	    						}
                    	    					
                    	    otherlv_7=(Token)match(input,RULE_ID,FOLLOW_34); 

                    	    						newLeafNode(otherlv_7, grammarAccess.getFileAccess().getContainsLLoginCrossReference_5_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt33 >= 1 ) break loop33;
                                EarlyExitException eee =
                                    new EarlyExitException(33, input);
                                throw eee;
                        }
                        cnt33++;
                    } while (true);


                    }
                    break;

            }

            // InternalNetSpec.g:1211:3: (otherlv_8= 'ContainsKeys' ( (otherlv_9= RULE_ID ) )+ )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==43) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalNetSpec.g:1212:4: otherlv_8= 'ContainsKeys' ( (otherlv_9= RULE_ID ) )+
                    {
                    otherlv_8=(Token)match(input,43,FOLLOW_12); 

                    				newLeafNode(otherlv_8, grammarAccess.getFileAccess().getContainsKeysKeyword_6_0());
                    			
                    // InternalNetSpec.g:1216:4: ( (otherlv_9= RULE_ID ) )+
                    int cnt35=0;
                    loop35:
                    do {
                        int alt35=2;
                        int LA35_0 = input.LA(1);

                        if ( (LA35_0==RULE_ID) ) {
                            alt35=1;
                        }


                        switch (alt35) {
                    	case 1 :
                    	    // InternalNetSpec.g:1217:5: (otherlv_9= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:1217:5: (otherlv_9= RULE_ID )
                    	    // InternalNetSpec.g:1218:6: otherlv_9= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getFileRule());
                    	    						}
                    	    					
                    	    otherlv_9=(Token)match(input,RULE_ID,FOLLOW_35); 

                    	    						newLeafNode(otherlv_9, grammarAccess.getFileAccess().getContainsKKeyCrossReference_6_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt35 >= 1 ) break loop35;
                                EarlyExitException eee =
                                    new EarlyExitException(35, input);
                                throw eee;
                        }
                        cnt35++;
                    } while (true);


                    }
                    break;

            }

            // InternalNetSpec.g:1230:3: (otherlv_10= 'Datas' ( (otherlv_11= RULE_ID ) )+ )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==26) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalNetSpec.g:1231:4: otherlv_10= 'Datas' ( (otherlv_11= RULE_ID ) )+
                    {
                    otherlv_10=(Token)match(input,26,FOLLOW_12); 

                    				newLeafNode(otherlv_10, grammarAccess.getFileAccess().getDatasKeyword_7_0());
                    			
                    // InternalNetSpec.g:1235:4: ( (otherlv_11= RULE_ID ) )+
                    int cnt37=0;
                    loop37:
                    do {
                        int alt37=2;
                        int LA37_0 = input.LA(1);

                        if ( (LA37_0==RULE_ID) ) {
                            alt37=1;
                        }


                        switch (alt37) {
                    	case 1 :
                    	    // InternalNetSpec.g:1236:5: (otherlv_11= RULE_ID )
                    	    {
                    	    // InternalNetSpec.g:1236:5: (otherlv_11= RULE_ID )
                    	    // InternalNetSpec.g:1237:6: otherlv_11= RULE_ID
                    	    {

                    	    						if (current==null) {
                    	    							current = createModelElement(grammarAccess.getFileRule());
                    	    						}
                    	    					
                    	    otherlv_11=(Token)match(input,RULE_ID,FOLLOW_15); 

                    	    						newLeafNode(otherlv_11, grammarAccess.getFileAccess().getDatasDataCrossReference_7_1_0());
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt37 >= 1 ) break loop37;
                                EarlyExitException eee =
                                    new EarlyExitException(37, input);
                                throw eee;
                        }
                        cnt37++;
                    } while (true);


                    }
                    break;

            }

            otherlv_12=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_12, grammarAccess.getFileAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFile"


    // $ANTLR start "entryRuleData"
    // InternalNetSpec.g:1257:1: entryRuleData returns [EObject current=null] : iv_ruleData= ruleData EOF ;
    public final EObject entryRuleData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleData = null;


        try {
            // InternalNetSpec.g:1257:45: (iv_ruleData= ruleData EOF )
            // InternalNetSpec.g:1258:2: iv_ruleData= ruleData EOF
            {
             newCompositeNode(grammarAccess.getDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleData=ruleData();

            state._fsp--;

             current =iv_ruleData; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleData"


    // $ANTLR start "ruleData"
    // InternalNetSpec.g:1264:1: ruleData returns [EObject current=null] : (otherlv_0= 'Data' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= '}' ) ;
    public final EObject ruleData() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalNetSpec.g:1270:2: ( (otherlv_0= 'Data' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= '}' ) )
            // InternalNetSpec.g:1271:2: (otherlv_0= 'Data' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= '}' )
            {
            // InternalNetSpec.g:1271:2: (otherlv_0= 'Data' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= '}' )
            // InternalNetSpec.g:1272:3: otherlv_0= 'Data' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= '}'
            {
            otherlv_0=(Token)match(input,44,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getDataAccess().getDataKeyword_0());
            		
            // InternalNetSpec.g:1276:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalNetSpec.g:1277:4: (lv_name_1_0= RULE_ID )
            {
            // InternalNetSpec.g:1277:4: (lv_name_1_0= RULE_ID )
            // InternalNetSpec.g:1278:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_1_0, grammarAccess.getDataAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDataRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getDataAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getDataAccess().getRightCurlyBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleData"


    // $ANTLR start "entryRuleLogin"
    // InternalNetSpec.g:1306:1: entryRuleLogin returns [EObject current=null] : iv_ruleLogin= ruleLogin EOF ;
    public final EObject entryRuleLogin() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogin = null;


        try {
            // InternalNetSpec.g:1306:46: (iv_ruleLogin= ruleLogin EOF )
            // InternalNetSpec.g:1307:2: iv_ruleLogin= ruleLogin EOF
            {
             newCompositeNode(grammarAccess.getLoginRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogin=ruleLogin();

            state._fsp--;

             current =iv_ruleLogin; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogin"


    // $ANTLR start "ruleLogin"
    // InternalNetSpec.g:1313:1: ruleLogin returns [EObject current=null] : ( () otherlv_1= 'Login' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}' ) ;
    public final EObject ruleLogin() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_level_5_1=null;
        Token lv_level_5_2=null;
        Token lv_level_5_3=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalNetSpec.g:1319:2: ( ( () otherlv_1= 'Login' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}' ) )
            // InternalNetSpec.g:1320:2: ( () otherlv_1= 'Login' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}' )
            {
            // InternalNetSpec.g:1320:2: ( () otherlv_1= 'Login' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}' )
            // InternalNetSpec.g:1321:3: () otherlv_1= 'Login' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}'
            {
            // InternalNetSpec.g:1321:3: ()
            // InternalNetSpec.g:1322:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLoginAccess().getLoginAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,37,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getLoginAccess().getLoginKeyword_1());
            		
            // InternalNetSpec.g:1332:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalNetSpec.g:1333:4: (lv_name_2_0= RULE_ID )
            {
            // InternalNetSpec.g:1333:4: (lv_name_2_0= RULE_ID )
            // InternalNetSpec.g:1334:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getLoginAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLoginRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_30); 

            			newLeafNode(otherlv_3, grammarAccess.getLoginAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,38,FOLLOW_36); 

            			newLeafNode(otherlv_4, grammarAccess.getLoginAccess().getLevelKeyword_4());
            		
            // InternalNetSpec.g:1358:3: ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) )
            // InternalNetSpec.g:1359:4: ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) )
            {
            // InternalNetSpec.g:1359:4: ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) )
            // InternalNetSpec.g:1360:5: (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' )
            {
            // InternalNetSpec.g:1360:5: (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' )
            int alt39=3;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt39=1;
                }
                break;
            case 16:
                {
                alt39=2;
                }
                break;
            case 17:
                {
                alt39=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;
            }

            switch (alt39) {
                case 1 :
                    // InternalNetSpec.g:1361:6: lv_level_5_1= 'Low'
                    {
                    lv_level_5_1=(Token)match(input,15,FOLLOW_20); 

                    						newLeafNode(lv_level_5_1, grammarAccess.getLoginAccess().getLevelLowKeyword_5_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getLoginRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:1372:6: lv_level_5_2= 'Medium'
                    {
                    lv_level_5_2=(Token)match(input,16,FOLLOW_20); 

                    						newLeafNode(lv_level_5_2, grammarAccess.getLoginAccess().getLevelMediumKeyword_5_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getLoginRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:1383:6: lv_level_5_3= 'High'
                    {
                    lv_level_5_3=(Token)match(input,17,FOLLOW_20); 

                    						newLeafNode(lv_level_5_3, grammarAccess.getLoginAccess().getLevelHighKeyword_5_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getLoginRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_3, null);
                    					

                    }
                    break;

            }


            }


            }

            otherlv_6=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getLoginAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogin"


    // $ANTLR start "entryRuleKey"
    // InternalNetSpec.g:1404:1: entryRuleKey returns [EObject current=null] : iv_ruleKey= ruleKey EOF ;
    public final EObject entryRuleKey() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleKey = null;


        try {
            // InternalNetSpec.g:1404:44: (iv_ruleKey= ruleKey EOF )
            // InternalNetSpec.g:1405:2: iv_ruleKey= ruleKey EOF
            {
             newCompositeNode(grammarAccess.getKeyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleKey=ruleKey();

            state._fsp--;

             current =iv_ruleKey; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleKey"


    // $ANTLR start "ruleKey"
    // InternalNetSpec.g:1411:1: ruleKey returns [EObject current=null] : ( () otherlv_1= 'Key' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}' ) ;
    public final EObject ruleKey() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_level_5_1=null;
        Token lv_level_5_2=null;
        Token lv_level_5_3=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalNetSpec.g:1417:2: ( ( () otherlv_1= 'Key' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}' ) )
            // InternalNetSpec.g:1418:2: ( () otherlv_1= 'Key' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}' )
            {
            // InternalNetSpec.g:1418:2: ( () otherlv_1= 'Key' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}' )
            // InternalNetSpec.g:1419:3: () otherlv_1= 'Key' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) ) otherlv_6= '}'
            {
            // InternalNetSpec.g:1419:3: ()
            // InternalNetSpec.g:1420:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getKeyAccess().getKeyAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,41,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getKeyAccess().getKeyKeyword_1());
            		
            // InternalNetSpec.g:1430:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalNetSpec.g:1431:4: (lv_name_2_0= RULE_ID )
            {
            // InternalNetSpec.g:1431:4: (lv_name_2_0= RULE_ID )
            // InternalNetSpec.g:1432:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getKeyAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getKeyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_30); 

            			newLeafNode(otherlv_3, grammarAccess.getKeyAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,38,FOLLOW_36); 

            			newLeafNode(otherlv_4, grammarAccess.getKeyAccess().getLevelKeyword_4());
            		
            // InternalNetSpec.g:1456:3: ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) ) )
            // InternalNetSpec.g:1457:4: ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) )
            {
            // InternalNetSpec.g:1457:4: ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' ) )
            // InternalNetSpec.g:1458:5: (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' )
            {
            // InternalNetSpec.g:1458:5: (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' )
            int alt40=3;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt40=1;
                }
                break;
            case 16:
                {
                alt40=2;
                }
                break;
            case 17:
                {
                alt40=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;
            }

            switch (alt40) {
                case 1 :
                    // InternalNetSpec.g:1459:6: lv_level_5_1= 'Low'
                    {
                    lv_level_5_1=(Token)match(input,15,FOLLOW_20); 

                    						newLeafNode(lv_level_5_1, grammarAccess.getKeyAccess().getLevelLowKeyword_5_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getKeyRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:1470:6: lv_level_5_2= 'Medium'
                    {
                    lv_level_5_2=(Token)match(input,16,FOLLOW_20); 

                    						newLeafNode(lv_level_5_2, grammarAccess.getKeyAccess().getLevelMediumKeyword_5_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getKeyRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:1481:6: lv_level_5_3= 'High'
                    {
                    lv_level_5_3=(Token)match(input,17,FOLLOW_20); 

                    						newLeafNode(lv_level_5_3, grammarAccess.getKeyAccess().getLevelHighKeyword_5_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getKeyRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_3, null);
                    					

                    }
                    break;

            }


            }


            }

            otherlv_6=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getKeyAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleKey"


    // $ANTLR start "entryRuleFirewall"
    // InternalNetSpec.g:1502:1: entryRuleFirewall returns [EObject current=null] : iv_ruleFirewall= ruleFirewall EOF ;
    public final EObject entryRuleFirewall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFirewall = null;


        try {
            // InternalNetSpec.g:1502:49: (iv_ruleFirewall= ruleFirewall EOF )
            // InternalNetSpec.g:1503:2: iv_ruleFirewall= ruleFirewall EOF
            {
             newCompositeNode(grammarAccess.getFirewallRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFirewall=ruleFirewall();

            state._fsp--;

             current =iv_ruleFirewall; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFirewall"


    // $ANTLR start "ruleFirewall"
    // InternalNetSpec.g:1509:1: ruleFirewall returns [EObject current=null] : ( () otherlv_1= 'Firewall' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}' ) ;
    public final EObject ruleFirewall() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_level_5_1=null;
        Token lv_level_5_2=null;
        Token lv_level_5_3=null;
        Token lv_level_5_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalNetSpec.g:1515:2: ( ( () otherlv_1= 'Firewall' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}' ) )
            // InternalNetSpec.g:1516:2: ( () otherlv_1= 'Firewall' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}' )
            {
            // InternalNetSpec.g:1516:2: ( () otherlv_1= 'Firewall' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}' )
            // InternalNetSpec.g:1517:3: () otherlv_1= 'Firewall' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= 'Level' ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) ) otherlv_6= '}'
            {
            // InternalNetSpec.g:1517:3: ()
            // InternalNetSpec.g:1518:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFirewallAccess().getFirewallAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,34,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getFirewallAccess().getFirewallKeyword_1());
            		
            // InternalNetSpec.g:1528:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalNetSpec.g:1529:4: (lv_name_2_0= RULE_ID )
            {
            // InternalNetSpec.g:1529:4: (lv_name_2_0= RULE_ID )
            // InternalNetSpec.g:1530:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getFirewallAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFirewallRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_30); 

            			newLeafNode(otherlv_3, grammarAccess.getFirewallAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,38,FOLLOW_31); 

            			newLeafNode(otherlv_4, grammarAccess.getFirewallAccess().getLevelKeyword_4());
            		
            // InternalNetSpec.g:1554:3: ( ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) ) )
            // InternalNetSpec.g:1555:4: ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) )
            {
            // InternalNetSpec.g:1555:4: ( (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' ) )
            // InternalNetSpec.g:1556:5: (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' )
            {
            // InternalNetSpec.g:1556:5: (lv_level_5_1= 'Low' | lv_level_5_2= 'Medium' | lv_level_5_3= 'High' | lv_level_5_4= 'Perfect' )
            int alt41=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt41=1;
                }
                break;
            case 16:
                {
                alt41=2;
                }
                break;
            case 17:
                {
                alt41=3;
                }
                break;
            case 39:
                {
                alt41=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;
            }

            switch (alt41) {
                case 1 :
                    // InternalNetSpec.g:1557:6: lv_level_5_1= 'Low'
                    {
                    lv_level_5_1=(Token)match(input,15,FOLLOW_20); 

                    						newLeafNode(lv_level_5_1, grammarAccess.getFirewallAccess().getLevelLowKeyword_5_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFirewallRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:1568:6: lv_level_5_2= 'Medium'
                    {
                    lv_level_5_2=(Token)match(input,16,FOLLOW_20); 

                    						newLeafNode(lv_level_5_2, grammarAccess.getFirewallAccess().getLevelMediumKeyword_5_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFirewallRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:1579:6: lv_level_5_3= 'High'
                    {
                    lv_level_5_3=(Token)match(input,17,FOLLOW_20); 

                    						newLeafNode(lv_level_5_3, grammarAccess.getFirewallAccess().getLevelHighKeyword_5_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFirewallRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:1590:6: lv_level_5_4= 'Perfect'
                    {
                    lv_level_5_4=(Token)match(input,39,FOLLOW_20); 

                    						newLeafNode(lv_level_5_4, grammarAccess.getFirewallAccess().getLevelPerfectKeyword_5_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFirewallRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_5_4, null);
                    					

                    }
                    break;

            }


            }


            }

            otherlv_6=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getFirewallAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFirewall"


    // $ANTLR start "entryRuleIds"
    // InternalNetSpec.g:1611:1: entryRuleIds returns [EObject current=null] : iv_ruleIds= ruleIds EOF ;
    public final EObject entryRuleIds() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIds = null;


        try {
            // InternalNetSpec.g:1611:44: (iv_ruleIds= ruleIds EOF )
            // InternalNetSpec.g:1612:2: iv_ruleIds= ruleIds EOF
            {
             newCompositeNode(grammarAccess.getIdsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIds=ruleIds();

            state._fsp--;

             current =iv_ruleIds; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIds"


    // $ANTLR start "ruleIds"
    // InternalNetSpec.g:1618:1: ruleIds returns [EObject current=null] : ( () otherlv_1= 'IDS' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_active_4_0= 'Active' ) )? otherlv_5= 'Level' ( ( (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' ) ) ) otherlv_7= '}' ) ;
    public final EObject ruleIds() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token lv_active_4_0=null;
        Token otherlv_5=null;
        Token lv_level_6_1=null;
        Token lv_level_6_2=null;
        Token lv_level_6_3=null;
        Token lv_level_6_4=null;
        Token otherlv_7=null;


        	enterRule();

        try {
            // InternalNetSpec.g:1624:2: ( ( () otherlv_1= 'IDS' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_active_4_0= 'Active' ) )? otherlv_5= 'Level' ( ( (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' ) ) ) otherlv_7= '}' ) )
            // InternalNetSpec.g:1625:2: ( () otherlv_1= 'IDS' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_active_4_0= 'Active' ) )? otherlv_5= 'Level' ( ( (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' ) ) ) otherlv_7= '}' )
            {
            // InternalNetSpec.g:1625:2: ( () otherlv_1= 'IDS' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_active_4_0= 'Active' ) )? otherlv_5= 'Level' ( ( (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' ) ) ) otherlv_7= '}' )
            // InternalNetSpec.g:1626:3: () otherlv_1= 'IDS' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_active_4_0= 'Active' ) )? otherlv_5= 'Level' ( ( (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' ) ) ) otherlv_7= '}'
            {
            // InternalNetSpec.g:1626:3: ()
            // InternalNetSpec.g:1627:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getIdsAccess().getIdsAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,33,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getIdsAccess().getIDSKeyword_1());
            		
            // InternalNetSpec.g:1637:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalNetSpec.g:1638:4: (lv_name_2_0= RULE_ID )
            {
            // InternalNetSpec.g:1638:4: (lv_name_2_0= RULE_ID )
            // InternalNetSpec.g:1639:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getIdsAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getIdsRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_37); 

            			newLeafNode(otherlv_3, grammarAccess.getIdsAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalNetSpec.g:1659:3: ( (lv_active_4_0= 'Active' ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==45) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalNetSpec.g:1660:4: (lv_active_4_0= 'Active' )
                    {
                    // InternalNetSpec.g:1660:4: (lv_active_4_0= 'Active' )
                    // InternalNetSpec.g:1661:5: lv_active_4_0= 'Active'
                    {
                    lv_active_4_0=(Token)match(input,45,FOLLOW_30); 

                    					newLeafNode(lv_active_4_0, grammarAccess.getIdsAccess().getActiveActiveKeyword_4_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getIdsRule());
                    					}
                    					setWithLastConsumed(current, "active", true, "Active");
                    				

                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,38,FOLLOW_31); 

            			newLeafNode(otherlv_5, grammarAccess.getIdsAccess().getLevelKeyword_5());
            		
            // InternalNetSpec.g:1677:3: ( ( (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' ) ) )
            // InternalNetSpec.g:1678:4: ( (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' ) )
            {
            // InternalNetSpec.g:1678:4: ( (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' ) )
            // InternalNetSpec.g:1679:5: (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' )
            {
            // InternalNetSpec.g:1679:5: (lv_level_6_1= 'Low' | lv_level_6_2= 'Medium' | lv_level_6_3= 'High' | lv_level_6_4= 'Perfect' )
            int alt43=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt43=1;
                }
                break;
            case 16:
                {
                alt43=2;
                }
                break;
            case 17:
                {
                alt43=3;
                }
                break;
            case 39:
                {
                alt43=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }

            switch (alt43) {
                case 1 :
                    // InternalNetSpec.g:1680:6: lv_level_6_1= 'Low'
                    {
                    lv_level_6_1=(Token)match(input,15,FOLLOW_20); 

                    						newLeafNode(lv_level_6_1, grammarAccess.getIdsAccess().getLevelLowKeyword_6_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getIdsRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_6_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:1691:6: lv_level_6_2= 'Medium'
                    {
                    lv_level_6_2=(Token)match(input,16,FOLLOW_20); 

                    						newLeafNode(lv_level_6_2, grammarAccess.getIdsAccess().getLevelMediumKeyword_6_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getIdsRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_6_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:1702:6: lv_level_6_3= 'High'
                    {
                    lv_level_6_3=(Token)match(input,17,FOLLOW_20); 

                    						newLeafNode(lv_level_6_3, grammarAccess.getIdsAccess().getLevelHighKeyword_6_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getIdsRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_6_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:1713:6: lv_level_6_4= 'Perfect'
                    {
                    lv_level_6_4=(Token)match(input,39,FOLLOW_20); 

                    						newLeafNode(lv_level_6_4, grammarAccess.getIdsAccess().getLevelPerfectKeyword_6_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getIdsRule());
                    						}
                    						setWithLastConsumed(current, "level", lv_level_6_4, null);
                    					

                    }
                    break;

            }


            }


            }

            otherlv_7=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getIdsAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIds"


    // $ANTLR start "entryRuleLink"
    // InternalNetSpec.g:1734:1: entryRuleLink returns [EObject current=null] : iv_ruleLink= ruleLink EOF ;
    public final EObject entryRuleLink() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLink = null;


        try {
            // InternalNetSpec.g:1734:45: (iv_ruleLink= ruleLink EOF )
            // InternalNetSpec.g:1735:2: iv_ruleLink= ruleLink EOF
            {
             newCompositeNode(grammarAccess.getLinkRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLink=ruleLink();

            state._fsp--;

             current =iv_ruleLink; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLink"


    // $ANTLR start "ruleLink"
    // InternalNetSpec.g:1741:1: ruleLink returns [EObject current=null] : ( () otherlv_1= 'Link' otherlv_2= '{' otherlv_3= 'H1' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'H2' ( (otherlv_6= RULE_ID ) ) (otherlv_7= 'IDS' ( (otherlv_8= RULE_ID ) ) )? (otherlv_9= 'Firewall' ( (otherlv_10= RULE_ID ) ) )? otherlv_11= '}' ) ;
    public final EObject ruleLink() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;


        	enterRule();

        try {
            // InternalNetSpec.g:1747:2: ( ( () otherlv_1= 'Link' otherlv_2= '{' otherlv_3= 'H1' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'H2' ( (otherlv_6= RULE_ID ) ) (otherlv_7= 'IDS' ( (otherlv_8= RULE_ID ) ) )? (otherlv_9= 'Firewall' ( (otherlv_10= RULE_ID ) ) )? otherlv_11= '}' ) )
            // InternalNetSpec.g:1748:2: ( () otherlv_1= 'Link' otherlv_2= '{' otherlv_3= 'H1' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'H2' ( (otherlv_6= RULE_ID ) ) (otherlv_7= 'IDS' ( (otherlv_8= RULE_ID ) ) )? (otherlv_9= 'Firewall' ( (otherlv_10= RULE_ID ) ) )? otherlv_11= '}' )
            {
            // InternalNetSpec.g:1748:2: ( () otherlv_1= 'Link' otherlv_2= '{' otherlv_3= 'H1' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'H2' ( (otherlv_6= RULE_ID ) ) (otherlv_7= 'IDS' ( (otherlv_8= RULE_ID ) ) )? (otherlv_9= 'Firewall' ( (otherlv_10= RULE_ID ) ) )? otherlv_11= '}' )
            // InternalNetSpec.g:1749:3: () otherlv_1= 'Link' otherlv_2= '{' otherlv_3= 'H1' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'H2' ( (otherlv_6= RULE_ID ) ) (otherlv_7= 'IDS' ( (otherlv_8= RULE_ID ) ) )? (otherlv_9= 'Firewall' ( (otherlv_10= RULE_ID ) ) )? otherlv_11= '}'
            {
            // InternalNetSpec.g:1749:3: ()
            // InternalNetSpec.g:1750:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLinkAccess().getLinkAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,46,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getLinkAccess().getLinkKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_38); 

            			newLeafNode(otherlv_2, grammarAccess.getLinkAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,47,FOLLOW_12); 

            			newLeafNode(otherlv_3, grammarAccess.getLinkAccess().getH1Keyword_3());
            		
            // InternalNetSpec.g:1768:3: ( (otherlv_4= RULE_ID ) )
            // InternalNetSpec.g:1769:4: (otherlv_4= RULE_ID )
            {
            // InternalNetSpec.g:1769:4: (otherlv_4= RULE_ID )
            // InternalNetSpec.g:1770:5: otherlv_4= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLinkRule());
            					}
            				
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_39); 

            					newLeafNode(otherlv_4, grammarAccess.getLinkAccess().getH1HostCrossReference_4_0());
            				

            }


            }

            otherlv_5=(Token)match(input,48,FOLLOW_12); 

            			newLeafNode(otherlv_5, grammarAccess.getLinkAccess().getH2Keyword_5());
            		
            // InternalNetSpec.g:1785:3: ( (otherlv_6= RULE_ID ) )
            // InternalNetSpec.g:1786:4: (otherlv_6= RULE_ID )
            {
            // InternalNetSpec.g:1786:4: (otherlv_6= RULE_ID )
            // InternalNetSpec.g:1787:5: otherlv_6= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLinkRule());
            					}
            				
            otherlv_6=(Token)match(input,RULE_ID,FOLLOW_40); 

            					newLeafNode(otherlv_6, grammarAccess.getLinkAccess().getH2HostCrossReference_6_0());
            				

            }


            }

            // InternalNetSpec.g:1798:3: (otherlv_7= 'IDS' ( (otherlv_8= RULE_ID ) ) )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==33) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalNetSpec.g:1799:4: otherlv_7= 'IDS' ( (otherlv_8= RULE_ID ) )
                    {
                    otherlv_7=(Token)match(input,33,FOLLOW_12); 

                    				newLeafNode(otherlv_7, grammarAccess.getLinkAccess().getIDSKeyword_7_0());
                    			
                    // InternalNetSpec.g:1803:4: ( (otherlv_8= RULE_ID ) )
                    // InternalNetSpec.g:1804:5: (otherlv_8= RULE_ID )
                    {
                    // InternalNetSpec.g:1804:5: (otherlv_8= RULE_ID )
                    // InternalNetSpec.g:1805:6: otherlv_8= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getLinkRule());
                    						}
                    					
                    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_41); 

                    						newLeafNode(otherlv_8, grammarAccess.getLinkAccess().getIdsIdsCrossReference_7_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalNetSpec.g:1817:3: (otherlv_9= 'Firewall' ( (otherlv_10= RULE_ID ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==34) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalNetSpec.g:1818:4: otherlv_9= 'Firewall' ( (otherlv_10= RULE_ID ) )
                    {
                    otherlv_9=(Token)match(input,34,FOLLOW_12); 

                    				newLeafNode(otherlv_9, grammarAccess.getLinkAccess().getFirewallKeyword_8_0());
                    			
                    // InternalNetSpec.g:1822:4: ( (otherlv_10= RULE_ID ) )
                    // InternalNetSpec.g:1823:5: (otherlv_10= RULE_ID )
                    {
                    // InternalNetSpec.g:1823:5: (otherlv_10= RULE_ID )
                    // InternalNetSpec.g:1824:6: otherlv_10= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getLinkRule());
                    						}
                    					
                    otherlv_10=(Token)match(input,RULE_ID,FOLLOW_20); 

                    						newLeafNode(otherlv_10, grammarAccess.getLinkAccess().getFirewallFirewallCrossReference_8_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getLinkAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLink"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000533E40000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000003C000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000001E00000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000001C00010L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001800010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001000010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000003D000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000039000010L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000031000010L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000021000010L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000F89000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000F09000010L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000F01000010L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000E01000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000C01000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000801000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000002109000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000109000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000009000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000008000038000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x00000E0005000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x00000C0005000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000080005000010L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000005000010L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000038000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000204000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000601000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000401000000L});

}