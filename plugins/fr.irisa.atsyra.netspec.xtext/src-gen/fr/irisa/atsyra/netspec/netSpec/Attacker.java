/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * generated by Xtext 2.10.0
 */
package fr.irisa.atsyra.netspec.netSpec;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attacker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getHack <em>Hack</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getCrypto <em>Crypto</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getVirus <em>Virus</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getLogin <em>Login</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getLoginsK <em>Logins K</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getKeysK <em>Keys K</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getHosts <em>Hosts</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.netspec.netSpec.NetSpecPackage#getAttacker()
 * @model
 * @generated
 */
public interface Attacker extends EObject
{
  /**
   * Returns the value of the '<em><b>Hack</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Hack</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Hack</em>' attribute.
   * @see #setHack(String)
   * @see fr.irisa.atsyra.netspec.netSpec.NetSpecPackage#getAttacker_Hack()
   * @model
   * @generated
   */
  String getHack();

  /**
   * Sets the value of the '{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getHack <em>Hack</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Hack</em>' attribute.
   * @see #getHack()
   * @generated
   */
  void setHack(String value);

  /**
   * Returns the value of the '<em><b>Crypto</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Crypto</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Crypto</em>' attribute.
   * @see #setCrypto(String)
   * @see fr.irisa.atsyra.netspec.netSpec.NetSpecPackage#getAttacker_Crypto()
   * @model
   * @generated
   */
  String getCrypto();

  /**
   * Sets the value of the '{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getCrypto <em>Crypto</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Crypto</em>' attribute.
   * @see #getCrypto()
   * @generated
   */
  void setCrypto(String value);

  /**
   * Returns the value of the '<em><b>Virus</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Virus</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Virus</em>' attribute.
   * @see #setVirus(String)
   * @see fr.irisa.atsyra.netspec.netSpec.NetSpecPackage#getAttacker_Virus()
   * @model
   * @generated
   */
  String getVirus();

  /**
   * Sets the value of the '{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getVirus <em>Virus</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Virus</em>' attribute.
   * @see #getVirus()
   * @generated
   */
  void setVirus(String value);

  /**
   * Returns the value of the '<em><b>Login</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Login</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Login</em>' attribute.
   * @see #setLogin(String)
   * @see fr.irisa.atsyra.netspec.netSpec.NetSpecPackage#getAttacker_Login()
   * @model
   * @generated
   */
  String getLogin();

  /**
   * Sets the value of the '{@link fr.irisa.atsyra.netspec.netSpec.Attacker#getLogin <em>Login</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Login</em>' attribute.
   * @see #getLogin()
   * @generated
   */
  void setLogin(String value);

  /**
   * Returns the value of the '<em><b>Logins K</b></em>' reference list.
   * The list contents are of type {@link fr.irisa.atsyra.netspec.netSpec.Login}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Logins K</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Logins K</em>' reference list.
   * @see fr.irisa.atsyra.netspec.netSpec.NetSpecPackage#getAttacker_LoginsK()
   * @model
   * @generated
   */
  EList<Login> getLoginsK();

  /**
   * Returns the value of the '<em><b>Keys K</b></em>' reference list.
   * The list contents are of type {@link fr.irisa.atsyra.netspec.netSpec.Key}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Keys K</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Keys K</em>' reference list.
   * @see fr.irisa.atsyra.netspec.netSpec.NetSpecPackage#getAttacker_KeysK()
   * @model
   * @generated
   */
  EList<Key> getKeysK();

  /**
   * Returns the value of the '<em><b>Hosts</b></em>' reference list.
   * The list contents are of type {@link fr.irisa.atsyra.netspec.netSpec.Host}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Hosts</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Hosts</em>' reference list.
   * @see fr.irisa.atsyra.netspec.netSpec.NetSpecPackage#getAttacker_Hosts()
   * @model
   * @generated
   */
  EList<Host> getHosts();

} // Attacker
