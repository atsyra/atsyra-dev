/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * generated by Xtext 2.10.0
 */
package fr.irisa.atsyra.netspec.netSpec.impl;

import fr.irisa.atsyra.netspec.netSpec.Antivirus;
import fr.irisa.atsyra.netspec.netSpec.Attacker;
import fr.irisa.atsyra.netspec.netSpec.Data;
import fr.irisa.atsyra.netspec.netSpec.File;
import fr.irisa.atsyra.netspec.netSpec.Firewall;
import fr.irisa.atsyra.netspec.netSpec.Goal;
import fr.irisa.atsyra.netspec.netSpec.Host;
import fr.irisa.atsyra.netspec.netSpec.Ids;
import fr.irisa.atsyra.netspec.netSpec.Key;
import fr.irisa.atsyra.netspec.netSpec.Link;
import fr.irisa.atsyra.netspec.netSpec.Login;
import fr.irisa.atsyra.netspec.netSpec.Model;
import fr.irisa.atsyra.netspec.netSpec.NetSpecPackage;
import fr.irisa.atsyra.netspec.netSpec.Session;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getAttacker <em>Attacker</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getGoal <em>Goal</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getHosts <em>Hosts</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getSessions <em>Sessions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getFiles <em>Files</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getLogins <em>Logins</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getKeys <em>Keys</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getFirewalls <em>Firewalls</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getIds <em>Ids</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getLinks <em>Links</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getAntivirus <em>Antivirus</em>}</li>
 *   <li>{@link fr.irisa.atsyra.netspec.netSpec.impl.ModelImpl#getDatas <em>Datas</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model
{
  /**
   * The cached value of the '{@link #getAttacker() <em>Attacker</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAttacker()
   * @generated
   * @ordered
   */
  protected Attacker attacker;

  /**
   * The cached value of the '{@link #getGoal() <em>Goal</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGoal()
   * @generated
   * @ordered
   */
  protected Goal goal;

  /**
   * The cached value of the '{@link #getHosts() <em>Hosts</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHosts()
   * @generated
   * @ordered
   */
  protected EList<Host> hosts;

  /**
   * The cached value of the '{@link #getSessions() <em>Sessions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSessions()
   * @generated
   * @ordered
   */
  protected EList<Session> sessions;

  /**
   * The cached value of the '{@link #getFiles() <em>Files</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFiles()
   * @generated
   * @ordered
   */
  protected EList<File> files;

  /**
   * The cached value of the '{@link #getLogins() <em>Logins</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLogins()
   * @generated
   * @ordered
   */
  protected EList<Login> logins;

  /**
   * The cached value of the '{@link #getKeys() <em>Keys</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeys()
   * @generated
   * @ordered
   */
  protected EList<Key> keys;

  /**
   * The cached value of the '{@link #getFirewalls() <em>Firewalls</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirewalls()
   * @generated
   * @ordered
   */
  protected EList<Firewall> firewalls;

  /**
   * The cached value of the '{@link #getIds() <em>Ids</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIds()
   * @generated
   * @ordered
   */
  protected EList<Ids> ids;

  /**
   * The cached value of the '{@link #getLinks() <em>Links</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLinks()
   * @generated
   * @ordered
   */
  protected EList<Link> links;

  /**
   * The cached value of the '{@link #getAntivirus() <em>Antivirus</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAntivirus()
   * @generated
   * @ordered
   */
  protected EList<Antivirus> antivirus;

  /**
   * The cached value of the '{@link #getDatas() <em>Datas</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDatas()
   * @generated
   * @ordered
   */
  protected EList<Data> datas;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return NetSpecPackage.Literals.MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Attacker getAttacker()
  {
    return attacker;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAttacker(Attacker newAttacker, NotificationChain msgs)
  {
    Attacker oldAttacker = attacker;
    attacker = newAttacker;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, NetSpecPackage.MODEL__ATTACKER, oldAttacker, newAttacker);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAttacker(Attacker newAttacker)
  {
    if (newAttacker != attacker)
    {
      NotificationChain msgs = null;
      if (attacker != null)
        msgs = ((InternalEObject)attacker).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - NetSpecPackage.MODEL__ATTACKER, null, msgs);
      if (newAttacker != null)
        msgs = ((InternalEObject)newAttacker).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - NetSpecPackage.MODEL__ATTACKER, null, msgs);
      msgs = basicSetAttacker(newAttacker, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, NetSpecPackage.MODEL__ATTACKER, newAttacker, newAttacker));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Goal getGoal()
  {
    return goal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGoal(Goal newGoal, NotificationChain msgs)
  {
    Goal oldGoal = goal;
    goal = newGoal;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, NetSpecPackage.MODEL__GOAL, oldGoal, newGoal);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGoal(Goal newGoal)
  {
    if (newGoal != goal)
    {
      NotificationChain msgs = null;
      if (goal != null)
        msgs = ((InternalEObject)goal).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - NetSpecPackage.MODEL__GOAL, null, msgs);
      if (newGoal != null)
        msgs = ((InternalEObject)newGoal).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - NetSpecPackage.MODEL__GOAL, null, msgs);
      msgs = basicSetGoal(newGoal, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, NetSpecPackage.MODEL__GOAL, newGoal, newGoal));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Host> getHosts()
  {
    if (hosts == null)
    {
      hosts = new EObjectContainmentEList<Host>(Host.class, this, NetSpecPackage.MODEL__HOSTS);
    }
    return hosts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Session> getSessions()
  {
    if (sessions == null)
    {
      sessions = new EObjectContainmentEList<Session>(Session.class, this, NetSpecPackage.MODEL__SESSIONS);
    }
    return sessions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<File> getFiles()
  {
    if (files == null)
    {
      files = new EObjectContainmentEList<File>(File.class, this, NetSpecPackage.MODEL__FILES);
    }
    return files;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Login> getLogins()
  {
    if (logins == null)
    {
      logins = new EObjectContainmentEList<Login>(Login.class, this, NetSpecPackage.MODEL__LOGINS);
    }
    return logins;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Key> getKeys()
  {
    if (keys == null)
    {
      keys = new EObjectContainmentEList<Key>(Key.class, this, NetSpecPackage.MODEL__KEYS);
    }
    return keys;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Firewall> getFirewalls()
  {
    if (firewalls == null)
    {
      firewalls = new EObjectContainmentEList<Firewall>(Firewall.class, this, NetSpecPackage.MODEL__FIREWALLS);
    }
    return firewalls;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Ids> getIds()
  {
    if (ids == null)
    {
      ids = new EObjectContainmentEList<Ids>(Ids.class, this, NetSpecPackage.MODEL__IDS);
    }
    return ids;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Link> getLinks()
  {
    if (links == null)
    {
      links = new EObjectContainmentEList<Link>(Link.class, this, NetSpecPackage.MODEL__LINKS);
    }
    return links;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Antivirus> getAntivirus()
  {
    if (antivirus == null)
    {
      antivirus = new EObjectContainmentEList<Antivirus>(Antivirus.class, this, NetSpecPackage.MODEL__ANTIVIRUS);
    }
    return antivirus;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Data> getDatas()
  {
    if (datas == null)
    {
      datas = new EObjectContainmentEList<Data>(Data.class, this, NetSpecPackage.MODEL__DATAS);
    }
    return datas;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case NetSpecPackage.MODEL__ATTACKER:
        return basicSetAttacker(null, msgs);
      case NetSpecPackage.MODEL__GOAL:
        return basicSetGoal(null, msgs);
      case NetSpecPackage.MODEL__HOSTS:
        return ((InternalEList<?>)getHosts()).basicRemove(otherEnd, msgs);
      case NetSpecPackage.MODEL__SESSIONS:
        return ((InternalEList<?>)getSessions()).basicRemove(otherEnd, msgs);
      case NetSpecPackage.MODEL__FILES:
        return ((InternalEList<?>)getFiles()).basicRemove(otherEnd, msgs);
      case NetSpecPackage.MODEL__LOGINS:
        return ((InternalEList<?>)getLogins()).basicRemove(otherEnd, msgs);
      case NetSpecPackage.MODEL__KEYS:
        return ((InternalEList<?>)getKeys()).basicRemove(otherEnd, msgs);
      case NetSpecPackage.MODEL__FIREWALLS:
        return ((InternalEList<?>)getFirewalls()).basicRemove(otherEnd, msgs);
      case NetSpecPackage.MODEL__IDS:
        return ((InternalEList<?>)getIds()).basicRemove(otherEnd, msgs);
      case NetSpecPackage.MODEL__LINKS:
        return ((InternalEList<?>)getLinks()).basicRemove(otherEnd, msgs);
      case NetSpecPackage.MODEL__ANTIVIRUS:
        return ((InternalEList<?>)getAntivirus()).basicRemove(otherEnd, msgs);
      case NetSpecPackage.MODEL__DATAS:
        return ((InternalEList<?>)getDatas()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case NetSpecPackage.MODEL__ATTACKER:
        return getAttacker();
      case NetSpecPackage.MODEL__GOAL:
        return getGoal();
      case NetSpecPackage.MODEL__HOSTS:
        return getHosts();
      case NetSpecPackage.MODEL__SESSIONS:
        return getSessions();
      case NetSpecPackage.MODEL__FILES:
        return getFiles();
      case NetSpecPackage.MODEL__LOGINS:
        return getLogins();
      case NetSpecPackage.MODEL__KEYS:
        return getKeys();
      case NetSpecPackage.MODEL__FIREWALLS:
        return getFirewalls();
      case NetSpecPackage.MODEL__IDS:
        return getIds();
      case NetSpecPackage.MODEL__LINKS:
        return getLinks();
      case NetSpecPackage.MODEL__ANTIVIRUS:
        return getAntivirus();
      case NetSpecPackage.MODEL__DATAS:
        return getDatas();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case NetSpecPackage.MODEL__ATTACKER:
        setAttacker((Attacker)newValue);
        return;
      case NetSpecPackage.MODEL__GOAL:
        setGoal((Goal)newValue);
        return;
      case NetSpecPackage.MODEL__HOSTS:
        getHosts().clear();
        getHosts().addAll((Collection<? extends Host>)newValue);
        return;
      case NetSpecPackage.MODEL__SESSIONS:
        getSessions().clear();
        getSessions().addAll((Collection<? extends Session>)newValue);
        return;
      case NetSpecPackage.MODEL__FILES:
        getFiles().clear();
        getFiles().addAll((Collection<? extends File>)newValue);
        return;
      case NetSpecPackage.MODEL__LOGINS:
        getLogins().clear();
        getLogins().addAll((Collection<? extends Login>)newValue);
        return;
      case NetSpecPackage.MODEL__KEYS:
        getKeys().clear();
        getKeys().addAll((Collection<? extends Key>)newValue);
        return;
      case NetSpecPackage.MODEL__FIREWALLS:
        getFirewalls().clear();
        getFirewalls().addAll((Collection<? extends Firewall>)newValue);
        return;
      case NetSpecPackage.MODEL__IDS:
        getIds().clear();
        getIds().addAll((Collection<? extends Ids>)newValue);
        return;
      case NetSpecPackage.MODEL__LINKS:
        getLinks().clear();
        getLinks().addAll((Collection<? extends Link>)newValue);
        return;
      case NetSpecPackage.MODEL__ANTIVIRUS:
        getAntivirus().clear();
        getAntivirus().addAll((Collection<? extends Antivirus>)newValue);
        return;
      case NetSpecPackage.MODEL__DATAS:
        getDatas().clear();
        getDatas().addAll((Collection<? extends Data>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case NetSpecPackage.MODEL__ATTACKER:
        setAttacker((Attacker)null);
        return;
      case NetSpecPackage.MODEL__GOAL:
        setGoal((Goal)null);
        return;
      case NetSpecPackage.MODEL__HOSTS:
        getHosts().clear();
        return;
      case NetSpecPackage.MODEL__SESSIONS:
        getSessions().clear();
        return;
      case NetSpecPackage.MODEL__FILES:
        getFiles().clear();
        return;
      case NetSpecPackage.MODEL__LOGINS:
        getLogins().clear();
        return;
      case NetSpecPackage.MODEL__KEYS:
        getKeys().clear();
        return;
      case NetSpecPackage.MODEL__FIREWALLS:
        getFirewalls().clear();
        return;
      case NetSpecPackage.MODEL__IDS:
        getIds().clear();
        return;
      case NetSpecPackage.MODEL__LINKS:
        getLinks().clear();
        return;
      case NetSpecPackage.MODEL__ANTIVIRUS:
        getAntivirus().clear();
        return;
      case NetSpecPackage.MODEL__DATAS:
        getDatas().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case NetSpecPackage.MODEL__ATTACKER:
        return attacker != null;
      case NetSpecPackage.MODEL__GOAL:
        return goal != null;
      case NetSpecPackage.MODEL__HOSTS:
        return hosts != null && !hosts.isEmpty();
      case NetSpecPackage.MODEL__SESSIONS:
        return sessions != null && !sessions.isEmpty();
      case NetSpecPackage.MODEL__FILES:
        return files != null && !files.isEmpty();
      case NetSpecPackage.MODEL__LOGINS:
        return logins != null && !logins.isEmpty();
      case NetSpecPackage.MODEL__KEYS:
        return keys != null && !keys.isEmpty();
      case NetSpecPackage.MODEL__FIREWALLS:
        return firewalls != null && !firewalls.isEmpty();
      case NetSpecPackage.MODEL__IDS:
        return ids != null && !ids.isEmpty();
      case NetSpecPackage.MODEL__LINKS:
        return links != null && !links.isEmpty();
      case NetSpecPackage.MODEL__ANTIVIRUS:
        return antivirus != null && !antivirus.isEmpty();
      case NetSpecPackage.MODEL__DATAS:
        return datas != null && !datas.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ModelImpl
