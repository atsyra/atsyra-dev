/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.xtext.ui.handlers;

import org.eclipse.compare.internal.merge.DocumentMerger.Diff;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.IContainer;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.IResourceDescription.Manager;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.resource.IResourceServiceProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.impl.ResourceDescriptionsProvider;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.findrefs.FindReferencesHandler;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import com.google.inject.Inject;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Value;

public class InspectResourceHandler extends AbstractHandler {

	
    @Inject IResourceServiceProvider.Registry rspr;
    @Inject IQualifiedNameConverter converter;
    @Inject EObjectAtOffsetHelper eObjectAtOffsetHelper;
    
    @Inject ResourceDescriptionsProvider indexProvider;
    @Inject IContainer.Manager manager;
     
    public void printExportedObjects(Resource resource) {
    	System.out.println("ExportedObjects :");
        IResourceServiceProvider resServiceProvider = rspr.getResourceServiceProvider(resource.getURI());
        Manager manager = resServiceProvider.getResourceDescriptionManager();
        IResourceDescription description = manager.getResourceDescription(resource);
        for (IEObjectDescription eod : description.getExportedObjects()) {
            System.out.println(converter.toString(eod.getQualifiedName()));
        }
    }
    
    
    public void printVisibleResources(Resource resource, IResourceDescriptions index) {
    	System.out.println("getAllResourceDescriptions :");
        IResourceDescription descr = index.getResourceDescription(resource.getURI());
        for (IResourceDescription rds : index.getAllResourceDescriptions()) {
        	System.out.println(rds.toString());
        }
        System.out.println("VisibleResources :");
        for (IContainer visibleContainer : manager.getVisibleContainers(descr, index)) {
            for (IResourceDescription visibleResourceDesc : visibleContainer.getResourceDescriptions()) {
            	System.out.println(visibleResourceDesc.getURI());
            }
        }
    }
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		//ISelection selection = HandlerUtil.getCurrentSelection(event);
        
       /* if (selection != null && selection instanceof IStructuredSelection ) {
        	IFile file = (IFile) ((IStructuredSelection) selection).getFirstElement();
            URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
        }
        FindReferencesHandler f;*/
		
		XtextEditor editor = EditorUtils.getActiveXtextEditor(event);
		if (editor != null) {
			final ITextSelection selection = (ITextSelection) editor.getSelectionProvider().getSelection();
			editor.getDocument().priorityReadOnly(new IUnitOfWork.Void<XtextResource>() {
				@Override
				public void process(XtextResource state) throws Exception {
					//EObject target = eObjectAtOffsetHelper.resolveElementAt(state, selection.getOffset());
					printExportedObjects(state);
					
					//indexProvider.
					printVisibleResources(state, indexProvider.getResourceDescriptions(state));
					//findReferences(target);
				}
			});
		}
		return null;
	}

}
