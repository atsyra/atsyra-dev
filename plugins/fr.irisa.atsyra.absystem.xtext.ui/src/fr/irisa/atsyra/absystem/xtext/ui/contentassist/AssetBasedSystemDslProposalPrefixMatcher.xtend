package fr.irisa.atsyra.absystem.xtext.ui.contentassist

import org.eclipse.xtext.ui.editor.contentassist.FQNPrefixMatcher

class AssetBasedSystemDslProposalPrefixMatcher extends FQNPrefixMatcher {
	
	override isCandidateMatchingPrefix(String name, String prefix) {
		if(prefix=="\"platform:"||prefix=="\"classpath:"||prefix=="\"") {return true}
		super.isCandidateMatchingPrefix(name,prefix)	
	}
	
}