package fr.irisa.atsyra.absystem.xtext.ui.hover

import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider
import org.eclipse.emf.ecore.EObject
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature
import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils
import com.google.inject.Inject
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.naming.IQualifiedNameConverter
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType
import fr.irisa.atsyra.absystem.model.absystem.Parameter
import org.eclipse.xtext.naming.QualifiedName

class AssetBasedSystemDslEObjectHoverProvider extends DefaultEObjectHoverProvider {
	@Inject
	IQualifiedNameProvider qualifiedNameProvider;
	
	@Inject
	IQualifiedNameConverter qualifiedNameConverter;
	
	override getFirstLine(EObject o) {
        getFirstLineDispatch(o)
    }
    
    def dispatch getFirstLineDispatch(EObject eobj) {
    	val qualifiedName = qualifiedNameProvider.getFullyQualifiedName(eobj)
    	if(qualifiedName === null) {
    		super.getFirstLine(eobj);
    	} else {
    		eobj?.eClass?.name + " <b>" + qualifiedNameConverter.toString(qualifiedName) + "</b>"
    	}
    }
    
    def dispatch getFirstLineDispatch(PrimitiveDataType primitiveDataType) {
    	val result = new StringBuilder
    	result.append(primitiveDataType.eClass?.name)
    	result.append(" <b>").append(qualifiedNameConverter.toString(qualifiedNameProvider.getFullyQualifiedName(primitiveDataType))).append("</b>")
    	return result.toString
    }
    
    def dispatch getFirstLineDispatch(AssetType assetType) {
    	val result = new StringBuilder
    	if(assetType.isAbstract) {
    		result.append("abstract ")
    	}
    	result.append(assetType.eClass?.name)
    	result.append(" <b>").append(qualifiedNameConverter.toString(qualifiedNameProvider.getFullyQualifiedName(assetType))).append("</b>")
    	return result.toString
    }
    
    def dispatch getFirstLineDispatch(AssetTypeAspect assetTypeAspect) {
    	assetTypeAspect?.eClass?.name + " of <b>" + assetTypeAspect?.baseAssetType?.name + "</b>"
    }
    
    def dispatch getFirstLineDispatch(AssetTypeFeature assetTypeFeature) {
    	val result = new StringBuilder
    	result.append(assetTypeFeature.eClass?.name).append(" ")
    	if(assetTypeFeature.eContainer instanceof AbstractAssetType) {
    		val baseType = ABSUtils.getBaseAssetType(assetTypeFeature.eContainer as AbstractAssetType)
    		val qualifiedNameBuilder = new QualifiedName.Builder(2)
    		qualifiedNameBuilder.add(baseType?.name)
    		qualifiedNameBuilder.add(assetTypeFeature.name)
    		result.append(qualifiedNameConverter.toString(qualifiedNameBuilder.build))
    	} else {
    		result.append(assetTypeFeature.name)
    	}
    	return result.toString
    }
    
    def dispatch getFirstLineDispatch(Parameter eobj) {
		super.getFirstLine(eobj);
    }
}