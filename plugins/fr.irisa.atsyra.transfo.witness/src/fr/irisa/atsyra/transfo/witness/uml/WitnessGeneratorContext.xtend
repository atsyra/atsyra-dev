package fr.irisa.atsyra.transfo.witness.uml

import org.eclipse.core.resources.IFile
import java.util.Properties

class WitnessGeneratorContext {

	public IFile traceFile
	
	public Properties properties
	
	/** some generation parameters */
	public boolean autoNumberActions = true
	/** if true, transitions that does a move from one location to another will be shortened following this scheme 
	 * thief_goes_from_exterior_to_corridor2_by_window2 => thief_get_through_window2
	 */
	public boolean shorterLocationTransition = true
	/** if true, transitions related the use of an item will be highlighted
	 */
	public boolean highlightItemUsageAction = true
	
	new(IFile traceFile){
		this.traceFile = traceFile 
		this.properties = new Properties()
		this.properties.load(traceFile.contents)
	}

	def public String getValueFromTrace( String variableKind, String galId){
		val varKindSet = properties.entrySet.filter[entry | (entry.key as String).startsWith(variableKind+".")]
		val entryFound = varKindSet.findFirst[entry | entry.value !== null && (entry.value as String).equals(galId)].key as String
		entryFound.replaceFirst(variableKind+".", "")
		
	}
	
	def public String getFirstAttackerName(){
		properties.keySet.map[k | k as String].findFirst[key|  key.startsWith("attacker.") ].replaceFirst("attacker.", "")
		
	}	
}