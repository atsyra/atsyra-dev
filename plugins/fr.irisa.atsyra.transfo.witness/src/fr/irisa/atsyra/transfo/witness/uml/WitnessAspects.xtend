package fr.irisa.atsyra.transfo.witness.uml

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.witness.witness.StepState
import fr.irisa.atsyra.witness.witness.AbstractStepStates

import static extension fr.irisa.atsyra.transfo.witness.uml.StepStateAspects.*

@Aspect (className=AbstractStepStates)
class AbstractStepStatesAspects {
	def public int getLocation() {
		// assume all the states share the same location
		_self.stepState.head.location
	}
	
	def public String getAttackerLocationValue(WitnessGeneratorContext context) {
		// assume all the states share the same location
		_self.stepState.head.getAttackerLocationValue(context)
	}
}

@Aspect (className=StepState)
class StepStateAspects {
	def public int getLocation() {
		val varstate = _self.varState.findFirst[v | v.name == "attacker.location"];
		if(varstate === null) 0 else varstate.value
	}
	
	/**
	 * variable name will typically be "attacker.location"
	 */
	def public String getAttackerLocationValue(WitnessGeneratorContext context) {
		val varstate = _self.varState.findFirst[v | v.name == context.getFirstAttackerName()+".location"];
		if(varstate === null) ""+0 else context.getValueFromTrace( "zone", ""+varstate.value)
	}

}