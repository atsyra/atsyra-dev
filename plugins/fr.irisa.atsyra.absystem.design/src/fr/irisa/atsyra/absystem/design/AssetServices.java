/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.design;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.Iterators;
import com.google.common.collect.UnmodifiableIterator;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant;
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression;
import fr.irisa.atsyra.absystem.model.absystem.EnumConstant;
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.IntConstant;
import fr.irisa.atsyra.absystem.model.absystem.StringConstant;
import fr.irisa.atsyra.absystem.model.absystem.Version;
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.model.absystem.util.PrimitiveDataTypeSwitch;

public class AssetServices {
	// Services written in xtend aren't correctly detected by Sirius, let's wrap
	// them in simple java
	public AssetType getAssetTypeAny(Asset self) {
		return new XtendServices().getAssetTypeAny(self);
	}

	public List<Asset> containedAssets(final Asset _self) {
		return new XtendServices().containedAssets(_self);
	}

	public List<Asset> containedAssets(final AssetGroup _self) {
		return new XtendServices().containedAssets(_self);
	}

	public Asset containerAsset(final Asset _self) {
		return new XtendServices().containerAsset(_self);
	}

	public boolean assetIsContained(EObject self) {
		if (self instanceof Asset && containerAsset((Asset) self) != null) {
			return true;
		}
		return false;
	}

	public AssetTypeReference findCompatibleContainmentRef(final Asset source, final Asset target) {
		return new XtendServices().findFirstCompatibleContainmentRef(source, target);
	}

	public void deleteAllRelatedLinks(final Asset asset) {
		new XtendServices().deleteAllRelatedLinks(asset);
	}

	public void deleteContainerLink(final Asset asset) {
		new XtendServices().deleteContainerLink(asset);
	}

	public void moveAssetInContainer(final Asset asset, final EObject newContainer) {
		new XtendServices().moveAssetInAssetContainer(asset, newContainer);
	}

	/**
	 * find all AssetTypes
	 * 
	 * @return
	 */
	public List<AssetType> getAllAssetTypes(Asset baseAsset) {
		List<AssetType> result = new ArrayList<>();
		UnmodifiableIterator<AssetType> allAssetTypeIterator = Iterators
				.filter(baseAsset.eResource().getResourceSet().getAllContents(), AssetType.class);
		// maybe find a way to sort them ?
		Iterators.addAll(result, allAssetTypeIterator);
		return result;
	}

	/**
	 * find all concrete assetTypes
	 * 
	 * @return
	 */
	public List<AssetType> getAllConcreteAssetTypes(Asset baseAsset) {
		List<AssetType> result = new ArrayList<>();
		UnmodifiableIterator<AssetType> allAssetTypeIterator = Iterators
				.filter(baseAsset.eResource().getResourceSet().getAllContents(), AssetType.class);
		UnmodifiableIterator<AssetType> allConcreteAssetTypeIterator = Iterators.filter(allAssetTypeIterator,
				assetType -> !assetType.isAbstract());
		// maybe find a way to sort them ?
		Iterators.addAll(result, allConcreteAssetTypeIterator);
		return result;
	}

	public String getAssetAttributeValueLabel(AssetAttributeValue aav) {
		StringBuilder sb = new StringBuilder();
		if (aav.getAttributeType() != null) {
			sb.append(aav.getAttributeType().getName() + " = ");
		}
		if(aav.isCollection()) {
			sb.append("[");
			boolean notfirst = false;
			for (ConstantExpression value : aav.getValues()) {
				if(notfirst) {
					sb.append(", ");
				}
				notfirst = true;
				sb.append(Services.xtextPrettyPrint(value));
			}
			sb.append("]");
		} else {
			if(!aav.getValues().isEmpty()) {
				sb.append(Services.xtextPrettyPrint(aav.getValues().get(0)));
			}
		}
		return sb.toString();
	}

	/**
	 * compute a label that is longer or shorter depending if the Asset is displayed
	 * as an inner Asset (containedBy) or directly in a subgroup
	 * 
	 * @param a
	 * @return
	 */
	public String getAssetSmartLabel(Asset a) {
		StringBuilder sb = new StringBuilder();
		Asset containingAsset = new XtendServices().containerAsset(a);
		if (containingAsset != null && !a.eContainer().equals(containingAsset.eContainer())) {
			sb.append(ABSUtils.getQualifiedName(a, "::").get());
		} else {
			sb.append(a.getName());
		}
		sb.append(" : ");
		sb.append(a.getAssetType().getName());
		return sb.toString();
	}

	public List<AssetTypeAttribute> getAllPossibleAssetTypeAttributeForAsset(Asset asset) {
		Set<AssetType> assetTypes = new HashSet<AssetType>();
		assetTypes.add(asset.getAssetType());
		assetTypes.addAll(ABSUtils.getSupertypesHierarchyWithAny(asset.getAssetType()));
		List<AssetTypeAttribute> assetTypeAttributes = assetTypes.stream()
				.flatMap(t -> t.getAssetTypeAttributes().stream()).collect(Collectors.toList());
		return assetTypeAttributes;
	}

	public List<AssetTypeAttribute> getPossibleAssetTypeAttributeForAsset(Asset asset) {
		List<AssetTypeAttribute> alreadyDefinedAssetTypeAttributes = asset.getAssetAttributeValues().stream()
				.map(f -> f.getAttributeType()).collect(Collectors.toList());
		List<AssetTypeAttribute> allAssetTypeAttributes = getAllPossibleAssetTypeAttributeForAsset(asset);
		List<AssetTypeAttribute> assetTypeAttributes = allAssetTypeAttributes.stream()
				.filter(ata -> !alreadyDefinedAssetTypeAttributes.contains(ata)).collect(Collectors.toList());
		return assetTypeAttributes;
	}
	
	
	public List<AssetTypeReference> getAllPossibleAssetTypeRefenceForAsset(Asset asset) {
		Set<AssetType> assetTypes = new HashSet<AssetType>();
		assetTypes.add(asset.getAssetType());
		assetTypes.addAll(ABSUtils.getSupertypesHierarchyWithAny(asset.getAssetType()));
		List<AssetTypeReference> assetTypeReferences = assetTypes.stream()
				.flatMap(t -> t.getAssetTypeProperties().stream()).collect(Collectors.toList());
		return assetTypeReferences;
	}
	
	
	/** 
	 * retrun the list of possible reference between 2 Asset
	 * @param self
	 * @param target
	 * @return
	 */
	public List<AssetTypeReference> getAllPossibleAssetTypeRefenceForAsset(Asset self, Asset target) {
		// type hierarchy of the target
		Set<AssetType> targetAssetTypes = new HashSet<AssetType>();
		targetAssetTypes.add(target.getAssetType());
		targetAssetTypes.addAll(ABSUtils.getSupertypesHierarchyWithAny(target.getAssetType()));
		return getAllPossibleAssetTypeRefenceForAsset(self).stream().filter(atr -> targetAssetTypes.contains(atr.getPropertyType()))
			.collect(Collectors.toList());
	}
	
	public boolean canConnectToAsset(Asset self, Asset target) {
		return !getAllPossibleAssetTypeRefenceForAsset(self, target).isEmpty();
	}

	/**
	 * Create constant values depending on the AssetAttributeType
	 * 
	 * @param container
	 */
	public void createDefaultConstantExpression(AssetAttributeValue container) {
		ConstantExpression cExp;
		if (container.getAttributeType().getAttributeType() instanceof EnumDataType) {
			cExp = AbsystemFactory.eINSTANCE.createEnumConstant();
			EnumDataType edt = (EnumDataType) container.getAttributeType().getAttributeType();
			EnumLiteral enumLit = edt.getEnumLiteral().get(0);
			if (!container.getAttributeType().getDefaultValues().isEmpty()) {
				enumLit = ((EnumConstant) container.getAttributeType().getDefaultValues().get(0)).getValue();
			}
			((EnumConstant) cExp).setValue(enumLit);

		} else {
			PrimitiveDataTypeSwitch<ConstantExpression> pdtSwitch = new PrimitiveDataTypeSwitch<ConstantExpression>() {
				@Override
				public ConstantExpression caseBoolean() {
					BooleanConstant c = AbsystemFactory.eINSTANCE.createBooleanConstant();
					String defaultVal = "false";
					if (!container.getAttributeType().getDefaultValues().isEmpty()) {
						defaultVal = ((BooleanConstant) container.getAttributeType().getDefaultValues().get(0))
								.getValue();
					}
					c.setValue(defaultVal);
					return c;
				}

				@Override
				public ConstantExpression caseInteger() {
					IntConstant c = AbsystemFactory.eINSTANCE.createIntConstant();

					Integer defaultVal = 0;
					if (!container.getAttributeType().getDefaultValues().isEmpty()) {
						defaultVal = ((IntConstant) container.getAttributeType().getDefaultValues().get(0)).getValue();
					}
					c.setValue(defaultVal);
					return c;
				}

				@Override
				public ConstantExpression caseString() {
					StringConstant c = AbsystemFactory.eINSTANCE.createStringConstant();
					String defaultVal = "";
					if (!container.getAttributeType().getDefaultValues().isEmpty()) {
						defaultVal = ((StringConstant) container.getAttributeType().getDefaultValues().get(0))
								.getValue();
					}
					c.setValue(defaultVal);
					return c;
				}

				@Override
				public ConstantExpression caseVersion() {
					VersionConstant c = AbsystemFactory.eINSTANCE.createVersionConstant();
					Version defaultVal = new Version();
					if (!container.getAttributeType().getDefaultValues().isEmpty()) {
						defaultVal = ((VersionConstant) container.getAttributeType().getDefaultValues().get(0))
								.getValue();
					}
					c.setValue(defaultVal);
					return c;
				}
			};
			cExp = pdtSwitch.doSwitch(container.getAttributeType().getAttributeType());
		}
		ECollections.setEList(container.getValues(), List.of(cExp));
	}

}
