/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.design;

import java.util.List;

import fr.irisa.atsyra.absystem.model.absystem.AssetLink;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;

public class LinkServices {

	/**
	 * Based on the main link info, add, correct or remove opposite info of the link
	 * 
	 * @param self
	 */
	public void fixOpposite(AssetLink self) {
		if (self.getReferenceType() != null
				&& self.getReferenceType().getOppositeTypeReference() != self.getOppositeReferenceType()) {
			self.setOppositeReferenceType(self.getReferenceType().getOppositeTypeReference());
		}
	}
	
	
	public List<AssetTypeReference> getAllPossibleAssetTypeRefence(AssetLink self) {
		return new AssetServices().getAllPossibleAssetTypeRefenceForAsset(self.getSourceAsset(), self.getTargetAsset());
	}
}