package fr.irisa.atsyra.absystem.design

import fr.irisa.atsyra.absystem.model.absystem.AssetType
import org.eclipse.emf.ecore.EObject
import fr.irisa.atsyra.absystem.model.absystem.Asset
import java.util.List
import fr.irisa.atsyra.absystem.model.absystem.AssetLink
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue
import fr.irisa.atsyra.absystem.model.absystem.StringConstant
import fr.irisa.atsyra.absystem.model.absystem.util.PrimitiveDataTypeSwitch
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType
import fr.irisa.atsyra.absystem.model.absystem.IntConstant
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant
import fr.irisa.atsyra.absystem.model.absystem.Version
import fr.irisa.atsyra.absystem.model.absystem.EnumConstant
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.emf.ecore.resource.Resource
import java.util.LinkedHashSet
import org.eclipse.emf.common.util.URI
import fr.irisa.atsyra.absystem.model.absystem.Import
import org.eclipse.emf.common.util.ECollections

class XtendServices {

	// TODO _self.eResource().getResourceSet().getAllContents().filter(XXX) is probably not very efficient on large models and can be optimized 
	
	def AssetType getAssetTypeAny(EObject _self) {
		
		val res = _self.eResource().getResourceSet().allContents.filter(AssetType).findFirst [ o |
			o.name == 'Any'
		]
		if(res === null) {
			// try harder by loading resources referenced via import statement
			collectImportUris(_self.eResource(),new LinkedHashSet<URI>(5))
			return getAssetTypeAny(_self)
		}
		return res as AssetType
	}
	
	def LinkedHashSet<URI> collectImportUris(Resource resource, LinkedHashSet<URI> uniqueImportURIs) {
		resource.allContents.filter(Import).filter[uri | !uri.importURI.nullOrEmpty].forEach[ uri |
					var includedUri = URI.createURI(uri.importURI)
					if (!resource.URI.isRelative) {
						includedUri = includedUri.resolve(resource.URI)
					}
					if (uniqueImportURIs.add(includedUri) && EcoreUtil2.isValidUri(resource, includedUri)) {
						collectImportUris(resource.getResourceSet().getResource(includedUri, true), uniqueImportURIs)
					}
				]
		uniqueImportURIs
	}
	
	def List<Asset> containedAssets(Asset _self) {
		val List<Asset> result = _self.eResource().getResourceSet().getAllContents().filter(AssetLink).filter [ al |
			al.referenceType !== null &&
			al.referenceType.isContainer &&
			al.targetAsset === _self 
		].map[al | al.sourceAsset].toList
		return result
	}
	
	def List<Asset> containedAssets(AssetGroup _self) {
		val List<Asset> containedByOtherAsset = _self.eResource().getResourceSet().getAllContents().filter(AssetLink).filter [ al |
			al.referenceType !== null &&
			al.referenceType.isContainer 
		].map[al | al.sourceAsset].toList
		val result = _self.elements.filter(Asset).filter[a | ! containedByOtherAsset.contains(a)].toList
		return result
	}
	
	def Asset containerAsset(Asset _self) {
		val AssetLink containerLink = _self.eResource().getResourceSet().getAllContents().filter(AssetLink).findFirst[ al |
			al.referenceType !== null &&
			al.referenceType.isContainer &&
			al.sourceAsset === _self 
		]
		if(containerLink !== null) {
			return containerLink.targetAsset
		} else 	return null
	}
	
	
	/**
	 * Find the first known containment AssetTypeReference that can accept these source and target Assets 
	 */
	def AssetTypeReference findFirstCompatibleContainmentRef(Asset source, Asset target) {
		val sourceTypes = ABSUtils.getSupertypesHierarchyWithAny(source.assetType)
		val targetTypes = ABSUtils.getSupertypesHierarchyWithAny(target.assetType)
		val AssetTypeReference result = source.eResource().getResourceSet().getAllContents().filter(AssetTypeReference).findFirst[ assetTypeReference | 
			assetTypeReference.isContainer &&
			sourceTypes.contains(assetTypeReference.eContainer) && 
			targetTypes.contains(assetTypeReference.propertyType)
		]
		
		return result
	}
	
	
	/**
	 * Find all links related to the given Asset
	 */
	def List<AssetLink> getAllRelatedLinks(Asset asset) {
		val relatedLinks = asset.eResource().getResourceSet().getAllContents().filter(AssetLink).filter[ al |
			al.sourceAsset === asset ||
			al.targetAsset === asset
		].toList
		return relatedLinks
	}
	
	
	/**
	 * Delete all links that refers to the given Asset (useful to clean a model)
	 */
	def void deleteAllRelatedLinks(Asset asset){
		val List<AssetLink> relatedLinks = asset.getAllRelatedLinks
		relatedLinks.forEach[ al |
			val AssetGroup g = al.eContainer as AssetGroup
			g.elements.remove(al)
		]
	}
	
	/**
	 * Delete the link that indicates the container of the given Asset 
	 */
	def void deleteContainerLink(Asset asset){
		val AssetLink containerLink = asset.eResource().getResourceSet().getAllContents().filter(AssetLink).findFirst[ al |
			al.referenceType !== null &&
			al.referenceType.isContainer &&
			al.sourceAsset === asset 
		]
		val AssetGroup g = containerLink.eContainer as AssetGroup
		g.elements.remove(containerLink)
	}
	
	
	/**
	 * move Asset from one Asset container to another 
	 */
	def void moveAssetInAssetContainer(Asset asset, EObject newContainer){
		val AssetLink previousContainerLink = asset.eResource().getResourceSet().getAllContents().filter(AssetLink).findFirst[ al |
			al.referenceType !== null &&
			al.referenceType.isContainer &&
			al.sourceAsset === asset 
		]
		if(newContainer instanceof Asset) {
			// move to an Asset
			var AssetLink containerLink
			if(previousContainerLink !== null) {
				// adapt existing AssetLink
				containerLink = previousContainerLink
			} else {
				// create new AssetLink
				containerLink = AbsystemFactory.eINSTANCE.createAssetLink
				containerLink.sourceAsset = asset
				val AssetGroup g = asset.eContainer as AssetGroup
				g.elements.add(containerLink)
			}
			containerLink.targetAsset = newContainer as Asset
			containerLink.referenceType = asset.findFirstCompatibleContainmentRef(newContainer as Asset)
		}
	}
	
	
	/**
	 * remove Asset from Asset container
	 */
	def void removeAssetFromAssetContainer(Asset asset){
		val AssetLink previousContainerLink = asset.eResource().getResourceSet().getAllContents().filter(AssetLink).findFirst[ al |
			al.referenceType !== null &&
			al.referenceType.isContainer &&
			al.sourceAsset === asset 
		]
		if(previousContainerLink !== null) {
			val AssetGroup g = previousContainerLink.eContainer as AssetGroup
			g.elements.remove(previousContainerLink)
		}
	}
	
	def String toDisplayString(Multiplicity _self) {
		switch (_self) {
			case ONE: {
				"1"
			}
			case ONE_OR_MANY: {
				"1..*"
			}
			case ZERO_OR_MANY: {
				"0..*"
			}
			case ZERO_OR_ONE: {
				"0..1"
			}
		}
	}
	
	def Multiplicity getMultiplicityFromString(String string) {
		switch (string) {
			case "1": {
				Multiplicity.ONE
			}
			case "1..*": {
				Multiplicity.ONE_OR_MANY
			}
			case "0..*": {
				Multiplicity.ZERO_OR_MANY
			}
			case "0..1": {
				Multiplicity.ZERO_OR_ONE
			}
		}
	}
	
	def PrimitiveDataType getPrimitiveDataType(EObject modelElement, String typeName) {
		modelElement.eResource.resourceSet.allContents.filter(PrimitiveDataType).findFirst[name == typeName]
	}
	
	/**
	 * Ensures that the new value will be accepted
	 */
	def boolean isValidConstantExpressionValueFromString(EObject modelElement, String str) {
		if(modelElement instanceof AssetAttributeValue) {
			if(modelElement?.attributeType.attributeType !== null) {
				val String[] allConstants = if(str.startsWith("[") && str.endsWith("]")) {
					str.substring(1, str.length - 1).split(",").map[trim]
				} else {
					#[str]
				}
				if(allConstants.empty) return false
				if(modelElement?.attributeType.attributeType instanceof EnumDataType) {
					allConstants.forall[value |
						val String litFragment = value.split("::").last
						val enumDT = modelElement.attributeType.attributeType as EnumDataType
						enumDT.enumLiteral.exists[el | el.name.equals(litFragment)]
					]
				} else  {
					val PrimitiveDataTypeSwitch<Boolean> pdtSwitch = new PrimitiveDataTypeSwitch<Boolean>() {
						override caseBoolean() {
							allConstants.forall [ value |
								value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")
							]
						}
						override caseString() {
							allConstants.forall [ value |
								value.startsWith("\"") && value.endsWith("\"")
							]
						}
						override caseInteger() {
							try {
								allConstants.forEach [ value |
									Integer.parseInt(value)
								]
								true
							} catch (NumberFormatException e) {
								false
							}
						}
						override caseVersion() {
							allConstants.forall [ value |
								Version.valueOf(value) !== null
							]
						}
					}
					pdtSwitch.doSwitch(modelElement?.attributeType.attributeType)
				}
				
			} else false
		} else {		
			false
		}
	}
	
	/**
	 * Assign a new Constant expression from the user input string
	 * no control, we supposed that isValidConstantExpressionValueFromString has been called first
	 */
	def EObject setConstantExpressionValueFromString(EObject modelElement, String str) {
		if(modelElement instanceof AssetAttributeValue) {
			if(modelElement?.attributeType.attributeType !== null) {
				val String[] allConstants = if(str.startsWith("[") && str.endsWith("]")) {
					modelElement.collection = true
					str.substring(1, str.length - 1).split(",").map[trim]
				} else {
					modelElement.collection = false
					#[str]
				}
				if(modelElement?.attributeType.attributeType instanceof EnumDataType) {
					val enumDT = modelElement.attributeType.attributeType as EnumDataType
					val newvalues = allConstants.map [ value |
						val String litFragment = value.split("::").last
						enumDT.enumLiteral.findFirst[el|el.name == litFragment]
					].map [ lit |
						modelElement.values.filter(EnumConstant).findFirst[constant|constant.value == lit] ?:
							(AbsystemFactory.eINSTANCE.createEnumConstant => [value = lit])
					]
					ECollections.setEList(modelElement.values, newvalues)
					return modelElement
				} else {
					val PrimitiveDataTypeSwitch<EObject> pdtSwitch = new PrimitiveDataTypeSwitch<EObject>() {

						override caseBoolean() {
							val newvalues = allConstants.map [ bvalue |
								modelElement.values.filter(BooleanConstant).
									findFirst[constant|constant.value == bvalue] ?:
									(AbsystemFactory.eINSTANCE.createBooleanConstant => [value = bvalue])
							]
							ECollections.setEList(modelElement.values, newvalues)
							return modelElement
						}

						override caseString() {
							val newvalues = allConstants.map [ value |
								value.substring(1, value.length - 1)
							].map [ svalue |
								modelElement.values.filter(StringConstant).
									findFirst[constant|constant.value == svalue] ?:
									(AbsystemFactory.eINSTANCE.createStringConstant => [value = svalue])
							]
							ECollections.setEList(modelElement.values, newvalues)
							return modelElement
						}

						override caseInteger() {
							val newvalues = allConstants.map [ value |
								Integer.parseInt(value)
							].map [ ivalue |
								modelElement.values.filter(IntConstant).findFirst[constant|constant.value == ivalue] ?:
									(AbsystemFactory.eINSTANCE.createIntConstant => [value = ivalue])
							]
							ECollections.setEList(modelElement.values, newvalues)
							return modelElement
						}

						override caseVersion() {
							val newvalues = allConstants.map [ value |
								Version::valueOf(value)
							].map [ vvalue |
								modelElement.values.filter(VersionConstant).
									findFirst[constant|constant.value == vvalue] ?:
									(AbsystemFactory.eINSTANCE.createVersionConstant => [value = vvalue])
							]
							ECollections.setEList(modelElement.values, newvalues)
							return modelElement
						}
					}
					pdtSwitch.doSwitch(modelElement?.attributeType.attributeType)
				}
			}
		}
		return modelElement
	}
	
}
