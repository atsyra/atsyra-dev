/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.design;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.resource.XtextResource;

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.Import;
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;

/**
 * The services class used by VSM.
 */
public class Services {

	/**
	 * See
	 * http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.sirius.doc%2Fdoc%2Findex.html&cp=24
	 * for documentation on how to write service methods.
	 */
	public EObject myService(EObject self, String arg) {
		// TODO Auto-generated code
		return self;
	}

	
	public String toDisplayString(final Multiplicity _self) {
		return new XtendServices().toDisplayString(_self);
	}
	
	public PrimitiveDataType getPrimitiveDataType(final EObject modelElement, final String name) {
		return new XtendServices().getPrimitiveDataType(modelElement, name);
	}
	
	public Multiplicity getMultiplicityFromString(final String string) {
		return new XtendServices().getMultiplicityFromString(string);
	}
	
	
	public boolean isValidConstantExpressionValueFromString(final EObject modelElement, final String string) {
		return new XtendServices().isValidConstantExpressionValueFromString(modelElement, string);
	}
	public EObject setConstantExpressionValueFromString(final EObject modelElement, final String string) {
		return new XtendServices().setConstantExpressionValueFromString(modelElement, string);
	}
	
	
	

    /**
     * Use Xtext to pretty print the EObject
     * Works only if the EObject uis already in an xtext resource
     * @param any
     * @return
     */
    public static String xtextPrettyPrint(EObject any) {
    	if (any != null && any.eResource() instanceof XtextResource) {    		
    		return ((XtextResource)any.eResource()).getSerializer().serialize(any);
    	}
		return "";
	}

	public List<Import> getImportsWithURI(EObject eobj) {
		EObject root = EcoreUtil.getRootContainer(eobj);
		if (root instanceof AssetBasedSystem) {
			return ((AssetBasedSystem) root).getImports().stream().filter(imp -> imp.getImportURI() != null)
					.collect(Collectors.toList());
		} else {
			return List.of();
		}
	}

	/**
	 * Utility function to print the current object in aql expressions
	 */
	public Object debug(Object object, String prefix) {
		System.out.println(prefix + object);
		return object;
	}

	public List<Object> debug(List<Object> object, String prefix) {
		System.out.println(prefix + "["
				+ object.stream().map(Object::toString).reduce((s1, s2) -> s1 + "," + s2).orElse("") + "]");
		return object;
    }
    
    public Set<Object> debug(Set<Object> object, String prefix) {
    	System.out.println(prefix + "[" + object.stream().map(Object::toString).reduce((s1, s2) -> s1 + "," + s2).orElse("") + "]");
    	return object;
    }
}
