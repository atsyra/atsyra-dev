/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.design;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.sirius.diagram.DNodeList;
import org.eclipse.sirius.diagram.DSemanticDiagram;
import org.eclipse.sirius.diagram.business.api.query.DDiagramQuery;

import com.google.common.collect.Iterators;
import com.google.common.collect.Sets;
import com.google.common.collect.UnmodifiableIterator;

import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;

public class AssetTypeServices {
	public List<AssetType> getSupertypes(AssetType type) {
		return ABSUtils.getSupertypesHierarchy(type);
	}
	
	public AssetType getBaseAssetType(AbstractAssetType type) {
		return ABSUtils.getBaseAssetType(type);
	}
	
	public Boolean noOpposite(AssetTypeReference ref) {
		return ref.getOppositeTypeReference() == null;
	}
	
	public Set<AssetType> getDisplayedAssetTypes(DSemanticDiagram diagram) {
		Set<AssetType> result = Sets.newLinkedHashSet();
		
		Iterator<DNodeList> it = Iterators.filter(new DDiagramQuery(diagram).getAllDiagramElements().iterator(),
				DNodeList.class);
		
		while (it.hasNext()) {
			DNodeList dec = it.next();
			if (dec.getTarget() instanceof AssetType && dec.isVisible()) {
				result.add((AssetType) dec.getTarget());
			}
		}
		return result;
	}
	
	/**
	 * build the list of unique AssetTypeReference (one for each opposite pair)
	 * @param context
	 * @param diagram
	 * @return
	 */
    public List<AssetTypeReference> getEOppositeEReferences(DefinitionGroup context, DSemanticDiagram diagram) {
        Collection<AssetType> assetTypes = getDisplayedAssetTypes(diagram);
        Set<AssetTypeReference> references = Sets.newLinkedHashSet();
        for (AssetType asetType : assetTypes) {
            references.addAll(asetType.getAssetTypeProperties());
        }
        Map<String, AssetTypeReference> map = new HashMap<String, AssetTypeReference>();
        for (AssetTypeReference ref : references) {
            if (ref.getOppositeTypeReference() != null) {
                String key1 = ref.getOppositeTypeReference().hashCode() + "" + ref.hashCode();
                String key2 = ref.hashCode() + "" + ref.getOppositeTypeReference().hashCode();
                if (map.get(key1) == null && map.get(key2) == null) {
                    map.put(key1, ref);
                }
            }
        }
        return new ArrayList<AssetTypeReference>(map.values());
    }
	
	public List<AbstractAssetType> getSupertypesWithAspects(AssetType type) {
		List<AbstractAssetType> result = new ArrayList<>();
		List<AssetType> superTypes = ABSUtils.getSupertypesHierarchy(type);
		result.addAll(superTypes);
		result.addAll(getAllAspects(superTypes));
		return result;
	}
	
	public List<AssetTypeAspect> getAllAspects(AssetType type) {
		List<AssetTypeAspect> result = new ArrayList<>();
		UnmodifiableIterator<AssetTypeAspect> allAspectsIterator = Iterators
				.filter(type.eResource().getResourceSet().getAllContents(), AssetTypeAspect.class);
		UnmodifiableIterator<AssetTypeAspect> allAspectsOfTypeIterator = Iterators.filter(allAspectsIterator,
				aspect -> aspect.getBaseAssetType() == type);
		Iterators.addAll(result, allAspectsOfTypeIterator);
		return result;
	}
	
	public List<AssetTypeAspect> getAllAspects(List<AssetType> typeList) {
		if(typeList.isEmpty()) {
			return List.of();
		} else {
			ResourceSet resourceSet = typeList.get(0).eResource().getResourceSet();
			List<AssetTypeAspect> result = new ArrayList<>();
			UnmodifiableIterator<AssetTypeAspect> allAspectsIterator = Iterators
					.filter(resourceSet.getAllContents(), AssetTypeAspect.class);
			UnmodifiableIterator<AssetTypeAspect> allAspectsOfTypeIterator = Iterators.filter(allAspectsIterator,
					aspect -> typeList.contains(aspect.getBaseAssetType()));
			Iterators.addAll(result, allAspectsOfTypeIterator);
			return result;
		}
	}

	
	public String getQualifiedName(AbstractAssetType at) {
		return ABSUtils.getQualifiedName(at, "::").get();
	}
	public String getQualifiedName(AssetTypeReference atr) {
		return ABSUtils.getQualifiedName(atr, "::").get();
	}
	
}
