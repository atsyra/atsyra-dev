package fr.irisa.atsyra.absystem.transfo.tests

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle
import org.junit.jupiter.api.TestMethodOrder
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import fr.irisa.atsyra.absystem.transfo.tests.AssetBasedSystemDslInjectorProvider
import org.eclipse.xtext.resource.XtextResourceSet
import org.junit.jupiter.api.BeforeEach
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.emf.common.util.URI
import fr.irisa.atsyra.absystem.transfo.aspects.SlicingAssetBasedSystemAspect
import fr.irisa.atsyra.absystem.model.absystem.Goal
import org.eclipse.xtext.serializer.ISerializer
import static org.junit.jupiter.api.Assertions.assertLinesMatch
import fr.irisa.atsyra.absystem.transfo.aspects.UndefControlAssetBasedSystemAspect

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation)
class UndefinedControlTest {
	@Inject
	XtextResourceSet absresourceSet;
	@Inject
	ISerializer serializer;

	AssetBasedSystem sourceabs
	AssetBasedSystem expectedabs

	@BeforeEach
	def void SetUpBeforeClass() {
		AbsystemPackage.eINSTANCE.eClass()
		absresourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE)
	}
	
	private def void LoadTestCase(String caseName) {
		val sourceuri = URI.createFileURI("testfiles/UndefinedControl/" + caseName + "/input.abs")
		val sourceRes = absresourceSet.getResource(sourceuri, true)
		sourceabs =  sourceRes.getContents().get(0) as AssetBasedSystem
		Assertions.assertNotNull(sourceabs)
		val expecteduri = URI.createFileURI("testfiles/UndefinedControl/" + caseName + "/expected.abs")
		val expectedRes = absresourceSet.getResource(expecteduri, true)
		expectedabs =  expectedRes.getContents().get(0) as AssetBasedSystem
		Assertions.assertNotNull(expectedabs)
	}
	
	private def runtest() {
		UndefControlAssetBasedSystemAspect.controlForUndefined(sourceabs)
		assertLinesMatch(serializer.serialize(expectedabs).lines, serializer.serialize(sourceabs).lines)
	}
	
	@Test
	def void test_1() {
		LoadTestCase("test1")
		runtest()
	}
	
	@Test
	def void test_2() {
		LoadTestCase("test2")
		runtest()
	}
	
	@Test
	def void controls_should_not_be_duplicated() {
		LoadTestCase("noduplicates")
		runtest()
	}

}
