/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore.provider;


import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.ResultstorePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import atsyragoal.AtsyraGoal;

/**
 * This is the item provider adapter for a {@link fr.irisa.atsyra.resultstore.GoalResult} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class GoalResultItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalResultItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addGoalPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Goal feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGoalPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GoalResult_goal_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GoalResult_goal_feature", "_UI_GoalResult_type"),
				 ResultstorePackage.Literals.GOAL_RESULT__GOAL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ResultstorePackage.Literals.GOAL_RESULT__REACHABILITY_RESULT);
			childrenFeatures.add(ResultstorePackage.Literals.GOAL_RESULT__SHORTEST_WITNESS_RESULT);
			childrenFeatures.add(ResultstorePackage.Literals.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS);
			childrenFeatures.add(ResultstorePackage.Literals.GOAL_RESULT__PRECONDITION);
			childrenFeatures.add(ResultstorePackage.Literals.GOAL_RESULT__POSTCONDITION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns GoalResult.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/GoalResult"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		AtsyraGoal goal = ((GoalResult)object).getGoal();
		String goalName = goal == null ? "" : goal.getName();
		return goalName == null ?
				getString("_UI_GoalResult_type") :
				getString("_UI_GoalResult_type") +" for "+ goalName;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(GoalResult.class)) {
			case ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT:
			case ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT:
			case ResultstorePackage.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS:
			case ResultstorePackage.GOAL_RESULT__PRECONDITION:
			case ResultstorePackage.GOAL_RESULT__POSTCONDITION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ResultstorePackage.Literals.GOAL_RESULT__REACHABILITY_RESULT,
				 ResultstoreFactory.eINSTANCE.createResult()));

		newChildDescriptors.add
			(createChildParameter
				(ResultstorePackage.Literals.GOAL_RESULT__SHORTEST_WITNESS_RESULT,
				 ResultstoreFactory.eINSTANCE.createWitnessResult()));

		newChildDescriptors.add
			(createChildParameter
				(ResultstorePackage.Literals.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS,
				 ResultstoreFactory.eINSTANCE.createWitnessResult()));

		newChildDescriptors.add
			(createChildParameter
				(ResultstorePackage.Literals.GOAL_RESULT__PRECONDITION,
				 ResultstoreFactory.eINSTANCE.createConditionResult()));

		newChildDescriptors.add
			(createChildParameter
				(ResultstorePackage.Literals.GOAL_RESULT__POSTCONDITION,
				 ResultstoreFactory.eINSTANCE.createConditionResult()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ResultstorePackage.Literals.GOAL_RESULT__SHORTEST_WITNESS_RESULT ||
			childFeature == ResultstorePackage.Literals.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS ||
			childFeature == ResultstorePackage.Literals.GOAL_RESULT__PRECONDITION ||
			childFeature == ResultstorePackage.Literals.GOAL_RESULT__POSTCONDITION;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ResultstoreEditPlugin.INSTANCE;
	}

}
