/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyra2.ide.ui.views.correctnessresult;

import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.TreeResult;

public class TreeRootWrapper {

	public TreeResult rootTreeResult = null;
	public GoalResult rootGoalResult = null;

	public TreeRootWrapper(TreeResult tr, GoalResult gr) {
		rootTreeResult = tr;
		rootGoalResult = gr;
	}

	public TreeRootWrapper(TreeResult at) {
		rootTreeResult = at;
	}

	public TreeRootWrapper(GoalResult at) {
		rootGoalResult = at;
	}
}
