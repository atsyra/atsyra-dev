/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyra2.ide.ui.views.correctnessresult;

import org.eclipse.jface.resource.CompositeImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

/**
 * A {@link CompositeImageDescriptor} that overlay an {@link Image} on top of a base {@link Image}.
 * 
 * @author <a href="mailto:yvan.lussaud@obeo.fr">Yvan Lussaud</a>
 */
public class OverlayImageDescriptor extends CompositeImageDescriptor {

	/**
	 * Base {@link Image}.
	 */
	private final Image image;

	/**
	 * Overlay {@link Image}.
	 */
	private final Image overlay;

	/**
	 * The size of the {@link OverlayImageDescriptor#image}.
	 */
	private final Point size;
	
	private final OverlayStrategy strategy;

	/**
	 * Constructor.
	 * 
	 * @param image
	 *            the base {@link Image}
	 * @param overlay
	 *            the overlay {@link Image}
	 */
	public OverlayImageDescriptor(Image image, Image overlay) {
		this(image, overlay, OverlayStrategy.OVERLAP);
	}
	
	public enum OverlayStrategy {
		OVERLAP,
		GROW_NORTH,
		GROW_EAST,
		GROW_WEST,
		GROW_SOUTH
	}
	/**
	 * Constructor.
	 * 
	 * @param image
	 *            the base {@link Image}
	 * @param overlay
	 *            the overlay {@link Image}
	 */
	public OverlayImageDescriptor(Image image, Image overlay, OverlayStrategy strategy) {
		this.image = image;
		this.strategy = strategy;
		final Rectangle bounds = image.getBounds();
		switch (strategy) {
		case GROW_NORTH:
		case GROW_SOUTH:
			bounds.height = bounds.height + overlay.getBounds().height;
			// maintain ratio
			bounds.width = bounds.width + overlay.getBounds().height;
			break;
		case GROW_EAST:
		case GROW_WEST:
			bounds.width = bounds.width + overlay.getBounds().width;
			// maintain ratio
			bounds.height = bounds.height + overlay.getBounds().width;
			break;
		default:
		}
		bounds.add(overlay.getBounds()); // get the max of both images in case it should grow
		this.size = new Point(bounds.width, bounds.height);
		this.overlay = overlay;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.jface.resource.CompositeImageDescriptor#drawCompositeImage(int, int)
	 */
	protected void drawCompositeImage(int arg0, int arg1) {
		
		switch (strategy) {
		case GROW_NORTH:
			drawImage(createCachedImageDataProvider(image), overlay.getBounds().height/2, overlay.getBounds().height);
			drawImage(createCachedImageDataProvider(overlay), overlay.getBounds().height/2, 0);
			break;
		case GROW_SOUTH:
			drawImage(createCachedImageDataProvider(image), overlay.getBounds().height/2, 0);
			drawImage(createCachedImageDataProvider(overlay), overlay.getBounds().height/2, image.getBounds().height);
			break;
		case GROW_EAST:
			drawImage(createCachedImageDataProvider(image), 0, overlay.getBounds().width/2);
			drawImage(createCachedImageDataProvider(overlay), image.getBounds().width, overlay.getBounds().width/2);
			break;
		case GROW_WEST:
			drawImage(createCachedImageDataProvider(image), overlay.getBounds().width, overlay.getBounds().width/2);
			drawImage(createCachedImageDataProvider(overlay), 0, overlay.getBounds().width/2);
			break;
		default:
			drawImage(createCachedImageDataProvider(image), 0, 0);
			drawImage(createCachedImageDataProvider(overlay), 0, 0);
		}
	}

	protected Point getSize() {
		return size;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof OverlayImageDescriptor && image.equals(((OverlayImageDescriptor)obj).image)
				&& overlay.equals(((OverlayImageDescriptor)obj).overlay);
	}

	@Override
	public int hashCode() {
		return image.hashCode() ^ overlay.hashCode();
	}

}
