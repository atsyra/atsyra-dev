/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyra2.ide.ui.views.correctnessresult;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IMarkSelection;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.sirius.diagram.AbstractDNode;
import org.eclipse.sirius.diagram.DNode;
import org.eclipse.sirius.diagram.DNodeContainer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.outline.impl.EObjectNode;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraTree;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.TreeResult;


/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class CorrectnessResultView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "fr.irisa.atsyra.atsyra2.ide.ui.views.CorrectnessResultView";

	private static final String RESULTFOLDER_NAME = "results";
	
	@Inject IWorkbench workbench;
	
	private PageBook pagebook;
	private TreeViewer treeviewer;
	private TextViewer textviewer;
	private DrillDownAdapter drillDownAdapter;
	private Action action1;
	private Action action2;
	private Action doubleClickAction;
	
	/** listener of selection in other views
	 * see https://www.eclipse.org/articles/Article-WorkbenchSelections/article.html 
	 * will trigger an update of the view based on last selected Atsyra tree */
	private ISelectionListener otherViewsSelectionListener = new ISelectionListener() {
        public void selectionChanged(IWorkbenchPart sourcepart, ISelection selection) {
        	// we ignore our own selections
			if (sourcepart != CorrectnessResultView.this) {
			    showSelection(sourcepart, selection);
			}
        }
    };
	 
	@Override
	public void createPartControl(Composite parent) {
		// the PageBook allows simple switching between two viewers
		pagebook = new PageBook(parent, SWT.NONE);
		
		// treeviewer page
		treeviewer = new TreeViewer(pagebook, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		drillDownAdapter = new DrillDownAdapter(treeviewer);
		
		treeviewer.setContentProvider(new ViewContentProvider(this));
		treeviewer.setInput(getViewSite());
		
		
		ILabelProvider baseLabelProvider = new ViewLabelProvider(this);
		ILabelDecorator decorator = PlatformUI.getWorkbench().getDecoratorManager().getLabelDecorator();
		treeviewer.setLabelProvider(new DecoratingLabelProvider(baseLabelProvider, decorator));

		// Create the help context id for the viewer's control
		workbench.getHelpSystem().setHelp(treeviewer.getControl(), "fr.irisa.atsyra.ide.ui.viewer");
		
		// we're cooperative and also provide our selection
		// at least for the treeviewer
		getSite().setSelectionProvider(treeviewer);
		
		
		// textviewer page
		textviewer = new TextViewer(pagebook, SWT.H_SCROLL | SWT.V_SCROLL);
		textviewer.setEditable(false);
		
		// other UI contributions
		
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
		
		// register to selection changes
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(otherViewsSelectionListener);
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				CorrectnessResultView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(treeviewer.getControl());
		treeviewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, treeviewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(action1);
		manager.add(new Separator());
		manager.add(action2);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(action1);
		manager.add(action2);
		manager.add(new Separator());
		drillDownAdapter.addNavigationActions(manager);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(action1);
		manager.add(action2);
		manager.add(new Separator());
		drillDownAdapter.addNavigationActions(manager);
	}

	private void makeActions() {
		action1 = new Action() {
			public void run() {
				showMessage("Action 1 executed");
			}
		};
		action1.setText("Action 1");
		action1.setToolTipText("Action 1 tooltip");
		action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
		
		action2 = new Action() {
			public void run() {
				showMessage("Action 2 executed");
			}
		};
		action2.setText("Action 2");
		action2.setToolTipText("Action 2 tooltip");
		action2.setImageDescriptor(workbench.getSharedImages().
				getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
		doubleClickAction = new Action() {
			public void run() {
				IStructuredSelection selection = treeviewer.getStructuredSelection();
				Object obj = selection.getFirstElement();
				showMessage("Double-click detected on "+obj.toString());
			}
		};
	}

	private void hookDoubleClickAction() {
		treeviewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}
	private void showMessage(String message) {
		MessageDialog.openInformation(
			pagebook.getShell(),
			"Correctness Result View",
			message);
	}

	@Override
	public void setFocus() {
		pagebook.setFocus();
	}
	
	
	/**
	 * Shows the given selection in this view.
	 */
	public void showSelection(IWorkbenchPart sourcepart, ISelection selection) {
		setContentDescription(sourcepart.getTitle() + " (" + selection.getClass().getName() + ")");
		if (selection instanceof IStructuredSelection) {
			Object selectedTreeOrGoal = null;
			IStructuredSelection ss = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = ss.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();
				
				if (element instanceof AtsyraTree) {
					selectedTreeOrGoal = (AtsyraTree) element;
					break;
				} else if (element instanceof IAdaptable) {
					AtsyraTree treeres = (AtsyraTree) ((IAdaptable) element)
							.getAdapter(AtsyraTree.class);
					if (treeres != null) {
						selectedTreeOrGoal = treeres;
						break;
					} else {
						AbstractDNode dnoderes = (AbstractDNode) ((IAdaptable) element)
								.getAdapter(AbstractDNode.class);
						if (dnoderes != null) {
							selectedTreeOrGoal = dnoderes.getTarget();
							break;
						}
					}
				}
				if (element instanceof DNodeContainer) {
					selectedTreeOrGoal = ((DNodeContainer)element).getTarget();
					break;
				} else	if (element instanceof DNode) {
					selectedTreeOrGoal = ((DNode)element).getTarget();
					break;
			//	} else if (element instanceof DNodeContainer2EditPart) {
			//		selectedTreeOrGoal = ((DNodeContainer2EditPart)element).;
			//		break;
				} else if (element instanceof EObjectNode) {
					// xtext outline element
					EObjectNode enode = (EObjectNode)element;
					ResourceSet resourceSet = new ResourceSetImpl();
			        Resource inResource = resourceSet.getResource(enode.getEObjectURI(),true);
			        EObject eobject = inResource.getEObject(enode.getEObjectURI().fragment());
			        selectedTreeOrGoal = eobject;
				}
				// still not found, try platform adapter
				if(selectedTreeOrGoal == null ) {
					selectedTreeOrGoal = Platform.getAdapterManager().getAdapter(element, AtsyraTree.class);
				}
			}
			if(selectedTreeOrGoal != null) {
				showTreeItem(selectedTreeOrGoal);
			} else {
			  showItems(ss.toArray());
			}
		}
		if (selection instanceof ITextSelection) {
			ITextSelection ts  = (ITextSelection) selection;
			if( sourcepart instanceof XtextEditor) {
				// Selection may be stale, get latest from editor
				XtextEditor xtextEditor = EditorUtils.getActiveXtextEditor();
				
				EObject xtextEObject = xtextEditor.getDocument().readOnly(resource -> {
						EObject e = new EObjectAtOffsetHelper().resolveContainedElementAt(resource, ts.getOffset());
						return e;
					});
				showTreeItem(xtextEObject);
			} else {
				showText(ts.getText());
			}
		}
		if (selection instanceof IMarkSelection) {
			IMarkSelection ms = (IMarkSelection) selection;
			try {
			    showText(ms.getDocument().get(ms.getOffset(), ms.getLength()));
			} catch (BadLocationException ble) { }
		}
	}
	
	private void showTreeItem(Object item){
		if (item instanceof AtsyraTree) {
			ResultStore resStore = getResultStoreModel((AtsyraTree)item);
			Optional<TreeResult> treeRes = resStore.getTreeResults().stream().filter(tr -> tr.getTree().getName().equals(((AtsyraTree)item).getName())).findFirst();
			if(treeRes.isPresent()) {
				treeviewer.setInput(treeRes.get());
			} else {
				// use a fake treeResult
				TreeResult tr =ResultstoreFactory.eINSTANCE.createTreeResult();
				tr.setTree((AtsyraTree) item);
				treeviewer.setInput(tr);
			}
		} else if (item instanceof AtsyraGoal) {
			ResultStore resStore = getResultStoreModel((AtsyraGoal)item);
			Optional<GoalResult> treeRes = resStore.getGoalResults().stream().filter(tr -> tr.getGoal().getName().equals(((AtsyraGoal)item).getName())).findFirst();
			if(treeRes.isPresent()) {
				treeviewer.setInput(treeRes.get());
			} else {
				// use a fake goalResult
				GoalResult tr =ResultstoreFactory.eINSTANCE.createGoalResult();
				tr.setGoal((AtsyraGoal) item);
				treeviewer.setInput(tr);
			}
		} else {
			treeviewer.setInput(item);
		}
		pagebook.showPage(treeviewer.getControl());
	}
	
	private void showItems(Object[] items) {
		treeviewer.setInput(items);
		pagebook.showPage(treeviewer.getControl());
	}
	
	private void showText(String text) {
		textviewer.setDocument(new Document(text));
		pagebook.showPage(textviewer.getControl());
	}
	
	public void dispose() {
		// important: We need do unregister our listener when the view is disposed
        ISelectionService s = getSite().getWorkbenchWindow().getSelectionService();
        s.removeSelectionListener(otherViewsSelectionListener);
        super.dispose();
    }
	
	public static ResultStore getResultStoreModel(EObject eobj) {
		IFile file = EMFResource.getIFile(eobj);
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(RESULTFOLDER_NAME);
		IFile resultStoreFile = file.getProject().getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension +".resultstore");
		
		if(!resultStoreFile.exists()) {
			// create an empty result store
			
		}
		
		URI resultStoreUri = URI.createPlatformResourceURI(resultStoreFile.getFullPath().toString(), true);
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("resultstore", new XMIResourceFactoryImpl());
		ResourceSet resSet;
		Resource res;
		if(!resultStoreFile.exists()) {
			// create an empty result store
			resSet = new ResourceSetImpl();
			res = resSet.createResource(resultStoreUri);
			res.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
			try {
				res.save(m);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// use the same resourceSet in order to ease equality comparison
		if (eobj.eResource() != null && eobj.eResource().getResourceSet() != null) {
			resSet = eobj.eResource().getResourceSet();
			res = resSet.getResource(resultStoreUri, true);
			// may need a reload if the resource of the resultstore has changed
			res.unload();
			res = resSet.getResource(resultStoreUri, true);
			
		} else {
			resSet = new ResourceSetImpl();
			res = resSet.getResource(resultStoreUri, true);
		}
		
		return (ResultStore) res.getContents().get(0);
	}
}
