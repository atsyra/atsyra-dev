/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyra2.ide.ui.views;

import java.util.stream.Collectors;

import org.eclipse.osgi.util.NLS;

import fr.irisa.atsyra.resultstore.TreeResult;
import fr.irisa.atsyra.resultstore.util.SyntheticResultHelper;

/**
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "fr.irisa.atsyra.ide.ui.views.messages"; //$NON-NLS-1$
	public static String PERFECT_LOCAL_MATCH;
	public static String UNRELATED_LOCAL_REFINEMENT;
	public static String WEAK_LOCAL_REFINEMENT;
	public static String UNPRECISE_LOCAL_REFINEMENT;
	public static String RESTRIVE_LOCAL_REFINEMENT;
	public static String INCOHERENT_RESULT;
	public static String INCOMPLETE_RESULT;
	public static String LEAF_RESULT;
	
	public static String TREE_ADMISSIBLE;
	public static String TREE_NOT_ADMISSIBLE_MAIN_GOAL;
	public static String TREE_NOT_ADMISSIBLE_REFINEMENT_GOAL;
	public static String TREE_ADMISSIBILITY_INCOMPLETE_RESULT;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
	
	
	public static String getSyntheticLocalRefinementTreeResultMsg(TreeResult tr) {
		switch (SyntheticResultHelper.getSyntheticLocalRefinementTreeResult(tr)) {
		case LEAF:
			return Messages.LEAF_RESULT;
		case INCOMPLETE_RESULT:
			return Messages.INCOMPLETE_RESULT;
		case PERFECT_LOCAL_MATCH:
			return Messages.PERFECT_LOCAL_MATCH;
		case INCOHERENT_RESULT:
			return Messages.INCOHERENT_RESULT;
		case UNRELATED_LOCAL_REFINEMENT:
			return Messages.UNRELATED_LOCAL_REFINEMENT;
		case WEAK_LOCAL_REFINEMENT:
			return Messages.WEAK_LOCAL_REFINEMENT;
		case UNPRECISE_LOCAL_REFINEMENT:
			return Messages.UNPRECISE_LOCAL_REFINEMENT;
		case RESTRIVE_LOCAL_REFINEMENT:
			return Messages.RESTRIVE_LOCAL_REFINEMENT;
		default:
			return Messages.INCOMPLETE_RESULT;
		}
	}
	
	public static String getSyntheticTreeAdmissibilityResultMsg(TreeResult tr) {
		switch (SyntheticResultHelper.getSyntheticTreeAdmissibilityResult(tr)) {
		case TREE_ADMISSIBLE:
			return Messages.TREE_ADMISSIBLE;
		case TREE_ADMISSIBILITY_INCOMPLETE_RESULT:
			return Messages.TREE_ADMISSIBILITY_INCOMPLETE_RESULT;
		case TREE_NOT_ADMISSIBLE_MAIN_GOAL:
			return Messages.TREE_NOT_ADMISSIBLE_MAIN_GOAL;
		case TREE_NOT_ADMISSIBLE_REFINEMENT_GOAL:
			return Messages.TREE_NOT_ADMISSIBLE_REFINEMENT_GOAL;
		default:
			return Messages.TREE_ADMISSIBILITY_INCOMPLETE_RESULT;
		}
	}
	public static String getDetailledTreeAdmissibilityResultMsg(TreeResult tr) {
		String msgcomplement= "";
		switch (SyntheticResultHelper.getSyntheticTreeAdmissibilityResult(tr)) {
		case TREE_ADMISSIBLE:
			return Messages.TREE_ADMISSIBLE;
		case TREE_ADMISSIBILITY_INCOMPLETE_RESULT:
			msgcomplement =SyntheticResultHelper.getGoalsWithMissingReachability(tr).stream().map(g -> g.getName()).collect( Collectors.joining( ", " ) );
			return Messages.TREE_ADMISSIBILITY_INCOMPLETE_RESULT + "\n[Goals with missing reachability analysis: " +msgcomplement+"]" ;
		case TREE_NOT_ADMISSIBLE_MAIN_GOAL:
			msgcomplement =SyntheticResultHelper.getNotReachableGoals(tr).stream().map(g -> g.getName()).collect( Collectors.joining( ", " ) );
			return Messages.TREE_NOT_ADMISSIBLE_MAIN_GOAL + "\n[not reachable goals: "+msgcomplement+"]";
		case TREE_NOT_ADMISSIBLE_REFINEMENT_GOAL:
			msgcomplement =SyntheticResultHelper.getNotReachableGoals(tr).stream().map(g -> g.getName()).collect( Collectors.joining( ", " ) );
			return Messages.TREE_NOT_ADMISSIBLE_REFINEMENT_GOAL + "\n[not reachable goals: "+msgcomplement+"]";
		default:
			return Messages.TREE_ADMISSIBILITY_INCOMPLETE_RESULT;
		}
	}
}
