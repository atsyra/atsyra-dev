/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyra2.ide.ui.views.correctnessresult;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISharedImages;

import fr.irisa.atsyra.atsyra2.ide.ui.views.Messages;
import fr.irisa.atsyra.atsyra2.ide.ui.views.correctnessresult.OverlayImageDescriptor.OverlayStrategy;
import fr.irisa.atsyra.ide.ui.Activator;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.TreeResult;
import fr.irisa.atsyra.resultstore.util.SyntheticResultHelper;
import fr.irisa.atsyra.resultstore.util.SyntheticResultHelper.SyntheticTreeAdmissibilityResult;

public class ViewLabelProvider extends LabelProvider {

	/**
	 * 
	 */
	private final CorrectnessResultView correctnessResultView;

	/**
	 * @param correctnessResultView
	 */
	public ViewLabelProvider(CorrectnessResultView correctnessResultView) {
		this.correctnessResultView = correctnessResultView;
	}

	public String getText(Object obj) {
		if( obj instanceof TreeResult) {
			StringBuilder sb = new StringBuilder();
			TreeResult treeRes = (TreeResult) obj;
			sb.append("Tree ");
			sb.append(treeRes.getTree().getName());
			sb.append("\n=> " + Messages.getSyntheticLocalRefinementTreeResultMsg(treeRes));
			sb.append("\n=> " + Messages.getSyntheticTreeAdmissibilityResultMsg(treeRes));
			
			return sb.toString();
		}
		if( obj instanceof GoalResult) {
			StringBuilder sb = new StringBuilder();
			GoalResult goalRes = (GoalResult) obj;
			sb.append("Goal ");
			sb.append(goalRes.getGoal().getName());
			return sb.toString();
		}
		if (obj instanceof TreeRootWrapper) {
			if (((TreeRootWrapper) obj).rootTreeResult != null) {
				return getText(((TreeRootWrapper) obj).rootTreeResult);
			}
			if (((TreeRootWrapper) obj).rootGoalResult != null) {
				return getText(((TreeRootWrapper) obj).rootGoalResult);
			}
		}
		if( obj instanceof Result) {
			Result object = (Result) obj;
			ComposedAdapterFactory factory = new ComposedAdapterFactory(
					ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

			// add factory for supported labelprovider
			// factory.addAdapterFactory(new ResultstoreItemProviderAdapterFactory());
			// last, use reflective factory
			factory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());

			if (factory.isFactoryForType(IItemLabelProvider.class)) {
				IItemLabelProvider labelProvider = (IItemLabelProvider) factory.adapt(object, IItemLabelProvider.class);
				if (labelProvider != null) {
					StringBuilder sb = new StringBuilder();
					sb.append(labelProvider.getText(object));
					if(object.getDetails() != null) {
						sb.append("\n=> " + object.getDetails());
					}
					return sb.toString();
				}
			}
		}
		if (obj instanceof EObject) {
			EObject object = (EObject) obj;
			ComposedAdapterFactory factory = new ComposedAdapterFactory(
					ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

			// add factory for supported labelprovider
			// factory.addAdapterFactory(new ResultstoreItemProviderAdapterFactory());
			// last, use reflective factory
			factory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());

			if (factory.isFactoryForType(IItemLabelProvider.class)) {
				IItemLabelProvider labelProvider = (IItemLabelProvider) factory.adapt(object, IItemLabelProvider.class);
				if (labelProvider != null) {
					return labelProvider.getText(object);
				}
			}
		}
		return obj.toString();
	}

	public Image getImage(Object obj) {
		if( obj instanceof TreeResult) {
			TreeResult treeRes = (TreeResult)obj;
			Image baseImage;
			switch (SyntheticResultHelper.getSyntheticLocalRefinementTreeResult(treeRes)) {
			case LEAF:
				baseImage = getReflectiveImage(treeRes.getTree());
				break;
			case INCOMPLETE_RESULT:
			case INCOHERENT_RESULT:
				Result result = ResultstoreFactory.eINSTANCE.createResult();
				result.setValue(ResultValue.NOT_RUN);
				baseImage = getReflectiveImage(result);
				break;
			case PERFECT_LOCAL_MATCH:
				baseImage = Activator.getDefault().getImageRegistry().get(Activator.IMAGE_LocalRefinement_PerfectMatch_ID);
				break;
			case UNRELATED_LOCAL_REFINEMENT:
				baseImage = Activator.getDefault().getImageRegistry().get(Activator.IMAGE_LocalRefinement_Unrelated_ID);
				break;
			case WEAK_LOCAL_REFINEMENT:
				baseImage = Activator.getDefault().getImageRegistry().get(Activator.IMAGE_LocalRefinement_Weak_ID);
				break;
			case UNPRECISE_LOCAL_REFINEMENT:
				baseImage = Activator.getDefault().getImageRegistry().get(Activator.IMAGE_LocalRefinement_NotPreciseEnough_ID);
				break;
			case RESTRIVE_LOCAL_REFINEMENT:
				baseImage = Activator.getDefault().getImageRegistry().get(Activator.IMAGE_LocalRefinement_TooStrong_ID);
				break;
			default:
				Result result2 = ResultstoreFactory.eINSTANCE.createResult();
				result2.setValue(ResultValue.NOT_RUN);
				baseImage = getReflectiveImage(result2);
			}
			return getImageForAdmissibilityResult(baseImage,SyntheticResultHelper.getSyntheticTreeAdmissibilityResult(treeRes));

		}
		if (obj instanceof TreeRootWrapper) {
			if (((TreeRootWrapper) obj).rootTreeResult != null) {
				return getImage(((TreeRootWrapper) obj).rootTreeResult);
			}
		}
		if (obj instanceof EObject) {
			return getImageWithTransparentOvelay(getReflectiveImage((EObject) obj));
		}
		String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
		if (obj instanceof TreeRootWrapper)
			imageKey = ISharedImages.IMG_OBJ_FOLDER;
		return getImageWithTransparentOvelay(this.correctnessResultView.workbench.getSharedImages().getImage(imageKey));
	}
	
	public Image getReflectiveImage(EObject object) {
		ComposedAdapterFactory factory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		// add factory for supported labelprovider
		// factory.addAdapterFactory(new ResultstoreItemProviderAdapterFactory());
		// last, use reflective factory
		factory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
		
		if (factory.isFactoryForType(IItemLabelProvider.class)) {
			IItemLabelProvider labelProvider = (IItemLabelProvider) factory.adapt(object, IItemLabelProvider.class);
			if (labelProvider != null) {
				Object image = labelProvider.getImage(object);
				return ExtendedImageRegistry.getInstance().getImage(image);
			}
		}
		// todo return default image 
		return null;
	}
	
	
	/**
	 * The {@link Image} cache.
	 */
	protected final Map<ImageDescriptor, Image> imagesCache = new HashMap<ImageDescriptor, Image>();
	
	/**
	 * Static data mainGoalWarningOverlay {@link Image}.
	 */
	private final Image mainGoalWarningOverlay = new Image(Display.getDefault(), 
			Activator.getImageDescriptor("icons/icon-main-goal-warning.png").getImageData(100));

	
	/**
	 * Static data refinementGoalWarningOverlay {@link Image}.
	 */
	private final Image refinementGoalWarningOverlay = new Image(Display.getDefault(), 
			Activator.getImageDescriptor("icons/icon-refinement-goal-warning.png").getImageData(100));
	
	/**
	 * Static data transparentOverlay {@link Image}.
	 * used to align all images size with same kind of overlay
	 */
	private final Image transparentOverlay = new Image(Display.getDefault(), 
			Activator.getImageDescriptor("icons/icon-transparent.png").getImageData(100));
	
	
	protected Image getImageForAdmissibilityResult(Image baseImage, SyntheticTreeAdmissibilityResult admissibility) {
		Image res;
		final OverlayImageDescriptor descriptor;
		switch (admissibility) {
		case TREE_ADMISSIBILITY_INCOMPLETE_RESULT:
			descriptor = new OverlayImageDescriptor(baseImage, mainGoalWarningOverlay, OverlayStrategy.GROW_SOUTH);
			break;
		case TREE_NOT_ADMISSIBLE_MAIN_GOAL:
			descriptor = new OverlayImageDescriptor(baseImage, mainGoalWarningOverlay, OverlayStrategy.GROW_SOUTH);
			break;
		case TREE_NOT_ADMISSIBLE_REFINEMENT_GOAL:
			descriptor = new OverlayImageDescriptor(baseImage, refinementGoalWarningOverlay, OverlayStrategy.GROW_SOUTH);
			break;
		default:
			descriptor = new OverlayImageDescriptor(baseImage, transparentOverlay, OverlayStrategy.GROW_SOUTH);
		}
		Image cachedImage = imagesCache.get(descriptor);
		if (cachedImage == null) {
			cachedImage = descriptor.createImage();
			imagesCache.put(descriptor, cachedImage);
		}
		res = cachedImage;
		return res;
	}
	
	protected Image getImageWithTransparentOvelay(Image baseImage) {
		Image res;
		final OverlayImageDescriptor descriptor;
		descriptor = new OverlayImageDescriptor(baseImage, transparentOverlay, OverlayStrategy.GROW_SOUTH);
		Image cachedImage = imagesCache.get(descriptor);
		if (cachedImage == null) {
			cachedImage = descriptor.createImage();
			imagesCache.put(descriptor, cachedImage);
		}
		res = cachedImage;
		return res;
	}
	
	@Override
	public void dispose() {
		mainGoalWarningOverlay.dispose();
		refinementGoalWarningOverlay.dispose();
		transparentOverlay.dispose();
		for (Image cachedImage : imagesCache.values()) {
			cachedImage.dispose();
		}
		super.dispose();
	}
}


