/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyra2.ide.ui.views.correctnessresult;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import fr.irisa.atsyra.atsyra2.ide.ui.views.Messages;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.TreeResult;

class ViewContentProvider implements ITreeContentProvider {
	/**
	 * 
	 */
	private final CorrectnessResultView correctnessResultView;

	/**
	 * @param correctnessResultView
	 */
	ViewContentProvider(CorrectnessResultView correctnessResultView) {
		this.correctnessResultView = correctnessResultView;
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		// ITreeContentProvider.super.inputChanged(viewer, oldInput, newInput);
	}

	TreeRootWrapper current = new TreeRootWrapper(null, null);

	public Object[] getElements(Object inputElement) {
		if (current.rootTreeResult == inputElement) {
			Object[] root = { current };
			return root;
		} else if (inputElement instanceof TreeResult) {
			current = new TreeRootWrapper((TreeResult) inputElement);
			Object[] root = { current };
			return root;
		}
		if (current.rootGoalResult == inputElement) {
			Object[] root = { current };
			return root;
		} else if (inputElement instanceof GoalResult) {
			current = new TreeRootWrapper((GoalResult) inputElement);
			Object[] root = { current };
			return root;
		}
		
		return ArrayContentProvider.getInstance().getElements(inputElement);
	}

	public Object getParent(Object child) {
		if (child instanceof TreeResult) {
			return ((TreeResult) child).getTree();
		}
		return null;
	}

	public Object[] getChildren(Object parent) {
		if (parent instanceof TreeRootWrapper) {
			ArrayList<Object> childrens = new ArrayList<Object>();
			if (((TreeRootWrapper) parent).rootTreeResult != null) {
				TreeResult treeRes = ((TreeRootWrapper) parent).rootTreeResult;
				if (treeRes.getMeetResult() != null) {
					childrens.add(treeRes.getMeetResult());
				} else {
					if(!treeRes.getTree().getOperands().isEmpty()) {
						childrens.add("Unknown meet status");
					}
				}
				;
				if (treeRes.getOvermatchResult() != null) {
					childrens.add(treeRes.getOvermatchResult());
				} else {
					if(!treeRes.getTree().getOperands().isEmpty()) {						
						childrens.add("Unknown overmatch status");
					}
				}
				;
				if (treeRes.getUndermatchResult() != null) {
					childrens.add(treeRes.getUndermatchResult());
				} else {
					if(!treeRes.getTree().getOperands().isEmpty()) {
						childrens.add("Unknown undermatch status");
					}
				}
				;
				if (treeRes.getMatchResult() != null) {
					childrens.add(treeRes.getMatchResult());
				} else {
					if(!treeRes.getTree().getOperands().isEmpty()) {
						childrens.add("Unknown match status");
					}
				}
				;

				if (treeRes.getAdmissibilityResult() != null) {
					childrens.add(treeRes.getAdmissibilityResult());
				} else {
					// use a fake result
					Result r =ResultstoreFactory.eINSTANCE.createResult();
					r.setName(Messages.getDetailledTreeAdmissibilityResultMsg(treeRes));
					childrens.add(r);
				}
				;
				return childrens.toArray();
			}
			if (((TreeRootWrapper) parent).rootGoalResult != null) {
				GoalResult goalRes = ((TreeRootWrapper) parent).rootGoalResult;
				// todo optionally drill down to display witnesses
			}
			return childrens.toArray();
		}
		// return nothing
		return new Object[0];
	}

	public boolean hasChildren(Object parent) {
		if (parent instanceof TreeRootWrapper) {
			return true;
		}
		return false;
	}

}