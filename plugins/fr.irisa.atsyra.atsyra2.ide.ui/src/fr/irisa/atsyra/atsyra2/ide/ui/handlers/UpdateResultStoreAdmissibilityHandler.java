/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyra2.ide.ui.handlers;

import java.io.IOException;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import fr.irisa.atsyra.atsyra2.ide.ui.views.Messages;
import fr.irisa.atsyra.ide.ui.Activator;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.TreeResult;
import fr.irisa.atsyra.resultstore.util.SyntheticResultHelper;

/**
 * Handler that update the admissibility results for all trees in a resultStore file
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class UpdateResultStoreAdmissibilityHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();
				IFile res = null;
				if (element instanceof IResource) {
					res = (IFile) element;
				} else if (element instanceof IAdaptable) {
					res = (IFile) ((IAdaptable) element).getAdapter(IFile.class);
				}
				if (res != null) {
					updateResultStoreAdmissibility(res);
				} else {
					IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
					MessageDialog.openInformation(
							window.getShell(),
							"ATSyRA IDE UI",
							"Cannot proceed updateResultStoreAdmissibility on this selection");
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Compute the reachability of all goals present in the given resource
	 * @param resource
	 */
	public static void updateResultStoreAdmissibility(IFile resource) {
		if (resource.getName().endsWith(".resultstore")) {
			final IFile file = (IFile) resource;
			try {

				Job job = new Job("Updating Admissibility result of all trees in "+file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						boolean hasValueChanges = false;
						final ResourceSet rs = new ResourceSetImpl();
						final URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
						Resource resultStoreRes = rs.getResource(uri, true);
						
						ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
						
						for(TreeResult tr : resultStore.getTreeResults()) {
							Result result = tr.getAdmissibilityResult();
							if(result == null) {
								result = ResultstoreFactory.eINSTANCE.createResult();
								result.setTimestamp(new java.util.Date());
								tr.setAdmissibilityResult(result);
								hasValueChanges = true;
							}
							
							String newResultName = Messages.getDetailledTreeAdmissibilityResultMsg(tr);
							if(!newResultName.equals(result.getName() )) {
								result.setName(newResultName);
								hasValueChanges = true;
								switch (SyntheticResultHelper.getSyntheticTreeAdmissibilityResult(tr)) {
								case TREE_ADMISSIBLE:
									result.setValue(ResultValue.TRUE);
									break;
								case TREE_NOT_ADMISSIBLE_MAIN_GOAL:
								case TREE_NOT_ADMISSIBLE_REFINEMENT_GOAL:
									result.setValue(ResultValue.FALSE);
									break;
								case TREE_ADMISSIBILITY_INCOMPLETE_RESULT:
								default:
									result.setValue(ResultValue.NOT_RUN);
								}
							}
						}
						if(hasValueChanges) {
							// save resource
							try {
								resultStoreRes.save(null);
							} catch (IOException e) {
								Activator.eclipseError(
										"Updating Admissibility result from "+file.getName()	+" raised an exception "
												+ e.getMessage(), e);
							}
						}
						return Status.OK_STATUS;
					};
				};
				job.setPriority(Job.LONG);
				// prevent concurrent job
				job.setRule(file);
				job.schedule();
									
			} catch (Exception e) {
				Activator.eclipseError(
						"Updating Admissibility result from "+file.getName()	+" raised an exception "
								+ e.getMessage(), e);
								
			}
		}
	} 	
	
	
	/*
	public Result getNewResultHandler(AtsyraGoal goal,  URI resultStoreUri){
		
		
		
		Resource resultStoreRes = goal.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<GoalResult> grOp = resultStore.getGoalResults().stream().filter(gr -> gr.getGoal().getName().equals(goal.getName())).findFirst();
		GoalResult gr;
		if(grOp.isPresent()){
			gr = grOp.get();
		} else {
			gr = ResultstoreFactory.eINSTANCE.createGoalResult();
			gr.setGoal(goal);
			resultStore.getGoalResults().add(gr);
		}
		Result result = ResultstoreFactory.eINSTANCE.createResult();
		result.setTimestamp(new java.util.Date());
		gr.setReachabilityResult(result);
		result.setName("Reachability of goal : "+goal.getName());
		return result;
	}
*/	
	/**
	 * look for a resultstore file with the same name, in genFolder
	 * @param URI
	 * @return
	 */
/*	public URI getResultStoreURI(IFile file){
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME);
		IFile resultStoreFile = file.getProject().getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension +".resultstore");
		
		URI resultStoreUri = URI.createPlatformResourceURI(resultStoreFile.getFullPath().toString(), true);
		if(!resultStoreFile.exists()){
			// make sure the file exist, 
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
	        Map<String, Object> m = reg.getExtensionToFactoryMap();
	        m.put("resultstore", new XMIResourceFactoryImpl());
			ResourceSet resSet = new ResourceSetImpl();
			Resource res = resSet.createResource(resultStoreUri);
			//Resource res = resSet.getResource(resultStoreUri, true);
			try {
				res.save(null);
			} catch (IOException e) {
				Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
			}
		}
		return resultStoreUri;
	}
	
	IFile getGalFile(IFile file, AtsyraGoal goal) {
		// look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
				
		IFile galFile = file.getProject()
				.getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension + "_" + goal.getName() + "_reach.gal");
		
		
		return galFile;
	}
	*/
	
}
