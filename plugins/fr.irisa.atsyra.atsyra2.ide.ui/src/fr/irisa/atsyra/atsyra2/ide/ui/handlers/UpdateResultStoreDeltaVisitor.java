/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyra2.ide.ui.handlers;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;

public class UpdateResultStoreDeltaVisitor implements IResourceDeltaVisitor {

	@Override
	public boolean visit(IResourceDelta delta) throws CoreException {
		IResource resource = delta.getResource();
		switch (delta.getKind()) {
		case IResourceDelta.ADDED:
			// handle added resource
			if (resource.getFileExtension() != null && resource.getFileExtension().equals("resultstore")
					&& resource instanceof IFile) {
				UpdateResultStoreAdmissibilityHandler.updateResultStoreAdmissibility((IFile) resource);
			}
			break;
		case IResourceDelta.REMOVED:
			// handle removed resource
			break;
		case IResourceDelta.CHANGED:
			// handle changed resource
			if (resource.getFileExtension() != null && resource.getFileExtension().equals("resultstore")
					&& resource instanceof IFile) {
				UpdateResultStoreAdmissibilityHandler.updateResultStoreAdmissibility((IFile) resource);
			}
			break;
		}
		// return true to continue visiting children.
		return true;
	}

}
