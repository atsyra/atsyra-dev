package fr.irisa.atsyra.absystem.pddl.transfo.aspects

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLFile
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel

@Aspect(className=PDDLModel)
class PDDLModelAspect {
	def PDDLModel initialize(PDDLFile type) {
		_self.file=type
		return _self
	}

}
