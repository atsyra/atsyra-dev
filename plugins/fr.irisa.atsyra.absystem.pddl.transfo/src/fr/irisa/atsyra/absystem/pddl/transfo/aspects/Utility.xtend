package fr.irisa.atsyra.absystem.pddl.transfo.aspects

import java.util.Set
import java.util.HashSet

enum ObjectCategory {
	Type, Object, Predicat, Function, Action, Axiom
}

enum LogType {
	Note, Comment, Warning, Error
}

class Utility {
	
	//Use Java Stacktrace ?
	
	def String getID(ObjectCategory category, String absName) {
		
	}
	
	def void log(LogType type, String description, String id, String Location) {
		
	}
	
}

class IdentifierGenerator {
	
	val Set<String> allID = new HashSet<String>()
	
	def String getID(ObjectCategory category, String absName) {
		return ""
	}
	
}

class Logger {
	
	def void log(LogType type, String description, String id, String Location) {
		
	}
	
}

class LocalVariableName {
	
	Set<String> allNames
	
	new() {
		allNames = new HashSet<String>()
	}
	
	def String newLocalName(String base) {
		if (allNames.contains(base)) {
			var int r = 1
			while (allNames.contains(base + r)) {
				r++
			}
			allNames.add(base + r)
			return (base + r)
		} else {
			allNames.add(base)
			return base
		}
	}
	
}
