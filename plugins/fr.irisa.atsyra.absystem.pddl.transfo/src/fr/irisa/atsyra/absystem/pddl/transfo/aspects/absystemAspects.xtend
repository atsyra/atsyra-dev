package fr.irisa.atsyra.absystem.pddl.transfo.aspects

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.Asset
import fr.irisa.atsyra.absystem.model.absystem.AssetLink
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup
import fr.irisa.atsyra.absystem.model.absystem.Import
import fr.irisa.atsyra.absystem.model.absystem.Tag
import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter
import fr.irisa.atsyra.absystem.model.absystem.Expression
import fr.irisa.atsyra.absystem.model.absystem.BinaryExpression
import fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression
import fr.irisa.atsyra.absystem.model.absystem.OrExpression
import fr.irisa.atsyra.absystem.model.absystem.AndExpression
import fr.irisa.atsyra.absystem.model.absystem.NotExpression
import fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression
import fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression
import fr.irisa.atsyra.absystem.model.absystem.Action
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression
import fr.irisa.atsyra.absystem.model.absystem.StringConstant
import fr.irisa.atsyra.absystem.model.absystem.IntConstant
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection
import fr.irisa.atsyra.absystem.model.absystem.Member
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef
import fr.irisa.atsyra.absystem.model.absystem.Symbol
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod
import fr.irisa.atsyra.absystem.model.absystem.LambdaParameter
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression
import fr.irisa.atsyra.absystem.model.absystem.LambdaAction
import fr.irisa.atsyra.absystem.model.absystem.Parameter
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity
import fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry
import fr.irisa.atsyra.absystem.model.absystem.AnnotationKey
import fr.irisa.atsyra.absystem.model.absystem.Goal
import fr.irisa.atsyra.absystem.model.absystem.Contract
import fr.irisa.atsyra.absystem.model.absystem.Guard
import fr.irisa.atsyra.absystem.model.absystem.EnumConstant
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetTypeTransfoAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetTypeReferenceAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetLinkAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetTypeAttributeAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetTypeFeatureAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.PrimitiveDataTypeAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.EnumDataTypeAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.EnumLiteralAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetAttributeValueAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.DefinitionGroupAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetGroupAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.ImportAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.TagAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AbstractAssetTypeAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetTypeAspectAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.GuardedActionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.GuardParameterAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.ExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.BinaryExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.ImpliesExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.OrExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AndExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.NotExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.EqualityComparisonExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.InequalityComparisonExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.ActionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.ConstantExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.StringConstantAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.IntConstantAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.BooleanConstantAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.VersionConstantAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.MemberSelectionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.MemberAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.SymbolRefAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.SymbolAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.StaticMethodAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.LambdaParameterAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.LambdaExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.LambdaActionAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.ParameterAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AnnotationEntryAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AnnotationKeyAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.GoalAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.ContractAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.GuardAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.EnumConstantAspect.*
import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.UndefinedConstantAspect.*
import java.util.HashMap
import java.util.ArrayList
import java.util.List
import java.util.Map
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import fr.irisa.atsyra.pddl.xtext.pDDL.Type
import fr.irisa.atsyra.pddl.xtext.pDDL.Types
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLAction
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLFactory
import fr.irisa.atsyra.pddl.xtext.pDDL.Parameters
import fr.irisa.atsyra.pddl.xtext.pDDL.InheritorTypes
import fr.irisa.atsyra.pddl.xtext.pDDL.TypedVariablesList
import fr.irisa.atsyra.pddl.xtext.pDDL.TypedVariable
import fr.irisa.atsyra.pddl.xtext.pDDL.Variable
import fr.irisa.atsyra.pddl.xtext.pDDL.DeclPredicatOrFunction
import org.eclipse.emf.common.util.EList
import fr.irisa.atsyra.pddl.xtext.pDDL.LogicalExpressionDM
import fr.irisa.atsyra.pddl.xtext.pDDL.PredicatDM
import org.eclipse.xtext.naming.QualifiedName
import fr.irisa.atsyra.pddl.xtext.pDDL.ObjectsList
import fr.irisa.atsyra.pddl.xtext.pDDL.NewObjects
import fr.irisa.atsyra.pddl.xtext.pDDL.PredicatPB
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLObject
import fr.irisa.atsyra.pddl.xtext.pDDL.LogicalExpressionPB
import fr.irisa.atsyra.absystem.model.absystem.ActionEnum
import java.util.stream.Stream
import java.util.Optional
import java.util.HashSet

/* TODO
 * DOCS sur les classes, fonctions et attributs
 * Donner aussi des idées de où on pourrait utiliser des Threads différents (sur la traduction des actions par exemples)
 */

// Represents one DefinitionGroup
class WorkDefinition {
	
	// the name of the current Definition Group
	public String name
	
	// Is multiple inheritance required
	public boolean multipleInheritanceNeeded
	
	// types of the current Definition Group
	public List<AssetType> absAssetType
	public List<AssetTypeAspect> absAssetTypeAspect
	public List<EnumDataType> absEnumDataType
	
	// GuardedAction of the current Definition Group
	public List<GuardedAction> absGuardedActions
	
	new(String definitionGroupName) {
		name = definitionGroupName
		multipleInheritanceNeeded = false
		absAssetType = new ArrayList<AssetType>()
		absAssetTypeAspect = new ArrayList<AssetTypeAspect>()
		absEnumDataType = new ArrayList<EnumDataType>()
	}
	
	def void merge(WorkDefinition other) {
		//We keep the current name
		
		multipleInheritanceNeeded = multipleInheritanceNeeded || other.multipleInheritanceNeeded
		
		var boolean alreadyDefined
		
		//TODO best merge function OU utiliser celle de Maxime
		
		//Merging Types
		for (newType : other.absAssetType) {
			alreadyDefined = false
			for (oldType : absAssetType) {
				if (newType.name.equals(oldType.name)) {
					alreadyDefined = true
				}
			}
			if (!alreadyDefined) {
				absAssetType.add(newType)
			}
		}
		for (newType : other.absAssetTypeAspect) {
			alreadyDefined = false
			for (oldType : absAssetTypeAspect) {
				if (newType.baseAssetType.name.equals(oldType.baseAssetType.name)) {
					alreadyDefined = true
				}
			}
			if (!alreadyDefined) {
				absAssetTypeAspect.add(newType)
			}
		}
		for (newType : other.absEnumDataType) {
			alreadyDefined = false
			for (oldType : absEnumDataType) {
				if (newType.name.equals(oldType.name)) {
					alreadyDefined = true
				}
			}
			if (!alreadyDefined) {
				absEnumDataType.add(newType)
			}
		}
		
		//Merging GuardedAction
		if (absGuardedActions === null || absGuardedActions.size == 0) {
			absGuardedActions = other.absGuardedActions
		} else {
			for (newGA : other.absGuardedActions) {
				alreadyDefined = false
				for (oldGA : absGuardedActions) {
					if (newGA.name.equals(oldGA.name)) {
						alreadyDefined = true
					}
				}
				if (!alreadyDefined) {
					absGuardedActions.add(newGA)
				}
			}
		}
		
	}
	
}

@Aspect(className=AssetType)
class AssetTypeTransfoAspect extends AbstractAssetTypeAspect {
	
}

@Aspect(className=AssetTypeReference)
class AssetTypeReferenceAspect extends AssetTypeFeatureAspect {

}

class WorkObjects {
	
	public String problemName
	
	public Map<String, List<String>> objects
	
	public List<PredicatPB> initPredicates
	
	public List<Asset> allAsset
	
	// Temporarily moved here because goals are now in AssetGroup, not sure where it is supposed to be
	public List<Goal> absGoals
	
	new(String name) {
		problemName = name
		objects = new HashMap<String, List<String>>
		initPredicates = new ArrayList<PredicatPB>
		allAsset = new ArrayList<Asset>
	}
	
	def void addObject(String object, String type) {
		if (objects.containsKey(type)) { //TODO contains ou equals ?
			objects.get(type).add(object)
		} else {
			val List<String> l = new ArrayList<String>()
			l.add(object)
			objects.put(type, l)
		}
		
	}
	
	def void merge(WorkObjects other) {
		//MERGING PROBLEMNAME
		if (problemName === null) {
			problemName = other.problemName
		}
		
		//MERGING OBJECTS
		for (type : other.objects.keySet) {
			if (objects.containsKey(type)) { //TODO contains or equals ?
				for (object : other.objects.get(type)) {
					objects.get(type).add(object)
				}
			} else {
				objects.put(type, other.objects.get(type))
			}
		}
		
		//MERGING PREDICATES
		initPredicates.addAll(other.initPredicates)
		
		//MERGING ASSET
		for (asset : other.allAsset) {
			if (!allAsset.contains(asset)) {
				allAsset.add(asset)
			}
		}
		
		//MERGING GOALS
		val mergedGoals = newArrayList
		if(absGoals !== null) {
			mergedGoals.addAll(absGoals)
		}
		if(other.absGoals !== null) {
			mergedGoals.addAll(other.absGoals)
		}
		absGoals = mergedGoals
	}
	
}

class DataOut {
	
	public PDDLModel domain
	public PDDLModel problem
	public List<PDDLModel> extendModels
	
	new(PDDLModel domainModel, PDDLModel problemModel) {
		domain = domainModel
		problem = problemModel
		extendModels = new ArrayList<PDDLModel>
	}
	
}

//XXX: Entry Class
@Aspect(className=AssetBasedSystem)
class AssetBasedSystemAspect {
	//TODO: HashMap<String, HashMap<String, String>> objets; //Pour surtype => soustype => objets
	//TODO: HashMap<String, HashMap<String, String>> objets; //Pour type => références et attributs => noms des prédicats correspondants
	
	ArrayList<String> extendsPDDL
	ArrayList<String> requirementsPDDL
	
	Map<String, String> typeNames
	Map<String, String> predicatesNames //and functions ?
	Map<String, String> actionsNames //and Axioms ?
	
	WorkDefinition domainDefinition
	WorkObjects problemDefinition
	
	Map<AssetType, List<AssetType>> isSuperTypeOf
	
	boolean withProblem //Détermine si il y a un problème ou juste un domaine à traduire...
	boolean multipleInheritanceRequired // Is multiple inheritance required
	
	PDDLModel domainOut
	PDDLModel problemOut
	
	public Map<AssetTypeFeature, String> predicatesName
	
	//Default Value Administrator
	public HashMap<AssetType, ArrayList<AssetTypeAttribute>> TrueByDefaultBooleanAttribute
	public HashMap<AssetType, ArrayList<AssetTypeAttribute>> EnumDataTypeWithDefaultValue
	public HashMap<Asset, ArrayList<AssetTypeAttribute>> alreadyDefinedFeatures
	
	//TraceabilityMapping traceabilityMapping; //THE traceability manager ?
	
	//liste des supports -> quelles extensions les models de sortie peuvent prendre (function, axioms, either, not dans le init, ...)
	
	
	def void initialize() {
		if (_self.assetGroups.size > 0) {
			_self.withProblem = true
		} else {
			_self.withProblem = false
		}
		_self.multipleInheritanceRequired = false
		_self.domainDefinition = null
		_self.isSuperTypeOf = new HashMap<AssetType, List<AssetType>>
		_self.predicatesName = new HashMap<AssetTypeFeature, String>
		
		_self.TrueByDefaultBooleanAttribute = new HashMap<AssetType, ArrayList<AssetTypeAttribute>>
		_self.EnumDataTypeWithDefaultValue = new HashMap<AssetType, ArrayList<AssetTypeAttribute>>
		_self.alreadyDefinedFeatures = new HashMap<Asset, ArrayList<AssetTypeAttribute>>
	}
	
	//Entry function
	def DataOut ABS2PDDL(/*String folderPath /* where the domain and the problem while be serialized */) {
		//if (folderPath === null) { return; }
		
		_self.initialize()
		
		//imports too... using PDDLExtends ? Or merge with _self.domainDefinition ?
		
		// RECOVERING DOMAIN
		// TODO utiliser la fonction Merge de Maxime -> Avant même d'appeler la transformation en PDDL ?
		for (definition : _self.definitionGroups) {
			//get types, suptypes, references, attributs...
			if (_self.domainDefinition === null) {
				_self.domainDefinition = definition.recoverFromABSDefinitionGroup()
			} else {
				_self.domainDefinition.merge(definition.recoverFromABSDefinitionGroup())
			}
		}
		
		
		// SERIALIZATION OF THE DOMAIN AND THE PROBLEM AT THE MEMORY ADDRESS ENTERED IN PARAMETER
		
		_self.domainOut = PDDLFactory.eINSTANCE.createPDDLModel
		_self.domainOut.file = PDDLFactory.eINSTANCE.createPDDLFile
		_self.domainOut.file.nameD = _self.domainDefinition.name
		_self.domainOut.file.contentD = PDDLFactory.eINSTANCE.createDomain
		//extends?
		//requirements?
		
		// NEW PDDL TYPES FROM ABS ASSETTYPES
		_self.setPDDLTypesIntoDomainOut()
		
		// NEW PREDICATES FROM ATTRIBUTES AND REFERENCIES
		_self.setPDDLPredicatesIntoDomainOut()
		
		// NEW ACTIONS FROM NEW PREDICATES (TODO using real predicates name)
		_self.setPDDLActionsIntoDomainOut()
		
		if (_self.withProblem) {
			// RECOVERING PROBLEM
			// TODO utiliser la fonction Merge de Maxime -> Avant même d'appeler la transformation en PDDL ?
			for (objects : _self.assetGroups) {
				if (_self.problemDefinition === null) {
					_self.problemDefinition = objects.recoverFromABSAssetGroup(_self)
				} else {
					_self.problemDefinition.merge(objects.recoverFromABSAssetGroup(_self))
				}
			}
			
			_self.problemOut = PDDLFactory.eINSTANCE.createPDDLModel
			_self.problemOut.file = PDDLFactory.eINSTANCE.createPDDLFile
			_self.problemOut.file.nameP = _self.problemDefinition.problemName
			_self.problemOut.file.contentP = PDDLFactory.eINSTANCE.createProblem
			_self.problemOut.file.contentP.domainRef = PDDLFactory.eINSTANCE.createDomainRef
			_self.problemOut.file.contentP.domainRef.domainName = _self.domainDefinition.name
			
			// ADD OBJECTS FROM ASSETTYPES (TODO using real type name)
			_self.setPDDLObjectsIntoDomainOut()
			
			// ADD INIT FROM ENUMDATATYPE
			for (workingType : _self.domainDefinition.absEnumDataType) {
				for (value : workingType.enumLiteral) {
					if (_self.problemOut.file.contentP.init === null) {
						_self.problemOut.file.contentP.init = PDDLFactory.eINSTANCE.createInit
					}
					_self.problemOut.file.contentP.init.predicat.add(_self.getPredicatPB(workingType.name + "-" + value.name, value.name))
				}
			}
			
			// ADD INIT FROM ASSETTYPES (TODO Attributes Too)
			for (initialPredicat : _self.problemDefinition.initPredicates) {
				if (_self.problemOut.file.contentP.init === null) {
					_self.problemOut.file.contentP.init = PDDLFactory.eINSTANCE.createInit
				}
				_self.problemOut.file.contentP.init.predicat.add(initialPredicat) // TODO (not ()???) )
			}
			
			// ADD INIT FROM DEFAULT VALUES
			for (initialPredicat : _self.problemDefinition.initPredicates) {
				if (_self.problemOut.file.contentP.init === null) {
					_self.problemOut.file.contentP.init = PDDLFactory.eINSTANCE.createInit
				}
				_self.setPDDLPredicatesFromDefaultValues() // TODO (not ()???) )
			}
			
			// ADD INIT FROM GOALS TODO
			if (_self.problemDefinition.absGoals.size > 0 && _self.problemDefinition.absGoals.get(0).precondition !== null) {
				if (_self.problemOut.file.contentP.init === null) {
					_self.problemOut.file.contentP.init = PDDLFactory.eINSTANCE.createInit
				}
				_self.setPDDLPredicatesFromABSGoalIntoProblemOut()
			}
			
			// ADD PDDL GOAL FROM ABS GOAL TODO
			_self.setPDDLGoalIntoProblemOut()
		}
		
		System.out.println("fin")
		
		return new DataOut(_self.domainOut, _self.problemOut)
	}
	
	def void setPDDLTypesIntoDomainOut() {
		_self.domainOut.file.contentD.types = PDDLFactory.eINSTANCE.createTypes
		_self.domainOut.file.contentD.types.newTypes = PDDLFactory.eINSTANCE.createNewTypes
		_self.domainOut.file.contentD.types.newTypes.declOtherTypes = PDDLFactory.eINSTANCE.createDeclarationTypes
		
		//EnumDataType
		for (workingType : _self.domainDefinition.absEnumDataType) {
			val Type newType = PDDLFactory.eINSTANCE.createType
			newType.typeName = workingType.name
			
			_self.domainOut.file.contentD.types.newTypes.declOtherTypes.declT.add(newType)
		}
		
		//Other Types (AssetTypes)
		var List<AssetType> subTypes
		var List<AssetType> notHeir = new ArrayList<AssetType>()
		for (workingType : _self.domainDefinition.absAssetType) {
			if (workingType.extends !== null && workingType.extends.size > 0) {
				
				// Check if multiple inheritance is needed
				if (workingType.extends.size > 1) {
					_self.multipleInheritanceRequired = true
				}
				
				for (supType : workingType.extends) {
					if (_self.isSuperTypeOf.containsKey(supType)) {
						subTypes = _self.isSuperTypeOf.get(supType)
						if (!subTypes.contains(workingType)) {
							subTypes.add(workingType)
						}
						_self.isSuperTypeOf.put(supType, subTypes)
					} else {
						subTypes = new ArrayList()
						subTypes.add(workingType)
						_self.isSuperTypeOf.put(supType, subTypes)
					}
				}
			} else {
				notHeir.add(workingType)
			}
		}
		
		if (_self.multipleInheritanceRequired) {
			//Bah non...
			
			/* Pour l'héritage multiple (Multiplication des attributs et références hérité + ...) : TODO
			 - Prédicats (isAlsoSubTypeOfSUPTYPE ?o - SubType) => compliqué
			 - Multiplication des prédicats/functions et des actions/axioms pour chaque types heritier multiple (nombre de paramètre * nombre de super classe) -> pas besoin de multiplier les actions -> utiliser les either type ???
			*/
		} else {
			for (superType : _self.isSuperTypeOf.keySet()) {
				val InheritorTypes newTypesAndSupTypes = PDDLFactory.eINSTANCE.createInheritorTypes
				
				newTypesAndSupTypes.supType = PDDLFactory.eINSTANCE.createType
				newTypesAndSupTypes.supType.typeName = superType.name
				newTypesAndSupTypes.subTypes = PDDLFactory.eINSTANCE.createDeclarationTypes
				
				//newTypesAndSupTypes.subTypes.declT = new ArrayList<Type>()
				
				for (subType : _self.isSuperTypeOf.get(superType)) {
					val Type newType = PDDLFactory.eINSTANCE.createType
					newType.typeName = subType.name
					
					newTypesAndSupTypes.subTypes.declT.add(newType)
				}
				
				_self.domainOut.file.contentD.types.newTypes.declInheritorTypes.add(newTypesAndSupTypes)
			}
			
			for (newTypeWithoutAncestor : notHeir) {
				val Type newType = PDDLFactory.eINSTANCE.createType
				newType.typeName = newTypeWithoutAncestor.name
				
				_self.domainOut.file.contentD.types.newTypes.declOtherTypes.declT.add(newType)
			}
		}
	}
	
	def void setPDDLPredicatesIntoDomainOut() {
		var boolean instansiatePredicatList = false
		
		//EnumDatatType values
		for (workingType : _self.domainDefinition.absEnumDataType) {
			
			if (!instansiatePredicatList) {
				_self.domainOut.file.contentD.predicates = PDDLFactory.eINSTANCE.createPredicates
				_self.domainOut.file.contentD.predicates.predicatList = PDDLFactory.eINSTANCE.createDeclPFList
				_self.domainOut.file.contentD.predicates.predicatList.getPfList
				instansiatePredicatList = true
			}
			
			for (value : workingType.enumLiteral) {
				val newPredicate = PDDLFactory.eINSTANCE.createDeclPredicatOrFunction
				newPredicate.typedVariablesList = PDDLFactory.eINSTANCE.createTypedVariablesList
				
				newPredicate.pfname = workingType.name + "-" + value.name
				// (EnumDataTypeName-EnumDataTypeValue ...)
				
				val TypedVariable newVariable = PDDLFactory.eINSTANCE.createTypedVariable
				val Variable newVariable0 = PDDLFactory.eINSTANCE.createVariable
				newVariable0.variableName = "?value"
				newVariable.variables.add(newVariable0)
				val Type newType = PDDLFactory.eINSTANCE.createType
				newType.typeName = workingType.name
				newVariable.type = newType
				newPredicate.typedVariablesList.variablesList.add(newVariable)
				// (EnumDataTypeName-EnumDataTypeValue ?value EnumDataTypeName)
				
				_self.domainOut.file.contentD.predicates.predicatList.pfList.add(newPredicate)
			}
			
		}
		
		for (workingType : _self.domainDefinition.absAssetType) {
			
			for (absReference : workingType.assetTypeProperties) {
				if (!instansiatePredicatList) {
					_self.domainOut.file.contentD.predicates = PDDLFactory.eINSTANCE.createPredicates
					_self.domainOut.file.contentD.predicates.predicatList = PDDLFactory.eINSTANCE.createDeclPFList
					_self.domainOut.file.contentD.predicates.predicatList.getPfList
					instansiatePredicatList = true
				}
				
				_self.createPDDLPredicatesFromAssetType(absReference, workingType, "Immutable")
			}
			
			for (absAttribute : workingType.assetTypeAttributes) {
				
				if (!instansiatePredicatList) {
					_self.domainOut.file.contentD.predicates = PDDLFactory.eINSTANCE.createPredicates
					_self.domainOut.file.contentD.predicates.predicatList = PDDLFactory.eINSTANCE.createDeclPFList
					_self.domainOut.file.contentD.predicates.predicatList.getPfList
					instansiatePredicatList = true
				}
				
				_self.createPDDLPredicatesFromAssetType(absAttribute, workingType, "Immutable")
			}
			
		}
		
		for (workingType : _self.domainDefinition.absAssetTypeAspect) {
			
			for (absReference : workingType.assetTypeProperties) {
				
				if (!instansiatePredicatList) {
					_self.domainOut.file.contentD.predicates = PDDLFactory.eINSTANCE.createPredicates
					_self.domainOut.file.contentD.predicates.predicatList = PDDLFactory.eINSTANCE.createDeclPFList
					_self.domainOut.file.contentD.predicates.predicatList.getPfList
					instansiatePredicatList = true
				}
				
				_self.createPDDLPredicatesFromAssetType(absReference, workingType.baseAssetType, "Mutable")
			}
			
			for (absAttribute : workingType.assetTypeAttributes) {
				
				if (!instansiatePredicatList) {
					_self.domainOut.file.contentD.predicates = PDDLFactory.eINSTANCE.createPredicates
					_self.domainOut.file.contentD.predicates.predicatList = PDDLFactory.eINSTANCE.createDeclPFList
					_self.domainOut.file.contentD.predicates.predicatList.getPfList
					instansiatePredicatList = true
				}
				
				_self.createPDDLPredicatesFromAssetType(absAttribute, workingType.baseAssetType, "Mutable")
			}
			
		}
		
	}
	
	def void createPDDLPredicatesFromAssetType(AssetTypeReference absReference, AssetType workingType, String mutableOrImmutable) {
		
		val newPredicate = PDDLFactory.eINSTANCE.createDeclPredicatOrFunction
		newPredicate.typedVariablesList = PDDLFactory.eINSTANCE.createTypedVariablesList
		
		val LocalVariableName finalID = new LocalVariableName
		
		_self.predicatesName.put(absReference, absReference.name + "-" + absReference.multiplicity + mutableOrImmutable + "Reference_From_" + workingType.name + "_To_" + absReference.propertyType.name)
		newPredicate.pfname = _self.predicatesName.get(absReference)
		// (predicatName ...)
		
		val TypedVariable newVariables1 = PDDLFactory.eINSTANCE.createTypedVariable
		val Variable newVariable1 = PDDLFactory.eINSTANCE.createVariable
		newVariable1.variableName = finalID.newLocalName(("?" + workingType.name.charAt(0)).toLowerCase()) // Pas de nom de variable en double
		newVariables1.variables.add(newVariable1)
		val Type newType1 = PDDLFactory.eINSTANCE.createType
		newType1.typeName = workingType.name
		newVariables1.type = newType1
		newPredicate.typedVariablesList.variablesList.add(newVariables1)
		// (predicatName ?x - X ...)
		
		val TypedVariable newVariables2 = PDDLFactory.eINSTANCE.createTypedVariable
		val Variable newVariable2 = PDDLFactory.eINSTANCE.createVariable
		newVariable2.variableName = finalID.newLocalName(("?" + absReference.propertyType.name.charAt(0)).toLowerCase()) // Pas de nom de variable en double
		newVariables2.variables.add(newVariable2)
		val Type newType2 = PDDLFactory.eINSTANCE.createType
		newType2.typeName = absReference.propertyType.name
		newVariables2.type = newType2
		newPredicate.typedVariablesList.variablesList.add(newVariables2)
		// (predicatName ?x - X ?y - Y)
		
		_self.domainOut.file.contentD.predicates.predicatList.pfList.add(newPredicate)
		//EX : (Data_MutableReference_To_Credential-encryptedWith ?d - data ?c - credential)
		
	}
	
	def void createPDDLPredicatesFromAssetType(AssetTypeAttribute absAttribute, AssetType workingType, String mutableOrImmutable) {
		
		if (absAttribute.attributeType instanceof PrimitiveDataType && absAttribute.attributeType.name.equals("Boolean")) {
			
			val newPredicate = PDDLFactory.eINSTANCE.createDeclPredicatOrFunction
			newPredicate.typedVariablesList = PDDLFactory.eINSTANCE.createTypedVariablesList
			
			_self.predicatesName.put(absAttribute, absAttribute.name + "-" + absAttribute.multiplicity + mutableOrImmutable + "Attribute_From_" + workingType.name + "_To_Boolean")
			newPredicate.pfname = _self.predicatesName.get(absAttribute)
			// (predicatName ...)
			
			val TypedVariable newVariables = PDDLFactory.eINSTANCE.createTypedVariable
			val Variable newVariable = PDDLFactory.eINSTANCE.createVariable
			newVariable.variableName = ("?" + workingType.name.charAt(0)).toLowerCase() // Pas de nom de variable en double
			newVariables.variables.add(newVariable)
			val Type newType = PDDLFactory.eINSTANCE.createType
			newType.typeName = workingType.name
			newVariables.type = newType
			newPredicate.typedVariablesList.variablesList.add(newVariables)
			// (predicatName ?x - X)
			
			_self.domainOut.file.contentD.predicates.predicatList.pfList.add(newPredicate)
			//EX : (Data_MutableAttribute-encrypted ?d - data)
			
			if (absAttribute.hasDefault && absAttribute.defaultValues.get(0) instanceof BooleanConstant && (absAttribute.defaultValues.get(0) as BooleanConstant).value.equals("true")) {
				var ArrayList<AssetTypeAttribute> l = _self.TrueByDefaultBooleanAttribute.get(workingType)
				
				if (l === null) {
					l = new ArrayList<AssetTypeAttribute>()
					l.add(absAttribute)
				} else if (!l.contains(absAttribute)) {
					l.add(absAttribute)
				}
				
				_self.TrueByDefaultBooleanAttribute.put(workingType, l)
			}
			
		} else if (absAttribute.attributeType instanceof EnumDataType) {
			
			val newPredicate = PDDLFactory.eINSTANCE.createDeclPredicatOrFunction
			newPredicate.typedVariablesList = PDDLFactory.eINSTANCE.createTypedVariablesList
			
			_self.predicatesName.put(absAttribute, absAttribute.name + "-" + absAttribute.multiplicity + mutableOrImmutable + "Attribute_From_" + workingType.name + "_To_" + absAttribute.attributeType.name)
			newPredicate.pfname = _self.predicatesName.get(absAttribute)
			// (predicatName ...)
			
			val TypedVariable newVariables = PDDLFactory.eINSTANCE.createTypedVariable
			val Variable newVariable = PDDLFactory.eINSTANCE.createVariable
			newVariable.variableName = ("?" + workingType.name.charAt(0)).toLowerCase() // Pas de nom de variable en double
			newVariables.variables.add(newVariable)
			val Type newType = PDDLFactory.eINSTANCE.createType
			newType.typeName = workingType.name
			newVariables.type = newType
			newPredicate.typedVariablesList.variablesList.add(newVariables)
			// (predicatName ?x - X)
			
			val TypedVariable newVariables2 = PDDLFactory.eINSTANCE.createTypedVariable
			val Variable newVariable2 = PDDLFactory.eINSTANCE.createVariable
			newVariable2.variableName = "?value"
			newVariables2.variables.add(newVariable2)
			val Type newType2 = PDDLFactory.eINSTANCE.createType
			newType2.typeName = absAttribute.attributeType.name
			newVariables2.type = newType2
			newPredicate.typedVariablesList.variablesList.add(newVariables2)
			// (predicatName ?x - X ?y - Y)
			
			_self.domainOut.file.contentD.predicates.predicatList.pfList.add(newPredicate)
			//EX : (isOn-ImmutableAttribute_From_Poste_To_OS ?p - Poste ?value OS)
			
			if (absAttribute.hasDefault) {
				var ArrayList<AssetTypeAttribute> l = _self.EnumDataTypeWithDefaultValue.get(workingType)
				
				if (l === null) {
					l = new ArrayList<AssetTypeAttribute>()
					l.add(absAttribute)
				} else if (!l.contains(absAttribute)) {
					l.add(absAttribute)
				}
				
				_self.EnumDataTypeWithDefaultValue.put(workingType, l)
			}
			
		}
		
	}
	
	def void setPDDLActionsIntoDomainOut() {
		for (newAction : _self.domainDefinition.absGuardedActions) {
			_self.domainOut.file.contentD.actions.add(newAction.recoverPDDLActionFromABSGuardedAction(_self))
		}
	}
	
	def void setPDDLObjectsIntoDomainOut() {
		_self.problemOut.file.contentP.objects = PDDLFactory.eINSTANCE.createObjects
		_self.problemOut.file.contentP.objects.newObjectsList = PDDLFactory.eINSTANCE.createObjectsList
		
		//EnumDataType
		for (workingType : _self.domainDefinition.absEnumDataType) {
			val NewObjects list = PDDLFactory.eINSTANCE.createNewObjects
			
			list.objectType = PDDLFactory.eINSTANCE.createType
			list.objectType.typeName = workingType.name
			
			for (value : workingType.enumLiteral) {
				val PDDLObject currentObject = PDDLFactory.eINSTANCE.createPDDLObject
				currentObject.objectName = value.name //TODO eviter les noms en double
				list.newObject.add(currentObject)
			}
			
			_self.problemOut.file.contentP.objects.newObjectsList.newObj.add(list)
		}
		
		// Asset -> Object
		for (type : _self.problemDefinition.objects.keySet) {
			val NewObjects list = PDDLFactory.eINSTANCE.createNewObjects
			for (object : _self.problemDefinition.objects.get(type)) {
				val PDDLObject currentObject = PDDLFactory.eINSTANCE.createPDDLObject
				currentObject.objectName = object
				list.newObject.add(currentObject)
			}
			list.objectType = PDDLFactory.eINSTANCE.createType
			list.objectType.typeName = type
			
			_self.problemOut.file.contentP.objects.newObjectsList.newObj.add(list)
		}
		
		for (asset : _self.problemDefinition.allAsset) {
			var ArrayList<AssetTypeAttribute> l = new ArrayList<AssetTypeAttribute>()
			_self.alreadyDefinedFeatures.put(asset, l)
		}
	}
	
	def void setPDDLPredicatesFromDefaultValues() {
		var ArrayList<Asset> list = new ArrayList<Asset>()
		list.addAll(_self.alreadyDefinedFeatures.keySet())
		
		for (objet : list) {
			
			val ArrayList<AssetTypeAttribute> attributeList0 = _self.alreadyDefinedFeatures.get(objet)
			if (attributeList0 !== null) {
				val ArrayList<AssetTypeAttribute> attributeList1 = _self.TrueByDefaultBooleanAttribute.get(objet.assetType)
				if (attributeList1 !== null) {
					for (absAttribute : attributeList1) {
						if (!attributeList0.contains(absAttribute)) {
							_self.problemOut.file.contentP.init.predicat.add(_self.getPredicatPB(_self.predicatesName.get(absAttribute), objet.name))
						}
					}
				}
				
				val ArrayList<AssetTypeAttribute> attributeList2 = _self.EnumDataTypeWithDefaultValue.get(objet.assetType)
				if (attributeList2 !== null) {
					for (absAttribute : attributeList2) {
						if (!attributeList0.contains(absAttribute)) {
							for (value : absAttribute.defaultValues) {
								_self.problemOut.file.contentP.init.predicat.add(_self.getPredicatPB(_self.predicatesName.get(absAttribute), objet.name, (value as EnumConstant).value.name))
							}
						}
					}
				}
			}
			
		}
	}
	
	def void setPDDLPredicatesFromABSGoalIntoProblemOut() {
		/*
		for (initialPredicat : _self.domainDefinition.absGoals.get(0).precondition.recoverPDDLInitPredicatesFromABSGoalPrecondition(_self)) {
			_self.problemOut.file.contentP.init.predicat.add(initialPredicat) // TODO (not ()???) )
		}
		*/
		// PROVISOIRE
		
		val LogicalExpressionPB le = _self.problemDefinition.absGoals.get(0).precondition.recoverPDDLGoalFromABSGoalPostCondition(_self).getLEValue()
		if (le.leAnd1 !== null) {
			_self.problemOut.file.contentP.init.predicat.add(le.leAnd1.p)
			for (lep : le.leAnd2) {
				if (lep.p !== null) {
					_self.problemOut.file.contentP.init.predicat.add(lep.p)
				} else {
					System.out.println("Not Implemented Yet")
				}
			}
		} else {
			System.out.println("Not Implemented Yet")
		}
	}
	
	def void setPDDLGoalIntoProblemOut() {
		if (_self.problemDefinition.absGoals.size > 0) {
			_self.problemOut.file.contentP.goal = PDDLFactory.eINSTANCE.createGoal
			// Select another Goal ? And change init value too...
			_self.problemOut.file.contentP.goal.leGoal = _self.problemDefinition.absGoals.get(0).postcondition.recoverPDDLGoalFromABSGoalPostCondition(_self).getLEValue()
		}
	}
	
	def PredicatDM getPredicatDM(String... args) {
		val PredicatDM p = PDDLFactory.eINSTANCE.createPredicatDM
		
		for (arg : args) {
			if (p.predicatName === null) {
				p.predicatName = arg
			} else {
				val Variable v = PDDLFactory.eINSTANCE.createVariable
				v.variableName = "?" + arg
				p.variablesList.add(v)
			}
		}
		
		return p
	}
	
	def PredicatPB getPredicatPB(String... args) {
		val PredicatPB p = PDDLFactory.eINSTANCE.createPredicatPB
		
		for (arg : args) {
			if (p.predicatName === null) {
				p.predicatName = arg
			} else {
				val PDDLObject o = PDDLFactory.eINSTANCE.createPDDLObject
				o.objectName = arg
				p.objects.add(o)
			}
		}
		
		return p
	}
	
	def LogicalExpressionDM whenOrImply(boolean usingWhen, LogicalExpressionDM pre, LogicalExpressionDM post) {
		val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		if (usingWhen) {
			le.leWhen1 = pre
			le.leWhen2 = post
		} else {
			le.leImply1 = pre
			le.leImply2 = post
		}
		return le
	}
	
}

@Aspect(className=Asset)
class AssetAspect extends SymbolAspect {

}

@Aspect(className=AssetLink)
class AssetLinkAspect {
	def PredicatPB recoverFromABSAssetLink(AssetBasedSystem context) {
		var String predicatName = ""
		
		if (_self.referenceType !== null) {
			predicatName = context.predicatesName.get(_self.referenceType)
		} else {
			predicatName = "ERROR-NO-REFERENCETYPE"
			for (eachAssetTypeReference : _self.sourceAsset.assetType.assetTypeProperties) {
				if (eachAssetTypeReference.propertyType == _self.targetAsset) {
					predicatName = context.predicatesName.get(eachAssetTypeReference)
				}
			}
			if (predicatName === "") {
				predicatName = "ERROR-NO-REFERENCETYPE-AND-NO-ASSETTYPEFEATURE" //FIXME trow new Exception()
			}
		}
		
		val PredicatPB predicat = context.getPredicatPB(predicatName, _self.sourceAsset.name, _self.targetAsset.name)
		
		return predicat
	}
}

@Aspect(className=AssetTypeAttribute)
class AssetTypeAttributeAspect extends AssetTypeFeatureAspect {

}

@Aspect(className=AssetTypeFeature)
abstract class AssetTypeFeatureAspect extends MemberAspect {

}

@Aspect(className=PrimitiveDataType)
class PrimitiveDataTypeAspect {

}

@Aspect(className=EnumDataType)
class EnumDataTypeAspect extends PrimitiveDataTypeAspect {

}

@Aspect(className=EnumLiteral)
class EnumLiteralAspect extends SymbolAspect {

}

@Aspect(className=AssetAttributeValue)
class AssetAttributeValueAspect {

}

@Aspect(className=DefinitionGroup)
class DefinitionGroupAspect {
	
	public WorkDefinition currentDefinitionGroup
	
	def void initialize() {
		_self.currentDefinitionGroup = new WorkDefinition(_self.name)
		/*
		_self.isSuperTypeOf = new HashMap<String, ArrayList<String>>()
		_self.heirOf = new HashMap<String, ArrayList<String>>()
		_self.multipleInheritanceNeeded = false
		*/
	}
	
	def WorkDefinition recoverFromABSDefinitionGroup() {
		
		_self.initialize();
		
		val List<AssetType> allAssetType = new ArrayList<AssetType>()
		val List<AssetTypeAspect> allAssetTypeAspect = new ArrayList<AssetTypeAspect>()
		for (type : _self.assetTypes) {
			if (type instanceof AssetType) {
				allAssetType.add(type)
			} else if (type instanceof AssetTypeAspect) {
				if (type.baseAssetType !== null) {
					allAssetTypeAspect.add(type)
				}
			}
		}
		
		val List<EnumDataType> allEnumDataType = new ArrayList<EnumDataType>()
		for (type : _self.primitiveDataTypes) {
			if (type instanceof EnumDataType) {
				allEnumDataType.add(type)
			}
		}
		
		_self.currentDefinitionGroup.absAssetType = allAssetType
		_self.currentDefinitionGroup.absAssetTypeAspect = allAssetTypeAspect
		_self.currentDefinitionGroup.absEnumDataType = allEnumDataType
		_self.currentDefinitionGroup.absGuardedActions = _self.guardedActions
		
		
		return _self.currentDefinitionGroup
	}
	
}

@Aspect(className=AssetGroup)
class AssetGroupAspect {
	
	def WorkObjects recoverFromABSAssetGroup(AssetBasedSystem context) {
		val WorkObjects allObjects = new WorkObjects(_self.name)
		
		for (absAsset : _self.assets) {
			allObjects.addObject(absAsset.name, absAsset.assetType.name)
			allObjects.allAsset.add(absAsset)
		}
		
		for (absAssetLink : _self.assetLinks) {
			allObjects.initPredicates.add(absAssetLink.recoverFromABSAssetLink(context))
		}
		//TODO Also attributes and references
		
		allObjects.absGoals = _self.goals
		
		return allObjects
	}
	
}

@Aspect(className=Import)
class ImportAspect {

}

@Aspect(className=Tag)
class TagAspect {

}

@Aspect(className=AbstractAssetType)
abstract class AbstractAssetTypeAspect {

}

@Aspect(className=AssetTypeAspect)
class AssetTypeAspectAspect extends AbstractAssetTypeAspect {
	
}

@Aspect(className=GuardedAction)
class GuardedActionAspect extends GuardAspect {
	
	public PDDLAction PDDLVersionOfMe
	public boolean doingEffects
	
	def void initialize() {
		_self.PDDLVersionOfMe = PDDLFactory.eINSTANCE.createPDDLAction
		_self.PDDLVersionOfMe.actionName = _self.name
		_self.doingEffects = false
	}
	
	def PDDLAction recoverPDDLActionFromABSGuardedAction(AssetBasedSystem context) {
		_self.initialize()
		
		// ADD PARAMETERS
		_self.PDDLVersionOfMe.parameters = PDDLFactory.eINSTANCE.createParameters
		_self.PDDLVersionOfMe.parameters.parametersList = PDDLFactory.eINSTANCE.createTypedVariablesList
		
		for (newABSParameter : _self.guardParameters) {
			val TypedVariable newPDDLParameter = PDDLFactory.eINSTANCE.createTypedVariable
			
			val Variable newPDDLVariable = PDDLFactory.eINSTANCE.createVariable
			newPDDLVariable.variableName = "?" + newABSParameter.name
			newPDDLParameter.variables.add(newPDDLVariable)
			
			newPDDLParameter.type = PDDLFactory.eINSTANCE.createType
			if (newABSParameter.parameterType instanceof AssetType) {
				newPDDLParameter.type.typeName = newABSParameter.parameterType.name
			} else if (newABSParameter.parameterType instanceof QualifiedName) {
				newPDDLParameter.type.typeName = (newABSParameter.parameterType as QualifiedName).toString
			}
			
			_self.PDDLVersionOfMe.parameters.parametersList.variablesList.add(newPDDLParameter)
		}
		
		// ADD PRECONDITIONS
		val WorkLogicalExpressionDM preC = _self.guardExpression.recoverPDDLDomainLogicalExpressionFromABSExpression(context, _self)
		if (!preC.isAlwaysTrue) {
			_self.PDDLVersionOfMe.preconditions = PDDLFactory.eINSTANCE.createPreconditions
			_self.PDDLVersionOfMe.preconditions.le = preC.getLEValue
		}
		
		
		// ADD POSTCONDITIONS
		_self.doingEffects = true
		_self.PDDLVersionOfMe.effects = PDDLFactory.eINSTANCE.createEffects
		
		val List<LogicalExpressionDM> pddlActions = new ArrayList<LogicalExpressionDM>
		for (absAction : _self.guardActions) {
			pddlActions.add(absAction.recoverPDDLDomainLogicalExpressionFromABSAction(context, _self).getLEValue)
		}
		
		if (pddlActions.size > 1) {
			_self.PDDLVersionOfMe.effects.le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
			_self.PDDLVersionOfMe.effects.le.leAnd1 = pddlActions.remove(0)
			for (pddlAction : pddlActions) {
				_self.PDDLVersionOfMe.effects.le.leAnd2.add(pddlAction)
			}
		} else if (pddlActions.size == 1) {
			_self.PDDLVersionOfMe.effects.le = pddlActions.get(0)
		}
		
		return _self.PDDLVersionOfMe
	}
	
	def String unassignment(String predicateName, String firstVariableName, String variableDefaultName, String variableTypeName, AssetBasedSystem context) {
		var Optional<String> tool = Optional.empty()
		if (_self.PDDLVersionOfMe.preconditions.le.p !== null) {
			tool = _self.unassignmentTool(_self.PDDLVersionOfMe.preconditions.le, predicateName, firstVariableName)
		} else if (_self.PDDLVersionOfMe.preconditions.le.leAnd1 !== null) {
			tool = _self.unassignmentTool(_self.PDDLVersionOfMe.preconditions.le.leAnd1, predicateName, firstVariableName)
			
			var int r = 0
			while (!tool.isPresent() && r < _self.PDDLVersionOfMe.preconditions.le.leAnd2.size()) {
				tool = _self.unassignmentTool(_self.PDDLVersionOfMe.preconditions.le.leAnd2.get(r), predicateName, firstVariableName)
				r++
			}
		}
		
		if (tool.isPresent()) {
			return tool.get().substring(1)
		} else {
			var String finalVariableName = _self.finalUnassignerVariableName(variableDefaultName)
			
			//Add new Parameter
			if (_self.PDDLVersionOfMe.parameters === null) {
				_self.PDDLVersionOfMe.parameters = PDDLFactory.eINSTANCE.createParameters
				_self.PDDLVersionOfMe.parameters.parametersList = PDDLFactory.eINSTANCE.createTypedVariablesList
			}
			val TypedVariable newTVar = PDDLFactory.eINSTANCE.createTypedVariable
			val Variable newVar = PDDLFactory.eINSTANCE.createVariable
			newVar.variableName = "?" + finalVariableName
			newTVar.variables.add(newVar)
			newTVar.type = PDDLFactory.eINSTANCE.createType
			newTVar.type.typeName = variableTypeName
			_self.PDDLVersionOfMe.parameters.parametersList.variablesList.add(newTVar)
			
			//Add new Precondition
			if (_self.PDDLVersionOfMe.preconditions === null) {
				_self.PDDLVersionOfMe.preconditions = PDDLFactory.eINSTANCE.createPreconditions
			}
			val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
			le.p = context.getPredicatDM(predicateName, firstVariableName, finalVariableName)
			if (_self.PDDLVersionOfMe.preconditions.le.leAnd1 !== null) {
				_self.PDDLVersionOfMe.preconditions.le.leAnd2.add(le)
			} else {
				val LogicalExpressionDM and = PDDLFactory.eINSTANCE.createLogicalExpressionDM
				and.leAnd1 = _self.PDDLVersionOfMe.preconditions.le
				and.leAnd2.add(le)
				_self.PDDLVersionOfMe.preconditions.le = and
			}
			
			return finalVariableName
		}
	}
	
	def Optional<String> unassignmentTool(LogicalExpressionDM le, String predicateName, String firstVariableName) {
		if (le.p !== null && le.p.predicatName.equals(predicateName) && le.p.variablesList.get(0)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          .variableName.equals("?" + firstVariableName)) {
			return Optional.of(le.p.variablesList.get(1).variableName)
		} else {
			return Optional.empty()
		}
	}
	
	def String finalUnassignerVariableName(String variableDefaultName) {
		var Boolean alreadyExists = false
		
		for (parameters : _self.PDDLVersionOfMe.parameters.parametersList.variablesList) {
			for (parameter : parameters.variables) {
				if (parameter.variableName.equals("?" + variableDefaultName)) {
					alreadyExists = true
				}
			}
		}
		
		if (!alreadyExists) {
			return variableDefaultName
		}
		
		var int r = -1
		var String testVariableName
		while (alreadyExists) {
			alreadyExists = false
			r++
			testVariableName = variableDefaultName + r
			
			for (parameters : _self.PDDLVersionOfMe.parameters.parametersList.variablesList) {
				for (parameter : parameters.variables) {
					if (parameter.variableName.equals("?" + testVariableName)) {
						alreadyExists = true
					}
				}
			}
		}
		
		return testVariableName
	}
	
}

class WorkLogicalExpressionDM {
	
	public LogicalExpressionDM le
	public boolean isAlwaysTrue
	public String valueName
	
	new(LogicalExpressionDM newLe) {
		le = newLe
		isAlwaysTrue = false
	}
	
	new(PredicatDM newP) {
		le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = newP
		isAlwaysTrue = false
	}
	
	new(String status) {
		if (status.equals("true")) {
			le = null
			isAlwaysTrue = true
		} else {
			le = null
			isAlwaysTrue = false
		}
	}
	
	def LogicalExpressionDM getLEValue() {
		if (le !== null) {
			return le
		} else {
			val LogicalExpressionDM help = PDDLFactory.eINSTANCE.createLogicalExpressionDM
			help.v = PDDLFactory.eINSTANCE.createVariable
			help.v.variableName = "?ERROR"
			return help //TODO À changer ???
		}
	}
	
}

class WorkLogicalExpressionPB {
	
	public LogicalExpressionPB le
	public boolean isAlwaysTrue
	
	new(LogicalExpressionPB newLe) {
		le = newLe
		isAlwaysTrue = false
	}
	
	new(PredicatPB newP) {
		le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
		le.p = newP
		isAlwaysTrue = false
	}
	
	new(String status) {
		if (status.equals("true")) {
			le = null
			isAlwaysTrue = true
		} else {
			le = null
			isAlwaysTrue = false
		}
	}
	
	def LogicalExpressionPB getLEValue() {
		if (le !== null) {
			return le
		} else {
			val LogicalExpressionPB help = PDDLFactory.eINSTANCE.createLogicalExpressionPB
			help.v = PDDLFactory.eINSTANCE.createVariable
			help.v.variableName = "?ERROR"
			return help //TODO À changer ???
		}
	}
	
}

@Aspect(className=GuardParameter)
class GuardParameterAspect extends SymbolAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/
	def HashMap<String, String> recoverParametersFromABSGuardParameters() {
		
	}

}

@Aspect(className=Expression)
abstract class ExpressionAspect {
	/*
	def LogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		return PDDLFactory.eINSTANCE.createLogicalExpressionDM
	}
	*/
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction)
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context)
	
	def WorkLogicalExpressionPB recoverPDDLProblemLogicalExpressionFromABSExpression()
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context)
	
	def String recoverName()
	
	def String recoverAssetTypeFeatureName(AssetBasedSystem context)
	
	def WorkLogicalExpressionDM recoverForAller(AssetBasedSystem context, GuardedAction currentAction, String variableName, WorkLogicalExpressionDM inner)
	
}

@Aspect(className=BinaryExpression)
abstract class BinaryExpressionAspect extends ExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystemAspect context, GuardedAction currentAction)
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context)
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context)
	
}

@Aspect(className=ImpliesExpression)
class ImpliesExpressionAspect extends BinaryExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le
		
		if (_self.rhs === null) {
			le = _self.lhs.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue
		} else {
			le = context.whenOrImply(currentAction.doingEffects, _self.lhs.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue, _self.rhs.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue)
		}
		
		return new WorkLogicalExpressionDM(le)
	}
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context) {
		var LogicalExpressionPB le
		
		if (_self.rhs === null) {
			le = _self.lhs.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue
		} else {
			le.leImply1 = _self.lhs.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue
			le.leImply2 = _self.rhs.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue
		}
		
		return new WorkLogicalExpressionPB(le)
	}
	
}

@Aspect(className=OrExpression)
class OrExpressionAspect extends BinaryExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le
		
		if (_self.rhs === null) {
			le = _self.lhs.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue
		} else {
			le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
			
			val ArrayList<LogicalExpressionDM> orList = new ArrayList<LogicalExpressionDM>
			
			orList.addAll(_self.orAggregationDM(_self.lhs.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue))
			orList.addAll(_self.orAggregationDM(_self.rhs.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue))
			
			le.leOr1 = orList.remove(0)
			for (eachLE : orList) {
				le.leOr2.add(eachLE)
			}
		}
		
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<LogicalExpressionDM> orAggregationDM(LogicalExpressionDM currentLE) {
		val ArrayList<LogicalExpressionDM> currentList =  new ArrayList<LogicalExpressionDM>
		
		if (currentLE.leOr1 !== null) {
			currentList.addAll(_self.orAggregationDM(currentLE.leOr1))
		} else {
			currentList.add(currentLE)
		}
		
		for (newLE : currentLE.leOr2) {
			if (newLE !== null) {
				currentList.addAll(_self.orAggregationDM(newLE))
			} else {
				currentList.add(newLE)
			}
		}
		
		return currentList
	}
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context) {
		var LogicalExpressionPB le
		
		if (_self.rhs === null) {
			le = _self.lhs.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue
		} else {
			le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
			
			val ArrayList<LogicalExpressionPB> orList = new ArrayList<LogicalExpressionPB>
			
			orList.addAll(_self.orAggregationPB(_self.lhs.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue))
			orList.addAll(_self.orAggregationPB(_self.rhs.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue))
			
			le.leOr1 = orList.remove(0)
			for (eachLE : orList) {
				le.leOr2.add(eachLE)
			}
		}
		
		return new WorkLogicalExpressionPB(le)
	}
	
	def List<LogicalExpressionPB> orAggregationPB(LogicalExpressionPB currentLE) {
		val ArrayList<LogicalExpressionPB> currentList =  new ArrayList<LogicalExpressionPB>
		
		if (currentLE.leOr1 !== null) {
			currentList.addAll(_self.orAggregationPB(currentLE.leOr1))
		} else {
			currentList.add(currentLE)
		}
		
		for (newLE : currentLE.leOr2) {
			if (newLE !== null) {
				currentList.addAll(_self.orAggregationPB(newLE))
			} else {
				currentList.add(newLE)
			}
		}
		
		return currentList
	}
	
}

@Aspect(className=AndExpression)
class AndExpressionAspect extends BinaryExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le
		
		if (_self.rhs === null) {
			le = _self.lhs.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue
		} else {
			le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
			
			val ArrayList<LogicalExpressionDM> andList = new ArrayList<LogicalExpressionDM>
			
			andList.addAll(_self.addAggregationDM(_self.lhs.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue))
			andList.addAll(_self.addAggregationDM(_self.rhs.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue))
			
			le.leAnd1 = andList.remove(0)
			for (eachLE : andList) {
				le.leAnd2.add(eachLE)
			}
		}
		
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<LogicalExpressionDM> addAggregationDM(LogicalExpressionDM currentLE) {
		val ArrayList<LogicalExpressionDM> currentList =  new ArrayList<LogicalExpressionDM>
		
		if (currentLE.leAnd1 !== null) {
			currentList.addAll(_self.addAggregationDM(currentLE.leAnd1))
		} else {
			currentList.add(currentLE)
		}
		
		for (newLE : currentLE.leAnd2) {
			if (newLE !== null) {
				currentList.addAll(_self.addAggregationDM(newLE))
			} else {
				currentList.add(newLE)
			}
		}
		
		return currentList
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		System.out.println("ICI :" + _self.lhs + _self.rhs)
		val List<PredicatPB> list = _self.lhs.recoverPDDLInitPredicatesFromABSGoalPrecondition(context)
		list.addAll(_self.rhs.recoverPDDLInitPredicatesFromABSGoalPrecondition(context))
		
		return list
	}
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context) {
		var LogicalExpressionPB le
		
		if (_self.rhs === null) {
			le = _self.lhs.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue
		} else {
			le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
			
			val ArrayList<LogicalExpressionPB> andList = new ArrayList<LogicalExpressionPB>
			
			andList.addAll(_self.addAggregationPB(_self.lhs.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue))
			andList.addAll(_self.addAggregationPB(_self.rhs.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue))
			
			le.leAnd1 = andList.remove(0)
			for (eachLE : andList) {
				le.leAnd2.add(eachLE)
			}
		}
		
		return new WorkLogicalExpressionPB(le)
	}
	
	def List<LogicalExpressionPB> addAggregationPB(LogicalExpressionPB currentLE) {
		val ArrayList<LogicalExpressionPB> currentList =  new ArrayList<LogicalExpressionPB>
		
		if (currentLE.leAnd1 !== null) {
			currentList.addAll(_self.addAggregationPB(currentLE.leAnd1))
		} else {
			currentList.add(currentLE)
		}
		
		for (newLE : currentLE.leAnd2) {
			if (newLE !== null) {
				currentList.addAll(_self.addAggregationPB(newLE))
			} else {
				currentList.add(newLE)
			}
		}
		
		return currentList
	}
	
}

@Aspect(className=NotExpression)
class NotExpressionAspect extends ExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		
		le.leNot = _self.expression.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue
		
		if (le.leNot.leNot !== null) {
			le = le.leNot.leNot
		}
		
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		return new ArrayList<PredicatPB>()
	}
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context) {
		var LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
		
		le.leNot = _self.expression.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue
		
		if (le.leNot.leNot !== null) {
			le = le.leNot.leNot
		}
		
		return new WorkLogicalExpressionPB(le)
	}
	
}

@Aspect(className=EqualityComparisonExpression)
class EqualityComparisonExpressionAspect extends BinaryExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		if (_self.op.equals("!=")) {
			if (_self.lhs instanceof SymbolRef) {
				if (_self.rhs instanceof SymbolRef) {
					le = _self.equalityComparisonDM(_self.lhs, _self.rhs, 0, true, context, currentAction)
				} else if (_self.rhs instanceof MemberSelection) {
					if ((_self.rhs as MemberSelection).member instanceof AssetTypeReference) {
						le = _self.equalityComparisonDM(_self.rhs, _self.lhs, 1, true, context, currentAction)
					} else if ((_self.rhs as MemberSelection).member instanceof AssetTypeAttribute) {
						le.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
						le.leNot.p = context.getPredicatDM(context.predicatesName.get(((_self.rhs as MemberSelection).member as AssetTypeAttribute)) + "-" + (_self.lhs as SymbolRef).symbol.name, ((_self.rhs as MemberSelection).receiver as SymbolRef).symbol.name)
					}
				}
			} else if (_self.lhs instanceof MemberSelection) {
				if (_self.rhs instanceof SymbolRef) {
					if ((_self.lhs as MemberSelection).member instanceof AssetTypeReference) {
						le = _self.equalityComparisonDM(_self.lhs, _self.rhs, 1, true, context, currentAction)
					} else if ((_self.lhs as MemberSelection).member instanceof AssetTypeAttribute) {
						le.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
						le.leNot.p = context.getPredicatDM(context.predicatesName.get(((_self.lhs as MemberSelection).member as AssetTypeAttribute)) + "-" + (_self.rhs as SymbolRef).symbol.name, ((_self.lhs as MemberSelection).receiver as SymbolRef).symbol.name)
					}
				} else if (_self.rhs instanceof MemberSelection) {
					le = _self.equalityComparisonDM(_self.lhs, _self.rhs, 2, true, context, currentAction)
				} else if (_self.rhs instanceof BooleanConstant) {
					if ((_self.rhs as BooleanConstant).value.equals("true")) {
						le.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
						le.leNot.p = context.getPredicatDM(context.predicatesName.get(((_self.lhs as MemberSelection).member as AssetTypeAttribute)), ((_self.lhs as MemberSelection).receiver as SymbolRef).symbol.name)
					} else if ((_self.rhs as BooleanConstant).value.equals("false")) {
						le.p = context.getPredicatDM(context.predicatesName.get(((_self.lhs as MemberSelection).member as AssetTypeAttribute)), ((_self.lhs as MemberSelection).receiver as SymbolRef).symbol.name)
					}
				}
			} else if (_self.lhs instanceof BooleanConstant) {
				if (_self.rhs instanceof MemberSelection) {
					if ((_self.lhs as BooleanConstant).value.equals("true")) {
						le.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
						
						le.leNot.p = context.getPredicatDM(context.predicatesName.get(((_self.rhs as MemberSelection).member as AssetTypeAttribute)), ((_self.rhs as MemberSelection).receiver as SymbolRef).symbol.name)
					} else if ((_self.lhs as BooleanConstant).value.equals("false")) {
						le.p = context.getPredicatDM(context.predicatesName.get(((_self.rhs as MemberSelection).member as AssetTypeAttribute)), ((_self.rhs as MemberSelection).receiver as SymbolRef).symbol.name)
					}
				}
			}
		} else {
			if (_self.lhs instanceof SymbolRef) {
				if (_self.rhs instanceof SymbolRef) {
					le = _self.equalityComparisonDM(_self.lhs, _self.rhs, 0, false, context, currentAction)
				} else if (_self.rhs instanceof MemberSelection) {
					if ((_self.rhs as MemberSelection).member instanceof AssetTypeReference) {
						le = _self.equalityComparisonDM(_self.rhs, _self.lhs, 1, false, context, currentAction)
					} else if ((_self.rhs as MemberSelection).member instanceof AssetTypeAttribute) {
						le.p = context.getPredicatDM(context.predicatesName.get(((_self.rhs as MemberSelection).member as AssetTypeAttribute)) + "-" + (_self.lhs as SymbolRef).symbol.name, ((_self.rhs as MemberSelection).receiver as SymbolRef).symbol.name)
					}
				}
			} else if (_self.lhs instanceof MemberSelection) {
				if (_self.rhs instanceof SymbolRef) {
					if ((_self.lhs as MemberSelection).member instanceof AssetTypeReference) {
						le = _self.equalityComparisonDM(_self.lhs, _self.rhs, 1, false, context, currentAction)
					} else if ((_self.lhs as MemberSelection).member instanceof AssetTypeAttribute) {
						le.p = context.getPredicatDM(context.predicatesName.get(((_self.lhs as MemberSelection).member as AssetTypeAttribute)) + "-" + (_self.rhs as SymbolRef).symbol.name, ((_self.lhs as MemberSelection).receiver as SymbolRef).symbol.name)
					}
				} else if (_self.rhs instanceof MemberSelection) {
					le = _self.equalityComparisonDM(_self.lhs, _self.rhs, 2, false, context, currentAction)
				} else if (_self.rhs instanceof BooleanConstant) {
					if ((_self.rhs as BooleanConstant).value.equals("true")) {
						le.p = context.getPredicatDM(context.predicatesName.get(((_self.lhs as MemberSelection).member as AssetTypeAttribute)), ((_self.lhs as MemberSelection).receiver as SymbolRef).symbol.name)
					} else if ((_self.rhs as BooleanConstant).value.equals("false")) {
						le.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
						le.leNot.p = context.getPredicatDM(context.predicatesName.get(((_self.lhs as MemberSelection).member as AssetTypeAttribute)), ((_self.lhs as MemberSelection).receiver as SymbolRef).symbol.name)
					}
				}
			} else if (_self.lhs instanceof BooleanConstant) {
				if (_self.rhs instanceof MemberSelection) {
					if ((_self.lhs as BooleanConstant).value.equals("true")) {
						le.p = context.getPredicatDM(context.predicatesName.get(((_self.rhs as MemberSelection).member as AssetTypeAttribute)), ((_self.rhs as MemberSelection).receiver as SymbolRef).symbol.name)
					} else if ((_self.lhs as BooleanConstant).value.equals("false")) {
						le.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
						
						le.leNot.p = context.getPredicatDM(context.predicatesName.get(((_self.rhs as MemberSelection).member as AssetTypeAttribute)), ((_self.rhs as MemberSelection).receiver as SymbolRef).symbol.name)
					}
				}
			}
			
		}
		
		return new WorkLogicalExpressionDM(le)
	}
	
	def LogicalExpressionDM equalityComparisonDM(Expression exp1, Expression exp2, int nbrMS, boolean not, AssetBasedSystem context, GuardedAction currentAction) {
		val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		
		if (nbrMS == 0) {
			le.VEqual1 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
			
			le.VEqual1.v = PDDLFactory.eINSTANCE.createVariable
			
			le.VEqual1.v.variableName = "?" + (exp1 as SymbolRef).symbol.name
			
			val LogicalExpressionDM le2 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
			le2.v = PDDLFactory.eINSTANCE.createVariable
			le2.v.variableName = "?" + (exp2 as SymbolRef).symbol.name
			
			le.VEqual2.add(le2)
		} else if (nbrMS == 1) {
			le.p = context.getPredicatDM(context.predicatesName.get(((_self.lhs as MemberSelection).member as AssetTypeReference)), ((exp1 as MemberSelection).receiver as SymbolRef).symbol.name, (exp2 as SymbolRef).symbol.name)
		} else if (nbrMS == 2) {
			//TODO (en créant un nouveau paramètre d'action)
			System.out.println("not implemented yet")
			le.p = context.getPredicatDM("NOT-IMPLEMENTED", "Missing")
		} else {
			//TODO ERROR
			System.out.println("impossible")
			le.p = context.getPredicatDM("NOT-IMPLEMENTED", "Missing")
		}
		
		if (not) {
			val LogicalExpressionDM returnNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
			returnNot.leNot = le
			return returnNot
		} else {
			return le
		}
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		System.out.println(_self.lhs)
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		
		if (_self.op.equals("!=")) {
			
			if (_self.lhs instanceof SymbolRef) {
				if (_self.rhs instanceof SymbolRef) {
					list.addAll(_self.equalityComparisonPredicatPB(_self.lhs, _self.rhs, 0, true, context))
				} else if (_self.rhs instanceof MemberSelection) {
					list.addAll(_self.equalityComparisonPredicatPB(_self.rhs, _self.lhs, 1, true, context))
				}
			} else if (_self.lhs instanceof MemberSelection) {
				if (_self.rhs instanceof SymbolRef) {
					list.addAll(_self.equalityComparisonPredicatPB(_self.lhs, _self.rhs, 1, true, context))
				} else if (_self.rhs instanceof MemberSelection) {
					list.addAll(_self.equalityComparisonPredicatPB(_self.lhs, _self.rhs, 2, true, context))
				}
			}
		} else {
			if (_self.lhs instanceof SymbolRef) {
				if (_self.rhs instanceof SymbolRef) {
					list.addAll(_self.equalityComparisonPredicatPB(_self.lhs, _self.rhs, 0, false, context))
				} else if (_self.rhs instanceof MemberSelection) {
					list.addAll(_self.equalityComparisonPredicatPB(_self.rhs, _self.lhs, 1, false, context))
				}
			} else if (_self.lhs instanceof MemberSelection) {
				if (_self.rhs instanceof SymbolRef) {
					list.addAll(_self.equalityComparisonPredicatPB(_self.lhs, _self.rhs, 1, false, context))
				} else if (_self.rhs instanceof MemberSelection) {
					list.addAll(_self.equalityComparisonPredicatPB(_self.lhs, _self.rhs, 2, false, context))
				}
			}
			/* TODO ?
			else if (_self.lhs instanceof QualifiedName) {
				System.out.println("Not Implemented Yet")
			}
			*/
			
		}
		
		return list
	}
	
	def List<PredicatPB> equalityComparisonPredicatPB(Expression exp1, Expression exp2, int nbrMS, boolean not, AssetBasedSystem context) {
		
		System.out.println(nbrMS)
		val ArrayList<PredicatPB> list = new ArrayList<PredicatPB>()
		
		var PredicatPB p = PDDLFactory.eINSTANCE.createPredicatPB
		
		if (nbrMS == 0) {
			// TODO
		} else if (nbrMS == 1) {
			p = context.getPredicatPB(context.predicatesName.get(((_self.lhs as MemberSelection).member as AssetTypeReference)), ((exp1 as MemberSelection).receiver as SymbolRef).symbol.name, (exp2 as SymbolRef).symbol.name)
		} else if (nbrMS == 2) {
			//TODO (en créant un nouveau paramètre d'action)
			System.out.println("not implemented yet")
		} else {
			//TODO ERROR
			System.out.println("impossible")
		}
		
		list.add(p)
		
		return list
	}
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context) {
		//TODO Boolean et type énumérés aussi
		var LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
		
		if (_self.op.equals("!=")) {
			
			if (_self.lhs instanceof SymbolRef) {
				if (_self.rhs instanceof SymbolRef) {
					le = _self.equalityComparisonPB(_self.lhs, _self.rhs, 0, true, context)
				} else if (_self.rhs instanceof MemberSelection) {
					le = _self.equalityComparisonPB(_self.rhs, _self.lhs, 1, true, context)
				}
			} else if (_self.lhs instanceof MemberSelection) {
				if (_self.rhs instanceof SymbolRef) {
					le = _self.equalityComparisonPB(_self.lhs, _self.rhs, 1, true, context)
				} else if (_self.rhs instanceof MemberSelection) {
					le = _self.equalityComparisonPB(_self.lhs, _self.rhs, 2, true, context)
				}
			}
		} else {
			if (_self.lhs instanceof SymbolRef) {
				if (_self.rhs instanceof SymbolRef) {
					le = _self.equalityComparisonPB(_self.lhs, _self.rhs, 0, false, context)
				} else if (_self.rhs instanceof MemberSelection) {
					le = _self.equalityComparisonPB(_self.rhs, _self.lhs, 1, false, context)
				}
			} else if (_self.lhs instanceof MemberSelection) {
				if (_self.rhs instanceof SymbolRef) {
					le = _self.equalityComparisonPB(_self.lhs, _self.rhs, 1, false, context)
				} else if (_self.rhs instanceof MemberSelection) {
					le = _self.equalityComparisonPB(_self.lhs, _self.rhs, 2, false, context)
				}
			}
			
		}
		
		return new WorkLogicalExpressionPB(le)
	}
	
	def LogicalExpressionPB equalityComparisonPB(Expression exp1, Expression exp2, int nbrMS, boolean not, AssetBasedSystem context) {
		val LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
		
		if (nbrMS == 0) {
			le.VEqual1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
			
			le.VEqual1.v = PDDLFactory.eINSTANCE.createVariable
			
			le.VEqual1.v.variableName = "?" + (exp1 as SymbolRef).symbol.name
			
			val LogicalExpressionPB le2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
			le2.v = PDDLFactory.eINSTANCE.createVariable
			le2.v.variableName = "?" + (exp2 as SymbolRef).symbol.name
			
			le.VEqual2.add(le2)
		} else if (nbrMS == 1) {
			le.p = context.getPredicatPB(context.predicatesName.get(((_self.lhs as MemberSelection).member as AssetTypeReference)), ((exp1 as MemberSelection).receiver as SymbolRef).symbol.name, (exp2 as SymbolRef).symbol.name)
		} else if (nbrMS == 2) {
			//TODO (en créant un nouveau paramètre d'action)
			System.out.println("not implemented yet")
		} else {
			//TODO ERROR
			System.out.println("impossible")
		}
		
		if (not) {
			val LogicalExpressionPB returnNot = PDDLFactory.eINSTANCE.createLogicalExpressionPB
			returnNot.leNot = le
			return returnNot
		} else {
			return le
		}
	}
	
}

@Aspect(className=InequalityComparisonExpression)
class InequalityComparisonExpressionAspect extends BinaryExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		//TODO C'est pas du tout ça
		System.out.println("Useless 01 (?)")
		val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "test")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		System.out.println("Useless InitPB 01 (?)")
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		return list
	}
	
}

class WorkExpressionDM {
	
	//UPPER/PREVIOUS PART
	//Expression
	//Anchor
	//VariableName
	
	//INTERIOR PART
	//Expression
	//Anchor
	//VariableName
	
	//LOWER/NEXT PART
	//Expression
	//Anchor
	//VariableName
	
}

@Aspect(className=Action)
class ActionAspect {
	
	var Boolean filter = false
	
	def String getTargetName(Expression target) {
		var String name = ""
		
		try {
			name = _self.getTargetName((_self.target as MemberSelection).receiver)
		} finally {
			name = ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name
		}
		
		return name
	}
	/*
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSAction(AssetBasedSystem context, GuardedAction currentAction) {
		return new WorkLogicalExpressionDM(context.getPredicatDM("Ceci", "Est", "Un", "Test"))
	}
	*/
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSAction(AssetBasedSystem context, GuardedAction currentAction) {
		System.out.println(_self.target)
		
		/*
		var WorkLogicalExpressionDM argsForAller = _self.args.get(0).recoverForAller(context, currentAction, "x", null)
		var WorkLogicalExpressionDM receiverForAller = _self.target.recoverForAller(context, currentAction, "y", null)
		*/
		if ((_self.target as MemberSelection).methodInvocation && !_self.filter) {
			System.out.println("FILTERING")
			System.out.println(_self.target)
			System.out.println((_self.target as MemberSelection).receiver)
			System.out.println(((_self.target as MemberSelection).receiver as MemberSelection).receiver)
			//System.out.println((((_self.target as MemberSelection).receiver as MemberSelection).receiver as SymbolRef).symbol.name)
			//System.out.println(((_self.target as MemberSelection).receiver as SymbolRef).symbol.name)
			//System.out.println((_self.args.get(0) as SymbolRef).symbol.name)
			
			//return (((_self.target as MemberSelection).receiver as MemberSelection).receiver as Action).recoverPDDLDomainLogicalExpressionFromABSAction(context, currentAction)
			
			_self.filter = true
			
			return ((_self.target as MemberSelection).args.get(0) as LambdaExpression).recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction)
			
		} else {
			
			switch _self.actionType {
				case ADD: {
					System.out.println("FILTERING")
					System.out.println(_self.target)
					System.out.println((_self.target as MemberSelection).receiver)
					System.out.println(((_self.target as MemberSelection).receiver as SymbolRef).symbol.name)
					//System.out.println((((_self.target as MemberSelection).receiver as MemberSelection).receiver as SymbolRef).symbol.name)
					//System.out.println(((_self.target as MemberSelection).receiver as SymbolRef).symbol.name)
					//System.out.println((_self.args.get(0) as SymbolRef).symbol.name)
					
					//return (((_self.target as MemberSelection).receiver as MemberSelection).receiver as Action).recoverPDDLDomainLogicalExpressionFromABSAction(context, currentAction)
					
					_self.filter = true
					
					return new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name))
					
					//return ((_self.target as MemberSelection).args.get(0) as LambdaExpression).recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction)
					
					/*
					var LogicalExpressionDM global = _self.concatForAller(argsForAller.getLEValue, receiverForAller.getLEValue)
					return new WorkLogicalExpressionDM(_self.concatForAller(global, new WorkLogicalExpressionDM(context.getPredicatDM(_self.target.recoverAssetTypeFeatureName(context), argsForAller.valueName, receiverForAller.valueName)).getLEValue()))
					* 
					*/
				}
				case ADD_ALL: {
					var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					
					val String newVar = "XXX"
					
					le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
					le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdForall.^var.variableName = "?" + newVar
					le.tdForall.type = PDDLFactory.eINSTANCE.createType
					le.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
					// ( forall (???) ( imply/when ... ... ) )
					
					le.leForall = context.whenOrImply(currentAction.doingEffects, (new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVar))).getLEValue, (new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get(((_self.target as MemberSelection).member as AssetTypeReference)), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar))).getLEValue)
					// ( forall (...) ( imply/when ??? ??? ) )
					
					return new WorkLogicalExpressionDM(le)
				}
				case ASSIGN: {
					// TODO Boolean too et énumérés
					
					val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					
					// Without leWhen1
					val String newVarName = currentAction.unassignment(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, "unassigner", ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name, context)
					
					
					//NewPostcondition
					
					// Unassigner
					le.leAnd1 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					le.leAnd1.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					le.leAnd1.leNot.p = context.getPredicatDM(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					
					// Assigner
					val LogicalExpressionDM and2 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					and2.p = context.getPredicatDM(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
					
					le.leAnd2.add(and2)
					/*
					
					//OBJECTIF : ( and ( forall (...) ( imply/when ( and ( not (= ...) ) (...) ) ( not (...) ) ) ... ) )
					//forAll pour désassigner toutes les autres valeurs lié à cette référence/cet attribut
					le.leAnd1 = PDDLFactory.eINSTANCE.createLogicalExpressionDM // unassigner
					
					val String newVar = "unassignerVar"
					
					le.leAnd1.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
					le.leAnd1.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
					le.leAnd1.tdForall.^var.variableName = "?" + newVar
					le.leAnd1.tdForall.type = PDDLFactory.eINSTANCE.createType
					le.leAnd1.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
					// ( and ( forall (???) ( imply/when ( and ( not (= ...) ) (...) ) ( not (...) ) ) ... ) )
					
					var LogicalExpressionDM and = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					and.leAnd1 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					and.leAnd1.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					and.leAnd1.leNot.VEqual1 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					and.leAnd1.leNot.VEqual1.v = PDDLFactory.eINSTANCE.createVariable
					and.leAnd1.leNot.VEqual1.v.variableName = "?" + newVar
					val LogicalExpressionDM variable2 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					variable2.v = PDDLFactory.eINSTANCE.createVariable
					variable2.v.variableName = "?" + (_self.args.get(0) as SymbolRef).symbol.name
					and.leAnd1.leNot.VEqual2.add(variable2)
					// ( and ( forall (...) ( imply ( and ( not (= ???) ) (...) ) ( not (...) ) ) ... ) )
					
					val LogicalExpressionDM implyAnd2 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					implyAnd2.p = context.getPredicatDM(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
					and.leAnd2.add(implyAnd2)
					// ( and ( forall (...) ( imply ( and ( not (= ...) ) (???) ) ( not (...) ) ) ... ) )
					
					val LogicalExpressionDM wi2 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					wi2.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					wi2.leNot.p = context.getPredicatDM(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
					// ( and ( forall (...) ( imply ( and ( not (= ...) ) (...) ) ( not (???) ) ) ... ) )
					
					le.leAnd1.leForall = context.whenOrImply(currentAction.doingEffects, and, wi2)
					
					// Assigner
					val LogicalExpressionDM and2 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					and2.p = context.getPredicatDM(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
					*/
					// ( and ( forall (...) ( imply/when ( and ( not (= ...) ) (...) ) ( not (...) ) ) ??? ) )
					
					return new WorkLogicalExpressionDM(le)
				}
				case CLEAR: {
					val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					
					val String newVar = "XXX"
					
					le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
					le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdForall.^var.variableName = "?" + newVar
					le.tdForall.type = PDDLFactory.eINSTANCE.createType
					le.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
					// ( forall (???) ( imply/when (...) ( not (...) ) ) )
					
					val LogicalExpressionDM wi2 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					wi2.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					wi2.leNot.p = context.getPredicatDM(context.predicatesName.get(((_self.target as MemberSelection).member as AssetTypeReference)), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
					// ( forall (...) ( imply/when (...) ( not (???) ) ) )
					
					le.leForall = context.whenOrImply(currentAction.doingEffects, (new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get(((_self.target as MemberSelection).member as AssetTypeReference)), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar))).getLEValue, wi2)
					// ( forall (...) ( imply/when (...) ( not (...) ) ) )
					
					return new WorkLogicalExpressionDM(le)
				}
				case FOR_ALL: {
					val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					
					le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
					le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdForall.^var.variableName = "?" + _self.lambdaAction.lambdaParameter.name
					le.tdForall.type = PDDLFactory.eINSTANCE.createType
					le.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
					// ( forall (???) ( imply/when ... ... ) )
					
					le.leForall = context.whenOrImply(currentAction.doingEffects, (new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, _self.lambdaAction.lambdaParameter.name))).getLEValue, _self.lambdaAction.recoverPDDLDomainLogicalExpressionFromABSExpression(context, currentAction).getLEValue())
					// ( forall (...) ( imply/when ??? ??? ) )
					
					return new WorkLogicalExpressionDM(le)
				}
				case REMOVE: {
					val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					le.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					
					le.leNot.p = context.getPredicatDM(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
					// (not (???))
					
					return new WorkLogicalExpressionDM(le)
				}
				case REMOVE_ALL: {
					val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					
					val String newVar = "XXX"
					
					le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
					le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdForall.^var.variableName = "?" + newVar
					le.tdForall.type = PDDLFactory.eINSTANCE.createType
					le.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
					// ( forall (???) ( imply/when ... ( not (...) ) ) )
					
					val LogicalExpressionDM wi2 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					wi2.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					wi2.leNot.p = context.getPredicatDM(context.predicatesName.get(((_self.target as MemberSelection).member as AssetTypeReference)), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
					// ( forall (...) ( imply/when (...) ( not (???) ) ) )
					
					le.leForall = context.whenOrImply(currentAction.doingEffects, (new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVar))).getLEValue, wi2)
					// ( forall (...) ( imply/when (...) ( not (...) ) ) )
					
					return new WorkLogicalExpressionDM(le)
				}
			
			}
		}
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val ArrayList<PredicatPB> list = new ArrayList<PredicatPB>()
		
		var PredicatPB p = PDDLFactory.eINSTANCE.createPredicatPB
		
		switch _self.actionType {
			case ADD: {
				p = context.getPredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
				list.add(p)
				return list
			}
			case ADD_ALL: {
				list.add(p) // À implémenter
				return list
			}
			case ASSIGN: {
				// TODO Boolean too et énumérés
				// TODO Always use the same Variable
				
				list.add(p) // À implémenter
				return list
			}
			case CLEAR: {
				list.add(p) // À implémenter
				return list
			}
			case FOR_ALL: {
				list.add(p) // À implémenter
				return list
			}
			case REMOVE: {
				list.add(p) // À implémenter
				return list
			}
			case REMOVE_ALL: {
				list.add(p) // À implémenter
				return list
			}
		}
	}
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context) {
		// TODO Optimisable (une seule Variable xxx)
		// + Nom de la Variable ?xxx
		switch _self.actionType {
			case ADD: {
				val LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.p = context.getPredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
				return new WorkLogicalExpressionPB(le)
			}
			case ADD_ALL: {
				val LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				
				val String newVar = "?" + "XXX"
				
				le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
				le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
				le.tdForall.^var.variableName = newVar
				le.tdForall.type = PDDLFactory.eINSTANCE.createType
				le.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
				// ( forall (???) ( imply/when ... ... ) )
				
				le.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply1.p = context.getPredicatPB(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
				// ( forall (...) ( imply ??? ... ) )
				
				le.leForall.leImply2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply2.p = context.getPredicatPB(context.predicatesName.get(((_self.target as MemberSelection).member as AssetTypeReference)), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
				// ( forall (...) ( imply ... ??? ) )
				
				return new WorkLogicalExpressionPB(le)
			}
			case ASSIGN: {//required for PDDLProblem ???
				// TODO Boolean too et énumérés
				
				val LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				
				/* Without leWhen1
				val String newVarName = "?" + "m"
				
				val PDDLObject v = PDDLFactory.eINSTANCE.createPDDLObject
				v.objectName = newVarName
				
				
				//New ActionParameter
				
				val TypedPDDLObject tv = PDDLFactory.eINSTANCE.createTypedPDDLObject
				tv.type = PDDLFactory.eINSTANCE.createType
				tv.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
				tv.variables.add(v)
				
				if (currentAction.PDDLVersionOfMe.parameters === null) {
					currentAction.PDDLVersionOfMe.parameters = PDDLFactory.eINSTANCE.createParameters
					currentAction.PDDLVersionOfMe.parameters.parametersList = PDDLFactory.eINSTANCE.createTypedPDDLObjectsList
					currentAction.PDDLVersionOfMe.parameters.parametersList.objects.add(tv)
				} else {
					currentAction.PDDLVersionOfMe.parameters.parametersList.objects.add(tv)
				}
				
				
				//New Precondition
				
				val LogicalExpressionPB newPreconditionLE = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				newPreconditionLE.p = context.getpredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
				
				if (currentAction.PDDLVersionOfMe.preconditions === null) {
					currentAction.PDDLVersionOfMe.preconditions = PDDLFactory.eINSTANCE.createPreconditions
					currentAction.PDDLVersionOfMe.preconditions.le = newPreconditionLE
				} else {
					if (currentAction.PDDLVersionOfMe.preconditions.le.leAnd1 === null) {
						val LogicalExpressionPB newAnd = PDDLFactory.eINSTANCE.createLogicalExpressionPB
						newAnd.leAnd1 = currentAction.PDDLVersionOfMe.preconditions.le
						newAnd.leAnd2.add(newPreconditionLE)
						currentAction.PDDLVersionOfMe.preconditions.le = newAnd
					} else {
						currentAction.PDDLVersionOfMe.preconditions.le.leAnd2.add(newPreconditionLE)
					}
				}
				
				
				//NewPostcondition
				
				// Unassigner
				le.leAnd1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leAnd1.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leAnd1.leNot.p = context.getpredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
				
				// Assigner
				val LogicalExpressionPB and2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				and2.p = context.getpredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
				le.leAnd2.add(and2)
				*/
				
				//OBJECTIF : ( and ( forall (...) ( imply/when ( and ( not (= ...) ) (...) ) ( not (...) ) ) ... ) )
				//forAll pour désassigner toutes les autres valeurs lié à cette référence/cet attribut
				le.leAnd1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB // unassigner
				
				val String newVar = "?" + "unassignerVar"
				
				le.leAnd1.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
				le.leAnd1.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
				le.leAnd1.tdForall.^var.variableName = newVar
				le.leAnd1.tdForall.type = PDDLFactory.eINSTANCE.createType
				le.leAnd1.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
				// ( and ( forall (???) ( imply/when ( and ( not (= ...) ) (...) ) ( not (...) ) ) ... ) )
				
				le.leAnd1.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leAnd1.leForall.leImply1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leAnd1.leForall.leImply1.leAnd1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leAnd1.leForall.leImply1.leAnd1.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leAnd1.leForall.leImply1.leAnd1.leNot.VEqual1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leAnd1.leForall.leImply1.leAnd1.leNot.VEqual1.v = PDDLFactory.eINSTANCE.createVariable
				le.leAnd1.leForall.leImply1.leAnd1.leNot.VEqual1.v.variableName = newVar
				val LogicalExpressionPB variable2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				variable2.v = PDDLFactory.eINSTANCE.createVariable
				variable2.v.variableName = (_self.args.get(0) as SymbolRef).symbol.name
				le.leAnd1.leForall.leImply1.leAnd1.leNot.VEqual2.add(variable2)
				// ( and ( forall (...) ( imply ( and ( not (= ???) ) (...) ) ( not (...) ) ) ... ) )
				
				val LogicalExpressionPB implyAnd2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				implyAnd2.p = context.getPredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
				le.leAnd1.leForall.leImply1.leAnd2.add(implyAnd2)
				// ( and ( forall (...) ( imply ( and ( not (= ...) ) (???) ) ( not (???) ) ) ... ) )
				
				le.leAnd1.leForall.leImply2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leAnd1.leForall.leImply2.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leAnd1.leForall.leImply2.leNot.p = context.getPredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
				// ( and ( forall (...) ( imply ( and ( not (= ...) ) (...) ) ( not (???) ) ) ... ) )
				
				// Assigner
				val LogicalExpressionPB and2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				and2.p = context.getPredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
				le.leAnd2.add(and2)
				// ( and ( forall (...) ( imply/when ( and ( not (= ...) ) (...) ) ( not (...) ) ) ??? ) )
				
				return new WorkLogicalExpressionPB(le)
			}
			case CLEAR: {
				val LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				
				val String newVar = "XXX"
				
				le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
				le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
				le.tdForall.^var.variableName = newVar
				le.tdForall.type = PDDLFactory.eINSTANCE.createType
				le.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
				// ( forall (???) ( imply/when (...) ( not (...) ) ) )
				
				le.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply1.p = context.getPredicatPB(context.predicatesName.get(((_self.target as MemberSelection).member as AssetTypeReference)), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
				// ( forall (...) ( imply (???) ( not (...) ) ) )
				
				le.leForall.leImply2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply2.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply2.leNot.p = context.getPredicatPB(context.predicatesName.get(((_self.target as MemberSelection).member as AssetTypeReference)), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
				// ( forall (...) ( imply (...) ( not (???) ) ) )
				
				return new WorkLogicalExpressionPB(le)
			}
			case FOR_ALL: {
				val LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				
				le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
				le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
				le.tdForall.^var.variableName = "?" + _self.lambdaAction.lambdaParameter.name
				le.tdForall.type = PDDLFactory.eINSTANCE.createType
				le.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
				// ( forall (???) ( imply ... ... ) )
				
				le.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply1.p = context.getPredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, _self.lambdaAction.lambdaParameter.name)
				// ( forall (...) ( imply ??? ... ) )
				
				le.leForall.leImply2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply2 = _self.lambdaAction.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue()
				// ( forall (...) ( imply ... ??? ) )
				
				return new WorkLogicalExpressionPB(le)
			}
			case REMOVE: {
				val LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				
				le.leNot.p = context.getPredicatPB(context.predicatesName.get((_self.target as MemberSelection).member), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
				// (not (???))
				
				return new WorkLogicalExpressionPB(le)
			}
			case REMOVE_ALL: {
				val LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				
				val String newVar = "?" + "XXX"
				
				le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
				le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
				le.tdForall.^var.variableName = newVar
				le.tdForall.type = PDDLFactory.eINSTANCE.createType
				le.tdForall.type.typeName = ((_self.target as MemberSelection).member as AssetTypeReference).propertyType.name
				// ( forall (???) ( imply/when ... ... ) )
				
				le.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply1.p = context.getPredicatPB(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
				// ( forall (...) ( imply ??? ... ) )
				
				le.leForall.leImply2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply2.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionPB
				le.leForall.leImply2.leNot.p = context.getPredicatPB(context.predicatesName.get(((_self.target as MemberSelection).member as AssetTypeReference)), ((_self.target as MemberSelection).receiver as SymbolRef).symbol.name, newVar)
				// ( forall (...) ( imply ... ??? ) )
				
				return new WorkLogicalExpressionPB(le)
			}
		}
	}
	
	def LogicalExpressionDM concatForAller(LogicalExpressionDM le1, LogicalExpressionDM le2) {
		if (le1.leForall !== null) {
			if (le1.leForall.leWhen1 !== null) {
				if (le1.leForall.leWhen2 !== null) {
					return _self.concatForAller(le1.leForall.leWhen2, le2)
				} else {
					le1.leForall.leWhen2 = le2
				}
			} else if (le1.leForall.leImply1 !== null) {
				if (le1.leForall.leImply2 !== null) {
					return _self.concatForAller(le1.leForall.leImply2, le2)
				} else {
					le1.leForall.leImply2 = le2
				}
			}
		}
		return le1
	}
	
}

@Aspect(className=ConstantExpression)
abstract class ConstantExpressionAspect extends ExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction2PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction2PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=StringConstant)
class StringConstantAspect extends ConstantExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction3PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction3PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=IntConstant)
class IntConstantAspect extends ConstantExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction4PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction4PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=BooleanConstant)
class BooleanConstantAspect extends ConstantExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		return new WorkLogicalExpressionDM(_self.value)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction5PasEncoreImplementee")
		list.add(p)
		return list
	}
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context) {
		return new WorkLogicalExpressionPB(_self.value)
	}
	
}

@Aspect(className=VersionConstant)
class VersionConstantAspect extends ConstantExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction6PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction6PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=MemberSelection)
class MemberSelectionAspect extends ExpressionAspect {
	/*
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		return new WorkLogicalExpressionDM(context.getPredicatDM("Ceci", "Est", "Un", "Test"))
	}
	*/
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		val LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		if (_self.methodInvocation) {
			switch _self.member.name {
				case "contains": {
					if ((_self.receiver as MemberSelection).member instanceof AssetTypeReference) {
						le.p = context.getPredicatDM(_self.recoverAssetTypeFeatureName(context), _self.recoverName(), _self.args.get(0).recoverName())
					} else if ((_self.receiver as MemberSelection).member instanceof AssetTypeAttribute) {
						if (_self.args.get(0) instanceof SymbolRef) {
							System.out.println((_self.receiver as MemberSelection).member)
							System.out.println((_self.args.get(0) as SymbolRef).symbol.name)
							le.p = context.getPredicatDM(context.predicatesName.get((_self.receiver as MemberSelection).member) + "-" + (_self.args.get(0) as SymbolRef).symbol.name, ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name)
						} else if (_self.args.get(0) instanceof MemberSelection) {
							System.out.println(((((_self.receiver as MemberSelection).member as AssetTypeAttribute).attributeType as EnumDataType).enumLiteral))
							System.out.println((_self.args.get(0) as MemberSelection).member)
							
							for (value : ((((_self.receiver as MemberSelection).member as AssetTypeAttribute).attributeType as EnumDataType).enumLiteral)) {
								val LogicalExpressionDM wi2 = context.whenOrImply(currentAction.doingEffects, (new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get((_self.receiver as MemberSelection).member) + "-" + value.name, ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name))).getLEValue, (new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get((_self.receiver as MemberSelection).member) + "-" + value.name, ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name))).getLEValue)
								
								if (le.leAnd1 === null) {
									le.leAnd1 = wi2
								} else {
									le.leAnd2.add(wi2)
								}
							}
						}
					}
				}
				case "containsAll": {
					System.out.println(((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name)	// MemberSelection
					System.out.println((_self.receiver as MemberSelection).member)	// MemberSelection
					System.out.println(_self.member)
					System.out.println(context.predicatesName.get((_self.args.get(0) as MemberSelection).member))		// SymboleRef
					System.out.println(((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name)
					
					le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
					
					val String newVarName = "m"
					
					le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdForall.^var.variableName = "?" + newVarName
					le.tdForall.type = PDDLFactory.eINSTANCE.createType
					le.tdForall.type.typeName = ((_self.receiver as MemberSelection).member as AssetTypeReference).propertyType.name // TODO on suppose que les types sont identiques (sinon pas de compilation ?)
					
					le.leForall = context.whenOrImply(currentAction.doingEffects, (new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVarName))).getLEValue, (new WorkLogicalExpressionDM(context.getPredicatDM(context.predicatesName.get((_self.receiver as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, newVarName))).getLEValue)
					
					/* Without leWhen1
					le.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					
					
					le.leForall.leOr1 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					le.leForall.leOr1.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					le.leForall.leOr1.leNot.p = context.getPredicatDM(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					
					val LogicalExpressionDM predicatNew = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					predicatNew.p = context.getPredicatDM(context.predicatesName.get((_self.receiver as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					le.leForall.leOr2.add(predicatNew)
					*/
				}
				case "containsAny": {
					le.tdExists = PDDLFactory.eINSTANCE.createTypeDescr
					
					val String newVarName = "m"
					
					le.tdExists.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdExists.^var.variableName = "?" + newVarName
					le.tdExists.type = PDDLFactory.eINSTANCE.createType
					le.tdExists.type.typeName = ((_self.receiver as MemberSelection).member as AssetTypeReference).propertyType.name
					
					/*
					le.leExists = context.whenOrImply(currentAction.doingEffects, (new WorkLogicalExpressionDM(context.getPredicatDM(context.getPredicatDM(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVarName))).getLEValue, (new WorkLogicalExpressionDM(context.getPredicatDM(context.getPredicatDM(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, newVarName))).getLEValue)
					*/
					le.leExists = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					le.leExists.leAnd1 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					le.leExists.leAnd1.p = context.getPredicatDM(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					
					
					val LogicalExpressionDM and2 = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					and2.p = context.getPredicatDM(context.predicatesName.get((_self.receiver as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					
					le.leExists.leAnd2.add(and2)
				}
				case "isEmpty": {
					le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
					
					val String newVarName = "m"
					
					le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdForall.^var.variableName = "?" + newVarName
					le.tdForall.type = PDDLFactory.eINSTANCE.createType
					le.tdForall.type.typeName = ((_self.receiver as MemberSelection).member as AssetTypeReference).propertyType.name
					
					le.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					le.leForall.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionDM
					le.leForall.leNot.p = context.getPredicatDM(context.predicatesName.get((_self.receiver as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
				}
				case "filter": {
					System.out.println("FILTERING")
					System.out.println(_self.receiver)
					System.out.println(_self.args.get(0))
				}
				default: {
					throw new RuntimeException("Unknown method call " + _self.member.name)
				}
			}
		} else {
			le.p = context.getPredicatDM(context.predicatesName.get(_self.member), (_self.receiver as SymbolRef).symbol.name)
		}
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val ArrayList<PredicatPB> list = new ArrayList<PredicatPB>()
		
		var PredicatPB p = PDDLFactory.eINSTANCE.createPredicatPB
		if (_self.methodInvocation) {
			switch _self.member.name {
				case "contains": {
					p = context.getPredicatPB(context.predicatesName.get((_self.receiver as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
					list.add(p)
					return list
				}
				case "containsAll": {
				list.add(p) // À implémenter
				return list
				}
				case "containsAny": {
				list.add(p) // À implémenter
				return list
				}
				case "isEmpty": {
				list.add(p) // À implémenter
				return list
				}
				case "filter": {
					System.out.println("FILTERING")
					System.out.println(_self.receiver)
					System.out.println(_self.args.get(0))
				}
				default: {
					throw new RuntimeException("Unknown method call " + _self.member.name)
				}
			}
		} else {
			System.out.println("Useless 07 (?)")
		}
		
		return list
	}
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context) {
		val LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
		if (_self.methodInvocation) {
			switch _self.member.name {
				case "contains": {
					le.p = context.getPredicatPB(context.predicatesName.get((_self.receiver as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, (_self.args.get(0) as SymbolRef).symbol.name)
				}
				case "containsAll": {
					System.out.println(((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name)	// MemberSelection
					System.out.println((_self.receiver as MemberSelection).member)	// MemberSelection
					System.out.println(_self.member)
					System.out.println(context.predicatesName.get((_self.args.get(0) as MemberSelection).member))		// SymboleRef
					System.out.println(((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name)
					
					le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
					
					val String newVarName = "?m"
					
					le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdForall.^var.variableName = newVarName
					le.tdForall.type = PDDLFactory.eINSTANCE.createType
					le.tdForall.type.typeName = ((_self.receiver as MemberSelection).member as AssetTypeReference).propertyType.name // TODO on suppose que les types sont identiques (sinon pas de compilation ?)
					
					
					le.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					le.leForall.leImply1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					le.leForall.leImply1.p = context.getPredicatPB(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					
					
					le.leForall.leImply2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					le.leForall.leImply2.p = context.getPredicatPB(context.predicatesName.get((_self.receiver as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					
					
					/* Without leWhen1
					le.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					
					
					le.leForall.leOr1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					le.leForall.leOr1.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					le.leForall.leOr1.leNot.p = context.getpredicatPB(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					
					val LogicalExpressionPB predicatNew = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					
					le.leForall.leOr2.add(predicatNew)
					*/
				}
				case "containsAny": {
					le.tdExists = PDDLFactory.eINSTANCE.createTypeDescr
					
					val String newVarName = "?m"
					
					le.tdExists.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdExists.^var.variableName = newVarName
					le.tdExists.type = PDDLFactory.eINSTANCE.createType
					le.tdExists.type.typeName = ((_self.receiver as MemberSelection).member as AssetTypeReference).propertyType.name
					
					le.leExists = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					le.leExists.leAnd1 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					le.leExists.leAnd1.p = context.getPredicatPB(context.predicatesName.get((_self.args.get(0) as MemberSelection).member), ((_self.args.get(0) as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					
					val LogicalExpressionPB and2 = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					and2.p = context.getPredicatPB(context.predicatesName.get((_self.receiver as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
					
					le.leExists.leAnd2.add(and2)
				}
				case "isEmpty": {
					le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
					
					val String newVarName = "?m"
					
					le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
					le.tdForall.^var.variableName = newVarName
					le.tdForall.type = PDDLFactory.eINSTANCE.createType
					le.tdForall.type.typeName = ((_self.receiver as MemberSelection).member as AssetTypeReference).propertyType.name
					
					le.leForall = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					le.leForall.leNot = PDDLFactory.eINSTANCE.createLogicalExpressionPB
					le.leForall.leNot.p = context.getPredicatPB(context.predicatesName.get((_self.receiver as MemberSelection).member), ((_self.receiver as MemberSelection).receiver as SymbolRef).symbol.name, newVarName)
				}
				case "filter": {
					System.out.println("FILTERING")
					System.out.println(_self.receiver)
					System.out.println(_self.args.get(0))
				}
				default: {
					throw new RuntimeException("Unknown method call " + _self.member.name)
				}
			}
		} else {
			System.out.println("Useless 07 (?)")
		}
		return new WorkLogicalExpressionPB(le)
	}
	
	def String recoverName() {
		return _self.receiver.recoverName()
		//return "NotAMember"
	}
	
	def String recoverAssetTypeFeatureName(AssetBasedSystem context) {
		return context.predicatesName.get((_self.receiver as MemberSelection).member)
	}
	
	def WorkLogicalExpressionDM recoverForAller(AssetBasedSystem context, GuardedAction currentAction, String variableName, LogicalExpressionDM inner) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM 
		
		le.tdForall = PDDLFactory.eINSTANCE.createTypeDescr
		le.tdForall.^var = PDDLFactory.eINSTANCE.createVariable
		le.tdForall.^var.variableName = "?" + variableName
		le.tdForall.type = PDDLFactory.eINSTANCE.createType
		le.tdForall.type.typeName = "grth"
		
		le.leForall = context.whenOrImply(currentAction.doingEffects, new WorkLogicalExpressionDM(context.getPredicatDM("jnkjn", "gdhgsfigre")).getLEValue, null)
		return new WorkLogicalExpressionDM(le)
	}
	
}

@Aspect(className=Member)
abstract class MemberAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction8PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction8PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=SymbolRef)
class SymbolRefAspect extends ExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		val Variable newVariable = PDDLFactory.eINSTANCE.createVariable
		newVariable.variableName = "?" + _self.symbol.name
		le.v = newVariable
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction9PasEncoreImplementee")
		list.add(p)
		return list
	}
	
	def String recoverName() {
		return _self.symbol.recoverName()
	}
	
}

@Aspect(className=Symbol)
abstract class SymbolAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction10PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction10PasEncoreImplementee")
		list.add(p)
		return list
	}
	
	def String recoverName() {
		return _self.name
	}
	
}

@Aspect(className=StaticMethod)
class StaticMethodAspect extends MemberAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction11PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction11PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=LambdaParameter)
class LambdaParameterAspect extends SymbolAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/

	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction12PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction12PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=LambdaExpression)
class LambdaExpressionAspect extends ExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		System.out.println("LambdaExpression")
		System.out.println(_self)
		System.out.println(_self.body)
		//System.out.println(_self.body.args)
		System.out.println()
		
		le.p = context.getPredicatDM("TEST", "Fonction13PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction13PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=LambdaAction)
class LambdaActionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		
		val List<LogicalExpressionDM> pddlActions = new ArrayList<LogicalExpressionDM>
		for (absAction : _self.actions) {
			pddlActions.add(absAction.recoverPDDLDomainLogicalExpressionFromABSAction(context, currentAction).getLEValue)
		}
		
		if (pddlActions.size > 1) {
			le.leAnd1 = pddlActions.remove(0)
			for (pddlAction : pddlActions) {
				le.leAnd2.add(pddlAction)
			}
		} else if (pddlActions.size == 1) {
			le = pddlActions.get(0)
		}
		
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction14PasEncoreImplementee")
		list.add(p)
		return list
	}
	
	def WorkLogicalExpressionPB recoverPDDLGoalFromABSGoalPostCondition(AssetBasedSystem context) {
		var LogicalExpressionPB le = PDDLFactory.eINSTANCE.createLogicalExpressionPB
		
		val List<LogicalExpressionPB> pddlActions = new ArrayList<LogicalExpressionPB>
		for (absAction : _self.actions) {
			pddlActions.add(absAction.recoverPDDLGoalFromABSGoalPostCondition(context).getLEValue)
		}
		
		if (pddlActions.size > 1) {
			le.leAnd1 = pddlActions.remove(0)
			for (pddlAction : pddlActions) {
				le.leAnd2.add(pddlAction)
			}
		} else if (pddlActions.size == 1) {
			le = pddlActions.get(0)
		}
		
		return new WorkLogicalExpressionPB(le)
	}
	
}

@Aspect(className=Parameter)
abstract class ParameterAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction15PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction15PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=AnnotationEntry)
class AnnotationEntryAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction16PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction16PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=AnnotationKey)
class AnnotationKeyAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction17PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction17PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=Goal)
class GoalAspect {
	
	def void getfromGoal() {
		//_self.name plutôt, pour nommer les problèmes
		
		//_self.precondition
		val WorkLogicalExpressionPB goalPrecondition = new WorkLogicalExpressionPB("FALSE")
		
		//_self.postcondition
		val WorkLogicalExpressionPB goalPostcondition = new WorkLogicalExpressionPB("FALSE")
	}
	
}

@Aspect(className=Contract)
class ContractAspect extends GuardAspect {

}

@Aspect(className=Guard)
abstract class GuardAspect {
	
}

@Aspect(className=EnumConstant)
class EnumConstantAspect extends ConstantExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction18PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction18PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}

@Aspect(className=UndefinedConstant)
class UndefinedConstantAspect extends ConstantExpressionAspect {
	
	def WorkLogicalExpressionDM recoverPDDLDomainLogicalExpressionFromABSExpression(AssetBasedSystem context, GuardedAction currentAction) {
		var LogicalExpressionDM le = PDDLFactory.eINSTANCE.createLogicalExpressionDM
		le.p = context.getPredicatDM("TEST", "Fonction19PasEncoreImplementee")
		return new WorkLogicalExpressionDM(le)
	}
	
	def List<PredicatPB> recoverPDDLInitPredicatesFromABSGoalPrecondition(AssetBasedSystem context) {
		val List<PredicatPB> list = new ArrayList<PredicatPB>()
		val PredicatPB p = context.getPredicatPB("TEST", "Fonction19PasEncoreImplementee")
		list.add(p)
		return list
	}
	
}
