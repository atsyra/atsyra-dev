/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absreport.tests;

import com.google.inject.Injector;
import fr.irisa.atsyra.absystem.xtext.tests.AssetBasedSystemDslInjectorProvider;


// cf. https://zarnekow.blogspot.com/2014/10/testing-multiple-xtext-dsls.html

public class MultiLangInjectorProvider extends ABSReportInjectorProvider {
	protected Injector internalCreateInjector() {
		// trigger injector creation of other language
		new AssetBasedSystemDslInjectorProvider().getInjector();
		return super.internalCreateInjector();
	}
}
