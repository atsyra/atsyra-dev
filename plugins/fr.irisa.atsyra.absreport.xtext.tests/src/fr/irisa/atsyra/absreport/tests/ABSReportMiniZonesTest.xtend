package fr.irisa.atsyra.absreport.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.testing.util.ResourceHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.eclipse.xtext.resource.XtextResourceSet
import com.google.inject.Provider
import fr.irisa.atsyra.absreport.aBSReport.ABSReportModel
import fr.irisa.atsyra.absreport.aBSReport.ABSReportPackage
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.resource.XtextResourceFactory
import org.eclipse.xtext.EcoreUtil2

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class ABSReportMiniZonesTest {
	// helper to directly parse strings 
	//@Inject
	//ParseHelper<AssetBasedSystem> parseHelper
	// Allows to obtain a new resource set
	@Inject 
	Provider<XtextResourceSet> resourceSetProvider
	// Helper to test all validation rules and ensure resolved links
	@Inject 
	ValidationTestHelper validationTester
	
	/**
	 *  load a single isolated, standalone resource in its own resourceSet
	 */
	def ABSReportModel simpleLoadResource(String testFileName) {		
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()
		ABSReportPackage.eINSTANCE.eClass()
		new org.eclipse.emf.mwe.utils.StandaloneSetup().setPlatformUri("../");
		val resourceSet = resourceSetProvider.get
		
		val res1 = resourceSet.getResource(URI.createFileURI("test-input-files/"+testFileName+".abs"), true)
		res1.load(emptyMap)
		EcoreUtil2.resolveAll(res1)
		val res2 = resourceSet.getResource(URI.createFileURI("test-input-files/"+testFileName+".absreport"), true)
		res2.load(emptyMap)
		return res2.contents.head as ABSReportModel
	}
	
	@Test
	def void mini_zonesTest_01() {
		val absModel = simpleLoadResource("mini_zones")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel)
		validationTester.assertNoErrors(absModel)
	}
	
	@Test
	def void mini_zonesTest_02() {
		val absModel = simpleLoadResource("mini_zones2")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel)
		validationTester.assertNoErrors(absModel)
	}
	
	@Test
	def void mini_zonesTest_previous() {
		val absModel = simpleLoadResource("mini_zones_previous")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel)
		validationTester.assertNoErrors(absModel)
	}
}
