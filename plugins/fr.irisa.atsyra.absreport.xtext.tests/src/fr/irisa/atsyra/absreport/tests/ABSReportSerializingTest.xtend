/*
 * generated by Xtext 2.25.0
 */
package fr.irisa.atsyra.absreport.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import com.google.inject.Provider
import org.eclipse.xtext.resource.XtextResourceSet
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absreport.aBSReport.ABSReportPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.serializer.ISerializer
import static org.junit.jupiter.api.Assertions.assertLinesMatch
import java.io.File
import java.util.Scanner
import org.eclipse.xtext.resource.SaveOptions

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class ABSReportSerializingTest {
	@Inject 
	Provider<XtextResourceSet> resourceSetProvider
	
	@Inject ISerializer serializer
	
	@Test
	def void test() {		
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()
		ABSReportPackage.eINSTANCE.eClass()
		
		new org.eclipse.emf.mwe.utils.StandaloneSetup().setPlatformUri("../");
		val resourceSet = resourceSetProvider.get
		
		val res1 = resourceSet.getResource(URI.createFileURI("test-input-files/mini_zones_previous.abs"), true)
		res1.load(emptyMap)
		EcoreUtil2.resolveAll(res1)
		val res2 = resourceSet.getResource(URI.createFileURI("test-input-files/mini_zones_previous.absreport"), true)
		res2.load(emptyMap)
		val optionsbuilder = SaveOptions.newBuilder()
		optionsbuilder.format;
		val serialized = serializer.serialize(res2.contents.head, optionsbuilder.options)
		val expectedFile = new File("test-input-files/mini_zones_serialized.absreport")
		val scanner = new Scanner(expectedFile)
		assertLinesMatch(scanner.useDelimiter("\n").tokens, serialized.lines())
	}
}
