/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.provider;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.util.Interpreter_vmAdapterFactory;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Interpreter_vmItemProviderAdapterFactory extends Interpreter_vmAdapterFactory
		implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interpreter_vmItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GuardOccurenceItemProvider guardOccurenceItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createGuardOccurenceAdapter() {
		if (guardOccurenceItemProvider == null) {
			guardOccurenceItemProvider = new GuardOccurenceItemProvider(this);
		}

		return guardOccurenceItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetArgument} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetArgumentItemProvider assetArgumentItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetArgument}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssetArgumentAdapter() {
		if (assetArgumentItemProvider == null) {
			assetArgumentItemProvider = new AssetArgumentItemProvider(this);
		}

		return assetArgumentItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ConstantArgument} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstantArgumentItemProvider constantArgumentItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ConstantArgument}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createConstantArgumentAdapter() {
		if (constantArgumentItemProvider == null) {
			constantArgumentItemProvider = new ConstantArgumentItemProvider(this);
		}

		return constantArgumentItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.UndefinedValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UndefinedValueItemProvider undefinedValueItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.UndefinedValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createUndefinedValueAdapter() {
		if (undefinedValueItemProvider == null) {
			undefinedValueItemProvider = new UndefinedValueItemProvider(this);
		}

		return undefinedValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.BooleanValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BooleanValueItemProvider booleanValueItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.BooleanValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBooleanValueAdapter() {
		if (booleanValueItemProvider == null) {
			booleanValueItemProvider = new BooleanValueItemProvider(this);
		}

		return booleanValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.IntegerValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegerValueItemProvider integerValueItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.IntegerValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createIntegerValueAdapter() {
		if (integerValueItemProvider == null) {
			integerValueItemProvider = new IntegerValueItemProvider(this);
		}

		return integerValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.VersionValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VersionValueItemProvider versionValueItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.VersionValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVersionValueAdapter() {
		if (versionValueItemProvider == null) {
			versionValueItemProvider = new VersionValueItemProvider(this);
		}

		return versionValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.StringValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StringValueItemProvider stringValueItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.StringValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createStringValueAdapter() {
		if (stringValueItemProvider == null) {
			stringValueItemProvider = new StringValueItemProvider(this);
		}

		return stringValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetValueItemProvider assetValueItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssetValueAdapter() {
		if (assetValueItemProvider == null) {
			assetValueItemProvider = new AssetValueItemProvider(this);
		}

		return assetValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ListValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ListValueItemProvider listValueItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ListValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createListValueAdapter() {
		if (listValueItemProvider == null) {
			listValueItemProvider = new ListValueItemProvider(this);
		}

		return listValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetFeatureValueEntryItemProvider assetFeatureValueEntryItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssetFeatureValueEntryAdapter() {
		if (assetFeatureValueEntryItemProvider == null) {
			assetFeatureValueEntryItemProvider = new AssetFeatureValueEntryItemProvider(this);
		}

		return assetFeatureValueEntryItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.EnumValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumValueItemProvider enumValueItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.EnumValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createEnumValueAdapter() {
		if (enumValueItemProvider == null) {
			enumValueItemProvider = new EnumValueItemProvider(this);
		}

		return enumValueItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>) type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void dispose() {
		if (guardOccurenceItemProvider != null)
			guardOccurenceItemProvider.dispose();
		if (assetArgumentItemProvider != null)
			assetArgumentItemProvider.dispose();
		if (constantArgumentItemProvider != null)
			constantArgumentItemProvider.dispose();
		if (undefinedValueItemProvider != null)
			undefinedValueItemProvider.dispose();
		if (booleanValueItemProvider != null)
			booleanValueItemProvider.dispose();
		if (integerValueItemProvider != null)
			integerValueItemProvider.dispose();
		if (versionValueItemProvider != null)
			versionValueItemProvider.dispose();
		if (stringValueItemProvider != null)
			stringValueItemProvider.dispose();
		if (assetValueItemProvider != null)
			assetValueItemProvider.dispose();
		if (listValueItemProvider != null)
			listValueItemProvider.dispose();
		if (assetFeatureValueEntryItemProvider != null)
			assetFeatureValueEntryItemProvider.dispose();
		if (enumValueItemProvider != null)
			enumValueItemProvider.dispose();
	}

}
