/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.provider;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmFactory;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage;

import fr.irisa.atsyra.absystem.model.absystem.provider.AssetBasedSystemEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AssetFeatureValueEntryItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetFeatureValueEntryItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addKeyPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AssetFeatureValueEntry_key_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AssetFeatureValueEntry_key_feature",
						"_UI_AssetFeatureValueEntry_type"),
				Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__KEY, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AssetFeatureValueEntry_name_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AssetFeatureValueEntry_name_feature",
								"_UI_AssetFeatureValueEntry_type"),
						Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__NAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__VALUE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns AssetFeatureValueEntry.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AssetFeatureValueEntry"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AssetFeatureValueEntry) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_AssetFeatureValueEntry_type")
				: getString("_UI_AssetFeatureValueEntry_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AssetFeatureValueEntry.class)) {
		case Interpreter_vmPackage.ASSET_FEATURE_VALUE_ENTRY__NAME:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case Interpreter_vmPackage.ASSET_FEATURE_VALUE_ENTRY__VALUE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__VALUE,
				Interpreter_vmFactory.eINSTANCE.createUndefinedValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__VALUE,
				Interpreter_vmFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__VALUE,
				Interpreter_vmFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__VALUE,
				Interpreter_vmFactory.eINSTANCE.createVersionValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__VALUE,
				Interpreter_vmFactory.eINSTANCE.createStringValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__VALUE,
				Interpreter_vmFactory.eINSTANCE.createAssetValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__VALUE,
				Interpreter_vmFactory.eINSTANCE.createListValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.ASSET_FEATURE_VALUE_ENTRY__VALUE,
				Interpreter_vmFactory.eINSTANCE.createEnumValue()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AssetBasedSystemEditPlugin.INSTANCE;
	}

}
