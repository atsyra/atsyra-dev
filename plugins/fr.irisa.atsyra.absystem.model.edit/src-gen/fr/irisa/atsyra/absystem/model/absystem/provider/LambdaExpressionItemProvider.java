/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.provider;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.LambdaExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LambdaExpressionItemProvider extends ExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LambdaExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(AbsystemPackage.Literals.LAMBDA_EXPRESSION__LAMBDA_PARAMETER);
			childrenFeatures.add(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns LambdaExpression.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LambdaExpression"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_LambdaExpression_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LambdaExpression.class)) {
		case AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER:
		case AbsystemPackage.LAMBDA_EXPRESSION__BODY:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__LAMBDA_PARAMETER,
				AbsystemFactory.eINSTANCE.createLambdaParameter()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createImpliesExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createOrExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createAndExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createNotExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createEqualityComparisonExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createInequalityComparisonExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createStringConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createIntConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createBooleanConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createVersionConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createMemberSelection()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createSymbolRef()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createLambdaExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createEnumConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createUndefinedConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.LAMBDA_EXPRESSION__BODY,
				AbsystemFactory.eINSTANCE.createCollection()));
	}

}
