/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.provider;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Action;
import fr.irisa.atsyra.absystem.model.absystem.ActionEnum;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.Action} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ActionItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addActionTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Action Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addActionTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Action_actionType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Action_actionType_feature",
								"_UI_Action_type"),
						AbsystemPackage.Literals.ACTION__ACTION_TYPE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(AbsystemPackage.Literals.ACTION__TARGET);
			childrenFeatures.add(AbsystemPackage.Literals.ACTION__ARGS);
			childrenFeatures.add(AbsystemPackage.Literals.ACTION__LAMBDA_ACTION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Action.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Action"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		ActionEnum labelValue = ((Action) object).getActionType();
		String label = labelValue == null ? null : labelValue.toString();
		return label == null || label.length() == 0 ? getString("_UI_Action_type")
				: getString("_UI_Action_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Action.class)) {
		case AbsystemPackage.ACTION__ACTION_TYPE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case AbsystemPackage.ACTION__TARGET:
		case AbsystemPackage.ACTION__ARGS:
		case AbsystemPackage.ACTION__LAMBDA_ACTION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createImpliesExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createOrExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createAndExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createNotExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createEqualityComparisonExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createInequalityComparisonExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createStringConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createIntConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createBooleanConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createVersionConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createMemberSelection()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createSymbolRef()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createLambdaExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createEnumConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createUndefinedConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__TARGET,
				AbsystemFactory.eINSTANCE.createCollection()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createImpliesExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createOrExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createAndExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createNotExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createEqualityComparisonExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createInequalityComparisonExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createStringConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createIntConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createBooleanConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createVersionConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createMemberSelection()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createSymbolRef()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createLambdaExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createEnumConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createUndefinedConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__ARGS,
				AbsystemFactory.eINSTANCE.createCollection()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.ACTION__LAMBDA_ACTION,
				AbsystemFactory.eINSTANCE.createLambdaAction()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == AbsystemPackage.Literals.ACTION__TARGET
				|| childFeature == AbsystemPackage.Literals.ACTION__ARGS;

		if (qualify) {
			return getString("_UI_CreateChild_text2",
					new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AssetBasedSystemEditPlugin.INSTANCE;
	}

}
