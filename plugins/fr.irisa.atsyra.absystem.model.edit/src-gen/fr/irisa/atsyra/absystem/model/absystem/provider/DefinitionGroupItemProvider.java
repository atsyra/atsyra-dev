/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.provider;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DefinitionGroupItemProvider extends AnnotatedItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionGroupItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addTagDefinitionsPropertyDescriptor(object);
			addTagsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Definition_id_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Definition_id_feature",
								"_UI_Definition_type"),
						AbsystemPackage.Literals.DEFINITION__ID, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_DefinitionGroup_name_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_DefinitionGroup_name_feature",
								"_UI_DefinitionGroup_type"),
						AbsystemPackage.Literals.DEFINITION_GROUP__NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Tag Definitions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagDefinitionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_DefinitionGroup_tagDefinitions_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_DefinitionGroup_tagDefinitions_feature",
								"_UI_DefinitionGroup_type"),
						AbsystemPackage.Literals.DEFINITION_GROUP__TAG_DEFINITIONS, false, false, false, null, null,
						null));
	}

	/**
	 * This adds a property descriptor for the Tags feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_DefinitionGroup_tags_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_DefinitionGroup_tags_feature",
								"_UI_DefinitionGroup_type"),
						AbsystemPackage.Literals.DEFINITION_GROUP__TAGS, true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS);
			childrenFeatures.add(AbsystemPackage.Literals.DEFINITION_GROUP__STATIC_METHODS);
			childrenFeatures.add(AbsystemPackage.Literals.DEFINITION_GROUP__PRIMITIVE_DATA_TYPES);
			childrenFeatures.add(AbsystemPackage.Literals.DEFINITION_GROUP__ANNOTATION_KEYS);
			childrenFeatures.add(AbsystemPackage.Literals.DEFINITION_GROUP__ASSET_TYPES);
			childrenFeatures.add(AbsystemPackage.Literals.DEFINITION_GROUP__GUARDED_ACTIONS);
			childrenFeatures.add(AbsystemPackage.Literals.DEFINITION_GROUP__CONTRACTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DefinitionGroup.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DefinitionGroup"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DefinitionGroup) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_DefinitionGroup_type")
				: getString("_UI_DefinitionGroup_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DefinitionGroup.class)) {
		case AbsystemPackage.DEFINITION_GROUP__ID:
		case AbsystemPackage.DEFINITION_GROUP__NAME:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case AbsystemPackage.DEFINITION_GROUP__DEFINITIONS:
		case AbsystemPackage.DEFINITION_GROUP__STATIC_METHODS:
		case AbsystemPackage.DEFINITION_GROUP__PRIMITIVE_DATA_TYPES:
		case AbsystemPackage.DEFINITION_GROUP__ANNOTATION_KEYS:
		case AbsystemPackage.DEFINITION_GROUP__ASSET_TYPES:
		case AbsystemPackage.DEFINITION_GROUP__GUARDED_ACTIONS:
		case AbsystemPackage.DEFINITION_GROUP__CONTRACTS:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createAssetType()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createPrimitiveDataType()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createEnumDataType()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createDefinitionGroup()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createTag()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createAssetTypeAspect()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createGuardedAction()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createStaticMethod()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createAnnotationKey()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createContract()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS,
				AbsystemFactory.eINSTANCE.createRequirement()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__STATIC_METHODS,
				AbsystemFactory.eINSTANCE.createStaticMethod()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__PRIMITIVE_DATA_TYPES,
				AbsystemFactory.eINSTANCE.createPrimitiveDataType()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__PRIMITIVE_DATA_TYPES,
				AbsystemFactory.eINSTANCE.createEnumDataType()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__ANNOTATION_KEYS,
				AbsystemFactory.eINSTANCE.createAnnotationKey()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__ASSET_TYPES,
				AbsystemFactory.eINSTANCE.createAssetType()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__ASSET_TYPES,
				AbsystemFactory.eINSTANCE.createAssetTypeAspect()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__GUARDED_ACTIONS,
				AbsystemFactory.eINSTANCE.createGuardedAction()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.DEFINITION_GROUP__CONTRACTS,
				AbsystemFactory.eINSTANCE.createContract()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == AbsystemPackage.Literals.DEFINITION_GROUP__DEFINITIONS
				|| childFeature == AbsystemPackage.Literals.DEFINITION_GROUP__ASSET_TYPES
				|| childFeature == AbsystemPackage.Literals.DEFINITION_GROUP__PRIMITIVE_DATA_TYPES
				|| childFeature == AbsystemPackage.Literals.DEFINITION_GROUP__GUARDED_ACTIONS
				|| childFeature == AbsystemPackage.Literals.DEFINITION_GROUP__STATIC_METHODS
				|| childFeature == AbsystemPackage.Literals.DEFINITION_GROUP__ANNOTATION_KEYS
				|| childFeature == AbsystemPackage.Literals.DEFINITION_GROUP__CONTRACTS;

		if (qualify) {
			return getString("_UI_CreateChild_text2",
					new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
