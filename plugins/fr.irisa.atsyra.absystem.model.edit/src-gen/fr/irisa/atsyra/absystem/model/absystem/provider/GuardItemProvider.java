/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.provider;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Guard;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.irisa.atsyra.absystem.model.absystem.Guard} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class GuardItemProvider extends AnnotatedItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GuardItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addDescriptionPropertyDescriptor(object);
			addDescriptionFormatPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Guard_name_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Guard_name_feature", "_UI_Guard_type"),
						AbsystemPackage.Literals.GUARD__NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Guard_description_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Guard_description_feature",
								"_UI_Guard_type"),
						AbsystemPackage.Literals.GUARD__DESCRIPTION, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Description Format feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionFormatPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Guard_descriptionFormat_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Guard_descriptionFormat_feature",
								"_UI_Guard_type"),
						AbsystemPackage.Literals.GUARD__DESCRIPTION_FORMAT, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION);
			childrenFeatures.add(AbsystemPackage.Literals.GUARD__GUARD_PARAMETERS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Guard) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Guard_type")
				: getString("_UI_Guard_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Guard.class)) {
		case AbsystemPackage.GUARD__NAME:
		case AbsystemPackage.GUARD__DESCRIPTION:
		case AbsystemPackage.GUARD__DESCRIPTION_FORMAT:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case AbsystemPackage.GUARD__GUARD_EXPRESSION:
		case AbsystemPackage.GUARD__GUARD_PARAMETERS:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createImpliesExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createOrExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createAndExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createNotExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createEqualityComparisonExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createInequalityComparisonExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createStringConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createIntConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createBooleanConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createVersionConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createMemberSelection()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createSymbolRef()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createLambdaExpression()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createEnumConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createUndefinedConstant()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_EXPRESSION,
				AbsystemFactory.eINSTANCE.createCollection()));

		newChildDescriptors.add(createChildParameter(AbsystemPackage.Literals.GUARD__GUARD_PARAMETERS,
				AbsystemFactory.eINSTANCE.createGuardParameter()));
	}

}
