/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.vm.model.absystem_vm;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage#getActionArgument()
 * @model abstract="true"
 * @generated
 */
public interface ActionArgument extends EObject {
} // ActionArgument
