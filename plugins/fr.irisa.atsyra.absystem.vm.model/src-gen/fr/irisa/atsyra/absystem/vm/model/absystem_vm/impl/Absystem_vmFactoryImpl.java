/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl;

import fr.irisa.atsyra.absystem.vm.model.absystem_vm.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Absystem_vmFactoryImpl extends EFactoryImpl implements Absystem_vmFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Absystem_vmFactory init() {
		try {
			Absystem_vmFactory theAbsystem_vmFactory = (Absystem_vmFactory) EPackage.Registry.INSTANCE
					.getEFactory(Absystem_vmPackage.eNS_URI);
			if (theAbsystem_vmFactory != null) {
				return theAbsystem_vmFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Absystem_vmFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Absystem_vmFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE:
			return createGuardedActionOccurence();
		case Absystem_vmPackage.ASSET_ARGUMENT:
			return createAssetArgument();
		case Absystem_vmPackage.CONSTANT_ARGUMENT:
			return createConstantArgument();
		case Absystem_vmPackage.BOOLEAN_VALUE:
			return createBooleanValue();
		case Absystem_vmPackage.INTEGER_VALUE:
			return createIntegerValue();
		case Absystem_vmPackage.VERSION_VALUE:
			return createVersionValue();
		case Absystem_vmPackage.STRING_VALUE:
			return createStringValue();
		case Absystem_vmPackage.ENUM_VALUE:
			return createEnumValue();
		case Absystem_vmPackage.ASSET_VALUE:
			return createAssetValue();
		case Absystem_vmPackage.LIST_VALUE:
			return createListValue();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GuardedActionOccurence createGuardedActionOccurence() {
		GuardedActionOccurenceImpl guardedActionOccurence = new GuardedActionOccurenceImpl();
		return guardedActionOccurence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetArgument createAssetArgument() {
		AssetArgumentImpl assetArgument = new AssetArgumentImpl();
		return assetArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantArgument createConstantArgument() {
		ConstantArgumentImpl constantArgument = new ConstantArgumentImpl();
		return constantArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanValue createBooleanValue() {
		BooleanValueImpl booleanValue = new BooleanValueImpl();
		return booleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerValue createIntegerValue() {
		IntegerValueImpl integerValue = new IntegerValueImpl();
		return integerValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VersionValue createVersionValue() {
		VersionValueImpl versionValue = new VersionValueImpl();
		return versionValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValue createStringValue() {
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumValue createEnumValue() {
		EnumValueImpl enumValue = new EnumValueImpl();
		return enumValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetValue createAssetValue() {
		AssetValueImpl assetValue = new AssetValueImpl();
		return assetValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListValue createListValue() {
		ListValueImpl listValue = new ListValueImpl();
		return listValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Absystem_vmPackage getAbsystem_vmPackage() {
		return (Absystem_vmPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Absystem_vmPackage getPackage() {
		return Absystem_vmPackage.eINSTANCE;
	}

} //Absystem_vmFactoryImpl
