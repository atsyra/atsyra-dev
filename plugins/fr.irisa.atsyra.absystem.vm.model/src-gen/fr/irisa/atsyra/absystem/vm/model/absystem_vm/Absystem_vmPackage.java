/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.vm.model.absystem_vm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmFactory
 * @model kind="package"
 * @generated
 */
public interface Absystem_vmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "absystem_vm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/atsyra/absystem_vm";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "absystem_vm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Absystem_vmPackage eINSTANCE = fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.GuardedActionOccurenceImpl <em>Guarded Action Occurence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.GuardedActionOccurenceImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getGuardedActionOccurence()
	 * @generated
	 */
	int GUARDED_ACTION_OCCURENCE = 0;

	/**
	 * The feature id for the '<em><b>Guarded Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION_OCCURENCE__GUARDED_ACTION = 0;

	/**
	 * The feature id for the '<em><b>Action Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION_OCCURENCE__ACTION_ARGUMENTS = 1;

	/**
	 * The number of structural features of the '<em>Guarded Action Occurence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION_OCCURENCE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Guarded Action Occurence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION_OCCURENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ActionArgumentImpl <em>Action Argument</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ActionArgumentImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getActionArgument()
	 * @generated
	 */
	int ACTION_ARGUMENT = 1;

	/**
	 * The number of structural features of the '<em>Action Argument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_ARGUMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Action Argument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_ARGUMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.AssetArgumentImpl <em>Asset Argument</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.AssetArgumentImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getAssetArgument()
	 * @generated
	 */
	int ASSET_ARGUMENT = 2;

	/**
	 * The feature id for the '<em><b>Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ARGUMENT__ASSET = ACTION_ARGUMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Asset Argument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ARGUMENT_FEATURE_COUNT = ACTION_ARGUMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Asset Argument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ARGUMENT_OPERATION_COUNT = ACTION_ARGUMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ConstantArgumentImpl <em>Constant Argument</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ConstantArgumentImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getConstantArgument()
	 * @generated
	 */
	int CONSTANT_ARGUMENT = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_ARGUMENT__VALUE = ACTION_ARGUMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constant Argument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_ARGUMENT_FEATURE_COUNT = ACTION_ARGUMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Constant Argument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_ARGUMENT_OPERATION_COUNT = ACTION_ARGUMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ValueImpl <em>Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ValueImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 4;

	/**
	 * The number of structural features of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.BooleanValueImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getBooleanValue()
	 * @generated
	 */
	int BOOLEAN_VALUE = 5;

	/**
	 * The feature id for the '<em><b>Boolean Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__BOOLEAN_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.IntegerValueImpl <em>Integer Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.IntegerValueImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getIntegerValue()
	 * @generated
	 */
	int INTEGER_VALUE = 6;

	/**
	 * The feature id for the '<em><b>Int Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE__INT_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Integer Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.VersionValueImpl <em>Version Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.VersionValueImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getVersionValue()
	 * @generated
	 */
	int VERSION_VALUE = 7;

	/**
	 * The feature id for the '<em><b>Version Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_VALUE__VERSION_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Version Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Version Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.StringValueImpl <em>String Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.StringValueImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getStringValue()
	 * @generated
	 */
	int STRING_VALUE = 8;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE__STRING_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.EnumValueImpl <em>Enum Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.EnumValueImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getEnumValue()
	 * @generated
	 */
	int ENUM_VALUE = 9;

	/**
	 * The feature id for the '<em><b>Enum Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__ENUM_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enum Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Enum Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.AssetValueImpl <em>Asset Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.AssetValueImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getAssetValue()
	 * @generated
	 */
	int ASSET_VALUE = 10;

	/**
	 * The feature id for the '<em><b>Asset Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_VALUE__ASSET_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Asset Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Asset Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ListValueImpl <em>List Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ListValueImpl
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getListValue()
	 * @generated
	 */
	int LIST_VALUE = 11;

	/**
	 * The feature id for the '<em><b>Owned Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_VALUE__OWNED_VALUES = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>List Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>List Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence <em>Guarded Action Occurence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guarded Action Occurence</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence
	 * @generated
	 */
	EClass getGuardedActionOccurence();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence#getGuardedAction <em>Guarded Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Guarded Action</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence#getGuardedAction()
	 * @see #getGuardedActionOccurence()
	 * @generated
	 */
	EReference getGuardedActionOccurence_GuardedAction();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence#getActionArguments <em>Action Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action Arguments</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence#getActionArguments()
	 * @see #getGuardedActionOccurence()
	 * @generated
	 */
	EReference getGuardedActionOccurence_ActionArguments();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.ActionArgument <em>Action Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Argument</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.ActionArgument
	 * @generated
	 */
	EClass getActionArgument();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.AssetArgument <em>Asset Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Argument</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.AssetArgument
	 * @generated
	 */
	EClass getAssetArgument();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.AssetArgument#getAsset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Asset</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.AssetArgument#getAsset()
	 * @see #getAssetArgument()
	 * @generated
	 */
	EReference getAssetArgument_Asset();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.ConstantArgument <em>Constant Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Argument</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.ConstantArgument
	 * @generated
	 */
	EClass getConstantArgument();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.ConstantArgument#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.ConstantArgument#getValue()
	 * @see #getConstantArgument()
	 * @generated
	 */
	EAttribute getConstantArgument_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Value
	 * @generated
	 */
	EClass getValue();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.BooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.BooleanValue
	 * @generated
	 */
	EClass getBooleanValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.BooleanValue#isBooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Boolean Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.BooleanValue#isBooleanValue()
	 * @see #getBooleanValue()
	 * @generated
	 */
	EAttribute getBooleanValue_BooleanValue();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.IntegerValue <em>Integer Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.IntegerValue
	 * @generated
	 */
	EClass getIntegerValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.IntegerValue#getIntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Int Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.IntegerValue#getIntValue()
	 * @see #getIntegerValue()
	 * @generated
	 */
	EAttribute getIntegerValue_IntValue();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.VersionValue <em>Version Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Version Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.VersionValue
	 * @generated
	 */
	EClass getVersionValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.VersionValue#getVersionValue <em>Version Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.VersionValue#getVersionValue()
	 * @see #getVersionValue()
	 * @generated
	 */
	EAttribute getVersionValue_VersionValue();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.StringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.StringValue
	 * @generated
	 */
	EClass getStringValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.StringValue#getStringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.StringValue#getStringValue()
	 * @see #getStringValue()
	 * @generated
	 */
	EAttribute getStringValue_StringValue();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.EnumValue <em>Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.EnumValue
	 * @generated
	 */
	EClass getEnumValue();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.EnumValue#getEnumValue <em>Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Enum Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.EnumValue#getEnumValue()
	 * @see #getEnumValue()
	 * @generated
	 */
	EReference getEnumValue_EnumValue();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.AssetValue <em>Asset Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.AssetValue
	 * @generated
	 */
	EClass getAssetValue();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.AssetValue#getAssetValue <em>Asset Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Asset Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.AssetValue#getAssetValue()
	 * @see #getAssetValue()
	 * @generated
	 */
	EReference getAssetValue_AssetValue();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.ListValue <em>List Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List Value</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.ListValue
	 * @generated
	 */
	EClass getListValue();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.ListValue#getOwnedValues <em>Owned Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Values</em>'.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.ListValue#getOwnedValues()
	 * @see #getListValue()
	 * @generated
	 */
	EReference getListValue_OwnedValues();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Absystem_vmFactory getAbsystem_vmFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.GuardedActionOccurenceImpl <em>Guarded Action Occurence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.GuardedActionOccurenceImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getGuardedActionOccurence()
		 * @generated
		 */
		EClass GUARDED_ACTION_OCCURENCE = eINSTANCE.getGuardedActionOccurence();

		/**
		 * The meta object literal for the '<em><b>Guarded Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GUARDED_ACTION_OCCURENCE__GUARDED_ACTION = eINSTANCE.getGuardedActionOccurence_GuardedAction();

		/**
		 * The meta object literal for the '<em><b>Action Arguments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GUARDED_ACTION_OCCURENCE__ACTION_ARGUMENTS = eINSTANCE.getGuardedActionOccurence_ActionArguments();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ActionArgumentImpl <em>Action Argument</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ActionArgumentImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getActionArgument()
		 * @generated
		 */
		EClass ACTION_ARGUMENT = eINSTANCE.getActionArgument();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.AssetArgumentImpl <em>Asset Argument</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.AssetArgumentImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getAssetArgument()
		 * @generated
		 */
		EClass ASSET_ARGUMENT = eINSTANCE.getAssetArgument();

		/**
		 * The meta object literal for the '<em><b>Asset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ARGUMENT__ASSET = eINSTANCE.getAssetArgument_Asset();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ConstantArgumentImpl <em>Constant Argument</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ConstantArgumentImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getConstantArgument()
		 * @generated
		 */
		EClass CONSTANT_ARGUMENT = eINSTANCE.getConstantArgument();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_ARGUMENT__VALUE = eINSTANCE.getConstantArgument_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ValueImpl <em>Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ValueImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getValue()
		 * @generated
		 */
		EClass VALUE = eINSTANCE.getValue();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.BooleanValueImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getBooleanValue()
		 * @generated
		 */
		EClass BOOLEAN_VALUE = eINSTANCE.getBooleanValue();

		/**
		 * The meta object literal for the '<em><b>Boolean Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_VALUE__BOOLEAN_VALUE = eINSTANCE.getBooleanValue_BooleanValue();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.IntegerValueImpl <em>Integer Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.IntegerValueImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getIntegerValue()
		 * @generated
		 */
		EClass INTEGER_VALUE = eINSTANCE.getIntegerValue();

		/**
		 * The meta object literal for the '<em><b>Int Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_VALUE__INT_VALUE = eINSTANCE.getIntegerValue_IntValue();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.VersionValueImpl <em>Version Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.VersionValueImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getVersionValue()
		 * @generated
		 */
		EClass VERSION_VALUE = eINSTANCE.getVersionValue();

		/**
		 * The meta object literal for the '<em><b>Version Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VERSION_VALUE__VERSION_VALUE = eINSTANCE.getVersionValue_VersionValue();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.StringValueImpl <em>String Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.StringValueImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getStringValue()
		 * @generated
		 */
		EClass STRING_VALUE = eINSTANCE.getStringValue();

		/**
		 * The meta object literal for the '<em><b>String Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_VALUE__STRING_VALUE = eINSTANCE.getStringValue_StringValue();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.EnumValueImpl <em>Enum Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.EnumValueImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getEnumValue()
		 * @generated
		 */
		EClass ENUM_VALUE = eINSTANCE.getEnumValue();

		/**
		 * The meta object literal for the '<em><b>Enum Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_VALUE__ENUM_VALUE = eINSTANCE.getEnumValue_EnumValue();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.AssetValueImpl <em>Asset Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.AssetValueImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getAssetValue()
		 * @generated
		 */
		EClass ASSET_VALUE = eINSTANCE.getAssetValue();

		/**
		 * The meta object literal for the '<em><b>Asset Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_VALUE__ASSET_VALUE = eINSTANCE.getAssetValue_AssetValue();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ListValueImpl <em>List Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.ListValueImpl
		 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.Absystem_vmPackageImpl#getListValue()
		 * @generated
		 */
		EClass LIST_VALUE = eINSTANCE.getListValue();

		/**
		 * The meta object literal for the '<em><b>Owned Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIST_VALUE__OWNED_VALUES = eINSTANCE.getListValue_OwnedValues();

	}

} //Absystem_vmPackage
