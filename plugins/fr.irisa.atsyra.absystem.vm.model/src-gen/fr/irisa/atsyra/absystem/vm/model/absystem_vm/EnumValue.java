/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.vm.model.absystem_vm;

import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.EnumValue#getEnumValue <em>Enum Value</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage#getEnumValue()
 * @model
 * @generated
 */
public interface EnumValue extends Value {
	/**
	 * Returns the value of the '<em><b>Enum Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum Value</em>' reference.
	 * @see #setEnumValue(EnumLiteral)
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage#getEnumValue_EnumValue()
	 * @model
	 * @generated
	 */
	EnumLiteral getEnumValue();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.EnumValue#getEnumValue <em>Enum Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enum Value</em>' reference.
	 * @see #getEnumValue()
	 * @generated
	 */
	void setEnumValue(EnumLiteral value);

} // EnumValue
