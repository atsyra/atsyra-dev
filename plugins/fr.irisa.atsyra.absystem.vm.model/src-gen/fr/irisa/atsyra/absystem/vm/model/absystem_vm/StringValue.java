/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.vm.model.absystem_vm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.StringValue#getStringValue <em>String Value</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage#getStringValue()
 * @model
 * @generated
 */
public interface StringValue extends Value {
	/**
	 * Returns the value of the '<em><b>String Value</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Value</em>' attribute.
	 * @see #setStringValue(String)
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage#getStringValue_StringValue()
	 * @model default=""
	 * @generated
	 */
	String getStringValue();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.StringValue#getStringValue <em>String Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Value</em>' attribute.
	 * @see #getStringValue()
	 * @generated
	 */
	void setStringValue(String value);

} // StringValue
