/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.vm.model.absystem_vm;

import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guarded Action Occurence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence#getGuardedAction <em>Guarded Action</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence#getActionArguments <em>Action Arguments</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage#getGuardedActionOccurence()
 * @model
 * @generated
 */
public interface GuardedActionOccurence extends EObject {
	/**
	 * Returns the value of the '<em><b>Guarded Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guarded Action</em>' reference.
	 * @see #setGuardedAction(GuardedAction)
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage#getGuardedActionOccurence_GuardedAction()
	 * @model
	 * @generated
	 */
	GuardedAction getGuardedAction();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence#getGuardedAction <em>Guarded Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guarded Action</em>' reference.
	 * @see #getGuardedAction()
	 * @generated
	 */
	void setGuardedAction(GuardedAction value);

	/**
	 * Returns the value of the '<em><b>Action Arguments</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.ActionArgument}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Arguments</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage#getGuardedActionOccurence_ActionArguments()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActionArgument> getActionArguments();

} // GuardedActionOccurence
