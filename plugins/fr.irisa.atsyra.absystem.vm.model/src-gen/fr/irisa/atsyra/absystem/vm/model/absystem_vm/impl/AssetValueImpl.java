/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl;

import fr.irisa.atsyra.absystem.model.absystem.Asset;

import fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage;
import fr.irisa.atsyra.absystem.vm.model.absystem_vm.AssetValue;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.AssetValueImpl#getAssetValue <em>Asset Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetValueImpl extends ValueImpl implements AssetValue {
	/**
	 * The cached value of the '{@link #getAssetValue() <em>Asset Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetValue()
	 * @generated
	 * @ordered
	 */
	protected Asset assetValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Absystem_vmPackage.Literals.ASSET_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Asset getAssetValue() {
		if (assetValue != null && assetValue.eIsProxy()) {
			InternalEObject oldAssetValue = (InternalEObject) assetValue;
			assetValue = (Asset) eResolveProxy(oldAssetValue);
			if (assetValue != oldAssetValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Absystem_vmPackage.ASSET_VALUE__ASSET_VALUE, oldAssetValue, assetValue));
			}
		}
		return assetValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Asset basicGetAssetValue() {
		return assetValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssetValue(Asset newAssetValue) {
		Asset oldAssetValue = assetValue;
		assetValue = newAssetValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Absystem_vmPackage.ASSET_VALUE__ASSET_VALUE,
					oldAssetValue, assetValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Absystem_vmPackage.ASSET_VALUE__ASSET_VALUE:
			if (resolve)
				return getAssetValue();
			return basicGetAssetValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Absystem_vmPackage.ASSET_VALUE__ASSET_VALUE:
			setAssetValue((Asset) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Absystem_vmPackage.ASSET_VALUE__ASSET_VALUE:
			setAssetValue((Asset) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Absystem_vmPackage.ASSET_VALUE__ASSET_VALUE:
			return assetValue != null;
		}
		return super.eIsSet(featureID);
	}

} //AssetValueImpl
