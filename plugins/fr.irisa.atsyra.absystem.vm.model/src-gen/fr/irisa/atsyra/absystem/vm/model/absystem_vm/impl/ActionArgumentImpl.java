/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl;

import fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage;
import fr.irisa.atsyra.absystem.vm.model.absystem_vm.ActionArgument;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ActionArgumentImpl extends MinimalEObjectImpl.Container implements ActionArgument {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionArgumentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Absystem_vmPackage.Literals.ACTION_ARGUMENT;
	}

} //ActionArgumentImpl
