/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl;

import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;

import fr.irisa.atsyra.absystem.vm.model.absystem_vm.Absystem_vmPackage;
import fr.irisa.atsyra.absystem.vm.model.absystem_vm.ActionArgument;
import fr.irisa.atsyra.absystem.vm.model.absystem_vm.GuardedActionOccurence;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Guarded Action Occurence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.GuardedActionOccurenceImpl#getGuardedAction <em>Guarded Action</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.vm.model.absystem_vm.impl.GuardedActionOccurenceImpl#getActionArguments <em>Action Arguments</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GuardedActionOccurenceImpl extends MinimalEObjectImpl.Container implements GuardedActionOccurence {
	/**
	 * The cached value of the '{@link #getGuardedAction() <em>Guarded Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuardedAction()
	 * @generated
	 * @ordered
	 */
	protected GuardedAction guardedAction;

	/**
	 * The cached value of the '{@link #getActionArguments() <em>Action Arguments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionArguments()
	 * @generated
	 * @ordered
	 */
	protected EList<ActionArgument> actionArguments;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GuardedActionOccurenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Absystem_vmPackage.Literals.GUARDED_ACTION_OCCURENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GuardedAction getGuardedAction() {
		if (guardedAction != null && guardedAction.eIsProxy()) {
			InternalEObject oldGuardedAction = (InternalEObject) guardedAction;
			guardedAction = (GuardedAction) eResolveProxy(oldGuardedAction);
			if (guardedAction != oldGuardedAction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__GUARDED_ACTION, oldGuardedAction,
							guardedAction));
			}
		}
		return guardedAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GuardedAction basicGetGuardedAction() {
		return guardedAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGuardedAction(GuardedAction newGuardedAction) {
		GuardedAction oldGuardedAction = guardedAction;
		guardedAction = newGuardedAction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__GUARDED_ACTION, oldGuardedAction, guardedAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActionArgument> getActionArguments() {
		if (actionArguments == null) {
			actionArguments = new EObjectContainmentEList<ActionArgument>(ActionArgument.class, this,
					Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__ACTION_ARGUMENTS);
		}
		return actionArguments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__ACTION_ARGUMENTS:
			return ((InternalEList<?>) getActionArguments()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__GUARDED_ACTION:
			if (resolve)
				return getGuardedAction();
			return basicGetGuardedAction();
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__ACTION_ARGUMENTS:
			return getActionArguments();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__GUARDED_ACTION:
			setGuardedAction((GuardedAction) newValue);
			return;
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__ACTION_ARGUMENTS:
			getActionArguments().clear();
			getActionArguments().addAll((Collection<? extends ActionArgument>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__GUARDED_ACTION:
			setGuardedAction((GuardedAction) null);
			return;
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__ACTION_ARGUMENTS:
			getActionArguments().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__GUARDED_ACTION:
			return guardedAction != null;
		case Absystem_vmPackage.GUARDED_ACTION_OCCURENCE__ACTION_ARGUMENTS:
			return actionArguments != null && !actionArguments.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //GuardedActionOccurenceImpl
