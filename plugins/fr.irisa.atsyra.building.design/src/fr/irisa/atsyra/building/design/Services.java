/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.design;


import fr.irisa.atsyra.building.BadgedDoor;
import fr.irisa.atsyra.building.Door;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;


/**
 * The services class used by VSM.
 */
public class Services {
    
    /**
    * See http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.sirius.doc%2Fdoc%2Findex.html&cp=24 for documentation on how to write service methods.
    */
    public EObject myService(EObject self, String arg) {
       // TODO Auto-generated code
      return self;
    }
    
    /**
     * Indicates if this Item is a key for one of the model Access
     * TODO may be look in all resources or add an opposite reference ?
     * @param buildingItem
     * @return
     */
    public boolean isKey(EObject buildingItem){
    	for (TreeIterator<EObject> iterator = buildingItem.eResource().getAllContents(); iterator.hasNext();) {
    		EObject o = iterator.next();
    		if (o instanceof Door) {
    			Door door = (Door) o;
    			if(door.getKeys().contains(buildingItem)) return true;
			}
		}
    	return false;
    }
    
    /**
     * Indicates if this Item is a key or a badge for one of the model Access
     * TODO may be look in all resources or add an opposite reference ?
     * @param buildingItem
     * @return
     */
    public boolean isKeyOrBadge(EObject buildingItem){
    	for (TreeIterator<EObject> iterator = buildingItem.eResource().getAllContents(); iterator.hasNext();) {
    		EObject o = iterator.next();
    		if (o instanceof Door) {
    			Door door = (Door) o;
    			if(door.getKeys().contains(buildingItem)) return true;
			}
    		if (o instanceof BadgedDoor) {
    			BadgedDoor door = (BadgedDoor) o;
    			if(door.getBadges().contains(buildingItem)) return true;
			}
		}
    	return false;
    }
}
