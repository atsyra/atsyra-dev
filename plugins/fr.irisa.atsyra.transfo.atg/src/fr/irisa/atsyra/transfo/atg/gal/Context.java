/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.transfo.atg.gal;

import fr.lip6.move.gal.Variable;
import fr.lip6.move.gal.GALTypeDeclaration;
import java.util.HashMap;
import fr.lip6.move.gal.GalFactory;

class Context {
	// our gal file
	public GALTypeDeclaration gal;	
	// variables for attackers
	private HashMap<String, Variable> Variables;
	
	public Context() {
		this.gal = GalFactory.eINSTANCE.createGALTypeDeclaration();
		this.Variables = new HashMap<String, Variable>();
	}
	
	public Variable getVariable(String str) {
		if(Variables.containsKey(str)){
			return Variables.get(str);
		}
		else{
			Variable newvar = GALBuildHelper.createVariable(str,0);
			Variables.put(str,newvar);
			return newvar;
		}
	} 
}