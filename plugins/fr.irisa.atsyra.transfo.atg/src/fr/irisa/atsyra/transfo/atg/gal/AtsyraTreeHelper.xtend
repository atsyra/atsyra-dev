package fr.irisa.atsyra.transfo.atg.gal

import atsyragoal.AbstractAtsyraTree
import atsyragoal.AndCondition
import atsyragoal.AtsyraGoal
import atsyragoal.AtsyraTree
import atsyragoal.AtsyraTreeReference
import atsyragoal.BooleanSystemCondition
import atsyragoal.DefaultValues
import atsyragoal.EqualsCondition
import atsyragoal.GoalCondition
import atsyragoal.NotCondition
import atsyragoal.OrCondition
import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.lip6.move.gal.BooleanExpression
import java.util.List

import static extension fr.irisa.atsyra.transfo.atg.ctl.TypedElementAspects.*
import static extension fr.irisa.atsyra.transfo.atg.gal.AtsyraGoalHelper.*
import static extension fr.irisa.atsyra.transfo.atg.gal.GoalConditionHelper.*
import static extension fr.irisa.atsyra.transfo.atg.gal.DefaultValuesHelper.*
import static extension fr.irisa.atsyra.transfo.atg.gal.DefaultAssignmentHelper.*
import atsyragoal.AtsyragoalFactory
import atsyragoal.SystemFeature
import atsyragoal.DefaultAssignment

@Aspect (className=AbstractAtsyraTree)
class AtsyraTreeHelper {
	public def AtsyraTree getConcreteTree() {
		switch(_self) {
			AtsyraTree: _self
			AtsyraTreeReference: _self.referencedTree.concreteTree
		}
		
	}
	
	public def List<AtsyraGoal> getSubgoals() {
		_self.concreteTree.operands.map[tree | tree.concreteTree.mainGoal]
	}
	
	public def String getPGoalString() {
		_self.concreteTree.mainGoal.string
	}
	
	public def List<String> getSubgoalStrings() {
		_self.subgoals.map[goal | goal.string]
	}
	
	public def String getOPString() {
		switch (_self.concreteTree.operator){
			case AND: {
				"AND"
			}
			case OR: {
				"OR"
			}
			case SAND: {
				"SAND"
			}
			case UNKNOWN: {
				""
			}
		}
	}
	
	public def boolean isLeaf() {
		_self.concreteTree.operands.empty
	} 
}

@Aspect (className=AtsyraGoal)
class AtsyraGoalHelper {
	
	public def String getString() {
		_self.preString + ',' +  _self.postString
	}

	public def String getPreString() {
		if(_self.defaultUsedInPre === null) {
			_self.precondition.string
		} else {
			_self.defaultUsedInPre.UsefulDefault(_self.precondition).string + " && (" +  _self.precondition.string + ")"
		}
		
	}
	
	public def String getPostString() {
		if(_self.defaultUsedInPost === null){
			_self.postcondition.string
		} else {
			_self.defaultUsedInPost.UsefulDefault(_self.postcondition).string + " && (" +  _self.postcondition.string + ")"
		}
		
	}
	
}

@Aspect (className=DefaultValues)
class DefaultValuesHelper {
	public def DefaultValues UsefulDefault(GoalCondition context) {
		var result = AtsyragoalFactory.eINSTANCE.createDefaultValues
		for(a : _self.defaultValueAssignments) {
			if(!context.contains(a.target)) {
				val copy = AtsyragoalFactory.eINSTANCE.createDefaultAssignment
				copy.target = a.target
				copy.assignedValue = a.assignedValue
				result.defaultValueAssignments.add(copy)
			}
		}
		return result
	}
	
	public def String getString() {
		'''«FOR a: _self.defaultValueAssignments SEPARATOR ' && '»«a.string»«ENDFOR»'''
	}
}

@Aspect (className=DefaultAssignment)
class DefaultAssignmentHelper {
	public def String getString() {
		_self.target.galName + " == " + _self.assignedValue.galName
	}
}

@Aspect (className=GoalCondition)
class GoalConditionHelper {
	
	public def String getString() {
		switch(_self) {
			AndCondition: _self.operands.tail.fold("(" + _self.operands.head.string + ")",[r,t | r + " && (" + t.string + ")"])
			OrCondition: _self.operands.tail.fold("(" + _self.operands.head.string + ")",[r,t | r + " || (" + t.string + ")"])
			NotCondition: "!(" + _self.operands.head.string + ")"
			BooleanSystemCondition: switch (_self.source.name) {
				case "true", case "false": _self.source.name
				default: _self.source.name + "=1"
			}
			EqualsCondition: _self.source.galName + "=" + _self.target.galName
		}
		
	}
	
	public def boolean contains(SystemFeature feature) {
		switch(_self) {
			AndCondition, OrCondition: !_self.operands.forall[t | !t.contains(feature)]
			NotCondition: _self.operands.head.contains(feature)
			//TODO is the comparison over names necessary? 
			BooleanSystemCondition:  _self.source.name == feature.name
			EqualsCondition: _self.source.name == feature.name || _self.target.name == feature.name 
			default: false
		}
	}
	
	public def String getLabel() {
		if (_self.eContainer instanceof AtsyraGoal) {
			return (_self.eContainer as AtsyraGoal).getName() + 
					"." + 
					_self.eContainingFeature().getName();
					
		} else {
			return _self.toString
		}
	}

	public def BooleanExpression getGalExpr(Context context) {
		switch(_self) {
			AndCondition: _self.operands.tail.fold(_self.operands.head.getGalExpr(context),[r,t | GALBuildHelper.createBoolExprAnd(r,t.getGalExpr(context))])
			OrCondition: _self.operands.tail.fold(_self.operands.head.getGalExpr(context),[r,t | GALBuildHelper.createBoolExprOr(r,t.getGalExpr(context))])
			NotCondition: GALBuildHelper.createBoolExprNot(_self.operands.head.getGalExpr(context))
			BooleanSystemCondition: switch (_self.source.name) {
				case "true": GALBuildHelper.boolTrue()
				case "false": GALBuildHelper.boolFalse()
				default: GALBuildHelper.createBoolExprEqVarCst(context.getVariable(_self.source.name), GALBuildHelper.galTrue)
			}
			//TODO: if source or target is a constant, don't create a variable
			EqualsCondition: GALBuildHelper.createBoolExprEqVarVar(context.getVariable(_self.source.name), context.getVariable(_self.target.name)) 
		}
	}
}