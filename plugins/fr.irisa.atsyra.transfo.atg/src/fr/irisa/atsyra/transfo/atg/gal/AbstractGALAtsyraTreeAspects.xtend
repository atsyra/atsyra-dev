package fr.irisa.atsyra.transfo.atg.gal

import atsyragoal.AbstractAtsyraTree
import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.transfo.atg.gal.Context

import static extension fr.irisa.atsyra.transfo.atg.gal.AtsyraTreeHelper.*
import static extension fr.irisa.atsyra.transfo.atg.gal.GoalConditionHelper.*
import fr.lip6.move.gal.GALTypeDeclaration

@Aspect (className=AbstractAtsyraTree)
class AbstractGALAtsyraTreeAspects {
	
	public def void updateGal(GALTypeDeclaration gal, Context context) {
		val name = _self.concreteTree.name
		if(_self.isLeaf) {
			val transi = GALBuildHelper.createTransition("do_" + name)
			gal.transitions.add(transi)
			transi.guard = _self.concreteTree.mainGoal.precondition.getGalExpr(context)
			transi.actions.add(GALBuildHelper.createVarAssignConst(context.getVariable("done_" + name),1))
			_self.concreteTree.mainGoal.postcondition.string //TODO add action from goal expr
		}
		else
		switch (_self.concreteTree.operator){
			case AND: {
				
			}
			case OR: {
				
			}
			case SAND: {
				
			}
			case UNKNOWN: {
				throw new Exception("Unknown operator. Should not happen for non-leaf trees")
			}
		}
	}
	
}
