package fr.irisa.atsyra.transfo.atg.ctl

import atsyragoal.AndCondition
import atsyragoal.BooleanSystemCondition
import atsyragoal.EqualsCondition
import atsyragoal.GoalCondition
import atsyragoal.NotCondition
import atsyragoal.OrCondition
import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import atsyragoal.TypedElement
import atsyragoal.SystemConstFeature
import atsyragoal.BooleanLiteral
import atsyragoal.AtsyraGoalModel
import static extension fr.irisa.atsyra.transfo.atg.ctl.TypedElementAspects.*
import static extension fr.irisa.atsyra.transfo.atg.ctl.GoalConditionAspects.*
import static extension fr.irisa.atsyra.transfo.atg.ctl.DefaultValuesAspects.*
import static extension fr.irisa.atsyra.transfo.atg.ctl.DefaultAssignmentAspects.*
import static extension fr.irisa.atsyra.transfo.atg.gal.GoalConditionHelper.*
import atsyragoal.AtsyraGoal
import atsyragoal.DefaultValues
import atsyragoal.DefaultAssignment
import atsyragoal.AtsyragoalFactory

@Aspect (className=GoalCondition)
class GoalConditionAspects {
	
	def public String getFormula() {
		switch _self {
			case null: "TRUE"
			AndCondition: '''«FOR operand : _self.operands BEFORE '(' SEPARATOR ' * ' AFTER ')'»«operand.formula»«ENDFOR»'''
			OrCondition:  '''«FOR operand : _self.operands BEFORE '(' SEPARATOR ' + ' AFTER ')'»«operand.formula»«ENDFOR»'''
			NotCondition: '''!«_self.operands.head.formula»'''
			EqualsCondition: '''«_self.source.galName»=«_self.target.galName»'''
			BooleanSystemCondition case _self.source.name=="true": "TRUE"
			BooleanSystemCondition case _self.source.name=="false": "FALSE"
			BooleanSystemCondition: '''«_self.source.name»=1'''
		}
	}
	
}

@Aspect (className=TypedElement)
class TypedElementAspects {
	def public String getGalName() {
		if(_self.isaConstant) {
			_self.constant.toString
		}
		else {
			_self.name // convention : name of TypeElement = name the GAL variable
		}
	}
	
	def public boolean isaConstant(){
		return _self instanceof SystemConstFeature || _self instanceof BooleanLiteral
	}
	
	def protected int getConstant(){
		switch _self {
			BooleanLiteral: _self.getBoolConstant()
			SystemConstFeature: _self.getSystConstant()
			default: throw new RuntimeException("TypedElement getConstant : Not defined")
		}
	}
	
	def private int getBoolConstant(){
		switch _self.name {
			case "true", case "^true": return 1
			case "false", case "^false": return 0
			default: throw new RuntimeException("TypedElement getBoolConstant : Not defined")
		}
	}
	
	def private int getSystConstant(){
		var i = 0
		for(element : (_self.eContainer as AtsyraGoalModel).typedElements) {
			if(element.isaConstant && element.type == _self.type)
			if(element != _self) i++ else return i+1
		}
		return i
	}
}

@Aspect (className=AtsyraGoal)
class AtsyraGoalAspects {
	

	public def String getPreFormula() {
		val res =
		if(_self.defaultUsedInPre === null) {
			_self.precondition.formula
		} else {
			_self.defaultUsedInPre.UsefulDefault(_self.precondition).formula + " && (" +  _self.precondition.formula + ")"
		}
		'(' + res + ')'
	}
	
	public def String getPostFormula() {
		val res =   
		if(_self.defaultUsedInPost === null){
			_self.postcondition.formula
		} else {
			_self.defaultUsedInPost.UsefulDefault(_self.postcondition).formula + " && (" +  _self.postcondition.formula + ")"
		}
		'(' + res + ')'
	}
	
}

@Aspect (className=DefaultValues)
class DefaultValuesAspects {
	public def DefaultValues UsefulDefault(GoalCondition context) {
		var result = AtsyragoalFactory.eINSTANCE.createDefaultValues
		for(a : _self.defaultValueAssignments) {
			if(!context.contains(a.target)) {
				val copy = AtsyragoalFactory.eINSTANCE.createDefaultAssignment
				copy.target = a.target
				copy.assignedValue = a.assignedValue
				result.defaultValueAssignments.add(copy)
			}
		}
		return result
	}
	
	public def String getFormula() {
		'''«FOR a: _self.defaultValueAssignments SEPARATOR ' && '»«a.formula»«ENDFOR»'''
	}
}

@Aspect (className=DefaultAssignment)
class DefaultAssignmentAspects {
	public def String getFormula() {
		_self.target.galName + " = " + _self.assignedValue.galName
	}
}

