package fr.irisa.atsyra.transfo.atg.ctl

import atsyragoal.AbstractAtsyraTree
import fr.inria.diverse.k3.al.annotationprocessor.Aspect

import static extension fr.irisa.atsyra.transfo.atg.ctl.GoalConditionAspects.*
import static extension fr.irisa.atsyra.transfo.atg.gal.AtsyraTreeHelper.*
import static extension fr.irisa.atsyra.transfo.atg.ctl.AtsyraGoalAspects.*
import java.util.Set
import java.util.List
import java.util.TreeSet
import java.util.LinkedList
import java.util.function.Consumer
import atsyragoal.AtsyraGoal
import java.util.ArrayList

@Aspect (className=AbstractAtsyraTree)
class AbstractCTLAtsyraTreeAspects {
	
	public def String getNotEmptyFormula() {
		switch (_self.concreteTree.operator){
			case AND: {
				_self.getANDFormula("TRUE", "TRUE") + ";";
			}
			case OR: {
				// (i1 * EF (f1) + ... + in * EF(fn)) 
				// with two modification : EX to skip the first, "virtual" state of the transition system, and init=1 to check that we start in a correct initial state
				'''EX(init=1 * «FOR subgoal : _self.concreteTree.subgoals SEPARATOR ' + '»«subgoal.preFormula» * EF «subgoal.postFormula»«ENDFOR»);'''
			}
			case SAND: {
				// i1 * EF (f1 * i2 * EF( f2 * ... * EF (fn))) 
				// with two modification : EX to skip the first, "virtual" state of the transition system, and init=1 to check that we start in a correct initial state
				'''EX(init=1 * «FOR subgoal : _self.concreteTree.subgoals SEPARATOR ' * '»«subgoal.preFormula» * EF («subgoal.postFormula»«ENDFOR»«FOR subgoal : _self.concreteTree.subgoals»)«ENDFOR»);'''
			}
			case UNKNOWN: {
				throw new Exception("Unknown operator. You should not try to test for the meet correctness for leaves!")
			}
		}
	}
	var static String formula = "";
	var static String formula_append = "";
	
	private def String getANDFormula(String initProp, String finalProp) {
		// CTL formula is of size factorial n
				val set = newHashSet()
				for(var i = 1; i <= _self.concreteTree.subgoals.size; i++) {
					set.add(i);
				}
				val AtsyraGoal[] subgoalarray = _self.concreteTree.subgoals.toArray(newArrayOfSize(0))
				formula = "" 
				formula_append = ""
				OrdersHelper.forAllOrder(set, newHashSet(), newLinkedList(),[ list | 
					formula += "EX(init=1 * " + initProp + " * " 
					formula_append = ")"
					for (element : list) {
						if(element.key == 0) {
							formula += subgoalarray.get(element.value-1).preFormula + " * EF("
						} else {
							formula += subgoalarray.get(element.value-1).postFormula + " * EF("
						}
						formula_append = ")" + formula_append
					}
					// now remove the last " * EF()"
					formula = formula.substring(0, formula.length-6)
					formula_append = formula_append.substring(1)
					formula += " * " + finalProp + formula_append + " + ";
				]); 
			// now remove the last " + "
			formula = formula.substring(0, formula.length-3)
			formula
	}

	public def String getMeetFormula() {
		switch (_self.concreteTree.operator){
			case AND: {
				//throw new Exception("Not a CTL formula!")
			_self.getANDFormula(_self.concreteTree.mainGoal.preFormula, _self.concreteTree.mainGoal.postFormula) + ";";
			}
			case OR: {
				// i * ((i1 * EF (f1 * f)) + ... + (in * EF(fn * f))) 
				// with two modification : EX to skip the first, "virtual" state of the transition system, and init=1 to check that we start in a correct initial state
				'''EX(init=1 * «_self.concreteTree.mainGoal.preFormula»«FOR subgoal : _self.concreteTree.subgoals BEFORE ' * (' SEPARATOR ' + ' AFTER ')'»(«subgoal.preFormula» * EF («subgoal.postFormula» * «_self.concreteTree.mainGoal.postFormula»))«ENDFOR»);'''
			}
			case SAND: {
				// i * i1 * EF (f1 * i2 * EF( f2 * ... * EF (fn * f))) 
				// with two modification : EX to skip the first, "virtual" state of the transition system, and init=1 to check that we start in a correct initial state
				'''EX(init=1 * «_self.concreteTree.mainGoal.preFormula»«FOR subgoal : _self.concreteTree.subgoals BEFORE ' * ' SEPARATOR ' * '»«subgoal.preFormula» * EF («subgoal.postFormula»«ENDFOR» * «_self.concreteTree.mainGoal.postFormula»«FOR subgoal : _self.concreteTree.subgoals»)«ENDFOR»);'''
			}
			case UNKNOWN: {
				throw new Exception("Unknown operator. You should not try to test for the meet correctness for leaves!")
			}
		}
	}
	
	public def String getUnderMatchFormula() {
		switch (_self.concreteTree.operator){
			case AND: {
				//throw new Exception("Not a CTL formula!")
				"!(" + _self.getANDFormula("!("+_self.concreteTree.mainGoal.preFormula +")", "TRUE")
				+ ") + !(" + _self.getANDFormula("TRUE", "!("+_self.concreteTree.mainGoal.postFormula+")") + ");";
			}
			case OR: {
				// !i * i1 * EF f1 + i1 * EF(f1 * !f) + ... + !i * in * EF fn + in * EF(fn * !f))
				// with two modification : EX to skip the first, "virtual" state of the transition system, and init=1 to check that we start in a correct initial state
				'''!EX(init=1 * «FOR subgoal : _self.concreteTree.subgoals SEPARATOR ' + ' »!«_self.concreteTree.mainGoal.preFormula» * «subgoal.preFormula» * EF «subgoal.postFormula» + «subgoal.preFormula» * EF («subgoal.postFormula» * !«_self.concreteTree.mainGoal.postFormula»)«ENDFOR»);'''
			}
			case SAND: {
				// !i * i1 * EF (f1 * i2 * EF( f2 * ... * EF (fn))) 
				// + i1 * EF (f1 * i2 * EF( f2 * ... * EF (fn * !f))) 
				// with two modification : EX to skip the first, "virtual" state of the transition system, and init=1 to check that we start in a correct initial state
				'''!EX(init=1 * !«_self.concreteTree.mainGoal.preFormula»«FOR subgoal : _self.concreteTree.subgoals BEFORE ' * ' SEPARATOR ' * '»«subgoal.preFormula» * EF («subgoal.postFormula»«ENDFOR»«FOR subgoal : _self.concreteTree.subgoals»)«ENDFOR» + «FOR subgoal : _self.concreteTree.subgoals SEPARATOR ' * '»«subgoal.preFormula» * EF («subgoal.postFormula»«ENDFOR» * !«_self.concreteTree.mainGoal.postFormula»«FOR subgoal : _self.concreteTree.subgoals»)«ENDFOR»);'''
			}
			case UNKNOWN: {
				throw new Exception("Unknown operator. You should not try to test for the meet correctness for leaves!")
			}
		}
	}
	
	public def String getOverMatchFormula() {
		val res = 
		switch (_self.concreteTree.operator){
			case OR: {
				'''!EX(init=1 * «FOR J : OrdersHelper.subsets(_self.concreteTree.subgoals.size) SEPARATOR ' + ' »«_self.concreteTree.mainGoal.preFormula»«FOR j : J BEFORE ' * ' SEPARATOR ' * '»!(«_self.concreteTree.subgoals.get(j).preFormula»)«ENDFOR» * EF(«_self.concreteTree.mainGoal.postFormula»«FOR j : OrdersHelper.complement(J,_self.concreteTree.subgoals.size) BEFORE ' * ' SEPARATOR ' * '»!(«_self.concreteTree.subgoals.get(j).postFormula»)«ENDFOR»)«ENDFOR»);'''
			}
			case SAND: {
				'''!EX(init=1 * (
				«_self.concreteTree.mainGoal.preFormula» * !(«_self.concreteTree.subgoals.get(0).preFormula») * EF «_self.concreteTree.mainGoal.postFormula»
				«FOR k : 1.._self.concreteTree.subgoals.size-2 BEFORE ' + ' SEPARATOR ' + '»
				«_self.concreteTree.mainGoal.preFormula»«FOR subgoal : _self.concreteTree.subgoals.subList(0,k) BEFORE ' * ' SEPARATOR ' * '»«subgoal.preFormula» * EF («subgoal.postFormula»«ENDFOR» * «_self.concreteTree.subgoals.get(k).preFormula» * E(!«_self.concreteTree.subgoals.get(k).postFormula» U !«_self.concreteTree.subgoals.get(k).postFormula» * «_self.concreteTree.mainGoal.postFormula»)«FOR subgoal : _self.concreteTree.subgoals.subList(0,k)»)«ENDFOR»
				«ENDFOR»
				 + 
				«_self.concreteTree.mainGoal.preFormula»«FOR subgoal : _self.concreteTree.subgoals.subList(0,_self.concreteTree.subgoals.size-1) BEFORE ' * ' SEPARATOR ' * '»«subgoal.preFormula» * EF («subgoal.postFormula»«ENDFOR» * «_self.concreteTree.subgoals.get(_self.concreteTree.subgoals.size-1).preFormula» * E(!«_self.concreteTree.subgoals.get(_self.concreteTree.subgoals.size-1).postFormula» U !«_self.concreteTree.subgoals.get(_self.concreteTree.subgoals.size-1).postFormula» * «_self.concreteTree.mainGoal.postFormula»)«FOR subgoal : _self.concreteTree.subgoals.subList(0,_self.concreteTree.subgoals.size-1)»)«ENDFOR»
				));'''
			}
			case AND: {
				'''!EX(init=1 *(
				«FOR subgoal : _self.concreteTree.subgoals SEPARATOR ' + '»«_self.concreteTree.mainGoal.preFormula» * E(!«subgoal.preFormula» U !«subgoal.preFormula» * «_self.concreteTree.mainGoal.postFormula»)«ENDFOR»
				«FOR subgoal : _self.concreteTree.subgoals BEFORE ' + ' SEPARATOR ' + '»«_self.concreteTree.mainGoal.preFormula» * E(!«subgoal.preFormula» U «subgoal.preFormula» * E(!«subgoal.postFormula» U !«subgoal.postFormula» * «_self.concreteTree.mainGoal.postFormula»))«ENDFOR»
				«FOR J : OrdersHelper.subsets(_self.concreteTree.subgoals.size) BEFORE ' + ' SEPARATOR ' + ' »«_self.concreteTree.mainGoal.preFormula»* E(TRUE«FOR j : J BEFORE ' * ' SEPARATOR ' * '»!(«_self.concreteTree.subgoals.get(j).preFormula»)«ENDFOR» U «FOR j : J SEPARATOR ' * ' AFTER ' * '»!(«_self.concreteTree.subgoals.get(j).preFormula»)«ENDFOR»E(TRUE«FOR j : OrdersHelper.complement(J,_self.concreteTree.subgoals.size) BEFORE ' * ' SEPARATOR ' * '»!(«_self.concreteTree.subgoals.get(j).postFormula»)«ENDFOR» U «FOR j : OrdersHelper.complement(J,_self.concreteTree.subgoals.size) SEPARATOR ' * ' AFTER ' * '»!(«_self.concreteTree.subgoals.get(j).postFormula»)«ENDFOR»«_self.concreteTree.mainGoal.postFormula»))«ENDFOR»
				));''';
			}
			case UNKNOWN: {
				throw new Exception("Unknown operator. You should not try to test for the meet correctness for leaves!")
			}
		}
		println(res)
		res
	}
	
}

class OrdersHelper {
	
//	public static def void forAllOrder(Set<Integer> choices, LinkedList<Integer> chosen, Consumer<List<Integer>> consumer) {
//		for(element : choices) {
//			val nextchoices = new TreeSet<Integer>(choices);
//			nextchoices.remove(element);
//			chosen.addLast(element);
//			forAllOrder(nextchoices,chosen,consumer);
//			chosen.pollLast();
//		}
//		if(choices.empty) {
//			consumer.accept(chosen);
//		}
//	}
	public static def void forAllOrder(Set<Integer> toOpen, Set<Integer> toClose, LinkedList<Pair<Integer,Integer>> chosen, Consumer<List<Pair<Integer,Integer>>> consumer) {
		for(element : toOpen) {
			val nextchoices = new TreeSet<Integer>(toOpen);
			nextchoices.remove(element);
			chosen.addLast(new Pair(0,element));
			toClose.add(element);
			forAllOrder(nextchoices,toClose,chosen,consumer);
			toClose.remove(element);
			chosen.pollLast();
		}
		for(element : toClose) {
			val nextchoices = new TreeSet<Integer>(toClose);
			nextchoices.remove(element);
			chosen.addLast(new Pair(1,element));
			forAllOrder(toOpen,nextchoices,chosen,consumer);
			chosen.pollLast();
		}
		if(toOpen.empty && toClose.empty) {
			consumer.accept(chosen);
		}
	}
	
	public static def ArrayList<ArrayList<Integer>> subsets(int max) {
		val result = newArrayList(newArrayList());
		for(var i = 0; i < max; i++) {
			val ii = i
			val newresults = newArrayList()
			result.forEach[l | 
				val listwith = newArrayList();
				listwith.addAll(l);
				listwith.add(ii);
				newresults.add(listwith);
				]
			result.addAll(newresults)
//			for (list : result) {
//				val listwith = newArrayList()
//				listwith.addAll(list);
//				listwith.add(i as Integer);
//				result.add(listwith);
//			}
		}
		result
	}
	
	public static def complement(ArrayList<Integer> list, int max) {
		val result = newArrayList()
		for (i : 0..max-1) {
			if(!list.contains(i)) {
				result.add(i)
			}
		}
		result
	}
}
