/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.gal;

public class UnableToFindITSToolException extends RuntimeException {
	private final String toolName;
	public UnableToFindITSToolException(String toolName, String message) {
		super(message);
		this.toolName = toolName;
	}
	
	public UnableToFindITSToolException(String toolName) {
		super("Unable to find " + toolName);
		this.toolName = toolName;
	}
	
	public UnableToFindITSToolException(String toolName, Throwable e) {
		super(e);
		this.toolName = toolName;
	}
	
	public String getToolName() {
		return toolName;
	}
}
