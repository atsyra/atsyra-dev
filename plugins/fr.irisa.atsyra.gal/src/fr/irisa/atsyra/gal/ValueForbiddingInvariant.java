/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.gal;

import java.util.stream.Stream;

import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class ValueForbiddingInvariant {
	Multimap<String, Integer> forbiddenValues;
	
	public ValueForbiddingInvariant() {
		forbiddenValues = HashMultimap.create();
	}
	
	public void addForbiddenValue(String variableName, int value) {
		forbiddenValues.put(variableName, value);
	}
	
	public void addInvariant(ValueForbiddingInvariant other) {
		forbiddenValues.putAll(other.forbiddenValues);
	}
	
	public String getInvariant() {
		final Function2<String, String, String> concatWithAnd = (s1, s2) -> {
			if (s1.isEmpty())
				return s2;
			else
				return s1 + "&&" + s2;
		};
		return IterableExtensions.fold(IterableExtensions.map(forbiddenValues.entries(), entry ->  entry.getKey() + "!=" + entry.getValue()),"", concatWithAnd);
	}
	
	public static Stream<ValueForbiddingInvariant> invariantsFromPossibleValues(Multimap<String, Integer> possibleValues) {
		return possibleValues.asMap().entrySet().stream().map(entry -> IterableExtensions.fold(entry.getValue(), new ValueForbiddingInvariant(), (inv, i) -> {
			inv.addForbiddenValue(entry.getKey(), i);
			return inv;
		}));
	}
	
	@Override
	public boolean equals(Object other) {
		if(!(other instanceof ValueForbiddingInvariant)) {
			return false;
		} else {
			final ValueForbiddingInvariant o = (ValueForbiddingInvariant) other;
			return this.forbiddenValues.equals(o.forbiddenValues);
		}
		
	}
	@Override
	public int hashCode() {
		return forbiddenValues.hashCode();
	}
}
