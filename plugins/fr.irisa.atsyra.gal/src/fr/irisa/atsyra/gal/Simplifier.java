/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.gal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.atsyra.gal.IntExpressionEval.NotEvaluableException;
import fr.lip6.move.gal.ArrayPrefix;
import fr.lip6.move.gal.BooleanExpression;
import fr.lip6.move.gal.GALTypeDeclaration;
import fr.lip6.move.gal.IntExpression;
import fr.lip6.move.gal.NamedDeclaration;
import fr.lip6.move.gal.Variable;
import fr.lip6.move.gal.VariableReference;
import fr.lip6.move.gal.support.Support;

/**
 * A Class that wraps fr.lip6.move.gal.instantiate.Simplifier, by adding removal of constant variables at simplification.
 * For use outside of ITSTools simplification process.
 * @author maxime
 *
 */
public class Simplifier {
	private Simplifier() {
		// static class
	}
	public static BooleanExpression simplify(BooleanExpression toSimplify, GALTypeDeclaration galModel) {
		if(galModel != null) {
			Set<Variable> constVars = new HashSet<>(galModel.getVariables());
			Map<ArrayPrefix, Set<Integer>> constantArrs = new HashMap<>();
			Set<NamedDeclaration> dontremove = new HashSet<>();
			Support support = new Support();
			fr.lip6.move.gal.instantiate.Simplifier.computeConstants(galModel, constVars, constantArrs, dontremove, support);
			replaceConstants(galModel, constVars, constantArrs, toSimplify);
		}
		return fr.lip6.move.gal.instantiate.Simplifier.simplify(toSimplify);
	}
	
	/**
	 * Same as {@link fr.lip6.move.gal.instantiate.Simplifier#replaceConstantRefs(GALTypeDeclaration, Set, Map) replaceConstants from ITSTools}, but only simplify the given object
	 * @see fr.lip6.move.gal.instantiate.Simplifier#replaceConstantRefs
	 * 
	 */
	
	private static void replaceConstants(GALTypeDeclaration galModel, Set<Variable> constVars,
			Map<ArrayPrefix, Set<Integer>> constantArrs, EObject toSimplify) {
		IntExpressionEval intExprEval = new IntExpressionEval(galModel);
		TreeIterator<EObject> allContents = toSimplify.eAllContents();
		while(allContents.hasNext()) {
			EObject content = allContents.next();
			if(content instanceof VariableReference) {
				VariableReference varref = (VariableReference) content;
				if(constVars.contains(varref.getRef())) {
					IntExpression value = EcoreUtil.copy(((Variable) varref.getRef()).getValue());
					EcoreUtil.replace(varref, value);
					allContents.prune();
				} else if (constantArrs.containsKey(varref.getRef())) {
					int index;
					try {
						index = intExprEval.eval(varref.getIndex());
						if(constantArrs.get(varref.getRef()).contains(index)) {
							IntExpression value = EcoreUtil.copy(((ArrayPrefix) varref.getRef()).getValues().get(index));
							EcoreUtil.replace(varref, value);
							allContents.prune();
						}
					} catch (NotEvaluableException e) {
						// index is not constant, we do not replace the array ref
					}
				}
			}
		}
	}
}
