package fr.irisa.atsyra.gal

import fr.irisa.atsyra.gal.process.InterruptibleProcessController
import fr.irisa.atsyra.gal.process.InterruptibleProcessController.KilledByUserException
import fr.lip6.move.gal.process.ProcessController.TimeOutException
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.util.List
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem
import fr.irisa.atsyra.preferences.GlobalPreferenceService

abstract class AbstractGalReachability {
	protected long timeout = 60
	protected File gal_file
	
	protected MessagingSystem messagingSystem;
	protected final String msgGroup = "GAL";
	
	public long executionDuration = 0
	
	boolean timeoutTriggered = false
	
	new(File galFile) {
		gal_file = galFile
		timeout = getDefaultTimeout()
		messagingSystem = Activator.getMessagingSystem
	}
	
	new(File galFile, MessagingSystem messagingsystem) {
		gal_file = galFile
		timeout = getDefaultTimeout()
		messagingSystem = messagingsystem
	}
	
	def runReachability(IProgressMonitor monitor) throws IOException {
		if (getITSToolPath() === null)
			throw new UnableToFindITSToolException("its-reach")

		val cmd = buildCommandArguments();
		val StringBuilder cmdSB = new StringBuilder
		cmd.forEach[s | if(s.contains(" ")) { cmdSB.append("\"");} cmdSB.append(s); if(s.contains(" ")) { cmdSB.append("\"");}; cmdSB.append (" ")]		
		messagingSystem.debug(cmdSB.toString, msgGroup)
		val errorOutput = new ByteArrayOutputStream();
		val stdOutput = new ByteArrayOutputStream();

		var long startTime = System.currentTimeMillis
		val controller = new InterruptibleProcessController(
				timeout * 1000, cmd.toArray(newArrayOfSize(cmd.size())), null,
				gal_file.parentFile, monitor, "ITSReach "+gal_file.name+" ", true); // workdir
		controller.forwardErrorOutput(errorOutput);
		controller.forwardOutput(stdOutput);
		controller.messagingSystem = messagingSystem
		var int exitCode
		try {
			exitCode = controller.execute();
			val stdOutString = stdOutput.toString();
			
			if (exitCode != 0) {
				// return new Status(IStatus.WARNING, ITSEditorPlugin.getID(),
				// "ITS exit code: " + exitCode + "."
				// + createContentMessage(errorOutput));
				// }
				if (errorOutput.size() > 0) {	
					messagingSystem.error("Computation of reachable scenarios from the BuildingAttack reported some errors :\n"+ errorOutput.toString, msgGroup);
				}
			} else {
				
				
				
				//resSet = resourceSetProvider.get(gtb_file.getProject());
				//resSet = new ResourceSetImpl();
				
				//updateActionDefinitionFromReachabilityTrace(stdOutput.toString());
				//GalOutput2AttackScenario.buildScenarioModelFromReachabilityTrace(stdOutput.toString(), scenario_file, lla_building_action_file, resSet);
				timeoutTriggered = false;
				processGALOutpout(stdOutString, false);
			}
		} catch (InterruptibleProcessController.TimeOutException | TimeOutException e){
			val stdOutString = stdOutput.toString();
			timeoutTriggered = true;
			processGALOutpout(stdOutString, true);
			messagingSystem.error(
							"Computation of reachable scenarios did not finish in a timely way.\n"
									+ errorOutput, msgGroup);
		} catch (KilledByUserException e) {
			val stdOutString = stdOutput.toString();
			//Activator.error(errorOutput.toString(), null);
			timeoutTriggered = true;
			processGALOutpout(stdOutString, true);
			//Activator.debug(stdOutString);
			messagingSystem.error("Computation of reachable scenarios interrupted by user.", msgGroup);						
			
		} finally {
			executionDuration = System.currentTimeMillis - startTime
		}
	}
	
	def String getITSToolPath() {
		val toolFullPath = GlobalPreferenceService.INSTANCE.getString("fr.lip6.move.gal.itstools.preference", "its-reach").orElse("");
		if (toolFullPath === null || toolFullPath.isEmpty() || !new File(toolFullPath).isFile()) {
			throw new UnableToFindITSToolException("its-reach")
		}
		return toolFullPath;
	}
	
	def getDefaultTimeout() {
		return GlobalPreferenceService.INSTANCE.getInt("fr.irisa.atsyra.ide.ui", "maxExecTimePreference").orElse(60)
	}
	
	def void setTimeout(long timeout) {
		this.timeout = timeout
	}
	
	def long getTimeout() {
		return this.timeout
	}
	
	def setMessagingSystem(MessagingSystem messagingSystem) {
		this.messagingSystem = messagingSystem
	}
	
	def protected void processGALOutpout(String stdOutString, boolean timeoutTriggered)
	
	def protected List<String> buildCommandArguments()
	
		static def String galCommandFromGalOutput(String galOutput) {
		return galOutput.lines().skip(2).findFirst().orElse("");
	}
	
	def protected getTimeoutTriggered() {
		timeoutTriggered
	}
}