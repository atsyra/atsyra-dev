/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.gal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.lip6.move.gal.ArrayPrefix;
import fr.lip6.move.gal.BinaryIntExpression;
import fr.lip6.move.gal.Constant;
import fr.lip6.move.gal.GALTypeDeclaration;
import fr.lip6.move.gal.IntExpression;
import fr.lip6.move.gal.NamedDeclaration;
import fr.lip6.move.gal.UnaryMinus;
import fr.lip6.move.gal.Variable;
import fr.lip6.move.gal.VariableReference;
import fr.lip6.move.gal.support.Support;

public class IntExpressionEval {
	public static class NotEvaluableException extends Exception {
		private static final long serialVersionUID = -3596376830317120324L;
	}
	
	private final Set<Variable> constvars;
	private final Map<ArrayPrefix, Set<Integer>> constantArrs;
	
	public IntExpressionEval() {
		constvars = Set.of();
		constantArrs = Map.of();
	}
	
	public IntExpressionEval(GALTypeDeclaration galModel) {
		constvars = new HashSet<>(galModel.getVariables());
		constantArrs = new HashMap<>();
		Set<NamedDeclaration> dontremove = new HashSet<>();
		Support support = new Support();
		fr.lip6.move.gal.instantiate.Simplifier.computeConstants(galModel, constvars, constantArrs, dontremove, support);
	}

	public int eval(IntExpression expr) throws NotEvaluableException {
		if (expr instanceof Constant) {
			return ((Constant) expr).getValue();
		} else if (expr instanceof UnaryMinus) {
			return -eval(((UnaryMinus) expr).getValue());
		} else if (expr instanceof BinaryIntExpression) {
			switch (((BinaryIntExpression) expr).getOp()) {
			case "+":
				return eval(((BinaryIntExpression) expr).getLeft()) + eval(((BinaryIntExpression) expr).getRight());
			case "-":
				return eval(((BinaryIntExpression) expr).getLeft()) - eval(((BinaryIntExpression) expr).getRight());
			case "*":
				return eval(((BinaryIntExpression) expr).getLeft()) * eval(((BinaryIntExpression) expr).getRight());
			case "/":
				return eval(((BinaryIntExpression) expr).getLeft()) / eval(((BinaryIntExpression) expr).getRight());
			case "%":
				return eval(((BinaryIntExpression) expr).getLeft()) % eval(((BinaryIntExpression) expr).getRight());
			default:
				throw new IllegalArgumentException("unknown operator " + ((BinaryIntExpression) expr).getOp());
			}
		} else if (expr instanceof VariableReference) {
			VariableReference vr = (VariableReference) expr;
			if (constvars.contains(vr.getRef())) {
				return eval(((Variable) vr.getRef()).getValue());
			} else if (constantArrs.containsKey(vr.getRef())) {
				int indexEval = eval(vr.getIndex());
				if (constantArrs.get(vr.getRef()).contains(indexEval)) {
					return eval(((ArrayPrefix) vr.getRef()).getValues().get(indexEval));
				}
			}
		}
		// Nothing to return
		throw new NotEvaluableException();
	}
}
