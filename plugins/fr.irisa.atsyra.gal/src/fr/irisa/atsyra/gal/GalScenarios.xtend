package fr.irisa.atsyra.gal

import java.io.File
import java.util.ArrayList
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Platform
import org.eclipse.core.runtime.Status
import org.eclipse.core.runtime.SubMonitor
import org.eclipse.core.resources.IFile
import fr.irisa.atsyra.preferences.GlobalPreferenceService

class GalScenarios extends AbstractGalReachability {

	
	new(File galFile) {
		super(galFile)
	}
	new(IFile galFile) {
		super(new File(galFile.location.toOSString))
	}
	int nbsearched = 10
	int statePrintLimit = 10
	String invariant
	def computeScenarios(int nbsearched, int statePrintLimit, String invariant, IProgressMonitor monitor) {
		val SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		subMonitor.taskName = "Searching witnesses"
		this.nbsearched = nbsearched
		this.statePrintLimit = statePrintLimit
		this.invariant = invariant
		if (gal_file !== null) {
			try {
				runReachability(subMonitor);
				return Status.OK_STATUS;						
			} catch (Exception e) {
				Activator.error(
						"Compute reachability raised an exception "
								+ e.getMessage(), e);	
			}
		}
	}
	
	override processGALOutpout(String stdOutString, boolean timeoutTrigerred){
		Activator.info(stdOutString)
	}	
	
	override buildCommandArguments() {
		// should build something like :
		// its-reach -i building.gal -t GAL -reachable 'goalReached==1'
		// --nowitness

		val cmd = new ArrayList<String>();
		cmd.add(getITSToolPath());
		cmd.add("--quiet");

		cmd.add("-i");
		cmd.add(gal_file.getName());
		cmd.add("-t");
		cmd.add("GAL");
		cmd.add("-reachable");
		//cmd.add("objectif_atteint==1");
		cmd.add("goal_reached==1");
		cmd.add("-manywitness");
		cmd.add(""+nbsearched);
		cmd.add("--trace-states");
		cmd.add("--print-limit");
		cmd.add(""+statePrintLimit);
		val getNonMinimalWitnesses = GlobalPreferenceService.INSTANCE.getBoolean("fr.irisa.atsyra.ide.ui", "nonMinimalManyPreference").orElse(false)
		if(getNonMinimalWitnesses) {		
			cmd.add("--init-gadget")	
		}
		if(!invariant.nullOrEmpty){
			cmd.add("-with-invariant")
			cmd.add(invariant)
			
		}

		return cmd;
	}
}