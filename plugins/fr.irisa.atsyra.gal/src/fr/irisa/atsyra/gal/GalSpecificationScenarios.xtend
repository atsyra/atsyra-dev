package fr.irisa.atsyra.gal

import fr.irisa.atsyra.preferences.GlobalPreferenceService
import fr.lip6.move.gal.BooleanExpression
import fr.lip6.move.gal.SafetyProp
import fr.lip6.move.gal.Specification
import fr.lip6.move.serialization.SerializationUtil
import java.io.File
import java.util.ArrayList
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Status
import org.eclipse.core.runtime.SubMonitor
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem

class GalSpecificationScenarios extends AbstractGalReachability {
	String goalName
	File propFile
	Specification specification
	StringBuilder result
	int nbsearched = 10
	int statePrintLimit = 10
	String invariant

	new(Specification spec, File galFile, File propFile, String goal) {
		super(galFile)
		goalName = goal
		this.propFile = propFile
		specification = EcoreUtil.copy(spec)
		result = new StringBuilder()
		nbsearched = nbWitness
	}

	new(Specification spec, File galFile, File propFile, String goal, StringBuilder resultBuffer) {
		super(galFile)
		goalName = goal
		this.propFile = propFile
		specification = EcoreUtil.copy(spec)
		result = resultBuffer
		nbsearched = nbWitness
	}

	new(Specification spec, File galFile, File propFile, String goal, MessagingSystem messagingsystem) {
		super(galFile, messagingsystem)
		goalName = goal
		this.propFile = propFile
		specification = EcoreUtil.copy(spec)
		result = new StringBuilder()
		nbsearched = nbWitness
	}

	new(Specification spec, File galFile, File propFile, String goal, StringBuilder resultBuffer,
		MessagingSystem messagingsystem) {
		super(galFile, messagingsystem)
		goalName = goal
		this.propFile = propFile
		specification = EcoreUtil.copy(spec)
		result = resultBuffer
		nbsearched = nbWitness
	}

	def computeReachability(IProgressMonitor monitor) {
		val subMonitor = SubMonitor.convert(monitor, 100);
		gal_file.createNewFile
		propFile.createNewFile
		specification.properties.removeIf[!(it.getBody() instanceof SafetyProp)]
		SerializationUtil.serializePropertiesForITSTools(gal_file.absolutePath, specification.getProperties(),
			propFile.absolutePath);
		subMonitor.worked(10);
		specification.properties.clear();
		SerializationUtil.systemToFile(specification, gal_file.absolutePath, false);
		subMonitor.worked(10);
		if (gal_file !== null) {
			runReachability(subMonitor.split(70));
		}
		subMonitor.done()
		if (timeoutTriggered) {
			return Status.error("timeout");
		} else {
			return Status.OK_STATUS;
		}
	}

	override processGALOutpout(String stdOutString, boolean timeoutTriggered) {
		result.append(stdOutString);
		// resultHandler.log = stdOutString
		if (stdOutString.contains("Reachability property " + goalName + " is true.")) {
			messagingSystem.important(goalName + " is reachable!", msgGroup)
			// resultHandler.value = ResultValue.TRUE
			if (timeoutTriggered) {
				messagingSystem.error("Unable to compute witnesses for " + goalName + " : timeout.", msgGroup)
			// resultHandler.value = ResultValue.TIMEOUT
			}
		} else {
			if (timeoutTriggered) {
				messagingSystem.error("Unable to compute reachability for " + goalName + " : timeout.", msgGroup)
			// resultHandler.value = ResultValue.TIMEOUT
			} else {
				messagingSystem.important(goalName + " is not reachable.", msgGroup)
			// resultHandler.value = ResultValue.FALSE
			}
		}
	}

	def void setInvariant(BooleanExpression expr) {
		invariant = SerializationUtil.getText(expr, true)
	}

	def void setInvariant(String expr) {
		invariant = expr
	}

	override buildCommandArguments() {
		val cmd = new ArrayList<String>();
		cmd.add(getITSToolPath());
		cmd.add("--quiet");
		cmd.add("-i");
		cmd.add(gal_file.name);
		cmd.add("-t");
		cmd.add("GAL");
		cmd.add("-reachable-file");
		cmd.add(propFile.absolutePath);
		cmd.add("-manywitness");
		cmd.add("" + nbsearched);
		if (invariant !== null) {
			cmd.add("-with-invariant");
			cmd.add(invariant);
		}
		cmd.add("--trace-states");
		cmd.add("--print-limit");
		cmd.add(Integer.toString(statePrintLimit));
		return cmd;
	}

	static def getNbWitness() {
		return GlobalPreferenceService.INSTANCE.getInt("fr.irisa.atsyra.ide.ui", "manyPreference").orElse(1)
	}
}
