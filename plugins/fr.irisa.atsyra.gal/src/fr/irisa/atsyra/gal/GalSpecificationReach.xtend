package fr.irisa.atsyra.gal

import fr.lip6.move.gal.SafetyProp
import fr.lip6.move.gal.Specification
import fr.lip6.move.serialization.SerializationUtil
import java.io.File
import java.util.ArrayList
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Status
import org.eclipse.core.runtime.SubMonitor
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem

class GalSpecificationReach extends AbstractGalReachability {
	String goalName
	File propFile
	Specification specification
	StringBuffer result

	new(Specification spec, File galFile, File propFile, String goal) {
		super(galFile)
		goalName = goal
		this.propFile = propFile
		specification = EcoreUtil.copy(spec)
		result = new StringBuffer()
	}

	new(Specification spec, File galFile, File propFile, String goal, StringBuffer resultBuffer) {
		super(galFile)
		goalName = goal
		this.propFile = propFile
		specification = EcoreUtil.copy(spec)
		result = resultBuffer
	}

	new(Specification spec, File galFile, File propFile, String goal, MessagingSystem messagingsystem) {
		super(galFile, messagingsystem)
		goalName = goal
		this.propFile = propFile
		specification = EcoreUtil.copy(spec)
		result = new StringBuffer()
	}

	new(Specification spec, File galFile, File propFile, String goal, StringBuffer resultBuffer,
		MessagingSystem messagingsystem) {
		super(galFile, messagingsystem)
		goalName = goal
		this.propFile = propFile
		specification = EcoreUtil.copy(spec)
		result = resultBuffer
	}

	def computeReachability(IProgressMonitor monitor) {
		val subMonitor = SubMonitor.convert(monitor, 100)
		gal_file.createNewFile
		propFile.createNewFile
		specification.properties.removeIf[!(it.getBody() instanceof SafetyProp)]
		SerializationUtil.serializePropertiesForITSTools(gal_file.absolutePath, specification.getProperties(),
			propFile.absolutePath)
		subMonitor.worked(10)
		specification.properties.clear()
		SerializationUtil.systemToFile(specification, gal_file.absolutePath, false)
		subMonitor.worked(10)
		runReachability(subMonitor.split(70))
		if (timeoutTriggered) {
			return Status.error("timeout");
		} else {
			return Status.OK_STATUS;
		}
	}

	override processGALOutpout(String stdOutString, boolean timeoutTrigerred) {
		result.append(stdOutString);
		// resultHandler.log = stdOutString
		if (stdOutString.contains("Reachability property " + goalName + " is true.")) {
			messagingSystem.important(goalName + " is reachable!", msgGroup)
		// resultHandler.value = ResultValue.TRUE
		} else {
			if (timeoutTrigerred) {
				messagingSystem.error("Unable to compute reachability for " + goalName + " : timeout.", msgGroup)
			// resultHandler.value = ResultValue.TIMEOUT
			} else {
				messagingSystem.important(goalName + " is not reachable.", msgGroup)
			// resultHandler.value = ResultValue.FALSE
			}
		}
	}

	override buildCommandArguments() {
		// should build something like :
		// its-reach -i  file.gal  -t  CGAL  -reachable-file  file.prop
		// --nowitness
		val cmd = new ArrayList<String>();
		cmd.add(getITSToolPath());
		cmd.add("--quiet");

		cmd.add("-i");
		cmd.add(gal_file.getName());
		cmd.add("-t");
		cmd.add("GAL");
		cmd.add("-reachable-file");
		cmd.add(propFile.getName());
		// cmd.add("--nowitness");
		return cmd;
	}
}
