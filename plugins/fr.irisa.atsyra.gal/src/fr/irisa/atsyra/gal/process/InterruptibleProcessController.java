/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.gal.process;



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Optional;
import java.util.logging.Logger;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;

import fr.irisa.atsyra.gal.Activator;


/**
 * Executes an external process synchronously, allowing the client to define a
 * maximum amount of time for the process to complete.
 */
public class InterruptibleProcessController {
	/**
	 * Thrown when a process being executed exceeds the maximum amount of time
	 * allowed for it to complete.
	 */
	public class TimeOutException extends Exception {
		/**
		 * All serializable objects should have a stable serialVersionUID
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Default constructor
		 */
		public TimeOutException() {
		}
		/**
		 * Constructor
		 * @param message the message.
		 */
		public TimeOutException(String message) {
			super(message);
		}
		
		public TimeOutException(long timeLimit, String[] params) {
			super("Timeout after "+timeLimit+" ms of process :"+Arrays.toString(params));
		}
	}

	/**
	 * Thrown when a process being executed exceeds the maximum amount of time
	 * allowed for it to complete.
	 */
	public class KilledByUserException extends Exception {
		/**
		 * All serializable objects should have a stable serialVersionUID
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Default constructor
		 */
		public KilledByUserException() {
		}
		/**
		 * Constructor
		 * @param message the message.
		 */
		public KilledByUserException(String message) {
			super(message);
		}
	}
	
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger("fr.lip6.move.gal"); //$NON-NLS-1$

	private boolean finished;
	private OutputStream forwardStdErr;
	private OutputStream forwardStdOut;
	private boolean killed;
	private String[] params;
	private Process process;
	private long timeLimit;
	private String[] env;
	private File baseDir;
	private IProgressMonitor monitor;
	protected String monitorTaskMsg;
	private boolean copyOutputToConsoleOnTheFly;
	private MessagingSystem messagingSystem;
	protected static final String msgGroup = "GAL";
	private Optional<IOException> thrown;

	/**
	 * Constructs an instance of ProcessController.<br>
	 * This does not creates an OS process. <code>run()</code> does that.
	 *
	 * @param timeout The maximum time the process should take to run
	 * @param params The parameters to be passed to the controlled process
	 * @param env The environment vars
	 * @param baseDir The base directory
	 */
	public InterruptibleProcessController(long timeout, 
			String[] params, 
			String[] env, 
			File baseDir, 
			IProgressMonitor monitor, 
			String monitorTaskMsg, 
			Boolean copyOutputToConsoleOnTheFly) {
		timeLimit = timeout;
		this.params = params;
		this.env = env;
		this.baseDir = baseDir;
		this.monitor = monitor;
		this.monitorTaskMsg = monitorTaskMsg;
		this.copyOutputToConsoleOnTheFly = copyOutputToConsoleOnTheFly;
	}

	/**
	 * Causes the process to start executing. This call will block until the
	 * process has completed. If <code>timeout</code> is specified, the
	 * process will be interrupted if it takes more than the specified amount of
	 * time to complete, causing a <code>TimedOutException</code> to be
	 * thrown. Specifying zero as <code>timeout</code> means the process is
	 * not time constrained.
	 *
	 * @return the process exit value
	 * @throws IOException if file problems
	 * @throws TimeOutException If the process did not complete in time
	 * @throws KilledByUserException 
	 */
	public final int execute() throws IOException, TimeOutException, KilledByUserException {
		Thread waiter = new Thread(() -> {
			try {
				thrown = Optional.empty();
				process = Runtime.getRuntime().exec(params, env, baseDir);
				process.waitFor();
			} catch (InterruptedException e) {
				// timeout !!
				LOGGER.warning(e.getMessage());
				Thread.currentThread().interrupt();
			} catch (IOException e) {
				LOGGER.warning(e.getMessage());
				thrown = Optional.of(e);
			}
		});
		waiter.start();
		long waittime = 100;
		long waitperiods = timeLimit / waittime;
		SubMonitor loopMonitor = SubMonitor.convert(monitor,(int) waitperiods);
		loopMonitor.setTaskName(monitorTaskMsg+" (timeout in "+timeLimit/1000+"s)");
		if (timeLimit == 0) {
			waittime = 0;
			waitperiods = 1;
		}
		try {
			for (int i = 0; i < waitperiods  && !monitor.isCanceled(); ++i) {
				// join 0 waits indefinitely
				waiter.join(waittime);
				forwardStreams();
				if(i%10 == 0) {
					loopMonitor.split(10);
					loopMonitor.setTaskName(monitorTaskMsg+" (timeout in "+((int)waitperiods-i)/10+"s)");
				}
			}
		} catch (InterruptedException e) {
			markFinished();
			forwardStreams();
			if (waiter.isAlive()) {
				waiter.interrupt();
				kill();
			}
			Thread.currentThread().interrupt();
		}
		if (waiter.isAlive()) {
			waiter.interrupt();
			kill();
			forwardStreams();
		}
		if(monitor.isCanceled()){
			throw new KilledByUserException();
		}
		if (wasKilled()) {
			throw new TimeOutException(timeLimit, params);
		}
		if(thrown.isPresent()) {
			throw thrown.get();
		}
		return process.exitValue();
	}

	/**
	 * Forwards the process standard output and error output.
	 */
	private void forwardStreams() {
		if (process == null)
			return;
		if (forwardStdErr != null) {
			forwardStream(process.getErrorStream(), forwardStdErr, true);
		}
		if (forwardStdOut != null) {
			forwardStream(process.getInputStream(), forwardStdOut, false);
		}
	}

	/**
	 * Forwards the process standard error output to the given output stream.
	 * Must be called before execution has started.
	 * @param err An output stream where to forward the process standard error output to
	 */
	public final void forwardErrorOutput(OutputStream err) {
		forwardStdErr = err;
	}

	/**
	 * Forwards the process standard output to the given output stream. Must be
	 * called before execution has started.
	 *
	 * @param out An output stream where to forward the process standard output to
	 */
	public final void forwardOutput(OutputStream out) {
		forwardStdOut = out;
	}

	/**
	 * Forward the stream, using a thread, with name "name"
	 * @param name name used to identify this thread
	 * @param in input
	 * @param out output
	 */
	private void forwardStream(final InputStream in, final OutputStream out, final boolean isErrStream) {
		try {
			int av=0;
			byte [] buff = new byte[1024];
			ByteArrayOutputStream teeOutput = new ByteArrayOutputStream();
			while ( (av= in.available()) > 0) {
				int toread = Math.min(av, buff.length);
				int read = in.read(buff,0,toread);
				out.write(buff,0,read);
				out.flush();
				if(copyOutputToConsoleOnTheFly) {
					teeOutput.write(buff,0,read);
				}
			}
			out.flush();
			if(copyOutputToConsoleOnTheFly) {
				teeOutput.flush();
				String outString = teeOutput.toString();
				if(!outString.isEmpty()) {
					if(isErrStream) getMessagingSystem().error(outString, msgGroup);
					else getMessagingSystem().debug(outString, msgGroup);
				}
			}
		} catch (IOException e) {
			LOGGER.warning(e.getMessage());
		}
	}

	/**
	 * Returns the controlled process.<br>
	 * Will return <code>null</code> before <code>execute</code> is called.
	 * @return the underlying process
	 */
	public final Process getProcess() {
		return process;
	}

	/**
	 * Test if task is finished
	 * @return true when it is...
	 */
	protected final synchronized boolean isFinished() {
		return finished;
	}

	/**
	 * Kills the process. Does nothing if it has been finished already.
	 */
	public final void kill() {
		if (isFinished()) {
			return;
		}
		killed = true;
		process.destroyForcibly();
	}

	/** mark finished state.
	 *
	 */
	private void markFinished() {
		finished = true;
	}

	/**
	 * Returns whether the process was killed due to a time out.
	 * @return <code>true</code> if the process was killed, <code>false</code> if the completed normally
	 */
	public final boolean wasKilled() {
		return killed;
	}
	
	public MessagingSystem getMessagingSystem() {
		if(messagingSystem == null) {
			messagingSystem = Activator.getMessagingSystem();
		}
		return messagingSystem;
	}
	
	/**
	 * Set the messaging system to which the process' output is printed, if copyOutputToConsoleOnTheFly is true.
	 */
	public void setMessagingSystem(MessagingSystem  messagingSystem) {
		this.messagingSystem = messagingSystem;
	}
}
