package fr.irisa.atsyra.gal;

import java.math.BigInteger;
import java.util.Map;
import java.util.Set;

import fr.irisa.atsyra.gal.IntExpressionEval.NotEvaluableException;
import fr.lip6.move.gal.ArrayPrefix;
import fr.lip6.move.gal.GALTypeDeclaration;
import fr.lip6.move.gal.Specification;
import fr.lip6.move.gal.TypeDeclaration;
import fr.lip6.move.gal.VarDecl;
import fr.lip6.move.gal.Variable;
import fr.lip6.move.gal.instantiate.DomainAnalyzer;

public class GALSizeInfo {
	
	private long nbVariable;
	private long nbTransition;
	private BigInteger nbState;

	public GALSizeInfo(long nbVariable, long nbTransition, BigInteger nbState) {
		this.nbVariable = nbVariable;
		this.nbTransition = nbTransition;
		this.nbState = nbState;
	}
	
	public String toString() {
		return "{\"Variable\": " + nbVariable 
				+ ", \"Transition\": " + nbTransition 
				+ ", \"State\": " + nbState
				+ "}";
	}
	
	/**
	 * 
	 * @param spec Sould be a valid GAL specification
	 * @return GALSizeInfo reprensenting the size of the GAL specification
	 */
	public static GALSizeInfo getGalModelSizeInfo(Specification spec) {
		long nbVariable = 0;
		long nbTransition = 0;
		for(TypeDeclaration type : spec.getTypes()) {
			if(type instanceof GALTypeDeclaration) {
				GALTypeDeclaration gal = (GALTypeDeclaration) type;
				IntExpressionEval galEval = new IntExpressionEval(gal);
				nbVariable += gal.getVariables().size();
				for (ArrayPrefix array : gal.getArrays()) {
					try {
						int arraySize = galEval.eval(array.getSize());
						nbVariable += arraySize;
					} catch (NotEvaluableException e) {
						// The gal is invalid, should not happen
						e.printStackTrace();
					}
				}
				nbTransition += gal.getTransitions().size();
			}
		}
		return new GALSizeInfo(nbVariable, nbTransition, getGalNumberOfStates(spec));
	}
	
	/**
	 * Only supports GALTypeDeclaration
	 * @param spec
	 * @return
	 */
	public static BigInteger getGalNumberOfStates(Specification spec) {
		BigInteger result = BigInteger.ONE;
		for (TypeDeclaration typeDecl : spec.getTypes()) {
			if(typeDecl instanceof GALTypeDeclaration) {
				GALTypeDeclaration gal = (GALTypeDeclaration) typeDecl;
				result = result.multiply(getGalNumberOfStates(gal));
			}
		}
		return result;
	}
	
	public static BigInteger getGalNumberOfStates(GALTypeDeclaration gal) {
		IntExpressionEval eval = new IntExpressionEval(gal);
		try {
			Map<VarDecl, Set<Integer>> domains = DomainAnalyzer.computeVariableDomains(gal);
			BigInteger result = BigInteger.ONE;
			for (Variable variable : gal.getVariables()) {
				Set<Integer> domain = domains.get(variable);
				if(domain != null) {
					result = result.multiply(BigInteger.valueOf(domain.size()));
				}
			}
			for (ArrayPrefix array : gal.getArrays()) {
				int size = eval.eval(array.getSize());
				Set<Integer> domain = domains.get(array);
				if(domain != null) {
					result = result.multiply(BigInteger.valueOf((long) domain.size() * size));
				}
			}
			return result;
		} catch (NotEvaluableException e) {
			// Should not happen for well-formed models
			e.printStackTrace();
			return BigInteger.ZERO;
		}
	}
	
}
