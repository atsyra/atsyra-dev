package fr.irisa.atsyra.gal

import fr.lip6.move.gal.BinaryIntExpression
import fr.lip6.move.gal.Comparison
import fr.lip6.move.gal.ComparisonOperators
import fr.lip6.move.gal.Constant
import fr.lip6.move.gal.False
import fr.lip6.move.gal.True
import fr.lip6.move.gal.UnaryMinus
import fr.lip6.move.gal.WrapBoolExpr

class GalExpressionInterpreter {
	
	// integer expression interpreter
	static def dispatch int intEvaluate(WrapBoolExpr exp) {
		return if(exp.value.boolEvaluate) 1 else 0
	}
	
	static def dispatch int intEvaluate(Constant exp) {
		return exp.value
	}
	
	static def dispatch int intEvaluate(UnaryMinus exp) {
		return - exp.value.intEvaluate
	}
	static def dispatch int intEvaluate(BinaryIntExpression _self) {
		val left =_self.left.intEvaluate
		val right = _self.right.intEvaluate
		switch (_self.op) {
			case "+": {
				return left + right
			}
			case "-": {
				return left - right
			}
			case "/": {
				return left / right
			}
			case "*": {
				return left * right
			}
			case "%": {
				return left % right
			}
			default: {
				throw new Exception("Unrecognized operator "+_self.op)		
			}
		}
	}
	
	
	// boolean expression interpreter
	static def dispatch boolean boolEvaluate(True exp) {
		return true
	}
	static def dispatch boolean boolEvaluate(False exp) {
		return false
	}
	static def dispatch boolean boolEvaluate(Comparison _self) {
		val left =_self.left.intEvaluate
		val right = _self.right.intEvaluate
		switch (_self.operator) {
			case ComparisonOperators.EQ: {
				println("evaluate "+left+ " "+ _self.operator + " "+ right+ " returns " + (left == right))
				return left == right
			}
			case ComparisonOperators.NE: {
				return left != right
			}
			case ComparisonOperators.GT: {
				return left > right
			}
			case ComparisonOperators.GE: {
				return left >= right
			}
			case ComparisonOperators.LT: {
				return left < right
			}
			case ComparisonOperators.LE: {
				return left <= right
			}
			default: {
				throw new Exception("Unrecognized operator "+_self.operator)		
			}
		}
	}
}