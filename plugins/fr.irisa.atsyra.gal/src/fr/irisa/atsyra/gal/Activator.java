/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.gal;

import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystemManager;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator implements BundleActivator {

	// The plug-in ID
	public static final String PLUGIN_ID = "fr.irisa.atsyra.gal";

	// The shared instance
	private static Activator plugin;
	

	public void start(BundleContext context) throws Exception {
		plugin = this;
	}

	public void stop(BundleContext context) throws Exception {
		plugin = null;
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	public static void debug(String msg){
		getMessagingSystem().debug(msg, "ATSyRA");
	}
	public static void error(String msg){
		getMessagingSystem().error(msg, "ATSyRA");
	}
	public static void error(String msg,  Throwable e){
		getMessagingSystem().error(msg, "ATSyRA", e);
	}
	public static void info(String str) {
		getMessagingSystem().info(str, "ATSyRA");
	}
	public static void important(String str) {
		getMessagingSystem().important(str, "ATSyRA");
	}
	
	public static final String msgGroup = "fr.irisa.atsyra";
	protected static MessagingSystem messagingSystem = null;
	public static MessagingSystem getMessagingSystem() {
		if(messagingSystem == null) {
			MessagingSystemManager msm = new MessagingSystemManager();
			messagingSystem = msm.createBestPlatformMessagingSystem(msgGroup, "ATSyRA");
		}
		return messagingSystem;
	}

}
