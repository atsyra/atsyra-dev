package fr.irisa.atsyra.atree.correctness.toolwrap

import java.util.BitSet


class BitSetFactory {
	
	def static BitSet bitsetFromString(String s) {
		val res = new BitSet(s.length);
		s.toCharArray.forEach[c, i| if(c == '1') res.set(i)]
		return res;
	}
	
}