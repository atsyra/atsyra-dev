package fr.irisa.atsyra.atree.correctness.toolwrap

import atsyragoal.AtsyraTree
import fr.irisa.atsyra.atree.correctness.Activator
import fr.irisa.atsyra.gal.process.InterruptibleProcessController
import fr.irisa.atsyra.gal.process.InterruptibleProcessController.KilledByUserException
import fr.irisa.atsyra.gal.process.InterruptibleProcessController.TimeOutException
import fr.irisa.atsyra.ide.ui.preferences.PreferenceConstants
import fr.irisa.atsyra.resultstore.ResultValue
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.attribute.PosixFilePermission
import java.util.ArrayList
import java.util.BitSet
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Platform


import static extension fr.irisa.atsyra.transfo.atg.gal.AtsyraTreeHelper.*
import static extension fr.irisa.atsyra.atree.correctness.toolwrap.BitSetFactory.*
import fr.irisa.atsyra.resultstore.Result

class CorrectnessTool {
	IFile gal_file;
	AtsyraTree tree;
	public long timeout = 60;
	String property;
	
	protected Result resultHandler
	
	new(IFile galFile, AtsyraTree atree, String aproperty, Result resultHandler) {
		super()
		gal_file = galFile
		tree = atree
		property = aproperty
		timeout = defaultTimeout
		this.resultHandler = resultHandler
	}
	
	def ResultValue computeCorrectness(IProgressMonitor monitor) {
		var long startTime
		if (gal_file !== null) {
			try {
				startTime = System.currentTimeMillis
				val correctnessToolPath = getCorrectnessToolPath();
				if (correctnessToolPath === null)
					return ResultValue.ABORTED;
				val correctnessToolFile = new File(correctnessToolPath)
				if (!correctnessToolFile.exists) {
					Activator.error("correctness tool executable not available at the specified location: "+correctnessToolPath);
					return ResultValue.ABORTED;
				}
				if(!Files.isExecutable(correctnessToolFile.toPath) && Activator.getOperatingSystemType() != "win") {
					val perm = Files.getPosixFilePermissions(correctnessToolFile.toPath)
					perm.add(PosixFilePermission.OWNER_EXECUTE)
					perm.add(PosixFilePermission.GROUP_EXECUTE)
					perm.add(PosixFilePermission.OTHERS_EXECUTE)
					Files.setPosixFilePermissions(correctnessToolFile.toPath, perm)
				}
				val cmd = buildCommandArguments();
				val errorOutput = new ByteArrayOutputStream();
				val stdOutput = new ByteArrayOutputStream();
				val env = new ArrayList<String>()
				val StringBuilder cmdSB = new StringBuilder
				cmd.forEach[s | if(s.contains(" ")) { cmdSB.append("\"");} cmdSB.append(s); if(s.contains(" ")) { cmdSB.append("\"");}; cmdSB.append (" ")]		
				Activator.debug(cmdSB.toString)
				//On my machine, I need to add this to the environment to load the correctness checking tool
				//It would be nice to add an option to configure the environment manually
				env.add("LD_LIBRARY_PATH=/lib:/usr/lib:/usr/local/lib")
		
				val controller = new InterruptibleProcessController(
						timeout * 1000, cmd.toArray(newArrayOfSize(cmd.size())), env.toArray(newArrayOfSize(env.size())),
						new File(gal_file.getParent().getLocationURI()), monitor, "RefinementCorrectness "+property+" ",true); 
				controller.forwardErrorOutput(errorOutput);
				controller.forwardOutput(stdOutput);
				var int exitCode
				try {
					exitCode = controller.execute();
					val stdOutString = stdOutput.toString();
					Activator.info(stdOutString)
					if (exitCode != 0) {
						Activator.error("model-checking exited with error code: "+ exitCode);
						if (errorOutput.size() > 0) {	
							Activator.error("model-checking reported some errors: "+ errorOutput.toString);
							Activator.error("model-checking reported some errors (cont.): "+ stdOutString);
							this.resultHandler.value = ResultValue.ABORTED
							return ResultValue.ABORTED;
						}
					} else {
						processOutpout(stdOutString);
					}
				} catch (IOException e) {
					Activator.error("Unexpected exception while running model-checking."
											+ errorOutput);
					this.resultHandler.value = ResultValue.ABORTED 
					return ResultValue.ABORTED;
				} catch (TimeOutException e) {
					Activator.error("model-checking did not finish in a timely way."
											+ errorOutput);
					Activator.error("Timeout!")
					this.resultHandler.value = ResultValue.TIMEOUT 
					return ResultValue.TIMEOUT;
				} catch (KilledByUserException e) {
					//Activator.debug(stdOutString);
					Activator.error("model-checking interrupted by user.");	
					this.resultHandler.value = ResultValue.ABORTED 					
					return ResultValue.ABORTED;
				}
					
			} finally {
				resultHandler.duration = System.currentTimeMillis - startTime
			}
		}
				
	}
	
	def ResultValue processOutpout(String stdOutString){
		
		this.resultHandler.log = stdOutString 
		if(stdOutString.contains("Property holds!")){
			Activator.important("Property is true!")
			if(property == "-meet"){
				Activator.important(stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property ")))
			}
			this.resultHandler.value = ResultValue.TRUE 
			return ResultValue.TRUE;
		}
		else{
			Activator.important("Property is false.")
			val index = stdOutString.indexOf("found a counterexample!")
			switch tree.operator {
				case AND: {
					switch property {
						case "-overmatch": 
						switch stdOutString.charAt(index + 24) {
							case 'F'.charAt(0): {// First case 
								Activator.info("Counterexample: " + stdOutString.substring(stdOutString.indexOf("the",index + 24),stdOutString.indexOf(".",index + 24)))
								val witness = stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property "))
								this.resultHandler.details = witness
								Activator.important(witness)
							}
							case 'S'.charAt(0): { // Second case 
								val ce = bitsetFromString(stdOutString.substring(index+46,index+78))
								val notce = ce.clone as BitSet
								notce.flip(0,ce.length)
								Activator.info("Counterexample: There is a path for the main goal that satisfies all the subgoals, but with a hole between " + ce.toString + " and " + notce.toString)
								val witness = stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property "))
								this.resultHandler.details = witness
								Activator.important(witness)
							}
						} 
						case "-undermatch": {
							Activator.info("Counterexample: " + stdOutString.substring(index+24,stdOutString.indexOf(".",index + 24)+1))
							val witness = stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property "))
							this.resultHandler.details = witness
							Activator.important(witness)
						}
					}
				}
				case OR: { 
					switch property {
						case "-overmatch": {
							val bitsetPrevToken = "found a counterexample! subgoals "
							val bitsetEndIndex = stdOutString.indexOf(",",index+bitsetPrevToken.length)
							val bitsetString = stdOutString.substring(index+bitsetPrevToken.length, bitsetEndIndex)
							val ce = bitsetFromString(bitsetString)
							Activator.info("Counterexample: starting, but not satisfying the subgoals in " + ce.toString)
							val witness = stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property "))
							this.resultHandler.details = witness
							Activator.important(witness)
						} 
						case "-undermatch": {
							val witness = stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property "))
							this.resultHandler.details = witness
							Activator.important(witness)
						}
					}
					
				}
				case SAND: { 
					switch property {
						case "-overmatch": {
							switch stdOutString.charAt(index + 24) {
								case 'F'.charAt(0): {// First case 
									Activator.info("Counterexample: there is a path that does not start in the first subgoal's pre") 
									val witness = stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property "))
									this.resultHandler.details = witness
									Activator.important(witness)
								}
								case 'S'.charAt(0): {// Second case 
									Activator.info("Counterexample: there is a path that does not end in the last subgoal's post") 
									val witness = stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property "))
									this.resultHandler.details = witness
									Activator.important(witness)
								}
								case 'T'.charAt(0): {// Third case 
									Activator.info("Counterexample: there is a path that does not visit the junction after the " + stdOutString.substring(index + 44, stdOutString.indexOf(',',index + 44)) + "-th subgoal")
									val witness = stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property "))
									this.resultHandler.details = witness
									Activator.important(witness)
								} 
							}
						} 
						case "-undermatch": {
							val witness = stdOutString.substring(stdOutString.indexOf("A witness is:"),stdOutString.indexOf("Property "))
							this.resultHandler.details = witness
							Activator.important(witness)
						}
					}
				}
				case UNKNOWN: { // to avoid incomplete switch warning
				}
			}
			this.resultHandler.value = ResultValue.FALSE
			return ResultValue.FALSE;
		}
	}
	
	def buildCommandArguments() {
		// should build something like :
		// its-ctl -i building.gal -t GAL -formula form.ctl

		val cmd = new ArrayList<String>();
		cmd.add(getCorrectnessToolPath());
		cmd.add("-i");
		cmd.add(gal_file.getName());
		cmd.add("-t");
		cmd.add("GAL");
		cmd.add(property);
		cmd.add("-refinement");
		cmd.add(tree.PGoalString);
		cmd.add(tree.OPString);
		cmd.add(tree.subgoals.size.toString);
		cmd.addAll(tree.subgoalStrings);
		return cmd;
	}
	
	def String getCorrectnessToolPath() {
		val bundle = Platform.getBundle("fr.irisa.atsyra.atree.correctness");
		if(Activator.getOperatingSystemType() == ""){
			throw new RuntimeException("Operating system not supported");
		}
		var suffix = ""
		if(Activator.getOperatingSystemType() == "win") {suffix = ".exe"}
		val url = bundle.getResource("tool/"+ Activator.getOperatingSystemType() +"/RefinementsCorrectness"+suffix)
		if(url === null) return null;
		val fileUrl = FileLocator.toFileURL(url);
		return fileUrl.file
	}
	
	def getDefaultTimeout() {
		val store = fr.irisa.atsyra.ide.ui.Activator.getDefault().getPreferenceStore();
		val time = store.getInt(PreferenceConstants.P_MAX_EXEC_TIME)
		return time;
	}
}
