/*******************************************************************************
 * Copyright (c) 2014, 2019 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atree.correctness;

import java.util.Locale;

import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystemManager;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class Activator extends AbstractUIPlugin {
	
	public static final String PLUGIN_ID = "fr.irisa.atsyra.atree.correctness"; //$NON-NLS-1$
	private static Activator plugin;


	public void start(BundleContext bundleContext) throws Exception {
		super.start(bundleContext);
		plugin = this;
	}

	public void stop(BundleContext bundleContext) throws Exception {
		plugin = null;
		super.stop(bundleContext);
	}
	
	static public Activator getDefault() {
		return plugin;
	}

	public static void debug(String msg){
		//System.out.println(msg);
		getMessagingSystem().debug(msg, "ATSyRA");
	}
	public static void error(String msg, Exception e){
		getMessagingSystem().error(msg, "ATSyRA", e);
	}
	public static void error(String msg){
		getMessagingSystem().error(msg, "ATSyRA");
	}
	public static void info(String str) {
		getMessagingSystem().info(str, "ATSyRA");
	}
	public static void important(String str) {
		getMessagingSystem().important(str, "ATSyRA");
	}
	
	public static String msgGroup = "fr.irisa.atsyra";
	protected static MessagingSystem messagingSystem = null;
	public static MessagingSystem getMessagingSystem() {
		if(messagingSystem == null) {
			MessagingSystemManager msm = new MessagingSystemManager();
			messagingSystem = msm.createBestPlatformMessagingSystem(msgGroup, "ATSyRA");
		}
		return messagingSystem;
	}
	protected static String detectedOS;
	
	public static String getOperatingSystemType() {
	    if (detectedOS == null) {
	      String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
	      if ((OS.indexOf("mac") >= 0) || (OS.indexOf("darwin") >= 0)) {
	        detectedOS = "mac";
	      } else if (OS.indexOf("win") >= 0) {
	        detectedOS = "win";
	      } else if (OS.indexOf("nux") >= 0) {
	        detectedOS = "linux";
	      } else {
	        detectedOS = "";
	      }
	    }
	    return detectedOS;
	  }
	
}
