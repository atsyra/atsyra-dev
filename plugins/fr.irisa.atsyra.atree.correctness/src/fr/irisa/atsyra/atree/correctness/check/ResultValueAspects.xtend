package fr.irisa.atsyra.atree.correctness.check

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.resultstore.ResultValue

@Aspect (className=ResultValue)
class ResultValueAspects {
	
	def ResultValue negation() {
		switch _self {
			case FALSE: {
				ResultValue.TRUE
			}
			case TRUE: {
				ResultValue.FALSE
			}
			default: {
				_self
			}
			
		}
	}
}