package fr.irisa.atsyra.atree.correctness.check

import atsyragoal.AtsyraTree
import atsyragoal.AtsyragoalFactory
import fr.irisa.atsyra.atree.correctness.Activator
import fr.irisa.atsyra.atree.correctness.toolwrap.CorrectnessTool
import fr.irisa.atsyra.building.BuildingModel
import fr.irisa.atsyra.atsyra2.gal.GalCTL
import fr.irisa.atsyra.resultstore.Result
import fr.irisa.atsyra.resultstore.ResultValue
import fr.irisa.atsyra.transfo.atg.ctl.AbstractCTLAtsyraTreeAspects
import fr.irisa.atsyra.transfo.building.gal.Building2GALForCTL
import fr.lip6.move.gal.GALTypeDeclaration
import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IResource
import org.eclipse.core.runtime.IProgressMonitor

import static extension fr.irisa.atsyra.transfo.atg.gal.AtsyraTreeHelper.*

class CorrectnessChecker {
	
	IFile gal_file;
	IFile ctl_file;
	GALTypeDeclaration gal;
	BuildingModel building_model
	
	/**
	 * set the correctness parameters
	 * note that the ctlFile information may be unused if the property is checked using checkPropertyWithTool( however we keep it in order to get a proper name)
	 * 
	 */
	new(IFile galFile, IFile ctlFile, BuildingModel buildingModel) {
		ctl_file = ctlFile
		gal_file = galFile;
		building_model = buildingModel
	}
	
	def checkAdmissibility(AtsyraTree tree, IProgressMonitor monitor, Result resultHandler) {
		// To check admissibility, we replace the main goal of tree by (true,true) and check meet
		val admissibilityTree = createTreeForAdmissibility(tree)
		switch tree.operator {
			case AND: {
				checkPropertyWithTool(admissibilityTree, monitor, "-meet", resultHandler)
			}
			case OR, case SAND:{
				checkMeetCTL(admissibilityTree, monitor, resultHandler)
			}
			case UNKNOWN: {
				ResultValue.ABORTED
			}
		}
	}

	def checkMeet(AtsyraTree tree, IProgressMonitor monitor, Result resultHandler) {
//		switch tree.operator {
//			case AND: {
//				checkPropertyWithTool(tree, monitor, "-meet")
//			}
//			case OR, case SAND:{
//				checkMeetCTL(tree, monitor)
//			}
//			case UNKNOWN: {
//				ResultValue.ABORTED
//			}
//		}
		checkMeetCTL(tree, monitor, resultHandler)
		//checkPropertyWithTool(tree, monitor, "-meet")
		
	}
	
	def checkUndermatch(AtsyraTree tree, IProgressMonitor monitor, Result resultHandler) {
//		switch tree.operator {
//			case AND: {
//				checkPropertyWithTool(tree, monitor, "-undermatch")
//			}
//			case OR, case SAND:{
//				checkUndermatchCTL(tree, monitor)
//			}
//			case UNKNOWN: {
//				ResultValue.ABORTED
//			}
//		}
		checkUndermatchCTL(tree, monitor, resultHandler)
		//checkPropertyWithTool(tree, monitor, "-undermatch")
	}
	
	def checkOvermatch(AtsyraTree tree, IProgressMonitor monitor, Result resultHanlder) {
		switch tree.operator {
			case OR, case SAND, case AND: {
				checkPropertyWithTool(tree, monitor, "-overmatch", resultHanlder)
				//checkOvermatchCTL(tree, monitor)
			}
			case UNKNOWN: {
				ResultValue.ABORTED
			}
			}
	}
	
	def private AtsyraTree createTreeForAdmissibility(AtsyraTree source) {
		// create the necessary copies for our tree for admissibility
		// the true,true main goal
		val trivialGoal = AtsyragoalFactory.eINSTANCE.createAtsyraGoal
		// the "true" literal
		val trueLiteral = AtsyragoalFactory.eINSTANCE.createBooleanLiteral
		trueLiteral.name = "true"
		// We create two different copies of the "true" condition, because each can be used only once, either as pre or post
		val trivialGoalPre = AtsyragoalFactory.eINSTANCE.createBooleanSystemCondition
		trivialGoalPre.source = trueLiteral
		trivialGoal.precondition = trivialGoalPre
		val trivialGoalPost = AtsyragoalFactory.eINSTANCE.createBooleanSystemCondition
		trivialGoalPost.source = trueLiteral
		trivialGoal.postcondition = trivialGoalPost
		// the copy of the tree for Admissibility checking
		val admissibilityTree = AtsyragoalFactory.eINSTANCE.createAtsyraTree
		admissibilityTree.name = source.name
		admissibilityTree.mainGoal = trivialGoal
		admissibilityTree.operator = source.operator
		// We copy each of the children of the tree, to not modify the original tree
		val childrenCopy = newArrayOfSize(source.operands.size)
		for(var i = 0; i < childrenCopy.size; i++){
			val childCopy = AtsyragoalFactory.eINSTANCE.createAtsyraTree
			childCopy.name = source.operands.get(i).concreteTree.name
			childCopy.mainGoal = source.operands.get(i).concreteTree.mainGoal
			childCopy.operator = source.operands.get(i).concreteTree.operator
			childrenCopy.set(i,childCopy)
		}
		admissibilityTree.operands.addAll(childrenCopy)
		return admissibilityTree
	}
	
	def private checkMeetCTL(AtsyraTree tree, IProgressMonitor monitor, Result resultHandler) {
		//we create the gal file for CTL model checking
		val btg = new Building2GALForCTL();
		gal = btg.transformToGAL(building_model);
		btg.flattenGAL(gal);
		btg.writeToFile(gal, gal_file, monitor);
		//We create the string of the CTL formula
		val formula = AbstractCTLAtsyraTreeAspects.getMeetFormula(tree);
		//We create the file and put set its content to the formula
		val formulaFile = ctl_file;
		val stream = new ByteArrayInputStream(formula.getBytes(StandardCharsets.UTF_8));
		if(!formulaFile.exists()) {
			formulaFile.create(stream, true, monitor);
		}
		else {
			formulaFile.setContents(stream, true, false, monitor);
		}
		
		gal_file.getProject().refreshLocal(IResource.DEPTH_INFINITE, monitor);
		val ctlmc = new GalCTL(gal_file, formulaFile, resultHandler);
		val result = ctlmc.computeCTL(monitor)
		switch result {
			case ABORTED: {
				Activator.info("Verification of the Meet property was aborted");
			}
			case FALSE: {
				Activator.info("Meet property is false");
			}
			case NOT_RUN: {
				Activator.error("Verification of the Meet property was not run");
			}
			case TIMEOUT: {
				Activator.info("Verification of the Meet property timed out!");
			}
			case TRUE: {
				Activator.info("Meet property is true");
			}
		}
		return result
	}
	
	def private checkUndermatchCTL(AtsyraTree tree, IProgressMonitor monitor, Result resultHandler) {
		//we create the gal file for CTL model checking
		val btg = new Building2GALForCTL();
		gal = btg.transformToGAL(building_model);
		btg.flattenGAL(gal);
		btg.writeToFile(gal, gal_file, monitor);
		//We create the string of the CTL formula
		val formula = AbstractCTLAtsyraTreeAspects.getUnderMatchFormula(tree);
		//We create the file and put set its content to the formula
		val formulaFile = ctl_file;
		val stream = new ByteArrayInputStream(formula.getBytes(StandardCharsets.UTF_8));
		if(!formulaFile.exists()) {
			formulaFile.create(stream, true, monitor);
		}
		else {
			formulaFile.setContents(stream, true, false, monitor);
		}
		gal_file.getProject().refreshLocal(IResource.DEPTH_INFINITE, monitor);
		val ctlmc = new GalCTL(gal_file, formulaFile, resultHandler);
		val result = ctlmc.computeCTL(monitor)
		switch result {
			case ABORTED: {
				Activator.info("Verification of the Undermatch property was aborted");
			}
			case FALSE: {
				Activator.info("Undermatch property is false");
			}
			case NOT_RUN: {
				Activator.error("Verification of the Undermatch property was not run");
			}
			case TIMEOUT: {
				Activator.info("Verification of the Undermatch property timed out!");
			}
			case TRUE: {
				Activator.info("Undermatch property is true");
			}
		}
		return result
	}
	
	def private checkOvermatchCTL(AtsyraTree tree, IProgressMonitor monitor, Result resultHandler) {
		//we create the gal file for CTL model checking
		val btg = new Building2GALForCTL();
		gal = btg.transformToGAL(building_model);
		btg.flattenGAL(gal);
		btg.writeToFile(gal, gal_file, monitor);
		//We create the string of the CTL formula
		val formula = AbstractCTLAtsyraTreeAspects.getOverMatchFormula(tree);
		//We create the file and put set its content to the formula
		val formulaFile = ctl_file;
		val stream = new ByteArrayInputStream(formula.getBytes(StandardCharsets.UTF_8));
		if(!formulaFile.exists()) {
			formulaFile.create(stream, true, monitor);
		}
		else {
			formulaFile.setContents(stream, true, false, monitor);
		}
		gal_file.getProject().refreshLocal(IResource.DEPTH_INFINITE, monitor);
		val ctlmc = new GalCTL(gal_file, formulaFile, resultHandler);
		val result = ctlmc.computeCTL(monitor)
		switch result {
			case ABORTED: {
				Activator.info("Verification of the Overmatch property was aborted");
			}
			case FALSE: {
				Activator.info("Overmatch property is false");
			}
			case NOT_RUN: {
				Activator.error("Verification of the Overmatch property was not run");
			}
			case TIMEOUT: {
				Activator.info("Verification of the Overmatch property timed out!");
			}
			case TRUE: {
				Activator.info("Overmatch property is true");
			}
		}
		return result
	}
	
	def private checkPropertyWithTool(AtsyraTree tree, IProgressMonitor monitor, String prop, Result resultHanlder) {
		
		if(ctl_file.exists){
			// no need for ctl formula when using CorrectnessTool, do some cleanup
			ctl_file.delete(true, monitor);
		}
		//we create the gal file for CTL model checking
		val btg = new Building2GALForCTL();
		gal = btg.transformToGAL(building_model);
		btg.flattenGAL(gal);
		btg.writeToFile(gal, gal_file, monitor);
		
		gal_file.getProject().refreshLocal(IResource.DEPTH_INFINITE, monitor);
		
		val ctool = new CorrectnessTool(gal_file, tree, prop, resultHanlder)
		ctool.computeCorrectness(monitor)
	}
	

	
}