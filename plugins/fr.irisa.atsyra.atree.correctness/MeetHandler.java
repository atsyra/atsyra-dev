/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Optional;

import org.eclipse.capra.core.adapters.TraceMetaModelAdapter;
import org.eclipse.capra.core.adapters.TracePersistenceAdapter;
import org.eclipse.capra.core.helpers.ArtifactHelper;
import org.eclipse.capra.core.helpers.ExtensionPointHelper;
import org.eclipse.capra.core.helpers.TraceHelper;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraGoalModel;
import atsyragoal.AtsyraTree;
import atsyragoal.AtsyraTreeOperator;
import fr.irisa.atsyra.atree.correctness.check.CorrectnessChecker;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.helpers.AtsyraHelper;
import fr.irisa.atsyra.gal.GalCTL;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.TreeResult;
import fr.irisa.atsyra.resultstore.WitnessResult;
import fr.irisa.atsyra.transfo.atg.ctl.AbstractAtsyraTreeAspects;
//import fr.irisa.atsyra.transfo.atg.ctl.AbstractAtsyraTreeAspects;
import fr.irisa.atsyra.transfo.building.gal.Building2GALForCTL;
import fr.lip6.move.gal.GALTypeDeclaration;

public class MeetHandler extends AbstractAtsyraTreeSelectHandler {

	@Override
	public Object executeForSelectedAtsyraTree(ExecutionEvent event, IProject updatedGemocAtsyraTreeProject,
			AtsyraTree tree) throws ExecutionException {
		IFile atsyraTreeFile = getAtsyraTreeIFile(event, tree);
		if(tree.getOperator() == AtsyraTreeOperator.UNKNOWN){
			return null;
		}
		computeCorrectness(atsyraTreeFile, tree);
		return null;
	}	

	void computeCorrectness(IResource resource, AtsyraTree tree) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {
			final IFile file = (IFile) resource;
			try {
				Job job = new Job("Checking the meet property for the tree "+ tree.getName()+" from " + file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						try {
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, monitor);
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute on " + ProcessConstants.GENFOLDER_NAME,
									e);
						}

						// do the job
						Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
								.getInjector(
										fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
						XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class);
						resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
						// URI uri =
						// URI.createFileURI(file.getRawLocationURI().getPath())
						// ;
						URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
						Resource eresource = resourceSet.getResource(uri, true);
						IFile gal_file = getGalFile(file);
						BuildingModel buildingModel = getBuildingModelFromGoalModel(resourceSet, file, eresource);
						CorrectnessChecker checker = new CorrectnessChecker(resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME),gal_file,buildingModel);
						ResultValue ans = checker.checkMeet(tree, monitor);

						URI resultStoreUri = getResultStoreURI(file);
						TreeResult tr = getTreeResultHandler(tree, resultStoreUri);
						Result result = ResultstoreFactory.eINSTANCE.createResult();
						result.setTimestamp(new java.util.Date());
						result.setName("Meet of tree : "+tree.getName());
						result.setValue(ans);
						tr.setMeetResult(result);
						
						return Status.OK_STATUS;
					};
				};
				job.setPriority(Job.LONG);
				// use mutex to prevent concurrent job
				// job.setRule(Activator.getDefault().getBuilderMutex().createCombinedRule(new
				// IResource[]{resource.getProject().findMember("atsyra-gen")}));
				job.setRule(resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME));
				job.schedule();

			} catch (Exception e) {
				Activator.eclipseError(
						"model-checking from " + file.getName() + " raised an exception " + e.getMessage(), e);
			}

		}
	}

	/**
	 * Search for the IFile of the goal either via the event or directly from
	 * the eResource
	 */
	protected IFile getAtsyraTreeIFile(ExecutionEvent event, AtsyraTree tree) {
		IFile atsyraTreeFile = getAtsyraTreeFileFromSelection(event);
		if (atsyraTreeFile == null) {
			// this means that we have to retrieve the IFile from the tree
			// instance (either because
			// it comes from an editor of because we have selected one tree
			// among other in the project)
			atsyraTreeFile = EMFResource.getIFile(tree);
		}
		return atsyraTreeFile;
	}

	IFile getGalFile(IFile file) {
		// look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		IFile galFile = file.getProject()
				.getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension + "_meet.gal");
		return galFile;
	}

	IFile getFormulaFile(AtsyraGoalModel goalModel, AtsyraTree tree, IFile file, IProgressMonitor monitor) throws CoreException {
		if (goalModel.getTrees().isEmpty())
			throw new RuntimeException("No tree to parse");

		// We create the string of the CTL formula
		String formula = AbstractAtsyraTreeAspects.getMeetFormula(tree);
		// We create the file and put set its content to the formula
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFile formulaFile = genFolder.getFile(fileNameNoExtension+"_"+tree.getName()+"_meet_formula.ctl");
		InputStream stream = new ByteArrayInputStream(formula.getBytes(StandardCharsets.UTF_8));
		if (!formulaFile.exists()) {
			formulaFile.create(stream, true, monitor);
		} else {
			formulaFile.setContents(stream, true, false, monitor);
		}
		return formulaFile;

	}

	public BuildingModel getBuildingModelFromGoalModel(XtextResourceSet resourceSet, IFile file, Resource resource) {

		// first tries using Capra to retrieve the .building
		//
		TraceMetaModelAdapter traceAdapter = ExtensionPointHelper.getTraceMetamodelAdapter().get();
		TracePersistenceAdapter persistenceAdapter = ExtensionPointHelper.getTracePersistenceAdapter().get();

		// ResourceSet capraResourceSet = new ResourceSetImpl();
		// add trace model to resource set
		EObject traceModel = persistenceAdapter.getTraceModel(resourceSet);
		// add artifact model to resource set
		EObject artifactModel = persistenceAdapter.getArtifactWrappers(resourceSet);

		ArtifactHelper artifactHelper = new ArtifactHelper(artifactModel);
		TraceHelper traceHelper = new TraceHelper(traceModel);

		AtsyraGoalModel goalModel = (AtsyraGoalModel) resource.getContents().get(0);
		Optional<IFile> f = goalModel.getImports() // TODO should look into all
													// imports transitively
				.stream().map(AtsyraHelper::getGoalImportTargetIFile)
				.flatMap(ifile -> AtsyraHelper.getRelatedIFilesEndingsWith(ifile, ".building").stream()).findFirst();

		URI buildingUri = null;
		if (f.isPresent() && f.get().exists()) {
			// ask to Capra
			buildingUri = URI.createPlatformResourceURI(f.get().getFullPath().toString(), true);
		} else {

			// then tries path convention: same path, same name, ends in
			// .building instead of .atg
			// URI uri =
			// URI.createFileURI(file.getFullPath().toString().substring(0,
			// resource.getURI().lastSegment().lastIndexOf("."))+".building") ;
			buildingUri = URI.createFileURI(
					file.getRawLocationURI().getPath().substring(0, file.getRawLocationURI().getPath().lastIndexOf("."))
							+ ".building");

		}

		Resource buildingRes = resourceSet.getResource(buildingUri, true);
		return (BuildingModel) buildingRes.getContents().get(0);
	}

	@Override
	public String getSelectionMessage() {

		return "Generate GAL and check Meet property for the selected tree.";
	}

}
