/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/*
 * generated by Xtext 2.25.0
 */
package fr.irisa.atsyra.absystem.xtext.ui.tests;

import com.google.inject.Injector;
import fr.irisa.atsyra.absystem.xtext.ui.internal.XtextActivator;
import org.eclipse.xtext.testing.IInjectorProvider;

public class AssetBasedSystemDslUiInjectorProvider implements IInjectorProvider {

	@Override
	public Injector getInjector() {
		return XtextActivator.getInstance().getInjector("fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl");
	}

}
