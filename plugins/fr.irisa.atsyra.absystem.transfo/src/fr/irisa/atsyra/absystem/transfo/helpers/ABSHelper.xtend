package fr.irisa.atsyra.absystem.transfo.helpers

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory
import fr.irisa.atsyra.absystem.model.absystem.Expression
import java.util.Optional
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod

class ABSHelper {
	def static createTrueExpr() {
		val trueExpr = AbsystemFactory.eINSTANCE.createBooleanConstant
		trueExpr.value = "true"
		return trueExpr
	}
	
	def static Expression createNotUndefined(Expression expr) {
		val res = AbsystemFactory.eINSTANCE.createEqualityComparisonExpression
		res.lhs = expr
		res.rhs = AbsystemFactory.eINSTANCE.createUndefinedConstant
		res.op = "!="
		res
	}
	
	def static Expression createAnd(Expression lhs, Expression rhs) {
		val res = AbsystemFactory.eINSTANCE.createAndExpression
		res.lhs = lhs
		res.rhs = rhs
		res
	}
	
	def static Optional<Expression> createAnd(Optional<Expression> lhs, Optional<Expression> rhs) {
		if (lhs.empty) {
			return rhs
		} else if (rhs.empty) {
			return lhs
		} else {
			val res = AbsystemFactory.eINSTANCE.createAndExpression
			res.lhs = lhs.get
			res.rhs = rhs.get
			return Optional.of(res)
		}
	}
	
	def static Optional<Expression> createBigAnd(Iterable<Expression> operands) {
		Optional.ofNullable(operands.reduce([acc, t |
			createAnd(t, acc)
		]))
	}
	
	def static Expression createBigAnd(Expression seed, Iterable<Expression> operands) {
		operands.fold(seed, [acc, t |
			createAnd(t, acc)
		])
	}
	
	def static Expression createForallExpr(Expression receiver, LambdaExpression lambda, AssetBasedSystem abs) {
		// First, negate the lambda
		lambda.body = AbsystemFactory.eINSTANCE.createNotExpression => [expression = lambda.body]
		// Then, filter all elements that satisfies the negation of the lambda
		val filter = AbsystemFactory.eINSTANCE.createMemberSelection
		filter.receiver = receiver
		filter.member = getStaticMethod("filter", abs)
		filter.methodInvocation = true
		filter.args.add(lambda)
		// Finally, test that there is no such elements
		val isempty = AbsystemFactory.eINSTANCE.createMemberSelection
		isempty.receiver = filter
		isempty.member = getStaticMethod("isEmpty", abs)
		isempty.methodInvocation = true
		isempty
	}
	
	def static StaticMethod getStaticMethod(String name, AssetBasedSystem abs) {
		abs.eAllContents.filter(StaticMethod).findFirst[it.name == name] ?: createStaticMethod(name, abs)
	}
	
	def static StaticMethod createStaticMethod(String name, AssetBasedSystem abs) {
		if(abs.definitionGroups.empty) {
			val dg = AbsystemFactory.eINSTANCE.createDefinitionGroup
			dg.name = "null"
			abs.definitionGroups.add(dg)
		}
		val dg = abs.definitionGroups.get(0)
		val method = AbsystemFactory.eINSTANCE.createStaticMethod
		method.name = name
		dg.staticMethods.add(method)
		method
	}
	
	def static <T> Iterable<T> removeOption(Iterable<Optional<T>> iterable) {
		iterable.filter[!it.empty].map[get]
	}
}