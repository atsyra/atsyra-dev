/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.files;

import java.io.File;
import java.util.Optional;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

/**
 * An interface for a component in charge of providing access to files, to be
 * used as generated intermediate files. For a given id and extension, all
 * methods should return a reference to the same file.
 * 
 * @author Maxime Audinot
 *
 */
public interface GeneratedFilesHandler {
	/**
	 * Provides with a file for, uniquely determined by the pair (id, extension).
	 * The file should be of the given extension.
	 * 
	 * @param id        an identifier to the file
	 * @param extension an optional extension to the file
	 * @return A File handle, unique for each id and extension.
	 */
	File getFile(String id, Optional<String> extension);

	/**
	 * Provides with a resource for, uniquely determined by the pair (id,
	 * extension). The file should be of the given extension. The resource must
	 * point to the same file in the filesystem as the result of calling
	 * {@link GeneratedFilesHandler#getFile(String, Optional) getFile(id,
	 * extension)}
	 * 
	 * @param id          an identifier to the file
	 * @param extension   an optional extension to the file
	 * @param resourceSet the ResourceSet in which the resource should be opened
	 * @return A Resource, unique for each id and extension. The resource is opened
	 *         in the provided resourceSet.
	 */
	Resource getResource(String id, Optional<String> extension, ResourceSet resourceSet);
}
