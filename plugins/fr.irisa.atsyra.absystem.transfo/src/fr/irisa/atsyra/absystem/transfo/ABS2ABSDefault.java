/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Clock;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSSizeInfo;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.transfo.aspects.SlicingAssetBasedSystemAspect;
import fr.irisa.atsyra.absystem.transfo.aspects.UndefControlAssetBasedSystemAspect;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandler;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.irisa.atsyra.preferences.GlobalPreferenceService;
import fr.irisa.atsyra.preferences.constants.AtsyraPreferenceConstants;

public class ABS2ABSDefault implements ABS2ABS {
	static final Logger logger = LoggerFactory.getLogger(ABS2ABSDefault.class);
	static Clock clock = Clock.systemDefaultZone();

	@Override
	public ABS2ABSresult abs2abs(ABS2ABScontext context, TransfoTraceModel traceModel, IProgressMonitor monitor) throws CoreException, IOException {
		SubMonitor submonitor = SubMonitor.convert(monitor, "ABS2ABS", 100);
		EcoreUtil.resolveAll(context.absResourceSet);
		Set<AssetBasedSystem> absSet = ABSUtils.getAllAbsInResourceSet(context.absResourceSet);
		printInputModelSize(absSet, context.generatedFilesHandler);
		AssetBasedSystem slice = abs2absSlicing(absSet, context, traceModel, submonitor.split(50));
		if(monitor.isCanceled()) {
			traceModel.eResource().save(Collections.emptyMap());
			return new ABS2ABSresult(slice, traceModel);
		}
		AssetBasedSystem uc = abs2absUc(slice, context, traceModel, submonitor.split(50));
		traceModel.eResource().save(Collections.emptyMap());
		printOutputModelSize(uc, context.generatedFilesHandler);
		return new ABS2ABSresult(uc, traceModel);
	}

	public AssetBasedSystem abs2absSlicing(Iterable<AssetBasedSystem> absSet, ABS2ABScontext context, TransfoTraceModel traceModel, 
			IProgressMonitor monitor) {
		final Resource sliceFile = context.generatedFilesHandler.getResource("slice", Optional.of("xmi"), new ResourceSetImpl());
		return abs2absSlicing(absSet, context, traceModel, monitor, sliceFile);
	}

	public AssetBasedSystem abs2absSlicing(Iterable<AssetBasedSystem> absSet, ABS2ABScontext context, TransfoTraceModel traceModel, 
			IProgressMonitor monitor, Resource sliceFile) {
		monitor.beginTask("Slicing", 50);
		AssetBasedSystem slice = SlicingAssetBasedSystemAspect.getSliceForGoal(absSet.iterator().next(), absSet,
				context.goal, sliceFile, traceModel);
		return slice;
	}

	public AssetBasedSystem abs2absUc(AssetBasedSystem abs, ABS2ABScontext context, TransfoTraceModel traceModel, IProgressMonitor monitor) {
		final Resource ucFile = context.generatedFilesHandler.getResource("uc", Optional.of("xmi"), new ResourceSetImpl());
		return abs2absUc(abs, context, traceModel, monitor, ucFile);
	}

	public AssetBasedSystem abs2absUc(AssetBasedSystem abs, ABS2ABScontext context, TransfoTraceModel traceModel, IProgressMonitor monitor, 
			Resource ucFile) {
		monitor.beginTask("Undefined control", 50);
		AssetBasedSystem controlledForUndefined = UndefControlAssetBasedSystemAspect.getModelControlledForUndefined(abs,
				ucFile, traceModel);
		return controlledForUndefined;
	}
	
	private void printInputModelSize(Set<AssetBasedSystem> absSet, GeneratedFilesHandler generatedFilesHandler) throws IOException {
		Optional<String> preference = GlobalPreferenceService.INSTANCE.getString(AtsyraPreferenceConstants.P_QUALIFIER, AtsyraPreferenceConstants.P_PRINT_SIZE);
		if(preference.isPresent()) {
			if(preference.get().equals("file")) {
				ABSSizeInfo sizeInfo = ABSSizeInfo.getAbsModelSizeInfo(absSet);
				try (FileWriter fw = new FileWriter(generatedFilesHandler.getFile("modelSizes", Optional.of("log")))) {
					String message = String.format("Starting ABS2ABS with ABS model size %s\n", sizeInfo);
					logger.info(message);
					fw.append(message);
				}
			} else if(preference.get().equals("log") && logger.isInfoEnabled()) {
				ABSSizeInfo sizeInfo = ABSSizeInfo.getAbsModelSizeInfo(absSet);
				logger.info("Starting ABS2ABS with ABS model size {}", sizeInfo);
			}
		}
	}
	
	private void printOutputModelSize(AssetBasedSystem output, GeneratedFilesHandler generatedFilesHandler) throws IOException {
		Optional<String> preference = GlobalPreferenceService.INSTANCE.getString(AtsyraPreferenceConstants.P_QUALIFIER, AtsyraPreferenceConstants.P_PRINT_SIZE);
		if(preference.isPresent()) {
			if(preference.get().equals("file")) {
				ABSSizeInfo sizeInfo = ABSSizeInfo.getAbsModelSizeInfo(output);
				try (FileWriter fw = new FileWriter(generatedFilesHandler.getFile("modelSizes", Optional.of("log")), true)) {
					String message = String.format("After ABS2ABS, ABS model size %s\n", sizeInfo);
					logger.info(message);
					fw.append(message);
				}
			} else if(preference.get().equals("log") && logger.isInfoEnabled()) {
				ABSSizeInfo sizeInfo = ABSSizeInfo.getAbsModelSizeInfo(output);
				logger.info("After ABS2ABS, ABS model size {}", sizeInfo);
			}
		}
	}
}
