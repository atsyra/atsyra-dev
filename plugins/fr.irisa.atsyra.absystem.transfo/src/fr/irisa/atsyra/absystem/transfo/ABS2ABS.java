/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo;

import java.io.IOException;
import java.time.Instant;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.resource.ResourceSet;

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandler;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;

public interface ABS2ABS {
	public static class ABS2ABScontext {
		final Goal goal;
		final ResourceSet absResourceSet;
		final Instant start;
		final GeneratedFilesHandler generatedFilesHandler;

		public ABS2ABScontext(GeneratedFilesHandler generatedFilesHandler, Goal goal, Instant start, ResourceSet absResourceSet) {
			this.generatedFilesHandler = generatedFilesHandler;
			this.goal = goal;
			this.absResourceSet = absResourceSet;
			this.start = start;
		}
	}

	public static class ABS2ABSresult {
		public final AssetBasedSystem model;
		public final TransfoTraceModel traceModel;

		public ABS2ABSresult(AssetBasedSystem model, TransfoTraceModel traceModel) {
			this.model = model;
			this.traceModel = traceModel;
		}
	}

	public ABS2ABSresult abs2abs(ABS2ABScontext context, TransfoTraceModel traceModel, IProgressMonitor monitor) throws CoreException, IOException;
}
