/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo;

import java.io.IOException;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.transfo.trace.MergeTraceBuilder;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;

public class Transformation {
	Iterable<AssetBasedSystem> source;
	AssetBasedSystem transformed;
	BiMap<EObject, EObject> source2transformedMap;

	public static Transformation merge(Iterable<AssetBasedSystem> absset, Resource mergefile) throws IOException {
		Transformation result = new Transformation();
		result.source = absset;
		result.transformed = AbsystemFactory.eINSTANCE.createAssetBasedSystem();
		// Copy the structure
		Copier cp = new EcoreUtil.Copier();
		for (AssetBasedSystem abs : absset) {
			result.transformed.getDefinitionGroups().addAll(cp.copyAll(abs.getDefinitionGroups()));
			result.transformed.getAssetGroups().addAll(cp.copyAll(abs.getAssetGroups()));
		}
		// Update the references
		cp.copyReferences();
		result.source2transformedMap = HashBiMap.create(cp);
		// add to the resource and serialize
		mergefile.getContents().clear();
		mergefile.getContents().add(result.transformed);
		mergefile.save(null);
		return result;
	}
	
	public static Transformation copy(AssetBasedSystem abs, Resource copyfile) throws IOException {
		return merge(List.of(abs), copyfile);
	}

	public AssetBasedSystem getTransformedModel() {
		return transformed;
	}

	public EObject getTransformed(EObject source) {
		return source2transformedMap.get(source);
	}

	public EObject getReverted(EObject source) {
		return source2transformedMap.inverse().get(source);
	}

	public TransfoTraceModel updateTraceModel(TransfoTraceModel traceModel) {
		MergeTraceBuilder builder = new MergeTraceBuilder(traceModel);
		builder.createAssetBasedSystemLink(source, transformed);
		builder.fromMap(source2transformedMap);
		return builder.getTraceModel();
	}

}
