package fr.irisa.atsyra.absystem.transfo.aspects

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory
import fr.irisa.atsyra.absystem.model.absystem.Action
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature
import fr.irisa.atsyra.absystem.model.absystem.BinaryExpression
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression
import fr.irisa.atsyra.absystem.model.absystem.Expression
import fr.irisa.atsyra.absystem.model.absystem.Goal
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction
import fr.irisa.atsyra.absystem.model.absystem.LambdaAction
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity
import fr.irisa.atsyra.absystem.model.absystem.NotExpression
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef
import fr.irisa.atsyra.absystem.transfo.Transformation
import fr.irisa.atsyra.absystem.transfo.helpers.ABSHelper
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel
import java.util.List
import java.util.Optional
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil

import static extension fr.irisa.atsyra.absystem.transfo.aspects.UndefControlActionAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.UndefControlExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.UndefControlGoalAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.UndefControlGuardedActionAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.UndefControlLambdaActionAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.UndefControlLambdaExpressionAspect.*
import fr.irisa.atsyra.absystem.model.absystem.Collection

class UndefControlAssetBasedSystemAspect {

	/**
	 * Merges the models and controls the merged copy for undefined
	 */
	static def AssetBasedSystem getModelControlledForUndefined(Iterable<AssetBasedSystem> allabs, Resource mergefile,
		TransfoTraceModel traceModel) {
		val transfo = Transformation.merge(allabs, mergefile)
		val result = transfo.transformedModel
		transfo.updateTraceModel(traceModel);
		result.controlForUndefined()
		mergefile.save(null);
		traceModel.eResource.save(null)
		return result
	}

	/**
	 * Copies the model and controls the copy for undefined
	 */
	static def AssetBasedSystem getModelControlledForUndefined(AssetBasedSystem _self, Resource copyfile,
		TransfoTraceModel traceModel) {
		getModelControlledForUndefined(List.of(_self), copyfile, traceModel)
	}

	/**
	 * Controls all {@code GuardedAction} and {@code Goal} for undefined
	 */
	static def void controlForUndefined(AssetBasedSystem _self) {
		_self.eAllContents.filter(GuardedAction).forEach[controlForUndefined()]
		_self.eAllContents.filter(Goal).forEach[controlForUndefined()]
	}
}

class UndefControlGuardedActionAspect {
	/**
	 * Controls the {@code GuardedAction} for undefined by adding controls to its precondition
	 */
	static def void controlForUndefined(GuardedAction _self) {
		val toControlFromActions = _self.guardActions.flatMap[getExpressionControls].toList
		_self.guardExpression = _self.guardExpression.withControl(toControlFromActions)
	}
}

class UndefControlGoalAspect {
	/**
	 * Controls the {@code Goal} for undefined by adding controls to its precondition
	 */
	static def void controlForUndefined(Goal _self) {
		_self.precondition = _self.precondition.withControl
	}
}

class UndefControlActionAspect {
	/**
	 * Returns the list of {@code Expression} (with possible duplicates) that controls for undefined in the {@code Action}
	 */
	static def List<Expression> getExpressionControls(Action _self) {
		val result = newArrayList
		val argcontrol = switch _self.actionType {
			case FOR_ALL: {
				val lambdaControls = _self.lambdaAction.lambdaControls
				lambdaControls.map [
					getExpressionControlFromLambdaControl(EcoreUtil.copy(_self.target),
						EcoreUtil.getRootContainer(_self) as AssetBasedSystem)
				]
			}
			default: {
				_self.args.flatMap[getExpressionControls(true)]
			}
		}
		result.addAll(argcontrol)
		val targercontrol = _self.target.getExpressionControls(false)
		result.addAll(targercontrol)
		result
	}
}

class UndefControlLambdaActionAspect {
	/**
	 * Returns a list of {@code LambdaExpression} (with possible duplicates) that controls the {@code LambdaAction} for undefined.
	 * {@code LambdaExpression} controls should be transformed into {@code Expression} controls via {@link UndefControlLambdaExpressionAspect#getExpressionControlFromLambdaControl(LambdaExpression, Expression, AssetBasedSystem) getExpressionControlFromLambdaControl}
	 */
	static def List<LambdaExpression> getLambdaControls(LambdaAction _self) {
		val controls = _self.actions.flatMap[expressionControls].toList
		controls.map [
			val res = AbsystemFactory.eINSTANCE.createLambdaExpression
			res.lambdaParameter = EcoreUtil.copy(_self.lambdaParameter)
			res.body = it
			res.body.eAllContents.filter(SymbolRef).filter[symbol == _self.lambdaParameter].forEach [
				symbol = res.lambdaParameter
			]
			res
		]
	}
}

abstract class UndefControlExpressionAspect {
	/**
	 * Returns the list of {@code Expression} (with possible duplicates) that controls the {@code Expression} for undefined.
	 */
	static def dispatch List<Expression> getExpressionControls(Expression _self, boolean shouldControlSelf) {
	}

	/**
	 * Returns the list of {@code Expression} (with possible duplicates) that controls the {@code BinaryExpression} for undefined.
	 */
	static def dispatch List<Expression> getExpressionControls(BinaryExpression _self, boolean shouldControlSelf) {
		val result = newArrayList
		result.addAll(_self.lhs.getExpressionControls(shouldControlSelf))
		result.addAll(_self.rhs.getExpressionControls(shouldControlSelf))
		result
	}

	/**
	 * Returns the list of {@code Expression} (with possible duplicates) that controls the {@code NotExpression} for undefined.
	 */
	static def dispatch List<Expression> getExpressionControls(NotExpression _self, boolean shouldControlSelf) {
		_self.expression.getExpressionControls(shouldControlSelf)
	}

	/**
	 * Returns the list of {@code Expression} (with possible duplicates) that controls the {@code ConstantExpression} for undefined.
	 */
	static def dispatch List<Expression> getExpressionControls(ConstantExpression _self, boolean shouldControlSelf) {
		List.of
	}

	/**
	 * Returns the list of {@code Expression} (with possible duplicates) that controls the {@code MemberSelection} for undefined.
	 */
	static def dispatch List<Expression> getExpressionControls(MemberSelection _self, boolean shouldControlSelf) {
		val result = newArrayList
		result.addAll(_self.receiver.getExpressionControls(true))
		if (_self.member instanceof StaticMethod && _self.member.name == "filter") {
			val lambdaControls = (_self.args.get(0) as LambdaExpression).lambdaControls
			val expressionsToControl = lambdaControls.map [
				getExpressionControlFromLambdaControl(EcoreUtil.copy(_self.receiver),
					EcoreUtil.getRootContainer(_self) as AssetBasedSystem)
			]
			result.addAll(expressionsToControl)
		} else {
			_self.args.forEach[result.addAll(getExpressionControls(true))]
		}
		if (shouldControlSelf && _self.member instanceof AssetTypeFeature &&
			(_self.member as AssetTypeFeature).multiplicity == Multiplicity.ZERO_OR_ONE) {
			result.add(ABSHelper.createNotUndefined(EcoreUtil.copy(_self)))
		}
		result
	}

	/**
	 * Returns the list of {@code Expression} (with possible duplicates) that controls the {@code SymbolRef} for undefined.
	 */
	static def dispatch List<Expression> getExpressionControls(SymbolRef _self, boolean shouldControlSelf) {
		List.of
	}
	
	/**
	 * Returns the list of {@code Expression} (with possible duplicates) that controls the {@code Collection} for undefined.
	 */
	static def dispatch List<Expression> getExpressionControls(Collection _self, boolean shouldControlSelf) {
		_self.elements.flatMap[getExpressionControls(true)].toList
	}
	
	

	/**
	 * Returns a new {@code Expression} that is the conjunction of itself and its controls for undefined
	 */
	static def Expression withControl(Expression _self) {
		_self.withControl(List.of)
	}

	/**
	 * Returns a new expression that is the conjunction of the{@code Expression} itself, its controls for undefined, and additional controls for undefined from additionalControls
	 */
	static def Expression withControl(Expression _self, List<Expression> additionalControls) {
		val expressionControlsFromSelf = _self.getExpressionControls(false);
		val List<Expression> uniqueToControl = newArrayList
		for (expr : expressionControlsFromSelf + additionalControls) {
			if (!uniqueToControl.exists[EcoreUtil.equals(it, expr)]) {
				uniqueToControl.add(expr)
			}
		}
		val allControl = uniqueToControl.stream.reduce [ l, r |
			ABSHelper.createAnd(l, r)
		]
		ABSHelper.createAnd(allControl, Optional.of(_self)).orElseThrow
	}
}

class UndefControlLambdaExpressionAspect {
	/**
	 * Returns a list of {@code LambdaExpression} (with possible duplicates) that controls the {@code LambdaExpression} for undefined.
	 * {@code LambdaExpression} controls should be transformed into {@code Expression} controls via {@link UndefControlLambdaExpressionAspect#getExpressionControlFromLambdaControl(LambdaExpression, Expression, AssetBasedSystem) getExpressionControlFromLambdaControl}
	 */
	static def List<LambdaExpression> getLambdaControls(LambdaExpression _self) {
		val controls = _self.body.getExpressionControls(false)
		controls.map [
			val result = AbsystemFactory.eINSTANCE.createLambdaExpression
			result.lambdaParameter = EcoreUtil.copy(_self.lambdaParameter)
			result.body = it
			result.body.eAllContents.filter(SymbolRef).filter[symbol == _self.lambdaParameter].forEach [
				symbol = result.lambdaParameter
			]
			result
		]
	}

	/**
	 * Transforms {@code LambdaExpression} controls into {@code Expression} controls by applying the control to all elements of receiver.
	 */
	static def Expression getExpressionControlFromLambdaControl(LambdaExpression _self, Expression receiver,
		AssetBasedSystem modelContainingStaticMethods) {
		ABSHelper.createForallExpr(EcoreUtil.copy(receiver), _self, modelContainingStaticMethods)
	}
}
