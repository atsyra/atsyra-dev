package fr.irisa.atsyra.absystem.transfo.aspects

import fr.irisa.atsyra.absystem.model.absystem.Action
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.AssetLink
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature
import fr.irisa.atsyra.absystem.model.absystem.BinaryExpression
import fr.irisa.atsyra.absystem.model.absystem.Collection
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression
import fr.irisa.atsyra.absystem.model.absystem.Contract
import fr.irisa.atsyra.absystem.model.absystem.Expression
import fr.irisa.atsyra.absystem.model.absystem.Goal
import fr.irisa.atsyra.absystem.model.absystem.Guard
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction
import fr.irisa.atsyra.absystem.model.absystem.LambdaAction
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection
import fr.irisa.atsyra.absystem.model.absystem.NotExpression
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef
import fr.irisa.atsyra.absystem.transfo.Transformation
import fr.irisa.atsyra.absystem.transfo.helpers.ABSHelper
import fr.irisa.atsyra.absystem.transfo.trace.SlicingTraceBuilder
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel
import java.util.List
import java.util.Set
import java.util.function.Consumer
import java.util.stream.Stream
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource

import static extension fr.irisa.atsyra.absystem.transfo.aspects.SlicingActionAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.SlicingEObjectAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.SlicingExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.SlicingGoalAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.SlicingGuardAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.SlicingGuardedActionAspect.*
import static extension fr.irisa.atsyra.absystem.transfo.aspects.SlicingLambdaActionAspect.*
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference
import java.util.HashSet

class SlicingAssetBasedSystemAspect {
	
	static def AssetBasedSystem getSliceForGoal(AssetBasedSystem _self, Iterable<AssetBasedSystem> allabs, Goal goal, Resource mergefile, TransfoTraceModel traceModel) {
		val transfo = Transformation.merge(allabs, mergefile)
		val result = transfo.transformedModel
		transfo.updateTraceModel(traceModel);
		val transformedGoal = transfo.getTransformed(goal) as Goal
		result.sliceForGoal(transformedGoal, traceModel)
		mergefile.save(null)
		traceModel.eResource.save(null)
		return result
	}
	
	static def AssetBasedSystem getSliceForGoal(AssetBasedSystem _self, Goal goal, Resource copyfile, TransfoTraceModel traceModel) {
		_self.getSliceForGoal(List.of(_self), goal, copyfile, traceModel)
	}
	
	static def void sliceForGoal(AssetBasedSystem _self, Goal goal, TransfoTraceModel traceModel) {
		val usefulVariables = newHashSet
		val usefulActions = newHashSet
		val newusefulVariables = newHashSet
		val newusefulActions = newHashSet
		val goalVariables = goal.getPostVariables()
		newusefulVariables.addAll(goalVariables)
		usefulVariables.addAll(goalVariables)
		val dynamicContractsVariables = getDynamicContractsVariables(_self)
		newusefulVariables.addAll(dynamicContractsVariables)
		usefulVariables.addAll(dynamicContractsVariables)
		while(!newusefulVariables.empty) {
			for(vari : newusefulVariables) {
				_self.foreachGuardedAction[
					if(it.getWriteVariables().contains(vari) && !usefulActions.contains(it)) {
						newusefulActions.add(it)
						usefulActions.add(it)
					}
				]
			}
			newusefulVariables.clear
			for(act : newusefulActions) {
				Stream.concat(act.getControlVariables().stream, act.getReadVariables().stream).filter[!usefulVariables.contains(it)].forEach[
					newusefulVariables.add(it)
					usefulVariables.add(it)
				]
			}
			newusefulActions.clear
		}
		_self.sliceForUsefullness(goal, new SlicingTraceBuilder(traceModel), usefulVariables, usefulActions)
	}
	
	private static def getDynamicContractsVariables(AssetBasedSystem _self) {
		_self.eResource.resourceSet.allContents.filter(Contract).filter[dynamic].flatMap[guardExpression.controlVariables.iterator].toSet
	}
	
	private static def void sliceForUsefullness(AssetBasedSystem _self, Goal goal, SlicingTraceBuilder traceBuilder, Set<AssetTypeFeature> usefulVariables, Set<GuardedAction> usefulActions) {
		_self.eAllContents.filter(GuardedAction).filter[!usefulActions.contains(it)].toList.forEach[delete(traceBuilder)]
		val uselessVariables = _self.complement(usefulVariables)
		usefulActions.forEach[removeVariablesRef(uselessVariables, traceBuilder)]
		_self.eAllContents.filter(Contract).forEach[removeVariablesRef(uselessVariables, traceBuilder)]
		_self.eAllContents.filter(Goal).filter[it.name != goal.name].toList.forEach[delete(traceBuilder)]
		goal.precondition.removeVariablesRef(uselessVariables, traceBuilder)
		_self.eAllContents.filter(AssetAttributeValue).filter[!usefulVariables.contains(it.attributeType)].toList.forEach[delete(traceBuilder)]
		_self.eAllContents.filter(AssetLink).filter[!usefulVariables.contains(it.referenceType)].toList.forEach[delete(traceBuilder)]
		_self.eAllContents.filter(AssetTypeFeature).filter[!usefulVariables.contains(it)].toList.forEach[delete(traceBuilder)]
	}
	
	private static def void foreachGuardedAction(AssetBasedSystem _self, Consumer<GuardedAction> consumer) {
		_self.eAllContents.filter(GuardedAction).forEachRemaining(consumer)
	}
	
	static def Set<AssetTypeFeature> complement(AssetBasedSystem _self, Set<AssetTypeFeature> variables) {
		_self.eAllContents.filter(AssetTypeFeature).filter[!variables.contains(it)].toSet
	}
}

class SlicingEObjectAspect {
	static def void delete(EObject _self, SlicingTraceBuilder traceBuilder) {
		(_self.eContainer.eGet(_self.eContainingFeature) as EList<?>).remove(_self)
		traceBuilder.removeAllLinksWithContents(_self)
	}
}

abstract class SlicingGuardAspect {
	static def Set<AssetTypeFeature> getControlVariables(Guard _self) {
		_self.guardExpression.controlVariables
	}
	/**
	 * recursively removes references to useless variables. 
	 */
	static def void removeVariablesRef(Guard _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		val keepGuard = _self.guardExpression.removeVariablesRef(uselessVariables, traceBuilder)
		if(!keepGuard) {
			traceBuilder.removeAllLinksWithContents(_self.guardExpression)
			_self.guardExpression = ABSHelper.createTrueExpr
		}
	}
}

class SlicingGuardedActionAspect extends SlicingGuardAspect {
	static def Set<AssetTypeFeature> getReadVariables(GuardedAction _self) {
		val result = newHashSet
		_self.guardActions.forEach[result.addAll(it.getReadVariables())]
		result
	}
	static def Set<AssetTypeFeature> getWriteVariables(GuardedAction _self) {
		val result = newHashSet
		_self.guardActions.forEach[result.addAll(it.getWriteVariables())]
		result
	}
	/**
	 * recursively removes references to useless variables. 
	 */
	static def void removeVariablesRef(GuardedAction _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		SlicingGuardAspect::removeVariablesRef(_self, uselessVariables, traceBuilder)
		for(val ite = _self.guardActions.iterator; ite.hasNext;) {
			val act = ite.next
			val success = act.removeVariablesRef(uselessVariables, traceBuilder)
			if(!success) {
				traceBuilder.removeAllLinksWithContents(act)
				ite.remove
			}
		}
	}
}

class SlicingGoalAspect {
	static def Set<AssetTypeFeature> getPostVariables(Goal _self) {
		_self.postcondition.getControlVariables()
	}
	
	static def Set<AssetTypeFeature> getPreVariables(Goal _self) {
		_self.precondition.getControlVariables()
	}
}

class SlicingActionAspect {
	static def Set<AssetTypeFeature> getReadVariables(Action _self) {
		val result = newHashSet
		switch _self.actionType {
			case ADD, case REMOVE, case ASSIGN: {
				result.addAll(_self.args.get(0).controlVariables)
			}
			case ADD_ALL, case REMOVE_ALL:{
				result.addAll(_self.args.flatMap[controlVariables])
			}
			case CLEAR: {
			}
			case FOR_ALL: {
				result.addAll(_self.target.controlVariables)
				result.addAll(_self.lambdaAction.readVariables)
			}
		}
		return result
	}
	static def Set<AssetTypeFeature> getWriteVariables(Action _self) {
		val result = newHashSet
		switch _self.actionType {
			case ADD, case ADD_ALL, case REMOVE, case REMOVE_ALL, case ASSIGN, case CLEAR: {
				result.addAll(_self.target.controlVariables)
			}
			case FOR_ALL: {
				result.addAll(_self.lambdaAction.writeVariables)
			}
		}
		return result
	}
	/**
	 * recursively removes references to useless variables. 
	 * @returns true if the action no longer references useless variables, false if the action should be removed from its container
	 */
	static def boolean removeVariablesRef(Action _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		switch _self.actionType {
			case ADD, case ADD_ALL, case REMOVE, case REMOVE_ALL, case ASSIGN: {
				_self.target.removeVariablesRef(uselessVariables, traceBuilder) && _self.args.get(0).removeVariablesRef(uselessVariables, traceBuilder)
			} 
			case CLEAR: {
				_self.target.removeVariablesRef(uselessVariables, traceBuilder)
			}
			case FOR_ALL: {
				_self.target.removeVariablesRef(uselessVariables, traceBuilder) && _self.lambdaAction.removeVariablesRef(uselessVariables, traceBuilder)
			}
		}
	}
}

class SlicingLambdaActionAspect {
	static def Set<AssetTypeFeature> getReadVariables(LambdaAction _self) {
		val result = newHashSet
		result.addAll(_self.actions.flatMap[readVariables])
		return result
	}
	static def Set<AssetTypeFeature> getWriteVariables(LambdaAction _self) {
		val result = newHashSet
		result.addAll(_self.actions.flatMap[writeVariables])
		return result
	}
	/**
	 * recursively removes references to useless variables. 
	 * @returns true if the action no longer references useless variables, false if the action should be removed from its container
	 */
	static def boolean removeVariablesRef(LambdaAction _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		for(val ite = _self.actions.iterator; ite.hasNext;) {
			val act = ite.next
			val success = act.removeVariablesRef(uselessVariables, traceBuilder)
			if(!success) {
				traceBuilder.removeAllLinksWithContents(act)
				ite.remove
			}
		}
		return !_self.actions.empty
	}
}

abstract class SlicingExpressionAspect {
	static def dispatch Set<AssetTypeFeature> getControlVariables(Expression _self) {
	}
	static def dispatch Set<AssetTypeFeature> getControlVariables(BinaryExpression _self) {
		val result = newHashSet
		result.addAll(_self.lhs.controlVariables)
		result.addAll(_self.rhs.controlVariables)
		return result
	}
	static def dispatch Set<AssetTypeFeature> getControlVariables(NotExpression _self) {
		_self.expression.controlVariables
	}
	static def dispatch Set<AssetTypeFeature> getControlVariables(ConstantExpression _self) {
		return newHashSet
	}
	static def dispatch Set<AssetTypeFeature> getControlVariables(MemberSelection _self) {
		val result = newHashSet
		result.addAll(_self.receiver.controlVariables)
		if(_self.isMethodInvocation) {
			result.addAll(_self.args.flatMap[controlVariables])
		} else {
			result.add(_self.member as AssetTypeFeature)
			if(_self.member instanceof AssetTypeReference) {
				val ref = _self.member as AssetTypeReference
				if(ref.oppositeTypeReference !== null) {
					result.add(ref.oppositeTypeReference)
				}
			}
		}
		return result
	}
	static def dispatch Set<AssetTypeFeature> getControlVariables(SymbolRef _self) {
		return newHashSet
	}
	static def dispatch Set<AssetTypeFeature> getControlVariables(LambdaExpression _self) {
		_self.body.controlVariables
	}
	static def dispatch Set<AssetTypeFeature> getControlVariables(Collection _self) {
		val result = newHashSet
		result.addAll(_self.elements.flatMap[controlVariables])
		result
	}
	
	/**
	 * recursively removes references to useless variables. 
	 * @returns true if the expression no longer references useless variables, false if the expression should be removed from its container
	 */
	static def dispatch boolean removeVariablesRef(Expression _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		false
	}
	static def dispatch boolean removeVariablesRef(BinaryExpression _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		val lhsclear = _self.lhs.removeVariablesRef(uselessVariables, traceBuilder)
		if(!lhsclear) {
			traceBuilder.removeAllLinksWithContents(_self.lhs)
			_self.lhs = ABSHelper.createTrueExpr
		}
		val rhsclear = _self.rhs.removeVariablesRef(uselessVariables, traceBuilder)
		if(!rhsclear) {
			traceBuilder.removeAllLinksWithContents(_self.rhs)
			_self.rhs = ABSHelper.createTrueExpr
		}
		rhsclear || lhsclear
	}
	static def dispatch boolean removeVariablesRef(NotExpression _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		_self.expression.removeVariablesRef(uselessVariables, traceBuilder)
	}
	static def dispatch boolean removeVariablesRef(ConstantExpression _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		true
	}
	static def dispatch boolean removeVariablesRef(MemberSelection _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		if(_self.member instanceof AssetTypeFeature && uselessVariables.contains(_self.member)) {
			return false
		} else {
			if (!_self.receiver.removeVariablesRef(uselessVariables, traceBuilder)) {
				return false
			} else {
				if (_self.args.empty) {
					return true
				} else {
					for (val ite = _self.args.iterator; ite.hasNext;) {
						val act = ite.next
						val success = act.removeVariablesRef(uselessVariables, traceBuilder)
						if (!success) {
							traceBuilder.removeAllLinksWithContents(act)
							ite.remove
						}
					}
					return !_self.args.empty
				}
			}
		}
	}
	static def dispatch boolean removeVariablesRef(SymbolRef _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		true
	}
	static def dispatch boolean removeVariablesRef(LambdaExpression _self, Set<AssetTypeFeature> uselessVariables, SlicingTraceBuilder traceBuilder) {
		_self.body.removeVariablesRef(uselessVariables, traceBuilder)
	}
	
	static def dispatch boolean removeVariablesRef(Collection _self, Set<AssetTypeFeature> uselessVariables,
		SlicingTraceBuilder traceBuilder) {
		for (val ite = _self.elements.iterator; ite.hasNext;) {
			val elem = ite.next
			val success = elem.removeVariablesRef(uselessVariables, traceBuilder)
			if (!success) {
				traceBuilder.removeAllLinksWithContents(elem)
				ite.remove
			}
		}
		return !_self.elements.empty
	}

}