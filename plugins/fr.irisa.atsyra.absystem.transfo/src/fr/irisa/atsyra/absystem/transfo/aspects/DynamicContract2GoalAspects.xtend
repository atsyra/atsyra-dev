package fr.irisa.atsyra.absystem.transfo.aspects

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.Contract
import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory
import fr.irisa.atsyra.absystem.model.absystem.Goal
import org.eclipse.emf.ecore.util.EcoreUtil

class DynamicContract2GoalAspects {
	public static final String DYNAMIC_CONTRACT_GOAL_GROUP = "DynamicContractsGoalGroup"
	
	static def Goal generateGoalFromDynamicContract(Contract contract, AssetBasedSystem rootContainer) {
		if(!contract.isDynamic) {
			throw new IllegalArgumentException()
		}
		val goal = toGoal(contract);
		var group = rootContainer.assetGroups.findFirst[name == DYNAMIC_CONTRACT_GOAL_GROUP]
		if(group === null) {
			group = AbsystemFactory.eINSTANCE.createAssetGroup
			rootContainer.assetGroups.add(group)
		}
		val alreadyPresent = group.elements.filter(Goal).findFirst[it.name==goal.name]
		if(alreadyPresent !== null) {
			group.elements.remove(alreadyPresent)
		}
		group.elements.add(goal);
		goal
	}
	
	static private def Goal toGoal(Contract contract) {
		val result = AbsystemFactory.eINSTANCE.createGoal()
		result.name = contract.name
		result.precondition = EcoreUtil.copy(contract.guardExpression)
		result.postcondition = AbsystemFactory.eINSTANCE.createNotExpression => [expression = EcoreUtil.copy(contract.guardExpression)]
		result
	}
}