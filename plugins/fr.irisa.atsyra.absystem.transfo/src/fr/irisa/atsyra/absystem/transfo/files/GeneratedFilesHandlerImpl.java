/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.files;

import java.io.File;
import java.util.Optional;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

public class GeneratedFilesHandlerImpl implements GeneratedFilesHandler {
	private IFolder genFolder;
	private String prefix;

	public GeneratedFilesHandlerImpl(IFolder genFolder, String prefix) {
		this.genFolder = genFolder;
		this.prefix = prefix;
	}
	
	public IFile getIFile(String id, Optional<String> extension) {
		StringBuilder fileNameBuilder = new StringBuilder(prefix);
		fileNameBuilder.append(id);
		if(extension.isPresent()) {
			fileNameBuilder.append('.').append(extension.orElseThrow());
		}
		return genFolder.getFile(fileNameBuilder.toString());
	}

	@Override
	public Resource getResource(String id, Optional<String> extension, ResourceSet resourceSet) {
		IFile ifile = getIFile(id, extension);
		String projectRelativePath = ifile.getFullPath().toOSString();
		Resource resource = resourceSet.getResource(URI.createPlatformResourceURI(projectRelativePath, true), false);
		if(resource == null) {
			resource = resourceSet.createResource(URI.createPlatformResourceURI(projectRelativePath, true));
		}
		return resource;
	}

	@Override
	public File getFile(String id, Optional<String> extension) {
		IFile ifile = getIFile(id, extension);
		return new File(ifile.getLocation().toOSString());
	}

}
