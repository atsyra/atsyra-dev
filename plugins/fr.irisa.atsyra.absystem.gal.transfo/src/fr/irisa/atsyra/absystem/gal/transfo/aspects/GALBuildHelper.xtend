package fr.irisa.atsyra.absystem.gal.transfo.aspects

import fr.lip6.move.gal.Abort
import fr.lip6.move.gal.ArrayPrefix
import fr.lip6.move.gal.BooleanExpression
import fr.lip6.move.gal.ComparisonOperators
import fr.lip6.move.gal.GalFactory
import fr.lip6.move.gal.IntExpression
import fr.lip6.move.gal.Label
import fr.lip6.move.gal.ParamRef
import fr.lip6.move.gal.Parameter
import fr.lip6.move.gal.Statement
import fr.lip6.move.gal.Transition
import fr.lip6.move.gal.TypedefDeclaration
import fr.lip6.move.gal.Variable
import fr.lip6.move.gal.VariableReference
import java.util.List
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetTypeAspect.*

final class GALBuildHelper {

// create a variable
	def static Variable createVariable(String name, int value) {
		if(name === null) throw new NullPointerException()
		var variable = GalFactory.eINSTANCE.createVariable()
		variable.setName(name)
		variable.setValue(createConstant(value))
		return variable
	}

// create a hotbit variable (for enumerated types)
	def static Variable createHotbitVariable(String name, int value) {
		if(name === null) throw new NullPointerException()
		var variable = GalFactory.eINSTANCE.createVariable()
		variable.setName(name)
		variable.setValue(createConstant(value))
		variable.hotbit = true
		return variable
	}

// create an array
	def static ArrayPrefix createArray(String name, IntExpression size) {
		if(name === null || size === null) throw new NullPointerException()
		var array = GalFactory.eINSTANCE.createArrayPrefix();
		array.setName(name)
		array.size = size
		return array
	}

	public static int galTrue = 1
	public static int galFalse = 0
	public static int galNone = -1

	def static BooleanExpression boolTrue() {
		GalFactory.eINSTANCE.createTrue()
	}

	def static BooleanExpression boolFalse() {
		GalFactory.eINSTANCE.createFalse()
	}

	def static Abort createAbort() {
		GalFactory.eINSTANCE.createAbort()
	}

// create a GAL constant - Or a UnaryMinus for negative values
	def static IntExpression createConstant(int value) {
		if (value >= 0) {
			GalFactory.eINSTANCE.createConstant() => [
				it.value = value
			]
		} else {
			GalFactory.eINSTANCE.createUnaryMinus() => [
				it.value = createConstant(-value)
			]
		}
	}

// create a GAL transition
	def static Transition createTransition(String name) {
		if(name === null) throw new NullPointerException()
		var transition = GalFactory.eINSTANCE.createTransition()
		transition.name = name
		return transition
	}

// get a comparison operator for a string representing it
	def static ComparisonOperators getComparisonOperator(String string) {
		switch (string) {
			case "==":
				ComparisonOperators.EQ
			case "!=":
				ComparisonOperators.NE
			case ">=":
				ComparisonOperators.GE
			case "<=":
				ComparisonOperators.LE
			case ">":
				ComparisonOperators.GT
			case "<":
				ComparisonOperators.LT
			default:
				throw new IllegalArgumentException("Unexpected input: " + string +
					" doesn't match with any comparison operator")
		}
	}

// create boolean expression : expr1 == expr2
	def static BooleanExpression createBoolExprComp(IntExpression expr1, IntExpression expr2, ComparisonOperators op) {
		if(expr1 === null || expr2 === null || op === null) throw new NullPointerException()
		val eq = GalFactory.eINSTANCE.createComparison()
		eq.setLeft(expr1)
		eq.setRight(expr2)
		eq.setOperator(op)
		return eq
	}

// create boolean expression : expr == 1 
	def static BooleanExpression createBoolExprIsTrue(IntExpression expr) {
		if(expr === null) throw new NullPointerException()
		return createBoolExprComp(expr, createConstant(galTrue), ComparisonOperators.EQ)
	}

// create boolean expression : expr == 0 
	def static BooleanExpression createBoolExprIsFalse(IntExpression expr) {
		if(expr === null) throw new NullPointerException()
		return createBoolExprComp(expr, createConstant(galFalse), ComparisonOperators.EQ)
	}

// create boolean expression : left & right
	def static BooleanExpression createBoolExprAnd(BooleanExpression left, BooleanExpression right) {
		if(left === null || right === null) throw new NullPointerException()
		val exp = GalFactory.eINSTANCE.createAnd()
		exp.setLeft(left)
		exp.setRight(right)
		return exp
	}

// create boolean expression : left || right
	def static BooleanExpression createBoolExprOr(BooleanExpression left, BooleanExpression right) {
		if(left === null || right === null) throw new NullPointerException()
		val exp = GalFactory.eINSTANCE.createOr()
		exp.setLeft(left)
		exp.setRight(right)
		return exp
	}

	// create boolean expression : ~left || right
	def static BooleanExpression createBoolExprImplies(BooleanExpression left, BooleanExpression right) {
		if(left === null || right === null) throw new NullPointerException()
		val exp = GalFactory.eINSTANCE.createOr()
		exp.setLeft(createBoolExprNot(left))
		exp.setRight(right)
		return exp
	}

// create boolean expression : ~ expr
	def static BooleanExpression createBoolExprNot(BooleanExpression expr) {
		if(expr === null) throw new NullPointerException()
		val exp = GalFactory.eINSTANCE.createNot()
		exp.value = expr
		return exp
	}

// create statement : variable = expr
	def static Statement createAssign(VariableReference variable, IntExpression expr) {
		if(variable === null || expr === null) throw new NullPointerException()
		val ass = GalFactory.eINSTANCE.createAssignment()
		ass.setLeft(variable)
		ass.setRight(expr)
		return ass
	}

	def static IntExpression createWrapBool(BooleanExpression expr) {
		if(expr === null) throw new NullPointerException()
		val res = GalFactory.eINSTANCE.createWrapBoolExpr()
		res.value = expr
		return res
	}

	def static IntExpression createOperation(IntExpression left, IntExpression right, String op) {
		if(left === null || right === null || op === null) throw new NullPointerException()
		val bie = GalFactory.eINSTANCE.createBinaryIntExpression
		bie.setLeft(left)
		bie.setRight(right)
		bie.op = op
		return bie
	}

	def static ParamRef createParamRef(Parameter parameter) {
		if(parameter === null) throw new NullPointerException()
		val ref = GalFactory.eINSTANCE.createParamRef()
		ref.refParam = parameter
		return ref
	}

	def static VariableReference createVarRef(Variable variable) {
		if(variable === null) throw new NullPointerException()
		val ref = GalFactory.eINSTANCE.createVariableReference()
		ref.ref = variable
		return ref
	}

	def static VariableReference createArrayRef(ArrayPrefix array, IntExpression index) {
		if(array === null || index === null) throw new NullPointerException()
		val ref = GalFactory.eINSTANCE.createVariableReference()
		ref.ref = array
		ref.index = index
		return ref
	}

// create the statement If(cond) {thenSt};
	def static Statement createIfThen(BooleanExpression cond, Statement thenSt) {
		if(cond === null || thenSt === null) throw new NullPointerException()
		val ite = GalFactory.eINSTANCE.createIte()
		ite.cond = cond
		ite.ifTrue.add(thenSt)
		return ite
	}

	def static Statement createIfThen(BooleanExpression cond, Iterable<Statement> thenSt) {
		if(cond === null || thenSt.empty) throw new NullPointerException()
		val ite = GalFactory.eINSTANCE.createIte()
		ite.cond = cond
		ite.ifTrue.addAll(thenSt)
		return ite
	}

// create the statement If(cond) {thenSt} else {elseSt};
	def static Statement createIfThenElse(BooleanExpression cond, Iterable<Statement> thenSt,
		Iterable<Statement> elseSt) {
		if(cond === null) throw new NullPointerException()
		val ite = GalFactory.eINSTANCE.createIte()
		ite.cond = cond
		ite.ifTrue.addAll(thenSt)
		ite.ifFalse.addAll(elseSt)
		return ite
	}

// create the statement self."called";
	def static Statement createCall(Label called) {
		if(called === null) throw new NullPointerException()
		val call = GalFactory.eINSTANCE.createSelfCall()
		call.label = called
		return call
	}

// create range: typedef name = min..max
	def static TypedefDeclaration createTypeDefDeclaration(String name, int min, int max) {
		if(name === null) throw new NullPointerException()
		val typedef = GalFactory.eINSTANCE.createTypedefDeclaration()
		typedef.name = name
		typedef.min = createConstant(min)
		typedef.max = createConstant(max)
		return typedef
	}

	def static Parameter createParam(String name, TypedefDeclaration type) {
		if(name === null || type === null) throw new NullPointerException()
		val param = GalFactory.eINSTANCE.createParameter()
		param.name = '$' + name
		param.type = type
		return param
	}

	def static Parameter createDummyParam(String name) {
		if(name === null) throw new NullPointerException()
		val param = GalFactory.eINSTANCE.createParameter()
		param.name = '$' + name
		return param
	}

	def static Label createLabel(String name) {
		if(name === null) throw new NullPointerException()
		val lab = GalFactory.eINSTANCE.createLabel()
		lab.name = name
		return lab
	}

	def static createBigOr(List<BooleanExpression> expressions) {
		expressions.reduce([r, t|GALBuildHelper.createBoolExprOr(r, t)])
	}

	def static createBigAnd(List<BooleanExpression> expressions) {
		expressions.reduce([r, t|GALBuildHelper.createBoolExprAnd(r, t)])
	}

	def static String convert2ParameterName(String name) {
		name.replace('.', '_')
	}

	def static GlobalValue2Proper(IntExpression global, int nbTypes) {
		createOperation(global, createConstant(nbTypes), "/")
	}

	def static GlobalValue2Proper(int global, int nbTypes) {
		global / nbTypes
	}

	def static GlobalValue2Type(IntExpression global, int nbTypes) {
		createOperation(global, createConstant(nbTypes), "%")
	}

	def static GlobalValue2Type(int global, int nbTypes) {
		global % nbTypes
	}

	def static ProperValue2Global(IntExpression proper, AssetType type, int nbTypes) {
		createOperation(createOperation(proper, createConstant(nbTypes), "*"), createConstant(type.typeNumber), "+")
	}
}
