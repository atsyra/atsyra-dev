/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.aspects;

import java.util.HashMap;

import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter;
import fr.irisa.atsyra.absystem.model.absystem.LambdaParameter;
import fr.irisa.atsyra.absystem.model.absystem.util.ExpressionType;
import fr.lip6.move.gal.IntExpression;
import fr.lip6.move.gal.Parameter;

public class Context {
	HashMap<LambdaParameter, IntExpression> lambdaref;
	HashMap<LambdaParameter, ExpressionType> lambdatype;
	HashMap<GuardParameter, Parameter> generatedParameter;
	HashMap<GuardParameter, AssetType> parameterType;
	
	public Context() {
		lambdaref = new HashMap<LambdaParameter, IntExpression>();
		lambdatype = new HashMap<LambdaParameter, ExpressionType>();
		generatedParameter = new HashMap<GuardParameter, Parameter>();
		parameterType = new HashMap<GuardParameter, AssetType>();
		
	}
	
	public Context(Context other) {
		//TODO shallow copy is not enough, should also copy the parameters and update the paramrefs
		lambdaref = (HashMap<LambdaParameter, IntExpression>) other.lambdaref.clone();
		lambdatype = (HashMap<LambdaParameter, ExpressionType>) other.lambdatype.clone();
		generatedParameter = (HashMap<GuardParameter, Parameter>) other.generatedParameter.clone();
		parameterType = (HashMap<GuardParameter, AssetType>) other.parameterType.clone();
	}
	
	public void addLambdaRef(LambdaParameter lambda, IntExpression expr) {
		lambdaref.put(lambda, expr);
	}
	
	public IntExpression getLambdaRef(LambdaParameter lambda) {
		return EcoreUtil.copy(lambdaref.get(lambda));
	}
	
	public void addLambdaType(LambdaParameter lambda, ExpressionType type) {
		lambdatype.put(lambda, type);
	}
	
	public ExpressionType getLambdaType(LambdaParameter lambda) {
		return lambdatype.get(lambda);
	}
	
	public void addParameter(GuardParameter gaparam, Parameter param, AssetType paramType) {
		generatedParameter.put(gaparam, param);
		parameterType.put(gaparam, paramType);
	}
	
	public Parameter getParameter(GuardParameter param) {
		return generatedParameter.get(param);
	}
	
	public AssetType getParameterType(GuardParameter param) {
		return parameterType.get(param);
	}
}
