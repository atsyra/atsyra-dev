/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetBasedSystemAspect;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetTypeAspect;
import fr.irisa.atsyra.absystem.gal.transfo.helpers.AssetMapBuilder;
import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder;
import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceQuery;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.FlattenLink;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSSizeInfo;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.model.absystem.util.GoalSizeInfo;
import fr.irisa.atsyra.absystem.transfo.ABS2ABS;
import fr.irisa.atsyra.absystem.transfo.ABS2ABS.ABS2ABScontext;
import fr.irisa.atsyra.absystem.transfo.ABS2ABS.ABS2ABSresult;
import fr.irisa.atsyra.absystem.transfo.ABS2ABSDefault;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandler;
import fr.irisa.atsyra.absystem.transfo.trace.ABS2ABSTraceQuery;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.irisa.atsyra.gal.GALSizeInfo;
import fr.irisa.atsyra.preferences.GlobalPreferenceService;
import fr.irisa.atsyra.preferences.constants.AtsyraPreferenceConstants;
import fr.lip6.move.gal.GALTypeDeclaration;
import fr.lip6.move.gal.Specification;
import fr.lip6.move.gal.Transition;
import fr.lip6.move.gal.instantiate.GALRewriter;
import fr.lip6.move.serialization.SerializationUtil;

public class ABS2GAL {
	static final Logger logger = LoggerFactory.getLogger(ABS2GAL.class);
	static Clock clock = Clock.systemDefaultZone();
	
	private ABS2ABS abs2abs;

	public ABS2GAL() {
		abs2abs = new ABS2ABSDefault();
	}
	
	public ABS2GAL(ABS2ABS preliminary) {
		abs2abs = Objects.requireNonNull(preliminary);
	}

	public static class ABS2GALContext {
		final GeneratedFilesHandler generatedFilesHandler;
		final ResourceSet absResourceSet;
		final Goal goal;
		final Instant start;

		public ABS2GALContext(GeneratedFilesHandler generatedFilesHandler, ResourceSet absResourceSet, Goal goal, Instant start) {
			this.generatedFilesHandler = generatedFilesHandler;
			this.absResourceSet = absResourceSet;
			this.goal = goal;
			this.start = start;
		}


		public ABS2GALContext(ResourceSet absResourceSet, Goal goal, ABS2GALContext context) {
			this.generatedFilesHandler = context.generatedFilesHandler;
			this.absResourceSet = absResourceSet;
			this.goal = goal;
			this.start = context.start;
		}

		public ABS2ABScontext toABS2ABScontext() {
			return new ABS2ABScontext(generatedFilesHandler, goal, start, absResourceSet);
		}
	}

	public static class ABS2GALresult {
		final Specification spec;
		final Specification nonflat;
		final AssetBasedSystem abs;
		final TransfoTraceModel tracemodel;

		public ABS2GALresult(Specification spec, Specification nonflat, AssetBasedSystem abs, TransfoTraceModel tracemodel) {
			this.spec = spec;
			this.abs = abs;
			this.tracemodel = tracemodel;
			this.nonflat = nonflat;
		}
	}

	public ABS2GALresult abs2gal(ABS2GALContext context, TransfoTraceModel traceModel, IProgressMonitor monitor)
			throws OperationCanceledException, IOException, CoreException {
		SubMonitor submonitor = SubMonitor.convert(monitor, 100);
		ABS2ABSresult abs2abSresult = abs2abs.abs2abs(context.toABS2ABScontext(), traceModel, submonitor.split(10));
		if(monitor.isCanceled()) {
			return null;
		}
		AssetBasedSystem transformed = abs2abSresult.model;
		traceModel = abs2abSresult.traceModel;
		Goal transformedGoal = (Goal) ABS2ABSTraceQuery.of(traceModel).getFinal(context.goal);
		printGoalSize(transformedGoal, context.generatedFilesHandler);
		ABS2GALresult abs2galresult = tabs2gal(transformed, transformedGoal, context, traceModel, 
				submonitor.split(89));
		if(!monitor.isCanceled() && GlobalPreferenceService.INSTANCE.getBoolean("fr.irisa.atsyra.ide.ui", "generateResultFiles").orElse(true)) {
			AssetMapBuilder.createAssetMap(transformed, context.generatedFilesHandler, submonitor.split(1));
		}
		return abs2galresult;
	}

	protected ABS2GALresult tabs2gal(AssetBasedSystem model, Goal goal, ABS2GALContext context, TransfoTraceModel traceModel, IProgressMonitor monitor)
			throws CoreException, IOException {
		SubMonitor abs2galMonitor = SubMonitor.convert(monitor, "ABS2GAL", 100);
		Specification spec = AssetBasedSystemAspect.convertGoal2GAL(model, goal, traceModel);
		Resource galResource = context.generatedFilesHandler.getResource("paramgal", Optional.of("gal"), new ResourceSetImpl());
		galResource.getContents().clear();
		galResource.getContents().add(spec);
		// galResource.save(Collections.emptyMap());
		File galFile = context.generatedFilesHandler.getFile("paramgal", Optional.of("gal"));
		writeSpecToFile(spec, galFile, abs2galMonitor.split(45));
		printParamGalSize(spec, context.generatedFilesHandler);
		Specification flatspec = EcoreUtil.copy(spec); //SerializationUtil.fileToGalSystem(galFile.getAbsolutePath());
		Resource flatgalResource = context.generatedFilesHandler.getResource("flatgal", Optional.of("gal"), new ResourceSetImpl());
		flatgalResource.getContents().clear();
		flatgalResource.getContents().add(flatspec);
		IProgressMonitor flattenMonitor = abs2galMonitor.split(7);
		GALRewriter.flatten(flatspec, false);
		flattenMonitor.done();
		File fileflat = context.generatedFilesHandler.getFile("flatgal", Optional.of("gal"));
		writeSpecToFile(flatspec, fileflat, abs2galMonitor.split(45));
		printFlatGallSize(flatspec, context.generatedFilesHandler);
		traceModel.eResource().save(Collections.emptyMap());
		createFlattenTrace(new ABS2GALTraceBuilder(traceModel), spec, flatspec, List.of(model));
		traceModel.eResource().save(Collections.emptyMap());
		return new ABS2GALresult(flatspec, spec, model, traceModel);
	}

	private void createFlattenTrace(ABS2GALTraceBuilder tracebuilder, Specification spec, Specification flatspec,
			List<AssetBasedSystem> absSourceModels) {
		GALTypeDeclaration gal = (GALTypeDeclaration) spec.getTypes().get(0);
		GALTypeDeclaration flatgal = (GALTypeDeclaration) flatspec.getTypes().get(0);
		createFlattenTrace(tracebuilder, gal, flatgal, absSourceModels);
	}

	private void createFlattenTrace(ABS2GALTraceBuilder tracebuilder, GALTypeDeclaration gal,
			GALTypeDeclaration flatgal, List<AssetBasedSystem> absSourceModels) {
		for (Transition tr : flatgal.getTransitions()) {
			if (!Objects.equals(tr.getName(), "init") && tr.getLabel() == null) {
				FlattenLink link = createFlattenLink(tracebuilder.getTraceModel(), gal, tr, absSourceModels);
				if (link.getTransition() == null) {
					throw new NullPointerException("Unable to find the source transition from " + tr.getName());
				}
				tracebuilder.addFlattenLink(link.getName(), link.getTransition(), link.getParametersinstances(),
						link.getTransitionflat());
			}
		}
	}

	private FlattenLink createFlattenLink(TransfoTraceModel tracemodel, GALTypeDeclaration gal, Transition tr,
			List<AssetBasedSystem> absSourceModels) {
		Set<AssetType> allAssetTypes = ABSUtils.getAllAssetTypes(Set.copyOf(absSourceModels));
		Map<String, AssetType> allAssetTypesByQualifiedName = allAssetTypes.stream()
				.collect(Collectors.toMap(AssetTypeAspect::getQualifiedName, Function.identity()));
		Transition notflat = null;
		List<Asset> parametersInstances = new ArrayList<>();
		try (Scanner nameScan = new Scanner(tr.getName())) {
			nameScan.useDelimiter("__");
			StringBuilder notflatName = new StringBuilder();
			// Scan for action name & number of repetition
			while (nameScan.hasNext()) {
				String next = nameScan.next();
				if (!isInteger(next)) {
					if (notflatName.length() > 0) {
						notflatName.append("__");
					}
					notflatName.append(next);
				} else {
					notflatName.append("__").append(next);
					break;
				}
			}
			// Scan for AssetType names
			List<String> paramTypes = new ArrayList<>();
			String lastSegment = null;
			while (nameScan.hasNext()) {
				if (lastSegment != null) {
					paramTypes.add(lastSegment);
					notflatName.append("__").append(lastSegment);
				}
				lastSegment = nameScan.next();
			}
			// lastSegment may be null for action with no parameters
			if (lastSegment != null) {
				try (Scanner lastsegmentScan = new Scanner(lastSegment)) {
					lastsegmentScan.useDelimiter("_");
					StringBuilder lastParamType = new StringBuilder();
					// Scan last param type
					while (lastsegmentScan.hasNext()) {
						String next = lastsegmentScan.next();
						if (lastParamType.length() > 0) {
							lastParamType.append("_");
						}
						lastParamType.append(next);
						if (allAssetTypesByQualifiedName.containsKey(lastParamType.toString())) {
							paramTypes.add(lastParamType.toString());
							notflatName.append("__").append(lastParamType.toString());
							break;
						}
					}
					notflat = getTransitionByName(gal, notflatName.toString());
					// Get parameter instantiations
					parametersInstances.addAll(getParamValues(tracemodel, lastsegmentScan, paramTypes,
							allAssetTypesByQualifiedName, notflat));
				}
			}
		}
		return new FlattenLink(tr.getName(), notflat, parametersInstances, tr);
	}

	private boolean isInteger(String s) {
		if (s.isEmpty())
			return false;
		for (int i = 0; i < s.length(); i++) {
			if (Character.digit(s.charAt(i), 10) < 0)
				return false;
		}
		return true;
	}

	private List<Asset> getParamValues(TransfoTraceModel tracemodel, Scanner lastsegmentScan, List<String> paramTypes,
			Map<String, AssetType> allAssetTypesByQualifiedName, Transition notflat) {
		List<Asset> result = new ArrayList<>();
		// Scan for instantiation of parameters
		List<String> paramNames = ListExtensions.map(notflat.getParams(), param -> param.getName().substring(1));
		ABS2GALTraceQuery query = ABS2GALTraceQuery.of(tracemodel);
		Map<Integer, Asset> paramValues = new HashMap<>();
		while (lastsegmentScan.hasNext()) {
			String next = lastsegmentScan.next();
			for (String paramName : paramNames) {
				if (next.startsWith(paramName)) {
					int pnum = paramNames.indexOf(paramName);
					AssetType ptype = allAssetTypesByQualifiedName.get(paramTypes.get(pnum));
					int ppropervalue = Integer.parseInt(next.substring(paramName.length()));
					Asset pvalue = query.getProperAsset(ptype, ppropervalue).orElseThrow();
					paramValues.put(pnum, pvalue);
					break;
				}
			}
		}
		// Save the parameters values in order
		for (int i = 0; i < paramValues.size(); i++) {
			result.add(paramValues.get(i));
		}
		return result;
	}

	private Transition getTransitionByName(GALTypeDeclaration gal, String name) {
		return gal.getTransitions().stream().filter(tr -> Objects.equals(tr.getName(), name)).findFirst().orElseThrow();
	}

	private void writeSpecToFile(Specification spec, File galFile, IProgressMonitor monitor) throws IOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		subMonitor.setTaskName("Saving " + galFile.getName());
		try (FileOutputStream out = new FileOutputStream(galFile)) {
			out.write(0);
			SerializationUtil.systemToFile(spec, galFile.getAbsolutePath(), true);
		}
		subMonitor.done();
	}
	
	private void printGoalSize(Goal goal, GeneratedFilesHandler generatedFilesHandler) throws IOException {
		Optional<String> preference = GlobalPreferenceService.INSTANCE.getString(AtsyraPreferenceConstants.P_QUALIFIER, AtsyraPreferenceConstants.P_PRINT_SIZE);
		if(preference.isPresent()) {
			if(preference.get().equals("file")) {
				GoalSizeInfo goalSizeInfo = GoalSizeInfo.getAbsModelSizeInfo(goal);
				try (FileWriter fw = new FileWriter(generatedFilesHandler.getFile("modelSizes", Optional.of("log")), true)) {
					String message = String.format("Goal size after abs2abs: %s\n", goalSizeInfo);
					logger.info(message);
					fw.append(message);
				}
			} else if (preference.get().equals("log") && logger.isInfoEnabled()) {
				GoalSizeInfo goalSizeInfo = GoalSizeInfo.getAbsModelSizeInfo(goal);
				logger.info("Goal size after abs2abs: {}", goalSizeInfo);
			}
		}
	}
	
	private void printParamGalSize(Specification spec, GeneratedFilesHandler generatedFilesHandler) throws IOException {
		Optional<String> preference = GlobalPreferenceService.INSTANCE.getString(AtsyraPreferenceConstants.P_QUALIFIER, AtsyraPreferenceConstants.P_PRINT_SIZE);
		if(preference.isPresent()) {
			if(preference.get().equals("file")) {
				GALSizeInfo sizeInfo = GALSizeInfo.getGalModelSizeInfo(spec);
				try (FileWriter fw = new FileWriter(generatedFilesHandler.getFile("modelSizes", Optional.of("log")), true)) {
					String message = String.format("ABS2GAL produced a model of size %s\n", sizeInfo);
					logger.info(message);
					fw.append(message);
				}
			} else if (preference.get().equals("log") && logger.isInfoEnabled()) {
				GALSizeInfo sizeInfo = GALSizeInfo.getGalModelSizeInfo(spec);
				logger.info("ABS2GAL produced a model of size {}", sizeInfo);
			}
		}
	}
	
	private void printFlatGallSize(Specification spec, GeneratedFilesHandler generatedFilesHandler) throws IOException {
		Optional<String> preference = GlobalPreferenceService.INSTANCE.getString(AtsyraPreferenceConstants.P_QUALIFIER, AtsyraPreferenceConstants.P_PRINT_SIZE);
		if (preference.isPresent()) {
			if (preference.get().equals("file")) {
				GALSizeInfo sizeInfo = GALSizeInfo.getGalModelSizeInfo(spec);
				try (FileWriter fw = new FileWriter(generatedFilesHandler.getFile("modelSizes", Optional.of("log")), true)) {
					String message = String.format("ABS2GAL produced a flatten model of size %s\n", sizeInfo);
					logger.info(message);
					fw.append(message);
				}
			} else if (preference.get().equals("log") && logger.isInfoEnabled()) {
				GALSizeInfo sizeInfo = GALSizeInfo.getGalModelSizeInfo(spec);
				logger.info("ABS2GAL produced a flatten model of size {}", sizeInfo);
			}
		}
	}
}
