/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.helpers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.xtend.lib.annotations.Data;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

import com.google.common.collect.HashMultimap;

import fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetAspect;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.GuardAspect;
import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceQuery;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.FlattenLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.GuardedActionLink;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;
import fr.irisa.atsyra.absystem.model.absystem.Scenario;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmFactory;
import fr.irisa.atsyra.absystem.transfo.trace.ABS2ABSTraceQuery;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.lip6.move.gal.GALTypeDeclaration;

public class ABSScenarioBuilder {
	ABSStateBuilder statebuilder;
	TransfoTraceModel traceModel;

	public ABSScenarioBuilder(TransfoTraceModel traceModel, ABSStateBuilder statebuilder) {
		this.traceModel = traceModel;
		this.statebuilder = statebuilder;
	}

	public List<Scenario> convert2ABSScenario(String galOutput, GALTypeDeclaration gal, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 1000);
		final List<Scenario> result = new ArrayList<>();
		List<ScenarioString> scenarioStrings = getWitnessStrings(galOutput);
		scenarioStrings.forEach(scenariostring -> {
			Scenario scenario = convertGalPathToABSScenario(Witness2Array(scenariostring.path),
					subMonitor.split(1000 / scenarioStrings.size()));
			for(int i = 0; i <= scenario.size(); i++) {
				scenario.setIntermediateStates(i, ListExtensions.map(scenariostring.interStates.get(i),
						statestr -> ABS2ABSTraceQuery.of(traceModel).getRevertedState(statebuilder.stateFromGalString(statestr, gal))));
			}
			result.add(scenario);
		});
		subMonitor.done();
		return result;
	}

	public void writeABSScenario2File(List<Scenario> scenarios, File file, IProgressMonitor monitor)
			throws IOException {
		final String scenariosString = IterableExtensions.fold(
				IterableExtensions.map(scenarios, s -> ABSScenario2String(s) + System.lineSeparator()), "",
				String::concat);
		FileUtils.writeStringToFile(file, scenariosString, "UTF-8");
		monitor.done();
	}

	public HashMultimap<String, Integer> getPossibleValuesInFinalStates(String galOutput, Set<String> allVariables,
			IProgressMonitor monitor) {
		final HashMultimap<String, Integer> result = HashMultimap.create();
		galOutput.lines().forEachOrdered(new Consumer<String>() {
			private boolean isFinalStates = false;

			@Override
			public void accept(String line) {
				if (isFinalStates) {
					if (!line.startsWith("[")) {
						isFinalStates = false;
					} else {
						if (line.endsWith("]")) {
							Set<String> remainingVariables = new HashSet<>(allVariables);
							String[] values = line.split("\\s");
							for (String value : values) {
								if (value.equals("[") || value.equals("]"))
									continue;
								String[] affected = value.split("=");
								if (affected[0].equals("_init"))
									continue;
								result.put(affected[0], Integer.parseInt(affected[1]));
								remainingVariables.remove(affected[0]);
							}
							for (String variable : remainingVariables) {
								result.put(variable, 0);
							}
						}
					}
				} else if (line.startsWith("Leads to final states")) {
					isFinalStates = true;
				}
			}
		});
		return result;
	}

	public Stream<String> invariantsFromPossibleValues(Map<String, Set<Integer>> possibleValues) {
		return possibleValues.entrySet().stream()
				.flatMap(entry -> entry.getValue().stream().map(i -> entry.getKey() + "!=" + i));
	}

	private List<ScenarioString> getWitnessStrings(String galOutput) {
		List<ScenarioString> result = new ArrayList<>();
		List<String> lines = galOutput.lines().collect(Collectors.toList());
		String path = "init";
		List<List<String>> intermediateStates = new ArrayList<>();
		boolean intermediateState = false, first = false;
		int stateIndex = 0;
		for (Iterator<String> iterator = lines.iterator(); iterator.hasNext();) {
			String line = iterator.next();
			if (line.startsWith("init,")) {
				path = line;
			} else if (line.startsWith("Transition 0 fire : init")) {
				intermediateState = true;
				stateIndex = 0;
				intermediateStates.add(new  ArrayList<>());
			} else if (line.startsWith("Transition ")) {
				intermediateState = true;
				stateIndex++;
				intermediateStates.add(new  ArrayList<>());
			} else if (intermediateState) {
				if (!line.startsWith("[")) {
					intermediateState = false;
				} else {
					intermediateStates.get(stateIndex).add(line);
				}
			} else if (line.startsWith("This shortest transition sequence")) {
				if (first) {
					result.add(new ScenarioString(path, new ArrayList<>(intermediateStates)));
				}
				first = true;
				path = "init";
				intermediateStates.clear();
				intermediateState = false;
			}
		}
		if (first) {
			result.add(new ScenarioString(path, new ArrayList<>(intermediateStates)));
		}
		return result;
	}

	private String[] Witness2Array(String witness) {
		return witness.split(", ");
	}
	
	public String convert2StringWithTraceabilityMapping(String galOutput, Set<String> allVariables, TransfoTraceModel tm, IProgressMonitor monitor) {
		StringBuilder result = new StringBuilder();
		List<ScenarioString> scenarioStrings = getWitnessStrings(galOutput);
		
		scenarioStrings.forEach(scenariostring -> {
			result.append(convertGalPathToString(Witness2Array(scenariostring.path), tm, monitor.slice(1)));
			result.append('\n');
		});
		monitor.done();
		return result.toString();
	}
	
	private String convertGalPathToString(String[] galPath, TransfoTraceModel tm, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 1000);
		StringBuilder result = new StringBuilder();
		for (String action : galPath) {
			if (action.equals("init"))
				continue;
			result.append(convertGalActionToGuardedAction(action, tm, subMonitor.split(950 / (action.length() - 1))));
		}
		subMonitor.done();
		return result.toString();
	}
	
	public String convertGalActionToGuardedAction(String galflatName, TransfoTraceModel tm, IProgressMonitor monitor) {
		StringBuilder result = new StringBuilder();
		ABS2GALTraceQuery traceQuery = ABS2GALTraceQuery.of(tm);
		FlattenLink flattenLink =  traceQuery.fromFlatTransitionName(galflatName);
		GuardedActionLink guardedActionLink = traceQuery.fromTransition(flattenLink.getTransition());
		GuardedAction guardedAction = (GuardedAction) guardedActionLink.getGuardedaction();
		result.append(guardedAction.getName());
		result.append('(');
		boolean first = true;
		for (Asset param : flattenLink.getParametersinstances()) {
			if(first) {
				first = false;
			} else {
				result.append(", ");
			}
			result.append(param.getName());
		}
		result.append(')');
		return result.toString();
	}

	private Scenario convertGalPathToABSScenario(String[] galPath, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, galPath.length + 1);
		Scenario result = new Scenario();
		ABS2GALTraceQuery query = ABS2GALTraceQuery.of(traceModel);
		for (String action : galPath) {
			if (action.equals("init"))
				continue;
			FlattenLink flattenLink = query.fromFlatTransitionName(action);
			List<AssetArgument> assetArguments = ListExtensions.map(flattenLink.getParametersinstances(), asset -> {
				AssetArgument assetarg = Interpreter_vmFactory.eINSTANCE.createAssetArgument();
				assetarg.setAsset((Asset) ABS2ABSTraceQuery.of(traceModel).getOrigin(asset));
				return assetarg;
			});
			GuardedActionLink guardedActionLink = query.fromTransition(flattenLink.getTransition());
			GuardedAction guardedaction = (GuardedAction) ABS2ABSTraceQuery.of(traceModel).getOrigin(guardedActionLink.getGuardedaction()) ;
			GuardOccurence go = Interpreter_vmFactory.eINSTANCE.createGuardOccurence();
			go.setGuard(guardedaction);
			go.getGuardOccurenceArguments().addAll(assetArguments);
			result.addAction(go);
			subMonitor.worked(1);
		}
		subMonitor.done();
		return result;
	}

	public String ABSScenario2String(Scenario scenario) {
		final Function2<String, String, String> concatWithComa = (s1, s2) -> {
			if (s1.isEmpty())
				return s2;
			else
				return s1 + ", " + s2;
		};
		return scenario.size() + ": "
				+ IterableExtensions
						.fold(IterableExtensions
								.map(scenario.getActions(),
										s -> GuardAspect.getQualifiedName(s.getGuard()) + '('
												+ IterableExtensions.fold(
														IterableExtensions.map(
																IterableExtensions.filter(s
																		.getGuardOccurenceArguments(),
																		AssetArgument.class),
																param -> AssetAspect
																		.getQualifiedName(param.getAsset())),
														"", concatWithComa)
												+ ')'),
								"", concatWithComa);
	}

	@Data
	private class ScenarioString {
		public final String path;
		public final  List<List<String>> interStates;

		public ScenarioString(String path, List<List<String>> interStates) {
			this.path = path;
			this.interStates = interStates;
		}
	}
}
