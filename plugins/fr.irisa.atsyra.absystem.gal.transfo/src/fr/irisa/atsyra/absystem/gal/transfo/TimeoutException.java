/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo;

import java.util.List;

public class TimeoutException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2517028762902205150L;
	
	private List<ReportEntry> scenarios;
	
	public TimeoutException(List<ReportEntry> scenarios) {
		this.scenarios = scenarios;
	}

	public List<ReportEntry> getScenarios() {
		return scenarios;
	}
}
