/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo;

import java.io.File;
import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystemManager;
import org.eclipse.xtend.lib.annotations.Data;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterators;
import com.google.common.collect.Multimap;
import com.google.common.collect.UnmodifiableIterator;

import fr.irisa.atsyra.absystem.gal.transfo.ABS2GAL.ABS2GALContext;
import fr.irisa.atsyra.absystem.gal.transfo.ABS2GAL.ABS2GALresult;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetBasedSystemAspect;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.ContractAspect;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.ExpressionAspect;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.GALBuildHelper;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.GuardAspect;
import fr.irisa.atsyra.absystem.gal.transfo.helpers.ABSScenarioBuilder;
import fr.irisa.atsyra.absystem.gal.transfo.helpers.ABSStateBuilder;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.Contract;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.Scenario;
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurenceComparator;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandler;
import fr.irisa.atsyra.absystem.transfo.trace.ABS2ABSTraceQuery;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.irisa.atsyra.gal.AbstractGalReachability;
import fr.irisa.atsyra.gal.GalSpecificationScenarios;
import fr.irisa.atsyra.gal.IntExpressionEval;
import fr.irisa.atsyra.gal.IntExpressionEval.NotEvaluableException;
import fr.irisa.atsyra.gal.Simplifier;
import fr.irisa.atsyra.gal.ValueForbiddingInvariant;
import fr.irisa.atsyra.preferences.GlobalPreferenceService;
import fr.lip6.move.gal.ArrayPrefix;
import fr.lip6.move.gal.BooleanExpression;
import fr.lip6.move.gal.GALTypeDeclaration;
import fr.lip6.move.gal.GalFactory;
import fr.lip6.move.gal.IntExpression;
import fr.lip6.move.gal.NeverProp;
import fr.lip6.move.gal.Property;
import fr.lip6.move.gal.Specification;
import fr.lip6.move.gal.VarDecl;
import fr.lip6.move.gal.VariableReference;
import fr.lip6.move.gal.instantiate.GALRewriter;
import fr.lip6.move.serialization.SerializationUtil;

public class ABS2Scenarios {
	static final Logger logger = LoggerFactory.getLogger(ABS2Scenarios.class);
	static final Clock clock = Clock.systemDefaultZone();
	
	ReportEntryFactory reportEntryFactory;
	
	MessagingSystem mainMessagingSystem;

	ABSScenarioBuilder scenarioBuilder;
	ABSStateBuilder stateBuilder;
	ScenarioCanonicityHelper canonicity;

	List<Scenario> alreadyFound;
	protected MessagingSystem galMessagingSystem;
	
	protected ABS2GAL abs2gal;
	private boolean useDynamicContracts;
	
	public ABS2Scenarios(ReportEntryFactory factory) {
		reportEntryFactory = factory;
		MessagingSystemManager msm = new MessagingSystemManager();
		mainMessagingSystem = msm.createBestPlatformMessagingSystem("fr.irisa.atsyra.ide.ui", "ATSyRA");
		alreadyFound = new ArrayList<>();
		canonicity = new ScenarioCanonicityHelper();
		abs2gal = new ABS2GAL();
		useDynamicContracts = GlobalPreferenceService.INSTANCE.getBoolean("fr.irisa.atsyra.ide.ui", "enforceDynamicContracts").orElse(true);
	}

	public ABS2Scenarios(ReportEntryFactory factory, MessagingSystem messagingSystem) {
		reportEntryFactory = factory;
		mainMessagingSystem = messagingSystem;
		alreadyFound = new ArrayList<>();
		canonicity = new ScenarioCanonicityHelper();
		abs2gal = new ABS2GAL();
		useDynamicContracts = GlobalPreferenceService.INSTANCE.getBoolean("fr.irisa.atsyra.ide.ui", "enforceDynamicContracts").orElse(true);
	}
	
	public ABS2Scenarios(ABS2GAL abs2gal, ReportEntryFactory factory) {
		reportEntryFactory = factory;
		MessagingSystemManager msm = new MessagingSystemManager();
		mainMessagingSystem = msm.createBestPlatformMessagingSystem("fr.irisa.atsyra.ide.ui", "ATSyRA");
		alreadyFound = new ArrayList<>();
		canonicity = new ScenarioCanonicityHelper();
		this.abs2gal = abs2gal;
		useDynamicContracts = GlobalPreferenceService.INSTANCE.getBoolean("fr.irisa.atsyra.ide.ui", "enforceDynamicContracts").orElse(true);
	}
	
	public ABS2Scenarios(ABS2GAL abs2gal, ReportEntryFactory factory, MessagingSystem messagingSystem) {
		reportEntryFactory = factory;
		mainMessagingSystem = messagingSystem;
		alreadyFound = new ArrayList<>();
		canonicity = new ScenarioCanonicityHelper();
		this.abs2gal = abs2gal;
		useDynamicContracts = GlobalPreferenceService.INSTANCE.getBoolean("fr.irisa.atsyra.ide.ui", "enforceDynamicContracts").orElse(true);
	}
	
	public void setUseDynamicContracts(boolean value) {
		useDynamicContracts = value;
	}

	public static class ABS2ScenariosContext {
		final GeneratedFilesHandler generatedFilesHandler;
		final ResourceSet absResourceSet;
		final Goal goal;
		final Instant start;
		final long timeout;
		final int nbWitness;
		final TransfoTraceModel traceModel;

		public ABS2ScenariosContext(GeneratedFilesHandler generatedFilesHandler, ResourceSet absResourceSet, Goal goal, TransfoTraceModel traceModel, Instant start,
				long timeout, int nbWitness) {
			this.generatedFilesHandler = generatedFilesHandler;
			this.absResourceSet = absResourceSet;
			this.goal = goal;
			this.start = start;
			this.timeout = timeout;
			this.nbWitness = nbWitness;
			this.traceModel = traceModel;
		}

		public ABS2GALContext toABS2GALContext() {
			return new ABS2GALContext(generatedFilesHandler, absResourceSet, goal, start);
		}
	}
	
	private static class ABS2ScenariosInternalContext {
		final GeneratedFilesHandler generatedFilesHandler;
		final AssetBasedSystem model;
		final Goal goal;
		final Instant start;
		final long timeout;
		final int nbWitness;
		final TransfoTraceModel traceModel;
		final Optional<BooleanExpression> invariant;

		public ABS2ScenariosInternalContext(ABS2ScenariosContext original, TransfoTraceModel tracemodel, AssetBasedSystem transformed, Optional<BooleanExpression> invariant) {
			this.generatedFilesHandler = original.generatedFilesHandler;
			this.model = transformed;
			this.goal = (Goal) ABS2ABSTraceQuery.of(tracemodel).getFinal(original.goal);
			this.start = original.start;
			this.timeout = original.timeout;
			this.nbWitness = original.nbWitness;
			this.traceModel = tracemodel;
			this.invariant = invariant;
		}
	}

	public List<ReportEntry> abs2Scenarios(ABS2ScenariosContext context, IProgressMonitor monitor)
			throws OperationCanceledException, IOException, CoreException {
		SubMonitor submonitor = SubMonitor.convert(monitor, 100);
		ABS2GALresult abs2galResult = abs2gal.abs2gal(context.toABS2GALContext(), context.traceModel, submonitor.split(10));
		if(submonitor.isCanceled()) {
			return List.of();
		}
		Optional<BooleanExpression> invariantFromDynamicContracts = getInvariantFromDynamicContractsForPrunedGal(abs2galResult.abs.eResource().getResourceSet(), abs2galResult.nonflat);
		ABS2ScenariosInternalContext transformedContext = new ABS2ScenariosInternalContext(context, abs2galResult.tracemodel, abs2galResult.abs, invariantFromDynamicContracts);
		createBuilders(abs2galResult.tracemodel);
		List<ReportEntry> entries = computeAllScenarios(abs2galResult.spec, transformedContext, submonitor.split(50));
		if(GlobalPreferenceService.INSTANCE.getBoolean("fr.irisa.atsyra.ide.ui", "generateResultFiles").orElse(true)) {
			createScenarioFile(scenariosOf(entries), context.generatedFilesHandler, submonitor.split(1));
		}
		return entries;
	}

	private List<ReportEntry> computeAllScenarios(Specification spec, ABS2ScenariosInternalContext context,
			IProgressMonitor monitor) {
		List<ReportEntry> result = new ArrayList<>();
		alreadyFound = new ArrayList<>();
		SubMonitor submonitor = SubMonitor.convert(monitor, 100);
		try {
			Queue<Optional<ValueForbiddingInvariant>> invariants = new ConcurrentLinkedQueue<>();
			invariants.add(Optional.empty());
			while (!invariants.isEmpty()) {
				long elapsed = Duration.between(context.start, clock.instant()).getSeconds();
				if (elapsed >= context.timeout) {
					logger.info("time out.");
					return result;
				}
				if (result.size() >= context.nbWitness) {
					logger.info("maximum number of witnesses found.");
					return result;
				}
				if (monitor.isCanceled()) {
					logger.info("cancelled.");
					return result;
				}
				Optional<ValueForbiddingInvariant> invariant = invariants.poll();
				submonitor.setWorkRemaining(100);
				ScenarioOutput scenarioOutput = galScenarios(spec, invariant, context, submonitor.split(20));
				alreadyFound.addAll(scenariosOf(scenarioOutput.scenarios));
				result.addAll(scenarioOutput.scenarios);
				invariants.addAll(ListExtensions.map(scenarioOutput.nextInvariants, Optional::of));
			}
		} catch (TimeoutException e) {
			result.addAll(e.getScenarios());
		}
		submonitor.done();
		return result;
	}

	private List<Scenario> scenariosOf(List<ReportEntry> entries) {
		return ListExtensions.map(entries, ReportEntry::getScenario);
	}

	private ScenarioOutput galScenarios(Specification spec, Optional<ValueForbiddingInvariant> invariant,
			ABS2ScenariosInternalContext context, IProgressMonitor monitor) throws TimeoutException {
		SubMonitor submonitor = SubMonitor.convert(monitor, 100);
		StringBuilder resultBuilder = new StringBuilder();
		File galFile = context.generatedFilesHandler.getFile("workgal", Optional.of("gal"));
		File propFile = context.generatedFilesHandler.getFile("workprop", Optional.of("prop"));
		GalSpecificationScenarios gr = new GalSpecificationScenarios(spec, galFile, propFile, context.goal.getName(),
				resultBuilder, getGALMessagingSystem());
		long elapsed = Duration.between(context.start, clock.instant()).getSeconds();
		gr.setTimeout(context.timeout - elapsed);
		if (invariant.isPresent()) {
			String fullInvariant = invariant.get().getInvariant() + context.invariant.map(inv -> "&&" + SerializationUtil.getText(inv, true)).orElse("");
			gr.setInvariant(fullInvariant);
			logger.debug("Invariant for GAL scenarios: {}", fullInvariant);
		} else if(context.invariant.isPresent()){
			String fullInvariant = SerializationUtil.getText(context.invariant.orElseThrow(), true);
			gr.setInvariant(fullInvariant);
			logger.debug("Invariant for GAL scenarios: {}", fullInvariant);
		}
		Instant reachstart = clock.instant();
		logger.debug("running its-reach");
		IStatus reachStatus = gr.computeReachability(submonitor.split(60));
		Duration reachdur = Duration.between(reachstart, clock.instant());
		if (reachStatus != Status.OK_STATUS && "timeout".equals(reachStatus.getMessage())) {
			logger.debug("its-reach timed out.");
			throw new TimeoutException(new ArrayList<>());
		}
		return processGALOutput(new GalOutput(resultBuilder, reachdur), invariant, spec, context, submonitor.split(40));
	}

	@Data
	private class ScenarioOutput {
		public ScenarioOutput(List<ReportEntry> scenarios, List<ValueForbiddingInvariant> nextInvariants) {
			this.scenarios = scenarios;
			this.nextInvariants = nextInvariants;
		}

		List<ReportEntry> scenarios;
		List<ValueForbiddingInvariant> nextInvariants;
	}

	@Data
	private class GalOutput {
		public GalOutput(StringBuilder resultBuilder, Duration reachdur2) {
			consoleOutput = resultBuilder;
			reachdur = reachdur2;
		}

		StringBuilder consoleOutput;
		Duration reachdur;
	}

	private ScenarioOutput processGALOutput(GalOutput galOutput, Optional<ValueForbiddingInvariant> invariant,
			Specification spec, ABS2ScenariosInternalContext context, IProgressMonitor monitor) throws TimeoutException {
		List<Scenario> scenarios = scenarioBuilder.convert2ABSScenario(galOutput.consoleOutput.toString(), (GALTypeDeclaration) spec.getMain(), monitor);
		if (scenarios.isEmpty()) {
			if (invariant.isPresent()) {
				logger.debug("longer scenarios: for invariant {}, found no witnesses.", invariant.get().getInvariant());
			} else {
				logger.debug("scenarios: found no witnesses.");
			}
			return new ScenarioOutput(new ArrayList<>(), new ArrayList<>());
		}
		scenarios.removeIf(e2 -> alreadyFound.stream().anyMatch(e1 -> e1.sameAs(e2)));
		List<Scenario> scenariosToKeep = canonicity.filterScenariosEqualsUpToPermutation(scenarios);
		List<ReportEntry> reportEntries = ListExtensions.map(scenariosToKeep,
				s -> reportEntryFactory.createReportEntry((Goal) ABS2ABSTraceQuery.of(context.traceModel).getOrigin(context.goal), s, galOutput.reachdur,
						AbstractGalReachability.galCommandFromGalOutput(galOutput.consoleOutput.toString()),
						clock.instant()));
		if (invariant.isPresent()) {
			logger.info("longer scenarios: for invariant {}, found {} witnesses.", invariant.get().getInvariant(), scenariosToKeep.size());
		} else {
			logger.info("scenarios: found {} witnesses.", scenariosToKeep.size());
		}
		for (Scenario s : scenariosToKeep) {
			if(logger.isInfoEnabled()) {
				logger.info(s.toReadableString());
			}
			mainMessagingSystem.info("Found a scenario: " + s.toReadableString(), "fr.irisa.atsyra.absystem.gal.transfo");
		}
		if (Duration.between(context.start, clock.instant()).getSeconds() > context.timeout) {
			logger.info("time out.");
			throw new TimeoutException(reportEntries);
		}
		List<Multimap<String, Integer>> possibleValues = mappingsInIntermediateStates(scenarios, stateBuilder, context);
		List<ValueForbiddingInvariant> nextInvariants = possibleValues.stream()
				.flatMap(ValueForbiddingInvariant::invariantsFromPossibleValues).distinct()
				.collect(Collectors.toList());
		if (invariant.isPresent()) {
			for (ValueForbiddingInvariant linv : nextInvariants) {
				linv.addInvariant(invariant.get());
			}
		}
		return new ScenarioOutput(reportEntries, nextInvariants);
	}

	public static List<Multimap<String, Integer>> mappingsInIntermediateStates(List<Scenario> scenarios,
			ABSStateBuilder stateBuilder, ABS2ScenariosInternalContext context) {
		if (scenarios.isEmpty()) {
			return new ArrayList<>();
		} else {
			int length = scenarios.get(0).size() + 1;
			return IntStream.range(0, length).mapToObj(i -> mergeMultiMappings(ListExtensions.map(scenarios,
					s -> mergeMappings(ListExtensions.map(s.getIntermediateStates(i), state -> stateBuilder.state2Mapping(ABS2ABSTraceQuery.of(context.traceModel).getTransformedState(state)))))))
					.collect(Collectors.toList());
		}
	}

	public static Multimap<String, Integer> mergeMappings(Iterable<Map<String, Integer>> mappings) {
		Multimap<String, Integer> result = HashMultimap.create();
		mappings.forEach(m -> m.forEach(result::put));
		return result;
	}

	public static Multimap<String, Integer> mergeMultiMappings(Iterable<Multimap<String, Integer>> mappings) {
		Multimap<String, Integer> result = HashMultimap.create();
		mappings.forEach(result::putAll);
		return result;
	}

	protected void createBuilders(TransfoTraceModel traceModel) {
		this.stateBuilder = new ABSStateBuilder(traceModel);
		this.scenarioBuilder = new ABSScenarioBuilder(traceModel, stateBuilder);
	}

	private class ScenarioCanonicityHelper {
		Multimap<TreeSet<GuardOccurence>, Scenario> scenariosCanonicRepresentation;

		public ScenarioCanonicityHelper() {
			scenariosCanonicRepresentation = HashMultimap.create();
		}

		public List<Scenario> filterScenariosEqualsUpToPermutation(Iterable<Scenario> scenarios) {
			List<Scenario> result = new ArrayList<>();
			for (Scenario scenario : scenarios) {
				TreeSet<GuardOccurence> canonicRepresentation = null;
				for (TreeSet<GuardOccurence> cr : scenariosCanonicRepresentation.keySet()) {
					if (cr.size() == scenario.size() && cr.containsAll(scenario.getActions())) {
						canonicRepresentation = cr;
						break;
					}
				}
				if (canonicRepresentation == null) {
					final TreeSet<GuardOccurence> cr = new TreeSet<>(new GuardOccurenceComparator());
					scenario.getActions().forEach(cr::add);
					result.add(scenario);
					canonicRepresentation = cr;
				}
				scenariosCanonicRepresentation.put(canonicRepresentation, scenario);
			}
			return result;
		}
	}

	public void createScenarioFile(List<Scenario> scenarios, GeneratedFilesHandler generatedFilesHandler, IProgressMonitor monitor)
			throws IOException, CoreException {
		SubMonitor submonitor = SubMonitor.convert(monitor, "Scenario file", 100);
		File scenariosFile = generatedFilesHandler.getFile("scenarios", Optional.empty());
		scenarioBuilder.writeABSScenario2File(scenarios, scenariosFile, submonitor.split(99));
	}
	
	private Optional<BooleanExpression> getInvariantFromDynamicContractsForPrunedGal(ResourceSet absResourceSet, Specification nonflatGalModel) {
		Optional<BooleanExpression> result = getInvariantFromDynamicContracts(absResourceSet);
		result.ifPresent(invariant -> logger.debug(SerializationUtil.getText(invariant, false)));
		return result.map(invariant -> simplifyInvariant(invariant,nonflatGalModel));
	}
	
	private Optional<BooleanExpression> getInvariantFromDynamicContracts(ResourceSet absResourceSet) {
		if(!useDynamicContracts) {
			return Optional.empty();
		}
		Optional<BooleanExpression> result = Optional.empty();
		UnmodifiableIterator<Contract> dynamicContactsIterator = Iterators
				.filter(Iterators.filter(absResourceSet.getAllContents(), Contract.class), Contract::isDynamic);
		if (dynamicContactsIterator.hasNext()) {
			result = Optional.ofNullable(GALBuildHelper
					.createBigAnd(ContractAspect.getGeneratedInstanciatedContracts(dynamicContactsIterator.next())
							.stream().map(EcoreUtil::copy).collect(Collectors.toList())));

		}
		return result;
	}
	
	private BooleanExpression simplifyInvariant(BooleanExpression invariant, Specification nonflatSpec) {
		if(logger.isDebugEnabled()) {
			logger.debug(SerializationUtil.getText(nonflatSpec, false));
		}
		NeverProp invariantProp = GalFactory.eINSTANCE.createNeverProp();
		invariantProp.setPredicate(invariant);
		Property prop = GalFactory.eINSTANCE.createProperty();
		prop.setName("invariant");
		prop.setBody(invariantProp);
		if(logger.isDebugEnabled()) {
			logger.debug(SerializationUtil.getText(nonflatSpec, false));
		}
		nonflatSpec.getProperties().add(prop);
		if(logger.isDebugEnabled()) {
			logger.debug(SerializationUtil.getText(prop, false));
		}
		if(logger.isDebugEnabled()) {
			logger.debug(SerializationUtil.getText(nonflatSpec, false));
		}
		invariant = Simplifier.simplify(invariant, (GALTypeDeclaration) nonflatSpec.getTypes().get(0));
		nonflatSpec.getProperties().remove(prop);
		return invariant;
	}

	protected MessagingSystem getGALMessagingSystem() {
		if(galMessagingSystem == null) {
			MessagingSystemManager msm = new MessagingSystemManager();
			galMessagingSystem = msm.createBestPlatformMessagingSystem("fr.irisa.atsyra.gal", "GAL");
		}
		return galMessagingSystem;
	}
	
	public void setGALMessagingSystem(MessagingSystem messagingSystem) {
		galMessagingSystem = messagingSystem;
	}
}
