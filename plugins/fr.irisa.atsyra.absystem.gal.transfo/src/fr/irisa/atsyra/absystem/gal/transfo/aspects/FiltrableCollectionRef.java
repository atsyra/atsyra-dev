/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.aspects;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.lip6.move.gal.BooleanExpression;
import fr.lip6.move.gal.IntExpression;
import fr.lip6.move.gal.VariableReference;

public abstract class FiltrableCollectionRef {

	private List<Function<IntExpression,BooleanExpression>> conditions;
	
	protected FiltrableCollectionRef() {
		conditions = new ArrayList<>();
	}
	
	protected FiltrableCollectionRef(FiltrableCollectionRef other) {
		conditions = new ArrayList<>(other.conditions);
	}
	
	/**
	 * Returns a expression that evaluates to 1 if the element i ins contained in the (unfiltered) collection
	 */
	public abstract IntExpression getIsContainedIntExpression(IntExpression i);

	/**
	 * Same as above, but may throw UnsuportedOperationException if the collection
	 * is immutable
	 */
	public abstract VariableReference getElementVarRef(IntExpression i);
	
	/**
	 * Returns a expression that evaluates to true if the element i ins contained in the (unfiltered) collection
	 */
	public BooleanExpression getIsContainedExpression(IntExpression i) {
		return GALBuildHelper.createBoolExprIsTrue(getIsContainedIntExpression(i));
	}

	/**
	Add a filter condition to the collection. This filter condition will be accumulated to the previously added ones.
	*/
	public void addFilterCondition(Function<IntExpression, BooleanExpression> condition) {
		if (conditions.isEmpty()) {
			conditions.add(condition);
		} else {
			int top = conditions.size() - 1;
			conditions.add( i ->
				GALBuildHelper.createBoolExprAnd(conditions.get(top).apply(i), condition.apply(EcoreUtil.copy(i)))
			);
		}
	}

	/**
	Returns an optional that is empty if there is no filter condition set on this CollectionRef, or that contains the cumulative filter condition.
	*/
	public Optional<Function<IntExpression, BooleanExpression>> getFilterCondition() {
		if(conditions.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(conditions.get(conditions.size() - 1));
	}

	/**
	 * Calls the copy constructor to return a copy
	 */
	public abstract FiltrableCollectionRef copy();
}
