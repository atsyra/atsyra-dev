/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.Scenario;

public interface ReportEntry {
	public Goal getGoal();
	public Scenario getScenario();
	public Duration getDuration();
	public String getCommand();
	public Instant getTimestamp();
	public String getInvariant();
	public List<Scenario> getPrevious();
}