/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.helpers;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

import com.google.common.collect.Iterators;

import fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetAspect;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetTypeAspect;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.IntConstantAspect;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.StringConstantAspect;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.IntConstant;
import fr.irisa.atsyra.absystem.model.absystem.StringConstant;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandler;

public class AssetMapBuilder {
	private AssetMapBuilder() {
		
	}
	
	public static void createAssetMap(AssetBasedSystem abs, GeneratedFilesHandler generatedFilesHandler, IProgressMonitor monitor)
			throws IOException, OperationCanceledException {
		File file = generatedFilesHandler.getFile("assetmap", Optional.empty());
		EcoreUtil.resolveAll(abs);
		Iterable<AssetBasedSystem> allImportedABS = ABSUtils.getAbsSet(abs);
		StringBuilder sb = new StringBuilder();
		buildAssetSection(sb, allImportedABS);
		buildStringSection(sb, allImportedABS);
		buildIntSection(sb, allImportedABS);
		FileUtils.writeStringToFile(file, sb.toString(), StandardCharsets.UTF_8);
		monitor.done();
	}
	
	private static StringBuilder buildAssetSection(StringBuilder builder, Iterable<AssetBasedSystem> allImportedABS) {
		builder.append("Assets:\n");
		Iterable<AssetType> allAssetTypes = IterableExtensions.<AssetBasedSystem, AssetType>flatMap(allImportedABS,
				(AssetBasedSystem it) ->  () -> Iterators.<AssetType>filter(it.eAllContents(), AssetType.class));
		for (AssetType type : allAssetTypes) {
			builder.append(type.getName()).append(": number = ").append(AssetTypeAspect.getTypeNumber(type)).append(", proper assets = {\n");
			for (Asset asset : AssetTypeAspect.assets(type)) {
				builder.append('\t').append(asset.getName()).append(" = ").append(AssetAspect.properValue(asset)).append(" (proper), ").append(AssetAspect.getGloballyTypedValue(asset)).append(" (global)\n");
			}
			builder.append("}, subtype assets = {\n");
			for (AssetType subtype : AssetTypeAspect.subtypes(type)) {
				if(subtype != type && AssetTypeAspect.count(subtype) > 0) {
					builder.append('\t').append("subtype ").append(subtype.getName()).append(" {\n");
					for (Asset asset : AssetTypeAspect.assets(subtype)) {
						builder.append('\t').append('\t').append(asset.getName()).append(" = ").append(AssetTypeAspect.getSubtypedValue(type, subtype, AssetAspect.properValue(asset))).append("\n");
					}
					builder.append('\t').append("}\n");
				}
			}
			builder.append("}\n");
		}
		return builder;
	}
	
	private static StringBuilder buildStringSection(StringBuilder builder, Iterable<AssetBasedSystem> allImportedABS) {
		builder.append("PrimitiveDataType String {\n");
		final HashSet<String> mappedStrings = new HashSet<>();
		Iterable<StringConstant> allStringConstants = IterableExtensions
				.<AssetBasedSystem, StringConstant>flatMap(allImportedABS, (AssetBasedSystem it) -> () -> Iterators.<StringConstant>filter(it.eAllContents(), StringConstant.class));
		for (StringConstant str : allStringConstants) {
			if (!mappedStrings.contains(str.getValue())) {
				mappedStrings.add(str.getValue());
				builder.append('\t').append(str.getValue()).append(" = ").append(StringConstantAspect.const2int(str)).append("\n");
			}
		}
		builder.append("}\n");
		return builder;
	}
	
	private static StringBuilder buildIntSection(StringBuilder builder, Iterable<AssetBasedSystem> allImportedABS) {
		builder.append("PrimitiveDataType Integer {\n");
		final HashSet<Integer> mappedIntegers = new HashSet<>();
		Iterable<IntConstant> allIntConstants = IterableExtensions
				.<AssetBasedSystem, IntConstant>flatMap(allImportedABS, (AssetBasedSystem it) -> () -> Iterators.<IntConstant>filter(it.eAllContents(), IntConstant.class));
		for (IntConstant cst : allIntConstants) {
			if (!mappedIntegers.contains(cst.getValue())) {
				mappedIntegers.add(cst.getValue());
				builder.append('\t').append(cst.getValue()).append(" = ").append(IntConstantAspect.const2int(cst)).append("\n");
			}
		}
		builder.append("}\n");
		return builder;
	}
}
