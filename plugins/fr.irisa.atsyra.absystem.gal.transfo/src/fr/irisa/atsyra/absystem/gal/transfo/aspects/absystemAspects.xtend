package fr.irisa.atsyra.absystem.gal.transfo.aspects

import com.google.common.collect.HashMultimap
import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.inria.diverse.k3.al.annotationprocessor.OverrideAspectMethod
import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.AssetTypeFeatureLink
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.ConstantLink
import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType
import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory
import fr.irisa.atsyra.absystem.model.absystem.Action
import fr.irisa.atsyra.absystem.model.absystem.AndExpression
import fr.irisa.atsyra.absystem.model.absystem.Asset
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup
import fr.irisa.atsyra.absystem.model.absystem.AssetLink
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference
import fr.irisa.atsyra.absystem.model.absystem.BinaryExpression
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression
import fr.irisa.atsyra.absystem.model.absystem.Contract
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup
import fr.irisa.atsyra.absystem.model.absystem.EnumConstant
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral
import fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression
import fr.irisa.atsyra.absystem.model.absystem.Expression
import fr.irisa.atsyra.absystem.model.absystem.Goal
import fr.irisa.atsyra.absystem.model.absystem.Guard
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction
import fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression
import fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression
import fr.irisa.atsyra.absystem.model.absystem.IntConstant
import fr.irisa.atsyra.absystem.model.absystem.LambdaAction
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression
import fr.irisa.atsyra.absystem.model.absystem.LambdaParameter
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity
import fr.irisa.atsyra.absystem.model.absystem.NotExpression
import fr.irisa.atsyra.absystem.model.absystem.OrExpression
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType
import fr.irisa.atsyra.absystem.model.absystem.StringConstant
import fr.irisa.atsyra.absystem.model.absystem.Symbol
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant
import fr.irisa.atsyra.absystem.model.absystem.util.ABSExpressionTypeProvider
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils
import fr.irisa.atsyra.absystem.model.absystem.util.ExpressionType
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel
import fr.lip6.move.gal.ArrayPrefix
import fr.lip6.move.gal.BooleanExpression
import fr.lip6.move.gal.ComparisonOperators
import fr.lip6.move.gal.GALTypeDeclaration
import fr.lip6.move.gal.GalFactory
import fr.lip6.move.gal.IntExpression
import fr.lip6.move.gal.Label
import fr.lip6.move.gal.ParamRef
import fr.lip6.move.gal.Parameter
import fr.lip6.move.gal.ReachableProp
import fr.lip6.move.gal.Specification
import fr.lip6.move.gal.Statement
import fr.lip6.move.gal.Transition
import fr.lip6.move.gal.TypedefDeclaration
import fr.lip6.move.gal.Variable
import fr.lip6.move.gal.VariableReference
import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Optional
import java.util.Set
import java.util.TreeSet
import java.util.function.Function
import java.util.stream.Collectors
import java.util.stream.IntStream
import java.util.stream.Stream
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtend.lib.annotations.Data
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1

import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AbstractAssetTypeAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.ActionAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetBasedSystemAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetGroupAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetLinkAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetTypeAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetTypeAttributeAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetTypeFeatureAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetTypeReferenceAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.ConstantExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.ContractAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.DefinitionGroupAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.EnumLiteralAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.ExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.ExpressionTypeAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.GoalAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.GuardAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.GuardParameterAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.GuardedActionAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.IntConstantAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.LambdaActionAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.PrimitiveDataTypeAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.StringConstantAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.SymbolAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.VersionConstantAspect.*
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.Collection
import java.util.Objects
import fr.irisa.atsyra.absystem.model.absystem.Version
import org.eclipse.xtext.EcoreUtil2

class ExpressionTypeAspect {
	static def generatedTypedef(ExpressionType type) {
		if (type.isPrimitiveType) {
			val ptype = type.type as PrimitiveDataType
			ptype.generatedTypedef
		} else if (type.isAssetType) {
			val atype = type.type as AssetType
			atype.generatedTypedef
		} else {
			throw new NullPointerException()
		}
	}
	
	static def int count(ExpressionType type) {
		if (type.isPrimitiveType) {
			val ptype = type.type as PrimitiveDataType
			ptype.count
		} else if (type.isAssetType) {
			val atype = type.type as AssetType
			atype.count
		} else {
			0
		}
	}
	
	static def int memberMultiplicityManyIndex(ExpressionType type, int asset, int member) {
		asset * type.count + member
	}

	static def subtypes(ExpressionType type) {
		if (type.isAssetType) {
			val atype = type.type as AssetType
			atype.subtypes.map[ExpressionType.create(type.collection, it)]
		} else {
			List.of(type)
		}
	}

	static def getGloballyTypedValue(ExpressionType type, IntExpression proper) {
		if (type.isAssetType) {
			val atype = type.type as AssetType
			atype.getGloballyTypedValue(proper)
		} else {
			proper
		}
	}
}

@Aspect(className=AssetType)
class AssetTypeAspect extends AbstractAssetTypeAspect {
	public ArrayList<Asset> assets;
	HashMap<String, ArrayPrefix> generatedArrays;
	HashMap<String, AssetTypeReference> properties;
	HashMap<String, AssetTypeAttribute> attributes;
	HashMap<String, AssetType> membersBaseType;
	public int count;
	public TypedefDeclaration generatedTypedef;
	public ArrayList<AssetType> subtypes;
	public AssetBasedSystem abs;

	def void initialize(AssetBasedSystem abs) {
		_self.assets = new ArrayList<Asset>();
		_self.generatedArrays = new HashMap<String, ArrayPrefix>();
		_self.membersBaseType = new HashMap<String, AssetType>();
		_self.properties = new HashMap<String, AssetTypeReference>();
		_self.attributes = new HashMap<String, AssetTypeAttribute>();
		_self.count = 0;
		_self.subtypes = new ArrayList<AssetType>();
		_self.subtypes.add(_self);
		_self.abs = abs
	}

	def void addProperty(String name, AssetTypeReference property, AssetType baseType) {
		_self.properties.put(name, property)
		_self.membersBaseType.put(name, baseType)
	}

	def AssetTypeReference getProperty(String name) {
		_self.properties.get(name)
	}

	def void addAttribute(String name, AssetTypeAttribute attribute, AssetType baseType) {
		_self.attributes.put(name, attribute)
		_self.membersBaseType.put(name, baseType)
	}

	def AssetTypeAttribute getAttribute(String name) {
		_self.attributes.get(name)
	}

	def AssetType getMemberBaseType(String name) {
		if (_self.membersBaseType.containsKey(name))
			_self.membersBaseType.get(name)
		else
			throw new RuntimeException("Error in getMemberBaseType: " + name + " is not a member of " +
				_self.qualifiedName)
	}

	def void addGeneratedArrayMultiplicityOne(String name, ArrayPrefix array) {
		_self.generatedArrays.put(name, array)
	}

	def void addGeneratedArrayMultiplicityMany(ExpressionType memberType, String name, ArrayPrefix array) {
		_self.generatedArrays.put(memberType.typeName + "." + name, array)
	}

	def ArrayPrefix getGeneratedArrayMultiplicityOne(String name) {
		_self.generatedArrays.get(name)
	}

	def ArrayPrefix getGeneratedArrayMultiplicityMany(ExpressionType memberType, String name) {
		_self.generatedArrays.get(memberType.typeName + "." + name)
	}

	def int getCountWithSubtypes() {
		_self.subtypes.fold(0, [i, type|i + type.count])
	}

	def int getSubtypedArraySize() {
		_self.subtypes.size * _self.subtypes.map[it.count].max
	}

	def Optional<Asset> getSubtypedAsset(int subtypedvalue) {
		val type = subtypedvalue % _self.subtypes.size
		val proper = subtypedvalue / _self.subtypes.size
		if (proper < _self.subtypes.get(type).assets.size) {
			Optional.of(_self.subtypes.get(type).assets.get(proper))
		} else {
			Optional.empty
		}
	}

	def String getQualifiedName() {
		EcoreUtil2.getContainerOfType(_self, DefinitionGroup).name + "_" + _self.name
	}

	def void convert2GAL() {
		if (_self.count > 0) {
			_self.generatedTypedef = GALBuildHelper.createTypeDefDeclaration(_self.qualifiedName, 0, _self.count - 1);
			_self.abs.generatedGALTypeDeclaration.typedefs.add(_self.generatedTypedef)
		}
		_self.assetTypeAttributes.forEach[t|t.setStatic(); t.convert2GAL(_self)]
		_self.assetTypeProperties.forEach[t|t.setStatic(); t.convert2GAL(_self)]
	}

	def void createTrace(ABS2GALTraceBuilder tracebuilder) {
		tracebuilder.addAssetTypeLink(_self.name, _self, _self.arrayofSubtypes, Optional.ofNullable(_self.generatedTypedef), _self.count, _self.getTypeNumber())
	}

	def void countProper(Asset asset) {
		asset.properValue = _self.count
		_self.assets.add(asset);
		_self.count = _self.count + 1;
	}

	@OverrideAspectMethod
	def void registerSubtypes() {
		ABSUtils.getAllSubtypes(_self).toList.sortBy[name].forEach[
			if(!_self.subtypes.contains(it)) _self.subtypes.add(it)
		]
	}

	def void registerAsset(Asset asset) {
		// Replace the asset in position asset.properValue by asset
		_self.assets.remove(asset.properValue)
		_self.assets.add(asset.properValue, asset);
	}

	def ExpressionType getMemberType(String member) {
		if (_self.properties.containsKey(member)) {
			ExpressionType.typeOf(_self.properties.get(member))
		} else if (_self.attributes.containsKey(member)) {
			ExpressionType.typeOf(_self.attributes.get(member))
		} else {
			throw new RuntimeException("Error in memberSelection: " + member + " is not a member of " + _self.name)
		}
	}

	def int getIndexOfSubtype(AssetType subtype) {
		_self.subtypes.indexOf(subtype)
	}

	def IntExpression getIndexOfSubtype(IntExpression subtype) {
		GALBuildHelper.createArrayRef(_self.getArrayofSubtypes, subtype)
	}

	ArrayPrefix subtypesArray

	def ArrayPrefix getArrayofSubtypes() {
		if (_self.subtypesArray === null) {
			_self.subtypesArray = GALBuildHelper.createArray(_self.name + "_types",
				GALBuildHelper.createConstant(_self.abs.nbTypes))
			for (type : _self.abs.allTypes) {
				_self.subtypesArray.values.add(GALBuildHelper.createConstant(_self.subtypes.indexOf(type)))
			}
			_self.abs.generatedGALTypeDeclaration.arrays.add(_self.subtypesArray)
		}
		_self.subtypesArray
	}

	def int getSubtypedValue(AssetType subtype, int properValue) {
		_self.getIndexOfSubtype(subtype) + _self.subtypes.size * properValue
	}

	def IntExpression getSubtypedValue(AssetType subtype, IntExpression properValue) {
		GALBuildHelper.createOperation(
			GALBuildHelper.createOperation(properValue, GALBuildHelper.createConstant(_self.subtypes.size), "*"),
			GALBuildHelper.createConstant(_self.getIndexOfSubtype(subtype)), "+")
	}

	def IntExpression getSubtypedValue(IntExpression subtypeNumber, IntExpression properValue) {
		GALBuildHelper.createOperation(
			GALBuildHelper.createOperation(properValue, GALBuildHelper.createConstant(_self.subtypes.size), "*"),
			_self.getIndexOfSubtype(subtypeNumber), "+")
	}

	int typeNumber

	def int getTypeNumber() {
		_self.typeNumber()
	}

	def void registerTypeNumber(List<AssetType> alltypes) {
		_self.typeNumber = alltypes.indexOf(_self)
	}

	def int getGloballyTypedValue(int properValue) {
		properValue * _self.abs.nbTypes + _self.typeNumber()
	}

	def IntExpression getGloballyTypedValue(IntExpression properValue) {
		GALBuildHelper.ProperValue2Global(properValue, _self, _self.abs.nbTypes)
	}

	def BooleanExpression typeTest(IntExpression typeExpr) {
		GALBuildHelper.createBoolExprComp(typeExpr, GALBuildHelper.createConstant(_self.typeNumber()),
			ComparisonOperators.EQ)
	}
}

@Aspect(className=AssetTypeReference)
class AssetTypeReferenceAspect extends AssetTypeFeatureAspect {

	def void convert2GAL(AssetType assetType) {
		assetType.subtypes.forEach[t|t.addProperty(_self.name, _self, assetType)]
		switch (_self.multiplicity) {
			case ONE,
			case ZERO_OR_ONE: {
				_self.convert2GALMultiplicityOne(assetType)
			}
			case ONE_OR_MANY,
			case ZERO_OR_MANY: {
				_self.propertyType.subtypes.forEach [ t2 |
					_self.convert2GALMultiplicityMany(assetType, t2)
				]
			}
		}
	}

	def void convert2GALMultiplicityOne(AssetType firstType) {
		val String GALname = firstType.qualifiedName + "." + _self.name;
		val arraySize = firstType.subtypedArraySize
		if (arraySize > 0) {
			val generatedArray = GALBuildHelper.createArray(GALname, GALBuildHelper.createConstant(arraySize))
			// Initial values, from links
			for (var i = 0; i < arraySize; i++) { // TODO replace with an iterator over subtyped assets
				val tryasset = firstType.getSubtypedAsset(i)
				if (!tryasset.empty) {
					val asset = tryasset.get
					val linkedAssets = asset.links.get(_self.name)
					if (linkedAssets.empty) {
						generatedArray.values.add(GALBuildHelper.createConstant(GALBuildHelper.galNone))
						if (!_self.isStatic) {
							_self.abs.uninitializedCells.addCellInfo(generatedArray, i, _self.hasDefault,
								_self.multiplicity, ExpressionType.typeOf(_self))
						}
					} else {
						val linkedAsset = linkedAssets.get(0)
						val GALValue = linkedAsset.getGloballyTypedValue()
						generatedArray.values.add(GALBuildHelper.createConstant(GALValue))
					}
				} else { // No assets, this cell is empty
					generatedArray.values.add(GALBuildHelper.createConstant(GALBuildHelper.galNone))
				}
			}
			_self.abs.generatedGALTypeDeclaration.arrays.add(generatedArray);
			firstType.subtypes.forEach[t|t.addGeneratedArrayMultiplicityOne(_self.name, generatedArray)]
			_self.featureLinks.add(new AssetTypeFeatureLink(_self.name, _self, Optional.empty, generatedArray))
		}
	}

	def void convert2GALMultiplicityMany(AssetType firstType, AssetType secondType) {
		val firstTypeSize = firstType.subtypedArraySize
		val arraySize = firstTypeSize * secondType.count
		if (arraySize > 0) {
			val String GALname = firstType.qualifiedName + "." + secondType.qualifiedName + "." + _self.name;
			val generatedArray = GALBuildHelper.createArray(GALname, GALBuildHelper.createConstant(arraySize))
			// Initial values, from links
			for (var i = 0; i < firstTypeSize; i++) {
				val tryasset = firstType.getSubtypedAsset(i)
				if (!tryasset.empty) {
					val asset = tryasset.get
					if (_self.multiplicity == Multiplicity.ONE_OR_MANY && !_self.isStatic)
						_self.abs.CollectionsMultiplicityOneOrMany.add(
							new ArrayCollectionRef(generatedArray, GALBuildHelper.createConstant(i), secondType.count))
					val linkedAssets = asset.links.get(_self.name)
					for (var j = 0; j < secondType.count; j++) {
						if (linkedAssets.empty && !_self.isStatic) {
							_self.abs.uninitializedCells.addCellInfo(generatedArray, i * secondType.count + j,
								_self.hasDefault, _self.multiplicity, ExpressionType.typeOf(_self))
						}
						val otherAsset = secondType.assets.get(j)
						val contained = if (linkedAssets.contains(otherAsset))
								GALBuildHelper.galTrue
							else
								GALBuildHelper.galFalse
						generatedArray.values.add(GALBuildHelper.createConstant(contained))
					}
				} else { // No assets, this cell is empty
					for (var j = 0; j < secondType.count; j++) {
						generatedArray.values.add(GALBuildHelper.createConstant(GALBuildHelper.galNone))
					}
				}
			}
			_self.abs.generatedGALTypeDeclaration.arrays.add(generatedArray);
			val mtype = ExpressionType.collection(secondType)
			firstType.addGeneratedArrayMultiplicityMany(mtype, _self.name, generatedArray)
			_self.featureLinks.add(new AssetTypeFeatureLink(_self.name, _self, Optional.of(secondType), generatedArray))
		}
	}

	def Set<Pair<ArrayPrefix, Integer>> referredCells(Expression receiver, Context context) {
		val rectype = receiver.getTypeForConversion(context).type as AssetType
		val propertybasetype = rectype.getMemberBaseType(_self.name)
		val possibleProperValues = receiver.referredProperValues(context)
		val possibleSubtypedValues = possibleProperValues.map[propertybasetype.getSubtypedValue(rectype, it)]
		switch (_self.multiplicity) {
			case ONE,
			case ZERO_OR_ONE: {
				possibleSubtypedValues.map[rectype.getGeneratedArrayMultiplicityOne(_self.name) -> it].toSet
			}
			case ONE_OR_MANY,
			case ZERO_OR_MANY: {
				_self.propertyType.subtypes.fold(new HashSet<Pair<ArrayPrefix, Integer>>(), [ set, type |
					set.addAll(possibleProperValues.flatMap [ assetValue |
						IntStream.range(0, type.count).mapToObj [ memberValue |
							val mtype = ExpressionType.collection(type)
							rectype.getGeneratedArrayMultiplicityMany(mtype, _self.name) ->
								mtype.memberMultiplicityManyIndex(assetValue, memberValue)
						].collect(Collectors.toSet)
					]);
					set
				])
			}
		}
	}
}

@Aspect(className=AssetBasedSystem)
class AssetBasedSystemAspect {
	public UninitializedCellsInfo uninitializedCells
	public ArrayList<ArrayCollectionRef> CollectionsMultiplicityOneOrMany
	public HashMap<String, Integer> nameRepetition
	public ArrayList<BooleanExpression> contracts
	public GALTypeDeclaration generatedGALTypeDeclaration
	public ABS2GALTraceBuilder tracebuilder
	List<AssetType> allTypes
	int nbTypes
	public ABSExpressionTypeProvider typeProvider

	def void initialize() {
		_self.uninitializedCells = new UninitializedCellsInfo()
		_self.CollectionsMultiplicityOneOrMany = new ArrayList<ArrayCollectionRef>()
		_self.nameRepetition = new HashMap<String, Integer>();
		_self.contracts = new ArrayList<BooleanExpression>()
		EcoreUtil.resolveAll(_self.eResource.resourceSet);
		_self.allTypes = new ArrayList<AssetType>()
		_self.typeProvider = new ABSExpressionTypeProvider(_self)
		_self.conver2GALImported [
			it.eAllContents.forEach [
				switch it {
					Goal: it.initialize(_self)
					Asset: it.initialize(_self)
					AssetType: it.initialize(_self)
					PrimitiveDataType: it.initialize(_self)
					Guard: it.initialize(_self)
					Expression: it.initialize(_self)
					AssetTypeFeature: it.initialize(_self)
					Action: it.initialize(_self)
				}
			]
		]
		//PrimitiveDataType that might not be defined in the model, but that still should be initialized
		val boolType = _self.typeProvider.getTypeOf(AbsystemFactory.eINSTANCE.createBooleanConstant).type as PrimitiveDataType
		boolType.initialize(_self)
		val intType = _self.typeProvider.getTypeOf(AbsystemFactory.eINSTANCE.createIntConstant).type as PrimitiveDataType
		intType.initialize(_self)
		val stringType = _self.typeProvider.getTypeOf(AbsystemFactory.eINSTANCE.createStringConstant).type as PrimitiveDataType
		stringType.initialize(_self)
		val versionType = _self.typeProvider.getTypeOf(AbsystemFactory.eINSTANCE.createVersionConstant).type as PrimitiveDataType
		versionType.initialize(_self)
	}

	def List<AssetType> getAllTypes() {
		_self.allTypes().immutableCopy
	}

	def Specification convertGoal2GAL(Goal goal, TransfoTraceModel traceModel) {
		val gal = _self.convertSystem2GAL(traceModel)
		val spec = GalFactory.eINSTANCE.createSpecification
		spec.types.add(gal)
		spec.main = gal;
		return goal.convert2GAL(gal, spec)
	}

	def Map<String, Specification> convertAllGoals2GAL(TransfoTraceModel traceModel) {
		val gal = _self.convertSystem2GAL(traceModel)
		// for each goal, copy the system and convert the goal
		val res = new HashMap<String, Specification>()
		_self.assetGroups.forEach [
			it.goals.forEach [
				val galc = EcoreUtil.copy(gal)
				val spec = GalFactory.eINSTANCE.createSpecification
				spec.types.add(galc)
				spec.main = galc;
				res.put(it.name, it.convert2GAL(galc, spec))
			]
		]
		return res
	}

	def GALTypeDeclaration convertSystem2GAL(TransfoTraceModel traceModel) {
		// Initialization of some variables
		_self.tracebuilder = new ABS2GALTraceBuilder(traceModel);
		_self.initialize()
		_self.generatedGALTypeDeclaration = GalFactory.eINSTANCE.createGALTypeDeclaration => [
			name = "abs"
		]
		// count the primitive data types elements
		_self.countPrimitiveTypeElements()
		// subtypes hierarchy
		_self.conver2GALImported[it.definitionGroups.forEach[g|g.registerSubtypes()]]
		// register all types
		_self.conver2GALImported [
			it.definitionGroups.forEach[g|g.assetTypes.filter(AssetType).forEach[type|_self.allTypes().add(type)]]
		]
		_self.nbTypes = _self.allTypes().size
		// Conversion to GAL
		// Convert from assetGroups: first, counting the assets of each type, and build the type hierarchy
		_self.conver2GALImported[it.assetGroups.forEach[g|g.count()]]
		_self.conver2GALImported [
			it.definitionGroups.forEach [ g |
				g.assetTypes.filter(AssetType).forEach[type|type.registerTypeNumber(_self.getAllTypes)]
			]
		]
		// Then, assign the Galvalue of each asset for each of its supertypes, and convert the asset links
		_self.conver2GALImported[it.assetGroups.forEach[it.assetLinks.forEach[t|t.registerLink()]]]
		_self.conver2GALImported[it.assetGroups.forEach[it.assets.forEach[t|t.convert2GAL()]]]
		// Convert from definitionGroups: arrays, typedefs, guards, contracts
		_self.conver2GALImported[it.definitionGroups.forEach[it.assetTypes.forEach[t|t.convert2GAL()]]]
		_self.conver2GALImported[it.definitionGroups.forEach[it.guardedActions.forEach[t|t.convert2GAL()]]]
		_self.conver2GALImported[it.definitionGroups.forEach[it.contracts.forEach[t|t.convert2GAL()]]]

		// Traceability mapping
		_self.conver2GALImported [
			it.eAllContents.filter(AssetType).forEach[it.createTrace(_self.tracebuilder)]
			it.eAllContents.filter(PrimitiveDataType).forEach[it.createTrace(_self.tracebuilder)]
			it.eAllContents.filter(Asset).forEach[it.createTrace(_self.tracebuilder)]
			it.eAllContents.filter(GuardedAction).forEach[it.createTrace(_self.tracebuilder)]
			it.eAllContents.filter(AssetTypeFeature).forEach[it.createTrace(_self.tracebuilder)]
		]
		return _self.generatedGALTypeDeclaration
	}
	
	def void countPrimitiveTypeElements() {
		val boolType = _self.typeProvider.getTypeOf(AbsystemFactory.eINSTANCE.createBooleanConstant).type as PrimitiveDataType
		boolType.countElements(_self)
		val intType = _self.typeProvider.getTypeOf(AbsystemFactory.eINSTANCE.createIntConstant).type as PrimitiveDataType
		intType.countElements(_self)
		val stringType = _self.typeProvider.getTypeOf(AbsystemFactory.eINSTANCE.createStringConstant).type as PrimitiveDataType
		stringType.countElements(_self)
		val versionType = _self.typeProvider.getTypeOf(AbsystemFactory.eINSTANCE.createVersionConstant).type as PrimitiveDataType
		versionType.countElements(_self)
		_self.conver2GALImported [
			it.eAllContents.filter(EnumDataType).forEach [
				it.countElements(_self)
			]
		]
	}

	/* Apply a conversion procedure to all the referenced abs, i.e. _self and imports
	 * @param convertion a procedure taking an (imported) AssetBasedSystem for conversion
	 */
	def void conver2GALImported(Procedure1<AssetBasedSystem> convertion) {
		for (r : _self.eResource.resourceSet.resources) {
			if (!r.contents.empty && r.contents.get(0) instanceof AssetBasedSystem) {
				val abs = r.contents.get(0) as AssetBasedSystem
				convertion.apply(abs)
			}
		}

	}

	def int getNbTypes() {
		_self.nbTypes()
	}
}

@Aspect(className=Asset)
class AssetAspect extends SymbolAspect {
	public HashMultimap<String, Asset> links;
	public HashMultimap<String, ConstantExpression> attributeValues;
	public int properValue;
	protected AssetBasedSystem abs;

	def void initialize(AssetBasedSystem abs) {
		_self.links = HashMultimap.create();
		_self.attributeValues = HashMultimap.create();
		_self.abs = abs
	}

	def String getQualifiedName() {
		EcoreUtil2.getContainerOfType(_self, AssetGroup).name + "_" + _self.name
	}

	def void count() {
		_self.assetType.countProper(_self)
	}

	def void convert2GAL() {
		_self.assetType.registerAsset(_self)
		_self.assetAttributeValues.forEach [ assetAttributeValue |
			assetAttributeValue.values.forEach [ value |
				_self.attributeValues.put(assetAttributeValue.attributeType.name, value)
			]
		]
	}

	@OverrideAspectMethod
	def ExpressionType getTypeForConversion(Context context) {
		ExpressionType.simple(_self.assetType)
	}

	def IntExpression assetRef2int(Context context) {
		GALBuildHelper.createConstant(_self.properValue)
	}

	def int getGloballyTypedValue() {
		_self.assetType.getGloballyTypedValue(_self.properValue)
	}

	@OverrideAspectMethod
	def Set<Integer> referredProperValues(Context context) {
		Set.of(_self.properValue)
	}

	def void createTrace(ABS2GALTraceBuilder tracebuilder) {
		tracebuilder.addAssetLink(_self.name, _self, _self.properValue, _self.globallyTypedValue)
	}
}

@Aspect(className=AssetLink)
class AssetLinkAspect {
	def void registerLink() {
		_self.sourceAsset.links.put(_self.referenceType.name, _self.targetAsset)
		if(_self.oppositeReferenceType !== null) {
			_self.targetAsset.links.put(_self.oppositeReferenceType.name, _self.sourceAsset)
		}
	}
}

@Aspect(className=AssetTypeAttribute)
class AssetTypeAttributeAspect extends AssetTypeFeatureAspect {
	def void convert2GAL(AssetType assetType) {
		assetType.subtypes.forEach[t|t.addAttribute(_self.name, _self, assetType)]
		switch (_self.multiplicity) {
			case ONE,
			case ZERO_OR_ONE: {
				_self.convert2GALMultiplicityOne(assetType)
			}
			case ONE_OR_MANY,
			case ZERO_OR_MANY: {
				_self.convert2GALMultiplicityMany(assetType, _self.attributeType)
			}
		}
	}

	def void convert2GALMultiplicityOne(AssetType firstType) {
		val arraySize = firstType.subtypedArraySize
		if (arraySize > 0) {
			val String GALname = firstType.qualifiedName + "." + _self.name;
			val generatedArray = GALBuildHelper.createArray(GALname, GALBuildHelper.createConstant(arraySize))
			// Initial values, from featuresMap and defaultValues
			for (var i = 0; i < arraySize; i++) {
				val tryasset = firstType.getSubtypedAsset(i)
				if (!tryasset.empty) {
					val asset = tryasset.get
					val values = asset.attributeValues.get(_self.name)
					if (values.empty && !_self.isStatic) {
						_self.abs.uninitializedCells.addCellInfo(generatedArray, i, _self.hasDefault,
							_self.multiplicity, ExpressionType.typeOf(_self))
					}
					val value = if (values.empty && _self.defaultValues.empty) {
							GALBuildHelper.galNone
						} else if (values.empty) {
							_self.defaultValues.get(0).const2int()
						} else {
							values.get(0).const2int()
						}
					generatedArray.values.add(GALBuildHelper.createConstant(value))
				} else { // No asset, the cell is empty
					generatedArray.values.add(GALBuildHelper.createConstant(GALBuildHelper.galNone))
				}
			}
			_self.abs.generatedGALTypeDeclaration.arrays.add(generatedArray);
			firstType.subtypes.forEach[t|t.addGeneratedArrayMultiplicityOne(_self.name, generatedArray)]
			_self.featureLinks.add(new AssetTypeFeatureLink(_self.name, _self, Optional.empty, generatedArray))
		}
	}

	def void convert2GALMultiplicityMany(AssetType firstType, PrimitiveDataType secondType) {
		val firstTypeSize = firstType.subtypedArraySize
		val arraySize = firstTypeSize * secondType.count
		if (arraySize > 0) {
			val String GALname = firstType.qualifiedName + "." + _self.name;
			val generatedArray = GALBuildHelper.createArray(GALname, GALBuildHelper.createConstant(arraySize))
			// Initial values, from featuresMap and defaultValues
			for (var i = 0; i < firstTypeSize; i++) {
				val tryasset = firstType.getSubtypedAsset(i)
				if (!tryasset.empty) {
					val asset = tryasset.get
					if (_self.multiplicity == Multiplicity.ONE_OR_MANY && !_self.isStatic)
						_self.abs.CollectionsMultiplicityOneOrMany.add(
							new ArrayCollectionRef(generatedArray, GALBuildHelper.createConstant(i), secondType.count))
					val values = asset.attributeValues.get(_self.name)
					for (var j = 0; j < secondType.count; j++) {
						if (values.empty && !_self.isStatic) {
							_self.abs.uninitializedCells.addCellInfo(generatedArray, i * secondType.count + j,
								_self.hasDefault, _self.multiplicity, ExpressionType.typeOf(_self))
						}
						val intvalues = if (values.empty) {
								_self.defaultValues.map[it.const2int()]
							} else {
								values.map[it.const2int()]
							}
						val contained = if (intvalues.contains(j))
								GALBuildHelper.galTrue
							else
								GALBuildHelper.galFalse
						generatedArray.values.add(GALBuildHelper.createConstant(contained))
					}
				} else { // No asset, the cells are empty
					for (var j = 0; j < secondType.count; j++) {
						generatedArray.values.add(GALBuildHelper.createConstant(GALBuildHelper.galNone))
					}
				}
			}
			_self.abs.generatedGALTypeDeclaration.arrays.add(generatedArray);
			val mtype = ExpressionType.collection(secondType)
			firstType.addGeneratedArrayMultiplicityMany(mtype, _self.name, generatedArray)
			_self.featureLinks.add(new AssetTypeFeatureLink(_self.name, _self, Optional.empty, generatedArray))
		}
	}

	def Set<Pair<ArrayPrefix, Integer>> referredCells(Expression receiver, Context context) {
		val rectype = receiver.getTypeForConversion(context).type as AssetType
		val propertybasetype = rectype.getMemberBaseType(_self.name)
		val possibleProperValues = receiver.referredProperValues(context)
		val possibleSubtypedValues = possibleProperValues.map[propertybasetype.getSubtypedValue(rectype, it)]
		switch (_self.multiplicity) {
			case ONE,
			case ZERO_OR_ONE: {
				possibleSubtypedValues.map[rectype.getGeneratedArrayMultiplicityOne(_self.name) -> it].toSet
			}
			case ONE_OR_MANY,
			case ZERO_OR_MANY: {
				val mtype = ExpressionType.typeOf(_self)
				possibleProperValues.flatMap [ assetValue |
					IntStream.range(0, mtype.count).mapToObj [ memberValue |
						rectype.getGeneratedArrayMultiplicityMany(mtype, _self.name) ->
							mtype.memberMultiplicityManyIndex(assetValue, memberValue)
					].collect(Collectors.toSet)
				].toSet
			}
		}
	}
}

@Aspect(className=Contract)
class ContractAspect extends GuardAspect {
	def void convert2GAL() {
		if (!_self.dynamic)
			return;
		var contexts = new HashSet<Context>()
		contexts.add(new Context())
		for (GuardParameter p : _self.guardParameters) {
			val next = new HashSet<Context>()
			contexts.forEach [ c |
				next.addAll(p.generateParameter(c))
			]
			contexts = next
		}
		val contracts = contexts.toList.map[it -> _self.guardExpression.expr2boolExpr(it)]
		val instantiated = contracts.map[_self.instantiateParameters(it.value, it.key)]
		_self.abs.contracts.addAll(instantiated)
	}

	def BooleanExpression instantiateParameters(BooleanExpression expr, Context context) {
		val paramMap = _self.getParametersMap(context)
		if (_self.guardParameters.empty) {
			expr
		} else {
			GALBuildHelper.createBigAnd(paramMap.map [ m |
				val exprcpy = EcoreUtil.copy(expr)
				exprcpy.eAllContents.filter(ParamRef).forEach [
					it.eContainer.eSet(it.eContainingFeature, GALBuildHelper.createConstant(m.get(it.refParam)))
				]
				exprcpy
			])
		}
	}

	def List<Map<Parameter, Integer>> getParametersMap(Context context) {
		val result = new ArrayList<Map<Parameter, Integer>>()
		result.add(new HashMap<Parameter, Integer>())
		_self.guardParameters.map [ param |
			IntStream.range(0, context.getParameterType(param).count).boxed.map[i|context.getParameter(param) -> i].
				collect(Collectors.toList)
		].fold(result, [ List<Map<Parameter, Integer>> r, list |
			r.flatMap [ Map<Parameter, Integer> m |
				list.map [ pair |
					val Map<Parameter, Integer> localresult = new HashMap<Parameter, Integer>()
					localresult.putAll(m)
					localresult.put(pair.key, pair.value)
					localresult
				]
			].toList
		])
	}
	
	def List<BooleanExpression> getGeneratedInstanciatedContracts() {
		_self.abs.contracts
	}
}

@Aspect(className=AssetTypeFeature)
abstract class AssetTypeFeatureAspect {
	boolean staticf = false;
	AssetBasedSystem abs;
	protected List<AssetTypeFeatureLink> featureLinks

	def void initialize(AssetBasedSystem abs) {
		_self.abs = abs
		_self.featureLinks = newArrayList
	}

	def AssetBasedSystem getAbs() {
		_self.abs()
	}

	def void setStatic() {
		_self.staticf = true;
	}

	def boolean isStatic() {
		_self.staticf
	}

	abstract def Set<Pair<ArrayPrefix, Integer>> referredCells(Expression receiver, Context context)

	def void createTrace(ABS2GALTraceBuilder tracebuilder) {
		_self.featureLinks.forEach[tracebuilder.addAssetTypeFeatureLink(it)]
	}
}

@Aspect(className=PrimitiveDataType)
class PrimitiveDataTypeAspect {
	public int count
	public TypedefDeclaration generatedTypedef
	protected AssetBasedSystem abs
	protected List<ConstantLink> cstlinks

	def void initialize(AssetBasedSystem abs) {
		_self.count = 0
		_self.abs = abs
		_self.cstlinks = newArrayList
	}

	def String getQualifiedName() {
		EcoreUtil2.getContainerOfType(_self, DefinitionGroup).name + "_" + _self.name
	}

	def void countElements(AssetBasedSystem abs) {
		switch _self.name {
			case "Boolean": {
				_self.count = 2
				abs.conver2GALImported [
					it.eAllContents.filter(BooleanConstant).forEach [ bcst |
						_self.cstlinks.add(new ConstantLink(bcst.value, bcst, bcst.value == "true"?1:0))
					]
				]
			}
			case "String": {
				val values = new HashMap<String, Integer>()
				abs.conver2GALImported [
					it.eAllContents.filter(StringConstant).forEach [ stringcst |
						stringcst.register(values)
						_self.cstlinks.add(new ConstantLink(stringcst.value, stringcst, values.get(stringcst.value)))
					]
				]
				_self.count = values.size
			}
			case "Integer": {
				// TreeSet because it is ordered
				val values = new TreeSet<Integer>()
				abs.conver2GALImported [
					it.eAllContents.filter(IntConstant).forEach [
						values.add(it.value)
					]
				]
				_self.count = values.size
				val galValues = new HashMap<Integer, Integer>()
				var i = 0;
				for (value : values) {
					galValues.put(value, i)
					i++
				}
				abs.conver2GALImported [
					it.eAllContents.filter(IntConstant).forEach[ intcst |
						intcst.register(galValues.get(intcst.value))
						_self.cstlinks.add(new ConstantLink(intcst.value.toString, intcst, galValues.get(intcst.value)))
					]
				]
			}
			case "Version": {
				// TreeSet because it is ordered
				val values = new TreeSet<Version>()
				abs.conver2GALImported [
					it.eAllContents.filter(VersionConstant).forEach [
						values.add(it.value)
					]
				]
				_self.count = values.size
				val galValues = new HashMap<Version, Integer>()
				var i = 0;
				for (value : values) {
					galValues.put(value, i)
					i++
				}
				abs.conver2GALImported [
					it.eAllContents.filter(VersionConstant).forEach[ vercst |
						vercst.register(galValues.get(vercst.value))
						_self.cstlinks.add(new ConstantLink(vercst.value.toString, vercst, galValues.get(vercst.value)))
					]
				]
			}
		}
		_self.generateTypedef()
	}

	def void generateTypedef() {
		if (_self.count > 0) {
			_self.generatedTypedef = GALBuildHelper.createTypeDefDeclaration(_self.name, 0, _self.count - 1)
			_self.abs.generatedGALTypeDeclaration.typedefs.add(_self.generatedTypedef)
		}
	}

	def void createTrace(ABS2GALTraceBuilder tracebuilder) {
		tracebuilder.addPrimitiveDataTypeLink(_self.name, _self, Optional.ofNullable(_self.generatedTypedef), _self.count)
		_self.cstlinks.forEach[tracebuilder.addConstantLink(it)]
	}
}

@Aspect(className=EnumDataType)
class EnumDataTypeAspect extends PrimitiveDataTypeAspect {
	@OverrideAspectMethod
	def void countElements(AssetBasedSystem abs) {
		_self.count = _self.enumLiteral.size;
		_self.enumLiteral.forEach [ literal, index |
			literal.register(index)
		]
		_self.generateTypedef()
	}

	def EnumLiteral getLitteral(int value) {
		_self.enumLiteral.get(value)
	}

	@OverrideAspectMethod
	def void createTrace(ABS2GALTraceBuilder tracebuilder) {
		tracebuilder.addPrimitiveDataTypeLink(_self.name, _self, Optional.ofNullable(_self.generatedTypedef), _self.count)
		_self.enumLiteral.forEach [ literal, index |
			tracebuilder.addEnumLiteralLink(literal.name, literal, index)
		]
	}
}

@Aspect(className=EnumLiteral)
class EnumLiteralAspect extends SymbolAspect {
	int GALValue

	def void register(int value) {
		_self.GALValue = value;
	}

	def int getGalValue() {
		_self.GALValue
	}

	@OverrideAspectMethod
	def ExpressionType getTypeForConversion(Context context) {
		ExpressionType.simple(ABSUtils.getEnumLiteralType(_self))
	}

	def IntExpression value2int(Context context) {
		GALBuildHelper.createConstant(_self.galValue)
	}
}

@Aspect(className=DefinitionGroup)
class DefinitionGroupAspect {

	def void registerSubtypes() {
		_self.assetTypes.forEach[t|t.registerSubtypes()]
	}
}

@Aspect(className=AssetGroup)
class AssetGroupAspect {

	def void count() {
		_self.assets.forEach[t|t.count()];
	}
}

@Aspect(className=AbstractAssetType)
abstract class AbstractAssetTypeAspect {

	abstract def void convert2GAL()

	def void registerSubtypes() {
	}
}

@Aspect(className=fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect)
class AssetTypeAspectAspect extends AbstractAssetTypeAspect {

	def void convert2GAL() {
		_self.assetTypeAttributes.forEach[t|t.convert2GAL(_self.baseAssetType)]
		_self.assetTypeProperties.forEach[t|t.convert2GAL(_self.baseAssetType)]
	}
}

@Aspect(className=Guard)
class GuardAspect {
	protected AssetBasedSystem abs;

	def void initialize(AssetBasedSystem abs) {
		_self.abs = abs
	}

	def String getQualifiedName() {
		EcoreUtil2.getContainerOfType(_self, DefinitionGroup).name + "_" + _self.name
	}
}

@Aspect(className=GuardedAction)
class GuardedActionAspect extends GuardAspect {
	protected List<Transition> generatedTransitions

	def void convert2GAL() {
		_self.generatedTransitions = newArrayList
		_self.abs.nameRepetition.merge(_self.qualifiedName, 1, [i, j|i + j])
		var String GALName = _self.qualifiedName + "__" + _self.abs.nameRepetition.get(_self.qualifiedName)
		val mainTransition = GALBuildHelper.createTransition(GALName)
		val context = new Context()
		// Generating copies of the transition for each subtype combination of the parameters
		var transitions = new HashMap<Transition, Context>()
		transitions.put(mainTransition, context)
		for (GuardParameter p : _self.guardParameters) {
			val next = new HashMap<Transition, Context>()
			transitions.forEach [ t, c |
				next.putAll(p.generateParameter(t, c))
			]
			transitions = next
		}
		transitions.forEach [ t, c |
			val guard = _self.guardExpression.expr2boolExpr(c)
			t.guard = guard
			_self.guardActions.forEach [ a |
				val actions = a.convert2GALAction(c)
				t.actions.addAll(actions.collect(Collectors.toList))
			]
			_self.abs.generatedGALTypeDeclaration.transitions.add(t);
			_self.generatedTransitions.add(t);
		]
	}

	def void createTrace(ABS2GALTraceBuilder tracebuilder) {
		tracebuilder.addGuardedActionLink(_self.name, _self, _self.generatedTransitions, _self.abs.nameRepetition.get(_self.qualifiedName))
	}
}

@Aspect(className=GuardParameter)
class GuardParameterAspect extends SymbolAspect {
	/*
	 * BE CAREFUL :
	 * 
	 * This class has more than one superclass
	 * please specify which parent you want with the 'super' expected calling
	 * 
	 */
	def Set<Context> generateParameter(Context context) {
		val res = new HashSet<Context>()
		_self.parameterType.subtypes.forEach [
			if (it.count > 0) {
				val param = GALBuildHelper.createParam(_self.name, it.generatedTypedef)
				val newcontext = new Context(context)
				newcontext.addParameter(_self, param, it)
				res.add(newcontext)
			}
		]
		res
	}

	def Map<Transition, Context> generateParameter(Transition transition, Context context) {
		val res = new HashMap<Transition, Context>()
		_self.parameterType.subtypes.forEach [
			if (it.count > 0) {
				val param = GALBuildHelper.createParam(_self.name, it.generatedTypedef)
				val newtransition = EcoreUtil.copy(transition)
				newtransition.name = transition.name + "__" + it.qualifiedName
				newtransition.params.add(param)
				val newcontext = new Context(context)
				newcontext.addParameter(_self, param, it)
				res.put(newtransition, newcontext)
			}
		]
		res
	}

	@OverrideAspectMethod
	def ExpressionType getTypeForConversion(Context context) {
		ExpressionType.simple(context.getParameterType(_self))
	}

	def IntExpression assetRef2int(Context context) {
		GALBuildHelper.createParamRef(context.getParameter(_self))
	}

}

@Aspect(className=Expression)
abstract class ExpressionAspect {
	protected AssetBasedSystem abs;

	def void initialize(AssetBasedSystem abs) {
		_self.abs = abs
	}

	/**	Convert the expression into a BooleanExpression
	 * @param context the compilation context
	 * @return the conversion to BooleanExpression 
	 */
	abstract def BooleanExpression expr2boolExpr(Context context)

	/**	Convert the expression into a IntExpression, in a context where we might expect a regular integer value
	 * @param context the compilation context
	 * @return the conversion to IntExpression 
	 */
	abstract def IntExpression expr2value(Context context)
	
	/**	Convert the expression into a IntExpression, in a context where we expect a proper value
	 * @param context the compilation context
	 * @return the conversion to IntExpression 
	 */
	def IntExpression expr2properValue(Context context) {
		if (_self.getTypeForConversion(context).isAssetType)
			_self.expr2properassetRef(context)
		else
			_self.expr2value(context)
	}

	/**	Convert the expression into a IntExpression, in a context where we expect a reference to an asset (as a index to an array)
	 * @param context the compilation context
	 * @return the conversion to IntExpression 
	 */
	abstract def IntExpression expr2properassetRef(Context context)

	/**	Convert the expression into a IntExpression, representing the proper type of the asset represented by the Expression
	 * @param context the compilation context
	 * @return the conversion to IntExpression 
	 */
	abstract def IntExpression expr2propertype(Context context)

	/**	Convert the expression into a IntExpression, in a context where we expect a reference to an asset (as a index to an array), and the value is encoded to be used with the AssetType type
	 * @param context the compilation context
	 * @param the type of the array in which the returned expression will be put, must be a supertype of _self.type.asAssetType()
	 * @return the conversion to IntExpression 
	 */
	def IntExpression expr2typedassetRef(Context context, AssetType type) {
		type.getSubtypedValue(_self.expr2propertype(context), _self.expr2properassetRef(context))
	}

	/**	Convert the expression representing a member of multiplicity one into a possibly typed value
	 * @param context the compilation context
	 * @param type : the expected type of the returned expression
	 * @return an IntExpression
	 */
	def IntExpression expr2memberReftypedvalue(Context context, ExpressionType type) {
		if (type.isAssetType)
			_self.expr2typedassetRef(context, type.type as AssetType)
		else
			_self.expr2value(context)
	}

	/**	Convert the expression into a VariableReference.
	 * 	Used for members of multiplicity One.
	 * 	e.g. anAsset.member is converted into anAssettype.member[anAsset]
	 * @param context the compilation context
	 * @return a VariableReference
	 */
	abstract def VariableReference expr2memberMultiplicityOneVarRef(Context context)

	/**	Convert the expression into a VariableReference.
	 * 	Used for members of multiplicity Many.
	 * @param context the compilation context
	 * @param memberSubType the subtype of the member that will be used when applying the second index
	 * @return a ArrayCollectionRef
	 * e.g. anAsset.member is converted into i -> anAssettype.memberSubType.member[anAsset * memberSubType.count + i]
	 */
	abstract def ArrayCollectionRef expr2memberMultiplicityManyArrayRef(Context context, ExpressionType memberSubType)
	
	/**
	 * Convert the expression into a CollectionRef
	 * May be a ArrayCollectionRef in case
	 */
	abstract def FiltrableCollectionRef expr2collectionRef(Context context, ExpressionType memberSubType)

	/**	Converts the Expression into a lambda Expression that, given an IntExpression i, builds the condition checking whether or not the asset corresponding to i of type memberSubType satisfies the filter _self
	 * @param context the compilation context
	 * @return a lambda expression taking an IntExpression and returning a BooleanExpression
	 */
	abstract def Function<IntExpression, BooleanExpression> expr2GALFilterCondition(Context context,
		ExpressionType memberSubType)

	/**	
	 * @param context the compilation context, including the GAL transition in which the expression is generated
	 * @return The type of the asset of the member referred by _self
	 */
	def ExpressionType getTypeForConversion(Context context) {
		_self.abs.typeProvider.getTypeOf(_self)
	}

	abstract def Set<Pair<ArrayPrefix, Integer>> referredCells(Context context)

	abstract def Set<Integer> referredProperValues(Context context)

}

@Aspect(className=BinaryExpression)
abstract class BinaryExpressionAspect extends ExpressionAspect {

	def Set<Pair<ArrayPrefix, Integer>> referredCells(Context context) {
		val result = new HashSet<Pair<ArrayPrefix, Integer>>()
		result.addAll(_self.lhs.referredCells(context))
		result.addAll(_self.rhs.referredCells(context))
		result
	}
}

@Aspect(className=ImpliesExpression)
class ImpliesExpressionAspect extends BinaryExpressionAspect {
	def BooleanExpression expr2boolExpr(Context context) {
		GALBuildHelper.createBoolExprImplies(_self.lhs.expr2boolExpr(context), _self.rhs.expr2boolExpr(context))
	}

	def IntExpression expr2value(Context context) {
		val expr = _self.expr2boolExpr(context)
		GALBuildHelper.createWrapBool(expr)
	}
}

@Aspect(className=OrExpression)
class OrExpressionAspect extends BinaryExpressionAspect {
	def BooleanExpression expr2boolExpr(Context context) {
		GALBuildHelper.createBoolExprOr(_self.lhs.expr2boolExpr(context), _self.rhs.expr2boolExpr(context))
	}

	def IntExpression expr2value(Context context) {
		val expr = _self.expr2boolExpr(context)
		GALBuildHelper.createWrapBool(expr)
	}
}

@Aspect(className=AndExpression)
class AndExpressionAspect extends BinaryExpressionAspect {
	def BooleanExpression expr2boolExpr(Context context) {
		GALBuildHelper.createBoolExprAnd(_self.lhs.expr2boolExpr(context), _self.rhs.expr2boolExpr(context))
	}

	def IntExpression expr2value(Context context) {
		val expr = _self.expr2boolExpr(context)
		GALBuildHelper.createWrapBool(expr)
	}
}

@Aspect(className=NotExpression)
class NotExpressionAspect extends ExpressionAspect {
	def BooleanExpression expr2boolExpr(Context context) {
		GALBuildHelper.createBoolExprNot(_self.expression.expr2boolExpr(context))
	}

	def IntExpression expr2value(Context context) {
		val expr = _self.expr2boolExpr(context)
		GALBuildHelper.createWrapBool(expr)
	}

	def Set<Pair<ArrayPrefix, Integer>> referredCells(Context context) {
		_self.expression.referredCells(context)
	}
}

@Aspect(className=EqualityComparisonExpression)
class EqualityComparisonExpressionAspect extends BinaryExpressionAspect {
	def BooleanExpression expr2boolExpr(Context context) {
		GALBuildHelper.createBoolExprComp(_self.lhs.expr2value(context), _self.rhs.expr2value(context),
			GALBuildHelper.getComparisonOperator(_self.op))
	}

	def IntExpression expr2value(Context context) {
		val expr = _self.expr2boolExpr(context)
		GALBuildHelper.createWrapBool(expr)
	}
}

@Aspect(className=InequalityComparisonExpression)
class InequalityComparisonExpressionAspect extends BinaryExpressionAspect {
	def BooleanExpression expr2boolExpr(Context context) {
		GALBuildHelper.createBoolExprComp(_self.lhs.expr2value(context), _self.rhs.expr2value(context),
			GALBuildHelper.getComparisonOperator(_self.op))
	}

	def IntExpression expr2value(Context context) {
		val expr = _self.expr2boolExpr(context)
		GALBuildHelper.createWrapBool(expr)
	}
}

@Aspect(className=Action)
class ActionAspect {
	protected AssetBasedSystem abs;

	def void initialize(AssetBasedSystem abs) {
		_self.abs = abs
	}

	def Stream<Statement> convert2GALAction(Context context) {
		// target '.' actionType '(' ((args+=Expression | lambdaAction=LambdaAction) (',' args+=Expression)*)? ')' 
		var Stream<Statement> result = Stream.empty;
		switch _self.actionType {
			case ADD: {
				val argtype = _self.args.get(0).getTypeForConversion(context)
				val addedIndex = _self.args.get(0).expr2value(context)
				if (argtype.isAssetType) {
					val aargtype = argtype.type as AssetType
					val builder = Stream.builder
					for (type : aargtype.subtypes) {
						val argsubtype = GALBuildHelper.GlobalValue2Type(EcoreUtil.copy(addedIndex), _self.abs.nbTypes)
						val argproper = GALBuildHelper.GlobalValue2Proper(EcoreUtil.copy(addedIndex), _self.abs.nbTypes)
						val targetRef = _self.target.expr2memberMultiplicityManyArrayRef(context, ExpressionType.collection(type))
						if (targetRef !== null) {
							builder.add(GALBuildHelper.createIfThen(type.typeTest(argsubtype),
								GALBuildHelper.createAssign(targetRef.getElementVarRef(argproper),
									GALBuildHelper.createConstant(GALBuildHelper.galTrue))))
						}
					}
					result = builder.build
				} else {
					val targetRef = _self.target.expr2memberMultiplicityManyArrayRef(context, argtype)
					result = Stream.of(GALBuildHelper.createAssign(targetRef.getElementVarRef(addedIndex),
						GALBuildHelper.createConstant(GALBuildHelper.galTrue)))
				}
			}
			case ADD_ALL: {
				val type = _self.args.get(0).getTypeForConversion(context)
				for (subtype : type.subtypes) {
					val sourceRef = _self.args.get(0).expr2collectionRef(context, subtype)
					val destRef = _self.target.expr2memberMultiplicityManyArrayRef(context, subtype)
					if (sourceRef !== null && destRef !== null) {
						for (var i = 0; i < destRef.memberSubtypeCount; i++) {
							val vali = i;
							val Provider<VariableReference> destvari = [destRef.getElementVarRef(GALBuildHelper.createConstant(vali))]
							val Provider<BooleanExpression> sourcevari = [sourceRef.getIsContainedExpression(GALBuildHelper.createConstant(vali))]
							val condi = sourceRef.getFilterCondition().map[GALBuildHelper.createBoolExprAnd(it.apply(GALBuildHelper.createConstant(vali)),
										sourcevari.get)].orElse(sourcevari.get)
							val assign = GALBuildHelper.createAssign(destvari.get,
								GALBuildHelper.createConstant(GALBuildHelper.galTrue))
							result = Stream::concat(result, Stream.of(GALBuildHelper.createIfThen(condi, assign)))
						}
					}
				}
			}
			case ASSIGN: {
				val valuei = _self.args.get(0).expr2value(context)
				val targetv = _self.target.expr2memberMultiplicityOneVarRef(context)
				result = Stream.of(GALBuildHelper.createAssign(targetv, EcoreUtil.copy(valuei)))
			}
			case CLEAR: {
				val type = _self.target.getTypeForConversion(context)
				for (subtype : type.subtypes) {
					val targeta = _self.target.expr2memberMultiplicityManyArrayRef(context, subtype)
					if (targeta !== null) {
						for (var i = 0; i < targeta.memberSubtypeCount; i++) {
							result = Stream::concat(result, Stream.of(
								GALBuildHelper.createAssign(targeta.getElementVarRef(GALBuildHelper.createConstant(i)),
									GALBuildHelper.createConstant(GALBuildHelper.galFalse))))
						}
					}
				}
			}
			case FOR_ALL: {
				val type = _self.target.getTypeForConversion(context)
				for (subtype : type.subtypes) {
					val targeta = _self.target.expr2memberMultiplicityManyArrayRef(context, subtype)
					if (targeta !== null) {
						for (var i = 0; i < targeta.memberSubtypeCount; i++) {
							val vali = i
							val Provider<VariableReference> vari = [targeta.getElementVarRef(GALBuildHelper.createConstant(vali))]
							val condi = targeta.getFilterCondition().map[GALBuildHelper.createBoolExprAnd(it.apply(GALBuildHelper.createConstant(vali)),
										GALBuildHelper.createBoolExprIsTrue(vari.get))].orElseGet[GALBuildHelper.createBoolExprIsTrue(vari.get)]
							val lastream = _self.lambdaAction.convert2GALAction(context, subtype, i)
							val st = lastream.map[s|GALBuildHelper.createIfThen(EcoreUtil.copy(condi), s)]
							result = Stream::concat(result, st)
						}
					}
				}
			}
			case REMOVE: {
				val argtype = _self.args.get(0).getTypeForConversion(context)
				val addedIndex = _self.args.get(0).expr2value(context)
				if (argtype.isAssetType) {
					val aargtype = argtype.type as AssetType
					val builder = Stream.builder
					for (type : aargtype.subtypes) {
						val argsubtype = GALBuildHelper.GlobalValue2Type(EcoreUtil.copy(addedIndex), _self.abs.nbTypes)
						val argproper = GALBuildHelper.GlobalValue2Proper(EcoreUtil.copy(addedIndex), _self.abs.nbTypes)
						val targetRef = _self.target.expr2memberMultiplicityManyArrayRef(context, ExpressionType.collection(type))
						if (targetRef !== null) {
							builder.add(GALBuildHelper.createIfThen(type.typeTest(argsubtype),
								GALBuildHelper.createAssign(targetRef.getElementVarRef(argproper),
									GALBuildHelper.createConstant(GALBuildHelper.galFalse))))
						}
					}
					result = builder.build
				} else {
					val targetRef = _self.target.expr2memberMultiplicityManyArrayRef(context, argtype)
					result = Stream.of(GALBuildHelper.createAssign(targetRef.getElementVarRef(addedIndex),
						GALBuildHelper.createConstant(GALBuildHelper.galFalse)))
				}
			}
			case REMOVE_ALL: {
				val type = _self.args.get(0).getTypeForConversion(context)
				for (subtype : type.subtypes) {
					val sourceRef = _self.args.get(0).expr2collectionRef(context, subtype)
					val destRef = _self.target.expr2memberMultiplicityManyArrayRef(context, subtype)
					if (sourceRef !== null && destRef !== null) {
						for (var i = 0; i < destRef.memberSubtypeCount; i++) {
							val vali = i;
							val Provider<VariableReference> destvari = [destRef.getElementVarRef(GALBuildHelper.createConstant(vali))]
							val Provider<BooleanExpression> sourcevari = [sourceRef.getIsContainedExpression(GALBuildHelper.createConstant(vali))]
							val condi = sourceRef.getFilterCondition().map[GALBuildHelper.createBoolExprAnd(it.apply(GALBuildHelper.createConstant(vali)),
										sourcevari.get)].orElseGet[sourcevari.get]
							val assign = GALBuildHelper.createAssign(destvari.get,
								GALBuildHelper.createConstant(GALBuildHelper.galFalse))
							val st = Stream.of(GALBuildHelper.createIfThen(condi, assign))
							result = Stream::concat(result, st)
						}
					}
				}
			}
		}
		result
	}
}

@Aspect(className=ConstantExpression)
abstract class ConstantExpressionAspect extends ExpressionAspect {
	abstract def int const2int()

	def IntExpression expr2value(Context context) {
		GALBuildHelper.createConstant(_self.const2int)
	}

	def Set<Pair<ArrayPrefix, Integer>> referredCells(Context context) {
		new HashSet<Pair<ArrayPrefix, Integer>>()
	}
}

@Aspect(className=StringConstant)
class StringConstantAspect extends ConstantExpressionAspect {
	int GALValue
	PrimitiveDataType stringType

	@OverrideAspectMethod
	def void initialize(AssetBasedSystem abs) {
		_self.super_initialize(abs)
		_self.stringType = abs.typeProvider.getTypeOf(_self).type as PrimitiveDataType
	}

	def void register(HashMap<String, Integer> values) {
		if (!values.containsKey(_self.value)) {
			values.put(_self.value, _self.stringType.count)
			_self.GALValue = _self.stringType.count
			_self.stringType.count = _self.stringType.count + 1
		} else {
			_self.GALValue = values.get(_self.value)
		}
	}

	def int const2int() {
		return _self.GALValue
	}
}

@Aspect(className=IntConstant)
class IntConstantAspect extends ConstantExpressionAspect {
	int GALvalue;

	def void register(int value) {
		_self.GALvalue = value
	}

	def int const2int() {
		_self.GALvalue
	}

}

@Aspect(className=BooleanConstant)
class BooleanConstantAspect extends ConstantExpressionAspect {

	def BooleanExpression expr2boolExpr(Context context) {
		switch (_self.value) {
			case "true": GALBuildHelper.boolTrue()
			case "false": GALBuildHelper.boolFalse()
			default: throw new IllegalArgumentException("Unexpected boolean constant: " + _self.value)
		}
	}

	def int const2int() {
		switch (_self.value) {
			case "true": GALBuildHelper.galTrue
			case "false": GALBuildHelper.galFalse
			default: throw new IllegalArgumentException("Unexpected boolean constant: " + _self.value)
		}
	}
}

@Aspect(className=EnumConstant)
class EnumConstantAspect extends ConstantExpressionAspect {
	def int const2int() {
		_self.value.galValue
	}
}

@Aspect(className=VersionConstant)
class VersionConstantAspect extends ConstantExpressionAspect {
	int GALvalue;

	def void register(int value) {
		_self.GALvalue = value
	}

	def int const2int() {
		_self.GALvalue
	}
}

@Aspect(className=UndefinedConstant)
class UndefinedConstantAspect extends ConstantExpressionAspect {
	def int const2int() {
		GALBuildHelper.galNone
	}
}

@Aspect(className=Collection)
class CollectionAspect extends ExpressionAspect {
	def FiltrableCollectionRef expr2collectionRef(Context context, ExpressionType memberSubType) {
		val collectionRef = new StaticCollectionRef()
		_self.elements.stream.filter [ element |
			Objects.equals(_self.abs.typeProvider.getTypeOf(element).type, memberSubType.type)
		].map[element|element.expr2properValue(context)].forEachOrdered[elementExpr|collectionRef.addElement(elementExpr)]
		collectionRef
	}
	
	def Set<Pair<ArrayPrefix, Integer>> referredCells(Context context) {
		new HashSet<Pair<ArrayPrefix, Integer>>()
	}
	
	def Set<Integer> referredProperValues(Context context) {
		_self.elements.stream.flatMapToInt[ element | 
			val type = element.getTypeForConversion(context)
		IntStream.range(0, type.count)
		].distinct.boxed.collect(Collectors.toSet)
	}
}

@Aspect(className=MemberSelection)
class MemberSelectionAspect extends ExpressionAspect {
	def BooleanExpression expr2boolExpr(Context context) {
		if (_self.methodInvocation) {
			switch _self.member.name {
				case "contains": {
					val argtype = _self.args.get(0).getTypeForConversion(context)
					val index = _self.args.get(0).expr2value(context)

					if (argtype.isAssetType) {
						val aargtype = argtype.type as AssetType
						val cases = newArrayList
						for (type : aargtype.subtypes) {
							val argsubtype = GALBuildHelper.GlobalValue2Type(EcoreUtil.copy(index), _self.abs.nbTypes)
							val argproper = GALBuildHelper.GlobalValue2Proper(EcoreUtil.copy(index), _self.abs.nbTypes)
							val arrayRef = _self.receiver.expr2collectionRef(context, ExpressionType.collection(type))
							if (arrayRef !== null) {
								var test = 
									arrayRef.getIsContainedExpression(EcoreUtil.copy(argproper))
								if (arrayRef.filterCondition.isPresent) {
									test = GALBuildHelper.createBoolExprAnd(
										arrayRef.filterCondition.get().apply(EcoreUtil.copy(argproper)), test)
								}
								test = GALBuildHelper.createBoolExprAnd(type.typeTest(argsubtype), test)
								cases.add(test)
							}
						}
						GALBuildHelper.createBigOr(cases)
					} else {
						val arrayRef = _self.receiver.expr2collectionRef(context, argtype)
						arrayRef.filterCondition.map[GALBuildHelper.createBoolExprAnd(it.apply(EcoreUtil.copy(index)),
								arrayRef.getIsContainedExpression(EcoreUtil.copy(index)))].orElseGet[arrayRef.getIsContainedExpression(EcoreUtil.copy(index))]
					}
				}
				case "containsAll": {
					val testType = _self.args.get(0).getTypeForConversion(context)
					val cases = new ArrayList<BooleanExpression>()
					for (subtype : testType.subtypes) {
						val testRef = _self.args.get(0).expr2collectionRef(context, subtype)
						val arrayRef = _self.receiver.expr2collectionRef(context, subtype)
						if (testRef !== null && arrayRef !== null) {
							for (var i = 0; i < subtype.count; i++) {
								val vali = i
								val Provider<BooleanExpression> testVarRefi = [
									testRef.getIsContainedExpression(GALBuildHelper.createConstant(vali))
								]
								val Provider<BooleanExpression> arrayVarRefi = [
									arrayRef.getIsContainedExpression(GALBuildHelper.createConstant(vali))
								]
								val expr = arrayRef.filterCondition.map [ arrayRefFilter |
									testRef.filterCondition.map [ testRefFilter |
										GALBuildHelper.createBoolExprOr(
											GALBuildHelper.createBoolExprAnd(
												arrayRefFilter.apply(GALBuildHelper.createConstant(vali)),
												arrayVarRefi.get),
											GALBuildHelper.createBoolExprNot(
												GALBuildHelper.createBoolExprAnd(
													testRefFilter.apply(GALBuildHelper.createConstant(vali)),
													testVarRefi.get
												)
											)
										)
									].orElseGet [
										GALBuildHelper.createBoolExprOr(
											GALBuildHelper.createBoolExprAnd(
												arrayRefFilter.apply(GALBuildHelper.createConstant(vali)),
												arrayVarRefi.get),
											GALBuildHelper.createBoolExprNot(testVarRefi.get)
										)
									]
								].orElseGet [
									testRef.filterCondition.map [ testRefFilter |
										GALBuildHelper.createBoolExprOr(
											arrayVarRefi.get,
											GALBuildHelper.createBoolExprNot(
												GALBuildHelper.createBoolExprAnd(
													testRefFilter.apply(GALBuildHelper.createConstant(vali)),
													testVarRefi.get
												)
											)
										)
									].orElseGet [
										GALBuildHelper.createBoolExprOr(
											arrayVarRefi.get,
											GALBuildHelper.createBoolExprNot(testVarRefi.get)
										)
									]
								]
								cases.add(expr)
							}
						}
					}
					GALBuildHelper.createBigAnd(cases)
				}
				case "containsAny": {
					// TODO inheritance : get the greatest common subtype of receiver.type and args.get(0).type
					val testType = _self.args.get(0).getTypeForConversion(context)
					val cases = new ArrayList<BooleanExpression>()
					for (subtype : testType.subtypes) {
						val testRef = _self.args.get(0).expr2collectionRef(context, subtype)
						val recRef = _self.receiver.expr2collectionRef(context, subtype)
						if (testRef !== null && recRef !== null) {
							for (var i = 0; i < subtype.count; i++) {
								var expr = GALBuildHelper.createBoolExprAnd(
										recRef.getIsContainedExpression(GALBuildHelper.createConstant(i)),
										testRef.getIsContainedExpression(GALBuildHelper.createConstant(i)))
								if (recRef.filterCondition.present)
									expr = GALBuildHelper.createBoolExprAnd(
										recRef.filterCondition.get.apply(GALBuildHelper.createConstant(i)), expr)
								if (testRef.filterCondition.present)
									expr = GALBuildHelper.createBoolExprAnd(
										testRef.filterCondition.get.apply(GALBuildHelper.createConstant(i)), expr)
								cases.add(expr)
							}
						}
					}
					GALBuildHelper.createBigOr(cases)
				}
				case "isEmpty": {
					val type = _self.receiver.getTypeForConversion(context)
					val cases = new ArrayList<BooleanExpression>()
					for (subtype : type.subtypes) {
						val arrayRef = _self.receiver.expr2collectionRef(context, subtype)
						if (arrayRef !== null) {
							for (var i = 0; i < subtype.count; i++) {
								val vali = i
								val Provider<BooleanExpression> arrayVarRefi = [arrayRef.getIsContainedExpression(GALBuildHelper.createConstant(vali))]
								cases.add(arrayRef.filterCondition.map [ arrayRefFilter |
									GALBuildHelper.createBoolExprImplies(
										arrayRefFilter.apply(GALBuildHelper.createConstant(vali)),
										GALBuildHelper.createBoolExprNot(
											arrayVarRefi.get))
								].orElseGet[GALBuildHelper.createBoolExprNot(
									arrayVarRefi.get)])
							}
						}
					}
					GALBuildHelper.createBigAnd(cases);
				}
				case "filter": {
					throw new RuntimeException("filter does not return a boolean expression.")
				}
				default: {
					throw new RuntimeException("Unknown method call " + _self.member.name)
				}
			}
		} else {
			GALBuildHelper.createBoolExprIsTrue(_self.expr2value(context))
		}
	}

	def IntExpression expr2value(Context context) {
		if (_self.methodInvocation) {
			switch _self.member.name {
				case "filter": {
					throw new RuntimeException("filter does not return a boolean expression or an asset.")
				}
				default: {
					val expr = _self.expr2boolExpr(context)
					GALBuildHelper.createWrapBool(expr)
				}
			}
		} else {
			_self.expr2memberMultiplicityOneVarRef(context)
		}

	}

	def IntExpression expr2properassetRef(Context context) {
		GALBuildHelper.GlobalValue2Proper(_self.expr2memberMultiplicityOneVarRef(context), _self.abs.nbTypes)
	}

	def IntExpression expr2propertype(Context context) {
		GALBuildHelper.GlobalValue2Type(_self.expr2memberMultiplicityOneVarRef(context), _self.abs.nbTypes)
	}

	def VariableReference expr2memberMultiplicityOneVarRef(Context context) {
		val rectype = _self.receiver.getTypeForConversion(context).type as AssetType
		val memberbasetype = rectype.getMemberBaseType(_self.member.name)
		val index = _self.receiver.expr2typedassetRef(context, memberbasetype)
		GALBuildHelper.createArrayRef(rectype.getGeneratedArrayMultiplicityOne(_self.member.name), index)
	}

	def ArrayCollectionRef expr2memberMultiplicityManyArrayRef(Context context, ExpressionType memberSubType) {
		if (_self.methodInvocation) {
			switch _self.member.name {
				case "filter": {
					val arrayRef = _self.receiver.expr2memberMultiplicityManyArrayRef(context, memberSubType)
					if (arrayRef !== null) {
						val filterCondition = _self.args.get(0).expr2GALFilterCondition(context, memberSubType)
						val condArray = new ArrayCollectionRef(arrayRef)
						condArray.addFilterCondition(filterCondition)
						condArray
					}
				}
				case "contains",
				case "containsAll",
				case "containsAny",
				case "isEmpty": {
					throw new RuntimeException(_self.member.name + " does not return a array ref.")
				}
				default: {
					throw new RuntimeException("Unknown method call " + _self.member.name)
				}
			}
		} else {
			val properref = _self.receiver.expr2properassetRef(context)
			val propertype = _self.receiver.expr2propertype(context)
			val feature = _self.member as AssetTypeFeature
			val featurecontainertype = ABSUtils.getContainerType(feature)
			val array = featurecontainertype.getGeneratedArrayMultiplicityMany(memberSubType, _self.member.name)
			if (array !== null)
				new ArrayCollectionRef(
					array,
					featurecontainertype.getSubtypedValue(propertype, properref),
					memberSubType.count
				)
		}
	}
	
	def FiltrableCollectionRef expr2collectionRef(Context context, ExpressionType memberSubType) {
		if(_self.methodInvocation) {
			if (_self.member.name == "filter") {
			val unfilteredCollectionRef = _self.receiver.expr2collectionRef(context, memberSubType)
			if (unfilteredCollectionRef !== null) {
				val filterCondition = _self.args.get(0).expr2GALFilterCondition(context, memberSubType)
				val filteredCollectionRef = unfilteredCollectionRef.copy
				filteredCollectionRef.addFilterCondition(filterCondition)
				filteredCollectionRef
			} else {
				throw new RuntimeException(_self.member.name + " does not return a collection ref.")
			}
		}
		}
		 else {
			_self.expr2memberMultiplicityManyArrayRef(context, memberSubType)
		}
		
	}

	def Function<IntExpression, BooleanExpression> expr2GALFilterCondition(Context context, ExpressionType memberSubType) {
		val arrayRef = _self.expr2memberMultiplicityManyArrayRef(context, memberSubType);
		[IntExpression i|GALBuildHelper.createBoolExprIsTrue(arrayRef.getElementVarRef(i))]
	}

	def Set<Pair<ArrayPrefix, Integer>> referredCells(Context context) {
		val result = new HashSet<Pair<ArrayPrefix, Integer>>()
		result.addAll(_self.receiver.referredCells(context))
		if (_self.methodInvocation) {
			_self.args.forEach[result.addAll(it.referredCells(context))]
		} else {
			val member = _self.member as AssetTypeFeature
			result.addAll(member.referredCells(_self.receiver, context))
		}
		result
	}

	def Set<Integer> referredProperValues(Context context) {
		val type = _self.getTypeForConversion(context)
		IntStream.range(0, type.count).boxed.collect(Collectors.toSet)
	}
}

@Aspect(className=SymbolRef)
class SymbolRefAspect extends ExpressionAspect {

	def IntExpression expr2properassetRef(Context context) {
		_self.symbol.assetRef2int(context)
	}

	def IntExpression expr2propertype(Context context) {
		GALBuildHelper.createConstant((_self.getTypeForConversion(context).type as AssetType).typeNumber)
	}

	def IntExpression expr2value(Context context) {
		val type = _self.symbol.getTypeForConversion(context)
		if (type.isAssetType) {
			val proper = _self.symbol.assetRef2int(context)
			type.getGloballyTypedValue(proper)
		} else
			_self.symbol.value2int(context)
	}
	
	@OverrideAspectMethod
	def ExpressionType getTypeForConversion(Context context) {
		_self.symbol.getTypeForConversion(context)
	}

	def Set<Pair<ArrayPrefix, Integer>> referredCells(Context context) {
		new HashSet<Pair<ArrayPrefix, Integer>>()
	}

	def Set<Integer> referredProperValues(Context context) {
		_self.symbol.referredProperValues(context)
	}
}

@Aspect(className=Symbol)
abstract class SymbolAspect {
	abstract def ExpressionType getTypeForConversion(Context context)

	abstract def IntExpression value2int(Context context)

	abstract def IntExpression assetRef2int(Context context)

	def Set<Integer> referredProperValues(Context context) {
		val type = _self.getTypeForConversion(context)
		IntStream.range(0, type.count).boxed.collect(Collectors.toSet)
	}
}

@Aspect(className=LambdaParameter)
class LambdaParameterAspect extends SymbolAspect {
	/*
	 * BE CAREFUL :
	 * 
	 * This class has more than one superclass
	 * please specify which parent you want with the 'super' expected calling
	 * 
	 */
	def ExpressionType getTypeForConversion(Context context) {
		context.getLambdaType(_self)
	}

	def IntExpression assetRef2int(Context context) {
		context.getLambdaRef(_self)
	}

	def IntExpression value2int(Context context) {
		context.getLambdaRef(_self)
	}
}

@Aspect(className=LambdaExpression)
class LambdaExpressionAspect extends ExpressionAspect {
	def Function<IntExpression, BooleanExpression> expr2GALFilterCondition(Context context, ExpressionType memberSubType) {
		val paramType = (_self.eContainer as MemberSelection).getTypeForConversion(context)
		context.addLambdaType(_self.lambdaParameter, paramType)
		// Create a fake parameter for the lambda
		val temp = GALBuildHelper.createDummyParam(_self.lambdaParameter.name)
		// register the parameter
		context.addLambdaRef(_self.lambdaParameter, GALBuildHelper.createParamRef(temp))
		val expr = _self.body.expr2boolExpr(context);
		[ IntExpression i |
			val exprcpy = EcoreUtil.copy(expr);
			exprcpy.eAllContents.filter(ParamRef).filter[it.refParam == temp].forEach [
				it.eContainer.eSet(it.eContainingFeature, EcoreUtil.copy(i))
			]
			exprcpy
		]
	}

	def Set<Pair<ArrayPrefix, Integer>> referredCells(Context context) {
		val paramType = (_self.eContainer as MemberSelection).getTypeForConversion(context)
		context.addLambdaType(_self.lambdaParameter, paramType)
		val result = new HashSet<Pair<ArrayPrefix, Integer>>()
		result.addAll(_self.body.referredCells(context))
		result
	}
}

@Aspect(className=LambdaAction)
class LambdaActionAspect {
	def Stream<Statement> convert2GALAction(Context context, ExpressionType type, int value) {
		// '{' lambdaParameter=LambdaParameter '->' actions+=Action '}'
		val result = new ArrayList<Stream<Statement>>();
		context.addLambdaRef(_self.lambdaParameter, GALBuildHelper.createConstant(value))
		context.addLambdaType(_self.lambdaParameter, type)
		_self.actions.stream.forEach [ a |
			val st = a.convert2GALAction(context)
			result.add(st)
		]
		result.stream().flatMap(Function.identity)
	}
}

@Aspect(className=Goal)
class GoalAspect {
	AssetBasedSystem abs;
	Variable initVar;

	def Specification convert2GAL(GALTypeDeclaration gal, Specification spec) {
		gal.transitions.add(_self.convertPre(gal))
		val property = GalFactory.eINSTANCE.createProperty
		property.name = _self.name
		property.body = _self.convertPost()
		spec.properties.add(property)
		spec
	}

	def void initialize(AssetBasedSystem abs) {
		_self.abs = abs
	}

	def Transition convertPre(GALTypeDeclaration gal) {
		val preconditionGAL = _self.precondition.expr2boolExpr(new Context())
		val contracts = GALBuildHelper.createBigAnd(_self.abs.contracts)
		val preconditionAndContracts = if (contracts === null)
				preconditionGAL
			else
				GALBuildHelper.createBoolExprAnd(preconditionGAL, contracts)
		val CollectionsOfMultiplicityOneCantBeEmpty = GALBuildHelper.createBigAnd(
			_self.abs.CollectionsMultiplicityOneOrMany.map [ a |
				GALBuildHelper.createBoolExprNot(GALBuildHelper.createBigAnd(IntStream.range(0, a.memberSubtypeCount).boxed.map [ i |
					GALBuildHelper.createBoolExprIsFalse(a.getElementVarRef(GALBuildHelper.createConstant(i)))
				].collect(Collectors.toList)))
			].toList)
		val preconditionAndContractsAndMultiplicityOne = if (CollectionsOfMultiplicityOneCantBeEmpty === null)
				preconditionAndContracts
			else
				GALBuildHelper.createBoolExprAnd(preconditionAndContracts, CollectionsOfMultiplicityOneCantBeEmpty)
		_self.initVar = GALBuildHelper.createVariable("_init", GALBuildHelper.galFalse)
		gal.variables.add(_self.initVar)
		val initTr = GALBuildHelper.createTransition("init")
		initTr.guard = GALBuildHelper.createBoolExprIsFalse(GALBuildHelper.createVarRef(_self.initVar))
		initTr.actions.add(
			GALBuildHelper.createAssign(GALBuildHelper.createVarRef(_self.initVar),
				GALBuildHelper.createConstant(GALBuildHelper.galTrue)))
		// Add the test for the init variable to every transition
		gal.transitions.forEach [ t |
			t.guard = GALBuildHelper.createBoolExprAnd(
				GALBuildHelper.createBoolExprIsTrue(GALBuildHelper.createVarRef(_self.initVar)), t.guard)
		]
		// Affect all possible values
		initTr.actions.addAll(_self.abs.uninitializedCells.createAllInitAssigns(gal, _self))

		// abort if the precondition is not satisfied
		initTr.actions.add(
			GALBuildHelper.createIfThen(GALBuildHelper.createBoolExprNot(preconditionAndContractsAndMultiplicityOne),
				GALBuildHelper.createAbort))
		initTr
	}

	def ReachableProp convertPost() {
		val reach = GalFactory.eINSTANCE.createReachableProp
		val post = _self.postcondition.expr2boolExpr(new Context())
		val initdone = GALBuildHelper.createBoolExprComp(GALBuildHelper.createVarRef(_self.initVar),
			GALBuildHelper.createConstant(1), GALBuildHelper.getComparisonOperator("=="))
		reach.predicate = GALBuildHelper.createBoolExprAnd(initdone, post)
		reach
	}
}

class UninitializedCellsInfo {
	@Data
	static private class Cell {
		public final ArrayPrefix array;
		public final int index;

		new(ArrayPrefix array, int index) {
			this.array = array
			this.index = index
		}

		new(Pair<ArrayPrefix, Integer> pair) {
			this.array = pair.key
			this.index = pair.value
		}

		def toPair() {
			array -> index
		}
	}

	HashMap<Cell, ExpressionType> cellType
	HashSet<Cell> cellWithDefault
	HashMap<ArrayPrefix, Multiplicity> multiplicityOfArray

	TypedefDeclaration boolTypedef

	new() {
		cellType = new HashMap<Cell, ExpressionType>()
		cellWithDefault = new HashSet<Cell>()
		multiplicityOfArray = new HashMap<ArrayPrefix, Multiplicity>()
	}

	def void addCellInfo(ArrayPrefix array, int index, boolean hasDefault, Multiplicity mult, ExpressionType featureType) {
		val cell = new Cell(array, index)
		cellType.put(cell, featureType)
		if (hasDefault) {
			cellWithDefault.add(cell)
		}
		multiplicityOfArray.put(array, mult)
	}

	def isMultiplicityZeroOrOne(ArrayPrefix array) {
		multiplicityOfArray.get(array) == Multiplicity.ZERO_OR_ONE
	}

	def isMultiplicityMany(ArrayPrefix array) {
		val mult = multiplicityOfArray.get(array)
		mult == Multiplicity.ZERO_OR_MANY || mult == Multiplicity.ONE_OR_MANY
	}

	def List<Statement> createAllInitAssigns(GALTypeDeclaration gal, Goal goal) {
		setupBoolTypeDef(gal, goal.eResource.resourceSet)
		val transis = newArrayList
		val labels = newArrayList
		cellType.filter [ cell, type |
			! cellWithDefault.contains(cell) || goal.precondition.referredCells(new Context()).contains(cell.toPair)
		].forEach [ cell, type |
			val array = cell.array
			val index = cell.index
			val label = GALBuildHelper.createLabel(array.name + "_" + index + "_init")
			labels.add(label)
			if (isMultiplicityMany(array)) {
				transis.add(createCellMultiplicityManyInitAssign(array, index, label))
			} else {
				transis.addAll(createAllCellMultiplicityOneInitAssigns(array, index, label))
			}
		]
		// sort to make the conversion more deterministic, easing the tests
		transis.sortBy[name].forEach[gal.transitions.add(it)]
		labels.sortBy[name].map[GALBuildHelper.createCall(it)]
	}

	def setupBoolTypeDef(GALTypeDeclaration gal, ResourceSet absrs) {
		if (boolTypedef === null) {
			val fromabs = absrs.allContents.filter(PrimitiveDataType).findFirst[it.name == "Boolean"]?.generatedTypedef
			if (fromabs !== null) {
				boolTypedef = fromabs
			} else {
				boolTypedef = GALBuildHelper.createTypeDefDeclaration("Bool", 0, 1)
				gal.typedefs.add(boolTypedef)
			}
		}
	}

	def Transition createCellMultiplicityManyInitAssign(ArrayPrefix array, int index, Label label) {
		val result = GALBuildHelper.createTransition(array.name + "_" + index + "_init")
		result.guard = GALBuildHelper.boolTrue
		result.label = EcoreUtil.copy(label)
		val param = GALBuildHelper.createParam(GALBuildHelper.convert2ParameterName(array.name), boolTypedef)
		result.params.add(param)
		result.actions.add(
			GALBuildHelper.createAssign(GALBuildHelper.createArrayRef(array, GALBuildHelper.createConstant(index)),
				GALBuildHelper.createParamRef(param)))
		return result
	}

	def List<Transition> createAllCellMultiplicityOneInitAssigns(ArrayPrefix array, int index, Label label) {
		val result = newArrayList
		if (isMultiplicityZeroOrOne(array)) {
			val transi_undef = GALBuildHelper.createTransition(array.name + "_" + index + "_init_undef")
			transi_undef.guard = GALBuildHelper.boolTrue
			transi_undef.label = EcoreUtil.copy(label)
			transi_undef.actions.add(
				GALBuildHelper.createAssign(GALBuildHelper.createArrayRef(array, GALBuildHelper.createConstant(index)),
					GALBuildHelper.createConstant(GALBuildHelper.galNone)))
			result.add(transi_undef)
		}
		val cell = new Cell(array, index)
		cellType.get(cell).subtypes.forEach [
			if(it.count > 0) result.add(createInitAssign(array, index, it, EcoreUtil.copy(label)))
		]
		return result
	}

	def Transition createInitAssign(ArrayPrefix a, int index, ExpressionType type, Label label) {
		if (type.count > 0) {
			val transi = GALBuildHelper.createTransition(a.name + "_" + index + "_" + type.typeName + "_init")
			val param = GALBuildHelper.createParam(GALBuildHelper.convert2ParameterName(a.name), type.generatedTypedef)
			transi.params.add(param)
			transi.guard = GALBuildHelper.boolTrue
			transi.label = label
			transi.actions.add(
				GALBuildHelper.createAssign(GALBuildHelper.createArrayRef(a, GALBuildHelper.createConstant(index)),
					type.getGloballyTypedValue(GALBuildHelper.createParamRef(param))))
			transi
		}
	}
}
