/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.aspects;

import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.lip6.move.gal.ArrayPrefix;
import fr.lip6.move.gal.IntExpression;
import fr.lip6.move.gal.VariableReference;

public class ArrayCollectionRef extends FiltrableCollectionRef {
	
	private ArrayPrefix array;
	private IntExpression ContainingAssetIndex;
	private int memberSubtypeCount;

	ArrayCollectionRef(ArrayPrefix a, IntExpression i1, int s1) {
		super();
		array = a;
		ContainingAssetIndex = i1;
		memberSubtypeCount = s1;
		
	}

	ArrayCollectionRef(ArrayCollectionRef other) {
		super(other);
		array = other.array;
		ContainingAssetIndex = EcoreUtil.copy(other.ContainingAssetIndex);
		memberSubtypeCount = other.memberSubtypeCount;
	}


	@Override
	public IntExpression getIsContainedIntExpression(IntExpression i) {
		return getElementVarRef(i);
	}

	@Override
	public VariableReference getElementVarRef(IntExpression i) {
		IntExpression index = GALBuildHelper.createOperation(
				GALBuildHelper.createOperation(EcoreUtil.copy(ContainingAssetIndex), GALBuildHelper.createConstant(memberSubtypeCount), "*"), i,
				"+");
			return GALBuildHelper.createArrayRef(array, index);
	}

	
	@Override
	public FiltrableCollectionRef copy() {
		return new ArrayCollectionRef(this);
	}
	
	public int getMemberSubtypeCount() {
		return memberSubtypeCount;
	}

}
