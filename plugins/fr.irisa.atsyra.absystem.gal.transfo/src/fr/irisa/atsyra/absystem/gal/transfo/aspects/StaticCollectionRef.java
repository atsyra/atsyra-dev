/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.aspects;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.lip6.move.gal.BooleanExpression;
import fr.lip6.move.gal.ComparisonOperators;
import fr.lip6.move.gal.IntExpression;
import fr.lip6.move.gal.VariableReference;

public class StaticCollectionRef extends FiltrableCollectionRef {

	private List<IntExpression> elements;

	public StaticCollectionRef() {
		super();
		elements = new ArrayList<>();
	}

	public StaticCollectionRef(StaticCollectionRef other) {
		super(other);
		elements = new ArrayList<>(other.elements);
	}

	@Override
	public IntExpression getIsContainedIntExpression(IntExpression i) {
		return GALBuildHelper.createWrapBool(getIsContainedExpression(i));
	}

	@Override
	public VariableReference getElementVarRef(IntExpression i) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public BooleanExpression getIsContainedExpression(IntExpression i) {
		return GALBuildHelper.createBigOr(elements.stream()
				.map(element -> GALBuildHelper.createBoolExprComp(EcoreUtil.copy(element), EcoreUtil.copy(i), ComparisonOperators.EQ))
				.collect(Collectors.toList()));
	}

	@Override
	public FiltrableCollectionRef copy() {
		return new StaticCollectionRef(this);
	}
	
	public void addElement(IntExpression element) {
		elements.add(element);
	}

}
