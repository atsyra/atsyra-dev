/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.irisa.atsyra.absystem.gal.transfo.helpers.ABSScenarioBuilder;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;

public class GAL2Scenario {
	
	public GAL2Scenario() {
	}
	
	public String gal2scenario(String galOutput, TransfoTraceModel traceModel, IProgressMonitor monitor) {
		ABSScenarioBuilder scenarioBuilder = new ABSScenarioBuilder(traceModel, null);
		return scenarioBuilder.convert2StringWithTraceabilityMapping(galOutput, null, traceModel,
				monitor.slice(1));
	}
}
