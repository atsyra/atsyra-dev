/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.helpers;

import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetTypeAspect;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.GALBuildHelper;
import fr.irisa.atsyra.absystem.gal.transfo.aspects.PrimitiveDataTypeAspect;
import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceQuery;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.AssetLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.AssetTypeFeatureLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.AssetTypeLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.ConstantLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.EnumLiteralLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.PrimitiveDataTypeLink;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetInstance;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant;
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression;
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.IntConstant;
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity;
import fr.irisa.atsyra.absystem.model.absystem.State;
import fr.irisa.atsyra.absystem.model.absystem.State.AssetMemberInstance;
import fr.irisa.atsyra.absystem.model.absystem.StringConstant;
import fr.irisa.atsyra.absystem.model.absystem.Version;
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.irisa.atsyra.gal.GalExpressionInterpreter;
import fr.irisa.atsyra.gal.IntExpressionEval;
import fr.irisa.atsyra.gal.IntExpressionEval.NotEvaluableException;
import fr.lip6.move.gal.GALTypeDeclaration;

public class ABSStateBuilder {
	static final Logger logger = LoggerFactory.getLogger(ABSStateBuilder.class);
	fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel traceModel;
	
	private IntExpressionEval intExpressionEval = new IntExpressionEval();

	public ABSStateBuilder(TransfoTraceModel traceModel) {
		this.traceModel = traceModel;
	}

	public State stateFromGalString(String galstring, GALTypeDeclaration gal) {
		return cellMapping2State(galstring2CellMapping(galstring, gal));
	}

	private Map<String, Integer> galstring2CellMapping(String galString, GALTypeDeclaration gal) {
		logger.trace("entering galstring2CellMapping");
		Set<String> remainingVariables = getAllVariables(gal);
		Map<String, Integer> map = new HashMap<>();
		String[] values = galString.split("\\s");
		for (String value : values) {
			String[] affected = value.split("=");
			if (affected.length < 2 || affected[0].equals("_init"))
				continue;
			map.put(affected[0], Integer.parseInt(affected[1]));
			remainingVariables.remove(affected[0]);
		}
		for (String variable : remainingVariables) {
			map.put(variable, 0);
		}
		return map;
	}

	private State cellMapping2State(Map<String, Integer> map) {
		logger.trace("entering cellMapping2State");
		State state = new State();
		for (Entry<String, Integer> entry : map.entrySet()) {
			String cellname = entry.getKey();
			Integer cellValue = entry.getValue();
			logger.trace("entry: {} = {}", cellname, cellValue);
			String[] cellnameSplit = cellname.split("\\[");
			String arrayName = cellnameSplit[0];
			int cellIndex = Integer.parseInt(cellnameSplit[1].substring(0, cellnameSplit[1].length() - 1));
			// We have arrayName, cellIndex and cellValue, continue to filling the state
			processCell(arrayName, cellIndex, cellValue).ifPresent(inst -> inst.addToState(state));
		}
		return state;
	}

	private Optional<AssetMemberInstance> processCell(String arrayName, int cellIndex, int cellValue) {
		AssetTypeFeature feature = arrayName2feature(arrayName);
		if(feature == null) {
			return Optional.empty();
		}
		if (feature.getMultiplicity().equals(Multiplicity.ONE)
				|| feature.getMultiplicity().equals(Multiplicity.ZERO_OR_ONE)) {
			return processSimpleCell(arrayName, feature, cellIndex, cellValue);
		} else {
			return processCollectionCell(arrayName, feature, cellIndex, cellValue);
		}
	}

	private Optional<AssetMemberInstance> processSimpleCell(String arrayName, AssetTypeFeature feature, int cellIndex,
			int cellValue) {
		logger.trace("entering processSimpleCell");
		AssetType cellType = ABSUtils.getContainerType(feature);
		Optional<Asset> cellAsset = getAssetFromSimpleCell(cellType, cellIndex);
		return cellAsset.map(asset -> {
			if (cellValue == -1) {
				return new AssetMemberInstance(asset, feature);
			} else if (feature instanceof AssetTypeAttribute) {
				// cellValue = primitive value
				AssetTypeAttribute attribute = (AssetTypeAttribute) feature;
				if (attribute.getAttributeType() instanceof EnumDataType) {
					EnumDataType enumtype = (EnumDataType) attribute.getAttributeType();
					EnumLiteral literal = ABS2GALTraceQuery.of(traceModel).fromTypeAndValue(enumtype, cellValue);
					return new AssetMemberInstance(asset, feature, literal);
				} else {
					ConstantExpression cvalue = ABS2GALTraceQuery.of(traceModel).fromTypeAndValue(attribute.getAttributeType(), cellValue);
					return new AssetMemberInstance(asset, feature, getValue(cvalue));
				}
			} else { // feature instanceof AssetTypeReference
				// cellValue = globally typed value
				AssetLink assetLink = ABS2GALTraceQuery.of(traceModel).fromGlobalValue(cellValue);
				Asset value = assetLink.getAsset();
				return new AssetMemberInstance(asset, feature, value);
			}
		});
	}

	private Object getValue(ConstantExpression cvalue) {
		if(cvalue instanceof BooleanConstant) {
			return ((BooleanConstant) cvalue).getValue();
		} else if(cvalue instanceof IntConstant) {
			return ((IntConstant) cvalue).getValue();
		} else if(cvalue instanceof StringConstant) {
			return ((StringConstant) cvalue).getValue();
		} else if(cvalue instanceof VersionConstant) {
			return ((VersionConstant) cvalue).getValue();
		} else {
			return null;
		}
	}

	private Optional<AssetMemberInstance> processCollectionCell(String arrayName, AssetTypeFeature feature,
			int cellIndex, int cellValue) {
		logger.trace("entering processCollectionCell");
		if (cellValue == 1) {
			AssetType cellType = ABSUtils.getContainerType(feature);
			Optional<Asset> cellAsset = getAssetFromCollectionCell(arrayName, feature, cellType, cellIndex);
			return cellAsset.map(asset -> {
				if (feature instanceof AssetTypeAttribute) {
					AssetTypeAttribute attribute = (AssetTypeAttribute) feature;
					if (attribute.getAttributeType() instanceof EnumDataType) {
						EnumDataType enumtype = (EnumDataType) attribute.getAttributeType();
						PrimitiveDataTypeLink link = ABS2GALTraceQuery.of(traceModel).fromPrimitiveDataType(enumtype);
						int memberTypeCount = link.getCount();
						EnumLiteral literal = ABS2GALTraceQuery.of(traceModel).fromTypeAndValue(enumtype, cellIndex % memberTypeCount);
						return new AssetMemberInstance(asset, feature, literal);
					} else {
						PrimitiveDataTypeLink link = ABS2GALTraceQuery.of(traceModel)
								.fromPrimitiveDataType(attribute.getAttributeType());
						int memberTypeCount = link.getCount();
						ConstantExpression cvalue = ABS2GALTraceQuery.of(traceModel).fromTypeAndValue(attribute.getAttributeType(), cellIndex % memberTypeCount);
						return new AssetMemberInstance(asset, feature, getValue(cvalue));
					}
				} else {
					AssetType memberAssetType = arrayName2memberType(arrayName);
					AssetTypeLink memberAssetTypeLink = ABS2GALTraceQuery.of(traceModel).fromAssetType(memberAssetType);
					int memberTypeCount = memberAssetTypeLink.getCount();
					return new AssetMemberInstance(asset, feature, ABS2GALTraceQuery.of(traceModel)
							.getProperAsset(memberAssetType, cellIndex % memberTypeCount).orElseThrow());
				}
			});
		} else {
			return Optional.empty();
		}
	}

	private Optional<Asset> getAssetFromSimpleCell(AssetType cellType, int cellIndex) {
		logger.trace("entering getAssetFromSimpleCell");
		// The cellIndex is already the subtyped value
		return getAssetFromSubtypedValue(cellType, cellIndex);
	}

	private Optional<Asset> getAssetFromCollectionCell(String arrayName, AssetTypeFeature feature, AssetType cellType, int cellIndex) {
		logger.trace("entering getAssetFromCollectionCell");
		if (feature instanceof AssetTypeAttribute) {
			AssetTypeAttribute attribute = (AssetTypeAttribute) feature;
			PrimitiveDataTypeLink link = ABS2GALTraceQuery.of(traceModel)
					.fromPrimitiveDataType(attribute.getAttributeType());
			int subtyped = cellIndex / link.getCount();
			return getAssetFromSubtypedValue(cellType, subtyped);
		} else {
			AssetType memberAssetType = arrayName2memberType(arrayName);
			AssetTypeLink link = ABS2GALTraceQuery.of(traceModel).fromAssetType(memberAssetType);
			int subtyped = cellIndex / link.getCount(); 
			return getAssetFromSubtypedValue(cellType, subtyped);
		}
	}

	private Optional<Asset> getAssetFromSubtypedValue(AssetType assetType, int subtypedValue) {
		logger.trace("entering getAssetFromSubtypedValue");
		AssetTypeLink assetTypeLink = ABS2GALTraceQuery.of(traceModel).fromAssetType(assetType);
		int nbSubtypes = (int) numberOfSubtypes(assetTypeLink);
		int properValue = subtypedValue / nbSubtypes;
		int subtypeLocalNumber = subtypedValue % nbSubtypes;
		int subtypeGloballNumber = typeNumberFromSubtypeNumber(assetTypeLink, subtypeLocalNumber);
		AssetTypeLink subtypeLink = ABS2GALTraceQuery.of(traceModel).fromAssetTypeNumber(subtypeGloballNumber);
		AssetType subtype = subtypeLink.getAssetType();
		return ABS2GALTraceQuery.of(traceModel).getProperAsset(subtype, properValue);
	}

	private AssetType arrayName2memberType(String arrayName) {
		logger.trace("entering arrayName2memberType");
		AssetTypeFeatureLink featureLink = ABS2GALTraceQuery.of(traceModel).fromArrayName(arrayName);
		return featureLink.getMemberSubtype().orElseThrow();
	}

	private AssetTypeFeature arrayName2feature(String arrayName) {
		logger.trace("entering arrayName2feature");
		AssetTypeFeatureLink featureLink = ABS2GALTraceQuery.of(traceModel).fromArrayName(arrayName);
		return featureLink == null ? null : featureLink.getFeature();
	}

	public Map<String, Integer> state2Mapping(State state) {
		logger.trace("entering state2Mapping");
		Map<String, Integer> result = new HashMap<>();
		for (Entry<Asset, AssetInstance> entry : state.getValuation().entrySet()) {
			Asset asset = entry.getKey();
			AssetLink assetLink = ABS2GALTraceQuery.of(traceModel).fromAsset(asset);
			AssetInstance instance = entry.getValue();
			logger.trace("entry: {}", asset.getName());
			for (Entry<AssetTypeReference, Collection<Asset>> rc : instance.getRefcollections().asMap().entrySet()) {
				for (Asset ref : ABSUtils.getAllSubtypedAssets(rc.getKey().getPropertyType())) {
					AssetLink link = ABS2GALTraceQuery.of(traceModel).fromAsset(ref);
					result.put(getGALVariableMany(rc.getKey(), assetLink, link), rc.getValue().contains(ref) ? 1 : 0);
				}
			}
			Stream<Entry<AssetTypeAttribute, ? extends Collection<?>>> primiviteCollections = Stream.concat(
					instance.getBoolcollections().asMap().entrySet().stream(),
					Stream.concat(instance.getIntcollections().asMap().entrySet().stream(),
							Stream.concat(instance.getStringcollections().asMap().entrySet().stream(),
									instance.getVersioncollections().asMap().entrySet().stream())));
			primiviteCollections.forEach(collecEntry -> {
				PrimitiveDataTypeLink typeLink = ABS2GALTraceQuery.of(traceModel).fromPrimitiveDataType(collecEntry.getKey().getAttributeType());
				for (ConstantLink link : ABS2GALTraceQuery.of(traceModel).getConstantsOfPrimitiveDataType(collecEntry.getKey().getAttributeType())) {
					result.put(getGALVariableMany(collecEntry.getKey(), assetLink, typeLink, link),
							collecEntry.getValue().contains(getValue(link.getConstant())) ? 1 : 0);
				}
			});
			for (Entry<AssetTypeAttribute, Collection<EnumLiteral>> ec : instance.getEnumcollections().asMap()
					.entrySet()) {
				PrimitiveDataTypeLink typeLink = ABS2GALTraceQuery.of(traceModel).fromPrimitiveDataType((EnumDataType) ec.getKey().getAttributeType());
				for (EnumLiteralLink link : ABS2GALTraceQuery.of(traceModel)
						.getEnumLiteralOfEnumDataType((EnumDataType) ec.getKey().getAttributeType())) {
					result.put(getGALVariableMany(ec.getKey(), assetLink, typeLink, link),
							ec.getValue().contains(link.getLiteral()) ? 1 : 0);
				}
			}
			Stream<Entry<? extends AssetTypeFeature, ? extends Optional<? extends Object>>> simpleFeatures = Stream
					.concat(instance.getRefvalues().entrySet().stream(),
							Stream.concat(instance.getBoolvalues().entrySet().stream(),
									Stream.concat(instance.getIntvalues().entrySet().stream(),
											Stream.concat(instance.getStringvalues().entrySet().stream(),
													instance.getEnumvalues().entrySet().stream()))));
			simpleFeatures.forEach(featureEntry -> result.put(getGALVariableOne(featureEntry.getKey(), assetLink),
					getGalGlobalValue(featureEntry.getKey(), featureEntry.getValue())));
		}
		return result;
	}

	private int getGalGlobalValue(AssetTypeFeature feature, Optional<?> o) {
		logger.trace("entering getGalGlobalValue");
		return o.map(opt -> getGalGlobalValue(feature, opt)).orElse(GALBuildHelper.galNone);
	}

	private int getGalGlobalValue(AssetTypeFeature feature, Object o) {
		if (o instanceof Asset) {
			AssetLink link = ABS2GALTraceQuery.of(traceModel).fromAsset((Asset) o);
			return link.getGlobalvalue();
		} else if (feature instanceof AssetTypeAttribute) {
			AssetTypeAttribute attribute = (AssetTypeAttribute) feature;
			if(attribute.getAttributeType() instanceof EnumDataType) {
				Collection<EnumLiteralLink> enumLiterals = ABS2GALTraceQuery.of(traceModel).getEnumLiteralOfEnumDataType((EnumDataType) attribute.getAttributeType());
				return enumLiterals.stream().filter(link -> Objects.equals(link.getLiteral(), o)).findAny()
						.map(EnumLiteralLink::getValue).orElseThrow();
			} else {
				Collection<ConstantLink> constants = ABS2GALTraceQuery.of(traceModel).getConstantsOfPrimitiveDataType(attribute.getAttributeType());
				return constants.stream().filter(clink -> Objects.equals(getValue(clink.getConstant()), o)).findAny()
						.map(ConstantLink::getValue).orElseThrow();
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	private String getTypeName(Object o) {
		if (o instanceof Boolean) {
			return "Boolean";
		} else if (o instanceof Integer) {
			return "Integer";
		} else if (o instanceof String) {
			return "String";
		} else if (o instanceof Version) {
			return "Version";
		} else if (o instanceof EnumLiteral) {
			return PrimitiveDataTypeAspect.getQualifiedName((ABSUtils.getEnumLiteralType((EnumLiteral) o)));
		} else if (o instanceof Asset) {
			return AssetTypeAspect.getQualifiedName(((Asset) o).getAssetType());
		} else {
			throw new InvalidParameterException();
		}
	}

	private int getSubtypedValue(AssetTypeFeature feature, AssetLink assetLink) {
		logger.trace("entering getSubtypedValue");
		final AssetType featureContainerType = ABSUtils.getContainerType(feature);
		final AssetTypeLink featureContainerTypeLink = ABS2GALTraceQuery.of(traceModel)
				.fromAssetType(featureContainerType);
		final AssetTypeLink subtypeLink = ABS2GALTraceQuery.of(traceModel)
				.fromAssetType(assetLink.getAsset().getAssetType());
		int subtypeNumber = subtypeNumberFromTypeNumber(featureContainerTypeLink, subtypeLink.getNumber());
		int numberOfSubtypes = (int) numberOfSubtypes(featureContainerTypeLink);
		return subtypeNumber + numberOfSubtypes * assetLink.getPropervalue();
	}

	private String getGALVariableOne(AssetTypeFeature feature, AssetLink assetLink) {
		logger.trace("entering getGALVariableOne");
		final AssetType featureContainerType = ABSUtils.getContainerType(feature);
		return AssetTypeAspect.getQualifiedName(featureContainerType) + "." + feature.getName() + "["
				+ getSubtypedValue(feature, assetLink) + "]";
	}

	private String getGALVariableMany(AssetTypeReference feature, AssetLink assetLink, AssetLink link) {
		logger.trace("entering getGALVariableMany");
		AssetTypeLink typeLink = ABS2GALTraceQuery.of(traceModel).fromAssetType(link.getAsset().getAssetType());
		return getTypeName(assetLink.getAsset()) + "."
				+ AssetTypeAspect.getQualifiedName(link.getAsset().getAssetType()) + "." + feature.getName() + "["
				+ (getSubtypedValue(feature, assetLink) * typeLink.getCount() + link.getPropervalue()) + "]";
	}

	private String getGALVariableMany(AssetTypeAttribute feature, AssetLink assetLink, PrimitiveDataTypeLink typeLink, ConstantLink link) {
		logger.trace("entering getGALVariableMany");
		return getTypeName(assetLink.getAsset()) + "." + feature.getName() + "["
				+ (getSubtypedValue(feature, assetLink) * typeLink.getCount() + link.getValue()) + "]";
	}

	private String getGALVariableMany(AssetTypeAttribute feature, AssetLink assetLink, PrimitiveDataTypeLink typeLink, EnumLiteralLink link) {
		logger.trace("entering getGALVariableMany");
		return getTypeName(assetLink.getAsset()) + "." + feature.getName() + "["
				+ (getSubtypedValue(feature, assetLink) * typeLink.getCount() + link.getValue()) + "]";
	}

	private Set<String> getAllVariables(GALTypeDeclaration gal) {
		Set<String> result = new HashSet<>();
		
		gal.getArrays().forEach(a -> {
			try {
				IntStream.range(0, intExpressionEval.eval(a.getSize()))
						.forEach(i -> result.add(a.getName() + '[' + i + ']'));
			} catch (NotEvaluableException e) {
				throw new IllegalStateException(e);
			}
		});
		return result;
	}

	private int typeNumberFromSubtypeNumber(AssetTypeLink supertypeLink, int subtypeNumber) {
		logger.trace("entering typeNumberFromSubtypeNumber");
		return ListExtensions.map(supertypeLink.getSubtypesArray().getValues(), GalExpressionInterpreter::intEvaluate)
				.indexOf(subtypeNumber);
	}

	private int subtypeNumberFromTypeNumber(AssetTypeLink supertypeLink, int typeNumber) {
		logger.trace("entering subtypeNumberFromTypeNumber");
		return GalExpressionInterpreter.intEvaluate(supertypeLink.getSubtypesArray().getValues().get(typeNumber));
	}

	private long numberOfSubtypes(AssetTypeLink supertypeLink) {
		logger.trace("entering numberOfSubtypes");
		return supertypeLink.getSubtypesArray().getValues().stream().map(GalExpressionInterpreter::intEvaluate)
				.filter(value -> value != GALBuildHelper.galNone).count();
	}
}
