/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore;

import atsyragoal.AtsyraTree;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tree Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.TreeResult#getMeetResult <em>Meet Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.TreeResult#getAdmissibilityResult <em>Admissibility Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.TreeResult#getUndermatchResult <em>Undermatch Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.TreeResult#getOvermatchResult <em>Overmatch Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.TreeResult#getMatchResult <em>Match Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.TreeResult#getTree <em>Tree</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getTreeResult()
 * @model
 * @generated
 */
public interface TreeResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Meet Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Meet Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Meet Result</em>' containment reference.
	 * @see #setMeetResult(Result)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getTreeResult_MeetResult()
	 * @model containment="true"
	 * @generated
	 */
	Result getMeetResult();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.TreeResult#getMeetResult <em>Meet Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Meet Result</em>' containment reference.
	 * @see #getMeetResult()
	 * @generated
	 */
	void setMeetResult(Result value);

	/**
	 * Returns the value of the '<em><b>Admissibility Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Admissibility Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Admissibility Result</em>' containment reference.
	 * @see #setAdmissibilityResult(Result)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getTreeResult_AdmissibilityResult()
	 * @model containment="true"
	 * @generated
	 */
	Result getAdmissibilityResult();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.TreeResult#getAdmissibilityResult <em>Admissibility Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Admissibility Result</em>' containment reference.
	 * @see #getAdmissibilityResult()
	 * @generated
	 */
	void setAdmissibilityResult(Result value);

	/**
	 * Returns the value of the '<em><b>Undermatch Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Undermatch Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Undermatch Result</em>' containment reference.
	 * @see #setUndermatchResult(Result)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getTreeResult_UndermatchResult()
	 * @model containment="true"
	 * @generated
	 */
	Result getUndermatchResult();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.TreeResult#getUndermatchResult <em>Undermatch Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Undermatch Result</em>' containment reference.
	 * @see #getUndermatchResult()
	 * @generated
	 */
	void setUndermatchResult(Result value);

	/**
	 * Returns the value of the '<em><b>Overmatch Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overmatch Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overmatch Result</em>' containment reference.
	 * @see #setOvermatchResult(Result)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getTreeResult_OvermatchResult()
	 * @model containment="true"
	 * @generated
	 */
	Result getOvermatchResult();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.TreeResult#getOvermatchResult <em>Overmatch Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overmatch Result</em>' containment reference.
	 * @see #getOvermatchResult()
	 * @generated
	 */
	void setOvermatchResult(Result value);

	/**
	 * Returns the value of the '<em><b>Match Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Match Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Match Result</em>' containment reference.
	 * @see #setMatchResult(Result)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getTreeResult_MatchResult()
	 * @model containment="true"
	 * @generated
	 */
	Result getMatchResult();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.TreeResult#getMatchResult <em>Match Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Match Result</em>' containment reference.
	 * @see #getMatchResult()
	 * @generated
	 */
	void setMatchResult(Result value);

	/**
	 * Returns the value of the '<em><b>Tree</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tree</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tree</em>' reference.
	 * @see #setTree(AtsyraTree)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getTreeResult_Tree()
	 * @model required="true"
	 * @generated
	 */
	AtsyraTree getTree();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.TreeResult#getTree <em>Tree</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tree</em>' reference.
	 * @see #getTree()
	 * @generated
	 */
	void setTree(AtsyraTree value);

} // TreeResult
