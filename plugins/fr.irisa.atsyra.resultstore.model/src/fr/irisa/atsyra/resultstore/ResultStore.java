/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Result Store</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.ResultStore#getGoalResults <em>Goal Results</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.ResultStore#getTreeResults <em>Tree Results</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getResultStore()
 * @model
 * @generated
 */
public interface ResultStore extends EObject {
	/**
	 * Returns the value of the '<em><b>Goal Results</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.resultstore.GoalResult}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goal Results</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goal Results</em>' containment reference list.
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getResultStore_GoalResults()
	 * @model containment="true"
	 * @generated
	 */
	EList<GoalResult> getGoalResults();

	/**
	 * Returns the value of the '<em><b>Tree Results</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.resultstore.TreeResult}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tree Results</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tree Results</em>' containment reference list.
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getResultStore_TreeResults()
	 * @model containment="true"
	 * @generated
	 */
	EList<TreeResult> getTreeResults();

} // ResultStore
