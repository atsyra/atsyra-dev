/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.atsyra.resultstore.ResultstoreFactory
 * @model kind="package"
 * @generated
 */
public interface ResultstorePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "resultstore";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/atsyra/resultstore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "resultstore";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ResultstorePackage eINSTANCE = fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.resultstore.impl.ResultStoreImpl <em>Result Store</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.resultstore.impl.ResultStoreImpl
	 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getResultStore()
	 * @generated
	 */
	int RESULT_STORE = 0;

	/**
	 * The feature id for the '<em><b>Goal Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_STORE__GOAL_RESULTS = 0;

	/**
	 * The feature id for the '<em><b>Tree Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_STORE__TREE_RESULTS = 1;

	/**
	 * The number of structural features of the '<em>Result Store</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_STORE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Result Store</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_STORE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.resultstore.impl.GoalResultImpl <em>Goal Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.resultstore.impl.GoalResultImpl
	 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getGoalResult()
	 * @generated
	 */
	int GOAL_RESULT = 1;

	/**
	 * The feature id for the '<em><b>Reachability Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RESULT__REACHABILITY_RESULT = 0;

	/**
	 * The feature id for the '<em><b>Goal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RESULT__GOAL = 1;

	/**
	 * The feature id for the '<em><b>Shortest Witness Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RESULT__SHORTEST_WITNESS_RESULT = 2;

	/**
	 * The feature id for the '<em><b>Complementary Witness Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS = 3;

	/**
	 * The feature id for the '<em><b>Precondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RESULT__PRECONDITION = 4;

	/**
	 * The feature id for the '<em><b>Postcondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RESULT__POSTCONDITION = 5;

	/**
	 * The number of structural features of the '<em>Goal Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RESULT_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Goal Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RESULT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.resultstore.impl.TreeResultImpl <em>Tree Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.resultstore.impl.TreeResultImpl
	 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getTreeResult()
	 * @generated
	 */
	int TREE_RESULT = 2;

	/**
	 * The feature id for the '<em><b>Meet Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_RESULT__MEET_RESULT = 0;

	/**
	 * The feature id for the '<em><b>Admissibility Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_RESULT__ADMISSIBILITY_RESULT = 1;

	/**
	 * The feature id for the '<em><b>Undermatch Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_RESULT__UNDERMATCH_RESULT = 2;

	/**
	 * The feature id for the '<em><b>Overmatch Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_RESULT__OVERMATCH_RESULT = 3;

	/**
	 * The feature id for the '<em><b>Match Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_RESULT__MATCH_RESULT = 4;

	/**
	 * The feature id for the '<em><b>Tree</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_RESULT__TREE = 5;

	/**
	 * The number of structural features of the '<em>Tree Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_RESULT_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Tree Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_RESULT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.resultstore.impl.ResultImpl <em>Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.resultstore.impl.ResultImpl
	 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getResult()
	 * @generated
	 */
	int RESULT = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Details</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT__DETAILS = 1;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT__TIMESTAMP = 2;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT__DURATION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT__NAME = 4;

	/**
	 * The feature id for the '<em><b>Log</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT__LOG = 5;

	/**
	 * The number of structural features of the '<em>Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl <em>Witness Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.resultstore.impl.WitnessResultImpl
	 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getWitnessResult()
	 * @generated
	 */
	int WITNESS_RESULT = 4;

	/**
	 * The feature id for the '<em><b>Witness Found</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT__WITNESS_FOUND = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT__VALUE = 1;

	/**
	 * The feature id for the '<em><b>Details</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT__DETAILS = 2;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT__TIMESTAMP = 3;

	/**
	 * The feature id for the '<em><b>Call Parameters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT__CALL_PARAMETERS = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT__NAME = 5;

	/**
	 * The feature id for the '<em><b>Log</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT__LOG = 6;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT__DURATION = 7;

	/**
	 * The number of structural features of the '<em>Witness Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Witness Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITNESS_RESULT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.resultstore.impl.ConditionResultImpl <em>Condition Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.resultstore.impl.ConditionResultImpl
	 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getConditionResult()
	 * @generated
	 */
	int CONDITION_RESULT = 5;

	/**
	 * The feature id for the '<em><b>Goalcondition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_RESULT__GOALCONDITION = 0;

	/**
	 * The feature id for the '<em><b>Scenarios Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_RESULT__SCENARIOS_RESULT = 1;

	/**
	 * The feature id for the '<em><b>Reachability Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_RESULT__REACHABILITY_RESULT = 2;

	/**
	 * The number of structural features of the '<em>Condition Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_RESULT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Condition Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_RESULT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.resultstore.ResultValue <em>Result Value</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.resultstore.ResultValue
	 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getResultValue()
	 * @generated
	 */
	int RESULT_VALUE = 6;


	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.resultstore.ResultStore <em>Result Store</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Result Store</em>'.
	 * @see fr.irisa.atsyra.resultstore.ResultStore
	 * @generated
	 */
	EClass getResultStore();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.resultstore.ResultStore#getGoalResults <em>Goal Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Goal Results</em>'.
	 * @see fr.irisa.atsyra.resultstore.ResultStore#getGoalResults()
	 * @see #getResultStore()
	 * @generated
	 */
	EReference getResultStore_GoalResults();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.resultstore.ResultStore#getTreeResults <em>Tree Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tree Results</em>'.
	 * @see fr.irisa.atsyra.resultstore.ResultStore#getTreeResults()
	 * @see #getResultStore()
	 * @generated
	 */
	EReference getResultStore_TreeResults();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.resultstore.GoalResult <em>Goal Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.GoalResult
	 * @generated
	 */
	EClass getGoalResult();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.GoalResult#getReachabilityResult <em>Reachability Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reachability Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.GoalResult#getReachabilityResult()
	 * @see #getGoalResult()
	 * @generated
	 */
	EReference getGoalResult_ReachabilityResult();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.resultstore.GoalResult#getGoal <em>Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Goal</em>'.
	 * @see fr.irisa.atsyra.resultstore.GoalResult#getGoal()
	 * @see #getGoalResult()
	 * @generated
	 */
	EReference getGoalResult_Goal();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.GoalResult#getShortestWitnessResult <em>Shortest Witness Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Shortest Witness Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.GoalResult#getShortestWitnessResult()
	 * @see #getGoalResult()
	 * @generated
	 */
	EReference getGoalResult_ShortestWitnessResult();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.resultstore.GoalResult#getComplementaryWitnessResults <em>Complementary Witness Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Complementary Witness Results</em>'.
	 * @see fr.irisa.atsyra.resultstore.GoalResult#getComplementaryWitnessResults()
	 * @see #getGoalResult()
	 * @generated
	 */
	EReference getGoalResult_ComplementaryWitnessResults();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.GoalResult#getPrecondition <em>Precondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Precondition</em>'.
	 * @see fr.irisa.atsyra.resultstore.GoalResult#getPrecondition()
	 * @see #getGoalResult()
	 * @generated
	 */
	EReference getGoalResult_Precondition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.GoalResult#getPostcondition <em>Postcondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Postcondition</em>'.
	 * @see fr.irisa.atsyra.resultstore.GoalResult#getPostcondition()
	 * @see #getGoalResult()
	 * @generated
	 */
	EReference getGoalResult_Postcondition();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.resultstore.TreeResult <em>Tree Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tree Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.TreeResult
	 * @generated
	 */
	EClass getTreeResult();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.TreeResult#getMeetResult <em>Meet Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Meet Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.TreeResult#getMeetResult()
	 * @see #getTreeResult()
	 * @generated
	 */
	EReference getTreeResult_MeetResult();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.TreeResult#getAdmissibilityResult <em>Admissibility Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Admissibility Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.TreeResult#getAdmissibilityResult()
	 * @see #getTreeResult()
	 * @generated
	 */
	EReference getTreeResult_AdmissibilityResult();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.TreeResult#getUndermatchResult <em>Undermatch Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Undermatch Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.TreeResult#getUndermatchResult()
	 * @see #getTreeResult()
	 * @generated
	 */
	EReference getTreeResult_UndermatchResult();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.TreeResult#getOvermatchResult <em>Overmatch Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Overmatch Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.TreeResult#getOvermatchResult()
	 * @see #getTreeResult()
	 * @generated
	 */
	EReference getTreeResult_OvermatchResult();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.TreeResult#getMatchResult <em>Match Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Match Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.TreeResult#getMatchResult()
	 * @see #getTreeResult()
	 * @generated
	 */
	EReference getTreeResult_MatchResult();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.resultstore.TreeResult#getTree <em>Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tree</em>'.
	 * @see fr.irisa.atsyra.resultstore.TreeResult#getTree()
	 * @see #getTreeResult()
	 * @generated
	 */
	EReference getTreeResult_Tree();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.resultstore.Result <em>Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.Result
	 * @generated
	 */
	EClass getResult();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.Result#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.resultstore.Result#getValue()
	 * @see #getResult()
	 * @generated
	 */
	EAttribute getResult_Value();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.Result#getDetails <em>Details</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Details</em>'.
	 * @see fr.irisa.atsyra.resultstore.Result#getDetails()
	 * @see #getResult()
	 * @generated
	 */
	EAttribute getResult_Details();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.Result#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see fr.irisa.atsyra.resultstore.Result#getTimestamp()
	 * @see #getResult()
	 * @generated
	 */
	EAttribute getResult_Timestamp();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.Result#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see fr.irisa.atsyra.resultstore.Result#getDuration()
	 * @see #getResult()
	 * @generated
	 */
	EAttribute getResult_Duration();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.Result#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.resultstore.Result#getName()
	 * @see #getResult()
	 * @generated
	 */
	EAttribute getResult_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.Result#getLog <em>Log</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Log</em>'.
	 * @see fr.irisa.atsyra.resultstore.Result#getLog()
	 * @see #getResult()
	 * @generated
	 */
	EAttribute getResult_Log();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.resultstore.WitnessResult <em>Witness Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Witness Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.WitnessResult
	 * @generated
	 */
	EClass getWitnessResult();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.atsyra.resultstore.WitnessResult#getWitnessFound <em>Witness Found</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Witness Found</em>'.
	 * @see fr.irisa.atsyra.resultstore.WitnessResult#getWitnessFound()
	 * @see #getWitnessResult()
	 * @generated
	 */
	EAttribute getWitnessResult_WitnessFound();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.WitnessResult#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.resultstore.WitnessResult#getValue()
	 * @see #getWitnessResult()
	 * @generated
	 */
	EAttribute getWitnessResult_Value();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.WitnessResult#getDetails <em>Details</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Details</em>'.
	 * @see fr.irisa.atsyra.resultstore.WitnessResult#getDetails()
	 * @see #getWitnessResult()
	 * @generated
	 */
	EAttribute getWitnessResult_Details();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.WitnessResult#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see fr.irisa.atsyra.resultstore.WitnessResult#getTimestamp()
	 * @see #getWitnessResult()
	 * @generated
	 */
	EAttribute getWitnessResult_Timestamp();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.WitnessResult#getCallParameters <em>Call Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Call Parameters</em>'.
	 * @see fr.irisa.atsyra.resultstore.WitnessResult#getCallParameters()
	 * @see #getWitnessResult()
	 * @generated
	 */
	EAttribute getWitnessResult_CallParameters();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.WitnessResult#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.resultstore.WitnessResult#getName()
	 * @see #getWitnessResult()
	 * @generated
	 */
	EAttribute getWitnessResult_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.WitnessResult#getLog <em>Log</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Log</em>'.
	 * @see fr.irisa.atsyra.resultstore.WitnessResult#getLog()
	 * @see #getWitnessResult()
	 * @generated
	 */
	EAttribute getWitnessResult_Log();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.resultstore.WitnessResult#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see fr.irisa.atsyra.resultstore.WitnessResult#getDuration()
	 * @see #getWitnessResult()
	 * @generated
	 */
	EAttribute getWitnessResult_Duration();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.resultstore.ConditionResult <em>Condition Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.ConditionResult
	 * @generated
	 */
	EClass getConditionResult();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.resultstore.ConditionResult#getGoalcondition <em>Goalcondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Goalcondition</em>'.
	 * @see fr.irisa.atsyra.resultstore.ConditionResult#getGoalcondition()
	 * @see #getConditionResult()
	 * @generated
	 */
	EReference getConditionResult_Goalcondition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.ConditionResult#getScenariosResult <em>Scenarios Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Scenarios Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.ConditionResult#getScenariosResult()
	 * @see #getConditionResult()
	 * @generated
	 */
	EReference getConditionResult_ScenariosResult();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.resultstore.ConditionResult#getReachabilityResult <em>Reachability Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reachability Result</em>'.
	 * @see fr.irisa.atsyra.resultstore.ConditionResult#getReachabilityResult()
	 * @see #getConditionResult()
	 * @generated
	 */
	EReference getConditionResult_ReachabilityResult();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.atsyra.resultstore.ResultValue <em>Result Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Result Value</em>'.
	 * @see fr.irisa.atsyra.resultstore.ResultValue
	 * @generated
	 */
	EEnum getResultValue();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ResultstoreFactory getResultstoreFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.resultstore.impl.ResultStoreImpl <em>Result Store</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.resultstore.impl.ResultStoreImpl
		 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getResultStore()
		 * @generated
		 */
		EClass RESULT_STORE = eINSTANCE.getResultStore();

		/**
		 * The meta object literal for the '<em><b>Goal Results</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESULT_STORE__GOAL_RESULTS = eINSTANCE.getResultStore_GoalResults();

		/**
		 * The meta object literal for the '<em><b>Tree Results</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESULT_STORE__TREE_RESULTS = eINSTANCE.getResultStore_TreeResults();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.resultstore.impl.GoalResultImpl <em>Goal Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.resultstore.impl.GoalResultImpl
		 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getGoalResult()
		 * @generated
		 */
		EClass GOAL_RESULT = eINSTANCE.getGoalResult();

		/**
		 * The meta object literal for the '<em><b>Reachability Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL_RESULT__REACHABILITY_RESULT = eINSTANCE.getGoalResult_ReachabilityResult();

		/**
		 * The meta object literal for the '<em><b>Goal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL_RESULT__GOAL = eINSTANCE.getGoalResult_Goal();

		/**
		 * The meta object literal for the '<em><b>Shortest Witness Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL_RESULT__SHORTEST_WITNESS_RESULT = eINSTANCE.getGoalResult_ShortestWitnessResult();

		/**
		 * The meta object literal for the '<em><b>Complementary Witness Results</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS = eINSTANCE.getGoalResult_ComplementaryWitnessResults();

		/**
		 * The meta object literal for the '<em><b>Precondition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL_RESULT__PRECONDITION = eINSTANCE.getGoalResult_Precondition();

		/**
		 * The meta object literal for the '<em><b>Postcondition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL_RESULT__POSTCONDITION = eINSTANCE.getGoalResult_Postcondition();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.resultstore.impl.TreeResultImpl <em>Tree Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.resultstore.impl.TreeResultImpl
		 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getTreeResult()
		 * @generated
		 */
		EClass TREE_RESULT = eINSTANCE.getTreeResult();

		/**
		 * The meta object literal for the '<em><b>Meet Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_RESULT__MEET_RESULT = eINSTANCE.getTreeResult_MeetResult();

		/**
		 * The meta object literal for the '<em><b>Admissibility Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_RESULT__ADMISSIBILITY_RESULT = eINSTANCE.getTreeResult_AdmissibilityResult();

		/**
		 * The meta object literal for the '<em><b>Undermatch Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_RESULT__UNDERMATCH_RESULT = eINSTANCE.getTreeResult_UndermatchResult();

		/**
		 * The meta object literal for the '<em><b>Overmatch Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_RESULT__OVERMATCH_RESULT = eINSTANCE.getTreeResult_OvermatchResult();

		/**
		 * The meta object literal for the '<em><b>Match Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_RESULT__MATCH_RESULT = eINSTANCE.getTreeResult_MatchResult();

		/**
		 * The meta object literal for the '<em><b>Tree</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_RESULT__TREE = eINSTANCE.getTreeResult_Tree();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.resultstore.impl.ResultImpl <em>Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.resultstore.impl.ResultImpl
		 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getResult()
		 * @generated
		 */
		EClass RESULT = eINSTANCE.getResult();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESULT__VALUE = eINSTANCE.getResult_Value();

		/**
		 * The meta object literal for the '<em><b>Details</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESULT__DETAILS = eINSTANCE.getResult_Details();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESULT__TIMESTAMP = eINSTANCE.getResult_Timestamp();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESULT__DURATION = eINSTANCE.getResult_Duration();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESULT__NAME = eINSTANCE.getResult_Name();

		/**
		 * The meta object literal for the '<em><b>Log</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESULT__LOG = eINSTANCE.getResult_Log();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl <em>Witness Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.resultstore.impl.WitnessResultImpl
		 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getWitnessResult()
		 * @generated
		 */
		EClass WITNESS_RESULT = eINSTANCE.getWitnessResult();

		/**
		 * The meta object literal for the '<em><b>Witness Found</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WITNESS_RESULT__WITNESS_FOUND = eINSTANCE.getWitnessResult_WitnessFound();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WITNESS_RESULT__VALUE = eINSTANCE.getWitnessResult_Value();

		/**
		 * The meta object literal for the '<em><b>Details</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WITNESS_RESULT__DETAILS = eINSTANCE.getWitnessResult_Details();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WITNESS_RESULT__TIMESTAMP = eINSTANCE.getWitnessResult_Timestamp();

		/**
		 * The meta object literal for the '<em><b>Call Parameters</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WITNESS_RESULT__CALL_PARAMETERS = eINSTANCE.getWitnessResult_CallParameters();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WITNESS_RESULT__NAME = eINSTANCE.getWitnessResult_Name();

		/**
		 * The meta object literal for the '<em><b>Log</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WITNESS_RESULT__LOG = eINSTANCE.getWitnessResult_Log();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WITNESS_RESULT__DURATION = eINSTANCE.getWitnessResult_Duration();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.resultstore.impl.ConditionResultImpl <em>Condition Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.resultstore.impl.ConditionResultImpl
		 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getConditionResult()
		 * @generated
		 */
		EClass CONDITION_RESULT = eINSTANCE.getConditionResult();

		/**
		 * The meta object literal for the '<em><b>Goalcondition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION_RESULT__GOALCONDITION = eINSTANCE.getConditionResult_Goalcondition();

		/**
		 * The meta object literal for the '<em><b>Scenarios Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION_RESULT__SCENARIOS_RESULT = eINSTANCE.getConditionResult_ScenariosResult();

		/**
		 * The meta object literal for the '<em><b>Reachability Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION_RESULT__REACHABILITY_RESULT = eINSTANCE.getConditionResult_ReachabilityResult();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.resultstore.ResultValue <em>Result Value</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.resultstore.ResultValue
		 * @see fr.irisa.atsyra.resultstore.impl.ResultstorePackageImpl#getResultValue()
		 * @generated
		 */
		EEnum RESULT_VALUE = eINSTANCE.getResultValue();

	}

} //ResultstorePackage
