/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Witness Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.WitnessResult#getWitnessFound <em>Witness Found</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.WitnessResult#getValue <em>Value</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.WitnessResult#getDetails <em>Details</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.WitnessResult#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.WitnessResult#getCallParameters <em>Call Parameters</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.WitnessResult#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.WitnessResult#getLog <em>Log</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.WitnessResult#getDuration <em>Duration</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getWitnessResult()
 * @model
 * @generated
 */
public interface WitnessResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Witness Found</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * List of witness sequences found
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Witness Found</em>' attribute list.
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getWitnessResult_WitnessFound()
	 * @model
	 * @generated
	 */
	EList<String> getWitnessFound();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.atsyra.resultstore.ResultValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * indicates if the search of witness was successfull, has timed out or has been aborted.
	 * In case of timeout or abort, there  may still have some witness sequences found.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see fr.irisa.atsyra.resultstore.ResultValue
	 * @see #setValue(ResultValue)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getWitnessResult_Value()
	 * @model
	 * @generated
	 */
	ResultValue getValue();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.WitnessResult#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see fr.irisa.atsyra.resultstore.ResultValue
	 * @see #getValue()
	 * @generated
	 */
	void setValue(ResultValue value);

	/**
	 * Returns the value of the '<em><b>Details</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Details</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Details</em>' attribute.
	 * @see #setDetails(String)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getWitnessResult_Details()
	 * @model
	 * @generated
	 */
	String getDetails();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.WitnessResult#getDetails <em>Details</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Details</em>' attribute.
	 * @see #getDetails()
	 * @generated
	 */
	void setDetails(String value);

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(Date)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getWitnessResult_Timestamp()
	 * @model
	 * @generated
	 */
	Date getTimestamp();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.WitnessResult#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(Date value);

	/**
	 * Returns the value of the '<em><b>Call Parameters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Parameters used when searching witness sequence
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Call Parameters</em>' attribute.
	 * @see #setCallParameters(String)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getWitnessResult_CallParameters()
	 * @model
	 * @generated
	 */
	String getCallParameters();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.WitnessResult#getCallParameters <em>Call Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Call Parameters</em>' attribute.
	 * @see #getCallParameters()
	 * @generated
	 */
	void setCallParameters(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Display name for the user in order to identify this result in the UI.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getWitnessResult_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.WitnessResult#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Log</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Stdout of the command that launched the search
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Log</em>' attribute.
	 * @see #setLog(String)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getWitnessResult_Log()
	 * @model
	 * @generated
	 */
	String getLog();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.WitnessResult#getLog <em>Log</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Log</em>' attribute.
	 * @see #getLog()
	 * @generated
	 */
	void setLog(String value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(long)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getWitnessResult_Duration()
	 * @model
	 * @generated
	 */
	long getDuration();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.WitnessResult#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(long value);

} // WitnessResult
