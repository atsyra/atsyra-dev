/*******************************************************************************
 * Copyright (c) 2014, 2019 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.resultstore.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;

import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraTree;
import atsyragoal.util.AtsyragoalHelper;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.resultstore.TreeResult;

public class SyntheticResultHelper
{
	
	private static final String RESULTFOLDER_NAME = "results";
	
	public enum SyntheticLocalRefinementResult {
		LEAF,
		PERFECT_LOCAL_MATCH, 
		UNRELATED_LOCAL_REFINEMENT, 
		WEAK_LOCAL_REFINEMENT,
		UNPRECISE_LOCAL_REFINEMENT,
		RESTRIVE_LOCAL_REFINEMENT,
		INCOHERENT_RESULT,
		INCOMPLETE_RESULT
	};
	
	public enum SyntheticTreeAdmissibilityResult {
		TREE_ADMISSIBLE, 
		TREE_NOT_ADMISSIBLE_MAIN_GOAL, 
		TREE_NOT_ADMISSIBLE_REFINEMENT_GOAL, 
		TREE_ADMISSIBILITY_INCOMPLETE_RESULT
	};
	
	
	
	public static SyntheticLocalRefinementResult getSyntheticLocalRefinementTreeResult(TreeResult tr) {
		SyntheticLocalRefinementResult res;
		if(tr.getTree().getOperands().isEmpty()) {
			return SyntheticLocalRefinementResult.LEAF;
		}
		if(tr.getOvermatchResult() == null || tr.getUndermatchResult() ==  null || tr.getMeetResult().getValue() == null ) {
			return SyntheticLocalRefinementResult.INCOMPLETE_RESULT;
		}
		String intreptretationValue =  
				tr.getOvermatchResult().getValue().toString() + " "+
				tr.getUndermatchResult().getValue().toString() + " " +
				tr.getMeetResult().getValue().toString();
		switch (intreptretationValue) {
		case "TRUE TRUE TRUE":
			res = SyntheticLocalRefinementResult.PERFECT_LOCAL_MATCH;
			break;
		case "TRUE TRUE FALSE":	
			res = SyntheticLocalRefinementResult.INCOHERENT_RESULT;
			break;
		case "FALSE FALSE FALSE":	
			res = SyntheticLocalRefinementResult.UNRELATED_LOCAL_REFINEMENT;
			break;
		case "FALSE FALSE TRUE":	
			res = SyntheticLocalRefinementResult.WEAK_LOCAL_REFINEMENT;
			break;
		case "TRUE FALSE TRUE":	
		case "TRUE FALSE FALSE":	
			res = SyntheticLocalRefinementResult.UNPRECISE_LOCAL_REFINEMENT;
			break;
		case "FALSE TRUE TRUE":	
		case "FALSE TRUE FALSE":	
			res = SyntheticLocalRefinementResult.RESTRIVE_LOCAL_REFINEMENT;
			break;
		default:
			res = SyntheticLocalRefinementResult.INCOMPLETE_RESULT;
			break;
		}
		return res;
	}
	
	public static SyntheticTreeAdmissibilityResult getSyntheticTreeAdmissibilityResult(TreeResult tr) {
		AtsyraTree tree = tr.getTree();
		ResultStore rs = (ResultStore) tr.eContainer();
		if(rs == null) {
			// this is a fake treeresult from CorrectnessResultView
			rs = getResultStoreModel(tree);
		}
		Optional<GoalResult> mainGoalResult = rs.getGoalResults().stream().filter(gr -> tree.getMainGoal() == gr.getGoal()).findFirst();
		if(mainGoalResult.isPresent()) {
			if(mainGoalResult.get().getReachabilityResult().getValue() == ResultValue.FALSE ) {
				return SyntheticTreeAdmissibilityResult.TREE_NOT_ADMISSIBLE_MAIN_GOAL;
			} else	if(mainGoalResult.get().getReachabilityResult().getValue() != ResultValue.TRUE ) {				
				return SyntheticTreeAdmissibilityResult.TREE_ADMISSIBILITY_INCOMPLETE_RESULT;
			}
			// ok main goal is reachable, continue the check
		} else {
			return SyntheticTreeAdmissibilityResult.TREE_ADMISSIBILITY_INCOMPLETE_RESULT;
		}
		
		List<AtsyraGoal> goals = AtsyragoalHelper.getOperandsGoals(tree);
		for (AtsyraGoal atsyraGoal : goals) {
			Optional<GoalResult> goalResult = rs.getGoalResults().stream().filter(gr -> atsyraGoal == gr.getGoal()).findFirst();
			if(goalResult.isPresent()) {
				if(goalResult.get().getReachabilityResult().getValue() == ResultValue.FALSE ) {
					return SyntheticTreeAdmissibilityResult.TREE_NOT_ADMISSIBLE_REFINEMENT_GOAL;
				} else	if(goalResult.get().getReachabilityResult().getValue() != ResultValue.TRUE ) {				
					return SyntheticTreeAdmissibilityResult.TREE_ADMISSIBILITY_INCOMPLETE_RESULT;
				}
			} else {
				return SyntheticTreeAdmissibilityResult.TREE_ADMISSIBILITY_INCOMPLETE_RESULT;
			}
		}
		// ok all involved goals (mains and operands) are admissible
		return SyntheticTreeAdmissibilityResult.TREE_ADMISSIBLE;
	}
	
	public static List<AtsyraGoal> getGoalsWithMissingReachability(TreeResult tr) {
		List<AtsyraGoal> result = new ArrayList<AtsyraGoal>();
		AtsyraTree tree = tr.getTree();
		ResultStore rs = (ResultStore) tr.eContainer();
		if(rs == null) {
			// this is a fake treeresult from CorrectnessResultView
			rs = getResultStoreModel(tree);
		}
		Optional<GoalResult> mainGoalResult = rs.getGoalResults().stream().filter(gr -> tree.getMainGoal() == gr.getGoal()).findFirst();
		if(mainGoalResult.isPresent()) {
			if(mainGoalResult.get().getReachabilityResult().getValue() != ResultValue.FALSE && mainGoalResult.get().getReachabilityResult().getValue() != ResultValue.TRUE ) {
				result.add(mainGoalResult.get().getGoal());
			} 
		} else {
			result.add(tree.getMainGoal());
		}
		
		List<AtsyraGoal> goals = AtsyragoalHelper.getOperandsGoals(tree);
		for (AtsyraGoal atsyraGoal : goals) {
			Optional<GoalResult> goalResult = rs.getGoalResults().stream().filter(gr -> atsyraGoal == gr.getGoal()).findFirst();
			if(goalResult.isPresent()) {
				if(goalResult.get().getReachabilityResult().getValue() != ResultValue.FALSE && goalResult.get().getReachabilityResult().getValue() != ResultValue.TRUE) {
					result.add(goalResult.get().getGoal());
				}
			} else {
				result.add(atsyraGoal);
			}
		}
		return result;
	}
	public static List<AtsyraGoal> getNotReachableGoals(TreeResult tr) {
		List<AtsyraGoal> result = new ArrayList<AtsyraGoal>();
		AtsyraTree tree = tr.getTree();
		ResultStore rs = (ResultStore) tr.eContainer();
		if(rs == null) {
			// this is a fake treeresult from CorrectnessResultView
			rs = getResultStoreModel(tree);
		}
		Optional<GoalResult> mainGoalResult = rs.getGoalResults().stream().filter(gr -> tree.getMainGoal() == gr.getGoal()).findFirst();
		if(mainGoalResult.isPresent()) {
			if(mainGoalResult.get().getReachabilityResult().getValue() == ResultValue.FALSE ) {
				result.add(mainGoalResult.get().getGoal());
			} 
		}
		
		List<AtsyraGoal> goals = AtsyragoalHelper.getOperandsGoals(tree);
		for (AtsyraGoal atsyraGoal : goals) {
			Optional<GoalResult> goalResult = rs.getGoalResults().stream().filter(gr -> atsyraGoal == gr.getGoal()).findFirst();
			if(goalResult.isPresent()) {
				if(goalResult.get().getReachabilityResult().getValue() == ResultValue.FALSE ) {
					result.add(goalResult.get().getGoal());
				}
			}
		}
		return result;
	}
	
	public static ResultStore getResultStoreModel(EObject eobj) {
		IFile file = EMFResource.getIFile(eobj);
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(RESULTFOLDER_NAME);
		IFile resultStoreFile = file.getProject().getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension +".resultstore");
		
		URI resultStoreUri = URI.createPlatformResourceURI(resultStoreFile.getFullPath().toString(), true);
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("resultstore", new XMIResourceFactoryImpl());
		ResourceSet resSet;
		Resource res;
		// use the same resourceSet in order to ease equality comparison
		if (eobj.eResource() != null && eobj.eResource().getResourceSet() != null) {
			resSet = eobj.eResource().getResourceSet();
			res = resSet.getResource(resultStoreUri, true);
			// may need a reload if the resource of the resultstore has changed
			res.unload();
			res = resSet.getResource(resultStoreUri, true);
			
		} else {
			resSet = new ResourceSetImpl();
			res = resSet.getResource(resultStoreUri, true);
		}
		
		return (ResultStore) res.getContents().get(0);
	}
	
}
