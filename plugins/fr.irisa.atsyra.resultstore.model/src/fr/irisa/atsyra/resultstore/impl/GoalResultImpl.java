/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore.impl;

import atsyragoal.AtsyraGoal;

import fr.irisa.atsyra.resultstore.ConditionResult;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultstorePackage;

import fr.irisa.atsyra.resultstore.WitnessResult;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goal Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.GoalResultImpl#getReachabilityResult <em>Reachability Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.GoalResultImpl#getGoal <em>Goal</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.GoalResultImpl#getShortestWitnessResult <em>Shortest Witness Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.GoalResultImpl#getComplementaryWitnessResults <em>Complementary Witness Results</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.GoalResultImpl#getPrecondition <em>Precondition</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.GoalResultImpl#getPostcondition <em>Postcondition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GoalResultImpl extends MinimalEObjectImpl.Container implements GoalResult {
	/**
	 * The cached value of the '{@link #getReachabilityResult() <em>Reachability Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReachabilityResult()
	 * @generated
	 * @ordered
	 */
	protected Result reachabilityResult;

	/**
	 * The cached value of the '{@link #getGoal() <em>Goal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGoal()
	 * @generated
	 * @ordered
	 */
	protected AtsyraGoal goal;

	/**
	 * The cached value of the '{@link #getShortestWitnessResult() <em>Shortest Witness Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortestWitnessResult()
	 * @generated
	 * @ordered
	 */
	protected WitnessResult shortestWitnessResult;

	/**
	 * The cached value of the '{@link #getComplementaryWitnessResults() <em>Complementary Witness Results</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComplementaryWitnessResults()
	 * @generated
	 * @ordered
	 */
	protected EList<WitnessResult> complementaryWitnessResults;

	/**
	 * The cached value of the '{@link #getPrecondition() <em>Precondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrecondition()
	 * @generated
	 * @ordered
	 */
	protected ConditionResult precondition;

	/**
	 * The cached value of the '{@link #getPostcondition() <em>Postcondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostcondition()
	 * @generated
	 * @ordered
	 */
	protected ConditionResult postcondition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultstorePackage.Literals.GOAL_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Result getReachabilityResult() {
		return reachabilityResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReachabilityResult(Result newReachabilityResult, NotificationChain msgs) {
		Result oldReachabilityResult = reachabilityResult;
		reachabilityResult = newReachabilityResult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT, oldReachabilityResult, newReachabilityResult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReachabilityResult(Result newReachabilityResult) {
		if (newReachabilityResult != reachabilityResult) {
			NotificationChain msgs = null;
			if (reachabilityResult != null)
				msgs = ((InternalEObject)reachabilityResult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT, null, msgs);
			if (newReachabilityResult != null)
				msgs = ((InternalEObject)newReachabilityResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT, null, msgs);
			msgs = basicSetReachabilityResult(newReachabilityResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT, newReachabilityResult, newReachabilityResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraGoal getGoal() {
		if (goal != null && goal.eIsProxy()) {
			InternalEObject oldGoal = (InternalEObject)goal;
			goal = (AtsyraGoal)eResolveProxy(oldGoal);
			if (goal != oldGoal) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ResultstorePackage.GOAL_RESULT__GOAL, oldGoal, goal));
			}
		}
		return goal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraGoal basicGetGoal() {
		return goal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGoal(AtsyraGoal newGoal) {
		AtsyraGoal oldGoal = goal;
		goal = newGoal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.GOAL_RESULT__GOAL, oldGoal, goal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WitnessResult getShortestWitnessResult() {
		return shortestWitnessResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShortestWitnessResult(WitnessResult newShortestWitnessResult, NotificationChain msgs) {
		WitnessResult oldShortestWitnessResult = shortestWitnessResult;
		shortestWitnessResult = newShortestWitnessResult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT, oldShortestWitnessResult, newShortestWitnessResult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortestWitnessResult(WitnessResult newShortestWitnessResult) {
		if (newShortestWitnessResult != shortestWitnessResult) {
			NotificationChain msgs = null;
			if (shortestWitnessResult != null)
				msgs = ((InternalEObject)shortestWitnessResult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT, null, msgs);
			if (newShortestWitnessResult != null)
				msgs = ((InternalEObject)newShortestWitnessResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT, null, msgs);
			msgs = basicSetShortestWitnessResult(newShortestWitnessResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT, newShortestWitnessResult, newShortestWitnessResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WitnessResult> getComplementaryWitnessResults() {
		if (complementaryWitnessResults == null) {
			complementaryWitnessResults = new EObjectContainmentEList<WitnessResult>(WitnessResult.class, this, ResultstorePackage.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS);
		}
		return complementaryWitnessResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionResult getPrecondition() {
		return precondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrecondition(ConditionResult newPrecondition, NotificationChain msgs) {
		ConditionResult oldPrecondition = precondition;
		precondition = newPrecondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.GOAL_RESULT__PRECONDITION, oldPrecondition, newPrecondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrecondition(ConditionResult newPrecondition) {
		if (newPrecondition != precondition) {
			NotificationChain msgs = null;
			if (precondition != null)
				msgs = ((InternalEObject)precondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.GOAL_RESULT__PRECONDITION, null, msgs);
			if (newPrecondition != null)
				msgs = ((InternalEObject)newPrecondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.GOAL_RESULT__PRECONDITION, null, msgs);
			msgs = basicSetPrecondition(newPrecondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.GOAL_RESULT__PRECONDITION, newPrecondition, newPrecondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionResult getPostcondition() {
		return postcondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPostcondition(ConditionResult newPostcondition, NotificationChain msgs) {
		ConditionResult oldPostcondition = postcondition;
		postcondition = newPostcondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.GOAL_RESULT__POSTCONDITION, oldPostcondition, newPostcondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostcondition(ConditionResult newPostcondition) {
		if (newPostcondition != postcondition) {
			NotificationChain msgs = null;
			if (postcondition != null)
				msgs = ((InternalEObject)postcondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.GOAL_RESULT__POSTCONDITION, null, msgs);
			if (newPostcondition != null)
				msgs = ((InternalEObject)newPostcondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.GOAL_RESULT__POSTCONDITION, null, msgs);
			msgs = basicSetPostcondition(newPostcondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.GOAL_RESULT__POSTCONDITION, newPostcondition, newPostcondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT:
				return basicSetReachabilityResult(null, msgs);
			case ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT:
				return basicSetShortestWitnessResult(null, msgs);
			case ResultstorePackage.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS:
				return ((InternalEList<?>)getComplementaryWitnessResults()).basicRemove(otherEnd, msgs);
			case ResultstorePackage.GOAL_RESULT__PRECONDITION:
				return basicSetPrecondition(null, msgs);
			case ResultstorePackage.GOAL_RESULT__POSTCONDITION:
				return basicSetPostcondition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT:
				return getReachabilityResult();
			case ResultstorePackage.GOAL_RESULT__GOAL:
				if (resolve) return getGoal();
				return basicGetGoal();
			case ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT:
				return getShortestWitnessResult();
			case ResultstorePackage.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS:
				return getComplementaryWitnessResults();
			case ResultstorePackage.GOAL_RESULT__PRECONDITION:
				return getPrecondition();
			case ResultstorePackage.GOAL_RESULT__POSTCONDITION:
				return getPostcondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT:
				setReachabilityResult((Result)newValue);
				return;
			case ResultstorePackage.GOAL_RESULT__GOAL:
				setGoal((AtsyraGoal)newValue);
				return;
			case ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT:
				setShortestWitnessResult((WitnessResult)newValue);
				return;
			case ResultstorePackage.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS:
				getComplementaryWitnessResults().clear();
				getComplementaryWitnessResults().addAll((Collection<? extends WitnessResult>)newValue);
				return;
			case ResultstorePackage.GOAL_RESULT__PRECONDITION:
				setPrecondition((ConditionResult)newValue);
				return;
			case ResultstorePackage.GOAL_RESULT__POSTCONDITION:
				setPostcondition((ConditionResult)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT:
				setReachabilityResult((Result)null);
				return;
			case ResultstorePackage.GOAL_RESULT__GOAL:
				setGoal((AtsyraGoal)null);
				return;
			case ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT:
				setShortestWitnessResult((WitnessResult)null);
				return;
			case ResultstorePackage.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS:
				getComplementaryWitnessResults().clear();
				return;
			case ResultstorePackage.GOAL_RESULT__PRECONDITION:
				setPrecondition((ConditionResult)null);
				return;
			case ResultstorePackage.GOAL_RESULT__POSTCONDITION:
				setPostcondition((ConditionResult)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultstorePackage.GOAL_RESULT__REACHABILITY_RESULT:
				return reachabilityResult != null;
			case ResultstorePackage.GOAL_RESULT__GOAL:
				return goal != null;
			case ResultstorePackage.GOAL_RESULT__SHORTEST_WITNESS_RESULT:
				return shortestWitnessResult != null;
			case ResultstorePackage.GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS:
				return complementaryWitnessResults != null && !complementaryWitnessResults.isEmpty();
			case ResultstorePackage.GOAL_RESULT__PRECONDITION:
				return precondition != null;
			case ResultstorePackage.GOAL_RESULT__POSTCONDITION:
				return postcondition != null;
		}
		return super.eIsSet(featureID);
	}

} //GoalResultImpl
