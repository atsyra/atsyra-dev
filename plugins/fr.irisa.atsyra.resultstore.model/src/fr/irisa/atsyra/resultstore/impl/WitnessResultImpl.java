/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore.impl;

import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.resultstore.ResultstorePackage;
import fr.irisa.atsyra.resultstore.WitnessResult;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Witness Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl#getWitnessFound <em>Witness Found</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl#getValue <em>Value</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl#getDetails <em>Details</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl#getCallParameters <em>Call Parameters</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl#getLog <em>Log</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.WitnessResultImpl#getDuration <em>Duration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WitnessResultImpl extends MinimalEObjectImpl.Container implements WitnessResult {
	/**
	 * The cached value of the '{@link #getWitnessFound() <em>Witness Found</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWitnessFound()
	 * @generated
	 * @ordered
	 */
	protected EList<String> witnessFound;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final ResultValue VALUE_EDEFAULT = ResultValue.FALSE;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected ResultValue value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDetails() <em>Details</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetails()
	 * @generated
	 * @ordered
	 */
	protected static final String DETAILS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDetails() <em>Details</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetails()
	 * @generated
	 * @ordered
	 */
	protected String details = DETAILS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected Date timestamp = TIMESTAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #getCallParameters() <em>Call Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallParameters()
	 * @generated
	 * @ordered
	 */
	protected static final String CALL_PARAMETERS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCallParameters() <em>Call Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallParameters()
	 * @generated
	 * @ordered
	 */
	protected String callParameters = CALL_PARAMETERS_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLog() <em>Log</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLog()
	 * @generated
	 * @ordered
	 */
	protected static final String LOG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLog() <em>Log</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLog()
	 * @generated
	 * @ordered
	 */
	protected String log = LOG_EDEFAULT;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final long DURATION_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected long duration = DURATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WitnessResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultstorePackage.Literals.WITNESS_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getWitnessFound() {
		if (witnessFound == null) {
			witnessFound = new EDataTypeUniqueEList<String>(String.class, this, ResultstorePackage.WITNESS_RESULT__WITNESS_FOUND);
		}
		return witnessFound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultValue getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(ResultValue newValue) {
		ResultValue oldValue = value;
		value = newValue == null ? VALUE_EDEFAULT : newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.WITNESS_RESULT__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDetails(String newDetails) {
		String oldDetails = details;
		details = newDetails;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.WITNESS_RESULT__DETAILS, oldDetails, details));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimestamp(Date newTimestamp) {
		Date oldTimestamp = timestamp;
		timestamp = newTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.WITNESS_RESULT__TIMESTAMP, oldTimestamp, timestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCallParameters() {
		return callParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallParameters(String newCallParameters) {
		String oldCallParameters = callParameters;
		callParameters = newCallParameters;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.WITNESS_RESULT__CALL_PARAMETERS, oldCallParameters, callParameters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.WITNESS_RESULT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLog() {
		return log;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLog(String newLog) {
		String oldLog = log;
		log = newLog;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.WITNESS_RESULT__LOG, oldLog, log));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(long newDuration) {
		long oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.WITNESS_RESULT__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultstorePackage.WITNESS_RESULT__WITNESS_FOUND:
				return getWitnessFound();
			case ResultstorePackage.WITNESS_RESULT__VALUE:
				return getValue();
			case ResultstorePackage.WITNESS_RESULT__DETAILS:
				return getDetails();
			case ResultstorePackage.WITNESS_RESULT__TIMESTAMP:
				return getTimestamp();
			case ResultstorePackage.WITNESS_RESULT__CALL_PARAMETERS:
				return getCallParameters();
			case ResultstorePackage.WITNESS_RESULT__NAME:
				return getName();
			case ResultstorePackage.WITNESS_RESULT__LOG:
				return getLog();
			case ResultstorePackage.WITNESS_RESULT__DURATION:
				return getDuration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultstorePackage.WITNESS_RESULT__WITNESS_FOUND:
				getWitnessFound().clear();
				getWitnessFound().addAll((Collection<? extends String>)newValue);
				return;
			case ResultstorePackage.WITNESS_RESULT__VALUE:
				setValue((ResultValue)newValue);
				return;
			case ResultstorePackage.WITNESS_RESULT__DETAILS:
				setDetails((String)newValue);
				return;
			case ResultstorePackage.WITNESS_RESULT__TIMESTAMP:
				setTimestamp((Date)newValue);
				return;
			case ResultstorePackage.WITNESS_RESULT__CALL_PARAMETERS:
				setCallParameters((String)newValue);
				return;
			case ResultstorePackage.WITNESS_RESULT__NAME:
				setName((String)newValue);
				return;
			case ResultstorePackage.WITNESS_RESULT__LOG:
				setLog((String)newValue);
				return;
			case ResultstorePackage.WITNESS_RESULT__DURATION:
				setDuration((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultstorePackage.WITNESS_RESULT__WITNESS_FOUND:
				getWitnessFound().clear();
				return;
			case ResultstorePackage.WITNESS_RESULT__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case ResultstorePackage.WITNESS_RESULT__DETAILS:
				setDetails(DETAILS_EDEFAULT);
				return;
			case ResultstorePackage.WITNESS_RESULT__TIMESTAMP:
				setTimestamp(TIMESTAMP_EDEFAULT);
				return;
			case ResultstorePackage.WITNESS_RESULT__CALL_PARAMETERS:
				setCallParameters(CALL_PARAMETERS_EDEFAULT);
				return;
			case ResultstorePackage.WITNESS_RESULT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ResultstorePackage.WITNESS_RESULT__LOG:
				setLog(LOG_EDEFAULT);
				return;
			case ResultstorePackage.WITNESS_RESULT__DURATION:
				setDuration(DURATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultstorePackage.WITNESS_RESULT__WITNESS_FOUND:
				return witnessFound != null && !witnessFound.isEmpty();
			case ResultstorePackage.WITNESS_RESULT__VALUE:
				return value != VALUE_EDEFAULT;
			case ResultstorePackage.WITNESS_RESULT__DETAILS:
				return DETAILS_EDEFAULT == null ? details != null : !DETAILS_EDEFAULT.equals(details);
			case ResultstorePackage.WITNESS_RESULT__TIMESTAMP:
				return TIMESTAMP_EDEFAULT == null ? timestamp != null : !TIMESTAMP_EDEFAULT.equals(timestamp);
			case ResultstorePackage.WITNESS_RESULT__CALL_PARAMETERS:
				return CALL_PARAMETERS_EDEFAULT == null ? callParameters != null : !CALL_PARAMETERS_EDEFAULT.equals(callParameters);
			case ResultstorePackage.WITNESS_RESULT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ResultstorePackage.WITNESS_RESULT__LOG:
				return LOG_EDEFAULT == null ? log != null : !LOG_EDEFAULT.equals(log);
			case ResultstorePackage.WITNESS_RESULT__DURATION:
				return duration != DURATION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (witnessFound: ");
		result.append(witnessFound);
		result.append(", value: ");
		result.append(value);
		result.append(", details: ");
		result.append(details);
		result.append(", timestamp: ");
		result.append(timestamp);
		result.append(", callParameters: ");
		result.append(callParameters);
		result.append(", name: ");
		result.append(name);
		result.append(", log: ");
		result.append(log);
		result.append(", duration: ");
		result.append(duration);
		result.append(')');
		return result.toString();
	}

} //WitnessResultImpl
