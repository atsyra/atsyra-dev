/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore.impl;

import atsyragoal.AtsyraTree;

import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultstorePackage;
import fr.irisa.atsyra.resultstore.TreeResult;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tree Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.TreeResultImpl#getMeetResult <em>Meet Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.TreeResultImpl#getAdmissibilityResult <em>Admissibility Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.TreeResultImpl#getUndermatchResult <em>Undermatch Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.TreeResultImpl#getOvermatchResult <em>Overmatch Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.TreeResultImpl#getMatchResult <em>Match Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.TreeResultImpl#getTree <em>Tree</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TreeResultImpl extends MinimalEObjectImpl.Container implements TreeResult {
	/**
	 * The cached value of the '{@link #getMeetResult() <em>Meet Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeetResult()
	 * @generated
	 * @ordered
	 */
	protected Result meetResult;

	/**
	 * The cached value of the '{@link #getAdmissibilityResult() <em>Admissibility Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdmissibilityResult()
	 * @generated
	 * @ordered
	 */
	protected Result admissibilityResult;

	/**
	 * The cached value of the '{@link #getUndermatchResult() <em>Undermatch Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUndermatchResult()
	 * @generated
	 * @ordered
	 */
	protected Result undermatchResult;

	/**
	 * The cached value of the '{@link #getOvermatchResult() <em>Overmatch Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOvermatchResult()
	 * @generated
	 * @ordered
	 */
	protected Result overmatchResult;

	/**
	 * The cached value of the '{@link #getMatchResult() <em>Match Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatchResult()
	 * @generated
	 * @ordered
	 */
	protected Result matchResult;

	/**
	 * The cached value of the '{@link #getTree() <em>Tree</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTree()
	 * @generated
	 * @ordered
	 */
	protected AtsyraTree tree;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TreeResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultstorePackage.Literals.TREE_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Result getMeetResult() {
		return meetResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMeetResult(Result newMeetResult, NotificationChain msgs) {
		Result oldMeetResult = meetResult;
		meetResult = newMeetResult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__MEET_RESULT, oldMeetResult, newMeetResult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeetResult(Result newMeetResult) {
		if (newMeetResult != meetResult) {
			NotificationChain msgs = null;
			if (meetResult != null)
				msgs = ((InternalEObject)meetResult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__MEET_RESULT, null, msgs);
			if (newMeetResult != null)
				msgs = ((InternalEObject)newMeetResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__MEET_RESULT, null, msgs);
			msgs = basicSetMeetResult(newMeetResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__MEET_RESULT, newMeetResult, newMeetResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Result getAdmissibilityResult() {
		return admissibilityResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdmissibilityResult(Result newAdmissibilityResult, NotificationChain msgs) {
		Result oldAdmissibilityResult = admissibilityResult;
		admissibilityResult = newAdmissibilityResult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__ADMISSIBILITY_RESULT, oldAdmissibilityResult, newAdmissibilityResult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdmissibilityResult(Result newAdmissibilityResult) {
		if (newAdmissibilityResult != admissibilityResult) {
			NotificationChain msgs = null;
			if (admissibilityResult != null)
				msgs = ((InternalEObject)admissibilityResult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__ADMISSIBILITY_RESULT, null, msgs);
			if (newAdmissibilityResult != null)
				msgs = ((InternalEObject)newAdmissibilityResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__ADMISSIBILITY_RESULT, null, msgs);
			msgs = basicSetAdmissibilityResult(newAdmissibilityResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__ADMISSIBILITY_RESULT, newAdmissibilityResult, newAdmissibilityResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Result getUndermatchResult() {
		return undermatchResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUndermatchResult(Result newUndermatchResult, NotificationChain msgs) {
		Result oldUndermatchResult = undermatchResult;
		undermatchResult = newUndermatchResult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__UNDERMATCH_RESULT, oldUndermatchResult, newUndermatchResult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUndermatchResult(Result newUndermatchResult) {
		if (newUndermatchResult != undermatchResult) {
			NotificationChain msgs = null;
			if (undermatchResult != null)
				msgs = ((InternalEObject)undermatchResult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__UNDERMATCH_RESULT, null, msgs);
			if (newUndermatchResult != null)
				msgs = ((InternalEObject)newUndermatchResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__UNDERMATCH_RESULT, null, msgs);
			msgs = basicSetUndermatchResult(newUndermatchResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__UNDERMATCH_RESULT, newUndermatchResult, newUndermatchResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Result getOvermatchResult() {
		return overmatchResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOvermatchResult(Result newOvermatchResult, NotificationChain msgs) {
		Result oldOvermatchResult = overmatchResult;
		overmatchResult = newOvermatchResult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__OVERMATCH_RESULT, oldOvermatchResult, newOvermatchResult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOvermatchResult(Result newOvermatchResult) {
		if (newOvermatchResult != overmatchResult) {
			NotificationChain msgs = null;
			if (overmatchResult != null)
				msgs = ((InternalEObject)overmatchResult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__OVERMATCH_RESULT, null, msgs);
			if (newOvermatchResult != null)
				msgs = ((InternalEObject)newOvermatchResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__OVERMATCH_RESULT, null, msgs);
			msgs = basicSetOvermatchResult(newOvermatchResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__OVERMATCH_RESULT, newOvermatchResult, newOvermatchResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Result getMatchResult() {
		return matchResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMatchResult(Result newMatchResult, NotificationChain msgs) {
		Result oldMatchResult = matchResult;
		matchResult = newMatchResult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__MATCH_RESULT, oldMatchResult, newMatchResult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMatchResult(Result newMatchResult) {
		if (newMatchResult != matchResult) {
			NotificationChain msgs = null;
			if (matchResult != null)
				msgs = ((InternalEObject)matchResult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__MATCH_RESULT, null, msgs);
			if (newMatchResult != null)
				msgs = ((InternalEObject)newMatchResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.TREE_RESULT__MATCH_RESULT, null, msgs);
			msgs = basicSetMatchResult(newMatchResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__MATCH_RESULT, newMatchResult, newMatchResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraTree getTree() {
		if (tree != null && tree.eIsProxy()) {
			InternalEObject oldTree = (InternalEObject)tree;
			tree = (AtsyraTree)eResolveProxy(oldTree);
			if (tree != oldTree) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ResultstorePackage.TREE_RESULT__TREE, oldTree, tree));
			}
		}
		return tree;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraTree basicGetTree() {
		return tree;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTree(AtsyraTree newTree) {
		AtsyraTree oldTree = tree;
		tree = newTree;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.TREE_RESULT__TREE, oldTree, tree));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResultstorePackage.TREE_RESULT__MEET_RESULT:
				return basicSetMeetResult(null, msgs);
			case ResultstorePackage.TREE_RESULT__ADMISSIBILITY_RESULT:
				return basicSetAdmissibilityResult(null, msgs);
			case ResultstorePackage.TREE_RESULT__UNDERMATCH_RESULT:
				return basicSetUndermatchResult(null, msgs);
			case ResultstorePackage.TREE_RESULT__OVERMATCH_RESULT:
				return basicSetOvermatchResult(null, msgs);
			case ResultstorePackage.TREE_RESULT__MATCH_RESULT:
				return basicSetMatchResult(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultstorePackage.TREE_RESULT__MEET_RESULT:
				return getMeetResult();
			case ResultstorePackage.TREE_RESULT__ADMISSIBILITY_RESULT:
				return getAdmissibilityResult();
			case ResultstorePackage.TREE_RESULT__UNDERMATCH_RESULT:
				return getUndermatchResult();
			case ResultstorePackage.TREE_RESULT__OVERMATCH_RESULT:
				return getOvermatchResult();
			case ResultstorePackage.TREE_RESULT__MATCH_RESULT:
				return getMatchResult();
			case ResultstorePackage.TREE_RESULT__TREE:
				if (resolve) return getTree();
				return basicGetTree();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultstorePackage.TREE_RESULT__MEET_RESULT:
				setMeetResult((Result)newValue);
				return;
			case ResultstorePackage.TREE_RESULT__ADMISSIBILITY_RESULT:
				setAdmissibilityResult((Result)newValue);
				return;
			case ResultstorePackage.TREE_RESULT__UNDERMATCH_RESULT:
				setUndermatchResult((Result)newValue);
				return;
			case ResultstorePackage.TREE_RESULT__OVERMATCH_RESULT:
				setOvermatchResult((Result)newValue);
				return;
			case ResultstorePackage.TREE_RESULT__MATCH_RESULT:
				setMatchResult((Result)newValue);
				return;
			case ResultstorePackage.TREE_RESULT__TREE:
				setTree((AtsyraTree)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultstorePackage.TREE_RESULT__MEET_RESULT:
				setMeetResult((Result)null);
				return;
			case ResultstorePackage.TREE_RESULT__ADMISSIBILITY_RESULT:
				setAdmissibilityResult((Result)null);
				return;
			case ResultstorePackage.TREE_RESULT__UNDERMATCH_RESULT:
				setUndermatchResult((Result)null);
				return;
			case ResultstorePackage.TREE_RESULT__OVERMATCH_RESULT:
				setOvermatchResult((Result)null);
				return;
			case ResultstorePackage.TREE_RESULT__MATCH_RESULT:
				setMatchResult((Result)null);
				return;
			case ResultstorePackage.TREE_RESULT__TREE:
				setTree((AtsyraTree)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultstorePackage.TREE_RESULT__MEET_RESULT:
				return meetResult != null;
			case ResultstorePackage.TREE_RESULT__ADMISSIBILITY_RESULT:
				return admissibilityResult != null;
			case ResultstorePackage.TREE_RESULT__UNDERMATCH_RESULT:
				return undermatchResult != null;
			case ResultstorePackage.TREE_RESULT__OVERMATCH_RESULT:
				return overmatchResult != null;
			case ResultstorePackage.TREE_RESULT__MATCH_RESULT:
				return matchResult != null;
			case ResultstorePackage.TREE_RESULT__TREE:
				return tree != null;
		}
		return super.eIsSet(featureID);
	}

} //TreeResultImpl
