/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore.impl;

import atsyragoal.AtsyragoalPackage;

import fr.irisa.atsyra.resultstore.ConditionResult;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.ResultstorePackage;
import fr.irisa.atsyra.resultstore.TreeResult;

import fr.irisa.atsyra.resultstore.WitnessResult;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ResultstorePackageImpl extends EPackageImpl implements ResultstorePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resultStoreEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass treeResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass witnessResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum resultValueEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ResultstorePackageImpl() {
		super(eNS_URI, ResultstoreFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ResultstorePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ResultstorePackage init() {
		if (isInited) return (ResultstorePackage)EPackage.Registry.INSTANCE.getEPackage(ResultstorePackage.eNS_URI);

		// Obtain or create and register package
		ResultstorePackageImpl theResultstorePackage = (ResultstorePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ResultstorePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ResultstorePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AtsyragoalPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theResultstorePackage.createPackageContents();

		// Initialize created meta-data
		theResultstorePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theResultstorePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ResultstorePackage.eNS_URI, theResultstorePackage);
		return theResultstorePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResultStore() {
		return resultStoreEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResultStore_GoalResults() {
		return (EReference)resultStoreEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResultStore_TreeResults() {
		return (EReference)resultStoreEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoalResult() {
		return goalResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoalResult_ReachabilityResult() {
		return (EReference)goalResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoalResult_Goal() {
		return (EReference)goalResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoalResult_ShortestWitnessResult() {
		return (EReference)goalResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoalResult_ComplementaryWitnessResults() {
		return (EReference)goalResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoalResult_Precondition() {
		return (EReference)goalResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoalResult_Postcondition() {
		return (EReference)goalResultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTreeResult() {
		return treeResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTreeResult_MeetResult() {
		return (EReference)treeResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTreeResult_AdmissibilityResult() {
		return (EReference)treeResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTreeResult_UndermatchResult() {
		return (EReference)treeResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTreeResult_OvermatchResult() {
		return (EReference)treeResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTreeResult_MatchResult() {
		return (EReference)treeResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTreeResult_Tree() {
		return (EReference)treeResultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResult() {
		return resultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResult_Value() {
		return (EAttribute)resultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResult_Details() {
		return (EAttribute)resultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResult_Timestamp() {
		return (EAttribute)resultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResult_Duration() {
		return (EAttribute)resultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResult_Name() {
		return (EAttribute)resultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResult_Log() {
		return (EAttribute)resultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWitnessResult() {
		return witnessResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWitnessResult_WitnessFound() {
		return (EAttribute)witnessResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWitnessResult_Value() {
		return (EAttribute)witnessResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWitnessResult_Details() {
		return (EAttribute)witnessResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWitnessResult_Timestamp() {
		return (EAttribute)witnessResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWitnessResult_CallParameters() {
		return (EAttribute)witnessResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWitnessResult_Name() {
		return (EAttribute)witnessResultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWitnessResult_Log() {
		return (EAttribute)witnessResultEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWitnessResult_Duration() {
		return (EAttribute)witnessResultEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConditionResult() {
		return conditionResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionResult_Goalcondition() {
		return (EReference)conditionResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionResult_ScenariosResult() {
		return (EReference)conditionResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionResult_ReachabilityResult() {
		return (EReference)conditionResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getResultValue() {
		return resultValueEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultstoreFactory getResultstoreFactory() {
		return (ResultstoreFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		resultStoreEClass = createEClass(RESULT_STORE);
		createEReference(resultStoreEClass, RESULT_STORE__GOAL_RESULTS);
		createEReference(resultStoreEClass, RESULT_STORE__TREE_RESULTS);

		goalResultEClass = createEClass(GOAL_RESULT);
		createEReference(goalResultEClass, GOAL_RESULT__REACHABILITY_RESULT);
		createEReference(goalResultEClass, GOAL_RESULT__GOAL);
		createEReference(goalResultEClass, GOAL_RESULT__SHORTEST_WITNESS_RESULT);
		createEReference(goalResultEClass, GOAL_RESULT__COMPLEMENTARY_WITNESS_RESULTS);
		createEReference(goalResultEClass, GOAL_RESULT__PRECONDITION);
		createEReference(goalResultEClass, GOAL_RESULT__POSTCONDITION);

		treeResultEClass = createEClass(TREE_RESULT);
		createEReference(treeResultEClass, TREE_RESULT__MEET_RESULT);
		createEReference(treeResultEClass, TREE_RESULT__ADMISSIBILITY_RESULT);
		createEReference(treeResultEClass, TREE_RESULT__UNDERMATCH_RESULT);
		createEReference(treeResultEClass, TREE_RESULT__OVERMATCH_RESULT);
		createEReference(treeResultEClass, TREE_RESULT__MATCH_RESULT);
		createEReference(treeResultEClass, TREE_RESULT__TREE);

		resultEClass = createEClass(RESULT);
		createEAttribute(resultEClass, RESULT__VALUE);
		createEAttribute(resultEClass, RESULT__DETAILS);
		createEAttribute(resultEClass, RESULT__TIMESTAMP);
		createEAttribute(resultEClass, RESULT__DURATION);
		createEAttribute(resultEClass, RESULT__NAME);
		createEAttribute(resultEClass, RESULT__LOG);

		witnessResultEClass = createEClass(WITNESS_RESULT);
		createEAttribute(witnessResultEClass, WITNESS_RESULT__WITNESS_FOUND);
		createEAttribute(witnessResultEClass, WITNESS_RESULT__VALUE);
		createEAttribute(witnessResultEClass, WITNESS_RESULT__DETAILS);
		createEAttribute(witnessResultEClass, WITNESS_RESULT__TIMESTAMP);
		createEAttribute(witnessResultEClass, WITNESS_RESULT__CALL_PARAMETERS);
		createEAttribute(witnessResultEClass, WITNESS_RESULT__NAME);
		createEAttribute(witnessResultEClass, WITNESS_RESULT__LOG);
		createEAttribute(witnessResultEClass, WITNESS_RESULT__DURATION);

		conditionResultEClass = createEClass(CONDITION_RESULT);
		createEReference(conditionResultEClass, CONDITION_RESULT__GOALCONDITION);
		createEReference(conditionResultEClass, CONDITION_RESULT__SCENARIOS_RESULT);
		createEReference(conditionResultEClass, CONDITION_RESULT__REACHABILITY_RESULT);

		// Create enums
		resultValueEEnum = createEEnum(RESULT_VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AtsyragoalPackage theAtsyragoalPackage = (AtsyragoalPackage)EPackage.Registry.INSTANCE.getEPackage(AtsyragoalPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(resultStoreEClass, ResultStore.class, "ResultStore", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResultStore_GoalResults(), this.getGoalResult(), null, "goalResults", null, 0, -1, ResultStore.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResultStore_TreeResults(), this.getTreeResult(), null, "treeResults", null, 0, -1, ResultStore.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(goalResultEClass, GoalResult.class, "GoalResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGoalResult_ReachabilityResult(), this.getResult(), null, "reachabilityResult", null, 0, 1, GoalResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGoalResult_Goal(), theAtsyragoalPackage.getAtsyraGoal(), null, "goal", null, 1, 1, GoalResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGoalResult_ShortestWitnessResult(), this.getWitnessResult(), null, "shortestWitnessResult", null, 0, 1, GoalResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGoalResult_ComplementaryWitnessResults(), this.getWitnessResult(), null, "complementaryWitnessResults", null, 0, -1, GoalResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGoalResult_Precondition(), this.getConditionResult(), null, "precondition", null, 0, 1, GoalResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGoalResult_Postcondition(), this.getConditionResult(), null, "postcondition", null, 0, 1, GoalResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(treeResultEClass, TreeResult.class, "TreeResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTreeResult_MeetResult(), this.getResult(), null, "meetResult", null, 0, 1, TreeResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTreeResult_AdmissibilityResult(), this.getResult(), null, "admissibilityResult", null, 0, 1, TreeResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTreeResult_UndermatchResult(), this.getResult(), null, "undermatchResult", null, 0, 1, TreeResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTreeResult_OvermatchResult(), this.getResult(), null, "overmatchResult", null, 0, 1, TreeResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTreeResult_MatchResult(), this.getResult(), null, "matchResult", null, 0, 1, TreeResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTreeResult_Tree(), theAtsyragoalPackage.getAtsyraTree(), null, "tree", null, 1, 1, TreeResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resultEClass, Result.class, "Result", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getResult_Value(), this.getResultValue(), "value", "NOT_RUN", 0, 1, Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResult_Details(), ecorePackage.getEString(), "details", null, 0, 1, Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResult_Timestamp(), ecorePackage.getEDate(), "timestamp", null, 0, 1, Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResult_Duration(), ecorePackage.getELong(), "duration", null, 0, 1, Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResult_Name(), ecorePackage.getEString(), "name", null, 0, 1, Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResult_Log(), ecorePackage.getEString(), "log", null, 0, 1, Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(witnessResultEClass, WitnessResult.class, "WitnessResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWitnessResult_WitnessFound(), ecorePackage.getEString(), "witnessFound", null, 0, -1, WitnessResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWitnessResult_Value(), this.getResultValue(), "value", null, 0, 1, WitnessResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWitnessResult_Details(), ecorePackage.getEString(), "details", null, 0, 1, WitnessResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWitnessResult_Timestamp(), ecorePackage.getEDate(), "timestamp", null, 0, 1, WitnessResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWitnessResult_CallParameters(), ecorePackage.getEString(), "callParameters", null, 0, 1, WitnessResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWitnessResult_Name(), ecorePackage.getEString(), "name", null, 0, 1, WitnessResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWitnessResult_Log(), ecorePackage.getEString(), "log", null, 0, 1, WitnessResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWitnessResult_Duration(), ecorePackage.getELong(), "duration", null, 0, 1, WitnessResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionResultEClass, ConditionResult.class, "ConditionResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConditionResult_Goalcondition(), theAtsyragoalPackage.getGoalCondition(), null, "goalcondition", null, 1, 1, ConditionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConditionResult_ScenariosResult(), this.getWitnessResult(), null, "scenariosResult", null, 0, 1, ConditionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConditionResult_ReachabilityResult(), this.getResult(), null, "reachabilityResult", null, 0, 1, ConditionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(resultValueEEnum, ResultValue.class, "ResultValue");
		addEEnumLiteral(resultValueEEnum, ResultValue.FALSE);
		addEEnumLiteral(resultValueEEnum, ResultValue.TRUE);
		addEEnumLiteral(resultValueEEnum, ResultValue.TIMEOUT);
		addEEnumLiteral(resultValueEEnum, ResultValue.ABORTED);
		addEEnumLiteral(resultValueEEnum, ResultValue.NOT_RUN);

		// Create resource
		createResource(eNS_URI);
	}

} //ResultstorePackageImpl
