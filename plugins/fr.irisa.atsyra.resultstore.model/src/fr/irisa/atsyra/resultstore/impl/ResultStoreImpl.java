/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore.impl;

import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultstorePackage;
import fr.irisa.atsyra.resultstore.TreeResult;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Result Store</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.ResultStoreImpl#getGoalResults <em>Goal Results</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.ResultStoreImpl#getTreeResults <em>Tree Results</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResultStoreImpl extends MinimalEObjectImpl.Container implements ResultStore {
	/**
	 * The cached value of the '{@link #getGoalResults() <em>Goal Results</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGoalResults()
	 * @generated
	 * @ordered
	 */
	protected EList<GoalResult> goalResults;

	/**
	 * The cached value of the '{@link #getTreeResults() <em>Tree Results</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTreeResults()
	 * @generated
	 * @ordered
	 */
	protected EList<TreeResult> treeResults;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResultStoreImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultstorePackage.Literals.RESULT_STORE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GoalResult> getGoalResults() {
		if (goalResults == null) {
			goalResults = new EObjectContainmentEList<GoalResult>(GoalResult.class, this, ResultstorePackage.RESULT_STORE__GOAL_RESULTS);
		}
		return goalResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TreeResult> getTreeResults() {
		if (treeResults == null) {
			treeResults = new EObjectContainmentEList<TreeResult>(TreeResult.class, this, ResultstorePackage.RESULT_STORE__TREE_RESULTS);
		}
		return treeResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResultstorePackage.RESULT_STORE__GOAL_RESULTS:
				return ((InternalEList<?>)getGoalResults()).basicRemove(otherEnd, msgs);
			case ResultstorePackage.RESULT_STORE__TREE_RESULTS:
				return ((InternalEList<?>)getTreeResults()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultstorePackage.RESULT_STORE__GOAL_RESULTS:
				return getGoalResults();
			case ResultstorePackage.RESULT_STORE__TREE_RESULTS:
				return getTreeResults();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultstorePackage.RESULT_STORE__GOAL_RESULTS:
				getGoalResults().clear();
				getGoalResults().addAll((Collection<? extends GoalResult>)newValue);
				return;
			case ResultstorePackage.RESULT_STORE__TREE_RESULTS:
				getTreeResults().clear();
				getTreeResults().addAll((Collection<? extends TreeResult>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultstorePackage.RESULT_STORE__GOAL_RESULTS:
				getGoalResults().clear();
				return;
			case ResultstorePackage.RESULT_STORE__TREE_RESULTS:
				getTreeResults().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultstorePackage.RESULT_STORE__GOAL_RESULTS:
				return goalResults != null && !goalResults.isEmpty();
			case ResultstorePackage.RESULT_STORE__TREE_RESULTS:
				return treeResults != null && !treeResults.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ResultStoreImpl
