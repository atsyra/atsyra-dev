/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore.impl;

import atsyragoal.GoalCondition;

import fr.irisa.atsyra.resultstore.ConditionResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultstorePackage;
import fr.irisa.atsyra.resultstore.WitnessResult;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Condition Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.ConditionResultImpl#getGoalcondition <em>Goalcondition</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.ConditionResultImpl#getScenariosResult <em>Scenarios Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.impl.ConditionResultImpl#getReachabilityResult <em>Reachability Result</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionResultImpl extends MinimalEObjectImpl.Container implements ConditionResult {
	/**
	 * The cached value of the '{@link #getGoalcondition() <em>Goalcondition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGoalcondition()
	 * @generated
	 * @ordered
	 */
	protected GoalCondition goalcondition;

	/**
	 * The cached value of the '{@link #getScenariosResult() <em>Scenarios Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenariosResult()
	 * @generated
	 * @ordered
	 */
	protected WitnessResult scenariosResult;

	/**
	 * The cached value of the '{@link #getReachabilityResult() <em>Reachability Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReachabilityResult()
	 * @generated
	 * @ordered
	 */
	protected Result reachabilityResult;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultstorePackage.Literals.CONDITION_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalCondition getGoalcondition() {
		if (goalcondition != null && goalcondition.eIsProxy()) {
			InternalEObject oldGoalcondition = (InternalEObject)goalcondition;
			goalcondition = (GoalCondition)eResolveProxy(oldGoalcondition);
			if (goalcondition != oldGoalcondition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ResultstorePackage.CONDITION_RESULT__GOALCONDITION, oldGoalcondition, goalcondition));
			}
		}
		return goalcondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalCondition basicGetGoalcondition() {
		return goalcondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGoalcondition(GoalCondition newGoalcondition) {
		GoalCondition oldGoalcondition = goalcondition;
		goalcondition = newGoalcondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.CONDITION_RESULT__GOALCONDITION, oldGoalcondition, goalcondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WitnessResult getScenariosResult() {
		return scenariosResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScenariosResult(WitnessResult newScenariosResult, NotificationChain msgs) {
		WitnessResult oldScenariosResult = scenariosResult;
		scenariosResult = newScenariosResult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.CONDITION_RESULT__SCENARIOS_RESULT, oldScenariosResult, newScenariosResult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScenariosResult(WitnessResult newScenariosResult) {
		if (newScenariosResult != scenariosResult) {
			NotificationChain msgs = null;
			if (scenariosResult != null)
				msgs = ((InternalEObject)scenariosResult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.CONDITION_RESULT__SCENARIOS_RESULT, null, msgs);
			if (newScenariosResult != null)
				msgs = ((InternalEObject)newScenariosResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.CONDITION_RESULT__SCENARIOS_RESULT, null, msgs);
			msgs = basicSetScenariosResult(newScenariosResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.CONDITION_RESULT__SCENARIOS_RESULT, newScenariosResult, newScenariosResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Result getReachabilityResult() {
		return reachabilityResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReachabilityResult(Result newReachabilityResult, NotificationChain msgs) {
		Result oldReachabilityResult = reachabilityResult;
		reachabilityResult = newReachabilityResult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultstorePackage.CONDITION_RESULT__REACHABILITY_RESULT, oldReachabilityResult, newReachabilityResult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReachabilityResult(Result newReachabilityResult) {
		if (newReachabilityResult != reachabilityResult) {
			NotificationChain msgs = null;
			if (reachabilityResult != null)
				msgs = ((InternalEObject)reachabilityResult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.CONDITION_RESULT__REACHABILITY_RESULT, null, msgs);
			if (newReachabilityResult != null)
				msgs = ((InternalEObject)newReachabilityResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultstorePackage.CONDITION_RESULT__REACHABILITY_RESULT, null, msgs);
			msgs = basicSetReachabilityResult(newReachabilityResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultstorePackage.CONDITION_RESULT__REACHABILITY_RESULT, newReachabilityResult, newReachabilityResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResultstorePackage.CONDITION_RESULT__SCENARIOS_RESULT:
				return basicSetScenariosResult(null, msgs);
			case ResultstorePackage.CONDITION_RESULT__REACHABILITY_RESULT:
				return basicSetReachabilityResult(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultstorePackage.CONDITION_RESULT__GOALCONDITION:
				if (resolve) return getGoalcondition();
				return basicGetGoalcondition();
			case ResultstorePackage.CONDITION_RESULT__SCENARIOS_RESULT:
				return getScenariosResult();
			case ResultstorePackage.CONDITION_RESULT__REACHABILITY_RESULT:
				return getReachabilityResult();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultstorePackage.CONDITION_RESULT__GOALCONDITION:
				setGoalcondition((GoalCondition)newValue);
				return;
			case ResultstorePackage.CONDITION_RESULT__SCENARIOS_RESULT:
				setScenariosResult((WitnessResult)newValue);
				return;
			case ResultstorePackage.CONDITION_RESULT__REACHABILITY_RESULT:
				setReachabilityResult((Result)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultstorePackage.CONDITION_RESULT__GOALCONDITION:
				setGoalcondition((GoalCondition)null);
				return;
			case ResultstorePackage.CONDITION_RESULT__SCENARIOS_RESULT:
				setScenariosResult((WitnessResult)null);
				return;
			case ResultstorePackage.CONDITION_RESULT__REACHABILITY_RESULT:
				setReachabilityResult((Result)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultstorePackage.CONDITION_RESULT__GOALCONDITION:
				return goalcondition != null;
			case ResultstorePackage.CONDITION_RESULT__SCENARIOS_RESULT:
				return scenariosResult != null;
			case ResultstorePackage.CONDITION_RESULT__REACHABILITY_RESULT:
				return reachabilityResult != null;
		}
		return super.eIsSet(featureID);
	}

} //ConditionResultImpl
