/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore;

import atsyragoal.AtsyraGoal;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.GoalResult#getReachabilityResult <em>Reachability Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.GoalResult#getGoal <em>Goal</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.GoalResult#getShortestWitnessResult <em>Shortest Witness Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.GoalResult#getComplementaryWitnessResults <em>Complementary Witness Results</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.GoalResult#getPrecondition <em>Precondition</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.GoalResult#getPostcondition <em>Postcondition</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getGoalResult()
 * @model
 * @generated
 */
public interface GoalResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Reachability Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reachability Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reachability Result</em>' containment reference.
	 * @see #setReachabilityResult(Result)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getGoalResult_ReachabilityResult()
	 * @model containment="true"
	 * @generated
	 */
	Result getReachabilityResult();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.GoalResult#getReachabilityResult <em>Reachability Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reachability Result</em>' containment reference.
	 * @see #getReachabilityResult()
	 * @generated
	 */
	void setReachabilityResult(Result value);

	/**
	 * Returns the value of the '<em><b>Goal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goal</em>' reference.
	 * @see #setGoal(AtsyraGoal)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getGoalResult_Goal()
	 * @model required="true"
	 * @generated
	 */
	AtsyraGoal getGoal();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.GoalResult#getGoal <em>Goal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Goal</em>' reference.
	 * @see #getGoal()
	 * @generated
	 */
	void setGoal(AtsyraGoal value);

	/**
	 * Returns the value of the '<em><b>Shortest Witness Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shortest Witness Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shortest Witness Result</em>' containment reference.
	 * @see #setShortestWitnessResult(WitnessResult)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getGoalResult_ShortestWitnessResult()
	 * @model containment="true"
	 * @generated
	 */
	WitnessResult getShortestWitnessResult();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.GoalResult#getShortestWitnessResult <em>Shortest Witness Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shortest Witness Result</em>' containment reference.
	 * @see #getShortestWitnessResult()
	 * @generated
	 */
	void setShortestWitnessResult(WitnessResult value);

	/**
	 * Returns the value of the '<em><b>Complementary Witness Results</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.resultstore.WitnessResult}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Complementary Witness Results</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Complementary Witness Results</em>' containment reference list.
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getGoalResult_ComplementaryWitnessResults()
	 * @model containment="true"
	 * @generated
	 */
	EList<WitnessResult> getComplementaryWitnessResults();

	/**
	 * Returns the value of the '<em><b>Precondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Precondition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precondition</em>' containment reference.
	 * @see #setPrecondition(ConditionResult)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getGoalResult_Precondition()
	 * @model containment="true"
	 * @generated
	 */
	ConditionResult getPrecondition();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.GoalResult#getPrecondition <em>Precondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Precondition</em>' containment reference.
	 * @see #getPrecondition()
	 * @generated
	 */
	void setPrecondition(ConditionResult value);

	/**
	 * Returns the value of the '<em><b>Postcondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Postcondition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postcondition</em>' containment reference.
	 * @see #setPostcondition(ConditionResult)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getGoalResult_Postcondition()
	 * @model containment="true"
	 * @generated
	 */
	ConditionResult getPostcondition();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.GoalResult#getPostcondition <em>Postcondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postcondition</em>' containment reference.
	 * @see #getPostcondition()
	 * @generated
	 */
	void setPostcondition(ConditionResult value);

} // GoalResult
