/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.resultstore;

import atsyragoal.GoalCondition;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.resultstore.ConditionResult#getGoalcondition <em>Goalcondition</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.ConditionResult#getScenariosResult <em>Scenarios Result</em>}</li>
 *   <li>{@link fr.irisa.atsyra.resultstore.ConditionResult#getReachabilityResult <em>Reachability Result</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getConditionResult()
 * @model
 * @generated
 */
public interface ConditionResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Goalcondition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goalcondition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goalcondition</em>' reference.
	 * @see #setGoalcondition(GoalCondition)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getConditionResult_Goalcondition()
	 * @model required="true"
	 * @generated
	 */
	GoalCondition getGoalcondition();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.ConditionResult#getGoalcondition <em>Goalcondition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Goalcondition</em>' reference.
	 * @see #getGoalcondition()
	 * @generated
	 */
	void setGoalcondition(GoalCondition value);

	/**
	 * Returns the value of the '<em><b>Scenarios Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenarios Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenarios Result</em>' containment reference.
	 * @see #setScenariosResult(WitnessResult)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getConditionResult_ScenariosResult()
	 * @model containment="true"
	 * @generated
	 */
	WitnessResult getScenariosResult();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.ConditionResult#getScenariosResult <em>Scenarios Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenarios Result</em>' containment reference.
	 * @see #getScenariosResult()
	 * @generated
	 */
	void setScenariosResult(WitnessResult value);

	/**
	 * Returns the value of the '<em><b>Reachability Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reachability Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reachability Result</em>' containment reference.
	 * @see #setReachabilityResult(Result)
	 * @see fr.irisa.atsyra.resultstore.ResultstorePackage#getConditionResult_ReachabilityResult()
	 * @model containment="true"
	 * @generated
	 */
	Result getReachabilityResult();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.resultstore.ConditionResult#getReachabilityResult <em>Reachability Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reachability Result</em>' containment reference.
	 * @see #getReachabilityResult()
	 * @generated
	 */
	void setReachabilityResult(Result value);

} // ConditionResult
