/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.json.transfo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.sirius.emfjson.resource.JsonResourceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.atsyra.absystem.model.absystem.Contract;
import fr.irisa.atsyra.absystem.model.absystem.GuardLocale;
import fr.irisa.atsyra.absystem.model.absystem.Import;
import fr.irisa.atsyra.absystem.model.absystem.Requirement;
import fr.irisa.atsyra.absystem.model.absystem.RequirementLocale;

public class ABS2JSON {
	static final Logger log = LoggerFactory.getLogger(ABS2JSON.class);

	private ABS2JSON() {
		throw new IllegalStateException("Not instantiable");
	}

	public static String abs2json(EObject absModel) {
		return JsonResourceImpl.toJson(absModel, null);
	}

	public static Collection<Resource> abs2jsonResourceMapping(Map<Resource, Resource> abs2jsonResourceCopyMapping,
			Map<Resource, Resource> abs2jsonResourceReferenceMapping) throws InvalidJsonResource {
		// copy all the abs in abs2jsonResourceCopyMapping to json
		Copier copier = new Copier();
		for (Entry<Resource, Resource> entry : abs2jsonResourceCopyMapping.entrySet()) {
			entry.getValue().getContents().clear();
			entry.getValue().getContents().add(copier.copy(entry.getKey().getContents().get(0)));
			updateResource(entry.getValue());
		}
		// also register changes to the references according to
		// abs2jsonResourceReferenceMapping
		for (Entry<Resource, Resource> entry : abs2jsonResourceReferenceMapping.entrySet()) {
			final Resource absResource = entry.getKey();
			final Resource jsonResource = entry.getValue();
			if (!absResource.getContents().isEmpty()) {
				if (jsonResource.getContents().isEmpty()) {
					throw new InvalidJsonResource(jsonResource);
				} else {
					for (TreeIterator<EObject> iterator = absResource.getAllContents(); iterator.hasNext();) {
						EObject next = iterator.next();
						final String contentURIFragment = absResource.getURIFragment(next);
						final EObject jsonContentCopy = jsonResource.getEObject(contentURIFragment);
						if (jsonContentCopy == null) {
							throw new InvalidJsonResource(jsonResource, contentURIFragment);
						}
						copier.put(next, jsonContentCopy);
					}
				}
			}
		}
		copier.copyReferences();
		return abs2jsonResourceCopyMapping.values();
	}
	
	private static void updateResource(Resource resource) {
		resource.getAllContents().forEachRemaining(eObject -> {
			if (eObject instanceof Contract || eObject instanceof GuardLocale || eObject instanceof Requirement
					|| eObject instanceof RequirementLocale) {
				sanitizeRequirementAndContractDescription(eObject);
			}
			if (eObject instanceof Import) {
				updateImportURI((Import) eObject);
			}
		});
	}

	private static void sanitizeRequirementAndContractDescription(EObject eObject) {
		try {
			Method setDescription = eObject.getClass().getMethod("setDescription", String.class);
			Method getDescription = eObject.getClass().getMethod("getDescription");
			String description = (String) getDescription.invoke(eObject);
			setDescription.invoke(eObject, description != null ? description.replaceAll("[\\n\\t]", "") : "");
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | ClassCastException e) {
			log.error("Error when sanitizing EObjects description", e);
		}
	}

	private static void updateImportURI(Import imp) {
		if (imp.getImportURI() != null) {
			imp.setImportURI(imp.getImportURI().replaceAll("\\.abs$", ".json"));
		}
	}
}
