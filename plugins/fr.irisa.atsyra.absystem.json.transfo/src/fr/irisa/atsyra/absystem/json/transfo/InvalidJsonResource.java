/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.json.transfo;

import org.eclipse.emf.ecore.resource.Resource;

public class InvalidJsonResource extends Exception {
	final Resource concernedResource;
	final String missingUriFragment;
	
	public InvalidJsonResource(Resource resource) {
		super("Invalid JSON Resource " + resource.getURI());
		concernedResource = resource;
		missingUriFragment = null;
	}
	
	public InvalidJsonResource(Resource resource, String uriFragment) {
		super("Invalid JSON Resource " + resource.getURI() + " : missing element of URI fragment " + uriFragment);
		concernedResource = resource;
		missingUriFragment = uriFragment;
	}

	public Resource getConcernedResource() {
		return concernedResource;
	}

	public String getMissingUriFragment() {
		return missingUriFragment;
	}
}
