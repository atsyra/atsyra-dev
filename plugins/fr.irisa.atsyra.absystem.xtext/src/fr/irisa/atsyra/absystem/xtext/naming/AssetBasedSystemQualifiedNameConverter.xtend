package fr.irisa.atsyra.absystem.xtext.naming

import org.eclipse.xtext.naming.IQualifiedNameConverter

class AssetBasedSystemQualifiedNameConverter extends IQualifiedNameConverter.DefaultImpl {
	
	override String getDelimiter() {
		return "::"
	}
}