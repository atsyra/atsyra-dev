package fr.irisa.atsyra.absystem.xtext.generator

import org.eclipse.xtext.generator.IOutputConfigurationProvider
import org.eclipse.xtext.generator.OutputConfiguration
import org.eclipse.xtext.generator.IFileSystemAccess

class ABSOutputConfigurationProvider implements IOutputConfigurationProvider {
	public static final String DOC_GEN_OUTPUT = "doc-gen";
	public static final String LOCALE_GEN_OUTPUT = "locale-gen";
	
	override getOutputConfigurations() {
		val OutputConfiguration defaultOutput = new OutputConfiguration(IFileSystemAccess.DEFAULT_OUTPUT);
	    defaultOutput.setDescription("Output Folder");
	    defaultOutput.setOutputDirectory("./src-gen");
	    defaultOutput.setOverrideExistingResources(true);
	    defaultOutput.setCreateOutputDirectory(true);
	    defaultOutput.setCleanUpDerivedResources(true);
	    defaultOutput.setSetDerivedProperty(true);
	    
	    val OutputConfiguration docgenOutput = new OutputConfiguration(DOC_GEN_OUTPUT);
	    docgenOutput.setDescription("Doc Output Folder");
	    docgenOutput.setOutputDirectory("./doc-gen");
	    docgenOutput.setOverrideExistingResources(true);
	    docgenOutput.setCreateOutputDirectory(true);
	    docgenOutput.setCleanUpDerivedResources(true);
	    docgenOutput.setSetDerivedProperty(true);

	    val OutputConfiguration localegenOutput = new OutputConfiguration(LOCALE_GEN_OUTPUT) => [
			setDescription("Localization template Output Folder");
			setOutputDirectory("./locales-gen");
			setOverrideExistingResources(true);
			setCreateOutputDirectory(true);
			setCleanUpDerivedResources(true);
			setSetDerivedProperty(true);
		]
	   
	    return newHashSet(defaultOutput, docgenOutput, localegenOutput);
	    
	}
	
}