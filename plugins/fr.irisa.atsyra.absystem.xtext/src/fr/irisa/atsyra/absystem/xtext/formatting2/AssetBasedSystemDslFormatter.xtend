package fr.irisa.atsyra.absystem.xtext.formatting2

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AnnotationKey
import fr.irisa.atsyra.absystem.model.absystem.Asset
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup
import fr.irisa.atsyra.absystem.model.absystem.AssetLink
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale
import fr.irisa.atsyra.absystem.model.absystem.Contract
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale
import fr.irisa.atsyra.absystem.model.absystem.Goal
import fr.irisa.atsyra.absystem.model.absystem.GuardLocale
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction
import fr.irisa.atsyra.absystem.model.absystem.Import
import fr.irisa.atsyra.absystem.model.absystem.LocaleGroup
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod
import fr.irisa.atsyra.absystem.model.absystem.Tag
import fr.irisa.atsyra.absystem.xtext.services.AssetBasedSystemDslGrammarAccess
import java.util.List
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegion
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale
import fr.irisa.atsyra.absystem.model.absystem.Requirement
import fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry
import fr.irisa.atsyra.absystem.model.absystem.RequirementLocale
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue

class AssetBasedSystemDslFormatter extends AbstractFormatter2 {

	@Inject extension AssetBasedSystemDslGrammarAccess

	def dispatch void format(AssetBasedSystem assetBasedSystem, extension IFormattableDocument document) {

		for (_import : assetBasedSystem.imports) {
			_import.format
		}

		for (localization : assetBasedSystem.localizations) {
			localization.format
		}

		for (definitionGroup : assetBasedSystem.definitionGroups) {
			definitionGroup.format
		}

		for (assetGroup : assetBasedSystem.assetGroups) {
			assetGroup.format
		}
	}

	def dispatch void format(Import _import, extension IFormattableDocument document) {
		_import.regionFor.keyword(importAccess.importKeyword_1_0).prepend[newLine]
	}

	def dispatch void format(DefinitionGroup definitionGroup, extension IFormattableDocument document) {
		formatModelObject(
			definitionGroup.regionFor.keyword(definitionGroupRuleAccess.definitionGroupKeyword_1),
			definitionGroup.regionFor.keyword(definitionGroupRuleAccess.leftCurlyBracketKeyword_3),
			definitionGroup.regionFor.keyword(definitionGroupRuleAccess.rightCurlyBracketKeyword_6),
			document
		)
		
		definitionGroup.regionFor.keyword(definitionGroupRuleAccess.tagsKeyword_4_0_0).prepend[newLine]
		definitionGroup.regionFor.keyword(definitionGroupRuleAccess.leftParenthesisKeyword_4_0_1).prepend[oneSpace].append[noSpace]
		definitionGroup.regionFor.keyword(definitionGroupRuleAccess.rightParenthesisKeyword_4_0_3).prepend[noSpace]
		definitionGroup.regionFor.keywords(definitionGroupRuleAccess.commaKeyword_4_0_2_1_0).forEach[prepend[noSpace].append[oneSpace]]
		
		formatModelObject(
			definitionGroup.regionFor.keyword(definitionGroupRuleAccess.annotationsKeyword_4_1_0),
			definitionGroup.regionFor.keyword(definitionGroupRuleAccess.leftCurlyBracketKeyword_4_1_1),
			definitionGroup.regionFor.keyword(definitionGroupRuleAccess.rightCurlyBracketKeyword_4_1_3),
			document
		)
		for(anno : definitionGroup.annotations) {
			anno.format
		}
		
		for (definition : definitionGroup.definitions) {
			definition.format
		}
	}

	def dispatch void format(PrimitiveDataType primitiveDataType, extension IFormattableDocument document) {
		primitiveDataType.regionFor.keyword(primitiveDataType_ImplAccess.primitiveDataTypeKeyword_1).prepend[newLine]
	}

	def dispatch void format(EnumDataType enumDataType, extension IFormattableDocument document) {
		formatModelObject(
			enumDataType.regionFor.keyword(enumDataTypeAccess.enumDataTypeKeyword_1),
			enumDataType.regionFor.keyword(enumDataTypeAccess.getLeftCurlyBracketKeyword_3),
			enumDataType.regionFor.keyword(enumDataTypeAccess.getRightCurlyBracketKeyword_7),
			document
		)
		formatModelObject(
			enumDataType.regionFor.keyword(enumDataTypeAccess.annotationsKeyword_4_0),
			enumDataType.regionFor.keyword(enumDataTypeAccess.leftCurlyBracketKeyword_4_1),
			enumDataType.regionFor.keyword(enumDataTypeAccess.rightCurlyBracketKeyword_4_3),
			document
		)
		enumDataType.annotations.forEach [ a |
			a.regionFor.keyword(annotationEntryAccess.equalsSignKeyword_2).surround[oneSpace]
			a.regionFor.ruleCalls(annotationEntryAccess.keyAnnotationKeyEStringParserRuleCall_1_0_1).forEach [
				prepend[newLine]
			]
		]
		enumDataType.regionFor.keywords(enumDataTypeAccess.commaKeyword_6_0).forEach[prepend[noSpace]]
		for (enumLiteral : enumDataType.enumLiteral) {
			enumLiteral.format
		}
	}

	def dispatch void format(EnumLiteral enumLiteral, extension IFormattableDocument document) {
		enumLiteral.regionFor.assignment(enumLiteralAccess.nameAssignment_1).prepend[newLine]
	}

	def dispatch void format(StaticMethod staticMethod, extension IFormattableDocument document) {
		staticMethod.regionFor.keyword(staticMethodAccess.staticMethodKeyword_1).prepend[setNewLines(1, 2, 2)]
	}

	def dispatch void format(Tag tag, extension IFormattableDocument document) {
		tag.regionFor.keyword(tagAccess.tagKeyword_1).prepend[setNewLines(1, 2, 2)]
	}

	def dispatch void format(AnnotationKey annotationKey, extension IFormattableDocument document) {
		annotationKey.regionFor.keyword(annotationKeyAccess.annotationKeyword_1).prepend[setNewLines(1, 2, 2)]
	}
	
	def dispatch void format(AnnotationEntry annotationEntry, extension IFormattableDocument document) {
		annotationEntry.regionFor.assignment(annotationEntryAccess.keyAssignment_1).prepend[newLine]
		annotationEntry.regionFor.keyword(annotationEntryAccess.equalsSignKeyword_2).surround[oneSpace]
	}

	def dispatch void format(AssetType assetType, extension IFormattableDocument document) {
		val firstKeywordRegion = if (assetType.isAbstract) {
				assetType.regionFor.keyword(assetTypeAccess.abstractAbstractKeyword_1_0).append[oneSpace]
			} else {
				assetType.regionFor.keyword(assetTypeAccess.assetTypeKeyword_2)
			}
		formatModelObject(
			firstKeywordRegion,
			assetType.regionFor.keyword(assetTypeAccess.leftCurlyBracketKeyword_5),
			assetType.regionFor.keyword(assetTypeAccess.rightCurlyBracketKeyword_8),
			document
		)

		assetType.regionFor.keyword(assetTypeAccess.levelKeyword_6_0_0).prepend[newLine]

		assetType.regionFor.keyword(assetTypeAccess.tagsKeyword_6_1_0).prepend[newLine]
		assetType.regionFor.keyword(assetTypeAccess.leftParenthesisKeyword_6_1_1).prepend[oneSpace].append[noSpace]
		assetType.regionFor.keyword(assetTypeAccess.getRightParenthesisKeyword_6_1_3).prepend[noSpace]
		assetType.regionFor.keywords(assetTypeAccess.commaKeyword_6_1_2_1_0).forEach[append[oneSpace]]

		assetType.regionFor.keyword(assetTypeAccess.descriptionKeyword_6_2_0).prepend[newLine].append[oneSpace]

		formatModelObject(
			assetType.regionFor.keyword(assetTypeAccess.annotationsKeyword_6_3_0),
			assetType.regionFor.keyword(assetTypeAccess.leftCurlyBracketKeyword_6_3_1),
			assetType.regionFor.keyword(assetTypeAccess.rightCurlyBracketKeyword_6_3_3),
			document
		)
		assetType.annotations.forEach [ a |
			a.regionFor.keyword(annotationEntryAccess.equalsSignKeyword_2).surround[oneSpace]
			a.regionFor.ruleCalls(annotationEntryAccess.keyAnnotationKeyEStringParserRuleCall_1_0_1).forEach [
				prepend[newLine]
			]
		]

		assetType.assetTypeAttributes.forEach [ attr |
			attr.regionFor.keyword(assetTypeAttributeAccess.attributeKeyword_1).prepend[newLine]
		]

		assetType.assetTypeProperties.forEach [ ref |
			ref.regionFor.keyword(assetTypeReferenceAccess.referenceKeyword_1).prepend[newLine]
		]
	}

	def dispatch void format(AssetTypeAspect assetTypeAspect, extension IFormattableDocument document) {
		formatModelObject(
			assetTypeAspect.regionFor.keyword(assetTypeAspectAccess.assetTypeAspectKeyword_0),
			assetTypeAspect.regionFor.keyword(assetTypeAspectAccess.leftCurlyBracketKeyword_2),
			assetTypeAspect.regionFor.keyword(assetTypeAspectAccess.rightCurlyBracketKeyword_4),
			document
		)
		assetTypeAspect.assetTypeAttributes.forEach [ attr |
			attr.regionFor.keyword(assetTypeAttributeAccess.attributeKeyword_1).prepend[newLine]
		]
		assetTypeAspect.assetTypeProperties.forEach [ ref |
			ref.regionFor.keyword(assetTypeReferenceAccess.referenceKeyword_1).prepend[newLine]
		]
	}

	def dispatch void format(GuardedAction guardedAction, extension IFormattableDocument document) {
		formatModelObject(
			guardedAction.regionFor.keyword(guardedActionAccess.guardedActionKeyword_1),
			guardedAction.regionFor.keyword(guardedActionAccess.leftCurlyBracketKeyword_6),
			guardedAction.regionFor.keyword(guardedActionAccess.rightCurlyBracketKeyword_15),
			document
		)
		formatModelObject(
			guardedAction.regionFor.keyword(guardedActionAccess.annotationsKeyword_7_1_0),
			guardedAction.regionFor.keyword(guardedActionAccess.leftCurlyBracketKeyword_7_1_1),
			guardedAction.regionFor.keyword(guardedActionAccess.rightCurlyBracketKeyword_7_1_3),
			document
		)
		guardedAction.annotations.forEach [ a |
			a.regionFor.keyword(annotationEntryAccess.equalsSignKeyword_2).surround[oneSpace]
			a.regionFor.ruleCalls(annotationEntryAccess.keyAnnotationKeyEStringParserRuleCall_1_0_1).forEach [
				prepend[newLine]
			]
		]
		guardedAction.regionFor.keyword(guardedActionAccess.descriptionKeyword_7_0_0).prepend[newLine].append[oneSpace]
		guardedAction.regionFor.keyword(guardedActionAccess.guardKeyword_8).prepend[newLine]
		guardedAction.regionFor.keyword(guardedActionAccess.equalsSignKeyword_9).surround[oneSpace]
		formatModelObject(
			guardedAction.regionFor.keyword(guardedActionAccess.actionKeyword_11),
			guardedAction.regionFor.keyword(guardedActionAccess.leftCurlyBracketKeyword_12),
			guardedAction.regionFor.keyword(guardedActionAccess.rightCurlyBracketKeyword_14),
			document
		)
		guardedAction.regionFor.keyword(guardedActionAccess.leftCurlyBracketKeyword_12).append[newLine]
		guardedAction.regionFor.keywords(guardedActionAccess.semicolonKeyword_13_1).reverseView.tail.forEach [
			append[newLine]
		]
	}

	def dispatch void format(Contract contract, extension IFormattableDocument document) {
		contract.regionFor.keyword(contractAccess.staticKeyword_1_1).prepend[newLine]
		formatModelObject(
			contract.regionFor.keyword(contractAccess.dynamicDynamicKeyword_1_0_0),
			contract.regionFor.keyword(contractAccess.leftCurlyBracketKeyword_7),
			contract.regionFor.keyword(contractAccess.rightCurlyBracketKeyword_12),
			document
		)
		formatModelObject(
			contract.regionFor.keyword(contractAccess.annotationsKeyword_8_2_0),
			contract.regionFor.keyword(contractAccess.leftCurlyBracketKeyword_8_2_1),
			contract.regionFor.keyword(contractAccess.rightCurlyBracketKeyword_8_2_3),
			document
		)
		contract.annotations.forEach [ c |
			c.regionFor.keyword(annotationEntryAccess.equalsSignKeyword_2).surround[oneSpace]
			c.regionFor.ruleCalls(annotationEntryAccess.keyAnnotationKeyEStringParserRuleCall_1_0_1).forEach [
				prepend[newLine]
			]
		]
		contract.regionFor.keyword(contractAccess.descriptionKeyword_8_1_0).prepend[newLine].append[oneSpace]
		contract.regionFor.keyword(contractAccess.guardKeyword_9).prepend[newLine]
		contract.regionFor.keyword(contractAccess.equalsSignKeyword_10).surround[oneSpace]
		contract.regionFor.keyword(contractAccess.severityKeyword_8_0_0).prepend[newLine]
		contract.regionFor.keyword(contractAccess.getEqualsSignKeyword_8_0_1).surround[oneSpace]
	}

	def dispatch void format(Goal goal, extension IFormattableDocument document) {
		formatModelObject(
			goal.regionFor.keyword(goalAccess.goalKeyword_1),
			goal.regionFor.keyword(goalAccess.leftCurlyBracketKeyword_3),
			goal.regionFor.keyword(goalAccess.rightCurlyBracketKeyword_7),
			document
		)
		formatModelObject(
			goal.regionFor.keyword(goalAccess.annotationsKeyword_4_1_0),
			goal.regionFor.keyword(goalAccess.leftCurlyBracketKeyword_4_1_1),
			goal.regionFor.keyword(goalAccess.rightCurlyBracketKeyword_4_1_3),
			document
		)
		goal.annotations.forEach [ g |
			g.regionFor.keyword(annotationEntryAccess.equalsSignKeyword_2).surround[oneSpace]
			g.regionFor.ruleCalls(annotationEntryAccess.keyAnnotationKeyEStringParserRuleCall_1_0_1).forEach [
				prepend[newLine]
			]
		]
		goal.regionFor.keyword(goalAccess.descriptionKeyword_4_0_0).prepend[newLine].append[oneSpace]
		goal.regionFor.keyword(goalAccess.preKeyword_5_0).prepend[newLine]
		goal.regionFor.keyword(goalAccess.equalsSignKeyword_5_1).surround[oneSpace]
		goal.regionFor.keyword(goalAccess.postKeyword_6_0).prepend[newLine]
		goal.regionFor.keyword(goalAccess.equalsSignKeyword_6_1).surround[oneSpace]
	}

	def dispatch void format(AssetGroup assetGroup, extension IFormattableDocument document) {
		formatModelObject(
			assetGroup.regionFor.keyword(assetGroupAccess.assetGroupKeyword_1),
			assetGroup.regionFor.keyword(assetGroupAccess.leftCurlyBracketKeyword_3),
			assetGroup.regionFor.keyword(assetGroupAccess.rightCurlyBracketKeyword_6),
			document
		)
		
		formatModelObject(
			assetGroup.regionFor.keyword(assetGroupAccess.annotationsKeyword_4_0),
			assetGroup.regionFor.keyword(assetGroupAccess.leftCurlyBracketKeyword_4_1),
			assetGroup.regionFor.keyword(assetGroupAccess.rightCurlyBracketKeyword_4_3),
			document
		)
		for(anno : assetGroup.annotations) {
			anno.format
		}

		for (element : assetGroup.elements) {
			element.format
		}

	}

	def dispatch void format(Asset asset, extension IFormattableDocument document) {
		formatModelObject(
			asset.regionFor.keyword(assetAccess.assetKeyword_0),
			asset.regionFor.keyword(assetAccess.leftCurlyBracketKeyword_4),
			asset.regionFor.keyword(assetAccess.rightCurlyBracketKeyword_7),
			document
		)

		asset.regionFor.keyword(assetAccess.descriptionKeyword_5_0).prepend[newLine].append[oneSpace]

		asset.assetAttributeValues.forEach [ attr |
			attr.format
		]
	}
	
	def dispatch void format(AssetAttributeValue assetAttributeValue, extension IFormattableDocument document) {
		assetAttributeValue.regionFor.keyword(assetAttributeValueAccess.attributeKeyword_1).prepend[newLine]
		assetAttributeValue.regionFor.keyword(assetAttributeValueAccess.colonEqualsSignKeyword_3).surround[oneSpace]
		assetAttributeValue.regionFor.keyword(assetAttributeValueAccess.collectionLeftSquareBracketKeyword_4_1_0_0).prepend[oneSpace].append[noSpace]
		assetAttributeValue.regionFor.keywords(assetAttributeValueAccess.commaKeyword_4_1_2_0).forEach[prepend[noSpace]append[oneSpace]]
		assetAttributeValue.regionFor.keyword(assetAttributeValueAccess.rightSquareBracketKeyword_4_1_3).prepend[noSpace]
	}

	def dispatch void format(AssetLink assetLink, extension IFormattableDocument document) {
		assetLink.regionFor.keyword(assetLinkAccess.linkKeyword_0).prepend[setNewLines(1, 2, 2)]
	}

	def void formatModelObjects(List<ISemanticRegion> objects, List<ISemanticRegion> openBrackets,
		List<ISemanticRegion> closeBrackets, extension IFormattableDocument document) {
		objects.forEach[prepend[setNewLines(1, 2, 2)]]
		openBrackets.forEach[prepend[oneSpace]]
		closeBrackets.forEach[prepend[setNewLines(1, 2, 2)]]
		objects.forEach [ object, idx |
			interior(openBrackets.get(idx), closeBrackets.get(idx))[indent]
		]
	}

	def void formatModelObject(ISemanticRegion object, ISemanticRegion openBracket, ISemanticRegion closeBracket,
		extension IFormattableDocument document) {
		object.prepend[setNewLines(1, 2, 2)]
		openBracket.prepend[oneSpace]
		closeBracket.prepend[setNewLines(1, 2, 2)]
		interior(openBracket, closeBracket)[indent]
	}

	def dispatch void format(LocaleGroup localeGroup, extension IFormattableDocument document) {
		formatModelObject(
			localeGroup.regionFor.keyword(localeGroupAccess.localeKeyword_0),
			localeGroup.regionFor.keyword(localeGroupAccess.leftCurlyBracketKeyword_1),
			localeGroup.regionFor.keyword(localeGroupAccess.rightCurlyBracketKeyword_6),
			document
		)
		localeGroup.regionFor.keyword(localeGroupAccess.localeKeyword_2).prepend[newLine]
		for (locale : localeGroup.objectLocales) {
			locale.format
		}
	}
	
	def dispatch void format(DefinitionGroupLocale locale, extension IFormattableDocument document) {
		formatModelObject(
			locale.regionFor.keyword(definitionGroupLocaleAccess.definitionGroupKeyword_0),
			locale.regionFor.keyword(definitionGroupLocaleAccess.leftCurlyBracketKeyword_2),
			locale.regionFor.keyword(definitionGroupLocaleAccess.rightCurlyBracketKeyword_5),
			document
		)
		formatModelObject(
			locale.regionFor.keyword(definitionGroupLocaleAccess.annotationsKeyword_3_0),
			locale.regionFor.keyword(definitionGroupLocaleAccess.leftCurlyBracketKeyword_3_1),
			locale.regionFor.keyword(definitionGroupLocaleAccess.rightCurlyBracketKeyword_3_3),
			document
		)
		for (annotatio : locale.annotations) {
			annotatio.format
		}
		for (sub : locale.definitionLocales) {
			sub.format
		}
	}

	def dispatch void format(AssetTypeLocale locale, extension IFormattableDocument document) {
		formatModelObject(
			locale.regionFor.keyword(assetTypeLocaleAccess.assetTypeKeyword_1),
			locale.regionFor.keyword(assetTypeLocaleAccess.leftCurlyBracketKeyword_5),
			locale.regionFor.keyword(assetTypeLocaleAccess.rightCurlyBracketKeyword_8),
			document
		)
		locale.regionFor.keyword(assetTypeLocaleAccess.descriptionKeyword_6_0).prepend[newLine]
		for (feature : locale.features) {
			feature.format
		}
	}

	def dispatch void format(AssetTypeAspectLocale locale, extension IFormattableDocument document) {
		formatModelObject(
			locale.regionFor.keyword(assetTypeAspectLocaleAccess.assetTypeAspectKeyword_1),
			locale.regionFor.keyword(assetTypeAspectLocaleAccess.leftCurlyBracketKeyword_3),
			locale.regionFor.keyword(assetTypeAspectLocaleAccess.rightCurlyBracketKeyword_5),
			document
		)
		for (feature : locale.features) {
			feature.format
		}
	}

	def dispatch void format(AssetTypeFeatureLocale locale, extension IFormattableDocument document) {
		locale.regionFor.keyword(assetTypeFeatureLocaleAccess.attributeKeyword_1_1_0).prepend[newLine]
		locale.regionFor.keyword(assetTypeFeatureLocaleAccess.referenceKeyword_1_0_0).prepend[newLine]
	}

	def dispatch void format(PrimitiveDataTypeLocale locale, extension IFormattableDocument document) {
		formatModelObject(
			locale.regionFor.keyword(primitiveDataTypeLocaleAccess.primitiveDataTypeKeyword_1),
			locale.regionFor.keyword(primitiveDataTypeLocaleAccess.leftCurlyBracketKeyword_5),
			locale.regionFor.keyword(primitiveDataTypeLocaleAccess.rightCurlyBracketKeyword_7),
			document
		)
	}

	def dispatch void format(EnumLiteralLocale locale, extension IFormattableDocument document) {
		locale.regionFor.keyword(enumLiteralLocaleAccess.enumLiteralKeyword_1).prepend[newLine]
	}

	def dispatch void format(GuardLocale locale, extension IFormattableDocument document) {
		formatModelObject(
			locale.regionFor.keyword(guardLocaleAccess.guardKeyword_1),
			locale.regionFor.keyword(guardLocaleAccess.leftCurlyBracketKeyword_5),
			locale.regionFor.keyword(guardLocaleAccess.rightCurlyBracketKeyword_8),
			document
		)
		locale.regionFor.keyword(guardLocaleAccess.descriptionKeyword_6_0).prepend[newLine]
		formatModelObject(
			locale.regionFor.keyword(guardLocaleAccess.annotationsKeyword_7_0),
			locale.regionFor.keyword(guardLocaleAccess.leftCurlyBracketKeyword_7_1),
			locale.regionFor.keyword(guardLocaleAccess.rightCurlyBracketKeyword_7_3),
			document
		)
		for (annotatio : locale.annotations) {
			annotatio.format
		}
	}
	
	def dispatch void format(Requirement req, extension IFormattableDocument document) {
		formatModelObject(
			req.regionFor.keyword(requirementAccess.requirementKeyword_0),
			req.regionFor.keyword(requirementAccess.leftCurlyBracketKeyword_2),
			req.regionFor.keyword(requirementAccess.rightCurlyBracketKeyword_7),
			document
		)
		formatModelObject(
			req.regionFor.keyword(requirementAccess.annotationsKeyword_5_0),
			req.regionFor.keyword(requirementAccess.leftCurlyBracketKeyword_5_1),
			req.regionFor.keyword(requirementAccess.rightCurlyBracketKeyword_5_3),
			document
		)
		req.annotations.forEach [ c |
			c.regionFor.keyword(annotationEntryAccess.equalsSignKeyword_2).surround[oneSpace]
			c.regionFor.ruleCalls(annotationEntryAccess.keyAnnotationKeyEStringParserRuleCall_1_0_1).forEach [
				prepend[newLine]
			]
		]
		req.regionFor.keyword(requirementAccess.titleKeyword_3_0).prepend[newLine].append[oneSpace]
		req.regionFor.keyword(requirementAccess.descriptionKeyword_4_0).prepend[newLine].append[oneSpace]
		req.regionFor.keyword(requirementAccess.contractsKeyword_6_0).prepend[newLine].append[oneSpace]
		req.regionFor.keyword(requirementAccess.leftParenthesisKeyword_6_1).append[noSpace]
		req.regionFor.keyword(requirementAccess.rightParenthesisKeyword_6_4).prepend[noSpace]
		req.regionFor.keywords(requirementAccess.commaKeyword_6_3_0).forEach[prepend[noSpace] append[oneSpace]]
	}
	
	def dispatch void format(RequirementLocale req, extension IFormattableDocument document) {
		formatModelObject(
			req.regionFor.keyword(requirementLocaleAccess.requirementKeyword_1),
			req.regionFor.keyword(requirementLocaleAccess.leftCurlyBracketKeyword_3),
			req.regionFor.keyword(requirementLocaleAccess.rightCurlyBracketKeyword_7),
			document
		)
		formatModelObject(
			req.regionFor.keyword(requirementLocaleAccess.annotationsKeyword_6_0),
			req.regionFor.keyword(requirementLocaleAccess.leftCurlyBracketKeyword_6_1),
			req.regionFor.keyword(requirementLocaleAccess.rightCurlyBracketKeyword_6_3),
			document
		)
		req.annotations.forEach [ c |
			c.regionFor.keyword(annotationEntryAccess.equalsSignKeyword_2).surround[oneSpace]
			c.regionFor.ruleCalls(annotationEntryAccess.keyAnnotationKeyEStringParserRuleCall_1_0_1).forEach [
				prepend[newLine]
			]
		]
		req.regionFor.keyword(requirementLocaleAccess.titleKeyword_4_0).prepend[newLine].append[oneSpace]
		req.regionFor.keyword(requirementLocaleAccess.descriptionKeyword_5_0).prepend[newLine].append[oneSpace]
	}
}
