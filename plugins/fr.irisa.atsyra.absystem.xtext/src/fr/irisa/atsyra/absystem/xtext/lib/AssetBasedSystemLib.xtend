package fr.irisa.atsyra.absystem.xtext.lib

import com.google.inject.Inject
import org.eclipse.xtext.naming.IQualifiedNameProvider

class AssetBasedSystemLib {
	
	@Inject extension IQualifiedNameProvider
	
	public val static LIB_PACKAGE = "abs.lang"
	
}