package fr.irisa.atsyra.absystem.xtext.scoping

import org.eclipse.xtext.scoping.impl.AbstractScope
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.resource.EObjectDescription
import org.eclipse.xtext.naming.QualifiedName
import java.util.function.Function

class RenamingScope extends AbstractScope {
	IScope wrapped;
	Function<QualifiedName, String> renaming
	
	protected new(IScope parent, IScope wrapped, Function<QualifiedName, String> renaming ,boolean ignoreCase) {
		super(parent, ignoreCase)
		this.wrapped = wrapped
		this.renaming = renaming
	}
	
	static def RenamingScope LocalizeRenamingScopeFrom(IScope parent, IScope wrapped) {
		new RenamingScope(parent, wrapped, [it.lastSegment], false)
	}
	
	override protected getAllLocalElements() {
		wrapped.allElements.filter[it.EClass !== null].map[
			EObjectDescription.create(renaming.apply(qualifiedName), it.EObjectOrProxy)
		]
	}
	
}