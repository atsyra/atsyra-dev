package fr.irisa.atsyra.absystem.xtext.documentation

import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral
import fr.irisa.atsyra.absystem.model.absystem.Guard
import fr.irisa.atsyra.absystem.model.absystem.Goal
import fr.irisa.atsyra.absystem.model.absystem.Asset
import fr.irisa.atsyra.absystem.model.absystem.Parameter
import java.util.Optional
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale
import java.util.Locale
import fr.irisa.atsyra.preferences.GlobalPreferenceService
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale
import fr.irisa.atsyra.absystem.model.absystem.GuardLocale
import fr.irisa.atsyra.absystem.model.absystem.TextFormat
import com.google.common.html.HtmlEscapers
import org.commonmark.parser.Parser
import org.commonmark.renderer.html.HtmlRenderer
import fr.irisa.atsyra.absystem.model.absystem.Requirement
import fr.irisa.atsyra.absystem.model.absystem.RequirementLocale
import fr.irisa.atsyra.absystem.model.absystem.Contract

class AssetBasedSystemDslEObjectDocumentationProvider {

	def dispatch getDocumentation(EObject eobj) {
		""
	}

	def dispatch getDocumentation(AssetType eobj) {
		val result = new StringBuilder
		val eobjLocale = ABSUtils.getLocalization(eobj, getLocale())
		if(eobjLocale.present) {
			result.append("<h1>Name: ").append(eobjLocale.orElseThrow.name).append("</h1>")
		}
		result.append(getDescription(eobj, eobjLocale))
		if (!eobj.allTags.empty) {
			result.append('''<p>Tags: («FOR tag : eobj.allTags SEPARATOR ", "»«tag.name»«ENDFOR»)</p>''')
		}
		if (eobj.level !== null) {
			result.append("<p>level=").append(eobj.level).append("</p>")
		}
		buildAssetTypeSection(eobj, result)
		result.toString
	}
	
	def buildAssetTypeSection(AssetType type, StringBuilder builder) {
		buildAssetTypeAttributeSection(type, builder)
		buildAssetTypeReferenceSection(type, builder)
	}

	def void buildAssetTypeAttributeSection(AssetType assetType, StringBuilder builder) {
		val lang = getLocale()
		builder.append("<h2>Attributes</h2>")
		builder.append("<ul>")
		for (AssetType supertype : ABSUtils.getSupertypesHierarchy(assetType)) {
			for (AssetTypeAttribute attribute : supertype.assetTypeAttributes) {
				builder.append("<li>")
				val attributeLocale = ABSUtils.getLocalization(attribute, lang)
				buildAssetTypeAttributeDescription(attribute, assetType, builder, attributeLocale)
				builder.append("</li>")
			}
			for (AssetTypeAspect aspect : ABSUtils.getAllAspectsOfType(supertype)) {
				for (AssetTypeAttribute attribute : aspect.assetTypeAttributes) {
					builder.append("<li>")
					val attributeLocale = ABSUtils.getLocalization(attribute, lang)
					buildAssetTypeAttributeDescription(attribute, assetType, builder, attributeLocale)
					builder.append("</li>")
				}
			}
		}
		builder.append("</ul>")
	}

	def void buildAssetTypeAttributeDescription(AssetTypeAttribute attribute, AssetType baseType,
		StringBuilder builder, Optional<AssetTypeFeatureLocale> attributeLocale) {
		if(attribute.attributeType !== null) {
			val attributeTypeLocale = ABSUtils.getLocalization(attribute.attributeType, getLocale())
			builder.append(attributeTypeLocale.map[name].orElse(attribute.attributeType.name)).append(" ")
		}
		builder.append("<b>").append(attribute.name).append("</b> ")
		attributeLocale.map[name].ifPresent[builder.append("(").append(it).append(") ")]
		builder.append(
			attribute.multiplicity.asString)
		if (attribute.hasDefault) {
			if (ABSUtils.isMany(attribute.multiplicity)) {
				builder.
					append(''' (default [«FOR expression : attribute.defaultValues SEPARATOR ", "»«expression.xtextText»«ENDFOR»])''')
			} else {
				builder.
					append(''' (default «FOR expression : attribute.defaultValues SEPARATOR ", "»«expression.xtextText»«ENDFOR»)''')
			}
		}
		if (attribute.eContainer instanceof AbstractAssetType) {
			val containerBaseType = ABSUtils.getBaseAssetType(attribute.eContainer as AbstractAssetType)
			if (containerBaseType !== baseType) {
				builder.append(" from ")
				val containerBaseTypeLocale = ABSUtils.getLocalization(containerBaseType, getLocale())
				builder.append(containerBaseTypeLocale.map[name].orElse(containerBaseType?.name))
			}
		}
	}

	def void buildAssetTypeReferenceSection(AssetType assetType, StringBuilder builder) {
		val lang = getLocale()
		builder.append("<h2>References</h2>")
		builder.append("<ul>")
		for (AssetType supertype : ABSUtils.getSupertypesHierarchy(assetType)) {
			for (AssetTypeReference reference : supertype.assetTypeProperties) {
				builder.append("<li>")
				val referenceLocale = ABSUtils.getLocalization(reference, lang)
				buildAssetTypeReferenceDescription(reference, assetType, builder, referenceLocale)
				builder.append("</li>")
			}
			for (AssetTypeAspect aspect : ABSUtils.getAllAspectsOfType(supertype)) {
				for (AssetTypeReference reference : aspect.assetTypeProperties) {
					builder.append("<li>")
					val referenceLocale = ABSUtils.getLocalization(reference, lang)
					buildAssetTypeReferenceDescription(reference, assetType, builder, referenceLocale)
					builder.append("</li>")
				}
			}
		}
		builder.append("</ul>")
	}

	def void buildAssetTypeReferenceDescription(AssetTypeReference reference, AssetType baseType, StringBuilder builder, Optional<AssetTypeFeatureLocale> referenceLocale) {
		if(reference.propertyType !== null) {
			val attributeTypeLocale = ABSUtils.getLocalization(reference.propertyType, getLocale())
			builder.append(attributeTypeLocale.map[name].orElse(reference.propertyType.name)).append(" ")
		}
		builder.append("<b>").append(reference.name).append("</b> ")
		referenceLocale.map[name].ifPresent[builder.append("(").append(it).append(") ")]
		builder.append(reference.multiplicity.asString)
		if (reference.hasDefault) {
			if (ABSUtils.isMany(reference.multiplicity)) {
				builder.append(" (default [])")
			} else {
				builder.append(" (default undefined)")
			}
		}
		if (reference.eContainer instanceof AbstractAssetType) {
			val containerBaseType = ABSUtils.getBaseAssetType(reference.eContainer as AbstractAssetType)
			if (containerBaseType !== baseType) {
				builder.append(" from ")
				val containerBaseTypeLocale = ABSUtils.getLocalization(containerBaseType, getLocale())
				builder.append(containerBaseTypeLocale.map[name].orElse(containerBaseType?.name))
			}
		}
	}

	def String getXtextText(EObject eobj) {
		val node = NodeModelUtils.findActualNodeFor(eobj);
		if (node !== null) {
			node.getText().trim();
		} else {
			""
		}
	}

	def String asString(Multiplicity multiplicity) {
		switch multiplicity {
			case ONE: {
				"[1]"
			}
			case ONE_OR_MANY: {
				"[1..*]"
			}
			case ZERO_OR_MANY: {
				"[0..*]"
			}
			case ZERO_OR_ONE: {
				"[0..1]"
			}
		}
	}
	
	def dispatch getDocumentation(EnumDataType eobj) {
		val result = new StringBuilder
		val eobjLocale = ABSUtils.getLocalization(eobj, getLocale())
		if(eobjLocale.present) {
			result.append("<h1>Name: ").append(eobjLocale.orElseThrow.name).append("</h1>")
		}
		result.append("<h2>Literals</h2>")
		result.append("<ul>")
		if (eobjLocale.present) {
			for (EnumLiteralLocale literal : eobjLocale.orElseThrow.literals) {
				result.append("<li>")
				result.append(literal.ref.name).append(" (").append(literal.name).append(")")
				result.append("</li>")
			}
		} else {
			for (EnumLiteral literal : eobj.enumLiteral) {
				result.append("<li>")
				result.append(literal.name)
				result.append("</li>")
			}
		}
		result.append("</ul>")
		result.toString
	}
	
	def dispatch getDocumentation(EnumLiteral eobj) {
		val result = new StringBuilder
		val eobjLocale = ABSUtils.getLocalization(eobj, getLocale())
		if(eobjLocale.present) {
			result.append("<h1>Name: ").append(eobjLocale.orElseThrow.name).append("</h1>")
		}
		result.toString
	}
	
	def dispatch getDocumentation(Guard eobj) {
		val result = new StringBuilder
		val eobjLocale = ABSUtils.getLocalization(eobj, getLocale())
		if(eobjLocale.present) {
			result.append("<h1>Name: ").append(eobjLocale.orElseThrow.name).append("</h1>")
		}
		result.append(getDescription(eobj, eobjLocale))
		result.toString
	}
	
	def dispatch getDocumentation(Goal eobj) {
		val result = new StringBuilder
		result.append(getDescription(eobj))
		result.toString
	}

	def dispatch getDocumentation(AssetTypeAttribute eobj) {
		val result = new StringBuilder
		val eobjLocale = ABSUtils.getLocalization(eobj, getLocale())
		if(eobjLocale.present) {
			result.append("<h1>Name: ").append(eobjLocale.orElseThrow.name).append("</h1>")
		}
		val attributeTypeLocale = ABSUtils.getLocalization(eobj.attributeType, getLocale())
		result.append("<p>Type: ").append(attributeTypeLocale.map[name].orElse(eobj.attributeType.name)).append("</p>")
		result.append("<p>Multiplicity: ").append(eobj.multiplicity.asString).append("</p>")
		if (eobj.hasDefault) {
			if (ABSUtils.isMany(eobj.multiplicity)) {
				result.
					append('''<p>Default: [«FOR expression : eobj.defaultValues SEPARATOR ", "»«expression.xtextText»«ENDFOR»]</p>''')
			} else {
				result.
					append('''<p>Default: «FOR expression : eobj.defaultValues SEPARATOR ", "»«expression.xtextText»«ENDFOR»</p>''')
			}
		}
		result.toString
	}

	def dispatch getDocumentation(AssetTypeReference eobj) {
		val result = new StringBuilder
		val eobjLocale = ABSUtils.getLocalization(eobj, getLocale())
		if(eobjLocale.present) {
			result.append("<h1>Name: ").append(eobjLocale.orElseThrow.name).append("</h1>")
		}
		val referenceTypeLocale = ABSUtils.getLocalization(eobj.propertyType, getLocale())
		result.append("<p>Type: ").append(referenceTypeLocale.map[name].orElse(eobj.propertyType.name)).append("</p>")
		result.append("<p>Multiplicity: ").append(eobj.multiplicity.asString).append("</p>")
		if (eobj.hasDefault) {
			if (ABSUtils.isMany(eobj.multiplicity)) {
				result.append("<p>Default: []</p>")
			} else {
				result.append("<p>Default: undefined</p>")
			}
		}
		result.toString
	}
	
	def dispatch getDocumentation(Parameter eobj) {
		val result = new StringBuilder
		if (eobj.parameterType !== null) {
			val parameterTypeLocale = ABSUtils.getLocalization(eobj.parameterType, getLocale())
			result.append("<p>Type: ").append(parameterTypeLocale.map[name].orElse(eobj.parameterType.name)).append("</p>")
			buildAssetTypeSection(eobj.parameterType, result)
		}
		result.toString
	}
	
	def dispatch getDocumentation(Asset eobj) {
		val result = new StringBuilder
		result.append(getDescription(eobj))
		if(eobj.assetType !== null) {
			val assetTypeLocale = ABSUtils.getLocalization(eobj.assetType, getLocale())
			result.append("<p>Type: ").append(assetTypeLocale.map[name].orElse(eobj.assetType.name)).append("</p>")
			buildAssetTypeSection(eobj.assetType, result)
		}
		result.toString
	}
	
	def getLocale() {
		val overrideBy = GlobalPreferenceService.INSTANCE.getString("fr.irisa.atsyra.ide.ui", "overrideLocale");
		if(overrideBy.present && !overrideBy.orElseThrow.empty) {
			overrideBy.orElseThrow
		} else {
			Locale.getDefault().language
		}
	}
	
	def getDescription(AssetType eobj, Optional<AssetTypeLocale> eobjLocale) {
		val localizedDescription = eobjLocale.map[description].orElse(eobj.description)
		val localizedDescriptionFormat = eobjLocale.map[descriptionFormat].orElse(eobj.descriptionFormat)
		if (localizedDescription !== null) {
			formatDescription(localizedDescription, localizedDescriptionFormat)
		} else {
			""
		}
	}
	
	def getDescription(Guard eobj, Optional<GuardLocale> eobjLocale) {
		val localizedDescription = eobjLocale.map[description].orElse(eobj.description)
		val localizedDescriptionFormat = eobjLocale.map[descriptionFormat].orElse(eobj.descriptionFormat)
		if (localizedDescription !== null) {
			formatDescription(localizedDescription, localizedDescriptionFormat)
		} else {
			""
		}
	}
	
	def getDescription(Goal eobj) {
		if (eobj.description !== null) {
			formatDescription(eobj.description, eobj.descriptionFormat)
		} else {
			""
		}
	}
	
	def getDescription(Asset eobj) {
		if (eobj.description !== null) {
			formatDescription(eobj.description, eobj.descriptionFormat)
		} else {
			""
		}
	}
	
	def getDescription(Requirement eobj, Optional<RequirementLocale> eobjLocale) {
		val localizedDescription = eobjLocale.map[description].orElse(eobj.description)
		val localizedDescriptionFormat = eobjLocale.map[descriptionFormat].orElse(eobj.descriptionFormat)
		if (localizedDescription !== null) {
			formatDescription(localizedDescription, localizedDescriptionFormat)
		} else {
			""
		}
	}
	
	def formatDescription(String description, TextFormat format) {
		switch format {
			case PLAINTEXT: {
				description.split("\n").join("<p>","</p><p>","</p>", [HtmlEscapers.htmlEscaper.escape(it)])
			}
			case HTML: {
				description
			}
			case MARKDOWN: {
				val parser = Parser.builder().build();
				val document = parser.parse(description);
				val renderer = HtmlRenderer.builder().build();
				renderer.render(document);
			}
		}
	}
	
	def dispatch getDocumentation(Requirement eobj) {
		val result = new StringBuilder
		val eobjLocale = ABSUtils.getLocalization(eobj, getLocale())
		if(eobjLocale.present) {
			result.append("<h1>Title: ").append(eobjLocale.orElseThrow.title).append("</h1>")
		} else if(eobj.title !== null) {
			result.append("<h1>Title: ").append(eobj.title).append("</h1>")
		}
		result.append(getDescription(eobj, eobjLocale))
		result.append("<h1>Contracts: </h1>")
		buildContractsSection(eobj, result)
		result.toString
	}

	def void buildContractsSection(Requirement eobj, StringBuilder builder) {
		builder.append("<ul>")
		for(contract : eobj.contracts) {
			builder.append("<li>")
			buildContractsItem(contract, builder)
			builder.append("</li>")
		}
		builder.append("</ul>")
	}
	
	def void buildContractsItem(Contract contract, StringBuilder builder) {
		val contractLocale = ABSUtils.getLocalization(contract, getLocale())
		builder.append(contractLocale.map[name].orElse(contract.name))
	}
}
