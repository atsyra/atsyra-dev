package fr.irisa.atsyra.absystem.xtext.resource

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.Import
import java.util.HashMap
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.resource.EObjectDescription
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy
import org.eclipse.xtext.scoping.impl.ImportUriResolver
import org.eclipse.xtext.util.IAcceptor
import org.eclipse.xtext.naming.IQualifiedNameProvider
import fr.irisa.atsyra.absystem.model.absystem.AssetType

// transitive imports management inspired from
// https://blogs.itemis.com/en/in-five-minutes-to-transitive-imports-within-a-dsl-with-xtext
class ABSResourceDescriptionStrategy extends DefaultResourceDescriptionStrategy {

	public val static String USER_KEY_IMPORT_URIS = "IMPORT_URIS";
	public val static String SEPARATOR_CHAR = ",";
	public val static String USER_KEY_ABSTRACT = "ABSTRACT";

	@Inject
	ImportUriResolver uriResolver
	
	@Inject 
	IQualifiedNameProvider qualifiedNameProvider

	override boolean createEObjectDescriptions(EObject eObject, IAcceptor<IEObjectDescription> acceptor) {
		if (eObject instanceof AssetBasedSystem) {
			this.createEObjectDescriptionForAssetBasedSystem(eObject, acceptor)
			return true
		} else if (eObject instanceof AssetType) {
			createEObjectDescriptionForAssetType(eObject, acceptor)
			return true
		} else {
			super.createEObjectDescriptions(eObject, acceptor)
		}
	}

	def void createEObjectDescriptionForAssetBasedSystem(AssetBasedSystem model,
		IAcceptor<IEObjectDescription> acceptor) {

		val uris = newArrayList()
		model.imports.forEach [ Import import |
			if (import.importURI !== null) {
				val resolvedURI = uriResolver.apply(import)
				uris.add(resolvedURI)
			}
		]
		val userData = new HashMap<String, String>
		userData.put(USER_KEY_IMPORT_URIS, uris.join(SEPARATOR_CHAR))
		acceptor.accept(EObjectDescription.create(QualifiedName.create(model.eResource.URI.toString), model, userData))
	}
	
	def void createEObjectDescriptionForAssetType(AssetType model, IAcceptor<IEObjectDescription> acceptor) {
		val userData = newHashMap
		userData.put(USER_KEY_ABSTRACT, model.abstract.toString)
		acceptor.accept(EObjectDescription.create(qualifiedNameProvider.getFullyQualifiedName(model), model, userData))
	}
}
