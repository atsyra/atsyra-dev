package fr.irisa.atsyra.absystem.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.atsyra.absystem.xtext.services.AssetBasedSystemDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalAssetBasedSystemDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'AssetBasedSystem'", "'DefinitionGroup'", "'{'", "'tags'", "'('", "','", "')'", "'annotations'", "'}'", "'AssetGroup'", "'annotation'", "'abstract'", "'AssetType'", "'extends'", "'level'", "'description'", "'AssetTypeAspect'", "'PrimitiveDataType'", "'StaticMethod'", "'Tag'", "'container'", "'reference'", "':'", "'opposite'", "'default'", "'undefined'", "'['", "']'", "'attribute'", "'EnumDataType'", "'Asset'", "'link'", "'to'", "'as'", "'#'", "':='", "'true'", "'false'", "'dynamic'", "'static'", "'contract'", "'severity'", "'='", "'guard'", "'GuardedAction'", "'action'", "';'", "'Goal'", "'pre'", "'post'", "'=>'", "'||'", "'&&'", "'!'", "'=='", "'!='", "'<'", "'<='", "'>'", "'>='", "'.'", "'->'", "'-'", "'+'", "'with'", "'from'", "'import'", "'::'", "'*'", "'Locale'", "'locale'", "'EnumLiteral'", "'Guard'", "'Requirement'", "'title'", "'contracts'", "'[1]'", "'[0..1]'", "'[*]'", "'[1..*]'", "'ERROR'", "'WARNING'", "'assign'", "'add'", "'addAll'", "'clear'", "'remove'", "'removeAll'", "'forAll'", "'plaintext'", "'html'", "'markdown'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__99=99;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalAssetBasedSystemDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalAssetBasedSystemDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalAssetBasedSystemDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalAssetBasedSystemDsl.g"; }



     	private AssetBasedSystemDslGrammarAccess grammarAccess;

        public InternalAssetBasedSystemDslParser(TokenStream input, AssetBasedSystemDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "AssetBasedSystem";
       	}

       	@Override
       	protected AssetBasedSystemDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleAssetBasedSystem"
    // InternalAssetBasedSystemDsl.g:65:1: entryRuleAssetBasedSystem returns [EObject current=null] : iv_ruleAssetBasedSystem= ruleAssetBasedSystem EOF ;
    public final EObject entryRuleAssetBasedSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetBasedSystem = null;


        try {
            // InternalAssetBasedSystemDsl.g:65:57: (iv_ruleAssetBasedSystem= ruleAssetBasedSystem EOF )
            // InternalAssetBasedSystemDsl.g:66:2: iv_ruleAssetBasedSystem= ruleAssetBasedSystem EOF
            {
             newCompositeNode(grammarAccess.getAssetBasedSystemRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetBasedSystem=ruleAssetBasedSystem();

            state._fsp--;

             current =iv_ruleAssetBasedSystem; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetBasedSystem"


    // $ANTLR start "ruleAssetBasedSystem"
    // InternalAssetBasedSystemDsl.g:72:1: ruleAssetBasedSystem returns [EObject current=null] : ( () otherlv_1= 'AssetBasedSystem' ( (lv_imports_2_0= ruleImport ) )* ( (lv_localizations_3_0= ruleLocaleGroup ) )* ( ( (lv_definitionGroups_4_0= ruleDefinitionGroupRule ) ) | ( (lv_assetGroups_5_0= ruleAssetGroup ) ) )* ) ;
    public final EObject ruleAssetBasedSystem() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_imports_2_0 = null;

        EObject lv_localizations_3_0 = null;

        EObject lv_definitionGroups_4_0 = null;

        EObject lv_assetGroups_5_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:78:2: ( ( () otherlv_1= 'AssetBasedSystem' ( (lv_imports_2_0= ruleImport ) )* ( (lv_localizations_3_0= ruleLocaleGroup ) )* ( ( (lv_definitionGroups_4_0= ruleDefinitionGroupRule ) ) | ( (lv_assetGroups_5_0= ruleAssetGroup ) ) )* ) )
            // InternalAssetBasedSystemDsl.g:79:2: ( () otherlv_1= 'AssetBasedSystem' ( (lv_imports_2_0= ruleImport ) )* ( (lv_localizations_3_0= ruleLocaleGroup ) )* ( ( (lv_definitionGroups_4_0= ruleDefinitionGroupRule ) ) | ( (lv_assetGroups_5_0= ruleAssetGroup ) ) )* )
            {
            // InternalAssetBasedSystemDsl.g:79:2: ( () otherlv_1= 'AssetBasedSystem' ( (lv_imports_2_0= ruleImport ) )* ( (lv_localizations_3_0= ruleLocaleGroup ) )* ( ( (lv_definitionGroups_4_0= ruleDefinitionGroupRule ) ) | ( (lv_assetGroups_5_0= ruleAssetGroup ) ) )* )
            // InternalAssetBasedSystemDsl.g:80:3: () otherlv_1= 'AssetBasedSystem' ( (lv_imports_2_0= ruleImport ) )* ( (lv_localizations_3_0= ruleLocaleGroup ) )* ( ( (lv_definitionGroups_4_0= ruleDefinitionGroupRule ) ) | ( (lv_assetGroups_5_0= ruleAssetGroup ) ) )*
            {
            // InternalAssetBasedSystemDsl.g:80:3: ()
            // InternalAssetBasedSystemDsl.g:81:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetBasedSystemAccess().getAssetBasedSystemAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetBasedSystemAccess().getAssetBasedSystemKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:91:3: ( (lv_imports_2_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==75||LA1_0==77) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:92:4: (lv_imports_2_0= ruleImport )
            	    {
            	    // InternalAssetBasedSystemDsl.g:92:4: (lv_imports_2_0= ruleImport )
            	    // InternalAssetBasedSystemDsl.g:93:5: lv_imports_2_0= ruleImport
            	    {

            	    					newCompositeNode(grammarAccess.getAssetBasedSystemAccess().getImportsImportParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_imports_2_0=ruleImport();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAssetBasedSystemRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_2_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Import");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalAssetBasedSystemDsl.g:110:3: ( (lv_localizations_3_0= ruleLocaleGroup ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==80) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:111:4: (lv_localizations_3_0= ruleLocaleGroup )
            	    {
            	    // InternalAssetBasedSystemDsl.g:111:4: (lv_localizations_3_0= ruleLocaleGroup )
            	    // InternalAssetBasedSystemDsl.g:112:5: lv_localizations_3_0= ruleLocaleGroup
            	    {

            	    					newCompositeNode(grammarAccess.getAssetBasedSystemAccess().getLocalizationsLocaleGroupParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_localizations_3_0=ruleLocaleGroup();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAssetBasedSystemRule());
            	    					}
            	    					add(
            	    						current,
            	    						"localizations",
            	    						lv_localizations_3_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.LocaleGroup");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalAssetBasedSystemDsl.g:129:3: ( ( (lv_definitionGroups_4_0= ruleDefinitionGroupRule ) ) | ( (lv_assetGroups_5_0= ruleAssetGroup ) ) )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==12) ) {
                    alt3=1;
                }
                else if ( (LA3_0==20) ) {
                    alt3=2;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:130:4: ( (lv_definitionGroups_4_0= ruleDefinitionGroupRule ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:130:4: ( (lv_definitionGroups_4_0= ruleDefinitionGroupRule ) )
            	    // InternalAssetBasedSystemDsl.g:131:5: (lv_definitionGroups_4_0= ruleDefinitionGroupRule )
            	    {
            	    // InternalAssetBasedSystemDsl.g:131:5: (lv_definitionGroups_4_0= ruleDefinitionGroupRule )
            	    // InternalAssetBasedSystemDsl.g:132:6: lv_definitionGroups_4_0= ruleDefinitionGroupRule
            	    {

            	    						newCompositeNode(grammarAccess.getAssetBasedSystemAccess().getDefinitionGroupsDefinitionGroupRuleParserRuleCall_4_0_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_definitionGroups_4_0=ruleDefinitionGroupRule();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetBasedSystemRule());
            	    						}
            	    						add(
            	    							current,
            	    							"definitionGroups",
            	    							lv_definitionGroups_4_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.DefinitionGroupRule");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalAssetBasedSystemDsl.g:150:4: ( (lv_assetGroups_5_0= ruleAssetGroup ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:150:4: ( (lv_assetGroups_5_0= ruleAssetGroup ) )
            	    // InternalAssetBasedSystemDsl.g:151:5: (lv_assetGroups_5_0= ruleAssetGroup )
            	    {
            	    // InternalAssetBasedSystemDsl.g:151:5: (lv_assetGroups_5_0= ruleAssetGroup )
            	    // InternalAssetBasedSystemDsl.g:152:6: lv_assetGroups_5_0= ruleAssetGroup
            	    {

            	    						newCompositeNode(grammarAccess.getAssetBasedSystemAccess().getAssetGroupsAssetGroupParserRuleCall_4_1_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_assetGroups_5_0=ruleAssetGroup();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetBasedSystemRule());
            	    						}
            	    						add(
            	    							current,
            	    							"assetGroups",
            	    							lv_assetGroups_5_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetGroup");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetBasedSystem"


    // $ANTLR start "entryRuleAbstractAssetType"
    // InternalAssetBasedSystemDsl.g:174:1: entryRuleAbstractAssetType returns [EObject current=null] : iv_ruleAbstractAssetType= ruleAbstractAssetType EOF ;
    public final EObject entryRuleAbstractAssetType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractAssetType = null;


        try {
            // InternalAssetBasedSystemDsl.g:174:58: (iv_ruleAbstractAssetType= ruleAbstractAssetType EOF )
            // InternalAssetBasedSystemDsl.g:175:2: iv_ruleAbstractAssetType= ruleAbstractAssetType EOF
            {
             newCompositeNode(grammarAccess.getAbstractAssetTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractAssetType=ruleAbstractAssetType();

            state._fsp--;

             current =iv_ruleAbstractAssetType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractAssetType"


    // $ANTLR start "ruleAbstractAssetType"
    // InternalAssetBasedSystemDsl.g:181:1: ruleAbstractAssetType returns [EObject current=null] : (this_AssetType_0= ruleAssetType | this_AssetTypeAspect_1= ruleAssetTypeAspect ) ;
    public final EObject ruleAbstractAssetType() throws RecognitionException {
        EObject current = null;

        EObject this_AssetType_0 = null;

        EObject this_AssetTypeAspect_1 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:187:2: ( (this_AssetType_0= ruleAssetType | this_AssetTypeAspect_1= ruleAssetTypeAspect ) )
            // InternalAssetBasedSystemDsl.g:188:2: (this_AssetType_0= ruleAssetType | this_AssetTypeAspect_1= ruleAssetTypeAspect )
            {
            // InternalAssetBasedSystemDsl.g:188:2: (this_AssetType_0= ruleAssetType | this_AssetTypeAspect_1= ruleAssetTypeAspect )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>=22 && LA4_0<=23)) ) {
                alt4=1;
            }
            else if ( (LA4_0==27) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:189:3: this_AssetType_0= ruleAssetType
                    {

                    			newCompositeNode(grammarAccess.getAbstractAssetTypeAccess().getAssetTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AssetType_0=ruleAssetType();

                    state._fsp--;


                    			current = this_AssetType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:198:3: this_AssetTypeAspect_1= ruleAssetTypeAspect
                    {

                    			newCompositeNode(grammarAccess.getAbstractAssetTypeAccess().getAssetTypeAspectParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AssetTypeAspect_1=ruleAssetTypeAspect();

                    state._fsp--;


                    			current = this_AssetTypeAspect_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractAssetType"


    // $ANTLR start "entryRulePrimitiveDataType"
    // InternalAssetBasedSystemDsl.g:210:1: entryRulePrimitiveDataType returns [EObject current=null] : iv_rulePrimitiveDataType= rulePrimitiveDataType EOF ;
    public final EObject entryRulePrimitiveDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveDataType = null;


        try {
            // InternalAssetBasedSystemDsl.g:210:58: (iv_rulePrimitiveDataType= rulePrimitiveDataType EOF )
            // InternalAssetBasedSystemDsl.g:211:2: iv_rulePrimitiveDataType= rulePrimitiveDataType EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimitiveDataType=rulePrimitiveDataType();

            state._fsp--;

             current =iv_rulePrimitiveDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveDataType"


    // $ANTLR start "rulePrimitiveDataType"
    // InternalAssetBasedSystemDsl.g:217:1: rulePrimitiveDataType returns [EObject current=null] : (this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl | this_EnumDataType_1= ruleEnumDataType ) ;
    public final EObject rulePrimitiveDataType() throws RecognitionException {
        EObject current = null;

        EObject this_PrimitiveDataType_Impl_0 = null;

        EObject this_EnumDataType_1 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:223:2: ( (this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl | this_EnumDataType_1= ruleEnumDataType ) )
            // InternalAssetBasedSystemDsl.g:224:2: (this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl | this_EnumDataType_1= ruleEnumDataType )
            {
            // InternalAssetBasedSystemDsl.g:224:2: (this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl | this_EnumDataType_1= ruleEnumDataType )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==28) ) {
                alt5=1;
            }
            else if ( (LA5_0==40) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:225:3: this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl
                    {

                    			newCompositeNode(grammarAccess.getPrimitiveDataTypeAccess().getPrimitiveDataType_ImplParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_PrimitiveDataType_Impl_0=rulePrimitiveDataType_Impl();

                    state._fsp--;


                    			current = this_PrimitiveDataType_Impl_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:234:3: this_EnumDataType_1= ruleEnumDataType
                    {

                    			newCompositeNode(grammarAccess.getPrimitiveDataTypeAccess().getEnumDataTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_EnumDataType_1=ruleEnumDataType();

                    state._fsp--;


                    			current = this_EnumDataType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveDataType"


    // $ANTLR start "entryRuleDefinitionGroupRule"
    // InternalAssetBasedSystemDsl.g:246:1: entryRuleDefinitionGroupRule returns [EObject current=null] : iv_ruleDefinitionGroupRule= ruleDefinitionGroupRule EOF ;
    public final EObject entryRuleDefinitionGroupRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinitionGroupRule = null;


        try {
            // InternalAssetBasedSystemDsl.g:246:60: (iv_ruleDefinitionGroupRule= ruleDefinitionGroupRule EOF )
            // InternalAssetBasedSystemDsl.g:247:2: iv_ruleDefinitionGroupRule= ruleDefinitionGroupRule EOF
            {
             newCompositeNode(grammarAccess.getDefinitionGroupRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefinitionGroupRule=ruleDefinitionGroupRule();

            state._fsp--;

             current =iv_ruleDefinitionGroupRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinitionGroupRule"


    // $ANTLR start "ruleDefinitionGroupRule"
    // InternalAssetBasedSystemDsl.g:253:1: ruleDefinitionGroupRule returns [EObject current=null] : ( () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}' ) ;
    public final EObject ruleDefinitionGroupRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_annotations_13_0 = null;

        EObject lv_definitions_15_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:259:2: ( ( () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}' ) )
            // InternalAssetBasedSystemDsl.g:260:2: ( () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}' )
            {
            // InternalAssetBasedSystemDsl.g:260:2: ( () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}' )
            // InternalAssetBasedSystemDsl.g:261:3: () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}'
            {
            // InternalAssetBasedSystemDsl.g:261:3: ()
            // InternalAssetBasedSystemDsl.g:262:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDefinitionGroupRuleAccess().getDefinitionGroupAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getDefinitionGroupRuleAccess().getDefinitionGroupKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:272:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:273:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:273:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:274:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_7);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDefinitionGroupRuleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_3, grammarAccess.getDefinitionGroupRuleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:295:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) )
            // InternalAssetBasedSystemDsl.g:296:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) )
            {
            // InternalAssetBasedSystemDsl.g:296:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) )
            // InternalAssetBasedSystemDsl.g:297:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4());
            				
            // InternalAssetBasedSystemDsl.g:300:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* )
            // InternalAssetBasedSystemDsl.g:301:6: ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )*
            {
            // InternalAssetBasedSystemDsl.g:301:6: ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )*
            loop9:
            do {
                int alt9=3;
                int LA9_0 = input.LA(1);

                if ( LA9_0 == 14 && getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 0) ) {
                    alt9=1;
                }
                else if ( LA9_0 == 18 && getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 1) ) {
                    alt9=2;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:302:4: ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:302:4: ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:303:5: {...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleDefinitionGroupRule", "getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 0)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:303:116: ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) )
            	    // InternalAssetBasedSystemDsl.g:304:6: ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 0);
            	    					
            	    // InternalAssetBasedSystemDsl.g:307:9: ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) )
            	    // InternalAssetBasedSystemDsl.g:307:10: {...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDefinitionGroupRule", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:307:19: (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' )
            	    // InternalAssetBasedSystemDsl.g:307:20: otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')'
            	    {
            	    otherlv_5=(Token)match(input,14,FOLLOW_9); 

            	    									newLeafNode(otherlv_5, grammarAccess.getDefinitionGroupRuleAccess().getTagsKeyword_4_0_0());
            	    								
            	    otherlv_6=(Token)match(input,15,FOLLOW_10); 

            	    									newLeafNode(otherlv_6, grammarAccess.getDefinitionGroupRuleAccess().getLeftParenthesisKeyword_4_0_1());
            	    								
            	    // InternalAssetBasedSystemDsl.g:315:9: ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )?
            	    int alt7=2;
            	    int LA7_0 = input.LA(1);

            	    if ( (LA7_0==RULE_ID) ) {
            	        alt7=1;
            	    }
            	    switch (alt7) {
            	        case 1 :
            	            // InternalAssetBasedSystemDsl.g:316:10: ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )*
            	            {
            	            // InternalAssetBasedSystemDsl.g:316:10: ( ( ruleQualifiedName ) )
            	            // InternalAssetBasedSystemDsl.g:317:11: ( ruleQualifiedName )
            	            {
            	            // InternalAssetBasedSystemDsl.g:317:11: ( ruleQualifiedName )
            	            // InternalAssetBasedSystemDsl.g:318:12: ruleQualifiedName
            	            {

            	            												if (current==null) {
            	            													current = createModelElement(grammarAccess.getDefinitionGroupRuleRule());
            	            												}
            	            											

            	            												newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getTagsTagCrossReference_4_0_2_0_0());
            	            											
            	            pushFollow(FOLLOW_11);
            	            ruleQualifiedName();

            	            state._fsp--;


            	            												afterParserOrEnumRuleCall();
            	            											

            	            }


            	            }

            	            // InternalAssetBasedSystemDsl.g:332:10: (otherlv_8= ',' ( ( ruleQualifiedName ) ) )*
            	            loop6:
            	            do {
            	                int alt6=2;
            	                int LA6_0 = input.LA(1);

            	                if ( (LA6_0==16) ) {
            	                    alt6=1;
            	                }


            	                switch (alt6) {
            	            	case 1 :
            	            	    // InternalAssetBasedSystemDsl.g:333:11: otherlv_8= ',' ( ( ruleQualifiedName ) )
            	            	    {
            	            	    otherlv_8=(Token)match(input,16,FOLLOW_12); 

            	            	    											newLeafNode(otherlv_8, grammarAccess.getDefinitionGroupRuleAccess().getCommaKeyword_4_0_2_1_0());
            	            	    										
            	            	    // InternalAssetBasedSystemDsl.g:337:11: ( ( ruleQualifiedName ) )
            	            	    // InternalAssetBasedSystemDsl.g:338:12: ( ruleQualifiedName )
            	            	    {
            	            	    // InternalAssetBasedSystemDsl.g:338:12: ( ruleQualifiedName )
            	            	    // InternalAssetBasedSystemDsl.g:339:13: ruleQualifiedName
            	            	    {

            	            	    													if (current==null) {
            	            	    														current = createModelElement(grammarAccess.getDefinitionGroupRuleRule());
            	            	    													}
            	            	    												

            	            	    													newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getTagsTagCrossReference_4_0_2_1_1_0());
            	            	    												
            	            	    pushFollow(FOLLOW_11);
            	            	    ruleQualifiedName();

            	            	    state._fsp--;


            	            	    													afterParserOrEnumRuleCall();
            	            	    												

            	            	    }


            	            	    }


            	            	    }
            	            	    break;

            	            	default :
            	            	    break loop6;
            	                }
            	            } while (true);


            	            }
            	            break;

            	    }

            	    otherlv_10=(Token)match(input,17,FOLLOW_8); 

            	    									newLeafNode(otherlv_10, grammarAccess.getDefinitionGroupRuleAccess().getRightParenthesisKeyword_4_0_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalAssetBasedSystemDsl.g:365:4: ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:365:4: ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:366:5: {...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleDefinitionGroupRule", "getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 1)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:366:116: ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) )
            	    // InternalAssetBasedSystemDsl.g:367:6: ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 1);
            	    					
            	    // InternalAssetBasedSystemDsl.g:370:9: ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) )
            	    // InternalAssetBasedSystemDsl.g:370:10: {...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDefinitionGroupRule", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:370:19: (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )
            	    // InternalAssetBasedSystemDsl.g:370:20: otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}'
            	    {
            	    otherlv_11=(Token)match(input,18,FOLLOW_7); 

            	    									newLeafNode(otherlv_11, grammarAccess.getDefinitionGroupRuleAccess().getAnnotationsKeyword_4_1_0());
            	    								
            	    otherlv_12=(Token)match(input,13,FOLLOW_13); 

            	    									newLeafNode(otherlv_12, grammarAccess.getDefinitionGroupRuleAccess().getLeftCurlyBracketKeyword_4_1_1());
            	    								
            	    // InternalAssetBasedSystemDsl.g:378:9: ( (lv_annotations_13_0= ruleAnnotationEntry ) )*
            	    loop8:
            	    do {
            	        int alt8=2;
            	        int LA8_0 = input.LA(1);

            	        if ( ((LA8_0>=RULE_STRING && LA8_0<=RULE_ID)) ) {
            	            alt8=1;
            	        }


            	        switch (alt8) {
            	    	case 1 :
            	    	    // InternalAssetBasedSystemDsl.g:379:10: (lv_annotations_13_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalAssetBasedSystemDsl.g:379:10: (lv_annotations_13_0= ruleAnnotationEntry )
            	    	    // InternalAssetBasedSystemDsl.g:380:11: lv_annotations_13_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getAnnotationsAnnotationEntryParserRuleCall_4_1_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_13);
            	    	    lv_annotations_13_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getDefinitionGroupRuleRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_13_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop8;
            	        }
            	    } while (true);

            	    otherlv_14=(Token)match(input,19,FOLLOW_8); 

            	    									newLeafNode(otherlv_14, grammarAccess.getDefinitionGroupRuleAccess().getRightCurlyBracketKeyword_4_1_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4());
            				

            }

            // InternalAssetBasedSystemDsl.g:414:3: ( (lv_definitions_15_0= ruleDefinition ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==12||(LA10_0>=21 && LA10_0<=23)||(LA10_0>=27 && LA10_0<=30)||LA10_0==40||(LA10_0>=49 && LA10_0<=51)||LA10_0==55||LA10_0==84) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:415:4: (lv_definitions_15_0= ruleDefinition )
            	    {
            	    // InternalAssetBasedSystemDsl.g:415:4: (lv_definitions_15_0= ruleDefinition )
            	    // InternalAssetBasedSystemDsl.g:416:5: lv_definitions_15_0= ruleDefinition
            	    {

            	    					newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getDefinitionsDefinitionParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_14);
            	    lv_definitions_15_0=ruleDefinition();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDefinitionGroupRuleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"definitions",
            	    						lv_definitions_15_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Definition");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            otherlv_16=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_16, grammarAccess.getDefinitionGroupRuleAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinitionGroupRule"


    // $ANTLR start "entryRuleDefinition"
    // InternalAssetBasedSystemDsl.g:441:1: entryRuleDefinition returns [EObject current=null] : iv_ruleDefinition= ruleDefinition EOF ;
    public final EObject entryRuleDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinition = null;


        try {
            // InternalAssetBasedSystemDsl.g:441:51: (iv_ruleDefinition= ruleDefinition EOF )
            // InternalAssetBasedSystemDsl.g:442:2: iv_ruleDefinition= ruleDefinition EOF
            {
             newCompositeNode(grammarAccess.getDefinitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefinition=ruleDefinition();

            state._fsp--;

             current =iv_ruleDefinition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinition"


    // $ANTLR start "ruleDefinition"
    // InternalAssetBasedSystemDsl.g:448:1: ruleDefinition returns [EObject current=null] : (this_DefinitionGroupRule_0= ruleDefinitionGroupRule | this_AbstractAssetType_1= ruleAbstractAssetType | this_Contract_2= ruleContract | this_PrimitiveDataType_3= rulePrimitiveDataType | this_StaticMethod_4= ruleStaticMethod | this_Tag_5= ruleTag | this_AnnotationKey_6= ruleAnnotationKey | this_GuardedAction_7= ruleGuardedAction | this_Requirement_8= ruleRequirement ) ;
    public final EObject ruleDefinition() throws RecognitionException {
        EObject current = null;

        EObject this_DefinitionGroupRule_0 = null;

        EObject this_AbstractAssetType_1 = null;

        EObject this_Contract_2 = null;

        EObject this_PrimitiveDataType_3 = null;

        EObject this_StaticMethod_4 = null;

        EObject this_Tag_5 = null;

        EObject this_AnnotationKey_6 = null;

        EObject this_GuardedAction_7 = null;

        EObject this_Requirement_8 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:454:2: ( (this_DefinitionGroupRule_0= ruleDefinitionGroupRule | this_AbstractAssetType_1= ruleAbstractAssetType | this_Contract_2= ruleContract | this_PrimitiveDataType_3= rulePrimitiveDataType | this_StaticMethod_4= ruleStaticMethod | this_Tag_5= ruleTag | this_AnnotationKey_6= ruleAnnotationKey | this_GuardedAction_7= ruleGuardedAction | this_Requirement_8= ruleRequirement ) )
            // InternalAssetBasedSystemDsl.g:455:2: (this_DefinitionGroupRule_0= ruleDefinitionGroupRule | this_AbstractAssetType_1= ruleAbstractAssetType | this_Contract_2= ruleContract | this_PrimitiveDataType_3= rulePrimitiveDataType | this_StaticMethod_4= ruleStaticMethod | this_Tag_5= ruleTag | this_AnnotationKey_6= ruleAnnotationKey | this_GuardedAction_7= ruleGuardedAction | this_Requirement_8= ruleRequirement )
            {
            // InternalAssetBasedSystemDsl.g:455:2: (this_DefinitionGroupRule_0= ruleDefinitionGroupRule | this_AbstractAssetType_1= ruleAbstractAssetType | this_Contract_2= ruleContract | this_PrimitiveDataType_3= rulePrimitiveDataType | this_StaticMethod_4= ruleStaticMethod | this_Tag_5= ruleTag | this_AnnotationKey_6= ruleAnnotationKey | this_GuardedAction_7= ruleGuardedAction | this_Requirement_8= ruleRequirement )
            int alt11=9;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt11=1;
                }
                break;
            case 22:
            case 23:
            case 27:
                {
                alt11=2;
                }
                break;
            case 49:
            case 50:
            case 51:
                {
                alt11=3;
                }
                break;
            case 28:
            case 40:
                {
                alt11=4;
                }
                break;
            case 29:
                {
                alt11=5;
                }
                break;
            case 30:
                {
                alt11=6;
                }
                break;
            case 21:
                {
                alt11=7;
                }
                break;
            case 55:
                {
                alt11=8;
                }
                break;
            case 84:
                {
                alt11=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:456:3: this_DefinitionGroupRule_0= ruleDefinitionGroupRule
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getDefinitionGroupRuleParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_DefinitionGroupRule_0=ruleDefinitionGroupRule();

                    state._fsp--;


                    			current = this_DefinitionGroupRule_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:465:3: this_AbstractAssetType_1= ruleAbstractAssetType
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getAbstractAssetTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AbstractAssetType_1=ruleAbstractAssetType();

                    state._fsp--;


                    			current = this_AbstractAssetType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalAssetBasedSystemDsl.g:474:3: this_Contract_2= ruleContract
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getContractParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Contract_2=ruleContract();

                    state._fsp--;


                    			current = this_Contract_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalAssetBasedSystemDsl.g:483:3: this_PrimitiveDataType_3= rulePrimitiveDataType
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getPrimitiveDataTypeParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_PrimitiveDataType_3=rulePrimitiveDataType();

                    state._fsp--;


                    			current = this_PrimitiveDataType_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalAssetBasedSystemDsl.g:492:3: this_StaticMethod_4= ruleStaticMethod
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getStaticMethodParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_StaticMethod_4=ruleStaticMethod();

                    state._fsp--;


                    			current = this_StaticMethod_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalAssetBasedSystemDsl.g:501:3: this_Tag_5= ruleTag
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getTagParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Tag_5=ruleTag();

                    state._fsp--;


                    			current = this_Tag_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalAssetBasedSystemDsl.g:510:3: this_AnnotationKey_6= ruleAnnotationKey
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getAnnotationKeyParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_AnnotationKey_6=ruleAnnotationKey();

                    state._fsp--;


                    			current = this_AnnotationKey_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalAssetBasedSystemDsl.g:519:3: this_GuardedAction_7= ruleGuardedAction
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getGuardedActionParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_GuardedAction_7=ruleGuardedAction();

                    state._fsp--;


                    			current = this_GuardedAction_7;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 9 :
                    // InternalAssetBasedSystemDsl.g:528:3: this_Requirement_8= ruleRequirement
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getRequirementParserRuleCall_8());
                    		
                    pushFollow(FOLLOW_2);
                    this_Requirement_8=ruleRequirement();

                    state._fsp--;


                    			current = this_Requirement_8;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinition"


    // $ANTLR start "entryRuleAssetGroup"
    // InternalAssetBasedSystemDsl.g:540:1: entryRuleAssetGroup returns [EObject current=null] : iv_ruleAssetGroup= ruleAssetGroup EOF ;
    public final EObject entryRuleAssetGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetGroup = null;


        try {
            // InternalAssetBasedSystemDsl.g:540:51: (iv_ruleAssetGroup= ruleAssetGroup EOF )
            // InternalAssetBasedSystemDsl.g:541:2: iv_ruleAssetGroup= ruleAssetGroup EOF
            {
             newCompositeNode(grammarAccess.getAssetGroupRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetGroup=ruleAssetGroup();

            state._fsp--;

             current =iv_ruleAssetGroup; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetGroup"


    // $ANTLR start "ruleAssetGroup"
    // InternalAssetBasedSystemDsl.g:547:1: ruleAssetGroup returns [EObject current=null] : ( () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}' ) ;
    public final EObject ruleAssetGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_annotations_6_0 = null;

        EObject lv_elements_8_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:553:2: ( ( () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}' ) )
            // InternalAssetBasedSystemDsl.g:554:2: ( () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}' )
            {
            // InternalAssetBasedSystemDsl.g:554:2: ( () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}' )
            // InternalAssetBasedSystemDsl.g:555:3: () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}'
            {
            // InternalAssetBasedSystemDsl.g:555:3: ()
            // InternalAssetBasedSystemDsl.g:556:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetGroupAccess().getAssetGroupAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,20,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetGroupAccess().getAssetGroupKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:566:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:567:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:567:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:568:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetGroupAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_7);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetGroupRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_15); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetGroupAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:589:3: (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==18) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:590:4: otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}'
                    {
                    otherlv_4=(Token)match(input,18,FOLLOW_7); 

                    				newLeafNode(otherlv_4, grammarAccess.getAssetGroupAccess().getAnnotationsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,13,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getAssetGroupAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalAssetBasedSystemDsl.g:598:4: ( (lv_annotations_6_0= ruleAnnotationEntry ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( ((LA12_0>=RULE_STRING && LA12_0<=RULE_ID)) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:599:5: (lv_annotations_6_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:599:5: (lv_annotations_6_0= ruleAnnotationEntry )
                    	    // InternalAssetBasedSystemDsl.g:600:6: lv_annotations_6_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getAssetGroupAccess().getAnnotationsAnnotationEntryParserRuleCall_4_2_0());
                    	    					
                    	    pushFollow(FOLLOW_13);
                    	    lv_annotations_6_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getAssetGroupRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_6_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,19,FOLLOW_16); 

                    				newLeafNode(otherlv_7, grammarAccess.getAssetGroupAccess().getRightCurlyBracketKeyword_4_3());
                    			

                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:622:3: ( (lv_elements_8_0= ruleAssetGroupContent ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==20||(LA14_0>=41 && LA14_0<=42)||LA14_0==58) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:623:4: (lv_elements_8_0= ruleAssetGroupContent )
            	    {
            	    // InternalAssetBasedSystemDsl.g:623:4: (lv_elements_8_0= ruleAssetGroupContent )
            	    // InternalAssetBasedSystemDsl.g:624:5: lv_elements_8_0= ruleAssetGroupContent
            	    {

            	    					newCompositeNode(grammarAccess.getAssetGroupAccess().getElementsAssetGroupContentParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_16);
            	    lv_elements_8_0=ruleAssetGroupContent();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAssetGroupRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_8_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetGroupContent");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            otherlv_9=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getAssetGroupAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetGroup"


    // $ANTLR start "entryRuleAssetGroupContent"
    // InternalAssetBasedSystemDsl.g:649:1: entryRuleAssetGroupContent returns [EObject current=null] : iv_ruleAssetGroupContent= ruleAssetGroupContent EOF ;
    public final EObject entryRuleAssetGroupContent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetGroupContent = null;


        try {
            // InternalAssetBasedSystemDsl.g:649:58: (iv_ruleAssetGroupContent= ruleAssetGroupContent EOF )
            // InternalAssetBasedSystemDsl.g:650:2: iv_ruleAssetGroupContent= ruleAssetGroupContent EOF
            {
             newCompositeNode(grammarAccess.getAssetGroupContentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetGroupContent=ruleAssetGroupContent();

            state._fsp--;

             current =iv_ruleAssetGroupContent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetGroupContent"


    // $ANTLR start "ruleAssetGroupContent"
    // InternalAssetBasedSystemDsl.g:656:1: ruleAssetGroupContent returns [EObject current=null] : (this_AssetGroup_0= ruleAssetGroup | this_Asset_1= ruleAsset | this_AssetLink_2= ruleAssetLink | this_Goal_3= ruleGoal ) ;
    public final EObject ruleAssetGroupContent() throws RecognitionException {
        EObject current = null;

        EObject this_AssetGroup_0 = null;

        EObject this_Asset_1 = null;

        EObject this_AssetLink_2 = null;

        EObject this_Goal_3 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:662:2: ( (this_AssetGroup_0= ruleAssetGroup | this_Asset_1= ruleAsset | this_AssetLink_2= ruleAssetLink | this_Goal_3= ruleGoal ) )
            // InternalAssetBasedSystemDsl.g:663:2: (this_AssetGroup_0= ruleAssetGroup | this_Asset_1= ruleAsset | this_AssetLink_2= ruleAssetLink | this_Goal_3= ruleGoal )
            {
            // InternalAssetBasedSystemDsl.g:663:2: (this_AssetGroup_0= ruleAssetGroup | this_Asset_1= ruleAsset | this_AssetLink_2= ruleAssetLink | this_Goal_3= ruleGoal )
            int alt15=4;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt15=1;
                }
                break;
            case 41:
                {
                alt15=2;
                }
                break;
            case 42:
                {
                alt15=3;
                }
                break;
            case 58:
                {
                alt15=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }

            switch (alt15) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:664:3: this_AssetGroup_0= ruleAssetGroup
                    {

                    			newCompositeNode(grammarAccess.getAssetGroupContentAccess().getAssetGroupParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AssetGroup_0=ruleAssetGroup();

                    state._fsp--;


                    			current = this_AssetGroup_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:673:3: this_Asset_1= ruleAsset
                    {

                    			newCompositeNode(grammarAccess.getAssetGroupContentAccess().getAssetParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Asset_1=ruleAsset();

                    state._fsp--;


                    			current = this_Asset_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalAssetBasedSystemDsl.g:682:3: this_AssetLink_2= ruleAssetLink
                    {

                    			newCompositeNode(grammarAccess.getAssetGroupContentAccess().getAssetLinkParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_AssetLink_2=ruleAssetLink();

                    state._fsp--;


                    			current = this_AssetLink_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalAssetBasedSystemDsl.g:691:3: this_Goal_3= ruleGoal
                    {

                    			newCompositeNode(grammarAccess.getAssetGroupContentAccess().getGoalParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Goal_3=ruleGoal();

                    state._fsp--;


                    			current = this_Goal_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetGroupContent"


    // $ANTLR start "entryRuleAnnotationKey"
    // InternalAssetBasedSystemDsl.g:703:1: entryRuleAnnotationKey returns [EObject current=null] : iv_ruleAnnotationKey= ruleAnnotationKey EOF ;
    public final EObject entryRuleAnnotationKey() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotationKey = null;


        try {
            // InternalAssetBasedSystemDsl.g:703:54: (iv_ruleAnnotationKey= ruleAnnotationKey EOF )
            // InternalAssetBasedSystemDsl.g:704:2: iv_ruleAnnotationKey= ruleAnnotationKey EOF
            {
             newCompositeNode(grammarAccess.getAnnotationKeyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnnotationKey=ruleAnnotationKey();

            state._fsp--;

             current =iv_ruleAnnotationKey; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotationKey"


    // $ANTLR start "ruleAnnotationKey"
    // InternalAssetBasedSystemDsl.g:710:1: ruleAnnotationKey returns [EObject current=null] : ( () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleAnnotationKey() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:716:2: ( ( () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalAssetBasedSystemDsl.g:717:2: ( () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalAssetBasedSystemDsl.g:717:2: ( () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) ) )
            // InternalAssetBasedSystemDsl.g:718:3: () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalAssetBasedSystemDsl.g:718:3: ()
            // InternalAssetBasedSystemDsl.g:719:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAnnotationKeyAccess().getAnnotationKeyAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,21,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getAnnotationKeyAccess().getAnnotationKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:729:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:730:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:730:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:731:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAnnotationKeyAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAnnotationKeyRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotationKey"


    // $ANTLR start "entryRuleAssetType"
    // InternalAssetBasedSystemDsl.g:752:1: entryRuleAssetType returns [EObject current=null] : iv_ruleAssetType= ruleAssetType EOF ;
    public final EObject entryRuleAssetType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetType = null;


        try {
            // InternalAssetBasedSystemDsl.g:752:50: (iv_ruleAssetType= ruleAssetType EOF )
            // InternalAssetBasedSystemDsl.g:753:2: iv_ruleAssetType= ruleAssetType EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetType=ruleAssetType();

            state._fsp--;

             current =iv_ruleAssetType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetType"


    // $ANTLR start "ruleAssetType"
    // InternalAssetBasedSystemDsl.g:759:1: ruleAssetType returns [EObject current=null] : ( () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}' ) ;
    public final EObject ruleAssetType() throws RecognitionException {
        EObject current = null;

        Token lv_abstract_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token lv_description_22_0=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_29=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;

        AntlrDatatypeRuleToken lv_level_13_0 = null;

        Enumerator lv_descriptionFormat_21_0 = null;

        EObject lv_annotations_25_0 = null;

        EObject lv_assetTypeProperties_27_0 = null;

        EObject lv_assetTypeAttributes_28_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:765:2: ( ( () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}' ) )
            // InternalAssetBasedSystemDsl.g:766:2: ( () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}' )
            {
            // InternalAssetBasedSystemDsl.g:766:2: ( () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}' )
            // InternalAssetBasedSystemDsl.g:767:3: () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}'
            {
            // InternalAssetBasedSystemDsl.g:767:3: ()
            // InternalAssetBasedSystemDsl.g:768:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeAccess().getAssetTypeAction_0(),
            					current);
            			

            }

            // InternalAssetBasedSystemDsl.g:774:3: ( (lv_abstract_1_0= 'abstract' ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==22) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:775:4: (lv_abstract_1_0= 'abstract' )
                    {
                    // InternalAssetBasedSystemDsl.g:775:4: (lv_abstract_1_0= 'abstract' )
                    // InternalAssetBasedSystemDsl.g:776:5: lv_abstract_1_0= 'abstract'
                    {
                    lv_abstract_1_0=(Token)match(input,22,FOLLOW_17); 

                    					newLeafNode(lv_abstract_1_0, grammarAccess.getAssetTypeAccess().getAbstractAbstractKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAssetTypeRule());
                    					}
                    					setWithLastConsumed(current, "abstract", lv_abstract_1_0 != null, "abstract");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,23,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getAssetTypeAccess().getAssetTypeKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:792:3: ( (lv_name_3_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:793:4: (lv_name_3_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:793:4: (lv_name_3_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:794:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetTypeAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_18);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAssetBasedSystemDsl.g:811:3: (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==24) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:812:4: otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_9); 

                    				newLeafNode(otherlv_4, grammarAccess.getAssetTypeAccess().getExtendsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,15,FOLLOW_10); 

                    				newLeafNode(otherlv_5, grammarAccess.getAssetTypeAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalAssetBasedSystemDsl.g:820:4: ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )?
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==RULE_ID) ) {
                        alt18=1;
                    }
                    switch (alt18) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:821:5: ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )*
                            {
                            // InternalAssetBasedSystemDsl.g:821:5: ( ( ruleQualifiedName ) )
                            // InternalAssetBasedSystemDsl.g:822:6: ( ruleQualifiedName )
                            {
                            // InternalAssetBasedSystemDsl.g:822:6: ( ruleQualifiedName )
                            // InternalAssetBasedSystemDsl.g:823:7: ruleQualifiedName
                            {

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAssetTypeRule());
                            							}
                            						

                            							newCompositeNode(grammarAccess.getAssetTypeAccess().getExtendsAssetTypeCrossReference_4_2_0_0());
                            						
                            pushFollow(FOLLOW_11);
                            ruleQualifiedName();

                            state._fsp--;


                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            // InternalAssetBasedSystemDsl.g:837:5: (otherlv_7= ',' ( ( ruleQualifiedName ) ) )*
                            loop17:
                            do {
                                int alt17=2;
                                int LA17_0 = input.LA(1);

                                if ( (LA17_0==16) ) {
                                    alt17=1;
                                }


                                switch (alt17) {
                            	case 1 :
                            	    // InternalAssetBasedSystemDsl.g:838:6: otherlv_7= ',' ( ( ruleQualifiedName ) )
                            	    {
                            	    otherlv_7=(Token)match(input,16,FOLLOW_12); 

                            	    						newLeafNode(otherlv_7, grammarAccess.getAssetTypeAccess().getCommaKeyword_4_2_1_0());
                            	    					
                            	    // InternalAssetBasedSystemDsl.g:842:6: ( ( ruleQualifiedName ) )
                            	    // InternalAssetBasedSystemDsl.g:843:7: ( ruleQualifiedName )
                            	    {
                            	    // InternalAssetBasedSystemDsl.g:843:7: ( ruleQualifiedName )
                            	    // InternalAssetBasedSystemDsl.g:844:8: ruleQualifiedName
                            	    {

                            	    								if (current==null) {
                            	    									current = createModelElement(grammarAccess.getAssetTypeRule());
                            	    								}
                            	    							

                            	    								newCompositeNode(grammarAccess.getAssetTypeAccess().getExtendsAssetTypeCrossReference_4_2_1_1_0());
                            	    							
                            	    pushFollow(FOLLOW_11);
                            	    ruleQualifiedName();

                            	    state._fsp--;


                            	    								afterParserOrEnumRuleCall();
                            	    							

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop17;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_9=(Token)match(input,17,FOLLOW_7); 

                    				newLeafNode(otherlv_9, grammarAccess.getAssetTypeAccess().getRightParenthesisKeyword_4_3());
                    			

                    }
                    break;

            }

            otherlv_10=(Token)match(input,13,FOLLOW_19); 

            			newLeafNode(otherlv_10, grammarAccess.getAssetTypeAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalAssetBasedSystemDsl.g:869:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) )
            // InternalAssetBasedSystemDsl.g:870:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) )
            {
            // InternalAssetBasedSystemDsl.g:870:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) )
            // InternalAssetBasedSystemDsl.g:871:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            				
            // InternalAssetBasedSystemDsl.g:874:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* )
            // InternalAssetBasedSystemDsl.g:875:6: ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )*
            {
            // InternalAssetBasedSystemDsl.g:875:6: ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )*
            loop24:
            do {
                int alt24=5;
                int LA24_0 = input.LA(1);

                if ( LA24_0 == 25 && getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 0) ) {
                    alt24=1;
                }
                else if ( LA24_0 == 14 && getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 1) ) {
                    alt24=2;
                }
                else if ( LA24_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 2) ) {
                    alt24=3;
                }
                else if ( LA24_0 == 18 && getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 3) ) {
                    alt24=4;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:876:4: ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:876:4: ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:877:5: {...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 0)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:877:106: ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:878:6: ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 0);
            	    					
            	    // InternalAssetBasedSystemDsl.g:881:9: ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:881:10: {...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:881:19: (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) )
            	    // InternalAssetBasedSystemDsl.g:881:20: otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) )
            	    {
            	    otherlv_12=(Token)match(input,25,FOLLOW_6); 

            	    									newLeafNode(otherlv_12, grammarAccess.getAssetTypeAccess().getLevelKeyword_6_0_0());
            	    								
            	    // InternalAssetBasedSystemDsl.g:885:9: ( (lv_level_13_0= ruleEString ) )
            	    // InternalAssetBasedSystemDsl.g:886:10: (lv_level_13_0= ruleEString )
            	    {
            	    // InternalAssetBasedSystemDsl.g:886:10: (lv_level_13_0= ruleEString )
            	    // InternalAssetBasedSystemDsl.g:887:11: lv_level_13_0= ruleEString
            	    {

            	    											newCompositeNode(grammarAccess.getAssetTypeAccess().getLevelEStringParserRuleCall_6_0_1_0());
            	    										
            	    pushFollow(FOLLOW_19);
            	    lv_level_13_0=ruleEString();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	    											}
            	    											set(
            	    												current,
            	    												"level",
            	    												lv_level_13_0,
            	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalAssetBasedSystemDsl.g:910:4: ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:910:4: ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:911:5: {...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 1)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:911:106: ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) )
            	    // InternalAssetBasedSystemDsl.g:912:6: ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 1);
            	    					
            	    // InternalAssetBasedSystemDsl.g:915:9: ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) )
            	    // InternalAssetBasedSystemDsl.g:915:10: {...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:915:19: (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' )
            	    // InternalAssetBasedSystemDsl.g:915:20: otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')'
            	    {
            	    otherlv_14=(Token)match(input,14,FOLLOW_9); 

            	    									newLeafNode(otherlv_14, grammarAccess.getAssetTypeAccess().getTagsKeyword_6_1_0());
            	    								
            	    otherlv_15=(Token)match(input,15,FOLLOW_10); 

            	    									newLeafNode(otherlv_15, grammarAccess.getAssetTypeAccess().getLeftParenthesisKeyword_6_1_1());
            	    								
            	    // InternalAssetBasedSystemDsl.g:923:9: ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )?
            	    int alt21=2;
            	    int LA21_0 = input.LA(1);

            	    if ( (LA21_0==RULE_ID) ) {
            	        alt21=1;
            	    }
            	    switch (alt21) {
            	        case 1 :
            	            // InternalAssetBasedSystemDsl.g:924:10: ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )*
            	            {
            	            // InternalAssetBasedSystemDsl.g:924:10: ( ( ruleQualifiedName ) )
            	            // InternalAssetBasedSystemDsl.g:925:11: ( ruleQualifiedName )
            	            {
            	            // InternalAssetBasedSystemDsl.g:925:11: ( ruleQualifiedName )
            	            // InternalAssetBasedSystemDsl.g:926:12: ruleQualifiedName
            	            {

            	            												if (current==null) {
            	            													current = createModelElement(grammarAccess.getAssetTypeRule());
            	            												}
            	            											

            	            												newCompositeNode(grammarAccess.getAssetTypeAccess().getTagsTagCrossReference_6_1_2_0_0());
            	            											
            	            pushFollow(FOLLOW_11);
            	            ruleQualifiedName();

            	            state._fsp--;


            	            												afterParserOrEnumRuleCall();
            	            											

            	            }


            	            }

            	            // InternalAssetBasedSystemDsl.g:940:10: (otherlv_17= ',' ( ( ruleQualifiedName ) ) )*
            	            loop20:
            	            do {
            	                int alt20=2;
            	                int LA20_0 = input.LA(1);

            	                if ( (LA20_0==16) ) {
            	                    alt20=1;
            	                }


            	                switch (alt20) {
            	            	case 1 :
            	            	    // InternalAssetBasedSystemDsl.g:941:11: otherlv_17= ',' ( ( ruleQualifiedName ) )
            	            	    {
            	            	    otherlv_17=(Token)match(input,16,FOLLOW_12); 

            	            	    											newLeafNode(otherlv_17, grammarAccess.getAssetTypeAccess().getCommaKeyword_6_1_2_1_0());
            	            	    										
            	            	    // InternalAssetBasedSystemDsl.g:945:11: ( ( ruleQualifiedName ) )
            	            	    // InternalAssetBasedSystemDsl.g:946:12: ( ruleQualifiedName )
            	            	    {
            	            	    // InternalAssetBasedSystemDsl.g:946:12: ( ruleQualifiedName )
            	            	    // InternalAssetBasedSystemDsl.g:947:13: ruleQualifiedName
            	            	    {

            	            	    													if (current==null) {
            	            	    														current = createModelElement(grammarAccess.getAssetTypeRule());
            	            	    													}
            	            	    												

            	            	    													newCompositeNode(grammarAccess.getAssetTypeAccess().getTagsTagCrossReference_6_1_2_1_1_0());
            	            	    												
            	            	    pushFollow(FOLLOW_11);
            	            	    ruleQualifiedName();

            	            	    state._fsp--;


            	            	    													afterParserOrEnumRuleCall();
            	            	    												

            	            	    }


            	            	    }


            	            	    }
            	            	    break;

            	            	default :
            	            	    break loop20;
            	                }
            	            } while (true);


            	            }
            	            break;

            	    }

            	    otherlv_19=(Token)match(input,17,FOLLOW_19); 

            	    									newLeafNode(otherlv_19, grammarAccess.getAssetTypeAccess().getRightParenthesisKeyword_6_1_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalAssetBasedSystemDsl.g:973:4: ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:973:4: ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:974:5: {...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 2)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:974:106: ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:975:6: ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 2);
            	    					
            	    // InternalAssetBasedSystemDsl.g:978:9: ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:978:10: {...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:978:19: (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) )
            	    // InternalAssetBasedSystemDsl.g:978:20: otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) )
            	    {
            	    otherlv_20=(Token)match(input,26,FOLLOW_20); 

            	    									newLeafNode(otherlv_20, grammarAccess.getAssetTypeAccess().getDescriptionKeyword_6_2_0());
            	    								
            	    // InternalAssetBasedSystemDsl.g:982:9: ( (lv_descriptionFormat_21_0= ruleTextFormat ) )?
            	    int alt22=2;
            	    int LA22_0 = input.LA(1);

            	    if ( ((LA22_0>=100 && LA22_0<=102)) ) {
            	        alt22=1;
            	    }
            	    switch (alt22) {
            	        case 1 :
            	            // InternalAssetBasedSystemDsl.g:983:10: (lv_descriptionFormat_21_0= ruleTextFormat )
            	            {
            	            // InternalAssetBasedSystemDsl.g:983:10: (lv_descriptionFormat_21_0= ruleTextFormat )
            	            // InternalAssetBasedSystemDsl.g:984:11: lv_descriptionFormat_21_0= ruleTextFormat
            	            {

            	            											newCompositeNode(grammarAccess.getAssetTypeAccess().getDescriptionFormatTextFormatEnumRuleCall_6_2_1_0());
            	            										
            	            pushFollow(FOLLOW_21);
            	            lv_descriptionFormat_21_0=ruleTextFormat();

            	            state._fsp--;


            	            											if (current==null) {
            	            												current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	            											}
            	            											set(
            	            												current,
            	            												"descriptionFormat",
            	            												lv_descriptionFormat_21_0,
            	            												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
            	            											afterParserOrEnumRuleCall();
            	            										

            	            }


            	            }
            	            break;

            	    }

            	    // InternalAssetBasedSystemDsl.g:1001:9: ( (lv_description_22_0= RULE_STRING ) )
            	    // InternalAssetBasedSystemDsl.g:1002:10: (lv_description_22_0= RULE_STRING )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1002:10: (lv_description_22_0= RULE_STRING )
            	    // InternalAssetBasedSystemDsl.g:1003:11: lv_description_22_0= RULE_STRING
            	    {
            	    lv_description_22_0=(Token)match(input,RULE_STRING,FOLLOW_19); 

            	    											newLeafNode(lv_description_22_0, grammarAccess.getAssetTypeAccess().getDescriptionSTRINGTerminalRuleCall_6_2_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getAssetTypeRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"description",
            	    												lv_description_22_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalAssetBasedSystemDsl.g:1025:4: ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1025:4: ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:1026:5: {...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 3)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:1026:106: ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) )
            	    // InternalAssetBasedSystemDsl.g:1027:6: ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 3);
            	    					
            	    // InternalAssetBasedSystemDsl.g:1030:9: ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) )
            	    // InternalAssetBasedSystemDsl.g:1030:10: {...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:1030:19: (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' )
            	    // InternalAssetBasedSystemDsl.g:1030:20: otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}'
            	    {
            	    otherlv_23=(Token)match(input,18,FOLLOW_7); 

            	    									newLeafNode(otherlv_23, grammarAccess.getAssetTypeAccess().getAnnotationsKeyword_6_3_0());
            	    								
            	    otherlv_24=(Token)match(input,13,FOLLOW_13); 

            	    									newLeafNode(otherlv_24, grammarAccess.getAssetTypeAccess().getLeftCurlyBracketKeyword_6_3_1());
            	    								
            	    // InternalAssetBasedSystemDsl.g:1038:9: ( (lv_annotations_25_0= ruleAnnotationEntry ) )*
            	    loop23:
            	    do {
            	        int alt23=2;
            	        int LA23_0 = input.LA(1);

            	        if ( ((LA23_0>=RULE_STRING && LA23_0<=RULE_ID)) ) {
            	            alt23=1;
            	        }


            	        switch (alt23) {
            	    	case 1 :
            	    	    // InternalAssetBasedSystemDsl.g:1039:10: (lv_annotations_25_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalAssetBasedSystemDsl.g:1039:10: (lv_annotations_25_0= ruleAnnotationEntry )
            	    	    // InternalAssetBasedSystemDsl.g:1040:11: lv_annotations_25_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getAssetTypeAccess().getAnnotationsAnnotationEntryParserRuleCall_6_3_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_13);
            	    	    lv_annotations_25_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_25_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop23;
            	        }
            	    } while (true);

            	    otherlv_26=(Token)match(input,19,FOLLOW_19); 

            	    									newLeafNode(otherlv_26, grammarAccess.getAssetTypeAccess().getRightCurlyBracketKeyword_6_3_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            				

            }

            // InternalAssetBasedSystemDsl.g:1074:3: ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )*
            loop25:
            do {
                int alt25=3;
                int LA25_0 = input.LA(1);

                if ( ((LA25_0>=31 && LA25_0<=32)) ) {
                    alt25=1;
                }
                else if ( (LA25_0==39) ) {
                    alt25=2;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:1075:4: ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1075:4: ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) )
            	    // InternalAssetBasedSystemDsl.g:1076:5: (lv_assetTypeProperties_27_0= ruleAssetTypeReference )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1076:5: (lv_assetTypeProperties_27_0= ruleAssetTypeReference )
            	    // InternalAssetBasedSystemDsl.g:1077:6: lv_assetTypeProperties_27_0= ruleAssetTypeReference
            	    {

            	    						newCompositeNode(grammarAccess.getAssetTypeAccess().getAssetTypePropertiesAssetTypeReferenceParserRuleCall_7_0_0());
            	    					
            	    pushFollow(FOLLOW_22);
            	    lv_assetTypeProperties_27_0=ruleAssetTypeReference();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"assetTypeProperties",
            	    							lv_assetTypeProperties_27_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeReference");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalAssetBasedSystemDsl.g:1095:4: ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1095:4: ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) )
            	    // InternalAssetBasedSystemDsl.g:1096:5: (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1096:5: (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute )
            	    // InternalAssetBasedSystemDsl.g:1097:6: lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute
            	    {

            	    						newCompositeNode(grammarAccess.getAssetTypeAccess().getAssetTypeAttributesAssetTypeAttributeParserRuleCall_7_1_0());
            	    					
            	    pushFollow(FOLLOW_22);
            	    lv_assetTypeAttributes_28_0=ruleAssetTypeAttribute();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"assetTypeAttributes",
            	    							lv_assetTypeAttributes_28_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeAttribute");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            otherlv_29=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_29, grammarAccess.getAssetTypeAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetType"


    // $ANTLR start "entryRuleAssetTypeAspect"
    // InternalAssetBasedSystemDsl.g:1123:1: entryRuleAssetTypeAspect returns [EObject current=null] : iv_ruleAssetTypeAspect= ruleAssetTypeAspect EOF ;
    public final EObject entryRuleAssetTypeAspect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeAspect = null;


        try {
            // InternalAssetBasedSystemDsl.g:1123:56: (iv_ruleAssetTypeAspect= ruleAssetTypeAspect EOF )
            // InternalAssetBasedSystemDsl.g:1124:2: iv_ruleAssetTypeAspect= ruleAssetTypeAspect EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeAspectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeAspect=ruleAssetTypeAspect();

            state._fsp--;

             current =iv_ruleAssetTypeAspect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeAspect"


    // $ANTLR start "ruleAssetTypeAspect"
    // InternalAssetBasedSystemDsl.g:1130:1: ruleAssetTypeAspect returns [EObject current=null] : (otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}' ) ;
    public final EObject ruleAssetTypeAspect() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_assetTypeProperties_3_0 = null;

        EObject lv_assetTypeAttributes_4_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:1136:2: ( (otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}' ) )
            // InternalAssetBasedSystemDsl.g:1137:2: (otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}' )
            {
            // InternalAssetBasedSystemDsl.g:1137:2: (otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}' )
            // InternalAssetBasedSystemDsl.g:1138:3: otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getAssetTypeAspectAccess().getAssetTypeAspectKeyword_0());
            		
            // InternalAssetBasedSystemDsl.g:1142:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:1143:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:1143:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:1144:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeAspectRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeAspectAccess().getBaseAssetTypeAssetTypeCrossReference_1_0());
            				
            pushFollow(FOLLOW_7);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_22); 

            			newLeafNode(otherlv_2, grammarAccess.getAssetTypeAspectAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:1162:3: ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )*
            loop26:
            do {
                int alt26=3;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>=31 && LA26_0<=32)) ) {
                    alt26=1;
                }
                else if ( (LA26_0==39) ) {
                    alt26=2;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:1163:4: ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1163:4: ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) )
            	    // InternalAssetBasedSystemDsl.g:1164:5: (lv_assetTypeProperties_3_0= ruleAssetTypeReference )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1164:5: (lv_assetTypeProperties_3_0= ruleAssetTypeReference )
            	    // InternalAssetBasedSystemDsl.g:1165:6: lv_assetTypeProperties_3_0= ruleAssetTypeReference
            	    {

            	    						newCompositeNode(grammarAccess.getAssetTypeAspectAccess().getAssetTypePropertiesAssetTypeReferenceParserRuleCall_3_0_0());
            	    					
            	    pushFollow(FOLLOW_22);
            	    lv_assetTypeProperties_3_0=ruleAssetTypeReference();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetTypeAspectRule());
            	    						}
            	    						add(
            	    							current,
            	    							"assetTypeProperties",
            	    							lv_assetTypeProperties_3_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeReference");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalAssetBasedSystemDsl.g:1183:4: ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1183:4: ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) )
            	    // InternalAssetBasedSystemDsl.g:1184:5: (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1184:5: (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute )
            	    // InternalAssetBasedSystemDsl.g:1185:6: lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute
            	    {

            	    						newCompositeNode(grammarAccess.getAssetTypeAspectAccess().getAssetTypeAttributesAssetTypeAttributeParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_22);
            	    lv_assetTypeAttributes_4_0=ruleAssetTypeAttribute();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetTypeAspectRule());
            	    						}
            	    						add(
            	    							current,
            	    							"assetTypeAttributes",
            	    							lv_assetTypeAttributes_4_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeAttribute");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

            otherlv_5=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getAssetTypeAspectAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeAspect"


    // $ANTLR start "entryRulePrimitiveDataType_Impl"
    // InternalAssetBasedSystemDsl.g:1211:1: entryRulePrimitiveDataType_Impl returns [EObject current=null] : iv_rulePrimitiveDataType_Impl= rulePrimitiveDataType_Impl EOF ;
    public final EObject entryRulePrimitiveDataType_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveDataType_Impl = null;


        try {
            // InternalAssetBasedSystemDsl.g:1211:63: (iv_rulePrimitiveDataType_Impl= rulePrimitiveDataType_Impl EOF )
            // InternalAssetBasedSystemDsl.g:1212:2: iv_rulePrimitiveDataType_Impl= rulePrimitiveDataType_Impl EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveDataType_ImplRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimitiveDataType_Impl=rulePrimitiveDataType_Impl();

            state._fsp--;

             current =iv_rulePrimitiveDataType_Impl; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveDataType_Impl"


    // $ANTLR start "rulePrimitiveDataType_Impl"
    // InternalAssetBasedSystemDsl.g:1218:1: rulePrimitiveDataType_Impl returns [EObject current=null] : ( () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject rulePrimitiveDataType_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:1224:2: ( ( () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalAssetBasedSystemDsl.g:1225:2: ( () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalAssetBasedSystemDsl.g:1225:2: ( () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) ) )
            // InternalAssetBasedSystemDsl.g:1226:3: () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalAssetBasedSystemDsl.g:1226:3: ()
            // InternalAssetBasedSystemDsl.g:1227:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPrimitiveDataType_ImplAccess().getPrimitiveDataTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,28,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getPrimitiveDataType_ImplAccess().getPrimitiveDataTypeKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:1237:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:1238:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:1238:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:1239:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPrimitiveDataType_ImplAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPrimitiveDataType_ImplRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveDataType_Impl"


    // $ANTLR start "entryRuleStaticMethod"
    // InternalAssetBasedSystemDsl.g:1260:1: entryRuleStaticMethod returns [EObject current=null] : iv_ruleStaticMethod= ruleStaticMethod EOF ;
    public final EObject entryRuleStaticMethod() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStaticMethod = null;


        try {
            // InternalAssetBasedSystemDsl.g:1260:53: (iv_ruleStaticMethod= ruleStaticMethod EOF )
            // InternalAssetBasedSystemDsl.g:1261:2: iv_ruleStaticMethod= ruleStaticMethod EOF
            {
             newCompositeNode(grammarAccess.getStaticMethodRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStaticMethod=ruleStaticMethod();

            state._fsp--;

             current =iv_ruleStaticMethod; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStaticMethod"


    // $ANTLR start "ruleStaticMethod"
    // InternalAssetBasedSystemDsl.g:1267:1: ruleStaticMethod returns [EObject current=null] : ( () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleStaticMethod() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:1273:2: ( ( () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalAssetBasedSystemDsl.g:1274:2: ( () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalAssetBasedSystemDsl.g:1274:2: ( () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) ) )
            // InternalAssetBasedSystemDsl.g:1275:3: () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalAssetBasedSystemDsl.g:1275:3: ()
            // InternalAssetBasedSystemDsl.g:1276:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStaticMethodAccess().getStaticMethodAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,29,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getStaticMethodAccess().getStaticMethodKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:1286:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:1287:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:1287:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:1288:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStaticMethodAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStaticMethodRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStaticMethod"


    // $ANTLR start "entryRuleLambdaParameter"
    // InternalAssetBasedSystemDsl.g:1309:1: entryRuleLambdaParameter returns [EObject current=null] : iv_ruleLambdaParameter= ruleLambdaParameter EOF ;
    public final EObject entryRuleLambdaParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLambdaParameter = null;


        try {
            // InternalAssetBasedSystemDsl.g:1309:56: (iv_ruleLambdaParameter= ruleLambdaParameter EOF )
            // InternalAssetBasedSystemDsl.g:1310:2: iv_ruleLambdaParameter= ruleLambdaParameter EOF
            {
             newCompositeNode(grammarAccess.getLambdaParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLambdaParameter=ruleLambdaParameter();

            state._fsp--;

             current =iv_ruleLambdaParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLambdaParameter"


    // $ANTLR start "ruleLambdaParameter"
    // InternalAssetBasedSystemDsl.g:1316:1: ruleLambdaParameter returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ) ;
    public final EObject ruleLambdaParameter() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:1322:2: ( ( () ( (lv_name_1_0= ruleEString ) ) ) )
            // InternalAssetBasedSystemDsl.g:1323:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            {
            // InternalAssetBasedSystemDsl.g:1323:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            // InternalAssetBasedSystemDsl.g:1324:3: () ( (lv_name_1_0= ruleEString ) )
            {
            // InternalAssetBasedSystemDsl.g:1324:3: ()
            // InternalAssetBasedSystemDsl.g:1325:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLambdaParameterAccess().getLambdaParameterAction_0(),
            					current);
            			

            }

            // InternalAssetBasedSystemDsl.g:1331:3: ( (lv_name_1_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:1332:4: (lv_name_1_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:1332:4: (lv_name_1_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:1333:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getLambdaParameterAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaParameterRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLambdaParameter"


    // $ANTLR start "entryRuleTag"
    // InternalAssetBasedSystemDsl.g:1354:1: entryRuleTag returns [EObject current=null] : iv_ruleTag= ruleTag EOF ;
    public final EObject entryRuleTag() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTag = null;


        try {
            // InternalAssetBasedSystemDsl.g:1354:44: (iv_ruleTag= ruleTag EOF )
            // InternalAssetBasedSystemDsl.g:1355:2: iv_ruleTag= ruleTag EOF
            {
             newCompositeNode(grammarAccess.getTagRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTag=ruleTag();

            state._fsp--;

             current =iv_ruleTag; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTag"


    // $ANTLR start "ruleTag"
    // InternalAssetBasedSystemDsl.g:1361:1: ruleTag returns [EObject current=null] : ( () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleTag() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:1367:2: ( ( () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalAssetBasedSystemDsl.g:1368:2: ( () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalAssetBasedSystemDsl.g:1368:2: ( () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) ) )
            // InternalAssetBasedSystemDsl.g:1369:3: () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalAssetBasedSystemDsl.g:1369:3: ()
            // InternalAssetBasedSystemDsl.g:1370:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTagAccess().getTagAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,30,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getTagAccess().getTagKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:1380:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:1381:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:1381:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:1382:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getTagAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTagRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTag"


    // $ANTLR start "entryRuleAssetTypeReference"
    // InternalAssetBasedSystemDsl.g:1403:1: entryRuleAssetTypeReference returns [EObject current=null] : iv_ruleAssetTypeReference= ruleAssetTypeReference EOF ;
    public final EObject entryRuleAssetTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeReference = null;


        try {
            // InternalAssetBasedSystemDsl.g:1403:59: (iv_ruleAssetTypeReference= ruleAssetTypeReference EOF )
            // InternalAssetBasedSystemDsl.g:1404:2: iv_ruleAssetTypeReference= ruleAssetTypeReference EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeReference=ruleAssetTypeReference();

            state._fsp--;

             current =iv_ruleAssetTypeReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeReference"


    // $ANTLR start "ruleAssetTypeReference"
    // InternalAssetBasedSystemDsl.g:1410:1: ruleAssetTypeReference returns [EObject current=null] : ( ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) ) ) ;
    public final EObject ruleAssetTypeReference() throws RecognitionException {
        EObject current = null;

        Token lv_isContainer_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_hasDefault_8_0=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token lv_hasDefault_13_0=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        Enumerator lv_multiplicity_5_0 = null;

        Enumerator lv_multiplicity_10_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:1416:2: ( ( ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) ) ) )
            // InternalAssetBasedSystemDsl.g:1417:2: ( ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) ) )
            {
            // InternalAssetBasedSystemDsl.g:1417:2: ( ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) ) )
            // InternalAssetBasedSystemDsl.g:1418:3: ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) )
            {
            // InternalAssetBasedSystemDsl.g:1418:3: ( (lv_isContainer_0_0= 'container' ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==31) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:1419:4: (lv_isContainer_0_0= 'container' )
                    {
                    // InternalAssetBasedSystemDsl.g:1419:4: (lv_isContainer_0_0= 'container' )
                    // InternalAssetBasedSystemDsl.g:1420:5: lv_isContainer_0_0= 'container'
                    {
                    lv_isContainer_0_0=(Token)match(input,31,FOLLOW_23); 

                    					newLeafNode(lv_isContainer_0_0, grammarAccess.getAssetTypeReferenceAccess().getIsContainerContainerKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                    					}
                    					setWithLastConsumed(current, "isContainer", lv_isContainer_0_0 != null, "container");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,32,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetTypeReferenceAccess().getReferenceKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:1436:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:1437:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:1437:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:1438:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetTypeReferenceAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_24);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetTypeReferenceRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,33,FOLLOW_12); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetTypeReferenceAccess().getColonKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:1459:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:1460:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:1460:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:1461:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeReferenceAccess().getPropertyTypeAssetTypeCrossReference_4_0());
            				
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAssetBasedSystemDsl.g:1475:3: ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==EOF||LA33_0==19||(LA33_0>=31 && LA33_0<=32)||(LA33_0>=34 && LA33_0<=35)||LA33_0==39||(LA33_0>=87 && LA33_0<=88)) ) {
                alt33=1;
            }
            else if ( ((LA33_0>=89 && LA33_0<=90)) ) {
                alt33=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:1476:4: ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? )
                    {
                    // InternalAssetBasedSystemDsl.g:1476:4: ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? )
                    // InternalAssetBasedSystemDsl.g:1477:5: ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )?
                    {
                    // InternalAssetBasedSystemDsl.g:1477:5: ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )?
                    int alt28=2;
                    int LA28_0 = input.LA(1);

                    if ( ((LA28_0>=87 && LA28_0<=88)) ) {
                        alt28=1;
                    }
                    switch (alt28) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:1478:6: (lv_multiplicity_5_0= ruleMultiplicitySingle )
                            {
                            // InternalAssetBasedSystemDsl.g:1478:6: (lv_multiplicity_5_0= ruleMultiplicitySingle )
                            // InternalAssetBasedSystemDsl.g:1479:7: lv_multiplicity_5_0= ruleMultiplicitySingle
                            {

                            							newCompositeNode(grammarAccess.getAssetTypeReferenceAccess().getMultiplicityMultiplicitySingleEnumRuleCall_5_0_0_0());
                            						
                            pushFollow(FOLLOW_26);
                            lv_multiplicity_5_0=ruleMultiplicitySingle();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getAssetTypeReferenceRule());
                            							}
                            							set(
                            								current,
                            								"multiplicity",
                            								lv_multiplicity_5_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.MultiplicitySingle");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:1496:5: (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )?
                    int alt29=2;
                    int LA29_0 = input.LA(1);

                    if ( (LA29_0==34) ) {
                        alt29=1;
                    }
                    switch (alt29) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:1497:6: otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) )
                            {
                            otherlv_6=(Token)match(input,34,FOLLOW_12); 

                            						newLeafNode(otherlv_6, grammarAccess.getAssetTypeReferenceAccess().getOppositeKeyword_5_0_1_0());
                            					
                            // InternalAssetBasedSystemDsl.g:1501:6: ( (otherlv_7= RULE_ID ) )
                            // InternalAssetBasedSystemDsl.g:1502:7: (otherlv_7= RULE_ID )
                            {
                            // InternalAssetBasedSystemDsl.g:1502:7: (otherlv_7= RULE_ID )
                            // InternalAssetBasedSystemDsl.g:1503:8: otherlv_7= RULE_ID
                            {

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                            								}
                            							
                            otherlv_7=(Token)match(input,RULE_ID,FOLLOW_27); 

                            								newLeafNode(otherlv_7, grammarAccess.getAssetTypeReferenceAccess().getOppositeTypeReferenceAssetTypeReferenceCrossReference_5_0_1_1_0());
                            							

                            }


                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:1515:5: ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )?
                    int alt30=2;
                    int LA30_0 = input.LA(1);

                    if ( (LA30_0==35) ) {
                        alt30=1;
                    }
                    switch (alt30) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:1516:6: ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined'
                            {
                            // InternalAssetBasedSystemDsl.g:1516:6: ( (lv_hasDefault_8_0= 'default' ) )
                            // InternalAssetBasedSystemDsl.g:1517:7: (lv_hasDefault_8_0= 'default' )
                            {
                            // InternalAssetBasedSystemDsl.g:1517:7: (lv_hasDefault_8_0= 'default' )
                            // InternalAssetBasedSystemDsl.g:1518:8: lv_hasDefault_8_0= 'default'
                            {
                            lv_hasDefault_8_0=(Token)match(input,35,FOLLOW_28); 

                            								newLeafNode(lv_hasDefault_8_0, grammarAccess.getAssetTypeReferenceAccess().getHasDefaultDefaultKeyword_5_0_2_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                            								}
                            								setWithLastConsumed(current, "hasDefault", lv_hasDefault_8_0 != null, "default");
                            							

                            }


                            }

                            otherlv_9=(Token)match(input,36,FOLLOW_2); 

                            						newLeafNode(otherlv_9, grammarAccess.getAssetTypeReferenceAccess().getUndefinedKeyword_5_0_2_1());
                            					

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:1537:4: ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? )
                    {
                    // InternalAssetBasedSystemDsl.g:1537:4: ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? )
                    // InternalAssetBasedSystemDsl.g:1538:5: ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )?
                    {
                    // InternalAssetBasedSystemDsl.g:1538:5: ( (lv_multiplicity_10_0= ruleMultiplicityMany ) )
                    // InternalAssetBasedSystemDsl.g:1539:6: (lv_multiplicity_10_0= ruleMultiplicityMany )
                    {
                    // InternalAssetBasedSystemDsl.g:1539:6: (lv_multiplicity_10_0= ruleMultiplicityMany )
                    // InternalAssetBasedSystemDsl.g:1540:7: lv_multiplicity_10_0= ruleMultiplicityMany
                    {

                    							newCompositeNode(grammarAccess.getAssetTypeReferenceAccess().getMultiplicityMultiplicityManyEnumRuleCall_5_1_0_0());
                    						
                    pushFollow(FOLLOW_26);
                    lv_multiplicity_10_0=ruleMultiplicityMany();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getAssetTypeReferenceRule());
                    							}
                    							set(
                    								current,
                    								"multiplicity",
                    								lv_multiplicity_10_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.MultiplicityMany");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:1557:5: (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )?
                    int alt31=2;
                    int LA31_0 = input.LA(1);

                    if ( (LA31_0==34) ) {
                        alt31=1;
                    }
                    switch (alt31) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:1558:6: otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) )
                            {
                            otherlv_11=(Token)match(input,34,FOLLOW_12); 

                            						newLeafNode(otherlv_11, grammarAccess.getAssetTypeReferenceAccess().getOppositeKeyword_5_1_1_0());
                            					
                            // InternalAssetBasedSystemDsl.g:1562:6: ( (otherlv_12= RULE_ID ) )
                            // InternalAssetBasedSystemDsl.g:1563:7: (otherlv_12= RULE_ID )
                            {
                            // InternalAssetBasedSystemDsl.g:1563:7: (otherlv_12= RULE_ID )
                            // InternalAssetBasedSystemDsl.g:1564:8: otherlv_12= RULE_ID
                            {

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                            								}
                            							
                            otherlv_12=(Token)match(input,RULE_ID,FOLLOW_27); 

                            								newLeafNode(otherlv_12, grammarAccess.getAssetTypeReferenceAccess().getOppositeTypeReferenceAssetTypeReferenceCrossReference_5_1_1_1_0());
                            							

                            }


                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:1576:5: ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )?
                    int alt32=2;
                    int LA32_0 = input.LA(1);

                    if ( (LA32_0==35) ) {
                        alt32=1;
                    }
                    switch (alt32) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:1577:6: ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']'
                            {
                            // InternalAssetBasedSystemDsl.g:1577:6: ( (lv_hasDefault_13_0= 'default' ) )
                            // InternalAssetBasedSystemDsl.g:1578:7: (lv_hasDefault_13_0= 'default' )
                            {
                            // InternalAssetBasedSystemDsl.g:1578:7: (lv_hasDefault_13_0= 'default' )
                            // InternalAssetBasedSystemDsl.g:1579:8: lv_hasDefault_13_0= 'default'
                            {
                            lv_hasDefault_13_0=(Token)match(input,35,FOLLOW_29); 

                            								newLeafNode(lv_hasDefault_13_0, grammarAccess.getAssetTypeReferenceAccess().getHasDefaultDefaultKeyword_5_1_2_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                            								}
                            								setWithLastConsumed(current, "hasDefault", lv_hasDefault_13_0 != null, "default");
                            							

                            }


                            }

                            otherlv_14=(Token)match(input,37,FOLLOW_30); 

                            						newLeafNode(otherlv_14, grammarAccess.getAssetTypeReferenceAccess().getLeftSquareBracketKeyword_5_1_2_1());
                            					
                            otherlv_15=(Token)match(input,38,FOLLOW_2); 

                            						newLeafNode(otherlv_15, grammarAccess.getAssetTypeReferenceAccess().getRightSquareBracketKeyword_5_1_2_2());
                            					

                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeReference"


    // $ANTLR start "entryRuleAssetTypeAttribute"
    // InternalAssetBasedSystemDsl.g:1606:1: entryRuleAssetTypeAttribute returns [EObject current=null] : iv_ruleAssetTypeAttribute= ruleAssetTypeAttribute EOF ;
    public final EObject entryRuleAssetTypeAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeAttribute = null;


        try {
            // InternalAssetBasedSystemDsl.g:1606:59: (iv_ruleAssetTypeAttribute= ruleAssetTypeAttribute EOF )
            // InternalAssetBasedSystemDsl.g:1607:2: iv_ruleAssetTypeAttribute= ruleAssetTypeAttribute EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeAttribute=ruleAssetTypeAttribute();

            state._fsp--;

             current =iv_ruleAssetTypeAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeAttribute"


    // $ANTLR start "ruleAssetTypeAttribute"
    // InternalAssetBasedSystemDsl.g:1613:1: ruleAssetTypeAttribute returns [EObject current=null] : ( () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) ) ) ;
    public final EObject ruleAssetTypeAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_hasDefault_6_0=null;
        Token lv_hasDefault_9_0=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        Enumerator lv_multiplicity_5_0 = null;

        EObject lv_defaultValues_7_0 = null;

        Enumerator lv_multiplicity_8_0 = null;

        EObject lv_defaultValues_11_0 = null;

        EObject lv_defaultValues_13_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:1619:2: ( ( () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) ) ) )
            // InternalAssetBasedSystemDsl.g:1620:2: ( () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) ) )
            {
            // InternalAssetBasedSystemDsl.g:1620:2: ( () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) ) )
            // InternalAssetBasedSystemDsl.g:1621:3: () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) )
            {
            // InternalAssetBasedSystemDsl.g:1621:3: ()
            // InternalAssetBasedSystemDsl.g:1622:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeAttributeAccess().getAssetTypeAttributeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,39,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetTypeAttributeAccess().getAttributeKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:1632:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:1633:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:1633:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:1634:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_24);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,33,FOLLOW_12); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetTypeAttributeAccess().getColonKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:1655:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:1656:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:1656:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:1657:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeAttributeRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getAttributeTypePrimitiveDataTypeCrossReference_4_0());
            				
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAssetBasedSystemDsl.g:1671:3: ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) )
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==EOF||LA39_0==19||(LA39_0>=31 && LA39_0<=32)||LA39_0==35||LA39_0==39||(LA39_0>=87 && LA39_0<=88)) ) {
                alt39=1;
            }
            else if ( ((LA39_0>=89 && LA39_0<=90)) ) {
                alt39=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;
            }
            switch (alt39) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:1672:4: ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? )
                    {
                    // InternalAssetBasedSystemDsl.g:1672:4: ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? )
                    // InternalAssetBasedSystemDsl.g:1673:5: ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )?
                    {
                    // InternalAssetBasedSystemDsl.g:1673:5: ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )?
                    int alt34=2;
                    int LA34_0 = input.LA(1);

                    if ( ((LA34_0>=87 && LA34_0<=88)) ) {
                        alt34=1;
                    }
                    switch (alt34) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:1674:6: (lv_multiplicity_5_0= ruleMultiplicitySingle )
                            {
                            // InternalAssetBasedSystemDsl.g:1674:6: (lv_multiplicity_5_0= ruleMultiplicitySingle )
                            // InternalAssetBasedSystemDsl.g:1675:7: lv_multiplicity_5_0= ruleMultiplicitySingle
                            {

                            							newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getMultiplicityMultiplicitySingleEnumRuleCall_5_0_0_0());
                            						
                            pushFollow(FOLLOW_27);
                            lv_multiplicity_5_0=ruleMultiplicitySingle();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                            							}
                            							set(
                            								current,
                            								"multiplicity",
                            								lv_multiplicity_5_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.MultiplicitySingle");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:1692:5: ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )?
                    int alt35=2;
                    int LA35_0 = input.LA(1);

                    if ( (LA35_0==35) ) {
                        alt35=1;
                    }
                    switch (alt35) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:1693:6: ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) )
                            {
                            // InternalAssetBasedSystemDsl.g:1693:6: ( (lv_hasDefault_6_0= 'default' ) )
                            // InternalAssetBasedSystemDsl.g:1694:7: (lv_hasDefault_6_0= 'default' )
                            {
                            // InternalAssetBasedSystemDsl.g:1694:7: (lv_hasDefault_6_0= 'default' )
                            // InternalAssetBasedSystemDsl.g:1695:8: lv_hasDefault_6_0= 'default'
                            {
                            lv_hasDefault_6_0=(Token)match(input,35,FOLLOW_31); 

                            								newLeafNode(lv_hasDefault_6_0, grammarAccess.getAssetTypeAttributeAccess().getHasDefaultDefaultKeyword_5_0_1_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeAttributeRule());
                            								}
                            								setWithLastConsumed(current, "hasDefault", lv_hasDefault_6_0 != null, "default");
                            							

                            }


                            }

                            // InternalAssetBasedSystemDsl.g:1707:6: ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) )
                            // InternalAssetBasedSystemDsl.g:1708:7: (lv_defaultValues_7_0= ruleAttributeConstantExpression )
                            {
                            // InternalAssetBasedSystemDsl.g:1708:7: (lv_defaultValues_7_0= ruleAttributeConstantExpression )
                            // InternalAssetBasedSystemDsl.g:1709:8: lv_defaultValues_7_0= ruleAttributeConstantExpression
                            {

                            								newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getDefaultValuesAttributeConstantExpressionParserRuleCall_5_0_1_1_0());
                            							
                            pushFollow(FOLLOW_2);
                            lv_defaultValues_7_0=ruleAttributeConstantExpression();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                            								}
                            								add(
                            									current,
                            									"defaultValues",
                            									lv_defaultValues_7_0,
                            									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                            								afterParserOrEnumRuleCall();
                            							

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:1729:4: ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? )
                    {
                    // InternalAssetBasedSystemDsl.g:1729:4: ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? )
                    // InternalAssetBasedSystemDsl.g:1730:5: ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )?
                    {
                    // InternalAssetBasedSystemDsl.g:1730:5: ( (lv_multiplicity_8_0= ruleMultiplicityMany ) )
                    // InternalAssetBasedSystemDsl.g:1731:6: (lv_multiplicity_8_0= ruleMultiplicityMany )
                    {
                    // InternalAssetBasedSystemDsl.g:1731:6: (lv_multiplicity_8_0= ruleMultiplicityMany )
                    // InternalAssetBasedSystemDsl.g:1732:7: lv_multiplicity_8_0= ruleMultiplicityMany
                    {

                    							newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getMultiplicityMultiplicityManyEnumRuleCall_5_1_0_0());
                    						
                    pushFollow(FOLLOW_27);
                    lv_multiplicity_8_0=ruleMultiplicityMany();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                    							}
                    							set(
                    								current,
                    								"multiplicity",
                    								lv_multiplicity_8_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.MultiplicityMany");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:1749:5: ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )?
                    int alt38=2;
                    int LA38_0 = input.LA(1);

                    if ( (LA38_0==35) ) {
                        alt38=1;
                    }
                    switch (alt38) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:1750:6: ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']'
                            {
                            // InternalAssetBasedSystemDsl.g:1750:6: ( (lv_hasDefault_9_0= 'default' ) )
                            // InternalAssetBasedSystemDsl.g:1751:7: (lv_hasDefault_9_0= 'default' )
                            {
                            // InternalAssetBasedSystemDsl.g:1751:7: (lv_hasDefault_9_0= 'default' )
                            // InternalAssetBasedSystemDsl.g:1752:8: lv_hasDefault_9_0= 'default'
                            {
                            lv_hasDefault_9_0=(Token)match(input,35,FOLLOW_29); 

                            								newLeafNode(lv_hasDefault_9_0, grammarAccess.getAssetTypeAttributeAccess().getHasDefaultDefaultKeyword_5_1_1_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeAttributeRule());
                            								}
                            								setWithLastConsumed(current, "hasDefault", lv_hasDefault_9_0 != null, "default");
                            							

                            }


                            }

                            otherlv_10=(Token)match(input,37,FOLLOW_32); 

                            						newLeafNode(otherlv_10, grammarAccess.getAssetTypeAttributeAccess().getLeftSquareBracketKeyword_5_1_1_1());
                            					
                            // InternalAssetBasedSystemDsl.g:1768:6: ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )?
                            int alt36=2;
                            int LA36_0 = input.LA(1);

                            if ( ((LA36_0>=RULE_STRING && LA36_0<=RULE_INT)||LA36_0==36||(LA36_0>=47 && LA36_0<=48)) ) {
                                alt36=1;
                            }
                            switch (alt36) {
                                case 1 :
                                    // InternalAssetBasedSystemDsl.g:1769:7: (lv_defaultValues_11_0= ruleAttributeConstantExpression )
                                    {
                                    // InternalAssetBasedSystemDsl.g:1769:7: (lv_defaultValues_11_0= ruleAttributeConstantExpression )
                                    // InternalAssetBasedSystemDsl.g:1770:8: lv_defaultValues_11_0= ruleAttributeConstantExpression
                                    {

                                    								newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getDefaultValuesAttributeConstantExpressionParserRuleCall_5_1_1_2_0());
                                    							
                                    pushFollow(FOLLOW_33);
                                    lv_defaultValues_11_0=ruleAttributeConstantExpression();

                                    state._fsp--;


                                    								if (current==null) {
                                    									current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                                    								}
                                    								add(
                                    									current,
                                    									"defaultValues",
                                    									lv_defaultValues_11_0,
                                    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                                    								afterParserOrEnumRuleCall();
                                    							

                                    }


                                    }
                                    break;

                            }

                            // InternalAssetBasedSystemDsl.g:1787:6: (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )*
                            loop37:
                            do {
                                int alt37=2;
                                int LA37_0 = input.LA(1);

                                if ( (LA37_0==16) ) {
                                    alt37=1;
                                }


                                switch (alt37) {
                            	case 1 :
                            	    // InternalAssetBasedSystemDsl.g:1788:7: otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) )
                            	    {
                            	    otherlv_12=(Token)match(input,16,FOLLOW_31); 

                            	    							newLeafNode(otherlv_12, grammarAccess.getAssetTypeAttributeAccess().getCommaKeyword_5_1_1_3_0());
                            	    						
                            	    // InternalAssetBasedSystemDsl.g:1792:7: ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) )
                            	    // InternalAssetBasedSystemDsl.g:1793:8: (lv_defaultValues_13_0= ruleAttributeConstantExpression )
                            	    {
                            	    // InternalAssetBasedSystemDsl.g:1793:8: (lv_defaultValues_13_0= ruleAttributeConstantExpression )
                            	    // InternalAssetBasedSystemDsl.g:1794:9: lv_defaultValues_13_0= ruleAttributeConstantExpression
                            	    {

                            	    									newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getDefaultValuesAttributeConstantExpressionParserRuleCall_5_1_1_3_1_0());
                            	    								
                            	    pushFollow(FOLLOW_33);
                            	    lv_defaultValues_13_0=ruleAttributeConstantExpression();

                            	    state._fsp--;


                            	    									if (current==null) {
                            	    										current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                            	    									}
                            	    									add(
                            	    										current,
                            	    										"defaultValues",
                            	    										lv_defaultValues_13_0,
                            	    										"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                            	    									afterParserOrEnumRuleCall();
                            	    								

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop37;
                                }
                            } while (true);

                            otherlv_14=(Token)match(input,38,FOLLOW_2); 

                            						newLeafNode(otherlv_14, grammarAccess.getAssetTypeAttributeAccess().getRightSquareBracketKeyword_5_1_1_4());
                            					

                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeAttribute"


    // $ANTLR start "entryRuleEnumDataType"
    // InternalAssetBasedSystemDsl.g:1823:1: entryRuleEnumDataType returns [EObject current=null] : iv_ruleEnumDataType= ruleEnumDataType EOF ;
    public final EObject entryRuleEnumDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumDataType = null;


        try {
            // InternalAssetBasedSystemDsl.g:1823:53: (iv_ruleEnumDataType= ruleEnumDataType EOF )
            // InternalAssetBasedSystemDsl.g:1824:2: iv_ruleEnumDataType= ruleEnumDataType EOF
            {
             newCompositeNode(grammarAccess.getEnumDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEnumDataType=ruleEnumDataType();

            state._fsp--;

             current =iv_ruleEnumDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumDataType"


    // $ANTLR start "ruleEnumDataType"
    // InternalAssetBasedSystemDsl.g:1830:1: ruleEnumDataType returns [EObject current=null] : ( () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}' ) ;
    public final EObject ruleEnumDataType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_annotations_6_0 = null;

        EObject lv_enumLiteral_8_0 = null;

        EObject lv_enumLiteral_10_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:1836:2: ( ( () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}' ) )
            // InternalAssetBasedSystemDsl.g:1837:2: ( () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}' )
            {
            // InternalAssetBasedSystemDsl.g:1837:2: ( () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}' )
            // InternalAssetBasedSystemDsl.g:1838:3: () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}'
            {
            // InternalAssetBasedSystemDsl.g:1838:3: ()
            // InternalAssetBasedSystemDsl.g:1839:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEnumDataTypeAccess().getEnumDataTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,40,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getEnumDataTypeAccess().getEnumDataTypeKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:1849:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:1850:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:1850:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:1851:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getEnumDataTypeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_7);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEnumDataTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_34); 

            			newLeafNode(otherlv_3, grammarAccess.getEnumDataTypeAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:1872:3: (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==18) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:1873:4: otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}'
                    {
                    otherlv_4=(Token)match(input,18,FOLLOW_7); 

                    				newLeafNode(otherlv_4, grammarAccess.getEnumDataTypeAccess().getAnnotationsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,13,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getEnumDataTypeAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalAssetBasedSystemDsl.g:1881:4: ( (lv_annotations_6_0= ruleAnnotationEntry ) )*
                    loop40:
                    do {
                        int alt40=2;
                        int LA40_0 = input.LA(1);

                        if ( ((LA40_0>=RULE_STRING && LA40_0<=RULE_ID)) ) {
                            alt40=1;
                        }


                        switch (alt40) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:1882:5: (lv_annotations_6_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:1882:5: (lv_annotations_6_0= ruleAnnotationEntry )
                    	    // InternalAssetBasedSystemDsl.g:1883:6: lv_annotations_6_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getEnumDataTypeAccess().getAnnotationsAnnotationEntryParserRuleCall_4_2_0());
                    	    					
                    	    pushFollow(FOLLOW_13);
                    	    lv_annotations_6_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getEnumDataTypeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_6_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop40;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,19,FOLLOW_34); 

                    				newLeafNode(otherlv_7, grammarAccess.getEnumDataTypeAccess().getRightCurlyBracketKeyword_4_3());
                    			

                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:1905:3: ( (lv_enumLiteral_8_0= ruleEnumLiteral ) )
            // InternalAssetBasedSystemDsl.g:1906:4: (lv_enumLiteral_8_0= ruleEnumLiteral )
            {
            // InternalAssetBasedSystemDsl.g:1906:4: (lv_enumLiteral_8_0= ruleEnumLiteral )
            // InternalAssetBasedSystemDsl.g:1907:5: lv_enumLiteral_8_0= ruleEnumLiteral
            {

            					newCompositeNode(grammarAccess.getEnumDataTypeAccess().getEnumLiteralEnumLiteralParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_35);
            lv_enumLiteral_8_0=ruleEnumLiteral();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEnumDataTypeRule());
            					}
            					add(
            						current,
            						"enumLiteral",
            						lv_enumLiteral_8_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EnumLiteral");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAssetBasedSystemDsl.g:1924:3: (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )*
            loop42:
            do {
                int alt42=2;
                int LA42_0 = input.LA(1);

                if ( (LA42_0==16) ) {
                    alt42=1;
                }


                switch (alt42) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:1925:4: otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) )
            	    {
            	    otherlv_9=(Token)match(input,16,FOLLOW_34); 

            	    				newLeafNode(otherlv_9, grammarAccess.getEnumDataTypeAccess().getCommaKeyword_6_0());
            	    			
            	    // InternalAssetBasedSystemDsl.g:1929:4: ( (lv_enumLiteral_10_0= ruleEnumLiteral ) )
            	    // InternalAssetBasedSystemDsl.g:1930:5: (lv_enumLiteral_10_0= ruleEnumLiteral )
            	    {
            	    // InternalAssetBasedSystemDsl.g:1930:5: (lv_enumLiteral_10_0= ruleEnumLiteral )
            	    // InternalAssetBasedSystemDsl.g:1931:6: lv_enumLiteral_10_0= ruleEnumLiteral
            	    {

            	    						newCompositeNode(grammarAccess.getEnumDataTypeAccess().getEnumLiteralEnumLiteralParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_35);
            	    lv_enumLiteral_10_0=ruleEnumLiteral();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getEnumDataTypeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"enumLiteral",
            	    							lv_enumLiteral_10_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EnumLiteral");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop42;
                }
            } while (true);

            otherlv_11=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getEnumDataTypeAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumDataType"


    // $ANTLR start "entryRuleEnumLiteral"
    // InternalAssetBasedSystemDsl.g:1957:1: entryRuleEnumLiteral returns [EObject current=null] : iv_ruleEnumLiteral= ruleEnumLiteral EOF ;
    public final EObject entryRuleEnumLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumLiteral = null;


        try {
            // InternalAssetBasedSystemDsl.g:1957:52: (iv_ruleEnumLiteral= ruleEnumLiteral EOF )
            // InternalAssetBasedSystemDsl.g:1958:2: iv_ruleEnumLiteral= ruleEnumLiteral EOF
            {
             newCompositeNode(grammarAccess.getEnumLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEnumLiteral=ruleEnumLiteral();

            state._fsp--;

             current =iv_ruleEnumLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumLiteral"


    // $ANTLR start "ruleEnumLiteral"
    // InternalAssetBasedSystemDsl.g:1964:1: ruleEnumLiteral returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ) ;
    public final EObject ruleEnumLiteral() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:1970:2: ( ( () ( (lv_name_1_0= ruleEString ) ) ) )
            // InternalAssetBasedSystemDsl.g:1971:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            {
            // InternalAssetBasedSystemDsl.g:1971:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            // InternalAssetBasedSystemDsl.g:1972:3: () ( (lv_name_1_0= ruleEString ) )
            {
            // InternalAssetBasedSystemDsl.g:1972:3: ()
            // InternalAssetBasedSystemDsl.g:1973:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEnumLiteralAccess().getEnumLiteralAction_0(),
            					current);
            			

            }

            // InternalAssetBasedSystemDsl.g:1979:3: ( (lv_name_1_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:1980:4: (lv_name_1_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:1980:4: (lv_name_1_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:1981:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getEnumLiteralAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEnumLiteralRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumLiteral"


    // $ANTLR start "entryRuleAsset"
    // InternalAssetBasedSystemDsl.g:2002:1: entryRuleAsset returns [EObject current=null] : iv_ruleAsset= ruleAsset EOF ;
    public final EObject entryRuleAsset() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAsset = null;


        try {
            // InternalAssetBasedSystemDsl.g:2002:46: (iv_ruleAsset= ruleAsset EOF )
            // InternalAssetBasedSystemDsl.g:2003:2: iv_ruleAsset= ruleAsset EOF
            {
             newCompositeNode(grammarAccess.getAssetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAsset=ruleAsset();

            state._fsp--;

             current =iv_ruleAsset; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAsset"


    // $ANTLR start "ruleAsset"
    // InternalAssetBasedSystemDsl.g:2009:1: ruleAsset returns [EObject current=null] : (otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}' ) ;
    public final EObject ruleAsset() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_description_7_0=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        Enumerator lv_descriptionFormat_6_0 = null;

        EObject lv_assetAttributeValues_8_0 = null;

        EObject lv_assetAttributeValues_9_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:2015:2: ( (otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}' ) )
            // InternalAssetBasedSystemDsl.g:2016:2: (otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}' )
            {
            // InternalAssetBasedSystemDsl.g:2016:2: (otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}' )
            // InternalAssetBasedSystemDsl.g:2017:3: otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,41,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getAssetAccess().getAssetKeyword_0());
            		
            // InternalAssetBasedSystemDsl.g:2021:3: ( (lv_name_1_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:2022:4: (lv_name_1_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:2022:4: (lv_name_1_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:2023:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_24);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,33,FOLLOW_12); 

            			newLeafNode(otherlv_2, grammarAccess.getAssetAccess().getColonKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:2044:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:2045:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:2045:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:2046:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetAccess().getAssetTypeAssetTypeCrossReference_3_0());
            				
            pushFollow(FOLLOW_7);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,13,FOLLOW_36); 

            			newLeafNode(otherlv_4, grammarAccess.getAssetAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalAssetBasedSystemDsl.g:2064:3: (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==26) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:2065:4: otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,26,FOLLOW_20); 

                    				newLeafNode(otherlv_5, grammarAccess.getAssetAccess().getDescriptionKeyword_5_0());
                    			
                    // InternalAssetBasedSystemDsl.g:2069:4: ( (lv_descriptionFormat_6_0= ruleTextFormat ) )?
                    int alt43=2;
                    int LA43_0 = input.LA(1);

                    if ( ((LA43_0>=100 && LA43_0<=102)) ) {
                        alt43=1;
                    }
                    switch (alt43) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:2070:5: (lv_descriptionFormat_6_0= ruleTextFormat )
                            {
                            // InternalAssetBasedSystemDsl.g:2070:5: (lv_descriptionFormat_6_0= ruleTextFormat )
                            // InternalAssetBasedSystemDsl.g:2071:6: lv_descriptionFormat_6_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getAssetAccess().getDescriptionFormatTextFormatEnumRuleCall_5_1_0());
                            					
                            pushFollow(FOLLOW_21);
                            lv_descriptionFormat_6_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getAssetRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_6_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:2088:4: ( (lv_description_7_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:2089:5: (lv_description_7_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:2089:5: (lv_description_7_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:2090:6: lv_description_7_0= RULE_STRING
                    {
                    lv_description_7_0=(Token)match(input,RULE_STRING,FOLLOW_37); 

                    						newLeafNode(lv_description_7_0, grammarAccess.getAssetAccess().getDescriptionSTRINGTerminalRuleCall_5_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAssetRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_7_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:2107:3: ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==39) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:2108:4: ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )*
                    {
                    // InternalAssetBasedSystemDsl.g:2108:4: ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) )
                    // InternalAssetBasedSystemDsl.g:2109:5: (lv_assetAttributeValues_8_0= ruleAssetAttributeValue )
                    {
                    // InternalAssetBasedSystemDsl.g:2109:5: (lv_assetAttributeValues_8_0= ruleAssetAttributeValue )
                    // InternalAssetBasedSystemDsl.g:2110:6: lv_assetAttributeValues_8_0= ruleAssetAttributeValue
                    {

                    						newCompositeNode(grammarAccess.getAssetAccess().getAssetAttributeValuesAssetAttributeValueParserRuleCall_6_0_0());
                    					
                    pushFollow(FOLLOW_37);
                    lv_assetAttributeValues_8_0=ruleAssetAttributeValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAssetRule());
                    						}
                    						add(
                    							current,
                    							"assetAttributeValues",
                    							lv_assetAttributeValues_8_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetAttributeValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:2127:4: ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )*
                    loop45:
                    do {
                        int alt45=2;
                        int LA45_0 = input.LA(1);

                        if ( (LA45_0==39) ) {
                            alt45=1;
                        }


                        switch (alt45) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:2128:5: (lv_assetAttributeValues_9_0= ruleAssetAttributeValue )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:2128:5: (lv_assetAttributeValues_9_0= ruleAssetAttributeValue )
                    	    // InternalAssetBasedSystemDsl.g:2129:6: lv_assetAttributeValues_9_0= ruleAssetAttributeValue
                    	    {

                    	    						newCompositeNode(grammarAccess.getAssetAccess().getAssetAttributeValuesAssetAttributeValueParserRuleCall_6_1_0());
                    	    					
                    	    pushFollow(FOLLOW_37);
                    	    lv_assetAttributeValues_9_0=ruleAssetAttributeValue();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getAssetRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"assetAttributeValues",
                    	    							lv_assetAttributeValues_9_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetAttributeValue");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop45;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getAssetAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAsset"


    // $ANTLR start "entryRuleAssetLink"
    // InternalAssetBasedSystemDsl.g:2155:1: entryRuleAssetLink returns [EObject current=null] : iv_ruleAssetLink= ruleAssetLink EOF ;
    public final EObject entryRuleAssetLink() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetLink = null;


        try {
            // InternalAssetBasedSystemDsl.g:2155:50: (iv_ruleAssetLink= ruleAssetLink EOF )
            // InternalAssetBasedSystemDsl.g:2156:2: iv_ruleAssetLink= ruleAssetLink EOF
            {
             newCompositeNode(grammarAccess.getAssetLinkRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetLink=ruleAssetLink();

            state._fsp--;

             current =iv_ruleAssetLink; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetLink"


    // $ANTLR start "ruleAssetLink"
    // InternalAssetBasedSystemDsl.g:2162:1: ruleAssetLink returns [EObject current=null] : (otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )? ) ;
    public final EObject ruleAssetLink() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:2168:2: ( (otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )? ) )
            // InternalAssetBasedSystemDsl.g:2169:2: (otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )? )
            {
            // InternalAssetBasedSystemDsl.g:2169:2: (otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )? )
            // InternalAssetBasedSystemDsl.g:2170:3: otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )?
            {
            otherlv_0=(Token)match(input,42,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getAssetLinkAccess().getLinkKeyword_0());
            		
            // InternalAssetBasedSystemDsl.g:2174:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:2175:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:2175:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:2176:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetLinkRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetLinkAccess().getSourceAssetAssetCrossReference_1_0());
            				
            pushFollow(FOLLOW_38);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,43,FOLLOW_12); 

            			newLeafNode(otherlv_2, grammarAccess.getAssetLinkAccess().getToKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:2194:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:2195:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:2195:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:2196:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetLinkRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetLinkAccess().getTargetAssetAssetCrossReference_3_0());
            				
            pushFollow(FOLLOW_39);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAssetBasedSystemDsl.g:2210:3: (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==44) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:2211:4: otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )?
                    {
                    otherlv_4=(Token)match(input,44,FOLLOW_12); 

                    				newLeafNode(otherlv_4, grammarAccess.getAssetLinkAccess().getAsKeyword_4_0());
                    			
                    // InternalAssetBasedSystemDsl.g:2215:4: ( ( ruleQualifiedName ) )
                    // InternalAssetBasedSystemDsl.g:2216:5: ( ruleQualifiedName )
                    {
                    // InternalAssetBasedSystemDsl.g:2216:5: ( ruleQualifiedName )
                    // InternalAssetBasedSystemDsl.g:2217:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAssetLinkRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getAssetLinkAccess().getReferenceTypeAssetTypeReferenceCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_40);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:2231:4: (otherlv_6= '#' ( ( ruleQualifiedName ) ) )?
                    int alt47=2;
                    int LA47_0 = input.LA(1);

                    if ( (LA47_0==45) ) {
                        alt47=1;
                    }
                    switch (alt47) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:2232:5: otherlv_6= '#' ( ( ruleQualifiedName ) )
                            {
                            otherlv_6=(Token)match(input,45,FOLLOW_12); 

                            					newLeafNode(otherlv_6, grammarAccess.getAssetLinkAccess().getNumberSignKeyword_4_2_0());
                            				
                            // InternalAssetBasedSystemDsl.g:2236:5: ( ( ruleQualifiedName ) )
                            // InternalAssetBasedSystemDsl.g:2237:6: ( ruleQualifiedName )
                            {
                            // InternalAssetBasedSystemDsl.g:2237:6: ( ruleQualifiedName )
                            // InternalAssetBasedSystemDsl.g:2238:7: ruleQualifiedName
                            {

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAssetLinkRule());
                            							}
                            						

                            							newCompositeNode(grammarAccess.getAssetLinkAccess().getOppositeReferenceTypeAssetTypeReferenceCrossReference_4_2_1_0());
                            						
                            pushFollow(FOLLOW_2);
                            ruleQualifiedName();

                            state._fsp--;


                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetLink"


    // $ANTLR start "entryRuleAssetAttributeValue"
    // InternalAssetBasedSystemDsl.g:2258:1: entryRuleAssetAttributeValue returns [EObject current=null] : iv_ruleAssetAttributeValue= ruleAssetAttributeValue EOF ;
    public final EObject entryRuleAssetAttributeValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetAttributeValue = null;


        try {
            // InternalAssetBasedSystemDsl.g:2258:60: (iv_ruleAssetAttributeValue= ruleAssetAttributeValue EOF )
            // InternalAssetBasedSystemDsl.g:2259:2: iv_ruleAssetAttributeValue= ruleAssetAttributeValue EOF
            {
             newCompositeNode(grammarAccess.getAssetAttributeValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetAttributeValue=ruleAssetAttributeValue();

            state._fsp--;

             current =iv_ruleAssetAttributeValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetAttributeValue"


    // $ANTLR start "ruleAssetAttributeValue"
    // InternalAssetBasedSystemDsl.g:2265:1: ruleAssetAttributeValue returns [EObject current=null] : ( () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) ) ) ;
    public final EObject ruleAssetAttributeValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_collection_5_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_values_4_0 = null;

        EObject lv_values_6_0 = null;

        EObject lv_values_8_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:2271:2: ( ( () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) ) ) )
            // InternalAssetBasedSystemDsl.g:2272:2: ( () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) ) )
            {
            // InternalAssetBasedSystemDsl.g:2272:2: ( () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) ) )
            // InternalAssetBasedSystemDsl.g:2273:3: () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) )
            {
            // InternalAssetBasedSystemDsl.g:2273:3: ()
            // InternalAssetBasedSystemDsl.g:2274:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetAttributeValueAccess().getAssetAttributeValueAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,39,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetAttributeValueAccess().getAttributeKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:2284:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:2285:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:2285:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:2286:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetAttributeValueRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetAttributeValueAccess().getAttributeTypeAssetTypeAttributeCrossReference_2_0());
            				
            pushFollow(FOLLOW_41);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,46,FOLLOW_42); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetAttributeValueAccess().getColonEqualsSignKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:2304:3: ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) )
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( ((LA50_0>=RULE_STRING && LA50_0<=RULE_INT)||LA50_0==36||(LA50_0>=47 && LA50_0<=48)) ) {
                alt50=1;
            }
            else if ( (LA50_0==37) ) {
                alt50=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }
            switch (alt50) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:2305:4: ( (lv_values_4_0= ruleAttributeConstantExpression ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2305:4: ( (lv_values_4_0= ruleAttributeConstantExpression ) )
                    // InternalAssetBasedSystemDsl.g:2306:5: (lv_values_4_0= ruleAttributeConstantExpression )
                    {
                    // InternalAssetBasedSystemDsl.g:2306:5: (lv_values_4_0= ruleAttributeConstantExpression )
                    // InternalAssetBasedSystemDsl.g:2307:6: lv_values_4_0= ruleAttributeConstantExpression
                    {

                    						newCompositeNode(grammarAccess.getAssetAttributeValueAccess().getValuesAttributeConstantExpressionParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_values_4_0=ruleAttributeConstantExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAssetAttributeValueRule());
                    						}
                    						add(
                    							current,
                    							"values",
                    							lv_values_4_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:2325:4: ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' )
                    {
                    // InternalAssetBasedSystemDsl.g:2325:4: ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' )
                    // InternalAssetBasedSystemDsl.g:2326:5: ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']'
                    {
                    // InternalAssetBasedSystemDsl.g:2326:5: ( (lv_collection_5_0= '[' ) )
                    // InternalAssetBasedSystemDsl.g:2327:6: (lv_collection_5_0= '[' )
                    {
                    // InternalAssetBasedSystemDsl.g:2327:6: (lv_collection_5_0= '[' )
                    // InternalAssetBasedSystemDsl.g:2328:7: lv_collection_5_0= '['
                    {
                    lv_collection_5_0=(Token)match(input,37,FOLLOW_31); 

                    							newLeafNode(lv_collection_5_0, grammarAccess.getAssetAttributeValueAccess().getCollectionLeftSquareBracketKeyword_4_1_0_0());
                    						

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getAssetAttributeValueRule());
                    							}
                    							setWithLastConsumed(current, "collection", lv_collection_5_0 != null, "[");
                    						

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:2340:5: ( (lv_values_6_0= ruleAttributeConstantExpression ) )
                    // InternalAssetBasedSystemDsl.g:2341:6: (lv_values_6_0= ruleAttributeConstantExpression )
                    {
                    // InternalAssetBasedSystemDsl.g:2341:6: (lv_values_6_0= ruleAttributeConstantExpression )
                    // InternalAssetBasedSystemDsl.g:2342:7: lv_values_6_0= ruleAttributeConstantExpression
                    {

                    							newCompositeNode(grammarAccess.getAssetAttributeValueAccess().getValuesAttributeConstantExpressionParserRuleCall_4_1_1_0());
                    						
                    pushFollow(FOLLOW_33);
                    lv_values_6_0=ruleAttributeConstantExpression();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getAssetAttributeValueRule());
                    							}
                    							add(
                    								current,
                    								"values",
                    								lv_values_6_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:2359:5: (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )*
                    loop49:
                    do {
                        int alt49=2;
                        int LA49_0 = input.LA(1);

                        if ( (LA49_0==16) ) {
                            alt49=1;
                        }


                        switch (alt49) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:2360:6: otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) )
                    	    {
                    	    otherlv_7=(Token)match(input,16,FOLLOW_31); 

                    	    						newLeafNode(otherlv_7, grammarAccess.getAssetAttributeValueAccess().getCommaKeyword_4_1_2_0());
                    	    					
                    	    // InternalAssetBasedSystemDsl.g:2364:6: ( (lv_values_8_0= ruleAttributeConstantExpression ) )
                    	    // InternalAssetBasedSystemDsl.g:2365:7: (lv_values_8_0= ruleAttributeConstantExpression )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:2365:7: (lv_values_8_0= ruleAttributeConstantExpression )
                    	    // InternalAssetBasedSystemDsl.g:2366:8: lv_values_8_0= ruleAttributeConstantExpression
                    	    {

                    	    								newCompositeNode(grammarAccess.getAssetAttributeValueAccess().getValuesAttributeConstantExpressionParserRuleCall_4_1_2_1_0());
                    	    							
                    	    pushFollow(FOLLOW_33);
                    	    lv_values_8_0=ruleAttributeConstantExpression();

                    	    state._fsp--;


                    	    								if (current==null) {
                    	    									current = createModelElementForParent(grammarAccess.getAssetAttributeValueRule());
                    	    								}
                    	    								add(
                    	    									current,
                    	    									"values",
                    	    									lv_values_8_0,
                    	    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                    	    								afterParserOrEnumRuleCall();
                    	    							

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop49;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,38,FOLLOW_2); 

                    					newLeafNode(otherlv_9, grammarAccess.getAssetAttributeValueAccess().getRightSquareBracketKeyword_4_1_3());
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetAttributeValue"


    // $ANTLR start "entryRuleAttributeConstantExpression"
    // InternalAssetBasedSystemDsl.g:2394:1: entryRuleAttributeConstantExpression returns [EObject current=null] : iv_ruleAttributeConstantExpression= ruleAttributeConstantExpression EOF ;
    public final EObject entryRuleAttributeConstantExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeConstantExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:2394:68: (iv_ruleAttributeConstantExpression= ruleAttributeConstantExpression EOF )
            // InternalAssetBasedSystemDsl.g:2395:2: iv_ruleAttributeConstantExpression= ruleAttributeConstantExpression EOF
            {
             newCompositeNode(grammarAccess.getAttributeConstantExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeConstantExpression=ruleAttributeConstantExpression();

            state._fsp--;

             current =iv_ruleAttributeConstantExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeConstantExpression"


    // $ANTLR start "ruleAttributeConstantExpression"
    // InternalAssetBasedSystemDsl.g:2401:1: ruleAttributeConstantExpression returns [EObject current=null] : ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Undefined_10= ruleUndefined ) ;
    public final EObject ruleAttributeConstantExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token lv_value_3_0=null;
        Token lv_value_5_1=null;
        Token lv_value_5_2=null;
        AntlrDatatypeRuleToken lv_value_7_0 = null;

        EObject this_Undefined_10 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:2407:2: ( ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Undefined_10= ruleUndefined ) )
            // InternalAssetBasedSystemDsl.g:2408:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Undefined_10= ruleUndefined )
            {
            // InternalAssetBasedSystemDsl.g:2408:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Undefined_10= ruleUndefined )
            int alt52=6;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                int LA52_1 = input.LA(2);

                if ( (LA52_1==EOF||LA52_1==16||LA52_1==19||(LA52_1>=31 && LA52_1<=32)||(LA52_1>=38 && LA52_1<=39)) ) {
                    alt52=1;
                }
                else if ( (LA52_1==71) ) {
                    alt52=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 52, 1, input);

                    throw nvae;
                }
                }
                break;
            case RULE_STRING:
                {
                alt52=2;
                }
                break;
            case 47:
            case 48:
                {
                alt52=3;
                }
                break;
            case RULE_ID:
                {
                alt52=5;
                }
                break;
            case 36:
                {
                alt52=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 52, 0, input);

                throw nvae;
            }

            switch (alt52) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:2409:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2409:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    // InternalAssetBasedSystemDsl.g:2410:4: () ( (lv_value_1_0= RULE_INT ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2410:4: ()
                    // InternalAssetBasedSystemDsl.g:2411:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getIntConstantAction_0_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:2417:4: ( (lv_value_1_0= RULE_INT ) )
                    // InternalAssetBasedSystemDsl.g:2418:5: (lv_value_1_0= RULE_INT )
                    {
                    // InternalAssetBasedSystemDsl.g:2418:5: (lv_value_1_0= RULE_INT )
                    // InternalAssetBasedSystemDsl.g:2419:6: lv_value_1_0= RULE_INT
                    {
                    lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    						newLeafNode(lv_value_1_0, grammarAccess.getAttributeConstantExpressionAccess().getValueINTTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_1_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:2437:3: ( () ( (lv_value_3_0= RULE_STRING ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2437:3: ( () ( (lv_value_3_0= RULE_STRING ) ) )
                    // InternalAssetBasedSystemDsl.g:2438:4: () ( (lv_value_3_0= RULE_STRING ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2438:4: ()
                    // InternalAssetBasedSystemDsl.g:2439:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getStringConstantAction_1_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:2445:4: ( (lv_value_3_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:2446:5: (lv_value_3_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:2446:5: (lv_value_3_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:2447:6: lv_value_3_0= RULE_STRING
                    {
                    lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getAttributeConstantExpressionAccess().getValueSTRINGTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalAssetBasedSystemDsl.g:2465:3: ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2465:3: ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) )
                    // InternalAssetBasedSystemDsl.g:2466:4: () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2466:4: ()
                    // InternalAssetBasedSystemDsl.g:2467:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getBooleanConstantAction_2_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:2473:4: ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) )
                    // InternalAssetBasedSystemDsl.g:2474:5: ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2474:5: ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) )
                    // InternalAssetBasedSystemDsl.g:2475:6: (lv_value_5_1= 'true' | lv_value_5_2= 'false' )
                    {
                    // InternalAssetBasedSystemDsl.g:2475:6: (lv_value_5_1= 'true' | lv_value_5_2= 'false' )
                    int alt51=2;
                    int LA51_0 = input.LA(1);

                    if ( (LA51_0==47) ) {
                        alt51=1;
                    }
                    else if ( (LA51_0==48) ) {
                        alt51=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 51, 0, input);

                        throw nvae;
                    }
                    switch (alt51) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:2476:7: lv_value_5_1= 'true'
                            {
                            lv_value_5_1=(Token)match(input,47,FOLLOW_2); 

                            							newLeafNode(lv_value_5_1, grammarAccess.getAttributeConstantExpressionAccess().getValueTrueKeyword_2_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_5_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalAssetBasedSystemDsl.g:2487:7: lv_value_5_2= 'false'
                            {
                            lv_value_5_2=(Token)match(input,48,FOLLOW_2); 

                            							newLeafNode(lv_value_5_2, grammarAccess.getAttributeConstantExpressionAccess().getValueFalseKeyword_2_1_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_5_2, null);
                            						

                            }
                            break;

                    }


                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalAssetBasedSystemDsl.g:2502:3: ( () ( (lv_value_7_0= ruleVersion ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2502:3: ( () ( (lv_value_7_0= ruleVersion ) ) )
                    // InternalAssetBasedSystemDsl.g:2503:4: () ( (lv_value_7_0= ruleVersion ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2503:4: ()
                    // InternalAssetBasedSystemDsl.g:2504:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getVersionConstantAction_3_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:2510:4: ( (lv_value_7_0= ruleVersion ) )
                    // InternalAssetBasedSystemDsl.g:2511:5: (lv_value_7_0= ruleVersion )
                    {
                    // InternalAssetBasedSystemDsl.g:2511:5: (lv_value_7_0= ruleVersion )
                    // InternalAssetBasedSystemDsl.g:2512:6: lv_value_7_0= ruleVersion
                    {

                    						newCompositeNode(grammarAccess.getAttributeConstantExpressionAccess().getValueVersionParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_7_0=ruleVersion();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeConstantExpressionRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Version");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalAssetBasedSystemDsl.g:2531:3: ( () ( ( ruleQualifiedName ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2531:3: ( () ( ( ruleQualifiedName ) ) )
                    // InternalAssetBasedSystemDsl.g:2532:4: () ( ( ruleQualifiedName ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2532:4: ()
                    // InternalAssetBasedSystemDsl.g:2533:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getEnumConstantAction_4_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:2539:4: ( ( ruleQualifiedName ) )
                    // InternalAssetBasedSystemDsl.g:2540:5: ( ruleQualifiedName )
                    {
                    // InternalAssetBasedSystemDsl.g:2540:5: ( ruleQualifiedName )
                    // InternalAssetBasedSystemDsl.g:2541:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getAttributeConstantExpressionAccess().getValueEnumLiteralCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalAssetBasedSystemDsl.g:2557:3: this_Undefined_10= ruleUndefined
                    {

                    			newCompositeNode(grammarAccess.getAttributeConstantExpressionAccess().getUndefinedParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Undefined_10=ruleUndefined();

                    state._fsp--;


                    			current = this_Undefined_10;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeConstantExpression"


    // $ANTLR start "entryRuleContract"
    // InternalAssetBasedSystemDsl.g:2569:1: entryRuleContract returns [EObject current=null] : iv_ruleContract= ruleContract EOF ;
    public final EObject entryRuleContract() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContract = null;


        try {
            // InternalAssetBasedSystemDsl.g:2569:49: (iv_ruleContract= ruleContract EOF )
            // InternalAssetBasedSystemDsl.g:2570:2: iv_ruleContract= ruleContract EOF
            {
             newCompositeNode(grammarAccess.getContractRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleContract=ruleContract();

            state._fsp--;

             current =iv_ruleContract; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContract"


    // $ANTLR start "ruleContract"
    // InternalAssetBasedSystemDsl.g:2576:1: ruleContract returns [EObject current=null] : ( () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}' ) ;
    public final EObject ruleContract() throws RecognitionException {
        EObject current = null;

        Token lv_dynamic_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token lv_description_17_0=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        EObject lv_guardParameters_6_0 = null;

        EObject lv_guardParameters_8_0 = null;

        Enumerator lv_severity_14_0 = null;

        Enumerator lv_descriptionFormat_16_0 = null;

        EObject lv_annotations_20_0 = null;

        EObject lv_guardExpression_24_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:2582:2: ( ( () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}' ) )
            // InternalAssetBasedSystemDsl.g:2583:2: ( () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}' )
            {
            // InternalAssetBasedSystemDsl.g:2583:2: ( () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}' )
            // InternalAssetBasedSystemDsl.g:2584:3: () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}'
            {
            // InternalAssetBasedSystemDsl.g:2584:3: ()
            // InternalAssetBasedSystemDsl.g:2585:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getContractAccess().getContractAction_0(),
            					current);
            			

            }

            // InternalAssetBasedSystemDsl.g:2591:3: ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? )
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==49) ) {
                alt54=1;
            }
            else if ( ((LA54_0>=50 && LA54_0<=51)) ) {
                alt54=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 54, 0, input);

                throw nvae;
            }
            switch (alt54) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:2592:4: ( (lv_dynamic_1_0= 'dynamic' ) )
                    {
                    // InternalAssetBasedSystemDsl.g:2592:4: ( (lv_dynamic_1_0= 'dynamic' ) )
                    // InternalAssetBasedSystemDsl.g:2593:5: (lv_dynamic_1_0= 'dynamic' )
                    {
                    // InternalAssetBasedSystemDsl.g:2593:5: (lv_dynamic_1_0= 'dynamic' )
                    // InternalAssetBasedSystemDsl.g:2594:6: lv_dynamic_1_0= 'dynamic'
                    {
                    lv_dynamic_1_0=(Token)match(input,49,FOLLOW_43); 

                    						newLeafNode(lv_dynamic_1_0, grammarAccess.getContractAccess().getDynamicDynamicKeyword_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getContractRule());
                    						}
                    						setWithLastConsumed(current, "dynamic", lv_dynamic_1_0 != null, "dynamic");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:2607:4: (otherlv_2= 'static' )?
                    {
                    // InternalAssetBasedSystemDsl.g:2607:4: (otherlv_2= 'static' )?
                    int alt53=2;
                    int LA53_0 = input.LA(1);

                    if ( (LA53_0==50) ) {
                        alt53=1;
                    }
                    switch (alt53) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:2608:5: otherlv_2= 'static'
                            {
                            otherlv_2=(Token)match(input,50,FOLLOW_43); 

                            					newLeafNode(otherlv_2, grammarAccess.getContractAccess().getStaticKeyword_1_1());
                            				

                            }
                            break;

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,51,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getContractAccess().getContractKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:2618:3: ( (lv_name_4_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:2619:4: (lv_name_4_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:2619:4: (lv_name_4_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:2620:5: lv_name_4_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getContractAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_9);
            lv_name_4_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getContractRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_4_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,15,FOLLOW_44); 

            			newLeafNode(otherlv_5, grammarAccess.getContractAccess().getLeftParenthesisKeyword_4());
            		
            // InternalAssetBasedSystemDsl.g:2641:3: ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( ((LA56_0>=RULE_STRING && LA56_0<=RULE_ID)) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:2642:4: ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )*
                    {
                    // InternalAssetBasedSystemDsl.g:2642:4: ( (lv_guardParameters_6_0= ruleGuardParameter ) )
                    // InternalAssetBasedSystemDsl.g:2643:5: (lv_guardParameters_6_0= ruleGuardParameter )
                    {
                    // InternalAssetBasedSystemDsl.g:2643:5: (lv_guardParameters_6_0= ruleGuardParameter )
                    // InternalAssetBasedSystemDsl.g:2644:6: lv_guardParameters_6_0= ruleGuardParameter
                    {

                    						newCompositeNode(grammarAccess.getContractAccess().getGuardParametersGuardParameterParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_guardParameters_6_0=ruleGuardParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getContractRule());
                    						}
                    						add(
                    							current,
                    							"guardParameters",
                    							lv_guardParameters_6_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardParameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:2661:4: (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )*
                    loop55:
                    do {
                        int alt55=2;
                        int LA55_0 = input.LA(1);

                        if ( (LA55_0==16) ) {
                            alt55=1;
                        }


                        switch (alt55) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:2662:5: otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) )
                    	    {
                    	    otherlv_7=(Token)match(input,16,FOLLOW_6); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getContractAccess().getCommaKeyword_5_1_0());
                    	    				
                    	    // InternalAssetBasedSystemDsl.g:2666:5: ( (lv_guardParameters_8_0= ruleGuardParameter ) )
                    	    // InternalAssetBasedSystemDsl.g:2667:6: (lv_guardParameters_8_0= ruleGuardParameter )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:2667:6: (lv_guardParameters_8_0= ruleGuardParameter )
                    	    // InternalAssetBasedSystemDsl.g:2668:7: lv_guardParameters_8_0= ruleGuardParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getContractAccess().getGuardParametersGuardParameterParserRuleCall_5_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    lv_guardParameters_8_0=ruleGuardParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getContractRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"guardParameters",
                    	    								lv_guardParameters_8_0,
                    	    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardParameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop55;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_9=(Token)match(input,17,FOLLOW_7); 

            			newLeafNode(otherlv_9, grammarAccess.getContractAccess().getRightParenthesisKeyword_6());
            		
            otherlv_10=(Token)match(input,13,FOLLOW_45); 

            			newLeafNode(otherlv_10, grammarAccess.getContractAccess().getLeftCurlyBracketKeyword_7());
            		
            // InternalAssetBasedSystemDsl.g:2695:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) )
            // InternalAssetBasedSystemDsl.g:2696:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) )
            {
            // InternalAssetBasedSystemDsl.g:2696:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) )
            // InternalAssetBasedSystemDsl.g:2697:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getContractAccess().getUnorderedGroup_8());
            				
            // InternalAssetBasedSystemDsl.g:2700:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* )
            // InternalAssetBasedSystemDsl.g:2701:6: ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )*
            {
            // InternalAssetBasedSystemDsl.g:2701:6: ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )*
            loop59:
            do {
                int alt59=4;
                int LA59_0 = input.LA(1);

                if ( LA59_0 == 52 && getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 0) ) {
                    alt59=1;
                }
                else if ( LA59_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 1) ) {
                    alt59=2;
                }
                else if ( LA59_0 == 18 && getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 2) ) {
                    alt59=3;
                }


                switch (alt59) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:2702:4: ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:2702:4: ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2703:5: {...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 0)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:2703:105: ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2704:6: ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getContractAccess().getUnorderedGroup_8(), 0);
            	    					
            	    // InternalAssetBasedSystemDsl.g:2707:9: ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2707:10: {...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:2707:19: (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) )
            	    // InternalAssetBasedSystemDsl.g:2707:20: otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) )
            	    {
            	    otherlv_12=(Token)match(input,52,FOLLOW_46); 

            	    									newLeafNode(otherlv_12, grammarAccess.getContractAccess().getSeverityKeyword_8_0_0());
            	    								
            	    otherlv_13=(Token)match(input,53,FOLLOW_47); 

            	    									newLeafNode(otherlv_13, grammarAccess.getContractAccess().getEqualsSignKeyword_8_0_1());
            	    								
            	    // InternalAssetBasedSystemDsl.g:2715:9: ( (lv_severity_14_0= ruleSeverity ) )
            	    // InternalAssetBasedSystemDsl.g:2716:10: (lv_severity_14_0= ruleSeverity )
            	    {
            	    // InternalAssetBasedSystemDsl.g:2716:10: (lv_severity_14_0= ruleSeverity )
            	    // InternalAssetBasedSystemDsl.g:2717:11: lv_severity_14_0= ruleSeverity
            	    {

            	    											newCompositeNode(grammarAccess.getContractAccess().getSeveritySeverityEnumRuleCall_8_0_2_0());
            	    										
            	    pushFollow(FOLLOW_45);
            	    lv_severity_14_0=ruleSeverity();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getContractRule());
            	    											}
            	    											set(
            	    												current,
            	    												"severity",
            	    												lv_severity_14_0,
            	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Severity");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getContractAccess().getUnorderedGroup_8());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalAssetBasedSystemDsl.g:2740:4: ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:2740:4: ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2741:5: {...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 1)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:2741:105: ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2742:6: ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getContractAccess().getUnorderedGroup_8(), 1);
            	    					
            	    // InternalAssetBasedSystemDsl.g:2745:9: ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2745:10: {...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:2745:19: (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) )
            	    // InternalAssetBasedSystemDsl.g:2745:20: otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) )
            	    {
            	    otherlv_15=(Token)match(input,26,FOLLOW_20); 

            	    									newLeafNode(otherlv_15, grammarAccess.getContractAccess().getDescriptionKeyword_8_1_0());
            	    								
            	    // InternalAssetBasedSystemDsl.g:2749:9: ( (lv_descriptionFormat_16_0= ruleTextFormat ) )?
            	    int alt57=2;
            	    int LA57_0 = input.LA(1);

            	    if ( ((LA57_0>=100 && LA57_0<=102)) ) {
            	        alt57=1;
            	    }
            	    switch (alt57) {
            	        case 1 :
            	            // InternalAssetBasedSystemDsl.g:2750:10: (lv_descriptionFormat_16_0= ruleTextFormat )
            	            {
            	            // InternalAssetBasedSystemDsl.g:2750:10: (lv_descriptionFormat_16_0= ruleTextFormat )
            	            // InternalAssetBasedSystemDsl.g:2751:11: lv_descriptionFormat_16_0= ruleTextFormat
            	            {

            	            											newCompositeNode(grammarAccess.getContractAccess().getDescriptionFormatTextFormatEnumRuleCall_8_1_1_0());
            	            										
            	            pushFollow(FOLLOW_21);
            	            lv_descriptionFormat_16_0=ruleTextFormat();

            	            state._fsp--;


            	            											if (current==null) {
            	            												current = createModelElementForParent(grammarAccess.getContractRule());
            	            											}
            	            											set(
            	            												current,
            	            												"descriptionFormat",
            	            												lv_descriptionFormat_16_0,
            	            												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
            	            											afterParserOrEnumRuleCall();
            	            										

            	            }


            	            }
            	            break;

            	    }

            	    // InternalAssetBasedSystemDsl.g:2768:9: ( (lv_description_17_0= RULE_STRING ) )
            	    // InternalAssetBasedSystemDsl.g:2769:10: (lv_description_17_0= RULE_STRING )
            	    {
            	    // InternalAssetBasedSystemDsl.g:2769:10: (lv_description_17_0= RULE_STRING )
            	    // InternalAssetBasedSystemDsl.g:2770:11: lv_description_17_0= RULE_STRING
            	    {
            	    lv_description_17_0=(Token)match(input,RULE_STRING,FOLLOW_45); 

            	    											newLeafNode(lv_description_17_0, grammarAccess.getContractAccess().getDescriptionSTRINGTerminalRuleCall_8_1_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getContractRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"description",
            	    												lv_description_17_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getContractAccess().getUnorderedGroup_8());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalAssetBasedSystemDsl.g:2792:4: ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:2792:4: ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2793:5: {...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 2)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:2793:105: ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) )
            	    // InternalAssetBasedSystemDsl.g:2794:6: ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getContractAccess().getUnorderedGroup_8(), 2);
            	    					
            	    // InternalAssetBasedSystemDsl.g:2797:9: ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) )
            	    // InternalAssetBasedSystemDsl.g:2797:10: {...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:2797:19: (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' )
            	    // InternalAssetBasedSystemDsl.g:2797:20: otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}'
            	    {
            	    otherlv_18=(Token)match(input,18,FOLLOW_7); 

            	    									newLeafNode(otherlv_18, grammarAccess.getContractAccess().getAnnotationsKeyword_8_2_0());
            	    								
            	    otherlv_19=(Token)match(input,13,FOLLOW_13); 

            	    									newLeafNode(otherlv_19, grammarAccess.getContractAccess().getLeftCurlyBracketKeyword_8_2_1());
            	    								
            	    // InternalAssetBasedSystemDsl.g:2805:9: ( (lv_annotations_20_0= ruleAnnotationEntry ) )*
            	    loop58:
            	    do {
            	        int alt58=2;
            	        int LA58_0 = input.LA(1);

            	        if ( ((LA58_0>=RULE_STRING && LA58_0<=RULE_ID)) ) {
            	            alt58=1;
            	        }


            	        switch (alt58) {
            	    	case 1 :
            	    	    // InternalAssetBasedSystemDsl.g:2806:10: (lv_annotations_20_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalAssetBasedSystemDsl.g:2806:10: (lv_annotations_20_0= ruleAnnotationEntry )
            	    	    // InternalAssetBasedSystemDsl.g:2807:11: lv_annotations_20_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getContractAccess().getAnnotationsAnnotationEntryParserRuleCall_8_2_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_13);
            	    	    lv_annotations_20_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getContractRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_20_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop58;
            	        }
            	    } while (true);

            	    otherlv_21=(Token)match(input,19,FOLLOW_45); 

            	    									newLeafNode(otherlv_21, grammarAccess.getContractAccess().getRightCurlyBracketKeyword_8_2_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getContractAccess().getUnorderedGroup_8());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop59;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getContractAccess().getUnorderedGroup_8());
            				

            }

            otherlv_22=(Token)match(input,54,FOLLOW_46); 

            			newLeafNode(otherlv_22, grammarAccess.getContractAccess().getGuardKeyword_9());
            		
            otherlv_23=(Token)match(input,53,FOLLOW_48); 

            			newLeafNode(otherlv_23, grammarAccess.getContractAccess().getEqualsSignKeyword_10());
            		
            // InternalAssetBasedSystemDsl.g:2849:3: ( (lv_guardExpression_24_0= ruleExpression ) )
            // InternalAssetBasedSystemDsl.g:2850:4: (lv_guardExpression_24_0= ruleExpression )
            {
            // InternalAssetBasedSystemDsl.g:2850:4: (lv_guardExpression_24_0= ruleExpression )
            // InternalAssetBasedSystemDsl.g:2851:5: lv_guardExpression_24_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getContractAccess().getGuardExpressionExpressionParserRuleCall_11_0());
            				
            pushFollow(FOLLOW_49);
            lv_guardExpression_24_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getContractRule());
            					}
            					set(
            						current,
            						"guardExpression",
            						lv_guardExpression_24_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_25=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_25, grammarAccess.getContractAccess().getRightCurlyBracketKeyword_12());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContract"


    // $ANTLR start "entryRuleGuardedAction"
    // InternalAssetBasedSystemDsl.g:2876:1: entryRuleGuardedAction returns [EObject current=null] : iv_ruleGuardedAction= ruleGuardedAction EOF ;
    public final EObject entryRuleGuardedAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardedAction = null;


        try {
            // InternalAssetBasedSystemDsl.g:2876:54: (iv_ruleGuardedAction= ruleGuardedAction EOF )
            // InternalAssetBasedSystemDsl.g:2877:2: iv_ruleGuardedAction= ruleGuardedAction EOF
            {
             newCompositeNode(grammarAccess.getGuardedActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuardedAction=ruleGuardedAction();

            state._fsp--;

             current =iv_ruleGuardedAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardedAction"


    // $ANTLR start "ruleGuardedAction"
    // InternalAssetBasedSystemDsl.g:2883:1: ruleGuardedAction returns [EObject current=null] : ( () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}' ) ;
    public final EObject ruleGuardedAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token lv_description_12_0=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_guardParameters_4_0 = null;

        EObject lv_guardParameters_6_0 = null;

        Enumerator lv_descriptionFormat_11_0 = null;

        EObject lv_annotations_15_0 = null;

        EObject lv_guardExpression_19_0 = null;

        EObject lv_guardActions_22_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:2889:2: ( ( () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}' ) )
            // InternalAssetBasedSystemDsl.g:2890:2: ( () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}' )
            {
            // InternalAssetBasedSystemDsl.g:2890:2: ( () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}' )
            // InternalAssetBasedSystemDsl.g:2891:3: () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}'
            {
            // InternalAssetBasedSystemDsl.g:2891:3: ()
            // InternalAssetBasedSystemDsl.g:2892:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGuardedActionAccess().getGuardedActionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,55,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getGuardedActionAccess().getGuardedActionKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:2902:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:2903:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:2903:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:2904:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getGuardedActionAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_9);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,15,FOLLOW_44); 

            			newLeafNode(otherlv_3, grammarAccess.getGuardedActionAccess().getLeftParenthesisKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:2925:3: ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( ((LA61_0>=RULE_STRING && LA61_0<=RULE_ID)) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:2926:4: ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )*
                    {
                    // InternalAssetBasedSystemDsl.g:2926:4: ( (lv_guardParameters_4_0= ruleGuardParameter ) )
                    // InternalAssetBasedSystemDsl.g:2927:5: (lv_guardParameters_4_0= ruleGuardParameter )
                    {
                    // InternalAssetBasedSystemDsl.g:2927:5: (lv_guardParameters_4_0= ruleGuardParameter )
                    // InternalAssetBasedSystemDsl.g:2928:6: lv_guardParameters_4_0= ruleGuardParameter
                    {

                    						newCompositeNode(grammarAccess.getGuardedActionAccess().getGuardParametersGuardParameterParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_guardParameters_4_0=ruleGuardParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGuardedActionRule());
                    						}
                    						add(
                    							current,
                    							"guardParameters",
                    							lv_guardParameters_4_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardParameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:2945:4: (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )*
                    loop60:
                    do {
                        int alt60=2;
                        int LA60_0 = input.LA(1);

                        if ( (LA60_0==16) ) {
                            alt60=1;
                        }


                        switch (alt60) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:2946:5: otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) )
                    	    {
                    	    otherlv_5=(Token)match(input,16,FOLLOW_6); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getGuardedActionAccess().getCommaKeyword_4_1_0());
                    	    				
                    	    // InternalAssetBasedSystemDsl.g:2950:5: ( (lv_guardParameters_6_0= ruleGuardParameter ) )
                    	    // InternalAssetBasedSystemDsl.g:2951:6: (lv_guardParameters_6_0= ruleGuardParameter )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:2951:6: (lv_guardParameters_6_0= ruleGuardParameter )
                    	    // InternalAssetBasedSystemDsl.g:2952:7: lv_guardParameters_6_0= ruleGuardParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getGuardedActionAccess().getGuardParametersGuardParameterParserRuleCall_4_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    lv_guardParameters_6_0=ruleGuardParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getGuardedActionRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"guardParameters",
                    	    								lv_guardParameters_6_0,
                    	    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardParameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop60;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,17,FOLLOW_7); 

            			newLeafNode(otherlv_7, grammarAccess.getGuardedActionAccess().getRightParenthesisKeyword_5());
            		
            otherlv_8=(Token)match(input,13,FOLLOW_50); 

            			newLeafNode(otherlv_8, grammarAccess.getGuardedActionAccess().getLeftCurlyBracketKeyword_6());
            		
            // InternalAssetBasedSystemDsl.g:2979:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) )
            // InternalAssetBasedSystemDsl.g:2980:4: ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) )
            {
            // InternalAssetBasedSystemDsl.g:2980:4: ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) )
            // InternalAssetBasedSystemDsl.g:2981:5: ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7());
            				
            // InternalAssetBasedSystemDsl.g:2984:5: ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* )
            // InternalAssetBasedSystemDsl.g:2985:6: ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )*
            {
            // InternalAssetBasedSystemDsl.g:2985:6: ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )*
            loop64:
            do {
                int alt64=3;
                int LA64_0 = input.LA(1);

                if ( LA64_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 0) ) {
                    alt64=1;
                }
                else if ( LA64_0 == 18 && getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 1) ) {
                    alt64=2;
                }


                switch (alt64) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:2986:4: ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:2986:4: ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2987:5: {...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleGuardedAction", "getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 0)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:2987:110: ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2988:6: ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 0);
            	    					
            	    // InternalAssetBasedSystemDsl.g:2991:9: ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:2991:10: {...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGuardedAction", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:2991:19: (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) )
            	    // InternalAssetBasedSystemDsl.g:2991:20: otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) )
            	    {
            	    otherlv_10=(Token)match(input,26,FOLLOW_20); 

            	    									newLeafNode(otherlv_10, grammarAccess.getGuardedActionAccess().getDescriptionKeyword_7_0_0());
            	    								
            	    // InternalAssetBasedSystemDsl.g:2995:9: ( (lv_descriptionFormat_11_0= ruleTextFormat ) )?
            	    int alt62=2;
            	    int LA62_0 = input.LA(1);

            	    if ( ((LA62_0>=100 && LA62_0<=102)) ) {
            	        alt62=1;
            	    }
            	    switch (alt62) {
            	        case 1 :
            	            // InternalAssetBasedSystemDsl.g:2996:10: (lv_descriptionFormat_11_0= ruleTextFormat )
            	            {
            	            // InternalAssetBasedSystemDsl.g:2996:10: (lv_descriptionFormat_11_0= ruleTextFormat )
            	            // InternalAssetBasedSystemDsl.g:2997:11: lv_descriptionFormat_11_0= ruleTextFormat
            	            {

            	            											newCompositeNode(grammarAccess.getGuardedActionAccess().getDescriptionFormatTextFormatEnumRuleCall_7_0_1_0());
            	            										
            	            pushFollow(FOLLOW_21);
            	            lv_descriptionFormat_11_0=ruleTextFormat();

            	            state._fsp--;


            	            											if (current==null) {
            	            												current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            	            											}
            	            											set(
            	            												current,
            	            												"descriptionFormat",
            	            												lv_descriptionFormat_11_0,
            	            												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
            	            											afterParserOrEnumRuleCall();
            	            										

            	            }


            	            }
            	            break;

            	    }

            	    // InternalAssetBasedSystemDsl.g:3014:9: ( (lv_description_12_0= RULE_STRING ) )
            	    // InternalAssetBasedSystemDsl.g:3015:10: (lv_description_12_0= RULE_STRING )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3015:10: (lv_description_12_0= RULE_STRING )
            	    // InternalAssetBasedSystemDsl.g:3016:11: lv_description_12_0= RULE_STRING
            	    {
            	    lv_description_12_0=(Token)match(input,RULE_STRING,FOLLOW_50); 

            	    											newLeafNode(lv_description_12_0, grammarAccess.getGuardedActionAccess().getDescriptionSTRINGTerminalRuleCall_7_0_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getGuardedActionRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"description",
            	    												lv_description_12_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalAssetBasedSystemDsl.g:3038:4: ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3038:4: ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:3039:5: {...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleGuardedAction", "getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 1)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:3039:110: ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) )
            	    // InternalAssetBasedSystemDsl.g:3040:6: ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 1);
            	    					
            	    // InternalAssetBasedSystemDsl.g:3043:9: ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) )
            	    // InternalAssetBasedSystemDsl.g:3043:10: {...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGuardedAction", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:3043:19: (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' )
            	    // InternalAssetBasedSystemDsl.g:3043:20: otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}'
            	    {
            	    otherlv_13=(Token)match(input,18,FOLLOW_7); 

            	    									newLeafNode(otherlv_13, grammarAccess.getGuardedActionAccess().getAnnotationsKeyword_7_1_0());
            	    								
            	    otherlv_14=(Token)match(input,13,FOLLOW_13); 

            	    									newLeafNode(otherlv_14, grammarAccess.getGuardedActionAccess().getLeftCurlyBracketKeyword_7_1_1());
            	    								
            	    // InternalAssetBasedSystemDsl.g:3051:9: ( (lv_annotations_15_0= ruleAnnotationEntry ) )*
            	    loop63:
            	    do {
            	        int alt63=2;
            	        int LA63_0 = input.LA(1);

            	        if ( ((LA63_0>=RULE_STRING && LA63_0<=RULE_ID)) ) {
            	            alt63=1;
            	        }


            	        switch (alt63) {
            	    	case 1 :
            	    	    // InternalAssetBasedSystemDsl.g:3052:10: (lv_annotations_15_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalAssetBasedSystemDsl.g:3052:10: (lv_annotations_15_0= ruleAnnotationEntry )
            	    	    // InternalAssetBasedSystemDsl.g:3053:11: lv_annotations_15_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getGuardedActionAccess().getAnnotationsAnnotationEntryParserRuleCall_7_1_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_13);
            	    	    lv_annotations_15_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_15_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop63;
            	        }
            	    } while (true);

            	    otherlv_16=(Token)match(input,19,FOLLOW_50); 

            	    									newLeafNode(otherlv_16, grammarAccess.getGuardedActionAccess().getRightCurlyBracketKeyword_7_1_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop64;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7());
            				

            }

            otherlv_17=(Token)match(input,54,FOLLOW_46); 

            			newLeafNode(otherlv_17, grammarAccess.getGuardedActionAccess().getGuardKeyword_8());
            		
            otherlv_18=(Token)match(input,53,FOLLOW_48); 

            			newLeafNode(otherlv_18, grammarAccess.getGuardedActionAccess().getEqualsSignKeyword_9());
            		
            // InternalAssetBasedSystemDsl.g:3095:3: ( (lv_guardExpression_19_0= ruleExpression ) )
            // InternalAssetBasedSystemDsl.g:3096:4: (lv_guardExpression_19_0= ruleExpression )
            {
            // InternalAssetBasedSystemDsl.g:3096:4: (lv_guardExpression_19_0= ruleExpression )
            // InternalAssetBasedSystemDsl.g:3097:5: lv_guardExpression_19_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getGuardedActionAccess().getGuardExpressionExpressionParserRuleCall_10_0());
            				
            pushFollow(FOLLOW_51);
            lv_guardExpression_19_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            					}
            					set(
            						current,
            						"guardExpression",
            						lv_guardExpression_19_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_20=(Token)match(input,56,FOLLOW_7); 

            			newLeafNode(otherlv_20, grammarAccess.getGuardedActionAccess().getActionKeyword_11());
            		
            otherlv_21=(Token)match(input,13,FOLLOW_52); 

            			newLeafNode(otherlv_21, grammarAccess.getGuardedActionAccess().getLeftCurlyBracketKeyword_12());
            		
            // InternalAssetBasedSystemDsl.g:3122:3: ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )*
            loop65:
            do {
                int alt65=2;
                int LA65_0 = input.LA(1);

                if ( ((LA65_0>=RULE_STRING && LA65_0<=RULE_INT)||LA65_0==15||(LA65_0>=36 && LA65_0<=37)||(LA65_0>=47 && LA65_0<=48)) ) {
                    alt65=1;
                }


                switch (alt65) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:3123:4: ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';'
            	    {
            	    // InternalAssetBasedSystemDsl.g:3123:4: ( (lv_guardActions_22_0= ruleAction ) )
            	    // InternalAssetBasedSystemDsl.g:3124:5: (lv_guardActions_22_0= ruleAction )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3124:5: (lv_guardActions_22_0= ruleAction )
            	    // InternalAssetBasedSystemDsl.g:3125:6: lv_guardActions_22_0= ruleAction
            	    {

            	    						newCompositeNode(grammarAccess.getGuardedActionAccess().getGuardActionsActionParserRuleCall_13_0_0());
            	    					
            	    pushFollow(FOLLOW_53);
            	    lv_guardActions_22_0=ruleAction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"guardActions",
            	    							lv_guardActions_22_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Action");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_23=(Token)match(input,57,FOLLOW_52); 

            	    				newLeafNode(otherlv_23, grammarAccess.getGuardedActionAccess().getSemicolonKeyword_13_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop65;
                }
            } while (true);

            otherlv_24=(Token)match(input,19,FOLLOW_49); 

            			newLeafNode(otherlv_24, grammarAccess.getGuardedActionAccess().getRightCurlyBracketKeyword_14());
            		
            otherlv_25=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_25, grammarAccess.getGuardedActionAccess().getRightCurlyBracketKeyword_15());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardedAction"


    // $ANTLR start "entryRuleGuardParameter"
    // InternalAssetBasedSystemDsl.g:3159:1: entryRuleGuardParameter returns [EObject current=null] : iv_ruleGuardParameter= ruleGuardParameter EOF ;
    public final EObject entryRuleGuardParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardParameter = null;


        try {
            // InternalAssetBasedSystemDsl.g:3159:55: (iv_ruleGuardParameter= ruleGuardParameter EOF )
            // InternalAssetBasedSystemDsl.g:3160:2: iv_ruleGuardParameter= ruleGuardParameter EOF
            {
             newCompositeNode(grammarAccess.getGuardParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuardParameter=ruleGuardParameter();

            state._fsp--;

             current =iv_ruleGuardParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardParameter"


    // $ANTLR start "ruleGuardParameter"
    // InternalAssetBasedSystemDsl.g:3166:1: ruleGuardParameter returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleGuardParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3172:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) ) )
            // InternalAssetBasedSystemDsl.g:3173:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) )
            {
            // InternalAssetBasedSystemDsl.g:3173:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) )
            // InternalAssetBasedSystemDsl.g:3174:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) )
            {
            // InternalAssetBasedSystemDsl.g:3174:3: ()
            // InternalAssetBasedSystemDsl.g:3175:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGuardParameterAccess().getGuardParameterAction_0(),
            					current);
            			

            }

            // InternalAssetBasedSystemDsl.g:3181:3: ( (lv_name_1_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:3182:4: (lv_name_1_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:3182:4: (lv_name_1_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:3183:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getGuardParameterAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_24);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGuardParameterRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,33,FOLLOW_12); 

            			newLeafNode(otherlv_2, grammarAccess.getGuardParameterAccess().getColonKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:3204:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:3205:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:3205:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:3206:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGuardParameterRule());
            					}
            				

            					newCompositeNode(grammarAccess.getGuardParameterAccess().getParameterTypeAssetTypeCrossReference_3_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardParameter"


    // $ANTLR start "entryRuleGoal"
    // InternalAssetBasedSystemDsl.g:3224:1: entryRuleGoal returns [EObject current=null] : iv_ruleGoal= ruleGoal EOF ;
    public final EObject entryRuleGoal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGoal = null;


        try {
            // InternalAssetBasedSystemDsl.g:3224:45: (iv_ruleGoal= ruleGoal EOF )
            // InternalAssetBasedSystemDsl.g:3225:2: iv_ruleGoal= ruleGoal EOF
            {
             newCompositeNode(grammarAccess.getGoalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGoal=ruleGoal();

            state._fsp--;

             current =iv_ruleGoal; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGoal"


    // $ANTLR start "ruleGoal"
    // InternalAssetBasedSystemDsl.g:3231:1: ruleGoal returns [EObject current=null] : ( () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}' ) ;
    public final EObject ruleGoal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_description_7_0=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        Enumerator lv_descriptionFormat_6_0 = null;

        EObject lv_annotations_10_0 = null;

        EObject lv_precondition_14_0 = null;

        EObject lv_postcondition_17_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3237:2: ( ( () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}' ) )
            // InternalAssetBasedSystemDsl.g:3238:2: ( () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}' )
            {
            // InternalAssetBasedSystemDsl.g:3238:2: ( () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}' )
            // InternalAssetBasedSystemDsl.g:3239:3: () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}'
            {
            // InternalAssetBasedSystemDsl.g:3239:3: ()
            // InternalAssetBasedSystemDsl.g:3240:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGoalAccess().getGoalAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,58,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getGoalAccess().getGoalKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:3250:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAssetBasedSystemDsl.g:3251:4: (lv_name_2_0= ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:3251:4: (lv_name_2_0= ruleEString )
            // InternalAssetBasedSystemDsl.g:3252:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getGoalAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_7);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGoalRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_54); 

            			newLeafNode(otherlv_3, grammarAccess.getGoalAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:3273:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) )
            // InternalAssetBasedSystemDsl.g:3274:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) )
            {
            // InternalAssetBasedSystemDsl.g:3274:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) )
            // InternalAssetBasedSystemDsl.g:3275:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getGoalAccess().getUnorderedGroup_4());
            				
            // InternalAssetBasedSystemDsl.g:3278:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* )
            // InternalAssetBasedSystemDsl.g:3279:6: ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )*
            {
            // InternalAssetBasedSystemDsl.g:3279:6: ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )*
            loop68:
            do {
                int alt68=3;
                int LA68_0 = input.LA(1);

                if ( LA68_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 0) ) {
                    alt68=1;
                }
                else if ( LA68_0 == 18 && getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 1) ) {
                    alt68=2;
                }


                switch (alt68) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:3280:4: ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3280:4: ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:3281:5: {...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleGoal", "getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 0)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:3281:101: ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:3282:6: ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 0);
            	    					
            	    // InternalAssetBasedSystemDsl.g:3285:9: ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:3285:10: {...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGoal", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:3285:19: (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )
            	    // InternalAssetBasedSystemDsl.g:3285:20: otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) )
            	    {
            	    otherlv_5=(Token)match(input,26,FOLLOW_20); 

            	    									newLeafNode(otherlv_5, grammarAccess.getGoalAccess().getDescriptionKeyword_4_0_0());
            	    								
            	    // InternalAssetBasedSystemDsl.g:3289:9: ( (lv_descriptionFormat_6_0= ruleTextFormat ) )?
            	    int alt66=2;
            	    int LA66_0 = input.LA(1);

            	    if ( ((LA66_0>=100 && LA66_0<=102)) ) {
            	        alt66=1;
            	    }
            	    switch (alt66) {
            	        case 1 :
            	            // InternalAssetBasedSystemDsl.g:3290:10: (lv_descriptionFormat_6_0= ruleTextFormat )
            	            {
            	            // InternalAssetBasedSystemDsl.g:3290:10: (lv_descriptionFormat_6_0= ruleTextFormat )
            	            // InternalAssetBasedSystemDsl.g:3291:11: lv_descriptionFormat_6_0= ruleTextFormat
            	            {

            	            											newCompositeNode(grammarAccess.getGoalAccess().getDescriptionFormatTextFormatEnumRuleCall_4_0_1_0());
            	            										
            	            pushFollow(FOLLOW_21);
            	            lv_descriptionFormat_6_0=ruleTextFormat();

            	            state._fsp--;


            	            											if (current==null) {
            	            												current = createModelElementForParent(grammarAccess.getGoalRule());
            	            											}
            	            											set(
            	            												current,
            	            												"descriptionFormat",
            	            												lv_descriptionFormat_6_0,
            	            												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
            	            											afterParserOrEnumRuleCall();
            	            										

            	            }


            	            }
            	            break;

            	    }

            	    // InternalAssetBasedSystemDsl.g:3308:9: ( (lv_description_7_0= RULE_STRING ) )
            	    // InternalAssetBasedSystemDsl.g:3309:10: (lv_description_7_0= RULE_STRING )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3309:10: (lv_description_7_0= RULE_STRING )
            	    // InternalAssetBasedSystemDsl.g:3310:11: lv_description_7_0= RULE_STRING
            	    {
            	    lv_description_7_0=(Token)match(input,RULE_STRING,FOLLOW_54); 

            	    											newLeafNode(lv_description_7_0, grammarAccess.getGoalAccess().getDescriptionSTRINGTerminalRuleCall_4_0_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getGoalRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"description",
            	    												lv_description_7_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGoalAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalAssetBasedSystemDsl.g:3332:4: ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3332:4: ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) )
            	    // InternalAssetBasedSystemDsl.g:3333:5: {...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleGoal", "getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 1)");
            	    }
            	    // InternalAssetBasedSystemDsl.g:3333:101: ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) )
            	    // InternalAssetBasedSystemDsl.g:3334:6: ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 1);
            	    					
            	    // InternalAssetBasedSystemDsl.g:3337:9: ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) )
            	    // InternalAssetBasedSystemDsl.g:3337:10: {...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGoal", "true");
            	    }
            	    // InternalAssetBasedSystemDsl.g:3337:19: (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )
            	    // InternalAssetBasedSystemDsl.g:3337:20: otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}'
            	    {
            	    otherlv_8=(Token)match(input,18,FOLLOW_7); 

            	    									newLeafNode(otherlv_8, grammarAccess.getGoalAccess().getAnnotationsKeyword_4_1_0());
            	    								
            	    otherlv_9=(Token)match(input,13,FOLLOW_13); 

            	    									newLeafNode(otherlv_9, grammarAccess.getGoalAccess().getLeftCurlyBracketKeyword_4_1_1());
            	    								
            	    // InternalAssetBasedSystemDsl.g:3345:9: ( (lv_annotations_10_0= ruleAnnotationEntry ) )*
            	    loop67:
            	    do {
            	        int alt67=2;
            	        int LA67_0 = input.LA(1);

            	        if ( ((LA67_0>=RULE_STRING && LA67_0<=RULE_ID)) ) {
            	            alt67=1;
            	        }


            	        switch (alt67) {
            	    	case 1 :
            	    	    // InternalAssetBasedSystemDsl.g:3346:10: (lv_annotations_10_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalAssetBasedSystemDsl.g:3346:10: (lv_annotations_10_0= ruleAnnotationEntry )
            	    	    // InternalAssetBasedSystemDsl.g:3347:11: lv_annotations_10_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getGoalAccess().getAnnotationsAnnotationEntryParserRuleCall_4_1_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_13);
            	    	    lv_annotations_10_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getGoalRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_10_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop67;
            	        }
            	    } while (true);

            	    otherlv_11=(Token)match(input,19,FOLLOW_54); 

            	    									newLeafNode(otherlv_11, grammarAccess.getGoalAccess().getRightCurlyBracketKeyword_4_1_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGoalAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop68;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getGoalAccess().getUnorderedGroup_4());
            				

            }

            // InternalAssetBasedSystemDsl.g:3381:3: (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )?
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==59) ) {
                alt69=1;
            }
            switch (alt69) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:3382:4: otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) )
                    {
                    otherlv_12=(Token)match(input,59,FOLLOW_46); 

                    				newLeafNode(otherlv_12, grammarAccess.getGoalAccess().getPreKeyword_5_0());
                    			
                    otherlv_13=(Token)match(input,53,FOLLOW_48); 

                    				newLeafNode(otherlv_13, grammarAccess.getGoalAccess().getEqualsSignKeyword_5_1());
                    			
                    // InternalAssetBasedSystemDsl.g:3390:4: ( (lv_precondition_14_0= ruleExpression ) )
                    // InternalAssetBasedSystemDsl.g:3391:5: (lv_precondition_14_0= ruleExpression )
                    {
                    // InternalAssetBasedSystemDsl.g:3391:5: (lv_precondition_14_0= ruleExpression )
                    // InternalAssetBasedSystemDsl.g:3392:6: lv_precondition_14_0= ruleExpression
                    {

                    						newCompositeNode(grammarAccess.getGoalAccess().getPreconditionExpressionParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_55);
                    lv_precondition_14_0=ruleExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGoalRule());
                    						}
                    						set(
                    							current,
                    							"precondition",
                    							lv_precondition_14_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:3410:3: (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )?
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==60) ) {
                alt70=1;
            }
            switch (alt70) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:3411:4: otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) )
                    {
                    otherlv_15=(Token)match(input,60,FOLLOW_46); 

                    				newLeafNode(otherlv_15, grammarAccess.getGoalAccess().getPostKeyword_6_0());
                    			
                    otherlv_16=(Token)match(input,53,FOLLOW_48); 

                    				newLeafNode(otherlv_16, grammarAccess.getGoalAccess().getEqualsSignKeyword_6_1());
                    			
                    // InternalAssetBasedSystemDsl.g:3419:4: ( (lv_postcondition_17_0= ruleExpression ) )
                    // InternalAssetBasedSystemDsl.g:3420:5: (lv_postcondition_17_0= ruleExpression )
                    {
                    // InternalAssetBasedSystemDsl.g:3420:5: (lv_postcondition_17_0= ruleExpression )
                    // InternalAssetBasedSystemDsl.g:3421:6: lv_postcondition_17_0= ruleExpression
                    {

                    						newCompositeNode(grammarAccess.getGoalAccess().getPostconditionExpressionParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_49);
                    lv_postcondition_17_0=ruleExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGoalRule());
                    						}
                    						set(
                    							current,
                    							"postcondition",
                    							lv_postcondition_17_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_18=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_18, grammarAccess.getGoalAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGoal"


    // $ANTLR start "entryRuleAnnotationEntry"
    // InternalAssetBasedSystemDsl.g:3447:1: entryRuleAnnotationEntry returns [EObject current=null] : iv_ruleAnnotationEntry= ruleAnnotationEntry EOF ;
    public final EObject entryRuleAnnotationEntry() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotationEntry = null;


        try {
            // InternalAssetBasedSystemDsl.g:3447:56: (iv_ruleAnnotationEntry= ruleAnnotationEntry EOF )
            // InternalAssetBasedSystemDsl.g:3448:2: iv_ruleAnnotationEntry= ruleAnnotationEntry EOF
            {
             newCompositeNode(grammarAccess.getAnnotationEntryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnnotationEntry=ruleAnnotationEntry();

            state._fsp--;

             current =iv_ruleAnnotationEntry; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotationEntry"


    // $ANTLR start "ruleAnnotationEntry"
    // InternalAssetBasedSystemDsl.g:3454:1: ruleAnnotationEntry returns [EObject current=null] : ( () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) ;
    public final EObject ruleAnnotationEntry() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_value_3_0=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3460:2: ( ( () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) )
            // InternalAssetBasedSystemDsl.g:3461:2: ( () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
            {
            // InternalAssetBasedSystemDsl.g:3461:2: ( () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
            // InternalAssetBasedSystemDsl.g:3462:3: () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) )
            {
            // InternalAssetBasedSystemDsl.g:3462:3: ()
            // InternalAssetBasedSystemDsl.g:3463:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAnnotationEntryAccess().getAnnotationEntryAction_0(),
            					current);
            			

            }

            // InternalAssetBasedSystemDsl.g:3469:3: ( ( ruleEString ) )
            // InternalAssetBasedSystemDsl.g:3470:4: ( ruleEString )
            {
            // InternalAssetBasedSystemDsl.g:3470:4: ( ruleEString )
            // InternalAssetBasedSystemDsl.g:3471:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAnnotationEntryRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAnnotationEntryAccess().getKeyAnnotationKeyCrossReference_1_0());
            				
            pushFollow(FOLLOW_46);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,53,FOLLOW_21); 

            			newLeafNode(otherlv_2, grammarAccess.getAnnotationEntryAccess().getEqualsSignKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:3489:3: ( (lv_value_3_0= RULE_STRING ) )
            // InternalAssetBasedSystemDsl.g:3490:4: (lv_value_3_0= RULE_STRING )
            {
            // InternalAssetBasedSystemDsl.g:3490:4: (lv_value_3_0= RULE_STRING )
            // InternalAssetBasedSystemDsl.g:3491:5: lv_value_3_0= RULE_STRING
            {
            lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_value_3_0, grammarAccess.getAnnotationEntryAccess().getValueSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAnnotationEntryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_3_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotationEntry"


    // $ANTLR start "entryRuleExpression"
    // InternalAssetBasedSystemDsl.g:3511:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:3511:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalAssetBasedSystemDsl.g:3512:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalAssetBasedSystemDsl.g:3518:1: ruleExpression returns [EObject current=null] : this_ImpliesExpression_0= ruleImpliesExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ImpliesExpression_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3524:2: (this_ImpliesExpression_0= ruleImpliesExpression )
            // InternalAssetBasedSystemDsl.g:3525:2: this_ImpliesExpression_0= ruleImpliesExpression
            {

            		newCompositeNode(grammarAccess.getExpressionAccess().getImpliesExpressionParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_ImpliesExpression_0=ruleImpliesExpression();

            state._fsp--;


            		current = this_ImpliesExpression_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleImpliesExpression"
    // InternalAssetBasedSystemDsl.g:3536:1: entryRuleImpliesExpression returns [EObject current=null] : iv_ruleImpliesExpression= ruleImpliesExpression EOF ;
    public final EObject entryRuleImpliesExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImpliesExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:3536:58: (iv_ruleImpliesExpression= ruleImpliesExpression EOF )
            // InternalAssetBasedSystemDsl.g:3537:2: iv_ruleImpliesExpression= ruleImpliesExpression EOF
            {
             newCompositeNode(grammarAccess.getImpliesExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImpliesExpression=ruleImpliesExpression();

            state._fsp--;

             current =iv_ruleImpliesExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImpliesExpression"


    // $ANTLR start "ruleImpliesExpression"
    // InternalAssetBasedSystemDsl.g:3543:1: ruleImpliesExpression returns [EObject current=null] : (this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )* ) ;
    public final EObject ruleImpliesExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_OrExpression_0 = null;

        EObject lv_rhs_3_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3549:2: ( (this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )* ) )
            // InternalAssetBasedSystemDsl.g:3550:2: (this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )* )
            {
            // InternalAssetBasedSystemDsl.g:3550:2: (this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )* )
            // InternalAssetBasedSystemDsl.g:3551:3: this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getImpliesExpressionAccess().getOrExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_56);
            this_OrExpression_0=ruleOrExpression();

            state._fsp--;


            			current = this_OrExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalAssetBasedSystemDsl.g:3559:3: ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )*
            loop71:
            do {
                int alt71=2;
                int LA71_0 = input.LA(1);

                if ( (LA71_0==61) ) {
                    alt71=1;
                }


                switch (alt71) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:3560:4: () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3560:4: ()
            	    // InternalAssetBasedSystemDsl.g:3561:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLhsAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,61,FOLLOW_48); 

            	    				newLeafNode(otherlv_2, grammarAccess.getImpliesExpressionAccess().getEqualsSignGreaterThanSignKeyword_1_1());
            	    			
            	    // InternalAssetBasedSystemDsl.g:3571:4: ( (lv_rhs_3_0= ruleOrExpression ) )
            	    // InternalAssetBasedSystemDsl.g:3572:5: (lv_rhs_3_0= ruleOrExpression )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3572:5: (lv_rhs_3_0= ruleOrExpression )
            	    // InternalAssetBasedSystemDsl.g:3573:6: lv_rhs_3_0= ruleOrExpression
            	    {

            	    						newCompositeNode(grammarAccess.getImpliesExpressionAccess().getRhsOrExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_56);
            	    lv_rhs_3_0=ruleOrExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getImpliesExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rhs",
            	    							lv_rhs_3_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.OrExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop71;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImpliesExpression"


    // $ANTLR start "entryRuleOrExpression"
    // InternalAssetBasedSystemDsl.g:3595:1: entryRuleOrExpression returns [EObject current=null] : iv_ruleOrExpression= ruleOrExpression EOF ;
    public final EObject entryRuleOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:3595:53: (iv_ruleOrExpression= ruleOrExpression EOF )
            // InternalAssetBasedSystemDsl.g:3596:2: iv_ruleOrExpression= ruleOrExpression EOF
            {
             newCompositeNode(grammarAccess.getOrExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOrExpression=ruleOrExpression();

            state._fsp--;

             current =iv_ruleOrExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrExpression"


    // $ANTLR start "ruleOrExpression"
    // InternalAssetBasedSystemDsl.g:3602:1: ruleOrExpression returns [EObject current=null] : (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )* ) ;
    public final EObject ruleOrExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_AndExpression_0 = null;

        EObject lv_rhs_3_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3608:2: ( (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )* ) )
            // InternalAssetBasedSystemDsl.g:3609:2: (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )* )
            {
            // InternalAssetBasedSystemDsl.g:3609:2: (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )* )
            // InternalAssetBasedSystemDsl.g:3610:3: this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrExpressionAccess().getAndExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_57);
            this_AndExpression_0=ruleAndExpression();

            state._fsp--;


            			current = this_AndExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalAssetBasedSystemDsl.g:3618:3: ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )*
            loop72:
            do {
                int alt72=2;
                int LA72_0 = input.LA(1);

                if ( (LA72_0==62) ) {
                    alt72=1;
                }


                switch (alt72) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:3619:4: () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3619:4: ()
            	    // InternalAssetBasedSystemDsl.g:3620:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrExpressionAccess().getOrExpressionLhsAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,62,FOLLOW_48); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrExpressionAccess().getVerticalLineVerticalLineKeyword_1_1());
            	    			
            	    // InternalAssetBasedSystemDsl.g:3630:4: ( (lv_rhs_3_0= ruleAndExpression ) )
            	    // InternalAssetBasedSystemDsl.g:3631:5: (lv_rhs_3_0= ruleAndExpression )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3631:5: (lv_rhs_3_0= ruleAndExpression )
            	    // InternalAssetBasedSystemDsl.g:3632:6: lv_rhs_3_0= ruleAndExpression
            	    {

            	    						newCompositeNode(grammarAccess.getOrExpressionAccess().getRhsAndExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_57);
            	    lv_rhs_3_0=ruleAndExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rhs",
            	    							lv_rhs_3_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AndExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop72;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrExpression"


    // $ANTLR start "entryRuleAndExpression"
    // InternalAssetBasedSystemDsl.g:3654:1: entryRuleAndExpression returns [EObject current=null] : iv_ruleAndExpression= ruleAndExpression EOF ;
    public final EObject entryRuleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:3654:54: (iv_ruleAndExpression= ruleAndExpression EOF )
            // InternalAssetBasedSystemDsl.g:3655:2: iv_ruleAndExpression= ruleAndExpression EOF
            {
             newCompositeNode(grammarAccess.getAndExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAndExpression=ruleAndExpression();

            state._fsp--;

             current =iv_ruleAndExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpression"


    // $ANTLR start "ruleAndExpression"
    // InternalAssetBasedSystemDsl.g:3661:1: ruleAndExpression returns [EObject current=null] : (this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )* ) ;
    public final EObject ruleAndExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_NotExpression_0 = null;

        EObject lv_rhs_3_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3667:2: ( (this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )* ) )
            // InternalAssetBasedSystemDsl.g:3668:2: (this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )* )
            {
            // InternalAssetBasedSystemDsl.g:3668:2: (this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )* )
            // InternalAssetBasedSystemDsl.g:3669:3: this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndExpressionAccess().getNotExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_58);
            this_NotExpression_0=ruleNotExpression();

            state._fsp--;


            			current = this_NotExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalAssetBasedSystemDsl.g:3677:3: ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )*
            loop73:
            do {
                int alt73=2;
                int LA73_0 = input.LA(1);

                if ( (LA73_0==63) ) {
                    alt73=1;
                }


                switch (alt73) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:3678:4: () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3678:4: ()
            	    // InternalAssetBasedSystemDsl.g:3679:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndExpressionAccess().getAndExpressionLhsAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,63,FOLLOW_48); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAndExpressionAccess().getAmpersandAmpersandKeyword_1_1());
            	    			
            	    // InternalAssetBasedSystemDsl.g:3689:4: ( (lv_rhs_3_0= ruleNotExpression ) )
            	    // InternalAssetBasedSystemDsl.g:3690:5: (lv_rhs_3_0= ruleNotExpression )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3690:5: (lv_rhs_3_0= ruleNotExpression )
            	    // InternalAssetBasedSystemDsl.g:3691:6: lv_rhs_3_0= ruleNotExpression
            	    {

            	    						newCompositeNode(grammarAccess.getAndExpressionAccess().getRhsNotExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_58);
            	    lv_rhs_3_0=ruleNotExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rhs",
            	    							lv_rhs_3_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.NotExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop73;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpression"


    // $ANTLR start "entryRuleNotExpression"
    // InternalAssetBasedSystemDsl.g:3713:1: entryRuleNotExpression returns [EObject current=null] : iv_ruleNotExpression= ruleNotExpression EOF ;
    public final EObject entryRuleNotExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:3713:54: (iv_ruleNotExpression= ruleNotExpression EOF )
            // InternalAssetBasedSystemDsl.g:3714:2: iv_ruleNotExpression= ruleNotExpression EOF
            {
             newCompositeNode(grammarAccess.getNotExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNotExpression=ruleNotExpression();

            state._fsp--;

             current =iv_ruleNotExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotExpression"


    // $ANTLR start "ruleNotExpression"
    // InternalAssetBasedSystemDsl.g:3720:1: ruleNotExpression returns [EObject current=null] : ( ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) ) | this_ComparisonExpression_3= ruleComparisonExpression ) ;
    public final EObject ruleNotExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;

        EObject this_ComparisonExpression_3 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3726:2: ( ( ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) ) | this_ComparisonExpression_3= ruleComparisonExpression ) )
            // InternalAssetBasedSystemDsl.g:3727:2: ( ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) ) | this_ComparisonExpression_3= ruleComparisonExpression )
            {
            // InternalAssetBasedSystemDsl.g:3727:2: ( ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) ) | this_ComparisonExpression_3= ruleComparisonExpression )
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==64) ) {
                alt74=1;
            }
            else if ( ((LA74_0>=RULE_STRING && LA74_0<=RULE_INT)||LA74_0==15||(LA74_0>=36 && LA74_0<=37)||(LA74_0>=47 && LA74_0<=48)) ) {
                alt74=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 74, 0, input);

                throw nvae;
            }
            switch (alt74) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:3728:3: ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:3728:3: ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) )
                    // InternalAssetBasedSystemDsl.g:3729:4: () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) )
                    {
                    // InternalAssetBasedSystemDsl.g:3729:4: ()
                    // InternalAssetBasedSystemDsl.g:3730:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getNotExpressionAccess().getNotExpressionAction_0_0(),
                    						current);
                    				

                    }

                    otherlv_1=(Token)match(input,64,FOLLOW_48); 

                    				newLeafNode(otherlv_1, grammarAccess.getNotExpressionAccess().getExclamationMarkKeyword_0_1());
                    			
                    // InternalAssetBasedSystemDsl.g:3740:4: ( (lv_expression_2_0= ruleNotExpression ) )
                    // InternalAssetBasedSystemDsl.g:3741:5: (lv_expression_2_0= ruleNotExpression )
                    {
                    // InternalAssetBasedSystemDsl.g:3741:5: (lv_expression_2_0= ruleNotExpression )
                    // InternalAssetBasedSystemDsl.g:3742:6: lv_expression_2_0= ruleNotExpression
                    {

                    						newCompositeNode(grammarAccess.getNotExpressionAccess().getExpressionNotExpressionParserRuleCall_0_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_expression_2_0=ruleNotExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getNotExpressionRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_2_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.NotExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:3761:3: this_ComparisonExpression_3= ruleComparisonExpression
                    {

                    			newCompositeNode(grammarAccess.getNotExpressionAccess().getComparisonExpressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ComparisonExpression_3=ruleComparisonExpression();

                    state._fsp--;


                    			current = this_ComparisonExpression_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotExpression"


    // $ANTLR start "entryRuleComparisonExpression"
    // InternalAssetBasedSystemDsl.g:3773:1: entryRuleComparisonExpression returns [EObject current=null] : iv_ruleComparisonExpression= ruleComparisonExpression EOF ;
    public final EObject entryRuleComparisonExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparisonExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:3773:61: (iv_ruleComparisonExpression= ruleComparisonExpression EOF )
            // InternalAssetBasedSystemDsl.g:3774:2: iv_ruleComparisonExpression= ruleComparisonExpression EOF
            {
             newCompositeNode(grammarAccess.getComparisonExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComparisonExpression=ruleComparisonExpression();

            state._fsp--;

             current =iv_ruleComparisonExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparisonExpression"


    // $ANTLR start "ruleComparisonExpression"
    // InternalAssetBasedSystemDsl.g:3780:1: ruleComparisonExpression returns [EObject current=null] : (this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) ) ) ;
    public final EObject ruleComparisonExpression() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        Token lv_op_5_1=null;
        Token lv_op_5_2=null;
        Token lv_op_5_3=null;
        Token lv_op_5_4=null;
        EObject this_SelectionExpression_0 = null;

        EObject lv_rhs_3_0 = null;

        EObject lv_rhs_6_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3786:2: ( (this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) ) ) )
            // InternalAssetBasedSystemDsl.g:3787:2: (this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) ) )
            {
            // InternalAssetBasedSystemDsl.g:3787:2: (this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) ) )
            // InternalAssetBasedSystemDsl.g:3788:3: this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) )
            {

            			newCompositeNode(grammarAccess.getComparisonExpressionAccess().getSelectionExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_59);
            this_SelectionExpression_0=ruleSelectionExpression();

            state._fsp--;


            			current = this_SelectionExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalAssetBasedSystemDsl.g:3796:3: ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) )
            int alt78=2;
            int LA78_0 = input.LA(1);

            if ( (LA78_0==EOF||(LA78_0>=16 && LA78_0<=17)||LA78_0==19||LA78_0==38||LA78_0==56||(LA78_0>=60 && LA78_0<=63)||(LA78_0>=65 && LA78_0<=66)) ) {
                alt78=1;
            }
            else if ( ((LA78_0>=67 && LA78_0<=70)) ) {
                alt78=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 78, 0, input);

                throw nvae;
            }
            switch (alt78) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:3797:4: ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )*
                    {
                    // InternalAssetBasedSystemDsl.g:3797:4: ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )*
                    loop76:
                    do {
                        int alt76=2;
                        int LA76_0 = input.LA(1);

                        if ( ((LA76_0>=65 && LA76_0<=66)) ) {
                            alt76=1;
                        }


                        switch (alt76) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:3798:5: () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:3798:5: ()
                    	    // InternalAssetBasedSystemDsl.g:3799:6: 
                    	    {

                    	    						current = forceCreateModelElementAndSet(
                    	    							grammarAccess.getComparisonExpressionAccess().getEqualityComparisonExpressionLhsAction_1_0_0(),
                    	    							current);
                    	    					

                    	    }

                    	    // InternalAssetBasedSystemDsl.g:3805:5: ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) )
                    	    // InternalAssetBasedSystemDsl.g:3806:6: ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:3806:6: ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) )
                    	    // InternalAssetBasedSystemDsl.g:3807:7: (lv_op_2_1= '==' | lv_op_2_2= '!=' )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:3807:7: (lv_op_2_1= '==' | lv_op_2_2= '!=' )
                    	    int alt75=2;
                    	    int LA75_0 = input.LA(1);

                    	    if ( (LA75_0==65) ) {
                    	        alt75=1;
                    	    }
                    	    else if ( (LA75_0==66) ) {
                    	        alt75=2;
                    	    }
                    	    else {
                    	        NoViableAltException nvae =
                    	            new NoViableAltException("", 75, 0, input);

                    	        throw nvae;
                    	    }
                    	    switch (alt75) {
                    	        case 1 :
                    	            // InternalAssetBasedSystemDsl.g:3808:8: lv_op_2_1= '=='
                    	            {
                    	            lv_op_2_1=(Token)match(input,65,FOLLOW_48); 

                    	            								newLeafNode(lv_op_2_1, grammarAccess.getComparisonExpressionAccess().getOpEqualsSignEqualsSignKeyword_1_0_1_0_0());
                    	            							

                    	            								if (current==null) {
                    	            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                    	            								}
                    	            								setWithLastConsumed(current, "op", lv_op_2_1, null);
                    	            							

                    	            }
                    	            break;
                    	        case 2 :
                    	            // InternalAssetBasedSystemDsl.g:3819:8: lv_op_2_2= '!='
                    	            {
                    	            lv_op_2_2=(Token)match(input,66,FOLLOW_48); 

                    	            								newLeafNode(lv_op_2_2, grammarAccess.getComparisonExpressionAccess().getOpExclamationMarkEqualsSignKeyword_1_0_1_0_1());
                    	            							

                    	            								if (current==null) {
                    	            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                    	            								}
                    	            								setWithLastConsumed(current, "op", lv_op_2_2, null);
                    	            							

                    	            }
                    	            break;

                    	    }


                    	    }


                    	    }

                    	    // InternalAssetBasedSystemDsl.g:3832:5: ( (lv_rhs_3_0= ruleSelectionExpression ) )
                    	    // InternalAssetBasedSystemDsl.g:3833:6: (lv_rhs_3_0= ruleSelectionExpression )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:3833:6: (lv_rhs_3_0= ruleSelectionExpression )
                    	    // InternalAssetBasedSystemDsl.g:3834:7: lv_rhs_3_0= ruleSelectionExpression
                    	    {

                    	    							newCompositeNode(grammarAccess.getComparisonExpressionAccess().getRhsSelectionExpressionParserRuleCall_1_0_2_0());
                    	    						
                    	    pushFollow(FOLLOW_60);
                    	    lv_rhs_3_0=ruleSelectionExpression();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getComparisonExpressionRule());
                    	    							}
                    	    							set(
                    	    								current,
                    	    								"rhs",
                    	    								lv_rhs_3_0,
                    	    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.SelectionExpression");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop76;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:3853:4: ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:3853:4: ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) )
                    // InternalAssetBasedSystemDsl.g:3854:5: () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) )
                    {
                    // InternalAssetBasedSystemDsl.g:3854:5: ()
                    // InternalAssetBasedSystemDsl.g:3855:6: 
                    {

                    						current = forceCreateModelElementAndSet(
                    							grammarAccess.getComparisonExpressionAccess().getInequalityComparisonExpressionLhsAction_1_1_0(),
                    							current);
                    					

                    }

                    // InternalAssetBasedSystemDsl.g:3861:5: ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) )
                    // InternalAssetBasedSystemDsl.g:3862:6: ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) )
                    {
                    // InternalAssetBasedSystemDsl.g:3862:6: ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) )
                    // InternalAssetBasedSystemDsl.g:3863:7: (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' )
                    {
                    // InternalAssetBasedSystemDsl.g:3863:7: (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' )
                    int alt77=4;
                    switch ( input.LA(1) ) {
                    case 67:
                        {
                        alt77=1;
                        }
                        break;
                    case 68:
                        {
                        alt77=2;
                        }
                        break;
                    case 69:
                        {
                        alt77=3;
                        }
                        break;
                    case 70:
                        {
                        alt77=4;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 77, 0, input);

                        throw nvae;
                    }

                    switch (alt77) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:3864:8: lv_op_5_1= '<'
                            {
                            lv_op_5_1=(Token)match(input,67,FOLLOW_48); 

                            								newLeafNode(lv_op_5_1, grammarAccess.getComparisonExpressionAccess().getOpLessThanSignKeyword_1_1_1_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                            								}
                            								setWithLastConsumed(current, "op", lv_op_5_1, null);
                            							

                            }
                            break;
                        case 2 :
                            // InternalAssetBasedSystemDsl.g:3875:8: lv_op_5_2= '<='
                            {
                            lv_op_5_2=(Token)match(input,68,FOLLOW_48); 

                            								newLeafNode(lv_op_5_2, grammarAccess.getComparisonExpressionAccess().getOpLessThanSignEqualsSignKeyword_1_1_1_0_1());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                            								}
                            								setWithLastConsumed(current, "op", lv_op_5_2, null);
                            							

                            }
                            break;
                        case 3 :
                            // InternalAssetBasedSystemDsl.g:3886:8: lv_op_5_3= '>'
                            {
                            lv_op_5_3=(Token)match(input,69,FOLLOW_48); 

                            								newLeafNode(lv_op_5_3, grammarAccess.getComparisonExpressionAccess().getOpGreaterThanSignKeyword_1_1_1_0_2());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                            								}
                            								setWithLastConsumed(current, "op", lv_op_5_3, null);
                            							

                            }
                            break;
                        case 4 :
                            // InternalAssetBasedSystemDsl.g:3897:8: lv_op_5_4= '>='
                            {
                            lv_op_5_4=(Token)match(input,70,FOLLOW_48); 

                            								newLeafNode(lv_op_5_4, grammarAccess.getComparisonExpressionAccess().getOpGreaterThanSignEqualsSignKeyword_1_1_1_0_3());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                            								}
                            								setWithLastConsumed(current, "op", lv_op_5_4, null);
                            							

                            }
                            break;

                    }


                    }


                    }

                    // InternalAssetBasedSystemDsl.g:3910:5: ( (lv_rhs_6_0= ruleSelectionExpression ) )
                    // InternalAssetBasedSystemDsl.g:3911:6: (lv_rhs_6_0= ruleSelectionExpression )
                    {
                    // InternalAssetBasedSystemDsl.g:3911:6: (lv_rhs_6_0= ruleSelectionExpression )
                    // InternalAssetBasedSystemDsl.g:3912:7: lv_rhs_6_0= ruleSelectionExpression
                    {

                    							newCompositeNode(grammarAccess.getComparisonExpressionAccess().getRhsSelectionExpressionParserRuleCall_1_1_2_0());
                    						
                    pushFollow(FOLLOW_2);
                    lv_rhs_6_0=ruleSelectionExpression();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getComparisonExpressionRule());
                    							}
                    							set(
                    								current,
                    								"rhs",
                    								lv_rhs_6_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.SelectionExpression");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparisonExpression"


    // $ANTLR start "entryRuleSelectionExpression"
    // InternalAssetBasedSystemDsl.g:3935:1: entryRuleSelectionExpression returns [EObject current=null] : iv_ruleSelectionExpression= ruleSelectionExpression EOF ;
    public final EObject entryRuleSelectionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSelectionExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:3935:60: (iv_ruleSelectionExpression= ruleSelectionExpression EOF )
            // InternalAssetBasedSystemDsl.g:3936:2: iv_ruleSelectionExpression= ruleSelectionExpression EOF
            {
             newCompositeNode(grammarAccess.getSelectionExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSelectionExpression=ruleSelectionExpression();

            state._fsp--;

             current =iv_ruleSelectionExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSelectionExpression"


    // $ANTLR start "ruleSelectionExpression"
    // InternalAssetBasedSystemDsl.g:3942:1: ruleSelectionExpression returns [EObject current=null] : (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )* ) ;
    public final EObject ruleSelectionExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_methodInvocation_4_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject this_TerminalExpression_0 = null;

        EObject lv_args_5_0 = null;

        EObject lv_args_7_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:3948:2: ( (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )* ) )
            // InternalAssetBasedSystemDsl.g:3949:2: (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )* )
            {
            // InternalAssetBasedSystemDsl.g:3949:2: (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )* )
            // InternalAssetBasedSystemDsl.g:3950:3: this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )*
            {

            			newCompositeNode(grammarAccess.getSelectionExpressionAccess().getTerminalExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_61);
            this_TerminalExpression_0=ruleTerminalExpression();

            state._fsp--;


            			current = this_TerminalExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalAssetBasedSystemDsl.g:3958:3: ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )*
            loop82:
            do {
                int alt82=2;
                int LA82_0 = input.LA(1);

                if ( (LA82_0==71) ) {
                    alt82=1;
                }


                switch (alt82) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:3959:4: () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )?
            	    {
            	    // InternalAssetBasedSystemDsl.g:3959:4: ()
            	    // InternalAssetBasedSystemDsl.g:3960:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getSelectionExpressionAccess().getMemberSelectionReceiverAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,71,FOLLOW_12); 

            	    				newLeafNode(otherlv_2, grammarAccess.getSelectionExpressionAccess().getFullStopKeyword_1_1());
            	    			
            	    // InternalAssetBasedSystemDsl.g:3970:4: ( ( ruleQualifiedName ) )
            	    // InternalAssetBasedSystemDsl.g:3971:5: ( ruleQualifiedName )
            	    {
            	    // InternalAssetBasedSystemDsl.g:3971:5: ( ruleQualifiedName )
            	    // InternalAssetBasedSystemDsl.g:3972:6: ruleQualifiedName
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getSelectionExpressionRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getSelectionExpressionAccess().getMemberMemberCrossReference_1_2_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    ruleQualifiedName();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalAssetBasedSystemDsl.g:3986:4: ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )?
            	    int alt81=2;
            	    int LA81_0 = input.LA(1);

            	    if ( (LA81_0==15) ) {
            	        alt81=1;
            	    }
            	    switch (alt81) {
            	        case 1 :
            	            // InternalAssetBasedSystemDsl.g:3987:5: ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')'
            	            {
            	            // InternalAssetBasedSystemDsl.g:3987:5: ( (lv_methodInvocation_4_0= '(' ) )
            	            // InternalAssetBasedSystemDsl.g:3988:6: (lv_methodInvocation_4_0= '(' )
            	            {
            	            // InternalAssetBasedSystemDsl.g:3988:6: (lv_methodInvocation_4_0= '(' )
            	            // InternalAssetBasedSystemDsl.g:3989:7: lv_methodInvocation_4_0= '('
            	            {
            	            lv_methodInvocation_4_0=(Token)match(input,15,FOLLOW_63); 

            	            							newLeafNode(lv_methodInvocation_4_0, grammarAccess.getSelectionExpressionAccess().getMethodInvocationLeftParenthesisKeyword_1_3_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getSelectionExpressionRule());
            	            							}
            	            							setWithLastConsumed(current, "methodInvocation", lv_methodInvocation_4_0 != null, "(");
            	            						

            	            }


            	            }

            	            // InternalAssetBasedSystemDsl.g:4001:5: ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )?
            	            int alt80=2;
            	            int LA80_0 = input.LA(1);

            	            if ( ((LA80_0>=RULE_STRING && LA80_0<=RULE_INT)||LA80_0==13||LA80_0==15||(LA80_0>=36 && LA80_0<=37)||(LA80_0>=47 && LA80_0<=48)||LA80_0==64) ) {
            	                alt80=1;
            	            }
            	            switch (alt80) {
            	                case 1 :
            	                    // InternalAssetBasedSystemDsl.g:4002:6: ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )*
            	                    {
            	                    // InternalAssetBasedSystemDsl.g:4002:6: ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) )
            	                    // InternalAssetBasedSystemDsl.g:4003:7: (lv_args_5_0= ruleExpressionOrLambdaExpression )
            	                    {
            	                    // InternalAssetBasedSystemDsl.g:4003:7: (lv_args_5_0= ruleExpressionOrLambdaExpression )
            	                    // InternalAssetBasedSystemDsl.g:4004:8: lv_args_5_0= ruleExpressionOrLambdaExpression
            	                    {

            	                    								newCompositeNode(grammarAccess.getSelectionExpressionAccess().getArgsExpressionOrLambdaExpressionParserRuleCall_1_3_1_0_0());
            	                    							
            	                    pushFollow(FOLLOW_11);
            	                    lv_args_5_0=ruleExpressionOrLambdaExpression();

            	                    state._fsp--;


            	                    								if (current==null) {
            	                    									current = createModelElementForParent(grammarAccess.getSelectionExpressionRule());
            	                    								}
            	                    								add(
            	                    									current,
            	                    									"args",
            	                    									lv_args_5_0,
            	                    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ExpressionOrLambdaExpression");
            	                    								afterParserOrEnumRuleCall();
            	                    							

            	                    }


            	                    }

            	                    // InternalAssetBasedSystemDsl.g:4021:6: (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )*
            	                    loop79:
            	                    do {
            	                        int alt79=2;
            	                        int LA79_0 = input.LA(1);

            	                        if ( (LA79_0==16) ) {
            	                            alt79=1;
            	                        }


            	                        switch (alt79) {
            	                    	case 1 :
            	                    	    // InternalAssetBasedSystemDsl.g:4022:7: otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) )
            	                    	    {
            	                    	    otherlv_6=(Token)match(input,16,FOLLOW_48); 

            	                    	    							newLeafNode(otherlv_6, grammarAccess.getSelectionExpressionAccess().getCommaKeyword_1_3_1_1_0());
            	                    	    						
            	                    	    // InternalAssetBasedSystemDsl.g:4026:7: ( (lv_args_7_0= ruleExpression ) )
            	                    	    // InternalAssetBasedSystemDsl.g:4027:8: (lv_args_7_0= ruleExpression )
            	                    	    {
            	                    	    // InternalAssetBasedSystemDsl.g:4027:8: (lv_args_7_0= ruleExpression )
            	                    	    // InternalAssetBasedSystemDsl.g:4028:9: lv_args_7_0= ruleExpression
            	                    	    {

            	                    	    									newCompositeNode(grammarAccess.getSelectionExpressionAccess().getArgsExpressionParserRuleCall_1_3_1_1_1_0());
            	                    	    								
            	                    	    pushFollow(FOLLOW_11);
            	                    	    lv_args_7_0=ruleExpression();

            	                    	    state._fsp--;


            	                    	    									if (current==null) {
            	                    	    										current = createModelElementForParent(grammarAccess.getSelectionExpressionRule());
            	                    	    									}
            	                    	    									add(
            	                    	    										current,
            	                    	    										"args",
            	                    	    										lv_args_7_0,
            	                    	    										"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            	                    	    									afterParserOrEnumRuleCall();
            	                    	    								

            	                    	    }


            	                    	    }


            	                    	    }
            	                    	    break;

            	                    	default :
            	                    	    break loop79;
            	                        }
            	                    } while (true);


            	                    }
            	                    break;

            	            }

            	            otherlv_8=(Token)match(input,17,FOLLOW_61); 

            	            					newLeafNode(otherlv_8, grammarAccess.getSelectionExpressionAccess().getRightParenthesisKeyword_1_3_2());
            	            				

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop82;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSelectionExpression"


    // $ANTLR start "entryRuleExpressionOrLambdaExpression"
    // InternalAssetBasedSystemDsl.g:4057:1: entryRuleExpressionOrLambdaExpression returns [EObject current=null] : iv_ruleExpressionOrLambdaExpression= ruleExpressionOrLambdaExpression EOF ;
    public final EObject entryRuleExpressionOrLambdaExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionOrLambdaExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:4057:69: (iv_ruleExpressionOrLambdaExpression= ruleExpressionOrLambdaExpression EOF )
            // InternalAssetBasedSystemDsl.g:4058:2: iv_ruleExpressionOrLambdaExpression= ruleExpressionOrLambdaExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionOrLambdaExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpressionOrLambdaExpression=ruleExpressionOrLambdaExpression();

            state._fsp--;

             current =iv_ruleExpressionOrLambdaExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionOrLambdaExpression"


    // $ANTLR start "ruleExpressionOrLambdaExpression"
    // InternalAssetBasedSystemDsl.g:4064:1: ruleExpressionOrLambdaExpression returns [EObject current=null] : (this_LambdaExpression_0= ruleLambdaExpression | this_Expression_1= ruleExpression ) ;
    public final EObject ruleExpressionOrLambdaExpression() throws RecognitionException {
        EObject current = null;

        EObject this_LambdaExpression_0 = null;

        EObject this_Expression_1 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:4070:2: ( (this_LambdaExpression_0= ruleLambdaExpression | this_Expression_1= ruleExpression ) )
            // InternalAssetBasedSystemDsl.g:4071:2: (this_LambdaExpression_0= ruleLambdaExpression | this_Expression_1= ruleExpression )
            {
            // InternalAssetBasedSystemDsl.g:4071:2: (this_LambdaExpression_0= ruleLambdaExpression | this_Expression_1= ruleExpression )
            int alt83=2;
            int LA83_0 = input.LA(1);

            if ( (LA83_0==13) ) {
                alt83=1;
            }
            else if ( ((LA83_0>=RULE_STRING && LA83_0<=RULE_INT)||LA83_0==15||(LA83_0>=36 && LA83_0<=37)||(LA83_0>=47 && LA83_0<=48)||LA83_0==64) ) {
                alt83=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 83, 0, input);

                throw nvae;
            }
            switch (alt83) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:4072:3: this_LambdaExpression_0= ruleLambdaExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionOrLambdaExpressionAccess().getLambdaExpressionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_LambdaExpression_0=ruleLambdaExpression();

                    state._fsp--;


                    			current = this_LambdaExpression_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:4081:3: this_Expression_1= ruleExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionOrLambdaExpressionAccess().getExpressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Expression_1=ruleExpression();

                    state._fsp--;


                    			current = this_Expression_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionOrLambdaExpression"


    // $ANTLR start "entryRuleLambdaExpression"
    // InternalAssetBasedSystemDsl.g:4093:1: entryRuleLambdaExpression returns [EObject current=null] : iv_ruleLambdaExpression= ruleLambdaExpression EOF ;
    public final EObject entryRuleLambdaExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLambdaExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:4093:57: (iv_ruleLambdaExpression= ruleLambdaExpression EOF )
            // InternalAssetBasedSystemDsl.g:4094:2: iv_ruleLambdaExpression= ruleLambdaExpression EOF
            {
             newCompositeNode(grammarAccess.getLambdaExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLambdaExpression=ruleLambdaExpression();

            state._fsp--;

             current =iv_ruleLambdaExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLambdaExpression"


    // $ANTLR start "ruleLambdaExpression"
    // InternalAssetBasedSystemDsl.g:4100:1: ruleLambdaExpression returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}' ) ;
    public final EObject ruleLambdaExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_lambdaParameter_2_0 = null;

        EObject lv_body_4_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:4106:2: ( ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}' ) )
            // InternalAssetBasedSystemDsl.g:4107:2: ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}' )
            {
            // InternalAssetBasedSystemDsl.g:4107:2: ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}' )
            // InternalAssetBasedSystemDsl.g:4108:3: () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}'
            {
            // InternalAssetBasedSystemDsl.g:4108:3: ()
            // InternalAssetBasedSystemDsl.g:4109:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLambdaExpressionAccess().getLambdaExpressionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,13,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getLambdaExpressionAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:4119:3: ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) )
            // InternalAssetBasedSystemDsl.g:4120:4: (lv_lambdaParameter_2_0= ruleLambdaParameter )
            {
            // InternalAssetBasedSystemDsl.g:4120:4: (lv_lambdaParameter_2_0= ruleLambdaParameter )
            // InternalAssetBasedSystemDsl.g:4121:5: lv_lambdaParameter_2_0= ruleLambdaParameter
            {

            					newCompositeNode(grammarAccess.getLambdaExpressionAccess().getLambdaParameterLambdaParameterParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_64);
            lv_lambdaParameter_2_0=ruleLambdaParameter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaExpressionRule());
            					}
            					set(
            						current,
            						"lambdaParameter",
            						lv_lambdaParameter_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.LambdaParameter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,72,FOLLOW_48); 

            			newLeafNode(otherlv_3, grammarAccess.getLambdaExpressionAccess().getHyphenMinusGreaterThanSignKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:4142:3: ( (lv_body_4_0= ruleExpression ) )
            // InternalAssetBasedSystemDsl.g:4143:4: (lv_body_4_0= ruleExpression )
            {
            // InternalAssetBasedSystemDsl.g:4143:4: (lv_body_4_0= ruleExpression )
            // InternalAssetBasedSystemDsl.g:4144:5: lv_body_4_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getLambdaExpressionAccess().getBodyExpressionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_49);
            lv_body_4_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaExpressionRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_4_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getLambdaExpressionAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLambdaExpression"


    // $ANTLR start "entryRuleTerminalExpression"
    // InternalAssetBasedSystemDsl.g:4169:1: entryRuleTerminalExpression returns [EObject current=null] : iv_ruleTerminalExpression= ruleTerminalExpression EOF ;
    public final EObject entryRuleTerminalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerminalExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:4169:59: (iv_ruleTerminalExpression= ruleTerminalExpression EOF )
            // InternalAssetBasedSystemDsl.g:4170:2: iv_ruleTerminalExpression= ruleTerminalExpression EOF
            {
             newCompositeNode(grammarAccess.getTerminalExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTerminalExpression=ruleTerminalExpression();

            state._fsp--;

             current =iv_ruleTerminalExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerminalExpression"


    // $ANTLR start "ruleTerminalExpression"
    // InternalAssetBasedSystemDsl.g:4176:1: ruleTerminalExpression returns [EObject current=null] : ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) ) ;
    public final EObject ruleTerminalExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token lv_value_3_0=null;
        Token lv_value_5_1=null;
        Token lv_value_5_2=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        AntlrDatatypeRuleToken lv_value_7_0 = null;

        EObject this_Collection_10 = null;

        EObject this_Undefined_11 = null;

        EObject this_Expression_13 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:4182:2: ( ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) ) )
            // InternalAssetBasedSystemDsl.g:4183:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) )
            {
            // InternalAssetBasedSystemDsl.g:4183:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) )
            int alt85=8;
            alt85 = dfa85.predict(input);
            switch (alt85) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:4184:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4184:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    // InternalAssetBasedSystemDsl.g:4185:4: () ( (lv_value_1_0= RULE_INT ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4185:4: ()
                    // InternalAssetBasedSystemDsl.g:4186:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getIntConstantAction_0_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:4192:4: ( (lv_value_1_0= RULE_INT ) )
                    // InternalAssetBasedSystemDsl.g:4193:5: (lv_value_1_0= RULE_INT )
                    {
                    // InternalAssetBasedSystemDsl.g:4193:5: (lv_value_1_0= RULE_INT )
                    // InternalAssetBasedSystemDsl.g:4194:6: lv_value_1_0= RULE_INT
                    {
                    lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    						newLeafNode(lv_value_1_0, grammarAccess.getTerminalExpressionAccess().getValueINTTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTerminalExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_1_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:4212:3: ( () ( (lv_value_3_0= RULE_STRING ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4212:3: ( () ( (lv_value_3_0= RULE_STRING ) ) )
                    // InternalAssetBasedSystemDsl.g:4213:4: () ( (lv_value_3_0= RULE_STRING ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4213:4: ()
                    // InternalAssetBasedSystemDsl.g:4214:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getStringConstantAction_1_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:4220:4: ( (lv_value_3_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:4221:5: (lv_value_3_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:4221:5: (lv_value_3_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:4222:6: lv_value_3_0= RULE_STRING
                    {
                    lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getTerminalExpressionAccess().getValueSTRINGTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTerminalExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalAssetBasedSystemDsl.g:4240:3: ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4240:3: ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) )
                    // InternalAssetBasedSystemDsl.g:4241:4: () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4241:4: ()
                    // InternalAssetBasedSystemDsl.g:4242:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getBooleanConstantAction_2_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:4248:4: ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) )
                    // InternalAssetBasedSystemDsl.g:4249:5: ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4249:5: ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) )
                    // InternalAssetBasedSystemDsl.g:4250:6: (lv_value_5_1= 'true' | lv_value_5_2= 'false' )
                    {
                    // InternalAssetBasedSystemDsl.g:4250:6: (lv_value_5_1= 'true' | lv_value_5_2= 'false' )
                    int alt84=2;
                    int LA84_0 = input.LA(1);

                    if ( (LA84_0==47) ) {
                        alt84=1;
                    }
                    else if ( (LA84_0==48) ) {
                        alt84=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 84, 0, input);

                        throw nvae;
                    }
                    switch (alt84) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:4251:7: lv_value_5_1= 'true'
                            {
                            lv_value_5_1=(Token)match(input,47,FOLLOW_2); 

                            							newLeafNode(lv_value_5_1, grammarAccess.getTerminalExpressionAccess().getValueTrueKeyword_2_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getTerminalExpressionRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_5_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalAssetBasedSystemDsl.g:4262:7: lv_value_5_2= 'false'
                            {
                            lv_value_5_2=(Token)match(input,48,FOLLOW_2); 

                            							newLeafNode(lv_value_5_2, grammarAccess.getTerminalExpressionAccess().getValueFalseKeyword_2_1_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getTerminalExpressionRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_5_2, null);
                            						

                            }
                            break;

                    }


                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalAssetBasedSystemDsl.g:4277:3: ( () ( (lv_value_7_0= ruleVersion ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4277:3: ( () ( (lv_value_7_0= ruleVersion ) ) )
                    // InternalAssetBasedSystemDsl.g:4278:4: () ( (lv_value_7_0= ruleVersion ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4278:4: ()
                    // InternalAssetBasedSystemDsl.g:4279:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getVersionConstantAction_3_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:4285:4: ( (lv_value_7_0= ruleVersion ) )
                    // InternalAssetBasedSystemDsl.g:4286:5: (lv_value_7_0= ruleVersion )
                    {
                    // InternalAssetBasedSystemDsl.g:4286:5: (lv_value_7_0= ruleVersion )
                    // InternalAssetBasedSystemDsl.g:4287:6: lv_value_7_0= ruleVersion
                    {

                    						newCompositeNode(grammarAccess.getTerminalExpressionAccess().getValueVersionParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_7_0=ruleVersion();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTerminalExpressionRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Version");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalAssetBasedSystemDsl.g:4306:3: ( () ( ( ruleQualifiedName ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4306:3: ( () ( ( ruleQualifiedName ) ) )
                    // InternalAssetBasedSystemDsl.g:4307:4: () ( ( ruleQualifiedName ) )
                    {
                    // InternalAssetBasedSystemDsl.g:4307:4: ()
                    // InternalAssetBasedSystemDsl.g:4308:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getSymbolRefAction_4_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:4314:4: ( ( ruleQualifiedName ) )
                    // InternalAssetBasedSystemDsl.g:4315:5: ( ruleQualifiedName )
                    {
                    // InternalAssetBasedSystemDsl.g:4315:5: ( ruleQualifiedName )
                    // InternalAssetBasedSystemDsl.g:4316:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTerminalExpressionRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getTerminalExpressionAccess().getSymbolSymbolCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalAssetBasedSystemDsl.g:4332:3: this_Collection_10= ruleCollection
                    {

                    			newCompositeNode(grammarAccess.getTerminalExpressionAccess().getCollectionParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Collection_10=ruleCollection();

                    state._fsp--;


                    			current = this_Collection_10;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalAssetBasedSystemDsl.g:4341:3: this_Undefined_11= ruleUndefined
                    {

                    			newCompositeNode(grammarAccess.getTerminalExpressionAccess().getUndefinedParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_Undefined_11=ruleUndefined();

                    state._fsp--;


                    			current = this_Undefined_11;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalAssetBasedSystemDsl.g:4350:3: (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' )
                    {
                    // InternalAssetBasedSystemDsl.g:4350:3: (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' )
                    // InternalAssetBasedSystemDsl.g:4351:4: otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')'
                    {
                    otherlv_12=(Token)match(input,15,FOLLOW_48); 

                    				newLeafNode(otherlv_12, grammarAccess.getTerminalExpressionAccess().getLeftParenthesisKeyword_7_0());
                    			

                    				newCompositeNode(grammarAccess.getTerminalExpressionAccess().getExpressionParserRuleCall_7_1());
                    			
                    pushFollow(FOLLOW_65);
                    this_Expression_13=ruleExpression();

                    state._fsp--;


                    				current = this_Expression_13;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_14=(Token)match(input,17,FOLLOW_2); 

                    				newLeafNode(otherlv_14, grammarAccess.getTerminalExpressionAccess().getRightParenthesisKeyword_7_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerminalExpression"


    // $ANTLR start "entryRuleCollection"
    // InternalAssetBasedSystemDsl.g:4372:1: entryRuleCollection returns [EObject current=null] : iv_ruleCollection= ruleCollection EOF ;
    public final EObject entryRuleCollection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollection = null;


        try {
            // InternalAssetBasedSystemDsl.g:4372:51: (iv_ruleCollection= ruleCollection EOF )
            // InternalAssetBasedSystemDsl.g:4373:2: iv_ruleCollection= ruleCollection EOF
            {
             newCompositeNode(grammarAccess.getCollectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCollection=ruleCollection();

            state._fsp--;

             current =iv_ruleCollection; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollection"


    // $ANTLR start "ruleCollection"
    // InternalAssetBasedSystemDsl.g:4379:1: ruleCollection returns [EObject current=null] : ( () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']' ) ;
    public final EObject ruleCollection() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_elements_2_0 = null;

        EObject lv_elements_4_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:4385:2: ( ( () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']' ) )
            // InternalAssetBasedSystemDsl.g:4386:2: ( () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']' )
            {
            // InternalAssetBasedSystemDsl.g:4386:2: ( () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']' )
            // InternalAssetBasedSystemDsl.g:4387:3: () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']'
            {
            // InternalAssetBasedSystemDsl.g:4387:3: ()
            // InternalAssetBasedSystemDsl.g:4388:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCollectionAccess().getCollectionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,37,FOLLOW_66); 

            			newLeafNode(otherlv_1, grammarAccess.getCollectionAccess().getLeftSquareBracketKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:4398:3: ( (lv_elements_2_0= ruleExpression ) )?
            int alt86=2;
            int LA86_0 = input.LA(1);

            if ( ((LA86_0>=RULE_STRING && LA86_0<=RULE_INT)||LA86_0==15||(LA86_0>=36 && LA86_0<=37)||(LA86_0>=47 && LA86_0<=48)||LA86_0==64) ) {
                alt86=1;
            }
            switch (alt86) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:4399:4: (lv_elements_2_0= ruleExpression )
                    {
                    // InternalAssetBasedSystemDsl.g:4399:4: (lv_elements_2_0= ruleExpression )
                    // InternalAssetBasedSystemDsl.g:4400:5: lv_elements_2_0= ruleExpression
                    {

                    					newCompositeNode(grammarAccess.getCollectionAccess().getElementsExpressionParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_33);
                    lv_elements_2_0=ruleExpression();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getCollectionRule());
                    					}
                    					add(
                    						current,
                    						"elements",
                    						lv_elements_2_0,
                    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:4417:3: (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )*
            loop87:
            do {
                int alt87=2;
                int LA87_0 = input.LA(1);

                if ( (LA87_0==16) ) {
                    alt87=1;
                }


                switch (alt87) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:4418:4: otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,16,FOLLOW_48); 

            	    				newLeafNode(otherlv_3, grammarAccess.getCollectionAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalAssetBasedSystemDsl.g:4422:4: ( (lv_elements_4_0= ruleExpression ) )
            	    // InternalAssetBasedSystemDsl.g:4423:5: (lv_elements_4_0= ruleExpression )
            	    {
            	    // InternalAssetBasedSystemDsl.g:4423:5: (lv_elements_4_0= ruleExpression )
            	    // InternalAssetBasedSystemDsl.g:4424:6: lv_elements_4_0= ruleExpression
            	    {

            	    						newCompositeNode(grammarAccess.getCollectionAccess().getElementsExpressionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_33);
            	    lv_elements_4_0=ruleExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getCollectionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"elements",
            	    							lv_elements_4_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop87;
                }
            } while (true);

            otherlv_5=(Token)match(input,38,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCollectionAccess().getRightSquareBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollection"


    // $ANTLR start "entryRuleActionSelectionExpression"
    // InternalAssetBasedSystemDsl.g:4450:1: entryRuleActionSelectionExpression returns [EObject current=null] : iv_ruleActionSelectionExpression= ruleActionSelectionExpression EOF ;
    public final EObject entryRuleActionSelectionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActionSelectionExpression = null;


        try {
            // InternalAssetBasedSystemDsl.g:4450:66: (iv_ruleActionSelectionExpression= ruleActionSelectionExpression EOF )
            // InternalAssetBasedSystemDsl.g:4451:2: iv_ruleActionSelectionExpression= ruleActionSelectionExpression EOF
            {
             newCompositeNode(grammarAccess.getActionSelectionExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleActionSelectionExpression=ruleActionSelectionExpression();

            state._fsp--;

             current =iv_ruleActionSelectionExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActionSelectionExpression"


    // $ANTLR start "ruleActionSelectionExpression"
    // InternalAssetBasedSystemDsl.g:4457:1: ruleActionSelectionExpression returns [EObject current=null] : ( () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )* ) ;
    public final EObject ruleActionSelectionExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_methodInvocation_4_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token lv_methodInvocation_12_0=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        EObject lv_receiver_1_0 = null;

        EObject lv_args_5_0 = null;

        EObject lv_args_7_0 = null;

        EObject lv_args_13_0 = null;

        EObject lv_args_15_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:4463:2: ( ( () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )* ) )
            // InternalAssetBasedSystemDsl.g:4464:2: ( () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )* )
            {
            // InternalAssetBasedSystemDsl.g:4464:2: ( () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )* )
            // InternalAssetBasedSystemDsl.g:4465:3: () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )*
            {
            // InternalAssetBasedSystemDsl.g:4465:3: ()
            // InternalAssetBasedSystemDsl.g:4466:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getActionSelectionExpressionAccess().getMemberSelectionAction_0(),
            					current);
            			

            }

            // InternalAssetBasedSystemDsl.g:4472:3: ( (lv_receiver_1_0= ruleTerminalExpression ) )
            // InternalAssetBasedSystemDsl.g:4473:4: (lv_receiver_1_0= ruleTerminalExpression )
            {
            // InternalAssetBasedSystemDsl.g:4473:4: (lv_receiver_1_0= ruleTerminalExpression )
            // InternalAssetBasedSystemDsl.g:4474:5: lv_receiver_1_0= ruleTerminalExpression
            {

            					newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getReceiverTerminalExpressionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_67);
            lv_receiver_1_0=ruleTerminalExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
            					}
            					set(
            						current,
            						"receiver",
            						lv_receiver_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TerminalExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,71,FOLLOW_12); 

            			newLeafNode(otherlv_2, grammarAccess.getActionSelectionExpressionAccess().getFullStopKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:4495:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:4496:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:4496:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:4497:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getActionSelectionExpressionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getMemberMemberCrossReference_3_0());
            				
            pushFollow(FOLLOW_62);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAssetBasedSystemDsl.g:4511:3: ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )?
            int alt90=2;
            int LA90_0 = input.LA(1);

            if ( (LA90_0==15) ) {
                alt90=1;
            }
            switch (alt90) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:4512:4: ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')'
                    {
                    // InternalAssetBasedSystemDsl.g:4512:4: ( (lv_methodInvocation_4_0= '(' ) )
                    // InternalAssetBasedSystemDsl.g:4513:5: (lv_methodInvocation_4_0= '(' )
                    {
                    // InternalAssetBasedSystemDsl.g:4513:5: (lv_methodInvocation_4_0= '(' )
                    // InternalAssetBasedSystemDsl.g:4514:6: lv_methodInvocation_4_0= '('
                    {
                    lv_methodInvocation_4_0=(Token)match(input,15,FOLLOW_63); 

                    						newLeafNode(lv_methodInvocation_4_0, grammarAccess.getActionSelectionExpressionAccess().getMethodInvocationLeftParenthesisKeyword_4_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getActionSelectionExpressionRule());
                    						}
                    						setWithLastConsumed(current, "methodInvocation", lv_methodInvocation_4_0 != null, "(");
                    					

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:4526:4: ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )?
                    int alt89=2;
                    int LA89_0 = input.LA(1);

                    if ( ((LA89_0>=RULE_STRING && LA89_0<=RULE_INT)||LA89_0==13||LA89_0==15||(LA89_0>=36 && LA89_0<=37)||(LA89_0>=47 && LA89_0<=48)||LA89_0==64) ) {
                        alt89=1;
                    }
                    switch (alt89) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:4527:5: ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )*
                            {
                            // InternalAssetBasedSystemDsl.g:4527:5: ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) )
                            // InternalAssetBasedSystemDsl.g:4528:6: (lv_args_5_0= ruleExpressionOrLambdaExpression )
                            {
                            // InternalAssetBasedSystemDsl.g:4528:6: (lv_args_5_0= ruleExpressionOrLambdaExpression )
                            // InternalAssetBasedSystemDsl.g:4529:7: lv_args_5_0= ruleExpressionOrLambdaExpression
                            {

                            							newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getArgsExpressionOrLambdaExpressionParserRuleCall_4_1_0_0());
                            						
                            pushFollow(FOLLOW_11);
                            lv_args_5_0=ruleExpressionOrLambdaExpression();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
                            							}
                            							add(
                            								current,
                            								"args",
                            								lv_args_5_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ExpressionOrLambdaExpression");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            // InternalAssetBasedSystemDsl.g:4546:5: (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )*
                            loop88:
                            do {
                                int alt88=2;
                                int LA88_0 = input.LA(1);

                                if ( (LA88_0==16) ) {
                                    alt88=1;
                                }


                                switch (alt88) {
                            	case 1 :
                            	    // InternalAssetBasedSystemDsl.g:4547:6: otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) )
                            	    {
                            	    otherlv_6=(Token)match(input,16,FOLLOW_48); 

                            	    						newLeafNode(otherlv_6, grammarAccess.getActionSelectionExpressionAccess().getCommaKeyword_4_1_1_0());
                            	    					
                            	    // InternalAssetBasedSystemDsl.g:4551:6: ( (lv_args_7_0= ruleExpression ) )
                            	    // InternalAssetBasedSystemDsl.g:4552:7: (lv_args_7_0= ruleExpression )
                            	    {
                            	    // InternalAssetBasedSystemDsl.g:4552:7: (lv_args_7_0= ruleExpression )
                            	    // InternalAssetBasedSystemDsl.g:4553:8: lv_args_7_0= ruleExpression
                            	    {

                            	    								newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getArgsExpressionParserRuleCall_4_1_1_1_0());
                            	    							
                            	    pushFollow(FOLLOW_11);
                            	    lv_args_7_0=ruleExpression();

                            	    state._fsp--;


                            	    								if (current==null) {
                            	    									current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
                            	    								}
                            	    								add(
                            	    									current,
                            	    									"args",
                            	    									lv_args_7_0,
                            	    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                            	    								afterParserOrEnumRuleCall();
                            	    							

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop88;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_8=(Token)match(input,17,FOLLOW_61); 

                    				newLeafNode(otherlv_8, grammarAccess.getActionSelectionExpressionAccess().getRightParenthesisKeyword_4_2());
                    			

                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:4577:3: ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )*
            loop94:
            do {
                int alt94=2;
                int LA94_0 = input.LA(1);

                if ( (LA94_0==71) ) {
                    int LA94_2 = input.LA(2);

                    if ( (LA94_2==RULE_ID) ) {
                        alt94=1;
                    }


                }


                switch (alt94) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:4578:4: () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )?
            	    {
            	    // InternalAssetBasedSystemDsl.g:4578:4: ()
            	    // InternalAssetBasedSystemDsl.g:4579:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getActionSelectionExpressionAccess().getMemberSelectionReceiverAction_5_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_10=(Token)match(input,71,FOLLOW_12); 

            	    				newLeafNode(otherlv_10, grammarAccess.getActionSelectionExpressionAccess().getFullStopKeyword_5_1());
            	    			
            	    // InternalAssetBasedSystemDsl.g:4589:4: ( ( ruleQualifiedName ) )
            	    // InternalAssetBasedSystemDsl.g:4590:5: ( ruleQualifiedName )
            	    {
            	    // InternalAssetBasedSystemDsl.g:4590:5: ( ruleQualifiedName )
            	    // InternalAssetBasedSystemDsl.g:4591:6: ruleQualifiedName
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getActionSelectionExpressionRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getMemberMemberCrossReference_5_2_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    ruleQualifiedName();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalAssetBasedSystemDsl.g:4605:4: ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )?
            	    int alt93=2;
            	    int LA93_0 = input.LA(1);

            	    if ( (LA93_0==15) ) {
            	        alt93=1;
            	    }
            	    switch (alt93) {
            	        case 1 :
            	            // InternalAssetBasedSystemDsl.g:4606:5: ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')'
            	            {
            	            // InternalAssetBasedSystemDsl.g:4606:5: ( (lv_methodInvocation_12_0= '(' ) )
            	            // InternalAssetBasedSystemDsl.g:4607:6: (lv_methodInvocation_12_0= '(' )
            	            {
            	            // InternalAssetBasedSystemDsl.g:4607:6: (lv_methodInvocation_12_0= '(' )
            	            // InternalAssetBasedSystemDsl.g:4608:7: lv_methodInvocation_12_0= '('
            	            {
            	            lv_methodInvocation_12_0=(Token)match(input,15,FOLLOW_63); 

            	            							newLeafNode(lv_methodInvocation_12_0, grammarAccess.getActionSelectionExpressionAccess().getMethodInvocationLeftParenthesisKeyword_5_3_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getActionSelectionExpressionRule());
            	            							}
            	            							setWithLastConsumed(current, "methodInvocation", lv_methodInvocation_12_0 != null, "(");
            	            						

            	            }


            	            }

            	            // InternalAssetBasedSystemDsl.g:4620:5: ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )?
            	            int alt92=2;
            	            int LA92_0 = input.LA(1);

            	            if ( ((LA92_0>=RULE_STRING && LA92_0<=RULE_INT)||LA92_0==13||LA92_0==15||(LA92_0>=36 && LA92_0<=37)||(LA92_0>=47 && LA92_0<=48)||LA92_0==64) ) {
            	                alt92=1;
            	            }
            	            switch (alt92) {
            	                case 1 :
            	                    // InternalAssetBasedSystemDsl.g:4621:6: ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )*
            	                    {
            	                    // InternalAssetBasedSystemDsl.g:4621:6: ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) )
            	                    // InternalAssetBasedSystemDsl.g:4622:7: (lv_args_13_0= ruleExpressionOrLambdaExpression )
            	                    {
            	                    // InternalAssetBasedSystemDsl.g:4622:7: (lv_args_13_0= ruleExpressionOrLambdaExpression )
            	                    // InternalAssetBasedSystemDsl.g:4623:8: lv_args_13_0= ruleExpressionOrLambdaExpression
            	                    {

            	                    								newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getArgsExpressionOrLambdaExpressionParserRuleCall_5_3_1_0_0());
            	                    							
            	                    pushFollow(FOLLOW_11);
            	                    lv_args_13_0=ruleExpressionOrLambdaExpression();

            	                    state._fsp--;


            	                    								if (current==null) {
            	                    									current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
            	                    								}
            	                    								add(
            	                    									current,
            	                    									"args",
            	                    									lv_args_13_0,
            	                    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ExpressionOrLambdaExpression");
            	                    								afterParserOrEnumRuleCall();
            	                    							

            	                    }


            	                    }

            	                    // InternalAssetBasedSystemDsl.g:4640:6: (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )*
            	                    loop91:
            	                    do {
            	                        int alt91=2;
            	                        int LA91_0 = input.LA(1);

            	                        if ( (LA91_0==16) ) {
            	                            alt91=1;
            	                        }


            	                        switch (alt91) {
            	                    	case 1 :
            	                    	    // InternalAssetBasedSystemDsl.g:4641:7: otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) )
            	                    	    {
            	                    	    otherlv_14=(Token)match(input,16,FOLLOW_48); 

            	                    	    							newLeafNode(otherlv_14, grammarAccess.getActionSelectionExpressionAccess().getCommaKeyword_5_3_1_1_0());
            	                    	    						
            	                    	    // InternalAssetBasedSystemDsl.g:4645:7: ( (lv_args_15_0= ruleExpression ) )
            	                    	    // InternalAssetBasedSystemDsl.g:4646:8: (lv_args_15_0= ruleExpression )
            	                    	    {
            	                    	    // InternalAssetBasedSystemDsl.g:4646:8: (lv_args_15_0= ruleExpression )
            	                    	    // InternalAssetBasedSystemDsl.g:4647:9: lv_args_15_0= ruleExpression
            	                    	    {

            	                    	    									newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getArgsExpressionParserRuleCall_5_3_1_1_1_0());
            	                    	    								
            	                    	    pushFollow(FOLLOW_11);
            	                    	    lv_args_15_0=ruleExpression();

            	                    	    state._fsp--;


            	                    	    									if (current==null) {
            	                    	    										current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
            	                    	    									}
            	                    	    									add(
            	                    	    										current,
            	                    	    										"args",
            	                    	    										lv_args_15_0,
            	                    	    										"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            	                    	    									afterParserOrEnumRuleCall();
            	                    	    								

            	                    	    }


            	                    	    }


            	                    	    }
            	                    	    break;

            	                    	default :
            	                    	    break loop91;
            	                        }
            	                    } while (true);


            	                    }
            	                    break;

            	            }

            	            otherlv_16=(Token)match(input,17,FOLLOW_61); 

            	            					newLeafNode(otherlv_16, grammarAccess.getActionSelectionExpressionAccess().getRightParenthesisKeyword_5_3_2());
            	            				

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop94;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionSelectionExpression"


    // $ANTLR start "entryRuleAction"
    // InternalAssetBasedSystemDsl.g:4676:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalAssetBasedSystemDsl.g:4676:47: (iv_ruleAction= ruleAction EOF )
            // InternalAssetBasedSystemDsl.g:4677:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalAssetBasedSystemDsl.g:4683:1: ruleAction returns [EObject current=null] : ( () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')' ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_target_1_0 = null;

        Enumerator lv_actionType_3_0 = null;

        EObject lv_args_5_0 = null;

        EObject lv_lambdaAction_6_0 = null;

        EObject lv_args_8_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:4689:2: ( ( () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')' ) )
            // InternalAssetBasedSystemDsl.g:4690:2: ( () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')' )
            {
            // InternalAssetBasedSystemDsl.g:4690:2: ( () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')' )
            // InternalAssetBasedSystemDsl.g:4691:3: () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')'
            {
            // InternalAssetBasedSystemDsl.g:4691:3: ()
            // InternalAssetBasedSystemDsl.g:4692:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getActionAccess().getActionAction_0(),
            					current);
            			

            }

            // InternalAssetBasedSystemDsl.g:4698:3: ( (lv_target_1_0= ruleActionSelectionExpression ) )
            // InternalAssetBasedSystemDsl.g:4699:4: (lv_target_1_0= ruleActionSelectionExpression )
            {
            // InternalAssetBasedSystemDsl.g:4699:4: (lv_target_1_0= ruleActionSelectionExpression )
            // InternalAssetBasedSystemDsl.g:4700:5: lv_target_1_0= ruleActionSelectionExpression
            {

            					newCompositeNode(grammarAccess.getActionAccess().getTargetActionSelectionExpressionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_67);
            lv_target_1_0=ruleActionSelectionExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					set(
            						current,
            						"target",
            						lv_target_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ActionSelectionExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,71,FOLLOW_68); 

            			newLeafNode(otherlv_2, grammarAccess.getActionAccess().getFullStopKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:4721:3: ( (lv_actionType_3_0= ruleActionEnum ) )
            // InternalAssetBasedSystemDsl.g:4722:4: (lv_actionType_3_0= ruleActionEnum )
            {
            // InternalAssetBasedSystemDsl.g:4722:4: (lv_actionType_3_0= ruleActionEnum )
            // InternalAssetBasedSystemDsl.g:4723:5: lv_actionType_3_0= ruleActionEnum
            {

            					newCompositeNode(grammarAccess.getActionAccess().getActionTypeActionEnumEnumRuleCall_3_0());
            				
            pushFollow(FOLLOW_9);
            lv_actionType_3_0=ruleActionEnum();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					set(
            						current,
            						"actionType",
            						lv_actionType_3_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ActionEnum");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_63); 

            			newLeafNode(otherlv_4, grammarAccess.getActionAccess().getLeftParenthesisKeyword_4());
            		
            // InternalAssetBasedSystemDsl.g:4744:3: ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )?
            int alt97=2;
            int LA97_0 = input.LA(1);

            if ( ((LA97_0>=RULE_STRING && LA97_0<=RULE_INT)||LA97_0==13||LA97_0==15||(LA97_0>=36 && LA97_0<=37)||(LA97_0>=47 && LA97_0<=48)||LA97_0==64) ) {
                alt97=1;
            }
            switch (alt97) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:4745:4: ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )*
                    {
                    // InternalAssetBasedSystemDsl.g:4745:4: ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) )
                    int alt95=2;
                    int LA95_0 = input.LA(1);

                    if ( ((LA95_0>=RULE_STRING && LA95_0<=RULE_INT)||LA95_0==15||(LA95_0>=36 && LA95_0<=37)||(LA95_0>=47 && LA95_0<=48)||LA95_0==64) ) {
                        alt95=1;
                    }
                    else if ( (LA95_0==13) ) {
                        alt95=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 95, 0, input);

                        throw nvae;
                    }
                    switch (alt95) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:4746:5: ( (lv_args_5_0= ruleExpression ) )
                            {
                            // InternalAssetBasedSystemDsl.g:4746:5: ( (lv_args_5_0= ruleExpression ) )
                            // InternalAssetBasedSystemDsl.g:4747:6: (lv_args_5_0= ruleExpression )
                            {
                            // InternalAssetBasedSystemDsl.g:4747:6: (lv_args_5_0= ruleExpression )
                            // InternalAssetBasedSystemDsl.g:4748:7: lv_args_5_0= ruleExpression
                            {

                            							newCompositeNode(grammarAccess.getActionAccess().getArgsExpressionParserRuleCall_5_0_0_0());
                            						
                            pushFollow(FOLLOW_11);
                            lv_args_5_0=ruleExpression();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getActionRule());
                            							}
                            							add(
                            								current,
                            								"args",
                            								lv_args_5_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalAssetBasedSystemDsl.g:4766:5: ( (lv_lambdaAction_6_0= ruleLambdaAction ) )
                            {
                            // InternalAssetBasedSystemDsl.g:4766:5: ( (lv_lambdaAction_6_0= ruleLambdaAction ) )
                            // InternalAssetBasedSystemDsl.g:4767:6: (lv_lambdaAction_6_0= ruleLambdaAction )
                            {
                            // InternalAssetBasedSystemDsl.g:4767:6: (lv_lambdaAction_6_0= ruleLambdaAction )
                            // InternalAssetBasedSystemDsl.g:4768:7: lv_lambdaAction_6_0= ruleLambdaAction
                            {

                            							newCompositeNode(grammarAccess.getActionAccess().getLambdaActionLambdaActionParserRuleCall_5_0_1_0());
                            						
                            pushFollow(FOLLOW_11);
                            lv_lambdaAction_6_0=ruleLambdaAction();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getActionRule());
                            							}
                            							set(
                            								current,
                            								"lambdaAction",
                            								lv_lambdaAction_6_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.LambdaAction");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:4786:4: (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )*
                    loop96:
                    do {
                        int alt96=2;
                        int LA96_0 = input.LA(1);

                        if ( (LA96_0==16) ) {
                            alt96=1;
                        }


                        switch (alt96) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:4787:5: otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) )
                    	    {
                    	    otherlv_7=(Token)match(input,16,FOLLOW_48); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getActionAccess().getCommaKeyword_5_1_0());
                    	    				
                    	    // InternalAssetBasedSystemDsl.g:4791:5: ( (lv_args_8_0= ruleExpression ) )
                    	    // InternalAssetBasedSystemDsl.g:4792:6: (lv_args_8_0= ruleExpression )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:4792:6: (lv_args_8_0= ruleExpression )
                    	    // InternalAssetBasedSystemDsl.g:4793:7: lv_args_8_0= ruleExpression
                    	    {

                    	    							newCompositeNode(grammarAccess.getActionAccess().getArgsExpressionParserRuleCall_5_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    lv_args_8_0=ruleExpression();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getActionRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"args",
                    	    								lv_args_8_0,
                    	    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop96;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_9=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getActionAccess().getRightParenthesisKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleLambdaAction"
    // InternalAssetBasedSystemDsl.g:4820:1: entryRuleLambdaAction returns [EObject current=null] : iv_ruleLambdaAction= ruleLambdaAction EOF ;
    public final EObject entryRuleLambdaAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLambdaAction = null;


        try {
            // InternalAssetBasedSystemDsl.g:4820:53: (iv_ruleLambdaAction= ruleLambdaAction EOF )
            // InternalAssetBasedSystemDsl.g:4821:2: iv_ruleLambdaAction= ruleLambdaAction EOF
            {
             newCompositeNode(grammarAccess.getLambdaActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLambdaAction=ruleLambdaAction();

            state._fsp--;

             current =iv_ruleLambdaAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLambdaAction"


    // $ANTLR start "ruleLambdaAction"
    // InternalAssetBasedSystemDsl.g:4827:1: ruleLambdaAction returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}' ) ;
    public final EObject ruleLambdaAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_lambdaParameter_2_0 = null;

        EObject lv_actions_4_0 = null;

        EObject lv_actions_6_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:4833:2: ( ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}' ) )
            // InternalAssetBasedSystemDsl.g:4834:2: ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}' )
            {
            // InternalAssetBasedSystemDsl.g:4834:2: ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}' )
            // InternalAssetBasedSystemDsl.g:4835:3: () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}'
            {
            // InternalAssetBasedSystemDsl.g:4835:3: ()
            // InternalAssetBasedSystemDsl.g:4836:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLambdaActionAccess().getLambdaActionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,13,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getLambdaActionAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:4846:3: ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) )
            // InternalAssetBasedSystemDsl.g:4847:4: (lv_lambdaParameter_2_0= ruleLambdaParameter )
            {
            // InternalAssetBasedSystemDsl.g:4847:4: (lv_lambdaParameter_2_0= ruleLambdaParameter )
            // InternalAssetBasedSystemDsl.g:4848:5: lv_lambdaParameter_2_0= ruleLambdaParameter
            {

            					newCompositeNode(grammarAccess.getLambdaActionAccess().getLambdaParameterLambdaParameterParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_64);
            lv_lambdaParameter_2_0=ruleLambdaParameter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaActionRule());
            					}
            					set(
            						current,
            						"lambdaParameter",
            						lv_lambdaParameter_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.LambdaParameter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,72,FOLLOW_48); 

            			newLeafNode(otherlv_3, grammarAccess.getLambdaActionAccess().getHyphenMinusGreaterThanSignKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:4869:3: ( (lv_actions_4_0= ruleAction ) )
            // InternalAssetBasedSystemDsl.g:4870:4: (lv_actions_4_0= ruleAction )
            {
            // InternalAssetBasedSystemDsl.g:4870:4: (lv_actions_4_0= ruleAction )
            // InternalAssetBasedSystemDsl.g:4871:5: lv_actions_4_0= ruleAction
            {

            					newCompositeNode(grammarAccess.getLambdaActionAccess().getActionsActionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_69);
            lv_actions_4_0=ruleAction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaActionRule());
            					}
            					add(
            						current,
            						"actions",
            						lv_actions_4_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Action");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAssetBasedSystemDsl.g:4888:3: (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )*
            loop98:
            do {
                int alt98=2;
                int LA98_0 = input.LA(1);

                if ( (LA98_0==57) ) {
                    alt98=1;
                }


                switch (alt98) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:4889:4: otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) )
            	    {
            	    otherlv_5=(Token)match(input,57,FOLLOW_48); 

            	    				newLeafNode(otherlv_5, grammarAccess.getLambdaActionAccess().getSemicolonKeyword_5_0());
            	    			
            	    // InternalAssetBasedSystemDsl.g:4893:4: ( (lv_actions_6_0= ruleAction ) )
            	    // InternalAssetBasedSystemDsl.g:4894:5: (lv_actions_6_0= ruleAction )
            	    {
            	    // InternalAssetBasedSystemDsl.g:4894:5: (lv_actions_6_0= ruleAction )
            	    // InternalAssetBasedSystemDsl.g:4895:6: lv_actions_6_0= ruleAction
            	    {

            	    						newCompositeNode(grammarAccess.getLambdaActionAccess().getActionsActionParserRuleCall_5_1_0());
            	    					
            	    pushFollow(FOLLOW_69);
            	    lv_actions_6_0=ruleAction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLambdaActionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"actions",
            	    							lv_actions_6_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Action");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop98;
                }
            } while (true);

            otherlv_7=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getLambdaActionAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLambdaAction"


    // $ANTLR start "entryRuleVersion"
    // InternalAssetBasedSystemDsl.g:4921:1: entryRuleVersion returns [String current=null] : iv_ruleVersion= ruleVersion EOF ;
    public final String entryRuleVersion() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleVersion = null;


        try {
            // InternalAssetBasedSystemDsl.g:4921:47: (iv_ruleVersion= ruleVersion EOF )
            // InternalAssetBasedSystemDsl.g:4922:2: iv_ruleVersion= ruleVersion EOF
            {
             newCompositeNode(grammarAccess.getVersionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVersion=ruleVersion();

            state._fsp--;

             current =iv_ruleVersion.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVersion"


    // $ANTLR start "ruleVersion"
    // InternalAssetBasedSystemDsl.g:4928:1: ruleVersion returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )? ) ;
    public final AntlrDatatypeRuleToken ruleVersion() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_INT_2=null;
        Token this_INT_4=null;
        AntlrDatatypeRuleToken this_VID_6 = null;

        AntlrDatatypeRuleToken this_VID_8 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:4934:2: ( (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )? ) )
            // InternalAssetBasedSystemDsl.g:4935:2: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )? )
            {
            // InternalAssetBasedSystemDsl.g:4935:2: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )? )
            // InternalAssetBasedSystemDsl.g:4936:3: this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )?
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_67); 

            			current.merge(this_INT_0);
            		

            			newLeafNode(this_INT_0, grammarAccess.getVersionAccess().getINTTerminalRuleCall_0());
            		
            kw=(Token)match(input,71,FOLLOW_70); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getVersionAccess().getFullStopKeyword_1());
            		
            this_INT_2=(Token)match(input,RULE_INT,FOLLOW_67); 

            			current.merge(this_INT_2);
            		

            			newLeafNode(this_INT_2, grammarAccess.getVersionAccess().getINTTerminalRuleCall_2());
            		
            kw=(Token)match(input,71,FOLLOW_70); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getVersionAccess().getFullStopKeyword_3());
            		
            this_INT_4=(Token)match(input,RULE_INT,FOLLOW_71); 

            			current.merge(this_INT_4);
            		

            			newLeafNode(this_INT_4, grammarAccess.getVersionAccess().getINTTerminalRuleCall_4());
            		
            // InternalAssetBasedSystemDsl.g:4967:3: (kw= '-' this_VID_6= ruleVID )?
            int alt99=2;
            int LA99_0 = input.LA(1);

            if ( (LA99_0==73) ) {
                alt99=1;
            }
            switch (alt99) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:4968:4: kw= '-' this_VID_6= ruleVID
                    {
                    kw=(Token)match(input,73,FOLLOW_72); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getVersionAccess().getHyphenMinusKeyword_5_0());
                    			

                    				newCompositeNode(grammarAccess.getVersionAccess().getVIDParserRuleCall_5_1());
                    			
                    pushFollow(FOLLOW_73);
                    this_VID_6=ruleVID();

                    state._fsp--;


                    				current.merge(this_VID_6);
                    			

                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:4984:3: (kw= '+' this_VID_8= ruleVID )?
            int alt100=2;
            int LA100_0 = input.LA(1);

            if ( (LA100_0==74) ) {
                alt100=1;
            }
            switch (alt100) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:4985:4: kw= '+' this_VID_8= ruleVID
                    {
                    kw=(Token)match(input,74,FOLLOW_72); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getVersionAccess().getPlusSignKeyword_6_0());
                    			

                    				newCompositeNode(grammarAccess.getVersionAccess().getVIDParserRuleCall_6_1());
                    			
                    pushFollow(FOLLOW_2);
                    this_VID_8=ruleVID();

                    state._fsp--;


                    				current.merge(this_VID_8);
                    			

                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVersion"


    // $ANTLR start "entryRuleVID"
    // InternalAssetBasedSystemDsl.g:5005:1: entryRuleVID returns [String current=null] : iv_ruleVID= ruleVID EOF ;
    public final String entryRuleVID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleVID = null;


        try {
            // InternalAssetBasedSystemDsl.g:5005:43: (iv_ruleVID= ruleVID EOF )
            // InternalAssetBasedSystemDsl.g:5006:2: iv_ruleVID= ruleVID EOF
            {
             newCompositeNode(grammarAccess.getVIDRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVID=ruleVID();

            state._fsp--;

             current =iv_ruleVID.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVID"


    // $ANTLR start "ruleVID"
    // InternalAssetBasedSystemDsl.g:5012:1: ruleVID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID | this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleVID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5018:2: ( (this_ID_0= RULE_ID | this_INT_1= RULE_INT ) )
            // InternalAssetBasedSystemDsl.g:5019:2: (this_ID_0= RULE_ID | this_INT_1= RULE_INT )
            {
            // InternalAssetBasedSystemDsl.g:5019:2: (this_ID_0= RULE_ID | this_INT_1= RULE_INT )
            int alt101=2;
            int LA101_0 = input.LA(1);

            if ( (LA101_0==RULE_ID) ) {
                alt101=1;
            }
            else if ( (LA101_0==RULE_INT) ) {
                alt101=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 101, 0, input);

                throw nvae;
            }
            switch (alt101) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:5020:3: this_ID_0= RULE_ID
                    {
                    this_ID_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_0);
                    		

                    			newLeafNode(this_ID_0, grammarAccess.getVIDAccess().getIDTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:5028:3: this_INT_1= RULE_INT
                    {
                    this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

                    			current.merge(this_INT_1);
                    		

                    			newLeafNode(this_INT_1, grammarAccess.getVIDAccess().getINTTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVID"


    // $ANTLR start "entryRuleUndefined"
    // InternalAssetBasedSystemDsl.g:5039:1: entryRuleUndefined returns [EObject current=null] : iv_ruleUndefined= ruleUndefined EOF ;
    public final EObject entryRuleUndefined() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUndefined = null;


        try {
            // InternalAssetBasedSystemDsl.g:5039:50: (iv_ruleUndefined= ruleUndefined EOF )
            // InternalAssetBasedSystemDsl.g:5040:2: iv_ruleUndefined= ruleUndefined EOF
            {
             newCompositeNode(grammarAccess.getUndefinedRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUndefined=ruleUndefined();

            state._fsp--;

             current =iv_ruleUndefined; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUndefined"


    // $ANTLR start "ruleUndefined"
    // InternalAssetBasedSystemDsl.g:5046:1: ruleUndefined returns [EObject current=null] : ( () otherlv_1= 'undefined' ) ;
    public final EObject ruleUndefined() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5052:2: ( ( () otherlv_1= 'undefined' ) )
            // InternalAssetBasedSystemDsl.g:5053:2: ( () otherlv_1= 'undefined' )
            {
            // InternalAssetBasedSystemDsl.g:5053:2: ( () otherlv_1= 'undefined' )
            // InternalAssetBasedSystemDsl.g:5054:3: () otherlv_1= 'undefined'
            {
            // InternalAssetBasedSystemDsl.g:5054:3: ()
            // InternalAssetBasedSystemDsl.g:5055:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getUndefinedAccess().getUndefinedConstantAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,36,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getUndefinedAccess().getUndefinedKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUndefined"


    // $ANTLR start "entryRuleEString"
    // InternalAssetBasedSystemDsl.g:5069:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalAssetBasedSystemDsl.g:5069:47: (iv_ruleEString= ruleEString EOF )
            // InternalAssetBasedSystemDsl.g:5070:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalAssetBasedSystemDsl.g:5076:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5082:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalAssetBasedSystemDsl.g:5083:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalAssetBasedSystemDsl.g:5083:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt102=2;
            int LA102_0 = input.LA(1);

            if ( (LA102_0==RULE_STRING) ) {
                alt102=1;
            }
            else if ( (LA102_0==RULE_ID) ) {
                alt102=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 102, 0, input);

                throw nvae;
            }
            switch (alt102) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:5084:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:5092:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleImport"
    // InternalAssetBasedSystemDsl.g:5103:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalAssetBasedSystemDsl.g:5103:47: (iv_ruleImport= ruleImport EOF )
            // InternalAssetBasedSystemDsl.g:5104:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalAssetBasedSystemDsl.g:5110:1: ruleImport returns [EObject current=null] : ( ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) ) | (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_importURI_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token lv_importURI_7_0=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_importedNamespace_2_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5116:2: ( ( ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) ) | (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' ) ) )
            // InternalAssetBasedSystemDsl.g:5117:2: ( ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) ) | (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' ) )
            {
            // InternalAssetBasedSystemDsl.g:5117:2: ( ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) ) | (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' ) )
            int alt104=2;
            int LA104_0 = input.LA(1);

            if ( (LA104_0==75) ) {
                alt104=1;
            }
            else if ( (LA104_0==77) ) {
                alt104=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 104, 0, input);

                throw nvae;
            }
            switch (alt104) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:5118:3: ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) )
                    {
                    // InternalAssetBasedSystemDsl.g:5118:3: ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) )
                    // InternalAssetBasedSystemDsl.g:5119:4: () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' )
                    {
                    // InternalAssetBasedSystemDsl.g:5119:4: ()
                    // InternalAssetBasedSystemDsl.g:5120:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getImportAccess().getImportAction_0_0(),
                    						current);
                    				

                    }

                    // InternalAssetBasedSystemDsl.g:5126:4: (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' )
                    // InternalAssetBasedSystemDsl.g:5127:5: otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';'
                    {
                    otherlv_1=(Token)match(input,75,FOLLOW_12); 

                    					newLeafNode(otherlv_1, grammarAccess.getImportAccess().getWithKeyword_0_1_0());
                    				
                    // InternalAssetBasedSystemDsl.g:5131:5: ( (lv_importedNamespace_2_0= ruleImportedFQN ) )
                    // InternalAssetBasedSystemDsl.g:5132:6: (lv_importedNamespace_2_0= ruleImportedFQN )
                    {
                    // InternalAssetBasedSystemDsl.g:5132:6: (lv_importedNamespace_2_0= ruleImportedFQN )
                    // InternalAssetBasedSystemDsl.g:5133:7: lv_importedNamespace_2_0= ruleImportedFQN
                    {

                    							newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceImportedFQNParserRuleCall_0_1_1_0());
                    						
                    pushFollow(FOLLOW_74);
                    lv_importedNamespace_2_0=ruleImportedFQN();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getImportRule());
                    							}
                    							set(
                    								current,
                    								"importedNamespace",
                    								lv_importedNamespace_2_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ImportedFQN");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:5150:5: (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )?
                    int alt103=2;
                    int LA103_0 = input.LA(1);

                    if ( (LA103_0==76) ) {
                        alt103=1;
                    }
                    switch (alt103) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:5151:6: otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) )
                            {
                            otherlv_3=(Token)match(input,76,FOLLOW_21); 

                            						newLeafNode(otherlv_3, grammarAccess.getImportAccess().getFromKeyword_0_1_2_0());
                            					
                            // InternalAssetBasedSystemDsl.g:5155:6: ( (lv_importURI_4_0= RULE_STRING ) )
                            // InternalAssetBasedSystemDsl.g:5156:7: (lv_importURI_4_0= RULE_STRING )
                            {
                            // InternalAssetBasedSystemDsl.g:5156:7: (lv_importURI_4_0= RULE_STRING )
                            // InternalAssetBasedSystemDsl.g:5157:8: lv_importURI_4_0= RULE_STRING
                            {
                            lv_importURI_4_0=(Token)match(input,RULE_STRING,FOLLOW_53); 

                            								newLeafNode(lv_importURI_4_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_0_1_2_1_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getImportRule());
                            								}
                            								setWithLastConsumed(
                            									current,
                            									"importURI",
                            									lv_importURI_4_0,
                            									"org.eclipse.xtext.common.Terminals.STRING");
                            							

                            }


                            }


                            }
                            break;

                    }

                    otherlv_5=(Token)match(input,57,FOLLOW_2); 

                    					newLeafNode(otherlv_5, grammarAccess.getImportAccess().getSemicolonKeyword_0_1_3());
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:5181:3: (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' )
                    {
                    // InternalAssetBasedSystemDsl.g:5181:3: (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' )
                    // InternalAssetBasedSystemDsl.g:5182:4: otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';'
                    {
                    otherlv_6=(Token)match(input,77,FOLLOW_21); 

                    				newLeafNode(otherlv_6, grammarAccess.getImportAccess().getImportKeyword_1_0());
                    			
                    // InternalAssetBasedSystemDsl.g:5186:4: ( (lv_importURI_7_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:5187:5: (lv_importURI_7_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:5187:5: (lv_importURI_7_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:5188:6: lv_importURI_7_0= RULE_STRING
                    {
                    lv_importURI_7_0=(Token)match(input,RULE_STRING,FOLLOW_53); 

                    						newLeafNode(lv_importURI_7_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getImportRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"importURI",
                    							lv_importURI_7_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }

                    otherlv_8=(Token)match(input,57,FOLLOW_2); 

                    				newLeafNode(otherlv_8, grammarAccess.getImportAccess().getSemicolonKeyword_1_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalAssetBasedSystemDsl.g:5213:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalAssetBasedSystemDsl.g:5213:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalAssetBasedSystemDsl.g:5214:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalAssetBasedSystemDsl.g:5220:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5226:2: ( (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* ) )
            // InternalAssetBasedSystemDsl.g:5227:2: (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* )
            {
            // InternalAssetBasedSystemDsl.g:5227:2: (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* )
            // InternalAssetBasedSystemDsl.g:5228:3: this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_75); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalAssetBasedSystemDsl.g:5235:3: (kw= '::' this_ID_2= RULE_ID )*
            loop105:
            do {
                int alt105=2;
                int LA105_0 = input.LA(1);

                if ( (LA105_0==78) ) {
                    int LA105_2 = input.LA(2);

                    if ( (LA105_2==RULE_ID) ) {
                        alt105=1;
                    }


                }


                switch (alt105) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:5236:4: kw= '::' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,78,FOLLOW_12); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getColonColonKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_75); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop105;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleImportedFQN"
    // InternalAssetBasedSystemDsl.g:5253:1: entryRuleImportedFQN returns [String current=null] : iv_ruleImportedFQN= ruleImportedFQN EOF ;
    public final String entryRuleImportedFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleImportedFQN = null;


        try {
            // InternalAssetBasedSystemDsl.g:5253:51: (iv_ruleImportedFQN= ruleImportedFQN EOF )
            // InternalAssetBasedSystemDsl.g:5254:2: iv_ruleImportedFQN= ruleImportedFQN EOF
            {
             newCompositeNode(grammarAccess.getImportedFQNRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImportedFQN=ruleImportedFQN();

            state._fsp--;

             current =iv_ruleImportedFQN.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportedFQN"


    // $ANTLR start "ruleImportedFQN"
    // InternalAssetBasedSystemDsl.g:5260:1: ruleImportedFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )? ) ;
    public final AntlrDatatypeRuleToken ruleImportedFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5266:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )? ) )
            // InternalAssetBasedSystemDsl.g:5267:2: (this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )? )
            {
            // InternalAssetBasedSystemDsl.g:5267:2: (this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )? )
            // InternalAssetBasedSystemDsl.g:5268:3: this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )?
            {

            			newCompositeNode(grammarAccess.getImportedFQNAccess().getQualifiedNameParserRuleCall_0());
            		
            pushFollow(FOLLOW_75);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            			current.merge(this_QualifiedName_0);
            		

            			afterParserOrEnumRuleCall();
            		
            // InternalAssetBasedSystemDsl.g:5278:3: (kw= '::' kw= '*' )?
            int alt106=2;
            int LA106_0 = input.LA(1);

            if ( (LA106_0==78) ) {
                alt106=1;
            }
            switch (alt106) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:5279:4: kw= '::' kw= '*'
                    {
                    kw=(Token)match(input,78,FOLLOW_76); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getImportedFQNAccess().getColonColonKeyword_1_0());
                    			
                    kw=(Token)match(input,79,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getImportedFQNAccess().getAsteriskKeyword_1_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportedFQN"


    // $ANTLR start "entryRuleLocaleGroup"
    // InternalAssetBasedSystemDsl.g:5294:1: entryRuleLocaleGroup returns [EObject current=null] : iv_ruleLocaleGroup= ruleLocaleGroup EOF ;
    public final EObject entryRuleLocaleGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLocaleGroup = null;


        try {
            // InternalAssetBasedSystemDsl.g:5294:52: (iv_ruleLocaleGroup= ruleLocaleGroup EOF )
            // InternalAssetBasedSystemDsl.g:5295:2: iv_ruleLocaleGroup= ruleLocaleGroup EOF
            {
             newCompositeNode(grammarAccess.getLocaleGroupRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLocaleGroup=ruleLocaleGroup();

            state._fsp--;

             current =iv_ruleLocaleGroup; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLocaleGroup"


    // $ANTLR start "ruleLocaleGroup"
    // InternalAssetBasedSystemDsl.g:5301:1: ruleLocaleGroup returns [EObject current=null] : (otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}' ) ;
    public final EObject ruleLocaleGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_locale_4_0=null;
        Token otherlv_6=null;
        EObject lv_objectLocales_5_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5307:2: ( (otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}' ) )
            // InternalAssetBasedSystemDsl.g:5308:2: (otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}' )
            {
            // InternalAssetBasedSystemDsl.g:5308:2: (otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}' )
            // InternalAssetBasedSystemDsl.g:5309:3: otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,80,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getLocaleGroupAccess().getLocaleKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_77); 

            			newLeafNode(otherlv_1, grammarAccess.getLocaleGroupAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,81,FOLLOW_46); 

            			newLeafNode(otherlv_2, grammarAccess.getLocaleGroupAccess().getLocaleKeyword_2());
            		
            otherlv_3=(Token)match(input,53,FOLLOW_21); 

            			newLeafNode(otherlv_3, grammarAccess.getLocaleGroupAccess().getEqualsSignKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:5325:3: ( (lv_locale_4_0= RULE_STRING ) )
            // InternalAssetBasedSystemDsl.g:5326:4: (lv_locale_4_0= RULE_STRING )
            {
            // InternalAssetBasedSystemDsl.g:5326:4: (lv_locale_4_0= RULE_STRING )
            // InternalAssetBasedSystemDsl.g:5327:5: lv_locale_4_0= RULE_STRING
            {
            lv_locale_4_0=(Token)match(input,RULE_STRING,FOLLOW_78); 

            					newLeafNode(lv_locale_4_0, grammarAccess.getLocaleGroupAccess().getLocaleSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLocaleGroupRule());
            					}
            					setWithLastConsumed(
            						current,
            						"locale",
            						lv_locale_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalAssetBasedSystemDsl.g:5343:3: ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )*
            loop107:
            do {
                int alt107=2;
                int LA107_0 = input.LA(1);

                if ( (LA107_0==12) ) {
                    alt107=1;
                }


                switch (alt107) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:5344:4: (lv_objectLocales_5_0= ruleDefinitionGroupLocale )
            	    {
            	    // InternalAssetBasedSystemDsl.g:5344:4: (lv_objectLocales_5_0= ruleDefinitionGroupLocale )
            	    // InternalAssetBasedSystemDsl.g:5345:5: lv_objectLocales_5_0= ruleDefinitionGroupLocale
            	    {

            	    					newCompositeNode(grammarAccess.getLocaleGroupAccess().getObjectLocalesDefinitionGroupLocaleParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_78);
            	    lv_objectLocales_5_0=ruleDefinitionGroupLocale();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getLocaleGroupRule());
            	    					}
            	    					add(
            	    						current,
            	    						"objectLocales",
            	    						lv_objectLocales_5_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.DefinitionGroupLocale");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop107;
                }
            } while (true);

            otherlv_6=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getLocaleGroupAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLocaleGroup"


    // $ANTLR start "entryRuleDefinitionGroupLocale"
    // InternalAssetBasedSystemDsl.g:5370:1: entryRuleDefinitionGroupLocale returns [EObject current=null] : iv_ruleDefinitionGroupLocale= ruleDefinitionGroupLocale EOF ;
    public final EObject entryRuleDefinitionGroupLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinitionGroupLocale = null;


        try {
            // InternalAssetBasedSystemDsl.g:5370:62: (iv_ruleDefinitionGroupLocale= ruleDefinitionGroupLocale EOF )
            // InternalAssetBasedSystemDsl.g:5371:2: iv_ruleDefinitionGroupLocale= ruleDefinitionGroupLocale EOF
            {
             newCompositeNode(grammarAccess.getDefinitionGroupLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefinitionGroupLocale=ruleDefinitionGroupLocale();

            state._fsp--;

             current =iv_ruleDefinitionGroupLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinitionGroupLocale"


    // $ANTLR start "ruleDefinitionGroupLocale"
    // InternalAssetBasedSystemDsl.g:5377:1: ruleDefinitionGroupLocale returns [EObject current=null] : (otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}' ) ;
    public final EObject ruleDefinitionGroupLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_annotations_5_0 = null;

        EObject lv_definitionLocales_7_1 = null;

        EObject lv_definitionLocales_7_2 = null;

        EObject lv_definitionLocales_7_3 = null;

        EObject lv_definitionLocales_7_4 = null;

        EObject lv_definitionLocales_7_5 = null;

        EObject lv_definitionLocales_7_6 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5383:2: ( (otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}' ) )
            // InternalAssetBasedSystemDsl.g:5384:2: (otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}' )
            {
            // InternalAssetBasedSystemDsl.g:5384:2: (otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}' )
            // InternalAssetBasedSystemDsl.g:5385:3: otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionGroupKeyword_0());
            		
            // InternalAssetBasedSystemDsl.g:5389:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:5390:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:5390:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:5391:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDefinitionGroupLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getRefDefinitionGroupCrossReference_1_0());
            				
            pushFollow(FOLLOW_7);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_79); 

            			newLeafNode(otherlv_2, grammarAccess.getDefinitionGroupLocaleAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:5409:3: (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )?
            int alt109=2;
            int LA109_0 = input.LA(1);

            if ( (LA109_0==18) ) {
                alt109=1;
            }
            switch (alt109) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:5410:4: otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}'
                    {
                    otherlv_3=(Token)match(input,18,FOLLOW_7); 

                    				newLeafNode(otherlv_3, grammarAccess.getDefinitionGroupLocaleAccess().getAnnotationsKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,13,FOLLOW_13); 

                    				newLeafNode(otherlv_4, grammarAccess.getDefinitionGroupLocaleAccess().getLeftCurlyBracketKeyword_3_1());
                    			
                    // InternalAssetBasedSystemDsl.g:5418:4: ( (lv_annotations_5_0= ruleAnnotationEntry ) )*
                    loop108:
                    do {
                        int alt108=2;
                        int LA108_0 = input.LA(1);

                        if ( ((LA108_0>=RULE_STRING && LA108_0<=RULE_ID)) ) {
                            alt108=1;
                        }


                        switch (alt108) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:5419:5: (lv_annotations_5_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:5419:5: (lv_annotations_5_0= ruleAnnotationEntry )
                    	    // InternalAssetBasedSystemDsl.g:5420:6: lv_annotations_5_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getAnnotationsAnnotationEntryParserRuleCall_3_2_0());
                    	    					
                    	    pushFollow(FOLLOW_13);
                    	    lv_annotations_5_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_5_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop108;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,19,FOLLOW_80); 

                    				newLeafNode(otherlv_6, grammarAccess.getDefinitionGroupLocaleAccess().getRightCurlyBracketKeyword_3_3());
                    			

                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:5442:3: ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )*
            loop111:
            do {
                int alt111=2;
                int LA111_0 = input.LA(1);

                if ( (LA111_0==12||LA111_0==23||(LA111_0>=27 && LA111_0<=28)||(LA111_0>=83 && LA111_0<=84)) ) {
                    alt111=1;
                }


                switch (alt111) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:5443:4: ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) )
            	    {
            	    // InternalAssetBasedSystemDsl.g:5443:4: ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) )
            	    // InternalAssetBasedSystemDsl.g:5444:5: (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale )
            	    {
            	    // InternalAssetBasedSystemDsl.g:5444:5: (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale )
            	    int alt110=6;
            	    switch ( input.LA(1) ) {
            	    case 12:
            	        {
            	        alt110=1;
            	        }
            	        break;
            	    case 23:
            	        {
            	        alt110=2;
            	        }
            	        break;
            	    case 27:
            	        {
            	        alt110=3;
            	        }
            	        break;
            	    case 28:
            	        {
            	        alt110=4;
            	        }
            	        break;
            	    case 83:
            	        {
            	        alt110=5;
            	        }
            	        break;
            	    case 84:
            	        {
            	        alt110=6;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 110, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt110) {
            	        case 1 :
            	            // InternalAssetBasedSystemDsl.g:5445:6: lv_definitionLocales_7_1= ruleDefinitionGroupLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesDefinitionGroupLocaleParserRuleCall_4_0_0());
            	            					
            	            pushFollow(FOLLOW_80);
            	            lv_definitionLocales_7_1=ruleDefinitionGroupLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_1,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.DefinitionGroupLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 2 :
            	            // InternalAssetBasedSystemDsl.g:5461:6: lv_definitionLocales_7_2= ruleAssetTypeLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesAssetTypeLocaleParserRuleCall_4_0_1());
            	            					
            	            pushFollow(FOLLOW_80);
            	            lv_definitionLocales_7_2=ruleAssetTypeLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_2,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 3 :
            	            // InternalAssetBasedSystemDsl.g:5477:6: lv_definitionLocales_7_3= ruleAssetTypeAspectLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesAssetTypeAspectLocaleParserRuleCall_4_0_2());
            	            					
            	            pushFollow(FOLLOW_80);
            	            lv_definitionLocales_7_3=ruleAssetTypeAspectLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_3,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeAspectLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 4 :
            	            // InternalAssetBasedSystemDsl.g:5493:6: lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesPrimitiveDataTypeLocaleParserRuleCall_4_0_3());
            	            					
            	            pushFollow(FOLLOW_80);
            	            lv_definitionLocales_7_4=rulePrimitiveDataTypeLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_4,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.PrimitiveDataTypeLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 5 :
            	            // InternalAssetBasedSystemDsl.g:5509:6: lv_definitionLocales_7_5= ruleGuardLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesGuardLocaleParserRuleCall_4_0_4());
            	            					
            	            pushFollow(FOLLOW_80);
            	            lv_definitionLocales_7_5=ruleGuardLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_5,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 6 :
            	            // InternalAssetBasedSystemDsl.g:5525:6: lv_definitionLocales_7_6= ruleRequirementLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesRequirementLocaleParserRuleCall_4_0_5());
            	            					
            	            pushFollow(FOLLOW_80);
            	            lv_definitionLocales_7_6=ruleRequirementLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_6,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.RequirementLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop111;
                }
            } while (true);

            otherlv_8=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getDefinitionGroupLocaleAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinitionGroupLocale"


    // $ANTLR start "entryRuleAssetTypeLocale"
    // InternalAssetBasedSystemDsl.g:5551:1: entryRuleAssetTypeLocale returns [EObject current=null] : iv_ruleAssetTypeLocale= ruleAssetTypeLocale EOF ;
    public final EObject entryRuleAssetTypeLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeLocale = null;


        try {
            // InternalAssetBasedSystemDsl.g:5551:56: (iv_ruleAssetTypeLocale= ruleAssetTypeLocale EOF )
            // InternalAssetBasedSystemDsl.g:5552:2: iv_ruleAssetTypeLocale= ruleAssetTypeLocale EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeLocale=ruleAssetTypeLocale();

            state._fsp--;

             current =iv_ruleAssetTypeLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeLocale"


    // $ANTLR start "ruleAssetTypeLocale"
    // InternalAssetBasedSystemDsl.g:5558:1: ruleAssetTypeLocale returns [EObject current=null] : ( () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}' ) ;
    public final EObject ruleAssetTypeLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_description_9_0=null;
        Token otherlv_11=null;
        Enumerator lv_descriptionFormat_8_0 = null;

        EObject lv_features_10_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5564:2: ( ( () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}' ) )
            // InternalAssetBasedSystemDsl.g:5565:2: ( () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}' )
            {
            // InternalAssetBasedSystemDsl.g:5565:2: ( () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}' )
            // InternalAssetBasedSystemDsl.g:5566:3: () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}'
            {
            // InternalAssetBasedSystemDsl.g:5566:3: ()
            // InternalAssetBasedSystemDsl.g:5567:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeLocaleAccess().getAssetTypeLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,23,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetTypeLocaleAccess().getAssetTypeKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:5577:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:5578:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:5578:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:5579:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeLocaleAccess().getRefAssetTypeCrossReference_2_0());
            				
            pushFollow(FOLLOW_46);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,53,FOLLOW_21); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetTypeLocaleAccess().getEqualsSignKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:5597:3: ( (lv_name_4_0= RULE_STRING ) )
            // InternalAssetBasedSystemDsl.g:5598:4: (lv_name_4_0= RULE_STRING )
            {
            // InternalAssetBasedSystemDsl.g:5598:4: (lv_name_4_0= RULE_STRING )
            // InternalAssetBasedSystemDsl.g:5599:5: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_7); 

            					newLeafNode(lv_name_4_0, grammarAccess.getAssetTypeLocaleAccess().getNameSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_5=(Token)match(input,13,FOLLOW_81); 

            			newLeafNode(otherlv_5, grammarAccess.getAssetTypeLocaleAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalAssetBasedSystemDsl.g:5619:3: (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )?
            int alt113=2;
            int LA113_0 = input.LA(1);

            if ( (LA113_0==26) ) {
                alt113=1;
            }
            switch (alt113) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:5620:4: otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) )
                    {
                    otherlv_6=(Token)match(input,26,FOLLOW_46); 

                    				newLeafNode(otherlv_6, grammarAccess.getAssetTypeLocaleAccess().getDescriptionKeyword_6_0());
                    			
                    otherlv_7=(Token)match(input,53,FOLLOW_20); 

                    				newLeafNode(otherlv_7, grammarAccess.getAssetTypeLocaleAccess().getEqualsSignKeyword_6_1());
                    			
                    // InternalAssetBasedSystemDsl.g:5628:4: ( (lv_descriptionFormat_8_0= ruleTextFormat ) )?
                    int alt112=2;
                    int LA112_0 = input.LA(1);

                    if ( ((LA112_0>=100 && LA112_0<=102)) ) {
                        alt112=1;
                    }
                    switch (alt112) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:5629:5: (lv_descriptionFormat_8_0= ruleTextFormat )
                            {
                            // InternalAssetBasedSystemDsl.g:5629:5: (lv_descriptionFormat_8_0= ruleTextFormat )
                            // InternalAssetBasedSystemDsl.g:5630:6: lv_descriptionFormat_8_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getAssetTypeLocaleAccess().getDescriptionFormatTextFormatEnumRuleCall_6_2_0());
                            					
                            pushFollow(FOLLOW_21);
                            lv_descriptionFormat_8_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getAssetTypeLocaleRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_8_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:5647:4: ( (lv_description_9_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:5648:5: (lv_description_9_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:5648:5: (lv_description_9_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:5649:6: lv_description_9_0= RULE_STRING
                    {
                    lv_description_9_0=(Token)match(input,RULE_STRING,FOLLOW_82); 

                    						newLeafNode(lv_description_9_0, grammarAccess.getAssetTypeLocaleAccess().getDescriptionSTRINGTerminalRuleCall_6_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAssetTypeLocaleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_9_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:5666:3: ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )*
            loop114:
            do {
                int alt114=2;
                int LA114_0 = input.LA(1);

                if ( (LA114_0==32||LA114_0==39) ) {
                    alt114=1;
                }


                switch (alt114) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:5667:4: (lv_features_10_0= ruleAssetTypeFeatureLocale )
            	    {
            	    // InternalAssetBasedSystemDsl.g:5667:4: (lv_features_10_0= ruleAssetTypeFeatureLocale )
            	    // InternalAssetBasedSystemDsl.g:5668:5: lv_features_10_0= ruleAssetTypeFeatureLocale
            	    {

            	    					newCompositeNode(grammarAccess.getAssetTypeLocaleAccess().getFeaturesAssetTypeFeatureLocaleParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_82);
            	    lv_features_10_0=ruleAssetTypeFeatureLocale();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAssetTypeLocaleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"features",
            	    						lv_features_10_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeFeatureLocale");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop114;
                }
            } while (true);

            otherlv_11=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getAssetTypeLocaleAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeLocale"


    // $ANTLR start "entryRuleAssetTypeAspectLocale"
    // InternalAssetBasedSystemDsl.g:5693:1: entryRuleAssetTypeAspectLocale returns [EObject current=null] : iv_ruleAssetTypeAspectLocale= ruleAssetTypeAspectLocale EOF ;
    public final EObject entryRuleAssetTypeAspectLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeAspectLocale = null;


        try {
            // InternalAssetBasedSystemDsl.g:5693:62: (iv_ruleAssetTypeAspectLocale= ruleAssetTypeAspectLocale EOF )
            // InternalAssetBasedSystemDsl.g:5694:2: iv_ruleAssetTypeAspectLocale= ruleAssetTypeAspectLocale EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeAspectLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeAspectLocale=ruleAssetTypeAspectLocale();

            state._fsp--;

             current =iv_ruleAssetTypeAspectLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeAspectLocale"


    // $ANTLR start "ruleAssetTypeAspectLocale"
    // InternalAssetBasedSystemDsl.g:5700:1: ruleAssetTypeAspectLocale returns [EObject current=null] : ( () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}' ) ;
    public final EObject ruleAssetTypeAspectLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_features_4_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5706:2: ( ( () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}' ) )
            // InternalAssetBasedSystemDsl.g:5707:2: ( () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}' )
            {
            // InternalAssetBasedSystemDsl.g:5707:2: ( () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}' )
            // InternalAssetBasedSystemDsl.g:5708:3: () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}'
            {
            // InternalAssetBasedSystemDsl.g:5708:3: ()
            // InternalAssetBasedSystemDsl.g:5709:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeAspectLocaleAccess().getAssetTypeAspectLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,27,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetTypeAspectLocaleAccess().getAssetTypeAspectKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:5719:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:5720:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:5720:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:5721:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeAspectLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeAspectLocaleAccess().getBaseAssetTypeAssetTypeCrossReference_2_0());
            				
            pushFollow(FOLLOW_7);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_82); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetTypeAspectLocaleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:5739:3: ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )*
            loop115:
            do {
                int alt115=2;
                int LA115_0 = input.LA(1);

                if ( (LA115_0==32||LA115_0==39) ) {
                    alt115=1;
                }


                switch (alt115) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:5740:4: (lv_features_4_0= ruleAssetTypeFeatureLocale )
            	    {
            	    // InternalAssetBasedSystemDsl.g:5740:4: (lv_features_4_0= ruleAssetTypeFeatureLocale )
            	    // InternalAssetBasedSystemDsl.g:5741:5: lv_features_4_0= ruleAssetTypeFeatureLocale
            	    {

            	    					newCompositeNode(grammarAccess.getAssetTypeAspectLocaleAccess().getFeaturesAssetTypeFeatureLocaleParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_82);
            	    lv_features_4_0=ruleAssetTypeFeatureLocale();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAssetTypeAspectLocaleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"features",
            	    						lv_features_4_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeFeatureLocale");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop115;
                }
            } while (true);

            otherlv_5=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getAssetTypeAspectLocaleAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeAspectLocale"


    // $ANTLR start "entryRuleAssetTypeFeatureLocale"
    // InternalAssetBasedSystemDsl.g:5766:1: entryRuleAssetTypeFeatureLocale returns [EObject current=null] : iv_ruleAssetTypeFeatureLocale= ruleAssetTypeFeatureLocale EOF ;
    public final EObject entryRuleAssetTypeFeatureLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeFeatureLocale = null;


        try {
            // InternalAssetBasedSystemDsl.g:5766:63: (iv_ruleAssetTypeFeatureLocale= ruleAssetTypeFeatureLocale EOF )
            // InternalAssetBasedSystemDsl.g:5767:2: iv_ruleAssetTypeFeatureLocale= ruleAssetTypeFeatureLocale EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeFeatureLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeFeatureLocale=ruleAssetTypeFeatureLocale();

            state._fsp--;

             current =iv_ruleAssetTypeFeatureLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeFeatureLocale"


    // $ANTLR start "ruleAssetTypeFeatureLocale"
    // InternalAssetBasedSystemDsl.g:5773:1: ruleAssetTypeFeatureLocale returns [EObject current=null] : ( () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) ) ) ;
    public final EObject ruleAssetTypeFeatureLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_name_6_0=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5779:2: ( ( () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) ) ) )
            // InternalAssetBasedSystemDsl.g:5780:2: ( () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) ) )
            {
            // InternalAssetBasedSystemDsl.g:5780:2: ( () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) ) )
            // InternalAssetBasedSystemDsl.g:5781:3: () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) )
            {
            // InternalAssetBasedSystemDsl.g:5781:3: ()
            // InternalAssetBasedSystemDsl.g:5782:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeFeatureLocaleAccess().getAssetTypeFeatureLocaleAction_0(),
            					current);
            			

            }

            // InternalAssetBasedSystemDsl.g:5788:3: ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) )
            int alt116=2;
            int LA116_0 = input.LA(1);

            if ( (LA116_0==32) ) {
                alt116=1;
            }
            else if ( (LA116_0==39) ) {
                alt116=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 116, 0, input);

                throw nvae;
            }
            switch (alt116) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:5789:4: (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:5789:4: (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) )
                    // InternalAssetBasedSystemDsl.g:5790:5: otherlv_1= 'reference' ( ( ruleQualifiedName ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_12); 

                    					newLeafNode(otherlv_1, grammarAccess.getAssetTypeFeatureLocaleAccess().getReferenceKeyword_1_0_0());
                    				
                    // InternalAssetBasedSystemDsl.g:5794:5: ( ( ruleQualifiedName ) )
                    // InternalAssetBasedSystemDsl.g:5795:6: ( ruleQualifiedName )
                    {
                    // InternalAssetBasedSystemDsl.g:5795:6: ( ruleQualifiedName )
                    // InternalAssetBasedSystemDsl.g:5796:7: ruleQualifiedName
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getAssetTypeFeatureLocaleRule());
                    							}
                    						

                    							newCompositeNode(grammarAccess.getAssetTypeFeatureLocaleAccess().getRefAssetTypeReferenceCrossReference_1_0_1_0());
                    						
                    pushFollow(FOLLOW_46);
                    ruleQualifiedName();

                    state._fsp--;


                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:5812:4: (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) )
                    {
                    // InternalAssetBasedSystemDsl.g:5812:4: (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) )
                    // InternalAssetBasedSystemDsl.g:5813:5: otherlv_3= 'attribute' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,39,FOLLOW_12); 

                    					newLeafNode(otherlv_3, grammarAccess.getAssetTypeFeatureLocaleAccess().getAttributeKeyword_1_1_0());
                    				
                    // InternalAssetBasedSystemDsl.g:5817:5: ( ( ruleQualifiedName ) )
                    // InternalAssetBasedSystemDsl.g:5818:6: ( ruleQualifiedName )
                    {
                    // InternalAssetBasedSystemDsl.g:5818:6: ( ruleQualifiedName )
                    // InternalAssetBasedSystemDsl.g:5819:7: ruleQualifiedName
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getAssetTypeFeatureLocaleRule());
                    							}
                    						

                    							newCompositeNode(grammarAccess.getAssetTypeFeatureLocaleAccess().getRefAssetTypeAttributeCrossReference_1_1_1_0());
                    						
                    pushFollow(FOLLOW_46);
                    ruleQualifiedName();

                    state._fsp--;


                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,53,FOLLOW_21); 

            			newLeafNode(otherlv_5, grammarAccess.getAssetTypeFeatureLocaleAccess().getEqualsSignKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:5839:3: ( (lv_name_6_0= RULE_STRING ) )
            // InternalAssetBasedSystemDsl.g:5840:4: (lv_name_6_0= RULE_STRING )
            {
            // InternalAssetBasedSystemDsl.g:5840:4: (lv_name_6_0= RULE_STRING )
            // InternalAssetBasedSystemDsl.g:5841:5: lv_name_6_0= RULE_STRING
            {
            lv_name_6_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_6_0, grammarAccess.getAssetTypeFeatureLocaleAccess().getNameSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeFeatureLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_6_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeFeatureLocale"


    // $ANTLR start "entryRulePrimitiveDataTypeLocale"
    // InternalAssetBasedSystemDsl.g:5861:1: entryRulePrimitiveDataTypeLocale returns [EObject current=null] : iv_rulePrimitiveDataTypeLocale= rulePrimitiveDataTypeLocale EOF ;
    public final EObject entryRulePrimitiveDataTypeLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveDataTypeLocale = null;


        try {
            // InternalAssetBasedSystemDsl.g:5861:64: (iv_rulePrimitiveDataTypeLocale= rulePrimitiveDataTypeLocale EOF )
            // InternalAssetBasedSystemDsl.g:5862:2: iv_rulePrimitiveDataTypeLocale= rulePrimitiveDataTypeLocale EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveDataTypeLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimitiveDataTypeLocale=rulePrimitiveDataTypeLocale();

            state._fsp--;

             current =iv_rulePrimitiveDataTypeLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveDataTypeLocale"


    // $ANTLR start "rulePrimitiveDataTypeLocale"
    // InternalAssetBasedSystemDsl.g:5868:1: rulePrimitiveDataTypeLocale returns [EObject current=null] : ( () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}' ) ;
    public final EObject rulePrimitiveDataTypeLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_literals_6_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5874:2: ( ( () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}' ) )
            // InternalAssetBasedSystemDsl.g:5875:2: ( () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}' )
            {
            // InternalAssetBasedSystemDsl.g:5875:2: ( () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}' )
            // InternalAssetBasedSystemDsl.g:5876:3: () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}'
            {
            // InternalAssetBasedSystemDsl.g:5876:3: ()
            // InternalAssetBasedSystemDsl.g:5877:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPrimitiveDataTypeLocaleAccess().getPrimitiveDataTypeLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,28,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getPrimitiveDataTypeLocaleAccess().getPrimitiveDataTypeKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:5887:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:5888:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:5888:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:5889:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPrimitiveDataTypeLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getPrimitiveDataTypeLocaleAccess().getRefEnumDataTypeCrossReference_2_0());
            				
            pushFollow(FOLLOW_46);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,53,FOLLOW_21); 

            			newLeafNode(otherlv_3, grammarAccess.getPrimitiveDataTypeLocaleAccess().getEqualsSignKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:5907:3: ( (lv_name_4_0= RULE_STRING ) )
            // InternalAssetBasedSystemDsl.g:5908:4: (lv_name_4_0= RULE_STRING )
            {
            // InternalAssetBasedSystemDsl.g:5908:4: (lv_name_4_0= RULE_STRING )
            // InternalAssetBasedSystemDsl.g:5909:5: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_7); 

            					newLeafNode(lv_name_4_0, grammarAccess.getPrimitiveDataTypeLocaleAccess().getNameSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPrimitiveDataTypeLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_5=(Token)match(input,13,FOLLOW_83); 

            			newLeafNode(otherlv_5, grammarAccess.getPrimitiveDataTypeLocaleAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalAssetBasedSystemDsl.g:5929:3: ( (lv_literals_6_0= ruleEnumLiteralLocale ) )*
            loop117:
            do {
                int alt117=2;
                int LA117_0 = input.LA(1);

                if ( (LA117_0==82) ) {
                    alt117=1;
                }


                switch (alt117) {
            	case 1 :
            	    // InternalAssetBasedSystemDsl.g:5930:4: (lv_literals_6_0= ruleEnumLiteralLocale )
            	    {
            	    // InternalAssetBasedSystemDsl.g:5930:4: (lv_literals_6_0= ruleEnumLiteralLocale )
            	    // InternalAssetBasedSystemDsl.g:5931:5: lv_literals_6_0= ruleEnumLiteralLocale
            	    {

            	    					newCompositeNode(grammarAccess.getPrimitiveDataTypeLocaleAccess().getLiteralsEnumLiteralLocaleParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_83);
            	    lv_literals_6_0=ruleEnumLiteralLocale();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPrimitiveDataTypeLocaleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"literals",
            	    						lv_literals_6_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EnumLiteralLocale");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop117;
                }
            } while (true);

            otherlv_7=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getPrimitiveDataTypeLocaleAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveDataTypeLocale"


    // $ANTLR start "entryRuleEnumLiteralLocale"
    // InternalAssetBasedSystemDsl.g:5956:1: entryRuleEnumLiteralLocale returns [EObject current=null] : iv_ruleEnumLiteralLocale= ruleEnumLiteralLocale EOF ;
    public final EObject entryRuleEnumLiteralLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumLiteralLocale = null;


        try {
            // InternalAssetBasedSystemDsl.g:5956:58: (iv_ruleEnumLiteralLocale= ruleEnumLiteralLocale EOF )
            // InternalAssetBasedSystemDsl.g:5957:2: iv_ruleEnumLiteralLocale= ruleEnumLiteralLocale EOF
            {
             newCompositeNode(grammarAccess.getEnumLiteralLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEnumLiteralLocale=ruleEnumLiteralLocale();

            state._fsp--;

             current =iv_ruleEnumLiteralLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumLiteralLocale"


    // $ANTLR start "ruleEnumLiteralLocale"
    // InternalAssetBasedSystemDsl.g:5963:1: ruleEnumLiteralLocale returns [EObject current=null] : ( () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) ) ;
    public final EObject ruleEnumLiteralLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:5969:2: ( ( () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) ) )
            // InternalAssetBasedSystemDsl.g:5970:2: ( () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) )
            {
            // InternalAssetBasedSystemDsl.g:5970:2: ( () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) )
            // InternalAssetBasedSystemDsl.g:5971:3: () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) )
            {
            // InternalAssetBasedSystemDsl.g:5971:3: ()
            // InternalAssetBasedSystemDsl.g:5972:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEnumLiteralLocaleAccess().getEnumLiteralLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,82,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getEnumLiteralLocaleAccess().getEnumLiteralKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:5982:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:5983:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:5983:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:5984:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEnumLiteralLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getEnumLiteralLocaleAccess().getRefEnumLiteralCrossReference_2_0());
            				
            pushFollow(FOLLOW_46);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,53,FOLLOW_21); 

            			newLeafNode(otherlv_3, grammarAccess.getEnumLiteralLocaleAccess().getEqualsSignKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:6002:3: ( (lv_name_4_0= RULE_STRING ) )
            // InternalAssetBasedSystemDsl.g:6003:4: (lv_name_4_0= RULE_STRING )
            {
            // InternalAssetBasedSystemDsl.g:6003:4: (lv_name_4_0= RULE_STRING )
            // InternalAssetBasedSystemDsl.g:6004:5: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_4_0, grammarAccess.getEnumLiteralLocaleAccess().getNameSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEnumLiteralLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumLiteralLocale"


    // $ANTLR start "entryRuleGuardLocale"
    // InternalAssetBasedSystemDsl.g:6024:1: entryRuleGuardLocale returns [EObject current=null] : iv_ruleGuardLocale= ruleGuardLocale EOF ;
    public final EObject entryRuleGuardLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardLocale = null;


        try {
            // InternalAssetBasedSystemDsl.g:6024:52: (iv_ruleGuardLocale= ruleGuardLocale EOF )
            // InternalAssetBasedSystemDsl.g:6025:2: iv_ruleGuardLocale= ruleGuardLocale EOF
            {
             newCompositeNode(grammarAccess.getGuardLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuardLocale=ruleGuardLocale();

            state._fsp--;

             current =iv_ruleGuardLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardLocale"


    // $ANTLR start "ruleGuardLocale"
    // InternalAssetBasedSystemDsl.g:6031:1: ruleGuardLocale returns [EObject current=null] : ( () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}' ) ;
    public final EObject ruleGuardLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_description_9_0=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Enumerator lv_descriptionFormat_8_0 = null;

        EObject lv_annotations_12_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:6037:2: ( ( () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}' ) )
            // InternalAssetBasedSystemDsl.g:6038:2: ( () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}' )
            {
            // InternalAssetBasedSystemDsl.g:6038:2: ( () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}' )
            // InternalAssetBasedSystemDsl.g:6039:3: () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}'
            {
            // InternalAssetBasedSystemDsl.g:6039:3: ()
            // InternalAssetBasedSystemDsl.g:6040:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGuardLocaleAccess().getGuardLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,83,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getGuardLocaleAccess().getGuardKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:6050:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:6051:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:6051:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:6052:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGuardLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getGuardLocaleAccess().getRefGuardCrossReference_2_0());
            				
            pushFollow(FOLLOW_46);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,53,FOLLOW_21); 

            			newLeafNode(otherlv_3, grammarAccess.getGuardLocaleAccess().getEqualsSignKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:6070:3: ( (lv_name_4_0= RULE_STRING ) )
            // InternalAssetBasedSystemDsl.g:6071:4: (lv_name_4_0= RULE_STRING )
            {
            // InternalAssetBasedSystemDsl.g:6071:4: (lv_name_4_0= RULE_STRING )
            // InternalAssetBasedSystemDsl.g:6072:5: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_7); 

            					newLeafNode(lv_name_4_0, grammarAccess.getGuardLocaleAccess().getNameSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGuardLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_5=(Token)match(input,13,FOLLOW_84); 

            			newLeafNode(otherlv_5, grammarAccess.getGuardLocaleAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalAssetBasedSystemDsl.g:6092:3: (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )?
            int alt119=2;
            int LA119_0 = input.LA(1);

            if ( (LA119_0==26) ) {
                alt119=1;
            }
            switch (alt119) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6093:4: otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) )
                    {
                    otherlv_6=(Token)match(input,26,FOLLOW_46); 

                    				newLeafNode(otherlv_6, grammarAccess.getGuardLocaleAccess().getDescriptionKeyword_6_0());
                    			
                    otherlv_7=(Token)match(input,53,FOLLOW_20); 

                    				newLeafNode(otherlv_7, grammarAccess.getGuardLocaleAccess().getEqualsSignKeyword_6_1());
                    			
                    // InternalAssetBasedSystemDsl.g:6101:4: ( (lv_descriptionFormat_8_0= ruleTextFormat ) )?
                    int alt118=2;
                    int LA118_0 = input.LA(1);

                    if ( ((LA118_0>=100 && LA118_0<=102)) ) {
                        alt118=1;
                    }
                    switch (alt118) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:6102:5: (lv_descriptionFormat_8_0= ruleTextFormat )
                            {
                            // InternalAssetBasedSystemDsl.g:6102:5: (lv_descriptionFormat_8_0= ruleTextFormat )
                            // InternalAssetBasedSystemDsl.g:6103:6: lv_descriptionFormat_8_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getGuardLocaleAccess().getDescriptionFormatTextFormatEnumRuleCall_6_2_0());
                            					
                            pushFollow(FOLLOW_21);
                            lv_descriptionFormat_8_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getGuardLocaleRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_8_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:6120:4: ( (lv_description_9_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:6121:5: (lv_description_9_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:6121:5: (lv_description_9_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:6122:6: lv_description_9_0= RULE_STRING
                    {
                    lv_description_9_0=(Token)match(input,RULE_STRING,FOLLOW_85); 

                    						newLeafNode(lv_description_9_0, grammarAccess.getGuardLocaleAccess().getDescriptionSTRINGTerminalRuleCall_6_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getGuardLocaleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_9_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:6139:3: (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )?
            int alt121=2;
            int LA121_0 = input.LA(1);

            if ( (LA121_0==18) ) {
                alt121=1;
            }
            switch (alt121) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6140:4: otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}'
                    {
                    otherlv_10=(Token)match(input,18,FOLLOW_7); 

                    				newLeafNode(otherlv_10, grammarAccess.getGuardLocaleAccess().getAnnotationsKeyword_7_0());
                    			
                    otherlv_11=(Token)match(input,13,FOLLOW_13); 

                    				newLeafNode(otherlv_11, grammarAccess.getGuardLocaleAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalAssetBasedSystemDsl.g:6148:4: ( (lv_annotations_12_0= ruleAnnotationEntry ) )*
                    loop120:
                    do {
                        int alt120=2;
                        int LA120_0 = input.LA(1);

                        if ( ((LA120_0>=RULE_STRING && LA120_0<=RULE_ID)) ) {
                            alt120=1;
                        }


                        switch (alt120) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:6149:5: (lv_annotations_12_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:6149:5: (lv_annotations_12_0= ruleAnnotationEntry )
                    	    // InternalAssetBasedSystemDsl.g:6150:6: lv_annotations_12_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getGuardLocaleAccess().getAnnotationsAnnotationEntryParserRuleCall_7_2_0());
                    	    					
                    	    pushFollow(FOLLOW_13);
                    	    lv_annotations_12_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getGuardLocaleRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_12_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop120;
                        }
                    } while (true);

                    otherlv_13=(Token)match(input,19,FOLLOW_49); 

                    				newLeafNode(otherlv_13, grammarAccess.getGuardLocaleAccess().getRightCurlyBracketKeyword_7_3());
                    			

                    }
                    break;

            }

            otherlv_14=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_14, grammarAccess.getGuardLocaleAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardLocale"


    // $ANTLR start "entryRuleRequirement"
    // InternalAssetBasedSystemDsl.g:6180:1: entryRuleRequirement returns [EObject current=null] : iv_ruleRequirement= ruleRequirement EOF ;
    public final EObject entryRuleRequirement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequirement = null;


        try {
            // InternalAssetBasedSystemDsl.g:6180:52: (iv_ruleRequirement= ruleRequirement EOF )
            // InternalAssetBasedSystemDsl.g:6181:2: iv_ruleRequirement= ruleRequirement EOF
            {
             newCompositeNode(grammarAccess.getRequirementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRequirement=ruleRequirement();

            state._fsp--;

             current =iv_ruleRequirement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequirement"


    // $ANTLR start "ruleRequirement"
    // InternalAssetBasedSystemDsl.g:6187:1: ruleRequirement returns [EObject current=null] : (otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}' ) ;
    public final EObject ruleRequirement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_title_4_0=null;
        Token otherlv_5=null;
        Token lv_description_7_0=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Enumerator lv_descriptionFormat_6_0 = null;

        EObject lv_annotations_10_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:6193:2: ( (otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}' ) )
            // InternalAssetBasedSystemDsl.g:6194:2: (otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}' )
            {
            // InternalAssetBasedSystemDsl.g:6194:2: (otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}' )
            // InternalAssetBasedSystemDsl.g:6195:3: otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}'
            {
            otherlv_0=(Token)match(input,84,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getRequirementAccess().getRequirementKeyword_0());
            		
            // InternalAssetBasedSystemDsl.g:6199:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAssetBasedSystemDsl.g:6200:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAssetBasedSystemDsl.g:6200:4: (lv_name_1_0= RULE_ID )
            // InternalAssetBasedSystemDsl.g:6201:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_name_1_0, grammarAccess.getRequirementAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRequirementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_86); 

            			newLeafNode(otherlv_2, grammarAccess.getRequirementAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalAssetBasedSystemDsl.g:6221:3: (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )?
            int alt122=2;
            int LA122_0 = input.LA(1);

            if ( (LA122_0==85) ) {
                alt122=1;
            }
            switch (alt122) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6222:4: otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) )
                    {
                    otherlv_3=(Token)match(input,85,FOLLOW_21); 

                    				newLeafNode(otherlv_3, grammarAccess.getRequirementAccess().getTitleKeyword_3_0());
                    			
                    // InternalAssetBasedSystemDsl.g:6226:4: ( (lv_title_4_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:6227:5: (lv_title_4_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:6227:5: (lv_title_4_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:6228:6: lv_title_4_0= RULE_STRING
                    {
                    lv_title_4_0=(Token)match(input,RULE_STRING,FOLLOW_87); 

                    						newLeafNode(lv_title_4_0, grammarAccess.getRequirementAccess().getTitleSTRINGTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"title",
                    							lv_title_4_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:6245:3: (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )?
            int alt124=2;
            int LA124_0 = input.LA(1);

            if ( (LA124_0==26) ) {
                alt124=1;
            }
            switch (alt124) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6246:4: otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,26,FOLLOW_20); 

                    				newLeafNode(otherlv_5, grammarAccess.getRequirementAccess().getDescriptionKeyword_4_0());
                    			
                    // InternalAssetBasedSystemDsl.g:6250:4: ( (lv_descriptionFormat_6_0= ruleTextFormat ) )?
                    int alt123=2;
                    int LA123_0 = input.LA(1);

                    if ( ((LA123_0>=100 && LA123_0<=102)) ) {
                        alt123=1;
                    }
                    switch (alt123) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:6251:5: (lv_descriptionFormat_6_0= ruleTextFormat )
                            {
                            // InternalAssetBasedSystemDsl.g:6251:5: (lv_descriptionFormat_6_0= ruleTextFormat )
                            // InternalAssetBasedSystemDsl.g:6252:6: lv_descriptionFormat_6_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getRequirementAccess().getDescriptionFormatTextFormatEnumRuleCall_4_1_0());
                            					
                            pushFollow(FOLLOW_21);
                            lv_descriptionFormat_6_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getRequirementRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_6_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:6269:4: ( (lv_description_7_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:6270:5: (lv_description_7_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:6270:5: (lv_description_7_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:6271:6: lv_description_7_0= RULE_STRING
                    {
                    lv_description_7_0=(Token)match(input,RULE_STRING,FOLLOW_88); 

                    						newLeafNode(lv_description_7_0, grammarAccess.getRequirementAccess().getDescriptionSTRINGTerminalRuleCall_4_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_7_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:6288:3: (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )?
            int alt126=2;
            int LA126_0 = input.LA(1);

            if ( (LA126_0==18) ) {
                alt126=1;
            }
            switch (alt126) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6289:4: otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}'
                    {
                    otherlv_8=(Token)match(input,18,FOLLOW_7); 

                    				newLeafNode(otherlv_8, grammarAccess.getRequirementAccess().getAnnotationsKeyword_5_0());
                    			
                    otherlv_9=(Token)match(input,13,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getRequirementAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalAssetBasedSystemDsl.g:6297:4: ( (lv_annotations_10_0= ruleAnnotationEntry ) )*
                    loop125:
                    do {
                        int alt125=2;
                        int LA125_0 = input.LA(1);

                        if ( ((LA125_0>=RULE_STRING && LA125_0<=RULE_ID)) ) {
                            alt125=1;
                        }


                        switch (alt125) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:6298:5: (lv_annotations_10_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:6298:5: (lv_annotations_10_0= ruleAnnotationEntry )
                    	    // InternalAssetBasedSystemDsl.g:6299:6: lv_annotations_10_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getRequirementAccess().getAnnotationsAnnotationEntryParserRuleCall_5_2_0());
                    	    					
                    	    pushFollow(FOLLOW_13);
                    	    lv_annotations_10_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRequirementRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_10_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop125;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,19,FOLLOW_89); 

                    				newLeafNode(otherlv_11, grammarAccess.getRequirementAccess().getRightCurlyBracketKeyword_5_3());
                    			

                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:6321:3: (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )?
            int alt128=2;
            int LA128_0 = input.LA(1);

            if ( (LA128_0==86) ) {
                alt128=1;
            }
            switch (alt128) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6322:4: otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')'
                    {
                    otherlv_12=(Token)match(input,86,FOLLOW_9); 

                    				newLeafNode(otherlv_12, grammarAccess.getRequirementAccess().getContractsKeyword_6_0());
                    			
                    otherlv_13=(Token)match(input,15,FOLLOW_12); 

                    				newLeafNode(otherlv_13, grammarAccess.getRequirementAccess().getLeftParenthesisKeyword_6_1());
                    			
                    // InternalAssetBasedSystemDsl.g:6330:4: ( ( ruleQualifiedName ) )
                    // InternalAssetBasedSystemDsl.g:6331:5: ( ruleQualifiedName )
                    {
                    // InternalAssetBasedSystemDsl.g:6331:5: ( ruleQualifiedName )
                    // InternalAssetBasedSystemDsl.g:6332:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getRequirementAccess().getContractsContractCrossReference_6_2_0());
                    					
                    pushFollow(FOLLOW_11);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAssetBasedSystemDsl.g:6346:4: (otherlv_15= ',' ( ( ruleQualifiedName ) ) )*
                    loop127:
                    do {
                        int alt127=2;
                        int LA127_0 = input.LA(1);

                        if ( (LA127_0==16) ) {
                            alt127=1;
                        }


                        switch (alt127) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:6347:5: otherlv_15= ',' ( ( ruleQualifiedName ) )
                    	    {
                    	    otherlv_15=(Token)match(input,16,FOLLOW_12); 

                    	    					newLeafNode(otherlv_15, grammarAccess.getRequirementAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalAssetBasedSystemDsl.g:6351:5: ( ( ruleQualifiedName ) )
                    	    // InternalAssetBasedSystemDsl.g:6352:6: ( ruleQualifiedName )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:6352:6: ( ruleQualifiedName )
                    	    // InternalAssetBasedSystemDsl.g:6353:7: ruleQualifiedName
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getRequirementRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getRequirementAccess().getContractsContractCrossReference_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    ruleQualifiedName();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop127;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,17,FOLLOW_49); 

                    				newLeafNode(otherlv_17, grammarAccess.getRequirementAccess().getRightParenthesisKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_18=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_18, grammarAccess.getRequirementAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequirement"


    // $ANTLR start "entryRuleRequirementLocale"
    // InternalAssetBasedSystemDsl.g:6381:1: entryRuleRequirementLocale returns [EObject current=null] : iv_ruleRequirementLocale= ruleRequirementLocale EOF ;
    public final EObject entryRuleRequirementLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequirementLocale = null;


        try {
            // InternalAssetBasedSystemDsl.g:6381:58: (iv_ruleRequirementLocale= ruleRequirementLocale EOF )
            // InternalAssetBasedSystemDsl.g:6382:2: iv_ruleRequirementLocale= ruleRequirementLocale EOF
            {
             newCompositeNode(grammarAccess.getRequirementLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRequirementLocale=ruleRequirementLocale();

            state._fsp--;

             current =iv_ruleRequirementLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequirementLocale"


    // $ANTLR start "ruleRequirementLocale"
    // InternalAssetBasedSystemDsl.g:6388:1: ruleRequirementLocale returns [EObject current=null] : ( () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}' ) ;
    public final EObject ruleRequirementLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_title_6_0=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token lv_description_10_0=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Enumerator lv_descriptionFormat_9_0 = null;

        EObject lv_annotations_13_0 = null;



        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:6394:2: ( ( () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}' ) )
            // InternalAssetBasedSystemDsl.g:6395:2: ( () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}' )
            {
            // InternalAssetBasedSystemDsl.g:6395:2: ( () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}' )
            // InternalAssetBasedSystemDsl.g:6396:3: () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}'
            {
            // InternalAssetBasedSystemDsl.g:6396:3: ()
            // InternalAssetBasedSystemDsl.g:6397:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRequirementLocaleAccess().getRequirementLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,84,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getRequirementLocaleAccess().getRequirementKeyword_1());
            		
            // InternalAssetBasedSystemDsl.g:6407:3: ( ( ruleQualifiedName ) )
            // InternalAssetBasedSystemDsl.g:6408:4: ( ruleQualifiedName )
            {
            // InternalAssetBasedSystemDsl.g:6408:4: ( ruleQualifiedName )
            // InternalAssetBasedSystemDsl.g:6409:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRequirementLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getRequirementLocaleAccess().getRefRequirementCrossReference_2_0());
            				
            pushFollow(FOLLOW_7);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_90); 

            			newLeafNode(otherlv_3, grammarAccess.getRequirementLocaleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAssetBasedSystemDsl.g:6427:3: (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )?
            int alt129=2;
            int LA129_0 = input.LA(1);

            if ( (LA129_0==85) ) {
                alt129=1;
            }
            switch (alt129) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6428:4: otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) )
                    {
                    otherlv_4=(Token)match(input,85,FOLLOW_46); 

                    				newLeafNode(otherlv_4, grammarAccess.getRequirementLocaleAccess().getTitleKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,53,FOLLOW_21); 

                    				newLeafNode(otherlv_5, grammarAccess.getRequirementLocaleAccess().getEqualsSignKeyword_4_1());
                    			
                    // InternalAssetBasedSystemDsl.g:6436:4: ( (lv_title_6_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:6437:5: (lv_title_6_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:6437:5: (lv_title_6_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:6438:6: lv_title_6_0= RULE_STRING
                    {
                    lv_title_6_0=(Token)match(input,RULE_STRING,FOLLOW_84); 

                    						newLeafNode(lv_title_6_0, grammarAccess.getRequirementLocaleAccess().getTitleSTRINGTerminalRuleCall_4_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementLocaleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"title",
                    							lv_title_6_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:6455:3: (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )?
            int alt131=2;
            int LA131_0 = input.LA(1);

            if ( (LA131_0==26) ) {
                alt131=1;
            }
            switch (alt131) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6456:4: otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) )
                    {
                    otherlv_7=(Token)match(input,26,FOLLOW_46); 

                    				newLeafNode(otherlv_7, grammarAccess.getRequirementLocaleAccess().getDescriptionKeyword_5_0());
                    			
                    otherlv_8=(Token)match(input,53,FOLLOW_20); 

                    				newLeafNode(otherlv_8, grammarAccess.getRequirementLocaleAccess().getEqualsSignKeyword_5_1());
                    			
                    // InternalAssetBasedSystemDsl.g:6464:4: ( (lv_descriptionFormat_9_0= ruleTextFormat ) )?
                    int alt130=2;
                    int LA130_0 = input.LA(1);

                    if ( ((LA130_0>=100 && LA130_0<=102)) ) {
                        alt130=1;
                    }
                    switch (alt130) {
                        case 1 :
                            // InternalAssetBasedSystemDsl.g:6465:5: (lv_descriptionFormat_9_0= ruleTextFormat )
                            {
                            // InternalAssetBasedSystemDsl.g:6465:5: (lv_descriptionFormat_9_0= ruleTextFormat )
                            // InternalAssetBasedSystemDsl.g:6466:6: lv_descriptionFormat_9_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getRequirementLocaleAccess().getDescriptionFormatTextFormatEnumRuleCall_5_2_0());
                            					
                            pushFollow(FOLLOW_21);
                            lv_descriptionFormat_9_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getRequirementLocaleRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_9_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalAssetBasedSystemDsl.g:6483:4: ( (lv_description_10_0= RULE_STRING ) )
                    // InternalAssetBasedSystemDsl.g:6484:5: (lv_description_10_0= RULE_STRING )
                    {
                    // InternalAssetBasedSystemDsl.g:6484:5: (lv_description_10_0= RULE_STRING )
                    // InternalAssetBasedSystemDsl.g:6485:6: lv_description_10_0= RULE_STRING
                    {
                    lv_description_10_0=(Token)match(input,RULE_STRING,FOLLOW_85); 

                    						newLeafNode(lv_description_10_0, grammarAccess.getRequirementLocaleAccess().getDescriptionSTRINGTerminalRuleCall_5_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementLocaleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_10_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAssetBasedSystemDsl.g:6502:3: (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )?
            int alt133=2;
            int LA133_0 = input.LA(1);

            if ( (LA133_0==18) ) {
                alt133=1;
            }
            switch (alt133) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6503:4: otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}'
                    {
                    otherlv_11=(Token)match(input,18,FOLLOW_7); 

                    				newLeafNode(otherlv_11, grammarAccess.getRequirementLocaleAccess().getAnnotationsKeyword_6_0());
                    			
                    otherlv_12=(Token)match(input,13,FOLLOW_13); 

                    				newLeafNode(otherlv_12, grammarAccess.getRequirementLocaleAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalAssetBasedSystemDsl.g:6511:4: ( (lv_annotations_13_0= ruleAnnotationEntry ) )*
                    loop132:
                    do {
                        int alt132=2;
                        int LA132_0 = input.LA(1);

                        if ( ((LA132_0>=RULE_STRING && LA132_0<=RULE_ID)) ) {
                            alt132=1;
                        }


                        switch (alt132) {
                    	case 1 :
                    	    // InternalAssetBasedSystemDsl.g:6512:5: (lv_annotations_13_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalAssetBasedSystemDsl.g:6512:5: (lv_annotations_13_0= ruleAnnotationEntry )
                    	    // InternalAssetBasedSystemDsl.g:6513:6: lv_annotations_13_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getRequirementLocaleAccess().getAnnotationsAnnotationEntryParserRuleCall_6_2_0());
                    	    					
                    	    pushFollow(FOLLOW_13);
                    	    lv_annotations_13_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRequirementLocaleRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_13_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop132;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,19,FOLLOW_49); 

                    				newLeafNode(otherlv_14, grammarAccess.getRequirementLocaleAccess().getRightCurlyBracketKeyword_6_3());
                    			

                    }
                    break;

            }

            otherlv_15=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getRequirementLocaleAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequirementLocale"


    // $ANTLR start "ruleMultiplicitySingle"
    // InternalAssetBasedSystemDsl.g:6543:1: ruleMultiplicitySingle returns [Enumerator current=null] : ( (enumLiteral_0= '[1]' ) | (enumLiteral_1= '[0..1]' ) ) ;
    public final Enumerator ruleMultiplicitySingle() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:6549:2: ( ( (enumLiteral_0= '[1]' ) | (enumLiteral_1= '[0..1]' ) ) )
            // InternalAssetBasedSystemDsl.g:6550:2: ( (enumLiteral_0= '[1]' ) | (enumLiteral_1= '[0..1]' ) )
            {
            // InternalAssetBasedSystemDsl.g:6550:2: ( (enumLiteral_0= '[1]' ) | (enumLiteral_1= '[0..1]' ) )
            int alt134=2;
            int LA134_0 = input.LA(1);

            if ( (LA134_0==87) ) {
                alt134=1;
            }
            else if ( (LA134_0==88) ) {
                alt134=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 134, 0, input);

                throw nvae;
            }
            switch (alt134) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6551:3: (enumLiteral_0= '[1]' )
                    {
                    // InternalAssetBasedSystemDsl.g:6551:3: (enumLiteral_0= '[1]' )
                    // InternalAssetBasedSystemDsl.g:6552:4: enumLiteral_0= '[1]'
                    {
                    enumLiteral_0=(Token)match(input,87,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicitySingleAccess().getOneEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getMultiplicitySingleAccess().getOneEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:6559:3: (enumLiteral_1= '[0..1]' )
                    {
                    // InternalAssetBasedSystemDsl.g:6559:3: (enumLiteral_1= '[0..1]' )
                    // InternalAssetBasedSystemDsl.g:6560:4: enumLiteral_1= '[0..1]'
                    {
                    enumLiteral_1=(Token)match(input,88,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicitySingleAccess().getZeroOrOneEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getMultiplicitySingleAccess().getZeroOrOneEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicitySingle"


    // $ANTLR start "ruleMultiplicityMany"
    // InternalAssetBasedSystemDsl.g:6570:1: ruleMultiplicityMany returns [Enumerator current=null] : ( (enumLiteral_0= '[*]' ) | (enumLiteral_1= '[1..*]' ) ) ;
    public final Enumerator ruleMultiplicityMany() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:6576:2: ( ( (enumLiteral_0= '[*]' ) | (enumLiteral_1= '[1..*]' ) ) )
            // InternalAssetBasedSystemDsl.g:6577:2: ( (enumLiteral_0= '[*]' ) | (enumLiteral_1= '[1..*]' ) )
            {
            // InternalAssetBasedSystemDsl.g:6577:2: ( (enumLiteral_0= '[*]' ) | (enumLiteral_1= '[1..*]' ) )
            int alt135=2;
            int LA135_0 = input.LA(1);

            if ( (LA135_0==89) ) {
                alt135=1;
            }
            else if ( (LA135_0==90) ) {
                alt135=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 135, 0, input);

                throw nvae;
            }
            switch (alt135) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6578:3: (enumLiteral_0= '[*]' )
                    {
                    // InternalAssetBasedSystemDsl.g:6578:3: (enumLiteral_0= '[*]' )
                    // InternalAssetBasedSystemDsl.g:6579:4: enumLiteral_0= '[*]'
                    {
                    enumLiteral_0=(Token)match(input,89,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicityManyAccess().getZeroOrManyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getMultiplicityManyAccess().getZeroOrManyEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:6586:3: (enumLiteral_1= '[1..*]' )
                    {
                    // InternalAssetBasedSystemDsl.g:6586:3: (enumLiteral_1= '[1..*]' )
                    // InternalAssetBasedSystemDsl.g:6587:4: enumLiteral_1= '[1..*]'
                    {
                    enumLiteral_1=(Token)match(input,90,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicityManyAccess().getOneOrManyEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getMultiplicityManyAccess().getOneOrManyEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicityMany"


    // $ANTLR start "ruleSeverity"
    // InternalAssetBasedSystemDsl.g:6597:1: ruleSeverity returns [Enumerator current=null] : ( (enumLiteral_0= 'ERROR' ) | (enumLiteral_1= 'WARNING' ) ) ;
    public final Enumerator ruleSeverity() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:6603:2: ( ( (enumLiteral_0= 'ERROR' ) | (enumLiteral_1= 'WARNING' ) ) )
            // InternalAssetBasedSystemDsl.g:6604:2: ( (enumLiteral_0= 'ERROR' ) | (enumLiteral_1= 'WARNING' ) )
            {
            // InternalAssetBasedSystemDsl.g:6604:2: ( (enumLiteral_0= 'ERROR' ) | (enumLiteral_1= 'WARNING' ) )
            int alt136=2;
            int LA136_0 = input.LA(1);

            if ( (LA136_0==91) ) {
                alt136=1;
            }
            else if ( (LA136_0==92) ) {
                alt136=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 136, 0, input);

                throw nvae;
            }
            switch (alt136) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6605:3: (enumLiteral_0= 'ERROR' )
                    {
                    // InternalAssetBasedSystemDsl.g:6605:3: (enumLiteral_0= 'ERROR' )
                    // InternalAssetBasedSystemDsl.g:6606:4: enumLiteral_0= 'ERROR'
                    {
                    enumLiteral_0=(Token)match(input,91,FOLLOW_2); 

                    				current = grammarAccess.getSeverityAccess().getERROREnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getSeverityAccess().getERROREnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:6613:3: (enumLiteral_1= 'WARNING' )
                    {
                    // InternalAssetBasedSystemDsl.g:6613:3: (enumLiteral_1= 'WARNING' )
                    // InternalAssetBasedSystemDsl.g:6614:4: enumLiteral_1= 'WARNING'
                    {
                    enumLiteral_1=(Token)match(input,92,FOLLOW_2); 

                    				current = grammarAccess.getSeverityAccess().getWARNINGEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getSeverityAccess().getWARNINGEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeverity"


    // $ANTLR start "ruleActionEnum"
    // InternalAssetBasedSystemDsl.g:6624:1: ruleActionEnum returns [Enumerator current=null] : ( (enumLiteral_0= 'assign' ) | (enumLiteral_1= 'add' ) | (enumLiteral_2= 'addAll' ) | (enumLiteral_3= 'clear' ) | (enumLiteral_4= 'remove' ) | (enumLiteral_5= 'removeAll' ) | (enumLiteral_6= 'forAll' ) ) ;
    public final Enumerator ruleActionEnum() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:6630:2: ( ( (enumLiteral_0= 'assign' ) | (enumLiteral_1= 'add' ) | (enumLiteral_2= 'addAll' ) | (enumLiteral_3= 'clear' ) | (enumLiteral_4= 'remove' ) | (enumLiteral_5= 'removeAll' ) | (enumLiteral_6= 'forAll' ) ) )
            // InternalAssetBasedSystemDsl.g:6631:2: ( (enumLiteral_0= 'assign' ) | (enumLiteral_1= 'add' ) | (enumLiteral_2= 'addAll' ) | (enumLiteral_3= 'clear' ) | (enumLiteral_4= 'remove' ) | (enumLiteral_5= 'removeAll' ) | (enumLiteral_6= 'forAll' ) )
            {
            // InternalAssetBasedSystemDsl.g:6631:2: ( (enumLiteral_0= 'assign' ) | (enumLiteral_1= 'add' ) | (enumLiteral_2= 'addAll' ) | (enumLiteral_3= 'clear' ) | (enumLiteral_4= 'remove' ) | (enumLiteral_5= 'removeAll' ) | (enumLiteral_6= 'forAll' ) )
            int alt137=7;
            switch ( input.LA(1) ) {
            case 93:
                {
                alt137=1;
                }
                break;
            case 94:
                {
                alt137=2;
                }
                break;
            case 95:
                {
                alt137=3;
                }
                break;
            case 96:
                {
                alt137=4;
                }
                break;
            case 97:
                {
                alt137=5;
                }
                break;
            case 98:
                {
                alt137=6;
                }
                break;
            case 99:
                {
                alt137=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 137, 0, input);

                throw nvae;
            }

            switch (alt137) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6632:3: (enumLiteral_0= 'assign' )
                    {
                    // InternalAssetBasedSystemDsl.g:6632:3: (enumLiteral_0= 'assign' )
                    // InternalAssetBasedSystemDsl.g:6633:4: enumLiteral_0= 'assign'
                    {
                    enumLiteral_0=(Token)match(input,93,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getAssignEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getActionEnumAccess().getAssignEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:6640:3: (enumLiteral_1= 'add' )
                    {
                    // InternalAssetBasedSystemDsl.g:6640:3: (enumLiteral_1= 'add' )
                    // InternalAssetBasedSystemDsl.g:6641:4: enumLiteral_1= 'add'
                    {
                    enumLiteral_1=(Token)match(input,94,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getAddEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getActionEnumAccess().getAddEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalAssetBasedSystemDsl.g:6648:3: (enumLiteral_2= 'addAll' )
                    {
                    // InternalAssetBasedSystemDsl.g:6648:3: (enumLiteral_2= 'addAll' )
                    // InternalAssetBasedSystemDsl.g:6649:4: enumLiteral_2= 'addAll'
                    {
                    enumLiteral_2=(Token)match(input,95,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getAddAllEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getActionEnumAccess().getAddAllEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalAssetBasedSystemDsl.g:6656:3: (enumLiteral_3= 'clear' )
                    {
                    // InternalAssetBasedSystemDsl.g:6656:3: (enumLiteral_3= 'clear' )
                    // InternalAssetBasedSystemDsl.g:6657:4: enumLiteral_3= 'clear'
                    {
                    enumLiteral_3=(Token)match(input,96,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getClearEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getActionEnumAccess().getClearEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalAssetBasedSystemDsl.g:6664:3: (enumLiteral_4= 'remove' )
                    {
                    // InternalAssetBasedSystemDsl.g:6664:3: (enumLiteral_4= 'remove' )
                    // InternalAssetBasedSystemDsl.g:6665:4: enumLiteral_4= 'remove'
                    {
                    enumLiteral_4=(Token)match(input,97,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getRemoveEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getActionEnumAccess().getRemoveEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalAssetBasedSystemDsl.g:6672:3: (enumLiteral_5= 'removeAll' )
                    {
                    // InternalAssetBasedSystemDsl.g:6672:3: (enumLiteral_5= 'removeAll' )
                    // InternalAssetBasedSystemDsl.g:6673:4: enumLiteral_5= 'removeAll'
                    {
                    enumLiteral_5=(Token)match(input,98,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getRemoveAllEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getActionEnumAccess().getRemoveAllEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalAssetBasedSystemDsl.g:6680:3: (enumLiteral_6= 'forAll' )
                    {
                    // InternalAssetBasedSystemDsl.g:6680:3: (enumLiteral_6= 'forAll' )
                    // InternalAssetBasedSystemDsl.g:6681:4: enumLiteral_6= 'forAll'
                    {
                    enumLiteral_6=(Token)match(input,99,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getForAllEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_6, grammarAccess.getActionEnumAccess().getForAllEnumLiteralDeclaration_6());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionEnum"


    // $ANTLR start "ruleTextFormat"
    // InternalAssetBasedSystemDsl.g:6691:1: ruleTextFormat returns [Enumerator current=null] : ( (enumLiteral_0= 'plaintext' ) | (enumLiteral_1= 'html' ) | (enumLiteral_2= 'markdown' ) ) ;
    public final Enumerator ruleTextFormat() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalAssetBasedSystemDsl.g:6697:2: ( ( (enumLiteral_0= 'plaintext' ) | (enumLiteral_1= 'html' ) | (enumLiteral_2= 'markdown' ) ) )
            // InternalAssetBasedSystemDsl.g:6698:2: ( (enumLiteral_0= 'plaintext' ) | (enumLiteral_1= 'html' ) | (enumLiteral_2= 'markdown' ) )
            {
            // InternalAssetBasedSystemDsl.g:6698:2: ( (enumLiteral_0= 'plaintext' ) | (enumLiteral_1= 'html' ) | (enumLiteral_2= 'markdown' ) )
            int alt138=3;
            switch ( input.LA(1) ) {
            case 100:
                {
                alt138=1;
                }
                break;
            case 101:
                {
                alt138=2;
                }
                break;
            case 102:
                {
                alt138=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 138, 0, input);

                throw nvae;
            }

            switch (alt138) {
                case 1 :
                    // InternalAssetBasedSystemDsl.g:6699:3: (enumLiteral_0= 'plaintext' )
                    {
                    // InternalAssetBasedSystemDsl.g:6699:3: (enumLiteral_0= 'plaintext' )
                    // InternalAssetBasedSystemDsl.g:6700:4: enumLiteral_0= 'plaintext'
                    {
                    enumLiteral_0=(Token)match(input,100,FOLLOW_2); 

                    				current = grammarAccess.getTextFormatAccess().getPlaintextEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTextFormatAccess().getPlaintextEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalAssetBasedSystemDsl.g:6707:3: (enumLiteral_1= 'html' )
                    {
                    // InternalAssetBasedSystemDsl.g:6707:3: (enumLiteral_1= 'html' )
                    // InternalAssetBasedSystemDsl.g:6708:4: enumLiteral_1= 'html'
                    {
                    enumLiteral_1=(Token)match(input,101,FOLLOW_2); 

                    				current = grammarAccess.getTextFormatAccess().getHTMLEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTextFormatAccess().getHTMLEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalAssetBasedSystemDsl.g:6715:3: (enumLiteral_2= 'markdown' )
                    {
                    // InternalAssetBasedSystemDsl.g:6715:3: (enumLiteral_2= 'markdown' )
                    // InternalAssetBasedSystemDsl.g:6716:4: enumLiteral_2= 'markdown'
                    {
                    enumLiteral_2=(Token)match(input,102,FOLLOW_2); 

                    				current = grammarAccess.getTextFormatAccess().getMarkdownEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getTextFormatAccess().getMarkdownEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTextFormat"

    // Delegated rules


    protected DFA85 dfa85 = new DFA85(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\1\uffff\1\11\11\uffff";
    static final String dfa_3s = "\1\4\1\20\6\uffff\1\5\2\uffff";
    static final String dfa_4s = "\1\60\1\107\6\uffff\1\6\2\uffff";
    static final String dfa_5s = "\2\uffff\1\2\1\3\1\5\1\6\1\7\1\10\1\uffff\1\1\1\4";
    static final String dfa_6s = "\13\uffff}>";
    static final String[] dfa_7s = {
            "\1\2\1\4\1\1\10\uffff\1\7\24\uffff\1\6\1\5\11\uffff\2\3",
            "\2\11\1\uffff\1\11\22\uffff\1\11\21\uffff\1\11\3\uffff\4\11\1\uffff\6\11\1\10",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\11\1\12",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA85 extends DFA {

        public DFA85(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 85;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "4183:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000101002L,0x0000000000012800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000101002L,0x0000000000010000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000101002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x008E010078EC5000L,0x0000000000100000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080030L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x008E010078E81000L,0x0000000000100000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x04000600001C1000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0400060000181000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001002000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x00000081860C4000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000010L,0x0000007000000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000008180080000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000C00000002L,0x0000000007800000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000C00000002L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0001801000000070L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0001805000010070L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000004000010000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000040030L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000090000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000008004080000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000008000080000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000100000000002L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000200000000002L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0001803000000070L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000020030L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0050000004040000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000000L,0x0000000018000000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0001803000008070L,0x0000000000000001L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0040000004040000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0001803000088070L,0x0000000000000001L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x18000000040C0000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x1000000000080000L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x2000000000000002L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x4000000000000002L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x8000000000000002L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000000000000002L,0x000000000000007EL});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000006L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000080L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0000000000008002L,0x0000000000000080L});
    public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x000180300002A070L,0x0000000000000001L});
    public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0001807000018070L,0x0000000000000001L});
    public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
    public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x0000000000000000L,0x0000000FE0000000L});
    public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x0200000000080000L});
    public static final BitSet FOLLOW_70 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_71 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000600L});
    public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_73 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000400L});
    public static final BitSet FOLLOW_74 = new BitSet(new long[]{0x0200000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_75 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
    public static final BitSet FOLLOW_76 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_77 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_78 = new BitSet(new long[]{0x0000000000081000L});
    public static final BitSet FOLLOW_79 = new BitSet(new long[]{0x00000000188C1000L,0x0000000000180000L});
    public static final BitSet FOLLOW_80 = new BitSet(new long[]{0x0000000018881000L,0x0000000000180000L});
    public static final BitSet FOLLOW_81 = new BitSet(new long[]{0x0000008104080000L});
    public static final BitSet FOLLOW_82 = new BitSet(new long[]{0x0000008100080000L});
    public static final BitSet FOLLOW_83 = new BitSet(new long[]{0x0000000000080000L,0x0000000000040000L});
    public static final BitSet FOLLOW_84 = new BitSet(new long[]{0x00000000040C0000L});
    public static final BitSet FOLLOW_85 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_86 = new BitSet(new long[]{0x00000000040C0000L,0x0000000000600000L});
    public static final BitSet FOLLOW_87 = new BitSet(new long[]{0x00000000040C0000L,0x0000000000400000L});
    public static final BitSet FOLLOW_88 = new BitSet(new long[]{0x00000000000C0000L,0x0000000000400000L});
    public static final BitSet FOLLOW_89 = new BitSet(new long[]{0x0000000000080000L,0x0000000000400000L});
    public static final BitSet FOLLOW_90 = new BitSet(new long[]{0x00000000040C0000L,0x0000000000200000L});

}
