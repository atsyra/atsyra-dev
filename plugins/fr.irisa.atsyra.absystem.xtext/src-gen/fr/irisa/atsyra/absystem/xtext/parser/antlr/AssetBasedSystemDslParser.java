/*
 * generated by Xtext 2.27.0
 */
package fr.irisa.atsyra.absystem.xtext.parser.antlr;

import com.google.inject.Inject;
import fr.irisa.atsyra.absystem.xtext.parser.antlr.internal.InternalAssetBasedSystemDslParser;
import fr.irisa.atsyra.absystem.xtext.services.AssetBasedSystemDslGrammarAccess;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;

public class AssetBasedSystemDslParser extends AbstractAntlrParser {

	@Inject
	private AssetBasedSystemDslGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalAssetBasedSystemDslParser createParser(XtextTokenStream stream) {
		return new InternalAssetBasedSystemDslParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "AssetBasedSystem";
	}

	public AssetBasedSystemDslGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(AssetBasedSystemDslGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
