/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.ui.handlers;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.XtextResource;

import fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetBasedSystemK3Aspect;
import fr.irisa.atsyra.absystem.k3dsa.commons.InvalidConfigurationException;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.Contract;
import fr.irisa.atsyra.absystem.model.absystem.Expression;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.ui.Activator;
import fr.irisa.atsyra.absystem.validation.DomainContractValidator;

public class CheckDomainContractsHandler extends AbstractHandler {

	private static final String MARKER_TYPE = "fr.irisa.atsyra.absystem.ui.absDomainContractProblem";
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IFile file = getFileFromSelection(event);
		checkDomainContracts(file);
		return null;
	}
	
	public void checkDomainContracts(IFile absResource) {
		final ResourceSet rs = new ResourceSetImpl();
		// we have only one Goal file in the selection
		final URI uri = URI.createPlatformResourceURI(absResource.getFullPath().toString(), true);
		Resource res = rs.getResource(uri, true);
		AssetBasedSystem abs = (AssetBasedSystem) res.getContents().get(0);
		Job job = new Job("Check Domain Contracts") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					absResource.deleteMarkers(MARKER_TYPE, false, IResource.DEPTH_ZERO);
				} catch (CoreException ce) {
				}
				checkDomainContracts(abs);
				return Status.OK_STATUS;
			}
		};
		job.setPriority(Job.SHORT);
		job.setRule(absResource.getProject());
		job.schedule();
	}
	
	protected void checkDomainContracts(AssetBasedSystem abs) {
		if (abs.eResource().getErrors().isEmpty()) {
			
			DomainContractValidator validator = DomainContractValidator.INSTANCE;

			EcoreUtil.resolveAll(abs.eResource().getResourceSet());
			Set<AssetBasedSystem> absSet = abs.eResource().getResourceSet().getResources().stream()
					.filter(resource -> !resource.getContents().isEmpty()
							&& resource.getContents().get(0) instanceof AssetBasedSystem)
					.map(resource -> (AssetBasedSystem) resource.getContents().get(0)).collect(Collectors.toSet());
			try {
				AssetBasedSystemK3Aspect.initializeModel(abs, new BasicEList<>());
			} catch (InvalidConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Set<GuardOccurence> guardOccurences = validator.getDomainContractsOccurences(absSet);

			Activator.eclipseDebug("Checking " + guardOccurences.size() + " Domain contracts occurences on "+abs.eResource().getURI());
			
			Clock clock = Clock.systemUTC();
			Instant startTime = clock.instant();

			

			BasicDiagnostic results = new BasicDiagnostic();
			boolean valid = validator.validate(absSet, results, new Locale(ABSUtils.getLocale()));
			
			if(valid) {
				Activator.eclipseInfo("[OK] Domain contracts are valid");
			} else {
				for(Diagnostic diag : results.getChildren()) {
					if(diag.getSeverity() > Diagnostic.OK) {
						StringBuilder errorMessage = new StringBuilder();
						int markerSeverity = IMarker.SEVERITY_ERROR;
						if(diag.getSeverity() == Diagnostic.ERROR) {
							errorMessage.append("[ERROR]");
						} else if(diag.getSeverity() == Diagnostic.WARNING) {
							errorMessage.append("[WARNING]");
							markerSeverity = IMarker.SEVERITY_WARNING;
						} else {
							errorMessage.append("[ERROR]");
						}
						errorMessage.append(diag.getMessage());
						Contract contract = (Contract) diag.getData().get(0);
						Expression exp = contract.getGuardExpression();
						if (exp != null && exp.eResource() instanceof XtextResource
								&& exp.eResource().getURI() != null) {
							String fileURI = exp.eResource().getURI().toPlatformString(true);
							IFile workspaceFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(fileURI));
							if (workspaceFile != null) {
								ICompositeNode node = NodeModelUtils.findActualNodeFor(exp);
								if (node != null) {
									errorMessage.append("\n" + node.getText().trim());
								}
							}
						}
						Activator.eclipseError(errorMessage.toString(), null);
						EObject targetObject = (EObject) diag.getData().get(1);
						addMarker(targetObject, diag.getMessage(), markerSeverity);
						if(diag.getCode() == DomainContractValidator.STATIC_CONTRACT_EVALUATION_FAILED) {
							addMarker(contract, diag.getMessage(), markerSeverity);
						}
					}
				}
			}
			Instant endTime = clock.instant();
			Duration execTime = Duration.between(startTime, endTime);
			Activator.eclipseInfo("Domain contracts validation execution time: " + execTime.toSeconds() + " seconds and " + execTime.toNanosPart() + " nanos.");
			
		}
	}
	
	protected IFile getFileFromSelection(ExecutionEvent event){
		IFile selectedIFile = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();

				if (element instanceof IFile) {
					selectedIFile = (IFile) element;

				}
				if (element instanceof IAdaptable) {
					IFile res = (IFile) ((IAdaptable) element)
							.getAdapter(IFile.class);
					if (res != null) {
						selectedIFile = res;
					}
				}
			}
		}
		return selectedIFile;
	}
	
	private void addMarker(EObject modelElement, String message, int severity) {
		String fileURI = modelElement.eResource().getURI().toPlatformString(true);
		IFile workspaceFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(fileURI));
		try {
			IMarker marker = workspaceFile.createMarker(MARKER_TYPE);
			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, severity);
			ICompositeNode node = NodeModelUtils.findActualNodeFor(modelElement);
			if(node != null) {
				
				marker.setAttribute(IMarker.LINE_NUMBER, node.getStartLine());
				//marker.setAttribute(IMarker.CHAR_START, node.getOffset());
				//marker.setAttribute(IMarker.CHAR_END, node.getTotalEndOffset());
	//			int offset = node.getOffset();
	//			int length = node.getTotalEndOffset() - offset;
			} else {
				marker.setAttribute(IMarker.LINE_NUMBER, 1);
			}
		} catch (CoreException e) {
		}
	}

}
