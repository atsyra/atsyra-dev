/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.ui.helpers;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;

import fr.irisa.atsyra.absreport.ABSReportRuntimeModule;
import fr.irisa.atsyra.absreport.aBSReport.ABSGoalWitness;
import fr.irisa.atsyra.absreport.aBSReport.ABSReportModel;
import fr.irisa.atsyra.absreport.aBSReport.GoalReport;

public class ABSReportHelper {
	
	public static void markAllWitnessesObsolete(IFile file) throws IOException {
		Resource res = getResource(file);
		if(!res.getContents().isEmpty()) {
			ABSReportModel report = (ABSReportModel) res.getContents().get(0);
			markAllWitnessesObsolete(report);
			res.save(null);
		}
	}

	private static Resource getResource(IFile file) {
		Injector injector = Guice.createInjector(new ABSReportRuntimeModule());
		Provider<ResourceSet> resourceSetProvider = injector.getProvider(ResourceSet.class);
		final ResourceSet rs = resourceSetProvider.get();
		final URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
		return rs.getResource(uri, true);
	}
	
	private static void markAllWitnessesObsolete(ABSReportModel model) {
		for(GoalReport goal : model.getReports()) {
			markGoalWitnessesObsolete(goal);
		}
	}

	private static void markGoalWitnessesObsolete(GoalReport goal) {
		for(ABSGoalWitness witness : goal.getWitnesses()) {
			witness.getMetadata().setObsolete(true);
		}
	}
}
