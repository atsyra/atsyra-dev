/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

public class FullCheckHandler extends AbstractHandler {

	
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		
		CheckDomainContractsHandler domainContractHandler = new CheckDomainContractsHandler();
		domainContractHandler.execute(event);
			
		CheckEMFStructureHandler emfStructureHandler = new CheckEMFStructureHandler();
		emfStructureHandler.execute(event);
		
		CheckRequiremenContractsHandler requirementContractHandler = new CheckRequiremenContractsHandler();
		requirementContractHandler.execute(event);
		
		return null;
	}
}
