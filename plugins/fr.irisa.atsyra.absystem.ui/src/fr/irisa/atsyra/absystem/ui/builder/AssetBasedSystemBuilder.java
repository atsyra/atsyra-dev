/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.ui.builder;

import java.io.IOException;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;

import fr.irisa.atsyra.absystem.ui.handlers.CheckRequiremenContractsHandler;
import fr.irisa.atsyra.absystem.ui.helpers.ABSReportHelper;

public class AssetBasedSystemBuilder extends IncrementalProjectBuilder {

	class AssetBasedSystemDeltaVisitor implements IResourceDeltaVisitor {
		@Override
		public boolean visit(IResourceDelta delta) throws CoreException {
			IResource resource = delta.getResource();
			if ("abs".equals(resource.getFileExtension())) {
				markAllWitnessesObsolete(getDefaultReport());
				switch (delta.getKind()) {
				case IResourceDelta.ADDED:
					// handle added resource
					checkABSContract(resource);
					break;
				case IResourceDelta.REMOVED:
					// handle removed resource
					break;
				case IResourceDelta.CHANGED:
					// handle changed resource
					checkABSContract(resource);
					break;
				}
			}
			// return true to continue visiting children.
			return true;
		}
	}

	class AssetBasedSystemResourceVisitor implements IResourceVisitor {
		public boolean visit(IResource resource) throws CoreException {
			if ("abs".equals(resource.getFileExtension())) {
				checkABSContract(resource);
			} else if ("absreport".equals(resource.getFileExtension())) {
				markAllWitnessesObsolete(resource);
			}
			// return true to continue visiting children.
			return true;
		}
	}

	public static final String BUILDER_ID = "fr.irisa.atsyra.absystem.ui.absBuilder";

	private static final String MARKER_TYPE = "fr.irisa.atsyra.absystem.ui.absContractProblem";

	@Override
	protected IProject[] build(int kind, Map<String, String> args, IProgressMonitor monitor) throws CoreException {
		if (kind == FULL_BUILD) {
			fullBuild(monitor);
		} else {
			IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				fullBuild(monitor);
			} else {
				incrementalBuild(delta, monitor);
			}
		}
		return null;
	}

	public IResource getDefaultReport() {
		return getProject().getFile(Path.fromOSString("gen/report.absreport"));
	}

	public void markAllWitnessesObsolete(IResource resource) throws CoreException {
		if (resource instanceof IFile && "absreport".equals(resource.getFileExtension()) && resource.exists()) {
			IFile file = (IFile) resource;
			try {
				ABSReportHelper.markAllWitnessesObsolete(file);
			} catch (IOException e) {
				throw new CoreException(Status.error(e.getMessage(), e));
			}
		}
	}

	@Override
	protected void clean(IProgressMonitor monitor) throws CoreException {
		// delete markers set and files created
		getProject().deleteMarkers(MARKER_TYPE, true, IResource.DEPTH_INFINITE);
		if (getProject().getFolder("gen").exists()) {
			for (IResource r : getProject().getFolder("gen").members(false)) {
				if (!"absreport".equals(r.getFileExtension()) && !".gitignore".equals(r.getName())) {
					r.delete(true, monitor);
				}
			}
		}
	}

	void checkABSContract(IResource resource) throws CoreException {
		if (resource instanceof IFile && resource.getName().endsWith(".abs")) {
			IFile file = (IFile) resource;
			deleteMarkers(file);
			new CheckRequiremenContractsHandler().checkStaticContracts(file);
		}
	}

	private void deleteMarkers(IFile file) throws CoreException {
		file.deleteMarkers(MARKER_TYPE, false, IResource.DEPTH_ZERO);
	}

	protected void fullBuild(final IProgressMonitor monitor) throws CoreException {
		getProject().accept(new AssetBasedSystemResourceVisitor());
	}

	protected void incrementalBuild(IResourceDelta delta, IProgressMonitor monitor) throws CoreException {
		// the visitor does the work.
		delta.accept(new AssetBasedSystemDeltaVisitor());
	}
}
