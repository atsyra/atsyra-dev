/*******************************************************************************
 * Copyright (c) 2014, 2019 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.transfo.witness.uml.Witness2UMLGenerator;
import fr.irisa.atsyra.transfo.witness.uml.WitnessGeneratorContext;
import fr.irisa.atsyra.witness.witness.WitnessModel;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class WitnessToUMLHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		
		ISelection selection = HandlerUtil.getActiveMenuSelection(event); // the command is supposed to be launched via a menu
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();
				IResource res = null;
				if (element instanceof IResource) {
					res = (IResource) element;
				} else if (element instanceof IAdaptable) {
					res = (IResource) ((IAdaptable) element).getAdapter(IResource.class);
				}
				if (res != null) {
					generateUML(res);
				} else {
					IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
					MessageDialog.openInformation(
							window.getShell(),
							"Building IDE UI",
							"Cannot proceed WitnessToUML on this selection");
				}
			}
		}
		
		return null;
	}
	
	void generateUML(IResource resource) {
		if (resource instanceof IFile && resource.getName().endsWith(".witness")) {
			final IFile file = (IFile) resource;
			try {

				Job job = new Job("WitnessToUML from "+file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
						try {
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, subMonitor.split(5));
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute on "+ProcessConstants.GENFOLDER_NAME,e);
						}
						subMonitor.split(10);
						// do the job
						Injector inj = fr.irisa.atsyra.witness.xtext.ui.internal.XtextActivator.getInstance()
								.getInjector(fr.irisa.atsyra.witness.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_WITNESS_WITNESS);
						//Injector inj = XtextActivator.getInstance().getInjector(XtextActivator.FR_IRISA_ATSYRA_BUILDING_XTEXT_BUILDING);

						
						XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class); 
						resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
						//URI uri = URI.createFileURI(file.getRawLocationURI().getPath()) ;
						URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
						Resource eresource = resourceSet.getResource(uri, true);
						// if there is no witnesses, do nothing
						if(eresource.getContents().isEmpty()) return Status.OK_STATUS;
						WitnessModel witnessModel = (WitnessModel) eresource.getContents().get(0);
						if(!EcoreUtil.UnresolvedProxyCrossReferencer.find(witnessModel).isEmpty()) {
							Activator.debug("not generating UML for "+file.getName()+" because some proxies cannot be resolved yet, you may retry");

							return Status.OK_STATUS;
						}
						
						SubMonitor loopMonitor = subMonitor.split(85).setWorkRemaining(witnessModel.getWitnesses().size());
						IFolder genFolder = resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
						Witness2UMLGenerator generator = new Witness2UMLGenerator();
						IFile traceFile = genFolder.getFile(eresource.getURI().lastSegment().substring(0, eresource.getURI().lastSegment().lastIndexOf(".")) + ".gal.trace");
						if(traceFile.exists()) {
							WitnessGeneratorContext context = new WitnessGeneratorContext(traceFile);
							// TODO get diagram parameter from preference page
									
							generator.doGenerate(eresource, resource, genFolder, context, loopMonitor);
						}
						return Status.OK_STATUS;
					};
				};
				job.setPriority(Job.BUILD);
				job.setRule(resource.getProject());
				job.schedule();

									
			} catch (Exception e) {
				Activator.eclipseError(
						"ATG2GAL from "+file.getName()+" raised an exception "
								+ e.getMessage(), e);			
			}
			
			
		}
	}	
	
	
	
}
