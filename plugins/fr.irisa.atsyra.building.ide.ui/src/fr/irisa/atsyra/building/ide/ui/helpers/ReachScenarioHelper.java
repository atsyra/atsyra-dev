/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.helpers;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;

import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraGoalModel;
import atsyragoal.GoalCondition;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.transfo.atg.gal.GoalConditionHelper;
import fr.irisa.atsyra.transfo.building.gal.Building2GALForGoalConditionReachability;
import fr.irisa.atsyra.transfo.building.gal.Building2GALForReachability;
import fr.lip6.move.gal.GALTypeDeclaration;

public class ReachScenarioHelper {

	/**
	 * Generate the gal for reachability/witness search for a given goal.
	 * It will not be generated if the file is up to date
	 * @param goal
	 * @param galfile
	 * @param monitor
	 */
	public static void generateGAL4GoalIfNecessary(AtsyraGoal goal, IFile galfile, IProgressMonitor monitor){
		try {
			SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
			subMonitor.setTaskName("Generating "+galfile.getName()+" if necessary");
			SubMonitor subMonitorGeneration = subMonitor.split(30);
			IFile goalFile = AtsyraHelper.getIFileForEObject(goal);
			IFolder genFolder = galfile.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
			// search for the Building model corresponding to the goal model 
			BuildingModel buildingModel = AtsyraHelper.getBuildingModelFromGoalModel(goal.eResource().getResourceSet(), goalFile, goal.eResource());
			IFile buildingModelFile = AtsyraHelper.getIFileForEObject(buildingModel);
			
			if(galfile.exists() && goal.eContainer() instanceof AtsyraGoalModel) {
				AtsyraGoalModel goalModel = (AtsyraGoalModel) goal.eContainer();
				if( galfile.getLocalTimeStamp() >= goalFile.getLocalTimeStamp() &&  
					galfile.getLocalTimeStamp() >= buildingModelFile.getLocalTimeStamp() &&
					AtsyraHelper.getAllGoalModelIFiles(goalModel).stream()
							.anyMatch(importedGoalFile  -> galfile.getLocalTimeStamp() >= importedGoalFile.getLocalTimeStamp())  ) {
					return; // file is already uptodate
				}
			}

			Building2GALForReachability btgal = new Building2GALForReachability();
			GALTypeDeclaration galType = btgal.transformToGAL(buildingModel, goal);
			subMonitorGeneration.done();
			//get the non-flattened version, that we can check manually for debug
			String galNotFlatFileName = galfile.getFullPath().lastSegment() + "param.gal";
			IFile galNotFlatFile = genFolder.getFile(galNotFlatFileName);
			if(!galNotFlatFile.exists()) { galNotFlatFile.create(null, false, subMonitor.split(1));}
			btgal.writeToFile(galType, galNotFlatFile,	subMonitor.split(5));
			SubMonitor subMonitorFlatten = subMonitor.split(20); // changed from 30 to 20 while we have the non-flattended writing
			subMonitorFlatten.setTaskName("Flattening "+galfile.getName());
			//Activator.debug("disabled flattening "+galfile.getName());
			btgal.flattenGAL(galType);
			subMonitorFlatten.done();
			
			btgal.writeToFile(galType, galfile,	subMonitor.split(40));
			Activator.debug("Generated "+galfile.getName()+" from goal "+goal.getName());
			
			
			// also print the traceability information
			IFileUtils.writeInFileIfDifferent(
					genFolder.getFile(galfile.getName()+".trace"), 
					btgal.getGalVariableMapping(buildingModel), 
					subMonitor.split(5));

			genFolder.refreshLocal(IResource.DEPTH_INFINITE, subMonitor.split(1));
		} catch (Exception e) {
			Activator.eclipseError(e.getMessage(), e);
		}
	}
	
	/**
	 * Generate the gal for reachability/witness search for a given GoalCondition.
	 * It will not be generated if the file is up to date
	 * @param condition
	 * @param galfile
	 * @param monitor
	 */
	public static void generateGAL4GoalConditionIfNecessary(GoalCondition condition, IFile galfile, IProgressMonitor monitor){
		try {
			SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
			subMonitor.setTaskName("Generating "+galfile.getName()+" if necessary");
			SubMonitor subMonitorGeneration = subMonitor.split(30);
			IFile goalFile = AtsyraHelper.getIFileForEObject(condition);
			IFolder genFolder = galfile.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
			// search for the Building model corresponding to the goal model 
			BuildingModel buildingModel = AtsyraHelper.getBuildingModelFromGoalModel(condition.eResource().getResourceSet(), goalFile, condition.eResource());
			IFile buildingModelFile = AtsyraHelper.getIFileForEObject(buildingModel);
			
			if(galfile.exists() && condition.eResource().getContents().get(0) instanceof AtsyraGoalModel) {
				AtsyraGoalModel goalModel = (AtsyraGoalModel) condition.eResource().getContents().get(0);
				if( galfile.getLocalTimeStamp() >= goalFile.getLocalTimeStamp() &&  
					galfile.getLocalTimeStamp() >= buildingModelFile.getLocalTimeStamp() &&
					AtsyraHelper.getAllGoalModelIFiles(goalModel).stream()
							.anyMatch(importedGoalFile  -> galfile.getLocalTimeStamp() >= importedGoalFile.getLocalTimeStamp())  ) {
					Activator.warn("Force Generated "+galfile.getName()+" for debug ");
					// return; // file is already uptodate
				}
			}

			Building2GALForGoalConditionReachability btgal = new Building2GALForGoalConditionReachability();
			GALTypeDeclaration galType = btgal.transformToGAL(buildingModel, condition);
			subMonitorGeneration.done();
			SubMonitor subMonitorFlatten = subMonitor.split(30);
			subMonitorFlatten.setTaskName("Flattening "+galfile.getName());
			Activator.debug("disabled flattening "+galfile.getName());
			//btgal.flattenGAL(galType);
			subMonitorFlatten.done();
			
			btgal.writeToFile(galType, galfile,	subMonitor.split(40));
			Activator.debug("Generated "+galfile.getName()+" from condition "+GoalConditionHelper.getLabel(condition));

			// also print the traceability information
			IFileUtils.writeInFileIfDifferent(
					genFolder.getFile(galfile.getName()+".trace"), 
					btgal.getGalVariableMapping(buildingModel), 
					subMonitor.split(5));
			
			genFolder.refreshLocal(IResource.DEPTH_INFINITE, subMonitor);
		} catch (Exception e) {
			Activator.eclipseError(e.getMessage(), e);
		}
	}
	
}
