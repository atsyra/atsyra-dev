/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.dialogs;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gemoc.xdsmlframework.ui.utils.dialogs.SelectAnyEObjectDialog;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import atsyragoal.AtsyraGoal;
import atsyragoal.GoalCondition;



public class SelectAnyGoalConditionDialog extends SelectAnyEObjectDialog  {

	public SelectAnyGoalConditionDialog(ResourceSet resourceSet, ILabelProvider renderer) {
		this(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), resourceSet, renderer);
	}	

	public SelectAnyGoalConditionDialog(Shell parent, ResourceSet resourceSet,
			ILabelProvider renderer) {
		super(parent, resourceSet, renderer);
	}

	protected boolean select(EObject obj){
		if(obj instanceof GoalCondition && obj.eContainer() instanceof AtsyraGoal){
			return true;
		}
		return false;
	}
}

