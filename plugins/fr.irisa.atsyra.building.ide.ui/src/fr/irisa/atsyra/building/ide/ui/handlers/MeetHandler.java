/*******************************************************************************
 * Copyright (c) 2014, 2019 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.io.IOException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import atsyragoal.AtsyraTree;
import atsyragoal.AtsyraTreeOperator;
import fr.irisa.atsyra.atree.correctness.check.CorrectnessChecker;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.dialogs.SelectAnyAtsyraTreeDialog;
import fr.irisa.atsyra.building.ide.ui.dialogs.SelectAnyAtsyraTreeLeafDialog;
import fr.irisa.atsyra.building.ide.ui.helpers.AtsyraHelper;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.TreeResult;

public class MeetHandler extends AbstractAtsyraTreeSelectHandler {

	@Override
	public Object executeForSelectedAtsyraTree(ExecutionEvent event, IProject updatedGemocAtsyraTreeProject,
			AtsyraTree tree) throws ExecutionException {
		IFile atsyraTreeFile = getAtsyraTreeIFile(event, tree);
		if(tree.getOperator() == AtsyraTreeOperator.UNKNOWN){
			return null;
		}
		computeCorrectness(atsyraTreeFile, tree);
		return null;
	}	

	void computeCorrectness(IResource resource, AtsyraTree tree) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {
			final IFile file = (IFile) resource;
			try {
				Job job = new Job("Checking the meet property for the tree "+ tree.getName()+" from " + file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						try {
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, monitor);
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute on " + ProcessConstants.GENFOLDER_NAME,
									e);
						}

						// do the job
						Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
								.getInjector(
										fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
						XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class);
						resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
						// URI uri =
						// URI.createFileURI(file.getRawLocationURI().getPath())
						// ;
						URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
						Resource eresource = resourceSet.getResource(uri, true);
						IFile gal_file = getGalFile(file, tree);
						IFile ctl_file = getFormulaFile(file, tree);
						BuildingModel buildingModel = AtsyraHelper.getBuildingModelFromGoalModel(resourceSet, file, eresource);
						CorrectnessChecker checker = new CorrectnessChecker(gal_file, ctl_file, buildingModel);
						URI resultStoreUri = getResultStoreURI(file);
						TreeResult tr = getTreeResultHandler(tree, resultStoreUri);
						Result result = ResultstoreFactory.eINSTANCE.createResult();
						result.setTimestamp(new java.util.Date());
						result.setName("Meet");
						
						ResultValue ans = checker.checkMeet(tree, monitor, result);

						result.setValue(ans);
						tr.setMeetResult(result);
						try {
							tr.eResource().save(null);
						} catch (IOException e) {
							Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
						}
						return Status.OK_STATUS;
					};
				};
				job.setPriority(Job.LONG);
				// use mutex to prevent concurrent job
				IResourceRuleFactory ruleFactory = ResourcesPlugin.getWorkspace().getRuleFactory();
				ISchedulingRule rule = MultiRule.combine(
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)),
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME)));
				job.setRule(rule);
				job.schedule();

			} catch (Exception e) {
				Activator.eclipseError(
						"model-checking from " + file.getName() + " raised an exception " + e.getMessage(), e);
			}

		}
	}

	/**
	 * Search for the IFile of the goal either via the event or directly from
	 * the eResource
	 */
	protected IFile getAtsyraTreeIFile(ExecutionEvent event, AtsyraTree tree) {
		IFile atsyraTreeFile = getAtsyraTreeFileFromSelection(event);
		if (atsyraTreeFile == null) {
			// this means that we have to retrieve the IFile from the tree
			// instance (either because
			// it comes from an editor of because we have selected one tree
			// among other in the project)
			atsyraTreeFile = EMFResource.getIFile(tree);
		}
		return atsyraTreeFile;
	}

	/**
	 * get the file that will contain the gal
	 * @param file
	 * @param tree
	 * @return
	 */
	IFile getGalFile(IFile file, AtsyraTree tree) {
		// look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		IFile galFile = file.getProject()
				.getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension +"_"+tree.getName()+ "_correctness.gal");
		return galFile;
	}
	
	/**
	 * get the file that will contain the ctl formula
	 * @param file
	 * @param tree
	 * @return
	 */
	protected IFile getFormulaFile(IFile file, AtsyraTree tree)  {
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFile formulaFile = genFolder.getFile(fileNameNoExtension+"_"+tree.getName()+"_correctness_meet_formula.ctl");
		
		return formulaFile;
	}

	@Override
	protected SelectAnyAtsyraTreeDialog getNewSelectAnyAtsyraTreeDialog(ResourceSet rs, ILabelProvider labelProvider){
		final SelectAnyAtsyraTreeDialog dialog = new SelectAnyAtsyraTreeLeafDialog(rs, labelProvider);
		dialog.setTitle("Select a Tree (non leaf)");
		return dialog;
	}

	@Override
	public String getSelectionMessage() {

		return "Generate GAL and check Meet property for the selected tree.";
	}

}
