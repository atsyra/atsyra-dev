/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraGoalModel;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.helpers.ReachScenarioHelper;
import fr.irisa.atsyra.atsyra2.gal.GalReach;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ReachabilityAllGoalsHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();
				IResource res = null;
				if (element instanceof IResource) {
					res = (IResource) element;
				} else if (element instanceof IAdaptable) {
					res = (IResource) ((IAdaptable) element).getAdapter(IResource.class);
				}
				if (res != null) {
					computeReachability(res);
				} else {
					IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
					MessageDialog.openInformation(
							window.getShell(),
							"Building IDE UI",
							"Cannont proceed Reachability on this selection");
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Compute the reachability of all goals present in the given resource
	 * @param resource
	 */
	void computeReachability(IResource resource) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {
			final IFile file = (IFile) resource;
			try {

				Job job = new Job("Reachability of all goals in "+file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						IFolder genFolder = resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
						try {
							genFolder.setDerived(true, monitor);
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute on "+ProcessConstants.GENFOLDER_NAME,e);
						}
						
						// do the job
						Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
								.getInjector(fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
												
						XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class); 
						resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
						URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
						URI resultStoreUri = getResultStoreURI(file);
						Resource eresource = resourceSet.getResource(uri, true);
						AtsyraGoalModel goalModel = (AtsyraGoalModel) eresource.getContents().get(0);
						for(AtsyraGoal goal : goalModel.getAtsyragoals()){
						
							IFile galFile = genFolder.getFile(eresource.getURI().lastSegment().substring(0, eresource.getURI().lastSegment().lastIndexOf("."))+ "_" + goal.getName() + "_reach.gal");
							ReachScenarioHelper.generateGAL4GoalIfNecessary(goal, galFile, monitor);
							
							Result resultHanlder = getNewResultHandler(goal, resultStoreUri);
							GalReach grch = new GalReach(galFile, resultHanlder);
							grch.computeReachability(monitor);
							try {
								resultHanlder.eResource().save(null);
							} catch (IOException e) {
								Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
							}
						}

						return Status.OK_STATUS;
					};
				};
				job.setPriority(Job.LONG);
				// use mutex to prevent concurrent job
				//job.setRule(Activator.getDefault().getBuilderMutex().createCombinedRule(new IResource[]{resource.getProject().findMember("atsyra-gen")}));
				// prevent concurrent job
				if(ResourcesPlugin.getWorkspace().getRoot().getProject("__WorkspaceTraceModels").exists()){
					IResourceRuleFactory ruleFactory = ResourcesPlugin.getWorkspace().getRuleFactory();
					ISchedulingRule rule = MultiRule.combine(
							ruleFactory.createRule(resource.getProject()),
							ruleFactory.createRule(ResourcesPlugin.getWorkspace().getRoot().getProject("__WorkspaceTraceModels")));
					job.setRule(rule);
				} else {
					// simply lock the workscpace
					job.setRule(ResourcesPlugin.getWorkspace().getRoot());
				}
				job.schedule();
									
			} catch (Exception e) {
				Activator.eclipseError(
						"Reachability from "+file.getName()	+" raised an exception "
								+ e.getMessage(), e);
			}
			
			
		}
	} 	
	
	
	
	public Result getNewResultHandler(AtsyraGoal goal,  URI resultStoreUri){
		
		
		
		Resource resultStoreRes = goal.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<GoalResult> grOp = resultStore.getGoalResults().stream().filter(gr -> gr.getGoal().getName().equals(goal.getName())).findFirst();
		GoalResult gr;
		if(grOp.isPresent()){
			gr = grOp.get();
		} else {
			gr = ResultstoreFactory.eINSTANCE.createGoalResult();
			gr.setGoal(goal);
			resultStore.getGoalResults().add(gr);
		}
		Result result = ResultstoreFactory.eINSTANCE.createResult();
		result.setTimestamp(new java.util.Date());
		gr.setReachabilityResult(result);
		result.setName("Reachability of goal : "+goal.getName());
		return result;
	}
	
	/**
	 * look for a resultstore file with the same name, in genFolder
	 * @param URI
	 * @return
	 */
	public URI getResultStoreURI(IFile file){
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME);
		IFile resultStoreFile = file.getProject().getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension +".resultstore");
		
		URI resultStoreUri = URI.createPlatformResourceURI(resultStoreFile.getFullPath().toString(), true);
		if(!resultStoreFile.exists()){
			// make sure the file exist, 
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
	        Map<String, Object> m = reg.getExtensionToFactoryMap();
	        m.put("resultstore", new XMIResourceFactoryImpl());
			ResourceSet resSet = new ResourceSetImpl();
			Resource res = resSet.createResource(resultStoreUri);
			//Resource res = resSet.getResource(resultStoreUri, true);
			try {
				res.save(null);
			} catch (IOException e) {
				Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
			}
		}
		return resultStoreUri;
	}
	
	IFile getGalFile(IFile file, AtsyraGoal goal) {
		// look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
				
		IFile galFile = file.getProject()
				.getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension + "_" + goal.getName() + "_reach.gal");
		
		
		return galFile;
	}
	
	
}
