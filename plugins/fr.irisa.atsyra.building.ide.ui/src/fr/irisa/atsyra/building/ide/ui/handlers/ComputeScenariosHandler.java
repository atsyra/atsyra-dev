/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.jface.preference.IPreferenceStore;

import atsyragoal.AtsyraGoal;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.helpers.AtsyraHelper;
import fr.irisa.atsyra.building.ide.ui.helpers.ReachScenarioHelper;
import fr.irisa.atsyra.building.ide.ui.transfo.GalOutput2Scenario;
import fr.irisa.atsyra.atsyra2.gal.GalReach;
import fr.irisa.atsyra.gal.GalScenarios;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.WitnessResult;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ComputeScenariosHandler extends AbstractAtsyraGoalSelectHandler {

	
	void computeScenarios(IResource resource, AtsyraGoal goal) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {
			final IFile file = (IFile) resource;
			try {

				Job job = new Job("Shortest witness scenarios from "+file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
						try {
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, subMonitor.split(1));
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute on "+ProcessConstants.GENFOLDER_NAME,e);
						}
						
						// do the job
						SubMonitor subMonitorGenerate = subMonitor.split(5);
						IFile galFile = getGalFile(file, goal);
						subMonitorGenerate.setTaskName("Generating "+galFile.getName());
						ReachScenarioHelper.generateGAL4GoalIfNecessary(goal, galFile, monitor);
						subMonitorGenerate.done();
						IFile scenario_file = getScenarioFile(file, goal);
						URI resultStoreUri = getResultStoreURI(file);
						SubMonitor subMonitorReachable = subMonitor.split(5);
						if(!getLastReachableResult(goal, resultStoreUri).isPresent()) {							
							// need to compute reachability first
							computeIsReachable(goal, galFile, subMonitorReachable, resultStoreUri);
						} else	if(!AtsyraHelper.isResultUpToDate(getLastReachableResult(goal, resultStoreUri).get(),Arrays.asList(galFile))) {
							// if reachable result not uptodate	
							// need to recompute reachability first
							computeIsReachable(goal, galFile, subMonitorReachable, resultStoreUri);
						}
						
						// if reachable then compute witness otherwise ignore
						if(!getLastReachableResult(goal, resultStoreUri).get().getValue().equals(ResultValue.TRUE)) {
							Activator.important(goal.getName()+"  is not reachable. Will not try to search witness");
							// create empty scenario file
							GalOutput2Scenario.buildScenarioFileFromReachabilityTrace("", scenario_file, galFile.getName().replace(".gal", ""), goal.getName());
							
							return Status.OK_STATUS;
						}
						
						WitnessResult witnessResultHandler = getNewShortestWitnessResultHandler(goal, resultStoreUri);
						GalScenarios gscen = new GalScenarios(galFile){
							public void processGALOutpout(String stdOutString, boolean timeoutTrigerred){
								//Activator.debug(stdOutString);
								GalOutput2Scenario.buildScenarioFileFromReachabilityTrace(stdOutString, scenario_file, gal_file.getName().replace(".gal", ""), goal.getName());
								witnessResultHandler.getWitnessFound().clear();
								GalOutput2Scenario.getScenarioLines(stdOutString).stream().forEach(s -> witnessResultHandler.getWitnessFound().add(s));
								if(stdOutString.contains("Reachability property goal_reached==1 is true.")){
									Activator.important("Found "+witnessResultHandler.getWitnessFound().size()+" witness(es) for goal "+goal.getName());
									// TODO add/manage marker
									if(timeoutTrigerred){
										Activator.important("Timeout when searching witness of goal "+goal.getName());
										witnessResultHandler.setValue(ResultValue.TIMEOUT);
									} else {
										witnessResultHandler.setValue(ResultValue.TRUE);
									}
								}
								else{
									if(timeoutTrigerred){
										Activator.important("Timeout when searching witness of goal "+goal.getName());
										witnessResultHandler.setValue(ResultValue.TIMEOUT);
									} else {
										Activator.important("Goal "+goal.getName()+"  is not reachable.");
										witnessResultHandler.setValue(ResultValue.FALSE);
									}
								}

								witnessResultHandler.setLog(stdOutString);
							}	
						};

						SubMonitor subMonitorSearchingWitness = subMonitor.split(92);
						IPreferenceStore store = fr.irisa.atsyra.ide.ui.Activator.getDefault().getPreferenceStore();
						int nbWitness = store.getInt(fr.irisa.atsyra.ide.ui.preferences.PreferenceConstants.P_MANYWITNESS);
						int statePrintLimit = store.getInt(fr.irisa.atsyra.ide.ui.preferences.PreferenceConstants.P_STATE_PRINT_LIMIT);
						gscen.computeScenarios(nbWitness, statePrintLimit, null, subMonitorSearchingWitness);
						witnessResultHandler.setDuration(gscen.executionDuration);
						subMonitorSearchingWitness.done();
						try {
							witnessResultHandler.eResource().save(null);
						} catch (IOException e) {
							Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
						}
						return Status.OK_STATUS;
					};
				};
				job.setPriority(Job.LONG);
				// use mutex to prevent concurrent job
				IResourceRuleFactory ruleFactory = ResourcesPlugin.getWorkspace().getRuleFactory();
				ISchedulingRule rule = MultiRule.combine(
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)),
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME)));
				job.setRule(rule);
				job.schedule();
									
			} catch (Exception e) {
				Activator.eclipseError(
						"Reachability from "+file.getName()	+" raised an exception "
								+ e.getMessage(), e);
			}
		}
	}	
	
	public IFile getGalFile(IFile file, AtsyraGoal goal){
		//look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		IFile galFile = file.getProject().getFile(genFolder.getProjectRelativePath() + 
				"/" + fileNameNoExtension + "_" + goal.getName()+"_reach.gal");
		return galFile;
	}
	
	public IFile getScenarioFile(IFile file, AtsyraGoal goal){
		//look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME);
		IFile galFile = file.getProject().getFile(genFolder.getProjectRelativePath() + 
				"/" + fileNameNoExtension + "_" + goal.getName()+"_reach.witness");
		return galFile;
	}
	
	/** Search for the IFile of the goal either via the event or directly from the eResource */
	protected IFile getAtsyraGoalIFile(ExecutionEvent event, AtsyraGoal goal) {
		IFile atsyraGoalFile = getAtsyraGoalFileFromSelection(event);
		if (atsyraGoalFile == null) {
			// this means that we have to retrieve the IFile from the goal instance (either because 
			// it comes from an editor of because we have selected one goal among other in the project)
			atsyraGoalFile = EMFResource.getIFile(goal);
		}
		return atsyraGoalFile;
	}

	@Override
	public Object executeForSelectedAtsyraGoal(ExecutionEvent event, IProject updatedAtsyraGoalProject,
			AtsyraGoal goal) throws ExecutionException {
		IFile atsyraGoalFile = getAtsyraGoalIFile(event, goal);
		computeScenarios(atsyraGoalFile, goal);
		return null;
	}

	@Override
	public String getSelectionMessage() {
		return "Compute shortest witness scenarios for the selected goal.";
	}
	
	public WitnessResult getNewShortestWitnessResultHandler(AtsyraGoal goal,  URI resultStoreUri){
		
		Resource resultStoreRes = goal.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<GoalResult> grOp = resultStore.getGoalResults().stream().filter(gr -> gr.getGoal().getName().equals(goal.getName())).findFirst();
		GoalResult gr;
		if(grOp.isPresent()){
			gr = grOp.get();
		} else {
			gr = ResultstoreFactory.eINSTANCE.createGoalResult();
			gr.setGoal(goal);
			resultStore.getGoalResults().add(gr);
		}
		WitnessResult result = ResultstoreFactory.eINSTANCE.createWitnessResult();
		result.setTimestamp(new java.util.Date());
		gr.setShortestWitnessResult(result);
		result.setName("Shortest witness of goal : "+goal.getName());
		return result;
	}
	
	public Optional<Result> getLastReachableResult(AtsyraGoal goal,  URI resultStoreUri){
		Resource resultStoreRes = goal.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			return Optional.empty();
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<GoalResult> grOp = resultStore.getGoalResults().stream().filter(gr -> gr.getGoal().getName().equals(goal.getName())).findFirst();
		if(grOp.isPresent()){
			return Optional.ofNullable(grOp.get().getReachabilityResult());
		} else {
			return Optional.empty();
		}
	}
	public Result getNewReachableResultHandler(AtsyraGoal goal,  URI resultStoreUri){
		Resource resultStoreRes = goal.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<GoalResult> grOp = resultStore.getGoalResults().stream().filter(gr -> gr.getGoal().getName().equals(goal.getName())).findFirst();
		GoalResult gr;
		if(grOp.isPresent()){
			gr = grOp.get();
		} else {
			gr = ResultstoreFactory.eINSTANCE.createGoalResult();
			gr.setGoal(goal);
			resultStore.getGoalResults().add(gr);
		}
		Result result = ResultstoreFactory.eINSTANCE.createResult();
		result.setTimestamp(new java.util.Date());
		gr.setReachabilityResult(result);
		result.setName("Reachability of goal : "+goal.getName());
		// also clear complementary witnesses
		gr.getComplementaryWitnessResults().clear();
		return result;
	}
	
	protected void computeIsReachable(AtsyraGoal goal, IFile galFile, IProgressMonitor subMonitorReachable,  URI resultStoreUri) {
		Result resultHanlder = getNewReachableResultHandler(goal, resultStoreUri);
		GalReach grch = new GalReach(galFile, resultHanlder);
		grch.computeReachability(subMonitorReachable);
		try {
			resultHanlder.eResource().save(null);
		} catch (IOException e) {
			Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
		}
	}

}
