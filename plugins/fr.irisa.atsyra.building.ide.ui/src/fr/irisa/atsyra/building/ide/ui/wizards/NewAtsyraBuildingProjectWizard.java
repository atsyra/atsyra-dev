/*******************************************************************************
 * Copyright (c) 2014, 2019 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.gemoc.commons.eclipse.pde.wizards.pages.pde.AbstractNewProjectWizardWithTemplates;
import org.eclipse.gemoc.commons.eclipse.pde.wizards.pages.pde.ui.IProjectContentWizard;
import org.eclipse.gemoc.commons.eclipse.pde.wizards.pages.pde.ui.ProjectTemplateApplicationOperation;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.builder.BuildingNature;
import fr.irisa.atsyra.building.ide.ui.wizards.pages.NewAtsyraBuildingProjectWizardFields;
import fr.irisa.atsyra.building.ide.ui.wizards.pages.NewAtsyraBuildingProjectWizardPage;
import fr.irisa.atsyra.ide.ui.builder.ATSyRANature;

public class NewAtsyraBuildingProjectWizard extends AbstractNewProjectWizardWithTemplates implements INewWizard {


	
	protected NewAtsyraBuildingProjectWizardFields 		context;
	
	protected NewAtsyraBuildingProjectWizardPage 		projectPage;
	
	
	public NewAtsyraBuildingProjectWizard() {
		context = new NewAtsyraBuildingProjectWizardFields();
	}
	
	@Override
	public void addPages() {
		projectPage			 = new NewAtsyraBuildingProjectWizardPage(this.context);
		
		addPage(projectPage);			
		addPage(getTemplateListSelectionPage(context));
		
		
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean performFinish() {	
		try {
			IWorkspace workspace = ResourcesPlugin.getWorkspace(); 
			final IProjectDescription description = workspace.newProjectDescription(this.context.projectName);
			if (!this.context.projectLocation.equals(workspace.getRoot().getLocation().toOSString()))
				description.setLocation(new Path(this.context.projectLocation));
			
			final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(this.context.projectName);
			IWorkspaceRunnable operation = new IWorkspaceRunnable() {
				public void run(IProgressMonitor monitor) throws CoreException {
					project.create(description, monitor);
					project.open(monitor);
					
					configureProject(project, monitor);
					
					
					// launch the template
					
					if(templateSelectionPage != null) {
						IProjectContentWizard contentWizard = templateSelectionPage.getSelectedWizard();
						try {
							getContainer().run(false, true, new ProjectTemplateApplicationOperation(context, project, contentWizard));
						} catch (InvocationTargetException e) {
							Activator.eclipseError(e.getMessage(), e);
						} catch (InterruptedException e) {
							Activator.eclipseError(e.getMessage(), e);
						}
					}
					
					//setClassPath(project, monitor);
					project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
				}
			};
			ResourcesPlugin.getWorkspace().run(operation, null);
			
		} catch (Exception exception) {
			Activator.eclipseError(exception.getMessage(), exception);
			return false;
		}
		return true;
	}
	
	
	@Override
	public boolean isHelpAvailable() {
		return true;
	}
	
	public void configureProject(IProject project, IProgressMonitor monitor) {
		try {
			
			IProjectDescription description;
			description = project.getDescription();
			addNature(description, BuildingNature.NATURE_ID);
			addNature(description, ATSyRANature.NATURE_ID);
			addNature(description, "org.eclipse.xtext.ui.shared.xtextNature");
			
			project.setDescription(description, monitor);
		} catch (Exception e) {
			Activator.eclipseError(e.getMessage(), e);
		}
	}
	
	public static void addNature(IProjectDescription description, String nature) {
		String[] natures = description.getNatureIds();
		String[] newNatures = new String[natures.length + 1];
		System.arraycopy(natures, 0, newNatures, 0, natures.length);
		newNatures[natures.length] = nature;
		description.setNatureIds(newNatures);
	}	
	

    

	

		
	
	public NewAtsyraBuildingProjectWizardFields getContext() {
		return context;
	}
	

	public void addNatureToProject(IProject project, Boolean tabNature[]) {
		IProjectDescription description;
		try {
			
			description = project.getDescription();
			if (!tabNature[0] && !description.hasNature("fr.inria.diverse.k3.ui.k3Nature")){
				addNature(description, "fr.inria.diverse.k3.ui.k3Nature");
			}
			if (!tabNature[1] && !description.hasNature("org.eclipse.jdt.core.javanature")){
				addNature(description, "org.eclipse.jdt.core.javanature");
			}
			if(!tabNature[2] && !description.hasNature("org.eclipse.xtext.ui.shared.xtextNature")){
				addNature(description, "org.eclipse.xtext.ui.shared.xtextNature");				
			}
			if(!tabNature[3] && (!description.hasNature("org.eclipse.pde.PluginNature"))){
				addNature(description, "org.eclipse.pde.PluginNature");				
			}
			if(!tabNature[4] && (!description.hasNature("org.eclipse.m2e.core.maven2Nature"))){
				addNature(description, "org.eclipse.m2e.core.maven2Nature");			
			}
			project.setDescription(description, null);
		} catch (CoreException e) {
			Activator.eclipseError(e.getMessage(), e);
		}
	}
	
	/*
	private String getContextNamePackage() {
		if(this.context.namePackage == null || this.context.namePackage.isEmpty()){
			// create a name from the project name
			return JavaNameHelper.getFormattedPackageName(context.projectName);
		}
		else {
			return this.context.namePackage;
		}
	}
*/
	
	
	public NewAtsyraBuildingProjectWizardPage getPageProject() {
		return this.projectPage;
	}
	


	@Override
	public String getTargetPluginId() {		
		return Activator.PLUGIN_ID;
	}
	
	
	
}
