/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui;

import org.eclipse.osgi.util.NLS;

/**
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "fr.irisa.atsyra.building.ide.ui.messages"; //$NON-NLS-1$
	public static String PERFECT_LOCAL_MATCH;
	public static String UNRELATED_LOCAL_REFINEMENT;
	public static String WEAK_LOCAL_REFINEMENT;
	public static String UNPRECISE_LOCAL_REFINEMENT;
	public static String RESTRIVE_LOCAL_REFINEMENT;
	public static String INCOHERENT_RESULT;
	public static String INCOMPLETE_RESULT;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
