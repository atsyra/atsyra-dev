/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.builder;

import java.util.LinkedHashSet;
import java.util.Map;

import javax.xml.parsers.SAXParserFactory;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISources;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

import atsyragoal.AtsyraGoalModel;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.helpers.AtsyraHelper;
import fr.irisa.atsyra.building.xtext.ui.internal.XtextActivator;
import fr.irisa.atsyra.transfo.building.atg.Building2ATGGenerator;

public class BuildingBuilder extends IncrementalProjectBuilder {

	class BuildingDeltaVisitor implements IResourceDeltaVisitor {
		@Override
		public boolean visit(IResourceDelta delta) throws CoreException {
			IResource resource = delta.getResource();
			switch (delta.getKind()) {
			case IResourceDelta.ADDED:
				// handle added resource
				generateATG(resource);
				generateGoalToGALReach(resource);
				generateSequenceDiagramsFromWitness(resource);
				break;
			case IResourceDelta.REMOVED:
				// handle removed resource
				cleanGeneratedATG(resource);
				cleanGeneratedGoalToGALReach(resource);
				cleanSequenceDiagramsFromWitness(resource);
				break;
			case IResourceDelta.CHANGED:
				// handle changed resource
				generateATG(resource);
				generateGoalToGALReach(resource);
				generateSequenceDiagramsFromWitness(resource);
				break;
			}
			// return true to continue visiting children.
			return true;
		}
	}

	class BuildingResourceVisitor implements IResourceVisitor {
		public boolean visit(IResource resource) {
			generateATG(resource);
			generateGoalToGALReach(resource);
			generateSequenceDiagramsFromWitness(resource);
			// return true to continue visiting children.
			return true;
		}
	}

	public static final String BUILDER_ID = "fr.irisa.atsyra.building.ide.ui.buildingBuilder";

	private static final String MARKER_TYPE = "fr.irisa.atsyra.building.ide.ui.atsyraBuildingProblem";

	private SAXParserFactory parserFactory;

	private void addMarker(IFile file, String message, int lineNumber, int severity) {
		try {
			IMarker marker = file.createMarker(MARKER_TYPE);
			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, severity);
			if (lineNumber == -1) {
				lineNumber = 1;
			}
			marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
		} catch (CoreException e) {
		}
	}

	@Override
	protected IProject[] build(int kind, Map args, IProgressMonitor monitor) throws CoreException {
		if (kind == FULL_BUILD) {
			fullBuild(monitor);
		} else {
			IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				fullBuild(monitor);
			} else {
				incrementalBuild(delta, monitor);
			}
		}
		return null;
	}

	protected void clean(IProgressMonitor monitor) throws CoreException {
		// delete markers set and files created
		getProject().deleteMarkers(MARKER_TYPE, true, IResource.DEPTH_INFINITE);
	}

	@Inject
	Provider<EclipseResourceFileSystemAccess2> fileAccessProvider;

	void generateATG(IResource resource) {
		if (resource instanceof IFile && resource.getName().endsWith(".building")) {
			final IFile file = (IFile) resource;
			try {

				Job job = new Job("Building2ATG of " + file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
						try {
							if (!resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).exists()) {
								resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).create(true, true, null);
							}
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, subMonitor.split(1));
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute or create " + ProcessConstants.GENFOLDER_NAME,
									e);
						}
						deleteMarkers(file);
						SubMonitor subMonitorGeneration = subMonitor.split(99);
						subMonitor.setTaskName("Generating _definition.atg for "+file.getName());
						// do the job
						Injector inj = XtextActivator.getInstance()
								.getInjector(XtextActivator.FR_IRISA_ATSYRA_BUILDING_XTEXT_BUILDING);

						XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class);
						resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
						URI uri = URI.createFileURI(file.getRawLocationURI().getPath());
						Resource eresource = resourceSet.getResource(uri, true);
						Building2ATGGenerator generator = new Building2ATGGenerator();
						generator.doGenerate(eresource, resource,
								resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME), subMonitorGeneration);
						return Status.OK_STATUS;
					};
				};
				job.setRule(resource.getProject());
				job.setPriority(Job.SHORT);				
				job.schedule();
			} catch (Exception e) {
				Activator.eclipseError("Building2ATG of " + file.getName() + " raised an exception " + e.getMessage(),
						e);
			}

		}
	}

	void cleanGeneratedATG(IResource resource) {
		Job job = new Job("Cleaning outputs of Building2ATG for " + resource.getName()) {
			protected IStatus run(IProgressMonitor monitor) {
				if (resource instanceof IFile && resource.getName().endsWith(".building")) {
					IFile generatedATG = resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)
							.getFile(resource.getName().substring(0, resource.getName().lastIndexOf(".")) + "_definition.atg");
					if (generatedATG.exists()) {
						try {
							generatedATG.delete(true, null);
						} catch (CoreException e) {
							Activator.eclipseError(e.getMessage(), e);
						}
					}
				}
				return Status.OK_STATUS;
			};
		};
		job.setPriority(Job.SHORT);
		job.setRule(resource.getProject()); // make sure to lock project in order to prevent intermediate generation 
		job.schedule();
	}

	/**
	 * generateGoalToGALReach is triggered on a change on an .atg
	 * 
	 * @param resource
	 */
	void generateGoalToGALReach(IResource resource) {
		if (resource instanceof IFile && 
				resource.getName().endsWith(".atg")  && 
				!resource.getProjectRelativePath().segment(0).equals("building-gen")) {
			final IFile file = (IFile) resource;

			// TODO we should probably do a clean in order to remove files due
			// to a change of the goal name
			// ie. remove <resource base name>_*_reach.gal files
			// however this would be inefficient
			
			// to generate, simply call to the command handler, it will take care of doing
			// nothing if there is nothing to do for the given resource ;-)
			callCommandWithFile("fr.irisa.atsyra.building.ide.ui.commands.goalToGALReachabilityCommand", file);
			

		}
		if (resource instanceof IFile && 
				resource.getName().endsWith(".atg")  && 
				resource.getProjectRelativePath().segment(0).equals("building-gen")) {
			// this is a generated atg (ie. _definition.atg)
			final IFile file = (IFile) resource;

			// search for atg files that imports this atg and call the command
			try {
				file.getProject().accept(new IResourceVisitor() {
					public boolean visit(IResource res) {
						if (res instanceof IFile && 
								res.getName().endsWith(".atg")  && 
								!res.getProjectRelativePath().segment(0).equals("building-gen")) {
							LinkedHashSet<IFile> iFileSet =  new LinkedHashSet<IFile>();
							Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
									.getInjector(fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);

							XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class); 
							resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
							//URI uri = URI.createFileURI(file.getRawLocationURI().getPath()) ;
							URI uri = URI.createPlatformResourceURI(res.getFullPath().toString(), true);
							Resource eresource = resourceSet.getResource(uri, true);
							AtsyraGoalModel goalModel = (AtsyraGoalModel) eresource.getContents().get(0);
							AtsyraHelper.getAllImportedIFiles(goalModel, iFileSet);
							if(iFileSet.contains(resource)) {
								callCommandWithFile("fr.irisa.atsyra.building.ide.ui.commands.goalToGALReachabilityCommand", (IFile) res);
							}
						}
				        return true;
				      }
				});
			} catch (CoreException e) {
				Activator.eclipseError(e.getMessage(), e);
			}
		}
	}

	/**
	 * cleanGeneratedGoalToGALReach is triggered on a change on an .atg
	 * 
	 * @param resource
	 */
	void cleanGeneratedGoalToGALReach(IResource resource) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {

			// TODO implement !
			// use Capra to find out every generated gal file from the given atg
			/*
			 * IFile generatedATG =
			 * resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).
			 * getFile( resource.getName().substring(0,
			 * resource.getName().lastIndexOf("."))+"_definition.atg");
			 * if(generatedATG.exists()){ try { generatedATG.delete(true, null);
			 * } catch (CoreException e) {
			 * Activator.eclipseError(e.getMessage(), e); } }
			 */
		}
	}
	
	void generateSequenceDiagramsFromWitness(IResource resource) {
		if (resource instanceof IFile && 
				resource.getName().endsWith(".witness")  && 
				!resource.getProjectRelativePath().segment(0).equals("building-gen")) {
			final IFile file = (IFile) resource;

			// TODO we should probably do a clean in order to remove files due
			// to a change of the goal name
			// ie. remove <resource base name>_*_reach.gal files
			// however this would be inefficient
			
			// to generate, simply call to the command handler, it will take care of doing
			// nothing if there is nothing to do for the given resource ;-)
			callCommandWithFile("fr.irisa.atsyra.building.ide.ui.commands.sequence", file);
		}
	}
	
	void cleanSequenceDiagramsFromWitness(IResource resource) {
		Job job = new Job("Cleaning outputs of Witness2SequenceDiagram for " + resource.getName()) {
			protected IStatus run(IProgressMonitor monitor) {
				if (resource instanceof IFile && resource.getName().endsWith(".witness") && 
						!resource.getProjectRelativePath().segment(0).equals("building-gen")) {
					IFile generated = resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)
							.getFile(resource.getName().substring(0, resource.getName().lastIndexOf(".")) + "_withsequencediagram.witness");
					if (generated.exists()) {
						try {
							generated.delete(true, null);
						} catch (CoreException e) {
							Activator.eclipseError(e.getMessage(), e);
						}
					}
				}
				return Status.OK_STATUS;
			};
		};
		job.setPriority(Job.SHORT);
		job.setRule(resource.getProject()); // make sure to lock project in order to prevent intermediate generation 
		job.schedule();
	}

	public void callCommandWithFile(String commandId, IFile fileSelection){
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				IWorkbench wb = PlatformUI.getWorkbench();
				ICommandService commandService = wb.getService(ICommandService.class);
				IHandlerService handlerService = wb.getService(IHandlerService.class);

				Command command = commandService
						.getCommand(commandId);
				ExecutionEvent executionEvent = handlerService.createExecutionEvent(command, null);
				IEvaluationContext context = (IEvaluationContext) executionEvent.getApplicationContext();
				context.getParent().addVariable(ISources.ACTIVE_MENU_SELECTION_NAME, new StructuredSelection(fileSelection));
				try {
					command.executeWithChecks(executionEvent);
				} catch (ExecutionException | NotDefinedException | NotEnabledException | NotHandledException e) {
					Activator.eclipseError(e.getMessage(), e);
				}
			}
		});
	}
	
	private void deleteMarkers(IFile file) {
		try {
			file.deleteMarkers(MARKER_TYPE, false, IResource.DEPTH_ZERO);
		} catch (CoreException ce) {
		}
	}

	protected void fullBuild(final IProgressMonitor monitor) throws CoreException {
		try {
			getProject().accept(new BuildingResourceVisitor());
		} catch (CoreException e) {
		}
	}


	protected void incrementalBuild(IResourceDelta delta, IProgressMonitor monitor) throws CoreException {
		// the visitor does the work.
		delta.accept(new BuildingDeltaVisitor());
	}
}
