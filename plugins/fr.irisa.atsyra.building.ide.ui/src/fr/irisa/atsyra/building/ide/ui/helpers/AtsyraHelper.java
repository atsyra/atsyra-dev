/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.helpers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import atsyragoal.AtsyraGoalModel;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.gal.Activator;
import fr.irisa.atsyra.resultstore.Result;

public class AtsyraHelper {

	public static String normalizeName(String input){
		return input.replaceAll("é", "e").replaceAll("è", "e")
				.replaceAll("ô", "o")
				.replaceAll("ù", "u")
				.replaceAll("'", "")
				.replaceAll(" ", "_");
	}
	
	
	/**
	 * retrieve the IFile corresponding to the given AtsyraGoal.Import 
	 * @return
	 */
	public static IFile getGoalImportTargetIFile(atsyragoal.Import imprt){
		URI u = URI.createURI(imprt.getImportURI());
		URI importedFileURI = u.resolve(imprt.eResource().getURI()); // resolve against base uri of the caller context
		if (importedFileURI.isPlatformResource()) {
			String platformString = importedFileURI.toPlatformString(true);
			return (IFile) ResourcesPlugin.getWorkspace().getRoot().findMember(platformString);
		}
		return null;
	}
	
	/**
	 * retrieve the IFile corresponding to the given Buidling.Import 
	 * @return
	 */
	public static IFile getBuildingImportTargetIFile(fr.irisa.atsyra.building.Import imprt){
		URI u = URI.createURI(imprt.getImportURI());
		URI importedFileURI = u.resolve(imprt.eResource().getURI()); // resolve against base uri of the caller context
		if (importedFileURI.isPlatformResource()) {
			String platformString = importedFileURI.toPlatformString(true);
			return (IFile) ResourcesPlugin.getWorkspace().getRoot().findMember(platformString);
		}
		return null;
	}

	/*
	 * open all imports of the GoalModel
	 * search imports whose name maps with a .building file at the root of the current project
	 * then try to search the first root .build (ie. the one that imports all other .building)
	 * PB how to deal with composite buildings ...
	 */
	
	public static BuildingModel getBuildingModelFromGoalModel(ResourceSet resourceSet, IFile file, AtsyraGoalModel goalModel){
		
		Resource resource = goalModel.eResource();
		//AtsyraGoalModel goalModel = (AtsyraGoalModel) resource.getContents().get(0);
		
		
		
		LinkedHashSet<IFile> goalModelIFiles = getAllGoalModelIFiles(goalModel);
		IProject currentProject = file.getProject();
		
		Stream<IFile> buildingIFileStream = goalModelIFiles.stream().
			map(goalIFile -> {
				String candidateBaseName = goalIFile.getName().substring(0,goalIFile.getName().lastIndexOf(".atg"));
				return currentProject.getFile(candidateBaseName+".building");
			}).
			filter(candidateBuildingIFile -> candidateBuildingIFile.exists());
		
		// deal with multiple .building files
		List<IFile> buildingIFiles = buildingIFileStream.collect(Collectors.toCollection(ArrayList::new));
		Optional<IFile> f = buildingIFiles.stream().findFirst();
		
		
		URI buildingUri = null;
		if(f.isPresent()){
			buildingUri = URI.createPlatformResourceURI(f.get().getFullPath().toString(), true);
			Resource buildingRes = resourceSet.getResource(buildingUri, true);
			return (BuildingModel) buildingRes.getContents().get(0);
		} else {	
			// pb, no building file found for the goal !
			Activator.error("No corresponding .building file found for "+resource.getURI().toString());
			return null;
		}
	}

	public static BuildingModel getBuildingModelFromGoalModel(ResourceSet resourceSet, IFile goalFile,
			Resource goalModelEResource) {
		return getBuildingModelFromGoalModel(resourceSet, goalFile, (AtsyraGoalModel) goalModelEResource.getContents().get(0));
	}
	
	/**
	 * returns a set of all directly or indirectly imported IFiles for the given GoalModel
	 * will also return the IFile of the provided goalModel itself
	 * @param goalModel
	 * @return
	 */
	public static LinkedHashSet<IFile> getAllGoalModelIFiles(AtsyraGoalModel goalModel) {
		LinkedHashSet<IFile> result = new LinkedHashSet<IFile>();
		result.add( getIFileForEObject(goalModel) );
		getAllImportedIFiles(goalModel, result);
		return result;
	}
	
	/**
	 * returns a set of all directly or indirectly imported IFiles for the given GoalModel
	 * will also return the IFile of the provided goalModel itself
	 * @param goalModel
	 * @return
	 */
	public static LinkedHashSet<IFile> getAllModelIFiles(BuildingModel buildingModel) {
		LinkedHashSet<IFile> result = new LinkedHashSet<IFile>();
		result.add( getIFileForEObject(buildingModel) );
		getAllImportedIFiles(buildingModel, result);
		return result;
	}
	
	/**
	 * Complement the set with newly found imported IFile
	 * @param goalModel
	 * @param iFileSet
	 * @return
	 */
	public static LinkedHashSet<IFile> getAllImportedIFiles(AtsyraGoalModel goalModel, final LinkedHashSet<IFile> iFileSet) {
		ResourceSet resourceSet = goalModel.eResource().getResourceSet();
		goalModel.getImports().stream()
				.map(AtsyraHelper::getGoalImportTargetIFile)
				.forEach(ifile -> {	
					if(!iFileSet.contains(ifile)) {
						iFileSet.add(ifile);
						// recursion
						try {
							URI uri = URI.createPlatformResourceURI(ifile.getFullPath().toString(), true);
							Resource newGoalModelRes = resourceSet.getResource(uri, true);
							AtsyraGoalModel newlyFoundGoalModel = (AtsyraGoalModel) newGoalModelRes.getContents().get(0);
							getAllImportedIFiles(newlyFoundGoalModel, iFileSet);
						} catch (Exception e) {
							// in case of error, (bad uri, or whatever just ignore and do not recurse
						}
					}}
				);
		return iFileSet;
	}
	
	/**
	 * Complement the set with newly found imported IFile
	 * @param buildingModel
	 * @param iFileSet
	 * @return
	 */
	public static LinkedHashSet<IFile> getAllImportedIFiles(BuildingModel buildingModel, final LinkedHashSet<IFile> iFileSet) {
		ResourceSet resourceSet = buildingModel.eResource().getResourceSet();
		buildingModel.getImports().stream()
				.map(AtsyraHelper::getBuildingImportTargetIFile)
				.forEach(ifile -> {	
					if(!iFileSet.contains(ifile)) {
						iFileSet.add(ifile);
						// recursion
						try {
							URI uri = URI.createPlatformResourceURI(ifile.getFullPath().toString(), true);
							Resource newModelRes = resourceSet.getResource(uri, true);
							BuildingModel newlyFoundModel = (BuildingModel) newModelRes.getContents().get(0);
							getAllImportedIFiles(newlyFoundModel, iFileSet);
						} catch (Exception e) {
							// in case of error, (bad uri, or whatever just ignore and do not recurse
						}
					}}
				);
		return iFileSet;
	}
	
	/**
	 * Tries to find a "top importer" file, ie. the file(s) that import them all 
	 * In case that we have more than one result, it'll try to order the result on a file name convention
	 * If there is no "ImportThemAll importer" will return the original list (this might be improved)
	 * @param buildingIFiles
	 * @return
	 */
	public static ArrayList<IFile> getTopImporters(ArrayList<IFile> buildingIFiles){
		ArrayList<IFile> result;
		Injector inj = fr.irisa.atsyra.building.xtext.ui.internal.XtextActivator.getInstance()
				.getInjector(fr.irisa.atsyra.building.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_BUILDING_XTEXT_BUILDING);
		XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class); 
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		
		// search for files that import all the other ones
		ArrayList<IFile> importThemAll = buildingIFiles.stream().filter(buildingIFile -> {
			URI uri = URI.createPlatformResourceURI(buildingIFile.getFullPath().toString(), true);
			Resource eresource = resourceSet.getResource(uri, true);
			BuildingModel buildingModel = (BuildingModel) eresource.getContents().get(0);
			return getAllModelIFiles(buildingModel).containsAll(buildingIFiles);
		}).collect(Collectors.toCollection(ArrayList::new));
		if(!importThemAll.isEmpty()) {
			result = importThemAll;
			Collections.sort(importThemAll, new Comparator<IFile>() {
		        @Override
		        public int compare(IFile f1, IFile f2)
		        {
		            return  f1.getName().compareToIgnoreCase(f2.getName());
		        }
		    });
		} else {
			result = buildingIFiles; 
		}
		
		return result;
	}
	
	public static IFile getIFileForEObject(EObject o){
		String buildingfileString;
		if(o.eResource().getURI().isFile()){
			buildingfileString = o.eResource().getURI().toFileString();
		} else if (o.eResource().getURI().isPlatform()){
			buildingfileString = o.eResource().getURI().toPlatformString(true);
		} else throw new Error("The kind of URI of buildingModel.eResource().getURI() is not supported!");
		
		IFile buildingModelFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(buildingfileString));
		return buildingModelFile;
	}
	
	public static void waitForAutoBuild() {
		boolean wasInterrupted = false;
		do {
			try {
				Job.getJobManager().join(ResourcesPlugin.FAMILY_AUTO_BUILD,
						null);
				wasInterrupted = false;
			} catch (OperationCanceledException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				wasInterrupted = true;
			}
		} while (wasInterrupted);
	}


	public static boolean isResultUpToDate(Result res, List<IFile> fileList) {
		long resTimestamp =res.getTimestamp().getTime();
		boolean isuptodate = fileList.stream().allMatch(f -> {
				long lastModified = new File(f.getLocation().toString()).lastModified();
				return lastModified <= resTimestamp;
			});
		return isuptodate;
		
	}



	
}
