/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.wizards;

import org.eclipse.osgi.util.NLS;

/**
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "fr.irisa.atsyra.building.ide.ui.wizards.messages"; //$NON-NLS-1$
	public static String DEFAULT_PROJECT_NAME;
	public static String ERROR_PROJECT_ALREADY_EXISTS;
	public static String NEW_ATSYRA_BUILDING_PROJECT_WIZARD_DESCRIPTION;
	public static String NEW_ATSYRA_BUILDING_PROJECT_WIZARD_PAGE_DESCRIPTION;
	public static String NEW_ATSYRA_BUILDING_PROJECT_WIZARD_PAGE_TITLE;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
