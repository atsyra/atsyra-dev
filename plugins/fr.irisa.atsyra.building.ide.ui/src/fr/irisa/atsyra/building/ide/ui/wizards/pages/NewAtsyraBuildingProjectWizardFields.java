/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.wizards.pages;

import org.eclipse.core.resources.ResourcesPlugin;

import org.eclipse.gemoc.commons.eclipse.pde.wizards.pages.pde.ui.BaseProjectWizardFields;
import fr.irisa.atsyra.building.ide.ui.wizards.Messages;

public class NewAtsyraBuildingProjectWizardFields extends BaseProjectWizardFields {
	
	public NewAtsyraBuildingProjectWizardFields () {
		super();
		this.projectLocation 		= ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString();
		this.projectName 			= Messages.DEFAULT_PROJECT_NAME;

	}
}
