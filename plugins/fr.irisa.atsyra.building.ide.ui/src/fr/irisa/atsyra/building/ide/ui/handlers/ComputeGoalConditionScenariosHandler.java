/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;

import atsyragoal.AtsyraGoal;
import atsyragoal.GoalCondition;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.helpers.AtsyraHelper;
import fr.irisa.atsyra.building.ide.ui.helpers.ReachScenarioHelper;
import fr.irisa.atsyra.building.ide.ui.transfo.GalOutput2Scenario;
import fr.irisa.atsyra.atsyra2.gal.GalReach;
import fr.irisa.atsyra.gal.GalScenarios;
import fr.irisa.atsyra.resultstore.ConditionResult;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.WitnessResult;
import fr.irisa.atsyra.transfo.atg.gal.GoalConditionHelper;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ComputeGoalConditionScenariosHandler extends AbstractGoalConditionSelectHandler {

	
	void computeScenarios(IResource resource, GoalCondition condition) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {
			final IFile file = (IFile) resource;
			try {

				Job job = new Job("Condition Initialization scenarios from "+file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
						try {
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, subMonitor.split(1));
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute on "+ProcessConstants.GENFOLDER_NAME,e);
						}
						
						// do the job
						SubMonitor subMonitorGenerate = subMonitor.split(5);
						IFile galFile = getGalFile(file, condition);
						subMonitorGenerate.setTaskName("Generating "+galFile.getName());
						ReachScenarioHelper.generateGAL4GoalConditionIfNecessary(condition, galFile, monitor);
						subMonitorGenerate.done();
						IFile scenario_file = getScenarioFile(file, condition);
						URI resultStoreUri = getResultStoreURI(file);
						SubMonitor subMonitorReachable = subMonitor.split(5);
						AtsyraGoal goal = (AtsyraGoal) condition.eContainer();
						if(!getLastReachableResult(goal, condition, resultStoreUri).isPresent()) {							
							// need to compute reachability first
							computeIsReachable(goal, condition, galFile, subMonitorReachable, resultStoreUri);
						}
						// if reachable resut not uptodate
						if(!AtsyraHelper.isResultUpToDate(getLastReachableResult(goal, condition, resultStoreUri).get(),Arrays.asList(galFile))) {	
							// need to recompute reachability first
							computeIsReachable(goal, condition, galFile, subMonitorReachable, resultStoreUri);
						}
						
						// if reachable then compute witness otherwise ignore
						if(!getLastReachableResult(goal, condition, resultStoreUri).get().getValue().equals(ResultValue.TRUE)) {
							Activator.important(GoalConditionHelper.getLabel(condition)+"  is not reachable. Will not try to search witness");
							return Status.OK_STATUS;
						}
						
						WitnessResult witnessResultHandler = getNewShortestWitnessResultHandler(goal, condition, resultStoreUri);
						GalScenarios gscen = new GalScenarios(galFile){
							public void processGALOutpout(String stdOutString, boolean timeoutTrigerred){
								/*if(stdOutString.length() > 2048) {
									Activator.debug(stdOutString.substring(0, 2048)+"\n[...]Truncated long gal traces on console[...]");
								} else	Activator.debug(stdOutString);*/
								GalOutput2Scenario.buildScenarioFileFromReachabilityTrace(stdOutString, scenario_file, gal_file.getName().replace(".gal", ""), goal.getName());
								witnessResultHandler.getWitnessFound().clear();
								GalOutput2Scenario.getScenarioLines(stdOutString).stream().forEach(s -> witnessResultHandler.getWitnessFound().add(s));
								if(stdOutString.contains("Reachability property goal_reached==1 is true.")){
									Activator.important("Found "+witnessResultHandler.getWitnessFound().size()+" initialization scenario(s) "+GoalConditionHelper.getLabel(condition));
									// TODO add/manage marker
									witnessResultHandler.setValue(ResultValue.TRUE);
								}
								else{
									if(timeoutTrigerred){
										Activator.important("Timeout when searching witness of "+GoalConditionHelper.getLabel(condition));
										witnessResultHandler.setValue(ResultValue.TIMEOUT);
									} else {
										Activator.important("Condition "+GoalConditionHelper.getLabel(condition)+"  is not reachable.");
										witnessResultHandler.setValue(ResultValue.FALSE);
									}
								}

								witnessResultHandler.setLog(stdOutString);
							}	
						};

						SubMonitor subMonitorSearchingWitness = subMonitor.split(92);
						gscen.computeScenarios(200, 200, null, subMonitorSearchingWitness);
						witnessResultHandler.setDuration(gscen.executionDuration);
						subMonitorSearchingWitness.done();
						try {
							witnessResultHandler.eResource().save(null);
						} catch (IOException e) {
							Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
						}
						return Status.OK_STATUS;
					};
				};
				job.setPriority(Job.LONG);
				// use mutex to prevent concurrent job
				IResourceRuleFactory ruleFactory = ResourcesPlugin.getWorkspace().getRuleFactory();
				ISchedulingRule rule = MultiRule.combine(
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)),
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME)));
				job.setRule(rule);
				job.schedule();
									
			} catch (Exception e) {
				Activator.eclipseError(
						"Reachability from "+file.getName()	+" raised an exception "
								+ e.getMessage(), e);
			}
		} 
	}	
	
	public IFile getGalFile(IFile file, GoalCondition condition){
		//look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		IFile galFile = file.getProject().getFile(genFolder.getProjectRelativePath() + 
				"/" + fileNameNoExtension + "_" + GoalConditionHelper.getLabel(condition)+"_reach.gal");
		return galFile;
	}
	
	public IFile getScenarioFile(IFile file, GoalCondition condition){
		//look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		IFile galFile = file.getProject().getFile(genFolder.getProjectRelativePath() + 
				"/" + fileNameNoExtension + "_" + GoalConditionHelper.getLabel(condition)+"_reach.scenario");
		return galFile;
	}
	
	/** Search for the IFile of the goal either via the event or directly from the eResource */
	protected IFile getGoalConditionIFile(ExecutionEvent event, GoalCondition goalCondition) {
		IFile atsyraGoalFile = getGoalConditionFileFromSelection(event);
		if (atsyraGoalFile == null) {
			// this means that we have to retrieve the IFile from the goal instance (either because 
			// it comes from an editor of because we have selected one goal among other in the project)
			atsyraGoalFile = EMFResource.getIFile(goalCondition);
		}
		return atsyraGoalFile;
	}

	@Override
	public Object executeForSelectedGoalCondition(ExecutionEvent event, IProject updatedAtsyraGoalProject,
			GoalCondition goalCondition) throws ExecutionException {
		IFile atsyraGoalFile = getGoalConditionIFile(event, goalCondition);
		computeScenarios(atsyraGoalFile, goalCondition);
		return null;
	}

	@Override
	public String getSelectionMessage() {
		return "Compute valid initialization scenarios for the selected goal condition.";
	}
	
	public WitnessResult getNewShortestWitnessResultHandler(AtsyraGoal goal, GoalCondition condition,  URI resultStoreUri){
		
		Resource resultStoreRes = condition.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<GoalResult> grOp = resultStore.getGoalResults().stream().filter(gr -> gr.getGoal().getName().equals(goal.getName())).findFirst();
		GoalResult gr;
		if(grOp.isPresent()){
			gr = grOp.get();
		} else {
			gr = ResultstoreFactory.eINSTANCE.createGoalResult();
			gr.setGoal(goal);
			resultStore.getGoalResults().add(gr);
		}
		ConditionResult cr = null;
		if(condition.eContainingFeature().getName().equals("precondition")) {
			if(gr.getPrecondition() != null) {
				cr = gr.getPrecondition();
			} else {
				cr = ResultstoreFactory.eINSTANCE.createConditionResult();
				cr.setGoalcondition(condition);
				gr.setPrecondition(cr);
			}
		} else if(condition.eContainingFeature().getName().equals("postcondition")) {
			if(gr.getPostcondition() != null) {
				cr = gr.getPostcondition();
			} else {
				cr = ResultstoreFactory.eINSTANCE.createConditionResult();
				cr.setGoalcondition(condition);
				gr.setPostcondition(cr);
			}
		}
		
		WitnessResult result = ResultstoreFactory.eINSTANCE.createWitnessResult();
		result.setTimestamp(new java.util.Date());
		cr.setScenariosResult(result);
		result.setName("Sceanario of "+GoalConditionHelper.getLabel(condition));
		return result;
	}
	
	public Optional<Result> getLastReachableResult(AtsyraGoal goal, GoalCondition condition,  URI resultStoreUri){
		Resource resultStoreRes = condition.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			return Optional.empty();
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<GoalResult> grOp = resultStore.getGoalResults().stream().filter(gr -> gr.getGoal().getName().equals(goal.getName())).findFirst();
		if(grOp.isPresent()){
			if(condition.eContainingFeature().getName().equals("precondition")) {
				if(grOp.get().getPrecondition() != null) {
					return Optional.ofNullable(grOp.get().getPrecondition().getReachabilityResult());
				}
			}
			if(condition.eContainingFeature().getName().equals("postcondition")) {
				if(grOp.get().getPostcondition() != null) {
					return Optional.ofNullable(grOp.get().getPostcondition().getReachabilityResult());
				}
			}
		} 
		return Optional.empty();
	}
	public Result getNewReachableResultHandler(AtsyraGoal goal, GoalCondition condition,  URI resultStoreUri){
		Resource resultStoreRes = condition.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<GoalResult> grOp = resultStore.getGoalResults().stream().filter(gr -> gr.getGoal().getName().equals(goal.getName())).findFirst();
		GoalResult gr;
		if(grOp.isPresent()){
			gr = grOp.get();
		} else {
			gr = ResultstoreFactory.eINSTANCE.createGoalResult();
			gr.setGoal(goal);
			resultStore.getGoalResults().add(gr);
		}
		ConditionResult cr = null;
		if(condition.eContainingFeature().getName().equals("precondition")) {
			if(gr.getPrecondition() != null) {
				cr = gr.getPrecondition();
			} else {
				cr = ResultstoreFactory.eINSTANCE.createConditionResult();
				cr.setGoalcondition(condition);
				gr.setPrecondition(cr);
			}
		} else if(condition.eContainingFeature().getName().equals("postcondition")) {
			if(gr.getPostcondition() != null) {
				cr = gr.getPostcondition();
			} else {
				cr = ResultstoreFactory.eINSTANCE.createConditionResult();
				cr.setGoalcondition(condition);
				gr.setPostcondition(cr);
			}
		}
		Result result = ResultstoreFactory.eINSTANCE.createResult();
		result.setTimestamp(new java.util.Date());
		cr.setReachabilityResult(result);
		result.setName("Reachability of : " + GoalConditionHelper.getLabel(condition));
		return result;
	}
	
	protected void computeIsReachable(AtsyraGoal goal, GoalCondition condition, IFile galFile, IProgressMonitor subMonitorReachable,  URI resultStoreUri) {
		Result resultHanlder = getNewReachableResultHandler(goal, condition, resultStoreUri);
		GalReach grch = new GalReach(galFile, resultHanlder);
		grch.computeReachability(subMonitorReachable);
		try {
			resultHanlder.eResource().save(null);
		} catch (IOException e) {
			Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
		}
	}

}
