/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.gemoc.commons.eclipse.core.resources.FileFinderVisitor;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.outline.impl.EObjectNode;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import atsyragoal.AtsyraTree;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.dialogs.SelectAnyAtsyraTreeDialog;
import fr.irisa.atsyra.resultstore.GoalResult;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.TreeResult;

abstract public class AbstractAtsyraTreeSelectHandler extends AbstractHandler {

	public abstract Object executeForSelectedAtsyraTree(ExecutionEvent event, IProject updatedGemocAtsyraTreeProject,
			AtsyraTree tree) throws ExecutionException;

	public abstract String getSelectionMessage();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// get the optional selection and eventually project data to preset the
		// wizard
		final IProject updatedGemocAtsyraTreeProject = getUpdatedAtsyraTreeProjectFromSelection(event);

		AtsyraTree tree = getAtsyraTreeFromSelection(event);
		if(tree != null && updatedGemocAtsyraTreeProject != null){
			// go for it, we have everything in the selection
			executeForSelectedAtsyraTree(event, updatedGemocAtsyraTreeProject, tree);
			return null;
		}
		
		final IFile atsyraGoalFile = getAtsyraTreeFileFromSelection(event);
		final ResourceSet rs = new ResourceSetImpl();
		if (atsyraGoalFile != null) {
			// we have only one AtsyraTree file in the selection
			final URI uri = URI.createPlatformResourceURI(atsyraGoalFile.getFullPath().toString(), true);
			rs.getResource(uri, true);
		} else {
			// we will search for all .atg files in the project
			FileFinderVisitor atsyraProjectVisitor = new FileFinderVisitor("atg");
			try {
				updatedGemocAtsyraTreeProject.accept(atsyraProjectVisitor);
				for (IFile projectAtsyraTreeIFile : atsyraProjectVisitor.getFiles()) {
					// consider all melange files in the project, get them in
					// the ResourceSet
					if (!(projectAtsyraTreeIFile.getFullPath().toString().contains("/bin/") | projectAtsyraTreeIFile
							.getFullPath().toString().contains("/target/"))) {
						// FIXME stupid check to remove some typical duplicates,
						// a better impl should look into java output

						final URI uri = URI.createPlatformResourceURI(projectAtsyraTreeIFile.getFullPath().toString(),
								true);
						rs.getResource(uri, true);
					}
				}
			} catch (CoreException e) {
				Activator.eclipseError(e.getMessage(), e);
			}
		}

		final LabelProvider labelProvider = new LabelProvider() {
			public String getText(Object element) {
				if (element instanceof AtsyraTree) {
					return ((AtsyraTree) element).getName();
				} else
					return super.getText(element);
			}
		};
		final SelectAnyAtsyraTreeDialog dialog = getNewSelectAnyAtsyraTreeDialog(rs, labelProvider);
		dialog.setMessage(getSelectionMessage());
		final int res = dialog.open();
		if (res == WizardDialog.OK) {
			tree = (AtsyraTree) dialog.getFirstResult();
			executeForSelectedAtsyraTree(event, updatedGemocAtsyraTreeProject, tree);
		}

		return null;
	}
	
	/**
	 * allows to override the Dialog instance (for example to restrict to a subset of the trees
	 * @param rs
	 * @param labelProvider
	 * @return
	 */
	protected SelectAnyAtsyraTreeDialog getNewSelectAnyAtsyraTreeDialog(ResourceSet rs, ILabelProvider labelProvider){
		final SelectAnyAtsyraTreeDialog dialog = new SelectAnyAtsyraTreeDialog(rs, labelProvider);
		dialog.setTitle("Select a Tree");
		return dialog;
	}
	
	protected IFile getAtsyraTreeFileFromSelection(ExecutionEvent event){
		IFile selectedAtsyraTreeIFile = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();

				if (element instanceof IFile) {
					selectedAtsyraTreeIFile = (IFile) element;

				}
				if (element instanceof IAdaptable) {
					IFile res = (IFile) ((IAdaptable) element)
							.getAdapter(IFile.class);
					if (res != null) {
						selectedAtsyraTreeIFile = res;
					}
				}
			}
		}
		return selectedAtsyraTreeIFile;
	}
		
	protected AtsyraTree getAtsyraTreeFromSelection(ExecutionEvent event){
		AtsyraTree selectedAtsyraTree = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();

				if (element instanceof AtsyraTree) {
					selectedAtsyraTree = (AtsyraTree) element;
				}
				if (element instanceof TreeResult) {
					selectedAtsyraTree = ((TreeResult) element).getTree();
				}
				if (element instanceof EObjectNode) {
					// try selection from xtexteditor outline
					final XtextEditor editor = org.eclipse.xtext.ui.editor.utils.EditorUtils.getActiveXtextEditor(event);					
					final IUnitOfWork<AtsyraTree, XtextResource> _function = (XtextResource it) -> {
						if(((EObjectNode) element).getEObject(it) instanceof AtsyraTree){
							return (AtsyraTree) ((EObjectNode) element).getEObject(it);
						} else {
							return null;
						}
					};
					final AtsyraTree goal = editor.getDocument().readOnly(_function);
					if(goal != null){
						return goal;
					}
				}
				if (element instanceof IAdaptable) {
					AtsyraTree res = (AtsyraTree) ((IAdaptable) element).getAdapter(AtsyraTree.class);
					if (res != null) {
						selectedAtsyraTree = res;
					}
				}
			}
		}
		else {
			// try selection from xtexteditor
			final XtextEditor editor = org.eclipse.xtext.ui.editor.utils.EditorUtils.getActiveXtextEditor(event);
			if (editor != null) {
				final ITextSelection textSelection = (ITextSelection)editor.getSelectionProvider().getSelection();
				final IUnitOfWork<AtsyraTree, XtextResource> _function = (XtextResource it) -> {
			        int _offset = textSelection.getOffset();
			        return this.getSelectedAtsyraTree(it, _offset);
				};
				
				final AtsyraTree lang = editor.getDocument().readOnly(_function);
				if(lang != null){
					return lang;
				}
			}
		}
		return selectedAtsyraTree;
	}
	protected AtsyraTree getSelectedAtsyraTree(XtextResource resource, int offset){
		final EObjectAtOffsetHelper eObjectAtOffsetHelper =
			resource.getResourceServiceProvider().get(EObjectAtOffsetHelper.class);
		EObject selectedElement = eObjectAtOffsetHelper.resolveContainedElementAt(resource, offset);
		if (selectedElement != null) {
			EObject currentElem = selectedElement;
			while(currentElem != null){
				if(currentElem instanceof AtsyraTree){
					return (AtsyraTree)currentElem;
				}
				currentElem = currentElem.eContainer();
			}
		}
		return null;
	}
	protected IProject getUpdatedAtsyraTreeProjectFromSelection(ExecutionEvent event) {
		IProject updatedGemocLanguageProject = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();

				if (element instanceof IResource) {
					updatedGemocLanguageProject = ((IResource) element)
							.getProject();

				}
				if (element instanceof AtsyraTree ){
					IFile goalFile = EMFResource.getIFile((AtsyraTree) element);
					if(goalFile != null){
						updatedGemocLanguageProject  = goalFile.getProject();
					}
				}
				if (element instanceof GoalResult ){
					IFile goalFile = EMFResource.getIFile(((GoalResult)element).getGoal());
					if(goalFile != null){
						updatedGemocLanguageProject  = goalFile.getProject();
					}
				}
				if (element instanceof EObjectNode) {
					// this is an outline element of an xtext editor
					final XtextEditor editor = org.eclipse.xtext.ui.editor.utils.EditorUtils.getActiveXtextEditor(event);
					updatedGemocLanguageProject = editor.getResource().getProject();
				}
				if (element instanceof IAdaptable) {
					IResource res = (IResource) ((IAdaptable) element)
							.getAdapter(IResource.class);
					if (res != null) {
						updatedGemocLanguageProject = res.getProject();
					} 
				}
			}
		}
		else if (selection != null & selection instanceof ITextSelection) {
			IResource res = (IResource) HandlerUtil
					.getActiveWorkbenchWindow(event).getActivePage()
					.getActiveEditor().getEditorInput()
					.getAdapter(IResource.class);
			if (res != null) {
				updatedGemocLanguageProject = res.getProject();
			}
		}
		return updatedGemocLanguageProject;
	}
	
	/**
	 * look for a resultstore file with the same name, in genFolder
	 * @param URI
	 * @return
	 */
	public URI getResultStoreURI(IFile file){
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME);
		IFile resultStoreFile = file.getProject().getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension +".resultstore");
		
		URI resultStoreUri = URI.createPlatformResourceURI(resultStoreFile.getFullPath().toString(), true);
		if(!resultStoreFile.exists()){
			// make sure the file exist, 
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
	        Map<String, Object> m = reg.getExtensionToFactoryMap();
	        m.put("resultstore", new XMIResourceFactoryImpl());
			ResourceSet resSet = new ResourceSetImpl();
			Resource res = resSet.createResource(resultStoreUri);
			try {
				res.save(null);
			} catch (IOException e) {
				Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
			}
		}
		return resultStoreUri;
	}
	
public TreeResult getTreeResultHandler(AtsyraTree tree,  URI resultStoreUri){
		
		Resource resultStoreRes = tree.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<TreeResult> trOp = resultStore.getTreeResults().stream().filter(gr -> gr.getTree().getName().equals(tree.getName())).findFirst();
		TreeResult tr;
		if(trOp.isPresent()){
			tr = trOp.get();
		} else {
			tr = ResultstoreFactory.eINSTANCE.createTreeResult();
			tr.setTree(tree);
			resultStore.getTreeResults().add(tr);
		}
		return tr;
	}
	/**
	 * Create a new Result with the given name
	 * Retreive (or create) the TreeResult for the given tree,  
	 * the function is typically used to set the new Result in the TreeResult using the appropriate setter
	 * (ex call : getNewResultHandler(tree, uri, (tr, r) -> { tr.setMeetResult(r);}, "Meet result for "+tree.getName());
	 * @param tree
	 * @param resultStoreUri
	 * @param func
	 * @param name
	 * @return
	 */
	public Result getNewResultHandler(AtsyraTree tree,  URI resultStoreUri, BiConsumer <TreeResult, Result> func, String name){
		
		Resource resultStoreRes = tree.eResource().getResourceSet().getResource(resultStoreUri, true);
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if(resultStoreRes.getContents().isEmpty()){
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
		}
		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
		Optional<TreeResult> trOp = resultStore.getTreeResults().stream().filter(tr -> tr.getTree().getName().equals(tree.getName())).findFirst();
		TreeResult tr;
		if(trOp.isPresent()){
			tr = trOp.get();
		} else {
			tr = ResultstoreFactory.eINSTANCE.createTreeResult();
			tr.setTree(tree);
			resultStore.getTreeResults().add(tr);
			// prepopulate TreeResult with dummy result in state "NOT Run"
			tr.setAdmissibilityResult(ResultstoreFactory.eINSTANCE.createResult());
			tr.getAdmissibilityResult().setName("Admissibility");
			tr.setMatchResult(ResultstoreFactory.eINSTANCE.createResult());
			tr.getMatchResult().setName("Match");
			tr.setMeetResult(ResultstoreFactory.eINSTANCE.createResult());
			tr.getMeetResult().setName("Meet");
			tr.setOvermatchResult(ResultstoreFactory.eINSTANCE.createResult());
			tr.getOvermatchResult().setName("Overmatch");
			tr.setUndermatchResult(ResultstoreFactory.eINSTANCE.createResult());
			tr.getUndermatchResult().setName("Undermatch");
		}
		Result result = ResultstoreFactory.eINSTANCE.createResult();
		result.setTimestamp(new java.util.Date());
		
		func.accept(tr, result);
		
		result.setName(name);
		
		return result;
	}

}
