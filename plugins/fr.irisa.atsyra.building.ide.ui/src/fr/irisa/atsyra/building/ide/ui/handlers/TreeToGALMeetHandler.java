/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import atsyragoal.AtsyraGoalModel;
import atsyragoal.AtsyraTree;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.dialogs.SelectAnyAtsyraTreeDialog;
import fr.irisa.atsyra.building.ide.ui.dialogs.SelectAnyAtsyraTreeLeafDialog;
import fr.irisa.atsyra.building.ide.ui.helpers.AtsyraHelper;
import fr.irisa.atsyra.atsyra2.gal.GalCTL;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultValue;
import fr.irisa.atsyra.transfo.atg.ctl.AbstractCTLAtsyraTreeAspects;
//import fr.irisa.atsyra.transfo.atg.ctl.AbstractAtsyraTreeAspects;
import fr.irisa.atsyra.transfo.building.gal.Building2GALForCTL;
import fr.lip6.move.gal.GALTypeDeclaration;

public class TreeToGALMeetHandler extends AbstractAtsyraTreeSelectHandler {

	@Override
	public Object executeForSelectedAtsyraTree(ExecutionEvent event, IProject updatedGemocAtsyraTreeProject,
			AtsyraTree tree) throws ExecutionException {
		IFile atsyraTreeFile = getAtsyraTreeIFile(event, tree);
		computeCTLmc(atsyraTreeFile, tree);
		return null;
	}	

	void computeCTLmc(IResource resource, AtsyraTree tree) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {
			final IFile file = (IFile) resource;
			try {
				Job job = new Job("CTL model-checking of tree "+ tree.getName()+" from " + file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						try {
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, monitor);
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute on " + ProcessConstants.GENFOLDER_NAME,
									e);
						}

						// do the job
						Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
								.getInjector(
										fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
						XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class);
						resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
						// URI uri =
						// URI.createFileURI(file.getRawLocationURI().getPath())
						// ;
						URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
						Resource eresource = resourceSet.getResource(uri, true);
						AtsyraGoalModel goalModel = (AtsyraGoalModel) eresource.getContents().get(0);
						// First create the gal file
						IFile gal_file = getGalFile(file);
						Building2GALForCTL btg = new Building2GALForCTL();
						BuildingModel buildingModel = AtsyraHelper.getBuildingModelFromGoalModel(resourceSet, file, eresource);
						GALTypeDeclaration gal = btg.transformToGAL(buildingModel);
						btg.flattenGAL(gal);
						btg.writeToFile(gal, gal_file, monitor);
						// Second run CTL model checking
						URI resultStoreUri = getResultStoreURI(file);
						Result resultHandler = getNewResultHandler(tree, resultStoreUri, (tr, r) -> { tr.setMeetResult(r);}, "Meet");
						try {
							IFile meet_formula_file = getFormulaFile(goalModel, tree, file, monitor);
							GalCTL ctlmc = new GalCTL(gal_file, meet_formula_file, resultHandler){
								public ResultValue processGALOutpout(String stdOutString, boolean timeoutTrigerred){
									//Activator.info(stdOutString);
									resultHandler.setLog(stdOutString);
									if(stdOutString.contains("Formula is TRUE")){
										Activator.important(resultHandler.getName()+" is true!");
										resultHandler.setValue(ResultValue.TRUE);
										return ResultValue.TRUE;
									}
									else{
										if(timeoutTrigerred){
											Activator.info("Unable to compute CTL property : timeout.");
											resultHandler.setValue(ResultValue.TIMEOUT);
											return ResultValue.TIMEOUT;
										} else {
											Activator.important(resultHandler.getName()+" is false.");
											resultHandler.setValue(ResultValue.FALSE);
											return ResultValue.FALSE;
										}
									}
								}
							};
							ctlmc.computeCTL(monitor);
							resultHandler.setDuration(ctlmc.executionDuration);
							try {
								resultHandler.eResource().save(null);
							} catch (IOException e) {
								Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
							}
							return Status.OK_STATUS;
						} catch (CoreException e) {
							Activator.eclipseError(e.getMessage(), e);
							return new Status(Status.ERROR, Activator.PLUGIN_ID,
									"Error checking the Meet Property : Unable to write in the .ctl file");
						}

					};
				};
				job.setPriority(Job.LONG);
				// use mutex to prevent concurrent job
				IResourceRuleFactory ruleFactory = ResourcesPlugin.getWorkspace().getRuleFactory();
				ISchedulingRule rule = MultiRule.combine(
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)),
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME)));
				job.setRule(rule);
				job.schedule();

			} catch (Exception e) {
				Activator.eclipseError(
						"CTM model-checking from " + file.getName() + " raised an exception " + e.getMessage(), e);
			}

		}
	}

	/**
	 * Search for the IFile of the goal either via the event or directly from
	 * the eResource
	 */
	protected IFile getAtsyraTreeIFile(ExecutionEvent event, AtsyraTree tree) {
		IFile atsyraTreeFile = getAtsyraTreeFileFromSelection(event);
		if (atsyraTreeFile == null) {
			// this means that we have to retrieve the IFile from the tree
			// instance (either because
			// it comes from an editor of because we have selected one tree
			// among other in the project)
			atsyraTreeFile = EMFResource.getIFile(tree);
		}
		return atsyraTreeFile;
	}

	IFile getGalFile(IFile file) {
		// look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		IFile galFile = file.getProject()
				.getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension + "_correctness.gal");
		return galFile;
	}

	IFile getFormulaFile(AtsyraGoalModel goalModel, AtsyraTree tree, IFile file, IProgressMonitor monitor) throws CoreException {
		if (goalModel.getTrees().isEmpty())
			throw new RuntimeException("No tree to parse");

		// We create the string of the CTL formula
		String formula = AbstractCTLAtsyraTreeAspects.getMeetFormula(tree);
		// We create the file and put set its content to the formula
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFile formulaFile = genFolder.getFile(fileNameNoExtension+"_"+tree.getName()+"_meet_formula.ctl");
		InputStream stream = new ByteArrayInputStream(formula.getBytes(StandardCharsets.UTF_8));
		if (!formulaFile.exists()) {
			formulaFile.create(stream, true, monitor);
		} else {
			formulaFile.setContents(stream, true, false, monitor);
		}
		return formulaFile;

	}

	

	@Override
	protected SelectAnyAtsyraTreeDialog getNewSelectAnyAtsyraTreeDialog(ResourceSet rs, ILabelProvider labelProvider){
		final SelectAnyAtsyraTreeDialog dialog = new SelectAnyAtsyraTreeLeafDialog(rs, labelProvider);
		dialog.setTitle("Select a Tree (non leaf)");
		return dialog;
	} 
	
	@Override
	public String getSelectionMessage() {

		return "Generate GAL and check Meet property for the selected tree.";
	}

}
