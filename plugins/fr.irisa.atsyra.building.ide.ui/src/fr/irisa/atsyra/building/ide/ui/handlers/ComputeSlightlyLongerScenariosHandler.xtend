package fr.irisa.atsyra.building.ide.ui.handlers

import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.resources.IProject
import atsyragoal.AtsyraGoal
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.resources.IFile
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource
import org.eclipse.core.resources.IResource
import org.eclipse.core.runtime.jobs.Job
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.IStatus
import org.eclipse.core.runtime.SubMonitor
import org.eclipse.core.runtime.MultiStatus
import fr.lip6.move.gal.Specification
import fr.lip6.move.serialization.SerializationUtil
import java.util.HashMap
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.common.util.TreeIterator
import fr.lip6.move.gal.Variable
import fr.irisa.atsyra.gal.GalExpressionInterpreter
import com.google.inject.Injector
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.core.runtime.Status
import fr.irisa.atsyra.witness.witness.WitnessModel
import java.util.Set
import org.eclipse.core.resources.IResourceRuleFactory
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.jobs.MultiRule
import org.eclipse.core.runtime.jobs.ISchedulingRule
import fr.irisa.atsyra.building.ide.ui.ProcessConstants
import fr.irisa.atsyra.building.ide.ui.Activator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.common.util.URI
import org.eclipse.core.runtime.CoreException
import fr.irisa.atsyra.building.ide.ui.helpers.ReachScenarioHelper
import org.eclipse.core.resources.IFolder
import java.util.HashSet
import fr.irisa.atsyra.witness.witness.Witness
import org.eclipse.emf.common.util.EList
import fr.irisa.atsyra.witness.witness.AbstractStepStates
import fr.irisa.atsyra.witness.witness.StepState
import java.util.Optional
import fr.irisa.atsyra.witness.witness.VarState
import fr.irisa.atsyra.resultstore.ResultValue
import fr.irisa.atsyra.building.ide.ui.transfo.GalOutput2Scenario
import fr.irisa.atsyra.resultstore.WitnessResult
import fr.irisa.atsyra.gal.GalScenarios
import org.eclipse.jface.preference.IPreferenceStore
import java.io.IOException
import fr.irisa.atsyra.resultstore.Result
import fr.irisa.atsyra.resultstore.ResultStore
import fr.irisa.atsyra.resultstore.GoalResult
import fr.irisa.atsyra.resultstore.ResultstoreFactory

class ComputeSlightlyLongerScenariosHandler extends AbstractAtsyraGoalSelectHandler  {
	
	override executeForSelectedAtsyraGoal(ExecutionEvent event, IProject updatedGemocAtsyraGoalProject, AtsyraGoal goal) throws ExecutionException {
		var IFile atsyraGoalFile=getAtsyraGoalIFile(event, goal) 
		computeScenarios(atsyraGoalFile, goal) 
		return null 

	}
	
	def computeScenarios(IResource resource, AtsyraGoal goal) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {
			val IFile file=(resource as IFile) 
			try {
				var Job job=new Job('''Find some slighly longer witness scenarios from «file.getName()»'''){
					override protected IStatus run(IProgressMonitor monitor) {
						var SubMonitor subMonitor=SubMonitor.convert(monitor, 100) 
						try {
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, subMonitor.split(1)) 
						} catch (CoreException e) {
							Activator.eclipseWarn('''Cannot set derive attribute on «ProcessConstants.GENFOLDER_NAME»''', e) 
						}
						
						// do the job
						var SubMonitor subMonitorGenerate=subMonitor.split(5) 
						var IFile galFile=getGalFile(file, goal) 
						subMonitorGenerate.setTaskName('''Generating «galFile.getName()»''') 
						ReachScenarioHelper.generateGAL4GoalIfNecessary(goal, galFile, monitor) 
						subMonitorGenerate.done() 
						var IFile scenario_file=getWitnessScenarioFile(file, goal) 
						var URI resultStoreUri=getResultStoreURI(file) 
						// check with an invariant on a single variable
						var MultiStatus mStatus 
						if (!scenario_file.exists() || scenario_file.getLocalTimeStamp() < galFile.getLocalTimeStamp()) {
							// check current witness file, in none or outdated, rerun basic search
							var IStatus shortessWitnessStatus=searchWitnesses(goal, resultStoreUri, galFile, scenario_file, null, subMonitor) 
							mStatus=new MultiStatus(Activator.PLUGIN_ID,shortessWitnessStatus.getCode(),#[shortessWitnessStatus],shortessWitnessStatus.getMessage(),null) 
						}
						var Specification galSpec=SerializationUtil.fileToGalSystem(galFile.getRawLocation().toString()) 
						// list all variables of the gal
						var HashMap<String, Integer> declaredVariables=new HashMap<String, Integer>() 
						var TreeIterator<EObject> it1=galSpec.eAllContents() 
						while (it1.hasNext()) {
							var EObject o=it1.next() 
							if (o instanceof Variable) {
								// warning, supposes that var are initialized with constants only
								// for more complex expression we need a better gal evaluation as the one in the gal interpreter
								var int varValue=GalExpressionInterpreter.intEvaluate(((o as Variable)).getValue()) 
								declaredVariables.put(((o as Variable)).getName(), varValue) 
								System.out.println('''«((o as Variable)).getName()» «varValue»''') 
							}
						}
						var Injector inj=fr.irisa.atsyra.witness.xtext.ui.internal.XtextActivator.getInstance().getInjector(fr.irisa.atsyra.witness.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_WITNESS_WITNESS) 
						//Injector inj = XtextActivator.getInstance().getInjector(XtextActivator.FR_IRISA_ATSYRA_BUILDING_XTEXT_BUILDING);
						var XtextResourceSet resourceSet=inj.getInstance(XtextResourceSet) 
						resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE) 
						//URI uri = URI.createFileURI(file.getRawLocationURI().getPath()) ;
						var URI uri=URI.createPlatformResourceURI(scenario_file.getFullPath().toString(), true) 
						var Resource eresource=resourceSet.getResource(uri, true) 
						// if there is no witnesses, do nothing
						if (eresource.getContents().isEmpty()) return Status.OK_STATUS 
						var WitnessModel witnessModel=(eresource.getContents().get(0) as WitnessModel) 
						for (String declaredVariableName : declaredVariables.keySet()) {
							// find possible final value
							var Set<Integer> finalReachedValues=getFinalReachedValuesInWitnesses(declaredVariableName, witnessModel) 
							// remove the initial value and see if there are some remaining values for this variable
							finalReachedValues.remove(declaredVariables.get(declaredVariableName)) 
							if (!finalReachedValues.isEmpty()) {
								// create a reachability search with an invariant preventing these values for the variable
								val String invariant = '''«FOR v : finalReachedValues SEPARATOR ' && '»«declaredVariableName»!=«v»«ENDFOR»'''
								System.out.println(''' will try with invariant «invariant»''') //finalReachedValues.stream().
								searchWitnesses(goal, resultStoreUri, galFile, scenario_file, invariant, subMonitor)		
							}
						}
						
						return Status.OK_STATUS 
					}} 
				job.setPriority(Job.LONG) 
				// use mutex to prevent concurrent job
				var IResourceRuleFactory ruleFactory=ResourcesPlugin.getWorkspace().getRuleFactory() 
				var ISchedulingRule rule=MultiRule.combine(ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)), ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME))) 
				job.setRule(rule) 
				job.schedule() 
			} catch (Exception e) {
				Activator.eclipseError('''Reachability from «file.getName()» raised an exception «e.getMessage()»''', e) 
			}
			
		}

	}
	
	/**
	 * retrieve in the witnesses the final values reached by a variables
	 * several values may be reached
	 * @param declaredVariableName
	 * @param witnessModel
	 * @return
	 */
	def protected Set<Integer> getFinalReachedValuesInWitnesses(String declaredVariableName, WitnessModel witnessModel) {
		var Set<Integer> result=new HashSet<Integer>() 
		for (Witness witness : witnessModel.getWitnesses()) {
			var EList<AbstractStepStates> stepStates=witness.getStepStates() 
			if (stepStates.size() > 1) {
				// last StepStates is reach_goal, ignore it
				var AbstractStepStates lastStepStates=witness.getStepStates().get(witness.getStepStates().size() - 2) 
				// in a state, a missing VarState val indicates a value of 0
				for (StepState sState : lastStepStates.getStepState()) {
					var vs1 = sState.getVarState().findFirst[vs2 | vs2.name==declaredVariableName]
					if(vs1!==null){
						result.add(vs1.value)
					} else {
						result.add(0)
					}
				}
			}
		}
		return result 

	}
	
	def Optional<Result> getLastReachableResult(AtsyraGoal goal,  URI resultStoreUri){
		var Resource resultStoreRes=goal.eResource().getResourceSet().getResource(resultStoreUri, true) 
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if (resultStoreRes.getContents().isEmpty()) {
			return Optional.empty() 
		}
		var ResultStore resultStore=(resultStoreRes.getContents().get(0) as ResultStore) 
		var Optional<GoalResult> grOp=resultStore.getGoalResults().stream().filter([gr | gr.getGoal().getName().equals(goal.getName())]).findFirst() 
		if (grOp.isPresent()) {
			return Optional.ofNullable(grOp.get().getReachabilityResult()) 
		} else {
			return Optional.empty() 
		}

	}
	
	/**
	 * if invariant is null or empty this is a shortest witness search 
	 * otherwise this is a complementary witness search
	 */
	protected def IStatus searchWitnesses(AtsyraGoal goal, 
			URI resultStoreUri, 
			IFile galFile, 
			IFile scenario_file, 
			String invariant,
			IProgressMonitor monitor) {
			var SubMonitor subMonitor=SubMonitor.convert(monitor, 100) 
		// if reachable then compute witness otherwise ignore
		if (!getLastReachableResult(goal, resultStoreUri).get().getValue().equals(ResultValue.TRUE)) {
			Activator.important('''«goal.getName()»  is not reachable. Will not try to search witness''') 
			// create empty scenario file
			GalOutput2Scenario.buildScenarioFileFromReachabilityTrace("", scenario_file, galFile.getName().replace(".gal", ""), goal.getName()) 
			return Status.OK_STATUS 
		}
		val WitnessResult witnessResultHandler = if(invariant.nullOrEmpty) 
				getNewShortestWitnessResultHandler(goal, resultStoreUri)
			else 
				getNewComplementaryWitnessResultHandler(goal,resultStoreUri, invariant)
		var GalScenarios gscen=new GalScenarios(galFile){
			override void processGALOutpout(String stdOutString, boolean timeoutTrigerred) {
				//Activator.debug(stdOutString) 
				if(invariant.isNullOrEmpty){
					GalOutput2Scenario.buildScenarioFileFromReachabilityTrace(stdOutString, scenario_file, gal_file.getName().replace(".gal", ""), goal.getName())					
				} else {
					GalOutput2Scenario.complementScenarioFileFromReachabilityTrace(stdOutString, scenario_file, gal_file.getName().replace(".gal", ""), goal.getName())
				} 
				witnessResultHandler.getWitnessFound().clear() 
				GalOutput2Scenario.getScenarioLines(stdOutString).stream().forEach([s | witnessResultHandler.getWitnessFound().add(s)]) 
				if (stdOutString.contains("Reachability property goal_reached==1 is true.")) {
					Activator.important('''Found «witnessResultHandler.getWitnessFound().size()» witness(es) for goal «goal.getName()»''') 
					// TODO add/manage marker
					witnessResultHandler.setValue(ResultValue.TRUE) 
				} else {
					if (timeoutTrigerred) {
						Activator.important('''Timeout when searching witness of goal «goal.getName()»''') 
						witnessResultHandler.setValue(ResultValue.TIMEOUT) 
					} else {
						Activator.important('''Goal «goal.getName()»  is not reachable.''') 
						witnessResultHandler.setValue(ResultValue.FALSE) 
					}
				}
				witnessResultHandler.setLog(stdOutString) 
			}} 
		var SubMonitor subMonitorSearchingWitness=subMonitor.split(92) 
		var IPreferenceStore store=fr.irisa.atsyra.ide.ui.Activator.getDefault().getPreferenceStore() 
		var int nbWitness=store.getInt(fr.irisa.atsyra.ide.ui.preferences.PreferenceConstants.P_MANYWITNESS) 
		var int statePrintLimit=store.getInt(fr.irisa.atsyra.ide.ui.preferences.PreferenceConstants.P_STATE_PRINT_LIMIT) 
		gscen.computeScenarios(nbWitness, statePrintLimit, invariant, subMonitorSearchingWitness) 
		witnessResultHandler.setDuration(gscen.executionDuration) 
		subMonitorSearchingWitness.done() 
		try {
			witnessResultHandler.eResource().save(null) 
		} catch (IOException e) {
			Activator.eclipseError('''failed to save result in: «resultStoreUri»''', e) 
		}
		
		return Status.OK_STATUS 
	
	}
	
	def WitnessResult getNewShortestWitnessResultHandler(AtsyraGoal goal,  URI resultStoreUri){
		
		var Resource resultStoreRes=goal.eResource().getResourceSet().getResource(resultStoreUri, true) 
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if (resultStoreRes.getContents().isEmpty()) {
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore()) 
		}
		var ResultStore resultStore=(resultStoreRes.getContents().get(0) as ResultStore) 
		var Optional<GoalResult> grOp=resultStore.getGoalResults().stream().filter([gr | gr.getGoal().getName().equals(goal.getName())]).findFirst() 
		var GoalResult gr 
		if (grOp.isPresent()) {
			gr=grOp.get() 
		} else {
			gr=ResultstoreFactory.eINSTANCE.createGoalResult() 
			gr.setGoal(goal) 
			resultStore.getGoalResults().add(gr) 
		}
		var WitnessResult result=ResultstoreFactory.eINSTANCE.createWitnessResult() 
		result.setTimestamp(new java.util.Date()) 
		gr.setShortestWitnessResult(result) 
		result.setName('''Shortest witnesses of goal : «goal.getName()»''') 
		// also clear complementary witnesses
		gr.complementaryWitnessResults.clear
		return result 

	}
	
	def WitnessResult getNewComplementaryWitnessResultHandler(AtsyraGoal goal,  URI resultStoreUri, String invariant){
		
		var Resource resultStoreRes=goal.eResource().getResourceSet().getResource(resultStoreUri, true) 
		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
		if (resultStoreRes.getContents().isEmpty()) {
			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore()) 
		}
		var ResultStore resultStore=(resultStoreRes.getContents().get(0) as ResultStore) 
		var Optional<GoalResult> grOp=resultStore.getGoalResults().stream().filter([gr | gr.getGoal().getName().equals(goal.getName())]).findFirst() 
		var GoalResult gr 
		if (grOp.isPresent()) {
			gr=grOp.get() 
		} else {
			gr=ResultstoreFactory.eINSTANCE.createGoalResult() 
			gr.setGoal(goal) 
			resultStore.getGoalResults().add(gr) 
		}
		val String rname = '''Complementary witnesses of goal : «goal.getName()» using invariant «invariant»'''
		val previousComplementaryResult = gr.complementaryWitnessResults.findFirst[name==rname]
		if (previousComplementaryResult !== null) gr.complementaryWitnessResults.remove(previousComplementaryResult)
		var WitnessResult result=ResultstoreFactory.eINSTANCE.createWitnessResult() 
		result.setTimestamp(new java.util.Date())
		gr.complementaryWitnessResults.add(result)
		result.setName(rname) 
		return result 

	}
	
	/** Search for the IFile of the goal either via the event or directly from the eResource */
	def IFile getAtsyraGoalIFile(ExecutionEvent event, AtsyraGoal goal) {
		var IFile atsyraGoalFile=getAtsyraGoalFileFromSelection(event) 
		if (atsyraGoalFile === null) {
			// this means that we have to retrieve the IFile from the goal instance (either because 
			// it comes from an editor of because we have selected one goal among other in the project)
			atsyraGoalFile=EMFResource.getIFile(goal) 
		}
		return atsyraGoalFile 

	}
	
	def IFile getGalFile(IFile file, AtsyraGoal goal){
		//look for a gal file with the same name, in the genFolder
		val String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'))
		val IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)
		val IFile galFile = file.getProject().getFile(genFolder.getProjectRelativePath() + 
				"/" + fileNameNoExtension + "_" + goal.getName()+"_reach.gal")
		return galFile
	}
	
	def  IFile getWitnessScenarioFile(IFile file, AtsyraGoal goal){
		//look for a witness file with the same name, in the genFolder
		var String fileNameNoExtension=file.getName().substring(0, file.getName().lastIndexOf(Character.valueOf('.').charValue)) 
		var IFolder genFolder=file.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME) 
		var IFile galFile=file.getProject().getFile('''«genFolder.getProjectRelativePath()»/«fileNameNoExtension»_«goal.getName()»_reach.witness''') 
		return galFile 

	}
	
	override getSelectionMessage() {
		return "Compute shortest witness scenarios for the selected goal.";
	}
	
}