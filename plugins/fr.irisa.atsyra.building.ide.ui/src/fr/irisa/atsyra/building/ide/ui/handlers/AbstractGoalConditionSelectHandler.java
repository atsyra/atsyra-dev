/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.gemoc.commons.eclipse.core.resources.FileFinderVisitor;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.outline.impl.EObjectNode;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import atsyragoal.AtsyraGoal;
import atsyragoal.GoalCondition;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.dialogs.SelectAnyGoalConditionDialog;
import fr.irisa.atsyra.resultstore.GoalResult;

abstract public class AbstractGoalConditionSelectHandler extends AbstractHandler {

	public abstract Object executeForSelectedGoalCondition(ExecutionEvent event, IProject updatedGemocAtsyraGoalProject,
			GoalCondition goalCondition) throws ExecutionException;

	public abstract String getSelectionMessage();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// get the optional selection and eventually project data to preset the
		// wizard
		final IProject updatedGemocAtsyraGoalProject = getUpdatedAtsyraGoalProjectFromSelection(event);

		GoalCondition cond = getGoalConditionFromSelection(event);
		if(cond != null && updatedGemocAtsyraGoalProject != null){
			// go for it, we have everything in the selection
			executeForSelectedGoalCondition(event, updatedGemocAtsyraGoalProject, cond);
			return null;
		}
		
		final IFile atsyraGoalFile = getGoalConditionFileFromSelection(event);
		final ResourceSet rs = new ResourceSetImpl();
		if (atsyraGoalFile != null) {
			// we have only one AtsyraGoal file in the selection
			final URI uri = URI.createPlatformResourceURI(atsyraGoalFile.getFullPath().toString(), true);
			rs.getResource(uri, true);
		} else {
			// we will search for all .atg files in the project
			FileFinderVisitor atsyraProjectVisitor = new FileFinderVisitor("atg");
			try {
				updatedGemocAtsyraGoalProject.accept(atsyraProjectVisitor);
				for (IFile projectAtsyraGoalIFile : atsyraProjectVisitor.getFiles()) {
					// consider all melange files in the project, get them in
					// the ResourceSet
					if (!(projectAtsyraGoalIFile.getFullPath().toString().contains("/bin/") | projectAtsyraGoalIFile
							.getFullPath().toString().contains("/target/"))) {
						// FIXME stupid check to remove some typical duplicates,
						// a better impl should look into java output

						final URI uri = URI.createPlatformResourceURI(projectAtsyraGoalIFile.getFullPath().toString(),
								true);
						rs.getResource(uri, true);
					}
				}
			} catch (CoreException e) {
				Activator.eclipseError(e.getMessage(), e);
			}
		}

		final LabelProvider labelProvider = new LabelProvider() {
			public String getText(Object element) {
				if (element instanceof GoalCondition && ((GoalCondition) element).eContainer() instanceof AtsyraGoal) {
					return ((AtsyraGoal)((GoalCondition) element).eContainer()).getName() + 
							"." + 
							((GoalCondition) element).eContainingFeature().getName();
					
				} else
					return super.getText(element);
			}
		};
		final SelectAnyGoalConditionDialog dialog = new SelectAnyGoalConditionDialog(rs, labelProvider);
		dialog.setTitle("Select a GoalCondition");
		dialog.setMessage(getSelectionMessage());
		final int res = dialog.open();
		if (res == WizardDialog.OK) {
			cond = (GoalCondition) dialog.getFirstResult();
			executeForSelectedGoalCondition(event, updatedGemocAtsyraGoalProject, cond);
		}

		return null;
	}
	
	protected IFile getGoalConditionFileFromSelection(ExecutionEvent event){
		IFile selectedAtsyraGoalIFile = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();

				if (element instanceof IFile) {
					selectedAtsyraGoalIFile = (IFile) element;

				}
				if (element instanceof IAdaptable) {
					IFile res = (IFile) ((IAdaptable) element)
							.getAdapter(IFile.class);
					if (res != null) {
						selectedAtsyraGoalIFile = res;
					}
				}
			}
		}
		return selectedAtsyraGoalIFile;
	}
		
	protected GoalCondition getGoalConditionFromSelection(ExecutionEvent event){
		GoalCondition selectedGoalCondition = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();

				if (element instanceof GoalCondition) {
					selectedGoalCondition = (GoalCondition) element;
				}
				// TODO
				/* if (element instanceof GoalConditionResult) {
					selectedGoalCondition = ((GoalConditionResult) element).getGoalCondition();
				} */
				if (element instanceof EObjectNode) {
					// try selection from xtexteditor outline
					final XtextEditor editor = org.eclipse.xtext.ui.editor.utils.EditorUtils.getActiveXtextEditor(event);					
					final IUnitOfWork<GoalCondition, XtextResource> _function = (XtextResource it) -> {
						if(((EObjectNode) element).getEObject(it) instanceof GoalCondition){
							return (GoalCondition) ((EObjectNode) element).getEObject(it);
						} else {
							return null;
						}
					};
					final GoalCondition goalCond = editor.getDocument().readOnly(_function);
					if(goalCond != null){
						return goalCond;
					}
				}
				if (element instanceof IAdaptable) {
					GoalCondition res = (GoalCondition) ((IAdaptable) element).getAdapter(GoalCondition.class);
					if (res != null) {
						selectedGoalCondition = res;
					}
				}
			}
		}
		else {
			// try selection from xtexteditor
			final XtextEditor editor = org.eclipse.xtext.ui.editor.utils.EditorUtils.getActiveXtextEditor(event);
			if (editor != null) {
				final ITextSelection textSelection = (ITextSelection)editor.getSelectionProvider().getSelection();
				final IUnitOfWork<GoalCondition, XtextResource> _function = (XtextResource it) -> {
			        int _offset = textSelection.getOffset();
			        return this.getSelectedGoalCondition(it, _offset);
				};
				
				final GoalCondition cond = editor.getDocument().readOnly(_function);
				if(cond != null){
					return cond;
				}
			}
		}
		return selectedGoalCondition;
	}
	protected GoalCondition getSelectedGoalCondition(XtextResource resource, int offset){
		final EObjectAtOffsetHelper eObjectAtOffsetHelper =
			resource.getResourceServiceProvider().get(EObjectAtOffsetHelper.class);
		EObject selectedElement = eObjectAtOffsetHelper.resolveContainedElementAt(resource, offset);
		if (selectedElement != null) {
			EObject currentElem = selectedElement;
			while(currentElem != null){
				if(currentElem instanceof GoalCondition){
					return (GoalCondition)currentElem;
				}
				currentElem = currentElem.eContainer();
			}
		}
		return null;
	}
	protected IProject getUpdatedAtsyraGoalProjectFromSelection(ExecutionEvent event) {
		IProject updatedGemocLanguageProject = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();

				if (element instanceof IResource) {
					updatedGemocLanguageProject = ((IResource) element)
							.getProject();

				}
				if (element instanceof AtsyraGoal ){
					IFile goalFile = EMFResource.getIFile((AtsyraGoal) element);
					if(goalFile != null){
						updatedGemocLanguageProject  = goalFile.getProject();
					}
				}
				if (element instanceof GoalCondition ){
					IFile goalFile = EMFResource.getIFile((GoalCondition) element);
					if(goalFile != null){
						updatedGemocLanguageProject  = goalFile.getProject();
					}
				}
				if (element instanceof GoalResult ){
					IFile goalFile = EMFResource.getIFile(((GoalResult)element).getGoal());
					if(goalFile != null){
						updatedGemocLanguageProject  = goalFile.getProject();
					}
				}
				if (element instanceof EObjectNode) {
					// this is an outline element of an xtext editor
					final XtextEditor editor = org.eclipse.xtext.ui.editor.utils.EditorUtils.getActiveXtextEditor(event);
					updatedGemocLanguageProject = editor.getResource().getProject();
				}
				if (element instanceof IAdaptable) {
					IResource res = (IResource) ((IAdaptable) element)
							.getAdapter(IResource.class);
					if (res != null) {
						updatedGemocLanguageProject = res.getProject();
					} 
				}
			}
		}
		else if (selection != null & selection instanceof ITextSelection) {
			IResource res = (IResource) HandlerUtil
					.getActiveWorkbenchWindow(event).getActivePage()
					.getActiveEditor().getEditorInput()
					.getAdapter(IResource.class);
			if (res != null) {
				updatedGemocLanguageProject = res.getProject();
			}
		}
		return updatedGemocLanguageProject;
	}
	
	/**
	 * look for a resultstore file with the same name, in genFolder
	 * @param URI
	 * @return
	 */
	public URI getResultStoreURI(IFile file){
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME);
		IFile resultStoreFile = file.getProject().getFile(genFolder.getProjectRelativePath() + "/" + fileNameNoExtension +".resultstore");
		
		URI resultStoreUri = URI.createPlatformResourceURI(resultStoreFile.getFullPath().toString(), true);
		if(!resultStoreFile.exists()){
			// make sure the file exist, 
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
	        Map<String, Object> m = reg.getExtensionToFactoryMap();
	        m.put("resultstore", new XMIResourceFactoryImpl());
			ResourceSet resSet = new ResourceSetImpl();
			Resource res = resSet.createResource(resultStoreUri);
			try {
				res.save(null);
			} catch (IOException e) {
				Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
			}
		}
		return resultStoreUri;
	}
	

}
