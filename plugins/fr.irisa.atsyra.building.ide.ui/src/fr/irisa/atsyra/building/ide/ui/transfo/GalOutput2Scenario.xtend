package fr.irisa.atsyra.building.ide.ui.transfo

import fr.irisa.atsyra.building.ide.ui.Activator
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.util.ArrayList
import java.util.List
import java.util.Scanner
import org.eclipse.core.resources.IFile
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors

/** This class is in charge of producing an AttackScenario model file from the output of the GAL reachability tool */
class GalOutput2Scenario {

	public static final String GOAL_ACTION = "reach_goal";
	public static final String INIT_GOAL_ACTION = "assign_initial_values";
	public static final String TESTINIT_GOAL_ACTION = "test_initial_values";

	/** filter the raw trace to get the relevant ones
	 * 
	 * @param raw GAL reachability tool trace
	 * @return list of relevant lines
	 */
	def static List<String> getScenarioLines(String reachabilitytrace) {
		val ArrayList<String> result = new ArrayList<String>();
		var boolean nextLineIsAScenario = false;
		val Scanner scanner = new Scanner(reachabilitytrace);
		while (scanner.hasNextLine()) {
			val String currentLine = scanner.nextLine();
			if (nextLineIsAScenario) {
				result.add(currentLine);
			}
			if (currentLine.startsWith("This shortest transition sequence of length")) {
				nextLineIsAScenario = true;
			} else {
				nextLineIsAScenario = false;
			}
		}
		return result;
	}

	/** filter the raw trace to get the relevant ones
	 * 
	 * @param raw GAL reachability tool trace
	 * @return list of relevant lines
	 */
	def static List<String> getStatesStrings4TransitionStep(String reachabilitytrace, String witnessScenario) {
		val ArrayList<String> result = new ArrayList<String>();

		var StringBuilder currentStepStates = new StringBuilder;
		var boolean isInStep = false;

		val Scanner scanner = new Scanner(reachabilitytrace);
		while (scanner.hasNextLine()) {
			val String currentLine = scanner.nextLine();
			// scenario starts
			if (currentLine.startsWith(witnessScenario)) {
				isInStep = true
				currentStepStates.append("-1 defaultValues\n")
			}
			// in scenario, assemble states
			if (isInStep && currentLine.startsWith("[") && currentLine.endsWith("]")) {
				// TODO sort the variables in a deterministic way ?, add missing variables ?
				currentStepStates.append("  "+currentLine)
				currentStepStates.append("\n")
			}
			// finalize a step
			if (isInStep && currentLine.startsWith("Transition")) {
				isInStep = true
				// prepend with summary
				val tansitionSummary = currentLine.split(" ")
				currentStepStates.append( tansitionSummary.get(1) + " " + tansitionSummary.get(4) + "\n")
				result.add(currentStepStates.toString)
				currentStepStates = new StringBuilder;
			}
			// scenario ends
			if (isInStep && currentLine.startsWith("Leads to final states :")) {
				isInStep = false

			}
			// transition with too many states
			if (isInStep && currentLine.startsWith("[") && currentLine.contains("] showing")) {
				val transitionSummary = currentLine.split(" ")
				currentStepStates.append("    [* " +transitionSummary.get(5) + "/" + transitionSummary.get(1) + " *]\n")
			}
		}
		return result;
	}

	/**
	 * creates a new fr.irisa.atsyra.atsyratree scenario model
	 * Note, this version use a simple pretty printer strategy 
	 * @param reachabilitytrace
	 */
	def static void buildScenarioFileFromReachabilityTrace(String reachabilitytrace, IFile scenario_file,
		String baseFileName, String goalName) {

		val s = '''
«FOR currentLine : GalOutput2Scenario.getScenarioLines(reachabilitytrace)»
witness «baseFileName» {
	goal "«goalName»"
	steps {
		«currentLine.trim. //replaceAll(INIT_GOAL_ACTION,"").
		//replaceAll(", "+TESTINIT_GOAL_ACTION+", ","").
		//replaceAll(", "+TESTINIT_GOAL_ACTION,"").
		//replaceAll(", "+GOAL_ACTION,"").replaceAll(GOAL_ACTION,"").
		replaceAll(",","\n")»
	}
	states {
		«FOR currentTransition : getStatesStrings4TransitionStep(reachabilitytrace, currentLine)»
		«currentTransition»
		«ENDFOR»
	}
}
«ENDFOR»'''

		Activator.info(s);
		if (scenario_file.exists) {
			// compare content before saving, is the same, do nothing : this prevent from unnecessary subsequent job
			if (!isEqual(scenario_file.contents, new ByteArrayInputStream(s.getBytes("UTF8")))) {
				scenario_file.setContents(new ByteArrayInputStream(s.getBytes("UTF8")), true, true, null);
			}

		} else {
			scenario_file.create(new ByteArrayInputStream(s.getBytes("UTF8")), true, null);
		}
	}

	def static void complementScenarioFileFromReachabilityTrace(String reachabilitytrace, IFile scenario_file,
		String baseFileName, String goalName) {
		val String prevResult = if (scenario_file.exists) {
			new BufferedReader(new InputStreamReader(scenario_file.contents)).lines().collect(Collectors.joining("\n"));
		} else ""
		val sb = new StringBuilder
		sb.append(prevResult)
		for(currentLine : GalOutput2Scenario.getScenarioLines(reachabilitytrace)){
			val stepsForCurrentLine = currentLine.trim.replaceAll(",","\n")
			val formattedStepsForCurrentLine = '''steps {
		«stepsForCurrentLine»
	}'''
			if(!prevResult.contains(formattedStepsForCurrentLine)){
				sb.append('''witness «baseFileName» {
	goal "«goalName»"
	steps {
		«stepsForCurrentLine»
	}
	states {
		«FOR currentTransition : getStatesStrings4TransitionStep(reachabilitytrace, currentLine)»
		«currentTransition»
		«ENDFOR»
	}
}''')
			}
		}

		//Activator.info(s);
		val bytes = sb.toString.getBytes("UTF8")
		if (scenario_file.exists) {
			// compare content before saving, is the same, do nothing : this prevent from unnecessary subsequent job
			if (!isEqual(scenario_file.contents, new ByteArrayInputStream(bytes))) {
				scenario_file.setContents(new ByteArrayInputStream(bytes), true, true, null);
			}

		} else {
			scenario_file.create(new ByteArrayInputStream(bytes), true, null);
		}			
	}

	/** compare the content of the inputstream
	 * TODO put this in an utils lib or use IOUtil form apache commons
	 */
	def static private boolean isEqual(InputStream i1, InputStream i2) throws IOException {
		try {
			// do the compare
			while (true) {
				val int fr = i1.read();
				val int tr = i2.read();

				if (fr != tr)
					return false;

				if (fr == -1)
					return true;
			}

		} finally {
			if (i1 !== null)
				i1.close();
			if (i2 !== null)
				i2.close();
		}
	}

}
