/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraGoalModel;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.helpers.AtsyraHelper;
import fr.irisa.atsyra.building.ide.ui.helpers.ReachScenarioHelper;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class GoalToGALReachabilityHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		
		ISelection selection = HandlerUtil.getActiveMenuSelection(event); // the command is supposed to be launched via a menu
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
				Iterator<Object> iterator = strucSelection.iterator(); 
				iterator.hasNext();) {
				
				Object element = iterator.next();
				IResource res = null;
				if (element instanceof IResource) {
					res = (IResource) element;
				} else if (element instanceof IAdaptable) {
					res = (IResource) ((IAdaptable) element).getAdapter(IResource.class);
				}
				if (res != null) {
					generateGAL(res);
				} else {
					IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
					MessageDialog.openInformation(
							window.getShell(),
							"Building IDE UI",
							"Cannont proceed GoalToGAL on this selection");
				}
			}
		}
		
		return null;
	}
	
	void generateGAL(IResource resource) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {
			final IFile file = (IFile) resource;
			try {

				Job job = new Job("ATG2GAL from "+file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
						try {
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, subMonitor.split(5));
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute on "+ProcessConstants.GENFOLDER_NAME,e);
						}
						subMonitor.split(10);
						// do the job
						Injector inj = fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.getInstance()
								.getInjector(fr.irisa.atsyra.atsyragoal.xtext.ui.internal.XtextActivator.FR_IRISA_ATSYRA_ATSYRAGOAL_XTEXT_ATSYRAGOAL);
						//Injector inj = XtextActivator.getInstance().getInjector(XtextActivator.FR_IRISA_ATSYRA_BUILDING_XTEXT_BUILDING);

						
						XtextResourceSet resourceSet = inj.getInstance(XtextResourceSet.class); 
						resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
						//URI uri = URI.createFileURI(file.getRawLocationURI().getPath()) ;
						URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
						Resource eresource = resourceSet.getResource(uri, true);
						AtsyraGoalModel goalModel = (AtsyraGoalModel) eresource.getContents().get(0);
						if(!EcoreUtil.UnresolvedProxyCrossReferencer.find(goalModel).isEmpty()) {
							Activator.debug("not generating *_reach.gal for "+file.getName()+" because some proxies cannot be resolved yet, you may retry");

							return Status.OK_STATUS;
						}
						if(!AtsyraHelper.getAllGoalModelIFiles(goalModel).stream().allMatch(f -> f != null && f.exists())){
							Activator.debug("not generating *_reach.gal for "+file.getName()+" because some imported atg does not exist yet, you may retry");

							return Status.OK_STATUS;
						}
						
						SubMonitor loopMonitor = subMonitor.split(85).setWorkRemaining(goalModel.getAtsyragoals().size());
						IFolder genFolder = resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
						for(AtsyraGoal goal : goalModel.getAtsyragoals()){
							SubMonitor iterationMonitor = loopMonitor.split(1);

							if(iterationMonitor.isCanceled()) {
								return Status.CANCEL_STATUS;
							}
							if(goal instanceof AtsyraGoal){
								try {
									IFile galfile = genFolder.getFile(eresource.getURI().lastSegment().substring(0, eresource.getURI().lastSegment().lastIndexOf("."))+ "_" + goal.getName() + "_reach.gal");
									ReachScenarioHelper.generateGAL4GoalIfNecessary(goal, galfile, iterationMonitor);									
								} catch (Exception e) {
									Activator.eclipseError(e.getMessage(), e);
								}
							}
						}

						return Status.OK_STATUS;
					};
				};
				job.setPriority(Job.BUILD);
				// prevent concurrent job
				/*IResourceRuleFactory ruleFactory = ResourcesPlugin.getWorkspace().getRuleFactory();
				ISchedulingRule rule = MultiRule.combine(
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)),
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME)));*/
				job.setRule(resource.getProject());
				job.schedule();

									
			} catch (Exception e) {
				Activator.eclipseError(
						"ATG2GAL from "+file.getName()+" raised an exception "
								+ e.getMessage(), e);			
			}
			
			
		}
	}	
	
	
	
}
