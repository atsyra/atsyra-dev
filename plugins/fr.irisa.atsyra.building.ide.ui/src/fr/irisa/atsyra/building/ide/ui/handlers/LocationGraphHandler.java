/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.handlers;

import java.util.ArrayList;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import atsyragoal.AtsyraGoal;
import fr.irisa.atsyra.building.BuildingModel;
import fr.irisa.atsyra.building.ide.ui.Activator;
import fr.irisa.atsyra.building.ide.ui.ProcessConstants;
import fr.irisa.atsyra.building.ide.ui.helpers.ReachScenarioHelper;
import fr.irisa.atsyra.building.locationgraph.LocationGraph;
import fr.irisa.atsyra.transfo.atg.gal.AtsyraGoalHelper;
import fr.lip6.move.gal.GALTypeDeclaration;
import fr.lip6.move.gal.Specification;
import fr.lip6.move.gal.Variable;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class LocationGraphHandler extends AbstractAtsyraGoalSelectHandler {
	
	void launchLocationGraphJob(IResource resource, AtsyraGoal goal) {
		if (resource instanceof IFile && resource.getName().endsWith(".atg")) {
			final IFile file = (IFile) resource;
			try {

				Job job = new Job("Computing the graph of locations of a goal from "+file.getName()) {
					protected IStatus run(IProgressMonitor monitor) {
						SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
						try {
							resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME).setDerived(true, subMonitor.split(1));
						} catch (CoreException e) {
							Activator.eclipseWarn("Cannot set derive attribute on "+ProcessConstants.GENFOLDER_NAME,e);
						}
						
						// do the job
						SubMonitor subMonitorGenerate = subMonitor.split(5);
						IFile galFile = getGalFile(file, goal);
						BuildingModel buildingModel = getBuildingModelFromAtg(file);
						subMonitorGenerate.setTaskName("Generating "+galFile.getName());
						ReachScenarioHelper.generateGAL4GoalIfNecessary(goal, galFile, monitor);
						subMonitorGenerate.done();
						URI resultStoreUri = getResultStoreURI(file);
						SubMonitor subMonitorReachable = subMonitor.split(95);
						// compute reachability for the goal
						computeLocationGraph(goal, galFile, buildingModel, subMonitorReachable, resultStoreUri);

						return Status.OK_STATUS;
					};
				};
				job.setPriority(Job.LONG);
				// use mutex to prevent concurrent job
				IResourceRuleFactory ruleFactory = ResourcesPlugin.getWorkspace().getRuleFactory();
				ISchedulingRule rule = MultiRule.combine(
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.GENFOLDER_NAME)),
						ruleFactory.createRule(resource.getProject().getFolder(ProcessConstants.RESULTFOLDER_NAME)));
				job.setRule(rule);
				job.schedule();
									
			} catch (Exception e) {
				Activator.eclipseError(
						"Location graph from "+file.getName()	+" raised an exception "
								+ e.getMessage(), e);
			}
		}
	}	
	
	public IFile getGalFile(IFile file, AtsyraGoal goal){
		//look for a gal file with the same name, in the genFolder
		String fileNameNoExtension = file.getName().substring(0, file.getName().lastIndexOf('.'));
		IFolder genFolder = file.getProject().getFolder(ProcessConstants.GENFOLDER_NAME);
		IFile galFile = file.getProject().getFile(genFolder.getProjectRelativePath() + 
				"/" + fileNameNoExtension + "_" + goal.getName()+"_locgraph.gal");
		return galFile;
	}
	
	public BuildingModel getBuildingModelFromAtg(IFile file){
		IFile buildingFile = file.getProject().getFile(file.getProjectRelativePath().removeFileExtension().addFileExtension("building"));
		XtextResourceSet resourceSet = new XtextResourceSet();
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		URI uri = URI.createPlatformResourceURI(buildingFile.getFullPath().toString(), true);
		Resource eresource = resourceSet.getResource(uri, true);
		return (BuildingModel) eresource.getContents().get(0);
	}
	
	private Specification getGalModelFromFile(IFile galFile) {
		XtextResourceSet resourceSet = new XtextResourceSet();
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		URI uri = URI.createPlatformResourceURI(galFile.getFullPath().toString(), true);
		Resource eresource = resourceSet.getResource(uri, true);
		return (Specification) eresource.getContents().get(0);
	}
	
	private String getLocationVariable(Specification gal, BuildingModel buildingModel) {
//		gal.eAllContents().forEachRemaining(
//				(object) -> {
//					if(object instanceof Variable && ((Variable) object).getName().endsWith(".location")) {
//						ArrayList<String> name = new ArrayList<String>();
//						name.add(((Variable) object).getName().substring(0, ((Variable) object).getName().indexOf('.')));
//						buildingModel.getBuildings().forEach((b) -> {
//							b.getAttackers().forEach((a) -> {
//								if(a.getName() == name.get(0)) result.add(a.getName());
//							});
//						});
//					}
//				});
		//TODO check that the variable actually exists in gal
		return buildingModel.getBuildings().get(0).getAttackers().get(0).getName() + ".location";
	}
	
	/** Search for the IFile of the goal either via the event or directly from the eResource */
	protected IFile getAtsyraGoalIFile(ExecutionEvent event, AtsyraGoal goal) {
		IFile atsyraGoalFile = getAtsyraGoalFileFromSelection(event);
		if (atsyraGoalFile == null) {
			// this means that we have to retrieve the IFile from the goal instance (either because 
			// it comes from an editor of because we have selected one goal among other in the project)
			atsyraGoalFile = EMFResource.getIFile(goal);
		}
		return atsyraGoalFile;
	}

	@Override
	public Object executeForSelectedAtsyraGoal(ExecutionEvent event, IProject updatedAtsyraGoalProject,
			AtsyraGoal goal) throws ExecutionException {
		IFile atsyraGoalFile = getAtsyraGoalIFile(event, goal);
		launchLocationGraphJob(atsyraGoalFile, goal);
		return null;
	}

	@Override
	public String getSelectionMessage() {
		return "Compute the graph of locations of the selected goal.";
	}
	

//	public Result getNewReachableResultHandler(AtsyraGoal goal,  URI resultStoreUri){
//		Resource resultStoreRes = goal.eResource().getResourceSet().getResource(resultStoreUri, true);
//		// find and replace the result handler for the given goal, creates intermediate model elements if necessary
//		if(resultStoreRes.getContents().isEmpty()){
//			resultStoreRes.getContents().add(ResultstoreFactory.eINSTANCE.createResultStore());
//		}
//		ResultStore resultStore = (ResultStore) resultStoreRes.getContents().get(0);
//		Optional<GoalResult> grOp = resultStore.getGoalResults().stream().filter(gr -> gr.getGoal().getName().equals(goal.getName())).findFirst();
//		GoalResult gr;
//		if(grOp.isPresent()){
//			gr = grOp.get();
//		} else {
//			gr = ResultstoreFactory.eINSTANCE.createGoalResult();
//			gr.setGoal(goal);
//			resultStore.getGoalResults().add(gr);
//		}
//		Result result = ResultstoreFactory.eINSTANCE.createResult();
//		result.setTimestamp(new java.util.Date());
//		gr.setReachabilityResult(result);
//		result.setName("Reachability of goal : "+goal.getName());
//		return result;
//	}
	
	protected void computeLocationGraph(AtsyraGoal goal, IFile galFile, BuildingModel buildingModel, IProgressMonitor subMonitorReachable,  URI resultStoreUri) {
//		Result resultHanlder = getNewReachableResultHandler(goal, resultStoreUri);
		String locationVariable = getLocationVariable(getGalModelFromFile(galFile),buildingModel);
		LocationGraph lg = new LocationGraph(galFile, AtsyraGoalHelper.getPostString(goal), locationVariable); 
		lg.computeLocationGraph(subMonitorReachable);
//		try {
//			resultHanlder.eResource().save(null);
//		} catch (IOException e) {
//			Activator.eclipseError("failed to save result in: "+resultStoreUri,e);
//		}
	}

}
