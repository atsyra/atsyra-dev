package fr.irisa.atsyra.absystem.plantuml.ui

import fr.irisa.atsyra.absreport.aBSReport.ABSReportModel
import fr.irisa.atsyra.absreport.aBSReport.Step
import java.util.List
import java.util.Optional
import java.util.stream.Collectors
import java.util.ArrayList
import net.sourceforge.plantuml.util.DiagramIntent
import fr.irisa.atsyra.absreport.aBSReport.GoalReport

class ABSReportGoalUnifiedStepsDiagramIntent implements DiagramIntent {
	GoalReport goalReport

	new(GoalReport goalReport) {
		this.goalReport = goalReport
	}

	override String getLabel() {
		return goalReport.goal.name
	}

	override String getDiagramText() {
		return
			'''
			@startuml
			«getDiagramText(goalReport)»
			@enduml
			'''

	}
	
	def String getDiagramText(GoalReport goalReport) {
		'''
			digraph «goalReport.goal.name» {
				labelloc="t";
				label="Goal «goalReport.goal.name» witnesses";
				"begin" [shape = doublecircle];
				"end" [shape = doublecircle];
				edge [colorscheme = set19];
				«FOR scenario : goalReport.witnesses»
				edge [color = «val index = goalReport.witnesses.indexOf(scenario)»«index+1»];
				«FOR edge : edgesFromSteps(scenario.steps, index)»
				«edge»
				«ENDFOR»
				«ENDFOR»
				{ rank = source; "begin"; }
				{ rank = sink; "end"; }
			}
		'''
	}
	def String getDiagramText(ABSReportModel reportModel) {
		if(reportModel.reports.empty){
			return null
		}
		else {		
			'''
			«FOR goalReport : reportModel.reports SEPARATOR '\nnewpage\n'»
			«getDiagramText(goalReport)»
			«ENDFOR»
			'''	
		}
	}

	override int getPriority() {
		return 0
	}
	
	
	private def edgesFromSteps(List<Step> steps, int scenarioIndex) {
		val result = newArrayList
		val labels = steps.stream.map[step | getStepName(step)].collect(Collectors.toCollection([new ArrayList<CharSequence>()]))
		labels.add(0,"begin")
		labels.add("end")
		val iterator = labels.listIterator
		var previous = Optional.empty
		while(iterator.hasNext) {
			val step = iterator.next
			if(previous.present) {
				val stepIndex = iterator.nextIndex - 2
				result.add('"' + previous.get() + '" -> "' + step + '" [label="' + scenarioIndex + '::' + stepIndex + '"];')
			}
			previous = Optional.of(step)
		}
		result
	}
	
	private def getStepName(Step step) {
		 '''«step.guardedAction.name»«FOR param : step.parameters BEFORE '(' SEPARATOR ', ' AFTER ')'»«param.name»«ENDFOR»'''
	}
	
}
