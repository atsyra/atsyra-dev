package fr.irisa.atsyra.absystem.plantuml.ui;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

import com.google.common.collect.Iterators;

import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemAdapterFactory;
import net.sourceforge.plantuml.util.DiagramIntent;

public class ABSDiagramIntentProvider extends AbstractABSDiagramIntentProvider {

	// used to test if an object is supported
	protected ComposedAdapterFactory adapterFactory ;
	
	public ABSDiagramIntentProvider() {
		super(IEditingDomainProvider.class);
		this.adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		this.adapterFactory.addAdapterFactory(new AbsystemAdapterFactory());
		//this.adapterFactory.addAdapterFactory(new AbsystemAdapterFactory());
	}

	public ABSDiagramIntentProvider(Class<?> partType) {
		super(partType);
		this.adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		this.adapterFactory.addAdapterFactory(new AbsystemAdapterFactory());
	}

	@Override
	protected boolean supportsEObject(EObject object) {
		return adapterFactory.isFactoryForType(object);
	}

	
	public boolean isABSDiagramObject(final Object object) {
		EObject eo = (EObject) object;
		if(adapterFactory.isFactoryForType(object)) {
			return true;
		} 
		if(getAssetBasedSystem(eo) != null) {
			return true;
		}
		return false;
	}
	
	@Override
	protected Collection<? extends DiagramIntent> getDiagramInfos(EObject eObject) {
		final Collection<DiagramIntent> diagrams = new ArrayList<>();
		final boolean isABSDiagram = isABSDiagramObject(eObject);
		if(isABSDiagram) {
			Asset asset = ABSDiagramHelper.getAncestor(eObject, Asset.class);
			if(asset != null) {
				diagrams.add(new ABSAssetSelectionDiagramIntent(asset));
			}
			AssetType assetType = ABSDiagramHelper.getAncestor(eObject, AssetType.class);
			if(assetType != null) {
				diagrams.add(new ABSAssetTypeSelectionDiagramIntent(assetType));
			}
			AssetBasedSystem abs = getAssetBasedSystem(eObject);
		    if (! IteratorExtensions.isEmpty(Iterators.<Asset>filter(abs.eAllContents(), Asset.class))) {
		    	diagrams.add(new ABSAssetDiagramIntent(abs));
		    }
		    if (! IteratorExtensions.isEmpty(Iterators.<AssetType>filter(abs.eAllContents(), AssetType.class))) {
		    	diagrams.add(new ABSAssetTypeDiagramIntent(abs));
		    }

		}
		
		return diagrams;
	}
	
	@Override
	protected Boolean supportsPath(final IPath path) {
		return "abs".equals(path.getFileExtension()) || "xmi".equals(path.getFileExtension());
	}
	

	protected AssetBasedSystem getAssetBasedSystem(final EObject selection) {
		return ABSDiagramHelper.getAncestor(selection, AssetBasedSystem.class);
	}

}
