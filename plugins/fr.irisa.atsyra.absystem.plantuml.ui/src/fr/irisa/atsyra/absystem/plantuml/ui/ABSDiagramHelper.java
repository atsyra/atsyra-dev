package fr.irisa.atsyra.absystem.plantuml.ui;

import org.eclipse.emf.ecore.EObject;

public class ABSDiagramHelper {

	public static <T> T getAncestor(EObject eObject, final Class<T> clazz) {
		while (eObject != null) {
			if (clazz.isInstance(eObject)) {
				return (T) eObject;
			}
			eObject = eObject.eContainer();
		}
		return null;
	}
}
