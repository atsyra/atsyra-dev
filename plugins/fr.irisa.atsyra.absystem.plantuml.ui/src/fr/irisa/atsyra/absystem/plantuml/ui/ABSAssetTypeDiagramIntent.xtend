package fr.irisa.atsyra.absystem.plantuml.ui

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils
import java.util.Set
import net.sourceforge.plantuml.util.DiagramIntent

abstract class AbstractABSAssetTypeDiagramIntent implements DiagramIntent {
	
	def String multiplicityString(Multiplicity m) {
		switch (m) {
			case Multiplicity.ONE: {
				'1'
			}
			case Multiplicity.ZERO_OR_MANY: {
				'*'
			}
			case Multiplicity.ONE_OR_MANY: {
				'1..*'
			}
			default: {
				'0..1'
			}
		}
	}
	
	def String getDiagramText(Set<AssetType> relevantAssetTypes) {
		val boolean isLargeModel = relevantAssetTypes.size > 10
		return
			'''
				@startuml
				«IF isLargeModel»scale 4000*4000«ENDIF»
				«FOR assetType : relevantAssetTypes»
					class «assetType.name» «FOR tag : assetType.tags BEFORE '<<' SEPARATOR ', ' AFTER '>>'»«tag.name»«ENDFOR» #DDDDDD {
						«FOR assetTypeAttribute : assetType.assetTypeAttributes»
							«assetTypeAttribute.name» : «assetTypeAttribute.attributeType.name»
						«ENDFOR»
					}
				«ENDFOR»
				
				«FOR assetType : relevantAssetTypes»
					«FOR assetTypeReference : assetType.assetTypeProperties»
						«assetType.name» «IF isLargeModel»--down->«ELSE»->«ENDIF» "«assetTypeReference.multiplicity.multiplicityString»" «assetTypeReference.propertyType.name» : «assetTypeReference.name»
					«ENDFOR»
				«ENDFOR»
				
				«FOR assetType : relevantAssetTypes»
					«FOR assetTypeParent : assetType.extends»
						«assetType.name» -up-|>  «assetTypeParent.name»
					«ENDFOR»
				«ENDFOR»
				@enduml
			'''
		
	}
	
}

class ABSAssetTypeDiagramIntent extends AbstractABSAssetTypeDiagramIntent {
	AssetBasedSystem abs

	new(AssetBasedSystem abs) {
		this.abs = abs
	}

	override String getLabel() {
		return abs.eResource.URI.toString
	}

	override String getDiagramText() {
		return getDiagramText(abs.eAllContents.filter(AssetType).toSet)
	}

	override int getPriority() {
		return 0
	}
}

class ABSAssetTypeSelectionDiagramIntent extends AbstractABSAssetTypeDiagramIntent {
	AssetType assetType

	new(AssetType assetType) {
		this.assetType = assetType
	}

	override String getLabel() {
		return assetType.name
	}

	override String getDiagramText() {
		var Set<AssetType> relevantAssetTypes = newHashSet(assetType) 
		relevantAssetTypes.addAll(ABSUtils.getAllSupertypes(assetType))
		
		return getDiagramText(relevantAssetTypes)
	}

	override int getPriority() {
		return 0
	}
	
}
