package fr.irisa.atsyra.absystem.plantuml.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;

import fr.irisa.atsyra.absreport.aBSReport.ABSReportModel;
import fr.irisa.atsyra.absreport.aBSReport.GoalReport;
import fr.irisa.atsyra.absreport.aBSReport.util.ABSReportAdapterFactory;
import net.sourceforge.plantuml.util.DiagramIntent;

public class ABSReportDiagramIntentProvider extends AbstractABSDiagramIntentProvider {

	// used to test if an object is supported
	protected ComposedAdapterFactory adapterFactory ;
	
	public ABSReportDiagramIntentProvider() {
		super(IEditingDomainProvider.class);
		this.adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		this.adapterFactory.addAdapterFactory(new ABSReportAdapterFactory());
		//this.adapterFactory.addAdapterFactory(new AbsystemAdapterFactory());
	}

	public ABSReportDiagramIntentProvider(Class<?> partType) {
		super(partType);
		this.adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		this.adapterFactory.addAdapterFactory(new ABSReportAdapterFactory());
	}

	@Override
	protected boolean supportsEObject(EObject object) {
		return adapterFactory.isFactoryForType(object);
	}

	
	public boolean isABSReportDiagramObject(final Object object) {
		EObject eo = (EObject) object;
		if(adapterFactory.isFactoryForType(object)) {
			return true;
		} 
		if(getABSReportModel(eo) != null) {
			return true;
		}
		// in case we select an abs element referenced by the abs report
		Collection<Setting> c = EcoreUtil.UsageCrossReferencer.find(eo, eo.eResource().getResourceSet());
		for(Setting s : c) {
			if(adapterFactory.isFactoryForType(s.getEObject())) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected Collection<? extends DiagramIntent> getDiagramInfos(EObject eObject) {
		final Collection<DiagramIntent> diagrams = new ArrayList<>();
		final boolean isABSReportDiagram = isABSReportDiagramObject(eObject);
		if(isABSReportDiagram) {
			GoalReport gr  = diagramHelper.getAncestor(eObject, GoalReport.class);
			if(gr != null) {
				diagrams.add(new ABSReportGoalUnifiedStepsDiagramIntent(gr));
			} else {
				ABSReportModel reportModel = getABSReportModel( eObject );
				if(reportModel != null) {

					for (GoalReport goalReport : reportModel.getReports()) {
						diagrams.add(new ABSReportGoalUnifiedStepsDiagramIntent(goalReport));	
					}
				} else {
					// in case we select an abs element referenced by the abs report
					Set<GoalReport> grSet = new HashSet<GoalReport>();
					Collection<Setting> c = EcoreUtil.UsageCrossReferencer.find(eObject, eObject.eResource().getResourceSet());
					for(Setting s : c) {
						if(adapterFactory.isFactoryForType(s.getEObject())) {
							gr = diagramHelper.getAncestor(s.getEObject(), GoalReport.class);
							if(gr != null) {
								grSet.add(gr);
							}
						}
					}
					for (GoalReport gr2 : grSet) {
						diagrams.add(new ABSReportGoalUnifiedStepsDiagramIntent(gr2));
					}
				}
			}
		}
		
		return diagrams;
	}
	
	@Override
	protected Boolean supportsPath(final IPath path) {
		return "absreport".equals(path.getFileExtension()) || "xmi".equals(path.getFileExtension());
	}
	
	protected final ABSDiagramHelper diagramHelper = new ABSDiagramHelper();
	
	protected ABSReportModel getABSReportModel(final EObject selection) {
		return diagramHelper.getAncestor(selection, ABSReportModel.class);
	}

}
