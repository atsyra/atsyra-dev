package fr.irisa.atsyra.absystem.plantuml.ui;

import java.util.Collection;

import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemAdapterFactory;
import net.sourceforge.plantuml.eclipse.utils.WorkbenchPartDiagramIntentProviderContext;
import net.sourceforge.plantuml.eclipse.utils.WorkspaceDiagramIntentProviderContext;
import net.sourceforge.plantuml.util.DiagramIntent;
import net.sourceforge.plantuml.util.DiagramIntentContext;

public class ABSTextDiagramIntentProvider extends ABSDiagramIntentProvider {

	// used to test if an object is supported
	protected ComposedAdapterFactory adapterFactory;

	public ABSTextDiagramIntentProvider() {
		//super(XtextEditor.class);
		super(null);
		this.adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		this.adapterFactory.addAdapterFactory(new AbsystemAdapterFactory());
	}

	@Override
	public boolean supportsEditor(final IEditorPart editorPart) {
		if(editorPart instanceof XtextEditor) {
			if (editorPart.getEditorInput() instanceof IPathEditorInput) {
				final IPathEditorInput editorInput = (IPathEditorInput) editorPart.getEditorInput();
				if ("abs".equals(editorInput.getPath().getFileExtension())) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	protected Collection<? extends DiagramIntent> getDiagramInfos(
			final WorkbenchPartDiagramIntentProviderContext context) {
		return ((XtextEditor) context.getWorkbenchPart()).getDocument()
				.readOnly(new IUnitOfWork<Collection<? extends DiagramIntent>, XtextResource>() {
					@Override
					public Collection<? extends DiagramIntent> exec(final XtextResource state) throws Exception {
						ISelection selection = context.getSelection();
						if (selection != null && selection instanceof TextSelection) {
							return getDiagramInfos((new EObjectAtOffsetHelper()).resolveElementAt(state,
									((TextSelection) selection).getOffset()));
						} else {
							return getDiagramInfos(((Resource) state).getResourceSet());
						}
					}
				});
	}

	@Override
	protected boolean supportsEObject(EObject object) {
		// TODO Auto-generated method stub
		return super.supportsEObject(object);
	}

	@Override
	protected Collection<? extends DiagramIntent> getDiagramInfos(EObject eObject) {
		// TODO Auto-generated method stub
		return super.getDiagramInfos(eObject);
	}

	@Override
	protected Boolean supportsPath(IPath path) {
		// TODO Auto-generated method stub
		return super.supportsPath(path);
	}

	@Override
	public Boolean supportsSelection(ISelection selection) {
		Boolean supported = super.supportsSelection(selection);
		if (selection instanceof TextSelection ) {
			supported = true;
		}
		return supported;
	}

	@Override
	protected Collection<? extends DiagramIntent> getDiagramInfos(ResourceSet resourceSet) {
		return super.getDiagramInfos(resourceSet);
	}

	@Override
	protected boolean supportsView(IViewPart viewPart) {
		return super.supportsView(viewPart);
	}

	@Override
	public Collection<? extends DiagramIntent> getDiagramInfos(DiagramIntentContext context) {
		if (context instanceof WorkbenchPartDiagramIntentProviderContext) {
			final WorkbenchPartDiagramIntentProviderContext workbenchContext = (WorkbenchPartDiagramIntentProviderContext) context;
			final IWorkbenchPart workbenchPart = workbenchContext.getWorkbenchPart();
			if (workbenchPart instanceof IEditorPart) {
				
				if (!supportsEditor((IEditorPart) workbenchPart)) {
					return null;
				}
				return getDiagramInfos(workbenchContext);
				
			} else {
				return null;
			}
		} else if (context instanceof WorkspaceDiagramIntentProviderContext) {
			final WorkspaceDiagramIntentProviderContext workspaceContext = (WorkspaceDiagramIntentProviderContext) context;
			if (!Boolean.FALSE.equals(supportsPath(workspaceContext.getPath()))) {
				return getDiagramInfos(workspaceContext);
			}
		}
		return null;
		// return super.getDiagramInfos(context);
	}

}
