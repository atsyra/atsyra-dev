package fr.irisa.atsyra.absystem.plantuml.ui

import fr.irisa.atsyra.absystem.model.absystem.Asset
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.AssetLink
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils
import net.sourceforge.plantuml.util.DiagramIntent
import java.util.List
import java.util.Set

abstract class AbstractABSAssetDiagramIntent implements DiagramIntent {
	
	def String getDiagramText(Set<AssetLink> relevantLinks, Set<Asset> relevantAssets) {
		val boolean isLargeModel = relevantAssets.size > 10
		return
			'''
				@startuml
				«IF isLargeModel»scale 4000*4000«ENDIF»
				«FOR asset : relevantAssets»
					object "«asset.name» : «asset.assetType.name»" as «asset.name» «objectColor(asset)»
				«ENDFOR»
				«FOR assetLink : relevantLinks»
					«assetLink.sourceAsset.name» «linkModifier(assetLink, isLargeModel)»-> «assetLink.targetAsset.name» : «assetLink.referenceType?.name»
				«ENDFOR»
				@enduml
			'''

	}
	
	
	def String linkModifier(AssetLink assetLink, boolean isLargeModel) {
		var containerLevel = ABSDiagramHelper.getAncestor(assetLink.referenceType, AssetType).computeLevel
		var targetLevel = assetLink?.referenceType?.propertyType.computeLevel
		if(containerLevel > targetLevel) {
			return "-"
		}
		if(containerLevel < targetLevel) return "-up"
		if("connectedTo" == assetLink.referenceType?.name) return "-up"
		if (isLargeModel) 
			return "-down"
		else
			return ""
	}

	/** look into inheritance for a parent  with a defined level 
	 *  
	 */
	def String computeLevel(AssetType assetType) {
		if(assetType === null) return "0"
		val allAssetTypes = newLinkedHashSet
		allAssetTypes.add(assetType)
		allAssetTypes.addAll(ABSUtils.getAllSupertypes(assetType))

		if (allAssetTypes.exists[at|at.level !== null]) {
			return allAssetTypes.findFirst[at|at.level !== null].level
		} else {
			return "0"
		}
	}

	def String objectColor(Asset asset) {
		switch (asset.assetType.computeLevel) {
			case "1":
				"#lavender"
			case "2":
				"#LightBlue"
			case "3":
				"#Aquamarine"
			case "4":
				"#Wheat"
			case "5":
				"#SandyBrown"
			default: {
				""
			}
		}
	}
}

class ABSAssetDiagramIntent extends AbstractABSAssetDiagramIntent {
	AssetBasedSystem abs

	new(AssetBasedSystem abs) {
		this.abs = abs
	}

	override String getLabel() {
		return abs.eResource.URI.toString
	}

	override String getDiagramText() {
		return getDiagramText(abs.eAllContents.filter(AssetLink).toSet, abs.eAllContents.filter(Asset).toSet)
	}

	override int getPriority() {
		return 0
	}
}

class ABSAssetSelectionDiagramIntent extends AbstractABSAssetDiagramIntent {
	Asset asset

	new(Asset asset) {
		this.asset = asset
	}

	override String getLabel() {
		return asset.name
	}

	override String getDiagramText() {
		val Set<AssetLink> relevantLinks = allLinkOfAsset(asset).toSet;
		var Set<Asset> relevantAssets = newHashSet(asset) 
		relevantAssets.addAll(relevantLinks.map[link | if( link.sourceAsset == asset) { link.targetAsset} else {link.sourceAsset}].toSet)
		return getDiagramText(relevantLinks, relevantAssets)

	}

	override int getPriority() {
		return 0
	}
	
	def List<AssetLink> allLinkOfAsset(Asset asset) {
		asset.eResource.resourceSet.allContents.filter(AssetLink).filter[assetLink | assetLink.sourceAsset == asset || assetLink.targetAsset == asset].toList
	}
}
