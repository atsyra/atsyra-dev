/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.validation;

import java.util.Locale;
import java.util.Set;

import org.eclipse.emf.common.util.DiagnosticChain;

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;

/**
 * 
 * @author Maxime Audinot
 *
 */
public interface Validator {
	/**
	 * @param abs the system to validate
	 * @param diagnostics a diagnostic chain to accumulate Diagnostics. if null, then no diagnostics should be generated.
	 */
	public boolean validate(AssetBasedSystem abs, DiagnosticChain diagnostics);
	
	/**
	 * Validates multiple ABS models together, which means that :
	 * 1) all AssetBasedSystem in absSet should be structurally validated independently
	 * 2) all contracts of all AssetBasedSystem in absSet should be checked on every AssetBasedSystem in absSet
	 * @param absSet a collection of system to validate together.
	 * @param diagnostics a diagnostic chain to accumulate Diagnostics. if null, then no diagnostics should be generated.
	 */
	public boolean validate(Set<AssetBasedSystem> absSet, DiagnosticChain diagnostics);
	
	/**
	 * @param abs the system to validate
	 * @param diagnostics a diagnostic chain to accumulate Diagnostics. if null, then no diagnostics should be generated.
	 * @param locale the locale the validation error message should be translated in
	 */
	public boolean validate(AssetBasedSystem abs, DiagnosticChain diagnostics, Locale locale);
	
	/**
	 * Validates multiple ABS models together, which means that :
	 * 1) all AssetBasedSystem in absSet should be structurally validated independently
	 * 2) all contracts of all AssetBasedSystem in absSet should be checked on every AssetBasedSystem in absSet
	 * @param absSet a collection of system to validate together.
	 * @param diagnostics a diagnostic chain to accumulate Diagnostics. if null, then no diagnostics should be generated.
	 * @param locale the locale the validation error message should be translated in
	 */
	public boolean validate(Set<AssetBasedSystem> absSet, DiagnosticChain diagnostics, Locale locale);
}
