/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.validation;

import java.util.Locale;
import java.util.Set;

import org.eclipse.emf.common.util.DiagnosticChain;

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;

/**
 * An implementation of {@link Validator} that performs all validations
 * - EMF Structure validation
 * - Requirement contracts
 * - Domain contracts
 * @author Maxime Audinot
 *
 */
public class FullValidator implements Validator {
	
	public static final FullValidator INSTANCE = new FullValidator();
	
	private EMFStructureValidator structuralValidator;
	private DomainContractValidator domainContractValidator;
	private RequirementContractValidator requirementContractValidator;

	public FullValidator() {
		structuralValidator = new EMFStructureValidator();
		domainContractValidator = new DomainContractValidator();
		requirementContractValidator = new RequirementContractValidator();
	}

	@Override
	public boolean validate(AssetBasedSystem abs, DiagnosticChain diagnostics) {
		boolean structurallyValid = structuralValidator.validate(abs, diagnostics);
		boolean domainContractsValid = domainContractValidator.validate(abs, diagnostics);
		boolean requirementContractsValid = requirementContractValidator.validate(abs, diagnostics);
		return structurallyValid && domainContractsValid && requirementContractsValid;
	}

	@Override
	public boolean validate(Set<AssetBasedSystem> absSet, DiagnosticChain diagnostics) {
		boolean structurallyValid = structuralValidator.validate(absSet, diagnostics);
		boolean domainContractsValid = domainContractValidator.validate(absSet, diagnostics);
		boolean requirementContractsValid = requirementContractValidator.validate(absSet, diagnostics);
		return structurallyValid && domainContractsValid && requirementContractsValid;
	}

	@Override
	public boolean validate(AssetBasedSystem abs, DiagnosticChain diagnostics, Locale locale) {
		boolean structurallyValid = structuralValidator.validate(abs, diagnostics, locale);
		boolean domainContractsValid = domainContractValidator.validate(abs, diagnostics, locale);
		boolean requirementContractsValid = requirementContractValidator.validate(abs, diagnostics, locale);
		return structurallyValid && domainContractsValid && requirementContractsValid;
	}

	@Override
	public boolean validate(Set<AssetBasedSystem> absSet, DiagnosticChain diagnostics, Locale locale) {
		boolean structurallyValid = structuralValidator.validate(absSet, diagnostics, locale);
		boolean domainContractsValid = domainContractValidator.validate(absSet, diagnostics, locale);
		boolean requirementContractsValid = requirementContractValidator.validate(absSet, diagnostics, locale);
		return structurallyValid && domainContractsValid && requirementContractsValid;
	}

}
