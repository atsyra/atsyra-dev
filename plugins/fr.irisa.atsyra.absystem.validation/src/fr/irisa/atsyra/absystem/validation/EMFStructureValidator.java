/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.validation;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.util.Diagnostician;

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator;

/**
 * An implementation of {@link Validator} that only performs EMF structure and OCL validation
 * 
 * @author Maxime Audinot
 *
 */
public class EMFStructureValidator implements Validator {
	
	public static final EMFStructureValidator INSTANCE = new EMFStructureValidator();
	
	public static final String DIAGNOSTIC_SOURCE = AbsystemValidator.DIAGNOSTIC_SOURCE;

	@Override
	public boolean validate(AssetBasedSystem abs, DiagnosticChain diagnostics) {
		return Diagnostician.INSTANCE.validate(abs, diagnostics);
	}

	@Override
	public boolean validate(Set<AssetBasedSystem> absSet, DiagnosticChain diagnostics) {
		boolean result = true;
		for (AssetBasedSystem abs : absSet) {
			result = validate(abs, diagnostics) && result;
			if (diagnostics == null && !result) {
				return result;
			}
		}
		return result;
	}

	@Override
	public boolean validate(AssetBasedSystem abs, DiagnosticChain diagnostics, Locale locale) {
		if(locale == null) {
			return validate(abs, diagnostics);
		}
		Map<Object,Object> context = new HashMap<>();
		context.put(Locale.class, locale);
		return Diagnostician.INSTANCE.validate(abs, diagnostics, context);
	}

	@Override
	public boolean validate(Set<AssetBasedSystem> absSet, DiagnosticChain diagnostics, Locale locale) {
		boolean result = true;
		for(AssetBasedSystem abs : absSet) {
			result = validate(abs, diagnostics, locale) && result;
			if(diagnostics == null && !result) {
				return result;
			}
		}
		return result;
	}

}
