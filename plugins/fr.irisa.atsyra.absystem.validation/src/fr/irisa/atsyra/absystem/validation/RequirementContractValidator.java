/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.validation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetBasedSystemK3Aspect;
import fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardK3Aspect;
import fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.GuardOccurenceK3Aspect;
import fr.irisa.atsyra.absystem.k3dsa.commons.AssetBasedSystemException;
import fr.irisa.atsyra.absystem.k3dsa.commons.InvalidConfigurationException;
import fr.irisa.atsyra.absystem.k3dsa.commons.UndefinedReceiverException;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.Contract;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.GuardLocale;
import fr.irisa.atsyra.absystem.model.absystem.Requirement;
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;

/**
 * An implementation of {@link Validator} that  requirement contract validation
 * Ie. contracts associated a at least a requirement
 * 
 * @author Maxime Audinot
 *
 */
public class RequirementContractValidator implements Validator {

	public static final RequirementContractValidator INSTANCE = new RequirementContractValidator();

	public static final String DIAGNOSTIC_SOURCE = "fr.irisa.atsyra.absystem.validation.requirement";

	public static final int STATIC_CONTRACT_BREACHED = 1;
	public static final int STATIC_CONTRACT_EVALUATION_FAILED = 2;
	public static final int STATIC_CONTRACT_MODEL_INITIALIZATION_FAILED = 3;

	@Override
	public boolean validate(AssetBasedSystem abs, DiagnosticChain diagnostics) {
		return validate(abs, diagnostics, null);
	}

	@Override
	public boolean validate(Set<AssetBasedSystem> absSet, DiagnosticChain diagnostics) {
		return validate(absSet, diagnostics, null);
	}

	@Override
	public boolean validate(AssetBasedSystem abs, DiagnosticChain diagnostics, Locale locale) {
		return validate(Set.of(abs), diagnostics, locale);
	}

	@Override
	public boolean validate(Set<AssetBasedSystem> absSet, DiagnosticChain diagnostics, Locale locale) {
		if (absSet.isEmpty()) {
			return true;
		} else {
			boolean result = true;
			AssetBasedSystem mainModel = absSet.iterator().next();
			EcoreUtil.resolveAll(mainModel.eResource().getResourceSet());
			try {
				if(!AssetBasedSystemK3Aspect.hasBeenInitialized(mainModel) ) { // may be already initialized by the handler
					AssetBasedSystemK3Aspect.initializeModel(mainModel, new BasicEList<>());
				}
			} catch (InvalidConfigurationException e) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
							STATIC_CONTRACT_MODEL_INITIALIZATION_FAILED,
							locale == null ? e.getMessage() : e.getLocalizedMessage(locale), null));
				}
				return false;
			}
			Set<GuardOccurence> staticContractsOccurences = getRequirementContractsOccurences(absSet);
			Set<Contract> evaluatedContracts = new HashSet<>();
			Set<Contract> failedContracts = new HashSet<>();
			for (GuardOccurence guardOccurence : staticContractsOccurences) {
				Contract instanciedContract = (Contract) guardOccurence.getGuard();
				evaluatedContracts.add(instanciedContract);
				try {
					boolean guardResult = GuardOccurenceK3Aspect.evaluateGuard(guardOccurence, mainModel);
					if (!guardResult) {
						result = false;
						failedContracts.add(instanciedContract);
						if (diagnostics != null) {
							int severity = 0;
							switch (instanciedContract.getSeverity()) {
							case ERROR:
								severity = Diagnostic.ERROR;
								break;
							case WARNING:
								severity = Diagnostic.WARNING;
								break;
							default:
								severity = Diagnostic.ERROR;
								break;
							}
							StringBuilder sb = new StringBuilder();
							sb.append(locale == null ? GuardOccurenceK3Aspect.toUserString(guardOccurence)
									: GuardOccurenceK3Aspect.toLocalizedUserString(guardOccurence, locale));
							if (guardOccurence.getGuard().getDescription() != null) {
								Optional<GuardLocale> localization = locale != null
										? ABSUtils.getLocalization(guardOccurence.getGuard(), locale.getLanguage())
										: Optional.empty();
								if (localization.isPresent()) {
									sb.append("\n" + localization.orElseThrow().getDescription());
								} else {
									sb.append("\n" + guardOccurence.getGuard().getDescription());
								}
							}

							EObject targetObject = mainModel;
							if (!guardOccurence.getGuardOccurenceArguments().isEmpty()
									&& guardOccurence.getGuardOccurenceArguments().get(0) instanceof AssetArgument) {
								targetObject = ((AssetArgument) guardOccurence.getGuardOccurenceArguments().get(0))
										.getAsset();
							}
							if (diagnostics != null)
								diagnostics
										.add(new BasicDiagnostic(severity, DIAGNOSTIC_SOURCE, STATIC_CONTRACT_BREACHED,
												sb.toString(), new Object[] { instanciedContract, targetObject }));
						} else {
							return false;
						}
					}
				} catch (AssetBasedSystemException | UndefinedReceiverException e) {
					result = false;
					failedContracts.add(instanciedContract);
					if (diagnostics != null) {
						StringBuilder sb = new StringBuilder();
						sb.append(locale == null ? GuardOccurenceK3Aspect.toUserString(guardOccurence)
								: GuardOccurenceK3Aspect.toLocalizedUserString(guardOccurence, locale));
						sb.append(' ');
						sb.append(locale == null ? e.getMessage() : e.getLocalizedMessage(locale));
						EObject targetObject = mainModel;
						if (!guardOccurence.getGuardOccurenceArguments().isEmpty()
								&& guardOccurence.getGuardOccurenceArguments().get(0) instanceof AssetArgument) {
							targetObject = ((AssetArgument) guardOccurence.getGuardOccurenceArguments().get(0))
									.getAsset();
						}

						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
								STATIC_CONTRACT_EVALUATION_FAILED, sb.toString(),
								new Object[] { instanciedContract, targetObject }));
					} else {
						return false;
					}
				}
			}
			if (diagnostics != null) {
				for (Contract contract : evaluatedContracts) {
					if (!failedContracts.contains(contract)) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.OK, DIAGNOSTIC_SOURCE, 0, "",
								new Object[] { contract }));
					}
				}
			}
			return result;
		}
	}

	public Set<GuardOccurence> getRequirementContractsOccurences(AssetBasedSystem abs) {
		EcoreUtil.resolveAll(abs.eResource().getResourceSet());
		return getRequirementContractsOccurences(Set.of(abs));
	}
	
	public Set<GuardOccurence> getRequirementContractsOccurences(Set<AssetBasedSystem> absSet) {
		List<Contract> processedContracts = new ArrayList<>();
		Set<GuardOccurence> guardOccurences = new HashSet<>();
		absSet.forEach(abs -> {
			abs.getDefinitionGroups().forEach(dg -> {
				guardOccurences
						.addAll(getRequirementContractsOccurences(dg, processedContracts).collect(Collectors.toSet()));
			});
		});
		return guardOccurences;
	}

	private Stream<GuardOccurence> getRequirementContractsOccurences(DefinitionGroup dg, List<Contract> processedContracts) {
		return dg.getDefinitions().stream().flatMap(def -> {
			if (def instanceof DefinitionGroup) {
				return getRequirementContractsOccurences((DefinitionGroup) def, processedContracts);
			} else if (def instanceof Requirement) {
				return getRequirementContractsOccurences((Requirement) def, processedContracts);
			}
			return null;
		});
	}

	private Stream<GuardOccurence> getRequirementContractsOccurences(Requirement req, List<Contract> processedContracts) {
		return req.getContracts().stream()
				.filter(contract -> !contract.isDynamic() && contract.getGuardExpression() != null
						&& !(contract.getGuardExpression() instanceof UndefinedConstant))
				.flatMap(contract -> {
					if (!processedContracts.contains(contract)) {
						processedContracts.add(contract);
						return GuardK3Aspect.generateAllPossibleGuardOccurence(contract).stream();
					}
					return null;
				});
	}
}
