/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Annotated;
import fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry;
import fr.irisa.atsyra.absystem.model.absystem.AnnotationKey;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.model.absystem.util.UnmodifiableEListCollector;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotated</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AnnotatedImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AnnotatedImpl#getAllAnnotations <em>All Annotations</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AnnotatedImpl extends MinimalEObjectImpl.Container implements Annotated {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<AnnotationEntry> annotations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotatedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ANNOTATED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AnnotationEntry> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectContainmentEList<AnnotationEntry>(AnnotationEntry.class, this,
					AbsystemPackage.ANNOTATED__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<AnnotationEntry> getAllAnnotations() {
		final List<AnnotationKey> keyList = getAnnotations().stream().map(AnnotationEntry::getKey)
				.collect(Collectors.toList());
		return ABSUtils
				.getHierarchy(eContainer()).filter(
						Annotated.class::isInstance)
				.map(Annotated.class::cast).findFirst().map(Annotated::getAllAnnotations)
				.map(parentAnnotations -> Stream
						.concat(parentAnnotations.stream()
								.filter(annotationEntry -> !keyList.contains(annotationEntry.getKey())),
								getAnnotations().stream())
						.collect(new UnmodifiableEListCollector<>(this,
								AbsystemPackage.eINSTANCE.getAnnotated_AllAnnotations())))
				.orElseGet(() -> new UnmodifiableEList<AnnotationEntry>(this,
						AbsystemPackage.eINSTANCE.getAnnotated_AllAnnotations(), getAnnotations().size(),
						getAnnotations().toArray()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.ANNOTATED__ANNOTATIONS:
			return ((InternalEList<?>) getAnnotations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ANNOTATED__ANNOTATIONS:
			return getAnnotations();
		case AbsystemPackage.ANNOTATED__ALL_ANNOTATIONS:
			return getAllAnnotations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ANNOTATED__ANNOTATIONS:
			getAnnotations().clear();
			getAnnotations().addAll((Collection<? extends AnnotationEntry>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ANNOTATED__ANNOTATIONS:
			getAnnotations().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ANNOTATED__ANNOTATIONS:
			return annotations != null && !annotations.isEmpty();
		case AbsystemPackage.ANNOTATED__ALL_ANNOTATIONS:
			return !getAllAnnotations().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AnnotatedImpl
