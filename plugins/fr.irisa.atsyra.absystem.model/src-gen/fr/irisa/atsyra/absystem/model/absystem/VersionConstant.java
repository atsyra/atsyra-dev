/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Version Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.VersionConstant#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getVersionConstant()
 * @model
 * @generated
 */
public interface VersionConstant extends ConstantExpression {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Version)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getVersionConstant_Value()
	 * @model dataType="fr.irisa.atsyra.absystem.model.absystem.EVersion"
	 * @generated
	 */
	Version getValue();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.VersionConstant#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Version value);

} // VersionConstant
