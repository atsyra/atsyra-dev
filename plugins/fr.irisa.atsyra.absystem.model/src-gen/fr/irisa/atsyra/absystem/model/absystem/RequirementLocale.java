/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement Locale</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getRef <em>Ref</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getTitle <em>Title</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getDescriptionFormat <em>Description Format</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirementLocale()
 * @model
 * @generated
 */
public interface RequirementLocale extends Annotated, ABSObjectLocale {
	/**
	 * Returns the value of the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref</em>' reference.
	 * @see #setRef(Requirement)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirementLocale_Ref()
	 * @model
	 * @generated
	 */
	Requirement getRef();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getRef <em>Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref</em>' reference.
	 * @see #getRef()
	 * @generated
	 */
	void setRef(Requirement value);

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirementLocale_Title()
	 * @model
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirementLocale_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Description Format</b></em>' attribute.
	 * The default value is <code>"plaintext"</code>.
	 * The literals are from the enumeration {@link fr.irisa.atsyra.absystem.model.absystem.TextFormat}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #setDescriptionFormat(TextFormat)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirementLocale_DescriptionFormat()
	 * @model default="plaintext"
	 * @generated
	 */
	TextFormat getDescriptionFormat();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getDescriptionFormat <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #getDescriptionFormat()
	 * @generated
	 */
	void setDescriptionFormat(TextFormat value);

} // RequirementLocale
