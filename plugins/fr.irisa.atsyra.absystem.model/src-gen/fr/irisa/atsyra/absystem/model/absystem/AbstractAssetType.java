/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Asset Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType#getAssetTypeAttributes <em>Asset Type Attributes</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType#getAssetTypeProperties <em>Asset Type Properties</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAbstractAssetType()
 * @model abstract="true"
 * @generated
 */
public interface AbstractAssetType extends Definition {
	/**
	 * Returns the value of the '<em><b>Asset Type Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Type Attributes</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAbstractAssetType_AssetTypeAttributes()
	 * @model containment="true" keys="name"
	 * @generated
	 */
	EList<AssetTypeAttribute> getAssetTypeAttributes();

	/**
	 * Returns the value of the '<em><b>Asset Type Properties</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Type Properties</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAbstractAssetType_AssetTypeProperties()
	 * @model containment="true" keys="name"
	 * @generated
	 */
	EList<AssetTypeReference> getAssetTypeProperties();

} // AbstractAssetType
