/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Type Aspect Locale</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAspectLocaleImpl#getBaseAssetType <em>Base Asset Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetTypeAspectLocaleImpl extends AbstractAssetTypeLocaleImpl implements AssetTypeAspectLocale {
	/**
	 * The cached value of the '{@link #getBaseAssetType() <em>Base Asset Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAssetType()
	 * @generated
	 * @ordered
	 */
	protected AssetType baseAssetType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetTypeAspectLocaleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ASSET_TYPE_ASPECT_LOCALE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetType getBaseAssetType() {
		if (baseAssetType != null && baseAssetType.eIsProxy()) {
			InternalEObject oldBaseAssetType = (InternalEObject) baseAssetType;
			baseAssetType = (AssetType) eResolveProxy(oldBaseAssetType);
			if (baseAssetType != oldBaseAssetType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AbsystemPackage.ASSET_TYPE_ASPECT_LOCALE__BASE_ASSET_TYPE, oldBaseAssetType,
							baseAssetType));
			}
		}
		return baseAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetType basicGetBaseAssetType() {
		return baseAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBaseAssetType(AssetType newBaseAssetType) {
		AssetType oldBaseAssetType = baseAssetType;
		baseAssetType = newBaseAssetType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AbsystemPackage.ASSET_TYPE_ASPECT_LOCALE__BASE_ASSET_TYPE, oldBaseAssetType, baseAssetType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE_ASPECT_LOCALE__BASE_ASSET_TYPE:
			if (resolve)
				return getBaseAssetType();
			return basicGetBaseAssetType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE_ASPECT_LOCALE__BASE_ASSET_TYPE:
			setBaseAssetType((AssetType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE_ASPECT_LOCALE__BASE_ASSET_TYPE:
			setBaseAssetType((AssetType) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE_ASPECT_LOCALE__BASE_ASSET_TYPE:
			return baseAssetType != null;
		}
		return super.eIsSet(featureID);
	}

} //AssetTypeAspectLocaleImpl
