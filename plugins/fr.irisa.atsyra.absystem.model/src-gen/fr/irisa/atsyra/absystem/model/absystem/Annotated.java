/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotated</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Annotated#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Annotated#getAllAnnotations <em>All Annotations</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAnnotated()
 * @model abstract="true"
 * @generated
 */
public interface Annotated extends EObject {
	/**
	 * Returns the value of the '<em><b>Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotations</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAnnotated_Annotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<AnnotationEntry> getAnnotations();

	/**
	 * Returns the value of the '<em><b>All Annotations</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Annotations</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAnnotated_AllAnnotations()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<AnnotationEntry> getAllAnnotations();

} // Annotated
