/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.util;

import fr.irisa.atsyra.absystem.model.absystem.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
 * @generated
 */
public class AbsystemSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AbsystemPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbsystemSwitch() {
		if (modelPackage == null) {
			modelPackage = AbsystemPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case AbsystemPackage.ASSET_TYPE: {
			AssetType assetType = (AssetType) theEObject;
			T result = caseAssetType(assetType);
			if (result == null)
				result = caseAnnotated(assetType);
			if (result == null)
				result = caseAbstractAssetType(assetType);
			if (result == null)
				result = caseDefinition(assetType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_TYPE_REFERENCE: {
			AssetTypeReference assetTypeReference = (AssetTypeReference) theEObject;
			T result = caseAssetTypeReference(assetTypeReference);
			if (result == null)
				result = caseAssetTypeFeature(assetTypeReference);
			if (result == null)
				result = caseMember(assetTypeReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_BASED_SYSTEM: {
			AssetBasedSystem assetBasedSystem = (AssetBasedSystem) theEObject;
			T result = caseAssetBasedSystem(assetBasedSystem);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET: {
			Asset asset = (Asset) theEObject;
			T result = caseAsset(asset);
			if (result == null)
				result = caseSymbol(asset);
			if (result == null)
				result = caseAssetGroupContent(asset);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_LINK: {
			AssetLink assetLink = (AssetLink) theEObject;
			T result = caseAssetLink(assetLink);
			if (result == null)
				result = caseAssetGroupContent(assetLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_TYPE_ATTRIBUTE: {
			AssetTypeAttribute assetTypeAttribute = (AssetTypeAttribute) theEObject;
			T result = caseAssetTypeAttribute(assetTypeAttribute);
			if (result == null)
				result = caseAssetTypeFeature(assetTypeAttribute);
			if (result == null)
				result = caseMember(assetTypeAttribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_TYPE_FEATURE: {
			AssetTypeFeature assetTypeFeature = (AssetTypeFeature) theEObject;
			T result = caseAssetTypeFeature(assetTypeFeature);
			if (result == null)
				result = caseMember(assetTypeFeature);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.PRIMITIVE_DATA_TYPE: {
			PrimitiveDataType primitiveDataType = (PrimitiveDataType) theEObject;
			T result = casePrimitiveDataType(primitiveDataType);
			if (result == null)
				result = caseAnnotated(primitiveDataType);
			if (result == null)
				result = caseDefinition(primitiveDataType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ENUM_DATA_TYPE: {
			EnumDataType enumDataType = (EnumDataType) theEObject;
			T result = caseEnumDataType(enumDataType);
			if (result == null)
				result = casePrimitiveDataType(enumDataType);
			if (result == null)
				result = caseAnnotated(enumDataType);
			if (result == null)
				result = caseDefinition(enumDataType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ENUM_LITERAL: {
			EnumLiteral enumLiteral = (EnumLiteral) theEObject;
			T result = caseEnumLiteral(enumLiteral);
			if (result == null)
				result = caseSymbol(enumLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE: {
			AssetAttributeValue assetAttributeValue = (AssetAttributeValue) theEObject;
			T result = caseAssetAttributeValue(assetAttributeValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.DEFINITION_GROUP: {
			DefinitionGroup definitionGroup = (DefinitionGroup) theEObject;
			T result = caseDefinitionGroup(definitionGroup);
			if (result == null)
				result = caseAnnotated(definitionGroup);
			if (result == null)
				result = caseDefinition(definitionGroup);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_GROUP: {
			AssetGroup assetGroup = (AssetGroup) theEObject;
			T result = caseAssetGroup(assetGroup);
			if (result == null)
				result = caseAnnotated(assetGroup);
			if (result == null)
				result = caseAssetGroupContent(assetGroup);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.IMPORT: {
			Import import_ = (Import) theEObject;
			T result = caseImport(import_);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.TAG: {
			Tag tag = (Tag) theEObject;
			T result = caseTag(tag);
			if (result == null)
				result = caseDefinition(tag);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ABSTRACT_ASSET_TYPE: {
			AbstractAssetType abstractAssetType = (AbstractAssetType) theEObject;
			T result = caseAbstractAssetType(abstractAssetType);
			if (result == null)
				result = caseDefinition(abstractAssetType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_TYPE_ASPECT: {
			AssetTypeAspect assetTypeAspect = (AssetTypeAspect) theEObject;
			T result = caseAssetTypeAspect(assetTypeAspect);
			if (result == null)
				result = caseAbstractAssetType(assetTypeAspect);
			if (result == null)
				result = caseDefinition(assetTypeAspect);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.GUARDED_ACTION: {
			GuardedAction guardedAction = (GuardedAction) theEObject;
			T result = caseGuardedAction(guardedAction);
			if (result == null)
				result = caseGuard(guardedAction);
			if (result == null)
				result = caseDefinition(guardedAction);
			if (result == null)
				result = caseAnnotated(guardedAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.GUARD_PARAMETER: {
			GuardParameter guardParameter = (GuardParameter) theEObject;
			T result = caseGuardParameter(guardParameter);
			if (result == null)
				result = caseSymbol(guardParameter);
			if (result == null)
				result = caseParameter(guardParameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.EXPRESSION: {
			Expression expression = (Expression) theEObject;
			T result = caseExpression(expression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.BINARY_EXPRESSION: {
			BinaryExpression binaryExpression = (BinaryExpression) theEObject;
			T result = caseBinaryExpression(binaryExpression);
			if (result == null)
				result = caseExpression(binaryExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.IMPLIES_EXPRESSION: {
			ImpliesExpression impliesExpression = (ImpliesExpression) theEObject;
			T result = caseImpliesExpression(impliesExpression);
			if (result == null)
				result = caseBinaryExpression(impliesExpression);
			if (result == null)
				result = caseExpression(impliesExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.OR_EXPRESSION: {
			OrExpression orExpression = (OrExpression) theEObject;
			T result = caseOrExpression(orExpression);
			if (result == null)
				result = caseBinaryExpression(orExpression);
			if (result == null)
				result = caseExpression(orExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.AND_EXPRESSION: {
			AndExpression andExpression = (AndExpression) theEObject;
			T result = caseAndExpression(andExpression);
			if (result == null)
				result = caseBinaryExpression(andExpression);
			if (result == null)
				result = caseExpression(andExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.NOT_EXPRESSION: {
			NotExpression notExpression = (NotExpression) theEObject;
			T result = caseNotExpression(notExpression);
			if (result == null)
				result = caseExpression(notExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.EQUALITY_COMPARISON_EXPRESSION: {
			EqualityComparisonExpression equalityComparisonExpression = (EqualityComparisonExpression) theEObject;
			T result = caseEqualityComparisonExpression(equalityComparisonExpression);
			if (result == null)
				result = caseBinaryExpression(equalityComparisonExpression);
			if (result == null)
				result = caseExpression(equalityComparisonExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.INEQUALITY_COMPARISON_EXPRESSION: {
			InequalityComparisonExpression inequalityComparisonExpression = (InequalityComparisonExpression) theEObject;
			T result = caseInequalityComparisonExpression(inequalityComparisonExpression);
			if (result == null)
				result = caseBinaryExpression(inequalityComparisonExpression);
			if (result == null)
				result = caseExpression(inequalityComparisonExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ACTION: {
			Action action = (Action) theEObject;
			T result = caseAction(action);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.CONSTANT_EXPRESSION: {
			ConstantExpression constantExpression = (ConstantExpression) theEObject;
			T result = caseConstantExpression(constantExpression);
			if (result == null)
				result = caseExpression(constantExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.STRING_CONSTANT: {
			StringConstant stringConstant = (StringConstant) theEObject;
			T result = caseStringConstant(stringConstant);
			if (result == null)
				result = caseConstantExpression(stringConstant);
			if (result == null)
				result = caseExpression(stringConstant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.INT_CONSTANT: {
			IntConstant intConstant = (IntConstant) theEObject;
			T result = caseIntConstant(intConstant);
			if (result == null)
				result = caseConstantExpression(intConstant);
			if (result == null)
				result = caseExpression(intConstant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.BOOLEAN_CONSTANT: {
			BooleanConstant booleanConstant = (BooleanConstant) theEObject;
			T result = caseBooleanConstant(booleanConstant);
			if (result == null)
				result = caseConstantExpression(booleanConstant);
			if (result == null)
				result = caseExpression(booleanConstant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.VERSION_CONSTANT: {
			VersionConstant versionConstant = (VersionConstant) theEObject;
			T result = caseVersionConstant(versionConstant);
			if (result == null)
				result = caseConstantExpression(versionConstant);
			if (result == null)
				result = caseExpression(versionConstant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.MEMBER_SELECTION: {
			MemberSelection memberSelection = (MemberSelection) theEObject;
			T result = caseMemberSelection(memberSelection);
			if (result == null)
				result = caseExpression(memberSelection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.MEMBER: {
			Member member = (Member) theEObject;
			T result = caseMember(member);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.SYMBOL_REF: {
			SymbolRef symbolRef = (SymbolRef) theEObject;
			T result = caseSymbolRef(symbolRef);
			if (result == null)
				result = caseExpression(symbolRef);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.SYMBOL: {
			Symbol symbol = (Symbol) theEObject;
			T result = caseSymbol(symbol);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.STATIC_METHOD: {
			StaticMethod staticMethod = (StaticMethod) theEObject;
			T result = caseStaticMethod(staticMethod);
			if (result == null)
				result = caseMember(staticMethod);
			if (result == null)
				result = caseDefinition(staticMethod);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.LAMBDA_PARAMETER: {
			LambdaParameter lambdaParameter = (LambdaParameter) theEObject;
			T result = caseLambdaParameter(lambdaParameter);
			if (result == null)
				result = caseSymbol(lambdaParameter);
			if (result == null)
				result = caseParameter(lambdaParameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.LAMBDA_EXPRESSION: {
			LambdaExpression lambdaExpression = (LambdaExpression) theEObject;
			T result = caseLambdaExpression(lambdaExpression);
			if (result == null)
				result = caseExpression(lambdaExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.LAMBDA_ACTION: {
			LambdaAction lambdaAction = (LambdaAction) theEObject;
			T result = caseLambdaAction(lambdaAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.PARAMETER: {
			Parameter parameter = (Parameter) theEObject;
			T result = caseParameter(parameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ANNOTATION_ENTRY: {
			AnnotationEntry annotationEntry = (AnnotationEntry) theEObject;
			T result = caseAnnotationEntry(annotationEntry);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ANNOTATION_KEY: {
			AnnotationKey annotationKey = (AnnotationKey) theEObject;
			T result = caseAnnotationKey(annotationKey);
			if (result == null)
				result = caseDefinition(annotationKey);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.GOAL: {
			Goal goal = (Goal) theEObject;
			T result = caseGoal(goal);
			if (result == null)
				result = caseAnnotated(goal);
			if (result == null)
				result = caseAssetGroupContent(goal);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.CONTRACT: {
			Contract contract = (Contract) theEObject;
			T result = caseContract(contract);
			if (result == null)
				result = caseGuard(contract);
			if (result == null)
				result = caseDefinition(contract);
			if (result == null)
				result = caseAnnotated(contract);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.GUARD: {
			Guard guard = (Guard) theEObject;
			T result = caseGuard(guard);
			if (result == null)
				result = caseAnnotated(guard);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ENUM_CONSTANT: {
			EnumConstant enumConstant = (EnumConstant) theEObject;
			T result = caseEnumConstant(enumConstant);
			if (result == null)
				result = caseConstantExpression(enumConstant);
			if (result == null)
				result = caseExpression(enumConstant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.UNDEFINED_CONSTANT: {
			UndefinedConstant undefinedConstant = (UndefinedConstant) theEObject;
			T result = caseUndefinedConstant(undefinedConstant);
			if (result == null)
				result = caseConstantExpression(undefinedConstant);
			if (result == null)
				result = caseExpression(undefinedConstant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.COLLECTION: {
			Collection collection = (Collection) theEObject;
			T result = caseCollection(collection);
			if (result == null)
				result = caseExpression(collection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.DEFINITION: {
			Definition definition = (Definition) theEObject;
			T result = caseDefinition(definition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_GROUP_CONTENT: {
			AssetGroupContent assetGroupContent = (AssetGroupContent) theEObject;
			T result = caseAssetGroupContent(assetGroupContent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.LOCALE_GROUP: {
			LocaleGroup localeGroup = (LocaleGroup) theEObject;
			T result = caseLocaleGroup(localeGroup);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ABS_OBJECT_LOCALE: {
			ABSObjectLocale absObjectLocale = (ABSObjectLocale) theEObject;
			T result = caseABSObjectLocale(absObjectLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.DEFINITION_GROUP_LOCALE: {
			DefinitionGroupLocale definitionGroupLocale = (DefinitionGroupLocale) theEObject;
			T result = caseDefinitionGroupLocale(definitionGroupLocale);
			if (result == null)
				result = caseAnnotated(definitionGroupLocale);
			if (result == null)
				result = caseABSObjectLocale(definitionGroupLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.PRIMITIVE_DATA_TYPE_LOCALE: {
			PrimitiveDataTypeLocale primitiveDataTypeLocale = (PrimitiveDataTypeLocale) theEObject;
			T result = casePrimitiveDataTypeLocale(primitiveDataTypeLocale);
			if (result == null)
				result = caseABSObjectLocale(primitiveDataTypeLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ENUM_LITERAL_LOCALE: {
			EnumLiteralLocale enumLiteralLocale = (EnumLiteralLocale) theEObject;
			T result = caseEnumLiteralLocale(enumLiteralLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ABSTRACT_ASSET_TYPE_LOCALE: {
			AbstractAssetTypeLocale abstractAssetTypeLocale = (AbstractAssetTypeLocale) theEObject;
			T result = caseAbstractAssetTypeLocale(abstractAssetTypeLocale);
			if (result == null)
				result = caseABSObjectLocale(abstractAssetTypeLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_TYPE_LOCALE: {
			AssetTypeLocale assetTypeLocale = (AssetTypeLocale) theEObject;
			T result = caseAssetTypeLocale(assetTypeLocale);
			if (result == null)
				result = caseAbstractAssetTypeLocale(assetTypeLocale);
			if (result == null)
				result = caseABSObjectLocale(assetTypeLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_TYPE_ASPECT_LOCALE: {
			AssetTypeAspectLocale assetTypeAspectLocale = (AssetTypeAspectLocale) theEObject;
			T result = caseAssetTypeAspectLocale(assetTypeAspectLocale);
			if (result == null)
				result = caseAbstractAssetTypeLocale(assetTypeAspectLocale);
			if (result == null)
				result = caseABSObjectLocale(assetTypeAspectLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ASSET_TYPE_FEATURE_LOCALE: {
			AssetTypeFeatureLocale assetTypeFeatureLocale = (AssetTypeFeatureLocale) theEObject;
			T result = caseAssetTypeFeatureLocale(assetTypeFeatureLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.GUARD_LOCALE: {
			GuardLocale guardLocale = (GuardLocale) theEObject;
			T result = caseGuardLocale(guardLocale);
			if (result == null)
				result = caseAnnotated(guardLocale);
			if (result == null)
				result = caseABSObjectLocale(guardLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.ANNOTATED: {
			Annotated annotated = (Annotated) theEObject;
			T result = caseAnnotated(annotated);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.REQUIREMENT: {
			Requirement requirement = (Requirement) theEObject;
			T result = caseRequirement(requirement);
			if (result == null)
				result = caseAnnotated(requirement);
			if (result == null)
				result = caseDefinition(requirement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AbsystemPackage.REQUIREMENT_LOCALE: {
			RequirementLocale requirementLocale = (RequirementLocale) theEObject;
			T result = caseRequirementLocale(requirementLocale);
			if (result == null)
				result = caseAnnotated(requirementLocale);
			if (result == null)
				result = caseABSObjectLocale(requirementLocale);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetType(AssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Type Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Type Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetTypeReference(AssetTypeReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Based System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Based System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetBasedSystem(AssetBasedSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsset(Asset object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetLink(AssetLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Type Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Type Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetTypeAttribute(AssetTypeAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Type Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Type Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetTypeFeature(AssetTypeFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Primitive Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitive Data Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrimitiveDataType(PrimitiveDataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Data Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumDataType(EnumDataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumLiteral(EnumLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Attribute Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Attribute Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetAttributeValue(AssetAttributeValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Definition Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Definition Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefinitionGroup(DefinitionGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetGroup(AssetGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImport(Import object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTag(Tag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractAssetType(AbstractAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Type Aspect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Type Aspect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetTypeAspect(AssetTypeAspect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Guarded Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Guarded Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGuardedAction(GuardedAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Guard Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Guard Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGuardParameter(GuardParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpression(Expression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryExpression(BinaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Implies Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Implies Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImpliesExpression(ImpliesExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrExpression(OrExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>And Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAndExpression(AndExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotExpression(NotExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equality Comparison Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equality Comparison Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEqualityComparisonExpression(EqualityComparisonExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inequality Comparison Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inequality Comparison Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInequalityComparisonExpression(InequalityComparisonExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantExpression(ConstantExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringConstant(StringConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntConstant(IntConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanConstant(BooleanConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Version Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Version Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVersionConstant(VersionConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Member Selection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Member Selection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMemberSelection(MemberSelection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Member</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Member</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMember(Member object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolRef(SymbolRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbol(Symbol object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Static Method</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Static Method</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStaticMethod(StaticMethod object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lambda Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lambda Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLambdaParameter(LambdaParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lambda Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lambda Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLambdaExpression(LambdaExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lambda Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lambda Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLambdaAction(LambdaAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotationEntry(AnnotationEntry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation Key</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation Key</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotationKey(AnnotationKey object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoal(Goal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contract</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contract</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContract(Contract object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Guard</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Guard</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGuard(Guard object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumConstant(EnumConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Undefined Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Undefined Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUndefinedConstant(UndefinedConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Collection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Collection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCollection(Collection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefinition(Definition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Group Content</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Group Content</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetGroupContent(AssetGroupContent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Locale Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Locale Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocaleGroup(LocaleGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ABS Object Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ABS Object Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseABSObjectLocale(ABSObjectLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Definition Group Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Definition Group Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefinitionGroupLocale(DefinitionGroupLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Primitive Data Type Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitive Data Type Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrimitiveDataTypeLocale(PrimitiveDataTypeLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Literal Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Literal Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumLiteralLocale(EnumLiteralLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Asset Type Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Asset Type Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractAssetTypeLocale(AbstractAssetTypeLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Type Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Type Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetTypeLocale(AssetTypeLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Type Aspect Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Type Aspect Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetTypeAspectLocale(AssetTypeAspectLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Type Feature Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Type Feature Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetTypeFeatureLocale(AssetTypeFeatureLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Guard Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Guard Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGuardLocale(GuardLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotated(Annotated object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequirement(Requirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requirement Locale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requirement Locale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequirementLocale(RequirementLocale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //AbsystemSwitch
