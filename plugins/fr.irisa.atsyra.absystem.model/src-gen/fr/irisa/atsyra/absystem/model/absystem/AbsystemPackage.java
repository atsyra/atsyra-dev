/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory
 * @model kind="package"
 * @generated
 */
public interface AbsystemPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "absystem";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/atsyra//absystem";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "absystem";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AbsystemPackage eINSTANCE = fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.Definition <em>Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.Definition
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getDefinition()
	 * @generated
	 */
	int DEFINITION = 50;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeImpl <em>Abstract Asset Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAbstractAssetType()
	 * @generated
	 */
	int ABSTRACT_ASSET_TYPE = 15;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl <em>Asset Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetType()
	 * @generated
	 */
	int ASSET_TYPE = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.MemberImpl <em>Member</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.MemberImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getMember()
	 * @generated
	 */
	int MEMBER = 34;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeFeatureImpl <em>Asset Type Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeFeatureImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeFeature()
	 * @generated
	 */
	int ASSET_TYPE_FEATURE = 6;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeReferenceImpl <em>Asset Type Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeReferenceImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeReference()
	 * @generated
	 */
	int ASSET_TYPE_REFERENCE = 1;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl <em>Asset Based System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetBasedSystem()
	 * @generated
	 */
	int ASSET_BASED_SYSTEM = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.SymbolImpl <em>Symbol</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.SymbolImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getSymbol()
	 * @generated
	 */
	int SYMBOL = 36;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl <em>Asset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAsset()
	 * @generated
	 */
	int ASSET = 3;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent <em>Asset Group Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetGroupContent()
	 * @generated
	 */
	int ASSET_GROUP_CONTENT = 51;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetLinkImpl <em>Asset Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetLinkImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetLink()
	 * @generated
	 */
	int ASSET_LINK = 4;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAttributeImpl <em>Asset Type Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAttributeImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeAttribute()
	 * @generated
	 */
	int ASSET_TYPE_ATTRIBUTE = 5;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.PrimitiveDataTypeImpl <em>Primitive Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.PrimitiveDataTypeImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getPrimitiveDataType()
	 * @generated
	 */
	int PRIMITIVE_DATA_TYPE = 7;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EnumDataTypeImpl <em>Enum Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EnumDataTypeImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEnumDataType()
	 * @generated
	 */
	int ENUM_DATA_TYPE = 8;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EnumLiteralImpl <em>Enum Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EnumLiteralImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEnumLiteral()
	 * @generated
	 */
	int ENUM_LITERAL = 9;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetAttributeValueImpl <em>Asset Attribute Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetAttributeValueImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetAttributeValue()
	 * @generated
	 */
	int ASSET_ATTRIBUTE_VALUE = 10;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl <em>Definition Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getDefinitionGroup()
	 * @generated
	 */
	int DEFINITION_GROUP = 11;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl <em>Asset Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetGroup()
	 * @generated
	 */
	int ASSET_GROUP = 12;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ImportImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 13;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.TagImpl <em>Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.TagImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getTag()
	 * @generated
	 */
	int TAG = 14;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAspectImpl <em>Asset Type Aspect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAspectImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeAspect()
	 * @generated
	 */
	int ASSET_TYPE_ASPECT = 16;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AnnotatedImpl <em>Annotated</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AnnotatedImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAnnotated()
	 * @generated
	 */
	int ANNOTATED = 62;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED__ANNOTATIONS = 0;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED__ALL_ANNOTATIONS = 1;

	/**
	 * The number of structural features of the '<em>Annotated</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Annotated</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__ID = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Asset Type Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__ASSET_TYPE_ATTRIBUTES = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Asset Type Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__ASSET_TYPE_PROPERTIES = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__NAME = ANNOTATED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__LEVEL = ANNOTATED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__TAGS = ANNOTATED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Extends</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__EXTENDS = ANNOTATED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__DESCRIPTION = ANNOTATED_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__DESCRIPTION_FORMAT = ANNOTATED_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__ABSTRACT = ANNOTATED_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>All Tags</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE__ALL_TAGS = ANNOTATED_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>Asset Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 11;

	/**
	 * The number of operations of the '<em>Asset Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER__NAME = 0;

	/**
	 * The number of structural features of the '<em>Member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE__NAME = MEMBER__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE__MULTIPLICITY = MEMBER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE__HAS_DEFAULT = MEMBER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Asset Type Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE_FEATURE_COUNT = MEMBER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Asset Type Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE_OPERATION_COUNT = MEMBER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_REFERENCE__NAME = ASSET_TYPE_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_REFERENCE__MULTIPLICITY = ASSET_TYPE_FEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Has Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_REFERENCE__HAS_DEFAULT = ASSET_TYPE_FEATURE__HAS_DEFAULT;

	/**
	 * The feature id for the '<em><b>Property Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_REFERENCE__PROPERTY_TYPE = ASSET_TYPE_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Container</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_REFERENCE__IS_CONTAINER = ASSET_TYPE_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Opposite Type Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_REFERENCE__OPPOSITE_TYPE_REFERENCE = ASSET_TYPE_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Asset Type Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_REFERENCE_FEATURE_COUNT = ASSET_TYPE_FEATURE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Asset Type Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_REFERENCE_OPERATION_COUNT = ASSET_TYPE_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Definition Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM__DEFINITION_GROUPS = 0;

	/**
	 * The feature id for the '<em><b>Asset Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM__ASSET_GROUPS = 1;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM__IMPORTS = 2;

	/**
	 * The feature id for the '<em><b>Possible Guarded Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM__POSSIBLE_GUARDED_ACTIONS = 3;

	/**
	 * The feature id for the '<em><b>Applied Actions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM__APPLIED_ACTIONS = 4;

	/**
	 * The feature id for the '<em><b>All Assets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM__ALL_ASSETS = 5;

	/**
	 * The feature id for the '<em><b>All Static Asset Links</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM__ALL_STATIC_ASSET_LINKS = 6;

	/**
	 * The feature id for the '<em><b>Localizations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM__LOCALIZATIONS = 7;

	/**
	 * The number of structural features of the '<em>Asset Based System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Asset Based System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_BASED_SYSTEM_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL__NAME = 0;

	/**
	 * The number of structural features of the '<em>Symbol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Symbol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__NAME = SYMBOL__NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__ID = SYMBOL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Asset Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__ASSET_TYPE = SYMBOL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Asset Attribute Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__ASSET_ATTRIBUTE_VALUES = SYMBOL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Features Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__FEATURES_MAP = SYMBOL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__DESCRIPTION = SYMBOL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__DESCRIPTION_FORMAT = SYMBOL_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_FEATURE_COUNT = SYMBOL_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_OPERATION_COUNT = SYMBOL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP_CONTENT__ID = 0;

	/**
	 * The number of structural features of the '<em>Asset Group Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP_CONTENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Asset Group Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP_CONTENT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_LINK__ID = ASSET_GROUP_CONTENT__ID;

	/**
	 * The feature id for the '<em><b>Reference Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_LINK__REFERENCE_TYPE = ASSET_GROUP_CONTENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_LINK__SOURCE_ASSET = ASSET_GROUP_CONTENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_LINK__TARGET_ASSET = ASSET_GROUP_CONTENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Opposite Reference Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_LINK__OPPOSITE_REFERENCE_TYPE = ASSET_GROUP_CONTENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Asset Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_LINK_FEATURE_COUNT = ASSET_GROUP_CONTENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Asset Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_LINK_OPERATION_COUNT = ASSET_GROUP_CONTENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ATTRIBUTE__NAME = ASSET_TYPE_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ATTRIBUTE__MULTIPLICITY = ASSET_TYPE_FEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Has Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ATTRIBUTE__HAS_DEFAULT = ASSET_TYPE_FEATURE__HAS_DEFAULT;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ATTRIBUTE__ATTRIBUTE_TYPE = ASSET_TYPE_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Default Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ATTRIBUTE__DEFAULT_VALUES = ASSET_TYPE_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Asset Type Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ATTRIBUTE_FEATURE_COUNT = ASSET_TYPE_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Asset Type Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ATTRIBUTE_OPERATION_COUNT = ASSET_TYPE_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE__ID = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE__NAME = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Primitive Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Primitive Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_DATA_TYPE__ANNOTATIONS = PRIMITIVE_DATA_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_DATA_TYPE__ALL_ANNOTATIONS = PRIMITIVE_DATA_TYPE__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_DATA_TYPE__ID = PRIMITIVE_DATA_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_DATA_TYPE__NAME = PRIMITIVE_DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Enum Literal</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_DATA_TYPE__ENUM_LITERAL = PRIMITIVE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enum Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_DATA_TYPE_FEATURE_COUNT = PRIMITIVE_DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Enum Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_DATA_TYPE_OPERATION_COUNT = PRIMITIVE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_LITERAL__NAME = SYMBOL__NAME;

	/**
	 * The number of structural features of the '<em>Enum Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_LITERAL_FEATURE_COUNT = SYMBOL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Enum Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_LITERAL_OPERATION_COUNT = SYMBOL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ATTRIBUTE_VALUE__ATTRIBUTE_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ATTRIBUTE_VALUE__VALUES = 1;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ATTRIBUTE_VALUE__COLLECTION = 2;

	/**
	 * The number of structural features of the '<em>Asset Attribute Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ATTRIBUTE_VALUE_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Only Collections Have Many Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ATTRIBUTE_VALUE___ONLY_COLLECTIONS_HAVE_MANY_VALUES__DIAGNOSTICCHAIN_MAP = 0;

	/**
	 * The number of operations of the '<em>Asset Attribute Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ATTRIBUTE_VALUE_OPERATION_COUNT = 1;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__ID = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__NAME = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Definitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__DEFINITIONS = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Static Methods</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__STATIC_METHODS = ANNOTATED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Primitive Data Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__PRIMITIVE_DATA_TYPES = ANNOTATED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Tag Definitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__TAG_DEFINITIONS = ANNOTATED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Annotation Keys</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__ANNOTATION_KEYS = ANNOTATED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Asset Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__ASSET_TYPES = ANNOTATED_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Guarded Actions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__GUARDED_ACTIONS = ANNOTATED_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Contracts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__CONTRACTS = ANNOTATED_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP__TAGS = ANNOTATED_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>Definition Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 11;

	/**
	 * The number of operations of the '<em>Definition Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP__ID = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP__NAME = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Assets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP__ASSETS = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Asset Links</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP__ASSET_LINKS = ANNOTATED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Goals</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP__GOALS = ANNOTATED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP__ELEMENTS = ANNOTATED_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Asset Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Asset Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_GROUP_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Imported Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__IMPORTED_NAMESPACE = 0;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__IMPORT_URI = 1;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__ID = 0;

	/**
	 * The number of structural features of the '<em>Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG__ID = DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG__NAME = DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_FEATURE_COUNT = DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_OPERATION_COUNT = DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ASSET_TYPE__ID = DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Asset Type Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES = DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Asset Type Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES = DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Abstract Asset Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ASSET_TYPE_FEATURE_COUNT = DEFINITION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Abstract Asset Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ASSET_TYPE_OPERATION_COUNT = DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT__ID = ABSTRACT_ASSET_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Asset Type Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT__ASSET_TYPE_ATTRIBUTES = ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Asset Type Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT__ASSET_TYPE_PROPERTIES = ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Base Asset Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT__BASE_ASSET_TYPE = ABSTRACT_ASSET_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Asset Type Aspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT_FEATURE_COUNT = ABSTRACT_ASSET_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Asset Type Aspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT_OPERATION_COUNT = ABSTRACT_ASSET_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardImpl <em>Guard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GuardImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGuard()
	 * @generated
	 */
	int GUARD = 46;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Guard Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__GUARD_EXPRESSION = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Guard Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__GUARD_PARAMETERS = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__NAME = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__DESCRIPTION = ANNOTATED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__DESCRIPTION_FORMAT = ANNOTATED_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Guard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Guard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardedActionImpl <em>Guarded Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GuardedActionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGuardedAction()
	 * @generated
	 */
	int GUARDED_ACTION = 17;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION__ANNOTATIONS = GUARD__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION__ALL_ANNOTATIONS = GUARD__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Guard Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION__GUARD_EXPRESSION = GUARD__GUARD_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Guard Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION__GUARD_PARAMETERS = GUARD__GUARD_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION__NAME = GUARD__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION__DESCRIPTION = GUARD__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION__DESCRIPTION_FORMAT = GUARD__DESCRIPTION_FORMAT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION__ID = GUARD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Guard Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION__GUARD_ACTIONS = GUARD_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Guarded Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION_FEATURE_COUNT = GUARD_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Guarded Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARDED_ACTION_OPERATION_COUNT = GUARD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardParameterImpl <em>Guard Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GuardParameterImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGuardParameter()
	 * @generated
	 */
	int GUARD_PARAMETER = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_PARAMETER__NAME = SYMBOL__NAME;

	/**
	 * The feature id for the '<em><b>Parameter Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_PARAMETER__PARAMETER_TYPE = SYMBOL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Guard Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_PARAMETER_FEATURE_COUNT = SYMBOL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Guard Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_PARAMETER_OPERATION_COUNT = SYMBOL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 19;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.BinaryExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getBinaryExpression()
	 * @generated
	 */
	int BINARY_EXPRESSION = 20;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__LHS = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__RHS = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ImpliesExpressionImpl <em>Implies Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ImpliesExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getImpliesExpression()
	 * @generated
	 */
	int IMPLIES_EXPRESSION = 21;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_EXPRESSION__LHS = BINARY_EXPRESSION__LHS;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_EXPRESSION__RHS = BINARY_EXPRESSION__RHS;

	/**
	 * The number of structural features of the '<em>Implies Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Implies Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_EXPRESSION_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.OrExpressionImpl <em>Or Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.OrExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getOrExpression()
	 * @generated
	 */
	int OR_EXPRESSION = 22;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_EXPRESSION__LHS = BINARY_EXPRESSION__LHS;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_EXPRESSION__RHS = BINARY_EXPRESSION__RHS;

	/**
	 * The number of structural features of the '<em>Or Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Or Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_EXPRESSION_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AndExpressionImpl <em>And Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AndExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAndExpression()
	 * @generated
	 */
	int AND_EXPRESSION = 23;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_EXPRESSION__LHS = BINARY_EXPRESSION__LHS;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_EXPRESSION__RHS = BINARY_EXPRESSION__RHS;

	/**
	 * The number of structural features of the '<em>And Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>And Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_EXPRESSION_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.NotExpressionImpl <em>Not Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.NotExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getNotExpression()
	 * @generated
	 */
	int NOT_EXPRESSION = 24;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Not Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Not Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EqualityComparisonExpressionImpl <em>Equality Comparison Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EqualityComparisonExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEqualityComparisonExpression()
	 * @generated
	 */
	int EQUALITY_COMPARISON_EXPRESSION = 25;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALITY_COMPARISON_EXPRESSION__LHS = BINARY_EXPRESSION__LHS;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALITY_COMPARISON_EXPRESSION__RHS = BINARY_EXPRESSION__RHS;

	/**
	 * The feature id for the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALITY_COMPARISON_EXPRESSION__OP = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Equality Comparison Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALITY_COMPARISON_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Equality Comparison Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALITY_COMPARISON_EXPRESSION_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.InequalityComparisonExpressionImpl <em>Inequality Comparison Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.InequalityComparisonExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getInequalityComparisonExpression()
	 * @generated
	 */
	int INEQUALITY_COMPARISON_EXPRESSION = 26;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEQUALITY_COMPARISON_EXPRESSION__LHS = BINARY_EXPRESSION__LHS;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEQUALITY_COMPARISON_EXPRESSION__RHS = BINARY_EXPRESSION__RHS;

	/**
	 * The feature id for the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEQUALITY_COMPARISON_EXPRESSION__OP = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Inequality Comparison Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEQUALITY_COMPARISON_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Inequality Comparison Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEQUALITY_COMPARISON_EXPRESSION_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ActionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 27;

	/**
	 * The feature id for the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ARGS = 1;

	/**
	 * The feature id for the '<em><b>Action Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ACTION_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Lambda Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__LAMBDA_ACTION = 3;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ConstantExpressionImpl <em>Constant Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ConstantExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getConstantExpression()
	 * @generated
	 */
	int CONSTANT_EXPRESSION = 28;

	/**
	 * The number of structural features of the '<em>Constant Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Constant Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.StringConstantImpl <em>String Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.StringConstantImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getStringConstant()
	 * @generated
	 */
	int STRING_CONSTANT = 29;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CONSTANT__VALUE = CONSTANT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CONSTANT_FEATURE_COUNT = CONSTANT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>String Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CONSTANT_OPERATION_COUNT = CONSTANT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.IntConstantImpl <em>Int Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.IntConstantImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getIntConstant()
	 * @generated
	 */
	int INT_CONSTANT = 30;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_CONSTANT__VALUE = CONSTANT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_CONSTANT_FEATURE_COUNT = CONSTANT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Int Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_CONSTANT_OPERATION_COUNT = CONSTANT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.BooleanConstantImpl <em>Boolean Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.BooleanConstantImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getBooleanConstant()
	 * @generated
	 */
	int BOOLEAN_CONSTANT = 31;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_CONSTANT__VALUE = CONSTANT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_CONSTANT_FEATURE_COUNT = CONSTANT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Boolean Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_CONSTANT_OPERATION_COUNT = CONSTANT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.VersionConstantImpl <em>Version Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.VersionConstantImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getVersionConstant()
	 * @generated
	 */
	int VERSION_CONSTANT = 32;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_CONSTANT__VALUE = CONSTANT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Version Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_CONSTANT_FEATURE_COUNT = CONSTANT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Version Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_CONSTANT_OPERATION_COUNT = CONSTANT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.MemberSelectionImpl <em>Member Selection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.MemberSelectionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getMemberSelection()
	 * @generated
	 */
	int MEMBER_SELECTION = 33;

	/**
	 * The feature id for the '<em><b>Receiver</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_SELECTION__RECEIVER = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Member</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_SELECTION__MEMBER = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Method Invocation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_SELECTION__METHOD_INVOCATION = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_SELECTION__ARGS = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Member Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_SELECTION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Member Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_SELECTION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.SymbolRefImpl <em>Symbol Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.SymbolRefImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getSymbolRef()
	 * @generated
	 */
	int SYMBOL_REF = 35;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_REF__SYMBOL = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Symbol Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_REF_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Symbol Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_REF_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.StaticMethodImpl <em>Static Method</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.StaticMethodImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getStaticMethod()
	 * @generated
	 */
	int STATIC_METHOD = 37;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_METHOD__NAME = MEMBER__NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_METHOD__ID = MEMBER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Static Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_METHOD_FEATURE_COUNT = MEMBER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Static Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_METHOD_OPERATION_COUNT = MEMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaParameterImpl <em>Lambda Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.LambdaParameterImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getLambdaParameter()
	 * @generated
	 */
	int LAMBDA_PARAMETER = 38;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_PARAMETER__NAME = SYMBOL__NAME;

	/**
	 * The feature id for the '<em><b>Parameter Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_PARAMETER__PARAMETER_TYPE = SYMBOL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Lambda Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_PARAMETER_FEATURE_COUNT = SYMBOL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Lambda Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_PARAMETER_OPERATION_COUNT = SYMBOL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaExpressionImpl <em>Lambda Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.LambdaExpressionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getLambdaExpression()
	 * @generated
	 */
	int LAMBDA_EXPRESSION = 39;

	/**
	 * The feature id for the '<em><b>Lambda Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_EXPRESSION__LAMBDA_PARAMETER = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_EXPRESSION__BODY = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Lambda Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Lambda Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaActionImpl <em>Lambda Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.LambdaActionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getLambdaAction()
	 * @generated
	 */
	int LAMBDA_ACTION = 40;

	/**
	 * The feature id for the '<em><b>Lambda Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_ACTION__LAMBDA_PARAMETER = 0;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_ACTION__ACTIONS = 1;

	/**
	 * The number of structural features of the '<em>Lambda Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_ACTION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Lambda Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMBDA_ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ParameterImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 41;

	/**
	 * The feature id for the '<em><b>Parameter Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__PARAMETER_TYPE = 0;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AnnotationEntryImpl <em>Annotation Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AnnotationEntryImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAnnotationEntry()
	 * @generated
	 */
	int ANNOTATION_ENTRY = 42;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Annotation Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Annotation Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AnnotationKeyImpl <em>Annotation Key</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AnnotationKeyImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAnnotationKey()
	 * @generated
	 */
	int ANNOTATION_KEY = 43;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_KEY__ID = DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_KEY__NAME = DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Annotation Key</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_KEY_FEATURE_COUNT = DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Annotation Key</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_KEY_OPERATION_COUNT = DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GoalImpl <em>Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GoalImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGoal()
	 * @generated
	 */
	int GOAL = 44;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__ID = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Precondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__PRECONDITION = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Postcondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__POSTCONDITION = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__NAME = ANNOTATED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__DESCRIPTION = ANNOTATED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__DESCRIPTION_FORMAT = ANNOTATED_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ContractImpl <em>Contract</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ContractImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getContract()
	 * @generated
	 */
	int CONTRACT = 45;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__ANNOTATIONS = GUARD__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__ALL_ANNOTATIONS = GUARD__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Guard Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__GUARD_EXPRESSION = GUARD__GUARD_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Guard Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__GUARD_PARAMETERS = GUARD__GUARD_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__NAME = GUARD__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__DESCRIPTION = GUARD__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__DESCRIPTION_FORMAT = GUARD__DESCRIPTION_FORMAT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__ID = GUARD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__DYNAMIC = GUARD_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__SEVERITY = GUARD_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Contract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_FEATURE_COUNT = GUARD_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Contract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_OPERATION_COUNT = GUARD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EnumConstantImpl <em>Enum Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EnumConstantImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEnumConstant()
	 * @generated
	 */
	int ENUM_CONSTANT = 47;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_CONSTANT__VALUE = CONSTANT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enum Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_CONSTANT_FEATURE_COUNT = CONSTANT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Enum Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_CONSTANT_OPERATION_COUNT = CONSTANT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.UndefinedConstantImpl <em>Undefined Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.UndefinedConstantImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getUndefinedConstant()
	 * @generated
	 */
	int UNDEFINED_CONSTANT = 48;

	/**
	 * The number of structural features of the '<em>Undefined Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDEFINED_CONSTANT_FEATURE_COUNT = CONSTANT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Undefined Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDEFINED_CONSTANT_OPERATION_COUNT = CONSTANT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.CollectionImpl <em>Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.CollectionImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getCollection()
	 * @generated
	 */
	int COLLECTION = 49;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__ELEMENTS = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.LocaleGroupImpl <em>Locale Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.LocaleGroupImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getLocaleGroup()
	 * @generated
	 */
	int LOCALE_GROUP = 52;

	/**
	 * The feature id for the '<em><b>Locale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALE_GROUP__LOCALE = 0;

	/**
	 * The feature id for the '<em><b>Object Locales</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALE_GROUP__OBJECT_LOCALES = 1;

	/**
	 * The number of structural features of the '<em>Locale Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALE_GROUP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Locale Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale <em>ABS Object Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getABSObjectLocale()
	 * @generated
	 */
	int ABS_OBJECT_LOCALE = 53;

	/**
	 * The number of structural features of the '<em>ABS Object Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABS_OBJECT_LOCALE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>ABS Object Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABS_OBJECT_LOCALE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupLocaleImpl <em>Definition Group Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupLocaleImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getDefinitionGroupLocale()
	 * @generated
	 */
	int DEFINITION_GROUP_LOCALE = 54;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP_LOCALE__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP_LOCALE__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP_LOCALE__REF = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Definition Locales</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP_LOCALE__DEFINITION_LOCALES = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Definition Group Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP_LOCALE_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Definition Group Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_GROUP_LOCALE_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.PrimitiveDataTypeLocaleImpl <em>Primitive Data Type Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.PrimitiveDataTypeLocaleImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getPrimitiveDataTypeLocale()
	 * @generated
	 */
	int PRIMITIVE_DATA_TYPE_LOCALE = 55;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE_LOCALE__REF = ABS_OBJECT_LOCALE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE_LOCALE__NAME = ABS_OBJECT_LOCALE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Literals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE_LOCALE__LITERALS = ABS_OBJECT_LOCALE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Primitive Data Type Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE_LOCALE_FEATURE_COUNT = ABS_OBJECT_LOCALE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Primitive Data Type Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_DATA_TYPE_LOCALE_OPERATION_COUNT = ABS_OBJECT_LOCALE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EnumLiteralLocaleImpl <em>Enum Literal Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EnumLiteralLocaleImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEnumLiteralLocale()
	 * @generated
	 */
	int ENUM_LITERAL_LOCALE = 56;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_LITERAL_LOCALE__REF = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_LITERAL_LOCALE__NAME = 1;

	/**
	 * The number of structural features of the '<em>Enum Literal Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_LITERAL_LOCALE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Enum Literal Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_LITERAL_LOCALE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeLocaleImpl <em>Abstract Asset Type Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeLocaleImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAbstractAssetTypeLocale()
	 * @generated
	 */
	int ABSTRACT_ASSET_TYPE_LOCALE = 57;

	/**
	 * The feature id for the '<em><b>Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ASSET_TYPE_LOCALE__FEATURES = ABS_OBJECT_LOCALE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Abstract Asset Type Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ASSET_TYPE_LOCALE_FEATURE_COUNT = ABS_OBJECT_LOCALE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Abstract Asset Type Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ASSET_TYPE_LOCALE_OPERATION_COUNT = ABS_OBJECT_LOCALE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeLocaleImpl <em>Asset Type Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeLocaleImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeLocale()
	 * @generated
	 */
	int ASSET_TYPE_LOCALE = 58;

	/**
	 * The feature id for the '<em><b>Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_LOCALE__FEATURES = ABSTRACT_ASSET_TYPE_LOCALE__FEATURES;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_LOCALE__REF = ABSTRACT_ASSET_TYPE_LOCALE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_LOCALE__NAME = ABSTRACT_ASSET_TYPE_LOCALE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_LOCALE__DESCRIPTION = ABSTRACT_ASSET_TYPE_LOCALE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_LOCALE__DESCRIPTION_FORMAT = ABSTRACT_ASSET_TYPE_LOCALE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Asset Type Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_LOCALE_FEATURE_COUNT = ABSTRACT_ASSET_TYPE_LOCALE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Asset Type Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_LOCALE_OPERATION_COUNT = ABSTRACT_ASSET_TYPE_LOCALE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAspectLocaleImpl <em>Asset Type Aspect Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAspectLocaleImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeAspectLocale()
	 * @generated
	 */
	int ASSET_TYPE_ASPECT_LOCALE = 59;

	/**
	 * The feature id for the '<em><b>Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT_LOCALE__FEATURES = ABSTRACT_ASSET_TYPE_LOCALE__FEATURES;

	/**
	 * The feature id for the '<em><b>Base Asset Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT_LOCALE__BASE_ASSET_TYPE = ABSTRACT_ASSET_TYPE_LOCALE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Asset Type Aspect Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT_LOCALE_FEATURE_COUNT = ABSTRACT_ASSET_TYPE_LOCALE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Asset Type Aspect Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_ASPECT_LOCALE_OPERATION_COUNT = ABSTRACT_ASSET_TYPE_LOCALE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeFeatureLocaleImpl <em>Asset Type Feature Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeFeatureLocaleImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeFeatureLocale()
	 * @generated
	 */
	int ASSET_TYPE_FEATURE_LOCALE = 60;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE_LOCALE__REF = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE_LOCALE__NAME = 1;

	/**
	 * The number of structural features of the '<em>Asset Type Feature Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE_LOCALE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Asset Type Feature Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_TYPE_FEATURE_LOCALE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardLocaleImpl <em>Guard Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GuardLocaleImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGuardLocale()
	 * @generated
	 */
	int GUARD_LOCALE = 61;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_LOCALE__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_LOCALE__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_LOCALE__REF = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_LOCALE__NAME = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_LOCALE__DESCRIPTION = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_LOCALE__DESCRIPTION_FORMAT = ANNOTATED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Guard Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_LOCALE_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Guard Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_LOCALE_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl <em>Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getRequirement()
	 * @generated
	 */
	int REQUIREMENT = 63;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__ID = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__NAME = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__TITLE = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__DESCRIPTION = ANNOTATED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__DESCRIPTION_FORMAT = ANNOTATED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Contracts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__CONTRACTS = ANNOTATED_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementLocaleImpl <em>Requirement Locale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.RequirementLocaleImpl
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getRequirementLocale()
	 * @generated
	 */
	int REQUIREMENT_LOCALE = 64;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_LOCALE__ANNOTATIONS = ANNOTATED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>All Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_LOCALE__ALL_ANNOTATIONS = ANNOTATED__ALL_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_LOCALE__REF = ANNOTATED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_LOCALE__TITLE = ANNOTATED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_LOCALE__DESCRIPTION = ANNOTATED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Description Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_LOCALE__DESCRIPTION_FORMAT = ANNOTATED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Requirement Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_LOCALE_FEATURE_COUNT = ANNOTATED_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Requirement Locale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_LOCALE_OPERATION_COUNT = ANNOTATED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.ActionEnum <em>Action Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.ActionEnum
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getActionEnum()
	 * @generated
	 */
	int ACTION_ENUM = 65;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.Multiplicity <em>Multiplicity</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.Multiplicity
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getMultiplicity()
	 * @generated
	 */
	int MULTIPLICITY = 66;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.Severity <em>Severity</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.Severity
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getSeverity()
	 * @generated
	 */
	int SEVERITY = 67;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.model.absystem.TextFormat <em>Text Format</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getTextFormat()
	 * @generated
	 */
	int TEXT_FORMAT = 68;

	/**
	 * The meta object id for the '<em>EVersion</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.model.absystem.Version
	 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEVersion()
	 * @generated
	 */
	int EVERSION = 69;

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType <em>Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType
	 * @generated
	 */
	EClass getAssetType();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType#getName()
	 * @see #getAssetType()
	 * @generated
	 */
	EAttribute getAssetType_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType#getLevel()
	 * @see #getAssetType()
	 * @generated
	 */
	EAttribute getAssetType_Level();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getTags <em>Tags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Tags</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType#getTags()
	 * @see #getAssetType()
	 * @generated
	 */
	EReference getAssetType_Tags();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getExtends <em>Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extends</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType#getExtends()
	 * @see #getAssetType()
	 * @generated
	 */
	EReference getAssetType_Extends();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType#getDescription()
	 * @see #getAssetType()
	 * @generated
	 */
	EAttribute getAssetType_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getDescriptionFormat <em>Description Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Format</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType#getDescriptionFormat()
	 * @see #getAssetType()
	 * @generated
	 */
	EAttribute getAssetType_DescriptionFormat();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType#isAbstract()
	 * @see #getAssetType()
	 * @generated
	 */
	EAttribute getAssetType_Abstract();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getAllTags <em>All Tags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Tags</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType#getAllTags()
	 * @see #getAssetType()
	 * @generated
	 */
	EReference getAssetType_AllTags();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference <em>Asset Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Type Reference</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference
	 * @generated
	 */
	EClass getAssetTypeReference();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#getPropertyType <em>Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Property Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#getPropertyType()
	 * @see #getAssetTypeReference()
	 * @generated
	 */
	EReference getAssetTypeReference_PropertyType();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#isIsContainer <em>Is Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Container</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#isIsContainer()
	 * @see #getAssetTypeReference()
	 * @generated
	 */
	EAttribute getAssetTypeReference_IsContainer();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#getOppositeTypeReference <em>Opposite Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite Type Reference</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#getOppositeTypeReference()
	 * @see #getAssetTypeReference()
	 * @generated
	 */
	EReference getAssetTypeReference_OppositeTypeReference();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem <em>Asset Based System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Based System</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
	 * @generated
	 */
	EClass getAssetBasedSystem();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getDefinitionGroups <em>Definition Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Definition Groups</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getDefinitionGroups()
	 * @see #getAssetBasedSystem()
	 * @generated
	 */
	EReference getAssetBasedSystem_DefinitionGroups();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAssetGroups <em>Asset Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Asset Groups</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAssetGroups()
	 * @see #getAssetBasedSystem()
	 * @generated
	 */
	EReference getAssetBasedSystem_AssetGroups();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getImports()
	 * @see #getAssetBasedSystem()
	 * @generated
	 */
	EReference getAssetBasedSystem_Imports();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getPossibleGuardedActions <em>Possible Guarded Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Possible Guarded Actions</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getPossibleGuardedActions()
	 * @see #getAssetBasedSystem()
	 * @generated
	 */
	EReference getAssetBasedSystem_PossibleGuardedActions();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAppliedActions <em>Applied Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Applied Actions</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAppliedActions()
	 * @see #getAssetBasedSystem()
	 * @generated
	 */
	EReference getAssetBasedSystem_AppliedActions();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAllAssets <em>All Assets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Assets</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAllAssets()
	 * @see #getAssetBasedSystem()
	 * @generated
	 */
	EReference getAssetBasedSystem_AllAssets();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAllStaticAssetLinks <em>All Static Asset Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Static Asset Links</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAllStaticAssetLinks()
	 * @see #getAssetBasedSystem()
	 * @generated
	 */
	EReference getAssetBasedSystem_AllStaticAssetLinks();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getLocalizations <em>Localizations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Localizations</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getLocalizations()
	 * @see #getAssetBasedSystem()
	 * @generated
	 */
	EReference getAssetBasedSystem_Localizations();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Asset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Asset
	 * @generated
	 */
	EClass getAsset();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getAssetType <em>Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Asset Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Asset#getAssetType()
	 * @see #getAsset()
	 * @generated
	 */
	EReference getAsset_AssetType();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getAssetAttributeValues <em>Asset Attribute Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Asset Attribute Values</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Asset#getAssetAttributeValues()
	 * @see #getAsset()
	 * @generated
	 */
	EReference getAsset_AssetAttributeValues();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getFeaturesMap <em>Features Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Features Map</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Asset#getFeaturesMap()
	 * @see #getAsset()
	 * @generated
	 */
	EReference getAsset_FeaturesMap();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Asset#getDescription()
	 * @see #getAsset()
	 * @generated
	 */
	EAttribute getAsset_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getDescriptionFormat <em>Description Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Format</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Asset#getDescriptionFormat()
	 * @see #getAsset()
	 * @generated
	 */
	EAttribute getAsset_DescriptionFormat();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink <em>Asset Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Link</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetLink
	 * @generated
	 */
	EClass getAssetLink();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getReferenceType <em>Reference Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetLink#getReferenceType()
	 * @see #getAssetLink()
	 * @generated
	 */
	EReference getAssetLink_ReferenceType();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getSourceAsset <em>Source Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source Asset</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetLink#getSourceAsset()
	 * @see #getAssetLink()
	 * @generated
	 */
	EReference getAssetLink_SourceAsset();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getTargetAsset <em>Target Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Asset</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetLink#getTargetAsset()
	 * @see #getAssetLink()
	 * @generated
	 */
	EReference getAssetLink_TargetAsset();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getOppositeReferenceType <em>Opposite Reference Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite Reference Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetLink#getOppositeReferenceType()
	 * @see #getAssetLink()
	 * @generated
	 */
	EReference getAssetLink_OppositeReferenceType();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute <em>Asset Type Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Type Attribute</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute
	 * @generated
	 */
	EClass getAssetTypeAttribute();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute#getAttributeType()
	 * @see #getAssetTypeAttribute()
	 * @generated
	 */
	EReference getAssetTypeAttribute_AttributeType();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute#getDefaultValues <em>Default Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Default Values</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute#getDefaultValues()
	 * @see #getAssetTypeAttribute()
	 * @generated
	 */
	EReference getAssetTypeAttribute_DefaultValues();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature <em>Asset Type Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Type Feature</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature
	 * @generated
	 */
	EClass getAssetTypeFeature();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiplicity</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature#getMultiplicity()
	 * @see #getAssetTypeFeature()
	 * @generated
	 */
	EAttribute getAssetTypeFeature_Multiplicity();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature#isHasDefault <em>Has Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Default</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature#isHasDefault()
	 * @see #getAssetTypeFeature()
	 * @generated
	 */
	EAttribute getAssetTypeFeature_HasDefault();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType <em>Primitive Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Data Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType
	 * @generated
	 */
	EClass getPrimitiveDataType();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType#getName()
	 * @see #getPrimitiveDataType()
	 * @generated
	 */
	EAttribute getPrimitiveDataType_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.EnumDataType <em>Enum Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Data Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumDataType
	 * @generated
	 */
	EClass getEnumDataType();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.EnumDataType#getEnumLiteral <em>Enum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Enum Literal</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumDataType#getEnumLiteral()
	 * @see #getEnumDataType()
	 * @generated
	 */
	EReference getEnumDataType_EnumLiteral();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.EnumLiteral <em>Enum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Literal</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumLiteral
	 * @generated
	 */
	EClass getEnumLiteral();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue <em>Asset Attribute Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Attribute Value</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue
	 * @generated
	 */
	EClass getAssetAttributeValue();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#getAttributeType()
	 * @see #getAssetAttributeValue()
	 * @generated
	 */
	EReference getAssetAttributeValue_AttributeType();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Values</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#getValues()
	 * @see #getAssetAttributeValue()
	 * @generated
	 */
	EReference getAssetAttributeValue_Values();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#isCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collection</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#isCollection()
	 * @see #getAssetAttributeValue()
	 * @generated
	 */
	EAttribute getAssetAttributeValue_Collection();

	/**
	 * Returns the meta object for the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#onlyCollectionsHaveManyValues(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Only Collections Have Many Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Only Collections Have Many Values</em>' operation.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#onlyCollectionsHaveManyValues(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAssetAttributeValue__OnlyCollectionsHaveManyValues__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup <em>Definition Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Definition Group</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup
	 * @generated
	 */
	EClass getDefinitionGroup();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getAssetTypes <em>Asset Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Asset Types</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getAssetTypes()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EReference getDefinitionGroup_AssetTypes();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getPrimitiveDataTypes <em>Primitive Data Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Primitive Data Types</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getPrimitiveDataTypes()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EReference getDefinitionGroup_PrimitiveDataTypes();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getTagDefinitions <em>Tag Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Tag Definitions</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getTagDefinitions()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EReference getDefinitionGroup_TagDefinitions();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getName()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EAttribute getDefinitionGroup_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getDefinitions <em>Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Definitions</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getDefinitions()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EReference getDefinitionGroup_Definitions();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getGuardedActions <em>Guarded Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Guarded Actions</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getGuardedActions()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EReference getDefinitionGroup_GuardedActions();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getStaticMethods <em>Static Methods</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Static Methods</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getStaticMethods()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EReference getDefinitionGroup_StaticMethods();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getAnnotationKeys <em>Annotation Keys</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Annotation Keys</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getAnnotationKeys()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EReference getDefinitionGroup_AnnotationKeys();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getContracts <em>Contracts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contracts</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getContracts()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EReference getDefinitionGroup_Contracts();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getTags <em>Tags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Tags</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getTags()
	 * @see #getDefinitionGroup()
	 * @generated
	 */
	EReference getDefinitionGroup_Tags();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroup <em>Asset Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Group</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroup
	 * @generated
	 */
	EClass getAssetGroup();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getAssets <em>Assets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assets</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getAssets()
	 * @see #getAssetGroup()
	 * @generated
	 */
	EReference getAssetGroup_Assets();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getAssetLinks <em>Asset Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Asset Links</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getAssetLinks()
	 * @see #getAssetGroup()
	 * @generated
	 */
	EReference getAssetGroup_AssetLinks();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getGoals <em>Goals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Goals</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getGoals()
	 * @see #getAssetGroup()
	 * @generated
	 */
	EReference getAssetGroup_Goals();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getElements()
	 * @see #getAssetGroup()
	 * @generated
	 */
	EReference getAssetGroup_Elements();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroup#getName()
	 * @see #getAssetGroup()
	 * @generated
	 */
	EAttribute getAssetGroup_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Import#getImportedNamespace <em>Imported Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Imported Namespace</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Import#getImportedNamespace()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_ImportedNamespace();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Import#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Import#getImportURI()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_ImportURI();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Tag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tag</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Tag
	 * @generated
	 */
	EClass getTag();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Tag#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Tag#getName()
	 * @see #getTag()
	 * @generated
	 */
	EAttribute getTag_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType <em>Abstract Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Asset Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType
	 * @generated
	 */
	EClass getAbstractAssetType();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType#getAssetTypeAttributes <em>Asset Type Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Asset Type Attributes</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType#getAssetTypeAttributes()
	 * @see #getAbstractAssetType()
	 * @generated
	 */
	EReference getAbstractAssetType_AssetTypeAttributes();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType#getAssetTypeProperties <em>Asset Type Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Asset Type Properties</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType#getAssetTypeProperties()
	 * @see #getAbstractAssetType()
	 * @generated
	 */
	EReference getAbstractAssetType_AssetTypeProperties();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect <em>Asset Type Aspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Type Aspect</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect
	 * @generated
	 */
	EClass getAssetTypeAspect();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect#getBaseAssetType <em>Base Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Asset Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect#getBaseAssetType()
	 * @see #getAssetTypeAspect()
	 * @generated
	 */
	EReference getAssetTypeAspect_BaseAssetType();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.GuardedAction <em>Guarded Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guarded Action</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardedAction
	 * @generated
	 */
	EClass getGuardedAction();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.GuardedAction#getGuardActions <em>Guard Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Guard Actions</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardedAction#getGuardActions()
	 * @see #getGuardedAction()
	 * @generated
	 */
	EReference getGuardedAction_GuardActions();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.GuardParameter <em>Guard Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guard Parameter</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardParameter
	 * @generated
	 */
	EClass getGuardParameter();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.BinaryExpression
	 * @generated
	 */
	EClass getBinaryExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.BinaryExpression#getLhs <em>Lhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lhs</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.BinaryExpression#getLhs()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_Lhs();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.BinaryExpression#getRhs <em>Rhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rhs</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.BinaryExpression#getRhs()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_Rhs();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression <em>Implies Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Implies Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression
	 * @generated
	 */
	EClass getImpliesExpression();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.OrExpression <em>Or Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.OrExpression
	 * @generated
	 */
	EClass getOrExpression();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AndExpression <em>And Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AndExpression
	 * @generated
	 */
	EClass getAndExpression();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.NotExpression <em>Not Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.NotExpression
	 * @generated
	 */
	EClass getNotExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.NotExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.NotExpression#getExpression()
	 * @see #getNotExpression()
	 * @generated
	 */
	EReference getNotExpression_Expression();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression <em>Equality Comparison Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equality Comparison Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression
	 * @generated
	 */
	EClass getEqualityComparisonExpression();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression#getOp <em>Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression#getOp()
	 * @see #getEqualityComparisonExpression()
	 * @generated
	 */
	EAttribute getEqualityComparisonExpression_Op();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression <em>Inequality Comparison Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inequality Comparison Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression
	 * @generated
	 */
	EClass getInequalityComparisonExpression();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression#getOp <em>Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression#getOp()
	 * @see #getInequalityComparisonExpression()
	 * @generated
	 */
	EAttribute getInequalityComparisonExpression_Op();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.Action#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Action#getTarget()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Target();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.Action#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Action#getArgs()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Args();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Action#getActionType <em>Action Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Action#getActionType()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_ActionType();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.Action#getLambdaAction <em>Lambda Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lambda Action</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Action#getLambdaAction()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_LambdaAction();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.ConstantExpression <em>Constant Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.ConstantExpression
	 * @generated
	 */
	EClass getConstantExpression();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.StringConstant <em>String Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Constant</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.StringConstant
	 * @generated
	 */
	EClass getStringConstant();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.StringConstant#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.StringConstant#getValue()
	 * @see #getStringConstant()
	 * @generated
	 */
	EAttribute getStringConstant_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.IntConstant <em>Int Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Constant</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.IntConstant
	 * @generated
	 */
	EClass getIntConstant();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.IntConstant#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.IntConstant#getValue()
	 * @see #getIntConstant()
	 * @generated
	 */
	EAttribute getIntConstant_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.BooleanConstant <em>Boolean Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Constant</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.BooleanConstant
	 * @generated
	 */
	EClass getBooleanConstant();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.BooleanConstant#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.BooleanConstant#getValue()
	 * @see #getBooleanConstant()
	 * @generated
	 */
	EAttribute getBooleanConstant_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.VersionConstant <em>Version Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Version Constant</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.VersionConstant
	 * @generated
	 */
	EClass getVersionConstant();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.VersionConstant#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.VersionConstant#getValue()
	 * @see #getVersionConstant()
	 * @generated
	 */
	EAttribute getVersionConstant_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection <em>Member Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Member Selection</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.MemberSelection
	 * @generated
	 */
	EClass getMemberSelection();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getReceiver <em>Receiver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Receiver</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getReceiver()
	 * @see #getMemberSelection()
	 * @generated
	 */
	EReference getMemberSelection_Receiver();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getMember <em>Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Member</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getMember()
	 * @see #getMemberSelection()
	 * @generated
	 */
	EReference getMemberSelection_Member();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#isMethodInvocation <em>Method Invocation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Method Invocation</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.MemberSelection#isMethodInvocation()
	 * @see #getMemberSelection()
	 * @generated
	 */
	EAttribute getMemberSelection_MethodInvocation();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getArgs()
	 * @see #getMemberSelection()
	 * @generated
	 */
	EReference getMemberSelection_Args();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Member <em>Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Member</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Member
	 * @generated
	 */
	EClass getMember();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Member#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Member#getName()
	 * @see #getMember()
	 * @generated
	 */
	EAttribute getMember_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.SymbolRef <em>Symbol Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.SymbolRef
	 * @generated
	 */
	EClass getSymbolRef();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.SymbolRef#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.SymbolRef#getSymbol()
	 * @see #getSymbolRef()
	 * @generated
	 */
	EReference getSymbolRef_Symbol();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Symbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Symbol
	 * @generated
	 */
	EClass getSymbol();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Symbol#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Symbol#getName()
	 * @see #getSymbol()
	 * @generated
	 */
	EAttribute getSymbol_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.StaticMethod <em>Static Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Static Method</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.StaticMethod
	 * @generated
	 */
	EClass getStaticMethod();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaParameter <em>Lambda Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lambda Parameter</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaParameter
	 * @generated
	 */
	EClass getLambdaParameter();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaExpression <em>Lambda Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lambda Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaExpression
	 * @generated
	 */
	EClass getLambdaExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaExpression#getLambdaParameter <em>Lambda Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lambda Parameter</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaExpression#getLambdaParameter()
	 * @see #getLambdaExpression()
	 * @generated
	 */
	EReference getLambdaExpression_LambdaParameter();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaExpression#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaExpression#getBody()
	 * @see #getLambdaExpression()
	 * @generated
	 */
	EReference getLambdaExpression_Body();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaAction <em>Lambda Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lambda Action</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaAction
	 * @generated
	 */
	EClass getLambdaAction();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaAction#getLambdaParameter <em>Lambda Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lambda Parameter</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaAction#getLambdaParameter()
	 * @see #getLambdaAction()
	 * @generated
	 */
	EReference getLambdaAction_LambdaParameter();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaAction#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaAction#getActions()
	 * @see #getLambdaAction()
	 * @generated
	 */
	EReference getLambdaAction_Actions();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.Parameter#getParameterType <em>Parameter Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Parameter#getParameterType()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_ParameterType();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry <em>Annotation Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Entry</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry
	 * @generated
	 */
	EClass getAnnotationEntry();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry#getValue()
	 * @see #getAnnotationEntry()
	 * @generated
	 */
	EAttribute getAnnotationEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry#getKey()
	 * @see #getAnnotationEntry()
	 * @generated
	 */
	EReference getAnnotationEntry_Key();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AnnotationKey <em>Annotation Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Key</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AnnotationKey
	 * @generated
	 */
	EClass getAnnotationKey();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AnnotationKey#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AnnotationKey#getName()
	 * @see #getAnnotationKey()
	 * @generated
	 */
	EAttribute getAnnotationKey_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Goal <em>Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Goal
	 * @generated
	 */
	EClass getGoal();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getPrecondition <em>Precondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Precondition</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Goal#getPrecondition()
	 * @see #getGoal()
	 * @generated
	 */
	EReference getGoal_Precondition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getPostcondition <em>Postcondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Postcondition</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Goal#getPostcondition()
	 * @see #getGoal()
	 * @generated
	 */
	EReference getGoal_Postcondition();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Goal#getName()
	 * @see #getGoal()
	 * @generated
	 */
	EAttribute getGoal_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Goal#getDescription()
	 * @see #getGoal()
	 * @generated
	 */
	EAttribute getGoal_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getDescriptionFormat <em>Description Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Format</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Goal#getDescriptionFormat()
	 * @see #getGoal()
	 * @generated
	 */
	EAttribute getGoal_DescriptionFormat();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Contract <em>Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contract</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Contract
	 * @generated
	 */
	EClass getContract();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Contract#isDynamic <em>Dynamic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dynamic</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Contract#isDynamic()
	 * @see #getContract()
	 * @generated
	 */
	EAttribute getContract_Dynamic();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Contract#getSeverity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Severity</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Contract#getSeverity()
	 * @see #getContract()
	 * @generated
	 */
	EAttribute getContract_Severity();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Guard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guard</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Guard
	 * @generated
	 */
	EClass getGuard();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.atsyra.absystem.model.absystem.Guard#getGuardExpression <em>Guard Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guard Expression</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Guard#getGuardExpression()
	 * @see #getGuard()
	 * @generated
	 */
	EReference getGuard_GuardExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.Guard#getGuardParameters <em>Guard Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Guard Parameters</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Guard#getGuardParameters()
	 * @see #getGuard()
	 * @generated
	 */
	EReference getGuard_GuardParameters();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Guard#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Guard#getName()
	 * @see #getGuard()
	 * @generated
	 */
	EAttribute getGuard_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Guard#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Guard#getDescription()
	 * @see #getGuard()
	 * @generated
	 */
	EAttribute getGuard_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Guard#getDescriptionFormat <em>Description Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Format</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Guard#getDescriptionFormat()
	 * @see #getGuard()
	 * @generated
	 */
	EAttribute getGuard_DescriptionFormat();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.EnumConstant <em>Enum Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Constant</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumConstant
	 * @generated
	 */
	EClass getEnumConstant();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.EnumConstant#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumConstant#getValue()
	 * @see #getEnumConstant()
	 * @generated
	 */
	EReference getEnumConstant_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant <em>Undefined Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Undefined Constant</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant
	 * @generated
	 */
	EClass getUndefinedConstant();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Collection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collection</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Collection
	 * @generated
	 */
	EClass getCollection();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.Collection#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Collection#getElements()
	 * @see #getCollection()
	 * @generated
	 */
	EReference getCollection_Elements();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Definition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Definition</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Definition
	 * @generated
	 */
	EClass getDefinition();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Definition#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Definition#getId()
	 * @see #getDefinition()
	 * @generated
	 */
	EAttribute getDefinition_Id();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent <em>Asset Group Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Group Content</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent
	 * @generated
	 */
	EClass getAssetGroupContent();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent#getId()
	 * @see #getAssetGroupContent()
	 * @generated
	 */
	EAttribute getAssetGroupContent_Id();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.LocaleGroup <em>Locale Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Locale Group</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LocaleGroup
	 * @generated
	 */
	EClass getLocaleGroup();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.LocaleGroup#getLocale <em>Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LocaleGroup#getLocale()
	 * @see #getLocaleGroup()
	 * @generated
	 */
	EAttribute getLocaleGroup_Locale();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.LocaleGroup#getObjectLocales <em>Object Locales</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Locales</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LocaleGroup#getObjectLocales()
	 * @see #getLocaleGroup()
	 * @generated
	 */
	EReference getLocaleGroup_ObjectLocales();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale <em>ABS Object Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ABS Object Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale
	 * @generated
	 */
	EClass getABSObjectLocale();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale <em>Definition Group Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Definition Group Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale
	 * @generated
	 */
	EClass getDefinitionGroupLocale();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale#getRef()
	 * @see #getDefinitionGroupLocale()
	 * @generated
	 */
	EReference getDefinitionGroupLocale_Ref();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale#getDefinitionLocales <em>Definition Locales</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Definition Locales</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale#getDefinitionLocales()
	 * @see #getDefinitionGroupLocale()
	 * @generated
	 */
	EReference getDefinitionGroupLocale_DefinitionLocales();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale <em>Primitive Data Type Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Data Type Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale
	 * @generated
	 */
	EClass getPrimitiveDataTypeLocale();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale#getRef()
	 * @see #getPrimitiveDataTypeLocale()
	 * @generated
	 */
	EReference getPrimitiveDataTypeLocale_Ref();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale#getName()
	 * @see #getPrimitiveDataTypeLocale()
	 * @generated
	 */
	EAttribute getPrimitiveDataTypeLocale_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale#getLiterals <em>Literals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Literals</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale#getLiterals()
	 * @see #getPrimitiveDataTypeLocale()
	 * @generated
	 */
	EReference getPrimitiveDataTypeLocale_Literals();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale <em>Enum Literal Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Literal Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale
	 * @generated
	 */
	EClass getEnumLiteralLocale();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale#getRef()
	 * @see #getEnumLiteralLocale()
	 * @generated
	 */
	EReference getEnumLiteralLocale_Ref();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale#getName()
	 * @see #getEnumLiteralLocale()
	 * @generated
	 */
	EAttribute getEnumLiteralLocale_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetTypeLocale <em>Abstract Asset Type Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Asset Type Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbstractAssetTypeLocale
	 * @generated
	 */
	EClass getAbstractAssetTypeLocale();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetTypeLocale#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Features</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbstractAssetTypeLocale#getFeatures()
	 * @see #getAbstractAssetTypeLocale()
	 * @generated
	 */
	EReference getAbstractAssetTypeLocale_Features();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale <em>Asset Type Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Type Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale
	 * @generated
	 */
	EClass getAssetTypeLocale();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale#getRef()
	 * @see #getAssetTypeLocale()
	 * @generated
	 */
	EReference getAssetTypeLocale_Ref();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale#getName()
	 * @see #getAssetTypeLocale()
	 * @generated
	 */
	EAttribute getAssetTypeLocale_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale#getDescription()
	 * @see #getAssetTypeLocale()
	 * @generated
	 */
	EAttribute getAssetTypeLocale_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale#getDescriptionFormat <em>Description Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Format</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale#getDescriptionFormat()
	 * @see #getAssetTypeLocale()
	 * @generated
	 */
	EAttribute getAssetTypeLocale_DescriptionFormat();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale <em>Asset Type Aspect Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Type Aspect Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale
	 * @generated
	 */
	EClass getAssetTypeAspectLocale();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale#getBaseAssetType <em>Base Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Asset Type</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale#getBaseAssetType()
	 * @see #getAssetTypeAspectLocale()
	 * @generated
	 */
	EReference getAssetTypeAspectLocale_BaseAssetType();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale <em>Asset Type Feature Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Type Feature Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale
	 * @generated
	 */
	EClass getAssetTypeFeatureLocale();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale#getRef()
	 * @see #getAssetTypeFeatureLocale()
	 * @generated
	 */
	EReference getAssetTypeFeatureLocale_Ref();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale#getName()
	 * @see #getAssetTypeFeatureLocale()
	 * @generated
	 */
	EAttribute getAssetTypeFeatureLocale_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.GuardLocale <em>Guard Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guard Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardLocale
	 * @generated
	 */
	EClass getGuardLocale();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.GuardLocale#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardLocale#getRef()
	 * @see #getGuardLocale()
	 * @generated
	 */
	EReference getGuardLocale_Ref();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.GuardLocale#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardLocale#getName()
	 * @see #getGuardLocale()
	 * @generated
	 */
	EAttribute getGuardLocale_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.GuardLocale#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardLocale#getDescription()
	 * @see #getGuardLocale()
	 * @generated
	 */
	EAttribute getGuardLocale_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.GuardLocale#getDescriptionFormat <em>Description Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Format</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardLocale#getDescriptionFormat()
	 * @see #getGuardLocale()
	 * @generated
	 */
	EAttribute getGuardLocale_DescriptionFormat();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Annotated <em>Annotated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotated</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Annotated
	 * @generated
	 */
	EClass getAnnotated();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.model.absystem.Annotated#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotations</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Annotated#getAnnotations()
	 * @see #getAnnotated()
	 * @generated
	 */
	EReference getAnnotated_Annotations();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.Annotated#getAllAnnotations <em>All Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Annotations</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Annotated#getAllAnnotations()
	 * @see #getAnnotated()
	 * @generated
	 */
	EReference getAnnotated_AllAnnotations();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement <em>Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requirement</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Requirement
	 * @generated
	 */
	EClass getRequirement();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Requirement#getName()
	 * @see #getRequirement()
	 * @generated
	 */
	EAttribute getRequirement_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Requirement#getTitle()
	 * @see #getRequirement()
	 * @generated
	 */
	EAttribute getRequirement_Title();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Requirement#getDescription()
	 * @see #getRequirement()
	 * @generated
	 */
	EAttribute getRequirement_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getDescriptionFormat <em>Description Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Format</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Requirement#getDescriptionFormat()
	 * @see #getRequirement()
	 * @generated
	 */
	EAttribute getRequirement_DescriptionFormat();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getContracts <em>Contracts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contracts</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Requirement#getContracts()
	 * @see #getRequirement()
	 * @generated
	 */
	EReference getRequirement_Contracts();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale <em>Requirement Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requirement Locale</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.RequirementLocale
	 * @generated
	 */
	EClass getRequirementLocale();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getRef()
	 * @see #getRequirementLocale()
	 * @generated
	 */
	EReference getRequirementLocale_Ref();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getTitle()
	 * @see #getRequirementLocale()
	 * @generated
	 */
	EAttribute getRequirementLocale_Title();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getDescription()
	 * @see #getRequirementLocale()
	 * @generated
	 */
	EAttribute getRequirementLocale_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getDescriptionFormat <em>Description Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Format</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.RequirementLocale#getDescriptionFormat()
	 * @see #getRequirementLocale()
	 * @generated
	 */
	EAttribute getRequirementLocale_DescriptionFormat();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.atsyra.absystem.model.absystem.ActionEnum <em>Action Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Action Enum</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.ActionEnum
	 * @generated
	 */
	EEnum getActionEnum();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.atsyra.absystem.model.absystem.Multiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Multiplicity</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Multiplicity
	 * @generated
	 */
	EEnum getMultiplicity();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.atsyra.absystem.model.absystem.Severity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Severity</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Severity
	 * @generated
	 */
	EEnum getSeverity();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.atsyra.absystem.model.absystem.TextFormat <em>Text Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Text Format</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @generated
	 */
	EEnum getTextFormat();

	/**
	 * Returns the meta object for data type '{@link fr.irisa.atsyra.absystem.model.absystem.Version <em>EVersion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EVersion</em>'.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Version
	 * @model instanceClass="fr.irisa.atsyra.absystem.model.absystem.Version"
	 * @generated
	 */
	EDataType getEVersion();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AbsystemFactory getAbsystemFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl <em>Asset Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetType()
		 * @generated
		 */
		EClass ASSET_TYPE = eINSTANCE.getAssetType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE__NAME = eINSTANCE.getAssetType_Name();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE__LEVEL = eINSTANCE.getAssetType_Level();

		/**
		 * The meta object literal for the '<em><b>Tags</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE__TAGS = eINSTANCE.getAssetType_Tags();

		/**
		 * The meta object literal for the '<em><b>Extends</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE__EXTENDS = eINSTANCE.getAssetType_Extends();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE__DESCRIPTION = eINSTANCE.getAssetType_Description();

		/**
		 * The meta object literal for the '<em><b>Description Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE__DESCRIPTION_FORMAT = eINSTANCE.getAssetType_DescriptionFormat();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE__ABSTRACT = eINSTANCE.getAssetType_Abstract();

		/**
		 * The meta object literal for the '<em><b>All Tags</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE__ALL_TAGS = eINSTANCE.getAssetType_AllTags();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeReferenceImpl <em>Asset Type Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeReferenceImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeReference()
		 * @generated
		 */
		EClass ASSET_TYPE_REFERENCE = eINSTANCE.getAssetTypeReference();

		/**
		 * The meta object literal for the '<em><b>Property Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE_REFERENCE__PROPERTY_TYPE = eINSTANCE.getAssetTypeReference_PropertyType();

		/**
		 * The meta object literal for the '<em><b>Is Container</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE_REFERENCE__IS_CONTAINER = eINSTANCE.getAssetTypeReference_IsContainer();

		/**
		 * The meta object literal for the '<em><b>Opposite Type Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE_REFERENCE__OPPOSITE_TYPE_REFERENCE = eINSTANCE
				.getAssetTypeReference_OppositeTypeReference();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl <em>Asset Based System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetBasedSystem()
		 * @generated
		 */
		EClass ASSET_BASED_SYSTEM = eINSTANCE.getAssetBasedSystem();

		/**
		 * The meta object literal for the '<em><b>Definition Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_BASED_SYSTEM__DEFINITION_GROUPS = eINSTANCE.getAssetBasedSystem_DefinitionGroups();

		/**
		 * The meta object literal for the '<em><b>Asset Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_BASED_SYSTEM__ASSET_GROUPS = eINSTANCE.getAssetBasedSystem_AssetGroups();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_BASED_SYSTEM__IMPORTS = eINSTANCE.getAssetBasedSystem_Imports();

		/**
		 * The meta object literal for the '<em><b>Possible Guarded Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_BASED_SYSTEM__POSSIBLE_GUARDED_ACTIONS = eINSTANCE
				.getAssetBasedSystem_PossibleGuardedActions();

		/**
		 * The meta object literal for the '<em><b>Applied Actions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_BASED_SYSTEM__APPLIED_ACTIONS = eINSTANCE.getAssetBasedSystem_AppliedActions();

		/**
		 * The meta object literal for the '<em><b>All Assets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_BASED_SYSTEM__ALL_ASSETS = eINSTANCE.getAssetBasedSystem_AllAssets();

		/**
		 * The meta object literal for the '<em><b>All Static Asset Links</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_BASED_SYSTEM__ALL_STATIC_ASSET_LINKS = eINSTANCE.getAssetBasedSystem_AllStaticAssetLinks();

		/**
		 * The meta object literal for the '<em><b>Localizations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_BASED_SYSTEM__LOCALIZATIONS = eINSTANCE.getAssetBasedSystem_Localizations();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl <em>Asset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAsset()
		 * @generated
		 */
		EClass ASSET = eINSTANCE.getAsset();

		/**
		 * The meta object literal for the '<em><b>Asset Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET__ASSET_TYPE = eINSTANCE.getAsset_AssetType();

		/**
		 * The meta object literal for the '<em><b>Asset Attribute Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET__ASSET_ATTRIBUTE_VALUES = eINSTANCE.getAsset_AssetAttributeValues();

		/**
		 * The meta object literal for the '<em><b>Features Map</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET__FEATURES_MAP = eINSTANCE.getAsset_FeaturesMap();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET__DESCRIPTION = eINSTANCE.getAsset_Description();

		/**
		 * The meta object literal for the '<em><b>Description Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET__DESCRIPTION_FORMAT = eINSTANCE.getAsset_DescriptionFormat();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetLinkImpl <em>Asset Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetLinkImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetLink()
		 * @generated
		 */
		EClass ASSET_LINK = eINSTANCE.getAssetLink();

		/**
		 * The meta object literal for the '<em><b>Reference Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_LINK__REFERENCE_TYPE = eINSTANCE.getAssetLink_ReferenceType();

		/**
		 * The meta object literal for the '<em><b>Source Asset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_LINK__SOURCE_ASSET = eINSTANCE.getAssetLink_SourceAsset();

		/**
		 * The meta object literal for the '<em><b>Target Asset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_LINK__TARGET_ASSET = eINSTANCE.getAssetLink_TargetAsset();

		/**
		 * The meta object literal for the '<em><b>Opposite Reference Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_LINK__OPPOSITE_REFERENCE_TYPE = eINSTANCE.getAssetLink_OppositeReferenceType();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAttributeImpl <em>Asset Type Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAttributeImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeAttribute()
		 * @generated
		 */
		EClass ASSET_TYPE_ATTRIBUTE = eINSTANCE.getAssetTypeAttribute();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE_ATTRIBUTE__ATTRIBUTE_TYPE = eINSTANCE.getAssetTypeAttribute_AttributeType();

		/**
		 * The meta object literal for the '<em><b>Default Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE_ATTRIBUTE__DEFAULT_VALUES = eINSTANCE.getAssetTypeAttribute_DefaultValues();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeFeatureImpl <em>Asset Type Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeFeatureImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeFeature()
		 * @generated
		 */
		EClass ASSET_TYPE_FEATURE = eINSTANCE.getAssetTypeFeature();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE_FEATURE__MULTIPLICITY = eINSTANCE.getAssetTypeFeature_Multiplicity();

		/**
		 * The meta object literal for the '<em><b>Has Default</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE_FEATURE__HAS_DEFAULT = eINSTANCE.getAssetTypeFeature_HasDefault();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.PrimitiveDataTypeImpl <em>Primitive Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.PrimitiveDataTypeImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getPrimitiveDataType()
		 * @generated
		 */
		EClass PRIMITIVE_DATA_TYPE = eINSTANCE.getPrimitiveDataType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIMITIVE_DATA_TYPE__NAME = eINSTANCE.getPrimitiveDataType_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EnumDataTypeImpl <em>Enum Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EnumDataTypeImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEnumDataType()
		 * @generated
		 */
		EClass ENUM_DATA_TYPE = eINSTANCE.getEnumDataType();

		/**
		 * The meta object literal for the '<em><b>Enum Literal</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_DATA_TYPE__ENUM_LITERAL = eINSTANCE.getEnumDataType_EnumLiteral();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EnumLiteralImpl <em>Enum Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EnumLiteralImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEnumLiteral()
		 * @generated
		 */
		EClass ENUM_LITERAL = eINSTANCE.getEnumLiteral();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetAttributeValueImpl <em>Asset Attribute Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetAttributeValueImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetAttributeValue()
		 * @generated
		 */
		EClass ASSET_ATTRIBUTE_VALUE = eINSTANCE.getAssetAttributeValue();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ATTRIBUTE_VALUE__ATTRIBUTE_TYPE = eINSTANCE.getAssetAttributeValue_AttributeType();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ATTRIBUTE_VALUE__VALUES = eINSTANCE.getAssetAttributeValue_Values();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_ATTRIBUTE_VALUE__COLLECTION = eINSTANCE.getAssetAttributeValue_Collection();

		/**
		 * The meta object literal for the '<em><b>Only Collections Have Many Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ASSET_ATTRIBUTE_VALUE___ONLY_COLLECTIONS_HAVE_MANY_VALUES__DIAGNOSTICCHAIN_MAP = eINSTANCE
				.getAssetAttributeValue__OnlyCollectionsHaveManyValues__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl <em>Definition Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getDefinitionGroup()
		 * @generated
		 */
		EClass DEFINITION_GROUP = eINSTANCE.getDefinitionGroup();

		/**
		 * The meta object literal for the '<em><b>Asset Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP__ASSET_TYPES = eINSTANCE.getDefinitionGroup_AssetTypes();

		/**
		 * The meta object literal for the '<em><b>Primitive Data Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP__PRIMITIVE_DATA_TYPES = eINSTANCE.getDefinitionGroup_PrimitiveDataTypes();

		/**
		 * The meta object literal for the '<em><b>Tag Definitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP__TAG_DEFINITIONS = eINSTANCE.getDefinitionGroup_TagDefinitions();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEFINITION_GROUP__NAME = eINSTANCE.getDefinitionGroup_Name();

		/**
		 * The meta object literal for the '<em><b>Definitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP__DEFINITIONS = eINSTANCE.getDefinitionGroup_Definitions();

		/**
		 * The meta object literal for the '<em><b>Guarded Actions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP__GUARDED_ACTIONS = eINSTANCE.getDefinitionGroup_GuardedActions();

		/**
		 * The meta object literal for the '<em><b>Static Methods</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP__STATIC_METHODS = eINSTANCE.getDefinitionGroup_StaticMethods();

		/**
		 * The meta object literal for the '<em><b>Annotation Keys</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP__ANNOTATION_KEYS = eINSTANCE.getDefinitionGroup_AnnotationKeys();

		/**
		 * The meta object literal for the '<em><b>Contracts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP__CONTRACTS = eINSTANCE.getDefinitionGroup_Contracts();

		/**
		 * The meta object literal for the '<em><b>Tags</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP__TAGS = eINSTANCE.getDefinitionGroup_Tags();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl <em>Asset Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetGroup()
		 * @generated
		 */
		EClass ASSET_GROUP = eINSTANCE.getAssetGroup();

		/**
		 * The meta object literal for the '<em><b>Assets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_GROUP__ASSETS = eINSTANCE.getAssetGroup_Assets();

		/**
		 * The meta object literal for the '<em><b>Asset Links</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_GROUP__ASSET_LINKS = eINSTANCE.getAssetGroup_AssetLinks();

		/**
		 * The meta object literal for the '<em><b>Goals</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_GROUP__GOALS = eINSTANCE.getAssetGroup_Goals();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_GROUP__ELEMENTS = eINSTANCE.getAssetGroup_Elements();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_GROUP__NAME = eINSTANCE.getAssetGroup_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ImportImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '<em><b>Imported Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__IMPORTED_NAMESPACE = eINSTANCE.getImport_ImportedNamespace();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__IMPORT_URI = eINSTANCE.getImport_ImportURI();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.TagImpl <em>Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.TagImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getTag()
		 * @generated
		 */
		EClass TAG = eINSTANCE.getTag();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAG__NAME = eINSTANCE.getTag_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeImpl <em>Abstract Asset Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAbstractAssetType()
		 * @generated
		 */
		EClass ABSTRACT_ASSET_TYPE = eINSTANCE.getAbstractAssetType();

		/**
		 * The meta object literal for the '<em><b>Asset Type Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES = eINSTANCE.getAbstractAssetType_AssetTypeAttributes();

		/**
		 * The meta object literal for the '<em><b>Asset Type Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES = eINSTANCE.getAbstractAssetType_AssetTypeProperties();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAspectImpl <em>Asset Type Aspect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAspectImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeAspect()
		 * @generated
		 */
		EClass ASSET_TYPE_ASPECT = eINSTANCE.getAssetTypeAspect();

		/**
		 * The meta object literal for the '<em><b>Base Asset Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE_ASPECT__BASE_ASSET_TYPE = eINSTANCE.getAssetTypeAspect_BaseAssetType();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardedActionImpl <em>Guarded Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GuardedActionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGuardedAction()
		 * @generated
		 */
		EClass GUARDED_ACTION = eINSTANCE.getGuardedAction();

		/**
		 * The meta object literal for the '<em><b>Guard Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GUARDED_ACTION__GUARD_ACTIONS = eINSTANCE.getGuardedAction_GuardActions();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardParameterImpl <em>Guard Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GuardParameterImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGuardParameter()
		 * @generated
		 */
		EClass GUARD_PARAMETER = eINSTANCE.getGuardParameter();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.BinaryExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getBinaryExpression()
		 * @generated
		 */
		EClass BINARY_EXPRESSION = eINSTANCE.getBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Lhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__LHS = eINSTANCE.getBinaryExpression_Lhs();

		/**
		 * The meta object literal for the '<em><b>Rhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__RHS = eINSTANCE.getBinaryExpression_Rhs();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ImpliesExpressionImpl <em>Implies Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ImpliesExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getImpliesExpression()
		 * @generated
		 */
		EClass IMPLIES_EXPRESSION = eINSTANCE.getImpliesExpression();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.OrExpressionImpl <em>Or Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.OrExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getOrExpression()
		 * @generated
		 */
		EClass OR_EXPRESSION = eINSTANCE.getOrExpression();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AndExpressionImpl <em>And Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AndExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAndExpression()
		 * @generated
		 */
		EClass AND_EXPRESSION = eINSTANCE.getAndExpression();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.NotExpressionImpl <em>Not Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.NotExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getNotExpression()
		 * @generated
		 */
		EClass NOT_EXPRESSION = eINSTANCE.getNotExpression();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOT_EXPRESSION__EXPRESSION = eINSTANCE.getNotExpression_Expression();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EqualityComparisonExpressionImpl <em>Equality Comparison Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EqualityComparisonExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEqualityComparisonExpression()
		 * @generated
		 */
		EClass EQUALITY_COMPARISON_EXPRESSION = eINSTANCE.getEqualityComparisonExpression();

		/**
		 * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUALITY_COMPARISON_EXPRESSION__OP = eINSTANCE.getEqualityComparisonExpression_Op();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.InequalityComparisonExpressionImpl <em>Inequality Comparison Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.InequalityComparisonExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getInequalityComparisonExpression()
		 * @generated
		 */
		EClass INEQUALITY_COMPARISON_EXPRESSION = eINSTANCE.getInequalityComparisonExpression();

		/**
		 * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INEQUALITY_COMPARISON_EXPRESSION__OP = eINSTANCE.getInequalityComparisonExpression_Op();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ActionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__TARGET = eINSTANCE.getAction_Target();

		/**
		 * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__ARGS = eINSTANCE.getAction_Args();

		/**
		 * The meta object literal for the '<em><b>Action Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__ACTION_TYPE = eINSTANCE.getAction_ActionType();

		/**
		 * The meta object literal for the '<em><b>Lambda Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__LAMBDA_ACTION = eINSTANCE.getAction_LambdaAction();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ConstantExpressionImpl <em>Constant Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ConstantExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getConstantExpression()
		 * @generated
		 */
		EClass CONSTANT_EXPRESSION = eINSTANCE.getConstantExpression();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.StringConstantImpl <em>String Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.StringConstantImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getStringConstant()
		 * @generated
		 */
		EClass STRING_CONSTANT = eINSTANCE.getStringConstant();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_CONSTANT__VALUE = eINSTANCE.getStringConstant_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.IntConstantImpl <em>Int Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.IntConstantImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getIntConstant()
		 * @generated
		 */
		EClass INT_CONSTANT = eINSTANCE.getIntConstant();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_CONSTANT__VALUE = eINSTANCE.getIntConstant_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.BooleanConstantImpl <em>Boolean Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.BooleanConstantImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getBooleanConstant()
		 * @generated
		 */
		EClass BOOLEAN_CONSTANT = eINSTANCE.getBooleanConstant();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_CONSTANT__VALUE = eINSTANCE.getBooleanConstant_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.VersionConstantImpl <em>Version Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.VersionConstantImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getVersionConstant()
		 * @generated
		 */
		EClass VERSION_CONSTANT = eINSTANCE.getVersionConstant();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VERSION_CONSTANT__VALUE = eINSTANCE.getVersionConstant_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.MemberSelectionImpl <em>Member Selection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.MemberSelectionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getMemberSelection()
		 * @generated
		 */
		EClass MEMBER_SELECTION = eINSTANCE.getMemberSelection();

		/**
		 * The meta object literal for the '<em><b>Receiver</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMBER_SELECTION__RECEIVER = eINSTANCE.getMemberSelection_Receiver();

		/**
		 * The meta object literal for the '<em><b>Member</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMBER_SELECTION__MEMBER = eINSTANCE.getMemberSelection_Member();

		/**
		 * The meta object literal for the '<em><b>Method Invocation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEMBER_SELECTION__METHOD_INVOCATION = eINSTANCE.getMemberSelection_MethodInvocation();

		/**
		 * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMBER_SELECTION__ARGS = eINSTANCE.getMemberSelection_Args();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.MemberImpl <em>Member</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.MemberImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getMember()
		 * @generated
		 */
		EClass MEMBER = eINSTANCE.getMember();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEMBER__NAME = eINSTANCE.getMember_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.SymbolRefImpl <em>Symbol Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.SymbolRefImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getSymbolRef()
		 * @generated
		 */
		EClass SYMBOL_REF = eINSTANCE.getSymbolRef();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_REF__SYMBOL = eINSTANCE.getSymbolRef_Symbol();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.SymbolImpl <em>Symbol</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.SymbolImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getSymbol()
		 * @generated
		 */
		EClass SYMBOL = eINSTANCE.getSymbol();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYMBOL__NAME = eINSTANCE.getSymbol_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.StaticMethodImpl <em>Static Method</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.StaticMethodImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getStaticMethod()
		 * @generated
		 */
		EClass STATIC_METHOD = eINSTANCE.getStaticMethod();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaParameterImpl <em>Lambda Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.LambdaParameterImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getLambdaParameter()
		 * @generated
		 */
		EClass LAMBDA_PARAMETER = eINSTANCE.getLambdaParameter();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaExpressionImpl <em>Lambda Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.LambdaExpressionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getLambdaExpression()
		 * @generated
		 */
		EClass LAMBDA_EXPRESSION = eINSTANCE.getLambdaExpression();

		/**
		 * The meta object literal for the '<em><b>Lambda Parameter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LAMBDA_EXPRESSION__LAMBDA_PARAMETER = eINSTANCE.getLambdaExpression_LambdaParameter();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LAMBDA_EXPRESSION__BODY = eINSTANCE.getLambdaExpression_Body();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaActionImpl <em>Lambda Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.LambdaActionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getLambdaAction()
		 * @generated
		 */
		EClass LAMBDA_ACTION = eINSTANCE.getLambdaAction();

		/**
		 * The meta object literal for the '<em><b>Lambda Parameter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LAMBDA_ACTION__LAMBDA_PARAMETER = eINSTANCE.getLambdaAction_LambdaParameter();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LAMBDA_ACTION__ACTIONS = eINSTANCE.getLambdaAction_Actions();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ParameterImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Parameter Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__PARAMETER_TYPE = eINSTANCE.getParameter_ParameterType();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AnnotationEntryImpl <em>Annotation Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AnnotationEntryImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAnnotationEntry()
		 * @generated
		 */
		EClass ANNOTATION_ENTRY = eINSTANCE.getAnnotationEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION_ENTRY__VALUE = eINSTANCE.getAnnotationEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATION_ENTRY__KEY = eINSTANCE.getAnnotationEntry_Key();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AnnotationKeyImpl <em>Annotation Key</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AnnotationKeyImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAnnotationKey()
		 * @generated
		 */
		EClass ANNOTATION_KEY = eINSTANCE.getAnnotationKey();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION_KEY__NAME = eINSTANCE.getAnnotationKey_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GoalImpl <em>Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GoalImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGoal()
		 * @generated
		 */
		EClass GOAL = eINSTANCE.getGoal();

		/**
		 * The meta object literal for the '<em><b>Precondition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL__PRECONDITION = eINSTANCE.getGoal_Precondition();

		/**
		 * The meta object literal for the '<em><b>Postcondition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL__POSTCONDITION = eINSTANCE.getGoal_Postcondition();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL__NAME = eINSTANCE.getGoal_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL__DESCRIPTION = eINSTANCE.getGoal_Description();

		/**
		 * The meta object literal for the '<em><b>Description Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL__DESCRIPTION_FORMAT = eINSTANCE.getGoal_DescriptionFormat();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.ContractImpl <em>Contract</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.ContractImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getContract()
		 * @generated
		 */
		EClass CONTRACT = eINSTANCE.getContract();

		/**
		 * The meta object literal for the '<em><b>Dynamic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTRACT__DYNAMIC = eINSTANCE.getContract_Dynamic();

		/**
		 * The meta object literal for the '<em><b>Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTRACT__SEVERITY = eINSTANCE.getContract_Severity();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardImpl <em>Guard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GuardImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGuard()
		 * @generated
		 */
		EClass GUARD = eINSTANCE.getGuard();

		/**
		 * The meta object literal for the '<em><b>Guard Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GUARD__GUARD_EXPRESSION = eINSTANCE.getGuard_GuardExpression();

		/**
		 * The meta object literal for the '<em><b>Guard Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GUARD__GUARD_PARAMETERS = eINSTANCE.getGuard_GuardParameters();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUARD__NAME = eINSTANCE.getGuard_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUARD__DESCRIPTION = eINSTANCE.getGuard_Description();

		/**
		 * The meta object literal for the '<em><b>Description Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUARD__DESCRIPTION_FORMAT = eINSTANCE.getGuard_DescriptionFormat();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EnumConstantImpl <em>Enum Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EnumConstantImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEnumConstant()
		 * @generated
		 */
		EClass ENUM_CONSTANT = eINSTANCE.getEnumConstant();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_CONSTANT__VALUE = eINSTANCE.getEnumConstant_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.UndefinedConstantImpl <em>Undefined Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.UndefinedConstantImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getUndefinedConstant()
		 * @generated
		 */
		EClass UNDEFINED_CONSTANT = eINSTANCE.getUndefinedConstant();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.CollectionImpl <em>Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.CollectionImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getCollection()
		 * @generated
		 */
		EClass COLLECTION = eINSTANCE.getCollection();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLECTION__ELEMENTS = eINSTANCE.getCollection_Elements();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.Definition <em>Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.Definition
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getDefinition()
		 * @generated
		 */
		EClass DEFINITION = eINSTANCE.getDefinition();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEFINITION__ID = eINSTANCE.getDefinition_Id();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent <em>Asset Group Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetGroupContent()
		 * @generated
		 */
		EClass ASSET_GROUP_CONTENT = eINSTANCE.getAssetGroupContent();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_GROUP_CONTENT__ID = eINSTANCE.getAssetGroupContent_Id();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.LocaleGroupImpl <em>Locale Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.LocaleGroupImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getLocaleGroup()
		 * @generated
		 */
		EClass LOCALE_GROUP = eINSTANCE.getLocaleGroup();

		/**
		 * The meta object literal for the '<em><b>Locale</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCALE_GROUP__LOCALE = eINSTANCE.getLocaleGroup_Locale();

		/**
		 * The meta object literal for the '<em><b>Object Locales</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCALE_GROUP__OBJECT_LOCALES = eINSTANCE.getLocaleGroup_ObjectLocales();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale <em>ABS Object Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getABSObjectLocale()
		 * @generated
		 */
		EClass ABS_OBJECT_LOCALE = eINSTANCE.getABSObjectLocale();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupLocaleImpl <em>Definition Group Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupLocaleImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getDefinitionGroupLocale()
		 * @generated
		 */
		EClass DEFINITION_GROUP_LOCALE = eINSTANCE.getDefinitionGroupLocale();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP_LOCALE__REF = eINSTANCE.getDefinitionGroupLocale_Ref();

		/**
		 * The meta object literal for the '<em><b>Definition Locales</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_GROUP_LOCALE__DEFINITION_LOCALES = eINSTANCE.getDefinitionGroupLocale_DefinitionLocales();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.PrimitiveDataTypeLocaleImpl <em>Primitive Data Type Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.PrimitiveDataTypeLocaleImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getPrimitiveDataTypeLocale()
		 * @generated
		 */
		EClass PRIMITIVE_DATA_TYPE_LOCALE = eINSTANCE.getPrimitiveDataTypeLocale();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMITIVE_DATA_TYPE_LOCALE__REF = eINSTANCE.getPrimitiveDataTypeLocale_Ref();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIMITIVE_DATA_TYPE_LOCALE__NAME = eINSTANCE.getPrimitiveDataTypeLocale_Name();

		/**
		 * The meta object literal for the '<em><b>Literals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMITIVE_DATA_TYPE_LOCALE__LITERALS = eINSTANCE.getPrimitiveDataTypeLocale_Literals();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.EnumLiteralLocaleImpl <em>Enum Literal Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.EnumLiteralLocaleImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEnumLiteralLocale()
		 * @generated
		 */
		EClass ENUM_LITERAL_LOCALE = eINSTANCE.getEnumLiteralLocale();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_LITERAL_LOCALE__REF = eINSTANCE.getEnumLiteralLocale_Ref();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENUM_LITERAL_LOCALE__NAME = eINSTANCE.getEnumLiteralLocale_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeLocaleImpl <em>Abstract Asset Type Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeLocaleImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAbstractAssetTypeLocale()
		 * @generated
		 */
		EClass ABSTRACT_ASSET_TYPE_LOCALE = eINSTANCE.getAbstractAssetTypeLocale();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_ASSET_TYPE_LOCALE__FEATURES = eINSTANCE.getAbstractAssetTypeLocale_Features();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeLocaleImpl <em>Asset Type Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeLocaleImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeLocale()
		 * @generated
		 */
		EClass ASSET_TYPE_LOCALE = eINSTANCE.getAssetTypeLocale();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE_LOCALE__REF = eINSTANCE.getAssetTypeLocale_Ref();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE_LOCALE__NAME = eINSTANCE.getAssetTypeLocale_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE_LOCALE__DESCRIPTION = eINSTANCE.getAssetTypeLocale_Description();

		/**
		 * The meta object literal for the '<em><b>Description Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE_LOCALE__DESCRIPTION_FORMAT = eINSTANCE.getAssetTypeLocale_DescriptionFormat();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAspectLocaleImpl <em>Asset Type Aspect Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeAspectLocaleImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeAspectLocale()
		 * @generated
		 */
		EClass ASSET_TYPE_ASPECT_LOCALE = eINSTANCE.getAssetTypeAspectLocale();

		/**
		 * The meta object literal for the '<em><b>Base Asset Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE_ASPECT_LOCALE__BASE_ASSET_TYPE = eINSTANCE.getAssetTypeAspectLocale_BaseAssetType();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeFeatureLocaleImpl <em>Asset Type Feature Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeFeatureLocaleImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAssetTypeFeatureLocale()
		 * @generated
		 */
		EClass ASSET_TYPE_FEATURE_LOCALE = eINSTANCE.getAssetTypeFeatureLocale();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_TYPE_FEATURE_LOCALE__REF = eINSTANCE.getAssetTypeFeatureLocale_Ref();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_TYPE_FEATURE_LOCALE__NAME = eINSTANCE.getAssetTypeFeatureLocale_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardLocaleImpl <em>Guard Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.GuardLocaleImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getGuardLocale()
		 * @generated
		 */
		EClass GUARD_LOCALE = eINSTANCE.getGuardLocale();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GUARD_LOCALE__REF = eINSTANCE.getGuardLocale_Ref();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUARD_LOCALE__NAME = eINSTANCE.getGuardLocale_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUARD_LOCALE__DESCRIPTION = eINSTANCE.getGuardLocale_Description();

		/**
		 * The meta object literal for the '<em><b>Description Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUARD_LOCALE__DESCRIPTION_FORMAT = eINSTANCE.getGuardLocale_DescriptionFormat();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.AnnotatedImpl <em>Annotated</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AnnotatedImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getAnnotated()
		 * @generated
		 */
		EClass ANNOTATED = eINSTANCE.getAnnotated();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATED__ANNOTATIONS = eINSTANCE.getAnnotated_Annotations();

		/**
		 * The meta object literal for the '<em><b>All Annotations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATED__ALL_ANNOTATIONS = eINSTANCE.getAnnotated_AllAnnotations();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl <em>Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getRequirement()
		 * @generated
		 */
		EClass REQUIREMENT = eINSTANCE.getRequirement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT__NAME = eINSTANCE.getRequirement_Name();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT__TITLE = eINSTANCE.getRequirement_Title();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT__DESCRIPTION = eINSTANCE.getRequirement_Description();

		/**
		 * The meta object literal for the '<em><b>Description Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT__DESCRIPTION_FORMAT = eINSTANCE.getRequirement_DescriptionFormat();

		/**
		 * The meta object literal for the '<em><b>Contracts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT__CONTRACTS = eINSTANCE.getRequirement_Contracts();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementLocaleImpl <em>Requirement Locale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.RequirementLocaleImpl
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getRequirementLocale()
		 * @generated
		 */
		EClass REQUIREMENT_LOCALE = eINSTANCE.getRequirementLocale();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_LOCALE__REF = eINSTANCE.getRequirementLocale_Ref();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT_LOCALE__TITLE = eINSTANCE.getRequirementLocale_Title();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT_LOCALE__DESCRIPTION = eINSTANCE.getRequirementLocale_Description();

		/**
		 * The meta object literal for the '<em><b>Description Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT_LOCALE__DESCRIPTION_FORMAT = eINSTANCE.getRequirementLocale_DescriptionFormat();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.ActionEnum <em>Action Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.ActionEnum
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getActionEnum()
		 * @generated
		 */
		EEnum ACTION_ENUM = eINSTANCE.getActionEnum();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.Multiplicity <em>Multiplicity</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.Multiplicity
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getMultiplicity()
		 * @generated
		 */
		EEnum MULTIPLICITY = eINSTANCE.getMultiplicity();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.Severity <em>Severity</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.Severity
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getSeverity()
		 * @generated
		 */
		EEnum SEVERITY = eINSTANCE.getSeverity();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.model.absystem.TextFormat <em>Text Format</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getTextFormat()
		 * @generated
		 */
		EEnum TEXT_FORMAT = eINSTANCE.getTextFormat();

		/**
		 * The meta object literal for the '<em>EVersion</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.model.absystem.Version
		 * @see fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl#getEVersion()
		 * @generated
		 */
		EDataType EVERSION = eINSTANCE.getEVersion();

	}

} //AbsystemPackage
