/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent;
import fr.irisa.atsyra.absystem.model.absystem.AssetLink;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetLinkImpl#getId <em>Id</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetLinkImpl#getReferenceType <em>Reference Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetLinkImpl#getSourceAsset <em>Source Asset</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetLinkImpl#getTargetAsset <em>Target Asset</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetLinkImpl#getOppositeReferenceType <em>Opposite Reference Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetLinkImpl extends MinimalEObjectImpl.Container implements AssetLink {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReferenceType() <em>Reference Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceType()
	 * @generated
	 * @ordered
	 */
	protected AssetTypeReference referenceType;

	/**
	 * The cached value of the '{@link #getSourceAsset() <em>Source Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceAsset()
	 * @generated
	 * @ordered
	 */
	protected Asset sourceAsset;

	/**
	 * The cached value of the '{@link #getTargetAsset() <em>Target Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetAsset()
	 * @generated
	 * @ordered
	 */
	protected Asset targetAsset;

	/**
	 * The cached value of the '{@link #getOppositeReferenceType() <em>Opposite Reference Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOppositeReferenceType()
	 * @generated
	 * @ordered
	 */
	protected AssetTypeReference oppositeReferenceType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ASSET_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getId() {
		StringBuilder result = new StringBuilder();
		result.append(eClass().getName()).append('-');
		if (eContainer() != null) {
			@SuppressWarnings("unchecked")
			List<AssetGroupContent> containingList = (List<AssetGroupContent>) eContainer().eGet(eContainingFeature());
			result.append(containingList.indexOf(this));
		}
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeReference getReferenceType() {
		if (referenceType != null && referenceType.eIsProxy()) {
			InternalEObject oldReferenceType = (InternalEObject) referenceType;
			referenceType = (AssetTypeReference) eResolveProxy(oldReferenceType);
			if (referenceType != oldReferenceType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AbsystemPackage.ASSET_LINK__REFERENCE_TYPE, oldReferenceType, referenceType));
			}
		}
		return referenceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetTypeReference basicGetReferenceType() {
		return referenceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReferenceType(AssetTypeReference newReferenceType) {
		AssetTypeReference oldReferenceType = referenceType;
		referenceType = newReferenceType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_LINK__REFERENCE_TYPE,
					oldReferenceType, referenceType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Asset getSourceAsset() {
		if (sourceAsset != null && sourceAsset.eIsProxy()) {
			InternalEObject oldSourceAsset = (InternalEObject) sourceAsset;
			sourceAsset = (Asset) eResolveProxy(oldSourceAsset);
			if (sourceAsset != oldSourceAsset) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AbsystemPackage.ASSET_LINK__SOURCE_ASSET,
							oldSourceAsset, sourceAsset));
			}
		}
		return sourceAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Asset basicGetSourceAsset() {
		return sourceAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceAsset(Asset newSourceAsset) {
		Asset oldSourceAsset = sourceAsset;
		sourceAsset = newSourceAsset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_LINK__SOURCE_ASSET,
					oldSourceAsset, sourceAsset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Asset getTargetAsset() {
		if (targetAsset != null && targetAsset.eIsProxy()) {
			InternalEObject oldTargetAsset = (InternalEObject) targetAsset;
			targetAsset = (Asset) eResolveProxy(oldTargetAsset);
			if (targetAsset != oldTargetAsset) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AbsystemPackage.ASSET_LINK__TARGET_ASSET,
							oldTargetAsset, targetAsset));
			}
		}
		return targetAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Asset basicGetTargetAsset() {
		return targetAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetAsset(Asset newTargetAsset) {
		Asset oldTargetAsset = targetAsset;
		targetAsset = newTargetAsset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_LINK__TARGET_ASSET,
					oldTargetAsset, targetAsset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeReference getOppositeReferenceType() {
		if (oppositeReferenceType != null && oppositeReferenceType.eIsProxy()) {
			InternalEObject oldOppositeReferenceType = (InternalEObject) oppositeReferenceType;
			oppositeReferenceType = (AssetTypeReference) eResolveProxy(oldOppositeReferenceType);
			if (oppositeReferenceType != oldOppositeReferenceType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AbsystemPackage.ASSET_LINK__OPPOSITE_REFERENCE_TYPE, oldOppositeReferenceType,
							oppositeReferenceType));
			}
		}
		return oppositeReferenceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetTypeReference basicGetOppositeReferenceType() {
		return oppositeReferenceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOppositeReferenceType(AssetTypeReference newOppositeReferenceType) {
		AssetTypeReference oldOppositeReferenceType = oppositeReferenceType;
		oppositeReferenceType = newOppositeReferenceType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_LINK__OPPOSITE_REFERENCE_TYPE,
					oldOppositeReferenceType, oppositeReferenceType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ASSET_LINK__ID:
			return getId();
		case AbsystemPackage.ASSET_LINK__REFERENCE_TYPE:
			if (resolve)
				return getReferenceType();
			return basicGetReferenceType();
		case AbsystemPackage.ASSET_LINK__SOURCE_ASSET:
			if (resolve)
				return getSourceAsset();
			return basicGetSourceAsset();
		case AbsystemPackage.ASSET_LINK__TARGET_ASSET:
			if (resolve)
				return getTargetAsset();
			return basicGetTargetAsset();
		case AbsystemPackage.ASSET_LINK__OPPOSITE_REFERENCE_TYPE:
			if (resolve)
				return getOppositeReferenceType();
			return basicGetOppositeReferenceType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ASSET_LINK__REFERENCE_TYPE:
			setReferenceType((AssetTypeReference) newValue);
			return;
		case AbsystemPackage.ASSET_LINK__SOURCE_ASSET:
			setSourceAsset((Asset) newValue);
			return;
		case AbsystemPackage.ASSET_LINK__TARGET_ASSET:
			setTargetAsset((Asset) newValue);
			return;
		case AbsystemPackage.ASSET_LINK__OPPOSITE_REFERENCE_TYPE:
			setOppositeReferenceType((AssetTypeReference) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_LINK__REFERENCE_TYPE:
			setReferenceType((AssetTypeReference) null);
			return;
		case AbsystemPackage.ASSET_LINK__SOURCE_ASSET:
			setSourceAsset((Asset) null);
			return;
		case AbsystemPackage.ASSET_LINK__TARGET_ASSET:
			setTargetAsset((Asset) null);
			return;
		case AbsystemPackage.ASSET_LINK__OPPOSITE_REFERENCE_TYPE:
			setOppositeReferenceType((AssetTypeReference) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_LINK__ID:
			return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
		case AbsystemPackage.ASSET_LINK__REFERENCE_TYPE:
			return referenceType != null;
		case AbsystemPackage.ASSET_LINK__SOURCE_ASSET:
			return sourceAsset != null;
		case AbsystemPackage.ASSET_LINK__TARGET_ASSET:
			return targetAsset != null;
		case AbsystemPackage.ASSET_LINK__OPPOSITE_REFERENCE_TYPE:
			return oppositeReferenceType != null;
		}
		return super.eIsSet(featureID);
	}

} //AssetLinkImpl
