/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Member Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getMember <em>Member</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#isMethodInvocation <em>Method Invocation</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getArgs <em>Args</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getMemberSelection()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='ReceiverMultiplicity ArgMultiplicity InvocationIsMethod NoUndefinedConstant Type'"
 * @generated
 */
public interface MemberSelection extends Expression {
	/**
	 * Returns the value of the '<em><b>Receiver</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiver</em>' containment reference.
	 * @see #setReceiver(Expression)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getMemberSelection_Receiver()
	 * @model containment="true"
	 * @generated
	 */
	Expression getReceiver();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getReceiver <em>Receiver</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receiver</em>' containment reference.
	 * @see #getReceiver()
	 * @generated
	 */
	void setReceiver(Expression value);

	/**
	 * Returns the value of the '<em><b>Member</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Member</em>' reference.
	 * @see #setMember(Member)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getMemberSelection_Member()
	 * @model
	 * @generated
	 */
	Member getMember();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#getMember <em>Member</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Member</em>' reference.
	 * @see #getMember()
	 * @generated
	 */
	void setMember(Member value);

	/**
	 * Returns the value of the '<em><b>Method Invocation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method Invocation</em>' attribute.
	 * @see #setMethodInvocation(boolean)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getMemberSelection_MethodInvocation()
	 * @model
	 * @generated
	 */
	boolean isMethodInvocation();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection#isMethodInvocation <em>Method Invocation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method Invocation</em>' attribute.
	 * @see #isMethodInvocation()
	 * @generated
	 */
	void setMethodInvocation(boolean value);

	/**
	 * Returns the value of the '<em><b>Args</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Expression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Args</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getMemberSelection_Args()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getArgs();

} // MemberSelection
