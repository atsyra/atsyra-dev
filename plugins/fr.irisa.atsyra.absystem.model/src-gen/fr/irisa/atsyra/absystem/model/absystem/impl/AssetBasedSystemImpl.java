/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup;
import fr.irisa.atsyra.absystem.model.absystem.AssetLink;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.Import;
import fr.irisa.atsyra.absystem.model.absystem.LocaleGroup;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Based System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl#getDefinitionGroups <em>Definition Groups</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl#getAssetGroups <em>Asset Groups</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl#getPossibleGuardedActions <em>Possible Guarded Actions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl#getAppliedActions <em>Applied Actions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl#getAllAssets <em>All Assets</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl#getAllStaticAssetLinks <em>All Static Asset Links</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetBasedSystemImpl#getLocalizations <em>Localizations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetBasedSystemImpl extends MinimalEObjectImpl.Container implements AssetBasedSystem {
	/**
	 * The cached value of the '{@link #getDefinitionGroups() <em>Definition Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitionGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<DefinitionGroup> definitionGroups;

	/**
	 * The cached value of the '{@link #getAssetGroups() <em>Asset Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetGroup> assetGroups;

	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> imports;

	/**
	 * The cached value of the '{@link #getPossibleGuardedActions() <em>Possible Guarded Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPossibleGuardedActions()
	 * @generated
	 * @ordered
	 */
	protected EList<GuardOccurence> possibleGuardedActions;

	/**
	 * The cached value of the '{@link #getAppliedActions() <em>Applied Actions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAppliedActions()
	 * @generated
	 * @ordered
	 */
	protected EList<GuardOccurence> appliedActions;

	/**
	 * The cached value of the '{@link #getAllAssets() <em>All Assets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllAssets()
	 * @generated
	 * @ordered
	 */
	protected EList<Asset> allAssets;

	/**
	 * The cached value of the '{@link #getAllStaticAssetLinks() <em>All Static Asset Links</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllStaticAssetLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetLink> allStaticAssetLinks;

	/**
	 * The cached value of the '{@link #getLocalizations() <em>Localizations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalizations()
	 * @generated
	 * @ordered
	 */
	protected EList<LocaleGroup> localizations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetBasedSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ASSET_BASED_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DefinitionGroup> getDefinitionGroups() {
		if (definitionGroups == null) {
			definitionGroups = new EObjectContainmentEList<DefinitionGroup>(DefinitionGroup.class, this,
					AbsystemPackage.ASSET_BASED_SYSTEM__DEFINITION_GROUPS);
		}
		return definitionGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetGroup> getAssetGroups() {
		if (assetGroups == null) {
			assetGroups = new EObjectContainmentEList<AssetGroup>(AssetGroup.class, this,
					AbsystemPackage.ASSET_BASED_SYSTEM__ASSET_GROUPS);
		}
		return assetGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Import> getImports() {
		if (imports == null) {
			imports = new EObjectContainmentEList<Import>(Import.class, this,
					AbsystemPackage.ASSET_BASED_SYSTEM__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<GuardOccurence> getPossibleGuardedActions() {
		if (possibleGuardedActions == null) {
			possibleGuardedActions = new EObjectContainmentEList<GuardOccurence>(GuardOccurence.class, this,
					AbsystemPackage.ASSET_BASED_SYSTEM__POSSIBLE_GUARDED_ACTIONS);
		}
		return possibleGuardedActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<GuardOccurence> getAppliedActions() {
		if (appliedActions == null) {
			appliedActions = new EObjectResolvingEList<GuardOccurence>(GuardOccurence.class, this,
					AbsystemPackage.ASSET_BASED_SYSTEM__APPLIED_ACTIONS);
		}
		return appliedActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Asset> getAllAssets() {
		if (allAssets == null) {
			allAssets = new EObjectResolvingEList<Asset>(Asset.class, this,
					AbsystemPackage.ASSET_BASED_SYSTEM__ALL_ASSETS);
		}
		return allAssets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetLink> getAllStaticAssetLinks() {
		if (allStaticAssetLinks == null) {
			allStaticAssetLinks = new EObjectResolvingEList<AssetLink>(AssetLink.class, this,
					AbsystemPackage.ASSET_BASED_SYSTEM__ALL_STATIC_ASSET_LINKS);
		}
		return allStaticAssetLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LocaleGroup> getLocalizations() {
		if (localizations == null) {
			localizations = new EObjectContainmentEList<LocaleGroup>(LocaleGroup.class, this,
					AbsystemPackage.ASSET_BASED_SYSTEM__LOCALIZATIONS);
		}
		return localizations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.ASSET_BASED_SYSTEM__DEFINITION_GROUPS:
			return ((InternalEList<?>) getDefinitionGroups()).basicRemove(otherEnd, msgs);
		case AbsystemPackage.ASSET_BASED_SYSTEM__ASSET_GROUPS:
			return ((InternalEList<?>) getAssetGroups()).basicRemove(otherEnd, msgs);
		case AbsystemPackage.ASSET_BASED_SYSTEM__IMPORTS:
			return ((InternalEList<?>) getImports()).basicRemove(otherEnd, msgs);
		case AbsystemPackage.ASSET_BASED_SYSTEM__POSSIBLE_GUARDED_ACTIONS:
			return ((InternalEList<?>) getPossibleGuardedActions()).basicRemove(otherEnd, msgs);
		case AbsystemPackage.ASSET_BASED_SYSTEM__LOCALIZATIONS:
			return ((InternalEList<?>) getLocalizations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ASSET_BASED_SYSTEM__DEFINITION_GROUPS:
			return getDefinitionGroups();
		case AbsystemPackage.ASSET_BASED_SYSTEM__ASSET_GROUPS:
			return getAssetGroups();
		case AbsystemPackage.ASSET_BASED_SYSTEM__IMPORTS:
			return getImports();
		case AbsystemPackage.ASSET_BASED_SYSTEM__POSSIBLE_GUARDED_ACTIONS:
			return getPossibleGuardedActions();
		case AbsystemPackage.ASSET_BASED_SYSTEM__APPLIED_ACTIONS:
			return getAppliedActions();
		case AbsystemPackage.ASSET_BASED_SYSTEM__ALL_ASSETS:
			return getAllAssets();
		case AbsystemPackage.ASSET_BASED_SYSTEM__ALL_STATIC_ASSET_LINKS:
			return getAllStaticAssetLinks();
		case AbsystemPackage.ASSET_BASED_SYSTEM__LOCALIZATIONS:
			return getLocalizations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ASSET_BASED_SYSTEM__DEFINITION_GROUPS:
			getDefinitionGroups().clear();
			getDefinitionGroups().addAll((Collection<? extends DefinitionGroup>) newValue);
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__ASSET_GROUPS:
			getAssetGroups().clear();
			getAssetGroups().addAll((Collection<? extends AssetGroup>) newValue);
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__IMPORTS:
			getImports().clear();
			getImports().addAll((Collection<? extends Import>) newValue);
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__POSSIBLE_GUARDED_ACTIONS:
			getPossibleGuardedActions().clear();
			getPossibleGuardedActions().addAll((Collection<? extends GuardOccurence>) newValue);
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__APPLIED_ACTIONS:
			getAppliedActions().clear();
			getAppliedActions().addAll((Collection<? extends GuardOccurence>) newValue);
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__ALL_ASSETS:
			getAllAssets().clear();
			getAllAssets().addAll((Collection<? extends Asset>) newValue);
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__ALL_STATIC_ASSET_LINKS:
			getAllStaticAssetLinks().clear();
			getAllStaticAssetLinks().addAll((Collection<? extends AssetLink>) newValue);
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__LOCALIZATIONS:
			getLocalizations().clear();
			getLocalizations().addAll((Collection<? extends LocaleGroup>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_BASED_SYSTEM__DEFINITION_GROUPS:
			getDefinitionGroups().clear();
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__ASSET_GROUPS:
			getAssetGroups().clear();
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__IMPORTS:
			getImports().clear();
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__POSSIBLE_GUARDED_ACTIONS:
			getPossibleGuardedActions().clear();
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__APPLIED_ACTIONS:
			getAppliedActions().clear();
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__ALL_ASSETS:
			getAllAssets().clear();
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__ALL_STATIC_ASSET_LINKS:
			getAllStaticAssetLinks().clear();
			return;
		case AbsystemPackage.ASSET_BASED_SYSTEM__LOCALIZATIONS:
			getLocalizations().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_BASED_SYSTEM__DEFINITION_GROUPS:
			return definitionGroups != null && !definitionGroups.isEmpty();
		case AbsystemPackage.ASSET_BASED_SYSTEM__ASSET_GROUPS:
			return assetGroups != null && !assetGroups.isEmpty();
		case AbsystemPackage.ASSET_BASED_SYSTEM__IMPORTS:
			return imports != null && !imports.isEmpty();
		case AbsystemPackage.ASSET_BASED_SYSTEM__POSSIBLE_GUARDED_ACTIONS:
			return possibleGuardedActions != null && !possibleGuardedActions.isEmpty();
		case AbsystemPackage.ASSET_BASED_SYSTEM__APPLIED_ACTIONS:
			return appliedActions != null && !appliedActions.isEmpty();
		case AbsystemPackage.ASSET_BASED_SYSTEM__ALL_ASSETS:
			return allAssets != null && !allAssets.isEmpty();
		case AbsystemPackage.ASSET_BASED_SYSTEM__ALL_STATIC_ASSET_LINKS:
			return allStaticAssetLinks != null && !allStaticAssetLinks.isEmpty();
		case AbsystemPackage.ASSET_BASED_SYSTEM__LOCALIZATIONS:
			return localizations != null && !localizations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssetBasedSystemImpl
