/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl;

import fr.irisa.atsyra.absystem.model.absystem.Guard;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurenceArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage;

import java.util.Collection;
import java.util.stream.Collectors;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Guard Occurence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl.GuardOccurenceImpl#getGuard <em>Guard</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl.GuardOccurenceImpl#getGuardOccurenceArguments <em>Guard Occurence Arguments</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl.GuardOccurenceImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GuardOccurenceImpl extends MinimalEObjectImpl.Container implements GuardOccurence {
	/**
	 * The cached value of the '{@link #getGuard() <em>Guard</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuard()
	 * @generated
	 * @ordered
	 */
	protected Guard guard;

	/**
	 * The cached value of the '{@link #getGuardOccurenceArguments() <em>Guard Occurence Arguments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuardOccurenceArguments()
	 * @generated
	 * @ordered
	 */
	protected EList<GuardOccurenceArgument> guardOccurenceArguments;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GuardOccurenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Interpreter_vmPackage.Literals.GUARD_OCCURENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Guard getGuard() {
		if (guard != null && guard.eIsProxy()) {
			InternalEObject oldGuard = (InternalEObject) guard;
			guard = (Guard) eResolveProxy(oldGuard);
			if (guard != oldGuard) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Interpreter_vmPackage.GUARD_OCCURENCE__GUARD, oldGuard, guard));
			}
		}
		return guard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Guard basicGetGuard() {
		return guard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGuard(Guard newGuard) {
		Guard oldGuard = guard;
		guard = newGuard;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Interpreter_vmPackage.GUARD_OCCURENCE__GUARD,
					oldGuard, guard));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<GuardOccurenceArgument> getGuardOccurenceArguments() {
		if (guardOccurenceArguments == null) {
			guardOccurenceArguments = new EObjectContainmentEList<GuardOccurenceArgument>(GuardOccurenceArgument.class,
					this, Interpreter_vmPackage.GUARD_OCCURENCE__GUARD_OCCURENCE_ARGUMENTS);
		}
		return guardOccurenceArguments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getName() {
		if (this.getGuard() != null) {
			return this.getGuard().getName() + "( " + this.getGuardOccurenceArguments().stream()
					.map(arg -> arg.getName()).collect(Collectors.joining(", ")) + ")";
		} else {
			return EcoreUtil.getURI(this).fragment();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Interpreter_vmPackage.GUARD_OCCURENCE__GUARD_OCCURENCE_ARGUMENTS:
			return ((InternalEList<?>) getGuardOccurenceArguments()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Interpreter_vmPackage.GUARD_OCCURENCE__GUARD:
			if (resolve)
				return getGuard();
			return basicGetGuard();
		case Interpreter_vmPackage.GUARD_OCCURENCE__GUARD_OCCURENCE_ARGUMENTS:
			return getGuardOccurenceArguments();
		case Interpreter_vmPackage.GUARD_OCCURENCE__NAME:
			return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Interpreter_vmPackage.GUARD_OCCURENCE__GUARD:
			setGuard((Guard) newValue);
			return;
		case Interpreter_vmPackage.GUARD_OCCURENCE__GUARD_OCCURENCE_ARGUMENTS:
			getGuardOccurenceArguments().clear();
			getGuardOccurenceArguments().addAll((Collection<? extends GuardOccurenceArgument>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.GUARD_OCCURENCE__GUARD:
			setGuard((Guard) null);
			return;
		case Interpreter_vmPackage.GUARD_OCCURENCE__GUARD_OCCURENCE_ARGUMENTS:
			getGuardOccurenceArguments().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.GUARD_OCCURENCE__GUARD:
			return guard != null;
		case Interpreter_vmPackage.GUARD_OCCURENCE__GUARD_OCCURENCE_ARGUMENTS:
			return guardOccurenceArguments != null && !guardOccurenceArguments.isEmpty();
		case Interpreter_vmPackage.GUARD_OCCURENCE__NAME:
			return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
		}
		return super.eIsSet(featureID);
	}

} //GuardOccurenceImpl
