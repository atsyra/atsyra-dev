/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ListValue;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Value;

import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>List Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl.ListValueImpl#getOwnedValues <em>Owned Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ListValueImpl extends ValueImpl implements ListValue {
	/**
	 * The cached value of the '{@link #getOwnedValues() <em>Owned Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Value> ownedValues;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ListValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Interpreter_vmPackage.Literals.LIST_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Value> getOwnedValues() {
		if (ownedValues == null) {
			ownedValues = new EObjectContainmentEList<Value>(Value.class, this,
					Interpreter_vmPackage.LIST_VALUE__OWNED_VALUES);
		}
		return ownedValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Interpreter_vmPackage.LIST_VALUE__OWNED_VALUES:
			return ((InternalEList<?>) getOwnedValues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getName() {
		return EcoreUtil.getURI(this).fragment();
		//		return "["
		//				+ this.getOwnedValues().stream().map(arg -> arg.getName()).collect(Collectors.joining(", "))
		//				+ "]";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Interpreter_vmPackage.LIST_VALUE__OWNED_VALUES:
			return getOwnedValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Interpreter_vmPackage.LIST_VALUE__OWNED_VALUES:
			getOwnedValues().clear();
			getOwnedValues().addAll((Collection<? extends Value>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.LIST_VALUE__OWNED_VALUES:
			getOwnedValues().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.LIST_VALUE__OWNED_VALUES:
			return ownedValues != null && !ownedValues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ListValueImpl
