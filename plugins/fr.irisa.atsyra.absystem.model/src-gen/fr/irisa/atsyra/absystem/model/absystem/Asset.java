/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getAssetType <em>Asset Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getAssetAttributeValues <em>Asset Attribute Values</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getFeaturesMap <em>Features Map</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getDescriptionFormat <em>Description Format</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAsset()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='AllMandatoryStaticFeaturesSet TypeIsNotAbstract'"
 * @generated
 */
public interface Asset extends Symbol, AssetGroupContent {
	/**
	 * Returns the value of the '<em><b>Asset Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Type</em>' reference.
	 * @see #setAssetType(AssetType)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAsset_AssetType()
	 * @model required="true"
	 * @generated
	 */
	AssetType getAssetType();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getAssetType <em>Asset Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asset Type</em>' reference.
	 * @see #getAssetType()
	 * @generated
	 */
	void setAssetType(AssetType value);

	/**
	 * Returns the value of the '<em><b>Asset Attribute Values</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset Attribute Values</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Attribute Values</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAsset_AssetAttributeValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<AssetAttributeValue> getAssetAttributeValues();

	/**
	 * Returns the value of the '<em><b>Features Map</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features Map</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAsset_FeaturesMap()
	 * @model containment="true"
	 *        annotation="aspect"
	 * @generated
	 */
	EList<AssetFeatureValueEntry> getFeaturesMap();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAsset_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Description Format</b></em>' attribute.
	 * The default value is <code>"plaintext"</code>.
	 * The literals are from the enumeration {@link fr.irisa.atsyra.absystem.model.absystem.TextFormat}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #setDescriptionFormat(TextFormat)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAsset_DescriptionFormat()
	 * @model default="plaintext"
	 * @generated
	 */
	TextFormat getDescriptionFormat();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Asset#getDescriptionFormat <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #getDescriptionFormat()
	 * @generated
	 */
	void setDescriptionFormat(TextFormat value);

} // Asset
