/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getTitle <em>Title</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getDescriptionFormat <em>Description Format</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getContracts <em>Contracts</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='ContractsShouldBeStatic'"
 * @generated
 */
public interface Requirement extends Annotated, Definition {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirement_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirement_Title()
	 * @model
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirement_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Description Format</b></em>' attribute.
	 * The default value is <code>"plaintext"</code>.
	 * The literals are from the enumeration {@link fr.irisa.atsyra.absystem.model.absystem.TextFormat}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #setDescriptionFormat(TextFormat)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirement_DescriptionFormat()
	 * @model default="plaintext"
	 * @generated
	 */
	TextFormat getDescriptionFormat();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement#getDescriptionFormat <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #getDescriptionFormat()
	 * @generated
	 */
	void setDescriptionFormat(TextFormat value);

	/**
	 * Returns the value of the '<em><b>Contracts</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Contract}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contracts</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getRequirement_Contracts()
	 * @model keys="id"
	 * @generated
	 */
	EList<Contract> getContracts();

} // Requirement
