/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Attribute Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#getAttributeType <em>Attribute Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#getValues <em>Values</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#isCollection <em>Collection</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetAttributeValue()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='NoDynamicAttributeSet NoDuplicateAttribute CollectionMatchesMultiplicity'"
 * @generated
 */
public interface AssetAttributeValue extends EObject {
	/**
	 * Returns the value of the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Type</em>' reference.
	 * @see #setAttributeType(AssetTypeAttribute)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetAttributeValue_AttributeType()
	 * @model
	 * @generated
	 */
	AssetTypeAttribute getAttributeType();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#getAttributeType <em>Attribute Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Type</em>' reference.
	 * @see #getAttributeType()
	 * @generated
	 */
	void setAttributeType(AssetTypeAttribute value);

	/**
	 * Returns the value of the '<em><b>Values</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.ConstantExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetAttributeValue_Values()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ConstantExpression> getValues();

	/**
	 * Returns the value of the '<em><b>Collection</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection</em>' attribute.
	 * @see #setCollection(boolean)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetAttributeValue_Collection()
	 * @model default="false"
	 * @generated
	 */
	boolean isCollection();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue#isCollection <em>Collection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection</em>' attribute.
	 * @see #isCollection()
	 * @generated
	 */
	void setCollection(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/Ecore invariant='true'"
	 * @generated
	 */
	boolean onlyCollectionsHaveManyValues(DiagnosticChain diagnostics, Map<Object, Object> context);

} // AssetAttributeValue
