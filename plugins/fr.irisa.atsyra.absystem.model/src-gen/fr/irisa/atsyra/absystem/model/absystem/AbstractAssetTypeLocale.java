/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Asset Type Locale</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetTypeLocale#getFeatures <em>Features</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAbstractAssetTypeLocale()
 * @model abstract="true"
 * @generated
 */
public interface AbstractAssetTypeLocale extends ABSObjectLocale {
	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAbstractAssetTypeLocale_Features()
	 * @model containment="true"
	 * @generated
	 */
	EList<AssetTypeFeatureLocale> getFeatures();

} // AbstractAssetTypeLocale
