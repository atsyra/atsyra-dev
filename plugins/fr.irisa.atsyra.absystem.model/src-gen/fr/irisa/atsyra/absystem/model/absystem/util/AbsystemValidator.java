/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import fr.irisa.atsyra.absystem.model.absystem.*;
import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Action;
import fr.irisa.atsyra.absystem.model.absystem.ActionEnum;
import fr.irisa.atsyra.absystem.model.absystem.AndExpression;
import fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry;
import fr.irisa.atsyra.absystem.model.absystem.AnnotationKey;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup;
import fr.irisa.atsyra.absystem.model.absystem.AssetLink;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.BinaryExpression;
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant;
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression;
import fr.irisa.atsyra.absystem.model.absystem.Contract;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.EnumConstant;
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression;
import fr.irisa.atsyra.absystem.model.absystem.Expression;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.Guard;
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter;
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;
import fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression;
import fr.irisa.atsyra.absystem.model.absystem.Import;
import fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression;
import fr.irisa.atsyra.absystem.model.absystem.IntConstant;
import fr.irisa.atsyra.absystem.model.absystem.LambdaAction;
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression;
import fr.irisa.atsyra.absystem.model.absystem.LambdaParameter;
import fr.irisa.atsyra.absystem.model.absystem.Member;
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection;
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity;
import fr.irisa.atsyra.absystem.model.absystem.NotExpression;
import fr.irisa.atsyra.absystem.model.absystem.OrExpression;
import fr.irisa.atsyra.absystem.model.absystem.Parameter;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;
import fr.irisa.atsyra.absystem.model.absystem.Severity;
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod;
import fr.irisa.atsyra.absystem.model.absystem.StringConstant;
import fr.irisa.atsyra.absystem.model.absystem.Symbol;
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef;
import fr.irisa.atsyra.absystem.model.absystem.Tag;
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant;
import fr.irisa.atsyra.absystem.model.absystem.Version;
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant;

/**
 * <!-- begin-user-doc --> The <b>Validator</b> for the model. <!-- end-user-doc
 * -->
 * 
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
 * @generated
 */
public class AbsystemValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final AbsystemValidator INSTANCE = new AbsystemValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "fr.irisa.atsyra.absystem.model.absystem";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Only Collections Have Many Values' of 'Asset Attribute Value'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ASSET_ATTRIBUTE_VALUE__ONLY_COLLECTIONS_HAVE_MANY_VALUES = 1;

	/**
	 * A constant with a fixed name that can be used as the base value for
	 * additional hand written constants. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 1;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	public static final int ASSET_LINK__IS_TARGET_WELL_TYPED = 1;
	public static final int ASSET_LINK__IS_SOURCE_WELL_TYPED = 2;
	public static final int ASSET_TYPE__NO_EXTENDS_CYCLE = 3;
	public static final int ASSET_TYPE_REFERENCE__DEFAULT_MULTIPLICITY = 4;
	public static final int ASSET__ALL_MANDATORY_FEATURES_SET = 5;
	public static final int ASSET_TYPE_ATTRIBUTE__DEFAULT_TYPE = 6;
	public static final int ASSET_TYPE_ATTRIBUTE__DEFAULT_MULTIPLICITY = 7;
	public static final int PRIMITIVE_DATA_TYPE__KNOWN_NAME = 8;
	public static final int ASSET_ATTRIBUTE_VALUE__NO_DYNAMIC_ATTRIBUTE_SET = 9;
	public static final int GUARDED_ACTION__SHOULD_HAVE_ACTIONS = 10;
	public static final int EQUALITY_COMPARISON_EXPRESSION__NO_COLLECTIONS = 11;
	public static final int EQUALITY_COMPARISON_EXPRESSION__COMPARABLE_TYPES = 12;
	public static final int INEQUALITY_COMPARISON_EXPRESSION__NO_COLLECTIONS = 13;
	public static final int INEQUALITY_COMPARISON_EXPRESSION__COMPARABLE_TYPES = 14;
	public static final int ACTION__TARGET_MULTIPLICITY = 15;
	public static final int ACTION__ARG_MULTIPLICITY = 16;
	public static final int ACTION__NO_UNDEFINED_CONSTANT = 17;
	public static final int MEMBER_SELECTION__RECEIVER_MULTIPLICITY = 18;
	public static final int MEMBER_SELECTION__ARG_MULTIPLICITY = 19;
	public static final int MEMBER_SELECTION__INVOCATION_IS_METHOD = 20;
	public static final int MEMBER_SELECTION__NO_UNDEFINED_CONSTANT = 21;
	public static final int SYMBOL_REF__IN_SCOPE = 22;
	public static final int STATIC_METHOD__KNOWN_NAME = 23;
	public static final int EXPRESSION__WRONG_TYPE = 24;
	public static final int LOCALIZATION__ILL_FORMED = 25;
	public static final int ASSET__TYPE_IS_NOT_ABSTRACT = 26;
	public static final int ASSET_LINK__RESPECTS_MULTIPLICITY = 27;
	public static final int ASSET_TYPE_FEATURE__SHADOWING = 28;
	public static final int ASSET_ATTRIBUTE_VALUE__NO_DUPLICATE_ATTRIBUTE = 29;
	public static final int ASSET_ATTRIBUTE_VALUE__COLLECTION_MATCHES_MULTIPLICITY = 30;
	public static final int REQUIREMENT__CONTRACTS_SOULD_BE_STATIC = 30;

	public static final String PRIMITIVEDATATYPE_BOOLEAN_NAME = "Boolean";
	public static final String PRIMITIVEDATATYPE_INTEGER_NAME = "Integer";
	public static final String PRIMITIVEDATATYPE_STRING_NAME = "String";
	public static final String PRIMITIVEDATATYPE_VERSION_NAME = "Version";

	public static final String STATICMETHOD_CONTAINS_NAME = "contains";
	public static final String STATICMETHOD_CONTAINSALL_NAME = "containsAll";
	public static final String STATICMETHOD_CONTAINSANY_NAME = "containsAny";
	public static final String STATICMETHOD_FILTER_NAME = "filter";
	public static final String STATICMETHOD_ISEMPTY_NAME = "isEmpty";

	/**
	 * Creates an instance of the switch. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	public AbsystemValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return AbsystemPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
		case AbsystemPackage.ASSET_TYPE:
			return validateAssetType((AssetType) value, diagnostics, context);
		case AbsystemPackage.ASSET_TYPE_REFERENCE:
			return validateAssetTypeReference((AssetTypeReference) value, diagnostics, context);
		case AbsystemPackage.ASSET_BASED_SYSTEM:
			return validateAssetBasedSystem((AssetBasedSystem) value, diagnostics, context);
		case AbsystemPackage.ASSET:
			return validateAsset((Asset) value, diagnostics, context);
		case AbsystemPackage.ASSET_LINK:
			return validateAssetLink((AssetLink) value, diagnostics, context);
		case AbsystemPackage.ASSET_TYPE_ATTRIBUTE:
			return validateAssetTypeAttribute((AssetTypeAttribute) value, diagnostics, context);
		case AbsystemPackage.ASSET_TYPE_FEATURE:
			return validateAssetTypeFeature((AssetTypeFeature) value, diagnostics, context);
		case AbsystemPackage.PRIMITIVE_DATA_TYPE:
			return validatePrimitiveDataType((PrimitiveDataType) value, diagnostics, context);
		case AbsystemPackage.ENUM_DATA_TYPE:
			return validateEnumDataType((EnumDataType) value, diagnostics, context);
		case AbsystemPackage.ENUM_LITERAL:
			return validateEnumLiteral((EnumLiteral) value, diagnostics, context);
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE:
			return validateAssetAttributeValue((AssetAttributeValue) value, diagnostics, context);
		case AbsystemPackage.DEFINITION_GROUP:
			return validateDefinitionGroup((DefinitionGroup) value, diagnostics, context);
		case AbsystemPackage.ASSET_GROUP:
			return validateAssetGroup((AssetGroup) value, diagnostics, context);
		case AbsystemPackage.IMPORT:
			return validateImport((Import) value, diagnostics, context);
		case AbsystemPackage.TAG:
			return validateTag((Tag) value, diagnostics, context);
		case AbsystemPackage.ABSTRACT_ASSET_TYPE:
			return validateAbstractAssetType((AbstractAssetType) value, diagnostics, context);
		case AbsystemPackage.ASSET_TYPE_ASPECT:
			return validateAssetTypeAspect((AssetTypeAspect) value, diagnostics, context);
		case AbsystemPackage.GUARDED_ACTION:
			return validateGuardedAction((GuardedAction) value, diagnostics, context);
		case AbsystemPackage.GUARD_PARAMETER:
			return validateGuardParameter((GuardParameter) value, diagnostics, context);
		case AbsystemPackage.EXPRESSION:
			return validateExpression((Expression) value, diagnostics, context);
		case AbsystemPackage.BINARY_EXPRESSION:
			return validateBinaryExpression((BinaryExpression) value, diagnostics, context);
		case AbsystemPackage.IMPLIES_EXPRESSION:
			return validateImpliesExpression((ImpliesExpression) value, diagnostics, context);
		case AbsystemPackage.OR_EXPRESSION:
			return validateOrExpression((OrExpression) value, diagnostics, context);
		case AbsystemPackage.AND_EXPRESSION:
			return validateAndExpression((AndExpression) value, diagnostics, context);
		case AbsystemPackage.NOT_EXPRESSION:
			return validateNotExpression((NotExpression) value, diagnostics, context);
		case AbsystemPackage.EQUALITY_COMPARISON_EXPRESSION:
			return validateEqualityComparisonExpression((EqualityComparisonExpression) value, diagnostics, context);
		case AbsystemPackage.INEQUALITY_COMPARISON_EXPRESSION:
			return validateInequalityComparisonExpression((InequalityComparisonExpression) value, diagnostics, context);
		case AbsystemPackage.ACTION:
			return validateAction((Action) value, diagnostics, context);
		case AbsystemPackage.CONSTANT_EXPRESSION:
			return validateConstantExpression((ConstantExpression) value, diagnostics, context);
		case AbsystemPackage.STRING_CONSTANT:
			return validateStringConstant((StringConstant) value, diagnostics, context);
		case AbsystemPackage.INT_CONSTANT:
			return validateIntConstant((IntConstant) value, diagnostics, context);
		case AbsystemPackage.BOOLEAN_CONSTANT:
			return validateBooleanConstant((BooleanConstant) value, diagnostics, context);
		case AbsystemPackage.VERSION_CONSTANT:
			return validateVersionConstant((VersionConstant) value, diagnostics, context);
		case AbsystemPackage.MEMBER_SELECTION:
			return validateMemberSelection((MemberSelection) value, diagnostics, context);
		case AbsystemPackage.MEMBER:
			return validateMember((Member) value, diagnostics, context);
		case AbsystemPackage.SYMBOL_REF:
			return validateSymbolRef((SymbolRef) value, diagnostics, context);
		case AbsystemPackage.SYMBOL:
			return validateSymbol((Symbol) value, diagnostics, context);
		case AbsystemPackage.STATIC_METHOD:
			return validateStaticMethod((StaticMethod) value, diagnostics, context);
		case AbsystemPackage.LAMBDA_PARAMETER:
			return validateLambdaParameter((LambdaParameter) value, diagnostics, context);
		case AbsystemPackage.LAMBDA_EXPRESSION:
			return validateLambdaExpression((LambdaExpression) value, diagnostics, context);
		case AbsystemPackage.LAMBDA_ACTION:
			return validateLambdaAction((LambdaAction) value, diagnostics, context);
		case AbsystemPackage.PARAMETER:
			return validateParameter((Parameter) value, diagnostics, context);
		case AbsystemPackage.ANNOTATION_ENTRY:
			return validateAnnotationEntry((AnnotationEntry) value, diagnostics, context);
		case AbsystemPackage.ANNOTATION_KEY:
			return validateAnnotationKey((AnnotationKey) value, diagnostics, context);
		case AbsystemPackage.GOAL:
			return validateGoal((Goal) value, diagnostics, context);
		case AbsystemPackage.CONTRACT:
			return validateContract((Contract) value, diagnostics, context);
		case AbsystemPackage.GUARD:
			return validateGuard((Guard) value, diagnostics, context);
		case AbsystemPackage.ENUM_CONSTANT:
			return validateEnumConstant((EnumConstant) value, diagnostics, context);
		case AbsystemPackage.UNDEFINED_CONSTANT:
			return validateUndefinedConstant((UndefinedConstant) value, diagnostics, context);
		case AbsystemPackage.COLLECTION:
			return validateCollection((Collection) value, diagnostics, context);
		case AbsystemPackage.DEFINITION:
			return validateDefinition((Definition) value, diagnostics, context);
		case AbsystemPackage.ASSET_GROUP_CONTENT:
			return validateAssetGroupContent((AssetGroupContent) value, diagnostics, context);
		case AbsystemPackage.LOCALE_GROUP:
			return validateLocaleGroup((LocaleGroup) value, diagnostics, context);
		case AbsystemPackage.ABS_OBJECT_LOCALE:
			return validateABSObjectLocale((ABSObjectLocale) value, diagnostics, context);
		case AbsystemPackage.DEFINITION_GROUP_LOCALE:
			return validateDefinitionGroupLocale((DefinitionGroupLocale) value, diagnostics, context);
		case AbsystemPackage.PRIMITIVE_DATA_TYPE_LOCALE:
			return validatePrimitiveDataTypeLocale((PrimitiveDataTypeLocale) value, diagnostics, context);
		case AbsystemPackage.ENUM_LITERAL_LOCALE:
			return validateEnumLiteralLocale((EnumLiteralLocale) value, diagnostics, context);
		case AbsystemPackage.ABSTRACT_ASSET_TYPE_LOCALE:
			return validateAbstractAssetTypeLocale((AbstractAssetTypeLocale) value, diagnostics, context);
		case AbsystemPackage.ASSET_TYPE_LOCALE:
			return validateAssetTypeLocale((AssetTypeLocale) value, diagnostics, context);
		case AbsystemPackage.ASSET_TYPE_ASPECT_LOCALE:
			return validateAssetTypeAspectLocale((AssetTypeAspectLocale) value, diagnostics, context);
		case AbsystemPackage.ASSET_TYPE_FEATURE_LOCALE:
			return validateAssetTypeFeatureLocale((AssetTypeFeatureLocale) value, diagnostics, context);
		case AbsystemPackage.GUARD_LOCALE:
			return validateGuardLocale((GuardLocale) value, diagnostics, context);
		case AbsystemPackage.ANNOTATED:
			return validateAnnotated((Annotated) value, diagnostics, context);
		case AbsystemPackage.REQUIREMENT:
			return validateRequirement((Requirement) value, diagnostics, context);
		case AbsystemPackage.REQUIREMENT_LOCALE:
			return validateRequirementLocale((RequirementLocale) value, diagnostics, context);
		case AbsystemPackage.ACTION_ENUM:
			return validateActionEnum((ActionEnum) value, diagnostics, context);
		case AbsystemPackage.MULTIPLICITY:
			return validateMultiplicity((Multiplicity) value, diagnostics, context);
		case AbsystemPackage.SEVERITY:
			return validateSeverity((Severity) value, diagnostics, context);
		case AbsystemPackage.TEXT_FORMAT:
			return validateTextFormat((TextFormat) value, diagnostics, context);
		case AbsystemPackage.EVERSION:
			return validateEVersion((Version) value, diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetType(AssetType assetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(assetType, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(assetType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(assetType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(assetType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(assetType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(assetType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(assetType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(assetType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(assetType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetType_NoExtendsCycle(assetType, diagnostics, context);
		return result;
	}

	/**
	 * Validates the NoExtendsCycle constraint of '<em>Asset Type</em>'. <!--
	 * begin-user-doc --> Produces an error when there is a cycle in the
	 * dependencies between AssetTypes by only going up following extension. <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAssetType_NoExtendsCycle(AssetType assetType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		Optional<List<AssetType>> extendsCycle = findCycle(assetType);
		if (extendsCycle.isPresent()) {
			if (diagnostics != null) {
				StringJoiner joiner = new StringJoiner(" -> ", "{ ", " }");
				extendsCycle.orElseThrow().stream().map(AssetType::getName).forEachOrdered(joiner::add);
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ASSET_TYPE__NO_EXTENDS_CYCLE,
						"_validation__asset_type__no_extends_cycle", new Object[] { joiner.toString() },
						new Object[] { assetType, extendsCycle.orElseThrow() }, context));
			}
			return false;
		}
		return true;
	}

	private Optional<List<AssetType>> findCycle(AssetType source) {
		Map<AssetType, AssetType> predecessor = new HashMap<>();
		Set<AssetType> visited = new HashSet<>();
		Set<AssetType> toVisit = new HashSet<>();
		toVisit.add(source);
		AssetType cycleStart = source;
		boolean found = false;
		while (!found && !toVisit.isEmpty()) {
			// breadth first to get a shortest cycle
			Set<AssetType> next = new HashSet<>();
			for (AssetType t : toVisit) {
				for (AssetType extend : t.getExtends()) {
					predecessor.put(extend, t);
					if (visited.contains(extend)) {
						cycleStart = extend;
						found = true;
					} else {
						next.add(extend);
					}
				}
			}
			visited.addAll(toVisit);
			toVisit.clear();
			toVisit.addAll(next);
		}
		if (found) {
			AssetType head = cycleStart;
			List<AssetType> cycle = new ArrayList<>();
			cycle.add(head);
			head = predecessor.get(head);
			while (head != cycleStart) {
				if (head == null)
					throw new NullPointerException();
				cycle.add(head);
				head = predecessor.get(head);
			}
			cycle.add(head);
			Collections.reverse(cycle);
			return Optional.of(cycle);
		}
		return Optional.empty();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetTypeReference(AssetTypeReference assetTypeReference, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(assetTypeReference, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(assetTypeReference, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(assetTypeReference, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(assetTypeReference, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(assetTypeReference, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(assetTypeReference, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(assetTypeReference, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(assetTypeReference, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(assetTypeReference, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetTypeFeature_MustNotShadow(assetTypeReference, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetTypeReference_DefaultMultiplicity(assetTypeReference, diagnostics, context);
		return result;
	}

	/**
	 * Validates the DefaultMultiplicity constraint of '<em>Asset Type
	 * Reference</em>'. <!-- begin-user-doc --> Produces an error when the
	 * multiplicity of the default values is not the same as the multiplicity of the
	 * AssetTypeReference. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAssetTypeReference_DefaultMultiplicity(AssetTypeReference assetTypeReference,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (assetTypeReference.isHasDefault()
				&& Objects.equals(assetTypeReference.getMultiplicity(), Multiplicity.ONE)) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_TYPE_REFERENCE__DEFAULT_MULTIPLICITY,
						"_validation__asset_type_reference__no_undefined_default_for_one", null,
						new Object[] { assetTypeReference }, context));
			}
			return false;
		} else if (assetTypeReference.isHasDefault()
				&& Objects.equals(assetTypeReference.getMultiplicity(), Multiplicity.ONE_OR_MANY)) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_TYPE_REFERENCE__DEFAULT_MULTIPLICITY,
						"_validation__asset_type_reference__no_empty_default_for_one_or_many", null,
						new Object[] { assetTypeReference }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetBasedSystem(AssetBasedSystem assetBasedSystem, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assetBasedSystem, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAsset(Asset asset, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(asset, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(asset, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(asset, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(asset, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(asset, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(asset, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(asset, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(asset, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(asset, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAsset_AllMandatoryStaticFeaturesSet(asset, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAsset_TypeIsNotAbstract(asset, diagnostics, context);
		return result;
	}

	/**
	 * Validates the AllMandatoryStaticFeaturesSet constraint of '<em>Asset</em>'.
	 * <!-- begin-user-doc --> Produces a warning when there is a mandatory
	 * attribute of the asset that is not set in an AssetAttributeValue, or a
	 * mandatory reference that is not set by any AssetLink. Produces an error when
	 * an mandatory feature is set to undefined by default. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAsset_AllMandatoryStaticFeaturesSet(Asset asset, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean result = true;
		for (AssetType supertype : ABSUtils.getAllSupertypes(asset.getAssetType())) {
			for (AssetTypeAttribute attribute : supertype.getAssetTypeAttributes()) {
				if (ABSUtils.isMandatory(attribute.getMultiplicity()) && !attribute.isHasDefault()
						&& asset.getAssetAttributeValues().stream()
								.noneMatch(attVal -> attVal.getAttributeType() == attribute)) {
					if (diagnostics != null) {
						diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
								ASSET__ALL_MANDATORY_FEATURES_SET,
								"_validation__asset__missing_mandatory_static_attribute", new Object[] { attribute },
								new Object[] { asset, attribute }, context));
					}
					result = false;
				} else if (ABSUtils.isMandatory(attribute.getMultiplicity()) && asset.getAssetAttributeValues().stream()
						.anyMatch(attVal -> attVal.getAttributeType() == attribute
								&& attVal.getValues().stream().anyMatch(UndefinedConstant.class::isInstance))) {
					if (diagnostics != null) {
						diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
								ASSET__ALL_MANDATORY_FEATURES_SET,
								"_validation__asset__wrong_mandatory_static_attribute", new Object[] { attribute },
								new Object[] { asset, attribute }, context));
					}
					result = false;
				}
			}
			for (AssetTypeReference reference : supertype.getAssetTypeProperties()) {
				if (ABSUtils.isMandatory(reference.getMultiplicity()) && !reference.isHasDefault() && !Iterators.any(
						Iterators.filter(asset.eResource().getResourceSet().getAllContents(), AssetLink.class),
						link -> (link.getReferenceType() == reference && link.getSourceAsset() == asset)
								|| (link.getOppositeReferenceType() == reference && link.getTargetAsset() == asset))) {
					if (diagnostics != null) {
						diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
								ASSET__ALL_MANDATORY_FEATURES_SET,
								"_validation__asset__missing_mandatory_static_reference", new Object[] { reference },
								new Object[] { asset, reference }, context));
					}
					result = false;
				}
			}
		}
		return result;
	}

	/**
	 * Validates the TypeIsNotAbstract constraint of '<em>Asset</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAsset_TypeIsNotAbstract(Asset asset, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (asset.getAssetType() != null && asset.getAssetType().isAbstract()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ASSET__TYPE_IS_NOT_ABSTRACT,
						"_validation__asset__type_is_not_abstract", new Object[] { asset.getAssetType() },
						new Object[] { asset }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetLink(AssetLink assetLink, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(assetLink, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetLink_WellTypedSource(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetLink_WellTypedTarget(assetLink, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetLink_RespectMultiplicity(assetLink, diagnostics, context);
		return result;
	}

	/**
	 * Validates the WellTypedSource constraint of '<em>Asset Link</em>'..
	 * <!--begin-user-doc --> Produces an error when the source asset of an
	 * AssetLink is of a type incompatible with the type of the container of the
	 * reference. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAssetLink_WellTypedSource(AssetLink assetLink, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (assetLink.getSourceAsset().getAssetType() != null && assetLink.getReferenceType() != null
				&& !ABSUtils.getAllAssetTypeProperties(assetLink.getSourceAsset().getAssetType())
						.contains(assetLink.getReferenceType())) {
			if (diagnostics != null) {
				diagnostics
						.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ASSET_LINK__IS_SOURCE_WELL_TYPED,
								"_validation__asset_link__wrong_source", null, new Object[] { assetLink }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the WellTypedTarget constraint of '<em>Asset Link</em>'. <!--
	 * begin-user-doc --> Produces an error when the target asset of an AssetLink is
	 * of a type incompatible with the type of the reference. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAssetLink_WellTypedTarget(AssetLink assetLink, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (assetLink.getTargetAsset().getAssetType() != null && assetLink.getReferenceType() != null
				&& !ABSUtils.getAllSupertypes(assetLink.getTargetAsset().getAssetType())
						.contains(assetLink.getReferenceType().getPropertyType())) {
			if (diagnostics != null) {
				diagnostics
						.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ASSET_LINK__IS_TARGET_WELL_TYPED,
								"_validation__asset_link__wrong_target", null, new Object[] { assetLink }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the RespectMultiplicity constraint of '<em>Asset Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateAssetLink_RespectMultiplicity(AssetLink assetLink, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (assetLink.getReferenceType() == null) {
			// type is not set, should be checked by another validator 
			return true;
		}
		if (ABSUtils.isMany(assetLink.getReferenceType().getMultiplicity())
				&& (assetLink.getOppositeReferenceType() == null
						|| ABSUtils.isMany(assetLink.getOppositeReferenceType().getMultiplicity()))) {
			return true;
		}
		List<AssetLink> allLinkForTheReference = new ArrayList<>();
		List<AssetLink> allLinkForTheOppositeReference = new ArrayList<>();
		assetLink.eResource().getResourceSet().getAllContents().forEachRemaining(content -> {
			if (content instanceof AssetLink) {
				AssetLink link = (AssetLink) content;
				if (link.getReferenceType() == assetLink.getReferenceType()
						&& link.getSourceAsset() == assetLink.getSourceAsset()
						|| link.getOppositeReferenceType() == assetLink.getReferenceType()
								&& link.getTargetAsset() == assetLink.getSourceAsset()) {
					allLinkForTheReference.add(link);
				}
				if (assetLink.getOppositeReferenceType() != null
						&& (link.getReferenceType() == assetLink.getOppositeReferenceType()
								&& link.getSourceAsset() == assetLink.getTargetAsset()
								|| link.getOppositeReferenceType() == assetLink.getOppositeReferenceType()
										&& link.getTargetAsset() == assetLink.getTargetAsset())) {
					allLinkForTheOppositeReference.add(link);
				}
			}
		});
		boolean result = true;
		if (!ABSUtils.isMany(assetLink.getReferenceType().getMultiplicity()) && allLinkForTheReference.size() > 1) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_LINK__RESPECTS_MULTIPLICITY, "_validation__asset_link__no_multiple_links_for_simple",
						new Object[] { assetLink.getReferenceType() }, new Object[] { assetLink }, context));
			}
			result = false;
		}
		if (assetLink.getOppositeReferenceType() != null
				&& !ABSUtils.isMany(assetLink.getOppositeReferenceType().getMultiplicity())
				&& allLinkForTheOppositeReference.size() > 1) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_LINK__RESPECTS_MULTIPLICITY, "_validation__asset_link__no_multiple_links_for_simple",
						new Object[] { assetLink.getOppositeReferenceType() }, new Object[] { assetLink }, context));
			}
			result = false;
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetTypeAttribute(AssetTypeAttribute assetTypeAttribute, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(assetTypeAttribute, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetTypeFeature_MustNotShadow(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetTypeAttribute_DefaultType(assetTypeAttribute, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetTypeAttribute_DefaultMultiplicity(assetTypeAttribute, diagnostics, context);
		return result;
	}

	/**
	 * Validates the DefaultType constraint of '<em>Asset Type Attribute</em>'. <!--
	 * begin-user-doc --> Produces an error when the default value for an
	 * AssetTypeAttribute is of a type that is incompatible with the type of the
	 * AssetTypeAttribute. Also Produces an error when the default values of a
	 * Collection attribute contains an UndefinedConstant. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAssetTypeAttribute_DefaultType(AssetTypeAttribute assetTypeAttribute,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = true;
		for (ConstantExpression defaultvalue : assetTypeAttribute.getDefaultValues()) {
			if (!ABSUtils.isOfType(defaultvalue, assetTypeAttribute.getAttributeType())) {
				if (diagnostics != null) {
					ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
							(AssetBasedSystem) EcoreUtil.getRootContainer(assetTypeAttribute));
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
							ASSET_TYPE_ATTRIBUTE__DEFAULT_TYPE, "_validation__asset_type_attribute__default_wrong_type",
							new Object[] { assetTypeAttribute.getAttributeType(),
									typeProvider.getTypeOf(defaultvalue).getType() },
							new Object[] { assetTypeAttribute, defaultvalue }, context));
				}
				result = false;
			}
			if (ABSUtils.isMany(assetTypeAttribute.getMultiplicity()) && defaultvalue instanceof UndefinedConstant) {
				if (diagnostics != null) {
					diagnostics.add(
							createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ASSET_TYPE_ATTRIBUTE__DEFAULT_TYPE,
									"_validation__asset_type_attribute__default_collection_not_undefined", null,
									new Object[] { assetTypeAttribute, defaultvalue }, context));
				}
				result = false;
			}
		}
		return result;
	}

	/**
	 * Validates the DefaultMultiplicity constraint of '<em>Asset Type
	 * Attribute</em>'. <!-- begin-user-doc --> Produces an error when the
	 * multiplicity of the default values is not compatible with the multiplicity of
	 * the AssetTypeAttribute. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAssetTypeAttribute_DefaultMultiplicity(AssetTypeAttribute assetTypeAttribute,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = true;
		if (assetTypeAttribute.isHasDefault() && !ABSUtils.isMany(assetTypeAttribute.getMultiplicity())
				&& assetTypeAttribute.getDefaultValues().size() > 1) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_TYPE_ATTRIBUTE__DEFAULT_MULTIPLICITY,
						"_validation__asset_type_attribute__default_not_collection", null,
						new Object[] { assetTypeAttribute }, context));
			}
			result = false;
		} else if (assetTypeAttribute.isHasDefault() && !ABSUtils.isMany(assetTypeAttribute.getMultiplicity())
				&& assetTypeAttribute.getDefaultValues().isEmpty()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_TYPE_ATTRIBUTE__DEFAULT_MULTIPLICITY,
						"_validation__asset_type_attribute__default_missing_simple", null,
						new Object[] { assetTypeAttribute }, context));
			}
			result = false;
		} else if (assetTypeAttribute.isHasDefault()
				&& Objects.equals(assetTypeAttribute.getMultiplicity(), Multiplicity.ONE_OR_MANY)
				&& assetTypeAttribute.getDefaultValues().isEmpty()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_TYPE_ATTRIBUTE__DEFAULT_MULTIPLICITY,
						"_validation__asset_type_attribute__default_not_empty_for_one_or_many", null,
						new Object[] { assetTypeAttribute }, context));
			}
			result = false;
		} else if (assetTypeAttribute.isHasDefault()
				&& Objects.equals(assetTypeAttribute.getMultiplicity(), Multiplicity.ONE)
				&& !assetTypeAttribute.getDefaultValues().isEmpty()
				&& assetTypeAttribute.getDefaultValues().get(0) instanceof UndefinedConstant) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_TYPE_ATTRIBUTE__DEFAULT_MULTIPLICITY,
						"_validation__asset_type_attribute__default_not_undefined_for_one", null,
						new Object[] { assetTypeAttribute }, context));
			}
			result = false;
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetTypeFeature(AssetTypeFeature assetTypeFeature, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(assetTypeFeature, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(assetTypeFeature, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(assetTypeFeature, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(assetTypeFeature, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(assetTypeFeature, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(assetTypeFeature, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(assetTypeFeature, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(assetTypeFeature, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(assetTypeFeature, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetTypeFeature_MustNotShadow(assetTypeFeature, diagnostics, context);
		return result;
	}

	/**
	 * Validates the MustNotShadow constraint of '<em>Asset Type Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateAssetTypeFeature_MustNotShadow(AssetTypeFeature assetTypeFeature,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = true;
		if (Objects.equals(assetTypeFeature.getName(), STATICMETHOD_CONTAINS_NAME)
				|| Objects.equals(assetTypeFeature.getName(), STATICMETHOD_CONTAINSALL_NAME)
				|| Objects.equals(assetTypeFeature.getName(), STATICMETHOD_CONTAINSANY_NAME)
				|| Objects.equals(assetTypeFeature.getName(), STATICMETHOD_FILTER_NAME)
				|| Objects.equals(assetTypeFeature.getName(), STATICMETHOD_ISEMPTY_NAME)) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ASSET_TYPE_FEATURE__SHADOWING,
						"_validation__asset_type_feature__must_not_shadow_method", null,
						new Object[] { assetTypeFeature }, context));
			}
			result = false;
		}
		AssetType containerType = ABSUtils.getContainerType(assetTypeFeature);
		for (AssetType supertype : ABSUtils.getSupertypesHierarchy(containerType)) {
			if (supertype == containerType)
				continue;
			for (AssetTypeFeature superfeature : Iterables.concat(supertype.getAssetTypeAttributes(),
					supertype.getAssetTypeProperties())) {
				if (Objects.equals(assetTypeFeature.getName(), superfeature.getName())) {
					if (diagnostics != null) {
						diagnostics.add(
								createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ASSET_TYPE_FEATURE__SHADOWING,
										"_validation__asset_type_feature__must_not_shadow_supertype",
										new Object[] { supertype }, new Object[] { assetTypeFeature }, context));
					}
					result = false;
				}
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePrimitiveDataType(PrimitiveDataType primitiveDataType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(primitiveDataType, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(primitiveDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(primitiveDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(primitiveDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(primitiveDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(primitiveDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(primitiveDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(primitiveDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(primitiveDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validatePrimitiveDataType_KnownName(primitiveDataType, diagnostics, context);
		return result;
	}

	/**
	 * Validates the KnownName constraint of '<em>Primitive Data Type</em>'. <!--
	 * begin-user-doc --> Produce an error when the name of the non-EnumDataType
	 * PrimitiveDataType is not known. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validatePrimitiveDataType_KnownName(PrimitiveDataType primitiveDataType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!(primitiveDataType instanceof EnumDataType)
				&& !(Objects.equals(primitiveDataType.getName(), PRIMITIVEDATATYPE_BOOLEAN_NAME)
						|| Objects.equals(primitiveDataType.getName(), PRIMITIVEDATATYPE_INTEGER_NAME)
						|| Objects.equals(primitiveDataType.getName(), PRIMITIVEDATATYPE_STRING_NAME)
						|| Objects.equals(primitiveDataType.getName(), PRIMITIVEDATATYPE_VERSION_NAME))) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						PRIMITIVE_DATA_TYPE__KNOWN_NAME, "_validation__primitive_data_type__unknown_name", null,
						new Object[] { primitiveDataType }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumDataType(EnumDataType enumDataType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(enumDataType, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(enumDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(enumDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(enumDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(enumDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(enumDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(enumDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(enumDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(enumDataType, diagnostics, context);
		if (result || diagnostics != null)
			result &= validatePrimitiveDataType_KnownName(enumDataType, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumLiteral(EnumLiteral enumLiteral, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumLiteral, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetAttributeValue(AssetAttributeValue assetAttributeValue, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(assetAttributeValue, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetAttributeValue_NoDynamicAttributeSet(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetAttributeValue_NoDuplicateAttribute(assetAttributeValue, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAssetAttributeValue_CollectionMatchesMultiplicity(assetAttributeValue, diagnostics,
					context);
		if (result || diagnostics != null)
			result &= validateAssetAttributeValue_onlyCollectionsHaveManyValues(assetAttributeValue, diagnostics,
					context);
		return result;
	}

	/**
	 * Validates the NoDynamicAttributeSet constraint of '<em>Asset Attribute
	 * Value</em>'. <!-- begin-user-doc --> Produces a warning when an attribute
	 * defined in an aspect is initialized as a static attribute. <!-- end-user-doc
	 * -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAssetAttributeValue_NoDynamicAttributeSet(AssetAttributeValue assetAttributeValue,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (assetAttributeValue.getAttributeType() != null
				&& assetAttributeValue.getAttributeType().eContainer() instanceof AssetTypeAspect) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.WARNING, DIAGNOSTIC_SOURCE,
						ASSET_ATTRIBUTE_VALUE__NO_DYNAMIC_ATTRIBUTE_SET,
						"_validation__asset_attribute_value__no_dynamic_attributes",
						new Object[] { assetAttributeValue.getAttributeType() }, new Object[] { assetAttributeValue },
						context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the NoDuplicateAttribute constraint of '<em>Asset Attribute Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateAssetAttributeValue_NoDuplicateAttribute(AssetAttributeValue assetAttributeValue,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		Asset asset = (Asset) assetAttributeValue.eContainer();
		if (asset.getAssetAttributeValues().stream().anyMatch(other -> other != assetAttributeValue
				&& other.getAttributeType() == assetAttributeValue.getAttributeType())) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_ATTRIBUTE_VALUE__NO_DUPLICATE_ATTRIBUTE,
						"_validation__asset_attribute_value__no_duplicate_attribute",
						new Object[] { assetAttributeValue.getAttributeType() }, new Object[] { assetAttributeValue },
						context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the CollectionMatchesMultiplicity constraint of '<em>Asset Attribute Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateAssetAttributeValue_CollectionMatchesMultiplicity(AssetAttributeValue assetAttributeValue,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (assetAttributeValue.getAttributeType() != null
				&& ABSUtils.isMany(assetAttributeValue.getAttributeType().getMultiplicity()) != assetAttributeValue
						.isCollection()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						ASSET_ATTRIBUTE_VALUE__NO_DUPLICATE_ATTRIBUTE,
						"_validation__asset_attribute_value__collection_matches_multiplicity",
						new Object[] { assetAttributeValue.getAttributeType() }, new Object[] { assetAttributeValue },
						context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the onlyCollectionsHaveManyValues constraint of '<em>Asset Attribute Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetAttributeValue_onlyCollectionsHaveManyValues(AssetAttributeValue assetAttributeValue,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return assetAttributeValue.onlyCollectionsHaveManyValues(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDefinitionGroup(DefinitionGroup definitionGroup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(definitionGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetGroup(AssetGroup assetGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assetGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateImport(Import import_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(import_, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTag(Tag tag, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(tag, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractAssetType(AbstractAssetType abstractAssetType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(abstractAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetTypeAspect(AssetTypeAspect assetTypeAspect, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assetTypeAspect, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGuardedAction(GuardedAction guardedAction, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(guardedAction, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(guardedAction, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(guardedAction, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(guardedAction, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(guardedAction, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(guardedAction, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(guardedAction, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(guardedAction, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(guardedAction, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateGuard_Type(guardedAction, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateGuardedAction_ShouldHaveActions(guardedAction, diagnostics, context);
		return result;
	}

	/**
	 * Validates the ShouldHaveActions constraint of '<em>Guarded Action</em>'. <!--
	 * begin-user-doc --> Produces a warning when a GuardedAction is missing
	 * actions. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateGuardedAction_ShouldHaveActions(GuardedAction guardedAction, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (guardedAction.getGuardActions().isEmpty()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.WARNING, DIAGNOSTIC_SOURCE,
						GUARDED_ACTION__SHOULD_HAVE_ACTIONS, "_validation__guarded_action__missing_actions", null,
						new Object[] { guardedAction }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGuardParameter(GuardParameter guardParameter, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(guardParameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExpression(Expression expression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(expression, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryExpression(BinaryExpression binaryExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(binaryExpression, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateImpliesExpression(ImpliesExpression impliesExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(impliesExpression, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(impliesExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(impliesExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(impliesExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(impliesExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(impliesExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(impliesExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(impliesExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(impliesExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateImpliesExpression_Type(impliesExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>Implies Expression</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateImpliesExpression_Type(ImpliesExpression impliesExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(impliesExpression));
		ExpressionType lhstype = typeProvider.getTypeOf(impliesExpression.getLhs());
		ExpressionType rhstype = typeProvider.getTypeOf(impliesExpression.getRhs());
		if (lhstype == null || rhstype == null) {
			return true;
		}
		if (!lhstype.isBooleanType() || !rhstype.isBooleanType()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
						"_validation__expression__require_boolean_operands",
						new Object[] { impliesExpression.eClass().getName() }, new Object[] { impliesExpression },
						context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOrExpression(OrExpression orExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(orExpression, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(orExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(orExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(orExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(orExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(orExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(orExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(orExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(orExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateOrExpression_Type(orExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>Or Expression</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateOrExpression_Type(OrExpression orExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(orExpression));
		ExpressionType lhstype = typeProvider.getTypeOf(orExpression.getLhs());
		ExpressionType rhstype = typeProvider.getTypeOf(orExpression.getRhs());
		if (lhstype == null || rhstype == null) {
			return true;
		}
		if (!lhstype.isBooleanType() || !rhstype.isBooleanType()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
						"_validation__expression__require_boolean_operands",
						new Object[] { orExpression.eClass().getName() }, new Object[] { orExpression }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAndExpression(AndExpression andExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(andExpression, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(andExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(andExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(andExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(andExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(andExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(andExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(andExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(andExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAndExpression_Type(andExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>And Expression</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAndExpression_Type(AndExpression andExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(andExpression));
		ExpressionType lhstype = typeProvider.getTypeOf(andExpression.getLhs());
		ExpressionType rhstype = typeProvider.getTypeOf(andExpression.getRhs());
		if (lhstype == null || rhstype == null) {
			return true;
		}
		if (!lhstype.isBooleanType() || !rhstype.isBooleanType()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
						"_validation__expression__require_boolean_operands",
						new Object[] { andExpression.eClass().getName() }, new Object[] { andExpression }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNotExpression(NotExpression notExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(notExpression, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(notExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(notExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(notExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(notExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(notExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(notExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(notExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(notExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateNotExpression_Type(notExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>Not Expression</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateNotExpression_Type(NotExpression notExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(notExpression));
		ExpressionType operandtype = typeProvider.getTypeOf(notExpression.getExpression());
		if (operandtype == null) {
			return true;
		}
		if (!operandtype.isBooleanType()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
						"_validation__expression__require_boolean_operand",
						new Object[] { notExpression.eClass().getName() }, new Object[] { notExpression }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEqualityComparisonExpression(EqualityComparisonExpression equalityComparisonExpression,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(equalityComparisonExpression, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(equalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(equalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(equalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(equalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(equalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(equalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(equalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(equalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateEqualityComparisonExpression_NoCollections(equalityComparisonExpression, diagnostics,
					context);
		if (result || diagnostics != null)
			result &= validateEqualityComparisonExpression_ComparableTypes(equalityComparisonExpression, diagnostics,
					context);
		return result;
	}

	/**
	 * Validates the NoCollections constraint of '<em>Equality Comparison
	 * Expression</em>'. <!-- begin-user-doc --> Produces an error when either the
	 * lhs of the rhs of an EqualityComparisonExpression is a collection <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateEqualityComparisonExpression_NoCollections(
			EqualityComparisonExpression equalityComparisonExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(equalityComparisonExpression));
		ExpressionType lhstype = typeProvider.getTypeOf(equalityComparisonExpression.getLhs());
		ExpressionType rhstype = typeProvider.getTypeOf(equalityComparisonExpression.getRhs());
		if (lhstype == null || rhstype == null) {
			return true;
		}
		if (lhstype.isCollection() || rhstype.isCollection()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						EQUALITY_COMPARISON_EXPRESSION__NO_COLLECTIONS,
						"_validation__equality_comparison_expression__no_collections",
						new Object[] { equalityComparisonExpression.getOp() },
						new Object[] { equalityComparisonExpression }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the ComparableTypes constraint of '<em>Equality Comparison
	 * Expression</em>'. <!-- begin-user-doc --> Produces an error when either the
	 * lhs and the rhs of an EqualityComparisonExpression are of incomparable types.
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateEqualityComparisonExpression_ComparableTypes(
			EqualityComparisonExpression equalityComparisonExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(equalityComparisonExpression));
		ExpressionType lhsType = typeProvider.getTypeOf(equalityComparisonExpression.getLhs());
		ExpressionType rhsType = typeProvider.getTypeOf(equalityComparisonExpression.getRhs());
		if (lhsType == null || rhsType == null) {
			return true;
		}
		if (!(lhsType.isUndefinedType() || rhsType.isUndefinedType())) {
			if (lhsType.isPrimitiveType() != rhsType.isPrimitiveType()) {
				if (diagnostics != null) {
					EObject assetType = lhsType.getType();
					EObject primitiveType = rhsType.getType();
					if (rhsType.isAssetType()) {
						assetType = rhsType.getType();
						primitiveType = lhsType.getType();
					}
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
							EQUALITY_COMPARISON_EXPRESSION__COMPARABLE_TYPES,
							"_validation__equality_comparison_expression__cannot_compare_assettype_and_primitivedatatype",
							new Object[] { assetType, primitiveType }, new Object[] { equalityComparisonExpression },
							context));
				}
				return false;
			}
			if (lhsType.isPrimitiveType() && rhsType.isPrimitiveType() && lhsType.getType() != rhsType.getType()) {
				if (diagnostics != null) {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
							EQUALITY_COMPARISON_EXPRESSION__COMPARABLE_TYPES,
							"_validation__equality_comparison_expression__cannot_compare_different_primitivedatatype",
							new Object[] { lhsType.getType(), rhsType.getType() },
							new Object[] { equalityComparisonExpression }, context));
				}
				return false;
			}
			if (lhsType.isAssetType() && rhsType.isAssetType()
					&& !ABSUtils.canShareConcreteAsset((AssetType) lhsType.getType(), (AssetType) rhsType.getType())) {
				if (diagnostics != null) {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
							EQUALITY_COMPARISON_EXPRESSION__COMPARABLE_TYPES,
							"_validation__equality_comparison_expression__cannot_compare_disjoint_assettype",
							new Object[] { lhsType.getType(), rhsType.getType() },
							new Object[] { equalityComparisonExpression }, context));
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInequalityComparisonExpression(InequalityComparisonExpression inequalityComparisonExpression,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(inequalityComparisonExpression, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(inequalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(inequalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(inequalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(inequalityComparisonExpression, diagnostics,
					context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(inequalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(inequalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(inequalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(inequalityComparisonExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateInequalityComparisonExpression_NoCollections(inequalityComparisonExpression, diagnostics,
					context);
		if (result || diagnostics != null)
			result &= validateInequalityComparisonExpression_ComparableTypes(inequalityComparisonExpression,
					diagnostics, context);
		return result;
	}

	/**
	 * Validates the NoCollections constraint of '<em>Inequality Comparison
	 * Expression</em>'. <!-- begin-user-doc --> Produces an error when either the
	 * lhs of the rhs of an InequalityComparisonExpression is a collection <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateInequalityComparisonExpression_NoCollections(
			InequalityComparisonExpression inequalityComparisonExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(inequalityComparisonExpression));
		ExpressionType lhsType = typeProvider.getTypeOf(inequalityComparisonExpression.getLhs());
		ExpressionType rhsType = typeProvider.getTypeOf(inequalityComparisonExpression.getRhs());
		if (lhsType == null || rhsType == null) {
			return true;
		}
		if (lhsType.isCollection() || rhsType.isCollection()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						INEQUALITY_COMPARISON_EXPRESSION__NO_COLLECTIONS,
						"_validation__inequality_comparison_expression__no_collections",
						new Object[] { inequalityComparisonExpression.getOp() },
						new Object[] { inequalityComparisonExpression }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the ComparableTypes constraint of '<em>Inequality Comparison
	 * Expression</em>'. <!-- begin-user-doc --> Produces an error when either the
	 * lhs and the rhs of an EqualityComparisonExpression are of incomparable types.
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateInequalityComparisonExpression_ComparableTypes(
			InequalityComparisonExpression inequalityComparisonExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(inequalityComparisonExpression));
		ExpressionType lhsType = typeProvider.getTypeOf(inequalityComparisonExpression.getLhs());
		ExpressionType rhsType = typeProvider.getTypeOf(inequalityComparisonExpression.getRhs());
		if (lhsType == null || rhsType == null) {
			return true;
		}
		boolean result = true;
		if (!lhsType.isInequalityComparable()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						INEQUALITY_COMPARISON_EXPRESSION__COMPARABLE_TYPES,
						"_validation__inequality_comparison_expression__invalid_type",
						new Object[] { lhsType.getType() }, new Object[] { inequalityComparisonExpression }, context));
			}
			result = false;
		}
		if (!rhsType.isInequalityComparable()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						INEQUALITY_COMPARISON_EXPRESSION__COMPARABLE_TYPES,
						"_validation__inequality_comparison_expression__invalid_type",
						new Object[] { rhsType.getType() }, new Object[] { inequalityComparisonExpression }, context));
			}
			result = false;
		}
		if (lhsType.getType() != rhsType.getType()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						INEQUALITY_COMPARISON_EXPRESSION__COMPARABLE_TYPES,
						"_validation__inequality_comparison_expression__different_types",
						new Object[] { lhsType.getType(), rhsType.getType() },
						new Object[] { inequalityComparisonExpression }, context));
			}
			result = false;
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAction(Action action, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(action, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAction_TargetMultiplicity(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAction_ArgMultiplicity(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAction_NoUndefinedConstant(action, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateAction_Type(action, diagnostics, context);
		return result;
	}

	/**
	 * Validates the TargetMultiplicity constraint of '<em>Action</em>'. <!--
	 * begin-user-doc --> Produces an error when either the multiplitity of the
	 * target of an Action is incompatible with its actionType. <!-- end-user-doc
	 * -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAction_TargetMultiplicity(Action action, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(action));
		ExpressionType targetType = typeProvider.getTypeOf(action.getTarget());
		if (targetType == null) {
			return true;
		}
		boolean result = true;
		if (!ABSUtils.actsOnCollections(action.getActionType()) && targetType.isCollection()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ACTION__TARGET_MULTIPLICITY,
						"_validation__action__target_not_collection", new Object[] { action.getActionType().getName() },
						new Object[] { action }, context));
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ACTION__TARGET_MULTIPLICITY,
						"Action " + action.getActionType().getName() + " cannot act on collections",
						new Object[] { action }));
			}
			result = false;
		}
		if (!ABSUtils.actsOnSimpleValues(action.getActionType()) && !targetType.isCollection()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ACTION__TARGET_MULTIPLICITY,
						"_validation__action__target_not_simple", new Object[] { action.getActionType().getName() },
						new Object[] { action }, context));
			}
			result = false;
		}
		return result;
	}

	/**
	 * Validates the ArgMultiplicity constraint of '<em>Action</em>'. <!--
	 * begin-user-doc --> Produces an error when the multiplicity of the arg of an
	 * Action, or its lambdaAction is incompatible with its actionType. <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAction_ArgMultiplicity(Action action, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean result = true;
		if (action.getLambdaAction() == null) {
			if (action.getArgs().isEmpty()) {
				if (!ABSUtils.takesNoArg(action.getActionType())) {
					if (diagnostics != null) {
						diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
								ACTION__ARG_MULTIPLICITY, "_validation__action__argument_missing",
								new Object[] { action.getActionType().getName() }, new Object[] { action }, context));
					}
					result = false;
				}
			} else {
				ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
						(AssetBasedSystem) EcoreUtil.getRootContainer(action));
				for (Expression arg : action.getArgs()) {
					ExpressionType argType = typeProvider.getTypeOf(arg);
					if (argType != null) {
						if (!ABSUtils.takesCollectionArg(action.getActionType()) && argType.isCollection()) {
							if (diagnostics != null) {
								diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
										ACTION__ARG_MULTIPLICITY, "_validation__action__argument_not_collection",
										new Object[] { action.getActionType().getName() }, new Object[] { action },
										context));
							}
							result = false;
						}
						if (!ABSUtils.takesSimpleValueArg(action.getActionType()) && !argType.isCollection()) {
							if (diagnostics != null) {
								diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
										ACTION__ARG_MULTIPLICITY, "_validation__action__argument_not_simple",
										new Object[] { action.getActionType().getName() }, new Object[] { action },
										context));
							}
							result = false;
						}
					}
				}
			}
		} else {
			if (action.getArgs().isEmpty()) {
				if (!ABSUtils.takesLambdaArg(action.getActionType())) {
					if (diagnostics != null) {
						diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
								ACTION__ARG_MULTIPLICITY, "_validation__action__argument_not_lambda",
								new Object[] { action.getActionType().getName() }, new Object[] { action }, context));
					}
					result = false;
				}
			} else {
				if (diagnostics != null) {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ACTION__ARG_MULTIPLICITY,
							"_validation__action__argument_not_both_lambda_and_notlambda",
							new Object[] { action.getActionType().getName() }, new Object[] { action }, context));
					result = false;
				}
			}
		}
		return result;
	}

	/**
	 * Validates the NoUndefinedConstant constraint of '<em>Action</em>'. <!--
	 * begin-user-doc --> Produces an error when the target or arg of an action is
	 * an UndefinedConstant. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAction_NoUndefinedConstant(Action action, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean result = true;
		if (action.getTarget() instanceof UndefinedConstant) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ACTION__NO_UNDEFINED_CONSTANT,
						"_validation__action__target_no_undefined", null, new Object[] { action }, context));
			}
			result = false;
		}
		if (action.getArgs().stream().anyMatch(UndefinedConstant.class::isInstance)) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, ACTION__NO_UNDEFINED_CONSTANT,
						"_validation__action__argument_no_undefined", null, new Object[] { action }, context));
			}
			result = false;
		}
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>Action</em>'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateAction_Type(Action action, DiagnosticChain diagnostics, Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(action));
		ExpressionType targettype = typeProvider.getTypeOf(action.getTarget());
		boolean result = true;
		if (targettype == null) {
			return true;
		}
		switch (action.getActionType()) {
		case ADD:
		case ADD_ALL:
		case ASSIGN:
		case REMOVE:
		case REMOVE_ALL:
			if (!targettype.isMutable()) {
				if (diagnostics != null) {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
							"_validation__action__target_mutable", null, new Object[] { action }, context));
				}
				result = false;
			}
			if (action.getArgs().isEmpty()) {
				return result;
			}
			ExpressionType argtype = typeProvider.getTypeOf(action.getArgs().get(0));
			if (argtype == null) {
				return result;
			}
			if (!argtype.isSubtypeOf(targettype)) {
				if (diagnostics != null) {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
							"_validation__action__wrong_type", null, new Object[] { action }, context));
				}
				result = false;
			}
			break;
		default:
			break;
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConstantExpression(ConstantExpression constantExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(constantExpression, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStringConstant(StringConstant stringConstant, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(stringConstant, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIntConstant(IntConstant intConstant, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(intConstant, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBooleanConstant(BooleanConstant booleanConstant, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(booleanConstant, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVersionConstant(VersionConstant versionConstant, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(versionConstant, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMemberSelection(MemberSelection memberSelection, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(memberSelection, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateMemberSelection_ReceiverMultiplicity(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateMemberSelection_ArgMultiplicity(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateMemberSelection_InvocationIsMethod(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateMemberSelection_NoUndefinedConstant(memberSelection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateMemberSelection_Type(memberSelection, diagnostics, context);
		return result;
	}

	/**
	 * Validates the ReceiverMultiplicity constraint of '<em>Member Selection</em>'.
	 * <!-- begin-user-doc --> Produces an error when the multiplicity of the
	 * receiver of a MemberSelection is incompatible with its member <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateMemberSelection_ReceiverMultiplicity(MemberSelection memberSelection,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(memberSelection));
		ExpressionType receiverType = typeProvider.getTypeOf(memberSelection.getReceiver());
		if (receiverType == null) {
			return true;
		}
		boolean result = true;
		if (memberSelection.getMember() instanceof StaticMethod) {
			if (!ABSUtils.appliesOnCollections((StaticMethod) memberSelection.getMember())
					&& receiverType.isCollection()) {
				if (diagnostics != null) {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
							MEMBER_SELECTION__RECEIVER_MULTIPLICITY,
							"_validation__member_selection__method_receiver_not_collection",
							new Object[] { memberSelection.getMember() }, new Object[] { memberSelection }, context));
				}
				result = false;
			}
			if (!ABSUtils.appliesOnSimpleValues((StaticMethod) memberSelection.getMember())
					&& !receiverType.isCollection()) {
				if (diagnostics != null) {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
							MEMBER_SELECTION__RECEIVER_MULTIPLICITY,
							"_validation__member_selection__method_receiver_not_simple",
							new Object[] { memberSelection.getMember() }, new Object[] { memberSelection }, context));
				}
				result = false;
			}
		}
		return result;
	}

	/**
	 * Validates the ArgMultiplicity constraint of '<em>Member Selection</em>'. <!--
	 * begin-user-doc --> Produces an error when the multiplicity of the arg of a
	 * MemberSelection is incompatible with its member <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateMemberSelection_ArgMultiplicity(MemberSelection memberSelection, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean result = true;
		if (memberSelection.getMember() instanceof StaticMethod
				&& !ABSUtils.takesNoArg((StaticMethod) memberSelection.getMember())
				&& memberSelection.getArgs().isEmpty()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						MEMBER_SELECTION__ARG_MULTIPLICITY, "_validation__member_selection__argument_missing",
						new Object[] { memberSelection.getMember() }, new Object[] { memberSelection }, context));
			}
			result = false;
		}
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(memberSelection));
		for (Expression arg : memberSelection.getArgs()) {
			if (arg instanceof LambdaExpression) {
				if (memberSelection.getMember() instanceof StaticMethod
						&& !ABSUtils.takesLambdaArg((StaticMethod) memberSelection.getMember())) {
					if (diagnostics != null) {
						diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
								MEMBER_SELECTION__ARG_MULTIPLICITY,
								"_validation__member_selection__argument_not_lambda",
								new Object[] { memberSelection.getMember() }, new Object[] { memberSelection },
								context));
					}
					result = false;
				}
			} else {
				ExpressionType argType = typeProvider.getTypeOf(arg);
				if (argType != null) {
					if (memberSelection.getMember() instanceof StaticMethod
							&& !ABSUtils.takesCollectionArg((StaticMethod) memberSelection.getMember())
							&& argType.isCollection()) {
						if (diagnostics != null) {
							diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
									MEMBER_SELECTION__ARG_MULTIPLICITY,
									"_validation__member_selection__argument_not_collection",
									new Object[] { memberSelection.getMember() }, new Object[] { memberSelection },
									context));
						}
						result = false;
					}
					if (memberSelection.getMember() instanceof StaticMethod
							&& !ABSUtils.takesSimpleValueArg((StaticMethod) memberSelection.getMember())
							&& !argType.isCollection()) {
						if (diagnostics != null) {
							diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
									MEMBER_SELECTION__ARG_MULTIPLICITY,
									"_validation__member_selection__argument_not_simple",
									new Object[] { memberSelection.getMember() }, new Object[] { memberSelection },
									context));
						}
						result = false;
					}
				}
			}
		}
		return result;
	}

	/**
	 * Validates the InvocationIsMethod constraint of '<em>Member Selection</em>'.
	 * <!-- begin-user-doc --> Produces an error when a MemberSelection is a method
	 * invocation, but its member is not a method. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateMemberSelection_InvocationIsMethod(MemberSelection memberSelection,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!memberSelection.isMethodInvocation() && !memberSelection.getArgs().isEmpty()) {
			if (diagnostics != null) {
				diagnostics.add(
						createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, MEMBER_SELECTION__INVOCATION_IS_METHOD,
								"_validation__member_selection__argument_only_for_method_invocation", null,
								new Object[] { memberSelection }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the NoUndefinedConstant constraint of '<em>Member Selection</em>'.
	 * <!-- begin-user-doc --> Produces an error when the receiver or arg of a
	 * MemberSelection is an UndefinedConstant. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateMemberSelection_NoUndefinedConstant(MemberSelection memberSelection,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = true;
		if (memberSelection.getReceiver() instanceof UndefinedConstant) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						MEMBER_SELECTION__NO_UNDEFINED_CONSTANT, "_validation__member_selection__receiver_no_undefined",
						null, new Object[] { memberSelection }, context));
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						MEMBER_SELECTION__NO_UNDEFINED_CONSTANT,
						"MemberSelection cannot have \"undefined\" as receiver", new Object[] { memberSelection }));
			}
			result = false;
		}
		if (memberSelection.getArgs().stream().anyMatch(UndefinedConstant.class::isInstance)) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						MEMBER_SELECTION__NO_UNDEFINED_CONSTANT, "_validation__member_selection__argument_no_undefined",
						null, new Object[] { memberSelection }, context));
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE,
						MEMBER_SELECTION__NO_UNDEFINED_CONSTANT,
						"MemberSelection cannot have \"undefined\" as argument", new Object[] { memberSelection }));
			}
			result = false;
		}
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>Member Selection</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateMemberSelection_Type(MemberSelection memberSelection, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(memberSelection));
		ExpressionType receivertype = typeProvider.getTypeOf(memberSelection.getReceiver());
		if (receivertype == null) {
			return true;
		}
		boolean result = true;
		if (memberSelection.isMethodInvocation()) {
			if (memberSelection.getMember() instanceof StaticMethod) {
				StaticMethod method = (StaticMethod) memberSelection.getMember();
				if ((Objects.equals(method.getName(), STATICMETHOD_CONTAINS_NAME)
						|| Objects.equals(method.getName(), STATICMETHOD_CONTAINSALL_NAME)
						|| Objects.equals(method.getName(), STATICMETHOD_CONTAINSANY_NAME))
						&& !memberSelection.getArgs().isEmpty()) {
					ExpressionType argtype = typeProvider.getTypeOf(memberSelection.getArgs().get(0));
					if (argtype != null && !argtype.isSubtypeOf(receivertype)) {
						if (diagnostics != null) {
							diagnostics.add(
									createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
											"_validation__member_selection__argument_subtype_of_receiver",
											new Object[] { memberSelection.getMember() },
											new Object[] { memberSelection }, context));
						}
						result = false;
					}
				}
			}
		} else {
			if (memberSelection.getMember() instanceof AssetTypeFeature && receivertype.isAssetType()
					&& !ABSUtils.getAllSupertypes((AssetType) receivertype.getType())
							.contains(ABSUtils.getContainerType((AssetTypeFeature) memberSelection.getMember()))) {
				if (diagnostics != null) {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
							"_validation__member_selection__feature_subtype_of_receiver",
							new Object[] { ABSUtils.getContainerType((AssetTypeFeature) memberSelection.getMember()),
									memberSelection.getMember() },
							new Object[] { memberSelection }, context));
				}
				result = false;
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMember(Member member, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(member, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSymbolRef(SymbolRef symbolRef, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(symbolRef, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(symbolRef, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(symbolRef, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(symbolRef, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(symbolRef, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(symbolRef, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(symbolRef, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(symbolRef, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(symbolRef, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateSymbolRef_InScope(symbolRef, diagnostics, context);
		return result;
	}

	/**
	 * Validates the InScope constraint of '<em>Symbol Ref</em>'. <!--
	 * begin-user-doc --> Produces an error when a SymbolRef references a symbol
	 * that is out of its scope. The scope of Assets is inside Goals. The scope of
	 * GuardParameters is inside the Guard that defines them. The scope of
	 * LambdaParameters is inside the LambdaExpression or LambdaAction that defines
	 * them. The scope of EnumLiterals is anywhere. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateSymbolRef_InScope(SymbolRef symbolRef, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean result = true;
		if (symbolRef.getSymbol().getName() == null) {
			return true;
		}
		if (symbolRef.getSymbol() instanceof Asset
				&& ABSUtils.getHierarchy(symbolRef).noneMatch(Goal.class::isInstance)) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, SYMBOL_REF__IN_SCOPE,
						"_validation__symbol_ref__asset_outside_of_goal", null, new Object[] { symbolRef }, context));
			}
			result = false;
		}
		if (symbolRef.getSymbol() instanceof GuardParameter
				&& ABSUtils.getHierarchy(symbolRef).filter(Guard.class::isInstance)
						.noneMatch(g -> ((Guard) g).getGuardParameters().contains(symbolRef.getSymbol()))) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, SYMBOL_REF__IN_SCOPE,
						"_validation__symbol_ref__parameter_outside_of_guard", null, new Object[] { symbolRef },
						context));
			}
			result = false;
		}
		if (symbolRef.getSymbol() instanceof LambdaParameter && ABSUtils.getHierarchy(symbolRef)
				.noneMatch(eobj -> (eobj instanceof LambdaExpression
						&& Objects.equals(((LambdaExpression) eobj).getLambdaParameter(), symbolRef.getSymbol()))
						|| (eobj instanceof LambdaAction && Objects.equals(((LambdaAction) eobj).getLambdaParameter(),
								symbolRef.getSymbol())))) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, SYMBOL_REF__IN_SCOPE,
						"_validation__symbol_ref__lambda_parameter_outside_of_lambda_expression", null,
						new Object[] { symbolRef }, context));
			}
			result = false;
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSymbol(Symbol symbol, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(symbol, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStaticMethod(StaticMethod staticMethod, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(staticMethod, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(staticMethod, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(staticMethod, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(staticMethod, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(staticMethod, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(staticMethod, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(staticMethod, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(staticMethod, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(staticMethod, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateStaticMethod_KnownName(staticMethod, diagnostics, context);
		return result;
	}

	/**
	 * Validates the KnownName constraint of '<em>Static Method</em>'. <!--
	 * begin-user-doc --> Produces an error when the name of a StaticMethod is
	 * unknown <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateStaticMethod_KnownName(StaticMethod staticMethod, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!Objects.equals(staticMethod.getName(), STATICMETHOD_CONTAINS_NAME)
				&& !Objects.equals(staticMethod.getName(), STATICMETHOD_CONTAINSALL_NAME)
				&& !Objects.equals(staticMethod.getName(), STATICMETHOD_CONTAINSANY_NAME)
				&& !Objects.equals(staticMethod.getName(), STATICMETHOD_FILTER_NAME)
				&& !Objects.equals(staticMethod.getName(), STATICMETHOD_ISEMPTY_NAME)) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, STATIC_METHOD__KNOWN_NAME,
						"_validation__static_method__unknown_name", null, new Object[] { staticMethod }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLambdaParameter(LambdaParameter lambdaParameter, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(lambdaParameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLambdaExpression(LambdaExpression lambdaExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(lambdaExpression, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(lambdaExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(lambdaExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(lambdaExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(lambdaExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(lambdaExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(lambdaExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(lambdaExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(lambdaExpression, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateLambdaExpression_Type(lambdaExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>Lambda Expression</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateLambdaExpression_Type(LambdaExpression lambdaExpression, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(lambdaExpression));
		ExpressionType bodytype = typeProvider.getTypeOf(lambdaExpression.getBody());
		if (bodytype != null && !bodytype.isBooleanType()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
						"_validation__lambda_expression__wrong_type", null, new Object[] { lambdaExpression },
						context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLambdaAction(LambdaAction lambdaAction, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(lambdaAction, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParameter(Parameter parameter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(parameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAnnotationEntry(AnnotationEntry annotationEntry, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(annotationEntry, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAnnotationKey(AnnotationKey annotationKey, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(annotationKey, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGoal(Goal goal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(goal, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(goal, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(goal, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(goal, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(goal, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(goal, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(goal, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(goal, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(goal, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateGoal_Type(goal, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>Goal</em>'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateGoal_Type(Goal goal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = true;
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(goal));
		ExpressionType pretype = typeProvider.getTypeOf(goal.getPrecondition());
		if (pretype != null && !pretype.isBooleanType()) {
			if (diagnostics != null) {
				if (pretype.isUndefinedType()) {
					diagnostics.add(createABSDiagnostic(Diagnostic.WARNING, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
							"_validation__goal__precondition_should_be_implemented", null, new Object[] { goal },
							context));
				} else {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
							"_validation__goal__precondition_wrong_type", null, new Object[] { goal }, context));
				}
			}
			result = false;
		}
		ExpressionType posttype = typeProvider.getTypeOf(goal.getPostcondition());
		if (posttype != null && !posttype.isBooleanType()) {
			if (diagnostics != null) {
				if (posttype.isUndefinedType()) {
					diagnostics.add(createABSDiagnostic(Diagnostic.WARNING, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
							"_validation__goal__postcondition_should_be_implemented", null, new Object[] { goal },
							context));
				} else {
					diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
							"_validation__goal__postcondition_wrong_type", null, new Object[] { goal }, context));
				}
			}
			result = false;
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateContract(Contract contract, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(contract, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(contract, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(contract, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(contract, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(contract, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(contract, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(contract, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(contract, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(contract, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateGuard_Type(contract, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGuard(Guard guard, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(guard, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(guard, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(guard, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(guard, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(guard, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(guard, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(guard, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(guard, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(guard, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateGuard_Type(guard, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>Guard</em>'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateGuard_Type(Guard guard, DiagnosticChain diagnostics, Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(guard));
		if (guard.getGuardExpression() instanceof UndefinedConstant) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.WARNING, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
						"_validation__guard__should_be_implemented", null, new Object[] { guard }, context));
			}
			return false;
		}
		ExpressionType guardtype = typeProvider.getTypeOf(guard.getGuardExpression());
		if (guardtype != null && !guardtype.isBooleanType()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
						"_validation__guard__wrong_type", null, new Object[] { guard }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumConstant(EnumConstant enumConstant, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumConstant, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUndefinedConstant(UndefinedConstant undefinedConstant, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(undefinedConstant, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCollection(Collection collection, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(collection, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(collection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(collection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(collection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(collection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(collection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(collection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(collection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(collection, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateCollection_Type(collection, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Type constraint of '<em>Collection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCollection_Type(Collection collection, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(collection));
		List<EObject> elementTypes = collection.getElements().stream().map(typeProvider::getTypeOf)
				.filter(Objects::nonNull).map(ExpressionType::getType).collect(Collectors.toList());
		if (ABSUtils.commonSuperTypesWithPrimitiveTypes(elementTypes).isEmpty()) {
			if (diagnostics != null) {
				diagnostics.add(createABSDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, EXPRESSION__WRONG_TYPE,
						"_validation__collection__wrong_type", null, new Object[] { collection }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDefinition(Definition definition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(definition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetGroupContent(AssetGroupContent assetGroupContent, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assetGroupContent, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLocaleGroup(LocaleGroup localeGroup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(localeGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateABSObjectLocale(ABSObjectLocale absObjectLocale, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(absObjectLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDefinitionGroupLocale(DefinitionGroupLocale definitionGroupLocale,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(definitionGroupLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePrimitiveDataTypeLocale(PrimitiveDataTypeLocale primitiveDataTypeLocale,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(primitiveDataTypeLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumLiteralLocale(EnumLiteralLocale enumLiteralLocale, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumLiteralLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractAssetTypeLocale(AbstractAssetTypeLocale abstractAssetTypeLocale,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(abstractAssetTypeLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetTypeLocale(AssetTypeLocale assetTypeLocale, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assetTypeLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetTypeAspectLocale(AssetTypeAspectLocale assetTypeAspectLocale,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assetTypeAspectLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssetTypeFeatureLocale(AssetTypeFeatureLocale assetTypeFeatureLocale,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assetTypeFeatureLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGuardLocale(GuardLocale guardLocale, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(guardLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAnnotated(Annotated annotated, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(annotated, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement(Requirement requirement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(requirement, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirement_ContractsShouldBeStatic(requirement, diagnostics, context);
		return result;
	}

	/**
	 * Validates the ContractsShouldBeStatic constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateRequirement_ContractsShouldBeStatic(Requirement requirement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean result = true;
		for (Contract contract : requirement.getContracts()) {
			if (contract.isDynamic()) {
				if (diagnostics != null) {
					diagnostics.add(createABSDiagnostic(Diagnostic.WARNING, DIAGNOSTIC_SOURCE,
							REQUIREMENT__CONTRACTS_SOULD_BE_STATIC,
							"_validation__requirement__contracts_should_be_static", new Object[] { contract },
							new Object[] { requirement, contract }, context));
				}
				result = false;
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirementLocale(RequirementLocale requirementLocale, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(requirementLocale, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActionEnum(ActionEnum actionEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMultiplicity(Multiplicity multiplicity, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSeverity(Severity severity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTextFormat(TextFormat textFormat, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEVersion(Version eVersion, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	private static final String RESOURCEBUNDLEPATH = "resources/AbsystemValidator";

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ResourceBundleLocatorProvider.getResourceBundleLocator(RESOURCEBUNDLEPATH,
				new Locale(ABSUtils.getLocale()), AbsystemValidator.class.getModule());
	}

	private SubstitutionLabelProvider getLocalizingSubstitutionLabelProvider(Optional<String> locale) {
		return new SubstitutionLabelProvider() {

			@Override
			public String getValueLabel(EDataType eDataType, Object value) {
				return EcoreUtil.convertToString(eDataType, value);
			}

			@Override
			public String getObjectLabel(EObject eObject) {
				if (locale.isEmpty()) {
					return ABSUtils.getName(eObject).orElseGet(() -> EcoreUtil.getIdentification(eObject));
				}
				return ABSUtils.getLocalizedName(eObject, locale.orElseThrow())
						.orElseGet(() -> EcoreUtil.getIdentification(eObject));
			}

			@Override
			public String getFeatureLabel(EStructuralFeature eStructuralFeature) {
				return eStructuralFeature.getName();
			}
		};
	}

	protected BasicDiagnostic createABSDiagnostic(int severity, String source, int code, String messageKey,
			Object[] messageSubstitutions, Object[] data, Map<Object, Object> context) {
		String locale = ABSUtils.getLocale();
		ResourceLocator resourceLocator = getResourceLocator();
		if (context.get(Locale.class) instanceof Locale) {
			locale = ((Locale) context.get(Locale.class)).getLanguage();
			resourceLocator = ResourceBundleLocatorProvider.getResourceBundleLocator(RESOURCEBUNDLEPATH,
					(Locale) context.get(Locale.class), AbsystemValidator.class.getModule());
		}
		Object oldSubstitutionLabelProvider = context.put(SubstitutionLabelProvider.class,
				getLocalizingSubstitutionLabelProvider(Optional.of(locale)));
		if (messageSubstitutions != null) {
			messageSubstitutions = Arrays.stream(messageSubstitutions).map(messageSubstitution -> {
				if (messageSubstitution instanceof EObject) {
					return getObjectLabel((EObject) messageSubstitution, context);
				} else {
					return messageSubstitution;
				}
			}).toArray();
		}
		String message = messageSubstitutions == null ? resourceLocator.getString(messageKey)
				: resourceLocator.getString(messageKey, messageSubstitutions);
		BasicDiagnostic result = new BasicDiagnostic(severity, source, code, message, data);
		if (oldSubstitutionLabelProvider != null) {
			context.put(SubstitutionLabelProvider.class, oldSubstitutionLabelProvider);
		}
		return result;
	}

} // AbsystemValidator
