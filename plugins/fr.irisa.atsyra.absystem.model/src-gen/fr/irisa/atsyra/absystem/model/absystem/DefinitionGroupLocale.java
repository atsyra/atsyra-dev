/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition Group Locale</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale#getRef <em>Ref</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale#getDefinitionLocales <em>Definition Locales</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroupLocale()
 * @model
 * @generated
 */
public interface DefinitionGroupLocale extends Annotated, ABSObjectLocale {
	/**
	 * Returns the value of the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref</em>' reference.
	 * @see #setRef(DefinitionGroup)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroupLocale_Ref()
	 * @model
	 * @generated
	 */
	DefinitionGroup getRef();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale#getRef <em>Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref</em>' reference.
	 * @see #getRef()
	 * @generated
	 */
	void setRef(DefinitionGroup value);

	/**
	 * Returns the value of the '<em><b>Definition Locales</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition Locales</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroupLocale_DefinitionLocales()
	 * @model containment="true"
	 * @generated
	 */
	EList<ABSObjectLocale> getDefinitionLocales();

} // DefinitionGroupLocale
