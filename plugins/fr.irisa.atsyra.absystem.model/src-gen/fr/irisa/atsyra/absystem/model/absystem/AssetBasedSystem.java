/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Based System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getDefinitionGroups <em>Definition Groups</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAssetGroups <em>Asset Groups</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getImports <em>Imports</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getPossibleGuardedActions <em>Possible Guarded Actions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAppliedActions <em>Applied Actions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAllAssets <em>All Assets</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getAllStaticAssetLinks <em>All Static Asset Links</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem#getLocalizations <em>Localizations</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetBasedSystem()
 * @model
 * @generated
 */
public interface AssetBasedSystem extends EObject {
	/**
	 * Returns the value of the '<em><b>Definition Groups</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition Groups</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetBasedSystem_DefinitionGroups()
	 * @model containment="true" keys="id"
	 * @generated
	 */
	EList<DefinitionGroup> getDefinitionGroups();

	/**
	 * Returns the value of the '<em><b>Asset Groups</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AssetGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Groups</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetBasedSystem_AssetGroups()
	 * @model containment="true" keys="name"
	 * @generated
	 */
	EList<AssetGroup> getAssetGroups();

	/**
	 * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetBasedSystem_Imports()
	 * @model containment="true"
	 * @generated
	 */
	EList<Import> getImports();

	/**
	 * Returns the value of the '<em><b>Possible Guarded Actions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Possible Guarded Actions</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetBasedSystem_PossibleGuardedActions()
	 * @model containment="true"
	 *        annotation="aspect"
	 * @generated
	 */
	EList<GuardOccurence> getPossibleGuardedActions();

	/**
	 * Returns the value of the '<em><b>Applied Actions</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applied Actions</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetBasedSystem_AppliedActions()
	 * @model annotation="aspect"
	 * @generated
	 */
	EList<GuardOccurence> getAppliedActions();

	/**
	 * Returns the value of the '<em><b>All Assets</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Asset}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Assets</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetBasedSystem_AllAssets()
	 * @model
	 * @generated
	 */
	EList<Asset> getAllAssets();

	/**
	 * Returns the value of the '<em><b>All Static Asset Links</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AssetLink}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Static Asset Links</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetBasedSystem_AllStaticAssetLinks()
	 * @model
	 * @generated
	 */
	EList<AssetLink> getAllStaticAssetLinks();

	/**
	 * Returns the value of the '<em><b>Localizations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.LocaleGroup}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Localizations</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetBasedSystem_Localizations()
	 * @model containment="true"
	 * @generated
	 */
	EList<LocaleGroup> getLocalizations();

} // AssetBasedSystem
