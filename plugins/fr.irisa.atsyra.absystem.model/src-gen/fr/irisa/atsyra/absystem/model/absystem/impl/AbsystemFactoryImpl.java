/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AbsystemFactoryImpl extends EFactoryImpl implements AbsystemFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AbsystemFactory init() {
		try {
			AbsystemFactory theAbsystemFactory = (AbsystemFactory) EPackage.Registry.INSTANCE
					.getEFactory(AbsystemPackage.eNS_URI);
			if (theAbsystemFactory != null) {
				return theAbsystemFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AbsystemFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbsystemFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case AbsystemPackage.ASSET_TYPE:
			return createAssetType();
		case AbsystemPackage.ASSET_TYPE_REFERENCE:
			return createAssetTypeReference();
		case AbsystemPackage.ASSET_BASED_SYSTEM:
			return createAssetBasedSystem();
		case AbsystemPackage.ASSET:
			return createAsset();
		case AbsystemPackage.ASSET_LINK:
			return createAssetLink();
		case AbsystemPackage.ASSET_TYPE_ATTRIBUTE:
			return createAssetTypeAttribute();
		case AbsystemPackage.PRIMITIVE_DATA_TYPE:
			return createPrimitiveDataType();
		case AbsystemPackage.ENUM_DATA_TYPE:
			return createEnumDataType();
		case AbsystemPackage.ENUM_LITERAL:
			return createEnumLiteral();
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE:
			return createAssetAttributeValue();
		case AbsystemPackage.DEFINITION_GROUP:
			return createDefinitionGroup();
		case AbsystemPackage.ASSET_GROUP:
			return createAssetGroup();
		case AbsystemPackage.IMPORT:
			return createImport();
		case AbsystemPackage.TAG:
			return createTag();
		case AbsystemPackage.ASSET_TYPE_ASPECT:
			return createAssetTypeAspect();
		case AbsystemPackage.GUARDED_ACTION:
			return createGuardedAction();
		case AbsystemPackage.GUARD_PARAMETER:
			return createGuardParameter();
		case AbsystemPackage.IMPLIES_EXPRESSION:
			return createImpliesExpression();
		case AbsystemPackage.OR_EXPRESSION:
			return createOrExpression();
		case AbsystemPackage.AND_EXPRESSION:
			return createAndExpression();
		case AbsystemPackage.NOT_EXPRESSION:
			return createNotExpression();
		case AbsystemPackage.EQUALITY_COMPARISON_EXPRESSION:
			return createEqualityComparisonExpression();
		case AbsystemPackage.INEQUALITY_COMPARISON_EXPRESSION:
			return createInequalityComparisonExpression();
		case AbsystemPackage.ACTION:
			return createAction();
		case AbsystemPackage.STRING_CONSTANT:
			return createStringConstant();
		case AbsystemPackage.INT_CONSTANT:
			return createIntConstant();
		case AbsystemPackage.BOOLEAN_CONSTANT:
			return createBooleanConstant();
		case AbsystemPackage.VERSION_CONSTANT:
			return createVersionConstant();
		case AbsystemPackage.MEMBER_SELECTION:
			return createMemberSelection();
		case AbsystemPackage.SYMBOL_REF:
			return createSymbolRef();
		case AbsystemPackage.STATIC_METHOD:
			return createStaticMethod();
		case AbsystemPackage.LAMBDA_PARAMETER:
			return createLambdaParameter();
		case AbsystemPackage.LAMBDA_EXPRESSION:
			return createLambdaExpression();
		case AbsystemPackage.LAMBDA_ACTION:
			return createLambdaAction();
		case AbsystemPackage.ANNOTATION_ENTRY:
			return createAnnotationEntry();
		case AbsystemPackage.ANNOTATION_KEY:
			return createAnnotationKey();
		case AbsystemPackage.GOAL:
			return createGoal();
		case AbsystemPackage.CONTRACT:
			return createContract();
		case AbsystemPackage.ENUM_CONSTANT:
			return createEnumConstant();
		case AbsystemPackage.UNDEFINED_CONSTANT:
			return createUndefinedConstant();
		case AbsystemPackage.COLLECTION:
			return createCollection();
		case AbsystemPackage.LOCALE_GROUP:
			return createLocaleGroup();
		case AbsystemPackage.DEFINITION_GROUP_LOCALE:
			return createDefinitionGroupLocale();
		case AbsystemPackage.PRIMITIVE_DATA_TYPE_LOCALE:
			return createPrimitiveDataTypeLocale();
		case AbsystemPackage.ENUM_LITERAL_LOCALE:
			return createEnumLiteralLocale();
		case AbsystemPackage.ASSET_TYPE_LOCALE:
			return createAssetTypeLocale();
		case AbsystemPackage.ASSET_TYPE_ASPECT_LOCALE:
			return createAssetTypeAspectLocale();
		case AbsystemPackage.ASSET_TYPE_FEATURE_LOCALE:
			return createAssetTypeFeatureLocale();
		case AbsystemPackage.GUARD_LOCALE:
			return createGuardLocale();
		case AbsystemPackage.REQUIREMENT:
			return createRequirement();
		case AbsystemPackage.REQUIREMENT_LOCALE:
			return createRequirementLocale();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case AbsystemPackage.ACTION_ENUM:
			return createActionEnumFromString(eDataType, initialValue);
		case AbsystemPackage.MULTIPLICITY:
			return createMultiplicityFromString(eDataType, initialValue);
		case AbsystemPackage.SEVERITY:
			return createSeverityFromString(eDataType, initialValue);
		case AbsystemPackage.TEXT_FORMAT:
			return createTextFormatFromString(eDataType, initialValue);
		case AbsystemPackage.EVERSION:
			return createEVersionFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case AbsystemPackage.ACTION_ENUM:
			return convertActionEnumToString(eDataType, instanceValue);
		case AbsystemPackage.MULTIPLICITY:
			return convertMultiplicityToString(eDataType, instanceValue);
		case AbsystemPackage.SEVERITY:
			return convertSeverityToString(eDataType, instanceValue);
		case AbsystemPackage.TEXT_FORMAT:
			return convertTextFormatToString(eDataType, instanceValue);
		case AbsystemPackage.EVERSION:
			return convertEVersionToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetType createAssetType() {
		AssetTypeImpl assetType = new AssetTypeImpl();
		return assetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeReference createAssetTypeReference() {
		AssetTypeReferenceImpl assetTypeReference = new AssetTypeReferenceImpl();
		return assetTypeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetBasedSystem createAssetBasedSystem() {
		AssetBasedSystemImpl assetBasedSystem = new AssetBasedSystemImpl();
		return assetBasedSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Asset createAsset() {
		AssetImpl asset = new AssetImpl();
		return asset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetLink createAssetLink() {
		AssetLinkImpl assetLink = new AssetLinkImpl();
		return assetLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeAttribute createAssetTypeAttribute() {
		AssetTypeAttributeImpl assetTypeAttribute = new AssetTypeAttributeImpl();
		return assetTypeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PrimitiveDataType createPrimitiveDataType() {
		PrimitiveDataTypeImpl primitiveDataType = new PrimitiveDataTypeImpl();
		return primitiveDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EnumDataType createEnumDataType() {
		EnumDataTypeImpl enumDataType = new EnumDataTypeImpl();
		return enumDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EnumLiteral createEnumLiteral() {
		EnumLiteralImpl enumLiteral = new EnumLiteralImpl();
		return enumLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetAttributeValue createAssetAttributeValue() {
		AssetAttributeValueImpl assetAttributeValue = new AssetAttributeValueImpl();
		return assetAttributeValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DefinitionGroup createDefinitionGroup() {
		DefinitionGroupImpl definitionGroup = new DefinitionGroupImpl();
		return definitionGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetGroup createAssetGroup() {
		AssetGroupImpl assetGroup = new AssetGroupImpl();
		return assetGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Import createImport() {
		ImportImpl import_ = new ImportImpl();
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Tag createTag() {
		TagImpl tag = new TagImpl();
		return tag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeAspect createAssetTypeAspect() {
		AssetTypeAspectImpl assetTypeAspect = new AssetTypeAspectImpl();
		return assetTypeAspect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GuardedAction createGuardedAction() {
		GuardedActionImpl guardedAction = new GuardedActionImpl();
		return guardedAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GuardParameter createGuardParameter() {
		GuardParameterImpl guardParameter = new GuardParameterImpl();
		return guardParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ImpliesExpression createImpliesExpression() {
		ImpliesExpressionImpl impliesExpression = new ImpliesExpressionImpl();
		return impliesExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OrExpression createOrExpression() {
		OrExpressionImpl orExpression = new OrExpressionImpl();
		return orExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AndExpression createAndExpression() {
		AndExpressionImpl andExpression = new AndExpressionImpl();
		return andExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotExpression createNotExpression() {
		NotExpressionImpl notExpression = new NotExpressionImpl();
		return notExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EqualityComparisonExpression createEqualityComparisonExpression() {
		EqualityComparisonExpressionImpl equalityComparisonExpression = new EqualityComparisonExpressionImpl();
		return equalityComparisonExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InequalityComparisonExpression createInequalityComparisonExpression() {
		InequalityComparisonExpressionImpl inequalityComparisonExpression = new InequalityComparisonExpressionImpl();
		return inequalityComparisonExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Action createAction() {
		ActionImpl action = new ActionImpl();
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StringConstant createStringConstant() {
		StringConstantImpl stringConstant = new StringConstantImpl();
		return stringConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IntConstant createIntConstant() {
		IntConstantImpl intConstant = new IntConstantImpl();
		return intConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BooleanConstant createBooleanConstant() {
		BooleanConstantImpl booleanConstant = new BooleanConstantImpl();
		return booleanConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VersionConstant createVersionConstant() {
		VersionConstantImpl versionConstant = new VersionConstantImpl();
		return versionConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MemberSelection createMemberSelection() {
		MemberSelectionImpl memberSelection = new MemberSelectionImpl();
		return memberSelection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SymbolRef createSymbolRef() {
		SymbolRefImpl symbolRef = new SymbolRefImpl();
		return symbolRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StaticMethod createStaticMethod() {
		StaticMethodImpl staticMethod = new StaticMethodImpl();
		return staticMethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LambdaParameter createLambdaParameter() {
		LambdaParameterImpl lambdaParameter = new LambdaParameterImpl();
		return lambdaParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LambdaExpression createLambdaExpression() {
		LambdaExpressionImpl lambdaExpression = new LambdaExpressionImpl();
		return lambdaExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LambdaAction createLambdaAction() {
		LambdaActionImpl lambdaAction = new LambdaActionImpl();
		return lambdaAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AnnotationEntry createAnnotationEntry() {
		AnnotationEntryImpl annotationEntry = new AnnotationEntryImpl();
		return annotationEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AnnotationKey createAnnotationKey() {
		AnnotationKeyImpl annotationKey = new AnnotationKeyImpl();
		return annotationKey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Goal createGoal() {
		GoalImpl goal = new GoalImpl();
		return goal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Contract createContract() {
		ContractImpl contract = new ContractImpl();
		return contract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EnumConstant createEnumConstant() {
		EnumConstantImpl enumConstant = new EnumConstantImpl();
		return enumConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UndefinedConstant createUndefinedConstant() {
		UndefinedConstantImpl undefinedConstant = new UndefinedConstantImpl();
		return undefinedConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection createCollection() {
		CollectionImpl collection = new CollectionImpl();
		return collection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LocaleGroup createLocaleGroup() {
		LocaleGroupImpl localeGroup = new LocaleGroupImpl();
		return localeGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DefinitionGroupLocale createDefinitionGroupLocale() {
		DefinitionGroupLocaleImpl definitionGroupLocale = new DefinitionGroupLocaleImpl();
		return definitionGroupLocale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PrimitiveDataTypeLocale createPrimitiveDataTypeLocale() {
		PrimitiveDataTypeLocaleImpl primitiveDataTypeLocale = new PrimitiveDataTypeLocaleImpl();
		return primitiveDataTypeLocale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EnumLiteralLocale createEnumLiteralLocale() {
		EnumLiteralLocaleImpl enumLiteralLocale = new EnumLiteralLocaleImpl();
		return enumLiteralLocale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeLocale createAssetTypeLocale() {
		AssetTypeLocaleImpl assetTypeLocale = new AssetTypeLocaleImpl();
		return assetTypeLocale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeAspectLocale createAssetTypeAspectLocale() {
		AssetTypeAspectLocaleImpl assetTypeAspectLocale = new AssetTypeAspectLocaleImpl();
		return assetTypeAspectLocale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeFeatureLocale createAssetTypeFeatureLocale() {
		AssetTypeFeatureLocaleImpl assetTypeFeatureLocale = new AssetTypeFeatureLocaleImpl();
		return assetTypeFeatureLocale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GuardLocale createGuardLocale() {
		GuardLocaleImpl guardLocale = new GuardLocaleImpl();
		return guardLocale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Requirement createRequirement() {
		RequirementImpl requirement = new RequirementImpl();
		return requirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RequirementLocale createRequirementLocale() {
		RequirementLocaleImpl requirementLocale = new RequirementLocaleImpl();
		return requirementLocale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionEnum createActionEnumFromString(EDataType eDataType, String initialValue) {
		ActionEnum result = ActionEnum.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActionEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity createMultiplicityFromString(EDataType eDataType, String initialValue) {
		Multiplicity result = Multiplicity.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMultiplicityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Severity createSeverityFromString(EDataType eDataType, String initialValue) {
		Severity result = Severity.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSeverityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TextFormat createTextFormatFromString(EDataType eDataType, String initialValue) {
		TextFormat result = TextFormat.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTextFormatToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Version createEVersionFromString(EDataType eDataType, String initialValue) {
		return (Version) super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEVersionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbsystemPackage getAbsystemPackage() {
		return (AbsystemPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AbsystemPackage getPackage() {
		return AbsystemPackage.eINSTANCE;
	}

} //AbsystemFactoryImpl
