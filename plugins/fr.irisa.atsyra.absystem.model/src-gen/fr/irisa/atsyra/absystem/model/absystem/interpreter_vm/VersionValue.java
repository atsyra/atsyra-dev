/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm;

import fr.irisa.atsyra.absystem.model.absystem.Version;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Version Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.VersionValue#getVersionValue <em>Version Value</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage#getVersionValue()
 * @model annotation="aspect"
 * @generated
 */
public interface VersionValue extends Value {
	/**
	 * Returns the value of the '<em><b>Version Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version Value</em>' attribute.
	 * @see #setVersionValue(Version)
	 * @see fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage#getVersionValue_VersionValue()
	 * @model dataType="fr.irisa.atsyra.absystem.model.absystem.EVersion"
	 * @generated
	 */
	Version getVersionValue();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.VersionValue#getVersionValue <em>Version Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version Value</em>' attribute.
	 * @see #getVersionValue()
	 * @generated
	 */
	void setVersionValue(Version value);

} // VersionValue
