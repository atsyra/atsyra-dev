/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.Definition;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.Tag;
import fr.irisa.atsyra.absystem.model.absystem.TextFormat;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.model.absystem.util.UnmodifiableEListCollector;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getId <em>Id</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getAssetTypeAttributes <em>Asset Type Attributes</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getAssetTypeProperties <em>Asset Type Properties</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getLevel <em>Level</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getExtends <em>Extends</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getDescriptionFormat <em>Description Format</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl#getAllTags <em>All Tags</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetTypeImpl extends AnnotatedImpl implements AssetType {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAssetTypeAttributes() <em>Asset Type Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetTypeAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetTypeAttribute> assetTypeAttributes;

	/**
	 * The cached value of the '{@link #getAssetTypeProperties() <em>Asset Type Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetTypeProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetTypeReference> assetTypeProperties;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected static final String LEVEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected String level = LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<Tag> tags;

	/**
	 * The cached value of the '{@link #getExtends() <em>Extends</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtends()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetType> extends_;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescriptionFormat() <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionFormat()
	 * @generated
	 * @ordered
	 */
	protected static final TextFormat DESCRIPTION_FORMAT_EDEFAULT = TextFormat.PLAINTEXT;

	/**
	 * The cached value of the '{@link #getDescriptionFormat() <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionFormat()
	 * @generated
	 * @ordered
	 */
	protected TextFormat descriptionFormat = DESCRIPTION_FORMAT_EDEFAULT;

	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ASSET_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getId() {
		StringBuilder result = new StringBuilder();
		result.append(eClass().getName()).append('-');
		if (name != null) {
			return result.append(name).toString();
		}
		if (eContainer() != null) {
			@SuppressWarnings("unchecked")
			List<Definition> containingList = (List<Definition>) eContainer().eGet(eContainingFeature());
			result.append(containingList.indexOf(this));
		}
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetTypeAttribute> getAssetTypeAttributes() {
		if (assetTypeAttributes == null) {
			assetTypeAttributes = new EObjectContainmentEList<AssetTypeAttribute>(AssetTypeAttribute.class, this,
					AbsystemPackage.ASSET_TYPE__ASSET_TYPE_ATTRIBUTES);
		}
		return assetTypeAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetTypeReference> getAssetTypeProperties() {
		if (assetTypeProperties == null) {
			assetTypeProperties = new EObjectContainmentEList<AssetTypeReference>(AssetTypeReference.class, this,
					AbsystemPackage.ASSET_TYPE__ASSET_TYPE_PROPERTIES);
		}
		return assetTypeProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLevel() {
		return level;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLevel(String newLevel) {
		String oldLevel = level;
		level = newLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_TYPE__LEVEL, oldLevel, level));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Tag> getTags() {
		if (tags == null) {
			tags = new EObjectResolvingEList<Tag>(Tag.class, this, AbsystemPackage.ASSET_TYPE__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetType> getExtends() {
		if (extends_ == null) {
			extends_ = new EObjectResolvingEList<AssetType>(AssetType.class, this, AbsystemPackage.ASSET_TYPE__EXTENDS);
		}
		return extends_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_TYPE__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextFormat getDescriptionFormat() {
		return descriptionFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescriptionFormat(TextFormat newDescriptionFormat) {
		TextFormat oldDescriptionFormat = descriptionFormat;
		descriptionFormat = newDescriptionFormat == null ? DESCRIPTION_FORMAT_EDEFAULT : newDescriptionFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_TYPE__DESCRIPTION_FORMAT,
					oldDescriptionFormat, descriptionFormat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_TYPE__ABSTRACT, oldAbstract,
					abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Tag> getAllTags() {
		return ABSUtils.getHierarchy(this).flatMap(eobj -> {
			if (eobj instanceof AssetType) {
				return ((AssetType) eobj).getTags().stream();
			} else if (eobj instanceof DefinitionGroup) {
				return ((DefinitionGroup) eobj).getTags().stream();
			} else {
				return Stream.empty();
			}
		}).distinct().collect(new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getAssetType_AllTags()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			return ((InternalEList<?>) getAssetTypeAttributes()).basicRemove(otherEnd, msgs);
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			return ((InternalEList<?>) getAssetTypeProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE__ID:
			return getId();
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			return getAssetTypeAttributes();
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			return getAssetTypeProperties();
		case AbsystemPackage.ASSET_TYPE__NAME:
			return getName();
		case AbsystemPackage.ASSET_TYPE__LEVEL:
			return getLevel();
		case AbsystemPackage.ASSET_TYPE__TAGS:
			return getTags();
		case AbsystemPackage.ASSET_TYPE__EXTENDS:
			return getExtends();
		case AbsystemPackage.ASSET_TYPE__DESCRIPTION:
			return getDescription();
		case AbsystemPackage.ASSET_TYPE__DESCRIPTION_FORMAT:
			return getDescriptionFormat();
		case AbsystemPackage.ASSET_TYPE__ABSTRACT:
			return isAbstract();
		case AbsystemPackage.ASSET_TYPE__ALL_TAGS:
			return getAllTags();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			getAssetTypeAttributes().clear();
			getAssetTypeAttributes().addAll((Collection<? extends AssetTypeAttribute>) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			getAssetTypeProperties().clear();
			getAssetTypeProperties().addAll((Collection<? extends AssetTypeReference>) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE__NAME:
			setName((String) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE__LEVEL:
			setLevel((String) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE__TAGS:
			getTags().clear();
			getTags().addAll((Collection<? extends Tag>) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE__EXTENDS:
			getExtends().clear();
			getExtends().addAll((Collection<? extends AssetType>) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE__DESCRIPTION_FORMAT:
			setDescriptionFormat((TextFormat) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE__ABSTRACT:
			setAbstract((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			getAssetTypeAttributes().clear();
			return;
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			getAssetTypeProperties().clear();
			return;
		case AbsystemPackage.ASSET_TYPE__NAME:
			setName(NAME_EDEFAULT);
			return;
		case AbsystemPackage.ASSET_TYPE__LEVEL:
			setLevel(LEVEL_EDEFAULT);
			return;
		case AbsystemPackage.ASSET_TYPE__TAGS:
			getTags().clear();
			return;
		case AbsystemPackage.ASSET_TYPE__EXTENDS:
			getExtends().clear();
			return;
		case AbsystemPackage.ASSET_TYPE__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case AbsystemPackage.ASSET_TYPE__DESCRIPTION_FORMAT:
			setDescriptionFormat(DESCRIPTION_FORMAT_EDEFAULT);
			return;
		case AbsystemPackage.ASSET_TYPE__ABSTRACT:
			setAbstract(ABSTRACT_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE__ID:
			return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			return assetTypeAttributes != null && !assetTypeAttributes.isEmpty();
		case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			return assetTypeProperties != null && !assetTypeProperties.isEmpty();
		case AbsystemPackage.ASSET_TYPE__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case AbsystemPackage.ASSET_TYPE__LEVEL:
			return LEVEL_EDEFAULT == null ? level != null : !LEVEL_EDEFAULT.equals(level);
		case AbsystemPackage.ASSET_TYPE__TAGS:
			return tags != null && !tags.isEmpty();
		case AbsystemPackage.ASSET_TYPE__EXTENDS:
			return extends_ != null && !extends_.isEmpty();
		case AbsystemPackage.ASSET_TYPE__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case AbsystemPackage.ASSET_TYPE__DESCRIPTION_FORMAT:
			return descriptionFormat != DESCRIPTION_FORMAT_EDEFAULT;
		case AbsystemPackage.ASSET_TYPE__ABSTRACT:
			return abstract_ != ABSTRACT_EDEFAULT;
		case AbsystemPackage.ASSET_TYPE__ALL_TAGS:
			return !getAllTags().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Definition.class) {
			switch (derivedFeatureID) {
			case AbsystemPackage.ASSET_TYPE__ID:
				return AbsystemPackage.DEFINITION__ID;
			default:
				return -1;
			}
		}
		if (baseClass == AbstractAssetType.class) {
			switch (derivedFeatureID) {
			case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
				return AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES;
			case AbsystemPackage.ASSET_TYPE__ASSET_TYPE_PROPERTIES:
				return AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Definition.class) {
			switch (baseFeatureID) {
			case AbsystemPackage.DEFINITION__ID:
				return AbsystemPackage.ASSET_TYPE__ID;
			default:
				return -1;
			}
		}
		if (baseClass == AbstractAssetType.class) {
			switch (baseFeatureID) {
			case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
				return AbsystemPackage.ASSET_TYPE__ASSET_TYPE_ATTRIBUTES;
			case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES:
				return AbsystemPackage.ASSET_TYPE__ASSET_TYPE_PROPERTIES;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", level: ");
		result.append(level);
		result.append(", description: ");
		result.append(description);
		result.append(", descriptionFormat: ");
		result.append(descriptionFormat);
		result.append(", abstract: ");
		result.append(abstract_);
		result.append(')');
		return result.toString();
	}

} //AssetTypeImpl
