/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.Definition;
import fr.irisa.atsyra.absystem.model.absystem.TextFormat;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl#getId <em>Id</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl#getAssetType <em>Asset Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl#getAssetAttributeValues <em>Asset Attribute Values</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl#getFeaturesMap <em>Features Map</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetImpl#getDescriptionFormat <em>Description Format</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetImpl extends SymbolImpl implements Asset {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAssetType() <em>Asset Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetType()
	 * @generated
	 * @ordered
	 */
	protected AssetType assetType;

	/**
	 * The cached value of the '{@link #getAssetAttributeValues() <em>Asset Attribute Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetAttributeValues()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetAttributeValue> assetAttributeValues;

	/**
	 * The cached value of the '{@link #getFeaturesMap() <em>Features Map</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeaturesMap()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetFeatureValueEntry> featuresMap;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescriptionFormat() <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionFormat()
	 * @generated
	 * @ordered
	 */
	protected static final TextFormat DESCRIPTION_FORMAT_EDEFAULT = TextFormat.PLAINTEXT;

	/**
	 * The cached value of the '{@link #getDescriptionFormat() <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionFormat()
	 * @generated
	 * @ordered
	 */
	protected TextFormat descriptionFormat = DESCRIPTION_FORMAT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ASSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getId() {
		StringBuilder result = new StringBuilder();
		result.append(eClass().getName()).append('-');
		if (name != null) {
			return result.append(name).toString();
		}
		if (eContainer() != null) {
			@SuppressWarnings("unchecked")
			List<AssetGroupContent> containingList = (List<AssetGroupContent>) eContainer().eGet(eContainingFeature());
			result.append(containingList.indexOf(this));
		}
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetType getAssetType() {
		if (assetType != null && assetType.eIsProxy()) {
			InternalEObject oldAssetType = (InternalEObject) assetType;
			assetType = (AssetType) eResolveProxy(oldAssetType);
			if (assetType != oldAssetType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AbsystemPackage.ASSET__ASSET_TYPE,
							oldAssetType, assetType));
			}
		}
		return assetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetType basicGetAssetType() {
		return assetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAssetType(AssetType newAssetType) {
		AssetType oldAssetType = assetType;
		assetType = newAssetType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET__ASSET_TYPE, oldAssetType,
					assetType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetAttributeValue> getAssetAttributeValues() {
		if (assetAttributeValues == null) {
			assetAttributeValues = new EObjectContainmentEList<AssetAttributeValue>(AssetAttributeValue.class, this,
					AbsystemPackage.ASSET__ASSET_ATTRIBUTE_VALUES);
		}
		return assetAttributeValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetFeatureValueEntry> getFeaturesMap() {
		if (featuresMap == null) {
			featuresMap = new EObjectContainmentEList<AssetFeatureValueEntry>(AssetFeatureValueEntry.class, this,
					AbsystemPackage.ASSET__FEATURES_MAP);
		}
		return featuresMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET__DESCRIPTION, oldDescription,
					description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextFormat getDescriptionFormat() {
		return descriptionFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescriptionFormat(TextFormat newDescriptionFormat) {
		TextFormat oldDescriptionFormat = descriptionFormat;
		descriptionFormat = newDescriptionFormat == null ? DESCRIPTION_FORMAT_EDEFAULT : newDescriptionFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET__DESCRIPTION_FORMAT,
					oldDescriptionFormat, descriptionFormat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.ASSET__ASSET_ATTRIBUTE_VALUES:
			return ((InternalEList<?>) getAssetAttributeValues()).basicRemove(otherEnd, msgs);
		case AbsystemPackage.ASSET__FEATURES_MAP:
			return ((InternalEList<?>) getFeaturesMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ASSET__ID:
			return getId();
		case AbsystemPackage.ASSET__ASSET_TYPE:
			if (resolve)
				return getAssetType();
			return basicGetAssetType();
		case AbsystemPackage.ASSET__ASSET_ATTRIBUTE_VALUES:
			return getAssetAttributeValues();
		case AbsystemPackage.ASSET__FEATURES_MAP:
			return getFeaturesMap();
		case AbsystemPackage.ASSET__DESCRIPTION:
			return getDescription();
		case AbsystemPackage.ASSET__DESCRIPTION_FORMAT:
			return getDescriptionFormat();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ASSET__ASSET_TYPE:
			setAssetType((AssetType) newValue);
			return;
		case AbsystemPackage.ASSET__ASSET_ATTRIBUTE_VALUES:
			getAssetAttributeValues().clear();
			getAssetAttributeValues().addAll((Collection<? extends AssetAttributeValue>) newValue);
			return;
		case AbsystemPackage.ASSET__FEATURES_MAP:
			getFeaturesMap().clear();
			getFeaturesMap().addAll((Collection<? extends AssetFeatureValueEntry>) newValue);
			return;
		case AbsystemPackage.ASSET__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case AbsystemPackage.ASSET__DESCRIPTION_FORMAT:
			setDescriptionFormat((TextFormat) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET__ASSET_TYPE:
			setAssetType((AssetType) null);
			return;
		case AbsystemPackage.ASSET__ASSET_ATTRIBUTE_VALUES:
			getAssetAttributeValues().clear();
			return;
		case AbsystemPackage.ASSET__FEATURES_MAP:
			getFeaturesMap().clear();
			return;
		case AbsystemPackage.ASSET__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case AbsystemPackage.ASSET__DESCRIPTION_FORMAT:
			setDescriptionFormat(DESCRIPTION_FORMAT_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET__ID:
			return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
		case AbsystemPackage.ASSET__ASSET_TYPE:
			return assetType != null;
		case AbsystemPackage.ASSET__ASSET_ATTRIBUTE_VALUES:
			return assetAttributeValues != null && !assetAttributeValues.isEmpty();
		case AbsystemPackage.ASSET__FEATURES_MAP:
			return featuresMap != null && !featuresMap.isEmpty();
		case AbsystemPackage.ASSET__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case AbsystemPackage.ASSET__DESCRIPTION_FORMAT:
			return descriptionFormat != DESCRIPTION_FORMAT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AssetGroupContent.class) {
			switch (derivedFeatureID) {
			case AbsystemPackage.ASSET__ID:
				return AbsystemPackage.ASSET_GROUP_CONTENT__ID;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AssetGroupContent.class) {
			switch (baseFeatureID) {
			case AbsystemPackage.ASSET_GROUP_CONTENT__ID:
				return AbsystemPackage.ASSET__ID;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", descriptionFormat: ");
		result.append(descriptionFormat);
		result.append(')');
		return result.toString();
	}

} //AssetImpl
