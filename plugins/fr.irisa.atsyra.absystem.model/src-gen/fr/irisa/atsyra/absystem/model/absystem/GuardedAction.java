/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guarded Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.GuardedAction#getGuardActions <em>Guard Actions</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getGuardedAction()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='ShouldHaveActions'"
 * @generated
 */
public interface GuardedAction extends Guard, Definition {
	/**
	 * Returns the value of the '<em><b>Guard Actions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Action}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guard Actions</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getGuardedAction_GuardActions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Action> getGuardActions();

} // GuardedAction
