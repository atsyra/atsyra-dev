/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Action#getTarget <em>Target</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Action#getArgs <em>Args</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Action#getActionType <em>Action Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Action#getLambdaAction <em>Lambda Action</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAction()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='TargetMultiplicity ArgMultiplicity NoUndefinedConstant Type'"
 * @generated
 */
public interface Action extends EObject {

	/**
	 * Returns the value of the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' containment reference.
	 * @see #setTarget(Expression)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAction_Target()
	 * @model containment="true"
	 * @generated
	 */
	Expression getTarget();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Action#getTarget <em>Target</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' containment reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Expression value);

	/**
	 * Returns the value of the '<em><b>Args</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Expression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Args</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAction_Args()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getArgs();

	/**
	 * Returns the value of the '<em><b>Action Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.atsyra.absystem.model.absystem.ActionEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Type</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.ActionEnum
	 * @see #setActionType(ActionEnum)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAction_ActionType()
	 * @model
	 * @generated
	 */
	ActionEnum getActionType();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Action#getActionType <em>Action Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Type</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.ActionEnum
	 * @see #getActionType()
	 * @generated
	 */
	void setActionType(ActionEnum value);

	/**
	 * Returns the value of the '<em><b>Lambda Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lambda Action</em>' containment reference.
	 * @see #setLambdaAction(LambdaAction)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAction_LambdaAction()
	 * @model containment="true"
	 * @generated
	 */
	LambdaAction getLambdaAction();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Action#getLambdaAction <em>Lambda Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lambda Action</em>' containment reference.
	 * @see #getLambdaAction()
	 * @generated
	 */
	void setLambdaAction(LambdaAction value);
} // Action
