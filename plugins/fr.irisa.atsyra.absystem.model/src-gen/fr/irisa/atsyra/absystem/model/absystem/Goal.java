/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getPrecondition <em>Precondition</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getPostcondition <em>Postcondition</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getDescriptionFormat <em>Description Format</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getGoal()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='Type'"
 * @generated
 */
public interface Goal extends Annotated, AssetGroupContent {
	/**
	 * Returns the value of the '<em><b>Precondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precondition</em>' containment reference.
	 * @see #setPrecondition(Expression)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getGoal_Precondition()
	 * @model containment="true"
	 * @generated
	 */
	Expression getPrecondition();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getPrecondition <em>Precondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Precondition</em>' containment reference.
	 * @see #getPrecondition()
	 * @generated
	 */
	void setPrecondition(Expression value);

	/**
	 * Returns the value of the '<em><b>Postcondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postcondition</em>' containment reference.
	 * @see #setPostcondition(Expression)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getGoal_Postcondition()
	 * @model containment="true"
	 * @generated
	 */
	Expression getPostcondition();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getPostcondition <em>Postcondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postcondition</em>' containment reference.
	 * @see #getPostcondition()
	 * @generated
	 */
	void setPostcondition(Expression value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getGoal_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getGoal_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Description Format</b></em>' attribute.
	 * The default value is <code>"plaintext"</code>.
	 * The literals are from the enumeration {@link fr.irisa.atsyra.absystem.model.absystem.TextFormat}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #setDescriptionFormat(TextFormat)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getGoal_DescriptionFormat()
	 * @model default="plaintext"
	 * @generated
	 */
	TextFormat getDescriptionFormat();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.Goal#getDescriptionFormat <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #getDescriptionFormat()
	 * @generated
	 */
	void setDescriptionFormat(TextFormat value);

} // Goal
