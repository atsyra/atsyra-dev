/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import java.util.Collection;
import java.util.stream.Stream;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.AnnotationKey;
import fr.irisa.atsyra.absystem.model.absystem.Contract;
import fr.irisa.atsyra.absystem.model.absystem.Definition;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod;
import fr.irisa.atsyra.absystem.model.absystem.Tag;
import fr.irisa.atsyra.absystem.model.absystem.util.UnmodifiableEListCollector;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definition Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getId <em>Id</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getDefinitions <em>Definitions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getStaticMethods <em>Static Methods</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getPrimitiveDataTypes <em>Primitive Data Types</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getTagDefinitions <em>Tag Definitions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getAnnotationKeys <em>Annotation Keys</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getAssetTypes <em>Asset Types</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getGuardedActions <em>Guarded Actions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getContracts <em>Contracts</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupImpl#getTags <em>Tags</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DefinitionGroupImpl extends AnnotatedImpl implements DefinitionGroup {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDefinitions() <em>Definitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Definition> definitions;

	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<Tag> tags;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefinitionGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.DEFINITION_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getId() {
		return eClass().getName() + '-' + getName();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public EList<AbstractAssetType> getAssetTypes() {
		return getDefinitions().stream().flatMap(element -> {
			if (element instanceof AbstractAssetType) {
				return Stream.of((AbstractAssetType) element);
			} else if (element instanceof DefinitionGroup) {
				return ((DefinitionGroup) element).getAssetTypes().stream();
			} else {
				return Stream.empty();
			}
		}).collect(new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getDefinitionGroup_AssetTypes()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public EList<PrimitiveDataType> getPrimitiveDataTypes() {
		return getDefinitions().stream().flatMap(element -> {
			if (element instanceof PrimitiveDataType) {
				return Stream.of((PrimitiveDataType) element);
			} else if (element instanceof DefinitionGroup) {
				return ((DefinitionGroup) element).getPrimitiveDataTypes().stream();
			} else {
				return Stream.empty();
			}
		}).collect(new UnmodifiableEListCollector<>(this,
				AbsystemPackage.eINSTANCE.getDefinitionGroup_PrimitiveDataTypes()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.DEFINITION_GROUP__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Definition> getDefinitions() {
		if (definitions == null) {
			definitions = new EObjectContainmentEList<Definition>(Definition.class, this,
					AbsystemPackage.DEFINITION_GROUP__DEFINITIONS);
		}
		return definitions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public EList<Tag> getTagDefinitions() {
		return getDefinitions().stream().flatMap(element -> {
			if (element instanceof Tag) {
				return Stream.of((Tag) element);
			} else if (element instanceof DefinitionGroup) {
				return ((DefinitionGroup) element).getTagDefinitions().stream();
			} else {
				return Stream.empty();
			}
		}).collect(
				new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getDefinitionGroup_TagDefinitions()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public EList<GuardedAction> getGuardedActions() {
		return getDefinitions().stream().flatMap(element -> {
			if (element instanceof GuardedAction) {
				return Stream.of((GuardedAction) element);
			} else if (element instanceof DefinitionGroup) {
				return ((DefinitionGroup) element).getGuardedActions().stream();
			} else {
				return Stream.empty();
			}
		}).collect(
				new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getDefinitionGroup_GuardedActions()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public EList<StaticMethod> getStaticMethods() {
		return getDefinitions().stream().flatMap(element -> {
			if (element instanceof StaticMethod) {
				return Stream.of((StaticMethod) element);
			} else if (element instanceof DefinitionGroup) {
				return ((DefinitionGroup) element).getStaticMethods().stream();
			} else {
				return Stream.empty();
			}
		}).collect(
				new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getDefinitionGroup_StaticMethods()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public EList<AnnotationKey> getAnnotationKeys() {
		return getDefinitions().stream().flatMap(element -> {
			if (element instanceof AnnotationKey) {
				return Stream.of((AnnotationKey) element);
			} else if (element instanceof DefinitionGroup) {
				return ((DefinitionGroup) element).getAnnotationKeys().stream();
			} else {
				return Stream.empty();
			}
		}).collect(
				new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getDefinitionGroup_AnnotationKeys()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public EList<Contract> getContracts() {
		return getDefinitions().stream().flatMap(element -> {
			if (element instanceof Contract) {
				return Stream.of((Contract) element);
			} else if (element instanceof DefinitionGroup) {
				return ((DefinitionGroup) element).getContracts().stream();
			} else {
				return Stream.empty();
			}
		}).collect(new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getDefinitionGroup_Contracts()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Tag> getTags() {
		if (tags == null) {
			tags = new EObjectResolvingEList<Tag>(Tag.class, this, AbsystemPackage.DEFINITION_GROUP__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP__DEFINITIONS:
			return ((InternalEList<?>) getDefinitions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP__ID:
			return getId();
		case AbsystemPackage.DEFINITION_GROUP__NAME:
			return getName();
		case AbsystemPackage.DEFINITION_GROUP__DEFINITIONS:
			return getDefinitions();
		case AbsystemPackage.DEFINITION_GROUP__STATIC_METHODS:
			return getStaticMethods();
		case AbsystemPackage.DEFINITION_GROUP__PRIMITIVE_DATA_TYPES:
			return getPrimitiveDataTypes();
		case AbsystemPackage.DEFINITION_GROUP__TAG_DEFINITIONS:
			return getTagDefinitions();
		case AbsystemPackage.DEFINITION_GROUP__ANNOTATION_KEYS:
			return getAnnotationKeys();
		case AbsystemPackage.DEFINITION_GROUP__ASSET_TYPES:
			return getAssetTypes();
		case AbsystemPackage.DEFINITION_GROUP__GUARDED_ACTIONS:
			return getGuardedActions();
		case AbsystemPackage.DEFINITION_GROUP__CONTRACTS:
			return getContracts();
		case AbsystemPackage.DEFINITION_GROUP__TAGS:
			return getTags();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP__NAME:
			setName((String) newValue);
			return;
		case AbsystemPackage.DEFINITION_GROUP__DEFINITIONS:
			getDefinitions().clear();
			getDefinitions().addAll((Collection<? extends Definition>) newValue);
			return;
		case AbsystemPackage.DEFINITION_GROUP__TAGS:
			getTags().clear();
			getTags().addAll((Collection<? extends Tag>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP__NAME:
			setName(NAME_EDEFAULT);
			return;
		case AbsystemPackage.DEFINITION_GROUP__DEFINITIONS:
			getDefinitions().clear();
			return;
		case AbsystemPackage.DEFINITION_GROUP__TAGS:
			getTags().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP__ID:
			return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
		case AbsystemPackage.DEFINITION_GROUP__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case AbsystemPackage.DEFINITION_GROUP__DEFINITIONS:
			return definitions != null && !definitions.isEmpty();
		case AbsystemPackage.DEFINITION_GROUP__STATIC_METHODS:
			return !getStaticMethods().isEmpty();
		case AbsystemPackage.DEFINITION_GROUP__PRIMITIVE_DATA_TYPES:
			return !getPrimitiveDataTypes().isEmpty();
		case AbsystemPackage.DEFINITION_GROUP__TAG_DEFINITIONS:
			return !getTagDefinitions().isEmpty();
		case AbsystemPackage.DEFINITION_GROUP__ANNOTATION_KEYS:
			return !getAnnotationKeys().isEmpty();
		case AbsystemPackage.DEFINITION_GROUP__ASSET_TYPES:
			return !getAssetTypes().isEmpty();
		case AbsystemPackage.DEFINITION_GROUP__GUARDED_ACTIONS:
			return !getGuardedActions().isEmpty();
		case AbsystemPackage.DEFINITION_GROUP__CONTRACTS:
			return !getContracts().isEmpty();
		case AbsystemPackage.DEFINITION_GROUP__TAGS:
			return tags != null && !tags.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Definition.class) {
			switch (derivedFeatureID) {
			case AbsystemPackage.DEFINITION_GROUP__ID:
				return AbsystemPackage.DEFINITION__ID;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Definition.class) {
			switch (baseFeatureID) {
			case AbsystemPackage.DEFINITION__ID:
				return AbsystemPackage.DEFINITION_GROUP__ID;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DefinitionGroupImpl
