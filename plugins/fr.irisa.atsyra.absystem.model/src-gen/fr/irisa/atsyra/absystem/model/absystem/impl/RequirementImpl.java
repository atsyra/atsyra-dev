/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Contract;
import fr.irisa.atsyra.absystem.model.absystem.Definition;
import fr.irisa.atsyra.absystem.model.absystem.Requirement;

import fr.irisa.atsyra.absystem.model.absystem.TextFormat;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl#getId <em>Id</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl#getDescriptionFormat <em>Description Format</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.RequirementImpl#getContracts <em>Contracts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequirementImpl extends AnnotatedImpl implements Requirement {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescriptionFormat() <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionFormat()
	 * @generated
	 * @ordered
	 */
	protected static final TextFormat DESCRIPTION_FORMAT_EDEFAULT = TextFormat.PLAINTEXT;

	/**
	 * The cached value of the '{@link #getDescriptionFormat() <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionFormat()
	 * @generated
	 * @ordered
	 */
	protected TextFormat descriptionFormat = DESCRIPTION_FORMAT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContracts() <em>Contracts</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContracts()
	 * @generated
	 * @ordered
	 */
	protected EList<Contract> contracts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getId() {
		return eClass().getName() + '-' + getName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.REQUIREMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.REQUIREMENT__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.REQUIREMENT__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextFormat getDescriptionFormat() {
		return descriptionFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescriptionFormat(TextFormat newDescriptionFormat) {
		TextFormat oldDescriptionFormat = descriptionFormat;
		descriptionFormat = newDescriptionFormat == null ? DESCRIPTION_FORMAT_EDEFAULT : newDescriptionFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.REQUIREMENT__DESCRIPTION_FORMAT,
					oldDescriptionFormat, descriptionFormat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Contract> getContracts() {
		if (contracts == null) {
			contracts = new EObjectResolvingEList<Contract>(Contract.class, this,
					AbsystemPackage.REQUIREMENT__CONTRACTS);
		}
		return contracts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.REQUIREMENT__ID:
			return getId();
		case AbsystemPackage.REQUIREMENT__NAME:
			return getName();
		case AbsystemPackage.REQUIREMENT__TITLE:
			return getTitle();
		case AbsystemPackage.REQUIREMENT__DESCRIPTION:
			return getDescription();
		case AbsystemPackage.REQUIREMENT__DESCRIPTION_FORMAT:
			return getDescriptionFormat();
		case AbsystemPackage.REQUIREMENT__CONTRACTS:
			return getContracts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.REQUIREMENT__NAME:
			setName((String) newValue);
			return;
		case AbsystemPackage.REQUIREMENT__TITLE:
			setTitle((String) newValue);
			return;
		case AbsystemPackage.REQUIREMENT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case AbsystemPackage.REQUIREMENT__DESCRIPTION_FORMAT:
			setDescriptionFormat((TextFormat) newValue);
			return;
		case AbsystemPackage.REQUIREMENT__CONTRACTS:
			getContracts().clear();
			getContracts().addAll((Collection<? extends Contract>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.REQUIREMENT__NAME:
			setName(NAME_EDEFAULT);
			return;
		case AbsystemPackage.REQUIREMENT__TITLE:
			setTitle(TITLE_EDEFAULT);
			return;
		case AbsystemPackage.REQUIREMENT__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case AbsystemPackage.REQUIREMENT__DESCRIPTION_FORMAT:
			setDescriptionFormat(DESCRIPTION_FORMAT_EDEFAULT);
			return;
		case AbsystemPackage.REQUIREMENT__CONTRACTS:
			getContracts().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.REQUIREMENT__ID:
			return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
		case AbsystemPackage.REQUIREMENT__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case AbsystemPackage.REQUIREMENT__TITLE:
			return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
		case AbsystemPackage.REQUIREMENT__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case AbsystemPackage.REQUIREMENT__DESCRIPTION_FORMAT:
			return descriptionFormat != DESCRIPTION_FORMAT_EDEFAULT;
		case AbsystemPackage.REQUIREMENT__CONTRACTS:
			return contracts != null && !contracts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Definition.class) {
			switch (derivedFeatureID) {
			case AbsystemPackage.REQUIREMENT__ID:
				return AbsystemPackage.DEFINITION__ID;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Definition.class) {
			switch (baseFeatureID) {
			case AbsystemPackage.DEFINITION__ID:
				return AbsystemPackage.REQUIREMENT__ID;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", title: ");
		result.append(title);
		result.append(", description: ");
		result.append(description);
		result.append(", descriptionFormat: ");
		result.append(descriptionFormat);
		result.append(')');
		return result.toString();
	}

} //RequirementImpl
