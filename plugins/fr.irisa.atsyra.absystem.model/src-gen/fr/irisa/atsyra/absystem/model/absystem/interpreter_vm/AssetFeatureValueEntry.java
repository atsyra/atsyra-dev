/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm;

import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Feature Value Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry#getKey <em>Key</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry#getValue <em>Value</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage#getAssetFeatureValueEntry()
 * @model annotation="aspect"
 * @generated
 */
public interface AssetFeatureValueEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(AssetTypeFeature)
	 * @see fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage#getAssetFeatureValueEntry_Key()
	 * @model required="true"
	 * @generated
	 */
	AssetTypeFeature getKey();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(AssetTypeFeature value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Value)
	 * @see fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage#getAssetFeatureValueEntry_Value()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Value getValue();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Value value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage#getAssetFeatureValueEntry_Name()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getName();

} // AssetFeatureValueEntry
