/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Expression;
import fr.irisa.atsyra.absystem.model.absystem.Member;
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Member Selection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.MemberSelectionImpl#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.MemberSelectionImpl#getMember <em>Member</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.MemberSelectionImpl#isMethodInvocation <em>Method Invocation</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.MemberSelectionImpl#getArgs <em>Args</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MemberSelectionImpl extends ExpressionImpl implements MemberSelection {
	/**
	 * The cached value of the '{@link #getReceiver() <em>Receiver</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceiver()
	 * @generated
	 * @ordered
	 */
	protected Expression receiver;

	/**
	 * The cached value of the '{@link #getMember() <em>Member</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMember()
	 * @generated
	 * @ordered
	 */
	protected Member member;

	/**
	 * The default value of the '{@link #isMethodInvocation() <em>Method Invocation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMethodInvocation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean METHOD_INVOCATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isMethodInvocation() <em>Method Invocation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMethodInvocation()
	 * @generated
	 * @ordered
	 */
	protected boolean methodInvocation = METHOD_INVOCATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getArgs() <em>Args</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArgs()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> args;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MemberSelectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.MEMBER_SELECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Expression getReceiver() {
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReceiver(Expression newReceiver, NotificationChain msgs) {
		Expression oldReceiver = receiver;
		receiver = newReceiver;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					AbsystemPackage.MEMBER_SELECTION__RECEIVER, oldReceiver, newReceiver);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReceiver(Expression newReceiver) {
		if (newReceiver != receiver) {
			NotificationChain msgs = null;
			if (receiver != null)
				msgs = ((InternalEObject) receiver).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.MEMBER_SELECTION__RECEIVER, null, msgs);
			if (newReceiver != null)
				msgs = ((InternalEObject) newReceiver).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.MEMBER_SELECTION__RECEIVER, null, msgs);
			msgs = basicSetReceiver(newReceiver, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.MEMBER_SELECTION__RECEIVER,
					newReceiver, newReceiver));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Member getMember() {
		if (member != null && member.eIsProxy()) {
			InternalEObject oldMember = (InternalEObject) member;
			member = (Member) eResolveProxy(oldMember);
			if (member != oldMember) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AbsystemPackage.MEMBER_SELECTION__MEMBER,
							oldMember, member));
			}
		}
		return member;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Member basicGetMember() {
		return member;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMember(Member newMember) {
		Member oldMember = member;
		member = newMember;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.MEMBER_SELECTION__MEMBER, oldMember,
					member));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isMethodInvocation() {
		return methodInvocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMethodInvocation(boolean newMethodInvocation) {
		boolean oldMethodInvocation = methodInvocation;
		methodInvocation = newMethodInvocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.MEMBER_SELECTION__METHOD_INVOCATION,
					oldMethodInvocation, methodInvocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Expression> getArgs() {
		if (args == null) {
			args = new EObjectContainmentEList<Expression>(Expression.class, this,
					AbsystemPackage.MEMBER_SELECTION__ARGS);
		}
		return args;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.MEMBER_SELECTION__RECEIVER:
			return basicSetReceiver(null, msgs);
		case AbsystemPackage.MEMBER_SELECTION__ARGS:
			return ((InternalEList<?>) getArgs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.MEMBER_SELECTION__RECEIVER:
			return getReceiver();
		case AbsystemPackage.MEMBER_SELECTION__MEMBER:
			if (resolve)
				return getMember();
			return basicGetMember();
		case AbsystemPackage.MEMBER_SELECTION__METHOD_INVOCATION:
			return isMethodInvocation();
		case AbsystemPackage.MEMBER_SELECTION__ARGS:
			return getArgs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.MEMBER_SELECTION__RECEIVER:
			setReceiver((Expression) newValue);
			return;
		case AbsystemPackage.MEMBER_SELECTION__MEMBER:
			setMember((Member) newValue);
			return;
		case AbsystemPackage.MEMBER_SELECTION__METHOD_INVOCATION:
			setMethodInvocation((Boolean) newValue);
			return;
		case AbsystemPackage.MEMBER_SELECTION__ARGS:
			getArgs().clear();
			getArgs().addAll((Collection<? extends Expression>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.MEMBER_SELECTION__RECEIVER:
			setReceiver((Expression) null);
			return;
		case AbsystemPackage.MEMBER_SELECTION__MEMBER:
			setMember((Member) null);
			return;
		case AbsystemPackage.MEMBER_SELECTION__METHOD_INVOCATION:
			setMethodInvocation(METHOD_INVOCATION_EDEFAULT);
			return;
		case AbsystemPackage.MEMBER_SELECTION__ARGS:
			getArgs().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.MEMBER_SELECTION__RECEIVER:
			return receiver != null;
		case AbsystemPackage.MEMBER_SELECTION__MEMBER:
			return member != null;
		case AbsystemPackage.MEMBER_SELECTION__METHOD_INVOCATION:
			return methodInvocation != METHOD_INVOCATION_EDEFAULT;
		case AbsystemPackage.MEMBER_SELECTION__ARGS:
			return args != null && !args.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (methodInvocation: ");
		result.append(methodInvocation);
		result.append(')');
		return result.toString();
	}

} //MemberSelectionImpl
