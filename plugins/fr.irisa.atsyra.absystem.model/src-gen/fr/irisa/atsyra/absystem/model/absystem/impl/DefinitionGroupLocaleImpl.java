/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definition Group Locale</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupLocaleImpl#getRef <em>Ref</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.DefinitionGroupLocaleImpl#getDefinitionLocales <em>Definition Locales</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DefinitionGroupLocaleImpl extends AnnotatedImpl implements DefinitionGroupLocale {
	/**
	 * The cached value of the '{@link #getRef() <em>Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRef()
	 * @generated
	 * @ordered
	 */
	protected DefinitionGroup ref;

	/**
	 * The cached value of the '{@link #getDefinitionLocales() <em>Definition Locales</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitionLocales()
	 * @generated
	 * @ordered
	 */
	protected EList<ABSObjectLocale> definitionLocales;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefinitionGroupLocaleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.DEFINITION_GROUP_LOCALE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DefinitionGroup getRef() {
		if (ref != null && ref.eIsProxy()) {
			InternalEObject oldRef = (InternalEObject) ref;
			ref = (DefinitionGroup) eResolveProxy(oldRef);
			if (ref != oldRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AbsystemPackage.DEFINITION_GROUP_LOCALE__REF, oldRef, ref));
			}
		}
		return ref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionGroup basicGetRef() {
		return ref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRef(DefinitionGroup newRef) {
		DefinitionGroup oldRef = ref;
		ref = newRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.DEFINITION_GROUP_LOCALE__REF, oldRef,
					ref));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ABSObjectLocale> getDefinitionLocales() {
		if (definitionLocales == null) {
			definitionLocales = new EObjectContainmentEList<ABSObjectLocale>(ABSObjectLocale.class, this,
					AbsystemPackage.DEFINITION_GROUP_LOCALE__DEFINITION_LOCALES);
		}
		return definitionLocales;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP_LOCALE__DEFINITION_LOCALES:
			return ((InternalEList<?>) getDefinitionLocales()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP_LOCALE__REF:
			if (resolve)
				return getRef();
			return basicGetRef();
		case AbsystemPackage.DEFINITION_GROUP_LOCALE__DEFINITION_LOCALES:
			return getDefinitionLocales();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP_LOCALE__REF:
			setRef((DefinitionGroup) newValue);
			return;
		case AbsystemPackage.DEFINITION_GROUP_LOCALE__DEFINITION_LOCALES:
			getDefinitionLocales().clear();
			getDefinitionLocales().addAll((Collection<? extends ABSObjectLocale>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP_LOCALE__REF:
			setRef((DefinitionGroup) null);
			return;
		case AbsystemPackage.DEFINITION_GROUP_LOCALE__DEFINITION_LOCALES:
			getDefinitionLocales().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.DEFINITION_GROUP_LOCALE__REF:
			return ref != null;
		case AbsystemPackage.DEFINITION_GROUP_LOCALE__DEFINITION_LOCALES:
			return definitionLocales != null && !definitionLocales.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DefinitionGroupLocaleImpl
