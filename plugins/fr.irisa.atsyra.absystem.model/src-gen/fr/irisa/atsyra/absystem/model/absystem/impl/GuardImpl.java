/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Expression;
import fr.irisa.atsyra.absystem.model.absystem.Guard;
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter;

import fr.irisa.atsyra.absystem.model.absystem.TextFormat;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Guard</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardImpl#getGuardExpression <em>Guard Expression</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardImpl#getGuardParameters <em>Guard Parameters</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.GuardImpl#getDescriptionFormat <em>Description Format</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class GuardImpl extends AnnotatedImpl implements Guard {
	/**
	 * The cached value of the '{@link #getGuardExpression() <em>Guard Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuardExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression guardExpression;

	/**
	 * The cached value of the '{@link #getGuardParameters() <em>Guard Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuardParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<GuardParameter> guardParameters;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescriptionFormat() <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionFormat()
	 * @generated
	 * @ordered
	 */
	protected static final TextFormat DESCRIPTION_FORMAT_EDEFAULT = TextFormat.PLAINTEXT;

	/**
	 * The cached value of the '{@link #getDescriptionFormat() <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionFormat()
	 * @generated
	 * @ordered
	 */
	protected TextFormat descriptionFormat = DESCRIPTION_FORMAT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GuardImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.GUARD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Expression getGuardExpression() {
		return guardExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGuardExpression(Expression newGuardExpression, NotificationChain msgs) {
		Expression oldGuardExpression = guardExpression;
		guardExpression = newGuardExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					AbsystemPackage.GUARD__GUARD_EXPRESSION, oldGuardExpression, newGuardExpression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGuardExpression(Expression newGuardExpression) {
		if (newGuardExpression != guardExpression) {
			NotificationChain msgs = null;
			if (guardExpression != null)
				msgs = ((InternalEObject) guardExpression).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.GUARD__GUARD_EXPRESSION, null, msgs);
			if (newGuardExpression != null)
				msgs = ((InternalEObject) newGuardExpression).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.GUARD__GUARD_EXPRESSION, null, msgs);
			msgs = basicSetGuardExpression(newGuardExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.GUARD__GUARD_EXPRESSION,
					newGuardExpression, newGuardExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<GuardParameter> getGuardParameters() {
		if (guardParameters == null) {
			guardParameters = new EObjectContainmentEList<GuardParameter>(GuardParameter.class, this,
					AbsystemPackage.GUARD__GUARD_PARAMETERS);
		}
		return guardParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.GUARD__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.GUARD__DESCRIPTION, oldDescription,
					description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextFormat getDescriptionFormat() {
		return descriptionFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescriptionFormat(TextFormat newDescriptionFormat) {
		TextFormat oldDescriptionFormat = descriptionFormat;
		descriptionFormat = newDescriptionFormat == null ? DESCRIPTION_FORMAT_EDEFAULT : newDescriptionFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.GUARD__DESCRIPTION_FORMAT,
					oldDescriptionFormat, descriptionFormat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.GUARD__GUARD_EXPRESSION:
			return basicSetGuardExpression(null, msgs);
		case AbsystemPackage.GUARD__GUARD_PARAMETERS:
			return ((InternalEList<?>) getGuardParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.GUARD__GUARD_EXPRESSION:
			return getGuardExpression();
		case AbsystemPackage.GUARD__GUARD_PARAMETERS:
			return getGuardParameters();
		case AbsystemPackage.GUARD__NAME:
			return getName();
		case AbsystemPackage.GUARD__DESCRIPTION:
			return getDescription();
		case AbsystemPackage.GUARD__DESCRIPTION_FORMAT:
			return getDescriptionFormat();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.GUARD__GUARD_EXPRESSION:
			setGuardExpression((Expression) newValue);
			return;
		case AbsystemPackage.GUARD__GUARD_PARAMETERS:
			getGuardParameters().clear();
			getGuardParameters().addAll((Collection<? extends GuardParameter>) newValue);
			return;
		case AbsystemPackage.GUARD__NAME:
			setName((String) newValue);
			return;
		case AbsystemPackage.GUARD__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case AbsystemPackage.GUARD__DESCRIPTION_FORMAT:
			setDescriptionFormat((TextFormat) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.GUARD__GUARD_EXPRESSION:
			setGuardExpression((Expression) null);
			return;
		case AbsystemPackage.GUARD__GUARD_PARAMETERS:
			getGuardParameters().clear();
			return;
		case AbsystemPackage.GUARD__NAME:
			setName(NAME_EDEFAULT);
			return;
		case AbsystemPackage.GUARD__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case AbsystemPackage.GUARD__DESCRIPTION_FORMAT:
			setDescriptionFormat(DESCRIPTION_FORMAT_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.GUARD__GUARD_EXPRESSION:
			return guardExpression != null;
		case AbsystemPackage.GUARD__GUARD_PARAMETERS:
			return guardParameters != null && !guardParameters.isEmpty();
		case AbsystemPackage.GUARD__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case AbsystemPackage.GUARD__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case AbsystemPackage.GUARD__DESCRIPTION_FORMAT:
			return descriptionFormat != DESCRIPTION_FORMAT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", descriptionFormat: ");
		result.append(descriptionFormat);
		result.append(')');
		return result.toString();
	}

} //GuardImpl
