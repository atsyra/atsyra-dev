/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Type Aspect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect#getBaseAssetType <em>Base Asset Type</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeAspect()
 * @model
 * @generated
 */
public interface AssetTypeAspect extends AbstractAssetType {
	/**
	 * Returns the value of the '<em><b>Base Asset Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Asset Type</em>' reference.
	 * @see #setBaseAssetType(AssetType)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeAspect_BaseAssetType()
	 * @model required="true"
	 * @generated
	 */
	AssetType getBaseAssetType();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect#getBaseAssetType <em>Base Asset Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Asset Type</em>' reference.
	 * @see #getBaseAssetType()
	 * @generated
	 */
	void setBaseAssetType(AssetType value);

} // AssetTypeAspect
