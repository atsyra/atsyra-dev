/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;

import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression;
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Attribute Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetAttributeValueImpl#getAttributeType <em>Attribute Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetAttributeValueImpl#getValues <em>Values</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetAttributeValueImpl#isCollection <em>Collection</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetAttributeValueImpl extends MinimalEObjectImpl.Container implements AssetAttributeValue {
	/**
	 * The cached value of the '{@link #getAttributeType() <em>Attribute Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeType()
	 * @generated
	 * @ordered
	 */
	protected AssetTypeAttribute attributeType;

	/**
	 * The cached value of the '{@link #getValues() <em>Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValues()
	 * @generated
	 * @ordered
	 */
	protected EList<ConstantExpression> values;

	/**
	 * The default value of the '{@link #isCollection() <em>Collection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCollection()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COLLECTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCollection() <em>Collection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCollection()
	 * @generated
	 * @ordered
	 */
	protected boolean collection = COLLECTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetAttributeValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ASSET_ATTRIBUTE_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeAttribute getAttributeType() {
		if (attributeType != null && attributeType.eIsProxy()) {
			InternalEObject oldAttributeType = (InternalEObject) attributeType;
			attributeType = (AssetTypeAttribute) eResolveProxy(oldAttributeType);
			if (attributeType != oldAttributeType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AbsystemPackage.ASSET_ATTRIBUTE_VALUE__ATTRIBUTE_TYPE, oldAttributeType, attributeType));
			}
		}
		return attributeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetTypeAttribute basicGetAttributeType() {
		return attributeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAttributeType(AssetTypeAttribute newAttributeType) {
		AssetTypeAttribute oldAttributeType = attributeType;
		attributeType = newAttributeType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_ATTRIBUTE_VALUE__ATTRIBUTE_TYPE,
					oldAttributeType, attributeType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ConstantExpression> getValues() {
		if (values == null) {
			values = new EObjectContainmentEList<ConstantExpression>(ConstantExpression.class, this,
					AbsystemPackage.ASSET_ATTRIBUTE_VALUE__VALUES);
		}
		return values;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isCollection() {
		return collection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCollection(boolean newCollection) {
		boolean oldCollection = collection;
		collection = newCollection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_ATTRIBUTE_VALUE__COLLECTION,
					oldCollection, collection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean onlyCollectionsHaveManyValues(DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!isCollection() && getValues().size() > 1) {
			if (diagnostics != null) {
				diagnostics
						.add(new BasicDiagnostic(Diagnostic.ERROR, AbsystemValidator.DIAGNOSTIC_SOURCE,
								AbsystemValidator.ASSET_ATTRIBUTE_VALUE__ONLY_COLLECTIONS_HAVE_MANY_VALUES,
								EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic",
										new Object[] { "onlyCollectionsHaveManyValues",
												EObjectValidator.getObjectLabel(this, context) }),
								new Object[] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__VALUES:
			return ((InternalEList<?>) getValues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__ATTRIBUTE_TYPE:
			if (resolve)
				return getAttributeType();
			return basicGetAttributeType();
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__VALUES:
			return getValues();
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__COLLECTION:
			return isCollection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__ATTRIBUTE_TYPE:
			setAttributeType((AssetTypeAttribute) newValue);
			return;
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__VALUES:
			getValues().clear();
			getValues().addAll((Collection<? extends ConstantExpression>) newValue);
			return;
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__COLLECTION:
			setCollection((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__ATTRIBUTE_TYPE:
			setAttributeType((AssetTypeAttribute) null);
			return;
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__VALUES:
			getValues().clear();
			return;
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__COLLECTION:
			setCollection(COLLECTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__ATTRIBUTE_TYPE:
			return attributeType != null;
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__VALUES:
			return values != null && !values.isEmpty();
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE__COLLECTION:
			return collection != COLLECTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case AbsystemPackage.ASSET_ATTRIBUTE_VALUE___ONLY_COLLECTIONS_HAVE_MANY_VALUES__DIAGNOSTICCHAIN_MAP:
			return onlyCollectionsHaveManyValues((DiagnosticChain) arguments.get(0),
					(Map<Object, Object>) arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (collection: ");
		result.append(collection);
		result.append(')');
		return result.toString();
	}

} //AssetAttributeValueImpl
