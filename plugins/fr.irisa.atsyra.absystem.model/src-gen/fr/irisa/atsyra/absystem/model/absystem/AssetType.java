/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getLevel <em>Level</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getTags <em>Tags</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getExtends <em>Extends</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getDescriptionFormat <em>Description Format</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getAllTags <em>All Tags</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetType()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='NoExtendsCycle'"
 * @generated
 */
public interface AssetType extends Annotated, AbstractAssetType {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetType_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see #setLevel(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetType_Level()
	 * @model
	 * @generated
	 */
	String getLevel();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(String value);

	/**
	 * Returns the value of the '<em><b>Tags</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Tag}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tags</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetType_Tags()
	 * @model
	 * @generated
	 */
	EList<Tag> getTags();

	/**
	 * Returns the value of the '<em><b>Extends</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AssetType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extends</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetType_Extends()
	 * @model
	 * @generated
	 */
	EList<AssetType> getExtends();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetType_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Description Format</b></em>' attribute.
	 * The default value is <code>"plaintext"</code>.
	 * The literals are from the enumeration {@link fr.irisa.atsyra.absystem.model.absystem.TextFormat}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #setDescriptionFormat(TextFormat)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetType_DescriptionFormat()
	 * @model default="plaintext"
	 * @generated
	 */
	TextFormat getDescriptionFormat();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#getDescriptionFormat <em>Description Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Format</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.TextFormat
	 * @see #getDescriptionFormat()
	 * @generated
	 */
	void setDescriptionFormat(TextFormat value);

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetType_Abstract()
	 * @model
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>All Tags</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Tag}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Tags</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetType_AllTags()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Tag> getAllTags();

} // AssetType
