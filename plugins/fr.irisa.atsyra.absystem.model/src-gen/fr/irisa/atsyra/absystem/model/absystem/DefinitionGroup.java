/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getDefinitions <em>Definitions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getStaticMethods <em>Static Methods</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getPrimitiveDataTypes <em>Primitive Data Types</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getTagDefinitions <em>Tag Definitions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getAnnotationKeys <em>Annotation Keys</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getAssetTypes <em>Asset Types</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getGuardedActions <em>Guarded Actions</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getContracts <em>Contracts</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getTags <em>Tags</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup()
 * @model
 * @generated
 */
public interface DefinitionGroup extends Annotated, Definition {
	/**
	 * Returns the value of the '<em><b>Asset Types</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Types</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_AssetTypes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<AbstractAssetType> getAssetTypes();

	/**
	 * Returns the value of the '<em><b>Primitive Data Types</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primitive Data Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primitive Data Types</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_PrimitiveDataTypes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<PrimitiveDataType> getPrimitiveDataTypes();

	/**
	 * Returns the value of the '<em><b>Tag Definitions</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Tag}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Definitions</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_TagDefinitions()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Tag> getTagDefinitions();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Definitions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Definition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definitions</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_Definitions()
	 * @model containment="true" keys="id"
	 * @generated
	 */
	EList<Definition> getDefinitions();

	/**
	 * Returns the value of the '<em><b>Guarded Actions</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.GuardedAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guarded Actions</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_GuardedActions()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<GuardedAction> getGuardedActions();

	/**
	 * Returns the value of the '<em><b>Static Methods</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.StaticMethod}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static Methods</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_StaticMethods()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<StaticMethod> getStaticMethods();

	/**
	 * Returns the value of the '<em><b>Annotation Keys</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.AnnotationKey}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Keys</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_AnnotationKeys()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<AnnotationKey> getAnnotationKeys();

	/**
	 * Returns the value of the '<em><b>Contracts</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Contract}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contracts</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_Contracts()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Contract> getContracts();

	/**
	 * Returns the value of the '<em><b>Tags</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Tag}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tags</em>' reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getDefinitionGroup_Tags()
	 * @model
	 * @generated
	 */
	EList<Tag> getTags();

} // DefinitionGroup
