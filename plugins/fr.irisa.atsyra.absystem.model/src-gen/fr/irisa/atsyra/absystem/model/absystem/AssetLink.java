/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getReferenceType <em>Reference Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getSourceAsset <em>Source Asset</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getTargetAsset <em>Target Asset</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getOppositeReferenceType <em>Opposite Reference Type</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetLink()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='WellTypedSource WellTypedTarget RespectMultiplicity'"
 * @generated
 */
public interface AssetLink extends AssetGroupContent {
	/**
	 * Returns the value of the '<em><b>Reference Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Type</em>' reference.
	 * @see #setReferenceType(AssetTypeReference)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetLink_ReferenceType()
	 * @model
	 * @generated
	 */
	AssetTypeReference getReferenceType();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getReferenceType <em>Reference Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Type</em>' reference.
	 * @see #getReferenceType()
	 * @generated
	 */
	void setReferenceType(AssetTypeReference value);

	/**
	 * Returns the value of the '<em><b>Source Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Asset</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Asset</em>' reference.
	 * @see #setSourceAsset(Asset)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetLink_SourceAsset()
	 * @model required="true"
	 * @generated
	 */
	Asset getSourceAsset();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getSourceAsset <em>Source Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Asset</em>' reference.
	 * @see #getSourceAsset()
	 * @generated
	 */
	void setSourceAsset(Asset value);

	/**
	 * Returns the value of the '<em><b>Target Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Asset</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Asset</em>' reference.
	 * @see #setTargetAsset(Asset)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetLink_TargetAsset()
	 * @model required="true"
	 * @generated
	 */
	Asset getTargetAsset();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getTargetAsset <em>Target Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Asset</em>' reference.
	 * @see #getTargetAsset()
	 * @generated
	 */
	void setTargetAsset(Asset value);

	/**
	 * Returns the value of the '<em><b>Opposite Reference Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opposite Reference Type</em>' reference.
	 * @see #setOppositeReferenceType(AssetTypeReference)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetLink_OppositeReferenceType()
	 * @model
	 * @generated
	 */
	AssetTypeReference getOppositeReferenceType();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink#getOppositeReferenceType <em>Opposite Reference Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opposite Reference Type</em>' reference.
	 * @see #getOppositeReferenceType()
	 * @generated
	 */
	void setOppositeReferenceType(AssetTypeReference value);

} // AssetLink
