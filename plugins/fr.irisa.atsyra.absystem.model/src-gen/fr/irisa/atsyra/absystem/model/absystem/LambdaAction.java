/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lambda Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.LambdaAction#getLambdaParameter <em>Lambda Parameter</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.LambdaAction#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getLambdaAction()
 * @model
 * @generated
 */
public interface LambdaAction extends EObject {
	/**
	 * Returns the value of the '<em><b>Lambda Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lambda Parameter</em>' containment reference.
	 * @see #setLambdaParameter(LambdaParameter)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getLambdaAction_LambdaParameter()
	 * @model containment="true"
	 * @generated
	 */
	LambdaParameter getLambdaParameter();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaAction#getLambdaParameter <em>Lambda Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lambda Parameter</em>' containment reference.
	 * @see #getLambdaParameter()
	 * @generated
	 */
	void setLambdaParameter(LambdaParameter value);

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Action}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getLambdaAction_Actions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Action> getActions();

} // LambdaAction
