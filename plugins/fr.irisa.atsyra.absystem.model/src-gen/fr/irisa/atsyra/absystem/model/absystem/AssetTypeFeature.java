/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Type Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature#isHasDefault <em>Has Default</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeFeature()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='MustNotShadow'"
 * @generated
 */
public interface AssetTypeFeature extends Member {
	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' attribute.
	 * The default value is <code>"ZeroOrOne"</code>.
	 * The literals are from the enumeration {@link fr.irisa.atsyra.absystem.model.absystem.Multiplicity}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Multiplicity
	 * @see #setMultiplicity(Multiplicity)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeFeature_Multiplicity()
	 * @model default="ZeroOrOne"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature#getMultiplicity <em>Multiplicity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' attribute.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Multiplicity
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

	/**
	 * Returns the value of the '<em><b>Has Default</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Default</em>' attribute.
	 * @see #setHasDefault(boolean)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeFeature_HasDefault()
	 * @model default="false"
	 * @generated
	 */
	boolean isHasDefault();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature#isHasDefault <em>Has Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Default</em>' attribute.
	 * @see #isHasDefault()
	 * @generated
	 */
	void setHasDefault(boolean value);

} // AssetTypeFeature
