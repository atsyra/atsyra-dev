/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Expression;
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression;
import fr.irisa.atsyra.absystem.model.absystem.LambdaParameter;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lambda Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaExpressionImpl#getLambdaParameter <em>Lambda Parameter</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaExpressionImpl#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LambdaExpressionImpl extends ExpressionImpl implements LambdaExpression {
	/**
	 * The cached value of the '{@link #getLambdaParameter() <em>Lambda Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLambdaParameter()
	 * @generated
	 * @ordered
	 */
	protected LambdaParameter lambdaParameter;

	/**
	 * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBody()
	 * @generated
	 * @ordered
	 */
	protected Expression body;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LambdaExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.LAMBDA_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LambdaParameter getLambdaParameter() {
		return lambdaParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLambdaParameter(LambdaParameter newLambdaParameter, NotificationChain msgs) {
		LambdaParameter oldLambdaParameter = lambdaParameter;
		lambdaParameter = newLambdaParameter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER, oldLambdaParameter, newLambdaParameter);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLambdaParameter(LambdaParameter newLambdaParameter) {
		if (newLambdaParameter != lambdaParameter) {
			NotificationChain msgs = null;
			if (lambdaParameter != null)
				msgs = ((InternalEObject) lambdaParameter).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER, null, msgs);
			if (newLambdaParameter != null)
				msgs = ((InternalEObject) newLambdaParameter).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER, null, msgs);
			msgs = basicSetLambdaParameter(newLambdaParameter, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER,
					newLambdaParameter, newLambdaParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Expression getBody() {
		return body;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBody(Expression newBody, NotificationChain msgs) {
		Expression oldBody = body;
		body = newBody;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					AbsystemPackage.LAMBDA_EXPRESSION__BODY, oldBody, newBody);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBody(Expression newBody) {
		if (newBody != body) {
			NotificationChain msgs = null;
			if (body != null)
				msgs = ((InternalEObject) body).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.LAMBDA_EXPRESSION__BODY, null, msgs);
			if (newBody != null)
				msgs = ((InternalEObject) newBody).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.LAMBDA_EXPRESSION__BODY, null, msgs);
			msgs = basicSetBody(newBody, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.LAMBDA_EXPRESSION__BODY, newBody,
					newBody));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER:
			return basicSetLambdaParameter(null, msgs);
		case AbsystemPackage.LAMBDA_EXPRESSION__BODY:
			return basicSetBody(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER:
			return getLambdaParameter();
		case AbsystemPackage.LAMBDA_EXPRESSION__BODY:
			return getBody();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER:
			setLambdaParameter((LambdaParameter) newValue);
			return;
		case AbsystemPackage.LAMBDA_EXPRESSION__BODY:
			setBody((Expression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER:
			setLambdaParameter((LambdaParameter) null);
			return;
		case AbsystemPackage.LAMBDA_EXPRESSION__BODY:
			setBody((Expression) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_EXPRESSION__LAMBDA_PARAMETER:
			return lambdaParameter != null;
		case AbsystemPackage.LAMBDA_EXPRESSION__BODY:
			return body != null;
		}
		return super.eIsSet(featureID);
	}

} //LambdaExpressionImpl
