/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale;
import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType;
import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetTypeLocale;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Action;
import fr.irisa.atsyra.absystem.model.absystem.ActionEnum;
import fr.irisa.atsyra.absystem.model.absystem.AndExpression;
import fr.irisa.atsyra.absystem.model.absystem.Annotated;
import fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry;
import fr.irisa.atsyra.absystem.model.absystem.AnnotationKey;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent;
import fr.irisa.atsyra.absystem.model.absystem.AssetLink;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.BinaryExpression;
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant;
import fr.irisa.atsyra.absystem.model.absystem.Collection;
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression;
import fr.irisa.atsyra.absystem.model.absystem.Contract;
import fr.irisa.atsyra.absystem.model.absystem.Definition;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale;
import fr.irisa.atsyra.absystem.model.absystem.EnumConstant;
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale;
import fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression;
import fr.irisa.atsyra.absystem.model.absystem.Expression;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.Guard;
import fr.irisa.atsyra.absystem.model.absystem.GuardLocale;
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter;
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;
import fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression;
import fr.irisa.atsyra.absystem.model.absystem.Import;
import fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression;
import fr.irisa.atsyra.absystem.model.absystem.IntConstant;
import fr.irisa.atsyra.absystem.model.absystem.LambdaAction;
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression;
import fr.irisa.atsyra.absystem.model.absystem.LambdaParameter;
import fr.irisa.atsyra.absystem.model.absystem.LocaleGroup;
import fr.irisa.atsyra.absystem.model.absystem.Member;
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection;
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity;
import fr.irisa.atsyra.absystem.model.absystem.NotExpression;
import fr.irisa.atsyra.absystem.model.absystem.OrExpression;
import fr.irisa.atsyra.absystem.model.absystem.Parameter;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale;
import fr.irisa.atsyra.absystem.model.absystem.Requirement;
import fr.irisa.atsyra.absystem.model.absystem.RequirementLocale;
import fr.irisa.atsyra.absystem.model.absystem.Severity;
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod;
import fr.irisa.atsyra.absystem.model.absystem.StringConstant;
import fr.irisa.atsyra.absystem.model.absystem.Symbol;
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef;
import fr.irisa.atsyra.absystem.model.absystem.Tag;
import fr.irisa.atsyra.absystem.model.absystem.TextFormat;
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant;
import fr.irisa.atsyra.absystem.model.absystem.Version;
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl.Interpreter_vmPackageImpl;

import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AbsystemPackageImpl extends EPackageImpl implements AbsystemPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetTypeReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetBasedSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetTypeAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetTypeFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitiveDataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumDataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetAttributeValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass definitionGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetTypeAspectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass guardedActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass guardParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impliesExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass andExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equalityComparisonExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inequalityComparisonExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constantExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass versionConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass memberSelectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass memberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolRefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass staticMethodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lambdaParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lambdaExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lambdaActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotationEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotationKeyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contractEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass guardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass undefinedConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass collectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass definitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetGroupContentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass localeGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass absObjectLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass definitionGroupLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitiveDataTypeLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumLiteralLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractAssetTypeLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetTypeLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetTypeAspectLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetTypeFeatureLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass guardLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotatedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requirementLocaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum actionEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum multiplicityEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum severityEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum textFormatEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eVersionEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AbsystemPackageImpl() {
		super(eNS_URI, AbsystemFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link AbsystemPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AbsystemPackage init() {
		if (isInited)
			return (AbsystemPackage) EPackage.Registry.INSTANCE.getEPackage(AbsystemPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredAbsystemPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		AbsystemPackageImpl theAbsystemPackage = registeredAbsystemPackage instanceof AbsystemPackageImpl
				? (AbsystemPackageImpl) registeredAbsystemPackage
				: new AbsystemPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(Interpreter_vmPackage.eNS_URI);
		Interpreter_vmPackageImpl theInterpreter_vmPackage = (Interpreter_vmPackageImpl) (registeredPackage instanceof Interpreter_vmPackageImpl
				? registeredPackage
				: Interpreter_vmPackage.eINSTANCE);

		// Create package meta-data objects
		theAbsystemPackage.createPackageContents();
		theInterpreter_vmPackage.createPackageContents();

		// Initialize created meta-data
		theAbsystemPackage.initializePackageContents();
		theInterpreter_vmPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put(theAbsystemPackage, new EValidator.Descriptor() {
			@Override
			public EValidator getEValidator() {
				return AbsystemValidator.INSTANCE;
			}
		});

		// Mark meta-data to indicate it can't be changed
		theAbsystemPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AbsystemPackage.eNS_URI, theAbsystemPackage);
		return theAbsystemPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetType() {
		return assetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetType_Name() {
		return (EAttribute) assetTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetType_Level() {
		return (EAttribute) assetTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetType_Tags() {
		return (EReference) assetTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetType_Extends() {
		return (EReference) assetTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetType_Description() {
		return (EAttribute) assetTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetType_DescriptionFormat() {
		return (EAttribute) assetTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetType_Abstract() {
		return (EAttribute) assetTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetType_AllTags() {
		return (EReference) assetTypeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetTypeReference() {
		return assetTypeReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetTypeReference_PropertyType() {
		return (EReference) assetTypeReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetTypeReference_IsContainer() {
		return (EAttribute) assetTypeReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetTypeReference_OppositeTypeReference() {
		return (EReference) assetTypeReferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetBasedSystem() {
		return assetBasedSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetBasedSystem_DefinitionGroups() {
		return (EReference) assetBasedSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetBasedSystem_AssetGroups() {
		return (EReference) assetBasedSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetBasedSystem_Imports() {
		return (EReference) assetBasedSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetBasedSystem_PossibleGuardedActions() {
		return (EReference) assetBasedSystemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetBasedSystem_AppliedActions() {
		return (EReference) assetBasedSystemEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetBasedSystem_AllAssets() {
		return (EReference) assetBasedSystemEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetBasedSystem_AllStaticAssetLinks() {
		return (EReference) assetBasedSystemEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetBasedSystem_Localizations() {
		return (EReference) assetBasedSystemEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAsset() {
		return assetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAsset_AssetType() {
		return (EReference) assetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAsset_AssetAttributeValues() {
		return (EReference) assetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAsset_FeaturesMap() {
		return (EReference) assetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAsset_Description() {
		return (EAttribute) assetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAsset_DescriptionFormat() {
		return (EAttribute) assetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetLink() {
		return assetLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetLink_ReferenceType() {
		return (EReference) assetLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetLink_SourceAsset() {
		return (EReference) assetLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetLink_TargetAsset() {
		return (EReference) assetLinkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetLink_OppositeReferenceType() {
		return (EReference) assetLinkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetTypeAttribute() {
		return assetTypeAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetTypeAttribute_AttributeType() {
		return (EReference) assetTypeAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetTypeAttribute_DefaultValues() {
		return (EReference) assetTypeAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetTypeFeature() {
		return assetTypeFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetTypeFeature_Multiplicity() {
		return (EAttribute) assetTypeFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetTypeFeature_HasDefault() {
		return (EAttribute) assetTypeFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPrimitiveDataType() {
		return primitiveDataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPrimitiveDataType_Name() {
		return (EAttribute) primitiveDataTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEnumDataType() {
		return enumDataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEnumDataType_EnumLiteral() {
		return (EReference) enumDataTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEnumLiteral() {
		return enumLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetAttributeValue() {
		return assetAttributeValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetAttributeValue_AttributeType() {
		return (EReference) assetAttributeValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetAttributeValue_Values() {
		return (EReference) assetAttributeValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetAttributeValue_Collection() {
		return (EAttribute) assetAttributeValueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAssetAttributeValue__OnlyCollectionsHaveManyValues__DiagnosticChain_Map() {
		return assetAttributeValueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDefinitionGroup() {
		return definitionGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroup_AssetTypes() {
		return (EReference) definitionGroupEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroup_PrimitiveDataTypes() {
		return (EReference) definitionGroupEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroup_TagDefinitions() {
		return (EReference) definitionGroupEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDefinitionGroup_Name() {
		return (EAttribute) definitionGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroup_Definitions() {
		return (EReference) definitionGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroup_GuardedActions() {
		return (EReference) definitionGroupEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroup_StaticMethods() {
		return (EReference) definitionGroupEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroup_AnnotationKeys() {
		return (EReference) definitionGroupEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroup_Contracts() {
		return (EReference) definitionGroupEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroup_Tags() {
		return (EReference) definitionGroupEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetGroup() {
		return assetGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetGroup_Assets() {
		return (EReference) assetGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetGroup_AssetLinks() {
		return (EReference) assetGroupEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetGroup_Goals() {
		return (EReference) assetGroupEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetGroup_Elements() {
		return (EReference) assetGroupEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetGroup_Name() {
		return (EAttribute) assetGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getImport() {
		return importEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImport_ImportedNamespace() {
		return (EAttribute) importEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImport_ImportURI() {
		return (EAttribute) importEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTag() {
		return tagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTag_Name() {
		return (EAttribute) tagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAbstractAssetType() {
		return abstractAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAbstractAssetType_AssetTypeAttributes() {
		return (EReference) abstractAssetTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAbstractAssetType_AssetTypeProperties() {
		return (EReference) abstractAssetTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetTypeAspect() {
		return assetTypeAspectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetTypeAspect_BaseAssetType() {
		return (EReference) assetTypeAspectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGuardedAction() {
		return guardedActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGuardedAction_GuardActions() {
		return (EReference) guardedActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGuardParameter() {
		return guardParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getExpression() {
		return expressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBinaryExpression() {
		return binaryExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryExpression_Lhs() {
		return (EReference) binaryExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryExpression_Rhs() {
		return (EReference) binaryExpressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getImpliesExpression() {
		return impliesExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOrExpression() {
		return orExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAndExpression() {
		return andExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNotExpression() {
		return notExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNotExpression_Expression() {
		return (EReference) notExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEqualityComparisonExpression() {
		return equalityComparisonExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEqualityComparisonExpression_Op() {
		return (EAttribute) equalityComparisonExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInequalityComparisonExpression() {
		return inequalityComparisonExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInequalityComparisonExpression_Op() {
		return (EAttribute) inequalityComparisonExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAction() {
		return actionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAction_Target() {
		return (EReference) actionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAction_Args() {
		return (EReference) actionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAction_ActionType() {
		return (EAttribute) actionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAction_LambdaAction() {
		return (EReference) actionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConstantExpression() {
		return constantExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringConstant() {
		return stringConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringConstant_Value() {
		return (EAttribute) stringConstantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIntConstant() {
		return intConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIntConstant_Value() {
		return (EAttribute) intConstantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBooleanConstant() {
		return booleanConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBooleanConstant_Value() {
		return (EAttribute) booleanConstantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVersionConstant() {
		return versionConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVersionConstant_Value() {
		return (EAttribute) versionConstantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMemberSelection() {
		return memberSelectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMemberSelection_Receiver() {
		return (EReference) memberSelectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMemberSelection_Member() {
		return (EReference) memberSelectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMemberSelection_MethodInvocation() {
		return (EAttribute) memberSelectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMemberSelection_Args() {
		return (EReference) memberSelectionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMember() {
		return memberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMember_Name() {
		return (EAttribute) memberEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSymbolRef() {
		return symbolRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSymbolRef_Symbol() {
		return (EReference) symbolRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSymbol() {
		return symbolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSymbol_Name() {
		return (EAttribute) symbolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStaticMethod() {
		return staticMethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLambdaParameter() {
		return lambdaParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLambdaExpression() {
		return lambdaExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLambdaExpression_LambdaParameter() {
		return (EReference) lambdaExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLambdaExpression_Body() {
		return (EReference) lambdaExpressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLambdaAction() {
		return lambdaActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLambdaAction_LambdaParameter() {
		return (EReference) lambdaActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLambdaAction_Actions() {
		return (EReference) lambdaActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getParameter() {
		return parameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParameter_ParameterType() {
		return (EReference) parameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAnnotationEntry() {
		return annotationEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAnnotationEntry_Value() {
		return (EAttribute) annotationEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAnnotationEntry_Key() {
		return (EReference) annotationEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAnnotationKey() {
		return annotationKeyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAnnotationKey_Name() {
		return (EAttribute) annotationKeyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGoal() {
		return goalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGoal_Precondition() {
		return (EReference) goalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGoal_Postcondition() {
		return (EReference) goalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGoal_Name() {
		return (EAttribute) goalEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGoal_Description() {
		return (EAttribute) goalEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGoal_DescriptionFormat() {
		return (EAttribute) goalEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getContract() {
		return contractEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getContract_Dynamic() {
		return (EAttribute) contractEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getContract_Severity() {
		return (EAttribute) contractEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGuard() {
		return guardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGuard_GuardExpression() {
		return (EReference) guardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGuard_GuardParameters() {
		return (EReference) guardEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGuard_Name() {
		return (EAttribute) guardEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGuard_Description() {
		return (EAttribute) guardEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGuard_DescriptionFormat() {
		return (EAttribute) guardEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEnumConstant() {
		return enumConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEnumConstant_Value() {
		return (EReference) enumConstantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUndefinedConstant() {
		return undefinedConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCollection() {
		return collectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCollection_Elements() {
		return (EReference) collectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDefinition() {
		return definitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDefinition_Id() {
		return (EAttribute) definitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetGroupContent() {
		return assetGroupContentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetGroupContent_Id() {
		return (EAttribute) assetGroupContentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLocaleGroup() {
		return localeGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLocaleGroup_Locale() {
		return (EAttribute) localeGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLocaleGroup_ObjectLocales() {
		return (EReference) localeGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getABSObjectLocale() {
		return absObjectLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDefinitionGroupLocale() {
		return definitionGroupLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroupLocale_Ref() {
		return (EReference) definitionGroupLocaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDefinitionGroupLocale_DefinitionLocales() {
		return (EReference) definitionGroupLocaleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPrimitiveDataTypeLocale() {
		return primitiveDataTypeLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPrimitiveDataTypeLocale_Ref() {
		return (EReference) primitiveDataTypeLocaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPrimitiveDataTypeLocale_Name() {
		return (EAttribute) primitiveDataTypeLocaleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPrimitiveDataTypeLocale_Literals() {
		return (EReference) primitiveDataTypeLocaleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEnumLiteralLocale() {
		return enumLiteralLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEnumLiteralLocale_Ref() {
		return (EReference) enumLiteralLocaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEnumLiteralLocale_Name() {
		return (EAttribute) enumLiteralLocaleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAbstractAssetTypeLocale() {
		return abstractAssetTypeLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAbstractAssetTypeLocale_Features() {
		return (EReference) abstractAssetTypeLocaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetTypeLocale() {
		return assetTypeLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetTypeLocale_Ref() {
		return (EReference) assetTypeLocaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetTypeLocale_Name() {
		return (EAttribute) assetTypeLocaleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetTypeLocale_Description() {
		return (EAttribute) assetTypeLocaleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetTypeLocale_DescriptionFormat() {
		return (EAttribute) assetTypeLocaleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetTypeAspectLocale() {
		return assetTypeAspectLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetTypeAspectLocale_BaseAssetType() {
		return (EReference) assetTypeAspectLocaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetTypeFeatureLocale() {
		return assetTypeFeatureLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetTypeFeatureLocale_Ref() {
		return (EReference) assetTypeFeatureLocaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetTypeFeatureLocale_Name() {
		return (EAttribute) assetTypeFeatureLocaleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGuardLocale() {
		return guardLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGuardLocale_Ref() {
		return (EReference) guardLocaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGuardLocale_Name() {
		return (EAttribute) guardLocaleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGuardLocale_Description() {
		return (EAttribute) guardLocaleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGuardLocale_DescriptionFormat() {
		return (EAttribute) guardLocaleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAnnotated() {
		return annotatedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAnnotated_Annotations() {
		return (EReference) annotatedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAnnotated_AllAnnotations() {
		return (EReference) annotatedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRequirement() {
		return requirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequirement_Name() {
		return (EAttribute) requirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequirement_Title() {
		return (EAttribute) requirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequirement_Description() {
		return (EAttribute) requirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequirement_DescriptionFormat() {
		return (EAttribute) requirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRequirement_Contracts() {
		return (EReference) requirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRequirementLocale() {
		return requirementLocaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRequirementLocale_Ref() {
		return (EReference) requirementLocaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequirementLocale_Title() {
		return (EAttribute) requirementLocaleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequirementLocale_Description() {
		return (EAttribute) requirementLocaleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequirementLocale_DescriptionFormat() {
		return (EAttribute) requirementLocaleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getActionEnum() {
		return actionEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getMultiplicity() {
		return multiplicityEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSeverity() {
		return severityEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getTextFormat() {
		return textFormatEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getEVersion() {
		return eVersionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbsystemFactory getAbsystemFactory() {
		return (AbsystemFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		assetTypeEClass = createEClass(ASSET_TYPE);
		createEAttribute(assetTypeEClass, ASSET_TYPE__NAME);
		createEAttribute(assetTypeEClass, ASSET_TYPE__LEVEL);
		createEReference(assetTypeEClass, ASSET_TYPE__TAGS);
		createEReference(assetTypeEClass, ASSET_TYPE__EXTENDS);
		createEAttribute(assetTypeEClass, ASSET_TYPE__DESCRIPTION);
		createEAttribute(assetTypeEClass, ASSET_TYPE__DESCRIPTION_FORMAT);
		createEAttribute(assetTypeEClass, ASSET_TYPE__ABSTRACT);
		createEReference(assetTypeEClass, ASSET_TYPE__ALL_TAGS);

		assetTypeReferenceEClass = createEClass(ASSET_TYPE_REFERENCE);
		createEReference(assetTypeReferenceEClass, ASSET_TYPE_REFERENCE__PROPERTY_TYPE);
		createEAttribute(assetTypeReferenceEClass, ASSET_TYPE_REFERENCE__IS_CONTAINER);
		createEReference(assetTypeReferenceEClass, ASSET_TYPE_REFERENCE__OPPOSITE_TYPE_REFERENCE);

		assetBasedSystemEClass = createEClass(ASSET_BASED_SYSTEM);
		createEReference(assetBasedSystemEClass, ASSET_BASED_SYSTEM__DEFINITION_GROUPS);
		createEReference(assetBasedSystemEClass, ASSET_BASED_SYSTEM__ASSET_GROUPS);
		createEReference(assetBasedSystemEClass, ASSET_BASED_SYSTEM__IMPORTS);
		createEReference(assetBasedSystemEClass, ASSET_BASED_SYSTEM__POSSIBLE_GUARDED_ACTIONS);
		createEReference(assetBasedSystemEClass, ASSET_BASED_SYSTEM__APPLIED_ACTIONS);
		createEReference(assetBasedSystemEClass, ASSET_BASED_SYSTEM__ALL_ASSETS);
		createEReference(assetBasedSystemEClass, ASSET_BASED_SYSTEM__ALL_STATIC_ASSET_LINKS);
		createEReference(assetBasedSystemEClass, ASSET_BASED_SYSTEM__LOCALIZATIONS);

		assetEClass = createEClass(ASSET);
		createEReference(assetEClass, ASSET__ASSET_TYPE);
		createEReference(assetEClass, ASSET__ASSET_ATTRIBUTE_VALUES);
		createEReference(assetEClass, ASSET__FEATURES_MAP);
		createEAttribute(assetEClass, ASSET__DESCRIPTION);
		createEAttribute(assetEClass, ASSET__DESCRIPTION_FORMAT);

		assetLinkEClass = createEClass(ASSET_LINK);
		createEReference(assetLinkEClass, ASSET_LINK__REFERENCE_TYPE);
		createEReference(assetLinkEClass, ASSET_LINK__SOURCE_ASSET);
		createEReference(assetLinkEClass, ASSET_LINK__TARGET_ASSET);
		createEReference(assetLinkEClass, ASSET_LINK__OPPOSITE_REFERENCE_TYPE);

		assetTypeAttributeEClass = createEClass(ASSET_TYPE_ATTRIBUTE);
		createEReference(assetTypeAttributeEClass, ASSET_TYPE_ATTRIBUTE__ATTRIBUTE_TYPE);
		createEReference(assetTypeAttributeEClass, ASSET_TYPE_ATTRIBUTE__DEFAULT_VALUES);

		assetTypeFeatureEClass = createEClass(ASSET_TYPE_FEATURE);
		createEAttribute(assetTypeFeatureEClass, ASSET_TYPE_FEATURE__MULTIPLICITY);
		createEAttribute(assetTypeFeatureEClass, ASSET_TYPE_FEATURE__HAS_DEFAULT);

		primitiveDataTypeEClass = createEClass(PRIMITIVE_DATA_TYPE);
		createEAttribute(primitiveDataTypeEClass, PRIMITIVE_DATA_TYPE__NAME);

		enumDataTypeEClass = createEClass(ENUM_DATA_TYPE);
		createEReference(enumDataTypeEClass, ENUM_DATA_TYPE__ENUM_LITERAL);

		enumLiteralEClass = createEClass(ENUM_LITERAL);

		assetAttributeValueEClass = createEClass(ASSET_ATTRIBUTE_VALUE);
		createEReference(assetAttributeValueEClass, ASSET_ATTRIBUTE_VALUE__ATTRIBUTE_TYPE);
		createEReference(assetAttributeValueEClass, ASSET_ATTRIBUTE_VALUE__VALUES);
		createEAttribute(assetAttributeValueEClass, ASSET_ATTRIBUTE_VALUE__COLLECTION);
		createEOperation(assetAttributeValueEClass,
				ASSET_ATTRIBUTE_VALUE___ONLY_COLLECTIONS_HAVE_MANY_VALUES__DIAGNOSTICCHAIN_MAP);

		definitionGroupEClass = createEClass(DEFINITION_GROUP);
		createEAttribute(definitionGroupEClass, DEFINITION_GROUP__NAME);
		createEReference(definitionGroupEClass, DEFINITION_GROUP__DEFINITIONS);
		createEReference(definitionGroupEClass, DEFINITION_GROUP__STATIC_METHODS);
		createEReference(definitionGroupEClass, DEFINITION_GROUP__PRIMITIVE_DATA_TYPES);
		createEReference(definitionGroupEClass, DEFINITION_GROUP__TAG_DEFINITIONS);
		createEReference(definitionGroupEClass, DEFINITION_GROUP__ANNOTATION_KEYS);
		createEReference(definitionGroupEClass, DEFINITION_GROUP__ASSET_TYPES);
		createEReference(definitionGroupEClass, DEFINITION_GROUP__GUARDED_ACTIONS);
		createEReference(definitionGroupEClass, DEFINITION_GROUP__CONTRACTS);
		createEReference(definitionGroupEClass, DEFINITION_GROUP__TAGS);

		assetGroupEClass = createEClass(ASSET_GROUP);
		createEAttribute(assetGroupEClass, ASSET_GROUP__NAME);
		createEReference(assetGroupEClass, ASSET_GROUP__ASSETS);
		createEReference(assetGroupEClass, ASSET_GROUP__ASSET_LINKS);
		createEReference(assetGroupEClass, ASSET_GROUP__GOALS);
		createEReference(assetGroupEClass, ASSET_GROUP__ELEMENTS);

		importEClass = createEClass(IMPORT);
		createEAttribute(importEClass, IMPORT__IMPORTED_NAMESPACE);
		createEAttribute(importEClass, IMPORT__IMPORT_URI);

		tagEClass = createEClass(TAG);
		createEAttribute(tagEClass, TAG__NAME);

		abstractAssetTypeEClass = createEClass(ABSTRACT_ASSET_TYPE);
		createEReference(abstractAssetTypeEClass, ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES);
		createEReference(abstractAssetTypeEClass, ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES);

		assetTypeAspectEClass = createEClass(ASSET_TYPE_ASPECT);
		createEReference(assetTypeAspectEClass, ASSET_TYPE_ASPECT__BASE_ASSET_TYPE);

		guardedActionEClass = createEClass(GUARDED_ACTION);
		createEReference(guardedActionEClass, GUARDED_ACTION__GUARD_ACTIONS);

		guardParameterEClass = createEClass(GUARD_PARAMETER);

		expressionEClass = createEClass(EXPRESSION);

		binaryExpressionEClass = createEClass(BINARY_EXPRESSION);
		createEReference(binaryExpressionEClass, BINARY_EXPRESSION__LHS);
		createEReference(binaryExpressionEClass, BINARY_EXPRESSION__RHS);

		impliesExpressionEClass = createEClass(IMPLIES_EXPRESSION);

		orExpressionEClass = createEClass(OR_EXPRESSION);

		andExpressionEClass = createEClass(AND_EXPRESSION);

		notExpressionEClass = createEClass(NOT_EXPRESSION);
		createEReference(notExpressionEClass, NOT_EXPRESSION__EXPRESSION);

		equalityComparisonExpressionEClass = createEClass(EQUALITY_COMPARISON_EXPRESSION);
		createEAttribute(equalityComparisonExpressionEClass, EQUALITY_COMPARISON_EXPRESSION__OP);

		inequalityComparisonExpressionEClass = createEClass(INEQUALITY_COMPARISON_EXPRESSION);
		createEAttribute(inequalityComparisonExpressionEClass, INEQUALITY_COMPARISON_EXPRESSION__OP);

		actionEClass = createEClass(ACTION);
		createEReference(actionEClass, ACTION__TARGET);
		createEReference(actionEClass, ACTION__ARGS);
		createEAttribute(actionEClass, ACTION__ACTION_TYPE);
		createEReference(actionEClass, ACTION__LAMBDA_ACTION);

		constantExpressionEClass = createEClass(CONSTANT_EXPRESSION);

		stringConstantEClass = createEClass(STRING_CONSTANT);
		createEAttribute(stringConstantEClass, STRING_CONSTANT__VALUE);

		intConstantEClass = createEClass(INT_CONSTANT);
		createEAttribute(intConstantEClass, INT_CONSTANT__VALUE);

		booleanConstantEClass = createEClass(BOOLEAN_CONSTANT);
		createEAttribute(booleanConstantEClass, BOOLEAN_CONSTANT__VALUE);

		versionConstantEClass = createEClass(VERSION_CONSTANT);
		createEAttribute(versionConstantEClass, VERSION_CONSTANT__VALUE);

		memberSelectionEClass = createEClass(MEMBER_SELECTION);
		createEReference(memberSelectionEClass, MEMBER_SELECTION__RECEIVER);
		createEReference(memberSelectionEClass, MEMBER_SELECTION__MEMBER);
		createEAttribute(memberSelectionEClass, MEMBER_SELECTION__METHOD_INVOCATION);
		createEReference(memberSelectionEClass, MEMBER_SELECTION__ARGS);

		memberEClass = createEClass(MEMBER);
		createEAttribute(memberEClass, MEMBER__NAME);

		symbolRefEClass = createEClass(SYMBOL_REF);
		createEReference(symbolRefEClass, SYMBOL_REF__SYMBOL);

		symbolEClass = createEClass(SYMBOL);
		createEAttribute(symbolEClass, SYMBOL__NAME);

		staticMethodEClass = createEClass(STATIC_METHOD);

		lambdaParameterEClass = createEClass(LAMBDA_PARAMETER);

		lambdaExpressionEClass = createEClass(LAMBDA_EXPRESSION);
		createEReference(lambdaExpressionEClass, LAMBDA_EXPRESSION__LAMBDA_PARAMETER);
		createEReference(lambdaExpressionEClass, LAMBDA_EXPRESSION__BODY);

		lambdaActionEClass = createEClass(LAMBDA_ACTION);
		createEReference(lambdaActionEClass, LAMBDA_ACTION__LAMBDA_PARAMETER);
		createEReference(lambdaActionEClass, LAMBDA_ACTION__ACTIONS);

		parameterEClass = createEClass(PARAMETER);
		createEReference(parameterEClass, PARAMETER__PARAMETER_TYPE);

		annotationEntryEClass = createEClass(ANNOTATION_ENTRY);
		createEAttribute(annotationEntryEClass, ANNOTATION_ENTRY__VALUE);
		createEReference(annotationEntryEClass, ANNOTATION_ENTRY__KEY);

		annotationKeyEClass = createEClass(ANNOTATION_KEY);
		createEAttribute(annotationKeyEClass, ANNOTATION_KEY__NAME);

		goalEClass = createEClass(GOAL);
		createEReference(goalEClass, GOAL__PRECONDITION);
		createEReference(goalEClass, GOAL__POSTCONDITION);
		createEAttribute(goalEClass, GOAL__NAME);
		createEAttribute(goalEClass, GOAL__DESCRIPTION);
		createEAttribute(goalEClass, GOAL__DESCRIPTION_FORMAT);

		contractEClass = createEClass(CONTRACT);
		createEAttribute(contractEClass, CONTRACT__DYNAMIC);
		createEAttribute(contractEClass, CONTRACT__SEVERITY);

		guardEClass = createEClass(GUARD);
		createEReference(guardEClass, GUARD__GUARD_EXPRESSION);
		createEReference(guardEClass, GUARD__GUARD_PARAMETERS);
		createEAttribute(guardEClass, GUARD__NAME);
		createEAttribute(guardEClass, GUARD__DESCRIPTION);
		createEAttribute(guardEClass, GUARD__DESCRIPTION_FORMAT);

		enumConstantEClass = createEClass(ENUM_CONSTANT);
		createEReference(enumConstantEClass, ENUM_CONSTANT__VALUE);

		undefinedConstantEClass = createEClass(UNDEFINED_CONSTANT);

		collectionEClass = createEClass(COLLECTION);
		createEReference(collectionEClass, COLLECTION__ELEMENTS);

		definitionEClass = createEClass(DEFINITION);
		createEAttribute(definitionEClass, DEFINITION__ID);

		assetGroupContentEClass = createEClass(ASSET_GROUP_CONTENT);
		createEAttribute(assetGroupContentEClass, ASSET_GROUP_CONTENT__ID);

		localeGroupEClass = createEClass(LOCALE_GROUP);
		createEAttribute(localeGroupEClass, LOCALE_GROUP__LOCALE);
		createEReference(localeGroupEClass, LOCALE_GROUP__OBJECT_LOCALES);

		absObjectLocaleEClass = createEClass(ABS_OBJECT_LOCALE);

		definitionGroupLocaleEClass = createEClass(DEFINITION_GROUP_LOCALE);
		createEReference(definitionGroupLocaleEClass, DEFINITION_GROUP_LOCALE__REF);
		createEReference(definitionGroupLocaleEClass, DEFINITION_GROUP_LOCALE__DEFINITION_LOCALES);

		primitiveDataTypeLocaleEClass = createEClass(PRIMITIVE_DATA_TYPE_LOCALE);
		createEReference(primitiveDataTypeLocaleEClass, PRIMITIVE_DATA_TYPE_LOCALE__REF);
		createEAttribute(primitiveDataTypeLocaleEClass, PRIMITIVE_DATA_TYPE_LOCALE__NAME);
		createEReference(primitiveDataTypeLocaleEClass, PRIMITIVE_DATA_TYPE_LOCALE__LITERALS);

		enumLiteralLocaleEClass = createEClass(ENUM_LITERAL_LOCALE);
		createEReference(enumLiteralLocaleEClass, ENUM_LITERAL_LOCALE__REF);
		createEAttribute(enumLiteralLocaleEClass, ENUM_LITERAL_LOCALE__NAME);

		abstractAssetTypeLocaleEClass = createEClass(ABSTRACT_ASSET_TYPE_LOCALE);
		createEReference(abstractAssetTypeLocaleEClass, ABSTRACT_ASSET_TYPE_LOCALE__FEATURES);

		assetTypeLocaleEClass = createEClass(ASSET_TYPE_LOCALE);
		createEReference(assetTypeLocaleEClass, ASSET_TYPE_LOCALE__REF);
		createEAttribute(assetTypeLocaleEClass, ASSET_TYPE_LOCALE__NAME);
		createEAttribute(assetTypeLocaleEClass, ASSET_TYPE_LOCALE__DESCRIPTION);
		createEAttribute(assetTypeLocaleEClass, ASSET_TYPE_LOCALE__DESCRIPTION_FORMAT);

		assetTypeAspectLocaleEClass = createEClass(ASSET_TYPE_ASPECT_LOCALE);
		createEReference(assetTypeAspectLocaleEClass, ASSET_TYPE_ASPECT_LOCALE__BASE_ASSET_TYPE);

		assetTypeFeatureLocaleEClass = createEClass(ASSET_TYPE_FEATURE_LOCALE);
		createEReference(assetTypeFeatureLocaleEClass, ASSET_TYPE_FEATURE_LOCALE__REF);
		createEAttribute(assetTypeFeatureLocaleEClass, ASSET_TYPE_FEATURE_LOCALE__NAME);

		guardLocaleEClass = createEClass(GUARD_LOCALE);
		createEReference(guardLocaleEClass, GUARD_LOCALE__REF);
		createEAttribute(guardLocaleEClass, GUARD_LOCALE__NAME);
		createEAttribute(guardLocaleEClass, GUARD_LOCALE__DESCRIPTION);
		createEAttribute(guardLocaleEClass, GUARD_LOCALE__DESCRIPTION_FORMAT);

		annotatedEClass = createEClass(ANNOTATED);
		createEReference(annotatedEClass, ANNOTATED__ANNOTATIONS);
		createEReference(annotatedEClass, ANNOTATED__ALL_ANNOTATIONS);

		requirementEClass = createEClass(REQUIREMENT);
		createEAttribute(requirementEClass, REQUIREMENT__NAME);
		createEAttribute(requirementEClass, REQUIREMENT__TITLE);
		createEAttribute(requirementEClass, REQUIREMENT__DESCRIPTION);
		createEAttribute(requirementEClass, REQUIREMENT__DESCRIPTION_FORMAT);
		createEReference(requirementEClass, REQUIREMENT__CONTRACTS);

		requirementLocaleEClass = createEClass(REQUIREMENT_LOCALE);
		createEReference(requirementLocaleEClass, REQUIREMENT_LOCALE__REF);
		createEAttribute(requirementLocaleEClass, REQUIREMENT_LOCALE__TITLE);
		createEAttribute(requirementLocaleEClass, REQUIREMENT_LOCALE__DESCRIPTION);
		createEAttribute(requirementLocaleEClass, REQUIREMENT_LOCALE__DESCRIPTION_FORMAT);

		// Create enums
		actionEnumEEnum = createEEnum(ACTION_ENUM);
		multiplicityEEnum = createEEnum(MULTIPLICITY);
		severityEEnum = createEEnum(SEVERITY);
		textFormatEEnum = createEEnum(TEXT_FORMAT);

		// Create data types
		eVersionEDataType = createEDataType(EVERSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Interpreter_vmPackage theInterpreter_vmPackage = (Interpreter_vmPackage) EPackage.Registry.INSTANCE
				.getEPackage(Interpreter_vmPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theInterpreter_vmPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		assetTypeEClass.getESuperTypes().add(this.getAnnotated());
		assetTypeEClass.getESuperTypes().add(this.getAbstractAssetType());
		assetTypeReferenceEClass.getESuperTypes().add(this.getAssetTypeFeature());
		assetEClass.getESuperTypes().add(this.getSymbol());
		assetEClass.getESuperTypes().add(this.getAssetGroupContent());
		assetLinkEClass.getESuperTypes().add(this.getAssetGroupContent());
		assetTypeAttributeEClass.getESuperTypes().add(this.getAssetTypeFeature());
		assetTypeFeatureEClass.getESuperTypes().add(this.getMember());
		primitiveDataTypeEClass.getESuperTypes().add(this.getAnnotated());
		primitiveDataTypeEClass.getESuperTypes().add(this.getDefinition());
		enumDataTypeEClass.getESuperTypes().add(this.getPrimitiveDataType());
		enumLiteralEClass.getESuperTypes().add(this.getSymbol());
		definitionGroupEClass.getESuperTypes().add(this.getAnnotated());
		definitionGroupEClass.getESuperTypes().add(this.getDefinition());
		assetGroupEClass.getESuperTypes().add(this.getAnnotated());
		assetGroupEClass.getESuperTypes().add(this.getAssetGroupContent());
		tagEClass.getESuperTypes().add(this.getDefinition());
		abstractAssetTypeEClass.getESuperTypes().add(this.getDefinition());
		assetTypeAspectEClass.getESuperTypes().add(this.getAbstractAssetType());
		guardedActionEClass.getESuperTypes().add(this.getGuard());
		guardedActionEClass.getESuperTypes().add(this.getDefinition());
		guardParameterEClass.getESuperTypes().add(this.getSymbol());
		guardParameterEClass.getESuperTypes().add(this.getParameter());
		binaryExpressionEClass.getESuperTypes().add(this.getExpression());
		impliesExpressionEClass.getESuperTypes().add(this.getBinaryExpression());
		orExpressionEClass.getESuperTypes().add(this.getBinaryExpression());
		andExpressionEClass.getESuperTypes().add(this.getBinaryExpression());
		notExpressionEClass.getESuperTypes().add(this.getExpression());
		equalityComparisonExpressionEClass.getESuperTypes().add(this.getBinaryExpression());
		inequalityComparisonExpressionEClass.getESuperTypes().add(this.getBinaryExpression());
		constantExpressionEClass.getESuperTypes().add(this.getExpression());
		stringConstantEClass.getESuperTypes().add(this.getConstantExpression());
		intConstantEClass.getESuperTypes().add(this.getConstantExpression());
		booleanConstantEClass.getESuperTypes().add(this.getConstantExpression());
		versionConstantEClass.getESuperTypes().add(this.getConstantExpression());
		memberSelectionEClass.getESuperTypes().add(this.getExpression());
		symbolRefEClass.getESuperTypes().add(this.getExpression());
		staticMethodEClass.getESuperTypes().add(this.getMember());
		staticMethodEClass.getESuperTypes().add(this.getDefinition());
		lambdaParameterEClass.getESuperTypes().add(this.getSymbol());
		lambdaParameterEClass.getESuperTypes().add(this.getParameter());
		lambdaExpressionEClass.getESuperTypes().add(this.getExpression());
		annotationKeyEClass.getESuperTypes().add(this.getDefinition());
		goalEClass.getESuperTypes().add(this.getAnnotated());
		goalEClass.getESuperTypes().add(this.getAssetGroupContent());
		contractEClass.getESuperTypes().add(this.getGuard());
		contractEClass.getESuperTypes().add(this.getDefinition());
		guardEClass.getESuperTypes().add(this.getAnnotated());
		enumConstantEClass.getESuperTypes().add(this.getConstantExpression());
		undefinedConstantEClass.getESuperTypes().add(this.getConstantExpression());
		collectionEClass.getESuperTypes().add(this.getExpression());
		definitionGroupLocaleEClass.getESuperTypes().add(this.getAnnotated());
		definitionGroupLocaleEClass.getESuperTypes().add(this.getABSObjectLocale());
		primitiveDataTypeLocaleEClass.getESuperTypes().add(this.getABSObjectLocale());
		abstractAssetTypeLocaleEClass.getESuperTypes().add(this.getABSObjectLocale());
		assetTypeLocaleEClass.getESuperTypes().add(this.getAbstractAssetTypeLocale());
		assetTypeAspectLocaleEClass.getESuperTypes().add(this.getAbstractAssetTypeLocale());
		guardLocaleEClass.getESuperTypes().add(this.getAnnotated());
		guardLocaleEClass.getESuperTypes().add(this.getABSObjectLocale());
		requirementEClass.getESuperTypes().add(this.getAnnotated());
		requirementEClass.getESuperTypes().add(this.getDefinition());
		requirementLocaleEClass.getESuperTypes().add(this.getAnnotated());
		requirementLocaleEClass.getESuperTypes().add(this.getABSObjectLocale());

		// Initialize classes, features, and operations; add parameters
		initEClass(assetTypeEClass, AssetType.class, "AssetType", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssetType_Name(), ecorePackage.getEString(), "name", null, 0, 1, AssetType.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetType_Level(), ecorePackage.getEString(), "level", null, 0, 1, AssetType.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetType_Tags(), this.getTag(), null, "tags", null, 0, -1, AssetType.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getAssetType_Extends(), this.getAssetType(), null, "extends", null, 0, -1, AssetType.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetType_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				AssetType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetType_DescriptionFormat(), this.getTextFormat(), "descriptionFormat", "plaintext", 0, 1,
				AssetType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetType_Abstract(), ecorePackage.getEBoolean(), "abstract", null, 0, 1, AssetType.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetType_AllTags(), this.getTag(), null, "allTags", null, 0, -1, AssetType.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(assetTypeReferenceEClass, AssetTypeReference.class, "AssetTypeReference", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetTypeReference_PropertyType(), this.getAssetType(), null, "propertyType", null, 1, 1,
				AssetTypeReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetTypeReference_IsContainer(), ecorePackage.getEBoolean(), "isContainer", null, 0, 1,
				AssetTypeReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getAssetTypeReference_OppositeTypeReference(), this.getAssetTypeReference(), null,
				"oppositeTypeReference", null, 0, 1, AssetTypeReference.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetBasedSystemEClass, AssetBasedSystem.class, "AssetBasedSystem", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetBasedSystem_DefinitionGroups(), this.getDefinitionGroup(), null, "definitionGroups",
				null, 0, -1, AssetBasedSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAssetBasedSystem_DefinitionGroups().getEKeys().add(this.getDefinition_Id());
		initEReference(getAssetBasedSystem_AssetGroups(), this.getAssetGroup(), null, "assetGroups", null, 0, -1,
				AssetBasedSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAssetBasedSystem_AssetGroups().getEKeys().add(this.getAssetGroup_Name());
		initEReference(getAssetBasedSystem_Imports(), this.getImport(), null, "imports", null, 0, -1,
				AssetBasedSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetBasedSystem_PossibleGuardedActions(), theInterpreter_vmPackage.getGuardOccurence(), null,
				"possibleGuardedActions", null, 0, -1, AssetBasedSystem.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetBasedSystem_AppliedActions(), theInterpreter_vmPackage.getGuardOccurence(), null,
				"appliedActions", null, 0, -1, AssetBasedSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetBasedSystem_AllAssets(), this.getAsset(), null, "allAssets", null, 0, -1,
				AssetBasedSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetBasedSystem_AllStaticAssetLinks(), this.getAssetLink(), null, "allStaticAssetLinks",
				null, 0, -1, AssetBasedSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetBasedSystem_Localizations(), this.getLocaleGroup(), null, "localizations", null, 0, -1,
				AssetBasedSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetEClass, Asset.class, "Asset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAsset_AssetType(), this.getAssetType(), null, "assetType", null, 1, 1, Asset.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAsset_AssetAttributeValues(), this.getAssetAttributeValue(), null, "assetAttributeValues",
				null, 0, -1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAsset_FeaturesMap(), theInterpreter_vmPackage.getAssetFeatureValueEntry(), null,
				"featuresMap", null, 0, -1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAsset_Description(), ecorePackage.getEString(), "description", null, 0, 1, Asset.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAsset_DescriptionFormat(), this.getTextFormat(), "descriptionFormat", "plaintext", 0, 1,
				Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(assetLinkEClass, AssetLink.class, "AssetLink", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetLink_ReferenceType(), this.getAssetTypeReference(), null, "referenceType", null, 0, 1,
				AssetLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetLink_SourceAsset(), this.getAsset(), null, "sourceAsset", null, 1, 1, AssetLink.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetLink_TargetAsset(), this.getAsset(), null, "targetAsset", null, 1, 1, AssetLink.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetLink_OppositeReferenceType(), this.getAssetTypeReference(), null,
				"oppositeReferenceType", null, 0, 1, AssetLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetTypeAttributeEClass, AssetTypeAttribute.class, "AssetTypeAttribute", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetTypeAttribute_AttributeType(), this.getPrimitiveDataType(), null, "attributeType", null,
				0, 1, AssetTypeAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetTypeAttribute_DefaultValues(), this.getConstantExpression(), null, "defaultValues", null,
				0, -1, AssetTypeAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetTypeFeatureEClass, AssetTypeFeature.class, "AssetTypeFeature", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssetTypeFeature_Multiplicity(), this.getMultiplicity(), "multiplicity", "ZeroOrOne", 0, 1,
				AssetTypeFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetTypeFeature_HasDefault(), ecorePackage.getEBoolean(), "hasDefault", "false", 0, 1,
				AssetTypeFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(primitiveDataTypeEClass, PrimitiveDataType.class, "PrimitiveDataType", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPrimitiveDataType_Name(), ecorePackage.getEString(), "name", null, 0, 1,
				PrimitiveDataType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(enumDataTypeEClass, EnumDataType.class, "EnumDataType", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumDataType_EnumLiteral(), this.getEnumLiteral(), null, "enumLiteral", null, 0, -1,
				EnumDataType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getEnumDataType_EnumLiteral().getEKeys().add(this.getSymbol_Name());

		initEClass(enumLiteralEClass, EnumLiteral.class, "EnumLiteral", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(assetAttributeValueEClass, AssetAttributeValue.class, "AssetAttributeValue", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetAttributeValue_AttributeType(), this.getAssetTypeAttribute(), null, "attributeType",
				null, 0, 1, AssetAttributeValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAttributeValue_Values(), this.getConstantExpression(), null, "values", null, 1, -1,
				AssetAttributeValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetAttributeValue_Collection(), ecorePackage.getEBoolean(), "collection", "false", 0, 1,
				AssetAttributeValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getAssetAttributeValue__OnlyCollectionsHaveManyValues__DiagnosticChain_Map(),
				ecorePackage.getEBoolean(), "onlyCollectionsHaveManyValues", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(definitionGroupEClass, DefinitionGroup.class, "DefinitionGroup", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDefinitionGroup_Name(), ecorePackage.getEString(), "name", null, 0, 1, DefinitionGroup.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDefinitionGroup_Definitions(), this.getDefinition(), null, "definitions", null, 0, -1,
				DefinitionGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getDefinitionGroup_Definitions().getEKeys().add(this.getDefinition_Id());
		initEReference(getDefinitionGroup_StaticMethods(), this.getStaticMethod(), null, "staticMethods", null, 0, -1,
				DefinitionGroup.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDefinitionGroup_PrimitiveDataTypes(), this.getPrimitiveDataType(), null, "primitiveDataTypes",
				null, 0, -1, DefinitionGroup.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDefinitionGroup_TagDefinitions(), this.getTag(), null, "tagDefinitions", null, 0, -1,
				DefinitionGroup.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDefinitionGroup_AnnotationKeys(), this.getAnnotationKey(), null, "annotationKeys", null, 0,
				-1, DefinitionGroup.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDefinitionGroup_AssetTypes(), this.getAbstractAssetType(), null, "assetTypes", null, 0, -1,
				DefinitionGroup.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDefinitionGroup_GuardedActions(), this.getGuardedAction(), null, "guardedActions", null, 0,
				-1, DefinitionGroup.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDefinitionGroup_Contracts(), this.getContract(), null, "contracts", null, 0, -1,
				DefinitionGroup.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDefinitionGroup_Tags(), this.getTag(), null, "tags", null, 0, -1, DefinitionGroup.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetGroupEClass, AssetGroup.class, "AssetGroup", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssetGroup_Name(), ecorePackage.getEString(), "name", null, 0, 1, AssetGroup.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetGroup_Assets(), this.getAsset(), null, "assets", null, 0, -1, AssetGroup.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAssetGroup_AssetLinks(), this.getAssetLink(), null, "assetLinks", null, 0, -1,
				AssetGroup.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAssetGroup_Goals(), this.getGoal(), null, "goals", null, 0, -1, AssetGroup.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAssetGroup_Elements(), this.getAssetGroupContent(), null, "elements", null, 0, -1,
				AssetGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAssetGroup_Elements().getEKeys().add(this.getAssetGroupContent_Id());

		initEClass(importEClass, Import.class, "Import", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImport_ImportedNamespace(), ecorePackage.getEString(), "importedNamespace", null, 0, 1,
				Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getImport_ImportURI(), ecorePackage.getEString(), "importURI", null, 0, 1, Import.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tagEClass, Tag.class, "Tag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTag_Name(), ecorePackage.getEString(), "name", null, 0, 1, Tag.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractAssetTypeEClass, AbstractAssetType.class, "AbstractAssetType", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractAssetType_AssetTypeAttributes(), this.getAssetTypeAttribute(), null,
				"assetTypeAttributes", null, 0, -1, AbstractAssetType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAbstractAssetType_AssetTypeAttributes().getEKeys().add(this.getMember_Name());
		initEReference(getAbstractAssetType_AssetTypeProperties(), this.getAssetTypeReference(), null,
				"assetTypeProperties", null, 0, -1, AbstractAssetType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAbstractAssetType_AssetTypeProperties().getEKeys().add(this.getMember_Name());

		initEClass(assetTypeAspectEClass, AssetTypeAspect.class, "AssetTypeAspect", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetTypeAspect_BaseAssetType(), this.getAssetType(), null, "baseAssetType", null, 1, 1,
				AssetTypeAspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(guardedActionEClass, GuardedAction.class, "GuardedAction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGuardedAction_GuardActions(), this.getAction(), null, "guardActions", null, 0, -1,
				GuardedAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(guardParameterEClass, GuardParameter.class, "GuardParameter", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(expressionEClass, Expression.class, "Expression", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(binaryExpressionEClass, BinaryExpression.class, "BinaryExpression", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinaryExpression_Lhs(), this.getExpression(), null, "lhs", null, 0, 1, BinaryExpression.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryExpression_Rhs(), this.getExpression(), null, "rhs", null, 0, 1, BinaryExpression.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impliesExpressionEClass, ImpliesExpression.class, "ImpliesExpression", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(orExpressionEClass, OrExpression.class, "OrExpression", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(andExpressionEClass, AndExpression.class, "AndExpression", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(notExpressionEClass, NotExpression.class, "NotExpression", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNotExpression_Expression(), this.getExpression(), null, "expression", null, 0, 1,
				NotExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equalityComparisonExpressionEClass, EqualityComparisonExpression.class,
				"EqualityComparisonExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEqualityComparisonExpression_Op(), ecorePackage.getEString(), "op", null, 0, 1,
				EqualityComparisonExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inequalityComparisonExpressionEClass, InequalityComparisonExpression.class,
				"InequalityComparisonExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInequalityComparisonExpression_Op(), ecorePackage.getEString(), "op", null, 0, 1,
				InequalityComparisonExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actionEClass, Action.class, "Action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAction_Target(), this.getExpression(), null, "target", null, 0, 1, Action.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAction_Args(), this.getExpression(), null, "args", null, 0, -1, Action.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getAction_ActionType(), this.getActionEnum(), "actionType", null, 0, 1, Action.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAction_LambdaAction(), this.getLambdaAction(), null, "lambdaAction", null, 0, 1, Action.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constantExpressionEClass, ConstantExpression.class, "ConstantExpression", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(stringConstantEClass, StringConstant.class, "StringConstant", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringConstant_Value(), ecorePackage.getEString(), "value", null, 0, 1, StringConstant.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(intConstantEClass, IntConstant.class, "IntConstant", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntConstant_Value(), ecorePackage.getEInt(), "value", null, 0, 1, IntConstant.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanConstantEClass, BooleanConstant.class, "BooleanConstant", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanConstant_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				BooleanConstant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(versionConstantEClass, VersionConstant.class, "VersionConstant", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVersionConstant_Value(), this.getEVersion(), "value", null, 0, 1, VersionConstant.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(memberSelectionEClass, MemberSelection.class, "MemberSelection", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMemberSelection_Receiver(), this.getExpression(), null, "receiver", null, 0, 1,
				MemberSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMemberSelection_Member(), this.getMember(), null, "member", null, 0, 1, MemberSelection.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMemberSelection_MethodInvocation(), ecorePackage.getEBoolean(), "methodInvocation", null, 0,
				1, MemberSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMemberSelection_Args(), this.getExpression(), null, "args", null, 0, -1,
				MemberSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(memberEClass, Member.class, "Member", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMember_Name(), ecorePackage.getEString(), "name", null, 0, 1, Member.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(symbolRefEClass, SymbolRef.class, "SymbolRef", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolRef_Symbol(), this.getSymbol(), null, "symbol", null, 0, 1, SymbolRef.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(symbolEClass, Symbol.class, "Symbol", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSymbol_Name(), ecorePackage.getEString(), "name", null, 0, 1, Symbol.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(staticMethodEClass, StaticMethod.class, "StaticMethod", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(lambdaParameterEClass, LambdaParameter.class, "LambdaParameter", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(lambdaExpressionEClass, LambdaExpression.class, "LambdaExpression", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLambdaExpression_LambdaParameter(), this.getLambdaParameter(), null, "lambdaParameter", null,
				0, 1, LambdaExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLambdaExpression_Body(), this.getExpression(), null, "body", null, 0, 1,
				LambdaExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lambdaActionEClass, LambdaAction.class, "LambdaAction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLambdaAction_LambdaParameter(), this.getLambdaParameter(), null, "lambdaParameter", null, 0,
				1, LambdaAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLambdaAction_Actions(), this.getAction(), null, "actions", null, 0, -1, LambdaAction.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(parameterEClass, Parameter.class, "Parameter", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getParameter_ParameterType(), this.getAssetType(), null, "parameterType", null, 0, 1,
				Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annotationEntryEClass, AnnotationEntry.class, "AnnotationEntry", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAnnotationEntry_Value(), ecorePackage.getEString(), "value", null, 1, 1,
				AnnotationEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getAnnotationEntry_Key(), this.getAnnotationKey(), null, "key", null, 1, 1,
				AnnotationEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annotationKeyEClass, AnnotationKey.class, "AnnotationKey", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAnnotationKey_Name(), ecorePackage.getEString(), "name", null, 1, 1, AnnotationKey.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(goalEClass, Goal.class, "Goal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGoal_Precondition(), this.getExpression(), null, "precondition", null, 0, 1, Goal.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGoal_Postcondition(), this.getExpression(), null, "postcondition", null, 0, 1, Goal.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGoal_Name(), ecorePackage.getEString(), "name", null, 0, 1, Goal.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGoal_Description(), ecorePackage.getEString(), "description", null, 0, 1, Goal.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGoal_DescriptionFormat(), this.getTextFormat(), "descriptionFormat", "plaintext", 0, 1,
				Goal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(contractEClass, Contract.class, "Contract", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getContract_Dynamic(), ecorePackage.getEBoolean(), "dynamic", null, 0, 1, Contract.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getContract_Severity(), this.getSeverity(), "severity", null, 0, 1, Contract.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(guardEClass, Guard.class, "Guard", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGuard_GuardExpression(), this.getExpression(), null, "guardExpression", null, 0, 1,
				Guard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGuard_GuardParameters(), this.getGuardParameter(), null, "guardParameters", null, 0, -1,
				Guard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGuard_Name(), ecorePackage.getEString(), "name", null, 0, 1, Guard.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGuard_Description(), ecorePackage.getEString(), "description", null, 0, 1, Guard.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGuard_DescriptionFormat(), this.getTextFormat(), "descriptionFormat", "plaintext", 0, 1,
				Guard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(enumConstantEClass, EnumConstant.class, "EnumConstant", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumConstant_Value(), this.getEnumLiteral(), null, "value", null, 1, 1, EnumConstant.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(undefinedConstantEClass, UndefinedConstant.class, "UndefinedConstant", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(collectionEClass, Collection.class, "Collection", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCollection_Elements(), this.getExpression(), null, "elements", null, 0, -1, Collection.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(definitionEClass, Definition.class, "Definition", IS_ABSTRACT, IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDefinition_Id(), ecorePackage.getEString(), "id", null, 1, 1, Definition.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(assetGroupContentEClass, AssetGroupContent.class, "AssetGroupContent", IS_ABSTRACT, IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssetGroupContent_Id(), ecorePackage.getEString(), "id", null, 1, 1, AssetGroupContent.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(localeGroupEClass, LocaleGroup.class, "LocaleGroup", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLocaleGroup_Locale(), ecorePackage.getEString(), "locale", null, 0, 1, LocaleGroup.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLocaleGroup_ObjectLocales(), this.getABSObjectLocale(), null, "objectLocales", null, 0, -1,
				LocaleGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(absObjectLocaleEClass, ABSObjectLocale.class, "ABSObjectLocale", IS_ABSTRACT, IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(definitionGroupLocaleEClass, DefinitionGroupLocale.class, "DefinitionGroupLocale", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDefinitionGroupLocale_Ref(), this.getDefinitionGroup(), null, "ref", null, 0, 1,
				DefinitionGroupLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDefinitionGroupLocale_DefinitionLocales(), this.getABSObjectLocale(), null,
				"definitionLocales", null, 0, -1, DefinitionGroupLocale.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(primitiveDataTypeLocaleEClass, PrimitiveDataTypeLocale.class, "PrimitiveDataTypeLocale",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPrimitiveDataTypeLocale_Ref(), this.getPrimitiveDataType(), null, "ref", null, 0, 1,
				PrimitiveDataTypeLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPrimitiveDataTypeLocale_Name(), ecorePackage.getEString(), "name", null, 0, 1,
				PrimitiveDataTypeLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPrimitiveDataTypeLocale_Literals(), this.getEnumLiteralLocale(), null, "literals", null, 0,
				-1, PrimitiveDataTypeLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(enumLiteralLocaleEClass, EnumLiteralLocale.class, "EnumLiteralLocale", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumLiteralLocale_Ref(), this.getEnumLiteral(), null, "ref", null, 0, 1,
				EnumLiteralLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnumLiteralLocale_Name(), ecorePackage.getEString(), "name", null, 0, 1,
				EnumLiteralLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(abstractAssetTypeLocaleEClass, AbstractAssetTypeLocale.class, "AbstractAssetTypeLocale", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractAssetTypeLocale_Features(), this.getAssetTypeFeatureLocale(), null, "features", null,
				0, -1, AbstractAssetTypeLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetTypeLocaleEClass, AssetTypeLocale.class, "AssetTypeLocale", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetTypeLocale_Ref(), this.getAssetType(), null, "ref", null, 0, 1, AssetTypeLocale.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetTypeLocale_Name(), ecorePackage.getEString(), "name", null, 0, 1, AssetTypeLocale.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetTypeLocale_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				AssetTypeLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetTypeLocale_DescriptionFormat(), this.getTextFormat(), "descriptionFormat", "plaintext",
				0, 1, AssetTypeLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetTypeAspectLocaleEClass, AssetTypeAspectLocale.class, "AssetTypeAspectLocale", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetTypeAspectLocale_BaseAssetType(), this.getAssetType(), null, "baseAssetType", null, 0, 1,
				AssetTypeAspectLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetTypeFeatureLocaleEClass, AssetTypeFeatureLocale.class, "AssetTypeFeatureLocale", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetTypeFeatureLocale_Ref(), this.getAssetTypeFeature(), null, "ref", null, 0, 1,
				AssetTypeFeatureLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetTypeFeatureLocale_Name(), ecorePackage.getEString(), "name", null, 0, 1,
				AssetTypeFeatureLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(guardLocaleEClass, GuardLocale.class, "GuardLocale", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGuardLocale_Ref(), this.getGuard(), null, "ref", null, 0, 1, GuardLocale.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getGuardLocale_Name(), ecorePackage.getEString(), "name", null, 0, 1, GuardLocale.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGuardLocale_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				GuardLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getGuardLocale_DescriptionFormat(), this.getTextFormat(), "descriptionFormat", "plaintext", 0, 1,
				GuardLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(annotatedEClass, Annotated.class, "Annotated", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnotated_Annotations(), this.getAnnotationEntry(), null, "annotations", null, 0, -1,
				Annotated.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnnotated_AllAnnotations(), this.getAnnotationEntry(), null, "allAnnotations", null, 0, -1,
				Annotated.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(requirementEClass, Requirement.class, "Requirement", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRequirement_Name(), ecorePackage.getEString(), "name", null, 0, 1, Requirement.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirement_Title(), ecorePackage.getEString(), "title", null, 0, 1, Requirement.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirement_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirement_DescriptionFormat(), this.getTextFormat(), "descriptionFormat", "plaintext", 0, 1,
				Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getRequirement_Contracts(), this.getContract(), null, "contracts", null, 0, -1,
				Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getRequirement_Contracts().getEKeys().add(this.getDefinition_Id());

		initEClass(requirementLocaleEClass, RequirementLocale.class, "RequirementLocale", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRequirementLocale_Ref(), this.getRequirement(), null, "ref", null, 0, 1,
				RequirementLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirementLocale_Title(), ecorePackage.getEString(), "title", null, 0, 1,
				RequirementLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirementLocale_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				RequirementLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirementLocale_DescriptionFormat(), this.getTextFormat(), "descriptionFormat", "plaintext",
				0, 1, RequirementLocale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(actionEnumEEnum, ActionEnum.class, "ActionEnum");
		addEEnumLiteral(actionEnumEEnum, ActionEnum.ASSIGN);
		addEEnumLiteral(actionEnumEEnum, ActionEnum.ADD);
		addEEnumLiteral(actionEnumEEnum, ActionEnum.REMOVE);
		addEEnumLiteral(actionEnumEEnum, ActionEnum.ADD_ALL);
		addEEnumLiteral(actionEnumEEnum, ActionEnum.REMOVE_ALL);
		addEEnumLiteral(actionEnumEEnum, ActionEnum.CLEAR);
		addEEnumLiteral(actionEnumEEnum, ActionEnum.FOR_ALL);

		initEEnum(multiplicityEEnum, Multiplicity.class, "Multiplicity");
		addEEnumLiteral(multiplicityEEnum, Multiplicity.ZERO_OR_ONE);
		addEEnumLiteral(multiplicityEEnum, Multiplicity.ONE);
		addEEnumLiteral(multiplicityEEnum, Multiplicity.ZERO_OR_MANY);
		addEEnumLiteral(multiplicityEEnum, Multiplicity.ONE_OR_MANY);

		initEEnum(severityEEnum, Severity.class, "Severity");
		addEEnumLiteral(severityEEnum, Severity.ERROR);
		addEEnumLiteral(severityEEnum, Severity.WARNING);

		initEEnum(textFormatEEnum, TextFormat.class, "TextFormat");
		addEEnumLiteral(textFormatEEnum, TextFormat.PLAINTEXT);
		addEEnumLiteral(textFormatEEnum, TextFormat.HTML);
		addEEnumLiteral(textFormatEEnum, TextFormat.MARKDOWN);

		// Initialize data types
		initEDataType(eVersionEDataType, Version.class, "EVersion", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// aspect
		createAspectAnnotations();
	}

	/**
	 * Initializes the annotations for <b>aspect</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createAspectAnnotations() {
		String source = "aspect";
		addAnnotation(getAssetBasedSystem_PossibleGuardedActions(), source, new String[] {});
		addAnnotation(getAssetBasedSystem_AppliedActions(), source, new String[] {});
		addAnnotation(getAsset_FeaturesMap(), source, new String[] {});
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";
		addAnnotation(assetTypeEClass, source, new String[] { "constraints", "NoExtendsCycle" });
		addAnnotation(assetTypeReferenceEClass, source, new String[] { "constraints", "DefaultMultiplicity" });
		addAnnotation(assetEClass, source,
				new String[] { "constraints", "AllMandatoryStaticFeaturesSet TypeIsNotAbstract" });
		addAnnotation(assetLinkEClass, source,
				new String[] { "constraints", "WellTypedSource WellTypedTarget RespectMultiplicity" });
		addAnnotation(assetTypeAttributeEClass, source,
				new String[] { "constraints", "DefaultType DefaultMultiplicity" });
		addAnnotation(assetTypeFeatureEClass, source, new String[] { "constraints", "MustNotShadow" });
		addAnnotation(primitiveDataTypeEClass, source, new String[] { "constraints", "KnownName" });
		addAnnotation(assetAttributeValueEClass, source, new String[] { "constraints",
				"NoDynamicAttributeSet NoDuplicateAttribute CollectionMatchesMultiplicity" });
		addAnnotation(getAssetAttributeValue__OnlyCollectionsHaveManyValues__DiagnosticChain_Map(), source,
				new String[] { "invariant", "true" });
		addAnnotation(guardedActionEClass, source, new String[] { "constraints", "ShouldHaveActions" });
		addAnnotation(impliesExpressionEClass, source, new String[] { "constraints", "Type" });
		addAnnotation(orExpressionEClass, source, new String[] { "constraints", "Type" });
		addAnnotation(andExpressionEClass, source, new String[] { "constraints", "Type" });
		addAnnotation(notExpressionEClass, source, new String[] { "constraints", "Type" });
		addAnnotation(equalityComparisonExpressionEClass, source,
				new String[] { "constraints", "NoCollections ComparableTypes" });
		addAnnotation(inequalityComparisonExpressionEClass, source,
				new String[] { "constraints", "NoCollections ComparableTypes" });
		addAnnotation(actionEClass, source,
				new String[] { "constraints", "TargetMultiplicity ArgMultiplicity NoUndefinedConstant Type" });
		addAnnotation(memberSelectionEClass, source, new String[] { "constraints",
				"ReceiverMultiplicity ArgMultiplicity InvocationIsMethod NoUndefinedConstant Type" });
		addAnnotation(symbolRefEClass, source, new String[] { "constraints", "InScope" });
		addAnnotation(staticMethodEClass, source, new String[] { "constraints", "KnownName" });
		addAnnotation(lambdaExpressionEClass, source, new String[] { "constraints", "Type" });
		addAnnotation(goalEClass, source, new String[] { "constraints", "Type" });
		addAnnotation(guardEClass, source, new String[] { "constraints", "Type" });
		addAnnotation(collectionEClass, source, new String[] { "constraints", "Type" });
		addAnnotation(requirementEClass, source, new String[] { "constraints", "ContractsShouldBeStatic" });
	}

} //AbsystemPackageImpl
