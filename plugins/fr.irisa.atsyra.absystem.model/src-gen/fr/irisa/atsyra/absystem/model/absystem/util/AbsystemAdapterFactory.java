/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.util;

import fr.irisa.atsyra.absystem.model.absystem.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
 * @generated
 */
public class AbsystemAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AbsystemPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbsystemAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = AbsystemPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbsystemSwitch<Adapter> modelSwitch = new AbsystemSwitch<Adapter>() {
		@Override
		public Adapter caseAssetType(AssetType object) {
			return createAssetTypeAdapter();
		}

		@Override
		public Adapter caseAssetTypeReference(AssetTypeReference object) {
			return createAssetTypeReferenceAdapter();
		}

		@Override
		public Adapter caseAssetBasedSystem(AssetBasedSystem object) {
			return createAssetBasedSystemAdapter();
		}

		@Override
		public Adapter caseAsset(Asset object) {
			return createAssetAdapter();
		}

		@Override
		public Adapter caseAssetLink(AssetLink object) {
			return createAssetLinkAdapter();
		}

		@Override
		public Adapter caseAssetTypeAttribute(AssetTypeAttribute object) {
			return createAssetTypeAttributeAdapter();
		}

		@Override
		public Adapter caseAssetTypeFeature(AssetTypeFeature object) {
			return createAssetTypeFeatureAdapter();
		}

		@Override
		public Adapter casePrimitiveDataType(PrimitiveDataType object) {
			return createPrimitiveDataTypeAdapter();
		}

		@Override
		public Adapter caseEnumDataType(EnumDataType object) {
			return createEnumDataTypeAdapter();
		}

		@Override
		public Adapter caseEnumLiteral(EnumLiteral object) {
			return createEnumLiteralAdapter();
		}

		@Override
		public Adapter caseAssetAttributeValue(AssetAttributeValue object) {
			return createAssetAttributeValueAdapter();
		}

		@Override
		public Adapter caseDefinitionGroup(DefinitionGroup object) {
			return createDefinitionGroupAdapter();
		}

		@Override
		public Adapter caseAssetGroup(AssetGroup object) {
			return createAssetGroupAdapter();
		}

		@Override
		public Adapter caseImport(Import object) {
			return createImportAdapter();
		}

		@Override
		public Adapter caseTag(Tag object) {
			return createTagAdapter();
		}

		@Override
		public Adapter caseAbstractAssetType(AbstractAssetType object) {
			return createAbstractAssetTypeAdapter();
		}

		@Override
		public Adapter caseAssetTypeAspect(AssetTypeAspect object) {
			return createAssetTypeAspectAdapter();
		}

		@Override
		public Adapter caseGuardedAction(GuardedAction object) {
			return createGuardedActionAdapter();
		}

		@Override
		public Adapter caseGuardParameter(GuardParameter object) {
			return createGuardParameterAdapter();
		}

		@Override
		public Adapter caseExpression(Expression object) {
			return createExpressionAdapter();
		}

		@Override
		public Adapter caseBinaryExpression(BinaryExpression object) {
			return createBinaryExpressionAdapter();
		}

		@Override
		public Adapter caseImpliesExpression(ImpliesExpression object) {
			return createImpliesExpressionAdapter();
		}

		@Override
		public Adapter caseOrExpression(OrExpression object) {
			return createOrExpressionAdapter();
		}

		@Override
		public Adapter caseAndExpression(AndExpression object) {
			return createAndExpressionAdapter();
		}

		@Override
		public Adapter caseNotExpression(NotExpression object) {
			return createNotExpressionAdapter();
		}

		@Override
		public Adapter caseEqualityComparisonExpression(EqualityComparisonExpression object) {
			return createEqualityComparisonExpressionAdapter();
		}

		@Override
		public Adapter caseInequalityComparisonExpression(InequalityComparisonExpression object) {
			return createInequalityComparisonExpressionAdapter();
		}

		@Override
		public Adapter caseAction(Action object) {
			return createActionAdapter();
		}

		@Override
		public Adapter caseConstantExpression(ConstantExpression object) {
			return createConstantExpressionAdapter();
		}

		@Override
		public Adapter caseStringConstant(StringConstant object) {
			return createStringConstantAdapter();
		}

		@Override
		public Adapter caseIntConstant(IntConstant object) {
			return createIntConstantAdapter();
		}

		@Override
		public Adapter caseBooleanConstant(BooleanConstant object) {
			return createBooleanConstantAdapter();
		}

		@Override
		public Adapter caseVersionConstant(VersionConstant object) {
			return createVersionConstantAdapter();
		}

		@Override
		public Adapter caseMemberSelection(MemberSelection object) {
			return createMemberSelectionAdapter();
		}

		@Override
		public Adapter caseMember(Member object) {
			return createMemberAdapter();
		}

		@Override
		public Adapter caseSymbolRef(SymbolRef object) {
			return createSymbolRefAdapter();
		}

		@Override
		public Adapter caseSymbol(Symbol object) {
			return createSymbolAdapter();
		}

		@Override
		public Adapter caseStaticMethod(StaticMethod object) {
			return createStaticMethodAdapter();
		}

		@Override
		public Adapter caseLambdaParameter(LambdaParameter object) {
			return createLambdaParameterAdapter();
		}

		@Override
		public Adapter caseLambdaExpression(LambdaExpression object) {
			return createLambdaExpressionAdapter();
		}

		@Override
		public Adapter caseLambdaAction(LambdaAction object) {
			return createLambdaActionAdapter();
		}

		@Override
		public Adapter caseParameter(Parameter object) {
			return createParameterAdapter();
		}

		@Override
		public Adapter caseAnnotationEntry(AnnotationEntry object) {
			return createAnnotationEntryAdapter();
		}

		@Override
		public Adapter caseAnnotationKey(AnnotationKey object) {
			return createAnnotationKeyAdapter();
		}

		@Override
		public Adapter caseGoal(Goal object) {
			return createGoalAdapter();
		}

		@Override
		public Adapter caseContract(Contract object) {
			return createContractAdapter();
		}

		@Override
		public Adapter caseGuard(Guard object) {
			return createGuardAdapter();
		}

		@Override
		public Adapter caseEnumConstant(EnumConstant object) {
			return createEnumConstantAdapter();
		}

		@Override
		public Adapter caseUndefinedConstant(UndefinedConstant object) {
			return createUndefinedConstantAdapter();
		}

		@Override
		public Adapter caseCollection(Collection object) {
			return createCollectionAdapter();
		}

		@Override
		public Adapter caseDefinition(Definition object) {
			return createDefinitionAdapter();
		}

		@Override
		public Adapter caseAssetGroupContent(AssetGroupContent object) {
			return createAssetGroupContentAdapter();
		}

		@Override
		public Adapter caseLocaleGroup(LocaleGroup object) {
			return createLocaleGroupAdapter();
		}

		@Override
		public Adapter caseABSObjectLocale(ABSObjectLocale object) {
			return createABSObjectLocaleAdapter();
		}

		@Override
		public Adapter caseDefinitionGroupLocale(DefinitionGroupLocale object) {
			return createDefinitionGroupLocaleAdapter();
		}

		@Override
		public Adapter casePrimitiveDataTypeLocale(PrimitiveDataTypeLocale object) {
			return createPrimitiveDataTypeLocaleAdapter();
		}

		@Override
		public Adapter caseEnumLiteralLocale(EnumLiteralLocale object) {
			return createEnumLiteralLocaleAdapter();
		}

		@Override
		public Adapter caseAbstractAssetTypeLocale(AbstractAssetTypeLocale object) {
			return createAbstractAssetTypeLocaleAdapter();
		}

		@Override
		public Adapter caseAssetTypeLocale(AssetTypeLocale object) {
			return createAssetTypeLocaleAdapter();
		}

		@Override
		public Adapter caseAssetTypeAspectLocale(AssetTypeAspectLocale object) {
			return createAssetTypeAspectLocaleAdapter();
		}

		@Override
		public Adapter caseAssetTypeFeatureLocale(AssetTypeFeatureLocale object) {
			return createAssetTypeFeatureLocaleAdapter();
		}

		@Override
		public Adapter caseGuardLocale(GuardLocale object) {
			return createGuardLocaleAdapter();
		}

		@Override
		public Adapter caseAnnotated(Annotated object) {
			return createAnnotatedAdapter();
		}

		@Override
		public Adapter caseRequirement(Requirement object) {
			return createRequirementAdapter();
		}

		@Override
		public Adapter caseRequirementLocale(RequirementLocale object) {
			return createRequirementLocaleAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetType <em>Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetType
	 * @generated
	 */
	public Adapter createAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference <em>Asset Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference
	 * @generated
	 */
	public Adapter createAssetTypeReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem <em>Asset Based System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
	 * @generated
	 */
	public Adapter createAssetBasedSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Asset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Asset
	 * @generated
	 */
	public Adapter createAssetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetLink <em>Asset Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetLink
	 * @generated
	 */
	public Adapter createAssetLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute <em>Asset Type Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute
	 * @generated
	 */
	public Adapter createAssetTypeAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature <em>Asset Type Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature
	 * @generated
	 */
	public Adapter createAssetTypeFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType <em>Primitive Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType
	 * @generated
	 */
	public Adapter createPrimitiveDataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.EnumDataType <em>Enum Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumDataType
	 * @generated
	 */
	public Adapter createEnumDataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.EnumLiteral <em>Enum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumLiteral
	 * @generated
	 */
	public Adapter createEnumLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue <em>Asset Attribute Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue
	 * @generated
	 */
	public Adapter createAssetAttributeValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup <em>Definition Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup
	 * @generated
	 */
	public Adapter createDefinitionGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroup <em>Asset Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroup
	 * @generated
	 */
	public Adapter createAssetGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Import
	 * @generated
	 */
	public Adapter createImportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Tag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Tag
	 * @generated
	 */
	public Adapter createTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType <em>Abstract Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType
	 * @generated
	 */
	public Adapter createAbstractAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect <em>Asset Type Aspect</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect
	 * @generated
	 */
	public Adapter createAssetTypeAspectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.GuardedAction <em>Guarded Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardedAction
	 * @generated
	 */
	public Adapter createGuardedActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.GuardParameter <em>Guard Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardParameter
	 * @generated
	 */
	public Adapter createGuardParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Expression
	 * @generated
	 */
	public Adapter createExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.BinaryExpression
	 * @generated
	 */
	public Adapter createBinaryExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression <em>Implies Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression
	 * @generated
	 */
	public Adapter createImpliesExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.OrExpression <em>Or Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.OrExpression
	 * @generated
	 */
	public Adapter createOrExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AndExpression <em>And Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AndExpression
	 * @generated
	 */
	public Adapter createAndExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.NotExpression <em>Not Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.NotExpression
	 * @generated
	 */
	public Adapter createNotExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression <em>Equality Comparison Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression
	 * @generated
	 */
	public Adapter createEqualityComparisonExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression <em>Inequality Comparison Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression
	 * @generated
	 */
	public Adapter createInequalityComparisonExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Action
	 * @generated
	 */
	public Adapter createActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.ConstantExpression <em>Constant Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.ConstantExpression
	 * @generated
	 */
	public Adapter createConstantExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.StringConstant <em>String Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.StringConstant
	 * @generated
	 */
	public Adapter createStringConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.IntConstant <em>Int Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.IntConstant
	 * @generated
	 */
	public Adapter createIntConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.BooleanConstant <em>Boolean Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.BooleanConstant
	 * @generated
	 */
	public Adapter createBooleanConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.VersionConstant <em>Version Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.VersionConstant
	 * @generated
	 */
	public Adapter createVersionConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.MemberSelection <em>Member Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.MemberSelection
	 * @generated
	 */
	public Adapter createMemberSelectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Member <em>Member</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Member
	 * @generated
	 */
	public Adapter createMemberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.SymbolRef <em>Symbol Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.SymbolRef
	 * @generated
	 */
	public Adapter createSymbolRefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Symbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Symbol
	 * @generated
	 */
	public Adapter createSymbolAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.StaticMethod <em>Static Method</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.StaticMethod
	 * @generated
	 */
	public Adapter createStaticMethodAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaParameter <em>Lambda Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaParameter
	 * @generated
	 */
	public Adapter createLambdaParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaExpression <em>Lambda Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaExpression
	 * @generated
	 */
	public Adapter createLambdaExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaAction <em>Lambda Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LambdaAction
	 * @generated
	 */
	public Adapter createLambdaActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Parameter
	 * @generated
	 */
	public Adapter createParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry <em>Annotation Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AnnotationEntry
	 * @generated
	 */
	public Adapter createAnnotationEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AnnotationKey <em>Annotation Key</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AnnotationKey
	 * @generated
	 */
	public Adapter createAnnotationKeyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Goal <em>Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Goal
	 * @generated
	 */
	public Adapter createGoalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Contract <em>Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Contract
	 * @generated
	 */
	public Adapter createContractAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Guard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Guard
	 * @generated
	 */
	public Adapter createGuardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.EnumConstant <em>Enum Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumConstant
	 * @generated
	 */
	public Adapter createEnumConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant <em>Undefined Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant
	 * @generated
	 */
	public Adapter createUndefinedConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Collection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Collection
	 * @generated
	 */
	public Adapter createCollectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Definition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Definition
	 * @generated
	 */
	public Adapter createDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent <em>Asset Group Content</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent
	 * @generated
	 */
	public Adapter createAssetGroupContentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.LocaleGroup <em>Locale Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.LocaleGroup
	 * @generated
	 */
	public Adapter createLocaleGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale <em>ABS Object Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.ABSObjectLocale
	 * @generated
	 */
	public Adapter createABSObjectLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale <em>Definition Group Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale
	 * @generated
	 */
	public Adapter createDefinitionGroupLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale <em>Primitive Data Type Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale
	 * @generated
	 */
	public Adapter createPrimitiveDataTypeLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale <em>Enum Literal Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale
	 * @generated
	 */
	public Adapter createEnumLiteralLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AbstractAssetTypeLocale <em>Abstract Asset Type Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbstractAssetTypeLocale
	 * @generated
	 */
	public Adapter createAbstractAssetTypeLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale <em>Asset Type Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale
	 * @generated
	 */
	public Adapter createAssetTypeLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale <em>Asset Type Aspect Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale
	 * @generated
	 */
	public Adapter createAssetTypeAspectLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale <em>Asset Type Feature Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale
	 * @generated
	 */
	public Adapter createAssetTypeFeatureLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.GuardLocale <em>Guard Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.GuardLocale
	 * @generated
	 */
	public Adapter createGuardLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Annotated <em>Annotated</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Annotated
	 * @generated
	 */
	public Adapter createAnnotatedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.Requirement <em>Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.Requirement
	 * @generated
	 */
	public Adapter createRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.atsyra.absystem.model.absystem.RequirementLocale <em>Requirement Locale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.atsyra.absystem.model.absystem.RequirementLocale
	 * @generated
	 */
	public Adapter createRequirementLocaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //AbsystemAdapterFactory
