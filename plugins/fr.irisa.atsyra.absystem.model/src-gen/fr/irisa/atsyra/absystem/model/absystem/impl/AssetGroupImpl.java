/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroupContent;
import fr.irisa.atsyra.absystem.model.absystem.AssetLink;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.util.UnmodifiableEListCollector;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl#getId <em>Id</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl#getAssets <em>Assets</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl#getAssetLinks <em>Asset Links</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl#getGoals <em>Goals</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetGroupImpl#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetGroupImpl extends AnnotatedImpl implements AssetGroup {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetGroupContent> elements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ASSET_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getId() {
		StringBuilder result = new StringBuilder();
		result.append(eClass().getName()).append('-');
		if (name != null) {
			return result.append(name).toString();
		}
		if (eContainer() != null) {
			@SuppressWarnings("unchecked")
			List<AssetGroupContent> containingList = (List<AssetGroupContent>) eContainer().eGet(eContainingFeature());
			result.append(containingList.indexOf(this));
		}
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Asset> getAssets() {
		return getElements().stream().flatMap(element -> {
			if (element instanceof Asset) {
				return Stream.of((Asset) element);
			} else if (element instanceof AssetGroup) {
				return ((AssetGroup) element).getAssets().stream();
			} else {
				return Stream.empty();
			}
		}).collect(new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getAssetGroup_Assets()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<AssetLink> getAssetLinks() {
		return getElements().stream().flatMap(element -> {
			if (element instanceof AssetLink) {
				return Stream.of((AssetLink) element);
			} else if (element instanceof AssetGroup) {
				return ((AssetGroup) element).getAssetLinks().stream();
			} else {
				return Stream.empty();
			}
		}).collect(new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getAssetGroup_AssetLinks()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Goal> getGoals() {
		return getElements().stream().flatMap(element -> {
			if (element instanceof Goal) {
				return Stream.of((Goal) element);
			} else if (element instanceof AssetGroup) {
				return ((AssetGroup) element).getGoals().stream();
			} else {
				return Stream.empty();
			}
		}).collect(new UnmodifiableEListCollector<>(this, AbsystemPackage.eINSTANCE.getAssetGroup_Goals()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetGroupContent> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList<AssetGroupContent>(AssetGroupContent.class, this,
					AbsystemPackage.ASSET_GROUP__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_GROUP__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.ASSET_GROUP__ELEMENTS:
			return ((InternalEList<?>) getElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ASSET_GROUP__ID:
			return getId();
		case AbsystemPackage.ASSET_GROUP__NAME:
			return getName();
		case AbsystemPackage.ASSET_GROUP__ASSETS:
			return getAssets();
		case AbsystemPackage.ASSET_GROUP__ASSET_LINKS:
			return getAssetLinks();
		case AbsystemPackage.ASSET_GROUP__GOALS:
			return getGoals();
		case AbsystemPackage.ASSET_GROUP__ELEMENTS:
			return getElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ASSET_GROUP__NAME:
			setName((String) newValue);
			return;
		case AbsystemPackage.ASSET_GROUP__ELEMENTS:
			getElements().clear();
			getElements().addAll((Collection<? extends AssetGroupContent>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_GROUP__NAME:
			setName(NAME_EDEFAULT);
			return;
		case AbsystemPackage.ASSET_GROUP__ELEMENTS:
			getElements().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_GROUP__ID:
			return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
		case AbsystemPackage.ASSET_GROUP__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case AbsystemPackage.ASSET_GROUP__ASSETS:
			return !getAssets().isEmpty();
		case AbsystemPackage.ASSET_GROUP__ASSET_LINKS:
			return !getAssetLinks().isEmpty();
		case AbsystemPackage.ASSET_GROUP__GOALS:
			return !getGoals().isEmpty();
		case AbsystemPackage.ASSET_GROUP__ELEMENTS:
			return elements != null && !elements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AssetGroupContent.class) {
			switch (derivedFeatureID) {
			case AbsystemPackage.ASSET_GROUP__ID:
				return AbsystemPackage.ASSET_GROUP_CONTENT__ID;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AssetGroupContent.class) {
			switch (baseFeatureID) {
			case AbsystemPackage.ASSET_GROUP_CONTENT__ID:
				return AbsystemPackage.ASSET_GROUP__ID;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //AssetGroupImpl
