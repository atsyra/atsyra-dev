/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Type Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeReferenceImpl#getPropertyType <em>Property Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeReferenceImpl#isIsContainer <em>Is Container</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeReferenceImpl#getOppositeTypeReference <em>Opposite Type Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetTypeReferenceImpl extends AssetTypeFeatureImpl implements AssetTypeReference {
	/**
	 * The cached value of the '{@link #getPropertyType() <em>Property Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyType()
	 * @generated
	 * @ordered
	 */
	protected AssetType propertyType;

	/**
	 * The default value of the '{@link #isIsContainer() <em>Is Container</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsContainer()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CONTAINER_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isIsContainer() <em>Is Container</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsContainer()
	 * @generated
	 * @ordered
	 */
	protected boolean isContainer = IS_CONTAINER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOppositeTypeReference() <em>Opposite Type Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOppositeTypeReference()
	 * @generated
	 * @ordered
	 */
	protected AssetTypeReference oppositeTypeReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetTypeReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ASSET_TYPE_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetType getPropertyType() {
		if (propertyType != null && propertyType.eIsProxy()) {
			InternalEObject oldPropertyType = (InternalEObject) propertyType;
			propertyType = (AssetType) eResolveProxy(oldPropertyType);
			if (propertyType != oldPropertyType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AbsystemPackage.ASSET_TYPE_REFERENCE__PROPERTY_TYPE, oldPropertyType, propertyType));
			}
		}
		return propertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetType basicGetPropertyType() {
		return propertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPropertyType(AssetType newPropertyType) {
		AssetType oldPropertyType = propertyType;
		propertyType = newPropertyType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_TYPE_REFERENCE__PROPERTY_TYPE,
					oldPropertyType, propertyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isIsContainer() {
		return isContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIsContainer(boolean newIsContainer) {
		boolean oldIsContainer = isContainer;
		isContainer = newIsContainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.ASSET_TYPE_REFERENCE__IS_CONTAINER,
					oldIsContainer, isContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetTypeReference getOppositeTypeReference() {
		if (oppositeTypeReference != null && oppositeTypeReference.eIsProxy()) {
			InternalEObject oldOppositeTypeReference = (InternalEObject) oppositeTypeReference;
			oppositeTypeReference = (AssetTypeReference) eResolveProxy(oldOppositeTypeReference);
			if (oppositeTypeReference != oldOppositeTypeReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AbsystemPackage.ASSET_TYPE_REFERENCE__OPPOSITE_TYPE_REFERENCE, oldOppositeTypeReference,
							oppositeTypeReference));
			}
		}
		return oppositeTypeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetTypeReference basicGetOppositeTypeReference() {
		return oppositeTypeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOppositeTypeReference(AssetTypeReference newOppositeTypeReference) {
		AssetTypeReference oldOppositeTypeReference = oppositeTypeReference;
		oppositeTypeReference = newOppositeTypeReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AbsystemPackage.ASSET_TYPE_REFERENCE__OPPOSITE_TYPE_REFERENCE, oldOppositeTypeReference,
					oppositeTypeReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE_REFERENCE__PROPERTY_TYPE:
			if (resolve)
				return getPropertyType();
			return basicGetPropertyType();
		case AbsystemPackage.ASSET_TYPE_REFERENCE__IS_CONTAINER:
			return isIsContainer();
		case AbsystemPackage.ASSET_TYPE_REFERENCE__OPPOSITE_TYPE_REFERENCE:
			if (resolve)
				return getOppositeTypeReference();
			return basicGetOppositeTypeReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE_REFERENCE__PROPERTY_TYPE:
			setPropertyType((AssetType) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE_REFERENCE__IS_CONTAINER:
			setIsContainer((Boolean) newValue);
			return;
		case AbsystemPackage.ASSET_TYPE_REFERENCE__OPPOSITE_TYPE_REFERENCE:
			setOppositeTypeReference((AssetTypeReference) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE_REFERENCE__PROPERTY_TYPE:
			setPropertyType((AssetType) null);
			return;
		case AbsystemPackage.ASSET_TYPE_REFERENCE__IS_CONTAINER:
			setIsContainer(IS_CONTAINER_EDEFAULT);
			return;
		case AbsystemPackage.ASSET_TYPE_REFERENCE__OPPOSITE_TYPE_REFERENCE:
			setOppositeTypeReference((AssetTypeReference) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ASSET_TYPE_REFERENCE__PROPERTY_TYPE:
			return propertyType != null;
		case AbsystemPackage.ASSET_TYPE_REFERENCE__IS_CONTAINER:
			return isContainer != IS_CONTAINER_EDEFAULT;
		case AbsystemPackage.ASSET_TYPE_REFERENCE__OPPOSITE_TYPE_REFERENCE:
			return oppositeTypeReference != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (isContainer: ");
		result.append(isContainer);
		result.append(')');
		return result.toString();
	}

} //AssetTypeReferenceImpl
