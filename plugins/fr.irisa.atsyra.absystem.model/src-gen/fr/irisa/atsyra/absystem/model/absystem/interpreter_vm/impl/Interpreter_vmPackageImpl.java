/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;

import fr.irisa.atsyra.absystem.model.absystem.impl.AbsystemPackageImpl;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetFeatureValueEntry;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetValue;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.BooleanValue;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ConstantArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.EnumValue;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurenceArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.IntegerValue;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmFactory;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ListValue;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.StringValue;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.UndefinedValue;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Value;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.VersionValue;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Interpreter_vmPackageImpl extends EPackageImpl implements Interpreter_vmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass guardOccurenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass guardOccurenceArgumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetArgumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constantArgumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass undefinedValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass versionValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass listValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetFeatureValueEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumValueEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Interpreter_vmPackageImpl() {
		super(eNS_URI, Interpreter_vmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link Interpreter_vmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Interpreter_vmPackage init() {
		if (isInited)
			return (Interpreter_vmPackage) EPackage.Registry.INSTANCE.getEPackage(Interpreter_vmPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredInterpreter_vmPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		Interpreter_vmPackageImpl theInterpreter_vmPackage = registeredInterpreter_vmPackage instanceof Interpreter_vmPackageImpl
				? (Interpreter_vmPackageImpl) registeredInterpreter_vmPackage
				: new Interpreter_vmPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(AbsystemPackage.eNS_URI);
		AbsystemPackageImpl theAbsystemPackage = (AbsystemPackageImpl) (registeredPackage instanceof AbsystemPackageImpl
				? registeredPackage
				: AbsystemPackage.eINSTANCE);

		// Create package meta-data objects
		theInterpreter_vmPackage.createPackageContents();
		theAbsystemPackage.createPackageContents();

		// Initialize created meta-data
		theInterpreter_vmPackage.initializePackageContents();
		theAbsystemPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theInterpreter_vmPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Interpreter_vmPackage.eNS_URI, theInterpreter_vmPackage);
		return theInterpreter_vmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGuardOccurence() {
		return guardOccurenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGuardOccurence_Guard() {
		return (EReference) guardOccurenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGuardOccurence_GuardOccurenceArguments() {
		return (EReference) guardOccurenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGuardOccurence_Name() {
		return (EAttribute) guardOccurenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGuardOccurenceArgument() {
		return guardOccurenceArgumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getGuardOccurenceArgument__GetName() {
		return guardOccurenceArgumentEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetArgument() {
		return assetArgumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetArgument_Asset() {
		return (EReference) assetArgumentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAssetArgument__GetName() {
		return assetArgumentEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConstantArgument() {
		return constantArgumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConstantArgument_Value() {
		return (EAttribute) constantArgumentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getConstantArgument__GetName() {
		return constantArgumentEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getValue() {
		return valueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getValue_Name() {
		return (EAttribute) valueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUndefinedValue() {
		return undefinedValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBooleanValue() {
		return booleanValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBooleanValue_BooleanValue() {
		return (EAttribute) booleanValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIntegerValue() {
		return integerValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIntegerValue_IntValue() {
		return (EAttribute) integerValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVersionValue() {
		return versionValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVersionValue_VersionValue() {
		return (EAttribute) versionValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringValue() {
		return stringValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringValue_StringValue() {
		return (EAttribute) stringValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetValue() {
		return assetValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetValue_AssetValue() {
		return (EReference) assetValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getListValue() {
		return listValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getListValue_OwnedValues() {
		return (EReference) listValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetFeatureValueEntry() {
		return assetFeatureValueEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetFeatureValueEntry_Key() {
		return (EReference) assetFeatureValueEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetFeatureValueEntry_Value() {
		return (EReference) assetFeatureValueEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetFeatureValueEntry_Name() {
		return (EAttribute) assetFeatureValueEntryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEnumValue() {
		return enumValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEnumValue_EnumliteralValue() {
		return (EReference) enumValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Interpreter_vmFactory getInterpreter_vmFactory() {
		return (Interpreter_vmFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		guardOccurenceEClass = createEClass(GUARD_OCCURENCE);
		createEReference(guardOccurenceEClass, GUARD_OCCURENCE__GUARD);
		createEReference(guardOccurenceEClass, GUARD_OCCURENCE__GUARD_OCCURENCE_ARGUMENTS);
		createEAttribute(guardOccurenceEClass, GUARD_OCCURENCE__NAME);

		guardOccurenceArgumentEClass = createEClass(GUARD_OCCURENCE_ARGUMENT);
		createEOperation(guardOccurenceArgumentEClass, GUARD_OCCURENCE_ARGUMENT___GET_NAME);

		assetArgumentEClass = createEClass(ASSET_ARGUMENT);
		createEReference(assetArgumentEClass, ASSET_ARGUMENT__ASSET);
		createEOperation(assetArgumentEClass, ASSET_ARGUMENT___GET_NAME);

		constantArgumentEClass = createEClass(CONSTANT_ARGUMENT);
		createEAttribute(constantArgumentEClass, CONSTANT_ARGUMENT__VALUE);
		createEOperation(constantArgumentEClass, CONSTANT_ARGUMENT___GET_NAME);

		valueEClass = createEClass(VALUE);
		createEAttribute(valueEClass, VALUE__NAME);

		undefinedValueEClass = createEClass(UNDEFINED_VALUE);

		booleanValueEClass = createEClass(BOOLEAN_VALUE);
		createEAttribute(booleanValueEClass, BOOLEAN_VALUE__BOOLEAN_VALUE);

		integerValueEClass = createEClass(INTEGER_VALUE);
		createEAttribute(integerValueEClass, INTEGER_VALUE__INT_VALUE);

		versionValueEClass = createEClass(VERSION_VALUE);
		createEAttribute(versionValueEClass, VERSION_VALUE__VERSION_VALUE);

		stringValueEClass = createEClass(STRING_VALUE);
		createEAttribute(stringValueEClass, STRING_VALUE__STRING_VALUE);

		assetValueEClass = createEClass(ASSET_VALUE);
		createEReference(assetValueEClass, ASSET_VALUE__ASSET_VALUE);

		listValueEClass = createEClass(LIST_VALUE);
		createEReference(listValueEClass, LIST_VALUE__OWNED_VALUES);

		assetFeatureValueEntryEClass = createEClass(ASSET_FEATURE_VALUE_ENTRY);
		createEReference(assetFeatureValueEntryEClass, ASSET_FEATURE_VALUE_ENTRY__KEY);
		createEReference(assetFeatureValueEntryEClass, ASSET_FEATURE_VALUE_ENTRY__VALUE);
		createEAttribute(assetFeatureValueEntryEClass, ASSET_FEATURE_VALUE_ENTRY__NAME);

		enumValueEClass = createEClass(ENUM_VALUE);
		createEReference(enumValueEClass, ENUM_VALUE__ENUMLITERAL_VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AbsystemPackage theAbsystemPackage = (AbsystemPackage) EPackage.Registry.INSTANCE
				.getEPackage(AbsystemPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		assetArgumentEClass.getESuperTypes().add(this.getGuardOccurenceArgument());
		constantArgumentEClass.getESuperTypes().add(this.getGuardOccurenceArgument());
		undefinedValueEClass.getESuperTypes().add(this.getValue());
		booleanValueEClass.getESuperTypes().add(this.getValue());
		integerValueEClass.getESuperTypes().add(this.getValue());
		versionValueEClass.getESuperTypes().add(this.getValue());
		stringValueEClass.getESuperTypes().add(this.getValue());
		assetValueEClass.getESuperTypes().add(this.getValue());
		listValueEClass.getESuperTypes().add(this.getValue());
		enumValueEClass.getESuperTypes().add(this.getValue());

		// Initialize classes, features, and operations; add parameters
		initEClass(guardOccurenceEClass, GuardOccurence.class, "GuardOccurence", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGuardOccurence_Guard(), theAbsystemPackage.getGuard(), null, "guard", null, 0, 1,
				GuardOccurence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGuardOccurence_GuardOccurenceArguments(), this.getGuardOccurenceArgument(), null,
				"guardOccurenceArguments", null, 0, -1, GuardOccurence.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGuardOccurence_Name(), ecorePackage.getEString(), "name", null, 0, 1, GuardOccurence.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(guardOccurenceArgumentEClass, GuardOccurenceArgument.class, "GuardOccurenceArgument", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getGuardOccurenceArgument__GetName(), ecorePackage.getEString(), "getName", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(assetArgumentEClass, AssetArgument.class, "AssetArgument", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetArgument_Asset(), theAbsystemPackage.getAsset(), null, "asset", null, 0, 1,
				AssetArgument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getAssetArgument__GetName(), ecorePackage.getEString(), "getName", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(constantArgumentEClass, ConstantArgument.class, "ConstantArgument", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConstantArgument_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				ConstantArgument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEOperation(getConstantArgument__GetName(), ecorePackage.getEString(), "getName", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(valueEClass, Value.class, "Value", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValue_Name(), ecorePackage.getEString(), "name", null, 0, 1, Value.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(undefinedValueEClass, UndefinedValue.class, "UndefinedValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(booleanValueEClass, BooleanValue.class, "BooleanValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanValue_BooleanValue(), ecorePackage.getEBoolean(), "booleanValue", null, 0, 1,
				BooleanValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(integerValueEClass, IntegerValue.class, "IntegerValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerValue_IntValue(), ecorePackage.getEInt(), "intValue", null, 0, 1, IntegerValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(versionValueEClass, VersionValue.class, "VersionValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVersionValue_VersionValue(), theAbsystemPackage.getEVersion(), "versionValue", null, 0, 1,
				VersionValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(stringValueEClass, StringValue.class, "StringValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringValue_StringValue(), ecorePackage.getEString(), "stringValue", null, 0, 1,
				StringValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(assetValueEClass, AssetValue.class, "AssetValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetValue_AssetValue(), theAbsystemPackage.getAsset(), null, "assetValue", null, 0, 1,
				AssetValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(listValueEClass, ListValue.class, "ListValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getListValue_OwnedValues(), this.getValue(), null, "ownedValues", null, 0, -1, ListValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetFeatureValueEntryEClass, AssetFeatureValueEntry.class, "AssetFeatureValueEntry", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetFeatureValueEntry_Key(), theAbsystemPackage.getAssetTypeFeature(), null, "key", null, 1,
				1, AssetFeatureValueEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetFeatureValueEntry_Value(), this.getValue(), null, "value", null, 1, 1,
				AssetFeatureValueEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetFeatureValueEntry_Name(), ecorePackage.getEString(), "name", null, 0, 1,
				AssetFeatureValueEntry.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(enumValueEClass, EnumValue.class, "EnumValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumValue_EnumliteralValue(), theAbsystemPackage.getEnumLiteral(), null, "enumliteralValue",
				null, 0, 1, EnumValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create annotations
		// aspect
		createAspectAnnotations();
	}

	/**
	 * Initializes the annotations for <b>aspect</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createAspectAnnotations() {
		String source = "aspect";
		addAnnotation(guardOccurenceEClass, source, new String[] {});
		addAnnotation(valueEClass, source, new String[] {});
		addAnnotation(booleanValueEClass, source, new String[] {});
		addAnnotation(integerValueEClass, source, new String[] {});
		addAnnotation(versionValueEClass, source, new String[] {});
		addAnnotation(stringValueEClass, source, new String[] {});
		addAnnotation(assetValueEClass, source, new String[] {});
		addAnnotation(listValueEClass, source, new String[] {});
		addAnnotation(assetFeatureValueEntryEClass, source, new String[] {});
		addAnnotation(enumValueEClass, source, new String[] {});
	}

} //Interpreter_vmPackageImpl
