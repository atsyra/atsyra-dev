/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Interpreter_vmFactoryImpl extends EFactoryImpl implements Interpreter_vmFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Interpreter_vmFactory init() {
		try {
			Interpreter_vmFactory theInterpreter_vmFactory = (Interpreter_vmFactory) EPackage.Registry.INSTANCE
					.getEFactory(Interpreter_vmPackage.eNS_URI);
			if (theInterpreter_vmFactory != null) {
				return theInterpreter_vmFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Interpreter_vmFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interpreter_vmFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case Interpreter_vmPackage.GUARD_OCCURENCE:
			return createGuardOccurence();
		case Interpreter_vmPackage.ASSET_ARGUMENT:
			return createAssetArgument();
		case Interpreter_vmPackage.CONSTANT_ARGUMENT:
			return createConstantArgument();
		case Interpreter_vmPackage.UNDEFINED_VALUE:
			return createUndefinedValue();
		case Interpreter_vmPackage.BOOLEAN_VALUE:
			return createBooleanValue();
		case Interpreter_vmPackage.INTEGER_VALUE:
			return createIntegerValue();
		case Interpreter_vmPackage.VERSION_VALUE:
			return createVersionValue();
		case Interpreter_vmPackage.STRING_VALUE:
			return createStringValue();
		case Interpreter_vmPackage.ASSET_VALUE:
			return createAssetValue();
		case Interpreter_vmPackage.LIST_VALUE:
			return createListValue();
		case Interpreter_vmPackage.ASSET_FEATURE_VALUE_ENTRY:
			return createAssetFeatureValueEntry();
		case Interpreter_vmPackage.ENUM_VALUE:
			return createEnumValue();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GuardOccurence createGuardOccurence() {
		GuardOccurenceImpl guardOccurence = new GuardOccurenceImpl();
		return guardOccurence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetArgument createAssetArgument() {
		AssetArgumentImpl assetArgument = new AssetArgumentImpl();
		return assetArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConstantArgument createConstantArgument() {
		ConstantArgumentImpl constantArgument = new ConstantArgumentImpl();
		return constantArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UndefinedValue createUndefinedValue() {
		UndefinedValueImpl undefinedValue = new UndefinedValueImpl();
		return undefinedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BooleanValue createBooleanValue() {
		BooleanValueImpl booleanValue = new BooleanValueImpl();
		return booleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IntegerValue createIntegerValue() {
		IntegerValueImpl integerValue = new IntegerValueImpl();
		return integerValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VersionValue createVersionValue() {
		VersionValueImpl versionValue = new VersionValueImpl();
		return versionValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StringValue createStringValue() {
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetValue createAssetValue() {
		AssetValueImpl assetValue = new AssetValueImpl();
		return assetValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ListValue createListValue() {
		ListValueImpl listValue = new ListValueImpl();
		return listValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetFeatureValueEntry createAssetFeatureValueEntry() {
		AssetFeatureValueEntryImpl assetFeatureValueEntry = new AssetFeatureValueEntryImpl();
		return assetFeatureValueEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EnumValue createEnumValue() {
		EnumValueImpl enumValue = new EnumValueImpl();
		return enumValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Interpreter_vmPackage getInterpreter_vmPackage() {
		return (Interpreter_vmPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Interpreter_vmPackage getPackage() {
		return Interpreter_vmPackage.eINSTANCE;
	}

} //Interpreter_vmFactoryImpl
