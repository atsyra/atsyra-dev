/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Type Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute#getAttributeType <em>Attribute Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute#getDefaultValues <em>Default Values</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeAttribute()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='DefaultType DefaultMultiplicity'"
 * @generated
 */
public interface AssetTypeAttribute extends AssetTypeFeature {
	/**
	 * Returns the value of the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Type</em>' reference.
	 * @see #setAttributeType(PrimitiveDataType)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeAttribute_AttributeType()
	 * @model
	 * @generated
	 */
	PrimitiveDataType getAttributeType();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute#getAttributeType <em>Attribute Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Type</em>' reference.
	 * @see #getAttributeType()
	 * @generated
	 */
	void setAttributeType(PrimitiveDataType value);

	/**
	 * Returns the value of the '<em><b>Default Values</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.ConstantExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Values</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeAttribute_DefaultValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConstantExpression> getDefaultValues();

} // AssetTypeAttribute
