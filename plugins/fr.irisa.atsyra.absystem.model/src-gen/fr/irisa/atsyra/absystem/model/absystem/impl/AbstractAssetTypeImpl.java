/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.Definition;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Asset Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeImpl#getId <em>Id</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeImpl#getAssetTypeAttributes <em>Asset Type Attributes</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.AbstractAssetTypeImpl#getAssetTypeProperties <em>Asset Type Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractAssetTypeImpl extends MinimalEObjectImpl.Container implements AbstractAssetType {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAssetTypeAttributes() <em>Asset Type Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetTypeAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetTypeAttribute> assetTypeAttributes;

	/**
	 * The cached value of the '{@link #getAssetTypeProperties() <em>Asset Type Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetTypeProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<AssetTypeReference> assetTypeProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractAssetTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.ABSTRACT_ASSET_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getId() {
		StringBuilder result = new StringBuilder();
		result.append(eClass().getName()).append('-');
		if (this instanceof AssetType) {
			String name = ((AssetType) this).getName();
			if (name != null) {
				return result.append(name).toString();
			}
		} else if (this instanceof AssetTypeAspect) {
			AssetType baseType = ((AssetTypeAspect) this).getBaseAssetType();
			if (baseType != null && baseType.getName() != null) {
				return result.append(baseType.getName()).toString();
			}
		}
		if (eContainer() != null) {
			@SuppressWarnings("unchecked")
			List<Definition> containingList = (List<Definition>) eContainer().eGet(eContainingFeature());
			result.append(containingList.indexOf(this));
		}
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetTypeAttribute> getAssetTypeAttributes() {
		if (assetTypeAttributes == null) {
			assetTypeAttributes = new EObjectContainmentEList<AssetTypeAttribute>(AssetTypeAttribute.class, this,
					AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES);
		}
		return assetTypeAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AssetTypeReference> getAssetTypeProperties() {
		if (assetTypeProperties == null) {
			assetTypeProperties = new EObjectContainmentEList<AssetTypeReference>(AssetTypeReference.class, this,
					AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES);
		}
		return assetTypeProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			return ((InternalEList<?>) getAssetTypeAttributes()).basicRemove(otherEnd, msgs);
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			return ((InternalEList<?>) getAssetTypeProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ID:
			return getId();
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			return getAssetTypeAttributes();
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			return getAssetTypeProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			getAssetTypeAttributes().clear();
			getAssetTypeAttributes().addAll((Collection<? extends AssetTypeAttribute>) newValue);
			return;
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			getAssetTypeProperties().clear();
			getAssetTypeProperties().addAll((Collection<? extends AssetTypeReference>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			getAssetTypeAttributes().clear();
			return;
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			getAssetTypeProperties().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ID:
			return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_ATTRIBUTES:
			return assetTypeAttributes != null && !assetTypeAttributes.isEmpty();
		case AbsystemPackage.ABSTRACT_ASSET_TYPE__ASSET_TYPE_PROPERTIES:
			return assetTypeProperties != null && !assetTypeProperties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AbstractAssetTypeImpl
