/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lambda Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.LambdaExpression#getLambdaParameter <em>Lambda Parameter</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.LambdaExpression#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getLambdaExpression()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='Type'"
 * @generated
 */
public interface LambdaExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Lambda Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lambda Parameter</em>' containment reference.
	 * @see #setLambdaParameter(LambdaParameter)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getLambdaExpression_LambdaParameter()
	 * @model containment="true"
	 * @generated
	 */
	LambdaParameter getLambdaParameter();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaExpression#getLambdaParameter <em>Lambda Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lambda Parameter</em>' containment reference.
	 * @see #getLambdaParameter()
	 * @generated
	 */
	void setLambdaParameter(LambdaParameter value);

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(Expression)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getLambdaExpression_Body()
	 * @model containment="true"
	 * @generated
	 */
	Expression getBody();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.LambdaExpression#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(Expression value);

} // LambdaExpression
