/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl;

import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.EnumValue;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enum Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.impl.EnumValueImpl#getEnumliteralValue <em>Enumliteral Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumValueImpl extends ValueImpl implements EnumValue {
	/**
	 * The cached value of the '{@link #getEnumliteralValue() <em>Enumliteral Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumliteralValue()
	 * @generated
	 * @ordered
	 */
	protected EnumLiteral enumliteralValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Interpreter_vmPackage.Literals.ENUM_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EnumLiteral getEnumliteralValue() {
		if (enumliteralValue != null && enumliteralValue.eIsProxy()) {
			InternalEObject oldEnumliteralValue = (InternalEObject) enumliteralValue;
			enumliteralValue = (EnumLiteral) eResolveProxy(oldEnumliteralValue);
			if (enumliteralValue != oldEnumliteralValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Interpreter_vmPackage.ENUM_VALUE__ENUMLITERAL_VALUE, oldEnumliteralValue,
							enumliteralValue));
			}
		}
		return enumliteralValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumLiteral basicGetEnumliteralValue() {
		return enumliteralValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEnumliteralValue(EnumLiteral newEnumliteralValue) {
		EnumLiteral oldEnumliteralValue = enumliteralValue;
		enumliteralValue = newEnumliteralValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Interpreter_vmPackage.ENUM_VALUE__ENUMLITERAL_VALUE,
					oldEnumliteralValue, enumliteralValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Interpreter_vmPackage.ENUM_VALUE__ENUMLITERAL_VALUE:
			if (resolve)
				return getEnumliteralValue();
			return basicGetEnumliteralValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Interpreter_vmPackage.ENUM_VALUE__ENUMLITERAL_VALUE:
			setEnumliteralValue((EnumLiteral) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.ENUM_VALUE__ENUMLITERAL_VALUE:
			setEnumliteralValue((EnumLiteral) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.ENUM_VALUE__ENUMLITERAL_VALUE:
			return enumliteralValue != null;
		}
		return super.eIsSet(featureID);
	}

} //EnumValueImpl
