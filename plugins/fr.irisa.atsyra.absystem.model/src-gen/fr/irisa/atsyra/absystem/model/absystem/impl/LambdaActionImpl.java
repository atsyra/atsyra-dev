/*******************************************************************************
 * Copyright (c) 2014, 2021 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem.impl;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Action;
import fr.irisa.atsyra.absystem.model.absystem.LambdaAction;
import fr.irisa.atsyra.absystem.model.absystem.LambdaParameter;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lambda Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaActionImpl#getLambdaParameter <em>Lambda Parameter</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.impl.LambdaActionImpl#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LambdaActionImpl extends MinimalEObjectImpl.Container implements LambdaAction {
	/**
	 * The cached value of the '{@link #getLambdaParameter() <em>Lambda Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLambdaParameter()
	 * @generated
	 * @ordered
	 */
	protected LambdaParameter lambdaParameter;

	/**
	 * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActions()
	 * @generated
	 * @ordered
	 */
	protected EList<Action> actions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LambdaActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbsystemPackage.Literals.LAMBDA_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LambdaParameter getLambdaParameter() {
		return lambdaParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLambdaParameter(LambdaParameter newLambdaParameter, NotificationChain msgs) {
		LambdaParameter oldLambdaParameter = lambdaParameter;
		lambdaParameter = newLambdaParameter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					AbsystemPackage.LAMBDA_ACTION__LAMBDA_PARAMETER, oldLambdaParameter, newLambdaParameter);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLambdaParameter(LambdaParameter newLambdaParameter) {
		if (newLambdaParameter != lambdaParameter) {
			NotificationChain msgs = null;
			if (lambdaParameter != null)
				msgs = ((InternalEObject) lambdaParameter).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.LAMBDA_ACTION__LAMBDA_PARAMETER, null, msgs);
			if (newLambdaParameter != null)
				msgs = ((InternalEObject) newLambdaParameter).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - AbsystemPackage.LAMBDA_ACTION__LAMBDA_PARAMETER, null, msgs);
			msgs = basicSetLambdaParameter(newLambdaParameter, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbsystemPackage.LAMBDA_ACTION__LAMBDA_PARAMETER,
					newLambdaParameter, newLambdaParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Action> getActions() {
		if (actions == null) {
			actions = new EObjectContainmentEList<Action>(Action.class, this, AbsystemPackage.LAMBDA_ACTION__ACTIONS);
		}
		return actions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_ACTION__LAMBDA_PARAMETER:
			return basicSetLambdaParameter(null, msgs);
		case AbsystemPackage.LAMBDA_ACTION__ACTIONS:
			return ((InternalEList<?>) getActions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_ACTION__LAMBDA_PARAMETER:
			return getLambdaParameter();
		case AbsystemPackage.LAMBDA_ACTION__ACTIONS:
			return getActions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_ACTION__LAMBDA_PARAMETER:
			setLambdaParameter((LambdaParameter) newValue);
			return;
		case AbsystemPackage.LAMBDA_ACTION__ACTIONS:
			getActions().clear();
			getActions().addAll((Collection<? extends Action>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_ACTION__LAMBDA_PARAMETER:
			setLambdaParameter((LambdaParameter) null);
			return;
		case AbsystemPackage.LAMBDA_ACTION__ACTIONS:
			getActions().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbsystemPackage.LAMBDA_ACTION__LAMBDA_PARAMETER:
			return lambdaParameter != null;
		case AbsystemPackage.LAMBDA_ACTION__ACTIONS:
			return actions != null && !actions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LambdaActionImpl
