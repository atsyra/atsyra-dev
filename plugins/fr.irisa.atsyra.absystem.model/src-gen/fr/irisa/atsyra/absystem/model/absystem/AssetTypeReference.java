/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.model.absystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Type Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#getPropertyType <em>Property Type</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#isIsContainer <em>Is Container</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#getOppositeTypeReference <em>Opposite Type Reference</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeReference()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='DefaultMultiplicity'"
 * @generated
 */
public interface AssetTypeReference extends AssetTypeFeature {
	/**
	 * Returns the value of the '<em><b>Property Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Type</em>' reference.
	 * @see #setPropertyType(AssetType)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeReference_PropertyType()
	 * @model required="true"
	 * @generated
	 */
	AssetType getPropertyType();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#getPropertyType <em>Property Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Type</em>' reference.
	 * @see #getPropertyType()
	 * @generated
	 */
	void setPropertyType(AssetType value);

	/**
	 * Returns the value of the '<em><b>Is Container</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Container</em>' attribute.
	 * @see #setIsContainer(boolean)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeReference_IsContainer()
	 * @model
	 * @generated
	 */
	boolean isIsContainer();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#isIsContainer <em>Is Container</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Container</em>' attribute.
	 * @see #isIsContainer()
	 * @generated
	 */
	void setIsContainer(boolean value);

	/**
	 * Returns the value of the '<em><b>Opposite Type Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opposite Type Reference</em>' reference.
	 * @see #setOppositeTypeReference(AssetTypeReference)
	 * @see fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage#getAssetTypeReference_OppositeTypeReference()
	 * @model
	 * @generated
	 */
	AssetTypeReference getOppositeTypeReference();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference#getOppositeTypeReference <em>Opposite Type Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opposite Type Reference</em>' reference.
	 * @see #getOppositeTypeReference()
	 * @generated
	 */
	void setOppositeTypeReference(AssetTypeReference value);

} // AssetTypeReference
