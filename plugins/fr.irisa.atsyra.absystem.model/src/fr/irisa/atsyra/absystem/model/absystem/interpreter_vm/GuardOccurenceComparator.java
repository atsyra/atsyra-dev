/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm;

import java.io.Serializable;
import java.util.Comparator;

import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.Guard;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;

public class GuardOccurenceComparator implements Comparator<GuardOccurence>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -461685474101075568L;

	@Override
	public int compare(GuardOccurence o1, GuardOccurence o2) {
		if(!getGuardQualifiedName(o1.getGuard()).equals(getGuardQualifiedName(o2.getGuard()))) {
			return getGuardQualifiedName(o1.getGuard()).compareTo(getGuardQualifiedName(o2.getGuard()));
		} else if(o1.getGuardOccurenceArguments().size() != o2.getGuardOccurenceArguments().size()) {
			return o1.getGuardOccurenceArguments().size() < o2.getGuardOccurenceArguments().size() ? -1 : 1;
		} else {
			for(int i = 0; i < o1.getGuard().getGuardParameters().size(); i++) {
				final AssetArgument parameter1 = (AssetArgument) o1.getGuardOccurenceArguments().get(i);
				final AssetArgument parameter2 = (AssetArgument) o2.getGuardOccurenceArguments().get(i);
				if(!getAssetQualifiedName(parameter1.getAsset()).equals(getAssetQualifiedName(parameter2.getAsset()))) {
					return getAssetQualifiedName(parameter1.getAsset()).compareTo(getAssetQualifiedName(parameter2.getAsset()));
				}
			}
		}
		return 0;
	}
	
	/**
	 * This function is used to get a unique identifier allowing for comparison of guards.
	 * @param guard
	 * @return a qualified name for guard
	 */
	private String getGuardQualifiedName(Guard guard) {
		return ABSUtils.getQualifiedName(guard, "::").orElseThrow();
	}
	
	/**
	 * This function is used to get a unique identifier allowing for comparison of assets.
	 * @param asset
	 * @return a qualified name for asset
	 */
	private String getAssetQualifiedName(Asset asset) {
		return ABSUtils.getQualifiedName(asset, "::").orElseThrow();
	}


}
