/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.interpreter_vm;

public class ABSInterpreterUtils {
	private ABSInterpreterUtils() {
		
	}
	
	public static String toReadableString(GuardOccurence go) {
		StringBuilder builder = new StringBuilder();
		builder.append(go.getGuard().getName());
		builder.append("(");
		boolean first = true;
		for(GuardOccurenceArgument goa : go.getGuardOccurenceArguments()) {
			if(!first) {
				builder.append(", ");
			}
			builder.append(toReadableString(goa));
			first = false;
		}
		builder.append(")");
		return builder.toString();
	}
	
	public static String toReadableString(GuardOccurenceArgument goa) {
		return goa.getName();
	}

	public static String toReadableString(AssetArgument goa) {
		return goa.getAsset().getName();
	}
	
	public static String toReadableString(ConstantArgument goa) {
		return goa.getValue();
	}
}
