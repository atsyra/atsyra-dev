/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import com.google.common.collect.Iterators;

import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType;
import fr.irisa.atsyra.absystem.model.absystem.ActionEnum;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup;
import fr.irisa.atsyra.absystem.model.absystem.AssetLink;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspectLocale;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeatureLocale;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeLocale;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant;
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroupLocale;
import fr.irisa.atsyra.absystem.model.absystem.EnumConstant;
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteralLocale;
import fr.irisa.atsyra.absystem.model.absystem.Expression;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.Guard;
import fr.irisa.atsyra.absystem.model.absystem.GuardLocale;
import fr.irisa.atsyra.absystem.model.absystem.IntConstant;
import fr.irisa.atsyra.absystem.model.absystem.LocaleGroup;
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection;
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataTypeLocale;
import fr.irisa.atsyra.absystem.model.absystem.Requirement;
import fr.irisa.atsyra.absystem.model.absystem.RequirementLocale;
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod;
import fr.irisa.atsyra.absystem.model.absystem.StringConstant;
import fr.irisa.atsyra.absystem.model.absystem.Symbol;
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef;
import fr.irisa.atsyra.absystem.model.absystem.Tag;
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant;
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant;
import fr.irisa.atsyra.preferences.GlobalPreferenceService;

public class ABSUtils {
	private ABSUtils() {
	}

	public static String getMemberName(Expression expression) {
		if (expression == null) {
			return "null";
		} else if (expression instanceof SymbolRef) {
			SymbolRef symbolRef = (SymbolRef) expression;
			return symbolRef.getSymbol().getName();
		} else if (expression instanceof MemberSelection) {
			MemberSelection memberSelection = (MemberSelection) expression;
			if (memberSelection.getMember() != null) {
				return memberSelection.getMember().getName();
			} else {
				return "null";
			}
		} else {
			throw new IllegalArgumentException(expression.toString() + " is invalid here");
		}
	}

	public static Set<AssetTypeReference> getAllAssetTypeProperties(AssetType type) {
		HashSet<AssetTypeReference> res = new HashSet<>();
		res.addAll(type.getAssetTypeProperties());
		for (AssetType anExtend : type.getExtends()) {
			res.addAll(getAllAssetTypeProperties(anExtend));
		}
		return res;
	}

	public static Set<AssetType> getAllSupertypes(AssetType type) {
		Set<AssetType> supertypes = new HashSet<>();
		if (type == null) {
			return supertypes;
		}
		Set<AssetType> newsupertypes = new HashSet<>(); // the set of remaining AssetType to process
		newsupertypes.add(type);
		while (!newsupertypes.isEmpty()) {
			// take the first element of the set with an iterator
			AssetType next = newsupertypes.iterator().next();
			supertypes.add(next);
			newsupertypes.remove(next);
			for (AssetType sup : next.getExtends()) {
				if (!supertypes.contains(sup)) {
					newsupertypes.add(sup);
				}
			}
		}
		return supertypes;
	}

	/**
	 * 
	 * @param type
	 * @return a list of the supertype of type, in an order according to the depth
	 *         in the supertype hierarchy
	 */
	public static List<AssetType> getSupertypesHierarchy(AssetType type) {
		if (type == null)
			return List.of();
		List<AssetType> supertypes = new ArrayList<>();
		Set<AssetType> newsupertypes = new HashSet<>(); // the set of remaining AssetType to process
		newsupertypes.add(type);
		while (!newsupertypes.isEmpty()) {
			Set<AssetType> newnewsupertypes = new HashSet<>();
			for (AssetType next : newsupertypes) {
				if (!supertypes.contains(next)) {
					supertypes.add(next);
					for (AssetType sup : next.getExtends()) {
						if (!supertypes.contains(sup)) {
							newnewsupertypes.add(sup);
						}
					}
				}
			}
			newsupertypes.clear();
			newsupertypes.addAll(newnewsupertypes);
		}
		return supertypes;
	}

	public static List<AssetType> getSupertypesHierarchyWithAny(AssetType type) {
		List<AssetType> supertypes = getSupertypesHierarchy(type);
		Set<AssetType> anyType = getAnyType(getAbsSet(type));
		supertypes.addAll(anyType);
		return supertypes;
	}

	public static Set<AssetType> getAnyType(Set<AssetBasedSystem> absSet) {
		return absSet.stream().flatMap(abs -> abs.getDefinitionGroups().stream())
				.flatMap(dg -> dg.getAssetTypes().stream()).filter(AssetType.class::isInstance)
				.map(AssetType.class::cast).filter(type -> Objects.equals(type.getName(), "Any"))
				.collect(Collectors.toSet());
	}

	public static boolean shareCommonSuperType(AssetType first, AssetType second) {
		Set<AssetType> firstSuperTypes = getAllSupertypes(first);
		Set<AssetType> secondSuperTypes = getAllSupertypes(second);
		return firstSuperTypes.stream().anyMatch(secondSuperTypes::contains);
	}
	
	public static boolean shareCommonSuperType(List<AssetType> types) {
		return !commonSuperTypes(types).isEmpty();
	}
	
	public static Set<AssetType> commonSuperTypes(List<AssetType> types) {
		Set<AssetType> commonSuperTypes = new HashSet<>();
		if(!types.isEmpty()) {
			commonSuperTypes.addAll(getAllSupertypes(types.get(0)));
			for(AssetType type : types) {
				Set<AssetType> supertypes = getAllSupertypes(type);
				commonSuperTypes.removeIf(supertype -> !supertypes.contains(supertype));
			}
		}
		return commonSuperTypes;
	}
	
	public static boolean canShareConcreteAsset(AssetType first, AssetType second) {
		Set<AssetType> firstSuperTypes = getAllSupertypes(first);
		Set<AssetType> secondSuperTypes = getAllSupertypes(second);
		return firstSuperTypes.stream().filter(type -> type == first || !type.isAbstract()).anyMatch(secondSuperTypes::contains);
	}
	
	private static AbsystemSwitch<Set<? extends EObject>> superTypesSwitch = new AbsystemSwitch<>() {
		@Override
		public Set<? extends EObject> caseAssetType(AssetType object) {
			return getAllSupertypes(object);
		}
		@Override
		public Set<? extends EObject> casePrimitiveDataType(PrimitiveDataType object) {
			return Set.of(object);
		}
	};
	
	public static Set<EObject> commonSuperTypesWithPrimitiveTypes(List<EObject> types) {
		Set<EObject> commonSuperTypes = new HashSet<>();
		if(!types.isEmpty()) {
			commonSuperTypes.addAll(superTypesSwitch.doSwitch(types.get(0)));
			for(EObject type : types) {
				Set<? extends EObject> supertypes = superTypesSwitch.doSwitch(type);
				commonSuperTypes.removeIf(commonSupertype -> !supertypes.contains(commonSupertype));
			}
		}
		return commonSuperTypes;
	}

	public static Set<AssetType> getAllSubtypes(AssetType type) {
		Set<AssetType> allAssetTypes = getAllAssetTypes(getAbsSet(type));
		Set<AssetType> subtypes = new HashSet<>();
		Set<AssetType> newsubtypes = new HashSet<>(); // the set of remaining AssetType to process
		newsubtypes.add(type);
		while (!newsubtypes.isEmpty()) {
			// take the first element of the set with an iterator
			AssetType next = newsubtypes.iterator().next();
			subtypes.add(next);
			newsubtypes.remove(next);
			allAssetTypes.stream().filter(atype -> atype.getExtends().contains(next)).forEach(sub -> {
				if (!subtypes.contains(sub)) {
					newsubtypes.add(sub);
				}
			});
		}
		return subtypes;
	}

	public static Set<Asset> getAllSubtypedAssets(AssetType type) {
		Set<AssetType> allSubtypes = getAllSubtypes(type);
		Set<Asset> allAssets = getAllAssets(getAbsSet(type));
		return allAssets.stream().filter(asset -> allSubtypes.contains(asset.getAssetType()))
				.collect(Collectors.toSet());
	}

	public static Set<Asset> getAllAssets(Set<AssetBasedSystem> absSet) {
		return absSet.stream().flatMap(abs -> abs.getAssetGroups().stream()).flatMap(ag -> ag.getAssets().stream())
				.collect(Collectors.toSet());
	}
	
	/**
	 * Find all links in the resourceSet that are related to the given Asset
	 */
	public static Set<AssetLink> getAllRelatedAssetLinks(Asset asset) {
		return getAllAssetLinks(asset.eResource().getResourceSet()).stream()
				.filter(al -> 
					asset.equals(al.getSourceAsset()) ||
					asset.equals(al.getTargetAsset()))
				.collect(Collectors.toSet());
	}
	
	/**
	 * Find all links in the resourceSet
	 */
	public static Set<AssetLink> getAllAssetLinks(ResourceSet resourceSet) {
		return getAllAssetGroups(resourceSet).stream().flatMap( ag -> ag.getAssetLinks().stream()).collect(Collectors.toSet());
	}
	
	public static Set<AssetGroup> getAllAssetGroups(ResourceSet resourceSet) {
		Set<AssetGroup> result = new HashSet<>();
		result.addAll(resourceSet.getResources().stream()
				.flatMap(resource -> resource.getContents().stream())
				.filter(AssetBasedSystem.class::isInstance).map(AssetBasedSystem.class::cast)
				.flatMap(abs -> abs.getAssetGroups().stream().flatMap(ag -> getAllAssetGroups(ag).stream())
						).collect(Collectors.toSet())
			);
		return result;
	}
	
	
	public static boolean areEquivalent(AssetLink al1, AssetLink al2) {
		return 
			(	// equals
				al1.getSourceAsset().equals(al2.getSourceAsset()) &&
				al1.getReferenceType().equals(al2.getReferenceType()) &&
				al1.getTargetAsset().equals(al2.getTargetAsset()) 
			) ||
			(	// similar due to opposite
				al1.getSourceAsset().equals(al2.getTargetAsset()) &&
				al1.getTargetAsset().equals(al2.getSourceAsset()) &&
				al1.getReferenceType().getOppositeTypeReference() != null &&
				al1.getReferenceType().getOppositeTypeReference().equals(al2.getReferenceType())
			);
	}
	
	/**
	 * recursively find all AssetGroups from the given parent AssetGroup
	 * @param parentGroup
	 * @return
	 */
	public static Set<AssetGroup> getAllAssetGroups(AssetGroup parentGroup) {
		Set<AssetGroup> result = new HashSet<>();
		result.add(parentGroup);
		result.addAll(parentGroup.getElements().stream()
				.filter(AssetGroup.class::isInstance).map(AssetGroup.class::cast)
				.flatMap(ag -> getAllAssetGroups(ag).stream())
				.collect(Collectors.toSet()));
		return result;
	}
	
	
/*	def List<AssetLink> getAllRelatedLinks(Asset asset) {
		val relatedLinks = asset.eResource().getResourceSet().getAllContents().filter(AssetLink).filter[ al |
			al.sourceAsset === asset ||
			al.targetAsset === asset
		].toList
		return relatedLinks
	}*/
	
	public static Set<AssetType> getAllAssetTypes(Set<AssetBasedSystem> absSet) {
		return absSet.stream().flatMap(abs -> abs.getDefinitionGroups().stream())
				.flatMap(dg -> dg.getAssetTypes().stream()).filter(AssetType.class::isInstance)
				.map(AssetType.class::cast).collect(Collectors.toSet());
	}

	public static AssetType getBaseAssetType(AbstractAssetType abstractType) {
		if (abstractType instanceof AssetType) {
			return (AssetType) abstractType;
		} else if (abstractType instanceof AssetTypeAspect) {
			return ((AssetTypeAspect) abstractType).getBaseAssetType();
		} else {
			return null;
		}
	}

	public static AssetType getContainerType(AssetTypeFeature feature) {
		return getBaseAssetType((AbstractAssetType) feature.eContainer());
	}

	public static boolean isMandatory(Multiplicity multiplicity) {
		return Objects.equals(multiplicity, Multiplicity.ONE) || Objects.equals(multiplicity, Multiplicity.ONE_OR_MANY);
	}

	public static boolean isMany(Multiplicity multiplicity) {
		return Objects.equals(multiplicity, Multiplicity.ZERO_OR_MANY)
				|| Objects.equals(multiplicity, Multiplicity.ONE_OR_MANY);
	}

	public static boolean isOfType(ConstantExpression constant, PrimitiveDataType attributeType) {
		if(attributeType == null) {
			return false;
		}
		if (constant instanceof UndefinedConstant) {
			return true;
		} else if (constant instanceof EnumConstant) {
			return Objects.equals(((EnumConstant) constant).getValue().eContainer(), attributeType);
		} else if (constant instanceof IntConstant) {
			return Objects.equals(AbsystemValidator.PRIMITIVEDATATYPE_INTEGER_NAME, attributeType.getName());
		} else if (constant instanceof StringConstant) {
			return Objects.equals(AbsystemValidator.PRIMITIVEDATATYPE_STRING_NAME, attributeType.getName());
		} else if (constant instanceof BooleanConstant) {
			return Objects.equals(AbsystemValidator.PRIMITIVEDATATYPE_BOOLEAN_NAME, attributeType.getName());
		} else if (constant instanceof VersionConstant) {
			return Objects.equals(AbsystemValidator.PRIMITIVEDATATYPE_VERSION_NAME, attributeType.getName());
		}
		return false;
	}

	public static String typeNameOf(ConstantExpression constant) {
		if (constant instanceof UndefinedConstant) {
			return "undefined";
		} else if (constant instanceof EnumConstant) {
			return ((EnumDataType) ((EnumConstant) constant).getValue().eContainer()).getName();
		} else if (constant instanceof IntConstant) {
			return AbsystemValidator.PRIMITIVEDATATYPE_INTEGER_NAME;
		} else if (constant instanceof StringConstant) {
			return AbsystemValidator.PRIMITIVEDATATYPE_STRING_NAME;
		} else if (constant instanceof BooleanConstant) {
			return AbsystemValidator.PRIMITIVEDATATYPE_BOOLEAN_NAME;
		} else if (constant instanceof VersionConstant) {
			return AbsystemValidator.PRIMITIVEDATATYPE_VERSION_NAME;
		}
		return null;
	}

	public static boolean actsOnCollections(ActionEnum actionType) {
		return Objects.equals(actionType, ActionEnum.ADD) || Objects.equals(actionType, ActionEnum.ADD_ALL)
				|| Objects.equals(actionType, ActionEnum.REMOVE) || Objects.equals(actionType, ActionEnum.REMOVE_ALL)
				|| Objects.equals(actionType, ActionEnum.CLEAR) || Objects.equals(actionType, ActionEnum.FOR_ALL);
	}

	public static boolean actsOnSimpleValues(ActionEnum actionType) {
		return Objects.equals(actionType, ActionEnum.ASSIGN);
	}

	public static boolean takesSimpleValueArg(ActionEnum actionType) {
		return Objects.equals(actionType, ActionEnum.ASSIGN) || Objects.equals(actionType, ActionEnum.ADD)
				|| Objects.equals(actionType, ActionEnum.REMOVE);
	}

	public static boolean takesCollectionArg(ActionEnum actionType) {
		return Objects.equals(actionType, ActionEnum.ADD_ALL) || Objects.equals(actionType, ActionEnum.REMOVE_ALL);
	}

	public static boolean takesLambdaArg(ActionEnum actionType) {
		return Objects.equals(actionType, ActionEnum.FOR_ALL);
	}

	public static boolean takesNoArg(ActionEnum actionType) {
		return Objects.equals(actionType, ActionEnum.CLEAR);
	}

	public static boolean appliesOnCollections(StaticMethod method) {
		return true;
	}

	public static boolean appliesOnSimpleValues(StaticMethod method) {
		return false;
	}

	public static boolean takesSimpleValueArg(StaticMethod method) {
		return Objects.equals(method.getName(), AbsystemValidator.STATICMETHOD_CONTAINS_NAME);
	}

	public static boolean takesCollectionArg(StaticMethod method) {
		return Objects.equals(method.getName(), AbsystemValidator.STATICMETHOD_CONTAINSALL_NAME)
				|| Objects.equals(method.getName(), AbsystemValidator.STATICMETHOD_CONTAINSANY_NAME);
	}

	public static boolean takesLambdaArg(StaticMethod method) {
		return Objects.equals(method.getName(), AbsystemValidator.STATICMETHOD_FILTER_NAME);
	}

	public static boolean takesNoArg(StaticMethod method) {
		return Objects.equals(method.getName(), AbsystemValidator.STATICMETHOD_ISEMPTY_NAME);
	}

	public static Stream<EObject> getHierarchy(EObject eObject) {
		return Stream.iterate(eObject, EObject::eContainer).takeWhile(Objects::nonNull);
	}

	public static Set<AssetBasedSystem> getAbsSet(EObject eobj) {
		return eobj.eResource().getResourceSet().getResources().stream()
				.filter(resource -> !resource.getContents().isEmpty()
						&& resource.getContents().get(0) instanceof AssetBasedSystem)
				.map(resource -> (AssetBasedSystem) resource.getContents().get(0)).collect(Collectors.toSet());
	}

	public static EnumDataType getEnumLiteralType(EnumLiteral literal) {
		return (EnumDataType) literal.eContainer();
	}
	
	/**
	 * Computes the relative-URI from <code>from </code> to <code>to</code>. If
	 * the schemes or the first two segments (i.e. quasi-autority, e.g. the
	 * 'resource' of 'platform:/resource', and the project/plugin name) the
	 * aren't equal, this method returns <code>to.toString()</code>.
	 */
	public static String relativeURIString(URI from, URI to) {
		String retVal = to.toString();
		if (from.scheme().equals( to.scheme())) {
			int noOfEqualSegments = 0;
			while (from.segmentCount() > noOfEqualSegments
					&& to.segmentCount() > noOfEqualSegments
					&& from.segment(noOfEqualSegments).equals(
							to.segment(noOfEqualSegments))) {
				noOfEqualSegments++;
			}
			final boolean urisBelongToSameProject = noOfEqualSegments >= 2;
			if (urisBelongToSameProject) {
				int noOfIndividualSegments = to.segments().length
						- noOfEqualSegments;
				if (noOfIndividualSegments > 0) {
					int goUp = from.segmentCount() - noOfEqualSegments - 1;
					String[] relativeSegments = new String[noOfIndividualSegments
							+ goUp];
					for (int i = 0; i < goUp; i++) {
						relativeSegments[i] = "..";
					}
					System.arraycopy(to.segments(), noOfEqualSegments,
							relativeSegments, goUp, noOfIndividualSegments);
					retVal = URI.createHierarchicalURI(relativeSegments, null,
							null).toString();
				}
			}
		}
		return retVal;
	}
	
	public static Optional<String> getName(EObject absEObject) {
		return Optional.ofNullable(new AbsystemSwitch<String>() {
			@Override
			public String caseAsset(Asset object) {
				return object.getName();
			}
			@Override
			public String caseAssetType(AssetType object) {
				return object.getName();
			}
			@Override
			public String caseAssetGroup(AssetGroup object) {
				return object.getName();
			}
			@Override
			public String caseAssetTypeFeature(AssetTypeFeature object) {
				return object.getName();
			}
			@Override
			public String caseDefinitionGroup(DefinitionGroup object) {
				return object.getName();
			}
			@Override
			public String caseEnumLiteral(EnumLiteral object) {
				return object.getName();
			}
			@Override
			public String caseGoal(Goal object) {
				return object.getName();
			}
			@Override
			public String caseGuard(Guard object) {
				return object.getName();
			}
			@Override
			public String casePrimitiveDataType(PrimitiveDataType object) {
				return object.getName();
			}
			@Override
			public String caseStaticMethod(StaticMethod object) {
				return object.getName();
			}
			@Override
			public String caseSymbol(Symbol object) {
				return object.getName();
			}
			@Override
			public String caseTag(Tag object) {
				return object.getName();
			}
		}.doSwitch(absEObject));
	}
	
	public static Optional<String> getQualifiedName(EObject absEObject, String qualifiedNameSeparator) {
		return getName(absEObject).map(name -> getQualifiedName(absEObject.eContainer(), qualifiedNameSeparator).map(containerName -> containerName + qualifiedNameSeparator + name).orElse(name));
	}
	
	public static URI resolveImportURI(String importURI, Resource importLocalResource) {
		return resolveImportURI(URI.createURI(importURI), importLocalResource);
	}
	
	public static URI resolveImportURI(URI importURI, Resource importLocalResource) {
		return importURI.isRelative() ? importURI.resolve(importLocalResource.getURI()) : importURI;
	}
	
	public static String simplePrettyPrint(EObject absEObject) {
		
		AbsystemSwitch<String> myPPSwitch = new AbsystemSwitch<String>() {
			@Override
			public String caseStringConstant(StringConstant object) {
				return "\""+object.getValue()+"\"";
			}
			@Override
			public String caseIntConstant(IntConstant object) {
				return Integer.toString(object.getValue());
			}
			@Override
			public String caseVersionConstant(VersionConstant object) {
				String result = String.format("%d.%d.%d", object.getValue().getMajor(), object.getValue().getMinor(),object.getValue().getPatch());
				result = result + Arrays.asList(object.getValue().getPrerelease()).stream().collect( Collectors.joining( "" ) );
				return result;
			}
			@Override
			public String caseBooleanConstant(BooleanConstant object) {
				return object.getValue();
			}

			@Override
			public String caseEnumConstant(EnumConstant object) {
				return object.getValue().getName();
			}
			
			@Override
            public String defaultCase(EObject o) {
                return o.toString();
            };
		};
		
		return myPPSwitch.doSwitch(absEObject);
	}

	public static Set<AssetBasedSystem> getAllAbsInResourceSet(ResourceSet resourceSet) {
		return resourceSet.getResources().stream().flatMap(resource -> resource.getContents().stream())
				.filter(AssetBasedSystem.class::isInstance).map(AssetBasedSystem.class::cast)
				.collect(Collectors.toSet());
	}
	
	public static Set<AssetTypeAspect> getAllAspectsOfType(AssetType assetType) {
		if(assetType.eResource() == null) {
			return Set.of();
		}
		Iterator<?> allContents;
		if(assetType.eResource().getResourceSet() == null) {
			allContents = assetType.eResource().getAllContents();
		} else {
			allContents = assetType.eResource().getResourceSet().getAllContents();
		}
		Set<AssetTypeAspect> result = new HashSet<>();
		Iterators.filter(allContents, AssetTypeAspect.class).forEachRemaining(aspect -> {
			if(aspect.getBaseAssetType() == assetType) {
				result.add(aspect);
			}
		});
		return result;
	}
	
	private static URI getLocalizationResourceURI(URI resourceToLocalizeURI, String locale) {
		int firstIndexOfModels = -1;
		String[] segments = resourceToLocalizeURI.segments();
		for(int i = 0; i < segments.length ; i++) {
			if(Objects.equals(segments[i], "models")) {
				firstIndexOfModels = i;
				break;
			}
		}
		if(firstIndexOfModels == -1) {
			return null;
		}
		// Expected File structure: 
		// model file : prefix/models/subpath where prefix contains no segment named "models"
		// locale file: prefix/locales/{locale}/subpath
		String[] subpath = Arrays.copyOfRange(resourceToLocalizeURI.segments(), firstIndexOfModels + 1, resourceToLocalizeURI.segmentCount());
		URI trimmed = resourceToLocalizeURI.trimSegments(resourceToLocalizeURI.segmentCount() - firstIndexOfModels);
		return trimmed.appendSegment("locales").appendSegment(locale).appendSegments(subpath);
	}
	
	public static Resource getLocalizationResource(Resource resourceToLocalize, String locale) {
		ResourceSet rs = resourceToLocalize.getResourceSet();
		if(rs == null) {
			rs = new ResourceSetImpl();
			rs.getResources().add(resourceToLocalize);
		}
		URI localeResourceURI = null;
		if(resourceToLocalize.getURI().isPlatformResource()) {
			localeResourceURI = getLocalizationResourceURI(resourceToLocalize.getURI(), locale);
		}
		if(localeResourceURI != null && rs.getURIConverter().exists(localeResourceURI, null)) {
			return rs.getResource(localeResourceURI, true);
		}
		else {
			return null;
		}
	}
	
	public static List<LocaleGroup> getLocalizationGroups(Resource resourceToLocalize, String locale) {
		Resource localeResource = getLocalizationResource(resourceToLocalize, locale);
		if (localeResource == null) {
			return List.of();
		}
		List<LocaleGroup> result = new ArrayList<>();
		Iterators.filter(Iterators.filter(localeResource.getAllContents(), LocaleGroup.class),
				localeGroup -> Objects.equals(localeGroup.getLocale(), locale)).forEachRemaining(result::add);
		return result;
	}

	public static Optional<DefinitionGroupLocale> getLocalization(DefinitionGroup eobj, String locale) {
		if(eobj.eResource() == null || eobj.eResource().getResourceSet() == null) {
			return Optional.empty();
		}
		List<LocaleGroup> localeGroups = getLocalizationGroups(eobj.eResource(), locale);
		List<DefinitionGroupLocale> matching = new ArrayList<>();
		for (LocaleGroup group : localeGroups) {
			Iterators.filter(Iterators.filter(group.eAllContents(), DefinitionGroupLocale.class),
					dgl -> dgl.getRef() == eobj).forEachRemaining(matching::add);
		}
		return matching.isEmpty() ? Optional.empty() : Optional.of(matching.get(0));
	}

	public static Optional<PrimitiveDataTypeLocale> getLocalization(PrimitiveDataType eobj, String locale) {
		if(eobj.eResource() == null || eobj.eResource().getResourceSet() == null) {
			return Optional.empty();
		}
		List<LocaleGroup> localeGroups = getLocalizationGroups(eobj.eResource(), locale);
		List<PrimitiveDataTypeLocale> matching = new ArrayList<>();
		for (LocaleGroup group : localeGroups) {
			Iterators.filter(Iterators.filter(group.eAllContents(), PrimitiveDataTypeLocale.class),
					dgl -> dgl.getRef() == eobj).forEachRemaining(matching::add);
		}
		return matching.isEmpty() ? Optional.empty() : Optional.of(matching.get(0));
	}

	public static Optional<EnumLiteralLocale> getLocalization(EnumLiteral eobj, String locale) {
		if(eobj.eResource() == null || eobj.eResource().getResourceSet() == null) {
			return Optional.empty();
		}
		List<LocaleGroup> localeGroups = getLocalizationGroups(eobj.eResource(), locale);
		List<EnumLiteralLocale> matching = new ArrayList<>();
		for (LocaleGroup group : localeGroups) {
			Iterators.filter(Iterators.filter(group.eAllContents(), EnumLiteralLocale.class),
					dgl -> dgl.getRef() == eobj).forEachRemaining(matching::add);
		}
		return matching.isEmpty() ? Optional.empty() : Optional.of(matching.get(0));
	}

	public static Optional<AssetTypeLocale> getLocalization(AssetType eobj, String locale) {
		if(eobj.eResource() == null || eobj.eResource().getResourceSet() == null) {
			return Optional.empty();
		}
		List<LocaleGroup> localeGroups = getLocalizationGroups(eobj.eResource(), locale);
		List<AssetTypeLocale> matching = new ArrayList<>();
		for (LocaleGroup group : localeGroups) {
			Iterators.filter(Iterators.filter(group.eAllContents(), AssetTypeLocale.class), dgl -> dgl.getRef() == eobj)
					.forEachRemaining(matching::add);
		}
		return matching.isEmpty() ? Optional.empty() : Optional.of(matching.get(0));
	}

	public static Optional<AssetTypeAspectLocale> getLocalization(AssetTypeAspect eobj, String locale) {
		if(eobj.eResource() == null || eobj.eResource().getResourceSet() == null) {
			return Optional.empty();
		}
		List<LocaleGroup> localeGroups = getLocalizationGroups(eobj.eResource(), locale);
		List<AssetTypeAspectLocale> matching = new ArrayList<>();
		for (LocaleGroup group : localeGroups) {
			Iterators
					.filter(Iterators.filter(group.eAllContents(), AssetTypeAspectLocale.class),
							dgl -> dgl.getBaseAssetType() == eobj.getBaseAssetType() && !dgl.getFeatures().isEmpty()
									&& dgl.getFeatures().get(0).getRef().eContainer() == eobj)
					.forEachRemaining(matching::add);
		}
		return matching.isEmpty() ? Optional.empty() : Optional.of(matching.get(0));
	}

	public static Optional<AssetTypeFeatureLocale> getLocalization(AssetTypeFeature eobj, String locale) {
		if(eobj.eResource() == null || eobj.eResource().getResourceSet() == null) {
			return Optional.empty();
		}
		List<LocaleGroup> localeGroups = getLocalizationGroups(eobj.eResource(), locale);
		List<AssetTypeFeatureLocale> matching = new ArrayList<>();
		for (LocaleGroup group : localeGroups) {
			Iterators.filter(Iterators.filter(group.eAllContents(), AssetTypeFeatureLocale.class),
					dgl -> dgl.getRef() == eobj).forEachRemaining(matching::add);
		}
		return matching.isEmpty() ? Optional.empty() : Optional.of(matching.get(0));
	}

	public static Optional<GuardLocale> getLocalization(Guard eobj, String locale) {
		if(eobj.eResource() == null || eobj.eResource().getResourceSet() == null) {
			return Optional.empty();
		}
		List<LocaleGroup> localeGroups = getLocalizationGroups(eobj.eResource(), locale);
		List<GuardLocale> matching = new ArrayList<>();
		for (LocaleGroup group : localeGroups) {
			Iterators.filter(Iterators.filter(group.eAllContents(), GuardLocale.class), dgl -> dgl.getRef() == eobj)
					.forEachRemaining(matching::add);
		}
		return matching.isEmpty() ? Optional.empty() : Optional.of(matching.get(0));
	}
	
	public static Optional<RequirementLocale> getLocalization(Requirement eobj, String locale) {
		if(eobj.eResource() == null || eobj.eResource().getResourceSet() == null) {
			return Optional.empty();
		}
		List<LocaleGroup> localeGroups = getLocalizationGroups(eobj.eResource(), locale);
		List<RequirementLocale> matching = new ArrayList<>();
		for (LocaleGroup group : localeGroups) {
			Iterators.filter(Iterators.filter(group.eAllContents(), RequirementLocale.class), dgl -> dgl.getRef() == eobj)
					.forEachRemaining(matching::add);
		}
		return matching.isEmpty() ? Optional.empty() : Optional.of(matching.get(0));
	}
	
	public static Optional<String> getLocalizedName(EObject eObject, String locale) {
		if(eObject instanceof PrimitiveDataType) {
			Optional<PrimitiveDataTypeLocale> localization = getLocalization((PrimitiveDataType) eObject, locale);
			if(localization.isPresent()) {
				return Optional.ofNullable(localization.orElseThrow().getName());
			}
		} else if(eObject instanceof EnumLiteral) {
			Optional<EnumLiteralLocale> localization = getLocalization((EnumLiteral) eObject, locale);
			if(localization.isPresent()) {
				return Optional.ofNullable(localization.orElseThrow().getName());
			}
		} else if(eObject instanceof AssetType) {
			Optional<AssetTypeLocale> localization = getLocalization((AssetType) eObject, locale);
			if(localization.isPresent()) {
				return Optional.ofNullable(localization.orElseThrow().getName());
			}
		} else if(eObject instanceof AssetTypeFeature) {
			Optional<AssetTypeFeatureLocale> localization = getLocalization((AssetTypeFeature) eObject, locale);
			if(localization.isPresent()) {
				return Optional.ofNullable(localization.orElseThrow().getName());
			}
		} else if(eObject instanceof Guard) {
			Optional<GuardLocale> localization = getLocalization((Guard) eObject, locale);
			if(localization.isPresent()) {
				return Optional.ofNullable(localization.orElseThrow().getName());
			}
		}
		return ABSUtils.getName(eObject);
	}
	
	public static String getLocale() {
		var overrideBy = GlobalPreferenceService.INSTANCE.getString("fr.irisa.atsyra.ide.ui", "overrideLocale");
		if(overrideBy.isPresent() && !overrideBy.orElseThrow().isEmpty()) {
			return overrideBy.orElseThrow();
		} else {
			return Locale.getDefault().getLanguage();
		}
	}
	
	public static Set<AssetBasedSystem> getAllImportedAbs(AssetBasedSystem model) {
		ResourceSet rs = model.eResource().getResourceSet();
		return model.getImports().stream().filter(imp -> imp.getImportURI() != null)
				.map(imp -> ABSUtils.resolveImportURI(imp.getImportURI(), model.eResource()))
				.filter(uri -> rs.getURIConverter().exists(uri, null))
				.map(uri -> rs.getResource(uri, true))
				.filter(resource -> !resource.getContents().isEmpty()
						&& resource.getContents().get(0) instanceof AssetBasedSystem)
				.map(resource -> (AssetBasedSystem) resource.getContents().get(0)).collect(Collectors.toSet());
	}

	public static Set<AssetBasedSystem> importsTransitiveClosure(AssetBasedSystem model) {
		Set<AssetBasedSystem> result = new HashSet<>();
		result.add(model);
		return importsTransitiveClosure(model, result);
	}
	
	public static Set<AssetBasedSystem> importsTransitiveClosure(Set<AssetBasedSystem> models) {
		Set<AssetBasedSystem> result = new HashSet<>(models);
		for (AssetBasedSystem model : models) {
			result = importsTransitiveClosure(model, result);
		}
		return result;
	}

	private static Set<AssetBasedSystem> importsTransitiveClosure(AssetBasedSystem model,
			Set<AssetBasedSystem> accumulator) {
		Set<AssetBasedSystem> referencedAbs = getAllImportedAbs(model);
		referencedAbs.removeAll(accumulator);
		accumulator.addAll(referencedAbs);
		for (AssetBasedSystem abs : referencedAbs) {
			accumulator = importsTransitiveClosure(abs, accumulator);
		}
		return accumulator;
	}
}
