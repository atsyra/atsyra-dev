/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.UnaryOperator;

import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.model.absystem.util.PrimitiveDataTypeSwitch;

public class AssetInstance {
	Map<AssetTypeReference, Optional<Asset>> refvalues;
	Map<AssetTypeAttribute, Optional<String>> stringvalues;
	Map<AssetTypeAttribute, Optional<Integer>> intvalues;
	Map<AssetTypeAttribute, Optional<Boolean>> boolvalues;
	Map<AssetTypeAttribute, Optional<EnumLiteral>> enumvalues;
	Map<AssetTypeAttribute, Optional<Version>> versionvalues;

	Multimap<AssetTypeReference, Asset> refcollections;
	Multimap<AssetTypeAttribute, String> stringcollections;
	Multimap<AssetTypeAttribute, Integer> intcollections;
	Multimap<AssetTypeAttribute, Boolean> boolcollections;
	Multimap<AssetTypeAttribute, EnumLiteral> enumcollections;
	Multimap<AssetTypeAttribute, Version> versioncollections;

	public AssetInstance() {
		refvalues = new HashMap<>();
		stringvalues = new HashMap<>();
		intvalues = new HashMap<>();
		boolvalues = new HashMap<>();
		enumvalues = new HashMap<>();
		versionvalues = new HashMap<>();
		refcollections = HashMultimap.create();
		stringcollections = HashMultimap.create();
		intcollections = HashMultimap.create();
		boolcollections = HashMultimap.create();
		enumcollections = HashMultimap.create();
		versioncollections = HashMultimap.create();
	}

	public void addInCollectionGeneric(Object o, AssetTypeFeature feature) {
		if (o instanceof Asset) {
			addInCollection((Asset) o, (AssetTypeReference) feature);
		} else if (o instanceof Boolean) {
			addInCollection((Boolean) o, (AssetTypeAttribute) feature);
		} else if (o instanceof Integer) {
			addInCollection((Integer) o, (AssetTypeAttribute) feature);
		} else if (o instanceof String) {
			addInCollection((String) o, (AssetTypeAttribute) feature);
		} else if (o instanceof EnumLiteral) {
			addInCollection((EnumLiteral) o, (AssetTypeAttribute) feature);
		} else if (o instanceof Version) {
			addInCollection((Version) o, (AssetTypeAttribute) feature);
		} else {
			throw new RuntimeException("addInCollectionGeneric: unknown type " + o.getClass().toString());
		}
	}

	private void addInCollection(Version value, AssetTypeAttribute attribute) {
		versioncollections.put(attribute, value);
	}

	private void addInCollection(String value, AssetTypeAttribute attribute) {
		stringcollections.put(attribute, value);
	}

	private void addInCollection(Integer value, AssetTypeAttribute attribute) {
		intcollections.put(attribute, value);
	}

	private void addInCollection(Boolean value, AssetTypeAttribute attribute) {
		boolcollections.put(attribute, value);
	}

	private void addInCollection(EnumLiteral value, AssetTypeAttribute attribute) {
		enumcollections.put(attribute, value);
	}

	private void addInCollection(Asset value, AssetTypeReference reference) {
		refcollections.put(reference, value);
	}

	public void addReference(Asset value, AssetTypeReference reference) {
		refvalues.put(reference, Optional.of(value));
	}

	public void addBooleanAttribute(boolean value, AssetTypeAttribute attribute) {
		boolvalues.put(attribute, Optional.of(value));
	}

	public void addIntegerAttribute(int value, AssetTypeAttribute attribute) {
		intvalues.put(attribute, Optional.of(value));
	}

	public void addStringAttribute(String value, AssetTypeAttribute attribute) {
		stringvalues.put(attribute, Optional.of(value));
	}

	public void addEnumAttribute(EnumLiteral litteral, AssetTypeAttribute attribute) {
		enumvalues.put(attribute, Optional.of(litteral));
	}

	public void addVersionAttribute(Version value, AssetTypeAttribute attribute) {
		versionvalues.put(attribute, Optional.of(value));
	}

	public void addUndefinedFeature(AssetTypeFeature feature) {
		if (feature instanceof AssetTypeReference) {
			addUndefinedFeature((AssetTypeReference) feature);
		} else if (feature instanceof AssetTypeAttribute) {
			addUndefinedFeature((AssetTypeAttribute) feature);
		} else
			throw new UnsupportedOperationException(
					feature + " should be either an AssetTypeReference or an AssetTypeAttribute.");
	}

	public void addUndefinedFeature(AssetTypeReference feature) {
		refvalues.put(feature, Optional.empty());
	}

	public void addUndefinedFeature(AssetTypeAttribute feature) {
		if (feature.getAttributeType() instanceof EnumDataType) {
			enumvalues.put(feature, Optional.empty());
		} else {
			switch (feature.getAttributeType().getName()) {
			case "Boolean":
				boolvalues.put(feature, Optional.empty());
				break;
			case "Integer":
				intvalues.put(feature, Optional.empty());
				break;
			case "String":
				stringvalues.put(feature, Optional.empty());
				break;
			case "Version":
				versionvalues.put(feature, Optional.empty());
				break;
			default:
				throw new UnsupportedOperationException(
						"unknown PrimitiveDataType " + feature.getAttributeType().getName());
			}
		}
	}

	public Map<AssetTypeReference, Optional<Asset>> getRefvalues() {
		return Collections.unmodifiableMap(refvalues);
	}

	public Map<AssetTypeAttribute, Optional<String>> getStringvalues() {
		return Collections.unmodifiableMap(stringvalues);
	}

	public Map<AssetTypeAttribute, Optional<Integer>> getIntvalues() {
		return Collections.unmodifiableMap(intvalues);
	}

	public Map<AssetTypeAttribute, Optional<Boolean>> getBoolvalues() {
		return Collections.unmodifiableMap(boolvalues);
	}

	public Map<AssetTypeAttribute, Optional<EnumLiteral>> getEnumvalues() {
		return Collections.unmodifiableMap(enumvalues);
	}

	public Map<AssetTypeAttribute, Optional<Version>> getVersionvalues() {
		return Collections.unmodifiableMap(versionvalues);
	}

	public Multimap<AssetTypeReference, Asset> getRefcollections() {
		return Multimaps.unmodifiableMultimap(refcollections);
	}

	public Multimap<AssetTypeAttribute, String> getStringcollections() {
		return Multimaps.unmodifiableMultimap(stringcollections);
	}

	public Multimap<AssetTypeAttribute, Integer> getIntcollections() {
		return Multimaps.unmodifiableMultimap(intcollections);
	}

	public Multimap<AssetTypeAttribute, Boolean> getBoolcollections() {
		return Multimaps.unmodifiableMultimap(boolcollections);
	}

	public Multimap<AssetTypeAttribute, EnumLiteral> getEnumcollections() {
		return Multimaps.unmodifiableMultimap(enumcollections);
	}

	public Multimap<AssetTypeAttribute, Version> getVersioncollections() {
		return Multimaps.unmodifiableMultimap(versioncollections);
	}
	
	public AssetInstance transform(UnaryOperator<EObject> transformation) {
		AssetInstance result = new AssetInstance();
		for (Entry<AssetTypeAttribute, Optional<Boolean>> entry : getBoolvalues().entrySet()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			Optional<Boolean> sourceValue = entry.getValue();
			if (sourceValue.isPresent()) {
				result.addBooleanAttribute(sourceValue.get(), (AssetTypeAttribute) transformation.apply(sourceAttribute));
			} else {
				result.addUndefinedFeature((AssetTypeAttribute) transformation.apply(sourceAttribute));
			}
		}
		for (Entry<AssetTypeAttribute, Optional<Integer>> entry : getIntvalues().entrySet()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			Optional<Integer> sourceValue = entry.getValue();
			if (sourceValue.isPresent()) {
				result.addIntegerAttribute(sourceValue.get(), (AssetTypeAttribute) transformation.apply(sourceAttribute));
			} else {
				result.addUndefinedFeature((AssetTypeAttribute) transformation.apply(sourceAttribute));
			}
		}
		for (Entry<AssetTypeAttribute, Optional<String>> entry : getStringvalues().entrySet()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			Optional<String> sourceValue = entry.getValue();
			if (sourceValue.isPresent()) {
				result.addStringAttribute(sourceValue.get(), (AssetTypeAttribute) transformation.apply(sourceAttribute));
			} else {
				result.addUndefinedFeature((AssetTypeAttribute) transformation.apply(sourceAttribute));
			}
		}
		for (Entry<AssetTypeAttribute, Optional<Version>> entry : getVersionvalues().entrySet()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			Optional<Version> sourceValue = entry.getValue();
			if (sourceValue.isPresent()) {
				result.addVersionAttribute(sourceValue.get(), (AssetTypeAttribute) transformation.apply(sourceAttribute));
			} else {
				result.addUndefinedFeature((AssetTypeAttribute) transformation.apply(sourceAttribute));
			}
		}
		for (Entry<AssetTypeAttribute, Optional<EnumLiteral>> entry : getEnumvalues().entrySet()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			Optional<EnumLiteral> sourceValue = entry.getValue();
			if (sourceValue.isPresent()) {
				result.addEnumAttribute((EnumLiteral) transformation.apply(sourceValue.get()),
						(AssetTypeAttribute) transformation.apply(sourceAttribute));
			} else {
				result.addUndefinedFeature((AssetTypeAttribute) transformation.apply(sourceAttribute));
			}
		}
		for (Entry<AssetTypeReference, Optional<Asset>> entry : getRefvalues().entrySet()) {
			AssetTypeReference sourceAttribute = entry.getKey();
			Optional<Asset> sourceValue = entry.getValue();
			if (sourceValue.isPresent()) {
				result.addReference((Asset) transformation.apply(sourceValue.get()),
						(AssetTypeReference) transformation.apply(sourceAttribute));
			} else {
				result.addUndefinedFeature((AssetTypeReference) transformation.apply(sourceAttribute));
			}
		}
		for (Entry<AssetTypeAttribute, Boolean> entry : getBoolcollections().entries()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			Boolean sourceValue = entry.getValue();
			result.addInCollectionGeneric(sourceValue, (AssetTypeFeature) transformation.apply(sourceAttribute));
		}
		for (Entry<AssetTypeAttribute, Integer> entry : getIntcollections().entries()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			Integer sourceValue = entry.getValue();
			result.addInCollectionGeneric(sourceValue, (AssetTypeFeature) transformation.apply(sourceAttribute));
		}
		for (Entry<AssetTypeAttribute, String> entry : getStringcollections().entries()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			String sourceValue = entry.getValue();
			result.addInCollectionGeneric(sourceValue, (AssetTypeFeature) transformation.apply(sourceAttribute));
		}
		for (Entry<AssetTypeAttribute, Version> entry : getVersioncollections().entries()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			Version sourceValue = entry.getValue();
			result.addInCollectionGeneric(sourceValue, (AssetTypeFeature) transformation.apply(sourceAttribute));
		}
		for (Entry<AssetTypeAttribute, EnumLiteral> entry : getEnumcollections().entries()) {
			AssetTypeAttribute sourceAttribute = entry.getKey();
			EnumLiteral sourceValue = entry.getValue();
			result.addInCollectionGeneric(transformation.apply(sourceValue), (AssetTypeFeature) transformation.apply(sourceAttribute));
		}
		for (Entry<AssetTypeReference, Asset> entry : getRefcollections().entries()) {
			AssetTypeReference sourceAttribute = entry.getKey();
			Asset sourceValue = entry.getValue();
			result.addInCollectionGeneric(transformation.apply(sourceValue), (AssetTypeFeature) transformation.apply(sourceAttribute));
		}
		return result;
	}
	
	/**
	 * 
	 * @param feature
	 * @return the value associated with the feature. It returns an Empty Optional for undefined, and null if the feature does not exists for this asset.
	 */
	public Object getValue(AssetTypeFeature feature) {
		if(feature instanceof AssetTypeAttribute) {
			AssetTypeAttribute attribute = (AssetTypeAttribute) feature;
			if(attribute.getAttributeType() instanceof EnumDataType) {
				if(ABSUtils.isMany(feature.getMultiplicity())) {
					return enumcollections.get(attribute);
				} else {
					return enumvalues.get(attribute);
				}
			} else {
				return new PrimitiveDataTypeSwitch<Object>() {
					@Override
					public Object caseBoolean() {
						if(ABSUtils.isMany(feature.getMultiplicity())) {
							return boolcollections.get(attribute);
						} else {
							return boolvalues.get(attribute);
						}
					}
					@Override
					public Object caseInteger() {
						if(ABSUtils.isMany(feature.getMultiplicity())) {
							return intcollections.get(attribute);
						} else {
							return intvalues.get(attribute);
						}
					}
					@Override
					public Object caseString() {
						if(ABSUtils.isMany(feature.getMultiplicity())) {
							return stringcollections.get(attribute);
						} else {
							return stringvalues.get(attribute);
						}
					}
					@Override
					public Object caseVersion() {
						if(ABSUtils.isMany(feature.getMultiplicity())) {
							return versioncollections.get(attribute);
						} else {
							return versionvalues.get(attribute);
						}
					}
				}.doSwitch(attribute.getAttributeType());
			}
		} else {
			AssetTypeReference reference = (AssetTypeReference) feature;
			if(ABSUtils.isMany(feature.getMultiplicity())) {
				return refcollections.get(reference);
			} else {
				return refvalues.get(reference);
			}
		}
	}

}
