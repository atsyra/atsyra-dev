package fr.irisa.atsyra.absystem.model.absystem.util;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.resource.ResourceSet;

import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType;
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter;
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;

public class ABSSizeInfo {
	
	private int nbAssetType = 0;
	private int nbAttribute = 0;
	private int nbReference = 0;
	private int nbEnumType = 0;
	private int nbAsset = 0;
	private int nbLink = 0;
	private int nbGuardedAction = 0;
	private int nbGuardedActionInstance = 0;
	private BigInteger nbStatesWithDefault = BigInteger.ZERO;
	private BigInteger nbStates = BigInteger.ZERO;
	
	public ABSSizeInfo(int nbAssetType, int nbAttribute, int nbReference, int nbEnumType, int nbAsset, int nbLink, int nbGuardedAction, int nbGuardedActionInstance, BigInteger nbStatesWithDefault, BigInteger nbStates) {
		this.nbAssetType = nbAssetType;
		this.nbAttribute = nbAttribute;
		this.nbReference = nbReference;
		this.nbEnumType = nbEnumType;
		this.nbAsset = nbAsset;
		this.nbLink = nbLink;
		this.nbGuardedAction = nbGuardedAction;
		this.nbGuardedActionInstance = nbGuardedActionInstance;
		this.nbStatesWithDefault = nbStatesWithDefault;
		this.nbStates = nbStates;
	}
	
	public String toString() {
		return "{\"AssetType\": " + nbAssetType 
				+ ", \"Attribute\": " + nbAttribute 
				+ ", \"Reference\": " + nbReference 
				+ ", \"EnumType\": " + nbEnumType 
				+ ", \"Asset\": " + nbAsset 
				+ ", \"Link\": " + nbLink 
				+ ", \"GuardedAction\": " + nbGuardedAction 
				+ ", \"GuardedActionInstance\": " + nbGuardedActionInstance 
				+ ", \"nbStatesWithDefault\": " + nbStatesWithDefault 
				+ ", \"nbStates\": " + nbStates 
				+"}";
	}
	
	public static ABSSizeInfo getAbsModelSizeInfo(AssetBasedSystem abs) {
		return getAbsModelSizeInfo(ABSUtils.importsTransitiveClosure(abs));
	}
	
	public static ABSSizeInfo getAbsModelSizeInfo(Set<AssetBasedSystem> absSet) {
		int nbAssetType = 0;
		int nbAttribute = 0;
		int nbReference = 0;
		int nbEnumType = 0;
		int nbAsset = 0;
		int nbLink = 0;
		int nbGuardedAction = 0;
		int nbGuardedActionInstance = 0;
		Map<AssetType, Integer> assetInstanceCount = new HashMap<>();
		//First loop to initialize assetInstanceCount
		for (AssetBasedSystem abs : absSet) {
			for (AssetGroup ag : abs.getAssetGroups()) {
				nbAsset += ag.getAssets().size();
				nbLink += ag.getAssetLinks().size();
				for(Asset asset : ag.getAssets()) {
					for (AssetType type : ABSUtils.getAllSupertypes(asset.getAssetType())) {
						assetInstanceCount.merge(type, 1, Integer::sum);
					}
				}
			}
		}
		//Now we can use assetInstanceCount
		for (AssetBasedSystem abs : absSet) {
			for (DefinitionGroup dg : abs.getDefinitionGroups()) {
				for (AbstractAssetType abstractAssetType : dg.getAssetTypes()) {
					if(abstractAssetType instanceof AssetType) {
						nbAssetType++;
					}
					nbAttribute += abstractAssetType.getAssetTypeAttributes().size();
					nbReference += abstractAssetType.getAssetTypeProperties().size();
				}
				for (PrimitiveDataType primitiveType : dg.getPrimitiveDataTypes()) {
					if(primitiveType instanceof EnumDataType) {
						nbEnumType++;
					}
				}
				nbGuardedAction += dg.getGuardedActions().size();
				for (GuardedAction guardedAction : dg.getGuardedActions()) {
					int nbInstances = 1;
					for(GuardParameter param : guardedAction.getGuardParameters()) {
						nbInstances *= assetInstanceCount.getOrDefault(param.getParameterType(),0);
					}
					nbGuardedActionInstance += nbInstances;
				}
			}
			
		}
		ABSExpressionEval exprEval = new ABSExpressionEval(absSet);
		return new ABSSizeInfo(nbAssetType, nbAttribute, nbReference, nbEnumType, nbAsset, nbLink, nbGuardedAction, nbGuardedActionInstance, exprEval.nbState(true), exprEval.nbState(false));
	}
}
