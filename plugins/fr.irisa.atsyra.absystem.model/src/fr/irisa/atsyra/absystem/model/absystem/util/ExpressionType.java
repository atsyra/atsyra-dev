/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.util;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;

public class ExpressionType {
	private boolean collection;
	private AssetType assetType;
	private PrimitiveDataType primitiveType;
	private boolean mutable;
	
	private ExpressionType(boolean collection, AssetType assetType, PrimitiveDataType primitiveType) {
		this.collection = collection;
		this.assetType = assetType;
		this.primitiveType = primitiveType;
		this.mutable = false;
	}
	
	public static ExpressionType create(boolean collection, Object type) {
		if(type instanceof AssetType) {
			return new ExpressionType(collection, (AssetType) type, null);
		} else if(type instanceof PrimitiveDataType) {
			return new ExpressionType(collection, null, (PrimitiveDataType) type);
		} 
		return null;
	}
	
	/**
	 * 
	 * @param type : An {@code AssetType} or {@code PrimitiveDataType}
	 * @return An {@code ExpressionType}  representing immutable collections of type {@code type}
	 */
	public static ExpressionType collection(Object type) {
		return create(true, type);
	}
	
	/**
	 * 
	 * @param type : An {@code AssetType} or {@code PrimitiveDataType}
	 * @return An {@code ExpressionType}  representing immutable value of type {@code type}
	 */
	public static ExpressionType simple(Object type) {
		return create(false, type);
	}
	
	public static ExpressionType typeOf(AssetTypeFeature feature) {
		if(feature instanceof AssetTypeReference) {
			AssetTypeReference reference = (AssetTypeReference) feature;
			return new ExpressionType(ABSUtils.isMany(feature.getMultiplicity()), reference.getPropertyType(), null);
		} else if(feature instanceof AssetTypeAttribute) {
			AssetTypeAttribute attribute = (AssetTypeAttribute) feature;
			return new ExpressionType(ABSUtils.isMany(feature.getMultiplicity()), null, attribute.getAttributeType());
		} 
		return null;
	}
	
	public static ExpressionType undefined() {
		return new ExpressionType(false, null, null);
	}
	
	/**
	 * Only use this function for collection from which the type cannot be inferred
	 */
	public static ExpressionType emptyCollection() {
		return new ExpressionType(true, null, null);
	}
	
	public boolean isPrimitiveType() {
		return primitiveType != null;
	}
	
	public boolean isAssetType() {
		return assetType != null;
	}
	
	public boolean isUndefinedType() {
		return assetType == null && primitiveType == null;
	}
	
	public boolean isInequalityComparable() {
		return isPrimitiveType()
				&& (Objects.equals(primitiveType.getName(), AbsystemValidator.PRIMITIVEDATATYPE_INTEGER_NAME)
						|| Objects.equals(primitiveType.getName(), AbsystemValidator.PRIMITIVEDATATYPE_VERSION_NAME)
						|| primitiveType instanceof EnumDataType);
	}
	
	public EObject getType() {
		if(isPrimitiveType()) {
			return primitiveType;
		} else {
			return assetType;
		}
	}
	
	public String getTypeName() {
		if(isPrimitiveType()) {
			return primitiveType.getName();
		} else if(isAssetType()) {
			return assetType.getName();
		} else {
			return "null";
		}
	}

	public boolean isCollection() {
		return collection;
	}
	
	public Set<ExpressionType> getAllSupertypes() {
		if(isAssetType()) {
			return ABSUtils.getAllSupertypes(assetType).stream().map(atype -> new ExpressionType(collection, atype, null)).collect(Collectors.toSet());
		} else {
			return Set.of(this);
		}
	}
	
	public boolean isBooleanType() {
		return isPrimitiveType() && Objects.equals(primitiveType.getName(), AbsystemValidator.PRIMITIVEDATATYPE_BOOLEAN_NAME);
	}
	
	public boolean isSubtypeOf(ExpressionType other) {
		if(isPrimitiveType()) {
			return other.isPrimitiveType() && primitiveType == other.primitiveType;
		} else if (isAssetType()){
			Set<AssetType> supertype = ABSUtils.getAllSupertypes(assetType);
			return other.isAssetType() && supertype.contains(other.assetType);
		} else {
			return false;
		}
		
	}

	public boolean isMutable() {
		return mutable;
	}

	public void setMutable(boolean mutable) {
		this.mutable = mutable;
	}
}