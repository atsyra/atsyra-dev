/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.Ordering;

import fr.irisa.atsyra.absystem.model.absystem.Version;

public final class Version implements Comparable<Version> {
	private int major;
	private int minor;
	private int patch;
	private String[] prerelease;
	private String[] metadata;
	
	public Version() {
		major = 0;
		minor = 0;
		patch = 0;
		prerelease = (String[]) Array.newInstance(String.class, 0);
		metadata = (String[]) Array.newInstance(String.class, 0);
	}

	public int getMajor() {
		return major;
	}

	public void setMajor(int major) {
		this.major = major;
	}

	public int getMinor() {
		return minor;
	}

	public void setMinor(int minor) {
		this.minor = minor;
	}

	public int getPatch() {
		return patch;
	}

	public void setPatch(int patch) {
		this.patch = patch;
	}

	public String[] getPrerelease() {
		return prerelease;
	}

	public void setPrerelease(String[] prerelease) {
		if(prerelease == null) {
			prerelease = (String[]) Array.newInstance(String.class, 0);
		}
		this.prerelease = prerelease;
	}

	public boolean hasPrerelease() {
		return this.prerelease != null && this.prerelease.length > 0;
	}

	public String[] getMetadata() {
		return metadata;
	}

	public void setMetadata(String[] metadata) {
		if(metadata == null) {
			metadata = (String[]) Array.newInstance(String.class, 0);
		}
		this.metadata = metadata;
	}

	public boolean hasMetadata() {
		return this.metadata != null && this.metadata.length > 0;
	}

	public String toString() {
		return "" + major + '.' + minor + '.' + patch
				+ (prerelease == null || prerelease.length == 0 ? "" : '-' + String.join(".", prerelease))
				+ (metadata == null || metadata.length == 0 ? "" : '+' + String.join(".", metadata));
	}

	public static Version valueOf(String s) {
		Version result = new Version();
		Pattern p = Pattern.compile(
				"^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?$");
		Matcher m = p.matcher(s);
		if (m.matches()) {
			result.major = Integer.parseInt(m.group(1));
			result.minor = Integer.parseInt(m.group(2));
			result.patch = Integer.parseInt(m.group(3));
			final String pre = m.group(4);
			result.prerelease = pre != null ? pre.split("\\.", -1) : (String[]) Array.newInstance(String.class, 0);
			final String meta = m.group(5);
			result.metadata = meta != null ? meta.split("\\.", -1) : (String[]) Array.newInstance(String.class, 0);
			return result;
		} else {
			return null;
		}
	}

	private int comparePrereleases(Version other) {
		Ordering<Integer> natint = Ordering.natural();
		Ordering<String> natstring = Ordering.natural();
		int lenghtToCompare = Math.min(this.prerelease.length, other.prerelease.length);
		for (int i = 0; i < lenghtToCompare; i++) {
			try {
				int x = Integer.parseInt(this.prerelease[i]);
				try {
					int y = Integer.parseInt(other.prerelease[i]);
					if (natint.compare(x, y) != 0) {
						return natint.compare(x, y);
					}
				} catch (NumberFormatException e) {
					return -1;
				}
			} catch (NumberFormatException e) {
				try {
					Integer.parseInt(other.prerelease[i]);
					return 1;
				} catch (NumberFormatException e2) {
					if (natstring.compare(this.prerelease[i], other.prerelease[i]) != 0) {
						return natstring.compare(this.prerelease[i], other.prerelease[i]);
					}
				}
			}
		}
		if (this.prerelease.length != other.prerelease.length) {
			return natint.compare(this.prerelease.length, other.prerelease.length);
		}
		return 0;
	}

	/** 
	 * Compares two versions following the SemVer specification. This is a partial order, so it may return 0 for a {@code Version} for which {@code equals} is false.
	 * 
	 * @param   other the {@code Version} to be compared.
	 * @return  the value {@code 0} if the argument Version is equal to
     *          this string; a value less than {@code 0} if this Version
     *          is less than the Version argument; and a
     *          value greater than {@code 0} if this Version is 
     *          greater than the Version argument.
	 */
	public int compareToAsSemVer(Version other) {
		if (other == null) {
			throw new NullPointerException();
		}
		Ordering<Integer> nat = Ordering.natural();
		if (nat.compare(this.major, other.major) != 0) {
			return nat.compare(this.major, other.major);
		} else if (nat.compare(this.minor, other.minor) != 0) {
			return nat.compare(this.minor, other.minor);
		} else if (nat.compare(this.patch, other.patch) != 0) {
			return nat.compare(this.patch, other.patch);
		} else if (!this.hasPrerelease() && other.hasPrerelease()) {
			return 1;
		} else if (this.hasPrerelease() && !other.hasPrerelease()) {
			return -1;
		} else if (this.hasPrerelease() && other.hasPrerelease()) {
			return comparePrereleases(other);
		} else {
			return 0;
		}
	}
	
	/**
	 * An total order on {@code Version}. Please use {@link Version#compareToAsSemVer(Version)} instead for SemVer comparison.
	 * @see Version#compareToAsSemVer(Version)
	 */
	public int compareTo(Version other) {
		int semverCompare = compareToAsSemVer(other);
		if(semverCompare == 0) {
			return Arrays.compare(metadata, other.metadata);
		} else {
			return semverCompare;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Version) {
			Version other = (Version) obj;
			return major == other.major && minor == other.minor && patch == other.patch && Arrays.equals(prerelease, other.prerelease) && Arrays.equals(metadata, other.metadata);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Integer.hashCode(major) + Integer.hashCode(minor) + Integer.hashCode(patch) + Arrays.hashCode(prerelease) + Arrays.hashCode(metadata);
	}
}
