package fr.irisa.atsyra.absystem.model.absystem.util;

import java.math.BigInteger;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSExpressionEval.TooManyStates;

public class GoalSizeInfo {
	
	private BigInteger nbInitState;
	private BigInteger nbFinalState;

	public GoalSizeInfo(BigInteger nbInitState, BigInteger nbFinalState) {
		this.nbInitState = nbInitState;
		this.nbFinalState = nbFinalState;
	}
	
	public String toString() {
		return "{\"InitState\": " + nbInitState + ", \"PotentialFinalState\": " + nbFinalState + "}";
	}

	public static GoalSizeInfo getAbsModelSizeInfo(Goal goal) {
		Set<AssetBasedSystem> absSet = ABSUtils.importsTransitiveClosure((AssetBasedSystem) EcoreUtil.getRootContainer(goal));
		ABSExpressionEval exprEval = new ABSExpressionEval(absSet);
		BigInteger nbInitStates;
		try {
			nbInitStates = exprEval.exprNbState(true, goal.getPrecondition());
		} catch (TooManyStates e) {
			nbInitStates = null;
		}
		BigInteger nbFinalStates;
		try {
			nbFinalStates = exprEval.exprNbState(false, goal.getPostcondition());
		} catch (TooManyStates e) {
			nbFinalStates = null;
		}
		return new GoalSizeInfo(nbInitStates, nbFinalStates);
	}
}
