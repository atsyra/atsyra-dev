/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectEList;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;

public class UnmodifiableEListCollector<T> implements Collector<T, List<T>, UnmodifiableEList<T>>{
	
	private InternalEObject container;
	private EStructuralFeature structuralFeature;

	public UnmodifiableEListCollector(InternalEObject container, EStructuralFeature structuralFeature) {
		this.container = container;
		this.structuralFeature = structuralFeature;
	}

	@Override
	public Supplier<List<T>> supplier() {
		return ArrayList::new;
	}

	@Override
	public BiConsumer<List<T>, T> accumulator() {
		return (list, asset) -> list.add(asset);
	}

	@Override
	public BinaryOperator<List<T>> combiner() {
		return (list1, list2) ->  {
			list1.addAll(list2);
			return list1;
		};
	}

	@Override
	public Function<List<T>, UnmodifiableEList<T>> finisher() {
		return list -> new UnmodifiableEList<>(container, structuralFeature, list.size() , list.toArray());
	}

	@Override
	public Set<Characteristics> characteristics() {
		return Set.of();
	}
	
}
