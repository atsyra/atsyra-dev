/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.util;

import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;

/**
 * <!-- begin-user-doc -->
 * A switch for PrimitiveDataType, based on their name.
 * It supports the calls {@link #doSwitch(PrimitiveDataType)} and {@link #doSwitch(String)}.
 * Users should override the caseXXX functions.
 * <!-- end-user-doc -->
 */
public class PrimitiveDataTypeSwitch<T> {
	
	public T doSwitch(PrimitiveDataType type) {
		return doSwitch(type.getName());
	}
	
	public T doSwitch(String typeName) {
		switch (typeName) {
		case "Boolean":
			return caseBoolean();
		case "Integer":
			return caseInteger();
		case "String":
			return caseString();
		case "Version":
			return caseVersion();
		default:
			return defaultCase(typeName);
		}
	}

	public T caseBoolean() {
		return null;
	}
	
	public T caseInteger() {
		return null;
	}
	
	public T caseString() {
		return null;
	}
	
	public T caseVersion() {
		return null;
	}
	
	public T defaultCase(String typeName) {
		return null;
	}
}
