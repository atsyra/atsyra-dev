/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.util;

import java.net.URL;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import org.eclipse.emf.common.util.ResourceLocator;

public class ResourceBundleLocator implements ResourceLocator {
	Module module;
	ResourceBundle localizedResourceBundle;
	ResourceBundle defaultResourceBundle;
	
	public ResourceBundleLocator(String resourcePath, Locale locale, Module module) {
		this.module = module;
		localizedResourceBundle = ResourceBundle.getBundle(resourcePath, locale, module);
		defaultResourceBundle = ResourceBundle.getBundle(resourcePath, module);
	}

	@Override
	public URL getBaseURL() {
		return module.getClassLoader().getResource("/");
	}

	@Override
	public Object getImage(String key) {
		return localizedResourceBundle.getObject(key);
	}

	@Override
	public String getString(String key) {
		return localizedResourceBundle.getString(key);
	}

	@Override
	public String getString(String key, boolean translate) {
		if(translate) {
			return localizedResourceBundle.getString(key);
		} else {
			return defaultResourceBundle.getString(key);
		}
	}

	@Override
	public String getString(String key, Object[] substitutions) {
		return MessageFormat.format(getString(key), substitutions);
	}

	@Override
	public String getString(String key, Object[] substitutions, boolean translate) {
		return MessageFormat.format(getString(key, translate), substitutions);
	}

}
