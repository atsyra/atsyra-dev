/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

public class ModelProviderExtensionPointHelper {

	public static String ENDPOINT_EXTENSIONPOINT_ID = "fr.irisa.atsyra.absystem.model.absystem_model_provider";
	public static String ENDPOINT_EXTENSIONPOINT_ID_PATH = "path";
	
	public static IConfigurationElement[] getEndPointExtensionConfiguration() {
		return Platform.getExtensionRegistry().getConfigurationElementsFor(ENDPOINT_EXTENSIONPOINT_ID);
	}
	
	
	public static List<String> getProvidedABSFilePathes() {
		List<String> result = new ArrayList<String>();
		IConfigurationElement[] e =Platform.getExtensionRegistry().getConfigurationElementsFor(ENDPOINT_EXTENSIONPOINT_ID);
		for(IConfigurationElement ce : e) {
			result.add("platform:/plugin/"+ce.getNamespaceIdentifier()+"/"+e[0].getAttribute(ENDPOINT_EXTENSIONPOINT_ID_PATH));
		}
		return result;
	}
}
