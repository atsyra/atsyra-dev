package fr.irisa.atsyra.absystem.model.absystem.util;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import fr.irisa.atsyra.absystem.model.absystem.AndExpression;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant;
import fr.irisa.atsyra.absystem.model.absystem.Collection;
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression;
import fr.irisa.atsyra.absystem.model.absystem.Expression;
import fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression;
import fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression;
import fr.irisa.atsyra.absystem.model.absystem.IntConstant;
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression;
import fr.irisa.atsyra.absystem.model.absystem.LambdaParameter;
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection;
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity;
import fr.irisa.atsyra.absystem.model.absystem.NotExpression;
import fr.irisa.atsyra.absystem.model.absystem.OrExpression;
import fr.irisa.atsyra.absystem.model.absystem.State;
import fr.irisa.atsyra.absystem.model.absystem.State.AssetMemberInstance;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSExpressionEval.TooManyStates;
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod;
import fr.irisa.atsyra.absystem.model.absystem.StringConstant;
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef;
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant;
import fr.irisa.atsyra.absystem.model.absystem.Version;
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant;

public class ABSExpressionEval {
	private Set<AssetBasedSystem> absSet;

	public ABSExpressionEval(Set<AssetBasedSystem> absSet) {
		this.absSet = absSet;
	}
	
	private class FeatureValues {
		private Multimap<AssetType, Asset> assets;
		private Set<String> strings;
		private Set<Integer> integers;
		private Set<Version> versions;
		
		public FeatureValues() {
			this.assets = HashMultimap.create(); 
			this.strings = new HashSet<>();
			this.integers = new HashSet<>();
			this.versions = new HashSet<>();
		}

		public Multimap<AssetType, Asset> getAssets() {
			return assets;
		}

		public Set<String> getStrings() {
			return strings;
		}

		public Set<Integer> getIntegers() {
			return integers;
		}

		public Set<Version> getVersions() {
			return versions;
		}
	}
	
	private class FeatureValuesVisitor {
		 public void visit(Set<AssetBasedSystem> models, FeatureValues featureValues) {
			 for(AssetBasedSystem model : models) {
				 visit(model, featureValues);
			 }
		 }
		 
		 private void visit(AssetBasedSystem model, FeatureValues featureValues) {
			 Iterable<EObject> iterableAllContents = model::eAllContents;
			 for (EObject eobj : iterableAllContents) {
				 if (eobj instanceof Asset) {
					 Asset asset = (Asset) eobj;
					 visitType(asset.getAssetType(), asset, featureValues.getAssets(), new HashSet<>());
				 }
				 if (eobj instanceof IntConstant) {
					 IntConstant intConstant = (IntConstant) eobj;
					 featureValues.getIntegers().add(intConstant.getValue());
				 }
				 if (eobj instanceof StringConstant) {
					 StringConstant stringConstant = (StringConstant) eobj;
					 featureValues.getStrings().add(stringConstant.getValue());
				 }
				 if (eobj instanceof VersionConstant) {
					 VersionConstant versionConstant = (VersionConstant) eobj;
					 featureValues.getVersions().add(versionConstant.getValue());
				 }
			 }
		 }

		private void visitType(AssetType assetType, Asset asset, Multimap<AssetType, Asset> assetsAccumulator, Set<AssetType> alreadyVisited) {
			assetsAccumulator.put(assetType, asset);
			alreadyVisited.add(assetType);
			for(AssetType parent: assetType.getExtends()) {
				if(!alreadyVisited.contains(parent)) {
					visitType(parent, asset, assetsAccumulator, alreadyVisited);
				}
			}
		}
	}
	
	private class HasDefaultVisitor {
		public void visit(Set<AssetBasedSystem> models, Set<AssetTypeFeature> result) {
			for(AssetBasedSystem model : models) {
				 visit(model, result);
			 }
		}
		
		private void visit(AssetBasedSystem model, Set<AssetTypeFeature> result) {
			 Iterable<EObject> iterableAllContents = model::eAllContents;
			 for (EObject eobj : iterableAllContents) {
				 if (eobj instanceof AssetTypeFeature) {
					 AssetTypeFeature feature = (AssetTypeFeature) eobj;
					 if(feature.isHasDefault()) {
						 result.add(feature);
					 }
				 }
			 }
		 }
	}
	
	public class TooManyStates extends Exception {
		private static final long serialVersionUID = -1020320178325124734L;
		
	}
	
	private class StateEnumerator {
		private final Set<AssetTypeFeature> hasDefault;
		private FeatureValues featureValues;
		private Set<AssetTypeFeature> referencedFeatures;
		
		public StateEnumerator(FeatureValues featureValues, Set<AssetTypeFeature> referencedFeatures, Set<AssetTypeFeature> hasDefault) {
			this.featureValues = featureValues;
			this.referencedFeatures = referencedFeatures;
			this.hasDefault = hasDefault;
		}
		
		public Set<AssetTypeFeature> getAllDynamicFeatures(Set<AssetBasedSystem> models) {
			Set<AssetTypeFeature> result = new HashSet<>();
			for(AssetBasedSystem model : models) {
				result.addAll(getAllDynamicFeatures(model));
			 }
			return result;
		}
		
		private Set<AssetTypeFeature> getAllDynamicFeatures(AssetBasedSystem model) {
			Set<AssetTypeFeature> result = new HashSet<>();
			Iterable<EObject> iterableAllContents = model::eAllContents;
			for (EObject eobj : iterableAllContents) {
				if (eobj instanceof AssetTypeFeature) {
					AssetTypeFeature feature = (AssetTypeFeature) eobj;
					if(feature.eContainer() instanceof AssetTypeAspect) { //meaning feature is dynamic
						 result.add(feature);
					}
				}
			}
			return result;
		}
		
		public Set<State> instanciate(Set<AssetTypeFeature> features, Set<State> partials) throws TooManyStates {
			for (AssetTypeFeature feature : features) {
				if(referencedFeatures.contains(feature)) {
					for(Asset asset : featureValues.getAssets().get(ABSUtils.getContainerType(feature))) {
						partials = instanciate(feature, asset, partials);
					}
				}
			}
			return partials;
		}
		
		private Set<State> instanciate(AssetTypeFeature feature, Asset container, Set<State> partials) throws TooManyStates {
			if(feature instanceof AssetTypeAttribute) {
				return instanciate((AssetTypeAttribute) feature, container, partials);
			} else if(feature instanceof AssetTypeReference) {
				return instanciate((AssetTypeReference) feature, container, partials);
			} else
				throw new RuntimeException("feature must be either attribute or reference");
		}
		
		private Set<State> instanciate(AssetTypeReference feature, Asset container, Set<State> partials) throws TooManyStates {
			if (partials.size() > 1000) {
				throw new TooManyStates();
			}
			Set<State> result = new HashSet<>();
			for(State partial : partials) {
				result.addAll(getPrecisedStates(partial, container, feature, featureValues.getAssets().get(feature.getPropertyType())));
			}
			return result;
		}
		
		private Set<State> getPrecisedStates(State partial, Asset container, AssetTypeFeature feature, java.util.Collection<?> values) {
			Set<State> result = new HashSet<>();
			switch(feature.getMultiplicity()) {
			case ONE:
				for (Object value : values) {
					result.add(getPrecisedState(partial, container, feature, value));
				}
				break;
			case ONE_OR_MANY:
				for (Set<?> value : powerSet(Set.copyOf(values))) {
					if(!value.isEmpty()) {
						result.add(getStatePrecisedBySet(partial, container, feature, value));
					}
				}
				break;
			case ZERO_OR_MANY:
				for (Set<?> value : powerSet(Set.copyOf(values))) {
					result.add(getStatePrecisedBySet(partial, container, feature, value));
				}
				break;
			case ZERO_OR_ONE:
				result.add(getPrecisedState(partial, container, feature, null));
				for (Object value : values) {
					result.add(getPrecisedState(partial, container, feature, value));
				}
				break;
			default:
				break;
			}
			return result;
		}
		
		private <T> Set<Set<T>> powerSet(Set<T> set) {
			if(set.isEmpty()) {
				return Set.of(Set.of());
			} else {
				T head = set.iterator().next();
				Set<T> tail = new HashSet<>(set);
				tail.remove(head);
				Set<Set<T>> result = new HashSet<>();
				for (Set<T> tailSubSet : powerSet(tail)) {
					// case not contains head
					result.add(tailSubSet);
					// case contains head
					Set<T> withHead = new HashSet<>(tailSubSet);
					withHead.add(head);
					result.add(withHead);
				}
				return result;
			}
		}
		
		private State getPrecisedState(State partial, Asset container, AssetTypeFeature feature, Object value) {
			AssetMemberInstance assetMemberInstance = new AssetMemberInstance(container, feature, value);
			State precisedState = new State(partial);
			assetMemberInstance.addToState(precisedState);
			return precisedState;
		}
		
		private State getStatePrecisedBySet(State partial, Asset container, AssetTypeFeature feature, Set<?> values) {
			State precisedState = new State(partial);
			for (Object value : values) {
				precisedState.get(container).addInCollectionGeneric(value, feature);
			}
			return precisedState;
		}
		
		private Set<State> instanciate(AssetTypeAttribute feature, Asset container, Set<State> partials) throws TooManyStates {
			if (partials.size() > 1000) {
				throw new TooManyStates();
			}
			Set<State> result = new HashSet<>();
			for(State partial : partials) {
				if (feature.getAttributeType() instanceof EnumDataType) {
					EnumDataType attributeType = (EnumDataType) feature.getAttributeType();
					for (EnumLiteral value : attributeType.getEnumLiteral()) {
						result.add(getPrecisedState(partial, container, feature, value));
					}
				} else if (Objects.equals(feature.getAttributeType().getName(), "String")) {
					for (String value : featureValues.getStrings()) {
						result.add(getPrecisedState(partial, container, feature, value));
					}
				} else if (Objects.equals(feature.getAttributeType().getName(), "Integer")) {
					for (Integer value : featureValues.getIntegers()) {
						result.add(getPrecisedState(partial, container, feature, value));
					}
				} else if (Objects.equals(feature.getAttributeType().getName(), "Version")) {
					for (Version value : featureValues.getVersions()) {
						result.add(getPrecisedState(partial, container, feature, value));
					}
				} else if (Objects.equals(feature.getAttributeType().getName(), "Boolean")) {
					result.add(getPrecisedState(partial, container, feature, true));
					result.add(getPrecisedState(partial, container, feature, false));
				}
			}
			return result;
		}
		
		public BigInteger countForNonReferenced(Set<AssetTypeFeature> features) {
			BigInteger count = BigInteger.ONE;
			for (AssetTypeFeature feature : features) {
				if(!referencedFeatures.contains(feature) && !hasDefault.contains(feature)) {
					count = count.multiply(count(feature));
				}
			}
			return count;
		}
		
		private BigInteger count(AssetTypeFeature feature) {
			switch (feature.getMultiplicity()) {
			case ONE:
				return countSingle(feature);
			case ONE_OR_MANY:
				return countMany(feature).subtract(BigInteger.ONE);
			case ZERO_OR_MANY:
				return countMany(feature);
			case ZERO_OR_ONE:
				return countSingle(feature).add(BigInteger.ONE);
			default:
				return BigInteger.ZERO;
			}
		}
		
		private BigInteger countSingle(AssetTypeFeature feature) {
			if(feature instanceof AssetTypeReference) {
				AssetTypeReference reference = (AssetTypeReference) feature;
				return BigInteger.valueOf(featureValues.getAssets().get(reference.getPropertyType()).size());
			} else if (feature instanceof AssetTypeAttribute) {
				AssetTypeAttribute attribute = (AssetTypeAttribute) feature;
				if (attribute.getAttributeType() instanceof EnumDataType) {
					EnumDataType attributeType = (EnumDataType) attribute.getAttributeType();
					return BigInteger.valueOf(attributeType.getEnumLiteral().size());
				} else if (Objects.equals(attribute.getAttributeType().getName(), "String")) {
					return BigInteger.valueOf(featureValues.getStrings().size());
				} else if (Objects.equals(attribute.getAttributeType().getName(), "Integer")) {
					return BigInteger.valueOf(featureValues.getIntegers().size());
				} else if (Objects.equals(attribute.getAttributeType().getName(), "Version")) {
					return BigInteger.valueOf(featureValues.getVersions().size());
				} else if (Objects.equals(attribute.getAttributeType().getName(), "Boolean")) {
					return BigInteger.TWO;
				}
			}
			return BigInteger.ZERO;
		}
		
		private BigInteger countMany(AssetTypeFeature feature) {
			return BigInteger.TWO.pow(countSingle(feature).intValueExact());
		}
	}
	
	private StateEnumerator getStateEnumerator(boolean withDefault, Set<AssetTypeFeature> referencedFeatures) {
		FeatureValues featureValues = new FeatureValues();
		new FeatureValuesVisitor().visit(absSet, featureValues);
		Set<AssetTypeFeature> isDefault = new HashSet<>();
		if (withDefault) {
			new HasDefaultVisitor().visit(absSet, isDefault);
			isDefault.removeAll(referencedFeatures);
		}
		return new StateEnumerator(featureValues, referencedFeatures, isDefault);
	}
	
	private Set<State> enumerateStates(StateEnumerator stateEnumerator, Set<AssetTypeFeature> dynamicFeatures) throws TooManyStates {
		Set<State> partials = new HashSet<>();
		partials.add(new State());
		return stateEnumerator.instanciate(dynamicFeatures, partials);
	}
	
	private Set<AssetTypeFeature> getReferencedFeatures(Expression expr) {
		Set<AssetTypeFeature> result = new HashSet<>();
		TreeIterator<EObject> exprAllContents = expr.eAllContents();
		while(exprAllContents.hasNext()) {
			EObject next = exprAllContents.next();
			if (next instanceof MemberSelection) {
				MemberSelection memberSelection = (MemberSelection) next;
				if (memberSelection.getMember() instanceof AssetTypeFeature) {
					result.add((AssetTypeFeature) memberSelection.getMember());
				}
			}
		}
		return result;
	}
	
	public BigInteger nbState(boolean withDefault) {
		StateEnumerator stateEnumerator = getStateEnumerator(withDefault, Set.of());
		Set<AssetTypeFeature> dynamicFeatures = stateEnumerator.getAllDynamicFeatures(absSet);
		return stateEnumerator.countForNonReferenced(dynamicFeatures);
	}
	
	public BigInteger exprNbState(boolean withDefault, Expression expr) throws TooManyStates {
		StateEnumerator stateEnumerator = getStateEnumerator(withDefault, getReferencedFeatures(expr));
		Set<AssetTypeFeature> dynamicFeatures = stateEnumerator.getAllDynamicFeatures(absSet);
		Set<State> validPartialStates = enumerateStates(stateEnumerator, dynamicFeatures).stream().filter(state -> evalExprInState(expr, state)).collect(Collectors.toSet());
		return stateEnumerator.countForNonReferenced(dynamicFeatures).multiply(BigInteger.valueOf(validPartialStates.size())) ;
	}
	
	private class ExpressionEvaluator extends AbsystemSwitch<Object> {
		private final State state;
		private Map<LambdaParameter, java.util.Collection<?>> lambdaContext;
		public ExpressionEvaluator(State state) {
			this.state = state;
			lambdaContext = new HashMap<>();
		}
		@Override
		public Object caseAndExpression(AndExpression object) {
			Object lhs = doSwitch(object.getLhs());
			Object rhs = doSwitch(object.getRhs());
			if (lhs == null || rhs == null) {
				return null;
			}
			return (boolean) lhs && (boolean) rhs;
		}
		@Override
		public Object caseOrExpression(OrExpression object) {
			Object lhs = doSwitch(object.getLhs());
			Object rhs = doSwitch(object.getRhs());
			if (lhs == null || rhs == null) {
				return null;
			}
			return (boolean) lhs || (boolean) rhs;
		}
		@Override
		public Object caseImpliesExpression(ImpliesExpression object) {
			Object lhs = doSwitch(object.getLhs());
			Object rhs = doSwitch(object.getRhs());
			if (lhs == null || rhs == null) {
				return null;
			}
			return !(boolean) lhs || (boolean) rhs;
		}
		@Override
		public Object caseNotExpression(NotExpression object) {
			Object expr = doSwitch(object.getExpression());
			if (expr == null) {
				return null;
			}
			return !(boolean) expr;
		}
		@Override
		public Object caseBooleanConstant(BooleanConstant object) {
			return Objects.equals(object.getValue(), "true");
		}
		@Override
		public Object caseIntConstant(IntConstant object) {
			return object.getValue();
		}
		@Override
		public Object caseStringConstant(StringConstant object) {
			return object.getValue();
		}
		@Override
		public Object caseVersionConstant(VersionConstant object) {
			return object.getValue();
		}
		@Override
		public Object caseUndefinedConstant(UndefinedConstant object) {
			return null;
		}
		@Override
		public Object caseCollection(Collection object) {
			return Set.of(object.getElements());
		}
		@Override
		public Object caseEqualityComparisonExpression(EqualityComparisonExpression object) {
			switch (object.getOp()) {
			case "==":
				return Objects.equals(doSwitch(object.getLhs()) , doSwitch(object.getRhs()));
			case "!=":
				return !Objects.equals(doSwitch(object.getLhs()) , doSwitch(object.getRhs()));
			default:
				return false;
			}
			
		}
		@Override
		public Object caseInequalityComparisonExpression(InequalityComparisonExpression object) {
			Object lhs = doSwitch(object.getLhs());
			Object rhs = doSwitch(object.getRhs());
			if (lhs instanceof Integer && rhs instanceof Integer) {
				switch (object.getOp()) {
				case "<":
					return (int) lhs < (int) rhs;
				case "<=":
					return (int) lhs <= (int) rhs;
				case ">":
					return (int) lhs > (int) rhs;
				case ">=":
					return (int) lhs >= (int) rhs;
				default:
					return false;
				}
				
			} else if (lhs instanceof String && rhs instanceof String) {
				switch (object.getOp()) {
				case "<":
					return ((String) lhs).compareTo((String) rhs) < 0;
				case "<=":
					return ((String) lhs).compareTo((String) rhs) <= 0;
				case ">":
					return ((String) lhs).compareTo((String) rhs) > 0;
				case ">=":
					return ((String) lhs).compareTo((String) rhs) >= 0;
				default:
					return false;
				}
			} else if(lhs instanceof Version && rhs instanceof Version){
				switch (object.getOp()) {
				case "<":
					return ((Version) lhs).compareTo((Version) rhs) < 0;
				case "<=":
					return ((Version) lhs).compareTo((Version) rhs) <= 0;
				case ">":
					return ((Version) lhs).compareTo((Version) rhs) > 0;
				case ">=":
					return ((Version) lhs).compareTo((Version) rhs) >= 0;
				default:
					return false;
				}
			} else {
				return false;
			}
		}
		@Override
		public Object caseSymbolRef(SymbolRef object) {
			return object.getSymbol();
		}
		@Override
		public Object caseMemberSelection(MemberSelection object) {
			Object receiver = doSwitch(object.getReceiver());
			if(object.getMember() instanceof AssetTypeFeature) {
				if(receiver instanceof Asset) {
					Object value = state.get((Asset) receiver).getValue((AssetTypeFeature) object.getMember()); 
					if (value instanceof Optional<?>) {
						value = ((Optional<?>) value).orElse(null);
					}
					return value;
				} else if(receiver instanceof java.util.Collection<?>) { // For LambdaParameter handling
					return ((java.util.Collection<?>) receiver).stream().map(Asset.class::cast).map(asset -> {
						Object value = state.get(asset).getValue((AssetTypeFeature) object.getMember()); 
						if (value instanceof Optional<?>) {
							return ((Optional<?>) value).orElse(null);
						}
						return value;
					}).collect(Collectors.toSet());
				}
			} else if (object.getMember() instanceof StaticMethod) {
				List<Object> args = object.getArgs().stream().map(this::doSwitch).collect(Collectors.toList());
				return new StaticMethodSwitch<>() {
					public Object caseContains() {
						java.util.Collection<?> recCollection = (java.util.Collection<?>) receiver;
						return recCollection.contains(args.get(0));
					}
					public Object caseContainsAll() {
						java.util.Collection<?> recCollection = (java.util.Collection<?>) receiver;
						return recCollection.containsAll(args);
					}
					public Object caseContainsAny() {
						java.util.Collection<?> recCollection = (java.util.Collection<?>) receiver;
						return recCollection.stream().anyMatch(args::contains);
					}
					public Object caseFilter() {
						java.util.Collection<?> recCollection = (java.util.Collection<?>) receiver;
						LambdaExpression lambda = (LambdaExpression) object.getArgs().get(0);
						lambdaContext.put(lambda.getLambdaParameter(), recCollection);
						return ExpressionEvaluator.this.doSwitch(lambda.getBody());
					}
					public Object caseIsEmpty() {
						java.util.Collection<?> recCollection = (java.util.Collection<?>) receiver;
						return recCollection.isEmpty();
					}
				}.doSwitch((StaticMethod) object.getMember());
			}
			return null;
		}
		
		@Override
		public Object caseLambdaExpression(LambdaExpression object) {
			// handled directly in filter case, nothing to do
			// Should return null to not try the evaluation before the lambaContext is updated
			return null;
		}
		
		@Override
		public Object caseLambdaParameter(LambdaParameter object) {
			return lambdaContext.get(object);
		}
		
		
	}
	
	public boolean evalExprInState(Expression expr, State state) {
		Object eval = new ExpressionEvaluator(state).doSwitch(expr);
		return eval != null && (boolean) eval;
	}
}
