/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.util;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ResourceBundleLocatorProvider {
	private static final ConcurrentMap<CacheKey, ResourceBundleLocator> cache
    = new ConcurrentHashMap<>();
	private static final ReferenceQueue<Module> referenceQueue = new ReferenceQueue<>();
	
	private static final class CacheKey {
		private String resourcePath; 
		private Locale locale; 
		private CacheKeyWeakReference<Module> module;
		
		public CacheKey(String resourcePath, Locale locale, Module module) {
			this.locale = locale;
			this.resourcePath = resourcePath;
			this.module = new CacheKeyWeakReference<>(Objects.requireNonNull(module), referenceQueue, this);
		}
		public String getResourcePath() {
			return resourcePath;
		}
		public Locale getLocale() {
			return locale;
		}
		public Module getModule() {
			return module.get();
		}
	}
	
	private static final class CacheKeyWeakReference<T> extends WeakReference<T> {
		private final CacheKey parent;

		public CacheKeyWeakReference(T referent, ReferenceQueue<? super T> q, CacheKey parent) {
			super(referent, q);
			this.parent = parent;
		}
		
		public CacheKey getParent() {
			return parent;
		}
	}
	
	private ResourceBundleLocatorProvider() {
		throw new IllegalStateException();
	}
	
	public static ResourceBundleLocator getResourceBundleLocator(String resourcePath, Locale locale, Module module) {
		cleanUp();
		CacheKey key = new CacheKey(resourcePath, locale, module);
		return cache.computeIfAbsent(key, akey -> new ResourceBundleLocator(akey.getResourcePath(), akey.getLocale(), akey.getModule()));
	}
	
	@SuppressWarnings("unchecked")
	protected static void cleanUp() {
		CacheKeyWeakReference<Module> ref;
        while ((ref = (CacheKeyWeakReference<Module>) referenceQueue.poll()) != null) {
            cache.remove(ref.getParent());
        }
	}
}
