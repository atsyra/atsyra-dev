/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.util;

import fr.irisa.atsyra.absystem.model.absystem.StaticMethod;

/**
 * <!-- begin-user-doc -->
 * A switch for StaticMethod, based on their name.
 * It supports the calls {@link #doSwitch(StaticMethod)} and {@link #doSwitch(String)}.
 * Users should override the caseXXX functions.
 * <!-- end-user-doc -->
 */
public class StaticMethodSwitch<T> {
	
	public T doSwitch(StaticMethod type) {
		return doSwitch(type.getName());
	}
	
	public T doSwitch(String typeName) {
		switch (typeName) {
		case "contains":
			return caseContains();
		case "containsAll":
			return caseContainsAll();
		case "containsAny":
			return caseContainsAny();
		case "filter":
			return caseFilter();
		case "isEmpty":
			return caseIsEmpty();
		default:
			return defaultCase(typeName);
		}
	}
	
	public T caseIsEmpty() {
		return null;
	}

	public T caseFilter() {
		return null;
	}

	public T caseContainsAny() {
		return null;
	}

	public T caseContainsAll() {
		return null;
	}

	public T caseContains() {
		return null;
	}

	public T defaultCase(String typeName) {
		return null;
	}
}
