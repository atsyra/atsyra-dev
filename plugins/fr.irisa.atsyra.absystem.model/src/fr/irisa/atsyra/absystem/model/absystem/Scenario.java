/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.xbase.lib.Pair;

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ABSInterpreterUtils;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurenceArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmFactory;

public class Scenario {
	List<GuardOccurence> actions;
	List<List<State>> intermediateStates;

	public Scenario() {
		actions = new ArrayList<>();
		intermediateStates = new ArrayList<>();
		intermediateStates.add(new ArrayList<>());
	}

	protected Scenario(List<GuardOccurence> actionList) {
		actions = new ArrayList<>(actionList);
		intermediateStates = new ArrayList<>();
		intermediateStates.add(new ArrayList<>());
		for(int i = 0; i < actionList.size(); i++) {
			intermediateStates.add(new ArrayList<>());
		}
	}

	public static Scenario fromSteps(List<Pair<GuardedAction, List<Asset>>> steps) {
		return new Scenario(steps.stream().map(step -> {
			GuardOccurence go = Interpreter_vmFactory.eINSTANCE.createGuardOccurence();
			go.setGuard(step.getKey());
			go.getGuardOccurenceArguments().addAll(step.getValue().stream().map(param -> {
				AssetArgument goa = Interpreter_vmFactory.eINSTANCE.createAssetArgument();
				goa.setAsset(param);
				return goa;
			}).collect(Collectors.toList()));
			return go;
		}).collect(Collectors.toList()));
	}

	public boolean sameAs(Scenario other) {
		ListIterator<GuardOccurence> it1 = actions.listIterator();
		ListIterator<GuardOccurence> it2 = other.actions.listIterator();
		boolean same = true;
		while (it1.hasNext()) {
			if (!it2.hasNext()) {
				same = false;
				break;
			}
			GuardOccurence occ1 = it1.next();
			GuardOccurence occ2 = it2.next();
			if (!EcoreUtil.equals(occ1, occ2)) {
				same = false;
			}
		}
		return same;
	}

	public List<GuardOccurence> getActions() {
		return Collections.unmodifiableList(actions);
	}
	
	public void addAction(GuardOccurence action) {
		actions.add(action);
		intermediateStates.add(new ArrayList<>());
	}
	
	public List<State> getInitialStates() {
		return intermediateStates.get(0);
	}
	
	public List<State> getFinalStates() {
		return intermediateStates.get(intermediateStates.size()-1);
	}
	
	public void setInitialStates(List<State> states) {
		setIntermediateStates(0, states);
	}
	
	public void setFinalStates(List<State> states) {
		setIntermediateStates(intermediateStates.size()-1, states);
	}
	
	public void setIntermediateStates(int index, List<State> states) {
		intermediateStates.set(index, states);
	}
	
	public List<State> getIntermediateStates(int index) {
		return intermediateStates.get(index);
	}

	public int size() {
		return actions.size();
	}
	
	public String toReadableString() {
		StringBuilder builder = new StringBuilder();
		for(GuardOccurence go : actions) {
			if(builder.length() > 0) {
				builder.append(", ");
			}
			builder.append(ABSInterpreterUtils.toReadableString(go));
		}
		return builder.toString();
	}
}
