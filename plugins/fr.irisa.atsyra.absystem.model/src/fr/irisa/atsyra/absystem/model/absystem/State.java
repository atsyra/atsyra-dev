/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.UnaryOperator;

import org.eclipse.emf.ecore.EObject;

public class State {
	Map<Asset, AssetInstance> valuation;

	public State() {
		valuation = new HashMap<>();
	}
	
	public State(State other) {
		this.valuation = new HashMap<>(other.valuation);
	}

	public AssetInstance get(Asset asset) {
		if (!valuation.containsKey(asset)) {
			valuation.put(asset, new AssetInstance());
		}
		return valuation.get(asset);
	}
	
	public Map<Asset, AssetInstance> getValuation() {
		return valuation;
	}
	
	public State transform(UnaryOperator<EObject> transformation) {
		State result = new State();
		for (Entry<Asset, AssetInstance> entry : getValuation().entrySet()) {
			Asset sourceAsset = entry.getKey();
			AssetInstance sourceInstance = entry.getValue();
			Asset transformedAsset = (Asset) transformation.apply(sourceAsset);
			AssetInstance transformedAssetInstance = sourceInstance.transform(transformation);
			result.getValuation().put(transformedAsset, transformedAssetInstance);
		}
		return result;
	}
	
	public static class AssetMemberInstance {
		Asset asset;

		AssetTypeFeature feature;

		Object value;

		public AssetMemberInstance(Asset asset, AssetTypeFeature feature) {
			this.asset = asset;
			this.feature = feature;
			this.value = null;
		}

		public AssetMemberInstance(Asset asset, AssetTypeFeature feature, Object value) {
			this.asset = asset;
			this.feature = feature;
			this.value = value;
		}

		public void addToState(State state) {
			if (feature.getMultiplicity().equals(Multiplicity.ONE)
					|| feature.getMultiplicity().equals(Multiplicity.ZERO_OR_ONE)) {
				if (value == null) {
					state.get(asset).addUndefinedFeature(feature);
				} else if (value instanceof Boolean) {
					state.get(asset).addBooleanAttribute((Boolean) value, (AssetTypeAttribute) feature);
				} else if (value instanceof Integer) {
					state.get(asset).addIntegerAttribute((Integer) value, (AssetTypeAttribute) feature);
				} else if (value instanceof String) {
					state.get(asset).addStringAttribute((String) value, (AssetTypeAttribute) feature);
				} else if (value instanceof Version) {
					state.get(asset).addVersionAttribute((Version) value, (AssetTypeAttribute) feature);
				} else if (value instanceof EnumLiteral) {
					state.get(asset).addEnumAttribute((EnumLiteral) value, (AssetTypeAttribute) feature);
				} else if (value instanceof Asset) {
					state.get(asset).addReference((Asset) value, (AssetTypeReference) feature);
				}
			} else {
				state.get(asset).addInCollectionGeneric(value, feature);
			}
		}
	}
}
