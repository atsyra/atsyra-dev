/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.model.absystem.util;

import com.google.common.collect.Iterators;

import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.Action;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;
import fr.irisa.atsyra.absystem.model.absystem.Collection;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.Expression;
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter;
import fr.irisa.atsyra.absystem.model.absystem.LambdaAction;
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression;
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection;
import fr.irisa.atsyra.absystem.model.absystem.NotExpression;
import fr.irisa.atsyra.absystem.model.absystem.OrExpression;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod;
import fr.irisa.atsyra.absystem.model.absystem.StringConstant;
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef;
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant;
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant;

public class ABSExpressionTypeProvider {
	PrimitiveDataType booleanType;
	PrimitiveDataType integerType;
	PrimitiveDataType stringType;
	PrimitiveDataType versionType;
	
	public ABSExpressionTypeProvider(AssetBasedSystem model) {
		Iterators.filter(model.eResource().getResourceSet().getAllContents(), PrimitiveDataType.class).forEachRemaining(type -> {
			switch (type.getName()) {
			case AbsystemValidator.PRIMITIVEDATATYPE_BOOLEAN_NAME:
				booleanType = type;
				break;
			case AbsystemValidator.PRIMITIVEDATATYPE_INTEGER_NAME:
				integerType = type;
				break;
			case AbsystemValidator.PRIMITIVEDATATYPE_STRING_NAME:
				stringType = type;
				break;
			case AbsystemValidator.PRIMITIVEDATATYPE_VERSION_NAME:
				versionType = type;
				break;
			default:
				break;
			}
		});
		if(booleanType == null) {
			booleanType = AbsystemFactory.eINSTANCE.createPrimitiveDataType();
			booleanType.setName(AbsystemValidator.PRIMITIVEDATATYPE_BOOLEAN_NAME);
		}
		if(integerType == null) {
			integerType = AbsystemFactory.eINSTANCE.createPrimitiveDataType();
			integerType.setName(AbsystemValidator.PRIMITIVEDATATYPE_INTEGER_NAME);
		}
		if(stringType == null) {
			stringType = AbsystemFactory.eINSTANCE.createPrimitiveDataType();
			stringType.setName(AbsystemValidator.PRIMITIVEDATATYPE_STRING_NAME);
		}
		if(versionType == null) {
			versionType = AbsystemFactory.eINSTANCE.createPrimitiveDataType();
			versionType.setName(AbsystemValidator.PRIMITIVEDATATYPE_VERSION_NAME);
		}
	}
	
	AbsystemSwitch<ExpressionType> typeSwitch = new AbsystemSwitch<ExpressionType>() {
		@Override
		public ExpressionType caseAndExpression(fr.irisa.atsyra.absystem.model.absystem.AndExpression object) {
			return ExpressionType.simple(booleanType);
		}
		@Override
		public ExpressionType caseAsset(Asset object) {
			return ExpressionType.simple(object.getAssetType());
		}
		@Override
		public ExpressionType caseAssetTypeAttribute(AssetTypeAttribute object) {
			if(ABSUtils.isMany(object.getMultiplicity())) {
				return ExpressionType.collection(object.getAttributeType());
			} else {
				return ExpressionType.simple(object.getAttributeType());
			}
		}
		@Override
		public ExpressionType caseAssetTypeReference(AssetTypeReference object) {
			if(ABSUtils.isMany(object.getMultiplicity())) {
				return ExpressionType.collection(object.getPropertyType());
			} else {
				return ExpressionType.simple(object.getPropertyType());
			}
		}
		@Override
		public ExpressionType caseBooleanConstant(fr.irisa.atsyra.absystem.model.absystem.BooleanConstant object) {
			return ExpressionType.simple(booleanType);
		}
		@Override
		public ExpressionType caseEnumConstant(fr.irisa.atsyra.absystem.model.absystem.EnumConstant object) {
			return ExpressionType.simple(object.getValue().eContainer());
		}
		@Override
		public ExpressionType caseEnumLiteral(EnumLiteral object) {
			return ExpressionType.simple(object.eContainer());
		}
		@Override
		public ExpressionType caseEqualityComparisonExpression(fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression object) {
			return ExpressionType.simple(booleanType);
		}
		@Override
		public ExpressionType caseGuardParameter(GuardParameter object) {
			return ExpressionType.simple(object.getParameterType());
		}
		@Override
		public ExpressionType caseImpliesExpression(fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression object) {
			return ExpressionType.simple(booleanType);
		}
		@Override
		public ExpressionType caseInequalityComparisonExpression(fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression object) {
			return ExpressionType.simple(booleanType);
		}
		@Override
		public ExpressionType caseIntConstant(fr.irisa.atsyra.absystem.model.absystem.IntConstant object) {
			return ExpressionType.simple(integerType);
		}
		@Override
		public ExpressionType caseLambdaAction(LambdaAction object) {
			return doSwitch(object.getLambdaParameter());
		}
		@Override
		public ExpressionType caseLambdaExpression(LambdaExpression object) {
			return doSwitch(object.getLambdaParameter());
		}
		@Override
		public ExpressionType caseLambdaParameter(fr.irisa.atsyra.absystem.model.absystem.LambdaParameter object) {
			if(object.getParameterType() != null) {
				return ExpressionType.simple(object.getParameterType());
			} else {
				ExpressionType receiverType;
				if(object.eContainer() instanceof LambdaExpression) {
					 receiverType = doSwitch(((MemberSelection)object.eContainer().eContainer()).getReceiver());
				} else if(object.eContainer() instanceof LambdaAction) {
					 receiverType = doSwitch(((Action)object.eContainer().eContainer()).getTarget());
				} else {
					receiverType = null;
				}
				if(receiverType != null) {
					return ExpressionType.simple(receiverType.getType());
				} else {
					return null;
				}
			}
			
		}
		@Override
		public ExpressionType caseMemberSelection(MemberSelection object) {
			if (object.getMember() instanceof StaticMethod) {
				return typeForStaticMethodCall((StaticMethod) object.getMember(), object.getReceiver());
			} else if (object.getMember() instanceof AssetTypeFeature) {
				ExpressionType result = doSwitch(object.getMember());
				if(result != null) {
					result.setMutable(true);
				}
				return result;
			}
			return null;
		}
		private ExpressionType typeForStaticMethodCall(StaticMethod method, Expression receiver) {
			switch (method.getName()) {
			case AbsystemValidator.STATICMETHOD_CONTAINS_NAME:
			case AbsystemValidator.STATICMETHOD_CONTAINSALL_NAME:
			case AbsystemValidator.STATICMETHOD_CONTAINSANY_NAME:
				return ExpressionType.simple(booleanType);
			case AbsystemValidator.STATICMETHOD_FILTER_NAME:
				ExpressionType receiverType = doSwitch(receiver);
				if(receiverType != null) {
					return ExpressionType.collection(receiverType.getType());
				} else {
					return null;
				}
			default:
				return null;
			}
		}
		@Override
		public ExpressionType caseNotExpression(NotExpression object) {
			return ExpressionType.simple(booleanType);
		}
		@Override
		public ExpressionType caseOrExpression(OrExpression object) {
			return ExpressionType.simple(booleanType);
		}
		@Override
		public ExpressionType caseStringConstant(StringConstant object) {
			return ExpressionType.simple(stringType);
		}
		@Override
		public ExpressionType caseSymbolRef(SymbolRef object) {
			if(object.getSymbol() == null) {
				return null;
			}
			return doSwitch(object.getSymbol());
		}
		@Override
		public ExpressionType caseUndefinedConstant(UndefinedConstant object) {
			return ExpressionType.undefined();
		}
		@Override
		public ExpressionType caseVersionConstant(VersionConstant object) {
			return ExpressionType.simple(versionType);
		}
		@Override
		public ExpressionType caseCollection(Collection object) {
			if(object.getElements().isEmpty()) {
				return ExpressionType.emptyCollection();
			} else {
				ExpressionType elementType = doSwitch(object.getElements().get(0));
				return ExpressionType.collection(elementType.getType());
			}
		}
	};
	
	public ExpressionType getTypeOf(Expression expression) {
		if(expression == null) {
			return null;
		}
		return typeSwitch.doSwitch(expression);
	}
	
	public ExpressionType getTypeOf(LambdaExpression lambdaExpression) {
		if(lambdaExpression == null) {
			return null;
		}
		return typeSwitch.doSwitch(lambdaExpression);
	}
	
	public ExpressionType getTypeOf(LambdaAction lambdaAction) {
		if(lambdaAction == null) {
			return null;
		}
		return typeSwitch.doSwitch(lambdaAction);
	}
}
