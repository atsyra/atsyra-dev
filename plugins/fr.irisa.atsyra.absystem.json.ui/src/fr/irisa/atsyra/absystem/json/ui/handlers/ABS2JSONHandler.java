/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.json.ui.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.sirius.emfjson.resource.JsonResource;
import org.eclipse.sirius.emfjson.resource.JsonResourceFactoryImpl;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.atsyra.absystem.json.transfo.ABS2JSON;
import fr.irisa.atsyra.absystem.json.transfo.InvalidJsonResource;

public class ABS2JSONHandler extends AbstractHandler {
	static final Logger log = LoggerFactory.getLogger(ABS2JSONHandler.class);

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		log.info("Starting ABS to JSON serialization");
		final List<IFile> files = getIFilesFromEvent(event);
		if (files.isEmpty()) {
			log.error("Impossible to find IFile for the ABS file");
			return null;
		}
		final ResourceSet absResourceSet = new ResourceSetImpl();
		for(IFile file : files) {
			EcoreUtil.resolveAll(getResourceFromIFile(file, absResourceSet));
		}
		final IWorkspace workspace = files.get(0).getWorkspace();
		
		final ISchedulingRule[] mainSchedulingRule = files.stream().map(IFile::getProject).distinct().toArray(ISchedulingRule[]::new);
		ISchedulingRule multirule = new MultiRule(mainSchedulingRule);

		// Register the JsonResourceFactory
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("json", new JsonResourceFactoryImpl());
		final ResourceSet jsonResourceSet = new ResourceSetImpl();
		jsonResourceSet.getLoadOptions().put(JsonResource.OPTION_DISPLAY_DYNAMIC_INSTANCES, true);

		// First, create (or get) the json resources. It will be used to update the
		// references
		final Map<Resource, Resource> abs2jsonResourceToCopy = new HashMap<>();
		final Map<Resource, Resource> abs2jsonResourceToReference = new HashMap<>();
		try {
			for (Resource resource : absResourceSet.getResources()) {
				final URI jsonUri = resource.getURI().trimFileExtension().appendFileExtension("json");
				final IProject project = getProjectFromResource(resource, workspace);
				if (project != null) {
					multirule = MultiRule.combine(multirule, project);
					abs2jsonResourceToCopy.put(resource, jsonResourceSet.createResource(jsonUri));
				} else {
					abs2jsonResourceToReference.put(resource, getOrCreateAndLoadResource(jsonUri, jsonResourceSet));
				}
			}
			final Job job = createABS2JSONJob(abs2jsonResourceToCopy, abs2jsonResourceToReference,
					"ABS2JSON");
			job.setPriority(Job.INTERACTIVE);
			job.setRule(multirule);
			job.schedule();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return null;
	}

	private Resource getOrCreateAndLoadResource(URI uri, ResourceSet rs) throws IOException {
		Resource resource = rs.getResource(uri, false);
		if (resource == null) {
			resource = rs.createResource(uri);
			if (rs.getURIConverter().exists(uri, null)) {// if file exists
				resource.load(null);
			}
		} else {
			if (!resource.isLoaded()) {
				resource.load(null);
			}
		}
		return resource;
	}

	private Job createABS2JSONJob(Map<Resource, Resource> abs2jsonResourceCopyMapping,
			Map<Resource, Resource> abs2jsonResourceReferenceMapping, String jobName) {
		return new Job(jobName) {
			protected IStatus run(IProgressMonitor monitor) {
				try {
					Collection<Resource> jsonResources = ABS2JSON.abs2jsonResourceMapping(abs2jsonResourceCopyMapping,
							abs2jsonResourceReferenceMapping);
					boolean success = true;
					Map<Object, Object> saveOptions = new HashMap<>();
					saveOptions.put(JsonResource.OPTION_DISPLAY_DYNAMIC_INSTANCES, true);
					saveOptions.put(JsonResource.OPTION_PRETTY_PRINTING_INDENT, JsonResource.INDENT_2_SPACES);
					for (Resource resource : jsonResources) {
						try {
							resource.save(saveOptions);
						} catch (IOException e) {
							success = false;
							log.error("Can't write json model to file {}", resource.getURI().toPlatformString(true));
						}
					}
					if (success) {
						log.info("ABS to JSON serialization succeeded 😀");
						return Status.OK_STATUS;
					} else {
						return Status.error("Failed writing some json resource, see logs for more information.");
					}
				} catch (InvalidJsonResource e1) {
					e1.printStackTrace();
					return Status.error("Invalid JSON Resource, see logs for more information.");
				}
			}
		};
	}

	private List<IFile> getIFilesFromEvent(ExecutionEvent event) {
		// get from selection in the active workbench window
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		return getIFilesFromSelection(selection);
	}

	private List<IFile> getIFilesFromSelection(ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			return getIFilesFromStructuredSelection((IStructuredSelection) selection);
		} else {
			return List.of();
		}
	}

	private List<IFile> getIFilesFromStructuredSelection(IStructuredSelection selection) {
		List<IFile> result = new ArrayList<>();
		for(Object selectedElement : selection) {
			if (selectedElement instanceof IResource) {
				IResource selectedResource = (IResource) selectedElement;
				try {
					selectedResource.accept(resource -> {
						if (resource instanceof IFile && "abs".equals(resource.getFileExtension())) {
							result.add((IFile) resource);
						}
						return true;
					});
				} catch (CoreException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
		return result;
	}

	private Resource getResourceFromIFile(IFile file, ResourceSet rs) {
		final URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
		return rs.getResource(uri, true);
	}

	private IProject getProjectFromResource(Resource resource, IWorkspace workspace) {
		final URI uri = resource.getURI();
		if (uri.isPlatformResource()) {
			IFile file = workspace.getRoot()
					.getFile(org.eclipse.core.runtime.Path.fromOSString(uri.toPlatformString(true)));
			return file.getProject();
		}
		return null;
	}
}
