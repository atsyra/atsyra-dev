/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.json.ui.handlers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.atsyra.absystem.json.transfo.ABS2JSON;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;

public class ABS2JSONMonofileHandler extends AbstractHandler {
	static final Logger log = LoggerFactory.getLogger(ABS2JSONMonofileHandler.class);

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		log.info("Starting ABS to JSON serialization");
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			@SuppressWarnings("unchecked")
			List<IFile> selectedElements = (List<IFile>) strucSelection.toList().stream()
					.filter(element -> element instanceof IFile).map(element -> (IFile) element)
					.collect(Collectors.toList());
			Job job = new Job("ABS2JSON") {
				protected IStatus run(IProgressMonitor monitor) {
					String jsonModel = ABS2JSON.abs2json(getFullABSModelFromIFile(selectedElements));
					Path outputPath = Paths
							.get(selectedElements.get(0).getProject().getLocation().addTrailingSeparator().toOSString()
									+ "metalmodel_" + System.currentTimeMillis() + ".json");
					try {
						Files.writeString(outputPath, jsonModel, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
					} catch (IOException e) {
						log.error("Can't write json model to file {}", outputPath);
					}
					log.info("ABS to JSON serialization succeed 😀");
					return Status.OK_STATUS;
				}
			};
			job.setPriority(Job.INTERACTIVE);
			job.setRule(selectedElements.get(0).getProject());
			job.schedule();
		}
		return null;
	}

	private EObject getFullABSModelFromIFile(List<IFile> files) {
		final ResourceSet rs = new ResourceSetImpl();
		AssetBasedSystem fullAbs = AbsystemFactory.eINSTANCE.createAssetBasedSystem();
		Copier cp = new EcoreUtil.Copier();

		files.forEach(f -> {
			final URI uri = URI.createPlatformResourceURI(f.getFullPath().toString(), true);
			Resource res = rs.getResource(uri, true);
			EObject abs = res.getContents().get(0);
			EcoreUtil.resolveAll(abs);
		});

		rs.getResources().forEach(r -> r.getContents().forEach(absEObject -> {
			fullAbs.getDefinitionGroups().addAll(cp.copyAll(((AssetBasedSystem) absEObject).getDefinitionGroups()));
			fullAbs.getAssetGroups().addAll(cp.copyAll(((AssetBasedSystem) absEObject).getAssetGroups()));
		}));

		cp.copyReferences();
		Resource resOut = rs.createResource(URI.createURI(""));
		resOut.getContents().add(fullAbs);

		return fullAbs;
	}
}
