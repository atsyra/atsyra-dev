/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.json.ui;

import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystemManager;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "fr.irisa.atsyra.absystem.json.transfo"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	public static void eclipseError(String msg, Throwable e){
		getMessagingSystem().error(msg, msgGroup, e);
		getMessagingSystem().focus();
	}
	
	public static void eclipseWarn(String msg, Throwable e){
		getMessagingSystem().warn(msg, msgGroup, e);
		getMessagingSystem().focus();
	}
	public static void eclipseImportant(String msg){
		getMessagingSystem().important( msg, msgGroup);
		getMessagingSystem().focus();
	}

	public static void eclipseInfo(String msg){
		getMessagingSystem().info(msg, msgGroup);
		getMessagingSystem().focus();
	}
	public static void eclipseDebug(String msg){
		getMessagingSystem().debug(msg, msgGroup);
		getMessagingSystem().focus();
	}
	public static String baseMsgGroup = "fr.irisa.atsyra";
	public static String msgGroup = baseMsgGroup+".ide.ui";
	protected static MessagingSystem messagingSystem = null;
	public static MessagingSystem getMessagingSystem() {
		if(messagingSystem == null) {
			MessagingSystemManager msm = new MessagingSystemManager();
			messagingSystem = msm.createBestPlatformMessagingSystem(baseMsgGroup, "ATSyRA");
		}
		return messagingSystem;
	}

}
