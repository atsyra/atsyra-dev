/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.preferences;

import java.util.Optional;

public interface PreferenceService {
	public Optional<Integer> getInt(String qualifier, String key);
	
	public Optional<Long> getLong(String qualifier, String key);
	
	public Optional<String> getString(String qualifier, String key);
	
	public Optional<Float> getFloat(String qualifier, String key);
	
	public Optional<Double> getDouble(String qualifier, String key);
	
	public Optional<Boolean> getBoolean(String qualifier, String key);
}
