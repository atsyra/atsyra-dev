/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.preferences.constants;

/**
 * Constant definitions for preferences
 */
public class GalPreferenceConstants {
	private GalPreferenceConstants() {
		// static class
	}
	
	// the qualifier of the eclipse preference plugin
	public static final String P_QUALIFIER = "fr.lip6.move.gal.itstools.preference";

	// Integer
	public static final String TIMEOUT_DURATION = "Maximum execution time";
	
	// Boolean
	public static final String QUIET_PARAMETER = "Low Verbosity";
	
	// Boolean
	public static final String DDDSDD_PARAMETER = "Privilege SDD encoding";
	
	// Combo
	public static final String SCALAR_PARAMETER = "Use recursive encodings for scalar";
	public static final String DEPTH2 = "Depth2";
	public static final String DEPTHREC = "DepthRec";
	public static final String DEPTHSHALLOW = "DepthShallow";
	
	// Integer
	public static final String GCTHRESHOLD = "GC Threshold, in MB";
	
	// Integer
	public static final String BLOCK_SIZE_PARAMETER = "Block size in Scalar encoding";
	
	// String
	public static final String ITSREACH_EXE = "its-reach";
	// String
	public static final String ITSCTL_EXE = "its-ctl";
	// String
	public static final String ITSLTL_EXE = "its-ltl";
	
}
