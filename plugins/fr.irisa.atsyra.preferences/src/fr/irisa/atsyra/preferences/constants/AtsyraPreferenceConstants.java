/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.preferences.constants;

/**
 * Constant definitions for preferences
 */
public class AtsyraPreferenceConstants {
	private AtsyraPreferenceConstants() {
		// static class
	}
	// the qualifier of the eclipse preference plugin
	public static final String P_QUALIFIER = "fr.irisa.atsyra.ide.ui";

	// Boolean
	public static final String P_GENERATE_OPTIONAL_FILES = "generateResultFiles";
	// Radio
	public static final String P_PRINT_SIZE = "printSize";
	// Radio
	public static final String P_OVERRIDE_LOCALE = "overrideLocale";
	
	// Integer
	public static final String P_MANYWITNESS = "manyPreference";
	// Boolean
	public static final String P_MANYWITNESS_NON_MINIMAL = "nonMinimalManyPreference";
	// Integer
	public static final String P_STATE_PRINT_LIMIT = "statePrintLimitPreference";
	// Integer
	public static final String P_MAX_EXEC_TIME = "maxExecTimePreference";
	// Boolean
	public static final String P_ENFORCE_DYNAMIC_CONTRACTS = "enforceDynamicContracts";
	
}
