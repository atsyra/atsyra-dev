/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.preferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PreferenceServiceAggregator implements PreferenceService {
	
	private List<PreferenceService> delegates;
	
	public PreferenceServiceAggregator() {
		delegates = new ArrayList<>();
	}
	
	public void addPreferenceService(PreferenceService delegate) {
		delegates.add(delegate);
	}

	@Override
	public Optional<Integer> getInt(String qualifier, String key) {
		for(PreferenceService delegate : delegates) {
			Optional<Integer> delegateResult = delegate.getInt(qualifier, key);
			if(delegateResult.isPresent()) {
				return delegateResult;
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<Long> getLong(String qualifier, String key) {
		for(PreferenceService delegate : delegates) {
			Optional<Long> delegateResult = delegate.getLong(qualifier, key);
			if(delegateResult.isPresent()) {
				return delegateResult;
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<String> getString(String qualifier, String key) {
		for(PreferenceService delegate : delegates) {
			Optional<String> delegateResult = delegate.getString(qualifier, key);
			if(delegateResult.isPresent()) {
				return delegateResult;
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<Float> getFloat(String qualifier, String key) {
		for(PreferenceService delegate : delegates) {
			Optional<Float> delegateResult = delegate.getFloat(qualifier, key);
			if(delegateResult.isPresent()) {
				return delegateResult;
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<Double> getDouble(String qualifier, String key) {
		for(PreferenceService delegate : delegates) {
			Optional<Double> delegateResult = delegate.getDouble(qualifier, key);
			if(delegateResult.isPresent()) {
				return delegateResult;
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<Boolean> getBoolean(String qualifier, String key) {
		for(PreferenceService delegate : delegates) {
			Optional<Boolean> delegateResult = delegate.getBoolean(qualifier, key);
			if(delegateResult.isPresent()) {
				return delegateResult;
			}
		}
		return Optional.empty();
	}

}
