/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atsyra Tree</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.AtsyraTree#getName <em>Name</em>}</li>
 *   <li>{@link atsyragoal.AtsyraTree#getMainGoal <em>Main Goal</em>}</li>
 *   <li>{@link atsyragoal.AtsyraTree#getOperator <em>Operator</em>}</li>
 *   <li>{@link atsyragoal.AtsyraTree#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @see atsyragoal.AtsyragoalPackage#getAtsyraTree()
 * @model
 * @generated
 */
public interface AtsyraTree extends AbstractAtsyraTree {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraTree_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link atsyragoal.AtsyraTree#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Main Goal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Goal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Goal</em>' reference.
	 * @see #setMainGoal(AtsyraGoal)
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraTree_MainGoal()
	 * @model keys="name"
	 * @generated
	 */
	AtsyraGoal getMainGoal();

	/**
	 * Sets the value of the '{@link atsyragoal.AtsyraTree#getMainGoal <em>Main Goal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Goal</em>' reference.
	 * @see #getMainGoal()
	 * @generated
	 */
	void setMainGoal(AtsyraGoal value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link atsyragoal.AtsyraTreeOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see atsyragoal.AtsyraTreeOperator
	 * @see #setOperator(AtsyraTreeOperator)
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraTree_Operator()
	 * @model
	 * @generated
	 */
	AtsyraTreeOperator getOperator();

	/**
	 * Sets the value of the '{@link atsyragoal.AtsyraTree#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see atsyragoal.AtsyraTreeOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(AtsyraTreeOperator value);

	/**
	 * Returns the value of the '<em><b>Operands</b></em>' containment reference list.
	 * The list contents are of type {@link atsyragoal.AbstractAtsyraTree}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operands</em>' containment reference list.
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraTree_Operands()
	 * @model containment="true" keys="qualifiedName"
	 * @generated
	 */
	EList<AbstractAtsyraTree> getOperands();

} // AtsyraTree
