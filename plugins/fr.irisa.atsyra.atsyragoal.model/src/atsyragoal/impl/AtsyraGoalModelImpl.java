/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.impl;

import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraGoalModel;
import atsyragoal.AtsyraTree;
import atsyragoal.AtsyragoalPackage;
import atsyragoal.DefaultValues;
import atsyragoal.Import;
import atsyragoal.Type;

import atsyragoal.TypedElement;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Atsyra Goal Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.impl.AtsyraGoalModelImpl#getAtsyragoals <em>Atsyragoals</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraGoalModelImpl#getTypes <em>Types</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraGoalModelImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraGoalModelImpl#getTypedElements <em>Typed Elements</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraGoalModelImpl#getTrees <em>Trees</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraGoalModelImpl#getDefaultValues <em>Default Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AtsyraGoalModelImpl extends MinimalEObjectImpl.Container implements AtsyraGoalModel {
	/**
	 * The cached value of the '{@link #getAtsyragoals() <em>Atsyragoals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtsyragoals()
	 * @generated
	 * @ordered
	 */
	protected EList<AtsyraGoal> atsyragoals;

	/**
	 * The cached value of the '{@link #getTypes() <em>Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<Type> types;

	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> imports;

	/**
	 * The cached value of the '{@link #getTypedElements() <em>Typed Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TypedElement> typedElements;

	/**
	 * The cached value of the '{@link #getTrees() <em>Trees</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrees()
	 * @generated
	 * @ordered
	 */
	protected EList<AtsyraTree> trees;

	/**
	 * The cached value of the '{@link #getDefaultValues() <em>Default Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultValues()
	 * @generated
	 * @ordered
	 */
	protected EList<DefaultValues> defaultValues;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtsyraGoalModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtsyraGoal> getAtsyragoals() {
		if (atsyragoals == null) {
			atsyragoals = new EObjectContainmentEList<AtsyraGoal>(AtsyraGoal.class, this, AtsyragoalPackage.ATSYRA_GOAL_MODEL__ATSYRAGOALS);
		}
		return atsyragoals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Type> getTypes() {
		if (types == null) {
			types = new EObjectContainmentEList<Type>(Type.class, this, AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPES);
		}
		return types;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Import> getImports() {
		if (imports == null) {
			imports = new EObjectContainmentEList<Import>(Import.class, this, AtsyragoalPackage.ATSYRA_GOAL_MODEL__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypedElement> getTypedElements() {
		if (typedElements == null) {
			typedElements = new EObjectContainmentEList<TypedElement>(TypedElement.class, this, AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS);
		}
		return typedElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtsyraTree> getTrees() {
		if (trees == null) {
			trees = new EObjectContainmentEList<AtsyraTree>(AtsyraTree.class, this, AtsyragoalPackage.ATSYRA_GOAL_MODEL__TREES);
		}
		return trees;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefaultValues> getDefaultValues() {
		if (defaultValues == null) {
			defaultValues = new EObjectContainmentEList<DefaultValues>(DefaultValues.class, this, AtsyragoalPackage.ATSYRA_GOAL_MODEL__DEFAULT_VALUES);
		}
		return defaultValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__ATSYRAGOALS:
				return ((InternalEList<?>)getAtsyragoals()).basicRemove(otherEnd, msgs);
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPES:
				return ((InternalEList<?>)getTypes()).basicRemove(otherEnd, msgs);
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__IMPORTS:
				return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS:
				return ((InternalEList<?>)getTypedElements()).basicRemove(otherEnd, msgs);
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TREES:
				return ((InternalEList<?>)getTrees()).basicRemove(otherEnd, msgs);
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__DEFAULT_VALUES:
				return ((InternalEList<?>)getDefaultValues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__ATSYRAGOALS:
				return getAtsyragoals();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPES:
				return getTypes();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__IMPORTS:
				return getImports();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS:
				return getTypedElements();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TREES:
				return getTrees();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__DEFAULT_VALUES:
				return getDefaultValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__ATSYRAGOALS:
				getAtsyragoals().clear();
				getAtsyragoals().addAll((Collection<? extends AtsyraGoal>)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPES:
				getTypes().clear();
				getTypes().addAll((Collection<? extends Type>)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends Import>)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS:
				getTypedElements().clear();
				getTypedElements().addAll((Collection<? extends TypedElement>)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TREES:
				getTrees().clear();
				getTrees().addAll((Collection<? extends AtsyraTree>)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__DEFAULT_VALUES:
				getDefaultValues().clear();
				getDefaultValues().addAll((Collection<? extends DefaultValues>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__ATSYRAGOALS:
				getAtsyragoals().clear();
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPES:
				getTypes().clear();
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__IMPORTS:
				getImports().clear();
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS:
				getTypedElements().clear();
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TREES:
				getTrees().clear();
				return;
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__DEFAULT_VALUES:
				getDefaultValues().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__ATSYRAGOALS:
				return atsyragoals != null && !atsyragoals.isEmpty();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPES:
				return types != null && !types.isEmpty();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__IMPORTS:
				return imports != null && !imports.isEmpty();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS:
				return typedElements != null && !typedElements.isEmpty();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TREES:
				return trees != null && !trees.isEmpty();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__DEFAULT_VALUES:
				return defaultValues != null && !defaultValues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AtsyraGoalModelImpl
