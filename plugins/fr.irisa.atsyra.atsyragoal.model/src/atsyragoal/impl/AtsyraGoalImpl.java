/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.impl;

import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyragoalPackage;
import atsyragoal.DefaultValues;
import atsyragoal.GoalCondition;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Atsyra Goal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.impl.AtsyraGoalImpl#getPrecondition <em>Precondition</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraGoalImpl#getPostcondition <em>Postcondition</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraGoalImpl#getName <em>Name</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraGoalImpl#getDefaultUsedInPre <em>Default Used In Pre</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraGoalImpl#getDefaultUsedInPost <em>Default Used In Post</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AtsyraGoalImpl extends MinimalEObjectImpl.Container implements AtsyraGoal {
	/**
	 * The cached value of the '{@link #getPrecondition() <em>Precondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrecondition()
	 * @generated
	 * @ordered
	 */
	protected GoalCondition precondition;

	/**
	 * The cached value of the '{@link #getPostcondition() <em>Postcondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostcondition()
	 * @generated
	 * @ordered
	 */
	protected GoalCondition postcondition;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDefaultUsedInPre() <em>Default Used In Pre</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultUsedInPre()
	 * @generated
	 * @ordered
	 */
	protected DefaultValues defaultUsedInPre;

	/**
	 * The cached value of the '{@link #getDefaultUsedInPost() <em>Default Used In Post</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultUsedInPost()
	 * @generated
	 * @ordered
	 */
	protected DefaultValues defaultUsedInPost;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtsyraGoalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AtsyragoalPackage.Literals.ATSYRA_GOAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalCondition getPrecondition() {
		return precondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrecondition(GoalCondition newPrecondition, NotificationChain msgs) {
		GoalCondition oldPrecondition = precondition;
		precondition = newPrecondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION, oldPrecondition, newPrecondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrecondition(GoalCondition newPrecondition) {
		if (newPrecondition != precondition) {
			NotificationChain msgs = null;
			if (precondition != null)
				msgs = ((InternalEObject)precondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION, null, msgs);
			if (newPrecondition != null)
				msgs = ((InternalEObject)newPrecondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION, null, msgs);
			msgs = basicSetPrecondition(newPrecondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION, newPrecondition, newPrecondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalCondition getPostcondition() {
		return postcondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPostcondition(GoalCondition newPostcondition, NotificationChain msgs) {
		GoalCondition oldPostcondition = postcondition;
		postcondition = newPostcondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION, oldPostcondition, newPostcondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostcondition(GoalCondition newPostcondition) {
		if (newPostcondition != postcondition) {
			NotificationChain msgs = null;
			if (postcondition != null)
				msgs = ((InternalEObject)postcondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION, null, msgs);
			if (newPostcondition != null)
				msgs = ((InternalEObject)newPostcondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION, null, msgs);
			msgs = basicSetPostcondition(newPostcondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION, newPostcondition, newPostcondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_GOAL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultValues getDefaultUsedInPre() {
		if (defaultUsedInPre != null && defaultUsedInPre.eIsProxy()) {
			InternalEObject oldDefaultUsedInPre = (InternalEObject)defaultUsedInPre;
			defaultUsedInPre = (DefaultValues)eResolveProxy(oldDefaultUsedInPre);
			if (defaultUsedInPre != oldDefaultUsedInPre) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_PRE, oldDefaultUsedInPre, defaultUsedInPre));
			}
		}
		return defaultUsedInPre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultValues basicGetDefaultUsedInPre() {
		return defaultUsedInPre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultUsedInPre(DefaultValues newDefaultUsedInPre) {
		DefaultValues oldDefaultUsedInPre = defaultUsedInPre;
		defaultUsedInPre = newDefaultUsedInPre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_PRE, oldDefaultUsedInPre, defaultUsedInPre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultValues getDefaultUsedInPost() {
		if (defaultUsedInPost != null && defaultUsedInPost.eIsProxy()) {
			InternalEObject oldDefaultUsedInPost = (InternalEObject)defaultUsedInPost;
			defaultUsedInPost = (DefaultValues)eResolveProxy(oldDefaultUsedInPost);
			if (defaultUsedInPost != oldDefaultUsedInPost) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_POST, oldDefaultUsedInPost, defaultUsedInPost));
			}
		}
		return defaultUsedInPost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultValues basicGetDefaultUsedInPost() {
		return defaultUsedInPost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultUsedInPost(DefaultValues newDefaultUsedInPost) {
		DefaultValues oldDefaultUsedInPost = defaultUsedInPost;
		defaultUsedInPost = newDefaultUsedInPost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_POST, oldDefaultUsedInPost, defaultUsedInPost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION:
				return basicSetPrecondition(null, msgs);
			case AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION:
				return basicSetPostcondition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION:
				return getPrecondition();
			case AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION:
				return getPostcondition();
			case AtsyragoalPackage.ATSYRA_GOAL__NAME:
				return getName();
			case AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_PRE:
				if (resolve) return getDefaultUsedInPre();
				return basicGetDefaultUsedInPre();
			case AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_POST:
				if (resolve) return getDefaultUsedInPost();
				return basicGetDefaultUsedInPost();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION:
				setPrecondition((GoalCondition)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION:
				setPostcondition((GoalCondition)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL__NAME:
				setName((String)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_PRE:
				setDefaultUsedInPre((DefaultValues)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_POST:
				setDefaultUsedInPost((DefaultValues)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION:
				setPrecondition((GoalCondition)null);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION:
				setPostcondition((GoalCondition)null);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL__NAME:
				setName(NAME_EDEFAULT);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_PRE:
				setDefaultUsedInPre((DefaultValues)null);
				return;
			case AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_POST:
				setDefaultUsedInPost((DefaultValues)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION:
				return precondition != null;
			case AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION:
				return postcondition != null;
			case AtsyragoalPackage.ATSYRA_GOAL__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_PRE:
				return defaultUsedInPre != null;
			case AtsyragoalPackage.ATSYRA_GOAL__DEFAULT_USED_IN_POST:
				return defaultUsedInPost != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //AtsyraGoalImpl
