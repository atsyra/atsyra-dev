/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.impl;

import atsyragoal.AbstractAtsyraTree;
import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraTree;
import atsyragoal.AtsyraTreeOperator;
import atsyragoal.AtsyragoalPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Atsyra Tree</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.impl.AtsyraTreeImpl#getName <em>Name</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraTreeImpl#getMainGoal <em>Main Goal</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraTreeImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link atsyragoal.impl.AtsyraTreeImpl#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AtsyraTreeImpl extends AbstractAtsyraTreeImpl implements AtsyraTree {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMainGoal() <em>Main Goal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainGoal()
	 * @generated
	 * @ordered
	 */
	protected AtsyraGoal mainGoal;

	/**
	 * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected static final AtsyraTreeOperator OPERATOR_EDEFAULT = AtsyraTreeOperator.UNKNOWN;

	/**
	 * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected AtsyraTreeOperator operator = OPERATOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOperands() <em>Operands</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperands()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractAtsyraTree> operands;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtsyraTreeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AtsyragoalPackage.Literals.ATSYRA_TREE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_TREE__NAME, oldName, name));
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getQualifiedName() {
		return getName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraGoal getMainGoal() {
		if (mainGoal != null && mainGoal.eIsProxy()) {
			InternalEObject oldMainGoal = (InternalEObject)mainGoal;
			mainGoal = (AtsyraGoal)eResolveProxy(oldMainGoal);
			if (mainGoal != oldMainGoal) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AtsyragoalPackage.ATSYRA_TREE__MAIN_GOAL, oldMainGoal, mainGoal));
			}
		}
		return mainGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraGoal basicGetMainGoal() {
		return mainGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMainGoal(AtsyraGoal newMainGoal) {
		AtsyraGoal oldMainGoal = mainGoal;
		mainGoal = newMainGoal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_TREE__MAIN_GOAL, oldMainGoal, mainGoal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraTreeOperator getOperator() {
		return operator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperator(AtsyraTreeOperator newOperator) {
		AtsyraTreeOperator oldOperator = operator;
		operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_TREE__OPERATOR, oldOperator, operator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractAtsyraTree> getOperands() {
		if (operands == null) {
			operands = new EObjectContainmentEList<AbstractAtsyraTree>(AbstractAtsyraTree.class, this, AtsyragoalPackage.ATSYRA_TREE__OPERANDS);
		}
		return operands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_TREE__OPERANDS:
				return ((InternalEList<?>)getOperands()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_TREE__NAME:
				return getName();
			case AtsyragoalPackage.ATSYRA_TREE__MAIN_GOAL:
				if (resolve) return getMainGoal();
				return basicGetMainGoal();
			case AtsyragoalPackage.ATSYRA_TREE__OPERATOR:
				return getOperator();
			case AtsyragoalPackage.ATSYRA_TREE__OPERANDS:
				return getOperands();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_TREE__NAME:
				setName((String)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_TREE__MAIN_GOAL:
				setMainGoal((AtsyraGoal)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_TREE__OPERATOR:
				setOperator((AtsyraTreeOperator)newValue);
				return;
			case AtsyragoalPackage.ATSYRA_TREE__OPERANDS:
				getOperands().clear();
				getOperands().addAll((Collection<? extends AbstractAtsyraTree>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_TREE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case AtsyragoalPackage.ATSYRA_TREE__MAIN_GOAL:
				setMainGoal((AtsyraGoal)null);
				return;
			case AtsyragoalPackage.ATSYRA_TREE__OPERATOR:
				setOperator(OPERATOR_EDEFAULT);
				return;
			case AtsyragoalPackage.ATSYRA_TREE__OPERANDS:
				getOperands().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_TREE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case AtsyragoalPackage.ATSYRA_TREE__MAIN_GOAL:
				return mainGoal != null;
			case AtsyragoalPackage.ATSYRA_TREE__OPERATOR:
				return operator != OPERATOR_EDEFAULT;
			case AtsyragoalPackage.ATSYRA_TREE__OPERANDS:
				return operands != null && !operands.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", operator: ");
		result.append(operator);
		result.append(')');
		return result.toString();
	}

} //AtsyraTreeImpl
