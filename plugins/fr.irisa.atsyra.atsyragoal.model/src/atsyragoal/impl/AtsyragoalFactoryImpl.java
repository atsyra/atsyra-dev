/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.impl;

import atsyragoal.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AtsyragoalFactoryImpl extends EFactoryImpl implements AtsyragoalFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AtsyragoalFactory init() {
		try {
			AtsyragoalFactory theAtsyragoalFactory = (AtsyragoalFactory)EPackage.Registry.INSTANCE.getEFactory(AtsyragoalPackage.eNS_URI);
			if (theAtsyragoalFactory != null) {
				return theAtsyragoalFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AtsyragoalFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyragoalFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AtsyragoalPackage.ATSYRA_GOAL: return createAtsyraGoal();
			case AtsyragoalPackage.AND_CONDITION: return createAndCondition();
			case AtsyragoalPackage.OR_CONDITION: return createOrCondition();
			case AtsyragoalPackage.NOT_CONDITION: return createNotCondition();
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL: return createAtsyraGoalModel();
			case AtsyragoalPackage.EQUALS_CONDITION: return createEqualsCondition();
			case AtsyragoalPackage.INTERNAL_TYPE: return createInternalType();
			case AtsyragoalPackage.BOOLEAN_LITERAL: return createBooleanLiteral();
			case AtsyragoalPackage.SYSTEM_TYPE: return createSystemType();
			case AtsyragoalPackage.SYSTEM_FEATURE: return createSystemFeature();
			case AtsyragoalPackage.SYSTEM_CONST_FEATURE: return createSystemConstFeature();
			case AtsyragoalPackage.BOOLEAN_SYSTEM_CONDITION: return createBooleanSystemCondition();
			case AtsyragoalPackage.IMPORT: return createImport();
			case AtsyragoalPackage.ATSYRA_TREE: return createAtsyraTree();
			case AtsyragoalPackage.ATSYRA_TREE_REFERENCE: return createAtsyraTreeReference();
			case AtsyragoalPackage.DEFAULT_VALUES: return createDefaultValues();
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT: return createDefaultAssignment();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case AtsyragoalPackage.ATSYRA_TREE_OPERATOR:
				return createAtsyraTreeOperatorFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case AtsyragoalPackage.ATSYRA_TREE_OPERATOR:
				return convertAtsyraTreeOperatorToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraGoal createAtsyraGoal() {
		AtsyraGoalImpl atsyraGoal = new AtsyraGoalImpl();
		return atsyraGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AndCondition createAndCondition() {
		AndConditionImpl andCondition = new AndConditionImpl();
		return andCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrCondition createOrCondition() {
		OrConditionImpl orCondition = new OrConditionImpl();
		return orCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotCondition createNotCondition() {
		NotConditionImpl notCondition = new NotConditionImpl();
		return notCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraGoalModel createAtsyraGoalModel() {
		AtsyraGoalModelImpl atsyraGoalModel = new AtsyraGoalModelImpl();
		return atsyraGoalModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EqualsCondition createEqualsCondition() {
		EqualsConditionImpl equalsCondition = new EqualsConditionImpl();
		return equalsCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalType createInternalType() {
		InternalTypeImpl internalType = new InternalTypeImpl();
		return internalType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanLiteral createBooleanLiteral() {
		BooleanLiteralImpl booleanLiteral = new BooleanLiteralImpl();
		return booleanLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemType createSystemType() {
		SystemTypeImpl systemType = new SystemTypeImpl();
		return systemType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemFeature createSystemFeature() {
		SystemFeatureImpl systemFeature = new SystemFeatureImpl();
		return systemFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConstFeature createSystemConstFeature() {
		SystemConstFeatureImpl systemConstFeature = new SystemConstFeatureImpl();
		return systemConstFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanSystemCondition createBooleanSystemCondition() {
		BooleanSystemConditionImpl booleanSystemCondition = new BooleanSystemConditionImpl();
		return booleanSystemCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Import createImport() {
		ImportImpl import_ = new ImportImpl();
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraTree createAtsyraTree() {
		AtsyraTreeImpl atsyraTree = new AtsyraTreeImpl();
		return atsyraTree;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraTreeReference createAtsyraTreeReference() {
		AtsyraTreeReferenceImpl atsyraTreeReference = new AtsyraTreeReferenceImpl();
		return atsyraTreeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultValues createDefaultValues() {
		DefaultValuesImpl defaultValues = new DefaultValuesImpl();
		return defaultValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultAssignment createDefaultAssignment() {
		DefaultAssignmentImpl defaultAssignment = new DefaultAssignmentImpl();
		return defaultAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraTreeOperator createAtsyraTreeOperatorFromString(EDataType eDataType, String initialValue) {
		AtsyraTreeOperator result = AtsyraTreeOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAtsyraTreeOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyragoalPackage getAtsyragoalPackage() {
		return (AtsyragoalPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AtsyragoalPackage getPackage() {
		return AtsyragoalPackage.eINSTANCE;
	}

} //AtsyragoalFactoryImpl
