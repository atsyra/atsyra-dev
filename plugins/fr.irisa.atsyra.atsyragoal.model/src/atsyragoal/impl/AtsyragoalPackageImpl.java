/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.impl;

import atsyragoal.AbstractAtsyraTree;
import atsyragoal.AndCondition;
import atsyragoal.AtomicCondition;
import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraGoalModel;
import atsyragoal.AtsyraTree;
import atsyragoal.AtsyraTreeOperator;
import atsyragoal.AtsyraTreeReference;
import atsyragoal.AtsyragoalFactory;
import atsyragoal.AtsyragoalPackage;
import atsyragoal.BooleanLiteral;
import atsyragoal.BooleanSystemCondition;
import atsyragoal.ConditionOperator;
import atsyragoal.DefaultAssignment;
import atsyragoal.DefaultValues;
import atsyragoal.EqualsCondition;
import atsyragoal.GoalCondition;
import atsyragoal.Import;
import atsyragoal.InternalType;
import atsyragoal.NotCondition;
import atsyragoal.OrCondition;
import atsyragoal.SystemConstFeature;
import atsyragoal.SystemFeature;
import atsyragoal.SystemType;
import atsyragoal.Type;
import atsyragoal.TypedElement;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AtsyragoalPackageImpl extends EPackageImpl implements AtsyragoalPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atsyraGoalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atomicConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionOperatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass andConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atsyraGoalModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equalsConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass internalTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemConstFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanSystemConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atsyraTreeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractAtsyraTreeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atsyraTreeReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass defaultValuesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass defaultAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum atsyraTreeOperatorEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see atsyragoal.AtsyragoalPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AtsyragoalPackageImpl() {
		super(eNS_URI, AtsyragoalFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AtsyragoalPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AtsyragoalPackage init() {
		if (isInited) return (AtsyragoalPackage)EPackage.Registry.INSTANCE.getEPackage(AtsyragoalPackage.eNS_URI);

		// Obtain or create and register package
		AtsyragoalPackageImpl theAtsyragoalPackage = (AtsyragoalPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AtsyragoalPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AtsyragoalPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theAtsyragoalPackage.createPackageContents();

		// Initialize created meta-data
		theAtsyragoalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAtsyragoalPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AtsyragoalPackage.eNS_URI, theAtsyragoalPackage);
		return theAtsyragoalPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAtsyraGoal() {
		return atsyraGoalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoal_Precondition() {
		return (EReference)atsyraGoalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoal_Postcondition() {
		return (EReference)atsyraGoalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAtsyraGoal_Name() {
		return (EAttribute)atsyraGoalEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoal_DefaultUsedInPre() {
		return (EReference)atsyraGoalEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoal_DefaultUsedInPost() {
		return (EReference)atsyraGoalEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoalCondition() {
		return goalConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAtomicCondition() {
		return atomicConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConditionOperator() {
		return conditionOperatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionOperator_Operands() {
		return (EReference)conditionOperatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAndCondition() {
		return andConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrCondition() {
		return orConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNotCondition() {
		return notConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAtsyraGoalModel() {
		return atsyraGoalModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoalModel_Atsyragoals() {
		return (EReference)atsyraGoalModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoalModel_Types() {
		return (EReference)atsyraGoalModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoalModel_Imports() {
		return (EReference)atsyraGoalModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoalModel_TypedElements() {
		return (EReference)atsyraGoalModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoalModel_Trees() {
		return (EReference)atsyraGoalModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraGoalModel_DefaultValues() {
		return (EReference)atsyraGoalModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEqualsCondition() {
		return equalsConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEqualsCondition_Source() {
		return (EReference)equalsConditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEqualsCondition_Target() {
		return (EReference)equalsConditionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypedElement() {
		return typedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypedElement_Type() {
		return (EReference)typedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTypedElement_Name() {
		return (EAttribute)typedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getType() {
		return typeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getType_Name() {
		return (EAttribute)typeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInternalType() {
		return internalTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanLiteral() {
		return booleanLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemType() {
		return systemTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemFeature() {
		return systemFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemConstFeature() {
		return systemConstFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanSystemCondition() {
		return booleanSystemConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBooleanSystemCondition_Source() {
		return (EReference)booleanSystemConditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImport() {
		return importEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImport_ImportURI() {
		return (EAttribute)importEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAtsyraTree() {
		return atsyraTreeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAtsyraTree_Name() {
		return (EAttribute)atsyraTreeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraTree_MainGoal() {
		return (EReference)atsyraTreeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAtsyraTree_Operator() {
		return (EAttribute)atsyraTreeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraTree_Operands() {
		return (EReference)atsyraTreeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractAtsyraTree() {
		return abstractAtsyraTreeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractAtsyraTree_QualifiedName() {
		return (EAttribute)abstractAtsyraTreeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAtsyraTreeReference() {
		return atsyraTreeReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtsyraTreeReference_ReferencedTree() {
		return (EReference)atsyraTreeReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDefaultValues() {
		return defaultValuesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDefaultValues_Name() {
		return (EAttribute)defaultValuesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDefaultValues_DefaultValueAssignments() {
		return (EReference)defaultValuesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDefaultAssignment() {
		return defaultAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDefaultAssignment_Target() {
		return (EReference)defaultAssignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDefaultAssignment_AssignedValue() {
		return (EReference)defaultAssignmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAtsyraTreeOperator() {
		return atsyraTreeOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyragoalFactory getAtsyragoalFactory() {
		return (AtsyragoalFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		atsyraGoalEClass = createEClass(ATSYRA_GOAL);
		createEReference(atsyraGoalEClass, ATSYRA_GOAL__PRECONDITION);
		createEReference(atsyraGoalEClass, ATSYRA_GOAL__POSTCONDITION);
		createEAttribute(atsyraGoalEClass, ATSYRA_GOAL__NAME);
		createEReference(atsyraGoalEClass, ATSYRA_GOAL__DEFAULT_USED_IN_PRE);
		createEReference(atsyraGoalEClass, ATSYRA_GOAL__DEFAULT_USED_IN_POST);

		goalConditionEClass = createEClass(GOAL_CONDITION);

		atomicConditionEClass = createEClass(ATOMIC_CONDITION);

		conditionOperatorEClass = createEClass(CONDITION_OPERATOR);
		createEReference(conditionOperatorEClass, CONDITION_OPERATOR__OPERANDS);

		andConditionEClass = createEClass(AND_CONDITION);

		orConditionEClass = createEClass(OR_CONDITION);

		notConditionEClass = createEClass(NOT_CONDITION);

		atsyraGoalModelEClass = createEClass(ATSYRA_GOAL_MODEL);
		createEReference(atsyraGoalModelEClass, ATSYRA_GOAL_MODEL__ATSYRAGOALS);
		createEReference(atsyraGoalModelEClass, ATSYRA_GOAL_MODEL__TYPES);
		createEReference(atsyraGoalModelEClass, ATSYRA_GOAL_MODEL__IMPORTS);
		createEReference(atsyraGoalModelEClass, ATSYRA_GOAL_MODEL__TYPED_ELEMENTS);
		createEReference(atsyraGoalModelEClass, ATSYRA_GOAL_MODEL__TREES);
		createEReference(atsyraGoalModelEClass, ATSYRA_GOAL_MODEL__DEFAULT_VALUES);

		equalsConditionEClass = createEClass(EQUALS_CONDITION);
		createEReference(equalsConditionEClass, EQUALS_CONDITION__SOURCE);
		createEReference(equalsConditionEClass, EQUALS_CONDITION__TARGET);

		typedElementEClass = createEClass(TYPED_ELEMENT);
		createEReference(typedElementEClass, TYPED_ELEMENT__TYPE);
		createEAttribute(typedElementEClass, TYPED_ELEMENT__NAME);

		typeEClass = createEClass(TYPE);
		createEAttribute(typeEClass, TYPE__NAME);

		internalTypeEClass = createEClass(INTERNAL_TYPE);

		booleanLiteralEClass = createEClass(BOOLEAN_LITERAL);

		systemTypeEClass = createEClass(SYSTEM_TYPE);

		systemFeatureEClass = createEClass(SYSTEM_FEATURE);

		systemConstFeatureEClass = createEClass(SYSTEM_CONST_FEATURE);

		booleanSystemConditionEClass = createEClass(BOOLEAN_SYSTEM_CONDITION);
		createEReference(booleanSystemConditionEClass, BOOLEAN_SYSTEM_CONDITION__SOURCE);

		importEClass = createEClass(IMPORT);
		createEAttribute(importEClass, IMPORT__IMPORT_URI);

		atsyraTreeEClass = createEClass(ATSYRA_TREE);
		createEAttribute(atsyraTreeEClass, ATSYRA_TREE__NAME);
		createEReference(atsyraTreeEClass, ATSYRA_TREE__MAIN_GOAL);
		createEAttribute(atsyraTreeEClass, ATSYRA_TREE__OPERATOR);
		createEReference(atsyraTreeEClass, ATSYRA_TREE__OPERANDS);

		abstractAtsyraTreeEClass = createEClass(ABSTRACT_ATSYRA_TREE);
		createEAttribute(abstractAtsyraTreeEClass, ABSTRACT_ATSYRA_TREE__QUALIFIED_NAME);

		atsyraTreeReferenceEClass = createEClass(ATSYRA_TREE_REFERENCE);
		createEReference(atsyraTreeReferenceEClass, ATSYRA_TREE_REFERENCE__REFERENCED_TREE);

		defaultValuesEClass = createEClass(DEFAULT_VALUES);
		createEAttribute(defaultValuesEClass, DEFAULT_VALUES__NAME);
		createEReference(defaultValuesEClass, DEFAULT_VALUES__DEFAULT_VALUE_ASSIGNMENTS);

		defaultAssignmentEClass = createEClass(DEFAULT_ASSIGNMENT);
		createEReference(defaultAssignmentEClass, DEFAULT_ASSIGNMENT__TARGET);
		createEReference(defaultAssignmentEClass, DEFAULT_ASSIGNMENT__ASSIGNED_VALUE);

		// Create enums
		atsyraTreeOperatorEEnum = createEEnum(ATSYRA_TREE_OPERATOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		atomicConditionEClass.getESuperTypes().add(this.getGoalCondition());
		conditionOperatorEClass.getESuperTypes().add(this.getGoalCondition());
		andConditionEClass.getESuperTypes().add(this.getConditionOperator());
		orConditionEClass.getESuperTypes().add(this.getConditionOperator());
		notConditionEClass.getESuperTypes().add(this.getConditionOperator());
		equalsConditionEClass.getESuperTypes().add(this.getAtomicCondition());
		internalTypeEClass.getESuperTypes().add(this.getType());
		booleanLiteralEClass.getESuperTypes().add(this.getTypedElement());
		systemTypeEClass.getESuperTypes().add(this.getType());
		systemFeatureEClass.getESuperTypes().add(this.getTypedElement());
		systemConstFeatureEClass.getESuperTypes().add(this.getTypedElement());
		booleanSystemConditionEClass.getESuperTypes().add(this.getAtomicCondition());
		atsyraTreeEClass.getESuperTypes().add(this.getAbstractAtsyraTree());
		atsyraTreeReferenceEClass.getESuperTypes().add(this.getAbstractAtsyraTree());

		// Initialize classes, features, and operations; add parameters
		initEClass(atsyraGoalEClass, AtsyraGoal.class, "AtsyraGoal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAtsyraGoal_Precondition(), this.getGoalCondition(), null, "precondition", null, 0, 1, AtsyraGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtsyraGoal_Postcondition(), this.getGoalCondition(), null, "postcondition", null, 0, 1, AtsyraGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAtsyraGoal_Name(), ecorePackage.getEString(), "name", null, 0, 1, AtsyraGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtsyraGoal_DefaultUsedInPre(), this.getDefaultValues(), null, "defaultUsedInPre", null, 0, 1, AtsyraGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAtsyraGoal_DefaultUsedInPre().getEKeys().add(this.getDefaultValues_Name());
		initEReference(getAtsyraGoal_DefaultUsedInPost(), this.getDefaultValues(), null, "defaultUsedInPost", null, 0, 1, AtsyraGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAtsyraGoal_DefaultUsedInPost().getEKeys().add(this.getDefaultValues_Name());

		initEClass(goalConditionEClass, GoalCondition.class, "GoalCondition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(atomicConditionEClass, AtomicCondition.class, "AtomicCondition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(conditionOperatorEClass, ConditionOperator.class, "ConditionOperator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConditionOperator_Operands(), this.getGoalCondition(), null, "operands", null, 1, -1, ConditionOperator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(andConditionEClass, AndCondition.class, "AndCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(orConditionEClass, OrCondition.class, "OrCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(notConditionEClass, NotCondition.class, "NotCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(atsyraGoalModelEClass, AtsyraGoalModel.class, "AtsyraGoalModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAtsyraGoalModel_Atsyragoals(), this.getAtsyraGoal(), null, "atsyragoals", null, 0, -1, AtsyraGoalModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtsyraGoalModel_Types(), this.getType(), null, "types", null, 0, -1, AtsyraGoalModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtsyraGoalModel_Imports(), this.getImport(), null, "imports", null, 0, -1, AtsyraGoalModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtsyraGoalModel_TypedElements(), this.getTypedElement(), null, "typedElements", null, 0, -1, AtsyraGoalModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtsyraGoalModel_Trees(), this.getAtsyraTree(), null, "trees", null, 0, -1, AtsyraGoalModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtsyraGoalModel_DefaultValues(), this.getDefaultValues(), null, "defaultValues", null, 0, -1, AtsyraGoalModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equalsConditionEClass, EqualsCondition.class, "EqualsCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEqualsCondition_Source(), this.getTypedElement(), null, "source", null, 1, 1, EqualsCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEqualsCondition_Target(), this.getTypedElement(), null, "target", null, 1, 1, EqualsCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typedElementEClass, TypedElement.class, "TypedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTypedElement_Type(), this.getType(), null, "type", null, 1, 1, TypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTypedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, TypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typeEClass, Type.class, "Type", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getType_Name(), ecorePackage.getEString(), "name", "Boolean", 0, 1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(internalTypeEClass, InternalType.class, "InternalType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(booleanLiteralEClass, BooleanLiteral.class, "BooleanLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(systemTypeEClass, SystemType.class, "SystemType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(systemFeatureEClass, SystemFeature.class, "SystemFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(systemConstFeatureEClass, SystemConstFeature.class, "SystemConstFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(booleanSystemConditionEClass, BooleanSystemCondition.class, "BooleanSystemCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBooleanSystemCondition_Source(), this.getTypedElement(), null, "source", null, 1, 1, BooleanSystemCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(importEClass, Import.class, "Import", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImport_ImportURI(), ecorePackage.getEString(), "importURI", null, 0, 1, Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(atsyraTreeEClass, AtsyraTree.class, "AtsyraTree", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAtsyraTree_Name(), ecorePackage.getEString(), "name", null, 1, 1, AtsyraTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtsyraTree_MainGoal(), this.getAtsyraGoal(), null, "mainGoal", null, 0, 1, AtsyraTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAtsyraTree_MainGoal().getEKeys().add(this.getAtsyraGoal_Name());
		initEAttribute(getAtsyraTree_Operator(), this.getAtsyraTreeOperator(), "operator", null, 0, 1, AtsyraTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtsyraTree_Operands(), this.getAbstractAtsyraTree(), null, "operands", null, 0, -1, AtsyraTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAtsyraTree_Operands().getEKeys().add(this.getAbstractAtsyraTree_QualifiedName());

		initEClass(abstractAtsyraTreeEClass, AbstractAtsyraTree.class, "AbstractAtsyraTree", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractAtsyraTree_QualifiedName(), ecorePackage.getEString(), "qualifiedName", "", 0, 1, AbstractAtsyraTree.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(atsyraTreeReferenceEClass, AtsyraTreeReference.class, "AtsyraTreeReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAtsyraTreeReference_ReferencedTree(), this.getAbstractAtsyraTree(), null, "referencedTree", null, 1, 1, AtsyraTreeReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getAtsyraTreeReference_ReferencedTree().getEKeys().add(this.getAbstractAtsyraTree_QualifiedName());

		initEClass(defaultValuesEClass, DefaultValues.class, "DefaultValues", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDefaultValues_Name(), ecorePackage.getEString(), "name", null, 0, 1, DefaultValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDefaultValues_DefaultValueAssignments(), this.getDefaultAssignment(), null, "defaultValueAssignments", null, 0, -1, DefaultValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(defaultAssignmentEClass, DefaultAssignment.class, "DefaultAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDefaultAssignment_Target(), this.getSystemFeature(), null, "target", null, 1, 1, DefaultAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDefaultAssignment_AssignedValue(), this.getTypedElement(), null, "assignedValue", null, 1, 1, DefaultAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(atsyraTreeOperatorEEnum, AtsyraTreeOperator.class, "AtsyraTreeOperator");
		addEEnumLiteral(atsyraTreeOperatorEEnum, AtsyraTreeOperator.UNKNOWN);
		addEEnumLiteral(atsyraTreeOperatorEEnum, AtsyraTreeOperator.OR);
		addEEnumLiteral(atsyraTreeOperatorEEnum, AtsyraTreeOperator.SAND);
		addEEnumLiteral(atsyraTreeOperatorEEnum, AtsyraTreeOperator.AND);

		// Create resource
		createResource(eNS_URI);
	}

} //AtsyragoalPackageImpl
