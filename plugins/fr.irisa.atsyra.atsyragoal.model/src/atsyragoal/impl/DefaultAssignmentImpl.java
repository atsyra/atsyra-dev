/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.impl;

import atsyragoal.AtsyragoalPackage;
import atsyragoal.DefaultAssignment;
import atsyragoal.SystemFeature;
import atsyragoal.TypedElement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Default Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.impl.DefaultAssignmentImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link atsyragoal.impl.DefaultAssignmentImpl#getAssignedValue <em>Assigned Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DefaultAssignmentImpl extends MinimalEObjectImpl.Container implements DefaultAssignment {
	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected SystemFeature target;

	/**
	 * The cached value of the '{@link #getAssignedValue() <em>Assigned Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignedValue()
	 * @generated
	 * @ordered
	 */
	protected TypedElement assignedValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AtsyragoalPackage.Literals.DEFAULT_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemFeature getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (SystemFeature)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AtsyragoalPackage.DEFAULT_ASSIGNMENT__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemFeature basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(SystemFeature newTarget) {
		SystemFeature oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.DEFAULT_ASSIGNMENT__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypedElement getAssignedValue() {
		if (assignedValue != null && assignedValue.eIsProxy()) {
			InternalEObject oldAssignedValue = (InternalEObject)assignedValue;
			assignedValue = (TypedElement)eResolveProxy(oldAssignedValue);
			if (assignedValue != oldAssignedValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AtsyragoalPackage.DEFAULT_ASSIGNMENT__ASSIGNED_VALUE, oldAssignedValue, assignedValue));
			}
		}
		return assignedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypedElement basicGetAssignedValue() {
		return assignedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignedValue(TypedElement newAssignedValue) {
		TypedElement oldAssignedValue = assignedValue;
		assignedValue = newAssignedValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.DEFAULT_ASSIGNMENT__ASSIGNED_VALUE, oldAssignedValue, assignedValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT__ASSIGNED_VALUE:
				if (resolve) return getAssignedValue();
				return basicGetAssignedValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT__TARGET:
				setTarget((SystemFeature)newValue);
				return;
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT__ASSIGNED_VALUE:
				setAssignedValue((TypedElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT__TARGET:
				setTarget((SystemFeature)null);
				return;
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT__ASSIGNED_VALUE:
				setAssignedValue((TypedElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT__TARGET:
				return target != null;
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT__ASSIGNED_VALUE:
				return assignedValue != null;
		}
		return super.eIsSet(featureID);
	}

} //DefaultAssignmentImpl
