/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.impl;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import atsyragoal.AbstractAtsyraTree;
import atsyragoal.AtsyraTree;
import atsyragoal.AtsyraTreeReference;
import atsyragoal.AtsyragoalPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Atsyra Tree Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.impl.AtsyraTreeReferenceImpl#getReferencedTree <em>Referenced Tree</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AtsyraTreeReferenceImpl extends AbstractAtsyraTreeImpl implements AtsyraTreeReference {
	/**
	 * The cached value of the '{@link #getReferencedTree() <em>Referenced Tree</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedTree()
	 * @generated
	 * @ordered
	 */
	protected AbstractAtsyraTree referencedTree;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtsyraTreeReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AtsyragoalPackage.Literals.ATSYRA_TREE_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getQualifiedName() {
		String referenceTreeQName;
		int posInContainer = ECollections.indexOf((List<?>) (this.eContainer().eGet(this.eContainmentFeature())), this, 0);
		
		if (referencedTree != null) {
			referenceTreeQName = referencedTree.getQualifiedName();
		} else {
			if(this.eContainer() instanceof AtsyraTree) {
				AtsyraTree containerTree = (AtsyraTree)this.eContainer();
				referenceTreeQName = containerTree.getName() + "_operands_" + posInContainer;			
			} else {
				// should never occur, return the position in container
				referenceTreeQName = this.eContainer().eClass().getName() + 
						"_" + this.eContainmentFeature().getName() + 
						"_" + posInContainer;	
			}
		}
		if(this.eContainer() instanceof AbstractAtsyraTree) {
			return ((AbstractAtsyraTree)this.eContainer()).getQualifiedName()+".["+posInContainer+"]"+referenceTreeQName;
		}
		// this should normally never occurs since AtsyraGoalModel contains only AtsyraTree and not AbstractAtsyraTree
		return referenceTreeQName;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractAtsyraTree getReferencedTree() {
		if (referencedTree != null && referencedTree.eIsProxy()) {
			InternalEObject oldReferencedTree = (InternalEObject)referencedTree;
			referencedTree = (AbstractAtsyraTree)eResolveProxy(oldReferencedTree);
			if (referencedTree != oldReferencedTree) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AtsyragoalPackage.ATSYRA_TREE_REFERENCE__REFERENCED_TREE, oldReferencedTree, referencedTree));
			}
		}
		return referencedTree;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractAtsyraTree basicGetReferencedTree() {
		return referencedTree;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencedTree(AbstractAtsyraTree newReferencedTree) {
		AbstractAtsyraTree oldReferencedTree = referencedTree;
		referencedTree = newReferencedTree;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AtsyragoalPackage.ATSYRA_TREE_REFERENCE__REFERENCED_TREE, oldReferencedTree, referencedTree));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_TREE_REFERENCE__REFERENCED_TREE:
				if (resolve) return getReferencedTree();
				return basicGetReferencedTree();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_TREE_REFERENCE__REFERENCED_TREE:
				setReferencedTree((AbstractAtsyraTree)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_TREE_REFERENCE__REFERENCED_TREE:
				setReferencedTree((AbstractAtsyraTree)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AtsyragoalPackage.ATSYRA_TREE_REFERENCE__REFERENCED_TREE:
				return referencedTree != null;
		}
		return super.eIsSet(featureID);
	}

} //AtsyraTreeReferenceImpl
