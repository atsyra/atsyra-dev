/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atsyra Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.AtsyraGoal#getPrecondition <em>Precondition</em>}</li>
 *   <li>{@link atsyragoal.AtsyraGoal#getPostcondition <em>Postcondition</em>}</li>
 *   <li>{@link atsyragoal.AtsyraGoal#getName <em>Name</em>}</li>
 *   <li>{@link atsyragoal.AtsyraGoal#getDefaultUsedInPre <em>Default Used In Pre</em>}</li>
 *   <li>{@link atsyragoal.AtsyraGoal#getDefaultUsedInPost <em>Default Used In Post</em>}</li>
 * </ul>
 *
 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoal()
 * @model
 * @generated
 */
public interface AtsyraGoal extends EObject {
	/**
	 * Returns the value of the '<em><b>Precondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Precondition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precondition</em>' containment reference.
	 * @see #setPrecondition(GoalCondition)
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoal_Precondition()
	 * @model containment="true"
	 * @generated
	 */
	GoalCondition getPrecondition();

	/**
	 * Sets the value of the '{@link atsyragoal.AtsyraGoal#getPrecondition <em>Precondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Precondition</em>' containment reference.
	 * @see #getPrecondition()
	 * @generated
	 */
	void setPrecondition(GoalCondition value);

	/**
	 * Returns the value of the '<em><b>Postcondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Postcondition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postcondition</em>' containment reference.
	 * @see #setPostcondition(GoalCondition)
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoal_Postcondition()
	 * @model containment="true"
	 * @generated
	 */
	GoalCondition getPostcondition();

	/**
	 * Sets the value of the '{@link atsyragoal.AtsyraGoal#getPostcondition <em>Postcondition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postcondition</em>' containment reference.
	 * @see #getPostcondition()
	 * @generated
	 */
	void setPostcondition(GoalCondition value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoal_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link atsyragoal.AtsyraGoal#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Default Used In Pre</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Used In Pre</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Used In Pre</em>' reference.
	 * @see #setDefaultUsedInPre(DefaultValues)
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoal_DefaultUsedInPre()
	 * @model keys="name"
	 * @generated
	 */
	DefaultValues getDefaultUsedInPre();

	/**
	 * Sets the value of the '{@link atsyragoal.AtsyraGoal#getDefaultUsedInPre <em>Default Used In Pre</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Used In Pre</em>' reference.
	 * @see #getDefaultUsedInPre()
	 * @generated
	 */
	void setDefaultUsedInPre(DefaultValues value);

	/**
	 * Returns the value of the '<em><b>Default Used In Post</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Used In Post</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Used In Post</em>' reference.
	 * @see #setDefaultUsedInPost(DefaultValues)
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoal_DefaultUsedInPost()
	 * @model keys="name"
	 * @generated
	 */
	DefaultValues getDefaultUsedInPost();

	/**
	 * Sets the value of the '{@link atsyragoal.AtsyraGoal#getDefaultUsedInPost <em>Default Used In Post</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Used In Post</em>' reference.
	 * @see #getDefaultUsedInPost()
	 * @generated
	 */
	void setDefaultUsedInPost(DefaultValues value);

} // AtsyraGoal
