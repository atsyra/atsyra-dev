package atsyragoal.util

import atsyragoal.AbstractAtsyraTree
import atsyragoal.AtsyraTree
import atsyragoal.AtsyraGoal
import java.util.List
import atsyragoal.AtsyraTreeReference

class AtsyragoalHelper {
	static def String getFullQualifiedName(AbstractAtsyraTree t) {
		var String prefix =  "";
		if( t.eContainer() instanceof AbstractAtsyraTree) {
			prefix = getFullQualifiedName( t.eContainer() as AbstractAtsyraTree) +".";
		}
		return prefix + t.getQualifiedName();
	}
	
	
	static def List<AtsyraGoal> getOperandsGoals(AtsyraTree t){
		val res = newArrayList
		res.addAll(t.operands.map[at | at.getGoal])
		res	
	}
	
	
	static dispatch def AtsyraGoal getGoal(AtsyraTreeReference t){
		t.referencedTree.goal
	}
	static dispatch def AtsyraGoal getGoal(AtsyraTree t){
		t.mainGoal
	}
	
}