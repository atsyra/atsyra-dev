/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.util;

import atsyragoal.*;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see atsyragoal.AtsyragoalPackage
 * @generated
 */
public class AtsyragoalSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AtsyragoalPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyragoalSwitch() {
		if (modelPackage == null) {
			modelPackage = AtsyragoalPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case AtsyragoalPackage.ATSYRA_GOAL: {
				AtsyraGoal atsyraGoal = (AtsyraGoal)theEObject;
				T result = caseAtsyraGoal(atsyraGoal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.GOAL_CONDITION: {
				GoalCondition goalCondition = (GoalCondition)theEObject;
				T result = caseGoalCondition(goalCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.ATOMIC_CONDITION: {
				AtomicCondition atomicCondition = (AtomicCondition)theEObject;
				T result = caseAtomicCondition(atomicCondition);
				if (result == null) result = caseGoalCondition(atomicCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.CONDITION_OPERATOR: {
				ConditionOperator conditionOperator = (ConditionOperator)theEObject;
				T result = caseConditionOperator(conditionOperator);
				if (result == null) result = caseGoalCondition(conditionOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.AND_CONDITION: {
				AndCondition andCondition = (AndCondition)theEObject;
				T result = caseAndCondition(andCondition);
				if (result == null) result = caseConditionOperator(andCondition);
				if (result == null) result = caseGoalCondition(andCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.OR_CONDITION: {
				OrCondition orCondition = (OrCondition)theEObject;
				T result = caseOrCondition(orCondition);
				if (result == null) result = caseConditionOperator(orCondition);
				if (result == null) result = caseGoalCondition(orCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.NOT_CONDITION: {
				NotCondition notCondition = (NotCondition)theEObject;
				T result = caseNotCondition(notCondition);
				if (result == null) result = caseConditionOperator(notCondition);
				if (result == null) result = caseGoalCondition(notCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL: {
				AtsyraGoalModel atsyraGoalModel = (AtsyraGoalModel)theEObject;
				T result = caseAtsyraGoalModel(atsyraGoalModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.EQUALS_CONDITION: {
				EqualsCondition equalsCondition = (EqualsCondition)theEObject;
				T result = caseEqualsCondition(equalsCondition);
				if (result == null) result = caseAtomicCondition(equalsCondition);
				if (result == null) result = caseGoalCondition(equalsCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.TYPED_ELEMENT: {
				TypedElement typedElement = (TypedElement)theEObject;
				T result = caseTypedElement(typedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.TYPE: {
				Type type = (Type)theEObject;
				T result = caseType(type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.INTERNAL_TYPE: {
				InternalType internalType = (InternalType)theEObject;
				T result = caseInternalType(internalType);
				if (result == null) result = caseType(internalType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.BOOLEAN_LITERAL: {
				BooleanLiteral booleanLiteral = (BooleanLiteral)theEObject;
				T result = caseBooleanLiteral(booleanLiteral);
				if (result == null) result = caseTypedElement(booleanLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.SYSTEM_TYPE: {
				SystemType systemType = (SystemType)theEObject;
				T result = caseSystemType(systemType);
				if (result == null) result = caseType(systemType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.SYSTEM_FEATURE: {
				SystemFeature systemFeature = (SystemFeature)theEObject;
				T result = caseSystemFeature(systemFeature);
				if (result == null) result = caseTypedElement(systemFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.SYSTEM_CONST_FEATURE: {
				SystemConstFeature systemConstFeature = (SystemConstFeature)theEObject;
				T result = caseSystemConstFeature(systemConstFeature);
				if (result == null) result = caseTypedElement(systemConstFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.BOOLEAN_SYSTEM_CONDITION: {
				BooleanSystemCondition booleanSystemCondition = (BooleanSystemCondition)theEObject;
				T result = caseBooleanSystemCondition(booleanSystemCondition);
				if (result == null) result = caseAtomicCondition(booleanSystemCondition);
				if (result == null) result = caseGoalCondition(booleanSystemCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.IMPORT: {
				Import import_ = (Import)theEObject;
				T result = caseImport(import_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.ATSYRA_TREE: {
				AtsyraTree atsyraTree = (AtsyraTree)theEObject;
				T result = caseAtsyraTree(atsyraTree);
				if (result == null) result = caseAbstractAtsyraTree(atsyraTree);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.ABSTRACT_ATSYRA_TREE: {
				AbstractAtsyraTree abstractAtsyraTree = (AbstractAtsyraTree)theEObject;
				T result = caseAbstractAtsyraTree(abstractAtsyraTree);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.ATSYRA_TREE_REFERENCE: {
				AtsyraTreeReference atsyraTreeReference = (AtsyraTreeReference)theEObject;
				T result = caseAtsyraTreeReference(atsyraTreeReference);
				if (result == null) result = caseAbstractAtsyraTree(atsyraTreeReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.DEFAULT_VALUES: {
				DefaultValues defaultValues = (DefaultValues)theEObject;
				T result = caseDefaultValues(defaultValues);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AtsyragoalPackage.DEFAULT_ASSIGNMENT: {
				DefaultAssignment defaultAssignment = (DefaultAssignment)theEObject;
				T result = caseDefaultAssignment(defaultAssignment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atsyra Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atsyra Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtsyraGoal(AtsyraGoal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoalCondition(GoalCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atomic Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atomic Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtomicCondition(AtomicCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionOperator(ConditionOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>And Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAndCondition(AndCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrCondition(OrCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotCondition(NotCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atsyra Goal Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atsyra Goal Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtsyraGoalModel(AtsyraGoalModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equals Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equals Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEqualsCondition(EqualsCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Typed Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Typed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypedElement(TypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseType(Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInternalType(InternalType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanLiteral(BooleanLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemType(SystemType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemFeature(SystemFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Const Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Const Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemConstFeature(SystemConstFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean System Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean System Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanSystemCondition(BooleanSystemCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImport(Import object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atsyra Tree</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atsyra Tree</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtsyraTree(AtsyraTree object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Atsyra Tree</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Atsyra Tree</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractAtsyraTree(AbstractAtsyraTree object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atsyra Tree Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atsyra Tree Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtsyraTreeReference(AtsyraTreeReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Default Values</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Default Values</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefaultValues(DefaultValues object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Default Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Default Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefaultAssignment(DefaultAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //AtsyragoalSwitch
