/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.util;

import atsyragoal.*;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see atsyragoal.AtsyragoalPackage
 * @generated
 */
public class AtsyragoalAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AtsyragoalPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyragoalAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = AtsyragoalPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtsyragoalSwitch<Adapter> modelSwitch =
		new AtsyragoalSwitch<Adapter>() {
			@Override
			public Adapter caseAtsyraGoal(AtsyraGoal object) {
				return createAtsyraGoalAdapter();
			}
			@Override
			public Adapter caseGoalCondition(GoalCondition object) {
				return createGoalConditionAdapter();
			}
			@Override
			public Adapter caseAtomicCondition(AtomicCondition object) {
				return createAtomicConditionAdapter();
			}
			@Override
			public Adapter caseConditionOperator(ConditionOperator object) {
				return createConditionOperatorAdapter();
			}
			@Override
			public Adapter caseAndCondition(AndCondition object) {
				return createAndConditionAdapter();
			}
			@Override
			public Adapter caseOrCondition(OrCondition object) {
				return createOrConditionAdapter();
			}
			@Override
			public Adapter caseNotCondition(NotCondition object) {
				return createNotConditionAdapter();
			}
			@Override
			public Adapter caseAtsyraGoalModel(AtsyraGoalModel object) {
				return createAtsyraGoalModelAdapter();
			}
			@Override
			public Adapter caseEqualsCondition(EqualsCondition object) {
				return createEqualsConditionAdapter();
			}
			@Override
			public Adapter caseTypedElement(TypedElement object) {
				return createTypedElementAdapter();
			}
			@Override
			public Adapter caseType(Type object) {
				return createTypeAdapter();
			}
			@Override
			public Adapter caseInternalType(InternalType object) {
				return createInternalTypeAdapter();
			}
			@Override
			public Adapter caseBooleanLiteral(BooleanLiteral object) {
				return createBooleanLiteralAdapter();
			}
			@Override
			public Adapter caseSystemType(SystemType object) {
				return createSystemTypeAdapter();
			}
			@Override
			public Adapter caseSystemFeature(SystemFeature object) {
				return createSystemFeatureAdapter();
			}
			@Override
			public Adapter caseSystemConstFeature(SystemConstFeature object) {
				return createSystemConstFeatureAdapter();
			}
			@Override
			public Adapter caseBooleanSystemCondition(BooleanSystemCondition object) {
				return createBooleanSystemConditionAdapter();
			}
			@Override
			public Adapter caseImport(Import object) {
				return createImportAdapter();
			}
			@Override
			public Adapter caseAtsyraTree(AtsyraTree object) {
				return createAtsyraTreeAdapter();
			}
			@Override
			public Adapter caseAbstractAtsyraTree(AbstractAtsyraTree object) {
				return createAbstractAtsyraTreeAdapter();
			}
			@Override
			public Adapter caseAtsyraTreeReference(AtsyraTreeReference object) {
				return createAtsyraTreeReferenceAdapter();
			}
			@Override
			public Adapter caseDefaultValues(DefaultValues object) {
				return createDefaultValuesAdapter();
			}
			@Override
			public Adapter caseDefaultAssignment(DefaultAssignment object) {
				return createDefaultAssignmentAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.AtsyraGoal <em>Atsyra Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.AtsyraGoal
	 * @generated
	 */
	public Adapter createAtsyraGoalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.GoalCondition <em>Goal Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.GoalCondition
	 * @generated
	 */
	public Adapter createGoalConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.AtomicCondition <em>Atomic Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.AtomicCondition
	 * @generated
	 */
	public Adapter createAtomicConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.ConditionOperator <em>Condition Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.ConditionOperator
	 * @generated
	 */
	public Adapter createConditionOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.AndCondition <em>And Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.AndCondition
	 * @generated
	 */
	public Adapter createAndConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.OrCondition <em>Or Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.OrCondition
	 * @generated
	 */
	public Adapter createOrConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.NotCondition <em>Not Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.NotCondition
	 * @generated
	 */
	public Adapter createNotConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.AtsyraGoalModel <em>Atsyra Goal Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.AtsyraGoalModel
	 * @generated
	 */
	public Adapter createAtsyraGoalModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.EqualsCondition <em>Equals Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.EqualsCondition
	 * @generated
	 */
	public Adapter createEqualsConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.TypedElement <em>Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.TypedElement
	 * @generated
	 */
	public Adapter createTypedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.Type
	 * @generated
	 */
	public Adapter createTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.InternalType <em>Internal Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.InternalType
	 * @generated
	 */
	public Adapter createInternalTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.BooleanLiteral <em>Boolean Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.BooleanLiteral
	 * @generated
	 */
	public Adapter createBooleanLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.SystemType <em>System Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.SystemType
	 * @generated
	 */
	public Adapter createSystemTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.SystemFeature <em>System Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.SystemFeature
	 * @generated
	 */
	public Adapter createSystemFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.SystemConstFeature <em>System Const Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.SystemConstFeature
	 * @generated
	 */
	public Adapter createSystemConstFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.BooleanSystemCondition <em>Boolean System Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.BooleanSystemCondition
	 * @generated
	 */
	public Adapter createBooleanSystemConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.Import
	 * @generated
	 */
	public Adapter createImportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.AtsyraTree <em>Atsyra Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.AtsyraTree
	 * @generated
	 */
	public Adapter createAtsyraTreeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.AbstractAtsyraTree <em>Abstract Atsyra Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.AbstractAtsyraTree
	 * @generated
	 */
	public Adapter createAbstractAtsyraTreeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.AtsyraTreeReference <em>Atsyra Tree Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.AtsyraTreeReference
	 * @generated
	 */
	public Adapter createAtsyraTreeReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.DefaultValues <em>Default Values</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.DefaultValues
	 * @generated
	 */
	public Adapter createDefaultValuesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link atsyragoal.DefaultAssignment <em>Default Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see atsyragoal.DefaultAssignment
	 * @generated
	 */
	public Adapter createDefaultAssignmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //AtsyragoalAdapterFactory
