/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equals Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.EqualsCondition#getSource <em>Source</em>}</li>
 *   <li>{@link atsyragoal.EqualsCondition#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see atsyragoal.AtsyragoalPackage#getEqualsCondition()
 * @model
 * @generated
 */
public interface EqualsCondition extends AtomicCondition {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(TypedElement)
	 * @see atsyragoal.AtsyragoalPackage#getEqualsCondition_Source()
	 * @model required="true"
	 * @generated
	 */
	TypedElement getSource();

	/**
	 * Sets the value of the '{@link atsyragoal.EqualsCondition#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(TypedElement value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(TypedElement)
	 * @see atsyragoal.AtsyragoalPackage#getEqualsCondition_Target()
	 * @model required="true"
	 * @generated
	 */
	TypedElement getTarget();

	/**
	 * Sets the value of the '{@link atsyragoal.EqualsCondition#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(TypedElement value);

} // EqualsCondition
