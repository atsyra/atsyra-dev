/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see atsyragoal.AtsyragoalFactory
 * @model kind="package"
 * @generated
 */
public interface AtsyragoalPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "atsyragoal";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/atsyra/atsyragoal";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "atsyragoal";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AtsyragoalPackage eINSTANCE = atsyragoal.impl.AtsyragoalPackageImpl.init();

	/**
	 * The meta object id for the '{@link atsyragoal.impl.AtsyraGoalImpl <em>Atsyra Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.AtsyraGoalImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraGoal()
	 * @generated
	 */
	int ATSYRA_GOAL = 0;

	/**
	 * The feature id for the '<em><b>Precondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL__PRECONDITION = 0;

	/**
	 * The feature id for the '<em><b>Postcondition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL__POSTCONDITION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL__NAME = 2;

	/**
	 * The feature id for the '<em><b>Default Used In Pre</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL__DEFAULT_USED_IN_PRE = 3;

	/**
	 * The feature id for the '<em><b>Default Used In Post</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL__DEFAULT_USED_IN_POST = 4;

	/**
	 * The number of structural features of the '<em>Atsyra Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Atsyra Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.GoalConditionImpl <em>Goal Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.GoalConditionImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getGoalCondition()
	 * @generated
	 */
	int GOAL_CONDITION = 1;

	/**
	 * The number of structural features of the '<em>Goal Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_CONDITION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Goal Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_CONDITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.AtomicConditionImpl <em>Atomic Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.AtomicConditionImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtomicCondition()
	 * @generated
	 */
	int ATOMIC_CONDITION = 2;

	/**
	 * The number of structural features of the '<em>Atomic Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_CONDITION_FEATURE_COUNT = GOAL_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Atomic Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_CONDITION_OPERATION_COUNT = GOAL_CONDITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.ConditionOperatorImpl <em>Condition Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.ConditionOperatorImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getConditionOperator()
	 * @generated
	 */
	int CONDITION_OPERATOR = 3;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_OPERATOR__OPERANDS = GOAL_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Condition Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_OPERATOR_FEATURE_COUNT = GOAL_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Condition Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_OPERATOR_OPERATION_COUNT = GOAL_CONDITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.AndConditionImpl <em>And Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.AndConditionImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAndCondition()
	 * @generated
	 */
	int AND_CONDITION = 4;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_CONDITION__OPERANDS = CONDITION_OPERATOR__OPERANDS;

	/**
	 * The number of structural features of the '<em>And Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_CONDITION_FEATURE_COUNT = CONDITION_OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>And Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_CONDITION_OPERATION_COUNT = CONDITION_OPERATOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.OrConditionImpl <em>Or Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.OrConditionImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getOrCondition()
	 * @generated
	 */
	int OR_CONDITION = 5;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_CONDITION__OPERANDS = CONDITION_OPERATOR__OPERANDS;

	/**
	 * The number of structural features of the '<em>Or Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_CONDITION_FEATURE_COUNT = CONDITION_OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Or Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_CONDITION_OPERATION_COUNT = CONDITION_OPERATOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.NotConditionImpl <em>Not Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.NotConditionImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getNotCondition()
	 * @generated
	 */
	int NOT_CONDITION = 6;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_CONDITION__OPERANDS = CONDITION_OPERATOR__OPERANDS;

	/**
	 * The number of structural features of the '<em>Not Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_CONDITION_FEATURE_COUNT = CONDITION_OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Not Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_CONDITION_OPERATION_COUNT = CONDITION_OPERATOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.AtsyraGoalModelImpl <em>Atsyra Goal Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.AtsyraGoalModelImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraGoalModel()
	 * @generated
	 */
	int ATSYRA_GOAL_MODEL = 7;

	/**
	 * The feature id for the '<em><b>Atsyragoals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_MODEL__ATSYRAGOALS = 0;

	/**
	 * The feature id for the '<em><b>Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_MODEL__TYPES = 1;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_MODEL__IMPORTS = 2;

	/**
	 * The feature id for the '<em><b>Typed Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_MODEL__TYPED_ELEMENTS = 3;

	/**
	 * The feature id for the '<em><b>Trees</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_MODEL__TREES = 4;

	/**
	 * The feature id for the '<em><b>Default Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_MODEL__DEFAULT_VALUES = 5;

	/**
	 * The number of structural features of the '<em>Atsyra Goal Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_MODEL_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Atsyra Goal Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_GOAL_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.EqualsConditionImpl <em>Equals Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.EqualsConditionImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getEqualsCondition()
	 * @generated
	 */
	int EQUALS_CONDITION = 8;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALS_CONDITION__SOURCE = ATOMIC_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALS_CONDITION__TARGET = ATOMIC_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Equals Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALS_CONDITION_FEATURE_COUNT = ATOMIC_CONDITION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Equals Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALS_CONDITION_OPERATION_COUNT = ATOMIC_CONDITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.TypedElementImpl <em>Typed Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.TypedElementImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getTypedElement()
	 * @generated
	 */
	int TYPED_ELEMENT = 9;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__NAME = 1;

	/**
	 * The number of structural features of the '<em>Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.TypeImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getType()
	 * @generated
	 */
	int TYPE = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.InternalTypeImpl <em>Internal Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.InternalTypeImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getInternalType()
	 * @generated
	 */
	int INTERNAL_TYPE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_TYPE__NAME = TYPE__NAME;

	/**
	 * The number of structural features of the '<em>Internal Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Internal Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_TYPE_OPERATION_COUNT = TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.BooleanLiteralImpl <em>Boolean Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.BooleanLiteralImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getBooleanLiteral()
	 * @generated
	 */
	int BOOLEAN_LITERAL = 12;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_LITERAL__TYPE = TYPED_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_LITERAL__NAME = TYPED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Boolean Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_LITERAL_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Boolean Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_LITERAL_OPERATION_COUNT = TYPED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.SystemTypeImpl <em>System Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.SystemTypeImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getSystemType()
	 * @generated
	 */
	int SYSTEM_TYPE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TYPE__NAME = TYPE__NAME;

	/**
	 * The number of structural features of the '<em>System Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>System Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TYPE_OPERATION_COUNT = TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.SystemFeatureImpl <em>System Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.SystemFeatureImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getSystemFeature()
	 * @generated
	 */
	int SYSTEM_FEATURE = 14;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FEATURE__TYPE = TYPED_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FEATURE__NAME = TYPED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>System Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FEATURE_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>System Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FEATURE_OPERATION_COUNT = TYPED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.SystemConstFeatureImpl <em>System Const Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.SystemConstFeatureImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getSystemConstFeature()
	 * @generated
	 */
	int SYSTEM_CONST_FEATURE = 15;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_CONST_FEATURE__TYPE = TYPED_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_CONST_FEATURE__NAME = TYPED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>System Const Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_CONST_FEATURE_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>System Const Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_CONST_FEATURE_OPERATION_COUNT = TYPED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.BooleanSystemConditionImpl <em>Boolean System Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.BooleanSystemConditionImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getBooleanSystemCondition()
	 * @generated
	 */
	int BOOLEAN_SYSTEM_CONDITION = 16;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_SYSTEM_CONDITION__SOURCE = ATOMIC_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean System Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_SYSTEM_CONDITION_FEATURE_COUNT = ATOMIC_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Boolean System Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_SYSTEM_CONDITION_OPERATION_COUNT = ATOMIC_CONDITION_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link atsyragoal.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.ImportImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 17;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__IMPORT_URI = 0;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link atsyragoal.impl.AbstractAtsyraTreeImpl <em>Abstract Atsyra Tree</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.AbstractAtsyraTreeImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAbstractAtsyraTree()
	 * @generated
	 */
	int ABSTRACT_ATSYRA_TREE = 19;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ATSYRA_TREE__QUALIFIED_NAME = 0;

	/**
	 * The number of structural features of the '<em>Abstract Atsyra Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ATSYRA_TREE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Abstract Atsyra Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ATSYRA_TREE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.AtsyraTreeImpl <em>Atsyra Tree</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.AtsyraTreeImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraTree()
	 * @generated
	 */
	int ATSYRA_TREE = 18;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE__QUALIFIED_NAME = ABSTRACT_ATSYRA_TREE__QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE__NAME = ABSTRACT_ATSYRA_TREE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Main Goal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE__MAIN_GOAL = ABSTRACT_ATSYRA_TREE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE__OPERATOR = ABSTRACT_ATSYRA_TREE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE__OPERANDS = ABSTRACT_ATSYRA_TREE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Atsyra Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE_FEATURE_COUNT = ABSTRACT_ATSYRA_TREE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Atsyra Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE_OPERATION_COUNT = ABSTRACT_ATSYRA_TREE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.AtsyraTreeReferenceImpl <em>Atsyra Tree Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.AtsyraTreeReferenceImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraTreeReference()
	 * @generated
	 */
	int ATSYRA_TREE_REFERENCE = 20;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE_REFERENCE__QUALIFIED_NAME = ABSTRACT_ATSYRA_TREE__QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Referenced Tree</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE_REFERENCE__REFERENCED_TREE = ABSTRACT_ATSYRA_TREE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Atsyra Tree Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE_REFERENCE_FEATURE_COUNT = ABSTRACT_ATSYRA_TREE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Atsyra Tree Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATSYRA_TREE_REFERENCE_OPERATION_COUNT = ABSTRACT_ATSYRA_TREE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.DefaultValuesImpl <em>Default Values</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.DefaultValuesImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getDefaultValues()
	 * @generated
	 */
	int DEFAULT_VALUES = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_VALUES__NAME = 0;

	/**
	 * The feature id for the '<em><b>Default Value Assignments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_VALUES__DEFAULT_VALUE_ASSIGNMENTS = 1;

	/**
	 * The number of structural features of the '<em>Default Values</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_VALUES_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Default Values</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_VALUES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link atsyragoal.impl.DefaultAssignmentImpl <em>Default Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.impl.DefaultAssignmentImpl
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getDefaultAssignment()
	 * @generated
	 */
	int DEFAULT_ASSIGNMENT = 22;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_ASSIGNMENT__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Assigned Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_ASSIGNMENT__ASSIGNED_VALUE = 1;

	/**
	 * The number of structural features of the '<em>Default Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_ASSIGNMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Default Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_ASSIGNMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link atsyragoal.AtsyraTreeOperator <em>Atsyra Tree Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see atsyragoal.AtsyraTreeOperator
	 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraTreeOperator()
	 * @generated
	 */
	int ATSYRA_TREE_OPERATOR = 23;

	/**
	 * Returns the meta object for class '{@link atsyragoal.AtsyraGoal <em>Atsyra Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atsyra Goal</em>'.
	 * @see atsyragoal.AtsyraGoal
	 * @generated
	 */
	EClass getAtsyraGoal();

	/**
	 * Returns the meta object for the containment reference '{@link atsyragoal.AtsyraGoal#getPrecondition <em>Precondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Precondition</em>'.
	 * @see atsyragoal.AtsyraGoal#getPrecondition()
	 * @see #getAtsyraGoal()
	 * @generated
	 */
	EReference getAtsyraGoal_Precondition();

	/**
	 * Returns the meta object for the containment reference '{@link atsyragoal.AtsyraGoal#getPostcondition <em>Postcondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Postcondition</em>'.
	 * @see atsyragoal.AtsyraGoal#getPostcondition()
	 * @see #getAtsyraGoal()
	 * @generated
	 */
	EReference getAtsyraGoal_Postcondition();

	/**
	 * Returns the meta object for the attribute '{@link atsyragoal.AtsyraGoal#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see atsyragoal.AtsyraGoal#getName()
	 * @see #getAtsyraGoal()
	 * @generated
	 */
	EAttribute getAtsyraGoal_Name();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.AtsyraGoal#getDefaultUsedInPre <em>Default Used In Pre</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Default Used In Pre</em>'.
	 * @see atsyragoal.AtsyraGoal#getDefaultUsedInPre()
	 * @see #getAtsyraGoal()
	 * @generated
	 */
	EReference getAtsyraGoal_DefaultUsedInPre();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.AtsyraGoal#getDefaultUsedInPost <em>Default Used In Post</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Default Used In Post</em>'.
	 * @see atsyragoal.AtsyraGoal#getDefaultUsedInPost()
	 * @see #getAtsyraGoal()
	 * @generated
	 */
	EReference getAtsyraGoal_DefaultUsedInPost();

	/**
	 * Returns the meta object for class '{@link atsyragoal.GoalCondition <em>Goal Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal Condition</em>'.
	 * @see atsyragoal.GoalCondition
	 * @generated
	 */
	EClass getGoalCondition();

	/**
	 * Returns the meta object for class '{@link atsyragoal.AtomicCondition <em>Atomic Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atomic Condition</em>'.
	 * @see atsyragoal.AtomicCondition
	 * @generated
	 */
	EClass getAtomicCondition();

	/**
	 * Returns the meta object for class '{@link atsyragoal.ConditionOperator <em>Condition Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Operator</em>'.
	 * @see atsyragoal.ConditionOperator
	 * @generated
	 */
	EClass getConditionOperator();

	/**
	 * Returns the meta object for the containment reference list '{@link atsyragoal.ConditionOperator#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operands</em>'.
	 * @see atsyragoal.ConditionOperator#getOperands()
	 * @see #getConditionOperator()
	 * @generated
	 */
	EReference getConditionOperator_Operands();

	/**
	 * Returns the meta object for class '{@link atsyragoal.AndCondition <em>And Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Condition</em>'.
	 * @see atsyragoal.AndCondition
	 * @generated
	 */
	EClass getAndCondition();

	/**
	 * Returns the meta object for class '{@link atsyragoal.OrCondition <em>Or Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Condition</em>'.
	 * @see atsyragoal.OrCondition
	 * @generated
	 */
	EClass getOrCondition();

	/**
	 * Returns the meta object for class '{@link atsyragoal.NotCondition <em>Not Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not Condition</em>'.
	 * @see atsyragoal.NotCondition
	 * @generated
	 */
	EClass getNotCondition();

	/**
	 * Returns the meta object for class '{@link atsyragoal.AtsyraGoalModel <em>Atsyra Goal Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atsyra Goal Model</em>'.
	 * @see atsyragoal.AtsyraGoalModel
	 * @generated
	 */
	EClass getAtsyraGoalModel();

	/**
	 * Returns the meta object for the containment reference list '{@link atsyragoal.AtsyraGoalModel#getAtsyragoals <em>Atsyragoals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Atsyragoals</em>'.
	 * @see atsyragoal.AtsyraGoalModel#getAtsyragoals()
	 * @see #getAtsyraGoalModel()
	 * @generated
	 */
	EReference getAtsyraGoalModel_Atsyragoals();

	/**
	 * Returns the meta object for the containment reference list '{@link atsyragoal.AtsyraGoalModel#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Types</em>'.
	 * @see atsyragoal.AtsyraGoalModel#getTypes()
	 * @see #getAtsyraGoalModel()
	 * @generated
	 */
	EReference getAtsyraGoalModel_Types();

	/**
	 * Returns the meta object for the containment reference list '{@link atsyragoal.AtsyraGoalModel#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see atsyragoal.AtsyraGoalModel#getImports()
	 * @see #getAtsyraGoalModel()
	 * @generated
	 */
	EReference getAtsyraGoalModel_Imports();

	/**
	 * Returns the meta object for the containment reference list '{@link atsyragoal.AtsyraGoalModel#getTypedElements <em>Typed Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Typed Elements</em>'.
	 * @see atsyragoal.AtsyraGoalModel#getTypedElements()
	 * @see #getAtsyraGoalModel()
	 * @generated
	 */
	EReference getAtsyraGoalModel_TypedElements();

	/**
	 * Returns the meta object for the containment reference list '{@link atsyragoal.AtsyraGoalModel#getTrees <em>Trees</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Trees</em>'.
	 * @see atsyragoal.AtsyraGoalModel#getTrees()
	 * @see #getAtsyraGoalModel()
	 * @generated
	 */
	EReference getAtsyraGoalModel_Trees();

	/**
	 * Returns the meta object for the containment reference list '{@link atsyragoal.AtsyraGoalModel#getDefaultValues <em>Default Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Default Values</em>'.
	 * @see atsyragoal.AtsyraGoalModel#getDefaultValues()
	 * @see #getAtsyraGoalModel()
	 * @generated
	 */
	EReference getAtsyraGoalModel_DefaultValues();

	/**
	 * Returns the meta object for class '{@link atsyragoal.EqualsCondition <em>Equals Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equals Condition</em>'.
	 * @see atsyragoal.EqualsCondition
	 * @generated
	 */
	EClass getEqualsCondition();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.EqualsCondition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see atsyragoal.EqualsCondition#getSource()
	 * @see #getEqualsCondition()
	 * @generated
	 */
	EReference getEqualsCondition_Source();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.EqualsCondition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see atsyragoal.EqualsCondition#getTarget()
	 * @see #getEqualsCondition()
	 * @generated
	 */
	EReference getEqualsCondition_Target();

	/**
	 * Returns the meta object for class '{@link atsyragoal.TypedElement <em>Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Typed Element</em>'.
	 * @see atsyragoal.TypedElement
	 * @generated
	 */
	EClass getTypedElement();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.TypedElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see atsyragoal.TypedElement#getType()
	 * @see #getTypedElement()
	 * @generated
	 */
	EReference getTypedElement_Type();

	/**
	 * Returns the meta object for the attribute '{@link atsyragoal.TypedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see atsyragoal.TypedElement#getName()
	 * @see #getTypedElement()
	 * @generated
	 */
	EAttribute getTypedElement_Name();

	/**
	 * Returns the meta object for class '{@link atsyragoal.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see atsyragoal.Type
	 * @generated
	 */
	EClass getType();

	/**
	 * Returns the meta object for the attribute '{@link atsyragoal.Type#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see atsyragoal.Type#getName()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_Name();

	/**
	 * Returns the meta object for class '{@link atsyragoal.InternalType <em>Internal Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Internal Type</em>'.
	 * @see atsyragoal.InternalType
	 * @generated
	 */
	EClass getInternalType();

	/**
	 * Returns the meta object for class '{@link atsyragoal.BooleanLiteral <em>Boolean Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Literal</em>'.
	 * @see atsyragoal.BooleanLiteral
	 * @generated
	 */
	EClass getBooleanLiteral();

	/**
	 * Returns the meta object for class '{@link atsyragoal.SystemType <em>System Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Type</em>'.
	 * @see atsyragoal.SystemType
	 * @generated
	 */
	EClass getSystemType();

	/**
	 * Returns the meta object for class '{@link atsyragoal.SystemFeature <em>System Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Feature</em>'.
	 * @see atsyragoal.SystemFeature
	 * @generated
	 */
	EClass getSystemFeature();

	/**
	 * Returns the meta object for class '{@link atsyragoal.SystemConstFeature <em>System Const Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Const Feature</em>'.
	 * @see atsyragoal.SystemConstFeature
	 * @generated
	 */
	EClass getSystemConstFeature();

	/**
	 * Returns the meta object for class '{@link atsyragoal.BooleanSystemCondition <em>Boolean System Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean System Condition</em>'.
	 * @see atsyragoal.BooleanSystemCondition
	 * @generated
	 */
	EClass getBooleanSystemCondition();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.BooleanSystemCondition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see atsyragoal.BooleanSystemCondition#getSource()
	 * @see #getBooleanSystemCondition()
	 * @generated
	 */
	EReference getBooleanSystemCondition_Source();

	/**
	 * Returns the meta object for class '{@link atsyragoal.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see atsyragoal.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the attribute '{@link atsyragoal.Import#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see atsyragoal.Import#getImportURI()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_ImportURI();

	/**
	 * Returns the meta object for class '{@link atsyragoal.AtsyraTree <em>Atsyra Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atsyra Tree</em>'.
	 * @see atsyragoal.AtsyraTree
	 * @generated
	 */
	EClass getAtsyraTree();

	/**
	 * Returns the meta object for the attribute '{@link atsyragoal.AtsyraTree#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see atsyragoal.AtsyraTree#getName()
	 * @see #getAtsyraTree()
	 * @generated
	 */
	EAttribute getAtsyraTree_Name();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.AtsyraTree#getMainGoal <em>Main Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Main Goal</em>'.
	 * @see atsyragoal.AtsyraTree#getMainGoal()
	 * @see #getAtsyraTree()
	 * @generated
	 */
	EReference getAtsyraTree_MainGoal();

	/**
	 * Returns the meta object for the attribute '{@link atsyragoal.AtsyraTree#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see atsyragoal.AtsyraTree#getOperator()
	 * @see #getAtsyraTree()
	 * @generated
	 */
	EAttribute getAtsyraTree_Operator();

	/**
	 * Returns the meta object for the containment reference list '{@link atsyragoal.AtsyraTree#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operands</em>'.
	 * @see atsyragoal.AtsyraTree#getOperands()
	 * @see #getAtsyraTree()
	 * @generated
	 */
	EReference getAtsyraTree_Operands();

	/**
	 * Returns the meta object for class '{@link atsyragoal.AbstractAtsyraTree <em>Abstract Atsyra Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Atsyra Tree</em>'.
	 * @see atsyragoal.AbstractAtsyraTree
	 * @generated
	 */
	EClass getAbstractAtsyraTree();

	/**
	 * Returns the meta object for the attribute '{@link atsyragoal.AbstractAtsyraTree#getQualifiedName <em>Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Qualified Name</em>'.
	 * @see atsyragoal.AbstractAtsyraTree#getQualifiedName()
	 * @see #getAbstractAtsyraTree()
	 * @generated
	 */
	EAttribute getAbstractAtsyraTree_QualifiedName();

	/**
	 * Returns the meta object for class '{@link atsyragoal.AtsyraTreeReference <em>Atsyra Tree Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atsyra Tree Reference</em>'.
	 * @see atsyragoal.AtsyraTreeReference
	 * @generated
	 */
	EClass getAtsyraTreeReference();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.AtsyraTreeReference#getReferencedTree <em>Referenced Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Tree</em>'.
	 * @see atsyragoal.AtsyraTreeReference#getReferencedTree()
	 * @see #getAtsyraTreeReference()
	 * @generated
	 */
	EReference getAtsyraTreeReference_ReferencedTree();

	/**
	 * Returns the meta object for class '{@link atsyragoal.DefaultValues <em>Default Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Default Values</em>'.
	 * @see atsyragoal.DefaultValues
	 * @generated
	 */
	EClass getDefaultValues();

	/**
	 * Returns the meta object for the attribute '{@link atsyragoal.DefaultValues#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see atsyragoal.DefaultValues#getName()
	 * @see #getDefaultValues()
	 * @generated
	 */
	EAttribute getDefaultValues_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link atsyragoal.DefaultValues#getDefaultValueAssignments <em>Default Value Assignments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Default Value Assignments</em>'.
	 * @see atsyragoal.DefaultValues#getDefaultValueAssignments()
	 * @see #getDefaultValues()
	 * @generated
	 */
	EReference getDefaultValues_DefaultValueAssignments();

	/**
	 * Returns the meta object for class '{@link atsyragoal.DefaultAssignment <em>Default Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Default Assignment</em>'.
	 * @see atsyragoal.DefaultAssignment
	 * @generated
	 */
	EClass getDefaultAssignment();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.DefaultAssignment#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see atsyragoal.DefaultAssignment#getTarget()
	 * @see #getDefaultAssignment()
	 * @generated
	 */
	EReference getDefaultAssignment_Target();

	/**
	 * Returns the meta object for the reference '{@link atsyragoal.DefaultAssignment#getAssignedValue <em>Assigned Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Assigned Value</em>'.
	 * @see atsyragoal.DefaultAssignment#getAssignedValue()
	 * @see #getDefaultAssignment()
	 * @generated
	 */
	EReference getDefaultAssignment_AssignedValue();

	/**
	 * Returns the meta object for enum '{@link atsyragoal.AtsyraTreeOperator <em>Atsyra Tree Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Atsyra Tree Operator</em>'.
	 * @see atsyragoal.AtsyraTreeOperator
	 * @generated
	 */
	EEnum getAtsyraTreeOperator();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AtsyragoalFactory getAtsyragoalFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link atsyragoal.impl.AtsyraGoalImpl <em>Atsyra Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.AtsyraGoalImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraGoal()
		 * @generated
		 */
		EClass ATSYRA_GOAL = eINSTANCE.getAtsyraGoal();

		/**
		 * The meta object literal for the '<em><b>Precondition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL__PRECONDITION = eINSTANCE.getAtsyraGoal_Precondition();

		/**
		 * The meta object literal for the '<em><b>Postcondition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL__POSTCONDITION = eINSTANCE.getAtsyraGoal_Postcondition();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATSYRA_GOAL__NAME = eINSTANCE.getAtsyraGoal_Name();

		/**
		 * The meta object literal for the '<em><b>Default Used In Pre</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL__DEFAULT_USED_IN_PRE = eINSTANCE.getAtsyraGoal_DefaultUsedInPre();

		/**
		 * The meta object literal for the '<em><b>Default Used In Post</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL__DEFAULT_USED_IN_POST = eINSTANCE.getAtsyraGoal_DefaultUsedInPost();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.GoalConditionImpl <em>Goal Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.GoalConditionImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getGoalCondition()
		 * @generated
		 */
		EClass GOAL_CONDITION = eINSTANCE.getGoalCondition();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.AtomicConditionImpl <em>Atomic Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.AtomicConditionImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtomicCondition()
		 * @generated
		 */
		EClass ATOMIC_CONDITION = eINSTANCE.getAtomicCondition();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.ConditionOperatorImpl <em>Condition Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.ConditionOperatorImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getConditionOperator()
		 * @generated
		 */
		EClass CONDITION_OPERATOR = eINSTANCE.getConditionOperator();

		/**
		 * The meta object literal for the '<em><b>Operands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION_OPERATOR__OPERANDS = eINSTANCE.getConditionOperator_Operands();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.AndConditionImpl <em>And Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.AndConditionImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAndCondition()
		 * @generated
		 */
		EClass AND_CONDITION = eINSTANCE.getAndCondition();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.OrConditionImpl <em>Or Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.OrConditionImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getOrCondition()
		 * @generated
		 */
		EClass OR_CONDITION = eINSTANCE.getOrCondition();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.NotConditionImpl <em>Not Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.NotConditionImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getNotCondition()
		 * @generated
		 */
		EClass NOT_CONDITION = eINSTANCE.getNotCondition();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.AtsyraGoalModelImpl <em>Atsyra Goal Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.AtsyraGoalModelImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraGoalModel()
		 * @generated
		 */
		EClass ATSYRA_GOAL_MODEL = eINSTANCE.getAtsyraGoalModel();

		/**
		 * The meta object literal for the '<em><b>Atsyragoals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL_MODEL__ATSYRAGOALS = eINSTANCE.getAtsyraGoalModel_Atsyragoals();

		/**
		 * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL_MODEL__TYPES = eINSTANCE.getAtsyraGoalModel_Types();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL_MODEL__IMPORTS = eINSTANCE.getAtsyraGoalModel_Imports();

		/**
		 * The meta object literal for the '<em><b>Typed Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL_MODEL__TYPED_ELEMENTS = eINSTANCE.getAtsyraGoalModel_TypedElements();

		/**
		 * The meta object literal for the '<em><b>Trees</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL_MODEL__TREES = eINSTANCE.getAtsyraGoalModel_Trees();

		/**
		 * The meta object literal for the '<em><b>Default Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_GOAL_MODEL__DEFAULT_VALUES = eINSTANCE.getAtsyraGoalModel_DefaultValues();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.EqualsConditionImpl <em>Equals Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.EqualsConditionImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getEqualsCondition()
		 * @generated
		 */
		EClass EQUALS_CONDITION = eINSTANCE.getEqualsCondition();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUALS_CONDITION__SOURCE = eINSTANCE.getEqualsCondition_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUALS_CONDITION__TARGET = eINSTANCE.getEqualsCondition_Target();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.TypedElementImpl <em>Typed Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.TypedElementImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getTypedElement()
		 * @generated
		 */
		EClass TYPED_ELEMENT = eINSTANCE.getTypedElement();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPED_ELEMENT__TYPE = eINSTANCE.getTypedElement_Type();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPED_ELEMENT__NAME = eINSTANCE.getTypedElement_Name();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.TypeImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getType()
		 * @generated
		 */
		EClass TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__NAME = eINSTANCE.getType_Name();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.InternalTypeImpl <em>Internal Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.InternalTypeImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getInternalType()
		 * @generated
		 */
		EClass INTERNAL_TYPE = eINSTANCE.getInternalType();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.BooleanLiteralImpl <em>Boolean Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.BooleanLiteralImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getBooleanLiteral()
		 * @generated
		 */
		EClass BOOLEAN_LITERAL = eINSTANCE.getBooleanLiteral();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.SystemTypeImpl <em>System Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.SystemTypeImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getSystemType()
		 * @generated
		 */
		EClass SYSTEM_TYPE = eINSTANCE.getSystemType();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.SystemFeatureImpl <em>System Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.SystemFeatureImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getSystemFeature()
		 * @generated
		 */
		EClass SYSTEM_FEATURE = eINSTANCE.getSystemFeature();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.SystemConstFeatureImpl <em>System Const Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.SystemConstFeatureImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getSystemConstFeature()
		 * @generated
		 */
		EClass SYSTEM_CONST_FEATURE = eINSTANCE.getSystemConstFeature();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.BooleanSystemConditionImpl <em>Boolean System Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.BooleanSystemConditionImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getBooleanSystemCondition()
		 * @generated
		 */
		EClass BOOLEAN_SYSTEM_CONDITION = eINSTANCE.getBooleanSystemCondition();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOLEAN_SYSTEM_CONDITION__SOURCE = eINSTANCE.getBooleanSystemCondition_Source();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.ImportImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__IMPORT_URI = eINSTANCE.getImport_ImportURI();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.AtsyraTreeImpl <em>Atsyra Tree</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.AtsyraTreeImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraTree()
		 * @generated
		 */
		EClass ATSYRA_TREE = eINSTANCE.getAtsyraTree();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATSYRA_TREE__NAME = eINSTANCE.getAtsyraTree_Name();

		/**
		 * The meta object literal for the '<em><b>Main Goal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_TREE__MAIN_GOAL = eINSTANCE.getAtsyraTree_MainGoal();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATSYRA_TREE__OPERATOR = eINSTANCE.getAtsyraTree_Operator();

		/**
		 * The meta object literal for the '<em><b>Operands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_TREE__OPERANDS = eINSTANCE.getAtsyraTree_Operands();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.AbstractAtsyraTreeImpl <em>Abstract Atsyra Tree</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.AbstractAtsyraTreeImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAbstractAtsyraTree()
		 * @generated
		 */
		EClass ABSTRACT_ATSYRA_TREE = eINSTANCE.getAbstractAtsyraTree();

		/**
		 * The meta object literal for the '<em><b>Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_ATSYRA_TREE__QUALIFIED_NAME = eINSTANCE.getAbstractAtsyraTree_QualifiedName();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.AtsyraTreeReferenceImpl <em>Atsyra Tree Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.AtsyraTreeReferenceImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraTreeReference()
		 * @generated
		 */
		EClass ATSYRA_TREE_REFERENCE = eINSTANCE.getAtsyraTreeReference();

		/**
		 * The meta object literal for the '<em><b>Referenced Tree</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATSYRA_TREE_REFERENCE__REFERENCED_TREE = eINSTANCE.getAtsyraTreeReference_ReferencedTree();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.DefaultValuesImpl <em>Default Values</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.DefaultValuesImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getDefaultValues()
		 * @generated
		 */
		EClass DEFAULT_VALUES = eINSTANCE.getDefaultValues();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEFAULT_VALUES__NAME = eINSTANCE.getDefaultValues_Name();

		/**
		 * The meta object literal for the '<em><b>Default Value Assignments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFAULT_VALUES__DEFAULT_VALUE_ASSIGNMENTS = eINSTANCE.getDefaultValues_DefaultValueAssignments();

		/**
		 * The meta object literal for the '{@link atsyragoal.impl.DefaultAssignmentImpl <em>Default Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.impl.DefaultAssignmentImpl
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getDefaultAssignment()
		 * @generated
		 */
		EClass DEFAULT_ASSIGNMENT = eINSTANCE.getDefaultAssignment();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFAULT_ASSIGNMENT__TARGET = eINSTANCE.getDefaultAssignment_Target();

		/**
		 * The meta object literal for the '<em><b>Assigned Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFAULT_ASSIGNMENT__ASSIGNED_VALUE = eINSTANCE.getDefaultAssignment_AssignedValue();

		/**
		 * The meta object literal for the '{@link atsyragoal.AtsyraTreeOperator <em>Atsyra Tree Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see atsyragoal.AtsyraTreeOperator
		 * @see atsyragoal.impl.AtsyragoalPackageImpl#getAtsyraTreeOperator()
		 * @generated
		 */
		EEnum ATSYRA_TREE_OPERATOR = eINSTANCE.getAtsyraTreeOperator();

	}

} //AtsyragoalPackage
