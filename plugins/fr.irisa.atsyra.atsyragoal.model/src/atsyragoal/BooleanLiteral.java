/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see atsyragoal.AtsyragoalPackage#getBooleanLiteral()
 * @model
 * @generated
 */
public interface BooleanLiteral extends TypedElement {

} // BooleanLiteral
