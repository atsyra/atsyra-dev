/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see atsyragoal.AtsyragoalPackage
 * @generated
 */
public interface AtsyragoalFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AtsyragoalFactory eINSTANCE = atsyragoal.impl.AtsyragoalFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Atsyra Goal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atsyra Goal</em>'.
	 * @generated
	 */
	AtsyraGoal createAtsyraGoal();

	/**
	 * Returns a new object of class '<em>And Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>And Condition</em>'.
	 * @generated
	 */
	AndCondition createAndCondition();

	/**
	 * Returns a new object of class '<em>Or Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Condition</em>'.
	 * @generated
	 */
	OrCondition createOrCondition();

	/**
	 * Returns a new object of class '<em>Not Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not Condition</em>'.
	 * @generated
	 */
	NotCondition createNotCondition();

	/**
	 * Returns a new object of class '<em>Atsyra Goal Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atsyra Goal Model</em>'.
	 * @generated
	 */
	AtsyraGoalModel createAtsyraGoalModel();

	/**
	 * Returns a new object of class '<em>Equals Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equals Condition</em>'.
	 * @generated
	 */
	EqualsCondition createEqualsCondition();

	/**
	 * Returns a new object of class '<em>Internal Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Internal Type</em>'.
	 * @generated
	 */
	InternalType createInternalType();

	/**
	 * Returns a new object of class '<em>Boolean Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Literal</em>'.
	 * @generated
	 */
	BooleanLiteral createBooleanLiteral();

	/**
	 * Returns a new object of class '<em>System Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System Type</em>'.
	 * @generated
	 */
	SystemType createSystemType();

	/**
	 * Returns a new object of class '<em>System Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System Feature</em>'.
	 * @generated
	 */
	SystemFeature createSystemFeature();

	/**
	 * Returns a new object of class '<em>System Const Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System Const Feature</em>'.
	 * @generated
	 */
	SystemConstFeature createSystemConstFeature();

	/**
	 * Returns a new object of class '<em>Boolean System Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean System Condition</em>'.
	 * @generated
	 */
	BooleanSystemCondition createBooleanSystemCondition();

	/**
	 * Returns a new object of class '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Import</em>'.
	 * @generated
	 */
	Import createImport();

	/**
	 * Returns a new object of class '<em>Atsyra Tree</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atsyra Tree</em>'.
	 * @generated
	 */
	AtsyraTree createAtsyraTree();

	/**
	 * Returns a new object of class '<em>Atsyra Tree Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atsyra Tree Reference</em>'.
	 * @generated
	 */
	AtsyraTreeReference createAtsyraTreeReference();

	/**
	 * Returns a new object of class '<em>Default Values</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Default Values</em>'.
	 * @generated
	 */
	DefaultValues createDefaultValues();

	/**
	 * Returns a new object of class '<em>Default Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Default Assignment</em>'.
	 * @generated
	 */
	DefaultAssignment createDefaultAssignment();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AtsyragoalPackage getAtsyragoalPackage();

} //AtsyragoalFactory
