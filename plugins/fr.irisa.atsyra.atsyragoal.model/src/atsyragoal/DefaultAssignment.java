/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.DefaultAssignment#getTarget <em>Target</em>}</li>
 *   <li>{@link atsyragoal.DefaultAssignment#getAssignedValue <em>Assigned Value</em>}</li>
 * </ul>
 *
 * @see atsyragoal.AtsyragoalPackage#getDefaultAssignment()
 * @model
 * @generated
 */
public interface DefaultAssignment extends EObject {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(SystemFeature)
	 * @see atsyragoal.AtsyragoalPackage#getDefaultAssignment_Target()
	 * @model required="true"
	 * @generated
	 */
	SystemFeature getTarget();

	/**
	 * Sets the value of the '{@link atsyragoal.DefaultAssignment#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(SystemFeature value);

	/**
	 * Returns the value of the '<em><b>Assigned Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * assignedValue cannot be a SystemFeature, and the type must match the target
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Assigned Value</em>' reference.
	 * @see #setAssignedValue(TypedElement)
	 * @see atsyragoal.AtsyragoalPackage#getDefaultAssignment_AssignedValue()
	 * @model required="true"
	 * @generated
	 */
	TypedElement getAssignedValue();

	/**
	 * Sets the value of the '{@link atsyragoal.DefaultAssignment#getAssignedValue <em>Assigned Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assigned Value</em>' reference.
	 * @see #getAssignedValue()
	 * @generated
	 */
	void setAssignedValue(TypedElement value);

} // DefaultAssignment
