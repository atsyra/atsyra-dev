/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atsyra Goal Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.AtsyraGoalModel#getAtsyragoals <em>Atsyragoals</em>}</li>
 *   <li>{@link atsyragoal.AtsyraGoalModel#getTypes <em>Types</em>}</li>
 *   <li>{@link atsyragoal.AtsyraGoalModel#getImports <em>Imports</em>}</li>
 *   <li>{@link atsyragoal.AtsyraGoalModel#getTypedElements <em>Typed Elements</em>}</li>
 *   <li>{@link atsyragoal.AtsyraGoalModel#getTrees <em>Trees</em>}</li>
 *   <li>{@link atsyragoal.AtsyraGoalModel#getDefaultValues <em>Default Values</em>}</li>
 * </ul>
 *
 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoalModel()
 * @model
 * @generated
 */
public interface AtsyraGoalModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Atsyragoals</b></em>' containment reference list.
	 * The list contents are of type {@link atsyragoal.AtsyraGoal}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atsyragoals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atsyragoals</em>' containment reference list.
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoalModel_Atsyragoals()
	 * @model containment="true"
	 * @generated
	 */
	EList<AtsyraGoal> getAtsyragoals();

	/**
	 * Returns the value of the '<em><b>Types</b></em>' containment reference list.
	 * The list contents are of type {@link atsyragoal.Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Types</em>' containment reference list.
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoalModel_Types()
	 * @model containment="true"
	 * @generated
	 */
	EList<Type> getTypes();

	/**
	 * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
	 * The list contents are of type {@link atsyragoal.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' containment reference list.
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoalModel_Imports()
	 * @model containment="true"
	 * @generated
	 */
	EList<Import> getImports();

	/**
	 * Returns the value of the '<em><b>Typed Elements</b></em>' containment reference list.
	 * The list contents are of type {@link atsyragoal.TypedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Elements</em>' containment reference list.
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoalModel_TypedElements()
	 * @model containment="true"
	 * @generated
	 */
	EList<TypedElement> getTypedElements();

	/**
	 * Returns the value of the '<em><b>Trees</b></em>' containment reference list.
	 * The list contents are of type {@link atsyragoal.AtsyraTree}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trees</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trees</em>' containment reference list.
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoalModel_Trees()
	 * @model containment="true"
	 * @generated
	 */
	EList<AtsyraTree> getTrees();

	/**
	 * Returns the value of the '<em><b>Default Values</b></em>' containment reference list.
	 * The list contents are of type {@link atsyragoal.DefaultValues}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Values</em>' containment reference list.
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraGoalModel_DefaultValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<DefaultValues> getDefaultValues();

} // AtsyraGoalModel
