/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atsyra Tree Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link atsyragoal.AtsyraTreeReference#getReferencedTree <em>Referenced Tree</em>}</li>
 * </ul>
 *
 * @see atsyragoal.AtsyragoalPackage#getAtsyraTreeReference()
 * @model
 * @generated
 */
public interface AtsyraTreeReference extends AbstractAtsyraTree {
	/**
	 * Returns the value of the '<em><b>Referenced Tree</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Tree</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Tree</em>' reference.
	 * @see #setReferencedTree(AbstractAtsyraTree)
	 * @see atsyragoal.AtsyragoalPackage#getAtsyraTreeReference_ReferencedTree()
	 * @model keys="qualifiedName" required="true"
	 * @generated
	 */
	AbstractAtsyraTree getReferencedTree();

	/**
	 * Sets the value of the '{@link atsyragoal.AtsyraTreeReference#getReferencedTree <em>Referenced Tree</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Tree</em>' reference.
	 * @see #getReferencedTree()
	 * @generated
	 */
	void setReferencedTree(AbstractAtsyraTree value);

} // AtsyraTreeReference
