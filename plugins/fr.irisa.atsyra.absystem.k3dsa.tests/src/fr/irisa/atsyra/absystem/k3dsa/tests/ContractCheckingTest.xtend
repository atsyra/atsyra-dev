package fr.irisa.atsyra.absystem.k3dsa.tests

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetBasedSystemK3Aspect
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence
import fr.irisa.atsyra.absystem.transfo.tests.AssetBasedSystemDslInjectorProvider
import java.util.List
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.serializer.ISerializer
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle
import org.junit.jupiter.api.TestMethodOrder
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.Assert.assertEquals
import fr.irisa.atsyra.absystem.model.absystem.Contract
import fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.GuardOccurenceK3Aspect
import java.util.function.Supplier
import static org.junit.Assert.assertTrue

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetBasedSystemK3Aspect.*;
import org.eclipse.emf.common.util.BasicEList

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation)
class ContractCheckingTest {
	@Inject
	XtextResourceSet absresourceSet;
	@Inject
	ISerializer serializer;

	AssetBasedSystem sourceabs

	@BeforeEach
	def void SetUpBeforeClass() {
		AbsystemPackage.eINSTANCE.eClass()
		if(absresourceSet !== null && !absresourceSet.resources.empty) {
			absresourceSet.resources.clear
		}
		absresourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE)
	}
	
	private def void LoadTestCase(String caseName) {
		val sourceuri = URI.createFileURI("testfiles/ContractChecking/" + caseName + "/input.abs")
		val sourceRes = absresourceSet.getResource(sourceuri, true)
		sourceabs =  sourceRes.getContents().get(0) as AssetBasedSystem
		sourceabs.initializeModel(new BasicEList())
		Assertions.assertNotNull(sourceabs)
	}
	
	private def checkContract(String contractName, int nbOccurrences, boolean shouldBeValid) {
		val List<GuardOccurence> guardOccurences = AssetBasedSystemK3Aspect.getAllPossibleContractOccurences(sourceabs, false).filter[guard.name == contractName].toList;
		
		assertEquals(nbOccurrences, guardOccurences.size)
		for (GuardOccurence guardOccurence : guardOccurences) {
				val Contract instanciedContract = guardOccurence.getGuard() as Contract ;
				val boolean guardResult = GuardOccurenceK3Aspect.evaluateGuard(guardOccurence, sourceabs);
				assertTrue("Contract "+instanciedContract.name + " should return "+ shouldBeValid + " on " + guardOccurence.guardOccurenceArguments.map[name].join(", "), guardResult == shouldBeValid)
		}
	}
	
	@Test
	def void test01() {
		LoadTestCase("test01")
		checkContract("insideIsDifferentFromOutSide", 1, false)
	}
	
	@Test
	def void test02_oppositelink_contract() {
		LoadTestCase("test02_oppositelink_contract")
		checkContract("insideIsDifferentFromOutSide", 1, false)
	}
	
	

}
