/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace.link;

import java.util.Objects;

import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;

public class AssetLink {
	private String name;
	private Asset asset;
	private int propervalue;
	private int globalvalue;

	public AssetLink(String name, Asset asset, int propervalue, int globalvalue) {
		this.name = name;
		this.asset = asset;
		this.propervalue = propervalue;
		this.globalvalue = globalvalue;
	}
	
	public static boolean is(Link link) {
		return Objects.equals(link.getTransfoID(), ABS2GALTraceBuilder.TRANSFO_ID)
				&& link.getOrigins().get(0) instanceof EObjectRef && ((EObjectRef) link.getOrigins().get(0)).getValue() instanceof Asset && link.getTargets().size() >= 2 && link.getTargets().get(0) instanceof IntRef && link.getTargets().get(1) instanceof IntRef;
	}
	
	public static AssetLink from(Link link) {
		try {
			Asset asset = (Asset) ((EObjectRef) link.getOrigins().get(0)).getValue();
			int propervalue = ((IntRef) link.getTargets().get(0)).getValue();
			int globalvalue = ((IntRef) link.getTargets().get(1)).getValue();
			return new AssetLink(link.getName(), asset, propervalue, globalvalue);
		} catch (ClassCastException|IndexOutOfBoundsException e) {
			throw new InvalidLinkFormatException(e.getMessage());
		}
	}

	public String getName() {
		return name;
	}

	public Asset getAsset() {
		return asset;
	}

	public int getPropervalue() {
		return propervalue;
	}

	public int getGlobalvalue() {
		return globalvalue;
	}
}
