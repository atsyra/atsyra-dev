/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace;

import java.util.List;
import java.util.Optional;

import fr.irisa.atsyra.absystem.gal.transfo.trace.link.AssetTypeFeatureLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.ConstantLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.EnumLiteralLink;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;
import fr.irisa.atsyra.absystem.transfo.trace.AbstractTraceBuilder;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.lip6.move.gal.ArrayPrefix;
import fr.lip6.move.gal.Transition;
import fr.lip6.move.gal.TypedefDeclaration;

public class ABS2GALTraceBuilder extends AbstractTraceBuilder {

	public static final String TRANSFO_ID = "fr.irisa.atsyra.absystem.gal.transfo";
	public static final String FLATTEN_ID = "fr.lip6.move.gal";

	public ABS2GALTraceBuilder(TransfoTraceModel traceModel) {
		super(traceModel);
	}

	public void addPrimitiveDataTypeLink(String name, PrimitiveDataType primitiveType, Optional<TypedefDeclaration> typedef,
			int count) {
		Link link = newLink(name);
		link.getOrigins().add(newRef(primitiveType));
		typedef.map(this::newRef).ifPresent(link.getTargets()::add);
		link.getTargets().add(newRef(count));
		addLink(link);
	}

	public void addAssetTypeLink(String name, AssetType assetType,
			ArrayPrefix subtypesArray, Optional<TypedefDeclaration> typedef, int count, int number) {
		Link link = newLink(name);
		link.getOrigins().add(newRef(assetType));
		link.getTargets().add(newRef(subtypesArray));
		typedef.map(this::newRef).ifPresent(link.getTargets()::add);
		link.getTargets().add(newRef(count));
		link.getTargets().add(newRef(number));
		addLink(link);
	}

	public void addConstantLink(String name, ConstantExpression constant, int value) {
		Link link = newLink(name);
		link.getOrigins().add(newRef(constant));
		link.getTargets().add(newRef(value));
		addLink(link);
	}
	
	public void addConstantLink(ConstantLink link) {
		addConstantLink(link.getName(), link.getConstant(), link.getValue());
	}

	public void addEnumLiteralLink(String name, EnumLiteral literal, int value) {
		Link link = newLink(name);
		link.getOrigins().add(newRef(literal));
		link.getTargets().add(newRef(value));
		addLink(link);
	}
	
	public void addEnumLiteralLink(EnumLiteralLink link) {
		addEnumLiteralLink(link.getName(), link.getLiteral(), link.getValue());
	}

	public void addAssetLink(String name, Asset asset, int propervalue, int globalvalue) {
		Link link = newLink(name);
		link.getOrigins().add(newRef(asset));
		link.getTargets().add(newRef(propervalue));
		link.getTargets().add(newRef(globalvalue));
		addLink(link);
	}

	public void addAssetTypeFeatureLink(String name, AssetTypeFeature feature, Optional<AssetType> memberSubtype,
			ArrayPrefix array) {
		Link link = newLink(name);
		link.getOrigins().add(newRef(feature));
		memberSubtype.map(this::newRef).ifPresent(link.getOrigins()::add);
		link.getTargets().add(newRef(array));
		addLink(link);
	}
	
	public void addAssetTypeFeatureLink(AssetTypeFeatureLink link) {
		addAssetTypeFeatureLink(link.getName(), link.getFeature(), link.getMemberSubtype(), link.getArray());
	}

	public void addGuardedActionLink(String name, GuardedAction guardedaction, List<Transition> transition,
			int repetition) {
		Link link = newLink(name);
		link.getOrigins().add(newRef(guardedaction));
		transition.stream().map(this::newRef).forEach(link.getTargets()::add);
		link.getTargets().add(newRef(repetition));
		addLink(link);
	}

	public void addFlattenLink(String name, Transition transition, List<Asset> parametersinstances,
			Transition transitionflat) {
		Link link = newLink(name);
		//Since flatten is done by itstools and not abs2gal, we use a different transfoID
		link.setTransfoID(getFlattenTransfoID());
		link.getOrigins().add(newRef(transition));
		parametersinstances.stream().map(this::newRef).forEach(link.getOrigins()::add);
		link.getTargets().add(newRef(transitionflat));
		addLink(link);
	}
	
	
	
	@Override
	public String getTransfoID() {
		return TRANSFO_ID;
	}
	
	public String getFlattenTransfoID() {
		return FLATTEN_ID;
	}
	
}
