/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace.link;

import java.util.Objects;

import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder;
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;

public class ConstantLink {
	private String name;
	private ConstantExpression constant;
	private int value;
	
	public ConstantLink(String name, ConstantExpression constant, int value) {
		this.name = name;
		this.constant = constant;
		this.value = value;
	}
	
	public static boolean is(Link link) {
		return Objects.equals(link.getTransfoID(), ABS2GALTraceBuilder.TRANSFO_ID)
				&& link.getOrigins().get(0) instanceof EObjectRef && ((EObjectRef) link.getOrigins().get(0)).getValue() instanceof ConstantExpression 
				&& !link.getTargets().isEmpty() && link.getTargets().get(0) instanceof IntRef;
	}
	
	public static ConstantLink from(Link link) {
		try {
			ConstantExpression constant = (ConstantExpression) ((EObjectRef) link.getOrigins().get(0)).getValue();
			int value = ((IntRef) link.getTargets().get(0)).getValue();
			return new ConstantLink(link.getName(), constant, value);
		} catch (ClassCastException|IndexOutOfBoundsException e) {
			throw new InvalidLinkFormatException(e.getMessage());
		}
	}

	public String getName() {
		return name;
	}

	public ConstantExpression getConstant() {
		return constant;
	}

	public int getValue() {
		return value;
	}
}
