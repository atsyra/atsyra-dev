/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace.link;

import java.util.Objects;
import java.util.Optional;

import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.lip6.move.gal.TypedefDeclaration;

public class PrimitiveDataTypeLink {
	private String name;
	private PrimitiveDataType primitiveType;
	private Optional<TypedefDeclaration> typedef;
	private int count;

	public PrimitiveDataTypeLink(String name, PrimitiveDataType primitiveType, Optional<TypedefDeclaration> typedef,
			int count) {
		this.name = name;
		this.primitiveType = primitiveType;
		this.typedef = typedef;
		this.count = count;
	}

	public static boolean is(Link link) {
		return Objects.equals(link.getTransfoID(), ABS2GALTraceBuilder.TRANSFO_ID)
				&& link.getOrigins().get(0) instanceof EObjectRef
				&& ((EObjectRef) link.getOrigins().get(0)).getValue() instanceof PrimitiveDataType
				&& ((link.getTargets().size() == 1 && link.getTargets().get(0) instanceof IntRef)
						|| (link.getTargets().size() == 2 && link.getTargets().get(0) instanceof EObjectRef
								&& ((EObjectRef) link.getTargets().get(0)).getValue() instanceof TypedefDeclaration
								&& link.getTargets().get(1) instanceof IntRef));
	}

	public static PrimitiveDataTypeLink from(Link link) {
		try {
			PrimitiveDataType primitiveType = (PrimitiveDataType) ((EObjectRef) link.getOrigins().get(0)).getValue();
			Optional<TypedefDeclaration> typedef;
			int count;
			if(link.getTargets().size() == 1) {
				typedef = Optional.empty();
				count = ((IntRef) link.getTargets().get(0)).getValue();
			} else {
				typedef = Optional.of((TypedefDeclaration) ((EObjectRef) link.getTargets().get(0)).getValue());
				count = ((IntRef) link.getTargets().get(1)).getValue();
			}
			return new PrimitiveDataTypeLink(link.getName(), primitiveType, typedef, count);
		} catch (ClassCastException | IndexOutOfBoundsException e) {
			throw new InvalidLinkFormatException(e.getMessage());
		}
	}

	public String getName() {
		return name;
	}

	public PrimitiveDataType getPrimitiveType() {
		return primitiveType;
	}

	public Optional<TypedefDeclaration> getTypedef() {
		return typedef;
	}

	public int getCount() {
		return count;
	}

}
