/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace.link;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder;
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.lip6.move.gal.Transition;

public class GuardedActionLink {
	private String name;
	private GuardedAction guardedaction;
	private List<Transition> transition;
	private int repetition;

	public GuardedActionLink(String name, GuardedAction guardedaction, List<Transition> transition, int repetition) {
		this.name = name;
		this.guardedaction = guardedaction;
		this.transition = transition;
		this.repetition = repetition;
	}

	public static boolean is(Link link) {
		return Objects.equals(link.getTransfoID(), ABS2GALTraceBuilder.TRANSFO_ID)
				&& link.getOrigins().get(0) instanceof EObjectRef
				&& ((EObjectRef) link.getOrigins().get(0)).getValue() instanceof GuardedAction
				&& !link.getTargets().isEmpty()
				&& link.getTargets().stream().filter(EObjectRef.class::isInstance).map(EObjectRef.class::cast)
						.map(EObjectRef::getValue).allMatch(Transition.class::isInstance)
				&& link.getTargets().stream().dropWhile(EObjectRef.class::isInstance).allMatch(IntRef.class::isInstance)
				&& link.getTargets().stream().dropWhile(EObjectRef.class::isInstance).count() == 1;
	}

	public static GuardedActionLink from(Link link) {
		try {
			GuardedAction guardedaction = (GuardedAction) ((EObjectRef) link.getOrigins().get(0)).getValue();
			List<Transition> transition = link.getTargets().stream().filter(EObjectRef.class::isInstance)
					.map(EObjectRef.class::cast).map(EObjectRef::getValue).map(Transition.class::cast)
					.collect(Collectors.toList());
			int repetition = link.getTargets().stream().dropWhile(EObjectRef.class::isInstance).map(IntRef.class::cast)
					.map(IntRef::getValue).findFirst()
					.orElseThrow(() -> new InvalidLinkFormatException("Missing int target"));
			return new GuardedActionLink(link.getName(), guardedaction, transition, repetition);
		} catch (ClassCastException | IndexOutOfBoundsException e) {
			throw new InvalidLinkFormatException(e.getMessage());
		}
	}

	public String getName() {
		return name;
	}

	public GuardedAction getGuardedaction() {
		return guardedaction;
	}

	public List<Transition> getTransition() {
		return transition;
	}

	public int getRepetition() {
		return repetition;
	}

}
