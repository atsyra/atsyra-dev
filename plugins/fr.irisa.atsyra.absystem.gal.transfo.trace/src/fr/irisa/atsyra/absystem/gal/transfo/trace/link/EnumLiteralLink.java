/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace.link;

import java.util.Objects;

import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;

public class EnumLiteralLink {
	private String name;
	private EnumLiteral literal;
	private int value;

	public EnumLiteralLink(String name, EnumLiteral literal, int value) {
		this.name = name;
		this.literal = literal;
		this.value = value;
	}

	public static boolean is(Link link) {
		return Objects.equals(link.getTransfoID(), ABS2GALTraceBuilder.TRANSFO_ID)
				&& link.getOrigins().get(0) instanceof EObjectRef
				&& ((EObjectRef) link.getOrigins().get(0)).getValue() instanceof EnumLiteral
				&& !link.getTargets().isEmpty() && link.getTargets().get(0) instanceof IntRef;
	}

	public static EnumLiteralLink from(Link link) {
		try {
			EnumLiteral literal = (EnumLiteral) ((EObjectRef) link.getOrigins().get(0)).getValue();
			int value = ((IntRef) link.getTargets().get(0)).getValue();
			return new EnumLiteralLink(link.getName(), literal, value);
		} catch (ClassCastException | IndexOutOfBoundsException e) {
			throw new InvalidLinkFormatException(e.getMessage());
		}
	}

	public String getName() {
		return name;
	}

	public EnumLiteral getLiteral() {
		return literal;
	}

	public int getValue() {
		return value;
	}
}
