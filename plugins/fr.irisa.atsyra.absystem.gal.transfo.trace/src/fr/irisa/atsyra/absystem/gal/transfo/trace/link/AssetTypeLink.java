/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace.link;

import java.util.Objects;
import java.util.Optional;

import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.lip6.move.gal.ArrayPrefix;
import fr.lip6.move.gal.TypedefDeclaration;

public class AssetTypeLink {
	private String name;
	private AssetType assetType;
	private Optional<TypedefDeclaration> typedef;
	private ArrayPrefix subtypesArray;
	private int count;
	private int number;

	public AssetTypeLink(String name, AssetType assetType, ArrayPrefix subtypesArray,
			Optional<TypedefDeclaration> typedef, int count, int number) {
		this.name = name;
		this.assetType = assetType;
		this.typedef = typedef;
		this.subtypesArray = subtypesArray;
		this.count = count;
		this.number = number;
	}

	public static boolean is(Link link) {
		return Objects.equals(link.getTransfoID(), ABS2GALTraceBuilder.TRANSFO_ID)
				&& link.getOrigins().stream().allMatch(EObjectRef.class::isInstance)
				&& ((EObjectRef) link.getOrigins().get(0)).getValue() instanceof AssetType
				&& ((link.getTargets().size() == 3 && link.getTargets().get(0) instanceof EObjectRef
						&& ((EObjectRef) link.getTargets().get(0)).getValue() instanceof ArrayPrefix
						&& link.getTargets().stream().skip(1).allMatch(IntRef.class::isInstance))
						|| (link.getTargets().size() == 4) && link.getTargets().get(0) instanceof EObjectRef
								&& ((EObjectRef) link.getTargets().get(0)).getValue() instanceof ArrayPrefix
								&& link.getTargets().get(1) instanceof EObjectRef
								&& ((EObjectRef) link.getTargets().get(1)).getValue() instanceof TypedefDeclaration
								&& link.getTargets().stream().skip(2).allMatch(IntRef.class::isInstance));
	}

	public static AssetTypeLink from(Link link) {
		try {
			AssetType assetType = (AssetType) ((EObjectRef) link.getOrigins().get(0)).getValue();
			ArrayPrefix subtypesArray = (ArrayPrefix) ((EObjectRef) link.getTargets().get(0)).getValue();
			Optional<TypedefDeclaration> typedef;
			int count;
			int number;
			if(link.getTargets().size() > 3) {
				typedef = Optional.of((TypedefDeclaration) ((EObjectRef) link.getTargets().get(1)).getValue());
				count = ((IntRef) link.getTargets().get(2)).getValue();
				number = ((IntRef) link.getTargets().get(3)).getValue();
			} else {
				typedef = Optional.empty();
				count = ((IntRef) link.getTargets().get(1)).getValue();
				number = ((IntRef) link.getTargets().get(2)).getValue();
			}
			return new AssetTypeLink(link.getName(), assetType, subtypesArray, typedef, count, number);
		} catch (ClassCastException | IndexOutOfBoundsException e) {
			throw new InvalidLinkFormatException(e.getMessage());
		}
	}

	public String getName() {
		return name;
	}

	public AssetType getAssetType() {
		return assetType;
	}

	public Optional<TypedefDeclaration> getTypedef() {
		return typedef;
	}

	public ArrayPrefix getSubtypesArray() {
		return subtypesArray;
	}

	public int getCount() {
		return count;
	}

	public int getNumber() {
		return number;
	}

}
