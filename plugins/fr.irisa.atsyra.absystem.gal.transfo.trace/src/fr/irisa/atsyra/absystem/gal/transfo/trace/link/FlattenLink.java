/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace.link;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.lip6.move.gal.Transition;

public class FlattenLink {
	private String name;
	private Transition transition;
	private List<Asset> parametersinstances;
	private Transition transitionflat;

	public FlattenLink(String name, Transition transition, List<Asset> parametersinstances, Transition transitionflat) {
		this.name = name;
		this.transition = transition;
		this.parametersinstances = parametersinstances;
		this.transitionflat = transitionflat;
	}
	
	public static boolean is(Link link) {
		return  Objects.equals(link.getTransfoID(), ABS2GALTraceBuilder.FLATTEN_ID)
				&& link.getOrigins().get(0) instanceof EObjectRef
				&& ((EObjectRef) link.getOrigins().get(0)).getValue() instanceof Transition 
				&& link.getOrigins().stream().skip(1).allMatch(EObjectRef.class::isInstance) 
				&& link.getOrigins().stream().skip(1).map(EObjectRef.class::cast).map(EObjectRef::getValue).allMatch(Asset.class::isInstance)
				&& !link.getTargets().isEmpty() 
				&& link.getTargets().get(0) instanceof EObjectRef
				&& ((EObjectRef) link.getTargets().get(0)).getValue() instanceof Transition;
	}
	
	public static FlattenLink from(Link link) {
		try {
			Transition transition = (Transition) ((EObjectRef) link.getOrigins().get(0)).getValue();
			List<Asset> parametersinstances = link.getOrigins().stream().skip(1).map(EObjectRef.class::cast).map(EObjectRef::getValue).map(Asset.class::cast).collect(Collectors.toList());
			Transition transitionflat = (Transition) ((EObjectRef) link.getTargets().get(0)).getValue();
			return new FlattenLink(link.getName(), transition, parametersinstances, transitionflat);
		} catch (ClassCastException|IndexOutOfBoundsException e) {
			throw new InvalidLinkFormatException(e.getMessage());
		}
	}

	public String getName() {
		return name;
	}

	public Transition getTransition() {
		return transition;
	}
	
	public List<Asset> getParametersinstances() {
		return parametersinstances;
	}

	public Transition getTransitionflat() {
		return transitionflat;
	}
}
