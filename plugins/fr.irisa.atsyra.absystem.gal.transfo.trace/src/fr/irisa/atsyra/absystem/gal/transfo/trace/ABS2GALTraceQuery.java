/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.atsyra.absystem.gal.transfo.trace.link.AssetLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.AssetTypeFeatureLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.AssetTypeLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.ConstantLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.EnumLiteralLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.FlattenLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.GuardedActionLink;
import fr.irisa.atsyra.absystem.gal.transfo.trace.link.PrimitiveDataTypeLink;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression;
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSExpressionTypeProvider;
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.irisa.atsyra.gal.GalExpressionInterpreter;
import fr.lip6.move.gal.Transition;

public class ABS2GALTraceQuery {

	private TransfoTraceModel traceModel;

	public ABS2GALTraceQuery(TransfoTraceModel traceModel) {
		this.traceModel = traceModel;
	}

	public static ABS2GALTraceQuery of(TransfoTraceModel traceModel) {
		return new ABS2GALTraceQuery(traceModel);
	}

	public FlattenLink fromFlatTransitionName(String name) {
		return traceModel.getLinks().stream().filter(FlattenLink::is).map(FlattenLink::from)
				.filter(flink -> Objects.equals(flink.getTransitionflat().getName(), name)).findFirst().orElse(null);
	}

	public GuardedActionLink fromTransition(Transition transition) {
		return traceModel.getLinks().stream().filter(GuardedActionLink::is).map(GuardedActionLink::from)
				.filter(glink -> glink.getTransition().stream().anyMatch(tr -> Objects.equals(tr, transition)))
				.findFirst().orElse(null);
	}

	public Optional<Asset> getProperAsset(AssetType type, int properValue) {
		return traceModel.getLinks().stream().filter(AssetLink::is)
				.map(AssetLink::from).filter(alink -> Objects.equals(alink.getAsset().getAssetType(), type)
						&& alink.getPropervalue() == properValue)
				.findFirst().map(AssetLink::getAsset);
	}

	public AssetTypeFeatureLink fromArrayName(String arrayName) {
		return traceModel.getLinks().stream().filter(AssetTypeFeatureLink::is).map(AssetTypeFeatureLink::from)
				.filter(flink -> Objects.equals(flink.getArray().getName(), arrayName)).findFirst().orElse(null);
	}

	public AssetTypeLink fromAssetType(AssetType assetType) {
		return traceModel.getLinks().stream().filter(AssetTypeLink::is).map(AssetTypeLink::from)
				.filter(atlink -> Objects.equals(atlink.getAssetType(), assetType)).findFirst().orElse(null);
	}

	public AssetLink fromAsset(Asset asset) {
		return traceModel.getLinks().stream().filter(AssetLink::is).map(AssetLink::from)
				.filter(alink -> Objects.equals(alink.getAsset(), asset)).findFirst().orElse(null);
	}

	public PrimitiveDataTypeLink fromPrimitiveDataType(PrimitiveDataType primitiveType) {
		return fromPrimitiveDataTypeName(primitiveType.getName());
	}

	public PrimitiveDataTypeLink fromPrimitiveDataTypeName(String primitiveTypeName) {
		return traceModel.getLinks().stream().filter(PrimitiveDataTypeLink::is).map(PrimitiveDataTypeLink::from)
				.filter(ptlink -> Objects.equals(ptlink.getPrimitiveType().getName(), primitiveTypeName)).findFirst()
				.orElse(null);
	}

	public long getNumberOfAssetTypes() {
		return traceModel.getLinks().stream().filter(AssetTypeLink::is).count();
	}

	public AssetTypeLink fromAssetTypeNumber(int typeNumber) {
		return traceModel.getLinks().stream().filter(AssetTypeLink::is).map(AssetTypeLink::from)
				.filter(link -> link.getNumber() == typeNumber).findFirst().orElse(null);
	}

	public AssetLink fromGlobalValue(int globalValue) {
		return traceModel.getLinks().stream().filter(AssetLink::is).map(AssetLink::from)
				.filter(link -> link.getGlobalvalue() == globalValue).findFirst().orElse(null);
	}

	public Collection<AssetType> getAllSubtypes(AssetType type) {
		AssetTypeLink assetTypeLink = fromAssetType(type);
		return assetTypeLink.getSubtypesArray().getValues().stream().map(GalExpressionInterpreter::intEvaluate)
				.filter(subtypenumber -> subtypenumber != -1)
				.map(subtypenumber -> fromAssetTypeNumber(subtypenumber).getAssetType()).collect(Collectors.toList());
	}
	
	public Collection<ConstantLink> getConstantsOfPrimitiveDataType(PrimitiveDataType type) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(type));
		return traceModel.getLinks().stream().filter(ConstantLink::is).map(ConstantLink::from)
				.filter(clink -> Objects.equals(typeProvider.getTypeOf(clink.getConstant()).getType(), type))
				.collect(Collectors.toList());
	}

	public ConstantExpression fromTypeAndValue(PrimitiveDataType type, int value) {
		ABSExpressionTypeProvider typeProvider = new ABSExpressionTypeProvider(
				(AssetBasedSystem) EcoreUtil.getRootContainer(type));
		return traceModel.getLinks().stream().filter(ConstantLink::is).map(ConstantLink::from)
				.filter(clink -> Objects.equals(typeProvider.getTypeOf(clink.getConstant()).getType(), type)
						&& clink.getValue() == value)
				.findFirst().map(ConstantLink::getConstant).orElse(null);
	}
	
	public Collection<EnumLiteralLink> getEnumLiteralOfEnumDataType(EnumDataType type) {
		return traceModel.getLinks().stream().filter(EnumLiteralLink::is).map(EnumLiteralLink::from)
				.filter(clink -> Objects.equals(ABSUtils.getEnumLiteralType(clink.getLiteral()), type))
				.collect(Collectors.toList());
	}
	
	public EnumLiteral fromTypeAndValue(EnumDataType type, int value) {
		return traceModel.getLinks().stream().filter(EnumLiteralLink::is).map(EnumLiteralLink::from)
				.filter(clink -> Objects.equals(ABSUtils.getEnumLiteralType(clink.getLiteral()), type)
						&& clink.getValue() == value)
				.findFirst().map(EnumLiteralLink::getLiteral).orElse(null);
	}
}
