/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.transfo.trace.link;

import java.util.Objects;
import java.util.Optional;

import fr.irisa.atsyra.absystem.gal.transfo.trace.ABS2GALTraceBuilder;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.lip6.move.gal.ArrayPrefix;

public class AssetTypeFeatureLink {
	private String name;
	private AssetTypeFeature feature;
	private Optional<AssetType> memberSubtype;
	private ArrayPrefix array;

	public AssetTypeFeatureLink(String name, AssetTypeFeature feature, Optional<AssetType> memberSubtype,
			ArrayPrefix array) {
		this.name = name;
		this.feature = feature;
		this.memberSubtype = memberSubtype;
		this.array = array;
	}

	public static boolean is(Link link) {
		return Objects.equals(link.getTransfoID(), ABS2GALTraceBuilder.TRANSFO_ID)
				&& link.getOrigins().stream().allMatch(EObjectRef.class::isInstance)
				&& ((EObjectRef) link.getOrigins().get(0)).getValue() instanceof AssetTypeFeature
				&& link.getOrigins().stream().skip(1).map(EObjectRef.class::cast).map(EObjectRef::getValue)
						.allMatch(AssetType.class::isInstance)
				&& !link.getTargets().isEmpty() && link.getTargets().get(0) instanceof EObjectRef
				&& ((EObjectRef) link.getTargets().get(0)).getValue() instanceof ArrayPrefix;
	}

	public static AssetTypeFeatureLink from(Link link) {
		try {
			AssetTypeFeature feature = (AssetTypeFeature) ((EObjectRef) link.getOrigins().get(0)).getValue();
			Optional<AssetType> memberSubtype = link.getOrigins().stream().skip(1).map(EObjectRef.class::cast)
					.map(EObjectRef::getValue).map(AssetType.class::cast).findFirst();
			ArrayPrefix array = (ArrayPrefix) ((EObjectRef) link.getTargets().get(0)).getValue();
			return new AssetTypeFeatureLink(link.getName(), feature, memberSubtype, array);
		} catch (ClassCastException | IndexOutOfBoundsException e) {
			throw new InvalidLinkFormatException(e.getMessage());
		}
	}

	public String getName() {
		return name;
	}

	public AssetTypeFeature getFeature() {
		return feature;
	}

	public Optional<AssetType> getMemberSubtype() {
		return memberSubtype;
	}

	public ArrayPrefix getArray() {
		return array;
	}

}
