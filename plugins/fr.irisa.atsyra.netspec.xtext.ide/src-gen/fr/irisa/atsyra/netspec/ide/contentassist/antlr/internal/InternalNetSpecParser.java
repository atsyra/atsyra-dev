/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.netspec.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.irisa.atsyra.netspec.services.NetSpecGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalNetSpecParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'None'", "'Low'", "'Medium'", "'High'", "'Perfect'", "'Attacker'", "'{'", "'hackLevel'", "'cryptoLevel'", "'virusLevel'", "'loginLevel'", "'}'", "'LoginsKnown'", "'KeysKnown'", "'HostsControlled'", "'Goal'", "'Datas'", "'Files'", "'Hosts'", "'Host'", "'Sessions'", "'Root'", "'IDS'", "'Firewall'", "'Antivirus'", "'Session'", "'Login'", "'Level'", "'File'", "'Key'", "'ContainsLogins'", "'ContainsKeys'", "'Data'", "'Link'", "'H1'", "'H2'", "'CanTriggerIds'", "'Active'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalNetSpecParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalNetSpecParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalNetSpecParser.tokenNames; }
    public String getGrammarFileName() { return "InternalNetSpec.g"; }


    	private NetSpecGrammarAccess grammarAccess;

    	public void setGrammarAccess(NetSpecGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModel"
    // InternalNetSpec.g:53:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalNetSpec.g:54:1: ( ruleModel EOF )
            // InternalNetSpec.g:55:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalNetSpec.g:62:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:66:2: ( ( ( rule__Model__Group__0 ) ) )
            // InternalNetSpec.g:67:2: ( ( rule__Model__Group__0 ) )
            {
            // InternalNetSpec.g:67:2: ( ( rule__Model__Group__0 ) )
            // InternalNetSpec.g:68:3: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // InternalNetSpec.g:69:3: ( rule__Model__Group__0 )
            // InternalNetSpec.g:69:4: rule__Model__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleAttacker"
    // InternalNetSpec.g:78:1: entryRuleAttacker : ruleAttacker EOF ;
    public final void entryRuleAttacker() throws RecognitionException {
        try {
            // InternalNetSpec.g:79:1: ( ruleAttacker EOF )
            // InternalNetSpec.g:80:1: ruleAttacker EOF
            {
             before(grammarAccess.getAttackerRule()); 
            pushFollow(FOLLOW_1);
            ruleAttacker();

            state._fsp--;

             after(grammarAccess.getAttackerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttacker"


    // $ANTLR start "ruleAttacker"
    // InternalNetSpec.g:87:1: ruleAttacker : ( ( rule__Attacker__Group__0 ) ) ;
    public final void ruleAttacker() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:91:2: ( ( ( rule__Attacker__Group__0 ) ) )
            // InternalNetSpec.g:92:2: ( ( rule__Attacker__Group__0 ) )
            {
            // InternalNetSpec.g:92:2: ( ( rule__Attacker__Group__0 ) )
            // InternalNetSpec.g:93:3: ( rule__Attacker__Group__0 )
            {
             before(grammarAccess.getAttackerAccess().getGroup()); 
            // InternalNetSpec.g:94:3: ( rule__Attacker__Group__0 )
            // InternalNetSpec.g:94:4: rule__Attacker__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttacker"


    // $ANTLR start "entryRuleGoal"
    // InternalNetSpec.g:103:1: entryRuleGoal : ruleGoal EOF ;
    public final void entryRuleGoal() throws RecognitionException {
        try {
            // InternalNetSpec.g:104:1: ( ruleGoal EOF )
            // InternalNetSpec.g:105:1: ruleGoal EOF
            {
             before(grammarAccess.getGoalRule()); 
            pushFollow(FOLLOW_1);
            ruleGoal();

            state._fsp--;

             after(grammarAccess.getGoalRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGoal"


    // $ANTLR start "ruleGoal"
    // InternalNetSpec.g:112:1: ruleGoal : ( ( rule__Goal__Group__0 ) ) ;
    public final void ruleGoal() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:116:2: ( ( ( rule__Goal__Group__0 ) ) )
            // InternalNetSpec.g:117:2: ( ( rule__Goal__Group__0 ) )
            {
            // InternalNetSpec.g:117:2: ( ( rule__Goal__Group__0 ) )
            // InternalNetSpec.g:118:3: ( rule__Goal__Group__0 )
            {
             before(grammarAccess.getGoalAccess().getGroup()); 
            // InternalNetSpec.g:119:3: ( rule__Goal__Group__0 )
            // InternalNetSpec.g:119:4: rule__Goal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Goal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGoalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGoal"


    // $ANTLR start "entryRuleHost"
    // InternalNetSpec.g:128:1: entryRuleHost : ruleHost EOF ;
    public final void entryRuleHost() throws RecognitionException {
        try {
            // InternalNetSpec.g:129:1: ( ruleHost EOF )
            // InternalNetSpec.g:130:1: ruleHost EOF
            {
             before(grammarAccess.getHostRule()); 
            pushFollow(FOLLOW_1);
            ruleHost();

            state._fsp--;

             after(grammarAccess.getHostRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHost"


    // $ANTLR start "ruleHost"
    // InternalNetSpec.g:137:1: ruleHost : ( ( rule__Host__Group__0 ) ) ;
    public final void ruleHost() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:141:2: ( ( ( rule__Host__Group__0 ) ) )
            // InternalNetSpec.g:142:2: ( ( rule__Host__Group__0 ) )
            {
            // InternalNetSpec.g:142:2: ( ( rule__Host__Group__0 ) )
            // InternalNetSpec.g:143:3: ( rule__Host__Group__0 )
            {
             before(grammarAccess.getHostAccess().getGroup()); 
            // InternalNetSpec.g:144:3: ( rule__Host__Group__0 )
            // InternalNetSpec.g:144:4: rule__Host__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Host__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHostAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHost"


    // $ANTLR start "entryRuleSession"
    // InternalNetSpec.g:153:1: entryRuleSession : ruleSession EOF ;
    public final void entryRuleSession() throws RecognitionException {
        try {
            // InternalNetSpec.g:154:1: ( ruleSession EOF )
            // InternalNetSpec.g:155:1: ruleSession EOF
            {
             before(grammarAccess.getSessionRule()); 
            pushFollow(FOLLOW_1);
            ruleSession();

            state._fsp--;

             after(grammarAccess.getSessionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSession"


    // $ANTLR start "ruleSession"
    // InternalNetSpec.g:162:1: ruleSession : ( ( rule__Session__Group__0 ) ) ;
    public final void ruleSession() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:166:2: ( ( ( rule__Session__Group__0 ) ) )
            // InternalNetSpec.g:167:2: ( ( rule__Session__Group__0 ) )
            {
            // InternalNetSpec.g:167:2: ( ( rule__Session__Group__0 ) )
            // InternalNetSpec.g:168:3: ( rule__Session__Group__0 )
            {
             before(grammarAccess.getSessionAccess().getGroup()); 
            // InternalNetSpec.g:169:3: ( rule__Session__Group__0 )
            // InternalNetSpec.g:169:4: rule__Session__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Session__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSessionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSession"


    // $ANTLR start "entryRuleAntivirus"
    // InternalNetSpec.g:178:1: entryRuleAntivirus : ruleAntivirus EOF ;
    public final void entryRuleAntivirus() throws RecognitionException {
        try {
            // InternalNetSpec.g:179:1: ( ruleAntivirus EOF )
            // InternalNetSpec.g:180:1: ruleAntivirus EOF
            {
             before(grammarAccess.getAntivirusRule()); 
            pushFollow(FOLLOW_1);
            ruleAntivirus();

            state._fsp--;

             after(grammarAccess.getAntivirusRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAntivirus"


    // $ANTLR start "ruleAntivirus"
    // InternalNetSpec.g:187:1: ruleAntivirus : ( ( rule__Antivirus__Group__0 ) ) ;
    public final void ruleAntivirus() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:191:2: ( ( ( rule__Antivirus__Group__0 ) ) )
            // InternalNetSpec.g:192:2: ( ( rule__Antivirus__Group__0 ) )
            {
            // InternalNetSpec.g:192:2: ( ( rule__Antivirus__Group__0 ) )
            // InternalNetSpec.g:193:3: ( rule__Antivirus__Group__0 )
            {
             before(grammarAccess.getAntivirusAccess().getGroup()); 
            // InternalNetSpec.g:194:3: ( rule__Antivirus__Group__0 )
            // InternalNetSpec.g:194:4: rule__Antivirus__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Antivirus__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAntivirusAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAntivirus"


    // $ANTLR start "entryRuleFile"
    // InternalNetSpec.g:203:1: entryRuleFile : ruleFile EOF ;
    public final void entryRuleFile() throws RecognitionException {
        try {
            // InternalNetSpec.g:204:1: ( ruleFile EOF )
            // InternalNetSpec.g:205:1: ruleFile EOF
            {
             before(grammarAccess.getFileRule()); 
            pushFollow(FOLLOW_1);
            ruleFile();

            state._fsp--;

             after(grammarAccess.getFileRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFile"


    // $ANTLR start "ruleFile"
    // InternalNetSpec.g:212:1: ruleFile : ( ( rule__File__Group__0 ) ) ;
    public final void ruleFile() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:216:2: ( ( ( rule__File__Group__0 ) ) )
            // InternalNetSpec.g:217:2: ( ( rule__File__Group__0 ) )
            {
            // InternalNetSpec.g:217:2: ( ( rule__File__Group__0 ) )
            // InternalNetSpec.g:218:3: ( rule__File__Group__0 )
            {
             before(grammarAccess.getFileAccess().getGroup()); 
            // InternalNetSpec.g:219:3: ( rule__File__Group__0 )
            // InternalNetSpec.g:219:4: rule__File__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__File__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFileAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFile"


    // $ANTLR start "entryRuleData"
    // InternalNetSpec.g:228:1: entryRuleData : ruleData EOF ;
    public final void entryRuleData() throws RecognitionException {
        try {
            // InternalNetSpec.g:229:1: ( ruleData EOF )
            // InternalNetSpec.g:230:1: ruleData EOF
            {
             before(grammarAccess.getDataRule()); 
            pushFollow(FOLLOW_1);
            ruleData();

            state._fsp--;

             after(grammarAccess.getDataRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleData"


    // $ANTLR start "ruleData"
    // InternalNetSpec.g:237:1: ruleData : ( ( rule__Data__Group__0 ) ) ;
    public final void ruleData() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:241:2: ( ( ( rule__Data__Group__0 ) ) )
            // InternalNetSpec.g:242:2: ( ( rule__Data__Group__0 ) )
            {
            // InternalNetSpec.g:242:2: ( ( rule__Data__Group__0 ) )
            // InternalNetSpec.g:243:3: ( rule__Data__Group__0 )
            {
             before(grammarAccess.getDataAccess().getGroup()); 
            // InternalNetSpec.g:244:3: ( rule__Data__Group__0 )
            // InternalNetSpec.g:244:4: rule__Data__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Data__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleData"


    // $ANTLR start "entryRuleLogin"
    // InternalNetSpec.g:253:1: entryRuleLogin : ruleLogin EOF ;
    public final void entryRuleLogin() throws RecognitionException {
        try {
            // InternalNetSpec.g:254:1: ( ruleLogin EOF )
            // InternalNetSpec.g:255:1: ruleLogin EOF
            {
             before(grammarAccess.getLoginRule()); 
            pushFollow(FOLLOW_1);
            ruleLogin();

            state._fsp--;

             after(grammarAccess.getLoginRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLogin"


    // $ANTLR start "ruleLogin"
    // InternalNetSpec.g:262:1: ruleLogin : ( ( rule__Login__Group__0 ) ) ;
    public final void ruleLogin() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:266:2: ( ( ( rule__Login__Group__0 ) ) )
            // InternalNetSpec.g:267:2: ( ( rule__Login__Group__0 ) )
            {
            // InternalNetSpec.g:267:2: ( ( rule__Login__Group__0 ) )
            // InternalNetSpec.g:268:3: ( rule__Login__Group__0 )
            {
             before(grammarAccess.getLoginAccess().getGroup()); 
            // InternalNetSpec.g:269:3: ( rule__Login__Group__0 )
            // InternalNetSpec.g:269:4: rule__Login__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Login__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLoginAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLogin"


    // $ANTLR start "entryRuleKey"
    // InternalNetSpec.g:278:1: entryRuleKey : ruleKey EOF ;
    public final void entryRuleKey() throws RecognitionException {
        try {
            // InternalNetSpec.g:279:1: ( ruleKey EOF )
            // InternalNetSpec.g:280:1: ruleKey EOF
            {
             before(grammarAccess.getKeyRule()); 
            pushFollow(FOLLOW_1);
            ruleKey();

            state._fsp--;

             after(grammarAccess.getKeyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleKey"


    // $ANTLR start "ruleKey"
    // InternalNetSpec.g:287:1: ruleKey : ( ( rule__Key__Group__0 ) ) ;
    public final void ruleKey() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:291:2: ( ( ( rule__Key__Group__0 ) ) )
            // InternalNetSpec.g:292:2: ( ( rule__Key__Group__0 ) )
            {
            // InternalNetSpec.g:292:2: ( ( rule__Key__Group__0 ) )
            // InternalNetSpec.g:293:3: ( rule__Key__Group__0 )
            {
             before(grammarAccess.getKeyAccess().getGroup()); 
            // InternalNetSpec.g:294:3: ( rule__Key__Group__0 )
            // InternalNetSpec.g:294:4: rule__Key__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Key__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getKeyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleKey"


    // $ANTLR start "entryRuleFirewall"
    // InternalNetSpec.g:303:1: entryRuleFirewall : ruleFirewall EOF ;
    public final void entryRuleFirewall() throws RecognitionException {
        try {
            // InternalNetSpec.g:304:1: ( ruleFirewall EOF )
            // InternalNetSpec.g:305:1: ruleFirewall EOF
            {
             before(grammarAccess.getFirewallRule()); 
            pushFollow(FOLLOW_1);
            ruleFirewall();

            state._fsp--;

             after(grammarAccess.getFirewallRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFirewall"


    // $ANTLR start "ruleFirewall"
    // InternalNetSpec.g:312:1: ruleFirewall : ( ( rule__Firewall__Group__0 ) ) ;
    public final void ruleFirewall() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:316:2: ( ( ( rule__Firewall__Group__0 ) ) )
            // InternalNetSpec.g:317:2: ( ( rule__Firewall__Group__0 ) )
            {
            // InternalNetSpec.g:317:2: ( ( rule__Firewall__Group__0 ) )
            // InternalNetSpec.g:318:3: ( rule__Firewall__Group__0 )
            {
             before(grammarAccess.getFirewallAccess().getGroup()); 
            // InternalNetSpec.g:319:3: ( rule__Firewall__Group__0 )
            // InternalNetSpec.g:319:4: rule__Firewall__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Firewall__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFirewallAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFirewall"


    // $ANTLR start "entryRuleIds"
    // InternalNetSpec.g:328:1: entryRuleIds : ruleIds EOF ;
    public final void entryRuleIds() throws RecognitionException {
        try {
            // InternalNetSpec.g:329:1: ( ruleIds EOF )
            // InternalNetSpec.g:330:1: ruleIds EOF
            {
             before(grammarAccess.getIdsRule()); 
            pushFollow(FOLLOW_1);
            ruleIds();

            state._fsp--;

             after(grammarAccess.getIdsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIds"


    // $ANTLR start "ruleIds"
    // InternalNetSpec.g:337:1: ruleIds : ( ( rule__Ids__Group__0 ) ) ;
    public final void ruleIds() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:341:2: ( ( ( rule__Ids__Group__0 ) ) )
            // InternalNetSpec.g:342:2: ( ( rule__Ids__Group__0 ) )
            {
            // InternalNetSpec.g:342:2: ( ( rule__Ids__Group__0 ) )
            // InternalNetSpec.g:343:3: ( rule__Ids__Group__0 )
            {
             before(grammarAccess.getIdsAccess().getGroup()); 
            // InternalNetSpec.g:344:3: ( rule__Ids__Group__0 )
            // InternalNetSpec.g:344:4: rule__Ids__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Ids__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIdsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIds"


    // $ANTLR start "entryRuleLink"
    // InternalNetSpec.g:353:1: entryRuleLink : ruleLink EOF ;
    public final void entryRuleLink() throws RecognitionException {
        try {
            // InternalNetSpec.g:354:1: ( ruleLink EOF )
            // InternalNetSpec.g:355:1: ruleLink EOF
            {
             before(grammarAccess.getLinkRule()); 
            pushFollow(FOLLOW_1);
            ruleLink();

            state._fsp--;

             after(grammarAccess.getLinkRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLink"


    // $ANTLR start "ruleLink"
    // InternalNetSpec.g:362:1: ruleLink : ( ( rule__Link__Group__0 ) ) ;
    public final void ruleLink() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:366:2: ( ( ( rule__Link__Group__0 ) ) )
            // InternalNetSpec.g:367:2: ( ( rule__Link__Group__0 ) )
            {
            // InternalNetSpec.g:367:2: ( ( rule__Link__Group__0 ) )
            // InternalNetSpec.g:368:3: ( rule__Link__Group__0 )
            {
             before(grammarAccess.getLinkAccess().getGroup()); 
            // InternalNetSpec.g:369:3: ( rule__Link__Group__0 )
            // InternalNetSpec.g:369:4: rule__Link__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Link__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLinkAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLink"


    // $ANTLR start "rule__Model__Alternatives_2"
    // InternalNetSpec.g:377:1: rule__Model__Alternatives_2 : ( ( ( rule__Model__HostsAssignment_2_0 ) ) | ( ( rule__Model__SessionsAssignment_2_1 ) ) | ( ( rule__Model__FilesAssignment_2_2 ) ) | ( ( rule__Model__LoginsAssignment_2_3 ) ) | ( ( rule__Model__KeysAssignment_2_4 ) ) | ( ( rule__Model__FirewallsAssignment_2_5 ) ) | ( ( rule__Model__IdsAssignment_2_6 ) ) | ( ( rule__Model__LinksAssignment_2_7 ) ) | ( ( rule__Model__AntivirusAssignment_2_8 ) ) | ( ( rule__Model__DatasAssignment_2_9 ) ) );
    public final void rule__Model__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:381:1: ( ( ( rule__Model__HostsAssignment_2_0 ) ) | ( ( rule__Model__SessionsAssignment_2_1 ) ) | ( ( rule__Model__FilesAssignment_2_2 ) ) | ( ( rule__Model__LoginsAssignment_2_3 ) ) | ( ( rule__Model__KeysAssignment_2_4 ) ) | ( ( rule__Model__FirewallsAssignment_2_5 ) ) | ( ( rule__Model__IdsAssignment_2_6 ) ) | ( ( rule__Model__LinksAssignment_2_7 ) ) | ( ( rule__Model__AntivirusAssignment_2_8 ) ) | ( ( rule__Model__DatasAssignment_2_9 ) ) )
            int alt1=10;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt1=1;
                }
                break;
            case 36:
                {
                alt1=2;
                }
                break;
            case 39:
                {
                alt1=3;
                }
                break;
            case 37:
                {
                alt1=4;
                }
                break;
            case 40:
                {
                alt1=5;
                }
                break;
            case 34:
                {
                alt1=6;
                }
                break;
            case 33:
                {
                alt1=7;
                }
                break;
            case 44:
                {
                alt1=8;
                }
                break;
            case 35:
                {
                alt1=9;
                }
                break;
            case 43:
                {
                alt1=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalNetSpec.g:382:2: ( ( rule__Model__HostsAssignment_2_0 ) )
                    {
                    // InternalNetSpec.g:382:2: ( ( rule__Model__HostsAssignment_2_0 ) )
                    // InternalNetSpec.g:383:3: ( rule__Model__HostsAssignment_2_0 )
                    {
                     before(grammarAccess.getModelAccess().getHostsAssignment_2_0()); 
                    // InternalNetSpec.g:384:3: ( rule__Model__HostsAssignment_2_0 )
                    // InternalNetSpec.g:384:4: rule__Model__HostsAssignment_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__HostsAssignment_2_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getHostsAssignment_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:388:2: ( ( rule__Model__SessionsAssignment_2_1 ) )
                    {
                    // InternalNetSpec.g:388:2: ( ( rule__Model__SessionsAssignment_2_1 ) )
                    // InternalNetSpec.g:389:3: ( rule__Model__SessionsAssignment_2_1 )
                    {
                     before(grammarAccess.getModelAccess().getSessionsAssignment_2_1()); 
                    // InternalNetSpec.g:390:3: ( rule__Model__SessionsAssignment_2_1 )
                    // InternalNetSpec.g:390:4: rule__Model__SessionsAssignment_2_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__SessionsAssignment_2_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getSessionsAssignment_2_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:394:2: ( ( rule__Model__FilesAssignment_2_2 ) )
                    {
                    // InternalNetSpec.g:394:2: ( ( rule__Model__FilesAssignment_2_2 ) )
                    // InternalNetSpec.g:395:3: ( rule__Model__FilesAssignment_2_2 )
                    {
                     before(grammarAccess.getModelAccess().getFilesAssignment_2_2()); 
                    // InternalNetSpec.g:396:3: ( rule__Model__FilesAssignment_2_2 )
                    // InternalNetSpec.g:396:4: rule__Model__FilesAssignment_2_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__FilesAssignment_2_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getFilesAssignment_2_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:400:2: ( ( rule__Model__LoginsAssignment_2_3 ) )
                    {
                    // InternalNetSpec.g:400:2: ( ( rule__Model__LoginsAssignment_2_3 ) )
                    // InternalNetSpec.g:401:3: ( rule__Model__LoginsAssignment_2_3 )
                    {
                     before(grammarAccess.getModelAccess().getLoginsAssignment_2_3()); 
                    // InternalNetSpec.g:402:3: ( rule__Model__LoginsAssignment_2_3 )
                    // InternalNetSpec.g:402:4: rule__Model__LoginsAssignment_2_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__LoginsAssignment_2_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getLoginsAssignment_2_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalNetSpec.g:406:2: ( ( rule__Model__KeysAssignment_2_4 ) )
                    {
                    // InternalNetSpec.g:406:2: ( ( rule__Model__KeysAssignment_2_4 ) )
                    // InternalNetSpec.g:407:3: ( rule__Model__KeysAssignment_2_4 )
                    {
                     before(grammarAccess.getModelAccess().getKeysAssignment_2_4()); 
                    // InternalNetSpec.g:408:3: ( rule__Model__KeysAssignment_2_4 )
                    // InternalNetSpec.g:408:4: rule__Model__KeysAssignment_2_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__KeysAssignment_2_4();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getKeysAssignment_2_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalNetSpec.g:412:2: ( ( rule__Model__FirewallsAssignment_2_5 ) )
                    {
                    // InternalNetSpec.g:412:2: ( ( rule__Model__FirewallsAssignment_2_5 ) )
                    // InternalNetSpec.g:413:3: ( rule__Model__FirewallsAssignment_2_5 )
                    {
                     before(grammarAccess.getModelAccess().getFirewallsAssignment_2_5()); 
                    // InternalNetSpec.g:414:3: ( rule__Model__FirewallsAssignment_2_5 )
                    // InternalNetSpec.g:414:4: rule__Model__FirewallsAssignment_2_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__FirewallsAssignment_2_5();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getFirewallsAssignment_2_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalNetSpec.g:418:2: ( ( rule__Model__IdsAssignment_2_6 ) )
                    {
                    // InternalNetSpec.g:418:2: ( ( rule__Model__IdsAssignment_2_6 ) )
                    // InternalNetSpec.g:419:3: ( rule__Model__IdsAssignment_2_6 )
                    {
                     before(grammarAccess.getModelAccess().getIdsAssignment_2_6()); 
                    // InternalNetSpec.g:420:3: ( rule__Model__IdsAssignment_2_6 )
                    // InternalNetSpec.g:420:4: rule__Model__IdsAssignment_2_6
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__IdsAssignment_2_6();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getIdsAssignment_2_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalNetSpec.g:424:2: ( ( rule__Model__LinksAssignment_2_7 ) )
                    {
                    // InternalNetSpec.g:424:2: ( ( rule__Model__LinksAssignment_2_7 ) )
                    // InternalNetSpec.g:425:3: ( rule__Model__LinksAssignment_2_7 )
                    {
                     before(grammarAccess.getModelAccess().getLinksAssignment_2_7()); 
                    // InternalNetSpec.g:426:3: ( rule__Model__LinksAssignment_2_7 )
                    // InternalNetSpec.g:426:4: rule__Model__LinksAssignment_2_7
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__LinksAssignment_2_7();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getLinksAssignment_2_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalNetSpec.g:430:2: ( ( rule__Model__AntivirusAssignment_2_8 ) )
                    {
                    // InternalNetSpec.g:430:2: ( ( rule__Model__AntivirusAssignment_2_8 ) )
                    // InternalNetSpec.g:431:3: ( rule__Model__AntivirusAssignment_2_8 )
                    {
                     before(grammarAccess.getModelAccess().getAntivirusAssignment_2_8()); 
                    // InternalNetSpec.g:432:3: ( rule__Model__AntivirusAssignment_2_8 )
                    // InternalNetSpec.g:432:4: rule__Model__AntivirusAssignment_2_8
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__AntivirusAssignment_2_8();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getAntivirusAssignment_2_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalNetSpec.g:436:2: ( ( rule__Model__DatasAssignment_2_9 ) )
                    {
                    // InternalNetSpec.g:436:2: ( ( rule__Model__DatasAssignment_2_9 ) )
                    // InternalNetSpec.g:437:3: ( rule__Model__DatasAssignment_2_9 )
                    {
                     before(grammarAccess.getModelAccess().getDatasAssignment_2_9()); 
                    // InternalNetSpec.g:438:3: ( rule__Model__DatasAssignment_2_9 )
                    // InternalNetSpec.g:438:4: rule__Model__DatasAssignment_2_9
                    {
                    pushFollow(FOLLOW_2);
                    rule__Model__DatasAssignment_2_9();

                    state._fsp--;


                    }

                     after(grammarAccess.getModelAccess().getDatasAssignment_2_9()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Alternatives_2"


    // $ANTLR start "rule__Attacker__HackAlternatives_4_0"
    // InternalNetSpec.g:446:1: rule__Attacker__HackAlternatives_4_0 : ( ( 'None' ) | ( 'Low' ) | ( 'Medium' ) | ( 'High' ) );
    public final void rule__Attacker__HackAlternatives_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:450:1: ( ( 'None' ) | ( 'Low' ) | ( 'Medium' ) | ( 'High' ) )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            case 14:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalNetSpec.g:451:2: ( 'None' )
                    {
                    // InternalNetSpec.g:451:2: ( 'None' )
                    // InternalNetSpec.g:452:3: 'None'
                    {
                     before(grammarAccess.getAttackerAccess().getHackNoneKeyword_4_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getHackNoneKeyword_4_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:457:2: ( 'Low' )
                    {
                    // InternalNetSpec.g:457:2: ( 'Low' )
                    // InternalNetSpec.g:458:3: 'Low'
                    {
                     before(grammarAccess.getAttackerAccess().getHackLowKeyword_4_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getHackLowKeyword_4_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:463:2: ( 'Medium' )
                    {
                    // InternalNetSpec.g:463:2: ( 'Medium' )
                    // InternalNetSpec.g:464:3: 'Medium'
                    {
                     before(grammarAccess.getAttackerAccess().getHackMediumKeyword_4_0_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getHackMediumKeyword_4_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:469:2: ( 'High' )
                    {
                    // InternalNetSpec.g:469:2: ( 'High' )
                    // InternalNetSpec.g:470:3: 'High'
                    {
                     before(grammarAccess.getAttackerAccess().getHackHighKeyword_4_0_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getHackHighKeyword_4_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__HackAlternatives_4_0"


    // $ANTLR start "rule__Attacker__CryptoAlternatives_6_0"
    // InternalNetSpec.g:479:1: rule__Attacker__CryptoAlternatives_6_0 : ( ( 'None' ) | ( 'Low' ) | ( 'Medium' ) | ( 'High' ) );
    public final void rule__Attacker__CryptoAlternatives_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:483:1: ( ( 'None' ) | ( 'Low' ) | ( 'Medium' ) | ( 'High' ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt3=1;
                }
                break;
            case 12:
                {
                alt3=2;
                }
                break;
            case 13:
                {
                alt3=3;
                }
                break;
            case 14:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalNetSpec.g:484:2: ( 'None' )
                    {
                    // InternalNetSpec.g:484:2: ( 'None' )
                    // InternalNetSpec.g:485:3: 'None'
                    {
                     before(grammarAccess.getAttackerAccess().getCryptoNoneKeyword_6_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getCryptoNoneKeyword_6_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:490:2: ( 'Low' )
                    {
                    // InternalNetSpec.g:490:2: ( 'Low' )
                    // InternalNetSpec.g:491:3: 'Low'
                    {
                     before(grammarAccess.getAttackerAccess().getCryptoLowKeyword_6_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getCryptoLowKeyword_6_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:496:2: ( 'Medium' )
                    {
                    // InternalNetSpec.g:496:2: ( 'Medium' )
                    // InternalNetSpec.g:497:3: 'Medium'
                    {
                     before(grammarAccess.getAttackerAccess().getCryptoMediumKeyword_6_0_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getCryptoMediumKeyword_6_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:502:2: ( 'High' )
                    {
                    // InternalNetSpec.g:502:2: ( 'High' )
                    // InternalNetSpec.g:503:3: 'High'
                    {
                     before(grammarAccess.getAttackerAccess().getCryptoHighKeyword_6_0_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getCryptoHighKeyword_6_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__CryptoAlternatives_6_0"


    // $ANTLR start "rule__Attacker__VirusAlternatives_8_0"
    // InternalNetSpec.g:512:1: rule__Attacker__VirusAlternatives_8_0 : ( ( 'None' ) | ( 'Low' ) | ( 'Medium' ) | ( 'High' ) );
    public final void rule__Attacker__VirusAlternatives_8_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:516:1: ( ( 'None' ) | ( 'Low' ) | ( 'Medium' ) | ( 'High' ) )
            int alt4=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt4=1;
                }
                break;
            case 12:
                {
                alt4=2;
                }
                break;
            case 13:
                {
                alt4=3;
                }
                break;
            case 14:
                {
                alt4=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalNetSpec.g:517:2: ( 'None' )
                    {
                    // InternalNetSpec.g:517:2: ( 'None' )
                    // InternalNetSpec.g:518:3: 'None'
                    {
                     before(grammarAccess.getAttackerAccess().getVirusNoneKeyword_8_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getVirusNoneKeyword_8_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:523:2: ( 'Low' )
                    {
                    // InternalNetSpec.g:523:2: ( 'Low' )
                    // InternalNetSpec.g:524:3: 'Low'
                    {
                     before(grammarAccess.getAttackerAccess().getVirusLowKeyword_8_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getVirusLowKeyword_8_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:529:2: ( 'Medium' )
                    {
                    // InternalNetSpec.g:529:2: ( 'Medium' )
                    // InternalNetSpec.g:530:3: 'Medium'
                    {
                     before(grammarAccess.getAttackerAccess().getVirusMediumKeyword_8_0_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getVirusMediumKeyword_8_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:535:2: ( 'High' )
                    {
                    // InternalNetSpec.g:535:2: ( 'High' )
                    // InternalNetSpec.g:536:3: 'High'
                    {
                     before(grammarAccess.getAttackerAccess().getVirusHighKeyword_8_0_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getVirusHighKeyword_8_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__VirusAlternatives_8_0"


    // $ANTLR start "rule__Attacker__LoginAlternatives_10_0"
    // InternalNetSpec.g:545:1: rule__Attacker__LoginAlternatives_10_0 : ( ( 'None' ) | ( 'Low' ) | ( 'Medium' ) | ( 'High' ) );
    public final void rule__Attacker__LoginAlternatives_10_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:549:1: ( ( 'None' ) | ( 'Low' ) | ( 'Medium' ) | ( 'High' ) )
            int alt5=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt5=1;
                }
                break;
            case 12:
                {
                alt5=2;
                }
                break;
            case 13:
                {
                alt5=3;
                }
                break;
            case 14:
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalNetSpec.g:550:2: ( 'None' )
                    {
                    // InternalNetSpec.g:550:2: ( 'None' )
                    // InternalNetSpec.g:551:3: 'None'
                    {
                     before(grammarAccess.getAttackerAccess().getLoginNoneKeyword_10_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getLoginNoneKeyword_10_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:556:2: ( 'Low' )
                    {
                    // InternalNetSpec.g:556:2: ( 'Low' )
                    // InternalNetSpec.g:557:3: 'Low'
                    {
                     before(grammarAccess.getAttackerAccess().getLoginLowKeyword_10_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getLoginLowKeyword_10_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:562:2: ( 'Medium' )
                    {
                    // InternalNetSpec.g:562:2: ( 'Medium' )
                    // InternalNetSpec.g:563:3: 'Medium'
                    {
                     before(grammarAccess.getAttackerAccess().getLoginMediumKeyword_10_0_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getLoginMediumKeyword_10_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:568:2: ( 'High' )
                    {
                    // InternalNetSpec.g:568:2: ( 'High' )
                    // InternalNetSpec.g:569:3: 'High'
                    {
                     before(grammarAccess.getAttackerAccess().getLoginHighKeyword_10_0_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getAttackerAccess().getLoginHighKeyword_10_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__LoginAlternatives_10_0"


    // $ANTLR start "rule__Antivirus__LevelAlternatives_5_0"
    // InternalNetSpec.g:578:1: rule__Antivirus__LevelAlternatives_5_0 : ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) | ( 'Perfect' ) );
    public final void rule__Antivirus__LevelAlternatives_5_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:582:1: ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) | ( 'Perfect' ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt6=1;
                }
                break;
            case 13:
                {
                alt6=2;
                }
                break;
            case 14:
                {
                alt6=3;
                }
                break;
            case 15:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalNetSpec.g:583:2: ( 'Low' )
                    {
                    // InternalNetSpec.g:583:2: ( 'Low' )
                    // InternalNetSpec.g:584:3: 'Low'
                    {
                     before(grammarAccess.getAntivirusAccess().getLevelLowKeyword_5_0_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAntivirusAccess().getLevelLowKeyword_5_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:589:2: ( 'Medium' )
                    {
                    // InternalNetSpec.g:589:2: ( 'Medium' )
                    // InternalNetSpec.g:590:3: 'Medium'
                    {
                     before(grammarAccess.getAntivirusAccess().getLevelMediumKeyword_5_0_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getAntivirusAccess().getLevelMediumKeyword_5_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:595:2: ( 'High' )
                    {
                    // InternalNetSpec.g:595:2: ( 'High' )
                    // InternalNetSpec.g:596:3: 'High'
                    {
                     before(grammarAccess.getAntivirusAccess().getLevelHighKeyword_5_0_2()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getAntivirusAccess().getLevelHighKeyword_5_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:601:2: ( 'Perfect' )
                    {
                    // InternalNetSpec.g:601:2: ( 'Perfect' )
                    // InternalNetSpec.g:602:3: 'Perfect'
                    {
                     before(grammarAccess.getAntivirusAccess().getLevelPerfectKeyword_5_0_3()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getAntivirusAccess().getLevelPerfectKeyword_5_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__LevelAlternatives_5_0"


    // $ANTLR start "rule__Login__LevelAlternatives_5_0"
    // InternalNetSpec.g:611:1: rule__Login__LevelAlternatives_5_0 : ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) );
    public final void rule__Login__LevelAlternatives_5_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:615:1: ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) )
            int alt7=3;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt7=1;
                }
                break;
            case 13:
                {
                alt7=2;
                }
                break;
            case 14:
                {
                alt7=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalNetSpec.g:616:2: ( 'Low' )
                    {
                    // InternalNetSpec.g:616:2: ( 'Low' )
                    // InternalNetSpec.g:617:3: 'Low'
                    {
                     before(grammarAccess.getLoginAccess().getLevelLowKeyword_5_0_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getLoginAccess().getLevelLowKeyword_5_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:622:2: ( 'Medium' )
                    {
                    // InternalNetSpec.g:622:2: ( 'Medium' )
                    // InternalNetSpec.g:623:3: 'Medium'
                    {
                     before(grammarAccess.getLoginAccess().getLevelMediumKeyword_5_0_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getLoginAccess().getLevelMediumKeyword_5_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:628:2: ( 'High' )
                    {
                    // InternalNetSpec.g:628:2: ( 'High' )
                    // InternalNetSpec.g:629:3: 'High'
                    {
                     before(grammarAccess.getLoginAccess().getLevelHighKeyword_5_0_2()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getLoginAccess().getLevelHighKeyword_5_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__LevelAlternatives_5_0"


    // $ANTLR start "rule__Key__LevelAlternatives_5_0"
    // InternalNetSpec.g:638:1: rule__Key__LevelAlternatives_5_0 : ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) );
    public final void rule__Key__LevelAlternatives_5_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:642:1: ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) )
            int alt8=3;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt8=1;
                }
                break;
            case 13:
                {
                alt8=2;
                }
                break;
            case 14:
                {
                alt8=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalNetSpec.g:643:2: ( 'Low' )
                    {
                    // InternalNetSpec.g:643:2: ( 'Low' )
                    // InternalNetSpec.g:644:3: 'Low'
                    {
                     before(grammarAccess.getKeyAccess().getLevelLowKeyword_5_0_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getKeyAccess().getLevelLowKeyword_5_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:649:2: ( 'Medium' )
                    {
                    // InternalNetSpec.g:649:2: ( 'Medium' )
                    // InternalNetSpec.g:650:3: 'Medium'
                    {
                     before(grammarAccess.getKeyAccess().getLevelMediumKeyword_5_0_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getKeyAccess().getLevelMediumKeyword_5_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:655:2: ( 'High' )
                    {
                    // InternalNetSpec.g:655:2: ( 'High' )
                    // InternalNetSpec.g:656:3: 'High'
                    {
                     before(grammarAccess.getKeyAccess().getLevelHighKeyword_5_0_2()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getKeyAccess().getLevelHighKeyword_5_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__LevelAlternatives_5_0"


    // $ANTLR start "rule__Firewall__LevelAlternatives_5_0"
    // InternalNetSpec.g:665:1: rule__Firewall__LevelAlternatives_5_0 : ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) | ( 'Perfect' ) );
    public final void rule__Firewall__LevelAlternatives_5_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:669:1: ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) | ( 'Perfect' ) )
            int alt9=4;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt9=1;
                }
                break;
            case 13:
                {
                alt9=2;
                }
                break;
            case 14:
                {
                alt9=3;
                }
                break;
            case 15:
                {
                alt9=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalNetSpec.g:670:2: ( 'Low' )
                    {
                    // InternalNetSpec.g:670:2: ( 'Low' )
                    // InternalNetSpec.g:671:3: 'Low'
                    {
                     before(grammarAccess.getFirewallAccess().getLevelLowKeyword_5_0_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getFirewallAccess().getLevelLowKeyword_5_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:676:2: ( 'Medium' )
                    {
                    // InternalNetSpec.g:676:2: ( 'Medium' )
                    // InternalNetSpec.g:677:3: 'Medium'
                    {
                     before(grammarAccess.getFirewallAccess().getLevelMediumKeyword_5_0_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getFirewallAccess().getLevelMediumKeyword_5_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:682:2: ( 'High' )
                    {
                    // InternalNetSpec.g:682:2: ( 'High' )
                    // InternalNetSpec.g:683:3: 'High'
                    {
                     before(grammarAccess.getFirewallAccess().getLevelHighKeyword_5_0_2()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getFirewallAccess().getLevelHighKeyword_5_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:688:2: ( 'Perfect' )
                    {
                    // InternalNetSpec.g:688:2: ( 'Perfect' )
                    // InternalNetSpec.g:689:3: 'Perfect'
                    {
                     before(grammarAccess.getFirewallAccess().getLevelPerfectKeyword_5_0_3()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getFirewallAccess().getLevelPerfectKeyword_5_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__LevelAlternatives_5_0"


    // $ANTLR start "rule__Ids__LevelAlternatives_6_0"
    // InternalNetSpec.g:698:1: rule__Ids__LevelAlternatives_6_0 : ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) | ( 'Perfect' ) );
    public final void rule__Ids__LevelAlternatives_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:702:1: ( ( 'Low' ) | ( 'Medium' ) | ( 'High' ) | ( 'Perfect' ) )
            int alt10=4;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt10=1;
                }
                break;
            case 13:
                {
                alt10=2;
                }
                break;
            case 14:
                {
                alt10=3;
                }
                break;
            case 15:
                {
                alt10=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalNetSpec.g:703:2: ( 'Low' )
                    {
                    // InternalNetSpec.g:703:2: ( 'Low' )
                    // InternalNetSpec.g:704:3: 'Low'
                    {
                     before(grammarAccess.getIdsAccess().getLevelLowKeyword_6_0_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getIdsAccess().getLevelLowKeyword_6_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNetSpec.g:709:2: ( 'Medium' )
                    {
                    // InternalNetSpec.g:709:2: ( 'Medium' )
                    // InternalNetSpec.g:710:3: 'Medium'
                    {
                     before(grammarAccess.getIdsAccess().getLevelMediumKeyword_6_0_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getIdsAccess().getLevelMediumKeyword_6_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalNetSpec.g:715:2: ( 'High' )
                    {
                    // InternalNetSpec.g:715:2: ( 'High' )
                    // InternalNetSpec.g:716:3: 'High'
                    {
                     before(grammarAccess.getIdsAccess().getLevelHighKeyword_6_0_2()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getIdsAccess().getLevelHighKeyword_6_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalNetSpec.g:721:2: ( 'Perfect' )
                    {
                    // InternalNetSpec.g:721:2: ( 'Perfect' )
                    // InternalNetSpec.g:722:3: 'Perfect'
                    {
                     before(grammarAccess.getIdsAccess().getLevelPerfectKeyword_6_0_3()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getIdsAccess().getLevelPerfectKeyword_6_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__LevelAlternatives_6_0"


    // $ANTLR start "rule__Model__Group__0"
    // InternalNetSpec.g:731:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:735:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // InternalNetSpec.g:736:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // InternalNetSpec.g:743:1: rule__Model__Group__0__Impl : ( ( rule__Model__AttackerAssignment_0 ) ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:747:1: ( ( ( rule__Model__AttackerAssignment_0 ) ) )
            // InternalNetSpec.g:748:1: ( ( rule__Model__AttackerAssignment_0 ) )
            {
            // InternalNetSpec.g:748:1: ( ( rule__Model__AttackerAssignment_0 ) )
            // InternalNetSpec.g:749:2: ( rule__Model__AttackerAssignment_0 )
            {
             before(grammarAccess.getModelAccess().getAttackerAssignment_0()); 
            // InternalNetSpec.g:750:2: ( rule__Model__AttackerAssignment_0 )
            // InternalNetSpec.g:750:3: rule__Model__AttackerAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Model__AttackerAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getAttackerAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // InternalNetSpec.g:758:1: rule__Model__Group__1 : rule__Model__Group__1__Impl rule__Model__Group__2 ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:762:1: ( rule__Model__Group__1__Impl rule__Model__Group__2 )
            // InternalNetSpec.g:763:2: rule__Model__Group__1__Impl rule__Model__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Model__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // InternalNetSpec.g:770:1: rule__Model__Group__1__Impl : ( ( rule__Model__GoalAssignment_1 ) ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:774:1: ( ( ( rule__Model__GoalAssignment_1 ) ) )
            // InternalNetSpec.g:775:1: ( ( rule__Model__GoalAssignment_1 ) )
            {
            // InternalNetSpec.g:775:1: ( ( rule__Model__GoalAssignment_1 ) )
            // InternalNetSpec.g:776:2: ( rule__Model__GoalAssignment_1 )
            {
             before(grammarAccess.getModelAccess().getGoalAssignment_1()); 
            // InternalNetSpec.g:777:2: ( rule__Model__GoalAssignment_1 )
            // InternalNetSpec.g:777:3: rule__Model__GoalAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Model__GoalAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGoalAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Model__Group__2"
    // InternalNetSpec.g:785:1: rule__Model__Group__2 : rule__Model__Group__2__Impl ;
    public final void rule__Model__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:789:1: ( rule__Model__Group__2__Impl )
            // InternalNetSpec.g:790:2: rule__Model__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2"


    // $ANTLR start "rule__Model__Group__2__Impl"
    // InternalNetSpec.g:796:1: rule__Model__Group__2__Impl : ( ( rule__Model__Alternatives_2 )* ) ;
    public final void rule__Model__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:800:1: ( ( ( rule__Model__Alternatives_2 )* ) )
            // InternalNetSpec.g:801:1: ( ( rule__Model__Alternatives_2 )* )
            {
            // InternalNetSpec.g:801:1: ( ( rule__Model__Alternatives_2 )* )
            // InternalNetSpec.g:802:2: ( rule__Model__Alternatives_2 )*
            {
             before(grammarAccess.getModelAccess().getAlternatives_2()); 
            // InternalNetSpec.g:803:2: ( rule__Model__Alternatives_2 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==30||(LA11_0>=33 && LA11_0<=37)||(LA11_0>=39 && LA11_0<=40)||(LA11_0>=43 && LA11_0<=44)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalNetSpec.g:803:3: rule__Model__Alternatives_2
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Model__Alternatives_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2__Impl"


    // $ANTLR start "rule__Attacker__Group__0"
    // InternalNetSpec.g:812:1: rule__Attacker__Group__0 : rule__Attacker__Group__0__Impl rule__Attacker__Group__1 ;
    public final void rule__Attacker__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:816:1: ( rule__Attacker__Group__0__Impl rule__Attacker__Group__1 )
            // InternalNetSpec.g:817:2: rule__Attacker__Group__0__Impl rule__Attacker__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Attacker__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__0"


    // $ANTLR start "rule__Attacker__Group__0__Impl"
    // InternalNetSpec.g:824:1: rule__Attacker__Group__0__Impl : ( () ) ;
    public final void rule__Attacker__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:828:1: ( ( () ) )
            // InternalNetSpec.g:829:1: ( () )
            {
            // InternalNetSpec.g:829:1: ( () )
            // InternalNetSpec.g:830:2: ()
            {
             before(grammarAccess.getAttackerAccess().getAttackerAction_0()); 
            // InternalNetSpec.g:831:2: ()
            // InternalNetSpec.g:831:3: 
            {
            }

             after(grammarAccess.getAttackerAccess().getAttackerAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__0__Impl"


    // $ANTLR start "rule__Attacker__Group__1"
    // InternalNetSpec.g:839:1: rule__Attacker__Group__1 : rule__Attacker__Group__1__Impl rule__Attacker__Group__2 ;
    public final void rule__Attacker__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:843:1: ( rule__Attacker__Group__1__Impl rule__Attacker__Group__2 )
            // InternalNetSpec.g:844:2: rule__Attacker__Group__1__Impl rule__Attacker__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Attacker__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__1"


    // $ANTLR start "rule__Attacker__Group__1__Impl"
    // InternalNetSpec.g:851:1: rule__Attacker__Group__1__Impl : ( 'Attacker' ) ;
    public final void rule__Attacker__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:855:1: ( ( 'Attacker' ) )
            // InternalNetSpec.g:856:1: ( 'Attacker' )
            {
            // InternalNetSpec.g:856:1: ( 'Attacker' )
            // InternalNetSpec.g:857:2: 'Attacker'
            {
             before(grammarAccess.getAttackerAccess().getAttackerKeyword_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getAttackerKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__1__Impl"


    // $ANTLR start "rule__Attacker__Group__2"
    // InternalNetSpec.g:866:1: rule__Attacker__Group__2 : rule__Attacker__Group__2__Impl rule__Attacker__Group__3 ;
    public final void rule__Attacker__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:870:1: ( rule__Attacker__Group__2__Impl rule__Attacker__Group__3 )
            // InternalNetSpec.g:871:2: rule__Attacker__Group__2__Impl rule__Attacker__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Attacker__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__2"


    // $ANTLR start "rule__Attacker__Group__2__Impl"
    // InternalNetSpec.g:878:1: rule__Attacker__Group__2__Impl : ( '{' ) ;
    public final void rule__Attacker__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:882:1: ( ( '{' ) )
            // InternalNetSpec.g:883:1: ( '{' )
            {
            // InternalNetSpec.g:883:1: ( '{' )
            // InternalNetSpec.g:884:2: '{'
            {
             before(grammarAccess.getAttackerAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__2__Impl"


    // $ANTLR start "rule__Attacker__Group__3"
    // InternalNetSpec.g:893:1: rule__Attacker__Group__3 : rule__Attacker__Group__3__Impl rule__Attacker__Group__4 ;
    public final void rule__Attacker__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:897:1: ( rule__Attacker__Group__3__Impl rule__Attacker__Group__4 )
            // InternalNetSpec.g:898:2: rule__Attacker__Group__3__Impl rule__Attacker__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Attacker__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__3"


    // $ANTLR start "rule__Attacker__Group__3__Impl"
    // InternalNetSpec.g:905:1: rule__Attacker__Group__3__Impl : ( 'hackLevel' ) ;
    public final void rule__Attacker__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:909:1: ( ( 'hackLevel' ) )
            // InternalNetSpec.g:910:1: ( 'hackLevel' )
            {
            // InternalNetSpec.g:910:1: ( 'hackLevel' )
            // InternalNetSpec.g:911:2: 'hackLevel'
            {
             before(grammarAccess.getAttackerAccess().getHackLevelKeyword_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getHackLevelKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__3__Impl"


    // $ANTLR start "rule__Attacker__Group__4"
    // InternalNetSpec.g:920:1: rule__Attacker__Group__4 : rule__Attacker__Group__4__Impl rule__Attacker__Group__5 ;
    public final void rule__Attacker__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:924:1: ( rule__Attacker__Group__4__Impl rule__Attacker__Group__5 )
            // InternalNetSpec.g:925:2: rule__Attacker__Group__4__Impl rule__Attacker__Group__5
            {
            pushFollow(FOLLOW_10);
            rule__Attacker__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__4"


    // $ANTLR start "rule__Attacker__Group__4__Impl"
    // InternalNetSpec.g:932:1: rule__Attacker__Group__4__Impl : ( ( rule__Attacker__HackAssignment_4 ) ) ;
    public final void rule__Attacker__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:936:1: ( ( ( rule__Attacker__HackAssignment_4 ) ) )
            // InternalNetSpec.g:937:1: ( ( rule__Attacker__HackAssignment_4 ) )
            {
            // InternalNetSpec.g:937:1: ( ( rule__Attacker__HackAssignment_4 ) )
            // InternalNetSpec.g:938:2: ( rule__Attacker__HackAssignment_4 )
            {
             before(grammarAccess.getAttackerAccess().getHackAssignment_4()); 
            // InternalNetSpec.g:939:2: ( rule__Attacker__HackAssignment_4 )
            // InternalNetSpec.g:939:3: rule__Attacker__HackAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__HackAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getHackAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__4__Impl"


    // $ANTLR start "rule__Attacker__Group__5"
    // InternalNetSpec.g:947:1: rule__Attacker__Group__5 : rule__Attacker__Group__5__Impl rule__Attacker__Group__6 ;
    public final void rule__Attacker__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:951:1: ( rule__Attacker__Group__5__Impl rule__Attacker__Group__6 )
            // InternalNetSpec.g:952:2: rule__Attacker__Group__5__Impl rule__Attacker__Group__6
            {
            pushFollow(FOLLOW_9);
            rule__Attacker__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__5"


    // $ANTLR start "rule__Attacker__Group__5__Impl"
    // InternalNetSpec.g:959:1: rule__Attacker__Group__5__Impl : ( 'cryptoLevel' ) ;
    public final void rule__Attacker__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:963:1: ( ( 'cryptoLevel' ) )
            // InternalNetSpec.g:964:1: ( 'cryptoLevel' )
            {
            // InternalNetSpec.g:964:1: ( 'cryptoLevel' )
            // InternalNetSpec.g:965:2: 'cryptoLevel'
            {
             before(grammarAccess.getAttackerAccess().getCryptoLevelKeyword_5()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getCryptoLevelKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__5__Impl"


    // $ANTLR start "rule__Attacker__Group__6"
    // InternalNetSpec.g:974:1: rule__Attacker__Group__6 : rule__Attacker__Group__6__Impl rule__Attacker__Group__7 ;
    public final void rule__Attacker__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:978:1: ( rule__Attacker__Group__6__Impl rule__Attacker__Group__7 )
            // InternalNetSpec.g:979:2: rule__Attacker__Group__6__Impl rule__Attacker__Group__7
            {
            pushFollow(FOLLOW_11);
            rule__Attacker__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__6"


    // $ANTLR start "rule__Attacker__Group__6__Impl"
    // InternalNetSpec.g:986:1: rule__Attacker__Group__6__Impl : ( ( rule__Attacker__CryptoAssignment_6 ) ) ;
    public final void rule__Attacker__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:990:1: ( ( ( rule__Attacker__CryptoAssignment_6 ) ) )
            // InternalNetSpec.g:991:1: ( ( rule__Attacker__CryptoAssignment_6 ) )
            {
            // InternalNetSpec.g:991:1: ( ( rule__Attacker__CryptoAssignment_6 ) )
            // InternalNetSpec.g:992:2: ( rule__Attacker__CryptoAssignment_6 )
            {
             before(grammarAccess.getAttackerAccess().getCryptoAssignment_6()); 
            // InternalNetSpec.g:993:2: ( rule__Attacker__CryptoAssignment_6 )
            // InternalNetSpec.g:993:3: rule__Attacker__CryptoAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__CryptoAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getCryptoAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__6__Impl"


    // $ANTLR start "rule__Attacker__Group__7"
    // InternalNetSpec.g:1001:1: rule__Attacker__Group__7 : rule__Attacker__Group__7__Impl rule__Attacker__Group__8 ;
    public final void rule__Attacker__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1005:1: ( rule__Attacker__Group__7__Impl rule__Attacker__Group__8 )
            // InternalNetSpec.g:1006:2: rule__Attacker__Group__7__Impl rule__Attacker__Group__8
            {
            pushFollow(FOLLOW_9);
            rule__Attacker__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__7"


    // $ANTLR start "rule__Attacker__Group__7__Impl"
    // InternalNetSpec.g:1013:1: rule__Attacker__Group__7__Impl : ( 'virusLevel' ) ;
    public final void rule__Attacker__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1017:1: ( ( 'virusLevel' ) )
            // InternalNetSpec.g:1018:1: ( 'virusLevel' )
            {
            // InternalNetSpec.g:1018:1: ( 'virusLevel' )
            // InternalNetSpec.g:1019:2: 'virusLevel'
            {
             before(grammarAccess.getAttackerAccess().getVirusLevelKeyword_7()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getVirusLevelKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__7__Impl"


    // $ANTLR start "rule__Attacker__Group__8"
    // InternalNetSpec.g:1028:1: rule__Attacker__Group__8 : rule__Attacker__Group__8__Impl rule__Attacker__Group__9 ;
    public final void rule__Attacker__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1032:1: ( rule__Attacker__Group__8__Impl rule__Attacker__Group__9 )
            // InternalNetSpec.g:1033:2: rule__Attacker__Group__8__Impl rule__Attacker__Group__9
            {
            pushFollow(FOLLOW_12);
            rule__Attacker__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__8"


    // $ANTLR start "rule__Attacker__Group__8__Impl"
    // InternalNetSpec.g:1040:1: rule__Attacker__Group__8__Impl : ( ( rule__Attacker__VirusAssignment_8 ) ) ;
    public final void rule__Attacker__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1044:1: ( ( ( rule__Attacker__VirusAssignment_8 ) ) )
            // InternalNetSpec.g:1045:1: ( ( rule__Attacker__VirusAssignment_8 ) )
            {
            // InternalNetSpec.g:1045:1: ( ( rule__Attacker__VirusAssignment_8 ) )
            // InternalNetSpec.g:1046:2: ( rule__Attacker__VirusAssignment_8 )
            {
             before(grammarAccess.getAttackerAccess().getVirusAssignment_8()); 
            // InternalNetSpec.g:1047:2: ( rule__Attacker__VirusAssignment_8 )
            // InternalNetSpec.g:1047:3: rule__Attacker__VirusAssignment_8
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__VirusAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getVirusAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__8__Impl"


    // $ANTLR start "rule__Attacker__Group__9"
    // InternalNetSpec.g:1055:1: rule__Attacker__Group__9 : rule__Attacker__Group__9__Impl rule__Attacker__Group__10 ;
    public final void rule__Attacker__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1059:1: ( rule__Attacker__Group__9__Impl rule__Attacker__Group__10 )
            // InternalNetSpec.g:1060:2: rule__Attacker__Group__9__Impl rule__Attacker__Group__10
            {
            pushFollow(FOLLOW_9);
            rule__Attacker__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__9"


    // $ANTLR start "rule__Attacker__Group__9__Impl"
    // InternalNetSpec.g:1067:1: rule__Attacker__Group__9__Impl : ( 'loginLevel' ) ;
    public final void rule__Attacker__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1071:1: ( ( 'loginLevel' ) )
            // InternalNetSpec.g:1072:1: ( 'loginLevel' )
            {
            // InternalNetSpec.g:1072:1: ( 'loginLevel' )
            // InternalNetSpec.g:1073:2: 'loginLevel'
            {
             before(grammarAccess.getAttackerAccess().getLoginLevelKeyword_9()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getLoginLevelKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__9__Impl"


    // $ANTLR start "rule__Attacker__Group__10"
    // InternalNetSpec.g:1082:1: rule__Attacker__Group__10 : rule__Attacker__Group__10__Impl rule__Attacker__Group__11 ;
    public final void rule__Attacker__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1086:1: ( rule__Attacker__Group__10__Impl rule__Attacker__Group__11 )
            // InternalNetSpec.g:1087:2: rule__Attacker__Group__10__Impl rule__Attacker__Group__11
            {
            pushFollow(FOLLOW_13);
            rule__Attacker__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__10"


    // $ANTLR start "rule__Attacker__Group__10__Impl"
    // InternalNetSpec.g:1094:1: rule__Attacker__Group__10__Impl : ( ( rule__Attacker__LoginAssignment_10 ) ) ;
    public final void rule__Attacker__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1098:1: ( ( ( rule__Attacker__LoginAssignment_10 ) ) )
            // InternalNetSpec.g:1099:1: ( ( rule__Attacker__LoginAssignment_10 ) )
            {
            // InternalNetSpec.g:1099:1: ( ( rule__Attacker__LoginAssignment_10 ) )
            // InternalNetSpec.g:1100:2: ( rule__Attacker__LoginAssignment_10 )
            {
             before(grammarAccess.getAttackerAccess().getLoginAssignment_10()); 
            // InternalNetSpec.g:1101:2: ( rule__Attacker__LoginAssignment_10 )
            // InternalNetSpec.g:1101:3: rule__Attacker__LoginAssignment_10
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__LoginAssignment_10();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getLoginAssignment_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__10__Impl"


    // $ANTLR start "rule__Attacker__Group__11"
    // InternalNetSpec.g:1109:1: rule__Attacker__Group__11 : rule__Attacker__Group__11__Impl rule__Attacker__Group__12 ;
    public final void rule__Attacker__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1113:1: ( rule__Attacker__Group__11__Impl rule__Attacker__Group__12 )
            // InternalNetSpec.g:1114:2: rule__Attacker__Group__11__Impl rule__Attacker__Group__12
            {
            pushFollow(FOLLOW_13);
            rule__Attacker__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__11"


    // $ANTLR start "rule__Attacker__Group__11__Impl"
    // InternalNetSpec.g:1121:1: rule__Attacker__Group__11__Impl : ( ( rule__Attacker__Group_11__0 )? ) ;
    public final void rule__Attacker__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1125:1: ( ( ( rule__Attacker__Group_11__0 )? ) )
            // InternalNetSpec.g:1126:1: ( ( rule__Attacker__Group_11__0 )? )
            {
            // InternalNetSpec.g:1126:1: ( ( rule__Attacker__Group_11__0 )? )
            // InternalNetSpec.g:1127:2: ( rule__Attacker__Group_11__0 )?
            {
             before(grammarAccess.getAttackerAccess().getGroup_11()); 
            // InternalNetSpec.g:1128:2: ( rule__Attacker__Group_11__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==23) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalNetSpec.g:1128:3: rule__Attacker__Group_11__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Attacker__Group_11__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttackerAccess().getGroup_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__11__Impl"


    // $ANTLR start "rule__Attacker__Group__12"
    // InternalNetSpec.g:1136:1: rule__Attacker__Group__12 : rule__Attacker__Group__12__Impl rule__Attacker__Group__13 ;
    public final void rule__Attacker__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1140:1: ( rule__Attacker__Group__12__Impl rule__Attacker__Group__13 )
            // InternalNetSpec.g:1141:2: rule__Attacker__Group__12__Impl rule__Attacker__Group__13
            {
            pushFollow(FOLLOW_13);
            rule__Attacker__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__12"


    // $ANTLR start "rule__Attacker__Group__12__Impl"
    // InternalNetSpec.g:1148:1: rule__Attacker__Group__12__Impl : ( ( rule__Attacker__Group_12__0 )? ) ;
    public final void rule__Attacker__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1152:1: ( ( ( rule__Attacker__Group_12__0 )? ) )
            // InternalNetSpec.g:1153:1: ( ( rule__Attacker__Group_12__0 )? )
            {
            // InternalNetSpec.g:1153:1: ( ( rule__Attacker__Group_12__0 )? )
            // InternalNetSpec.g:1154:2: ( rule__Attacker__Group_12__0 )?
            {
             before(grammarAccess.getAttackerAccess().getGroup_12()); 
            // InternalNetSpec.g:1155:2: ( rule__Attacker__Group_12__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==24) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalNetSpec.g:1155:3: rule__Attacker__Group_12__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Attacker__Group_12__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttackerAccess().getGroup_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__12__Impl"


    // $ANTLR start "rule__Attacker__Group__13"
    // InternalNetSpec.g:1163:1: rule__Attacker__Group__13 : rule__Attacker__Group__13__Impl rule__Attacker__Group__14 ;
    public final void rule__Attacker__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1167:1: ( rule__Attacker__Group__13__Impl rule__Attacker__Group__14 )
            // InternalNetSpec.g:1168:2: rule__Attacker__Group__13__Impl rule__Attacker__Group__14
            {
            pushFollow(FOLLOW_13);
            rule__Attacker__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__13"


    // $ANTLR start "rule__Attacker__Group__13__Impl"
    // InternalNetSpec.g:1175:1: rule__Attacker__Group__13__Impl : ( ( rule__Attacker__Group_13__0 )? ) ;
    public final void rule__Attacker__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1179:1: ( ( ( rule__Attacker__Group_13__0 )? ) )
            // InternalNetSpec.g:1180:1: ( ( rule__Attacker__Group_13__0 )? )
            {
            // InternalNetSpec.g:1180:1: ( ( rule__Attacker__Group_13__0 )? )
            // InternalNetSpec.g:1181:2: ( rule__Attacker__Group_13__0 )?
            {
             before(grammarAccess.getAttackerAccess().getGroup_13()); 
            // InternalNetSpec.g:1182:2: ( rule__Attacker__Group_13__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==25) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalNetSpec.g:1182:3: rule__Attacker__Group_13__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Attacker__Group_13__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttackerAccess().getGroup_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__13__Impl"


    // $ANTLR start "rule__Attacker__Group__14"
    // InternalNetSpec.g:1190:1: rule__Attacker__Group__14 : rule__Attacker__Group__14__Impl ;
    public final void rule__Attacker__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1194:1: ( rule__Attacker__Group__14__Impl )
            // InternalNetSpec.g:1195:2: rule__Attacker__Group__14__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__Group__14__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__14"


    // $ANTLR start "rule__Attacker__Group__14__Impl"
    // InternalNetSpec.g:1201:1: rule__Attacker__Group__14__Impl : ( '}' ) ;
    public final void rule__Attacker__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1205:1: ( ( '}' ) )
            // InternalNetSpec.g:1206:1: ( '}' )
            {
            // InternalNetSpec.g:1206:1: ( '}' )
            // InternalNetSpec.g:1207:2: '}'
            {
             before(grammarAccess.getAttackerAccess().getRightCurlyBracketKeyword_14()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getRightCurlyBracketKeyword_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group__14__Impl"


    // $ANTLR start "rule__Attacker__Group_11__0"
    // InternalNetSpec.g:1217:1: rule__Attacker__Group_11__0 : rule__Attacker__Group_11__0__Impl rule__Attacker__Group_11__1 ;
    public final void rule__Attacker__Group_11__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1221:1: ( rule__Attacker__Group_11__0__Impl rule__Attacker__Group_11__1 )
            // InternalNetSpec.g:1222:2: rule__Attacker__Group_11__0__Impl rule__Attacker__Group_11__1
            {
            pushFollow(FOLLOW_14);
            rule__Attacker__Group_11__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group_11__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_11__0"


    // $ANTLR start "rule__Attacker__Group_11__0__Impl"
    // InternalNetSpec.g:1229:1: rule__Attacker__Group_11__0__Impl : ( 'LoginsKnown' ) ;
    public final void rule__Attacker__Group_11__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1233:1: ( ( 'LoginsKnown' ) )
            // InternalNetSpec.g:1234:1: ( 'LoginsKnown' )
            {
            // InternalNetSpec.g:1234:1: ( 'LoginsKnown' )
            // InternalNetSpec.g:1235:2: 'LoginsKnown'
            {
             before(grammarAccess.getAttackerAccess().getLoginsKnownKeyword_11_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getLoginsKnownKeyword_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_11__0__Impl"


    // $ANTLR start "rule__Attacker__Group_11__1"
    // InternalNetSpec.g:1244:1: rule__Attacker__Group_11__1 : rule__Attacker__Group_11__1__Impl ;
    public final void rule__Attacker__Group_11__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1248:1: ( rule__Attacker__Group_11__1__Impl )
            // InternalNetSpec.g:1249:2: rule__Attacker__Group_11__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__Group_11__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_11__1"


    // $ANTLR start "rule__Attacker__Group_11__1__Impl"
    // InternalNetSpec.g:1255:1: rule__Attacker__Group_11__1__Impl : ( ( ( rule__Attacker__LoginsKAssignment_11_1 ) ) ( ( rule__Attacker__LoginsKAssignment_11_1 )* ) ) ;
    public final void rule__Attacker__Group_11__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1259:1: ( ( ( ( rule__Attacker__LoginsKAssignment_11_1 ) ) ( ( rule__Attacker__LoginsKAssignment_11_1 )* ) ) )
            // InternalNetSpec.g:1260:1: ( ( ( rule__Attacker__LoginsKAssignment_11_1 ) ) ( ( rule__Attacker__LoginsKAssignment_11_1 )* ) )
            {
            // InternalNetSpec.g:1260:1: ( ( ( rule__Attacker__LoginsKAssignment_11_1 ) ) ( ( rule__Attacker__LoginsKAssignment_11_1 )* ) )
            // InternalNetSpec.g:1261:2: ( ( rule__Attacker__LoginsKAssignment_11_1 ) ) ( ( rule__Attacker__LoginsKAssignment_11_1 )* )
            {
            // InternalNetSpec.g:1261:2: ( ( rule__Attacker__LoginsKAssignment_11_1 ) )
            // InternalNetSpec.g:1262:3: ( rule__Attacker__LoginsKAssignment_11_1 )
            {
             before(grammarAccess.getAttackerAccess().getLoginsKAssignment_11_1()); 
            // InternalNetSpec.g:1263:3: ( rule__Attacker__LoginsKAssignment_11_1 )
            // InternalNetSpec.g:1263:4: rule__Attacker__LoginsKAssignment_11_1
            {
            pushFollow(FOLLOW_15);
            rule__Attacker__LoginsKAssignment_11_1();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getLoginsKAssignment_11_1()); 

            }

            // InternalNetSpec.g:1266:2: ( ( rule__Attacker__LoginsKAssignment_11_1 )* )
            // InternalNetSpec.g:1267:3: ( rule__Attacker__LoginsKAssignment_11_1 )*
            {
             before(grammarAccess.getAttackerAccess().getLoginsKAssignment_11_1()); 
            // InternalNetSpec.g:1268:3: ( rule__Attacker__LoginsKAssignment_11_1 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalNetSpec.g:1268:4: rule__Attacker__LoginsKAssignment_11_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Attacker__LoginsKAssignment_11_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getAttackerAccess().getLoginsKAssignment_11_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_11__1__Impl"


    // $ANTLR start "rule__Attacker__Group_12__0"
    // InternalNetSpec.g:1278:1: rule__Attacker__Group_12__0 : rule__Attacker__Group_12__0__Impl rule__Attacker__Group_12__1 ;
    public final void rule__Attacker__Group_12__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1282:1: ( rule__Attacker__Group_12__0__Impl rule__Attacker__Group_12__1 )
            // InternalNetSpec.g:1283:2: rule__Attacker__Group_12__0__Impl rule__Attacker__Group_12__1
            {
            pushFollow(FOLLOW_14);
            rule__Attacker__Group_12__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group_12__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_12__0"


    // $ANTLR start "rule__Attacker__Group_12__0__Impl"
    // InternalNetSpec.g:1290:1: rule__Attacker__Group_12__0__Impl : ( 'KeysKnown' ) ;
    public final void rule__Attacker__Group_12__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1294:1: ( ( 'KeysKnown' ) )
            // InternalNetSpec.g:1295:1: ( 'KeysKnown' )
            {
            // InternalNetSpec.g:1295:1: ( 'KeysKnown' )
            // InternalNetSpec.g:1296:2: 'KeysKnown'
            {
             before(grammarAccess.getAttackerAccess().getKeysKnownKeyword_12_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getKeysKnownKeyword_12_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_12__0__Impl"


    // $ANTLR start "rule__Attacker__Group_12__1"
    // InternalNetSpec.g:1305:1: rule__Attacker__Group_12__1 : rule__Attacker__Group_12__1__Impl ;
    public final void rule__Attacker__Group_12__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1309:1: ( rule__Attacker__Group_12__1__Impl )
            // InternalNetSpec.g:1310:2: rule__Attacker__Group_12__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__Group_12__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_12__1"


    // $ANTLR start "rule__Attacker__Group_12__1__Impl"
    // InternalNetSpec.g:1316:1: rule__Attacker__Group_12__1__Impl : ( ( ( rule__Attacker__KeysKAssignment_12_1 ) ) ( ( rule__Attacker__KeysKAssignment_12_1 )* ) ) ;
    public final void rule__Attacker__Group_12__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1320:1: ( ( ( ( rule__Attacker__KeysKAssignment_12_1 ) ) ( ( rule__Attacker__KeysKAssignment_12_1 )* ) ) )
            // InternalNetSpec.g:1321:1: ( ( ( rule__Attacker__KeysKAssignment_12_1 ) ) ( ( rule__Attacker__KeysKAssignment_12_1 )* ) )
            {
            // InternalNetSpec.g:1321:1: ( ( ( rule__Attacker__KeysKAssignment_12_1 ) ) ( ( rule__Attacker__KeysKAssignment_12_1 )* ) )
            // InternalNetSpec.g:1322:2: ( ( rule__Attacker__KeysKAssignment_12_1 ) ) ( ( rule__Attacker__KeysKAssignment_12_1 )* )
            {
            // InternalNetSpec.g:1322:2: ( ( rule__Attacker__KeysKAssignment_12_1 ) )
            // InternalNetSpec.g:1323:3: ( rule__Attacker__KeysKAssignment_12_1 )
            {
             before(grammarAccess.getAttackerAccess().getKeysKAssignment_12_1()); 
            // InternalNetSpec.g:1324:3: ( rule__Attacker__KeysKAssignment_12_1 )
            // InternalNetSpec.g:1324:4: rule__Attacker__KeysKAssignment_12_1
            {
            pushFollow(FOLLOW_15);
            rule__Attacker__KeysKAssignment_12_1();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getKeysKAssignment_12_1()); 

            }

            // InternalNetSpec.g:1327:2: ( ( rule__Attacker__KeysKAssignment_12_1 )* )
            // InternalNetSpec.g:1328:3: ( rule__Attacker__KeysKAssignment_12_1 )*
            {
             before(grammarAccess.getAttackerAccess().getKeysKAssignment_12_1()); 
            // InternalNetSpec.g:1329:3: ( rule__Attacker__KeysKAssignment_12_1 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalNetSpec.g:1329:4: rule__Attacker__KeysKAssignment_12_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Attacker__KeysKAssignment_12_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getAttackerAccess().getKeysKAssignment_12_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_12__1__Impl"


    // $ANTLR start "rule__Attacker__Group_13__0"
    // InternalNetSpec.g:1339:1: rule__Attacker__Group_13__0 : rule__Attacker__Group_13__0__Impl rule__Attacker__Group_13__1 ;
    public final void rule__Attacker__Group_13__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1343:1: ( rule__Attacker__Group_13__0__Impl rule__Attacker__Group_13__1 )
            // InternalNetSpec.g:1344:2: rule__Attacker__Group_13__0__Impl rule__Attacker__Group_13__1
            {
            pushFollow(FOLLOW_14);
            rule__Attacker__Group_13__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attacker__Group_13__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_13__0"


    // $ANTLR start "rule__Attacker__Group_13__0__Impl"
    // InternalNetSpec.g:1351:1: rule__Attacker__Group_13__0__Impl : ( 'HostsControlled' ) ;
    public final void rule__Attacker__Group_13__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1355:1: ( ( 'HostsControlled' ) )
            // InternalNetSpec.g:1356:1: ( 'HostsControlled' )
            {
            // InternalNetSpec.g:1356:1: ( 'HostsControlled' )
            // InternalNetSpec.g:1357:2: 'HostsControlled'
            {
             before(grammarAccess.getAttackerAccess().getHostsControlledKeyword_13_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getHostsControlledKeyword_13_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_13__0__Impl"


    // $ANTLR start "rule__Attacker__Group_13__1"
    // InternalNetSpec.g:1366:1: rule__Attacker__Group_13__1 : rule__Attacker__Group_13__1__Impl ;
    public final void rule__Attacker__Group_13__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1370:1: ( rule__Attacker__Group_13__1__Impl )
            // InternalNetSpec.g:1371:2: rule__Attacker__Group_13__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__Group_13__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_13__1"


    // $ANTLR start "rule__Attacker__Group_13__1__Impl"
    // InternalNetSpec.g:1377:1: rule__Attacker__Group_13__1__Impl : ( ( ( rule__Attacker__HostsAssignment_13_1 ) ) ( ( rule__Attacker__HostsAssignment_13_1 )* ) ) ;
    public final void rule__Attacker__Group_13__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1381:1: ( ( ( ( rule__Attacker__HostsAssignment_13_1 ) ) ( ( rule__Attacker__HostsAssignment_13_1 )* ) ) )
            // InternalNetSpec.g:1382:1: ( ( ( rule__Attacker__HostsAssignment_13_1 ) ) ( ( rule__Attacker__HostsAssignment_13_1 )* ) )
            {
            // InternalNetSpec.g:1382:1: ( ( ( rule__Attacker__HostsAssignment_13_1 ) ) ( ( rule__Attacker__HostsAssignment_13_1 )* ) )
            // InternalNetSpec.g:1383:2: ( ( rule__Attacker__HostsAssignment_13_1 ) ) ( ( rule__Attacker__HostsAssignment_13_1 )* )
            {
            // InternalNetSpec.g:1383:2: ( ( rule__Attacker__HostsAssignment_13_1 ) )
            // InternalNetSpec.g:1384:3: ( rule__Attacker__HostsAssignment_13_1 )
            {
             before(grammarAccess.getAttackerAccess().getHostsAssignment_13_1()); 
            // InternalNetSpec.g:1385:3: ( rule__Attacker__HostsAssignment_13_1 )
            // InternalNetSpec.g:1385:4: rule__Attacker__HostsAssignment_13_1
            {
            pushFollow(FOLLOW_15);
            rule__Attacker__HostsAssignment_13_1();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getHostsAssignment_13_1()); 

            }

            // InternalNetSpec.g:1388:2: ( ( rule__Attacker__HostsAssignment_13_1 )* )
            // InternalNetSpec.g:1389:3: ( rule__Attacker__HostsAssignment_13_1 )*
            {
             before(grammarAccess.getAttackerAccess().getHostsAssignment_13_1()); 
            // InternalNetSpec.g:1390:3: ( rule__Attacker__HostsAssignment_13_1 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==RULE_ID) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalNetSpec.g:1390:4: rule__Attacker__HostsAssignment_13_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Attacker__HostsAssignment_13_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getAttackerAccess().getHostsAssignment_13_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__Group_13__1__Impl"


    // $ANTLR start "rule__Goal__Group__0"
    // InternalNetSpec.g:1400:1: rule__Goal__Group__0 : rule__Goal__Group__0__Impl rule__Goal__Group__1 ;
    public final void rule__Goal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1404:1: ( rule__Goal__Group__0__Impl rule__Goal__Group__1 )
            // InternalNetSpec.g:1405:2: rule__Goal__Group__0__Impl rule__Goal__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Goal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__0"


    // $ANTLR start "rule__Goal__Group__0__Impl"
    // InternalNetSpec.g:1412:1: rule__Goal__Group__0__Impl : ( () ) ;
    public final void rule__Goal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1416:1: ( ( () ) )
            // InternalNetSpec.g:1417:1: ( () )
            {
            // InternalNetSpec.g:1417:1: ( () )
            // InternalNetSpec.g:1418:2: ()
            {
             before(grammarAccess.getGoalAccess().getGoalAction_0()); 
            // InternalNetSpec.g:1419:2: ()
            // InternalNetSpec.g:1419:3: 
            {
            }

             after(grammarAccess.getGoalAccess().getGoalAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__0__Impl"


    // $ANTLR start "rule__Goal__Group__1"
    // InternalNetSpec.g:1427:1: rule__Goal__Group__1 : rule__Goal__Group__1__Impl rule__Goal__Group__2 ;
    public final void rule__Goal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1431:1: ( rule__Goal__Group__1__Impl rule__Goal__Group__2 )
            // InternalNetSpec.g:1432:2: rule__Goal__Group__1__Impl rule__Goal__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Goal__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__1"


    // $ANTLR start "rule__Goal__Group__1__Impl"
    // InternalNetSpec.g:1439:1: rule__Goal__Group__1__Impl : ( 'Goal' ) ;
    public final void rule__Goal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1443:1: ( ( 'Goal' ) )
            // InternalNetSpec.g:1444:1: ( 'Goal' )
            {
            // InternalNetSpec.g:1444:1: ( 'Goal' )
            // InternalNetSpec.g:1445:2: 'Goal'
            {
             before(grammarAccess.getGoalAccess().getGoalKeyword_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getGoalKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__1__Impl"


    // $ANTLR start "rule__Goal__Group__2"
    // InternalNetSpec.g:1454:1: rule__Goal__Group__2 : rule__Goal__Group__2__Impl rule__Goal__Group__3 ;
    public final void rule__Goal__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1458:1: ( rule__Goal__Group__2__Impl rule__Goal__Group__3 )
            // InternalNetSpec.g:1459:2: rule__Goal__Group__2__Impl rule__Goal__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__Goal__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__2"


    // $ANTLR start "rule__Goal__Group__2__Impl"
    // InternalNetSpec.g:1466:1: rule__Goal__Group__2__Impl : ( '{' ) ;
    public final void rule__Goal__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1470:1: ( ( '{' ) )
            // InternalNetSpec.g:1471:1: ( '{' )
            {
            // InternalNetSpec.g:1471:1: ( '{' )
            // InternalNetSpec.g:1472:2: '{'
            {
             before(grammarAccess.getGoalAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__2__Impl"


    // $ANTLR start "rule__Goal__Group__3"
    // InternalNetSpec.g:1481:1: rule__Goal__Group__3 : rule__Goal__Group__3__Impl rule__Goal__Group__4 ;
    public final void rule__Goal__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1485:1: ( rule__Goal__Group__3__Impl rule__Goal__Group__4 )
            // InternalNetSpec.g:1486:2: rule__Goal__Group__3__Impl rule__Goal__Group__4
            {
            pushFollow(FOLLOW_16);
            rule__Goal__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__3"


    // $ANTLR start "rule__Goal__Group__3__Impl"
    // InternalNetSpec.g:1493:1: rule__Goal__Group__3__Impl : ( ( rule__Goal__Group_3__0 )? ) ;
    public final void rule__Goal__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1497:1: ( ( ( rule__Goal__Group_3__0 )? ) )
            // InternalNetSpec.g:1498:1: ( ( rule__Goal__Group_3__0 )? )
            {
            // InternalNetSpec.g:1498:1: ( ( rule__Goal__Group_3__0 )? )
            // InternalNetSpec.g:1499:2: ( rule__Goal__Group_3__0 )?
            {
             before(grammarAccess.getGoalAccess().getGroup_3()); 
            // InternalNetSpec.g:1500:2: ( rule__Goal__Group_3__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==27) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalNetSpec.g:1500:3: rule__Goal__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Goal__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGoalAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__3__Impl"


    // $ANTLR start "rule__Goal__Group__4"
    // InternalNetSpec.g:1508:1: rule__Goal__Group__4 : rule__Goal__Group__4__Impl rule__Goal__Group__5 ;
    public final void rule__Goal__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1512:1: ( rule__Goal__Group__4__Impl rule__Goal__Group__5 )
            // InternalNetSpec.g:1513:2: rule__Goal__Group__4__Impl rule__Goal__Group__5
            {
            pushFollow(FOLLOW_16);
            rule__Goal__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__4"


    // $ANTLR start "rule__Goal__Group__4__Impl"
    // InternalNetSpec.g:1520:1: rule__Goal__Group__4__Impl : ( ( rule__Goal__Group_4__0 )? ) ;
    public final void rule__Goal__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1524:1: ( ( ( rule__Goal__Group_4__0 )? ) )
            // InternalNetSpec.g:1525:1: ( ( rule__Goal__Group_4__0 )? )
            {
            // InternalNetSpec.g:1525:1: ( ( rule__Goal__Group_4__0 )? )
            // InternalNetSpec.g:1526:2: ( rule__Goal__Group_4__0 )?
            {
             before(grammarAccess.getGoalAccess().getGroup_4()); 
            // InternalNetSpec.g:1527:2: ( rule__Goal__Group_4__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==28) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalNetSpec.g:1527:3: rule__Goal__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Goal__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGoalAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__4__Impl"


    // $ANTLR start "rule__Goal__Group__5"
    // InternalNetSpec.g:1535:1: rule__Goal__Group__5 : rule__Goal__Group__5__Impl rule__Goal__Group__6 ;
    public final void rule__Goal__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1539:1: ( rule__Goal__Group__5__Impl rule__Goal__Group__6 )
            // InternalNetSpec.g:1540:2: rule__Goal__Group__5__Impl rule__Goal__Group__6
            {
            pushFollow(FOLLOW_16);
            rule__Goal__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__5"


    // $ANTLR start "rule__Goal__Group__5__Impl"
    // InternalNetSpec.g:1547:1: rule__Goal__Group__5__Impl : ( ( rule__Goal__Group_5__0 )? ) ;
    public final void rule__Goal__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1551:1: ( ( ( rule__Goal__Group_5__0 )? ) )
            // InternalNetSpec.g:1552:1: ( ( rule__Goal__Group_5__0 )? )
            {
            // InternalNetSpec.g:1552:1: ( ( rule__Goal__Group_5__0 )? )
            // InternalNetSpec.g:1553:2: ( rule__Goal__Group_5__0 )?
            {
             before(grammarAccess.getGoalAccess().getGroup_5()); 
            // InternalNetSpec.g:1554:2: ( rule__Goal__Group_5__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==29) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalNetSpec.g:1554:3: rule__Goal__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Goal__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGoalAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__5__Impl"


    // $ANTLR start "rule__Goal__Group__6"
    // InternalNetSpec.g:1562:1: rule__Goal__Group__6 : rule__Goal__Group__6__Impl rule__Goal__Group__7 ;
    public final void rule__Goal__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1566:1: ( rule__Goal__Group__6__Impl rule__Goal__Group__7 )
            // InternalNetSpec.g:1567:2: rule__Goal__Group__6__Impl rule__Goal__Group__7
            {
            pushFollow(FOLLOW_16);
            rule__Goal__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__6"


    // $ANTLR start "rule__Goal__Group__6__Impl"
    // InternalNetSpec.g:1574:1: rule__Goal__Group__6__Impl : ( ( rule__Goal__IdsAssignment_6 )? ) ;
    public final void rule__Goal__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1578:1: ( ( ( rule__Goal__IdsAssignment_6 )? ) )
            // InternalNetSpec.g:1579:1: ( ( rule__Goal__IdsAssignment_6 )? )
            {
            // InternalNetSpec.g:1579:1: ( ( rule__Goal__IdsAssignment_6 )? )
            // InternalNetSpec.g:1580:2: ( rule__Goal__IdsAssignment_6 )?
            {
             before(grammarAccess.getGoalAccess().getIdsAssignment_6()); 
            // InternalNetSpec.g:1581:2: ( rule__Goal__IdsAssignment_6 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==47) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalNetSpec.g:1581:3: rule__Goal__IdsAssignment_6
                    {
                    pushFollow(FOLLOW_2);
                    rule__Goal__IdsAssignment_6();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGoalAccess().getIdsAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__6__Impl"


    // $ANTLR start "rule__Goal__Group__7"
    // InternalNetSpec.g:1589:1: rule__Goal__Group__7 : rule__Goal__Group__7__Impl ;
    public final void rule__Goal__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1593:1: ( rule__Goal__Group__7__Impl )
            // InternalNetSpec.g:1594:2: rule__Goal__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Goal__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__7"


    // $ANTLR start "rule__Goal__Group__7__Impl"
    // InternalNetSpec.g:1600:1: rule__Goal__Group__7__Impl : ( '}' ) ;
    public final void rule__Goal__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1604:1: ( ( '}' ) )
            // InternalNetSpec.g:1605:1: ( '}' )
            {
            // InternalNetSpec.g:1605:1: ( '}' )
            // InternalNetSpec.g:1606:2: '}'
            {
             before(grammarAccess.getGoalAccess().getRightCurlyBracketKeyword_7()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__7__Impl"


    // $ANTLR start "rule__Goal__Group_3__0"
    // InternalNetSpec.g:1616:1: rule__Goal__Group_3__0 : rule__Goal__Group_3__0__Impl rule__Goal__Group_3__1 ;
    public final void rule__Goal__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1620:1: ( rule__Goal__Group_3__0__Impl rule__Goal__Group_3__1 )
            // InternalNetSpec.g:1621:2: rule__Goal__Group_3__0__Impl rule__Goal__Group_3__1
            {
            pushFollow(FOLLOW_14);
            rule__Goal__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_3__0"


    // $ANTLR start "rule__Goal__Group_3__0__Impl"
    // InternalNetSpec.g:1628:1: rule__Goal__Group_3__0__Impl : ( 'Datas' ) ;
    public final void rule__Goal__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1632:1: ( ( 'Datas' ) )
            // InternalNetSpec.g:1633:1: ( 'Datas' )
            {
            // InternalNetSpec.g:1633:1: ( 'Datas' )
            // InternalNetSpec.g:1634:2: 'Datas'
            {
             before(grammarAccess.getGoalAccess().getDatasKeyword_3_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getDatasKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_3__0__Impl"


    // $ANTLR start "rule__Goal__Group_3__1"
    // InternalNetSpec.g:1643:1: rule__Goal__Group_3__1 : rule__Goal__Group_3__1__Impl ;
    public final void rule__Goal__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1647:1: ( rule__Goal__Group_3__1__Impl )
            // InternalNetSpec.g:1648:2: rule__Goal__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Goal__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_3__1"


    // $ANTLR start "rule__Goal__Group_3__1__Impl"
    // InternalNetSpec.g:1654:1: rule__Goal__Group_3__1__Impl : ( ( ( rule__Goal__DatasAssignment_3_1 ) ) ( ( rule__Goal__DatasAssignment_3_1 )* ) ) ;
    public final void rule__Goal__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1658:1: ( ( ( ( rule__Goal__DatasAssignment_3_1 ) ) ( ( rule__Goal__DatasAssignment_3_1 )* ) ) )
            // InternalNetSpec.g:1659:1: ( ( ( rule__Goal__DatasAssignment_3_1 ) ) ( ( rule__Goal__DatasAssignment_3_1 )* ) )
            {
            // InternalNetSpec.g:1659:1: ( ( ( rule__Goal__DatasAssignment_3_1 ) ) ( ( rule__Goal__DatasAssignment_3_1 )* ) )
            // InternalNetSpec.g:1660:2: ( ( rule__Goal__DatasAssignment_3_1 ) ) ( ( rule__Goal__DatasAssignment_3_1 )* )
            {
            // InternalNetSpec.g:1660:2: ( ( rule__Goal__DatasAssignment_3_1 ) )
            // InternalNetSpec.g:1661:3: ( rule__Goal__DatasAssignment_3_1 )
            {
             before(grammarAccess.getGoalAccess().getDatasAssignment_3_1()); 
            // InternalNetSpec.g:1662:3: ( rule__Goal__DatasAssignment_3_1 )
            // InternalNetSpec.g:1662:4: rule__Goal__DatasAssignment_3_1
            {
            pushFollow(FOLLOW_15);
            rule__Goal__DatasAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getGoalAccess().getDatasAssignment_3_1()); 

            }

            // InternalNetSpec.g:1665:2: ( ( rule__Goal__DatasAssignment_3_1 )* )
            // InternalNetSpec.g:1666:3: ( rule__Goal__DatasAssignment_3_1 )*
            {
             before(grammarAccess.getGoalAccess().getDatasAssignment_3_1()); 
            // InternalNetSpec.g:1667:3: ( rule__Goal__DatasAssignment_3_1 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==RULE_ID) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalNetSpec.g:1667:4: rule__Goal__DatasAssignment_3_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Goal__DatasAssignment_3_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getGoalAccess().getDatasAssignment_3_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_3__1__Impl"


    // $ANTLR start "rule__Goal__Group_4__0"
    // InternalNetSpec.g:1677:1: rule__Goal__Group_4__0 : rule__Goal__Group_4__0__Impl rule__Goal__Group_4__1 ;
    public final void rule__Goal__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1681:1: ( rule__Goal__Group_4__0__Impl rule__Goal__Group_4__1 )
            // InternalNetSpec.g:1682:2: rule__Goal__Group_4__0__Impl rule__Goal__Group_4__1
            {
            pushFollow(FOLLOW_14);
            rule__Goal__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_4__0"


    // $ANTLR start "rule__Goal__Group_4__0__Impl"
    // InternalNetSpec.g:1689:1: rule__Goal__Group_4__0__Impl : ( 'Files' ) ;
    public final void rule__Goal__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1693:1: ( ( 'Files' ) )
            // InternalNetSpec.g:1694:1: ( 'Files' )
            {
            // InternalNetSpec.g:1694:1: ( 'Files' )
            // InternalNetSpec.g:1695:2: 'Files'
            {
             before(grammarAccess.getGoalAccess().getFilesKeyword_4_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getFilesKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_4__0__Impl"


    // $ANTLR start "rule__Goal__Group_4__1"
    // InternalNetSpec.g:1704:1: rule__Goal__Group_4__1 : rule__Goal__Group_4__1__Impl ;
    public final void rule__Goal__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1708:1: ( rule__Goal__Group_4__1__Impl )
            // InternalNetSpec.g:1709:2: rule__Goal__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Goal__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_4__1"


    // $ANTLR start "rule__Goal__Group_4__1__Impl"
    // InternalNetSpec.g:1715:1: rule__Goal__Group_4__1__Impl : ( ( ( rule__Goal__FilesAssignment_4_1 ) ) ( ( rule__Goal__FilesAssignment_4_1 )* ) ) ;
    public final void rule__Goal__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1719:1: ( ( ( ( rule__Goal__FilesAssignment_4_1 ) ) ( ( rule__Goal__FilesAssignment_4_1 )* ) ) )
            // InternalNetSpec.g:1720:1: ( ( ( rule__Goal__FilesAssignment_4_1 ) ) ( ( rule__Goal__FilesAssignment_4_1 )* ) )
            {
            // InternalNetSpec.g:1720:1: ( ( ( rule__Goal__FilesAssignment_4_1 ) ) ( ( rule__Goal__FilesAssignment_4_1 )* ) )
            // InternalNetSpec.g:1721:2: ( ( rule__Goal__FilesAssignment_4_1 ) ) ( ( rule__Goal__FilesAssignment_4_1 )* )
            {
            // InternalNetSpec.g:1721:2: ( ( rule__Goal__FilesAssignment_4_1 ) )
            // InternalNetSpec.g:1722:3: ( rule__Goal__FilesAssignment_4_1 )
            {
             before(grammarAccess.getGoalAccess().getFilesAssignment_4_1()); 
            // InternalNetSpec.g:1723:3: ( rule__Goal__FilesAssignment_4_1 )
            // InternalNetSpec.g:1723:4: rule__Goal__FilesAssignment_4_1
            {
            pushFollow(FOLLOW_15);
            rule__Goal__FilesAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getGoalAccess().getFilesAssignment_4_1()); 

            }

            // InternalNetSpec.g:1726:2: ( ( rule__Goal__FilesAssignment_4_1 )* )
            // InternalNetSpec.g:1727:3: ( rule__Goal__FilesAssignment_4_1 )*
            {
             before(grammarAccess.getGoalAccess().getFilesAssignment_4_1()); 
            // InternalNetSpec.g:1728:3: ( rule__Goal__FilesAssignment_4_1 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==RULE_ID) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalNetSpec.g:1728:4: rule__Goal__FilesAssignment_4_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Goal__FilesAssignment_4_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getGoalAccess().getFilesAssignment_4_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_4__1__Impl"


    // $ANTLR start "rule__Goal__Group_5__0"
    // InternalNetSpec.g:1738:1: rule__Goal__Group_5__0 : rule__Goal__Group_5__0__Impl rule__Goal__Group_5__1 ;
    public final void rule__Goal__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1742:1: ( rule__Goal__Group_5__0__Impl rule__Goal__Group_5__1 )
            // InternalNetSpec.g:1743:2: rule__Goal__Group_5__0__Impl rule__Goal__Group_5__1
            {
            pushFollow(FOLLOW_14);
            rule__Goal__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_5__0"


    // $ANTLR start "rule__Goal__Group_5__0__Impl"
    // InternalNetSpec.g:1750:1: rule__Goal__Group_5__0__Impl : ( 'Hosts' ) ;
    public final void rule__Goal__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1754:1: ( ( 'Hosts' ) )
            // InternalNetSpec.g:1755:1: ( 'Hosts' )
            {
            // InternalNetSpec.g:1755:1: ( 'Hosts' )
            // InternalNetSpec.g:1756:2: 'Hosts'
            {
             before(grammarAccess.getGoalAccess().getHostsKeyword_5_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getHostsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_5__0__Impl"


    // $ANTLR start "rule__Goal__Group_5__1"
    // InternalNetSpec.g:1765:1: rule__Goal__Group_5__1 : rule__Goal__Group_5__1__Impl ;
    public final void rule__Goal__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1769:1: ( rule__Goal__Group_5__1__Impl )
            // InternalNetSpec.g:1770:2: rule__Goal__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Goal__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_5__1"


    // $ANTLR start "rule__Goal__Group_5__1__Impl"
    // InternalNetSpec.g:1776:1: rule__Goal__Group_5__1__Impl : ( ( ( rule__Goal__HostsAssignment_5_1 ) ) ( ( rule__Goal__HostsAssignment_5_1 )* ) ) ;
    public final void rule__Goal__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1780:1: ( ( ( ( rule__Goal__HostsAssignment_5_1 ) ) ( ( rule__Goal__HostsAssignment_5_1 )* ) ) )
            // InternalNetSpec.g:1781:1: ( ( ( rule__Goal__HostsAssignment_5_1 ) ) ( ( rule__Goal__HostsAssignment_5_1 )* ) )
            {
            // InternalNetSpec.g:1781:1: ( ( ( rule__Goal__HostsAssignment_5_1 ) ) ( ( rule__Goal__HostsAssignment_5_1 )* ) )
            // InternalNetSpec.g:1782:2: ( ( rule__Goal__HostsAssignment_5_1 ) ) ( ( rule__Goal__HostsAssignment_5_1 )* )
            {
            // InternalNetSpec.g:1782:2: ( ( rule__Goal__HostsAssignment_5_1 ) )
            // InternalNetSpec.g:1783:3: ( rule__Goal__HostsAssignment_5_1 )
            {
             before(grammarAccess.getGoalAccess().getHostsAssignment_5_1()); 
            // InternalNetSpec.g:1784:3: ( rule__Goal__HostsAssignment_5_1 )
            // InternalNetSpec.g:1784:4: rule__Goal__HostsAssignment_5_1
            {
            pushFollow(FOLLOW_15);
            rule__Goal__HostsAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getGoalAccess().getHostsAssignment_5_1()); 

            }

            // InternalNetSpec.g:1787:2: ( ( rule__Goal__HostsAssignment_5_1 )* )
            // InternalNetSpec.g:1788:3: ( rule__Goal__HostsAssignment_5_1 )*
            {
             before(grammarAccess.getGoalAccess().getHostsAssignment_5_1()); 
            // InternalNetSpec.g:1789:3: ( rule__Goal__HostsAssignment_5_1 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==RULE_ID) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalNetSpec.g:1789:4: rule__Goal__HostsAssignment_5_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Goal__HostsAssignment_5_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getGoalAccess().getHostsAssignment_5_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group_5__1__Impl"


    // $ANTLR start "rule__Host__Group__0"
    // InternalNetSpec.g:1799:1: rule__Host__Group__0 : rule__Host__Group__0__Impl rule__Host__Group__1 ;
    public final void rule__Host__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1803:1: ( rule__Host__Group__0__Impl rule__Host__Group__1 )
            // InternalNetSpec.g:1804:2: rule__Host__Group__0__Impl rule__Host__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__Host__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__0"


    // $ANTLR start "rule__Host__Group__0__Impl"
    // InternalNetSpec.g:1811:1: rule__Host__Group__0__Impl : ( () ) ;
    public final void rule__Host__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1815:1: ( ( () ) )
            // InternalNetSpec.g:1816:1: ( () )
            {
            // InternalNetSpec.g:1816:1: ( () )
            // InternalNetSpec.g:1817:2: ()
            {
             before(grammarAccess.getHostAccess().getHostAction_0()); 
            // InternalNetSpec.g:1818:2: ()
            // InternalNetSpec.g:1818:3: 
            {
            }

             after(grammarAccess.getHostAccess().getHostAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__0__Impl"


    // $ANTLR start "rule__Host__Group__1"
    // InternalNetSpec.g:1826:1: rule__Host__Group__1 : rule__Host__Group__1__Impl rule__Host__Group__2 ;
    public final void rule__Host__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1830:1: ( rule__Host__Group__1__Impl rule__Host__Group__2 )
            // InternalNetSpec.g:1831:2: rule__Host__Group__1__Impl rule__Host__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Host__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__1"


    // $ANTLR start "rule__Host__Group__1__Impl"
    // InternalNetSpec.g:1838:1: rule__Host__Group__1__Impl : ( 'Host' ) ;
    public final void rule__Host__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1842:1: ( ( 'Host' ) )
            // InternalNetSpec.g:1843:1: ( 'Host' )
            {
            // InternalNetSpec.g:1843:1: ( 'Host' )
            // InternalNetSpec.g:1844:2: 'Host'
            {
             before(grammarAccess.getHostAccess().getHostKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getHostKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__1__Impl"


    // $ANTLR start "rule__Host__Group__2"
    // InternalNetSpec.g:1853:1: rule__Host__Group__2 : rule__Host__Group__2__Impl rule__Host__Group__3 ;
    public final void rule__Host__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1857:1: ( rule__Host__Group__2__Impl rule__Host__Group__3 )
            // InternalNetSpec.g:1858:2: rule__Host__Group__2__Impl rule__Host__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Host__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__2"


    // $ANTLR start "rule__Host__Group__2__Impl"
    // InternalNetSpec.g:1865:1: rule__Host__Group__2__Impl : ( ( rule__Host__NameAssignment_2 ) ) ;
    public final void rule__Host__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1869:1: ( ( ( rule__Host__NameAssignment_2 ) ) )
            // InternalNetSpec.g:1870:1: ( ( rule__Host__NameAssignment_2 ) )
            {
            // InternalNetSpec.g:1870:1: ( ( rule__Host__NameAssignment_2 ) )
            // InternalNetSpec.g:1871:2: ( rule__Host__NameAssignment_2 )
            {
             before(grammarAccess.getHostAccess().getNameAssignment_2()); 
            // InternalNetSpec.g:1872:2: ( rule__Host__NameAssignment_2 )
            // InternalNetSpec.g:1872:3: rule__Host__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Host__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getHostAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__2__Impl"


    // $ANTLR start "rule__Host__Group__3"
    // InternalNetSpec.g:1880:1: rule__Host__Group__3 : rule__Host__Group__3__Impl rule__Host__Group__4 ;
    public final void rule__Host__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1884:1: ( rule__Host__Group__3__Impl rule__Host__Group__4 )
            // InternalNetSpec.g:1885:2: rule__Host__Group__3__Impl rule__Host__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__Host__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__3"


    // $ANTLR start "rule__Host__Group__3__Impl"
    // InternalNetSpec.g:1892:1: rule__Host__Group__3__Impl : ( '{' ) ;
    public final void rule__Host__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1896:1: ( ( '{' ) )
            // InternalNetSpec.g:1897:1: ( '{' )
            {
            // InternalNetSpec.g:1897:1: ( '{' )
            // InternalNetSpec.g:1898:2: '{'
            {
             before(grammarAccess.getHostAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__3__Impl"


    // $ANTLR start "rule__Host__Group__4"
    // InternalNetSpec.g:1907:1: rule__Host__Group__4 : rule__Host__Group__4__Impl rule__Host__Group__5 ;
    public final void rule__Host__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1911:1: ( rule__Host__Group__4__Impl rule__Host__Group__5 )
            // InternalNetSpec.g:1912:2: rule__Host__Group__4__Impl rule__Host__Group__5
            {
            pushFollow(FOLLOW_18);
            rule__Host__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__4"


    // $ANTLR start "rule__Host__Group__4__Impl"
    // InternalNetSpec.g:1919:1: rule__Host__Group__4__Impl : ( ( rule__Host__Group_4__0 )? ) ;
    public final void rule__Host__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1923:1: ( ( ( rule__Host__Group_4__0 )? ) )
            // InternalNetSpec.g:1924:1: ( ( rule__Host__Group_4__0 )? )
            {
            // InternalNetSpec.g:1924:1: ( ( rule__Host__Group_4__0 )? )
            // InternalNetSpec.g:1925:2: ( rule__Host__Group_4__0 )?
            {
             before(grammarAccess.getHostAccess().getGroup_4()); 
            // InternalNetSpec.g:1926:2: ( rule__Host__Group_4__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==31) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalNetSpec.g:1926:3: rule__Host__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Host__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getHostAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__4__Impl"


    // $ANTLR start "rule__Host__Group__5"
    // InternalNetSpec.g:1934:1: rule__Host__Group__5 : rule__Host__Group__5__Impl rule__Host__Group__6 ;
    public final void rule__Host__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1938:1: ( rule__Host__Group__5__Impl rule__Host__Group__6 )
            // InternalNetSpec.g:1939:2: rule__Host__Group__5__Impl rule__Host__Group__6
            {
            pushFollow(FOLLOW_18);
            rule__Host__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__5"


    // $ANTLR start "rule__Host__Group__5__Impl"
    // InternalNetSpec.g:1946:1: rule__Host__Group__5__Impl : ( ( rule__Host__Group_5__0 )? ) ;
    public final void rule__Host__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1950:1: ( ( ( rule__Host__Group_5__0 )? ) )
            // InternalNetSpec.g:1951:1: ( ( rule__Host__Group_5__0 )? )
            {
            // InternalNetSpec.g:1951:1: ( ( rule__Host__Group_5__0 )? )
            // InternalNetSpec.g:1952:2: ( rule__Host__Group_5__0 )?
            {
             before(grammarAccess.getHostAccess().getGroup_5()); 
            // InternalNetSpec.g:1953:2: ( rule__Host__Group_5__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==28) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalNetSpec.g:1953:3: rule__Host__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Host__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getHostAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__5__Impl"


    // $ANTLR start "rule__Host__Group__6"
    // InternalNetSpec.g:1961:1: rule__Host__Group__6 : rule__Host__Group__6__Impl rule__Host__Group__7 ;
    public final void rule__Host__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1965:1: ( rule__Host__Group__6__Impl rule__Host__Group__7 )
            // InternalNetSpec.g:1966:2: rule__Host__Group__6__Impl rule__Host__Group__7
            {
            pushFollow(FOLLOW_18);
            rule__Host__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__6"


    // $ANTLR start "rule__Host__Group__6__Impl"
    // InternalNetSpec.g:1973:1: rule__Host__Group__6__Impl : ( ( rule__Host__Group_6__0 )? ) ;
    public final void rule__Host__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1977:1: ( ( ( rule__Host__Group_6__0 )? ) )
            // InternalNetSpec.g:1978:1: ( ( rule__Host__Group_6__0 )? )
            {
            // InternalNetSpec.g:1978:1: ( ( rule__Host__Group_6__0 )? )
            // InternalNetSpec.g:1979:2: ( rule__Host__Group_6__0 )?
            {
             before(grammarAccess.getHostAccess().getGroup_6()); 
            // InternalNetSpec.g:1980:2: ( rule__Host__Group_6__0 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==32) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalNetSpec.g:1980:3: rule__Host__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Host__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getHostAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__6__Impl"


    // $ANTLR start "rule__Host__Group__7"
    // InternalNetSpec.g:1988:1: rule__Host__Group__7 : rule__Host__Group__7__Impl rule__Host__Group__8 ;
    public final void rule__Host__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:1992:1: ( rule__Host__Group__7__Impl rule__Host__Group__8 )
            // InternalNetSpec.g:1993:2: rule__Host__Group__7__Impl rule__Host__Group__8
            {
            pushFollow(FOLLOW_18);
            rule__Host__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__7"


    // $ANTLR start "rule__Host__Group__7__Impl"
    // InternalNetSpec.g:2000:1: rule__Host__Group__7__Impl : ( ( rule__Host__Group_7__0 )? ) ;
    public final void rule__Host__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2004:1: ( ( ( rule__Host__Group_7__0 )? ) )
            // InternalNetSpec.g:2005:1: ( ( rule__Host__Group_7__0 )? )
            {
            // InternalNetSpec.g:2005:1: ( ( rule__Host__Group_7__0 )? )
            // InternalNetSpec.g:2006:2: ( rule__Host__Group_7__0 )?
            {
             before(grammarAccess.getHostAccess().getGroup_7()); 
            // InternalNetSpec.g:2007:2: ( rule__Host__Group_7__0 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==33) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalNetSpec.g:2007:3: rule__Host__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Host__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getHostAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__7__Impl"


    // $ANTLR start "rule__Host__Group__8"
    // InternalNetSpec.g:2015:1: rule__Host__Group__8 : rule__Host__Group__8__Impl rule__Host__Group__9 ;
    public final void rule__Host__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2019:1: ( rule__Host__Group__8__Impl rule__Host__Group__9 )
            // InternalNetSpec.g:2020:2: rule__Host__Group__8__Impl rule__Host__Group__9
            {
            pushFollow(FOLLOW_18);
            rule__Host__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__8"


    // $ANTLR start "rule__Host__Group__8__Impl"
    // InternalNetSpec.g:2027:1: rule__Host__Group__8__Impl : ( ( rule__Host__Group_8__0 )? ) ;
    public final void rule__Host__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2031:1: ( ( ( rule__Host__Group_8__0 )? ) )
            // InternalNetSpec.g:2032:1: ( ( rule__Host__Group_8__0 )? )
            {
            // InternalNetSpec.g:2032:1: ( ( rule__Host__Group_8__0 )? )
            // InternalNetSpec.g:2033:2: ( rule__Host__Group_8__0 )?
            {
             before(grammarAccess.getHostAccess().getGroup_8()); 
            // InternalNetSpec.g:2034:2: ( rule__Host__Group_8__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==34) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalNetSpec.g:2034:3: rule__Host__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Host__Group_8__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getHostAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__8__Impl"


    // $ANTLR start "rule__Host__Group__9"
    // InternalNetSpec.g:2042:1: rule__Host__Group__9 : rule__Host__Group__9__Impl rule__Host__Group__10 ;
    public final void rule__Host__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2046:1: ( rule__Host__Group__9__Impl rule__Host__Group__10 )
            // InternalNetSpec.g:2047:2: rule__Host__Group__9__Impl rule__Host__Group__10
            {
            pushFollow(FOLLOW_18);
            rule__Host__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__9"


    // $ANTLR start "rule__Host__Group__9__Impl"
    // InternalNetSpec.g:2054:1: rule__Host__Group__9__Impl : ( ( rule__Host__Group_9__0 )? ) ;
    public final void rule__Host__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2058:1: ( ( ( rule__Host__Group_9__0 )? ) )
            // InternalNetSpec.g:2059:1: ( ( rule__Host__Group_9__0 )? )
            {
            // InternalNetSpec.g:2059:1: ( ( rule__Host__Group_9__0 )? )
            // InternalNetSpec.g:2060:2: ( rule__Host__Group_9__0 )?
            {
             before(grammarAccess.getHostAccess().getGroup_9()); 
            // InternalNetSpec.g:2061:2: ( rule__Host__Group_9__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==35) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalNetSpec.g:2061:3: rule__Host__Group_9__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Host__Group_9__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getHostAccess().getGroup_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__9__Impl"


    // $ANTLR start "rule__Host__Group__10"
    // InternalNetSpec.g:2069:1: rule__Host__Group__10 : rule__Host__Group__10__Impl ;
    public final void rule__Host__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2073:1: ( rule__Host__Group__10__Impl )
            // InternalNetSpec.g:2074:2: rule__Host__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Host__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__10"


    // $ANTLR start "rule__Host__Group__10__Impl"
    // InternalNetSpec.g:2080:1: rule__Host__Group__10__Impl : ( '}' ) ;
    public final void rule__Host__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2084:1: ( ( '}' ) )
            // InternalNetSpec.g:2085:1: ( '}' )
            {
            // InternalNetSpec.g:2085:1: ( '}' )
            // InternalNetSpec.g:2086:2: '}'
            {
             before(grammarAccess.getHostAccess().getRightCurlyBracketKeyword_10()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group__10__Impl"


    // $ANTLR start "rule__Host__Group_4__0"
    // InternalNetSpec.g:2096:1: rule__Host__Group_4__0 : rule__Host__Group_4__0__Impl rule__Host__Group_4__1 ;
    public final void rule__Host__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2100:1: ( rule__Host__Group_4__0__Impl rule__Host__Group_4__1 )
            // InternalNetSpec.g:2101:2: rule__Host__Group_4__0__Impl rule__Host__Group_4__1
            {
            pushFollow(FOLLOW_14);
            rule__Host__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_4__0"


    // $ANTLR start "rule__Host__Group_4__0__Impl"
    // InternalNetSpec.g:2108:1: rule__Host__Group_4__0__Impl : ( 'Sessions' ) ;
    public final void rule__Host__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2112:1: ( ( 'Sessions' ) )
            // InternalNetSpec.g:2113:1: ( 'Sessions' )
            {
            // InternalNetSpec.g:2113:1: ( 'Sessions' )
            // InternalNetSpec.g:2114:2: 'Sessions'
            {
             before(grammarAccess.getHostAccess().getSessionsKeyword_4_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getSessionsKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_4__0__Impl"


    // $ANTLR start "rule__Host__Group_4__1"
    // InternalNetSpec.g:2123:1: rule__Host__Group_4__1 : rule__Host__Group_4__1__Impl ;
    public final void rule__Host__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2127:1: ( rule__Host__Group_4__1__Impl )
            // InternalNetSpec.g:2128:2: rule__Host__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Host__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_4__1"


    // $ANTLR start "rule__Host__Group_4__1__Impl"
    // InternalNetSpec.g:2134:1: rule__Host__Group_4__1__Impl : ( ( ( rule__Host__SessionsAssignment_4_1 ) ) ( ( rule__Host__SessionsAssignment_4_1 )* ) ) ;
    public final void rule__Host__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2138:1: ( ( ( ( rule__Host__SessionsAssignment_4_1 ) ) ( ( rule__Host__SessionsAssignment_4_1 )* ) ) )
            // InternalNetSpec.g:2139:1: ( ( ( rule__Host__SessionsAssignment_4_1 ) ) ( ( rule__Host__SessionsAssignment_4_1 )* ) )
            {
            // InternalNetSpec.g:2139:1: ( ( ( rule__Host__SessionsAssignment_4_1 ) ) ( ( rule__Host__SessionsAssignment_4_1 )* ) )
            // InternalNetSpec.g:2140:2: ( ( rule__Host__SessionsAssignment_4_1 ) ) ( ( rule__Host__SessionsAssignment_4_1 )* )
            {
            // InternalNetSpec.g:2140:2: ( ( rule__Host__SessionsAssignment_4_1 ) )
            // InternalNetSpec.g:2141:3: ( rule__Host__SessionsAssignment_4_1 )
            {
             before(grammarAccess.getHostAccess().getSessionsAssignment_4_1()); 
            // InternalNetSpec.g:2142:3: ( rule__Host__SessionsAssignment_4_1 )
            // InternalNetSpec.g:2142:4: rule__Host__SessionsAssignment_4_1
            {
            pushFollow(FOLLOW_15);
            rule__Host__SessionsAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getHostAccess().getSessionsAssignment_4_1()); 

            }

            // InternalNetSpec.g:2145:2: ( ( rule__Host__SessionsAssignment_4_1 )* )
            // InternalNetSpec.g:2146:3: ( rule__Host__SessionsAssignment_4_1 )*
            {
             before(grammarAccess.getHostAccess().getSessionsAssignment_4_1()); 
            // InternalNetSpec.g:2147:3: ( rule__Host__SessionsAssignment_4_1 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==RULE_ID) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalNetSpec.g:2147:4: rule__Host__SessionsAssignment_4_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Host__SessionsAssignment_4_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getHostAccess().getSessionsAssignment_4_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_4__1__Impl"


    // $ANTLR start "rule__Host__Group_5__0"
    // InternalNetSpec.g:2157:1: rule__Host__Group_5__0 : rule__Host__Group_5__0__Impl rule__Host__Group_5__1 ;
    public final void rule__Host__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2161:1: ( rule__Host__Group_5__0__Impl rule__Host__Group_5__1 )
            // InternalNetSpec.g:2162:2: rule__Host__Group_5__0__Impl rule__Host__Group_5__1
            {
            pushFollow(FOLLOW_14);
            rule__Host__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_5__0"


    // $ANTLR start "rule__Host__Group_5__0__Impl"
    // InternalNetSpec.g:2169:1: rule__Host__Group_5__0__Impl : ( 'Files' ) ;
    public final void rule__Host__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2173:1: ( ( 'Files' ) )
            // InternalNetSpec.g:2174:1: ( 'Files' )
            {
            // InternalNetSpec.g:2174:1: ( 'Files' )
            // InternalNetSpec.g:2175:2: 'Files'
            {
             before(grammarAccess.getHostAccess().getFilesKeyword_5_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getFilesKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_5__0__Impl"


    // $ANTLR start "rule__Host__Group_5__1"
    // InternalNetSpec.g:2184:1: rule__Host__Group_5__1 : rule__Host__Group_5__1__Impl ;
    public final void rule__Host__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2188:1: ( rule__Host__Group_5__1__Impl )
            // InternalNetSpec.g:2189:2: rule__Host__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Host__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_5__1"


    // $ANTLR start "rule__Host__Group_5__1__Impl"
    // InternalNetSpec.g:2195:1: rule__Host__Group_5__1__Impl : ( ( ( rule__Host__FilesAssignment_5_1 ) ) ( ( rule__Host__FilesAssignment_5_1 )* ) ) ;
    public final void rule__Host__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2199:1: ( ( ( ( rule__Host__FilesAssignment_5_1 ) ) ( ( rule__Host__FilesAssignment_5_1 )* ) ) )
            // InternalNetSpec.g:2200:1: ( ( ( rule__Host__FilesAssignment_5_1 ) ) ( ( rule__Host__FilesAssignment_5_1 )* ) )
            {
            // InternalNetSpec.g:2200:1: ( ( ( rule__Host__FilesAssignment_5_1 ) ) ( ( rule__Host__FilesAssignment_5_1 )* ) )
            // InternalNetSpec.g:2201:2: ( ( rule__Host__FilesAssignment_5_1 ) ) ( ( rule__Host__FilesAssignment_5_1 )* )
            {
            // InternalNetSpec.g:2201:2: ( ( rule__Host__FilesAssignment_5_1 ) )
            // InternalNetSpec.g:2202:3: ( rule__Host__FilesAssignment_5_1 )
            {
             before(grammarAccess.getHostAccess().getFilesAssignment_5_1()); 
            // InternalNetSpec.g:2203:3: ( rule__Host__FilesAssignment_5_1 )
            // InternalNetSpec.g:2203:4: rule__Host__FilesAssignment_5_1
            {
            pushFollow(FOLLOW_15);
            rule__Host__FilesAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getHostAccess().getFilesAssignment_5_1()); 

            }

            // InternalNetSpec.g:2206:2: ( ( rule__Host__FilesAssignment_5_1 )* )
            // InternalNetSpec.g:2207:3: ( rule__Host__FilesAssignment_5_1 )*
            {
             before(grammarAccess.getHostAccess().getFilesAssignment_5_1()); 
            // InternalNetSpec.g:2208:3: ( rule__Host__FilesAssignment_5_1 )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==RULE_ID) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalNetSpec.g:2208:4: rule__Host__FilesAssignment_5_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Host__FilesAssignment_5_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

             after(grammarAccess.getHostAccess().getFilesAssignment_5_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_5__1__Impl"


    // $ANTLR start "rule__Host__Group_6__0"
    // InternalNetSpec.g:2218:1: rule__Host__Group_6__0 : rule__Host__Group_6__0__Impl rule__Host__Group_6__1 ;
    public final void rule__Host__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2222:1: ( rule__Host__Group_6__0__Impl rule__Host__Group_6__1 )
            // InternalNetSpec.g:2223:2: rule__Host__Group_6__0__Impl rule__Host__Group_6__1
            {
            pushFollow(FOLLOW_14);
            rule__Host__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_6__0"


    // $ANTLR start "rule__Host__Group_6__0__Impl"
    // InternalNetSpec.g:2230:1: rule__Host__Group_6__0__Impl : ( 'Root' ) ;
    public final void rule__Host__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2234:1: ( ( 'Root' ) )
            // InternalNetSpec.g:2235:1: ( 'Root' )
            {
            // InternalNetSpec.g:2235:1: ( 'Root' )
            // InternalNetSpec.g:2236:2: 'Root'
            {
             before(grammarAccess.getHostAccess().getRootKeyword_6_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getRootKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_6__0__Impl"


    // $ANTLR start "rule__Host__Group_6__1"
    // InternalNetSpec.g:2245:1: rule__Host__Group_6__1 : rule__Host__Group_6__1__Impl ;
    public final void rule__Host__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2249:1: ( rule__Host__Group_6__1__Impl )
            // InternalNetSpec.g:2250:2: rule__Host__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Host__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_6__1"


    // $ANTLR start "rule__Host__Group_6__1__Impl"
    // InternalNetSpec.g:2256:1: rule__Host__Group_6__1__Impl : ( ( rule__Host__RootPwdAssignment_6_1 ) ) ;
    public final void rule__Host__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2260:1: ( ( ( rule__Host__RootPwdAssignment_6_1 ) ) )
            // InternalNetSpec.g:2261:1: ( ( rule__Host__RootPwdAssignment_6_1 ) )
            {
            // InternalNetSpec.g:2261:1: ( ( rule__Host__RootPwdAssignment_6_1 ) )
            // InternalNetSpec.g:2262:2: ( rule__Host__RootPwdAssignment_6_1 )
            {
             before(grammarAccess.getHostAccess().getRootPwdAssignment_6_1()); 
            // InternalNetSpec.g:2263:2: ( rule__Host__RootPwdAssignment_6_1 )
            // InternalNetSpec.g:2263:3: rule__Host__RootPwdAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Host__RootPwdAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getHostAccess().getRootPwdAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_6__1__Impl"


    // $ANTLR start "rule__Host__Group_7__0"
    // InternalNetSpec.g:2272:1: rule__Host__Group_7__0 : rule__Host__Group_7__0__Impl rule__Host__Group_7__1 ;
    public final void rule__Host__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2276:1: ( rule__Host__Group_7__0__Impl rule__Host__Group_7__1 )
            // InternalNetSpec.g:2277:2: rule__Host__Group_7__0__Impl rule__Host__Group_7__1
            {
            pushFollow(FOLLOW_14);
            rule__Host__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_7__0"


    // $ANTLR start "rule__Host__Group_7__0__Impl"
    // InternalNetSpec.g:2284:1: rule__Host__Group_7__0__Impl : ( 'IDS' ) ;
    public final void rule__Host__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2288:1: ( ( 'IDS' ) )
            // InternalNetSpec.g:2289:1: ( 'IDS' )
            {
            // InternalNetSpec.g:2289:1: ( 'IDS' )
            // InternalNetSpec.g:2290:2: 'IDS'
            {
             before(grammarAccess.getHostAccess().getIDSKeyword_7_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getIDSKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_7__0__Impl"


    // $ANTLR start "rule__Host__Group_7__1"
    // InternalNetSpec.g:2299:1: rule__Host__Group_7__1 : rule__Host__Group_7__1__Impl ;
    public final void rule__Host__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2303:1: ( rule__Host__Group_7__1__Impl )
            // InternalNetSpec.g:2304:2: rule__Host__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Host__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_7__1"


    // $ANTLR start "rule__Host__Group_7__1__Impl"
    // InternalNetSpec.g:2310:1: rule__Host__Group_7__1__Impl : ( ( rule__Host__IdsAssignment_7_1 ) ) ;
    public final void rule__Host__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2314:1: ( ( ( rule__Host__IdsAssignment_7_1 ) ) )
            // InternalNetSpec.g:2315:1: ( ( rule__Host__IdsAssignment_7_1 ) )
            {
            // InternalNetSpec.g:2315:1: ( ( rule__Host__IdsAssignment_7_1 ) )
            // InternalNetSpec.g:2316:2: ( rule__Host__IdsAssignment_7_1 )
            {
             before(grammarAccess.getHostAccess().getIdsAssignment_7_1()); 
            // InternalNetSpec.g:2317:2: ( rule__Host__IdsAssignment_7_1 )
            // InternalNetSpec.g:2317:3: rule__Host__IdsAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Host__IdsAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getHostAccess().getIdsAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_7__1__Impl"


    // $ANTLR start "rule__Host__Group_8__0"
    // InternalNetSpec.g:2326:1: rule__Host__Group_8__0 : rule__Host__Group_8__0__Impl rule__Host__Group_8__1 ;
    public final void rule__Host__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2330:1: ( rule__Host__Group_8__0__Impl rule__Host__Group_8__1 )
            // InternalNetSpec.g:2331:2: rule__Host__Group_8__0__Impl rule__Host__Group_8__1
            {
            pushFollow(FOLLOW_14);
            rule__Host__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_8__0"


    // $ANTLR start "rule__Host__Group_8__0__Impl"
    // InternalNetSpec.g:2338:1: rule__Host__Group_8__0__Impl : ( 'Firewall' ) ;
    public final void rule__Host__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2342:1: ( ( 'Firewall' ) )
            // InternalNetSpec.g:2343:1: ( 'Firewall' )
            {
            // InternalNetSpec.g:2343:1: ( 'Firewall' )
            // InternalNetSpec.g:2344:2: 'Firewall'
            {
             before(grammarAccess.getHostAccess().getFirewallKeyword_8_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getFirewallKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_8__0__Impl"


    // $ANTLR start "rule__Host__Group_8__1"
    // InternalNetSpec.g:2353:1: rule__Host__Group_8__1 : rule__Host__Group_8__1__Impl ;
    public final void rule__Host__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2357:1: ( rule__Host__Group_8__1__Impl )
            // InternalNetSpec.g:2358:2: rule__Host__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Host__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_8__1"


    // $ANTLR start "rule__Host__Group_8__1__Impl"
    // InternalNetSpec.g:2364:1: rule__Host__Group_8__1__Impl : ( ( rule__Host__FirewallAssignment_8_1 ) ) ;
    public final void rule__Host__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2368:1: ( ( ( rule__Host__FirewallAssignment_8_1 ) ) )
            // InternalNetSpec.g:2369:1: ( ( rule__Host__FirewallAssignment_8_1 ) )
            {
            // InternalNetSpec.g:2369:1: ( ( rule__Host__FirewallAssignment_8_1 ) )
            // InternalNetSpec.g:2370:2: ( rule__Host__FirewallAssignment_8_1 )
            {
             before(grammarAccess.getHostAccess().getFirewallAssignment_8_1()); 
            // InternalNetSpec.g:2371:2: ( rule__Host__FirewallAssignment_8_1 )
            // InternalNetSpec.g:2371:3: rule__Host__FirewallAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__Host__FirewallAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getHostAccess().getFirewallAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_8__1__Impl"


    // $ANTLR start "rule__Host__Group_9__0"
    // InternalNetSpec.g:2380:1: rule__Host__Group_9__0 : rule__Host__Group_9__0__Impl rule__Host__Group_9__1 ;
    public final void rule__Host__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2384:1: ( rule__Host__Group_9__0__Impl rule__Host__Group_9__1 )
            // InternalNetSpec.g:2385:2: rule__Host__Group_9__0__Impl rule__Host__Group_9__1
            {
            pushFollow(FOLLOW_14);
            rule__Host__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Host__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_9__0"


    // $ANTLR start "rule__Host__Group_9__0__Impl"
    // InternalNetSpec.g:2392:1: rule__Host__Group_9__0__Impl : ( 'Antivirus' ) ;
    public final void rule__Host__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2396:1: ( ( 'Antivirus' ) )
            // InternalNetSpec.g:2397:1: ( 'Antivirus' )
            {
            // InternalNetSpec.g:2397:1: ( 'Antivirus' )
            // InternalNetSpec.g:2398:2: 'Antivirus'
            {
             before(grammarAccess.getHostAccess().getAntivirusKeyword_9_0()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getAntivirusKeyword_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_9__0__Impl"


    // $ANTLR start "rule__Host__Group_9__1"
    // InternalNetSpec.g:2407:1: rule__Host__Group_9__1 : rule__Host__Group_9__1__Impl ;
    public final void rule__Host__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2411:1: ( rule__Host__Group_9__1__Impl )
            // InternalNetSpec.g:2412:2: rule__Host__Group_9__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Host__Group_9__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_9__1"


    // $ANTLR start "rule__Host__Group_9__1__Impl"
    // InternalNetSpec.g:2418:1: rule__Host__Group_9__1__Impl : ( ( rule__Host__AntivirusAssignment_9_1 ) ) ;
    public final void rule__Host__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2422:1: ( ( ( rule__Host__AntivirusAssignment_9_1 ) ) )
            // InternalNetSpec.g:2423:1: ( ( rule__Host__AntivirusAssignment_9_1 ) )
            {
            // InternalNetSpec.g:2423:1: ( ( rule__Host__AntivirusAssignment_9_1 ) )
            // InternalNetSpec.g:2424:2: ( rule__Host__AntivirusAssignment_9_1 )
            {
             before(grammarAccess.getHostAccess().getAntivirusAssignment_9_1()); 
            // InternalNetSpec.g:2425:2: ( rule__Host__AntivirusAssignment_9_1 )
            // InternalNetSpec.g:2425:3: rule__Host__AntivirusAssignment_9_1
            {
            pushFollow(FOLLOW_2);
            rule__Host__AntivirusAssignment_9_1();

            state._fsp--;


            }

             after(grammarAccess.getHostAccess().getAntivirusAssignment_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__Group_9__1__Impl"


    // $ANTLR start "rule__Session__Group__0"
    // InternalNetSpec.g:2434:1: rule__Session__Group__0 : rule__Session__Group__0__Impl rule__Session__Group__1 ;
    public final void rule__Session__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2438:1: ( rule__Session__Group__0__Impl rule__Session__Group__1 )
            // InternalNetSpec.g:2439:2: rule__Session__Group__0__Impl rule__Session__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__Session__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Session__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__0"


    // $ANTLR start "rule__Session__Group__0__Impl"
    // InternalNetSpec.g:2446:1: rule__Session__Group__0__Impl : ( () ) ;
    public final void rule__Session__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2450:1: ( ( () ) )
            // InternalNetSpec.g:2451:1: ( () )
            {
            // InternalNetSpec.g:2451:1: ( () )
            // InternalNetSpec.g:2452:2: ()
            {
             before(grammarAccess.getSessionAccess().getSessionAction_0()); 
            // InternalNetSpec.g:2453:2: ()
            // InternalNetSpec.g:2453:3: 
            {
            }

             after(grammarAccess.getSessionAccess().getSessionAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__0__Impl"


    // $ANTLR start "rule__Session__Group__1"
    // InternalNetSpec.g:2461:1: rule__Session__Group__1 : rule__Session__Group__1__Impl rule__Session__Group__2 ;
    public final void rule__Session__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2465:1: ( rule__Session__Group__1__Impl rule__Session__Group__2 )
            // InternalNetSpec.g:2466:2: rule__Session__Group__1__Impl rule__Session__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Session__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Session__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__1"


    // $ANTLR start "rule__Session__Group__1__Impl"
    // InternalNetSpec.g:2473:1: rule__Session__Group__1__Impl : ( 'Session' ) ;
    public final void rule__Session__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2477:1: ( ( 'Session' ) )
            // InternalNetSpec.g:2478:1: ( 'Session' )
            {
            // InternalNetSpec.g:2478:1: ( 'Session' )
            // InternalNetSpec.g:2479:2: 'Session'
            {
             before(grammarAccess.getSessionAccess().getSessionKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getSessionAccess().getSessionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__1__Impl"


    // $ANTLR start "rule__Session__Group__2"
    // InternalNetSpec.g:2488:1: rule__Session__Group__2 : rule__Session__Group__2__Impl rule__Session__Group__3 ;
    public final void rule__Session__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2492:1: ( rule__Session__Group__2__Impl rule__Session__Group__3 )
            // InternalNetSpec.g:2493:2: rule__Session__Group__2__Impl rule__Session__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Session__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Session__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__2"


    // $ANTLR start "rule__Session__Group__2__Impl"
    // InternalNetSpec.g:2500:1: rule__Session__Group__2__Impl : ( ( rule__Session__NameAssignment_2 ) ) ;
    public final void rule__Session__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2504:1: ( ( ( rule__Session__NameAssignment_2 ) ) )
            // InternalNetSpec.g:2505:1: ( ( rule__Session__NameAssignment_2 ) )
            {
            // InternalNetSpec.g:2505:1: ( ( rule__Session__NameAssignment_2 ) )
            // InternalNetSpec.g:2506:2: ( rule__Session__NameAssignment_2 )
            {
             before(grammarAccess.getSessionAccess().getNameAssignment_2()); 
            // InternalNetSpec.g:2507:2: ( rule__Session__NameAssignment_2 )
            // InternalNetSpec.g:2507:3: rule__Session__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Session__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSessionAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__2__Impl"


    // $ANTLR start "rule__Session__Group__3"
    // InternalNetSpec.g:2515:1: rule__Session__Group__3 : rule__Session__Group__3__Impl rule__Session__Group__4 ;
    public final void rule__Session__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2519:1: ( rule__Session__Group__3__Impl rule__Session__Group__4 )
            // InternalNetSpec.g:2520:2: rule__Session__Group__3__Impl rule__Session__Group__4
            {
            pushFollow(FOLLOW_20);
            rule__Session__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Session__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__3"


    // $ANTLR start "rule__Session__Group__3__Impl"
    // InternalNetSpec.g:2527:1: rule__Session__Group__3__Impl : ( '{' ) ;
    public final void rule__Session__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2531:1: ( ( '{' ) )
            // InternalNetSpec.g:2532:1: ( '{' )
            {
            // InternalNetSpec.g:2532:1: ( '{' )
            // InternalNetSpec.g:2533:2: '{'
            {
             before(grammarAccess.getSessionAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSessionAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__3__Impl"


    // $ANTLR start "rule__Session__Group__4"
    // InternalNetSpec.g:2542:1: rule__Session__Group__4 : rule__Session__Group__4__Impl rule__Session__Group__5 ;
    public final void rule__Session__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2546:1: ( rule__Session__Group__4__Impl rule__Session__Group__5 )
            // InternalNetSpec.g:2547:2: rule__Session__Group__4__Impl rule__Session__Group__5
            {
            pushFollow(FOLLOW_20);
            rule__Session__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Session__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__4"


    // $ANTLR start "rule__Session__Group__4__Impl"
    // InternalNetSpec.g:2554:1: rule__Session__Group__4__Impl : ( ( rule__Session__Group_4__0 )? ) ;
    public final void rule__Session__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2558:1: ( ( ( rule__Session__Group_4__0 )? ) )
            // InternalNetSpec.g:2559:1: ( ( rule__Session__Group_4__0 )? )
            {
            // InternalNetSpec.g:2559:1: ( ( rule__Session__Group_4__0 )? )
            // InternalNetSpec.g:2560:2: ( rule__Session__Group_4__0 )?
            {
             before(grammarAccess.getSessionAccess().getGroup_4()); 
            // InternalNetSpec.g:2561:2: ( rule__Session__Group_4__0 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==37) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalNetSpec.g:2561:3: rule__Session__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Session__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSessionAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__4__Impl"


    // $ANTLR start "rule__Session__Group__5"
    // InternalNetSpec.g:2569:1: rule__Session__Group__5 : rule__Session__Group__5__Impl rule__Session__Group__6 ;
    public final void rule__Session__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2573:1: ( rule__Session__Group__5__Impl rule__Session__Group__6 )
            // InternalNetSpec.g:2574:2: rule__Session__Group__5__Impl rule__Session__Group__6
            {
            pushFollow(FOLLOW_20);
            rule__Session__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Session__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__5"


    // $ANTLR start "rule__Session__Group__5__Impl"
    // InternalNetSpec.g:2581:1: rule__Session__Group__5__Impl : ( ( rule__Session__RootAssignment_5 )? ) ;
    public final void rule__Session__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2585:1: ( ( ( rule__Session__RootAssignment_5 )? ) )
            // InternalNetSpec.g:2586:1: ( ( rule__Session__RootAssignment_5 )? )
            {
            // InternalNetSpec.g:2586:1: ( ( rule__Session__RootAssignment_5 )? )
            // InternalNetSpec.g:2587:2: ( rule__Session__RootAssignment_5 )?
            {
             before(grammarAccess.getSessionAccess().getRootAssignment_5()); 
            // InternalNetSpec.g:2588:2: ( rule__Session__RootAssignment_5 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==32) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalNetSpec.g:2588:3: rule__Session__RootAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__Session__RootAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSessionAccess().getRootAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__5__Impl"


    // $ANTLR start "rule__Session__Group__6"
    // InternalNetSpec.g:2596:1: rule__Session__Group__6 : rule__Session__Group__6__Impl rule__Session__Group__7 ;
    public final void rule__Session__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2600:1: ( rule__Session__Group__6__Impl rule__Session__Group__7 )
            // InternalNetSpec.g:2601:2: rule__Session__Group__6__Impl rule__Session__Group__7
            {
            pushFollow(FOLLOW_20);
            rule__Session__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Session__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__6"


    // $ANTLR start "rule__Session__Group__6__Impl"
    // InternalNetSpec.g:2608:1: rule__Session__Group__6__Impl : ( ( rule__Session__Group_6__0 )? ) ;
    public final void rule__Session__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2612:1: ( ( ( rule__Session__Group_6__0 )? ) )
            // InternalNetSpec.g:2613:1: ( ( rule__Session__Group_6__0 )? )
            {
            // InternalNetSpec.g:2613:1: ( ( rule__Session__Group_6__0 )? )
            // InternalNetSpec.g:2614:2: ( rule__Session__Group_6__0 )?
            {
             before(grammarAccess.getSessionAccess().getGroup_6()); 
            // InternalNetSpec.g:2615:2: ( rule__Session__Group_6__0 )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==28) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalNetSpec.g:2615:3: rule__Session__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Session__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSessionAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__6__Impl"


    // $ANTLR start "rule__Session__Group__7"
    // InternalNetSpec.g:2623:1: rule__Session__Group__7 : rule__Session__Group__7__Impl ;
    public final void rule__Session__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2627:1: ( rule__Session__Group__7__Impl )
            // InternalNetSpec.g:2628:2: rule__Session__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Session__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__7"


    // $ANTLR start "rule__Session__Group__7__Impl"
    // InternalNetSpec.g:2634:1: rule__Session__Group__7__Impl : ( '}' ) ;
    public final void rule__Session__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2638:1: ( ( '}' ) )
            // InternalNetSpec.g:2639:1: ( '}' )
            {
            // InternalNetSpec.g:2639:1: ( '}' )
            // InternalNetSpec.g:2640:2: '}'
            {
             before(grammarAccess.getSessionAccess().getRightCurlyBracketKeyword_7()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getSessionAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group__7__Impl"


    // $ANTLR start "rule__Session__Group_4__0"
    // InternalNetSpec.g:2650:1: rule__Session__Group_4__0 : rule__Session__Group_4__0__Impl rule__Session__Group_4__1 ;
    public final void rule__Session__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2654:1: ( rule__Session__Group_4__0__Impl rule__Session__Group_4__1 )
            // InternalNetSpec.g:2655:2: rule__Session__Group_4__0__Impl rule__Session__Group_4__1
            {
            pushFollow(FOLLOW_14);
            rule__Session__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Session__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group_4__0"


    // $ANTLR start "rule__Session__Group_4__0__Impl"
    // InternalNetSpec.g:2662:1: rule__Session__Group_4__0__Impl : ( 'Login' ) ;
    public final void rule__Session__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2666:1: ( ( 'Login' ) )
            // InternalNetSpec.g:2667:1: ( 'Login' )
            {
            // InternalNetSpec.g:2667:1: ( 'Login' )
            // InternalNetSpec.g:2668:2: 'Login'
            {
             before(grammarAccess.getSessionAccess().getLoginKeyword_4_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getSessionAccess().getLoginKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group_4__0__Impl"


    // $ANTLR start "rule__Session__Group_4__1"
    // InternalNetSpec.g:2677:1: rule__Session__Group_4__1 : rule__Session__Group_4__1__Impl ;
    public final void rule__Session__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2681:1: ( rule__Session__Group_4__1__Impl )
            // InternalNetSpec.g:2682:2: rule__Session__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Session__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group_4__1"


    // $ANTLR start "rule__Session__Group_4__1__Impl"
    // InternalNetSpec.g:2688:1: rule__Session__Group_4__1__Impl : ( ( rule__Session__LoginAssignment_4_1 ) ) ;
    public final void rule__Session__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2692:1: ( ( ( rule__Session__LoginAssignment_4_1 ) ) )
            // InternalNetSpec.g:2693:1: ( ( rule__Session__LoginAssignment_4_1 ) )
            {
            // InternalNetSpec.g:2693:1: ( ( rule__Session__LoginAssignment_4_1 ) )
            // InternalNetSpec.g:2694:2: ( rule__Session__LoginAssignment_4_1 )
            {
             before(grammarAccess.getSessionAccess().getLoginAssignment_4_1()); 
            // InternalNetSpec.g:2695:2: ( rule__Session__LoginAssignment_4_1 )
            // InternalNetSpec.g:2695:3: rule__Session__LoginAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Session__LoginAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getSessionAccess().getLoginAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group_4__1__Impl"


    // $ANTLR start "rule__Session__Group_6__0"
    // InternalNetSpec.g:2704:1: rule__Session__Group_6__0 : rule__Session__Group_6__0__Impl rule__Session__Group_6__1 ;
    public final void rule__Session__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2708:1: ( rule__Session__Group_6__0__Impl rule__Session__Group_6__1 )
            // InternalNetSpec.g:2709:2: rule__Session__Group_6__0__Impl rule__Session__Group_6__1
            {
            pushFollow(FOLLOW_14);
            rule__Session__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Session__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group_6__0"


    // $ANTLR start "rule__Session__Group_6__0__Impl"
    // InternalNetSpec.g:2716:1: rule__Session__Group_6__0__Impl : ( 'Files' ) ;
    public final void rule__Session__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2720:1: ( ( 'Files' ) )
            // InternalNetSpec.g:2721:1: ( 'Files' )
            {
            // InternalNetSpec.g:2721:1: ( 'Files' )
            // InternalNetSpec.g:2722:2: 'Files'
            {
             before(grammarAccess.getSessionAccess().getFilesKeyword_6_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getSessionAccess().getFilesKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group_6__0__Impl"


    // $ANTLR start "rule__Session__Group_6__1"
    // InternalNetSpec.g:2731:1: rule__Session__Group_6__1 : rule__Session__Group_6__1__Impl ;
    public final void rule__Session__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2735:1: ( rule__Session__Group_6__1__Impl )
            // InternalNetSpec.g:2736:2: rule__Session__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Session__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group_6__1"


    // $ANTLR start "rule__Session__Group_6__1__Impl"
    // InternalNetSpec.g:2742:1: rule__Session__Group_6__1__Impl : ( ( ( rule__Session__FilesAssignment_6_1 ) ) ( ( rule__Session__FilesAssignment_6_1 )* ) ) ;
    public final void rule__Session__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2746:1: ( ( ( ( rule__Session__FilesAssignment_6_1 ) ) ( ( rule__Session__FilesAssignment_6_1 )* ) ) )
            // InternalNetSpec.g:2747:1: ( ( ( rule__Session__FilesAssignment_6_1 ) ) ( ( rule__Session__FilesAssignment_6_1 )* ) )
            {
            // InternalNetSpec.g:2747:1: ( ( ( rule__Session__FilesAssignment_6_1 ) ) ( ( rule__Session__FilesAssignment_6_1 )* ) )
            // InternalNetSpec.g:2748:2: ( ( rule__Session__FilesAssignment_6_1 ) ) ( ( rule__Session__FilesAssignment_6_1 )* )
            {
            // InternalNetSpec.g:2748:2: ( ( rule__Session__FilesAssignment_6_1 ) )
            // InternalNetSpec.g:2749:3: ( rule__Session__FilesAssignment_6_1 )
            {
             before(grammarAccess.getSessionAccess().getFilesAssignment_6_1()); 
            // InternalNetSpec.g:2750:3: ( rule__Session__FilesAssignment_6_1 )
            // InternalNetSpec.g:2750:4: rule__Session__FilesAssignment_6_1
            {
            pushFollow(FOLLOW_15);
            rule__Session__FilesAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getSessionAccess().getFilesAssignment_6_1()); 

            }

            // InternalNetSpec.g:2753:2: ( ( rule__Session__FilesAssignment_6_1 )* )
            // InternalNetSpec.g:2754:3: ( rule__Session__FilesAssignment_6_1 )*
            {
             before(grammarAccess.getSessionAccess().getFilesAssignment_6_1()); 
            // InternalNetSpec.g:2755:3: ( rule__Session__FilesAssignment_6_1 )*
            loop36:
            do {
                int alt36=2;
                int LA36_0 = input.LA(1);

                if ( (LA36_0==RULE_ID) ) {
                    alt36=1;
                }


                switch (alt36) {
            	case 1 :
            	    // InternalNetSpec.g:2755:4: rule__Session__FilesAssignment_6_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Session__FilesAssignment_6_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop36;
                }
            } while (true);

             after(grammarAccess.getSessionAccess().getFilesAssignment_6_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__Group_6__1__Impl"


    // $ANTLR start "rule__Antivirus__Group__0"
    // InternalNetSpec.g:2765:1: rule__Antivirus__Group__0 : rule__Antivirus__Group__0__Impl rule__Antivirus__Group__1 ;
    public final void rule__Antivirus__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2769:1: ( rule__Antivirus__Group__0__Impl rule__Antivirus__Group__1 )
            // InternalNetSpec.g:2770:2: rule__Antivirus__Group__0__Impl rule__Antivirus__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__Antivirus__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Antivirus__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__0"


    // $ANTLR start "rule__Antivirus__Group__0__Impl"
    // InternalNetSpec.g:2777:1: rule__Antivirus__Group__0__Impl : ( () ) ;
    public final void rule__Antivirus__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2781:1: ( ( () ) )
            // InternalNetSpec.g:2782:1: ( () )
            {
            // InternalNetSpec.g:2782:1: ( () )
            // InternalNetSpec.g:2783:2: ()
            {
             before(grammarAccess.getAntivirusAccess().getAntivirusAction_0()); 
            // InternalNetSpec.g:2784:2: ()
            // InternalNetSpec.g:2784:3: 
            {
            }

             after(grammarAccess.getAntivirusAccess().getAntivirusAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__0__Impl"


    // $ANTLR start "rule__Antivirus__Group__1"
    // InternalNetSpec.g:2792:1: rule__Antivirus__Group__1 : rule__Antivirus__Group__1__Impl rule__Antivirus__Group__2 ;
    public final void rule__Antivirus__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2796:1: ( rule__Antivirus__Group__1__Impl rule__Antivirus__Group__2 )
            // InternalNetSpec.g:2797:2: rule__Antivirus__Group__1__Impl rule__Antivirus__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Antivirus__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Antivirus__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__1"


    // $ANTLR start "rule__Antivirus__Group__1__Impl"
    // InternalNetSpec.g:2804:1: rule__Antivirus__Group__1__Impl : ( 'Antivirus' ) ;
    public final void rule__Antivirus__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2808:1: ( ( 'Antivirus' ) )
            // InternalNetSpec.g:2809:1: ( 'Antivirus' )
            {
            // InternalNetSpec.g:2809:1: ( 'Antivirus' )
            // InternalNetSpec.g:2810:2: 'Antivirus'
            {
             before(grammarAccess.getAntivirusAccess().getAntivirusKeyword_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getAntivirusAccess().getAntivirusKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__1__Impl"


    // $ANTLR start "rule__Antivirus__Group__2"
    // InternalNetSpec.g:2819:1: rule__Antivirus__Group__2 : rule__Antivirus__Group__2__Impl rule__Antivirus__Group__3 ;
    public final void rule__Antivirus__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2823:1: ( rule__Antivirus__Group__2__Impl rule__Antivirus__Group__3 )
            // InternalNetSpec.g:2824:2: rule__Antivirus__Group__2__Impl rule__Antivirus__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Antivirus__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Antivirus__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__2"


    // $ANTLR start "rule__Antivirus__Group__2__Impl"
    // InternalNetSpec.g:2831:1: rule__Antivirus__Group__2__Impl : ( ( rule__Antivirus__NameAssignment_2 ) ) ;
    public final void rule__Antivirus__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2835:1: ( ( ( rule__Antivirus__NameAssignment_2 ) ) )
            // InternalNetSpec.g:2836:1: ( ( rule__Antivirus__NameAssignment_2 ) )
            {
            // InternalNetSpec.g:2836:1: ( ( rule__Antivirus__NameAssignment_2 ) )
            // InternalNetSpec.g:2837:2: ( rule__Antivirus__NameAssignment_2 )
            {
             before(grammarAccess.getAntivirusAccess().getNameAssignment_2()); 
            // InternalNetSpec.g:2838:2: ( rule__Antivirus__NameAssignment_2 )
            // InternalNetSpec.g:2838:3: rule__Antivirus__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Antivirus__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAntivirusAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__2__Impl"


    // $ANTLR start "rule__Antivirus__Group__3"
    // InternalNetSpec.g:2846:1: rule__Antivirus__Group__3 : rule__Antivirus__Group__3__Impl rule__Antivirus__Group__4 ;
    public final void rule__Antivirus__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2850:1: ( rule__Antivirus__Group__3__Impl rule__Antivirus__Group__4 )
            // InternalNetSpec.g:2851:2: rule__Antivirus__Group__3__Impl rule__Antivirus__Group__4
            {
            pushFollow(FOLLOW_22);
            rule__Antivirus__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Antivirus__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__3"


    // $ANTLR start "rule__Antivirus__Group__3__Impl"
    // InternalNetSpec.g:2858:1: rule__Antivirus__Group__3__Impl : ( '{' ) ;
    public final void rule__Antivirus__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2862:1: ( ( '{' ) )
            // InternalNetSpec.g:2863:1: ( '{' )
            {
            // InternalNetSpec.g:2863:1: ( '{' )
            // InternalNetSpec.g:2864:2: '{'
            {
             before(grammarAccess.getAntivirusAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getAntivirusAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__3__Impl"


    // $ANTLR start "rule__Antivirus__Group__4"
    // InternalNetSpec.g:2873:1: rule__Antivirus__Group__4 : rule__Antivirus__Group__4__Impl rule__Antivirus__Group__5 ;
    public final void rule__Antivirus__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2877:1: ( rule__Antivirus__Group__4__Impl rule__Antivirus__Group__5 )
            // InternalNetSpec.g:2878:2: rule__Antivirus__Group__4__Impl rule__Antivirus__Group__5
            {
            pushFollow(FOLLOW_23);
            rule__Antivirus__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Antivirus__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__4"


    // $ANTLR start "rule__Antivirus__Group__4__Impl"
    // InternalNetSpec.g:2885:1: rule__Antivirus__Group__4__Impl : ( 'Level' ) ;
    public final void rule__Antivirus__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2889:1: ( ( 'Level' ) )
            // InternalNetSpec.g:2890:1: ( 'Level' )
            {
            // InternalNetSpec.g:2890:1: ( 'Level' )
            // InternalNetSpec.g:2891:2: 'Level'
            {
             before(grammarAccess.getAntivirusAccess().getLevelKeyword_4()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getAntivirusAccess().getLevelKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__4__Impl"


    // $ANTLR start "rule__Antivirus__Group__5"
    // InternalNetSpec.g:2900:1: rule__Antivirus__Group__5 : rule__Antivirus__Group__5__Impl rule__Antivirus__Group__6 ;
    public final void rule__Antivirus__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2904:1: ( rule__Antivirus__Group__5__Impl rule__Antivirus__Group__6 )
            // InternalNetSpec.g:2905:2: rule__Antivirus__Group__5__Impl rule__Antivirus__Group__6
            {
            pushFollow(FOLLOW_24);
            rule__Antivirus__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Antivirus__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__5"


    // $ANTLR start "rule__Antivirus__Group__5__Impl"
    // InternalNetSpec.g:2912:1: rule__Antivirus__Group__5__Impl : ( ( rule__Antivirus__LevelAssignment_5 ) ) ;
    public final void rule__Antivirus__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2916:1: ( ( ( rule__Antivirus__LevelAssignment_5 ) ) )
            // InternalNetSpec.g:2917:1: ( ( rule__Antivirus__LevelAssignment_5 ) )
            {
            // InternalNetSpec.g:2917:1: ( ( rule__Antivirus__LevelAssignment_5 ) )
            // InternalNetSpec.g:2918:2: ( rule__Antivirus__LevelAssignment_5 )
            {
             before(grammarAccess.getAntivirusAccess().getLevelAssignment_5()); 
            // InternalNetSpec.g:2919:2: ( rule__Antivirus__LevelAssignment_5 )
            // InternalNetSpec.g:2919:3: rule__Antivirus__LevelAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Antivirus__LevelAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getAntivirusAccess().getLevelAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__5__Impl"


    // $ANTLR start "rule__Antivirus__Group__6"
    // InternalNetSpec.g:2927:1: rule__Antivirus__Group__6 : rule__Antivirus__Group__6__Impl ;
    public final void rule__Antivirus__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2931:1: ( rule__Antivirus__Group__6__Impl )
            // InternalNetSpec.g:2932:2: rule__Antivirus__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Antivirus__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__6"


    // $ANTLR start "rule__Antivirus__Group__6__Impl"
    // InternalNetSpec.g:2938:1: rule__Antivirus__Group__6__Impl : ( '}' ) ;
    public final void rule__Antivirus__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2942:1: ( ( '}' ) )
            // InternalNetSpec.g:2943:1: ( '}' )
            {
            // InternalNetSpec.g:2943:1: ( '}' )
            // InternalNetSpec.g:2944:2: '}'
            {
             before(grammarAccess.getAntivirusAccess().getRightCurlyBracketKeyword_6()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getAntivirusAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__Group__6__Impl"


    // $ANTLR start "rule__File__Group__0"
    // InternalNetSpec.g:2954:1: rule__File__Group__0 : rule__File__Group__0__Impl rule__File__Group__1 ;
    public final void rule__File__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2958:1: ( rule__File__Group__0__Impl rule__File__Group__1 )
            // InternalNetSpec.g:2959:2: rule__File__Group__0__Impl rule__File__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__File__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__0"


    // $ANTLR start "rule__File__Group__0__Impl"
    // InternalNetSpec.g:2966:1: rule__File__Group__0__Impl : ( () ) ;
    public final void rule__File__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2970:1: ( ( () ) )
            // InternalNetSpec.g:2971:1: ( () )
            {
            // InternalNetSpec.g:2971:1: ( () )
            // InternalNetSpec.g:2972:2: ()
            {
             before(grammarAccess.getFileAccess().getFileAction_0()); 
            // InternalNetSpec.g:2973:2: ()
            // InternalNetSpec.g:2973:3: 
            {
            }

             after(grammarAccess.getFileAccess().getFileAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__0__Impl"


    // $ANTLR start "rule__File__Group__1"
    // InternalNetSpec.g:2981:1: rule__File__Group__1 : rule__File__Group__1__Impl rule__File__Group__2 ;
    public final void rule__File__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2985:1: ( rule__File__Group__1__Impl rule__File__Group__2 )
            // InternalNetSpec.g:2986:2: rule__File__Group__1__Impl rule__File__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__File__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__1"


    // $ANTLR start "rule__File__Group__1__Impl"
    // InternalNetSpec.g:2993:1: rule__File__Group__1__Impl : ( 'File' ) ;
    public final void rule__File__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:2997:1: ( ( 'File' ) )
            // InternalNetSpec.g:2998:1: ( 'File' )
            {
            // InternalNetSpec.g:2998:1: ( 'File' )
            // InternalNetSpec.g:2999:2: 'File'
            {
             before(grammarAccess.getFileAccess().getFileKeyword_1()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getFileKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__1__Impl"


    // $ANTLR start "rule__File__Group__2"
    // InternalNetSpec.g:3008:1: rule__File__Group__2 : rule__File__Group__2__Impl rule__File__Group__3 ;
    public final void rule__File__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3012:1: ( rule__File__Group__2__Impl rule__File__Group__3 )
            // InternalNetSpec.g:3013:2: rule__File__Group__2__Impl rule__File__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__File__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__2"


    // $ANTLR start "rule__File__Group__2__Impl"
    // InternalNetSpec.g:3020:1: rule__File__Group__2__Impl : ( ( rule__File__NameAssignment_2 ) ) ;
    public final void rule__File__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3024:1: ( ( ( rule__File__NameAssignment_2 ) ) )
            // InternalNetSpec.g:3025:1: ( ( rule__File__NameAssignment_2 ) )
            {
            // InternalNetSpec.g:3025:1: ( ( rule__File__NameAssignment_2 ) )
            // InternalNetSpec.g:3026:2: ( rule__File__NameAssignment_2 )
            {
             before(grammarAccess.getFileAccess().getNameAssignment_2()); 
            // InternalNetSpec.g:3027:2: ( rule__File__NameAssignment_2 )
            // InternalNetSpec.g:3027:3: rule__File__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__File__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFileAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__2__Impl"


    // $ANTLR start "rule__File__Group__3"
    // InternalNetSpec.g:3035:1: rule__File__Group__3 : rule__File__Group__3__Impl rule__File__Group__4 ;
    public final void rule__File__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3039:1: ( rule__File__Group__3__Impl rule__File__Group__4 )
            // InternalNetSpec.g:3040:2: rule__File__Group__3__Impl rule__File__Group__4
            {
            pushFollow(FOLLOW_26);
            rule__File__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__3"


    // $ANTLR start "rule__File__Group__3__Impl"
    // InternalNetSpec.g:3047:1: rule__File__Group__3__Impl : ( '{' ) ;
    public final void rule__File__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3051:1: ( ( '{' ) )
            // InternalNetSpec.g:3052:1: ( '{' )
            {
            // InternalNetSpec.g:3052:1: ( '{' )
            // InternalNetSpec.g:3053:2: '{'
            {
             before(grammarAccess.getFileAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__3__Impl"


    // $ANTLR start "rule__File__Group__4"
    // InternalNetSpec.g:3062:1: rule__File__Group__4 : rule__File__Group__4__Impl rule__File__Group__5 ;
    public final void rule__File__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3066:1: ( rule__File__Group__4__Impl rule__File__Group__5 )
            // InternalNetSpec.g:3067:2: rule__File__Group__4__Impl rule__File__Group__5
            {
            pushFollow(FOLLOW_26);
            rule__File__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__4"


    // $ANTLR start "rule__File__Group__4__Impl"
    // InternalNetSpec.g:3074:1: rule__File__Group__4__Impl : ( ( rule__File__Group_4__0 )? ) ;
    public final void rule__File__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3078:1: ( ( ( rule__File__Group_4__0 )? ) )
            // InternalNetSpec.g:3079:1: ( ( rule__File__Group_4__0 )? )
            {
            // InternalNetSpec.g:3079:1: ( ( rule__File__Group_4__0 )? )
            // InternalNetSpec.g:3080:2: ( rule__File__Group_4__0 )?
            {
             before(grammarAccess.getFileAccess().getGroup_4()); 
            // InternalNetSpec.g:3081:2: ( rule__File__Group_4__0 )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==40) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalNetSpec.g:3081:3: rule__File__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__File__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFileAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__4__Impl"


    // $ANTLR start "rule__File__Group__5"
    // InternalNetSpec.g:3089:1: rule__File__Group__5 : rule__File__Group__5__Impl rule__File__Group__6 ;
    public final void rule__File__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3093:1: ( rule__File__Group__5__Impl rule__File__Group__6 )
            // InternalNetSpec.g:3094:2: rule__File__Group__5__Impl rule__File__Group__6
            {
            pushFollow(FOLLOW_26);
            rule__File__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__5"


    // $ANTLR start "rule__File__Group__5__Impl"
    // InternalNetSpec.g:3101:1: rule__File__Group__5__Impl : ( ( rule__File__Group_5__0 )? ) ;
    public final void rule__File__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3105:1: ( ( ( rule__File__Group_5__0 )? ) )
            // InternalNetSpec.g:3106:1: ( ( rule__File__Group_5__0 )? )
            {
            // InternalNetSpec.g:3106:1: ( ( rule__File__Group_5__0 )? )
            // InternalNetSpec.g:3107:2: ( rule__File__Group_5__0 )?
            {
             before(grammarAccess.getFileAccess().getGroup_5()); 
            // InternalNetSpec.g:3108:2: ( rule__File__Group_5__0 )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==41) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalNetSpec.g:3108:3: rule__File__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__File__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFileAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__5__Impl"


    // $ANTLR start "rule__File__Group__6"
    // InternalNetSpec.g:3116:1: rule__File__Group__6 : rule__File__Group__6__Impl rule__File__Group__7 ;
    public final void rule__File__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3120:1: ( rule__File__Group__6__Impl rule__File__Group__7 )
            // InternalNetSpec.g:3121:2: rule__File__Group__6__Impl rule__File__Group__7
            {
            pushFollow(FOLLOW_26);
            rule__File__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__6"


    // $ANTLR start "rule__File__Group__6__Impl"
    // InternalNetSpec.g:3128:1: rule__File__Group__6__Impl : ( ( rule__File__Group_6__0 )? ) ;
    public final void rule__File__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3132:1: ( ( ( rule__File__Group_6__0 )? ) )
            // InternalNetSpec.g:3133:1: ( ( rule__File__Group_6__0 )? )
            {
            // InternalNetSpec.g:3133:1: ( ( rule__File__Group_6__0 )? )
            // InternalNetSpec.g:3134:2: ( rule__File__Group_6__0 )?
            {
             before(grammarAccess.getFileAccess().getGroup_6()); 
            // InternalNetSpec.g:3135:2: ( rule__File__Group_6__0 )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==42) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalNetSpec.g:3135:3: rule__File__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__File__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFileAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__6__Impl"


    // $ANTLR start "rule__File__Group__7"
    // InternalNetSpec.g:3143:1: rule__File__Group__7 : rule__File__Group__7__Impl rule__File__Group__8 ;
    public final void rule__File__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3147:1: ( rule__File__Group__7__Impl rule__File__Group__8 )
            // InternalNetSpec.g:3148:2: rule__File__Group__7__Impl rule__File__Group__8
            {
            pushFollow(FOLLOW_26);
            rule__File__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__7"


    // $ANTLR start "rule__File__Group__7__Impl"
    // InternalNetSpec.g:3155:1: rule__File__Group__7__Impl : ( ( rule__File__Group_7__0 )? ) ;
    public final void rule__File__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3159:1: ( ( ( rule__File__Group_7__0 )? ) )
            // InternalNetSpec.g:3160:1: ( ( rule__File__Group_7__0 )? )
            {
            // InternalNetSpec.g:3160:1: ( ( rule__File__Group_7__0 )? )
            // InternalNetSpec.g:3161:2: ( rule__File__Group_7__0 )?
            {
             before(grammarAccess.getFileAccess().getGroup_7()); 
            // InternalNetSpec.g:3162:2: ( rule__File__Group_7__0 )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==27) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalNetSpec.g:3162:3: rule__File__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__File__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFileAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__7__Impl"


    // $ANTLR start "rule__File__Group__8"
    // InternalNetSpec.g:3170:1: rule__File__Group__8 : rule__File__Group__8__Impl ;
    public final void rule__File__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3174:1: ( rule__File__Group__8__Impl )
            // InternalNetSpec.g:3175:2: rule__File__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__File__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__8"


    // $ANTLR start "rule__File__Group__8__Impl"
    // InternalNetSpec.g:3181:1: rule__File__Group__8__Impl : ( '}' ) ;
    public final void rule__File__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3185:1: ( ( '}' ) )
            // InternalNetSpec.g:3186:1: ( '}' )
            {
            // InternalNetSpec.g:3186:1: ( '}' )
            // InternalNetSpec.g:3187:2: '}'
            {
             before(grammarAccess.getFileAccess().getRightCurlyBracketKeyword_8()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__8__Impl"


    // $ANTLR start "rule__File__Group_4__0"
    // InternalNetSpec.g:3197:1: rule__File__Group_4__0 : rule__File__Group_4__0__Impl rule__File__Group_4__1 ;
    public final void rule__File__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3201:1: ( rule__File__Group_4__0__Impl rule__File__Group_4__1 )
            // InternalNetSpec.g:3202:2: rule__File__Group_4__0__Impl rule__File__Group_4__1
            {
            pushFollow(FOLLOW_14);
            rule__File__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_4__0"


    // $ANTLR start "rule__File__Group_4__0__Impl"
    // InternalNetSpec.g:3209:1: rule__File__Group_4__0__Impl : ( 'Key' ) ;
    public final void rule__File__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3213:1: ( ( 'Key' ) )
            // InternalNetSpec.g:3214:1: ( 'Key' )
            {
            // InternalNetSpec.g:3214:1: ( 'Key' )
            // InternalNetSpec.g:3215:2: 'Key'
            {
             before(grammarAccess.getFileAccess().getKeyKeyword_4_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getKeyKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_4__0__Impl"


    // $ANTLR start "rule__File__Group_4__1"
    // InternalNetSpec.g:3224:1: rule__File__Group_4__1 : rule__File__Group_4__1__Impl ;
    public final void rule__File__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3228:1: ( rule__File__Group_4__1__Impl )
            // InternalNetSpec.g:3229:2: rule__File__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__File__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_4__1"


    // $ANTLR start "rule__File__Group_4__1__Impl"
    // InternalNetSpec.g:3235:1: rule__File__Group_4__1__Impl : ( ( rule__File__KeyAssignment_4_1 ) ) ;
    public final void rule__File__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3239:1: ( ( ( rule__File__KeyAssignment_4_1 ) ) )
            // InternalNetSpec.g:3240:1: ( ( rule__File__KeyAssignment_4_1 ) )
            {
            // InternalNetSpec.g:3240:1: ( ( rule__File__KeyAssignment_4_1 ) )
            // InternalNetSpec.g:3241:2: ( rule__File__KeyAssignment_4_1 )
            {
             before(grammarAccess.getFileAccess().getKeyAssignment_4_1()); 
            // InternalNetSpec.g:3242:2: ( rule__File__KeyAssignment_4_1 )
            // InternalNetSpec.g:3242:3: rule__File__KeyAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__File__KeyAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getFileAccess().getKeyAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_4__1__Impl"


    // $ANTLR start "rule__File__Group_5__0"
    // InternalNetSpec.g:3251:1: rule__File__Group_5__0 : rule__File__Group_5__0__Impl rule__File__Group_5__1 ;
    public final void rule__File__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3255:1: ( rule__File__Group_5__0__Impl rule__File__Group_5__1 )
            // InternalNetSpec.g:3256:2: rule__File__Group_5__0__Impl rule__File__Group_5__1
            {
            pushFollow(FOLLOW_14);
            rule__File__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_5__0"


    // $ANTLR start "rule__File__Group_5__0__Impl"
    // InternalNetSpec.g:3263:1: rule__File__Group_5__0__Impl : ( 'ContainsLogins' ) ;
    public final void rule__File__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3267:1: ( ( 'ContainsLogins' ) )
            // InternalNetSpec.g:3268:1: ( 'ContainsLogins' )
            {
            // InternalNetSpec.g:3268:1: ( 'ContainsLogins' )
            // InternalNetSpec.g:3269:2: 'ContainsLogins'
            {
             before(grammarAccess.getFileAccess().getContainsLoginsKeyword_5_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getContainsLoginsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_5__0__Impl"


    // $ANTLR start "rule__File__Group_5__1"
    // InternalNetSpec.g:3278:1: rule__File__Group_5__1 : rule__File__Group_5__1__Impl ;
    public final void rule__File__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3282:1: ( rule__File__Group_5__1__Impl )
            // InternalNetSpec.g:3283:2: rule__File__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__File__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_5__1"


    // $ANTLR start "rule__File__Group_5__1__Impl"
    // InternalNetSpec.g:3289:1: rule__File__Group_5__1__Impl : ( ( ( rule__File__ContainsLAssignment_5_1 ) ) ( ( rule__File__ContainsLAssignment_5_1 )* ) ) ;
    public final void rule__File__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3293:1: ( ( ( ( rule__File__ContainsLAssignment_5_1 ) ) ( ( rule__File__ContainsLAssignment_5_1 )* ) ) )
            // InternalNetSpec.g:3294:1: ( ( ( rule__File__ContainsLAssignment_5_1 ) ) ( ( rule__File__ContainsLAssignment_5_1 )* ) )
            {
            // InternalNetSpec.g:3294:1: ( ( ( rule__File__ContainsLAssignment_5_1 ) ) ( ( rule__File__ContainsLAssignment_5_1 )* ) )
            // InternalNetSpec.g:3295:2: ( ( rule__File__ContainsLAssignment_5_1 ) ) ( ( rule__File__ContainsLAssignment_5_1 )* )
            {
            // InternalNetSpec.g:3295:2: ( ( rule__File__ContainsLAssignment_5_1 ) )
            // InternalNetSpec.g:3296:3: ( rule__File__ContainsLAssignment_5_1 )
            {
             before(grammarAccess.getFileAccess().getContainsLAssignment_5_1()); 
            // InternalNetSpec.g:3297:3: ( rule__File__ContainsLAssignment_5_1 )
            // InternalNetSpec.g:3297:4: rule__File__ContainsLAssignment_5_1
            {
            pushFollow(FOLLOW_15);
            rule__File__ContainsLAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getFileAccess().getContainsLAssignment_5_1()); 

            }

            // InternalNetSpec.g:3300:2: ( ( rule__File__ContainsLAssignment_5_1 )* )
            // InternalNetSpec.g:3301:3: ( rule__File__ContainsLAssignment_5_1 )*
            {
             before(grammarAccess.getFileAccess().getContainsLAssignment_5_1()); 
            // InternalNetSpec.g:3302:3: ( rule__File__ContainsLAssignment_5_1 )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==RULE_ID) ) {
                    alt41=1;
                }


                switch (alt41) {
            	case 1 :
            	    // InternalNetSpec.g:3302:4: rule__File__ContainsLAssignment_5_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__File__ContainsLAssignment_5_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);

             after(grammarAccess.getFileAccess().getContainsLAssignment_5_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_5__1__Impl"


    // $ANTLR start "rule__File__Group_6__0"
    // InternalNetSpec.g:3312:1: rule__File__Group_6__0 : rule__File__Group_6__0__Impl rule__File__Group_6__1 ;
    public final void rule__File__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3316:1: ( rule__File__Group_6__0__Impl rule__File__Group_6__1 )
            // InternalNetSpec.g:3317:2: rule__File__Group_6__0__Impl rule__File__Group_6__1
            {
            pushFollow(FOLLOW_14);
            rule__File__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_6__0"


    // $ANTLR start "rule__File__Group_6__0__Impl"
    // InternalNetSpec.g:3324:1: rule__File__Group_6__0__Impl : ( 'ContainsKeys' ) ;
    public final void rule__File__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3328:1: ( ( 'ContainsKeys' ) )
            // InternalNetSpec.g:3329:1: ( 'ContainsKeys' )
            {
            // InternalNetSpec.g:3329:1: ( 'ContainsKeys' )
            // InternalNetSpec.g:3330:2: 'ContainsKeys'
            {
             before(grammarAccess.getFileAccess().getContainsKeysKeyword_6_0()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getContainsKeysKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_6__0__Impl"


    // $ANTLR start "rule__File__Group_6__1"
    // InternalNetSpec.g:3339:1: rule__File__Group_6__1 : rule__File__Group_6__1__Impl ;
    public final void rule__File__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3343:1: ( rule__File__Group_6__1__Impl )
            // InternalNetSpec.g:3344:2: rule__File__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__File__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_6__1"


    // $ANTLR start "rule__File__Group_6__1__Impl"
    // InternalNetSpec.g:3350:1: rule__File__Group_6__1__Impl : ( ( ( rule__File__ContainsKAssignment_6_1 ) ) ( ( rule__File__ContainsKAssignment_6_1 )* ) ) ;
    public final void rule__File__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3354:1: ( ( ( ( rule__File__ContainsKAssignment_6_1 ) ) ( ( rule__File__ContainsKAssignment_6_1 )* ) ) )
            // InternalNetSpec.g:3355:1: ( ( ( rule__File__ContainsKAssignment_6_1 ) ) ( ( rule__File__ContainsKAssignment_6_1 )* ) )
            {
            // InternalNetSpec.g:3355:1: ( ( ( rule__File__ContainsKAssignment_6_1 ) ) ( ( rule__File__ContainsKAssignment_6_1 )* ) )
            // InternalNetSpec.g:3356:2: ( ( rule__File__ContainsKAssignment_6_1 ) ) ( ( rule__File__ContainsKAssignment_6_1 )* )
            {
            // InternalNetSpec.g:3356:2: ( ( rule__File__ContainsKAssignment_6_1 ) )
            // InternalNetSpec.g:3357:3: ( rule__File__ContainsKAssignment_6_1 )
            {
             before(grammarAccess.getFileAccess().getContainsKAssignment_6_1()); 
            // InternalNetSpec.g:3358:3: ( rule__File__ContainsKAssignment_6_1 )
            // InternalNetSpec.g:3358:4: rule__File__ContainsKAssignment_6_1
            {
            pushFollow(FOLLOW_15);
            rule__File__ContainsKAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getFileAccess().getContainsKAssignment_6_1()); 

            }

            // InternalNetSpec.g:3361:2: ( ( rule__File__ContainsKAssignment_6_1 )* )
            // InternalNetSpec.g:3362:3: ( rule__File__ContainsKAssignment_6_1 )*
            {
             before(grammarAccess.getFileAccess().getContainsKAssignment_6_1()); 
            // InternalNetSpec.g:3363:3: ( rule__File__ContainsKAssignment_6_1 )*
            loop42:
            do {
                int alt42=2;
                int LA42_0 = input.LA(1);

                if ( (LA42_0==RULE_ID) ) {
                    alt42=1;
                }


                switch (alt42) {
            	case 1 :
            	    // InternalNetSpec.g:3363:4: rule__File__ContainsKAssignment_6_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__File__ContainsKAssignment_6_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop42;
                }
            } while (true);

             after(grammarAccess.getFileAccess().getContainsKAssignment_6_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_6__1__Impl"


    // $ANTLR start "rule__File__Group_7__0"
    // InternalNetSpec.g:3373:1: rule__File__Group_7__0 : rule__File__Group_7__0__Impl rule__File__Group_7__1 ;
    public final void rule__File__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3377:1: ( rule__File__Group_7__0__Impl rule__File__Group_7__1 )
            // InternalNetSpec.g:3378:2: rule__File__Group_7__0__Impl rule__File__Group_7__1
            {
            pushFollow(FOLLOW_14);
            rule__File__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_7__0"


    // $ANTLR start "rule__File__Group_7__0__Impl"
    // InternalNetSpec.g:3385:1: rule__File__Group_7__0__Impl : ( 'Datas' ) ;
    public final void rule__File__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3389:1: ( ( 'Datas' ) )
            // InternalNetSpec.g:3390:1: ( 'Datas' )
            {
            // InternalNetSpec.g:3390:1: ( 'Datas' )
            // InternalNetSpec.g:3391:2: 'Datas'
            {
             before(grammarAccess.getFileAccess().getDatasKeyword_7_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getDatasKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_7__0__Impl"


    // $ANTLR start "rule__File__Group_7__1"
    // InternalNetSpec.g:3400:1: rule__File__Group_7__1 : rule__File__Group_7__1__Impl ;
    public final void rule__File__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3404:1: ( rule__File__Group_7__1__Impl )
            // InternalNetSpec.g:3405:2: rule__File__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__File__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_7__1"


    // $ANTLR start "rule__File__Group_7__1__Impl"
    // InternalNetSpec.g:3411:1: rule__File__Group_7__1__Impl : ( ( ( rule__File__DatasAssignment_7_1 ) ) ( ( rule__File__DatasAssignment_7_1 )* ) ) ;
    public final void rule__File__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3415:1: ( ( ( ( rule__File__DatasAssignment_7_1 ) ) ( ( rule__File__DatasAssignment_7_1 )* ) ) )
            // InternalNetSpec.g:3416:1: ( ( ( rule__File__DatasAssignment_7_1 ) ) ( ( rule__File__DatasAssignment_7_1 )* ) )
            {
            // InternalNetSpec.g:3416:1: ( ( ( rule__File__DatasAssignment_7_1 ) ) ( ( rule__File__DatasAssignment_7_1 )* ) )
            // InternalNetSpec.g:3417:2: ( ( rule__File__DatasAssignment_7_1 ) ) ( ( rule__File__DatasAssignment_7_1 )* )
            {
            // InternalNetSpec.g:3417:2: ( ( rule__File__DatasAssignment_7_1 ) )
            // InternalNetSpec.g:3418:3: ( rule__File__DatasAssignment_7_1 )
            {
             before(grammarAccess.getFileAccess().getDatasAssignment_7_1()); 
            // InternalNetSpec.g:3419:3: ( rule__File__DatasAssignment_7_1 )
            // InternalNetSpec.g:3419:4: rule__File__DatasAssignment_7_1
            {
            pushFollow(FOLLOW_15);
            rule__File__DatasAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getFileAccess().getDatasAssignment_7_1()); 

            }

            // InternalNetSpec.g:3422:2: ( ( rule__File__DatasAssignment_7_1 )* )
            // InternalNetSpec.g:3423:3: ( rule__File__DatasAssignment_7_1 )*
            {
             before(grammarAccess.getFileAccess().getDatasAssignment_7_1()); 
            // InternalNetSpec.g:3424:3: ( rule__File__DatasAssignment_7_1 )*
            loop43:
            do {
                int alt43=2;
                int LA43_0 = input.LA(1);

                if ( (LA43_0==RULE_ID) ) {
                    alt43=1;
                }


                switch (alt43) {
            	case 1 :
            	    // InternalNetSpec.g:3424:4: rule__File__DatasAssignment_7_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__File__DatasAssignment_7_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop43;
                }
            } while (true);

             after(grammarAccess.getFileAccess().getDatasAssignment_7_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group_7__1__Impl"


    // $ANTLR start "rule__Data__Group__0"
    // InternalNetSpec.g:3434:1: rule__Data__Group__0 : rule__Data__Group__0__Impl rule__Data__Group__1 ;
    public final void rule__Data__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3438:1: ( rule__Data__Group__0__Impl rule__Data__Group__1 )
            // InternalNetSpec.g:3439:2: rule__Data__Group__0__Impl rule__Data__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Data__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Data__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__Group__0"


    // $ANTLR start "rule__Data__Group__0__Impl"
    // InternalNetSpec.g:3446:1: rule__Data__Group__0__Impl : ( 'Data' ) ;
    public final void rule__Data__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3450:1: ( ( 'Data' ) )
            // InternalNetSpec.g:3451:1: ( 'Data' )
            {
            // InternalNetSpec.g:3451:1: ( 'Data' )
            // InternalNetSpec.g:3452:2: 'Data'
            {
             before(grammarAccess.getDataAccess().getDataKeyword_0()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getDataAccess().getDataKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__Group__0__Impl"


    // $ANTLR start "rule__Data__Group__1"
    // InternalNetSpec.g:3461:1: rule__Data__Group__1 : rule__Data__Group__1__Impl rule__Data__Group__2 ;
    public final void rule__Data__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3465:1: ( rule__Data__Group__1__Impl rule__Data__Group__2 )
            // InternalNetSpec.g:3466:2: rule__Data__Group__1__Impl rule__Data__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Data__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Data__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__Group__1"


    // $ANTLR start "rule__Data__Group__1__Impl"
    // InternalNetSpec.g:3473:1: rule__Data__Group__1__Impl : ( ( rule__Data__NameAssignment_1 ) ) ;
    public final void rule__Data__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3477:1: ( ( ( rule__Data__NameAssignment_1 ) ) )
            // InternalNetSpec.g:3478:1: ( ( rule__Data__NameAssignment_1 ) )
            {
            // InternalNetSpec.g:3478:1: ( ( rule__Data__NameAssignment_1 ) )
            // InternalNetSpec.g:3479:2: ( rule__Data__NameAssignment_1 )
            {
             before(grammarAccess.getDataAccess().getNameAssignment_1()); 
            // InternalNetSpec.g:3480:2: ( rule__Data__NameAssignment_1 )
            // InternalNetSpec.g:3480:3: rule__Data__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Data__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__Group__1__Impl"


    // $ANTLR start "rule__Data__Group__2"
    // InternalNetSpec.g:3488:1: rule__Data__Group__2 : rule__Data__Group__2__Impl rule__Data__Group__3 ;
    public final void rule__Data__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3492:1: ( rule__Data__Group__2__Impl rule__Data__Group__3 )
            // InternalNetSpec.g:3493:2: rule__Data__Group__2__Impl rule__Data__Group__3
            {
            pushFollow(FOLLOW_24);
            rule__Data__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Data__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__Group__2"


    // $ANTLR start "rule__Data__Group__2__Impl"
    // InternalNetSpec.g:3500:1: rule__Data__Group__2__Impl : ( '{' ) ;
    public final void rule__Data__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3504:1: ( ( '{' ) )
            // InternalNetSpec.g:3505:1: ( '{' )
            {
            // InternalNetSpec.g:3505:1: ( '{' )
            // InternalNetSpec.g:3506:2: '{'
            {
             before(grammarAccess.getDataAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getDataAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__Group__2__Impl"


    // $ANTLR start "rule__Data__Group__3"
    // InternalNetSpec.g:3515:1: rule__Data__Group__3 : rule__Data__Group__3__Impl ;
    public final void rule__Data__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3519:1: ( rule__Data__Group__3__Impl )
            // InternalNetSpec.g:3520:2: rule__Data__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Data__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__Group__3"


    // $ANTLR start "rule__Data__Group__3__Impl"
    // InternalNetSpec.g:3526:1: rule__Data__Group__3__Impl : ( '}' ) ;
    public final void rule__Data__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3530:1: ( ( '}' ) )
            // InternalNetSpec.g:3531:1: ( '}' )
            {
            // InternalNetSpec.g:3531:1: ( '}' )
            // InternalNetSpec.g:3532:2: '}'
            {
             before(grammarAccess.getDataAccess().getRightCurlyBracketKeyword_3()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getDataAccess().getRightCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__Group__3__Impl"


    // $ANTLR start "rule__Login__Group__0"
    // InternalNetSpec.g:3542:1: rule__Login__Group__0 : rule__Login__Group__0__Impl rule__Login__Group__1 ;
    public final void rule__Login__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3546:1: ( rule__Login__Group__0__Impl rule__Login__Group__1 )
            // InternalNetSpec.g:3547:2: rule__Login__Group__0__Impl rule__Login__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__Login__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Login__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__0"


    // $ANTLR start "rule__Login__Group__0__Impl"
    // InternalNetSpec.g:3554:1: rule__Login__Group__0__Impl : ( () ) ;
    public final void rule__Login__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3558:1: ( ( () ) )
            // InternalNetSpec.g:3559:1: ( () )
            {
            // InternalNetSpec.g:3559:1: ( () )
            // InternalNetSpec.g:3560:2: ()
            {
             before(grammarAccess.getLoginAccess().getLoginAction_0()); 
            // InternalNetSpec.g:3561:2: ()
            // InternalNetSpec.g:3561:3: 
            {
            }

             after(grammarAccess.getLoginAccess().getLoginAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__0__Impl"


    // $ANTLR start "rule__Login__Group__1"
    // InternalNetSpec.g:3569:1: rule__Login__Group__1 : rule__Login__Group__1__Impl rule__Login__Group__2 ;
    public final void rule__Login__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3573:1: ( rule__Login__Group__1__Impl rule__Login__Group__2 )
            // InternalNetSpec.g:3574:2: rule__Login__Group__1__Impl rule__Login__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Login__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Login__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__1"


    // $ANTLR start "rule__Login__Group__1__Impl"
    // InternalNetSpec.g:3581:1: rule__Login__Group__1__Impl : ( 'Login' ) ;
    public final void rule__Login__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3585:1: ( ( 'Login' ) )
            // InternalNetSpec.g:3586:1: ( 'Login' )
            {
            // InternalNetSpec.g:3586:1: ( 'Login' )
            // InternalNetSpec.g:3587:2: 'Login'
            {
             before(grammarAccess.getLoginAccess().getLoginKeyword_1()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getLoginAccess().getLoginKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__1__Impl"


    // $ANTLR start "rule__Login__Group__2"
    // InternalNetSpec.g:3596:1: rule__Login__Group__2 : rule__Login__Group__2__Impl rule__Login__Group__3 ;
    public final void rule__Login__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3600:1: ( rule__Login__Group__2__Impl rule__Login__Group__3 )
            // InternalNetSpec.g:3601:2: rule__Login__Group__2__Impl rule__Login__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Login__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Login__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__2"


    // $ANTLR start "rule__Login__Group__2__Impl"
    // InternalNetSpec.g:3608:1: rule__Login__Group__2__Impl : ( ( rule__Login__NameAssignment_2 ) ) ;
    public final void rule__Login__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3612:1: ( ( ( rule__Login__NameAssignment_2 ) ) )
            // InternalNetSpec.g:3613:1: ( ( rule__Login__NameAssignment_2 ) )
            {
            // InternalNetSpec.g:3613:1: ( ( rule__Login__NameAssignment_2 ) )
            // InternalNetSpec.g:3614:2: ( rule__Login__NameAssignment_2 )
            {
             before(grammarAccess.getLoginAccess().getNameAssignment_2()); 
            // InternalNetSpec.g:3615:2: ( rule__Login__NameAssignment_2 )
            // InternalNetSpec.g:3615:3: rule__Login__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Login__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getLoginAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__2__Impl"


    // $ANTLR start "rule__Login__Group__3"
    // InternalNetSpec.g:3623:1: rule__Login__Group__3 : rule__Login__Group__3__Impl rule__Login__Group__4 ;
    public final void rule__Login__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3627:1: ( rule__Login__Group__3__Impl rule__Login__Group__4 )
            // InternalNetSpec.g:3628:2: rule__Login__Group__3__Impl rule__Login__Group__4
            {
            pushFollow(FOLLOW_22);
            rule__Login__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Login__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__3"


    // $ANTLR start "rule__Login__Group__3__Impl"
    // InternalNetSpec.g:3635:1: rule__Login__Group__3__Impl : ( '{' ) ;
    public final void rule__Login__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3639:1: ( ( '{' ) )
            // InternalNetSpec.g:3640:1: ( '{' )
            {
            // InternalNetSpec.g:3640:1: ( '{' )
            // InternalNetSpec.g:3641:2: '{'
            {
             before(grammarAccess.getLoginAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getLoginAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__3__Impl"


    // $ANTLR start "rule__Login__Group__4"
    // InternalNetSpec.g:3650:1: rule__Login__Group__4 : rule__Login__Group__4__Impl rule__Login__Group__5 ;
    public final void rule__Login__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3654:1: ( rule__Login__Group__4__Impl rule__Login__Group__5 )
            // InternalNetSpec.g:3655:2: rule__Login__Group__4__Impl rule__Login__Group__5
            {
            pushFollow(FOLLOW_28);
            rule__Login__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Login__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__4"


    // $ANTLR start "rule__Login__Group__4__Impl"
    // InternalNetSpec.g:3662:1: rule__Login__Group__4__Impl : ( 'Level' ) ;
    public final void rule__Login__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3666:1: ( ( 'Level' ) )
            // InternalNetSpec.g:3667:1: ( 'Level' )
            {
            // InternalNetSpec.g:3667:1: ( 'Level' )
            // InternalNetSpec.g:3668:2: 'Level'
            {
             before(grammarAccess.getLoginAccess().getLevelKeyword_4()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getLoginAccess().getLevelKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__4__Impl"


    // $ANTLR start "rule__Login__Group__5"
    // InternalNetSpec.g:3677:1: rule__Login__Group__5 : rule__Login__Group__5__Impl rule__Login__Group__6 ;
    public final void rule__Login__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3681:1: ( rule__Login__Group__5__Impl rule__Login__Group__6 )
            // InternalNetSpec.g:3682:2: rule__Login__Group__5__Impl rule__Login__Group__6
            {
            pushFollow(FOLLOW_24);
            rule__Login__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Login__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__5"


    // $ANTLR start "rule__Login__Group__5__Impl"
    // InternalNetSpec.g:3689:1: rule__Login__Group__5__Impl : ( ( rule__Login__LevelAssignment_5 ) ) ;
    public final void rule__Login__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3693:1: ( ( ( rule__Login__LevelAssignment_5 ) ) )
            // InternalNetSpec.g:3694:1: ( ( rule__Login__LevelAssignment_5 ) )
            {
            // InternalNetSpec.g:3694:1: ( ( rule__Login__LevelAssignment_5 ) )
            // InternalNetSpec.g:3695:2: ( rule__Login__LevelAssignment_5 )
            {
             before(grammarAccess.getLoginAccess().getLevelAssignment_5()); 
            // InternalNetSpec.g:3696:2: ( rule__Login__LevelAssignment_5 )
            // InternalNetSpec.g:3696:3: rule__Login__LevelAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Login__LevelAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getLoginAccess().getLevelAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__5__Impl"


    // $ANTLR start "rule__Login__Group__6"
    // InternalNetSpec.g:3704:1: rule__Login__Group__6 : rule__Login__Group__6__Impl ;
    public final void rule__Login__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3708:1: ( rule__Login__Group__6__Impl )
            // InternalNetSpec.g:3709:2: rule__Login__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Login__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__6"


    // $ANTLR start "rule__Login__Group__6__Impl"
    // InternalNetSpec.g:3715:1: rule__Login__Group__6__Impl : ( '}' ) ;
    public final void rule__Login__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3719:1: ( ( '}' ) )
            // InternalNetSpec.g:3720:1: ( '}' )
            {
            // InternalNetSpec.g:3720:1: ( '}' )
            // InternalNetSpec.g:3721:2: '}'
            {
             before(grammarAccess.getLoginAccess().getRightCurlyBracketKeyword_6()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getLoginAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__Group__6__Impl"


    // $ANTLR start "rule__Key__Group__0"
    // InternalNetSpec.g:3731:1: rule__Key__Group__0 : rule__Key__Group__0__Impl rule__Key__Group__1 ;
    public final void rule__Key__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3735:1: ( rule__Key__Group__0__Impl rule__Key__Group__1 )
            // InternalNetSpec.g:3736:2: rule__Key__Group__0__Impl rule__Key__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__Key__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Key__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__0"


    // $ANTLR start "rule__Key__Group__0__Impl"
    // InternalNetSpec.g:3743:1: rule__Key__Group__0__Impl : ( () ) ;
    public final void rule__Key__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3747:1: ( ( () ) )
            // InternalNetSpec.g:3748:1: ( () )
            {
            // InternalNetSpec.g:3748:1: ( () )
            // InternalNetSpec.g:3749:2: ()
            {
             before(grammarAccess.getKeyAccess().getKeyAction_0()); 
            // InternalNetSpec.g:3750:2: ()
            // InternalNetSpec.g:3750:3: 
            {
            }

             after(grammarAccess.getKeyAccess().getKeyAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__0__Impl"


    // $ANTLR start "rule__Key__Group__1"
    // InternalNetSpec.g:3758:1: rule__Key__Group__1 : rule__Key__Group__1__Impl rule__Key__Group__2 ;
    public final void rule__Key__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3762:1: ( rule__Key__Group__1__Impl rule__Key__Group__2 )
            // InternalNetSpec.g:3763:2: rule__Key__Group__1__Impl rule__Key__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Key__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Key__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__1"


    // $ANTLR start "rule__Key__Group__1__Impl"
    // InternalNetSpec.g:3770:1: rule__Key__Group__1__Impl : ( 'Key' ) ;
    public final void rule__Key__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3774:1: ( ( 'Key' ) )
            // InternalNetSpec.g:3775:1: ( 'Key' )
            {
            // InternalNetSpec.g:3775:1: ( 'Key' )
            // InternalNetSpec.g:3776:2: 'Key'
            {
             before(grammarAccess.getKeyAccess().getKeyKeyword_1()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getKeyAccess().getKeyKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__1__Impl"


    // $ANTLR start "rule__Key__Group__2"
    // InternalNetSpec.g:3785:1: rule__Key__Group__2 : rule__Key__Group__2__Impl rule__Key__Group__3 ;
    public final void rule__Key__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3789:1: ( rule__Key__Group__2__Impl rule__Key__Group__3 )
            // InternalNetSpec.g:3790:2: rule__Key__Group__2__Impl rule__Key__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Key__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Key__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__2"


    // $ANTLR start "rule__Key__Group__2__Impl"
    // InternalNetSpec.g:3797:1: rule__Key__Group__2__Impl : ( ( rule__Key__NameAssignment_2 ) ) ;
    public final void rule__Key__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3801:1: ( ( ( rule__Key__NameAssignment_2 ) ) )
            // InternalNetSpec.g:3802:1: ( ( rule__Key__NameAssignment_2 ) )
            {
            // InternalNetSpec.g:3802:1: ( ( rule__Key__NameAssignment_2 ) )
            // InternalNetSpec.g:3803:2: ( rule__Key__NameAssignment_2 )
            {
             before(grammarAccess.getKeyAccess().getNameAssignment_2()); 
            // InternalNetSpec.g:3804:2: ( rule__Key__NameAssignment_2 )
            // InternalNetSpec.g:3804:3: rule__Key__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Key__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getKeyAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__2__Impl"


    // $ANTLR start "rule__Key__Group__3"
    // InternalNetSpec.g:3812:1: rule__Key__Group__3 : rule__Key__Group__3__Impl rule__Key__Group__4 ;
    public final void rule__Key__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3816:1: ( rule__Key__Group__3__Impl rule__Key__Group__4 )
            // InternalNetSpec.g:3817:2: rule__Key__Group__3__Impl rule__Key__Group__4
            {
            pushFollow(FOLLOW_22);
            rule__Key__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Key__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__3"


    // $ANTLR start "rule__Key__Group__3__Impl"
    // InternalNetSpec.g:3824:1: rule__Key__Group__3__Impl : ( '{' ) ;
    public final void rule__Key__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3828:1: ( ( '{' ) )
            // InternalNetSpec.g:3829:1: ( '{' )
            {
            // InternalNetSpec.g:3829:1: ( '{' )
            // InternalNetSpec.g:3830:2: '{'
            {
             before(grammarAccess.getKeyAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getKeyAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__3__Impl"


    // $ANTLR start "rule__Key__Group__4"
    // InternalNetSpec.g:3839:1: rule__Key__Group__4 : rule__Key__Group__4__Impl rule__Key__Group__5 ;
    public final void rule__Key__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3843:1: ( rule__Key__Group__4__Impl rule__Key__Group__5 )
            // InternalNetSpec.g:3844:2: rule__Key__Group__4__Impl rule__Key__Group__5
            {
            pushFollow(FOLLOW_28);
            rule__Key__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Key__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__4"


    // $ANTLR start "rule__Key__Group__4__Impl"
    // InternalNetSpec.g:3851:1: rule__Key__Group__4__Impl : ( 'Level' ) ;
    public final void rule__Key__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3855:1: ( ( 'Level' ) )
            // InternalNetSpec.g:3856:1: ( 'Level' )
            {
            // InternalNetSpec.g:3856:1: ( 'Level' )
            // InternalNetSpec.g:3857:2: 'Level'
            {
             before(grammarAccess.getKeyAccess().getLevelKeyword_4()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getKeyAccess().getLevelKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__4__Impl"


    // $ANTLR start "rule__Key__Group__5"
    // InternalNetSpec.g:3866:1: rule__Key__Group__5 : rule__Key__Group__5__Impl rule__Key__Group__6 ;
    public final void rule__Key__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3870:1: ( rule__Key__Group__5__Impl rule__Key__Group__6 )
            // InternalNetSpec.g:3871:2: rule__Key__Group__5__Impl rule__Key__Group__6
            {
            pushFollow(FOLLOW_24);
            rule__Key__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Key__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__5"


    // $ANTLR start "rule__Key__Group__5__Impl"
    // InternalNetSpec.g:3878:1: rule__Key__Group__5__Impl : ( ( rule__Key__LevelAssignment_5 ) ) ;
    public final void rule__Key__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3882:1: ( ( ( rule__Key__LevelAssignment_5 ) ) )
            // InternalNetSpec.g:3883:1: ( ( rule__Key__LevelAssignment_5 ) )
            {
            // InternalNetSpec.g:3883:1: ( ( rule__Key__LevelAssignment_5 ) )
            // InternalNetSpec.g:3884:2: ( rule__Key__LevelAssignment_5 )
            {
             before(grammarAccess.getKeyAccess().getLevelAssignment_5()); 
            // InternalNetSpec.g:3885:2: ( rule__Key__LevelAssignment_5 )
            // InternalNetSpec.g:3885:3: rule__Key__LevelAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Key__LevelAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getKeyAccess().getLevelAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__5__Impl"


    // $ANTLR start "rule__Key__Group__6"
    // InternalNetSpec.g:3893:1: rule__Key__Group__6 : rule__Key__Group__6__Impl ;
    public final void rule__Key__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3897:1: ( rule__Key__Group__6__Impl )
            // InternalNetSpec.g:3898:2: rule__Key__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Key__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__6"


    // $ANTLR start "rule__Key__Group__6__Impl"
    // InternalNetSpec.g:3904:1: rule__Key__Group__6__Impl : ( '}' ) ;
    public final void rule__Key__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3908:1: ( ( '}' ) )
            // InternalNetSpec.g:3909:1: ( '}' )
            {
            // InternalNetSpec.g:3909:1: ( '}' )
            // InternalNetSpec.g:3910:2: '}'
            {
             before(grammarAccess.getKeyAccess().getRightCurlyBracketKeyword_6()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getKeyAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__Group__6__Impl"


    // $ANTLR start "rule__Firewall__Group__0"
    // InternalNetSpec.g:3920:1: rule__Firewall__Group__0 : rule__Firewall__Group__0__Impl rule__Firewall__Group__1 ;
    public final void rule__Firewall__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3924:1: ( rule__Firewall__Group__0__Impl rule__Firewall__Group__1 )
            // InternalNetSpec.g:3925:2: rule__Firewall__Group__0__Impl rule__Firewall__Group__1
            {
            pushFollow(FOLLOW_30);
            rule__Firewall__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Firewall__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__0"


    // $ANTLR start "rule__Firewall__Group__0__Impl"
    // InternalNetSpec.g:3932:1: rule__Firewall__Group__0__Impl : ( () ) ;
    public final void rule__Firewall__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3936:1: ( ( () ) )
            // InternalNetSpec.g:3937:1: ( () )
            {
            // InternalNetSpec.g:3937:1: ( () )
            // InternalNetSpec.g:3938:2: ()
            {
             before(grammarAccess.getFirewallAccess().getFirewallAction_0()); 
            // InternalNetSpec.g:3939:2: ()
            // InternalNetSpec.g:3939:3: 
            {
            }

             after(grammarAccess.getFirewallAccess().getFirewallAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__0__Impl"


    // $ANTLR start "rule__Firewall__Group__1"
    // InternalNetSpec.g:3947:1: rule__Firewall__Group__1 : rule__Firewall__Group__1__Impl rule__Firewall__Group__2 ;
    public final void rule__Firewall__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3951:1: ( rule__Firewall__Group__1__Impl rule__Firewall__Group__2 )
            // InternalNetSpec.g:3952:2: rule__Firewall__Group__1__Impl rule__Firewall__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Firewall__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Firewall__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__1"


    // $ANTLR start "rule__Firewall__Group__1__Impl"
    // InternalNetSpec.g:3959:1: rule__Firewall__Group__1__Impl : ( 'Firewall' ) ;
    public final void rule__Firewall__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3963:1: ( ( 'Firewall' ) )
            // InternalNetSpec.g:3964:1: ( 'Firewall' )
            {
            // InternalNetSpec.g:3964:1: ( 'Firewall' )
            // InternalNetSpec.g:3965:2: 'Firewall'
            {
             before(grammarAccess.getFirewallAccess().getFirewallKeyword_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getFirewallAccess().getFirewallKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__1__Impl"


    // $ANTLR start "rule__Firewall__Group__2"
    // InternalNetSpec.g:3974:1: rule__Firewall__Group__2 : rule__Firewall__Group__2__Impl rule__Firewall__Group__3 ;
    public final void rule__Firewall__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3978:1: ( rule__Firewall__Group__2__Impl rule__Firewall__Group__3 )
            // InternalNetSpec.g:3979:2: rule__Firewall__Group__2__Impl rule__Firewall__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Firewall__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Firewall__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__2"


    // $ANTLR start "rule__Firewall__Group__2__Impl"
    // InternalNetSpec.g:3986:1: rule__Firewall__Group__2__Impl : ( ( rule__Firewall__NameAssignment_2 ) ) ;
    public final void rule__Firewall__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:3990:1: ( ( ( rule__Firewall__NameAssignment_2 ) ) )
            // InternalNetSpec.g:3991:1: ( ( rule__Firewall__NameAssignment_2 ) )
            {
            // InternalNetSpec.g:3991:1: ( ( rule__Firewall__NameAssignment_2 ) )
            // InternalNetSpec.g:3992:2: ( rule__Firewall__NameAssignment_2 )
            {
             before(grammarAccess.getFirewallAccess().getNameAssignment_2()); 
            // InternalNetSpec.g:3993:2: ( rule__Firewall__NameAssignment_2 )
            // InternalNetSpec.g:3993:3: rule__Firewall__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Firewall__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFirewallAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__2__Impl"


    // $ANTLR start "rule__Firewall__Group__3"
    // InternalNetSpec.g:4001:1: rule__Firewall__Group__3 : rule__Firewall__Group__3__Impl rule__Firewall__Group__4 ;
    public final void rule__Firewall__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4005:1: ( rule__Firewall__Group__3__Impl rule__Firewall__Group__4 )
            // InternalNetSpec.g:4006:2: rule__Firewall__Group__3__Impl rule__Firewall__Group__4
            {
            pushFollow(FOLLOW_22);
            rule__Firewall__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Firewall__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__3"


    // $ANTLR start "rule__Firewall__Group__3__Impl"
    // InternalNetSpec.g:4013:1: rule__Firewall__Group__3__Impl : ( '{' ) ;
    public final void rule__Firewall__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4017:1: ( ( '{' ) )
            // InternalNetSpec.g:4018:1: ( '{' )
            {
            // InternalNetSpec.g:4018:1: ( '{' )
            // InternalNetSpec.g:4019:2: '{'
            {
             before(grammarAccess.getFirewallAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getFirewallAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__3__Impl"


    // $ANTLR start "rule__Firewall__Group__4"
    // InternalNetSpec.g:4028:1: rule__Firewall__Group__4 : rule__Firewall__Group__4__Impl rule__Firewall__Group__5 ;
    public final void rule__Firewall__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4032:1: ( rule__Firewall__Group__4__Impl rule__Firewall__Group__5 )
            // InternalNetSpec.g:4033:2: rule__Firewall__Group__4__Impl rule__Firewall__Group__5
            {
            pushFollow(FOLLOW_23);
            rule__Firewall__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Firewall__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__4"


    // $ANTLR start "rule__Firewall__Group__4__Impl"
    // InternalNetSpec.g:4040:1: rule__Firewall__Group__4__Impl : ( 'Level' ) ;
    public final void rule__Firewall__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4044:1: ( ( 'Level' ) )
            // InternalNetSpec.g:4045:1: ( 'Level' )
            {
            // InternalNetSpec.g:4045:1: ( 'Level' )
            // InternalNetSpec.g:4046:2: 'Level'
            {
             before(grammarAccess.getFirewallAccess().getLevelKeyword_4()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getFirewallAccess().getLevelKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__4__Impl"


    // $ANTLR start "rule__Firewall__Group__5"
    // InternalNetSpec.g:4055:1: rule__Firewall__Group__5 : rule__Firewall__Group__5__Impl rule__Firewall__Group__6 ;
    public final void rule__Firewall__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4059:1: ( rule__Firewall__Group__5__Impl rule__Firewall__Group__6 )
            // InternalNetSpec.g:4060:2: rule__Firewall__Group__5__Impl rule__Firewall__Group__6
            {
            pushFollow(FOLLOW_24);
            rule__Firewall__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Firewall__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__5"


    // $ANTLR start "rule__Firewall__Group__5__Impl"
    // InternalNetSpec.g:4067:1: rule__Firewall__Group__5__Impl : ( ( rule__Firewall__LevelAssignment_5 ) ) ;
    public final void rule__Firewall__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4071:1: ( ( ( rule__Firewall__LevelAssignment_5 ) ) )
            // InternalNetSpec.g:4072:1: ( ( rule__Firewall__LevelAssignment_5 ) )
            {
            // InternalNetSpec.g:4072:1: ( ( rule__Firewall__LevelAssignment_5 ) )
            // InternalNetSpec.g:4073:2: ( rule__Firewall__LevelAssignment_5 )
            {
             before(grammarAccess.getFirewallAccess().getLevelAssignment_5()); 
            // InternalNetSpec.g:4074:2: ( rule__Firewall__LevelAssignment_5 )
            // InternalNetSpec.g:4074:3: rule__Firewall__LevelAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Firewall__LevelAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getFirewallAccess().getLevelAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__5__Impl"


    // $ANTLR start "rule__Firewall__Group__6"
    // InternalNetSpec.g:4082:1: rule__Firewall__Group__6 : rule__Firewall__Group__6__Impl ;
    public final void rule__Firewall__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4086:1: ( rule__Firewall__Group__6__Impl )
            // InternalNetSpec.g:4087:2: rule__Firewall__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Firewall__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__6"


    // $ANTLR start "rule__Firewall__Group__6__Impl"
    // InternalNetSpec.g:4093:1: rule__Firewall__Group__6__Impl : ( '}' ) ;
    public final void rule__Firewall__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4097:1: ( ( '}' ) )
            // InternalNetSpec.g:4098:1: ( '}' )
            {
            // InternalNetSpec.g:4098:1: ( '}' )
            // InternalNetSpec.g:4099:2: '}'
            {
             before(grammarAccess.getFirewallAccess().getRightCurlyBracketKeyword_6()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getFirewallAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__Group__6__Impl"


    // $ANTLR start "rule__Ids__Group__0"
    // InternalNetSpec.g:4109:1: rule__Ids__Group__0 : rule__Ids__Group__0__Impl rule__Ids__Group__1 ;
    public final void rule__Ids__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4113:1: ( rule__Ids__Group__0__Impl rule__Ids__Group__1 )
            // InternalNetSpec.g:4114:2: rule__Ids__Group__0__Impl rule__Ids__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__Ids__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ids__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__0"


    // $ANTLR start "rule__Ids__Group__0__Impl"
    // InternalNetSpec.g:4121:1: rule__Ids__Group__0__Impl : ( () ) ;
    public final void rule__Ids__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4125:1: ( ( () ) )
            // InternalNetSpec.g:4126:1: ( () )
            {
            // InternalNetSpec.g:4126:1: ( () )
            // InternalNetSpec.g:4127:2: ()
            {
             before(grammarAccess.getIdsAccess().getIdsAction_0()); 
            // InternalNetSpec.g:4128:2: ()
            // InternalNetSpec.g:4128:3: 
            {
            }

             after(grammarAccess.getIdsAccess().getIdsAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__0__Impl"


    // $ANTLR start "rule__Ids__Group__1"
    // InternalNetSpec.g:4136:1: rule__Ids__Group__1 : rule__Ids__Group__1__Impl rule__Ids__Group__2 ;
    public final void rule__Ids__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4140:1: ( rule__Ids__Group__1__Impl rule__Ids__Group__2 )
            // InternalNetSpec.g:4141:2: rule__Ids__Group__1__Impl rule__Ids__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Ids__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ids__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__1"


    // $ANTLR start "rule__Ids__Group__1__Impl"
    // InternalNetSpec.g:4148:1: rule__Ids__Group__1__Impl : ( 'IDS' ) ;
    public final void rule__Ids__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4152:1: ( ( 'IDS' ) )
            // InternalNetSpec.g:4153:1: ( 'IDS' )
            {
            // InternalNetSpec.g:4153:1: ( 'IDS' )
            // InternalNetSpec.g:4154:2: 'IDS'
            {
             before(grammarAccess.getIdsAccess().getIDSKeyword_1()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getIdsAccess().getIDSKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__1__Impl"


    // $ANTLR start "rule__Ids__Group__2"
    // InternalNetSpec.g:4163:1: rule__Ids__Group__2 : rule__Ids__Group__2__Impl rule__Ids__Group__3 ;
    public final void rule__Ids__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4167:1: ( rule__Ids__Group__2__Impl rule__Ids__Group__3 )
            // InternalNetSpec.g:4168:2: rule__Ids__Group__2__Impl rule__Ids__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Ids__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ids__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__2"


    // $ANTLR start "rule__Ids__Group__2__Impl"
    // InternalNetSpec.g:4175:1: rule__Ids__Group__2__Impl : ( ( rule__Ids__NameAssignment_2 ) ) ;
    public final void rule__Ids__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4179:1: ( ( ( rule__Ids__NameAssignment_2 ) ) )
            // InternalNetSpec.g:4180:1: ( ( rule__Ids__NameAssignment_2 ) )
            {
            // InternalNetSpec.g:4180:1: ( ( rule__Ids__NameAssignment_2 ) )
            // InternalNetSpec.g:4181:2: ( rule__Ids__NameAssignment_2 )
            {
             before(grammarAccess.getIdsAccess().getNameAssignment_2()); 
            // InternalNetSpec.g:4182:2: ( rule__Ids__NameAssignment_2 )
            // InternalNetSpec.g:4182:3: rule__Ids__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Ids__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getIdsAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__2__Impl"


    // $ANTLR start "rule__Ids__Group__3"
    // InternalNetSpec.g:4190:1: rule__Ids__Group__3 : rule__Ids__Group__3__Impl rule__Ids__Group__4 ;
    public final void rule__Ids__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4194:1: ( rule__Ids__Group__3__Impl rule__Ids__Group__4 )
            // InternalNetSpec.g:4195:2: rule__Ids__Group__3__Impl rule__Ids__Group__4
            {
            pushFollow(FOLLOW_32);
            rule__Ids__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ids__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__3"


    // $ANTLR start "rule__Ids__Group__3__Impl"
    // InternalNetSpec.g:4202:1: rule__Ids__Group__3__Impl : ( '{' ) ;
    public final void rule__Ids__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4206:1: ( ( '{' ) )
            // InternalNetSpec.g:4207:1: ( '{' )
            {
            // InternalNetSpec.g:4207:1: ( '{' )
            // InternalNetSpec.g:4208:2: '{'
            {
             before(grammarAccess.getIdsAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getIdsAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__3__Impl"


    // $ANTLR start "rule__Ids__Group__4"
    // InternalNetSpec.g:4217:1: rule__Ids__Group__4 : rule__Ids__Group__4__Impl rule__Ids__Group__5 ;
    public final void rule__Ids__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4221:1: ( rule__Ids__Group__4__Impl rule__Ids__Group__5 )
            // InternalNetSpec.g:4222:2: rule__Ids__Group__4__Impl rule__Ids__Group__5
            {
            pushFollow(FOLLOW_32);
            rule__Ids__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ids__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__4"


    // $ANTLR start "rule__Ids__Group__4__Impl"
    // InternalNetSpec.g:4229:1: rule__Ids__Group__4__Impl : ( ( rule__Ids__ActiveAssignment_4 )? ) ;
    public final void rule__Ids__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4233:1: ( ( ( rule__Ids__ActiveAssignment_4 )? ) )
            // InternalNetSpec.g:4234:1: ( ( rule__Ids__ActiveAssignment_4 )? )
            {
            // InternalNetSpec.g:4234:1: ( ( rule__Ids__ActiveAssignment_4 )? )
            // InternalNetSpec.g:4235:2: ( rule__Ids__ActiveAssignment_4 )?
            {
             before(grammarAccess.getIdsAccess().getActiveAssignment_4()); 
            // InternalNetSpec.g:4236:2: ( rule__Ids__ActiveAssignment_4 )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==48) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalNetSpec.g:4236:3: rule__Ids__ActiveAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Ids__ActiveAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getIdsAccess().getActiveAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__4__Impl"


    // $ANTLR start "rule__Ids__Group__5"
    // InternalNetSpec.g:4244:1: rule__Ids__Group__5 : rule__Ids__Group__5__Impl rule__Ids__Group__6 ;
    public final void rule__Ids__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4248:1: ( rule__Ids__Group__5__Impl rule__Ids__Group__6 )
            // InternalNetSpec.g:4249:2: rule__Ids__Group__5__Impl rule__Ids__Group__6
            {
            pushFollow(FOLLOW_23);
            rule__Ids__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ids__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__5"


    // $ANTLR start "rule__Ids__Group__5__Impl"
    // InternalNetSpec.g:4256:1: rule__Ids__Group__5__Impl : ( 'Level' ) ;
    public final void rule__Ids__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4260:1: ( ( 'Level' ) )
            // InternalNetSpec.g:4261:1: ( 'Level' )
            {
            // InternalNetSpec.g:4261:1: ( 'Level' )
            // InternalNetSpec.g:4262:2: 'Level'
            {
             before(grammarAccess.getIdsAccess().getLevelKeyword_5()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getIdsAccess().getLevelKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__5__Impl"


    // $ANTLR start "rule__Ids__Group__6"
    // InternalNetSpec.g:4271:1: rule__Ids__Group__6 : rule__Ids__Group__6__Impl rule__Ids__Group__7 ;
    public final void rule__Ids__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4275:1: ( rule__Ids__Group__6__Impl rule__Ids__Group__7 )
            // InternalNetSpec.g:4276:2: rule__Ids__Group__6__Impl rule__Ids__Group__7
            {
            pushFollow(FOLLOW_24);
            rule__Ids__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ids__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__6"


    // $ANTLR start "rule__Ids__Group__6__Impl"
    // InternalNetSpec.g:4283:1: rule__Ids__Group__6__Impl : ( ( rule__Ids__LevelAssignment_6 ) ) ;
    public final void rule__Ids__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4287:1: ( ( ( rule__Ids__LevelAssignment_6 ) ) )
            // InternalNetSpec.g:4288:1: ( ( rule__Ids__LevelAssignment_6 ) )
            {
            // InternalNetSpec.g:4288:1: ( ( rule__Ids__LevelAssignment_6 ) )
            // InternalNetSpec.g:4289:2: ( rule__Ids__LevelAssignment_6 )
            {
             before(grammarAccess.getIdsAccess().getLevelAssignment_6()); 
            // InternalNetSpec.g:4290:2: ( rule__Ids__LevelAssignment_6 )
            // InternalNetSpec.g:4290:3: rule__Ids__LevelAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Ids__LevelAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getIdsAccess().getLevelAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__6__Impl"


    // $ANTLR start "rule__Ids__Group__7"
    // InternalNetSpec.g:4298:1: rule__Ids__Group__7 : rule__Ids__Group__7__Impl ;
    public final void rule__Ids__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4302:1: ( rule__Ids__Group__7__Impl )
            // InternalNetSpec.g:4303:2: rule__Ids__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Ids__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__7"


    // $ANTLR start "rule__Ids__Group__7__Impl"
    // InternalNetSpec.g:4309:1: rule__Ids__Group__7__Impl : ( '}' ) ;
    public final void rule__Ids__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4313:1: ( ( '}' ) )
            // InternalNetSpec.g:4314:1: ( '}' )
            {
            // InternalNetSpec.g:4314:1: ( '}' )
            // InternalNetSpec.g:4315:2: '}'
            {
             before(grammarAccess.getIdsAccess().getRightCurlyBracketKeyword_7()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getIdsAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__Group__7__Impl"


    // $ANTLR start "rule__Link__Group__0"
    // InternalNetSpec.g:4325:1: rule__Link__Group__0 : rule__Link__Group__0__Impl rule__Link__Group__1 ;
    public final void rule__Link__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4329:1: ( rule__Link__Group__0__Impl rule__Link__Group__1 )
            // InternalNetSpec.g:4330:2: rule__Link__Group__0__Impl rule__Link__Group__1
            {
            pushFollow(FOLLOW_33);
            rule__Link__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__0"


    // $ANTLR start "rule__Link__Group__0__Impl"
    // InternalNetSpec.g:4337:1: rule__Link__Group__0__Impl : ( () ) ;
    public final void rule__Link__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4341:1: ( ( () ) )
            // InternalNetSpec.g:4342:1: ( () )
            {
            // InternalNetSpec.g:4342:1: ( () )
            // InternalNetSpec.g:4343:2: ()
            {
             before(grammarAccess.getLinkAccess().getLinkAction_0()); 
            // InternalNetSpec.g:4344:2: ()
            // InternalNetSpec.g:4344:3: 
            {
            }

             after(grammarAccess.getLinkAccess().getLinkAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__0__Impl"


    // $ANTLR start "rule__Link__Group__1"
    // InternalNetSpec.g:4352:1: rule__Link__Group__1 : rule__Link__Group__1__Impl rule__Link__Group__2 ;
    public final void rule__Link__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4356:1: ( rule__Link__Group__1__Impl rule__Link__Group__2 )
            // InternalNetSpec.g:4357:2: rule__Link__Group__1__Impl rule__Link__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Link__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__1"


    // $ANTLR start "rule__Link__Group__1__Impl"
    // InternalNetSpec.g:4364:1: rule__Link__Group__1__Impl : ( 'Link' ) ;
    public final void rule__Link__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4368:1: ( ( 'Link' ) )
            // InternalNetSpec.g:4369:1: ( 'Link' )
            {
            // InternalNetSpec.g:4369:1: ( 'Link' )
            // InternalNetSpec.g:4370:2: 'Link'
            {
             before(grammarAccess.getLinkAccess().getLinkKeyword_1()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getLinkKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__1__Impl"


    // $ANTLR start "rule__Link__Group__2"
    // InternalNetSpec.g:4379:1: rule__Link__Group__2 : rule__Link__Group__2__Impl rule__Link__Group__3 ;
    public final void rule__Link__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4383:1: ( rule__Link__Group__2__Impl rule__Link__Group__3 )
            // InternalNetSpec.g:4384:2: rule__Link__Group__2__Impl rule__Link__Group__3
            {
            pushFollow(FOLLOW_34);
            rule__Link__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__2"


    // $ANTLR start "rule__Link__Group__2__Impl"
    // InternalNetSpec.g:4391:1: rule__Link__Group__2__Impl : ( '{' ) ;
    public final void rule__Link__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4395:1: ( ( '{' ) )
            // InternalNetSpec.g:4396:1: ( '{' )
            {
            // InternalNetSpec.g:4396:1: ( '{' )
            // InternalNetSpec.g:4397:2: '{'
            {
             before(grammarAccess.getLinkAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__2__Impl"


    // $ANTLR start "rule__Link__Group__3"
    // InternalNetSpec.g:4406:1: rule__Link__Group__3 : rule__Link__Group__3__Impl rule__Link__Group__4 ;
    public final void rule__Link__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4410:1: ( rule__Link__Group__3__Impl rule__Link__Group__4 )
            // InternalNetSpec.g:4411:2: rule__Link__Group__3__Impl rule__Link__Group__4
            {
            pushFollow(FOLLOW_14);
            rule__Link__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__3"


    // $ANTLR start "rule__Link__Group__3__Impl"
    // InternalNetSpec.g:4418:1: rule__Link__Group__3__Impl : ( 'H1' ) ;
    public final void rule__Link__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4422:1: ( ( 'H1' ) )
            // InternalNetSpec.g:4423:1: ( 'H1' )
            {
            // InternalNetSpec.g:4423:1: ( 'H1' )
            // InternalNetSpec.g:4424:2: 'H1'
            {
             before(grammarAccess.getLinkAccess().getH1Keyword_3()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getH1Keyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__3__Impl"


    // $ANTLR start "rule__Link__Group__4"
    // InternalNetSpec.g:4433:1: rule__Link__Group__4 : rule__Link__Group__4__Impl rule__Link__Group__5 ;
    public final void rule__Link__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4437:1: ( rule__Link__Group__4__Impl rule__Link__Group__5 )
            // InternalNetSpec.g:4438:2: rule__Link__Group__4__Impl rule__Link__Group__5
            {
            pushFollow(FOLLOW_35);
            rule__Link__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__4"


    // $ANTLR start "rule__Link__Group__4__Impl"
    // InternalNetSpec.g:4445:1: rule__Link__Group__4__Impl : ( ( rule__Link__H1Assignment_4 ) ) ;
    public final void rule__Link__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4449:1: ( ( ( rule__Link__H1Assignment_4 ) ) )
            // InternalNetSpec.g:4450:1: ( ( rule__Link__H1Assignment_4 ) )
            {
            // InternalNetSpec.g:4450:1: ( ( rule__Link__H1Assignment_4 ) )
            // InternalNetSpec.g:4451:2: ( rule__Link__H1Assignment_4 )
            {
             before(grammarAccess.getLinkAccess().getH1Assignment_4()); 
            // InternalNetSpec.g:4452:2: ( rule__Link__H1Assignment_4 )
            // InternalNetSpec.g:4452:3: rule__Link__H1Assignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Link__H1Assignment_4();

            state._fsp--;


            }

             after(grammarAccess.getLinkAccess().getH1Assignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__4__Impl"


    // $ANTLR start "rule__Link__Group__5"
    // InternalNetSpec.g:4460:1: rule__Link__Group__5 : rule__Link__Group__5__Impl rule__Link__Group__6 ;
    public final void rule__Link__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4464:1: ( rule__Link__Group__5__Impl rule__Link__Group__6 )
            // InternalNetSpec.g:4465:2: rule__Link__Group__5__Impl rule__Link__Group__6
            {
            pushFollow(FOLLOW_14);
            rule__Link__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__5"


    // $ANTLR start "rule__Link__Group__5__Impl"
    // InternalNetSpec.g:4472:1: rule__Link__Group__5__Impl : ( 'H2' ) ;
    public final void rule__Link__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4476:1: ( ( 'H2' ) )
            // InternalNetSpec.g:4477:1: ( 'H2' )
            {
            // InternalNetSpec.g:4477:1: ( 'H2' )
            // InternalNetSpec.g:4478:2: 'H2'
            {
             before(grammarAccess.getLinkAccess().getH2Keyword_5()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getH2Keyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__5__Impl"


    // $ANTLR start "rule__Link__Group__6"
    // InternalNetSpec.g:4487:1: rule__Link__Group__6 : rule__Link__Group__6__Impl rule__Link__Group__7 ;
    public final void rule__Link__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4491:1: ( rule__Link__Group__6__Impl rule__Link__Group__7 )
            // InternalNetSpec.g:4492:2: rule__Link__Group__6__Impl rule__Link__Group__7
            {
            pushFollow(FOLLOW_36);
            rule__Link__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__6"


    // $ANTLR start "rule__Link__Group__6__Impl"
    // InternalNetSpec.g:4499:1: rule__Link__Group__6__Impl : ( ( rule__Link__H2Assignment_6 ) ) ;
    public final void rule__Link__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4503:1: ( ( ( rule__Link__H2Assignment_6 ) ) )
            // InternalNetSpec.g:4504:1: ( ( rule__Link__H2Assignment_6 ) )
            {
            // InternalNetSpec.g:4504:1: ( ( rule__Link__H2Assignment_6 ) )
            // InternalNetSpec.g:4505:2: ( rule__Link__H2Assignment_6 )
            {
             before(grammarAccess.getLinkAccess().getH2Assignment_6()); 
            // InternalNetSpec.g:4506:2: ( rule__Link__H2Assignment_6 )
            // InternalNetSpec.g:4506:3: rule__Link__H2Assignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Link__H2Assignment_6();

            state._fsp--;


            }

             after(grammarAccess.getLinkAccess().getH2Assignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__6__Impl"


    // $ANTLR start "rule__Link__Group__7"
    // InternalNetSpec.g:4514:1: rule__Link__Group__7 : rule__Link__Group__7__Impl rule__Link__Group__8 ;
    public final void rule__Link__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4518:1: ( rule__Link__Group__7__Impl rule__Link__Group__8 )
            // InternalNetSpec.g:4519:2: rule__Link__Group__7__Impl rule__Link__Group__8
            {
            pushFollow(FOLLOW_36);
            rule__Link__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__7"


    // $ANTLR start "rule__Link__Group__7__Impl"
    // InternalNetSpec.g:4526:1: rule__Link__Group__7__Impl : ( ( rule__Link__Group_7__0 )? ) ;
    public final void rule__Link__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4530:1: ( ( ( rule__Link__Group_7__0 )? ) )
            // InternalNetSpec.g:4531:1: ( ( rule__Link__Group_7__0 )? )
            {
            // InternalNetSpec.g:4531:1: ( ( rule__Link__Group_7__0 )? )
            // InternalNetSpec.g:4532:2: ( rule__Link__Group_7__0 )?
            {
             before(grammarAccess.getLinkAccess().getGroup_7()); 
            // InternalNetSpec.g:4533:2: ( rule__Link__Group_7__0 )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==33) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalNetSpec.g:4533:3: rule__Link__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Link__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLinkAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__7__Impl"


    // $ANTLR start "rule__Link__Group__8"
    // InternalNetSpec.g:4541:1: rule__Link__Group__8 : rule__Link__Group__8__Impl rule__Link__Group__9 ;
    public final void rule__Link__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4545:1: ( rule__Link__Group__8__Impl rule__Link__Group__9 )
            // InternalNetSpec.g:4546:2: rule__Link__Group__8__Impl rule__Link__Group__9
            {
            pushFollow(FOLLOW_36);
            rule__Link__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__8"


    // $ANTLR start "rule__Link__Group__8__Impl"
    // InternalNetSpec.g:4553:1: rule__Link__Group__8__Impl : ( ( rule__Link__Group_8__0 )? ) ;
    public final void rule__Link__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4557:1: ( ( ( rule__Link__Group_8__0 )? ) )
            // InternalNetSpec.g:4558:1: ( ( rule__Link__Group_8__0 )? )
            {
            // InternalNetSpec.g:4558:1: ( ( rule__Link__Group_8__0 )? )
            // InternalNetSpec.g:4559:2: ( rule__Link__Group_8__0 )?
            {
             before(grammarAccess.getLinkAccess().getGroup_8()); 
            // InternalNetSpec.g:4560:2: ( rule__Link__Group_8__0 )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==34) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalNetSpec.g:4560:3: rule__Link__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Link__Group_8__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLinkAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__8__Impl"


    // $ANTLR start "rule__Link__Group__9"
    // InternalNetSpec.g:4568:1: rule__Link__Group__9 : rule__Link__Group__9__Impl ;
    public final void rule__Link__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4572:1: ( rule__Link__Group__9__Impl )
            // InternalNetSpec.g:4573:2: rule__Link__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Link__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__9"


    // $ANTLR start "rule__Link__Group__9__Impl"
    // InternalNetSpec.g:4579:1: rule__Link__Group__9__Impl : ( '}' ) ;
    public final void rule__Link__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4583:1: ( ( '}' ) )
            // InternalNetSpec.g:4584:1: ( '}' )
            {
            // InternalNetSpec.g:4584:1: ( '}' )
            // InternalNetSpec.g:4585:2: '}'
            {
             before(grammarAccess.getLinkAccess().getRightCurlyBracketKeyword_9()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group__9__Impl"


    // $ANTLR start "rule__Link__Group_7__0"
    // InternalNetSpec.g:4595:1: rule__Link__Group_7__0 : rule__Link__Group_7__0__Impl rule__Link__Group_7__1 ;
    public final void rule__Link__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4599:1: ( rule__Link__Group_7__0__Impl rule__Link__Group_7__1 )
            // InternalNetSpec.g:4600:2: rule__Link__Group_7__0__Impl rule__Link__Group_7__1
            {
            pushFollow(FOLLOW_14);
            rule__Link__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group_7__0"


    // $ANTLR start "rule__Link__Group_7__0__Impl"
    // InternalNetSpec.g:4607:1: rule__Link__Group_7__0__Impl : ( 'IDS' ) ;
    public final void rule__Link__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4611:1: ( ( 'IDS' ) )
            // InternalNetSpec.g:4612:1: ( 'IDS' )
            {
            // InternalNetSpec.g:4612:1: ( 'IDS' )
            // InternalNetSpec.g:4613:2: 'IDS'
            {
             before(grammarAccess.getLinkAccess().getIDSKeyword_7_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getIDSKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group_7__0__Impl"


    // $ANTLR start "rule__Link__Group_7__1"
    // InternalNetSpec.g:4622:1: rule__Link__Group_7__1 : rule__Link__Group_7__1__Impl ;
    public final void rule__Link__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4626:1: ( rule__Link__Group_7__1__Impl )
            // InternalNetSpec.g:4627:2: rule__Link__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Link__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group_7__1"


    // $ANTLR start "rule__Link__Group_7__1__Impl"
    // InternalNetSpec.g:4633:1: rule__Link__Group_7__1__Impl : ( ( rule__Link__IdsAssignment_7_1 ) ) ;
    public final void rule__Link__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4637:1: ( ( ( rule__Link__IdsAssignment_7_1 ) ) )
            // InternalNetSpec.g:4638:1: ( ( rule__Link__IdsAssignment_7_1 ) )
            {
            // InternalNetSpec.g:4638:1: ( ( rule__Link__IdsAssignment_7_1 ) )
            // InternalNetSpec.g:4639:2: ( rule__Link__IdsAssignment_7_1 )
            {
             before(grammarAccess.getLinkAccess().getIdsAssignment_7_1()); 
            // InternalNetSpec.g:4640:2: ( rule__Link__IdsAssignment_7_1 )
            // InternalNetSpec.g:4640:3: rule__Link__IdsAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Link__IdsAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getLinkAccess().getIdsAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group_7__1__Impl"


    // $ANTLR start "rule__Link__Group_8__0"
    // InternalNetSpec.g:4649:1: rule__Link__Group_8__0 : rule__Link__Group_8__0__Impl rule__Link__Group_8__1 ;
    public final void rule__Link__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4653:1: ( rule__Link__Group_8__0__Impl rule__Link__Group_8__1 )
            // InternalNetSpec.g:4654:2: rule__Link__Group_8__0__Impl rule__Link__Group_8__1
            {
            pushFollow(FOLLOW_14);
            rule__Link__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Link__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group_8__0"


    // $ANTLR start "rule__Link__Group_8__0__Impl"
    // InternalNetSpec.g:4661:1: rule__Link__Group_8__0__Impl : ( 'Firewall' ) ;
    public final void rule__Link__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4665:1: ( ( 'Firewall' ) )
            // InternalNetSpec.g:4666:1: ( 'Firewall' )
            {
            // InternalNetSpec.g:4666:1: ( 'Firewall' )
            // InternalNetSpec.g:4667:2: 'Firewall'
            {
             before(grammarAccess.getLinkAccess().getFirewallKeyword_8_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getFirewallKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group_8__0__Impl"


    // $ANTLR start "rule__Link__Group_8__1"
    // InternalNetSpec.g:4676:1: rule__Link__Group_8__1 : rule__Link__Group_8__1__Impl ;
    public final void rule__Link__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4680:1: ( rule__Link__Group_8__1__Impl )
            // InternalNetSpec.g:4681:2: rule__Link__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Link__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group_8__1"


    // $ANTLR start "rule__Link__Group_8__1__Impl"
    // InternalNetSpec.g:4687:1: rule__Link__Group_8__1__Impl : ( ( rule__Link__FirewallAssignment_8_1 ) ) ;
    public final void rule__Link__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4691:1: ( ( ( rule__Link__FirewallAssignment_8_1 ) ) )
            // InternalNetSpec.g:4692:1: ( ( rule__Link__FirewallAssignment_8_1 ) )
            {
            // InternalNetSpec.g:4692:1: ( ( rule__Link__FirewallAssignment_8_1 ) )
            // InternalNetSpec.g:4693:2: ( rule__Link__FirewallAssignment_8_1 )
            {
             before(grammarAccess.getLinkAccess().getFirewallAssignment_8_1()); 
            // InternalNetSpec.g:4694:2: ( rule__Link__FirewallAssignment_8_1 )
            // InternalNetSpec.g:4694:3: rule__Link__FirewallAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__Link__FirewallAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getLinkAccess().getFirewallAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__Group_8__1__Impl"


    // $ANTLR start "rule__Model__AttackerAssignment_0"
    // InternalNetSpec.g:4703:1: rule__Model__AttackerAssignment_0 : ( ruleAttacker ) ;
    public final void rule__Model__AttackerAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4707:1: ( ( ruleAttacker ) )
            // InternalNetSpec.g:4708:2: ( ruleAttacker )
            {
            // InternalNetSpec.g:4708:2: ( ruleAttacker )
            // InternalNetSpec.g:4709:3: ruleAttacker
            {
             before(grammarAccess.getModelAccess().getAttackerAttackerParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleAttacker();

            state._fsp--;

             after(grammarAccess.getModelAccess().getAttackerAttackerParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__AttackerAssignment_0"


    // $ANTLR start "rule__Model__GoalAssignment_1"
    // InternalNetSpec.g:4718:1: rule__Model__GoalAssignment_1 : ( ruleGoal ) ;
    public final void rule__Model__GoalAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4722:1: ( ( ruleGoal ) )
            // InternalNetSpec.g:4723:2: ( ruleGoal )
            {
            // InternalNetSpec.g:4723:2: ( ruleGoal )
            // InternalNetSpec.g:4724:3: ruleGoal
            {
             before(grammarAccess.getModelAccess().getGoalGoalParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleGoal();

            state._fsp--;

             after(grammarAccess.getModelAccess().getGoalGoalParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__GoalAssignment_1"


    // $ANTLR start "rule__Model__HostsAssignment_2_0"
    // InternalNetSpec.g:4733:1: rule__Model__HostsAssignment_2_0 : ( ruleHost ) ;
    public final void rule__Model__HostsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4737:1: ( ( ruleHost ) )
            // InternalNetSpec.g:4738:2: ( ruleHost )
            {
            // InternalNetSpec.g:4738:2: ( ruleHost )
            // InternalNetSpec.g:4739:3: ruleHost
            {
             before(grammarAccess.getModelAccess().getHostsHostParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_2);
            ruleHost();

            state._fsp--;

             after(grammarAccess.getModelAccess().getHostsHostParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__HostsAssignment_2_0"


    // $ANTLR start "rule__Model__SessionsAssignment_2_1"
    // InternalNetSpec.g:4748:1: rule__Model__SessionsAssignment_2_1 : ( ruleSession ) ;
    public final void rule__Model__SessionsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4752:1: ( ( ruleSession ) )
            // InternalNetSpec.g:4753:2: ( ruleSession )
            {
            // InternalNetSpec.g:4753:2: ( ruleSession )
            // InternalNetSpec.g:4754:3: ruleSession
            {
             before(grammarAccess.getModelAccess().getSessionsSessionParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSession();

            state._fsp--;

             after(grammarAccess.getModelAccess().getSessionsSessionParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__SessionsAssignment_2_1"


    // $ANTLR start "rule__Model__FilesAssignment_2_2"
    // InternalNetSpec.g:4763:1: rule__Model__FilesAssignment_2_2 : ( ruleFile ) ;
    public final void rule__Model__FilesAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4767:1: ( ( ruleFile ) )
            // InternalNetSpec.g:4768:2: ( ruleFile )
            {
            // InternalNetSpec.g:4768:2: ( ruleFile )
            // InternalNetSpec.g:4769:3: ruleFile
            {
             before(grammarAccess.getModelAccess().getFilesFileParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_2);
            ruleFile();

            state._fsp--;

             after(grammarAccess.getModelAccess().getFilesFileParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__FilesAssignment_2_2"


    // $ANTLR start "rule__Model__LoginsAssignment_2_3"
    // InternalNetSpec.g:4778:1: rule__Model__LoginsAssignment_2_3 : ( ruleLogin ) ;
    public final void rule__Model__LoginsAssignment_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4782:1: ( ( ruleLogin ) )
            // InternalNetSpec.g:4783:2: ( ruleLogin )
            {
            // InternalNetSpec.g:4783:2: ( ruleLogin )
            // InternalNetSpec.g:4784:3: ruleLogin
            {
             before(grammarAccess.getModelAccess().getLoginsLoginParserRuleCall_2_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogin();

            state._fsp--;

             after(grammarAccess.getModelAccess().getLoginsLoginParserRuleCall_2_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__LoginsAssignment_2_3"


    // $ANTLR start "rule__Model__KeysAssignment_2_4"
    // InternalNetSpec.g:4793:1: rule__Model__KeysAssignment_2_4 : ( ruleKey ) ;
    public final void rule__Model__KeysAssignment_2_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4797:1: ( ( ruleKey ) )
            // InternalNetSpec.g:4798:2: ( ruleKey )
            {
            // InternalNetSpec.g:4798:2: ( ruleKey )
            // InternalNetSpec.g:4799:3: ruleKey
            {
             before(grammarAccess.getModelAccess().getKeysKeyParserRuleCall_2_4_0()); 
            pushFollow(FOLLOW_2);
            ruleKey();

            state._fsp--;

             after(grammarAccess.getModelAccess().getKeysKeyParserRuleCall_2_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__KeysAssignment_2_4"


    // $ANTLR start "rule__Model__FirewallsAssignment_2_5"
    // InternalNetSpec.g:4808:1: rule__Model__FirewallsAssignment_2_5 : ( ruleFirewall ) ;
    public final void rule__Model__FirewallsAssignment_2_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4812:1: ( ( ruleFirewall ) )
            // InternalNetSpec.g:4813:2: ( ruleFirewall )
            {
            // InternalNetSpec.g:4813:2: ( ruleFirewall )
            // InternalNetSpec.g:4814:3: ruleFirewall
            {
             before(grammarAccess.getModelAccess().getFirewallsFirewallParserRuleCall_2_5_0()); 
            pushFollow(FOLLOW_2);
            ruleFirewall();

            state._fsp--;

             after(grammarAccess.getModelAccess().getFirewallsFirewallParserRuleCall_2_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__FirewallsAssignment_2_5"


    // $ANTLR start "rule__Model__IdsAssignment_2_6"
    // InternalNetSpec.g:4823:1: rule__Model__IdsAssignment_2_6 : ( ruleIds ) ;
    public final void rule__Model__IdsAssignment_2_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4827:1: ( ( ruleIds ) )
            // InternalNetSpec.g:4828:2: ( ruleIds )
            {
            // InternalNetSpec.g:4828:2: ( ruleIds )
            // InternalNetSpec.g:4829:3: ruleIds
            {
             before(grammarAccess.getModelAccess().getIdsIdsParserRuleCall_2_6_0()); 
            pushFollow(FOLLOW_2);
            ruleIds();

            state._fsp--;

             after(grammarAccess.getModelAccess().getIdsIdsParserRuleCall_2_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__IdsAssignment_2_6"


    // $ANTLR start "rule__Model__LinksAssignment_2_7"
    // InternalNetSpec.g:4838:1: rule__Model__LinksAssignment_2_7 : ( ruleLink ) ;
    public final void rule__Model__LinksAssignment_2_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4842:1: ( ( ruleLink ) )
            // InternalNetSpec.g:4843:2: ( ruleLink )
            {
            // InternalNetSpec.g:4843:2: ( ruleLink )
            // InternalNetSpec.g:4844:3: ruleLink
            {
             before(grammarAccess.getModelAccess().getLinksLinkParserRuleCall_2_7_0()); 
            pushFollow(FOLLOW_2);
            ruleLink();

            state._fsp--;

             after(grammarAccess.getModelAccess().getLinksLinkParserRuleCall_2_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__LinksAssignment_2_7"


    // $ANTLR start "rule__Model__AntivirusAssignment_2_8"
    // InternalNetSpec.g:4853:1: rule__Model__AntivirusAssignment_2_8 : ( ruleAntivirus ) ;
    public final void rule__Model__AntivirusAssignment_2_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4857:1: ( ( ruleAntivirus ) )
            // InternalNetSpec.g:4858:2: ( ruleAntivirus )
            {
            // InternalNetSpec.g:4858:2: ( ruleAntivirus )
            // InternalNetSpec.g:4859:3: ruleAntivirus
            {
             before(grammarAccess.getModelAccess().getAntivirusAntivirusParserRuleCall_2_8_0()); 
            pushFollow(FOLLOW_2);
            ruleAntivirus();

            state._fsp--;

             after(grammarAccess.getModelAccess().getAntivirusAntivirusParserRuleCall_2_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__AntivirusAssignment_2_8"


    // $ANTLR start "rule__Model__DatasAssignment_2_9"
    // InternalNetSpec.g:4868:1: rule__Model__DatasAssignment_2_9 : ( ruleData ) ;
    public final void rule__Model__DatasAssignment_2_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4872:1: ( ( ruleData ) )
            // InternalNetSpec.g:4873:2: ( ruleData )
            {
            // InternalNetSpec.g:4873:2: ( ruleData )
            // InternalNetSpec.g:4874:3: ruleData
            {
             before(grammarAccess.getModelAccess().getDatasDataParserRuleCall_2_9_0()); 
            pushFollow(FOLLOW_2);
            ruleData();

            state._fsp--;

             after(grammarAccess.getModelAccess().getDatasDataParserRuleCall_2_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__DatasAssignment_2_9"


    // $ANTLR start "rule__Attacker__HackAssignment_4"
    // InternalNetSpec.g:4883:1: rule__Attacker__HackAssignment_4 : ( ( rule__Attacker__HackAlternatives_4_0 ) ) ;
    public final void rule__Attacker__HackAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4887:1: ( ( ( rule__Attacker__HackAlternatives_4_0 ) ) )
            // InternalNetSpec.g:4888:2: ( ( rule__Attacker__HackAlternatives_4_0 ) )
            {
            // InternalNetSpec.g:4888:2: ( ( rule__Attacker__HackAlternatives_4_0 ) )
            // InternalNetSpec.g:4889:3: ( rule__Attacker__HackAlternatives_4_0 )
            {
             before(grammarAccess.getAttackerAccess().getHackAlternatives_4_0()); 
            // InternalNetSpec.g:4890:3: ( rule__Attacker__HackAlternatives_4_0 )
            // InternalNetSpec.g:4890:4: rule__Attacker__HackAlternatives_4_0
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__HackAlternatives_4_0();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getHackAlternatives_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__HackAssignment_4"


    // $ANTLR start "rule__Attacker__CryptoAssignment_6"
    // InternalNetSpec.g:4898:1: rule__Attacker__CryptoAssignment_6 : ( ( rule__Attacker__CryptoAlternatives_6_0 ) ) ;
    public final void rule__Attacker__CryptoAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4902:1: ( ( ( rule__Attacker__CryptoAlternatives_6_0 ) ) )
            // InternalNetSpec.g:4903:2: ( ( rule__Attacker__CryptoAlternatives_6_0 ) )
            {
            // InternalNetSpec.g:4903:2: ( ( rule__Attacker__CryptoAlternatives_6_0 ) )
            // InternalNetSpec.g:4904:3: ( rule__Attacker__CryptoAlternatives_6_0 )
            {
             before(grammarAccess.getAttackerAccess().getCryptoAlternatives_6_0()); 
            // InternalNetSpec.g:4905:3: ( rule__Attacker__CryptoAlternatives_6_0 )
            // InternalNetSpec.g:4905:4: rule__Attacker__CryptoAlternatives_6_0
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__CryptoAlternatives_6_0();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getCryptoAlternatives_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__CryptoAssignment_6"


    // $ANTLR start "rule__Attacker__VirusAssignment_8"
    // InternalNetSpec.g:4913:1: rule__Attacker__VirusAssignment_8 : ( ( rule__Attacker__VirusAlternatives_8_0 ) ) ;
    public final void rule__Attacker__VirusAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4917:1: ( ( ( rule__Attacker__VirusAlternatives_8_0 ) ) )
            // InternalNetSpec.g:4918:2: ( ( rule__Attacker__VirusAlternatives_8_0 ) )
            {
            // InternalNetSpec.g:4918:2: ( ( rule__Attacker__VirusAlternatives_8_0 ) )
            // InternalNetSpec.g:4919:3: ( rule__Attacker__VirusAlternatives_8_0 )
            {
             before(grammarAccess.getAttackerAccess().getVirusAlternatives_8_0()); 
            // InternalNetSpec.g:4920:3: ( rule__Attacker__VirusAlternatives_8_0 )
            // InternalNetSpec.g:4920:4: rule__Attacker__VirusAlternatives_8_0
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__VirusAlternatives_8_0();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getVirusAlternatives_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__VirusAssignment_8"


    // $ANTLR start "rule__Attacker__LoginAssignment_10"
    // InternalNetSpec.g:4928:1: rule__Attacker__LoginAssignment_10 : ( ( rule__Attacker__LoginAlternatives_10_0 ) ) ;
    public final void rule__Attacker__LoginAssignment_10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4932:1: ( ( ( rule__Attacker__LoginAlternatives_10_0 ) ) )
            // InternalNetSpec.g:4933:2: ( ( rule__Attacker__LoginAlternatives_10_0 ) )
            {
            // InternalNetSpec.g:4933:2: ( ( rule__Attacker__LoginAlternatives_10_0 ) )
            // InternalNetSpec.g:4934:3: ( rule__Attacker__LoginAlternatives_10_0 )
            {
             before(grammarAccess.getAttackerAccess().getLoginAlternatives_10_0()); 
            // InternalNetSpec.g:4935:3: ( rule__Attacker__LoginAlternatives_10_0 )
            // InternalNetSpec.g:4935:4: rule__Attacker__LoginAlternatives_10_0
            {
            pushFollow(FOLLOW_2);
            rule__Attacker__LoginAlternatives_10_0();

            state._fsp--;


            }

             after(grammarAccess.getAttackerAccess().getLoginAlternatives_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__LoginAssignment_10"


    // $ANTLR start "rule__Attacker__LoginsKAssignment_11_1"
    // InternalNetSpec.g:4943:1: rule__Attacker__LoginsKAssignment_11_1 : ( ( RULE_ID ) ) ;
    public final void rule__Attacker__LoginsKAssignment_11_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4947:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:4948:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:4948:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:4949:3: ( RULE_ID )
            {
             before(grammarAccess.getAttackerAccess().getLoginsKLoginCrossReference_11_1_0()); 
            // InternalNetSpec.g:4950:3: ( RULE_ID )
            // InternalNetSpec.g:4951:4: RULE_ID
            {
             before(grammarAccess.getAttackerAccess().getLoginsKLoginIDTerminalRuleCall_11_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getLoginsKLoginIDTerminalRuleCall_11_1_0_1()); 

            }

             after(grammarAccess.getAttackerAccess().getLoginsKLoginCrossReference_11_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__LoginsKAssignment_11_1"


    // $ANTLR start "rule__Attacker__KeysKAssignment_12_1"
    // InternalNetSpec.g:4962:1: rule__Attacker__KeysKAssignment_12_1 : ( ( RULE_ID ) ) ;
    public final void rule__Attacker__KeysKAssignment_12_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4966:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:4967:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:4967:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:4968:3: ( RULE_ID )
            {
             before(grammarAccess.getAttackerAccess().getKeysKKeyCrossReference_12_1_0()); 
            // InternalNetSpec.g:4969:3: ( RULE_ID )
            // InternalNetSpec.g:4970:4: RULE_ID
            {
             before(grammarAccess.getAttackerAccess().getKeysKKeyIDTerminalRuleCall_12_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getKeysKKeyIDTerminalRuleCall_12_1_0_1()); 

            }

             after(grammarAccess.getAttackerAccess().getKeysKKeyCrossReference_12_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__KeysKAssignment_12_1"


    // $ANTLR start "rule__Attacker__HostsAssignment_13_1"
    // InternalNetSpec.g:4981:1: rule__Attacker__HostsAssignment_13_1 : ( ( RULE_ID ) ) ;
    public final void rule__Attacker__HostsAssignment_13_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:4985:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:4986:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:4986:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:4987:3: ( RULE_ID )
            {
             before(grammarAccess.getAttackerAccess().getHostsHostCrossReference_13_1_0()); 
            // InternalNetSpec.g:4988:3: ( RULE_ID )
            // InternalNetSpec.g:4989:4: RULE_ID
            {
             before(grammarAccess.getAttackerAccess().getHostsHostIDTerminalRuleCall_13_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttackerAccess().getHostsHostIDTerminalRuleCall_13_1_0_1()); 

            }

             after(grammarAccess.getAttackerAccess().getHostsHostCrossReference_13_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attacker__HostsAssignment_13_1"


    // $ANTLR start "rule__Goal__DatasAssignment_3_1"
    // InternalNetSpec.g:5000:1: rule__Goal__DatasAssignment_3_1 : ( ( RULE_ID ) ) ;
    public final void rule__Goal__DatasAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5004:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5005:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5005:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5006:3: ( RULE_ID )
            {
             before(grammarAccess.getGoalAccess().getDatasDataCrossReference_3_1_0()); 
            // InternalNetSpec.g:5007:3: ( RULE_ID )
            // InternalNetSpec.g:5008:4: RULE_ID
            {
             before(grammarAccess.getGoalAccess().getDatasDataIDTerminalRuleCall_3_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getDatasDataIDTerminalRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getGoalAccess().getDatasDataCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__DatasAssignment_3_1"


    // $ANTLR start "rule__Goal__FilesAssignment_4_1"
    // InternalNetSpec.g:5019:1: rule__Goal__FilesAssignment_4_1 : ( ( RULE_ID ) ) ;
    public final void rule__Goal__FilesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5023:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5024:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5024:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5025:3: ( RULE_ID )
            {
             before(grammarAccess.getGoalAccess().getFilesFileCrossReference_4_1_0()); 
            // InternalNetSpec.g:5026:3: ( RULE_ID )
            // InternalNetSpec.g:5027:4: RULE_ID
            {
             before(grammarAccess.getGoalAccess().getFilesFileIDTerminalRuleCall_4_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getFilesFileIDTerminalRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getGoalAccess().getFilesFileCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__FilesAssignment_4_1"


    // $ANTLR start "rule__Goal__HostsAssignment_5_1"
    // InternalNetSpec.g:5038:1: rule__Goal__HostsAssignment_5_1 : ( ( RULE_ID ) ) ;
    public final void rule__Goal__HostsAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5042:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5043:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5043:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5044:3: ( RULE_ID )
            {
             before(grammarAccess.getGoalAccess().getHostsHostCrossReference_5_1_0()); 
            // InternalNetSpec.g:5045:3: ( RULE_ID )
            // InternalNetSpec.g:5046:4: RULE_ID
            {
             before(grammarAccess.getGoalAccess().getHostsHostIDTerminalRuleCall_5_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getHostsHostIDTerminalRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getGoalAccess().getHostsHostCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__HostsAssignment_5_1"


    // $ANTLR start "rule__Goal__IdsAssignment_6"
    // InternalNetSpec.g:5057:1: rule__Goal__IdsAssignment_6 : ( ( 'CanTriggerIds' ) ) ;
    public final void rule__Goal__IdsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5061:1: ( ( ( 'CanTriggerIds' ) ) )
            // InternalNetSpec.g:5062:2: ( ( 'CanTriggerIds' ) )
            {
            // InternalNetSpec.g:5062:2: ( ( 'CanTriggerIds' ) )
            // InternalNetSpec.g:5063:3: ( 'CanTriggerIds' )
            {
             before(grammarAccess.getGoalAccess().getIdsCanTriggerIdsKeyword_6_0()); 
            // InternalNetSpec.g:5064:3: ( 'CanTriggerIds' )
            // InternalNetSpec.g:5065:4: 'CanTriggerIds'
            {
             before(grammarAccess.getGoalAccess().getIdsCanTriggerIdsKeyword_6_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getIdsCanTriggerIdsKeyword_6_0()); 

            }

             after(grammarAccess.getGoalAccess().getIdsCanTriggerIdsKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__IdsAssignment_6"


    // $ANTLR start "rule__Host__NameAssignment_2"
    // InternalNetSpec.g:5076:1: rule__Host__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Host__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5080:1: ( ( RULE_ID ) )
            // InternalNetSpec.g:5081:2: ( RULE_ID )
            {
            // InternalNetSpec.g:5081:2: ( RULE_ID )
            // InternalNetSpec.g:5082:3: RULE_ID
            {
             before(grammarAccess.getHostAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__NameAssignment_2"


    // $ANTLR start "rule__Host__SessionsAssignment_4_1"
    // InternalNetSpec.g:5091:1: rule__Host__SessionsAssignment_4_1 : ( ( RULE_ID ) ) ;
    public final void rule__Host__SessionsAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5095:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5096:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5096:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5097:3: ( RULE_ID )
            {
             before(grammarAccess.getHostAccess().getSessionsSessionCrossReference_4_1_0()); 
            // InternalNetSpec.g:5098:3: ( RULE_ID )
            // InternalNetSpec.g:5099:4: RULE_ID
            {
             before(grammarAccess.getHostAccess().getSessionsSessionIDTerminalRuleCall_4_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getSessionsSessionIDTerminalRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getHostAccess().getSessionsSessionCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__SessionsAssignment_4_1"


    // $ANTLR start "rule__Host__FilesAssignment_5_1"
    // InternalNetSpec.g:5110:1: rule__Host__FilesAssignment_5_1 : ( ( RULE_ID ) ) ;
    public final void rule__Host__FilesAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5114:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5115:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5115:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5116:3: ( RULE_ID )
            {
             before(grammarAccess.getHostAccess().getFilesFileCrossReference_5_1_0()); 
            // InternalNetSpec.g:5117:3: ( RULE_ID )
            // InternalNetSpec.g:5118:4: RULE_ID
            {
             before(grammarAccess.getHostAccess().getFilesFileIDTerminalRuleCall_5_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getFilesFileIDTerminalRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getHostAccess().getFilesFileCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__FilesAssignment_5_1"


    // $ANTLR start "rule__Host__RootPwdAssignment_6_1"
    // InternalNetSpec.g:5129:1: rule__Host__RootPwdAssignment_6_1 : ( ( RULE_ID ) ) ;
    public final void rule__Host__RootPwdAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5133:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5134:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5134:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5135:3: ( RULE_ID )
            {
             before(grammarAccess.getHostAccess().getRootPwdLoginCrossReference_6_1_0()); 
            // InternalNetSpec.g:5136:3: ( RULE_ID )
            // InternalNetSpec.g:5137:4: RULE_ID
            {
             before(grammarAccess.getHostAccess().getRootPwdLoginIDTerminalRuleCall_6_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getRootPwdLoginIDTerminalRuleCall_6_1_0_1()); 

            }

             after(grammarAccess.getHostAccess().getRootPwdLoginCrossReference_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__RootPwdAssignment_6_1"


    // $ANTLR start "rule__Host__IdsAssignment_7_1"
    // InternalNetSpec.g:5148:1: rule__Host__IdsAssignment_7_1 : ( ( RULE_ID ) ) ;
    public final void rule__Host__IdsAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5152:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5153:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5153:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5154:3: ( RULE_ID )
            {
             before(grammarAccess.getHostAccess().getIdsIdsCrossReference_7_1_0()); 
            // InternalNetSpec.g:5155:3: ( RULE_ID )
            // InternalNetSpec.g:5156:4: RULE_ID
            {
             before(grammarAccess.getHostAccess().getIdsIdsIDTerminalRuleCall_7_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getIdsIdsIDTerminalRuleCall_7_1_0_1()); 

            }

             after(grammarAccess.getHostAccess().getIdsIdsCrossReference_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__IdsAssignment_7_1"


    // $ANTLR start "rule__Host__FirewallAssignment_8_1"
    // InternalNetSpec.g:5167:1: rule__Host__FirewallAssignment_8_1 : ( ( RULE_ID ) ) ;
    public final void rule__Host__FirewallAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5171:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5172:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5172:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5173:3: ( RULE_ID )
            {
             before(grammarAccess.getHostAccess().getFirewallFirewallCrossReference_8_1_0()); 
            // InternalNetSpec.g:5174:3: ( RULE_ID )
            // InternalNetSpec.g:5175:4: RULE_ID
            {
             before(grammarAccess.getHostAccess().getFirewallFirewallIDTerminalRuleCall_8_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getFirewallFirewallIDTerminalRuleCall_8_1_0_1()); 

            }

             after(grammarAccess.getHostAccess().getFirewallFirewallCrossReference_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__FirewallAssignment_8_1"


    // $ANTLR start "rule__Host__AntivirusAssignment_9_1"
    // InternalNetSpec.g:5186:1: rule__Host__AntivirusAssignment_9_1 : ( ( RULE_ID ) ) ;
    public final void rule__Host__AntivirusAssignment_9_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5190:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5191:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5191:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5192:3: ( RULE_ID )
            {
             before(grammarAccess.getHostAccess().getAntivirusAntivirusCrossReference_9_1_0()); 
            // InternalNetSpec.g:5193:3: ( RULE_ID )
            // InternalNetSpec.g:5194:4: RULE_ID
            {
             before(grammarAccess.getHostAccess().getAntivirusAntivirusIDTerminalRuleCall_9_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHostAccess().getAntivirusAntivirusIDTerminalRuleCall_9_1_0_1()); 

            }

             after(grammarAccess.getHostAccess().getAntivirusAntivirusCrossReference_9_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Host__AntivirusAssignment_9_1"


    // $ANTLR start "rule__Session__NameAssignment_2"
    // InternalNetSpec.g:5205:1: rule__Session__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Session__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5209:1: ( ( RULE_ID ) )
            // InternalNetSpec.g:5210:2: ( RULE_ID )
            {
            // InternalNetSpec.g:5210:2: ( RULE_ID )
            // InternalNetSpec.g:5211:3: RULE_ID
            {
             before(grammarAccess.getSessionAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSessionAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__NameAssignment_2"


    // $ANTLR start "rule__Session__LoginAssignment_4_1"
    // InternalNetSpec.g:5220:1: rule__Session__LoginAssignment_4_1 : ( ( RULE_ID ) ) ;
    public final void rule__Session__LoginAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5224:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5225:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5225:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5226:3: ( RULE_ID )
            {
             before(grammarAccess.getSessionAccess().getLoginLoginCrossReference_4_1_0()); 
            // InternalNetSpec.g:5227:3: ( RULE_ID )
            // InternalNetSpec.g:5228:4: RULE_ID
            {
             before(grammarAccess.getSessionAccess().getLoginLoginIDTerminalRuleCall_4_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSessionAccess().getLoginLoginIDTerminalRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getSessionAccess().getLoginLoginCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__LoginAssignment_4_1"


    // $ANTLR start "rule__Session__RootAssignment_5"
    // InternalNetSpec.g:5239:1: rule__Session__RootAssignment_5 : ( ( 'Root' ) ) ;
    public final void rule__Session__RootAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5243:1: ( ( ( 'Root' ) ) )
            // InternalNetSpec.g:5244:2: ( ( 'Root' ) )
            {
            // InternalNetSpec.g:5244:2: ( ( 'Root' ) )
            // InternalNetSpec.g:5245:3: ( 'Root' )
            {
             before(grammarAccess.getSessionAccess().getRootRootKeyword_5_0()); 
            // InternalNetSpec.g:5246:3: ( 'Root' )
            // InternalNetSpec.g:5247:4: 'Root'
            {
             before(grammarAccess.getSessionAccess().getRootRootKeyword_5_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getSessionAccess().getRootRootKeyword_5_0()); 

            }

             after(grammarAccess.getSessionAccess().getRootRootKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__RootAssignment_5"


    // $ANTLR start "rule__Session__FilesAssignment_6_1"
    // InternalNetSpec.g:5258:1: rule__Session__FilesAssignment_6_1 : ( ( RULE_ID ) ) ;
    public final void rule__Session__FilesAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5262:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5263:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5263:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5264:3: ( RULE_ID )
            {
             before(grammarAccess.getSessionAccess().getFilesFileCrossReference_6_1_0()); 
            // InternalNetSpec.g:5265:3: ( RULE_ID )
            // InternalNetSpec.g:5266:4: RULE_ID
            {
             before(grammarAccess.getSessionAccess().getFilesFileIDTerminalRuleCall_6_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSessionAccess().getFilesFileIDTerminalRuleCall_6_1_0_1()); 

            }

             after(grammarAccess.getSessionAccess().getFilesFileCrossReference_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Session__FilesAssignment_6_1"


    // $ANTLR start "rule__Antivirus__NameAssignment_2"
    // InternalNetSpec.g:5277:1: rule__Antivirus__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Antivirus__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5281:1: ( ( RULE_ID ) )
            // InternalNetSpec.g:5282:2: ( RULE_ID )
            {
            // InternalNetSpec.g:5282:2: ( RULE_ID )
            // InternalNetSpec.g:5283:3: RULE_ID
            {
             before(grammarAccess.getAntivirusAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAntivirusAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__NameAssignment_2"


    // $ANTLR start "rule__Antivirus__LevelAssignment_5"
    // InternalNetSpec.g:5292:1: rule__Antivirus__LevelAssignment_5 : ( ( rule__Antivirus__LevelAlternatives_5_0 ) ) ;
    public final void rule__Antivirus__LevelAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5296:1: ( ( ( rule__Antivirus__LevelAlternatives_5_0 ) ) )
            // InternalNetSpec.g:5297:2: ( ( rule__Antivirus__LevelAlternatives_5_0 ) )
            {
            // InternalNetSpec.g:5297:2: ( ( rule__Antivirus__LevelAlternatives_5_0 ) )
            // InternalNetSpec.g:5298:3: ( rule__Antivirus__LevelAlternatives_5_0 )
            {
             before(grammarAccess.getAntivirusAccess().getLevelAlternatives_5_0()); 
            // InternalNetSpec.g:5299:3: ( rule__Antivirus__LevelAlternatives_5_0 )
            // InternalNetSpec.g:5299:4: rule__Antivirus__LevelAlternatives_5_0
            {
            pushFollow(FOLLOW_2);
            rule__Antivirus__LevelAlternatives_5_0();

            state._fsp--;


            }

             after(grammarAccess.getAntivirusAccess().getLevelAlternatives_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Antivirus__LevelAssignment_5"


    // $ANTLR start "rule__File__NameAssignment_2"
    // InternalNetSpec.g:5307:1: rule__File__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__File__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5311:1: ( ( RULE_ID ) )
            // InternalNetSpec.g:5312:2: ( RULE_ID )
            {
            // InternalNetSpec.g:5312:2: ( RULE_ID )
            // InternalNetSpec.g:5313:3: RULE_ID
            {
             before(grammarAccess.getFileAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__NameAssignment_2"


    // $ANTLR start "rule__File__KeyAssignment_4_1"
    // InternalNetSpec.g:5322:1: rule__File__KeyAssignment_4_1 : ( ( RULE_ID ) ) ;
    public final void rule__File__KeyAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5326:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5327:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5327:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5328:3: ( RULE_ID )
            {
             before(grammarAccess.getFileAccess().getKeyKeyCrossReference_4_1_0()); 
            // InternalNetSpec.g:5329:3: ( RULE_ID )
            // InternalNetSpec.g:5330:4: RULE_ID
            {
             before(grammarAccess.getFileAccess().getKeyKeyIDTerminalRuleCall_4_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getKeyKeyIDTerminalRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getFileAccess().getKeyKeyCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__KeyAssignment_4_1"


    // $ANTLR start "rule__File__ContainsLAssignment_5_1"
    // InternalNetSpec.g:5341:1: rule__File__ContainsLAssignment_5_1 : ( ( RULE_ID ) ) ;
    public final void rule__File__ContainsLAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5345:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5346:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5346:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5347:3: ( RULE_ID )
            {
             before(grammarAccess.getFileAccess().getContainsLLoginCrossReference_5_1_0()); 
            // InternalNetSpec.g:5348:3: ( RULE_ID )
            // InternalNetSpec.g:5349:4: RULE_ID
            {
             before(grammarAccess.getFileAccess().getContainsLLoginIDTerminalRuleCall_5_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getContainsLLoginIDTerminalRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getFileAccess().getContainsLLoginCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__ContainsLAssignment_5_1"


    // $ANTLR start "rule__File__ContainsKAssignment_6_1"
    // InternalNetSpec.g:5360:1: rule__File__ContainsKAssignment_6_1 : ( ( RULE_ID ) ) ;
    public final void rule__File__ContainsKAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5364:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5365:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5365:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5366:3: ( RULE_ID )
            {
             before(grammarAccess.getFileAccess().getContainsKKeyCrossReference_6_1_0()); 
            // InternalNetSpec.g:5367:3: ( RULE_ID )
            // InternalNetSpec.g:5368:4: RULE_ID
            {
             before(grammarAccess.getFileAccess().getContainsKKeyIDTerminalRuleCall_6_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getContainsKKeyIDTerminalRuleCall_6_1_0_1()); 

            }

             after(grammarAccess.getFileAccess().getContainsKKeyCrossReference_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__ContainsKAssignment_6_1"


    // $ANTLR start "rule__File__DatasAssignment_7_1"
    // InternalNetSpec.g:5379:1: rule__File__DatasAssignment_7_1 : ( ( RULE_ID ) ) ;
    public final void rule__File__DatasAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5383:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5384:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5384:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5385:3: ( RULE_ID )
            {
             before(grammarAccess.getFileAccess().getDatasDataCrossReference_7_1_0()); 
            // InternalNetSpec.g:5386:3: ( RULE_ID )
            // InternalNetSpec.g:5387:4: RULE_ID
            {
             before(grammarAccess.getFileAccess().getDatasDataIDTerminalRuleCall_7_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFileAccess().getDatasDataIDTerminalRuleCall_7_1_0_1()); 

            }

             after(grammarAccess.getFileAccess().getDatasDataCrossReference_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__DatasAssignment_7_1"


    // $ANTLR start "rule__Data__NameAssignment_1"
    // InternalNetSpec.g:5398:1: rule__Data__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Data__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5402:1: ( ( RULE_ID ) )
            // InternalNetSpec.g:5403:2: ( RULE_ID )
            {
            // InternalNetSpec.g:5403:2: ( RULE_ID )
            // InternalNetSpec.g:5404:3: RULE_ID
            {
             before(grammarAccess.getDataAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDataAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__NameAssignment_1"


    // $ANTLR start "rule__Login__NameAssignment_2"
    // InternalNetSpec.g:5413:1: rule__Login__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Login__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5417:1: ( ( RULE_ID ) )
            // InternalNetSpec.g:5418:2: ( RULE_ID )
            {
            // InternalNetSpec.g:5418:2: ( RULE_ID )
            // InternalNetSpec.g:5419:3: RULE_ID
            {
             before(grammarAccess.getLoginAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLoginAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__NameAssignment_2"


    // $ANTLR start "rule__Login__LevelAssignment_5"
    // InternalNetSpec.g:5428:1: rule__Login__LevelAssignment_5 : ( ( rule__Login__LevelAlternatives_5_0 ) ) ;
    public final void rule__Login__LevelAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5432:1: ( ( ( rule__Login__LevelAlternatives_5_0 ) ) )
            // InternalNetSpec.g:5433:2: ( ( rule__Login__LevelAlternatives_5_0 ) )
            {
            // InternalNetSpec.g:5433:2: ( ( rule__Login__LevelAlternatives_5_0 ) )
            // InternalNetSpec.g:5434:3: ( rule__Login__LevelAlternatives_5_0 )
            {
             before(grammarAccess.getLoginAccess().getLevelAlternatives_5_0()); 
            // InternalNetSpec.g:5435:3: ( rule__Login__LevelAlternatives_5_0 )
            // InternalNetSpec.g:5435:4: rule__Login__LevelAlternatives_5_0
            {
            pushFollow(FOLLOW_2);
            rule__Login__LevelAlternatives_5_0();

            state._fsp--;


            }

             after(grammarAccess.getLoginAccess().getLevelAlternatives_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Login__LevelAssignment_5"


    // $ANTLR start "rule__Key__NameAssignment_2"
    // InternalNetSpec.g:5443:1: rule__Key__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Key__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5447:1: ( ( RULE_ID ) )
            // InternalNetSpec.g:5448:2: ( RULE_ID )
            {
            // InternalNetSpec.g:5448:2: ( RULE_ID )
            // InternalNetSpec.g:5449:3: RULE_ID
            {
             before(grammarAccess.getKeyAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getKeyAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__NameAssignment_2"


    // $ANTLR start "rule__Key__LevelAssignment_5"
    // InternalNetSpec.g:5458:1: rule__Key__LevelAssignment_5 : ( ( rule__Key__LevelAlternatives_5_0 ) ) ;
    public final void rule__Key__LevelAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5462:1: ( ( ( rule__Key__LevelAlternatives_5_0 ) ) )
            // InternalNetSpec.g:5463:2: ( ( rule__Key__LevelAlternatives_5_0 ) )
            {
            // InternalNetSpec.g:5463:2: ( ( rule__Key__LevelAlternatives_5_0 ) )
            // InternalNetSpec.g:5464:3: ( rule__Key__LevelAlternatives_5_0 )
            {
             before(grammarAccess.getKeyAccess().getLevelAlternatives_5_0()); 
            // InternalNetSpec.g:5465:3: ( rule__Key__LevelAlternatives_5_0 )
            // InternalNetSpec.g:5465:4: rule__Key__LevelAlternatives_5_0
            {
            pushFollow(FOLLOW_2);
            rule__Key__LevelAlternatives_5_0();

            state._fsp--;


            }

             after(grammarAccess.getKeyAccess().getLevelAlternatives_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Key__LevelAssignment_5"


    // $ANTLR start "rule__Firewall__NameAssignment_2"
    // InternalNetSpec.g:5473:1: rule__Firewall__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Firewall__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5477:1: ( ( RULE_ID ) )
            // InternalNetSpec.g:5478:2: ( RULE_ID )
            {
            // InternalNetSpec.g:5478:2: ( RULE_ID )
            // InternalNetSpec.g:5479:3: RULE_ID
            {
             before(grammarAccess.getFirewallAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFirewallAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__NameAssignment_2"


    // $ANTLR start "rule__Firewall__LevelAssignment_5"
    // InternalNetSpec.g:5488:1: rule__Firewall__LevelAssignment_5 : ( ( rule__Firewall__LevelAlternatives_5_0 ) ) ;
    public final void rule__Firewall__LevelAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5492:1: ( ( ( rule__Firewall__LevelAlternatives_5_0 ) ) )
            // InternalNetSpec.g:5493:2: ( ( rule__Firewall__LevelAlternatives_5_0 ) )
            {
            // InternalNetSpec.g:5493:2: ( ( rule__Firewall__LevelAlternatives_5_0 ) )
            // InternalNetSpec.g:5494:3: ( rule__Firewall__LevelAlternatives_5_0 )
            {
             before(grammarAccess.getFirewallAccess().getLevelAlternatives_5_0()); 
            // InternalNetSpec.g:5495:3: ( rule__Firewall__LevelAlternatives_5_0 )
            // InternalNetSpec.g:5495:4: rule__Firewall__LevelAlternatives_5_0
            {
            pushFollow(FOLLOW_2);
            rule__Firewall__LevelAlternatives_5_0();

            state._fsp--;


            }

             after(grammarAccess.getFirewallAccess().getLevelAlternatives_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Firewall__LevelAssignment_5"


    // $ANTLR start "rule__Ids__NameAssignment_2"
    // InternalNetSpec.g:5503:1: rule__Ids__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Ids__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5507:1: ( ( RULE_ID ) )
            // InternalNetSpec.g:5508:2: ( RULE_ID )
            {
            // InternalNetSpec.g:5508:2: ( RULE_ID )
            // InternalNetSpec.g:5509:3: RULE_ID
            {
             before(grammarAccess.getIdsAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getIdsAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__NameAssignment_2"


    // $ANTLR start "rule__Ids__ActiveAssignment_4"
    // InternalNetSpec.g:5518:1: rule__Ids__ActiveAssignment_4 : ( ( 'Active' ) ) ;
    public final void rule__Ids__ActiveAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5522:1: ( ( ( 'Active' ) ) )
            // InternalNetSpec.g:5523:2: ( ( 'Active' ) )
            {
            // InternalNetSpec.g:5523:2: ( ( 'Active' ) )
            // InternalNetSpec.g:5524:3: ( 'Active' )
            {
             before(grammarAccess.getIdsAccess().getActiveActiveKeyword_4_0()); 
            // InternalNetSpec.g:5525:3: ( 'Active' )
            // InternalNetSpec.g:5526:4: 'Active'
            {
             before(grammarAccess.getIdsAccess().getActiveActiveKeyword_4_0()); 
            match(input,48,FOLLOW_2); 
             after(grammarAccess.getIdsAccess().getActiveActiveKeyword_4_0()); 

            }

             after(grammarAccess.getIdsAccess().getActiveActiveKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__ActiveAssignment_4"


    // $ANTLR start "rule__Ids__LevelAssignment_6"
    // InternalNetSpec.g:5537:1: rule__Ids__LevelAssignment_6 : ( ( rule__Ids__LevelAlternatives_6_0 ) ) ;
    public final void rule__Ids__LevelAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5541:1: ( ( ( rule__Ids__LevelAlternatives_6_0 ) ) )
            // InternalNetSpec.g:5542:2: ( ( rule__Ids__LevelAlternatives_6_0 ) )
            {
            // InternalNetSpec.g:5542:2: ( ( rule__Ids__LevelAlternatives_6_0 ) )
            // InternalNetSpec.g:5543:3: ( rule__Ids__LevelAlternatives_6_0 )
            {
             before(grammarAccess.getIdsAccess().getLevelAlternatives_6_0()); 
            // InternalNetSpec.g:5544:3: ( rule__Ids__LevelAlternatives_6_0 )
            // InternalNetSpec.g:5544:4: rule__Ids__LevelAlternatives_6_0
            {
            pushFollow(FOLLOW_2);
            rule__Ids__LevelAlternatives_6_0();

            state._fsp--;


            }

             after(grammarAccess.getIdsAccess().getLevelAlternatives_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ids__LevelAssignment_6"


    // $ANTLR start "rule__Link__H1Assignment_4"
    // InternalNetSpec.g:5552:1: rule__Link__H1Assignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__Link__H1Assignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5556:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5557:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5557:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5558:3: ( RULE_ID )
            {
             before(grammarAccess.getLinkAccess().getH1HostCrossReference_4_0()); 
            // InternalNetSpec.g:5559:3: ( RULE_ID )
            // InternalNetSpec.g:5560:4: RULE_ID
            {
             before(grammarAccess.getLinkAccess().getH1HostIDTerminalRuleCall_4_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getH1HostIDTerminalRuleCall_4_0_1()); 

            }

             after(grammarAccess.getLinkAccess().getH1HostCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__H1Assignment_4"


    // $ANTLR start "rule__Link__H2Assignment_6"
    // InternalNetSpec.g:5571:1: rule__Link__H2Assignment_6 : ( ( RULE_ID ) ) ;
    public final void rule__Link__H2Assignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5575:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5576:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5576:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5577:3: ( RULE_ID )
            {
             before(grammarAccess.getLinkAccess().getH2HostCrossReference_6_0()); 
            // InternalNetSpec.g:5578:3: ( RULE_ID )
            // InternalNetSpec.g:5579:4: RULE_ID
            {
             before(grammarAccess.getLinkAccess().getH2HostIDTerminalRuleCall_6_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getH2HostIDTerminalRuleCall_6_0_1()); 

            }

             after(grammarAccess.getLinkAccess().getH2HostCrossReference_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__H2Assignment_6"


    // $ANTLR start "rule__Link__IdsAssignment_7_1"
    // InternalNetSpec.g:5590:1: rule__Link__IdsAssignment_7_1 : ( ( RULE_ID ) ) ;
    public final void rule__Link__IdsAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5594:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5595:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5595:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5596:3: ( RULE_ID )
            {
             before(grammarAccess.getLinkAccess().getIdsIdsCrossReference_7_1_0()); 
            // InternalNetSpec.g:5597:3: ( RULE_ID )
            // InternalNetSpec.g:5598:4: RULE_ID
            {
             before(grammarAccess.getLinkAccess().getIdsIdsIDTerminalRuleCall_7_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getIdsIdsIDTerminalRuleCall_7_1_0_1()); 

            }

             after(grammarAccess.getLinkAccess().getIdsIdsCrossReference_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__IdsAssignment_7_1"


    // $ANTLR start "rule__Link__FirewallAssignment_8_1"
    // InternalNetSpec.g:5609:1: rule__Link__FirewallAssignment_8_1 : ( ( RULE_ID ) ) ;
    public final void rule__Link__FirewallAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNetSpec.g:5613:1: ( ( ( RULE_ID ) ) )
            // InternalNetSpec.g:5614:2: ( ( RULE_ID ) )
            {
            // InternalNetSpec.g:5614:2: ( ( RULE_ID ) )
            // InternalNetSpec.g:5615:3: ( RULE_ID )
            {
             before(grammarAccess.getLinkAccess().getFirewallFirewallCrossReference_8_1_0()); 
            // InternalNetSpec.g:5616:3: ( RULE_ID )
            // InternalNetSpec.g:5617:4: RULE_ID
            {
             before(grammarAccess.getLinkAccess().getFirewallFirewallIDTerminalRuleCall_8_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLinkAccess().getFirewallFirewallIDTerminalRuleCall_8_1_0_1()); 

            }

             after(grammarAccess.getLinkAccess().getFirewallFirewallCrossReference_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Link__FirewallAssignment_8_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000019BE40000000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000019BE40000002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000007800L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000003C00000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000800038400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000F90400000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000002110400000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x000000000000F000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000070008400000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000007000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0001004000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000600400000L});

}