/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.provider;


import atsyragoal.AtsyraGoalModel;
import atsyragoal.AtsyragoalFactory;
import atsyragoal.AtsyragoalPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link atsyragoal.AtsyraGoalModel} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AtsyraGoalModelItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraGoalModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__ATSYRAGOALS);
			childrenFeatures.add(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__TYPES);
			childrenFeatures.add(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__IMPORTS);
			childrenFeatures.add(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS);
			childrenFeatures.add(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__TREES);
			childrenFeatures.add(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__DEFAULT_VALUES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns AtsyraGoalModel.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AtsyraGoalModel"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_AtsyraGoalModel_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AtsyraGoalModel.class)) {
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__ATSYRAGOALS:
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPES:
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__IMPORTS:
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS:
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__TREES:
			case AtsyragoalPackage.ATSYRA_GOAL_MODEL__DEFAULT_VALUES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__ATSYRAGOALS,
				 AtsyragoalFactory.eINSTANCE.createAtsyraGoal()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__TYPES,
				 AtsyragoalFactory.eINSTANCE.createInternalType()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__TYPES,
				 AtsyragoalFactory.eINSTANCE.createSystemType()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__IMPORTS,
				 AtsyragoalFactory.eINSTANCE.createImport()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS,
				 AtsyragoalFactory.eINSTANCE.createBooleanLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS,
				 AtsyragoalFactory.eINSTANCE.createSystemFeature()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__TYPED_ELEMENTS,
				 AtsyragoalFactory.eINSTANCE.createSystemConstFeature()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__TREES,
				 AtsyragoalFactory.eINSTANCE.createAtsyraTree()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL_MODEL__DEFAULT_VALUES,
				 AtsyragoalFactory.eINSTANCE.createDefaultValues()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AtsyragoalEditPlugin.INSTANCE;
	}

}
