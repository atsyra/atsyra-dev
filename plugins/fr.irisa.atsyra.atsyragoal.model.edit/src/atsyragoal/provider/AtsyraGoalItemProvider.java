/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.provider;


import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyragoalFactory;
import atsyragoal.AtsyragoalPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link atsyragoal.AtsyraGoal} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AtsyraGoalItemProvider 
	extends ItemProviderAdapter implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraGoalItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addDefaultUsedInPrePropertyDescriptor(object);
			addDefaultUsedInPostPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AtsyraGoal_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AtsyraGoal_name_feature", "_UI_AtsyraGoal_type"),
				 AtsyragoalPackage.Literals.ATSYRA_GOAL__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Default Used In Pre feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefaultUsedInPrePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AtsyraGoal_defaultUsedInPre_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AtsyraGoal_defaultUsedInPre_feature", "_UI_AtsyraGoal_type"),
				 AtsyragoalPackage.Literals.ATSYRA_GOAL__DEFAULT_USED_IN_PRE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Default Used In Post feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefaultUsedInPostPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AtsyraGoal_defaultUsedInPost_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AtsyraGoal_defaultUsedInPost_feature", "_UI_AtsyraGoal_type"),
				 AtsyragoalPackage.Literals.ATSYRA_GOAL__DEFAULT_USED_IN_POST,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(AtsyragoalPackage.Literals.ATSYRA_GOAL__PRECONDITION);
			childrenFeatures.add(AtsyragoalPackage.Literals.ATSYRA_GOAL__POSTCONDITION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns AtsyraGoal.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AtsyraGoal"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AtsyraGoal)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_AtsyraGoal_type") :
			getString("_UI_AtsyraGoal_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AtsyraGoal.class)) {
			case AtsyragoalPackage.ATSYRA_GOAL__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case AtsyragoalPackage.ATSYRA_GOAL__PRECONDITION:
			case AtsyragoalPackage.ATSYRA_GOAL__POSTCONDITION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__PRECONDITION,
				 AtsyragoalFactory.eINSTANCE.createAndCondition()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__PRECONDITION,
				 AtsyragoalFactory.eINSTANCE.createOrCondition()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__PRECONDITION,
				 AtsyragoalFactory.eINSTANCE.createNotCondition()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__PRECONDITION,
				 AtsyragoalFactory.eINSTANCE.createEqualsCondition()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__PRECONDITION,
				 AtsyragoalFactory.eINSTANCE.createBooleanSystemCondition()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__POSTCONDITION,
				 AtsyragoalFactory.eINSTANCE.createAndCondition()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__POSTCONDITION,
				 AtsyragoalFactory.eINSTANCE.createOrCondition()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__POSTCONDITION,
				 AtsyragoalFactory.eINSTANCE.createNotCondition()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__POSTCONDITION,
				 AtsyragoalFactory.eINSTANCE.createEqualsCondition()));

		newChildDescriptors.add
			(createChildParameter
				(AtsyragoalPackage.Literals.ATSYRA_GOAL__POSTCONDITION,
				 AtsyragoalFactory.eINSTANCE.createBooleanSystemCondition()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == AtsyragoalPackage.Literals.ATSYRA_GOAL__PRECONDITION ||
			childFeature == AtsyragoalPackage.Literals.ATSYRA_GOAL__POSTCONDITION;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AtsyragoalEditPlugin.INSTANCE;
	}

}
