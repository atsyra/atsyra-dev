/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.provider;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedImage;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;

import atsyragoal.AtsyraTree;
import atsyragoal.AtsyraTreeOperator;
import atsyragoal.AtsyraTreeReference;
import atsyragoal.AtsyragoalPackage;

/**
 * This is the item provider adapter for a {@link atsyragoal.AtsyraTreeReference} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AtsyraTreeReferenceItemProvider extends AbstractAtsyraTreeItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyraTreeReferenceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addReferencedTreePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Referenced Tree feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReferencedTreePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AtsyraTreeReference_referencedTree_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AtsyraTreeReference_referencedTree_feature", "_UI_AtsyraTreeReference_type"),
				 AtsyragoalPackage.Literals.ATSYRA_TREE_REFERENCE__REFERENCED_TREE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns AtsyraTreeReference.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		List<Object> images = new ArrayList<Object>(2);
		AtsyraTreeReference tref = (AtsyraTreeReference) object;
		AtsyraTree t = null;
		if(tref.getReferencedTree() != null && tref.getReferencedTree()  instanceof AtsyraTree) {
			t = (AtsyraTree) tref.getReferencedTree();
		}
		if(t != null && t.getOperator() != null && t.getOperator() != AtsyraTreeOperator.UNKNOWN) {
			images.add(getResourceLocator().getImage("full/obj16/AtsyraTree_"+t.getOperator().getLiteral()+".png"));
		} else {
			images.add(getResourceLocator().getImage("full/obj16/AtsyraTree.png"));
		}
	    images.add(getResourceLocator().getImage("/full/ovr16/link_ovr.png"));
		return new ComposedImage(images);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		final AtsyraTreeReference atsyraTreeReference = ((AtsyraTreeReference)object);
		if(atsyraTreeReference.getReferencedTree() != null) {
			final IItemLabelProvider provider = (IItemLabelProvider) getAdapterFactory().adapt(atsyraTreeReference.getReferencedTree(), IItemLabelProvider.class);
			String label = provider.getText(atsyraTreeReference.getReferencedTree());
			return label == null || label.length() == 0 ?
				getString("_UI_AtsyraTreeReference_type") :
				"-> "+label;
		} else {
			return getString("_UI_AtsyraTreeReference_type") + " " + atsyraTreeReference.getQualifiedName();
		}
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
