/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package atsyragoal.provider;

import atsyragoal.util.AtsyragoalAdapterFactory;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AtsyragoalItemProviderAdapterFactory extends AtsyragoalAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtsyragoalItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.AtsyraGoal} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtsyraGoalItemProvider atsyraGoalItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.AtsyraGoal}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAtsyraGoalAdapter() {
		if (atsyraGoalItemProvider == null) {
			atsyraGoalItemProvider = new AtsyraGoalItemProvider(this);
		}

		return atsyraGoalItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.AndCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AndConditionItemProvider andConditionItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.AndCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAndConditionAdapter() {
		if (andConditionItemProvider == null) {
			andConditionItemProvider = new AndConditionItemProvider(this);
		}

		return andConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.OrCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrConditionItemProvider orConditionItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.OrCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createOrConditionAdapter() {
		if (orConditionItemProvider == null) {
			orConditionItemProvider = new OrConditionItemProvider(this);
		}

		return orConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.NotCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NotConditionItemProvider notConditionItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.NotCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createNotConditionAdapter() {
		if (notConditionItemProvider == null) {
			notConditionItemProvider = new NotConditionItemProvider(this);
		}

		return notConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.AtsyraGoalModel} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtsyraGoalModelItemProvider atsyraGoalModelItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.AtsyraGoalModel}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAtsyraGoalModelAdapter() {
		if (atsyraGoalModelItemProvider == null) {
			atsyraGoalModelItemProvider = new AtsyraGoalModelItemProvider(this);
		}

		return atsyraGoalModelItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.EqualsCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EqualsConditionItemProvider equalsConditionItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.EqualsCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createEqualsConditionAdapter() {
		if (equalsConditionItemProvider == null) {
			equalsConditionItemProvider = new EqualsConditionItemProvider(this);
		}

		return equalsConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.InternalType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InternalTypeItemProvider internalTypeItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.InternalType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createInternalTypeAdapter() {
		if (internalTypeItemProvider == null) {
			internalTypeItemProvider = new InternalTypeItemProvider(this);
		}

		return internalTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.BooleanLiteral} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BooleanLiteralItemProvider booleanLiteralItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.BooleanLiteral}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBooleanLiteralAdapter() {
		if (booleanLiteralItemProvider == null) {
			booleanLiteralItemProvider = new BooleanLiteralItemProvider(this);
		}

		return booleanLiteralItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.SystemType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemTypeItemProvider systemTypeItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.SystemType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSystemTypeAdapter() {
		if (systemTypeItemProvider == null) {
			systemTypeItemProvider = new SystemTypeItemProvider(this);
		}

		return systemTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.SystemFeature} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemFeatureItemProvider systemFeatureItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.SystemFeature}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSystemFeatureAdapter() {
		if (systemFeatureItemProvider == null) {
			systemFeatureItemProvider = new SystemFeatureItemProvider(this);
		}

		return systemFeatureItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.SystemConstFeature} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemConstFeatureItemProvider systemConstFeatureItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.SystemConstFeature}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSystemConstFeatureAdapter() {
		if (systemConstFeatureItemProvider == null) {
			systemConstFeatureItemProvider = new SystemConstFeatureItemProvider(this);
		}

		return systemConstFeatureItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.BooleanSystemCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BooleanSystemConditionItemProvider booleanSystemConditionItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.BooleanSystemCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBooleanSystemConditionAdapter() {
		if (booleanSystemConditionItemProvider == null) {
			booleanSystemConditionItemProvider = new BooleanSystemConditionItemProvider(this);
		}

		return booleanSystemConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.Import} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImportItemProvider importItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.Import}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createImportAdapter() {
		if (importItemProvider == null) {
			importItemProvider = new ImportItemProvider(this);
		}

		return importItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.AtsyraTree} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtsyraTreeItemProvider atsyraTreeItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.AtsyraTree}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAtsyraTreeAdapter() {
		if (atsyraTreeItemProvider == null) {
			atsyraTreeItemProvider = new AtsyraTreeItemProvider(this);
		}

		return atsyraTreeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.AtsyraTreeReference} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtsyraTreeReferenceItemProvider atsyraTreeReferenceItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.AtsyraTreeReference}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAtsyraTreeReferenceAdapter() {
		if (atsyraTreeReferenceItemProvider == null) {
			atsyraTreeReferenceItemProvider = new AtsyraTreeReferenceItemProvider(this);
		}

		return atsyraTreeReferenceItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.DefaultValues} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultValuesItemProvider defaultValuesItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.DefaultValues}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createDefaultValuesAdapter() {
		if (defaultValuesItemProvider == null) {
			defaultValuesItemProvider = new DefaultValuesItemProvider(this);
		}

		return defaultValuesItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link atsyragoal.DefaultAssignment} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultAssignmentItemProvider defaultAssignmentItemProvider;

	/**
	 * This creates an adapter for a {@link atsyragoal.DefaultAssignment}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createDefaultAssignmentAdapter() {
		if (defaultAssignmentItemProvider == null) {
			defaultAssignmentItemProvider = new DefaultAssignmentItemProvider(this);
		}

		return defaultAssignmentItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (atsyraGoalItemProvider != null) atsyraGoalItemProvider.dispose();
		if (andConditionItemProvider != null) andConditionItemProvider.dispose();
		if (orConditionItemProvider != null) orConditionItemProvider.dispose();
		if (notConditionItemProvider != null) notConditionItemProvider.dispose();
		if (atsyraGoalModelItemProvider != null) atsyraGoalModelItemProvider.dispose();
		if (equalsConditionItemProvider != null) equalsConditionItemProvider.dispose();
		if (internalTypeItemProvider != null) internalTypeItemProvider.dispose();
		if (booleanLiteralItemProvider != null) booleanLiteralItemProvider.dispose();
		if (systemTypeItemProvider != null) systemTypeItemProvider.dispose();
		if (systemFeatureItemProvider != null) systemFeatureItemProvider.dispose();
		if (systemConstFeatureItemProvider != null) systemConstFeatureItemProvider.dispose();
		if (booleanSystemConditionItemProvider != null) booleanSystemConditionItemProvider.dispose();
		if (importItemProvider != null) importItemProvider.dispose();
		if (atsyraTreeItemProvider != null) atsyraTreeItemProvider.dispose();
		if (atsyraTreeReferenceItemProvider != null) atsyraTreeReferenceItemProvider.dispose();
		if (defaultValuesItemProvider != null) defaultValuesItemProvider.dispose();
		if (defaultAssignmentItemProvider != null) defaultAssignmentItemProvider.dispose();
	}

}
