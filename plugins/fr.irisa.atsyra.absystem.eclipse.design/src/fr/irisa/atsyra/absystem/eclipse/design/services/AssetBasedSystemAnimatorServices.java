/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.eclipse.design.services;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;

import org.eclipse.gemoc.executionframework.extensions.sirius.services.AbstractGemocAnimatorServices;
import fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetK3Aspect;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;

public class AssetBasedSystemAnimatorServices  extends AbstractGemocAnimatorServices {

	
	 protected List<StringCouple> getRepresentationRefreshList() {
		List<StringCouple> res = new ArrayList<StringCouple>();
		res.add(new StringCouple("AssetDiagram", "Animation"));
		res.add(new StringCouple("fr.irisa.atsyra.absystem.AssetBasedSystem", "Animation"));
		return res;
	}
	
	
	
	/** 
	 * indicates if the element is on of the parameter of the last GuardedAction Occurence
	 */
	public boolean isInvolvedByLastAction(EObject o) {
		if (o instanceof Asset) {
			EList<GuardOccurence> actions = EcoreUtil2.getContainerOfType(o, AssetBasedSystem.class).getAppliedActions();
			GuardOccurence lastAppliedaction = actions.get(actions.size());
			if(lastAppliedaction != null) {
				boolean res = lastAppliedaction.getGuardOccurenceArguments().stream().anyMatch(actionArg -> 
				(actionArg != null && actionArg instanceof AssetArgument && 
				o != null && o.equals(((AssetArgument)actionArg).getAsset())));
				/*		
						.exists[actionArg | 
					actionArg instanceof AssetArgument && (actionArg as AssetArgument).asset == o
				]*/
				if(res) 
					System.out.println("Asset involved "+((Asset)o).getName());
				return res;
			} else return false;
		} else {
			return false;
		}
	}
	
	public String getRTDString(EObject o) {
		if (o instanceof Asset) {
			String rtdString = AssetK3Aspect.runtimeDataToAnimatorString((Asset) o);
			return rtdString;
		} else {
			return "";
		}
	}
}
