package fr.irisa.atsyra.building.locationgraph;

import fr.irisa.atsyra.gal.process.InterruptibleProcessController
import fr.irisa.atsyra.gal.process.InterruptibleProcessController.KilledByUserException
import fr.irisa.atsyra.gal.process.InterruptibleProcessController.TimeOutException
import fr.irisa.atsyra.ide.ui.preferences.PreferenceConstants
import fr.irisa.atsyra.transfo.building.gal.GalNamer
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.attribute.PosixFilePermission
import java.util.ArrayList
import java.util.List
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Platform

class LocationGraph {
	IFile gal_file;
	public long timeout = 60;
	String goal;
	private String locationVariable;
	List<String> variables;
	boolean itemMode;
	
	
	new(IFile galFile, String agoal) {
		super()
		gal_file = galFile
		goal = agoal
		timeout = defaultTimeout
		itemMode = false;
		locationVariable = GalNamer.getVarName("attacker", "location");
	}
	
	new(IFile galFile, String agoal, String variable) {
		super()
		gal_file = galFile
		goal = agoal
		timeout = defaultTimeout
		itemMode = false;
		locationVariable = variable;
	}
	
	new(IFile galFile, String agoal, List<String> variablesList) {
		super()
		gal_file = galFile
		goal = agoal
		timeout = defaultTimeout
		variables = variablesList
		itemMode = true;
	}
	
	def public void computeLocationGraph(IProgressMonitor monitor) {
		val locationGraphToolPath = getToolPath();
		if (locationGraphToolPath === null)
			return;
		
		val locationGraphToolFile = new File(locationGraphToolPath)
		if (!locationGraphToolFile.exists) {
			Activator.error("LocationGraph tool executable not available at the specified location: "+locationGraphToolPath);
			return;
		}	
		
		if(!Files.isExecutable(locationGraphToolFile.toPath) && Activator.getOperatingSystemType() != "win") {
			val perm = Files.getPosixFilePermissions(locationGraphToolFile.toPath)
			perm.add(PosixFilePermission.OWNER_EXECUTE)
			perm.add(PosixFilePermission.GROUP_EXECUTE)
			perm.add(PosixFilePermission.OTHERS_EXECUTE)
			Files.setPosixFilePermissions(locationGraphToolFile.toPath, perm)
		}
		val cmd = if(itemMode) {buildCommandArgumentsItems()} else {buildCommandArgumentsLocation()};
		val errorOutput = new ByteArrayOutputStream();
		val stdOutput = new ByteArrayOutputStream();
		val env = new ArrayList<String>()
		val StringBuilder cmdSB = new StringBuilder
		cmd.forEach[s | if(s.contains(" ")) { cmdSB.append("\"");} cmdSB.append(s); if(s.contains(" ")) { cmdSB.append("\"");}; cmdSB.append (" ")]		
		Activator.debug(cmdSB.toString)
		
		val controller = new InterruptibleProcessController(
				timeout * 1000, cmd.toArray(newArrayOfSize(cmd.size())), env.toArray(newArrayOfSize(env.size())),
				new File(gal_file.getParent().getLocationURI()), monitor, "LocationGraph "+goal+" ", true); 
		controller.forwardErrorOutput(errorOutput);
		controller.forwardOutput(stdOutput);
		var int exitCode
		try {
			exitCode = controller.execute();
			val stdOutString = stdOutput.toString();
			Activator.info(stdOutString)
			if (exitCode != 0) {
				Activator.error("LocationGraph exited with error code: "+ exitCode);
				if (errorOutput.size() > 0) {	
					Activator.error("LocationGraph reported some errors: "+ errorOutput.toString);
					Activator.error("LocationGraph reported some errors (cont.): "+ stdOutString);
					return;
				}
			} else {
				Activator.important("Graph of locations successfully computed!");
			}
		} catch (IOException e) {
			Activator.error("Unexpected exception while running LocationGraph."
									+ errorOutput);
			return;
		} catch (TimeOutException e) {
			Activator.error("LocationGraph did not finish in a timely way."
									+ errorOutput);
			Activator.error("Timeout!")
			return;
		} catch (KilledByUserException e) {
			//Activator.debug(stdOutString);
			Activator.error("LocationGraph interrupted by user.");						
			return;
		}
	}
	
	def buildCommandArgumentsLocation() {
		val cmd = new ArrayList<String>();
		cmd.add(getToolPath());
		cmd.add("-f");
		cmd.add(gal_file.getName());
		cmd.add("-v");
		cmd.add(locationVariable);
		cmd.add("-g");
		//cmd.add(goal);
		cmd.add("goal_reached=1");
		cmd.add("-o");
		cmd.add(gal_file.getName().substring(0, gal_file.getName().lastIndexOf('.')));
		return cmd;
	}
	
	def buildCommandArgumentsItems() {
		val cmd = new ArrayList<String>();
		cmd.add(getToolPath());
		cmd.add("-f");
		cmd.add(gal_file.getName());
		cmd.add("-v");
		cmd.addAll(variables);
		cmd.add("-g");
		//cmd.add(goal);
		cmd.add("goal_reached=1");
		cmd.add("-o");
		cmd.add(gal_file.getName().substring(0, gal_file.getName().lastIndexOf('.')));
		cmd.add("-i");
		return cmd;
	}
	
	def public String getToolPath() {
		val bundle = Platform.getBundle("fr.irisa.atsyra.building.locationGraph");
		if(Activator.getOperatingSystemType() == ""){
			throw new RuntimeException("Operating system not supported");
		}
		var suffix = ""
		if(Activator.getOperatingSystemType() == "win") {suffix = ".exe"}
		val toolName = "VariationFronts" //if(!itemMode) "locationGraph" else "itemGraph"
		val url = bundle.getResource("tool/"+ Activator.getOperatingSystemType() +"/" + toolName +suffix)
		if(url === null) return null;
		val fileUrl = FileLocator.toFileURL(url);
		return fileUrl.file
	}
	
	def getDefaultTimeout() {
		val store = fr.irisa.atsyra.ide.ui.Activator.getDefault().getPreferenceStore();
		val time = store.getInt(PreferenceConstants.P_MAX_EXEC_TIME)
		return time;
	}
}	