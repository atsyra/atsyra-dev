/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.ide.ui.preferences;

import org.eclipse.jface.preference.*;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.IWorkbench;
import fr.irisa.atsyra.ide.ui.Activator;
import fr.irisa.atsyra.preferences.constants.AtsyraPreferenceConstants;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class ATSyRAComputationPreferencePage
	extends FieldEditorPreferencePage
	implements IWorkbenchPreferencePage {

	public ATSyRAComputationPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("ATSyRA computation preferences");
	}
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {
		
		addField(new IntegerFieldEditor(AtsyraPreferenceConstants.P_MANYWITNESS,
				"&Manywitness, compute up to xxx witnesses",
				getFieldEditorParent()));
		addField(new BooleanFieldEditor(AtsyraPreferenceConstants.P_MANYWITNESS_NON_MINIMAL,
				"&Non minimal length witnesses (uses --init-gadget)",
				getFieldEditorParent()));
		addField(new IntegerFieldEditor(AtsyraPreferenceConstants.P_STATE_PRINT_LIMIT,
				"&Print-limit, threshold for printout of states in witnesses", 
				getFieldEditorParent()));
		addField(new IntegerFieldEditor(AtsyraPreferenceConstants.P_MAX_EXEC_TIME,
				"Maximum e&xecution time",
				getFieldEditorParent()));
		addField(new BooleanFieldEditor(AtsyraPreferenceConstants.P_ENFORCE_DYNAMIC_CONTRACTS,
				"Enforce &dynamic contracts",
				getFieldEditorParent()));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}