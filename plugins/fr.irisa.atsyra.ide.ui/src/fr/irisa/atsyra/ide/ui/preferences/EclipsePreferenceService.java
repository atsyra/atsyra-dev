/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.ide.ui.preferences;

import java.util.Optional;

import org.eclipse.core.runtime.Platform;

import fr.irisa.atsyra.preferences.GlobalPreferenceService;
import fr.irisa.atsyra.preferences.PreferenceService;

public class EclipsePreferenceService implements PreferenceService {
	
	private static boolean registered = false;
	
	public static void register() {
		if(!registered) {
			registered = true;
			GlobalPreferenceService.INSTANCE.addPreferenceService(new EclipsePreferenceService());
		}
	}

	@Override
	public Optional<Integer> getInt(String qualifier, String key) {
		int preferenceValue = Platform.getPreferencesService().getInt(qualifier, key, 0, null);
		if(preferenceValue == Platform.getPreferencesService().getInt(qualifier, key, 1, null)) {
			return Optional.of(preferenceValue);
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Optional<Long> getLong(String qualifier, String key) {
		long preferenceValue = Platform.getPreferencesService().getLong(qualifier, key, 0, null);
		if(preferenceValue == Platform.getPreferencesService().getLong(qualifier, key, 1, null)) {
			return Optional.of(preferenceValue);
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Optional<String> getString(String qualifier, String key) {
		String preferenceValue = Platform.getPreferencesService().getString(qualifier, key, null, null);
		return Optional.ofNullable(preferenceValue);
	}

	@Override
	public Optional<Float> getFloat(String qualifier, String key) {
		float preferenceValue = Platform.getPreferencesService().getFloat(qualifier, key, 0, null);
		if(preferenceValue == Platform.getPreferencesService().getFloat(qualifier, key, 1, null)) {
			return Optional.of(preferenceValue);
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Optional<Double> getDouble(String qualifier, String key) {
		double preferenceValue = Platform.getPreferencesService().getDouble(qualifier, key, 0, null);
		if(preferenceValue == Platform.getPreferencesService().getDouble(qualifier, key, 1, null)) {
			return Optional.of(preferenceValue);
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Optional<Boolean> getBoolean(String qualifier, String key) {
		boolean preferenceValue = Platform.getPreferencesService().getBoolean(qualifier, key, false, null);
		if(preferenceValue == Platform.getPreferencesService().getBoolean(qualifier, key, true, null)) {
			return Optional.of(preferenceValue);
		} else {
			return Optional.empty();
		}
	}

}
