/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.ide.ui.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import fr.irisa.atsyra.ide.ui.Activator;
import fr.irisa.atsyra.preferences.constants.AtsyraPreferenceConstants;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(AtsyraPreferenceConstants.P_GENERATE_OPTIONAL_FILES, true);
		store.setDefault(AtsyraPreferenceConstants.P_PRINT_SIZE, "");
		store.setDefault(AtsyraPreferenceConstants.P_OVERRIDE_LOCALE, "");
		
		store.setDefault(AtsyraPreferenceConstants.P_MANYWITNESS, 10);
		store.setDefault(AtsyraPreferenceConstants.P_MANYWITNESS_NON_MINIMAL, true);
		store.setDefault(AtsyraPreferenceConstants.P_STATE_PRINT_LIMIT, 50);
		store.setDefault(AtsyraPreferenceConstants.P_MAX_EXEC_TIME, 60);
		store.setDefault(AtsyraPreferenceConstants.P_ENFORCE_DYNAMIC_CONTRACTS, true);
	}

}
