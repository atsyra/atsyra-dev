/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.ide.ui;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystemManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import fr.irisa.atsyra.ide.ui.preferences.EclipsePreferenceService;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "fr.irisa.atsyra.ide.ui"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		EclipsePreferenceService.register();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static final String IMAGE_LocalRefinement_NotPreciseEnough_PATH ="icons/icon-LocalRefinement-NotPreciseEnough.png";
	public static final String IMAGE_LocalRefinement_NotPreciseEnough_ID ="icon-LocalRefinement-NotPreciseEnough.png";
	public static final String IMAGE_LocalRefinement_PerfectMatch_PATH ="icons/icon-LocalRefinement-PerfectMatch.png";
	public static final String IMAGE_LocalRefinement_PerfectMatch_ID ="icon-LocalRefinement-PerfectMatch.png";
	public static final String IMAGE_LocalRefinement_TooStrong_PATH ="icons/icon-LocalRefinement-TooStrong.png";
	public static final String IMAGE_LocalRefinement_TooStrong_ID ="icon-LocalRefinement-TooStrong.png";
	public static final String IMAGE_LocalRefinement_Unrelated_PATH ="icons/icon-LocalRefinement-Unrelated.png";
	public static final String IMAGE_LocalRefinement_Unrelated_ID ="icon-LocalRefinement-Unrelated.png";
	public static final String IMAGE_LocalRefinement_Weak_PATH ="icons/icon-LocalRefinement-Weak.png";
	public static final String IMAGE_LocalRefinement_Weak_ID ="icon-LocalRefinement-Weak.png";
	
	protected void initializeImageRegistry(ImageRegistry registry) {
		// cf. https://wiki.eclipse.org/FAQ_How_do_I_create_an_image_registry_for_my_plug-in%3F
        Bundle bundle = Platform.getBundle(PLUGIN_ID);
        registry.put(IMAGE_LocalRefinement_NotPreciseEnough_ID,  
        		ImageDescriptor.createFromURL(
        				FileLocator.find(bundle, 
        						new Path(IMAGE_LocalRefinement_NotPreciseEnough_PATH), null)));
        registry.put(IMAGE_LocalRefinement_PerfectMatch_ID,  
        		ImageDescriptor.createFromURL(
        				FileLocator.find(bundle, 
        						new Path(IMAGE_LocalRefinement_PerfectMatch_PATH), null)));
        registry.put(IMAGE_LocalRefinement_TooStrong_ID,  
        		ImageDescriptor.createFromURL(
        				FileLocator.find(bundle, 
        						new Path(IMAGE_LocalRefinement_TooStrong_PATH), null)));
        registry.put(IMAGE_LocalRefinement_Unrelated_ID,  
        		ImageDescriptor.createFromURL(
        				FileLocator.find(bundle, 
        						new Path(IMAGE_LocalRefinement_Unrelated_PATH), null)));
        registry.put(IMAGE_LocalRefinement_Weak_ID,  
        		ImageDescriptor.createFromURL(
        				FileLocator.find(bundle, 
        						new Path(IMAGE_LocalRefinement_Weak_PATH), null)));
     }
	
	/**
	 * Use this when something went wrong in the plugin itself (internal warning)
	 * @param msg
	 * @param e
	 */
	public static void eclipseWarn(String msg, Throwable e){
		getMessagingSystem().warn(msg, msgGroup, e);
		getMessagingSystem().focus();
	}
	/**
	 * Use this when something went wrong in the plugin itself (internal error)
	 * @param msg
	 * @param e
	 */
	public static void eclipseError(String msg, Throwable e){
		getMessagingSystem().error(msg, msgGroup, e);
		getMessagingSystem().focus();
	}
	public static void debug(String msg){
		//System.out.println(msg);
		getMessagingSystem().debug(msg, msgGroup);
	}
	public static void error(String msg){
		getMessagingSystem().error(msg, msgGroup);
		getMessagingSystem().focus();
	}
	public static void info(String str) {
		getMessagingSystem().info(str, msgGroup);
		getMessagingSystem().focus();
	}
	public static void warn(String str) {
		getMessagingSystem().warn(str, msgGroup);
		getMessagingSystem().focus();
	}
	public static void important(String str) {
		getMessagingSystem().important(str, msgGroup);
		getMessagingSystem().focus();
	}
	
	public static String baseMsgGroup = "fr.irisa.atsyra";
	public static String msgGroup = baseMsgGroup+".ide.ui";
	protected static MessagingSystem messagingSystem = null;
	public static MessagingSystem getMessagingSystem() {
		if(messagingSystem == null) {
			MessagingSystemManager msm = new MessagingSystemManager();
			messagingSystem = msm.createBestPlatformMessagingSystem(baseMsgGroup, "ATSyRA");
		}
		return messagingSystem;
	}
}
