/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.ide.ui.preferences;

import fr.irisa.atsyra.preferences.constants.AtsyraPreferenceConstants;

/**
 * Constant definitions for plug-in preferences
 * Redirects to to {@link fr.irisa.atsyra.preferences.constants#AtsyraPreferenceConstants AtsyraPreferenceConstants} for compatibility.
 */
public class PreferenceConstants {

	public static final String P_GENERATE_OPTIONAL_FILES = AtsyraPreferenceConstants.P_GENERATE_OPTIONAL_FILES;
	public static final String P_OVERRIDE_LOCALE = AtsyraPreferenceConstants.P_OVERRIDE_LOCALE;
	
	public static final String P_MANYWITNESS = AtsyraPreferenceConstants.P_MANYWITNESS;
	public static final String P_MANYWITNESS_NON_MINIMAL = AtsyraPreferenceConstants.P_MANYWITNESS_NON_MINIMAL;
	public static final String P_STATE_PRINT_LIMIT = AtsyraPreferenceConstants.P_STATE_PRINT_LIMIT;
	public static final String P_MAX_EXEC_TIME = AtsyraPreferenceConstants.P_MAX_EXEC_TIME;
	public static final String P_ENFORCE_DYNAMIC_CONTRACTS = AtsyraPreferenceConstants.P_ENFORCE_DYNAMIC_CONTRACTS;
	
}
