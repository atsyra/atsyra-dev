package fr.irisa.atsyra.absystem.xtext.tests

import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.Guard

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class AnnotationTest extends AbstractSimpleABSTest{

	@Test
	def void group_annotation_should_be_inherited() {
		val absModel = simpleLoadResource("GroupAnnotationTest.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val testedAssetType = absModel.eAllContents.filter(AssetType).head
		Assertions.assertEquals(4, testedAssetType.allTags.size)
		val testedGuard = absModel.eAllContents.filter(Guard).head
		Assertions.assertEquals(4, testedGuard.allAnnotations.size)
		Assertions.assertEquals("value4,2", testedGuard.allAnnotations.findFirst[key.name == "key4"].value)
		Assertions.assertEquals("value2,2", testedGuard.allAnnotations.findFirst[key.name == "key2"].value)
	}
}
