package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.xtext.validation.AssetBasedSystemDslValidator
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABSFromTest extends AbstractSimpleABSTest{
	
	@Inject
	ValidationTestHelper validationTester

	@Test
	def void loadFromBase_Test() {
		val absModel = simpleLoadResource("FromTest_base.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel.eResource)
	}

	@Test
	def void loadFromG1_Test() {
		val absModel = simpleLoadResource("FromTest_g1.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel.eResource)
	}
	
	@Test
	def void loadFromG2_Test() {
		val absModel = simpleLoadResource("FromTest_g2.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel.eResource)
	}
	
	@Test
	def void loadFromG2_WarningTest() {
		val absModel = simpleLoadResource("FromTest_g2_warning.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertIssue(absModel.imports.get(1), AbsystemPackage.Literals.IMPORT,
			AssetBasedSystemDslValidator.INVALID_WITH, 23, 9, Severity.WARNING)
		validationTester.assertIssue(absModel.imports.get(2), AbsystemPackage.Literals.IMPORT,
			AssetBasedSystemDslValidator.INVALID_WITH, 135, 10, Severity.WARNING)
	}
}
