
package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator
import org.eclipse.emf.common.util.BasicDiagnostic
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.Diagnostician
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.jupiter.api.Assertions.*
import org.eclipse.emf.common.util.Diagnostic
import fr.irisa.atsyra.absystem.model.absystem.AssetType

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABSAttributeDefaultTest {
	// helper to directly parse strings 
	//@Inject
	//ParseHelper<AssetBasedSystem> parseHelper
	// Allows to obtain a new resource set
	@Inject 
	Provider<XtextResourceSet> resourceSetProvider
	// Helper to test all validation rules and ensure resolved links
	@Inject 
	ValidationTestHelper validationTester
	
	/**
	 *  load a single isolated, standalone resource in its own resourceSet
	 */
	def AssetBasedSystem simpleLoadResource(String testFileName) {		
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()
		
		val resourceSet = resourceSetProvider.get
		val res = resourceSet.createResource(URI.createFileURI("test-input-files/"+testFileName))		
		res.load(emptyMap)
		return res.contents.head as AssetBasedSystem
	}
	
	@Test
	def void attributeDefaultTest_01() {
		val absModel = simpleLoadResource("AttributeDefaultTest_01.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel)
		validationTester.assertNoErrors(absModel)
	}
	
	@Test
	def void attributeDefaultTypeErrorTest_01() {
		val absModel = simpleLoadResource("AttributeDefaultTypeErrorTest_01.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val t1AssetTypeAspect = absModel.definitionGroups
			.findFirst[d | d.name == "Behavior_def"]
			.assetTypes.filter(AssetTypeAspect)
			.findFirst[ata | ata.baseAssetType.name == "T1"]
		// all declared attribute must report an error
		t1AssetTypeAspect.assetTypeAttributes.forEach[att |
			assertTrue(!AbsystemValidator.INSTANCE.validateAssetTypeAttribute_DefaultType(att, null, newHashMap), "No error reported on attribute `"+att.name+"'")
		]
	}
	
	@Test
	def void attributeDefaultTest_02() {
		val absModel = simpleLoadResource("AttributeDefaultTest_02.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel)
		validationTester.assertNoErrors(absModel)
	}
	
	@Test
	def void attributeDefaultTypeErrorTest_02() {
		val absModel = simpleLoadResource("AttributeDefaultTypeErrorTest_02.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val t1AssetTypeAspect = absModel.definitionGroups
			.findFirst[d | d.name == "Behavior_def"]
			.assetTypes.filter(AssetTypeAspect)
			.findFirst[ata | ata.baseAssetType.name == "T1"]
		// all declared attribute must report an error
		t1AssetTypeAspect.assetTypeAttributes.forEach[att |
			assertTrue(!AbsystemValidator.INSTANCE.validateAssetTypeAttribute_DefaultType(att, null, newHashMap), "No error reported on attribute `"+att.name+"'")
		]
	}
	
	/* @Test
	def void attributeDefaultTypeErrorTest_03() {
		val absModel = simpleLoadResource("AttributeDefaultTypeErrorTest_03.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val t1AssetTypeAspect = absModel.definitionGroups
			.findFirst[d | d.name == "Behavior_def"]
			.assetTypes.filter(AssetTypeAspect)
			.findFirst[ata | ata.baseAssetType.name == "T1"]
		val issues = validationTester.validate(absModel)
		// all declared attribute must report an error
		t1AssetTypeAspect.assetTypeAttributes.forEach[att |
			val node = NodeModelUtils.findActualNodeFor(att);
			if (node !== null) {
				val offset = node.getOffset();
				val totalEndOffset = node.getTotalEndOffset(); 
				assertTrue(issues.exists[issue | 
					(issue.offset >= offset) && (issue.offset <= totalEndOffset) && 
					issue.code.equals(AssetBasedSystemDslValidator.INVALID_ATTRIBUTE_MULTIPLICITY 
					)
				], "No error reported on attribute `"+att.name+"'")
				//validationTester.assertError(att, AbsystemPackage.Literals.ASSET_TYPE_ATTRIBUTE, AssetBasedSystemDslValidator.INVALID_ATTRIBUTE_TYPE, offset, length)
			} else {
				fail("Cannot find attribute '"+att.name+"'")
			
			}
		]
			
		//validationTester.assertNoIssues(absModel)
		//validationTester.assertNoErrors(absModel)
	}*/
	
	@Test
	def void MultiplicityDefaultTest() {
		val absModel = simpleLoadResource("MultiplicityDefaultTest.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val t1AssetTypeAspect = absModel.definitionGroups
			.findFirst[d | d.name == "group1"]
			.assetTypes.filter(AssetType)
			.findFirst[ata | ata.name == "T1"]
		// all declared attribute must report an error
		t1AssetTypeAspect.assetTypeAttributes.forEach[att |
			assertTrue(!AbsystemValidator.INSTANCE.validateAssetTypeAttribute_DefaultMultiplicity(att, null, newHashMap), "No error reported on attribute `"+att.name+"'")
		]
	}
	
}
