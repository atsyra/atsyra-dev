package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class CycleTest {
	@Inject
	Provider<XtextResourceSet> resourceSetProvider

	def AssetBasedSystem simpleLoadResource(String testFileName) {
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()

		val resourceSet = resourceSetProvider.get
		val res = resourceSet.createResource(URI.createFileURI("test-input-files/" + testFileName))
		res.load(emptyMap)
		return res.contents.head as AssetBasedSystem
	}

	@Test
	def void detect_cycles_in_type_hierarchy() {
		val absModel = simpleLoadResource("cycleTest.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		for(type : absModel.definitionGroups.get(0).assetTypes.filter(AssetType)) {
			if(type.name != "T7") {
				Assertions.assertFalse(AbsystemValidator.INSTANCE.validateAssetType_NoExtendsCycle(type, null, newHashMap), "No error reported on AssetType `"+type.name+"'")
			}
		}
	}
}
