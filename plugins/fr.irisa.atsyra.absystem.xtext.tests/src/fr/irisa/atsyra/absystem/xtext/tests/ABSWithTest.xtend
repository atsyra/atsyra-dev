package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.xtext.validation.AssetBasedSystemDslValidator
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABSWithTest  extends AbstractSimpleABSTest {
	
	@Inject
	ValidationTestHelper validationTester

	@Test
	def void withSyntaxErrorTest() {
		val absModel = simpleLoadResource("WithTest.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel, AbsystemPackage.Literals.IMPORT,
			AssetBasedSystemDslValidator.INVALID_WITH, 23, 10, Severity.ERROR)
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.IMPORT,
			AssetBasedSystemDslValidator.INVALID_WITH, 39, 9, Severity.ERROR)
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.IMPORT,
			AssetBasedSystemDslValidator.INVALID_WITH, 55, 10, Severity.ERROR)
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.IMPORT,
			AssetBasedSystemDslValidator.INVALID_WITH, 72, 20, Severity.ERROR)
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.IMPORT,
			AssetBasedSystemDslValidator.INVALID_WITH, 99, 13, Severity.ERROR)
		validationTester.assertNoIssues(absModel, AbsystemPackage.Literals.IMPORT,
			AssetBasedSystemDslValidator.INVALID_WITH, 119, 21, Severity.ERROR)
	}

}
