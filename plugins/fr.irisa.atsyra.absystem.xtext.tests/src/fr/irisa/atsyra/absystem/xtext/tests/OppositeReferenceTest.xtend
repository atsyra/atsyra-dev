package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.xtext.validation.AssetBasedSystemDslValidator
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator
import org.eclipse.xtext.diagnostics.Severity

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class OppositeReferenceTest {
	// Allows to obtain a new resource set
	@Inject
	Provider<XtextResourceSet> resourceSetProvider
	// Helper to test all validation rules and ensure resolved links
	@Inject
	ValidationTestHelper validationTester

	/**
	 *  load a single isolated, standalone resource in its own resourceSet
	 */
	def AssetBasedSystem simpleLoadResource(String testFileName) {
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()

		val resourceSet = resourceSetProvider.get
		val res = resourceSet.createResource(URI.createFileURI("test-input-files/" + testFileName))

		EcoreUtil2.resolveAll(res)
		res.load(emptyMap)
		return res.contents.head as AssetBasedSystem
	}

	@Test
	def void oppositeReferenceTest_01() {
		val absModel = simpleLoadResource("OppositeReferenceTest_01.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel)
		validationTester.assertNoErrors(absModel)
	}

	@Test
	def void oppositeReferenceTest_01b() {
		val absModel = simpleLoadResource("OppositeReferenceTest_01b.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel)
		validationTester.assertNoErrors(absModel)
	}

	@Test
	def void oppositeReferenceTest_02() {
		val absModel = simpleLoadResource("OppositeReferenceTest_02.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel)
		validationTester.assertNoErrors(absModel)
	}

	@Test
	def void oppositeReferenceTest_incorrect_opposite_01() {
		val absModel = simpleLoadResource("OppositeReferenceTest_validation_01.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')

		absModel.definitionGroups.get(0).assetTypes.get(1).assetTypeProperties.get(0)

		validationTester.assertError(absModel.definitionGroups.get(0).assetTypes.get(1).assetTypeProperties.get(0),
			AbsystemPackage.Literals.ASSET_TYPE_REFERENCE, AssetBasedSystemDslValidator.INVALID_OPPOSITE_REFERENCE, 220,
			2, "Invalid opposite definition: a2  doesn't declare a1 as opposite")

	}

	@Test
	def void oppositeReferenceTest_incorrect_opposite_02() {
		val absModel = simpleLoadResource("OppositeReferenceTest_validation_02.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')

		absModel.definitionGroups.get(0).assetTypes.get(1).assetTypeProperties.get(0)

		validationTester.assertError(absModel, AbsystemPackage.Literals.ASSET_TYPE_REFERENCE,
			AssetBasedSystemDslValidator.INVALID_OPPOSITE_REFERENCE, 239, 2,
			"Invalid opposite definition: a2  doesn't declare a1 as opposite")

	}

	@Test
	def void oppositeReferenceTest_assetvalidation_01() {
		val absModel = simpleLoadResource("OppositeReferenceTest_assetvalidation_01.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected load errors: «errors.join(", ")»''')

		validationTester.assertIssue(absModel, AbsystemPackage.Literals.ASSET_LINK,
			AssetBasedSystemDslValidator.DUPLICATED_ASSET_LINK, 349, 6, Severity.ERROR)
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.ASSET_LINK,
			AssetBasedSystemDslValidator.DUPLICATED_ASSET_LINK, 359, 29, Severity.ERROR)

	}

	@Test
	def void oppositeReferenceTest_assetvalidation_02() {
		val absModel = simpleLoadResource("OppositeReferenceTest_assetvalidation_02.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected load errors: «errors.join(", ")»''')

		validationTester.assertIssue(absModel, AbsystemPackage.Literals.ASSET_LINK,
			AssetBasedSystemDslValidator.DUPLICATED_ASSET_LINK, 325, 4, Severity.ERROR)
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.ASSET_LINK,
			AssetBasedSystemDslValidator.DUPLICATED_ASSET_LINK, 354, 4, Severity.ERROR)

	}
	
	@Test
	def void oppositeReferenceTest_assetvalidation_03() {
		val absModel = simpleLoadResource("OppositeReferenceTest_assetvalidation_03.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected load errors: «errors.join(", ")»''')

		validationTester.assertIssue(absModel, AbsystemPackage.Literals.ASSET_LINK,
			AssetBasedSystemDslValidator.INVALID_OPPOSITE_REFERENCE, 341, 13, Severity.ERROR)

	}
	
		@Test
	def void oppositeReferenceTest_assetvalidation_04() {
		val absModel = simpleLoadResource("OppositeReferenceTest_assetvalidation_04.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected load errors: «errors.join(", ")»''')

		validationTester.assertIssue(absModel, AbsystemPackage.Literals.ASSET_LINK,
			AssetBasedSystemDslValidator.MISSING_OPPOSITE_LINK, 320, 29, Severity.WARNING)

	}
	

}
