
package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.Asset
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator
import java.util.Map
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABSAssetValidationTest {
	// Allows to obtain a new resource set
	@Inject 
	Provider<XtextResourceSet> resourceSetProvider
	
	/**
	 *  load a single isolated, standalone resource in its own resourceSet
	 */
	def AssetBasedSystem simpleLoadResource(String testFileName) {		
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()
		
		val resourceSet = resourceSetProvider.get
		val res = resourceSet.createResource(URI.createFileURI("test-input-files/"+testFileName))		
		res.load(emptyMap)
		return res.contents.head as AssetBasedSystem
	}
	
	@Test
	def void asset_may_not_instantiate_() {
		val absModel = simpleLoadResource("AbstractAssetTypeMayNotBeInstantiatedValidationTest.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val toTest = absModel.eResource.allContents.filter(Asset).head
		Assertions.assertFalse(AbsystemValidator.INSTANCE.validateAsset_TypeIsNotAbstract(toTest , null, Map.of))
	}
	
}
