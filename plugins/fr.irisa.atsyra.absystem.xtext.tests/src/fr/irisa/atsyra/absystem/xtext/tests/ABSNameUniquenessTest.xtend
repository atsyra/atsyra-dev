package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.xtext.validation.AssetBasedSystemDslValidator
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABSNameUniquenessTest {
	@Inject
	Provider<XtextResourceSet> resourceSetProvider

	@Inject
	ValidationTestHelper validationTester

	def AssetBasedSystem simpleLoadResource(String testFileName) {
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()

		val resourceSet = resourceSetProvider.get
		val res = resourceSet.createResource(URI.createFileURI("test-input-files/" + testFileName))
		res.load(emptyMap)
		return res.contents.head as AssetBasedSystem
	}

	@Test
	def void should_print_error_on_duplicated_guarded_action_name() {
		val absModel = simpleLoadResource("NameUniquenessTest_01.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.DEFINITION_GROUP,
			AssetBasedSystemDslValidator.DUPLICATED_GUARDED_ACTION_NAME, Severity.ERROR)
	}

	@Test
	def void should_print_error_on_duplicated_asset_type_name() {
		val absModel = simpleLoadResource("NameUniquenessTest_01.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.DEFINITION_GROUP,
			AssetBasedSystemDslValidator.DUPLICATED_ASSET_TYPE_NAME, Severity.ERROR)
	}

	@Test
	def void should_print_error_on_duplicated_asset_name() {
		val absModel = simpleLoadResource("NameUniquenessTest_01.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.ASSET_GROUP,
			AssetBasedSystemDslValidator.DUPLICATED_ASSET_NAME, Severity.ERROR)
	}

	@Test
	def void should_print_error_on_duplicated_goal_name() {
		val absModel = simpleLoadResource("NameUniquenessTest_01.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertIssue(absModel, AbsystemPackage.Literals.ASSET_GROUP,
			AssetBasedSystemDslValidator.DUPLICATED_GOAL_NAME, Severity.ERROR)
	}
}
