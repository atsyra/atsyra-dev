package fr.irisa.atsyra.absystem.xtext.tests


import com.google.inject.Inject
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.emf.common.util.URI

abstract class AbstractSimpleABSTest {
	// Allows to obtain a new resource set
	@Inject
	Provider<XtextResourceSet> resourceSetProvider
	
	
	/**
	 *  load a single isolated, standalone resource in its own resourceSet
	 */
	def AssetBasedSystem simpleLoadResource(String testFileName) {
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()


		new org.eclipse.emf.mwe.utils.StandaloneSetup().setPlatformUri("../");
		val resourceSet = resourceSetProvider.get
		val res = resourceSet.createResource(URI.createFileURI("test-input-files/" + testFileName))
		res.load(emptyMap)
		return res.contents.head as AssetBasedSystem
	}
}