
package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABSAttributeInAssetTest {
	// Allows to obtain a new resource set
	@Inject 
	Provider<XtextResourceSet> resourceSetProvider
	
	/**
	 *  load a single isolated, standalone resource in its own resourceSet
	 */
	def AssetBasedSystem simpleLoadResource(String testFileName) {		
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()
		
		val resourceSet = resourceSetProvider.get
		val res = resourceSet.createResource(URI.createFileURI("test-input-files/"+testFileName))		
		res.load(emptyMap)
		return res.contents.head as AssetBasedSystem
	}
	
	
	@Test
	def void attributeInAssetWarningTest_01() {
		val absModel = simpleLoadResource("AttributeInAssetWarningTest_01.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')

		val toTest = absModel.assetGroups.get(0).assets.get(1).assetAttributeValues.get(0)
		Assertions.assertFalse(AbsystemValidator.INSTANCE.validateAssetAttributeValue_NoDynamicAttributeSet(toTest, null, newHashMap))
	}
	
	@Test
	def void attributeInAssetErrorTest_01() {
		val absModel = simpleLoadResource("AttributeInAssetErrorTest_01.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')

		val toTest = absModel.assetGroups.get(0).assets.get(1).assetAttributeValues.get(0)
		Assertions.assertFalse(AbsystemValidator.INSTANCE.validateAssetAttributeValue_NoDuplicateAttribute(toTest, null, newHashMap))
	}
	
}
