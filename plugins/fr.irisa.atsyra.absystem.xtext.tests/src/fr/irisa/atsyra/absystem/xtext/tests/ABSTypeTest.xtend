package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator
import org.eclipse.emf.common.util.BasicDiagnostic
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABSTypeTest {
	// Allows to obtain a new resource set
	@Inject 
	Provider<XtextResourceSet> resourceSetProvider

	/**
	 *  load a single isolated, standalone resource in its own resourceSet
	 */
	def AssetBasedSystem simpleLoadResource(String testFileName) {		
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()
		
		val resourceSet = resourceSetProvider.get
		val res = resourceSet.createResource(URI.createFileURI("test-input-files/"+testFileName))		
		res.load(emptyMap)
		return res.contents.head as AssetBasedSystem
	}
	
	@Test
	def void typeTest() {
		val absModel = simpleLoadResource("TypeTest.abs")
		
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val issuesChain = new BasicDiagnostic
		absModel.eAllContents.forEach[AbsystemValidator.INSTANCE.validate(it, issuesChain, newHashMap)]
		val issues = issuesChain.children
       	Assertions.assertEquals(11, issues.size)
  		issues.forEach[Assertions.assertEquals(it.code, AbsystemValidator.EXPRESSION__WRONG_TYPE)]
	}
	
	
}
