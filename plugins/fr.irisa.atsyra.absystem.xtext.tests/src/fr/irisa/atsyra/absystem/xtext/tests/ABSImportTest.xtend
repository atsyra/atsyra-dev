package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABSImportTest extends AbstractSimpleABSTest {

	@Inject
	ValidationTestHelper validationTester

	@Test
	def void loadImportBase_Test() {
		val absModel = simpleLoadResource("ImportTest_base.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel.eResource)
	}

	@Test
	def void loadImportG1_Test() {
		val absModel = simpleLoadResource("ImportTest_g1.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel.eResource)
	}
	
	@Test
	def void loadImportG2_Test() {
		val absModel = simpleLoadResource("ImportTest_g2.abs")
		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		validationTester.assertNoIssues(absModel.eResource)
	}

}
