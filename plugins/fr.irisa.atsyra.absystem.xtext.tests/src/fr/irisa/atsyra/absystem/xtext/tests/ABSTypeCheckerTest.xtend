package fr.irisa.atsyra.absystem.xtext.tests

import com.google.inject.Inject
import com.google.inject.Provider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.xtext.validation.AssetBasedSystemDslValidator
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import fr.irisa.atsyra.absystem.model.absystem.util.AbsystemValidator
import fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression
import static org.junit.jupiter.api.Assertions.*
import fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression
import fr.irisa.atsyra.absystem.model.absystem.AndExpression
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection
import fr.irisa.atsyra.absystem.model.absystem.OrExpression

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABSTypeCheckerTest {
	@Inject
	Provider<XtextResourceSet> resourceSetProvider

	def AssetBasedSystem simpleLoadResource(String testFileName) {
		// enforce EPackage registration
		AbsystemPackage.eINSTANCE.eClass()

		val resourceSet = resourceSetProvider.get
		val res = resourceSet.createResource(URI.createFileURI("test-input-files/" + testFileName))
		res.load(emptyMap)
		return res.contents.head as AssetBasedSystem
	}

	@Test
	def void inequality_type_errors() {
		val absModel = simpleLoadResource("ComparisonTypeTest.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		
		val toTest = absModel.definitionGroups.findFirst[name == "Behavior_def"].guardedActions.get(0).guardExpression as InequalityComparisonExpression
		assertFalse(AbsystemValidator.INSTANCE.validateInequalityComparisonExpression_ComparableTypes(toTest , null, newHashMap))
	}
	
	@Test
	def void inequality_isnotmany_errors() {
		val absModel = simpleLoadResource("InequalityNotManyTest.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val toTest = absModel.definitionGroups.findFirst[name == "Behavior_def"].guardedActions.get(0).guardExpression as InequalityComparisonExpression
		assertFalse(AbsystemValidator.INSTANCE.validateInequalityComparisonExpression_NoCollections(toTest, null, newHashMap))
	}
	
	@Test
	def void equality_isnotmany_errors() {
		val absModel = simpleLoadResource("EqualityNotManyTest.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val toTest = absModel.definitionGroups.findFirst[name == "Behavior_def"].guardedActions.get(0).guardExpression as EqualityComparisonExpression
		assertFalse(AbsystemValidator.INSTANCE.validateEqualityComparisonExpression_NoCollections(toTest, null, newHashMap))
	}
	
	@Test
	def void action_vs_collections_errors() {
		val absModel = simpleLoadResource("ActionVsCollectionsTest.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		absModel.definitionGroups.findFirst[name == "Behavior_def"].guardedActions.get(0).guardActions.forEach[
			assertFalse(AbsystemValidator.INSTANCE.validateAction_TargetMultiplicity(it, null, newHashMap))
		]
	}
	
	@Test
	def void action_args_errors() {
		val absModel = simpleLoadResource("ActionArgsValidationTest.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		absModel.definitionGroups.findFirst[name == "Behavior_def"].guardedActions.get(0).guardActions.forEach[
			assertFalse(AbsystemValidator.INSTANCE.validateAction_ArgMultiplicity(it, null, newHashMap))
		]
	}
	
	@Test
	def void memberselection_args_errors() {
		val absModel = simpleLoadResource("MemberSelectionArgsValidationTest.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val guardExpression = absModel.definitionGroups.findFirst[name == "Behavior_def"].guardedActions.get(0).guardExpression as AndExpression
		val toTest1 = (guardExpression.rhs as MemberSelection).receiver as MemberSelection
		val toTest2 = (guardExpression.lhs as AndExpression).rhs as MemberSelection
		val toTest3 = ((guardExpression.lhs as AndExpression).lhs as AndExpression).rhs as MemberSelection
		val toTest4 = ((guardExpression.lhs as AndExpression).lhs as AndExpression).lhs as MemberSelection
		assertFalse(AbsystemValidator.INSTANCE.validateMemberSelection_ArgMultiplicity(toTest1, null, newHashMap))
		assertFalse(AbsystemValidator.INSTANCE.validateMemberSelection_ArgMultiplicity(toTest2, null, newHashMap))
		assertFalse(AbsystemValidator.INSTANCE.validateMemberSelection_ArgMultiplicity(toTest3, null, newHashMap))
		assertFalse(AbsystemValidator.INSTANCE.validateMemberSelection_ArgMultiplicity(toTest4, null, newHashMap))
	}
	
	@Test
	def void equality_type_test_errors() {
		val absModel = simpleLoadResource("EqualityComparisonTypeTest.abs")

		Assertions.assertNotNull(absModel)
		val errors = absModel.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val guardExpression = absModel.definitionGroups.get(0).contracts.get(0).guardExpression as OrExpression
		assertFalse(AbsystemValidator.INSTANCE.validateEqualityComparisonExpression_ComparableTypes(guardExpression.lhs as EqualityComparisonExpression, null, newHashMap))
		assertTrue(AbsystemValidator.INSTANCE.validateEqualityComparisonExpression_ComparableTypes(guardExpression.rhs as EqualityComparisonExpression, null, newHashMap))
	}
}
