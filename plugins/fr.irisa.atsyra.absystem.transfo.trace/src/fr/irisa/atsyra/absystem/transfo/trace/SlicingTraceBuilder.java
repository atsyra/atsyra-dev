/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.trace;

import java.util.ListIterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;

public class SlicingTraceBuilder extends AbstractTraceBuilder {

	public SlicingTraceBuilder(TransfoTraceModel traceModel) {
		super(traceModel);
	}

	@Override
	public String getTransfoID() {
		return "fr.irisa.atsyra.absystem.transfo.slicing";
	}
	
	public void removeAllLinksWithContents(EObject eobj) {
		eobj.eAllContents().forEachRemaining(this::removeLinkTo);
		removeLinkTo(eobj);
	}
	
	public void removeLinkTo(EObject eobj) {
		for(ListIterator<Link> iterator = getLinks().listIterator(); iterator.hasNext();) {
			Link next = iterator.next();
			if(next.getTargets().size() == 1 && next.getTargets().get(0) instanceof EObjectRef && EcoreUtil.equals(((EObjectRef) next.getTargets().get(0)).getValue(), eobj)) {
				iterator.remove();
			}
		}
	}
	
}
