/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.trace;

import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;

public class UndefControlTraceBuilder extends AbstractTraceBuilder {

	public UndefControlTraceBuilder(TransfoTraceModel traceModel) {
		super(traceModel);
	}

	@Override
	public String getTransfoID() {
		return "fr.irisa.atsyra.absystem.transfo.undef";
	}
	
	
	
}
