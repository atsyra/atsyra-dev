/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.trace;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;

public class MergeTraceBuilder extends AbstractTraceBuilder {

	public static final String TRANSFO_ID = "fr.irisa.atsyra.absystem.transfo.merge";

	public MergeTraceBuilder(TransfoTraceModel traceModel) {
		super(traceModel);
	}

	@Override
	public String getTransfoID() {
		return TRANSFO_ID;
	}
	
	public void createAssetBasedSystemLink(Iterable<AssetBasedSystem> origins, AssetBasedSystem target) {
		Link link = newLink("");
		for(AssetBasedSystem origin : origins) {
			link.getOrigins().add(newRef(origin));
		}
		link.getTargets().add(newRef(target));
		addLink(link);
	}
	
	public void fromMap(Map<EObject, EObject> map) {
		map.forEach((origin, target) -> {
			Link link = newLink("");
			link.getOrigins().add(newRef(origin));
			link.getTargets().add(newRef(target));
			addLink(link);
		});
	}
	
}
