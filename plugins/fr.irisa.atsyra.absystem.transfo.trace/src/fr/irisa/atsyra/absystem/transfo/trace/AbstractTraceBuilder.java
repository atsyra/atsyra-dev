/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.trace;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Ref;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.StringRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotraceFactory;

public abstract class AbstractTraceBuilder {
	private TransfoTraceModel traceModel;
	
	protected AbstractTraceBuilder(TransfoTraceModel traceModel) {
		this.traceModel = traceModel;
	}
	
	public TransfoTraceModel getTraceModel() {
		return traceModel;
	}
	
	protected Ref newRef(EObject eobject) {
		EObjectRef result = TransfotraceFactory.eINSTANCE.createEObjectRef();
		result.setValue(eobject);
		return result;
	}
	
	protected Ref newRef(int value) {
		IntRef result = TransfotraceFactory.eINSTANCE.createIntRef();
		result.setValue(value);
		return result;
	}
	
	protected Ref newRef(String value) {
		StringRef result = TransfotraceFactory.eINSTANCE.createStringRef();
		result.setValue(value);
		return result;
	}
	
	public abstract String getTransfoID();
	
	/**
	 * Create a new link with a name and a tranfoID set. 
	 * The origins and targets still have to be set and the link have to be added to the tracemodel in order for the link to be completely handled.
	 * @param name the name of the link
	 * @return a new link to be completed and added to the trace model
	 */
	protected Link newLink(String name) {
		Link link = TransfotraceFactory.eINSTANCE.createLink();
		link.setName(name);
		link.setTransfoID(getTransfoID());
		return link;
	}
	
	protected void addLink(Link link) {
		if(traceModel != null) {
			traceModel.getLinks().add(link);
		}
	}
	
	protected List<Link> getLinks() {
		if(traceModel == null) {
			return List.of();
		} else {
			return traceModel.getLinks();
		}
	}
}
