/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.transfo.trace;

import java.util.Optional;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.atsyra.absystem.model.absystem.State;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;

public class ABS2ABSTraceQuery {
	private TransfoTraceModel traceModel;

	public ABS2ABSTraceQuery(TransfoTraceModel traceModel) {
		this.traceModel = traceModel;
	}

	public static ABS2ABSTraceQuery of(TransfoTraceModel traceModel) {
		return new ABS2ABSTraceQuery(traceModel);
	}

	public EObject getOrigin(EObject eobj) {
		EObject current = eobj;
		Optional<EObject> previous = getPrevious(current);
		while(previous.isPresent()) {
			current = previous.orElseThrow();
			previous = getPrevious(current);
		}
		return current;
	}
	
	public EObject getFinal(EObject eobj) {
		EObject current = eobj;
		Optional<EObject> next = getNext(current);
		while(next.isPresent()) {
			current = next.orElseThrow();
			next = getNext(current);
		}
		return current;
	}

	public Optional<EObject> getPrevious(EObject eobj) {
		return traceModel.getLinks().stream()
				.filter(link -> link.getOrigins().size() == 1 && link.getOrigins().get(0) instanceof EObjectRef
						&& ((EObjectRef) link.getOrigins().get(0)).getValue().eClass() == eobj.eClass()
						&& link.getTargets().stream().filter(EObjectRef.class::isInstance).map(EObjectRef.class::cast).anyMatch(ref -> ref.getValue() == eobj))
				.map(link -> ((EObjectRef) link.getOrigins().get(0)).getValue()).findAny();
	}

	public Optional<EObject> getNext(EObject eobj) {
		return traceModel.getLinks().stream()
				.filter(link -> link.getOrigins().stream().filter(EObjectRef.class::isInstance).map(EObjectRef.class::cast).anyMatch(ref -> ref.getValue() == eobj)
						&& link.getTargets().size() == 1 && link.getTargets().get(0) instanceof EObjectRef
						&& ((EObjectRef) link.getTargets().get(0)).getValue().eClass() == eobj.eClass())
				.map(link -> ((EObjectRef) link.getTargets().get(0)).getValue()).findAny();
	}
	
	public State getTransformedState(State state) {
		return state.transform(this::getFinal);
	}
	
	public State getRevertedState(State state) {
		return state.transform(this::getOrigin);
	}
}
