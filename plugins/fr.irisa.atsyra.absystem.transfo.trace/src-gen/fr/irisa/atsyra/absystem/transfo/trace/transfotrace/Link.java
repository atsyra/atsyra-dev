/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.transfo.trace.transfotrace;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getOrigins <em>Origins</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getTargets <em>Targets</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getTransfoID <em>Transfo ID</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage#getLink()
 * @model
 * @generated
 */
public interface Link extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage#getLink_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Origins</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Ref}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origins</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage#getLink_Origins()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Ref> getOrigins();

	/**
	 * Returns the value of the '<em><b>Targets</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Ref}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Targets</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage#getLink_Targets()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Ref> getTargets();

	/**
	 * Returns the value of the '<em><b>Transfo ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transfo ID</em>' attribute.
	 * @see #setTransfoID(String)
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage#getLink_TransfoID()
	 * @model
	 * @generated
	 */
	String getTransfoID();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getTransfoID <em>Transfo ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transfo ID</em>' attribute.
	 * @see #getTransfoID()
	 * @generated
	 */
	void setTransfoID(String value);

} // Link
