/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.transfo.trace.transfotrace;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotraceFactory
 * @model kind="package"
 * @generated
 */
public interface TransfotracePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "transfotrace";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/atsyra/transfotrace";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "transfotrace";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TransfotracePackage eINSTANCE = fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfoTraceModelImpl <em>Transfo Trace Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfoTraceModelImpl
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getTransfoTraceModel()
	 * @generated
	 */
	int TRANSFO_TRACE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFO_TRACE_MODEL__LINKS = 0;

	/**
	 * The number of structural features of the '<em>Transfo Trace Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFO_TRACE_MODEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Transfo Trace Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFO_TRACE_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.LinkImpl <em>Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.LinkImpl
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getLink()
	 * @generated
	 */
	int LINK = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__NAME = 0;

	/**
	 * The feature id for the '<em><b>Origins</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__ORIGINS = 1;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__TARGETS = 2;

	/**
	 * The feature id for the '<em><b>Transfo ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__TRANSFO_ID = 3;

	/**
	 * The number of structural features of the '<em>Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.RefImpl <em>Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.RefImpl
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getRef()
	 * @generated
	 */
	int REF = 2;

	/**
	 * The number of structural features of the '<em>Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.IntRefImpl <em>Int Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.IntRefImpl
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getIntRef()
	 * @generated
	 */
	int INT_REF = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_REF__VALUE = REF_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_REF_FEATURE_COUNT = REF_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Int Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_REF_OPERATION_COUNT = REF_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.EObjectRefImpl <em>EObject Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.EObjectRefImpl
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getEObjectRef()
	 * @generated
	 */
	int EOBJECT_REF = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_REF__VALUE = REF_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EObject Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_REF_FEATURE_COUNT = REF_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>EObject Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_REF_OPERATION_COUNT = REF_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.StringRefImpl <em>String Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.StringRefImpl
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getStringRef()
	 * @generated
	 */
	int STRING_REF = 5;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_REF__VALUE = REF_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_REF_FEATURE_COUNT = REF_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>String Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_REF_OPERATION_COUNT = REF_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel <em>Transfo Trace Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transfo Trace Model</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel
	 * @generated
	 */
	EClass getTransfoTraceModel();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel#getLinks <em>Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Links</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel#getLinks()
	 * @see #getTransfoTraceModel()
	 * @generated
	 */
	EReference getTransfoTraceModel_Links();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link <em>Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Link</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link
	 * @generated
	 */
	EClass getLink();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getName()
	 * @see #getLink()
	 * @generated
	 */
	EAttribute getLink_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getOrigins <em>Origins</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Origins</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getOrigins()
	 * @see #getLink()
	 * @generated
	 */
	EReference getLink_Origins();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Targets</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getTargets()
	 * @see #getLink()
	 * @generated
	 */
	EReference getLink_Targets();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getTransfoID <em>Transfo ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transfo ID</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link#getTransfoID()
	 * @see #getLink()
	 * @generated
	 */
	EAttribute getLink_TransfoID();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Ref <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Ref
	 * @generated
	 */
	EClass getRef();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef <em>Int Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef
	 * @generated
	 */
	EClass getIntRef();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.IntRef#getValue()
	 * @see #getIntRef()
	 * @generated
	 */
	EAttribute getIntRef_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef <em>EObject Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EObject Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef
	 * @generated
	 */
	EClass getEObjectRef();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef#getValue()
	 * @see #getEObjectRef()
	 * @generated
	 */
	EReference getEObjectRef_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.StringRef <em>String Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Ref</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.StringRef
	 * @generated
	 */
	EClass getStringRef();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.StringRef#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.StringRef#getValue()
	 * @see #getStringRef()
	 * @generated
	 */
	EAttribute getStringRef_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TransfotraceFactory getTransfotraceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfoTraceModelImpl <em>Transfo Trace Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfoTraceModelImpl
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getTransfoTraceModel()
		 * @generated
		 */
		EClass TRANSFO_TRACE_MODEL = eINSTANCE.getTransfoTraceModel();

		/**
		 * The meta object literal for the '<em><b>Links</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFO_TRACE_MODEL__LINKS = eINSTANCE.getTransfoTraceModel_Links();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.LinkImpl <em>Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.LinkImpl
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getLink()
		 * @generated
		 */
		EClass LINK = eINSTANCE.getLink();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINK__NAME = eINSTANCE.getLink_Name();

		/**
		 * The meta object literal for the '<em><b>Origins</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK__ORIGINS = eINSTANCE.getLink_Origins();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK__TARGETS = eINSTANCE.getLink_Targets();

		/**
		 * The meta object literal for the '<em><b>Transfo ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINK__TRANSFO_ID = eINSTANCE.getLink_TransfoID();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.RefImpl <em>Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.RefImpl
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getRef()
		 * @generated
		 */
		EClass REF = eINSTANCE.getRef();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.IntRefImpl <em>Int Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.IntRefImpl
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getIntRef()
		 * @generated
		 */
		EClass INT_REF = eINSTANCE.getIntRef();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_REF__VALUE = eINSTANCE.getIntRef_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.EObjectRefImpl <em>EObject Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.EObjectRefImpl
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getEObjectRef()
		 * @generated
		 */
		EClass EOBJECT_REF = eINSTANCE.getEObjectRef();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_REF__VALUE = eINSTANCE.getEObjectRef_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.StringRefImpl <em>String Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.StringRefImpl
		 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.TransfotracePackageImpl#getStringRef()
		 * @generated
		 */
		EClass STRING_REF = eINSTANCE.getStringRef();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_REF__VALUE = eINSTANCE.getStringRef_Value();

	}

} //TransfotracePackage
