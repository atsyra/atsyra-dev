/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl;

import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Ref;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.LinkImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.LinkImpl#getOrigins <em>Origins</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.LinkImpl#getTargets <em>Targets</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl.LinkImpl#getTransfoID <em>Transfo ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LinkImpl extends MinimalEObjectImpl.Container implements Link {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOrigins() <em>Origins</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrigins()
	 * @generated
	 * @ordered
	 */
	protected EList<Ref> origins;

	/**
	 * The cached value of the '{@link #getTargets() <em>Targets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargets()
	 * @generated
	 * @ordered
	 */
	protected EList<Ref> targets;

	/**
	 * The default value of the '{@link #getTransfoID() <em>Transfo ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransfoID()
	 * @generated
	 * @ordered
	 */
	protected static final String TRANSFO_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTransfoID() <em>Transfo ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransfoID()
	 * @generated
	 * @ordered
	 */
	protected String transfoID = TRANSFO_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransfotracePackage.Literals.LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransfotracePackage.LINK__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Ref> getOrigins() {
		if (origins == null) {
			origins = new EObjectContainmentEList<Ref>(Ref.class, this, TransfotracePackage.LINK__ORIGINS);
		}
		return origins;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Ref> getTargets() {
		if (targets == null) {
			targets = new EObjectContainmentEList<Ref>(Ref.class, this, TransfotracePackage.LINK__TARGETS);
		}
		return targets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTransfoID() {
		return transfoID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransfoID(String newTransfoID) {
		String oldTransfoID = transfoID;
		transfoID = newTransfoID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransfotracePackage.LINK__TRANSFO_ID, oldTransfoID, transfoID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TransfotracePackage.LINK__ORIGINS:
				return ((InternalEList<?>)getOrigins()).basicRemove(otherEnd, msgs);
			case TransfotracePackage.LINK__TARGETS:
				return ((InternalEList<?>)getTargets()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransfotracePackage.LINK__NAME:
				return getName();
			case TransfotracePackage.LINK__ORIGINS:
				return getOrigins();
			case TransfotracePackage.LINK__TARGETS:
				return getTargets();
			case TransfotracePackage.LINK__TRANSFO_ID:
				return getTransfoID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransfotracePackage.LINK__NAME:
				setName((String)newValue);
				return;
			case TransfotracePackage.LINK__ORIGINS:
				getOrigins().clear();
				getOrigins().addAll((Collection<? extends Ref>)newValue);
				return;
			case TransfotracePackage.LINK__TARGETS:
				getTargets().clear();
				getTargets().addAll((Collection<? extends Ref>)newValue);
				return;
			case TransfotracePackage.LINK__TRANSFO_ID:
				setTransfoID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransfotracePackage.LINK__NAME:
				setName(NAME_EDEFAULT);
				return;
			case TransfotracePackage.LINK__ORIGINS:
				getOrigins().clear();
				return;
			case TransfotracePackage.LINK__TARGETS:
				getTargets().clear();
				return;
			case TransfotracePackage.LINK__TRANSFO_ID:
				setTransfoID(TRANSFO_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransfotracePackage.LINK__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case TransfotracePackage.LINK__ORIGINS:
				return origins != null && !origins.isEmpty();
			case TransfotracePackage.LINK__TARGETS:
				return targets != null && !targets.isEmpty();
			case TransfotracePackage.LINK__TRANSFO_ID:
				return TRANSFO_ID_EDEFAULT == null ? transfoID != null : !TRANSFO_ID_EDEFAULT.equals(transfoID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", transfoID: ");
		result.append(transfoID);
		result.append(')');
		return result.toString();
	}

} //LinkImpl
