/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.transfo.trace.transfotrace;

import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EObject Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage#getEObjectRef()
 * @model
 * @generated
 */
public interface EObjectRef extends Ref {

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #setValue(EObject)
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage#getEObjectRef_Value()
	 * @model
	 * @generated
	 */
	EObject getValue();

	/**
	 * Sets the value of the '{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.EObjectRef#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(EObject value);
} // EObjectRef
