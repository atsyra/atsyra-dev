/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.transfo.trace.transfotrace.impl;

import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TransfotraceFactoryImpl extends EFactoryImpl implements TransfotraceFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TransfotraceFactory init() {
		try {
			TransfotraceFactory theTransfotraceFactory = (TransfotraceFactory)EPackage.Registry.INSTANCE.getEFactory(TransfotracePackage.eNS_URI);
			if (theTransfotraceFactory != null) {
				return theTransfotraceFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TransfotraceFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransfotraceFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TransfotracePackage.TRANSFO_TRACE_MODEL: return createTransfoTraceModel();
			case TransfotracePackage.LINK: return createLink();
			case TransfotracePackage.INT_REF: return createIntRef();
			case TransfotracePackage.EOBJECT_REF: return createEObjectRef();
			case TransfotracePackage.STRING_REF: return createStringRef();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransfoTraceModel createTransfoTraceModel() {
		TransfoTraceModelImpl transfoTraceModel = new TransfoTraceModelImpl();
		return transfoTraceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Link createLink() {
		LinkImpl link = new LinkImpl();
		return link;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntRef createIntRef() {
		IntRefImpl intRef = new IntRefImpl();
		return intRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObjectRef createEObjectRef() {
		EObjectRefImpl eObjectRef = new EObjectRefImpl();
		return eObjectRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringRef createStringRef() {
		StringRefImpl stringRef = new StringRefImpl();
		return stringRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransfotracePackage getTransfotracePackage() {
		return (TransfotracePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TransfotracePackage getPackage() {
		return TransfotracePackage.eINSTANCE;
	}

} //TransfotraceFactoryImpl
