/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.absystem.transfo.trace.transfotrace;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transfo Trace Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel#getLinks <em>Links</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage#getTransfoTraceModel()
 * @model
 * @generated
 */
public interface TransfoTraceModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Links</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.atsyra.absystem.transfo.trace.transfotrace.Link}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Links</em>' containment reference list.
	 * @see fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotracePackage#getTransfoTraceModel_Links()
	 * @model containment="true"
	 * @generated
	 */
	EList<Link> getLinks();

} // TransfoTraceModel
