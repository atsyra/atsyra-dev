/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.ui.helpers;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import fr.irisa.atsyra.absystem.gal.transfo.ReportEntry;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.Scenario;

public class ReportEntryAdapter extends fr.irisa.atsyra.absreport.helpers.AbsreportHelper.ReportEntry implements ReportEntry {

	public ReportEntryAdapter(Goal goal2, Scenario scenario2, Duration duration2, String command2, Instant timestamp2) {
		super(goal2, scenario2, duration2, command2, timestamp2);
	}
	
	public ReportEntryAdapter(Goal goal2, Scenario scenario2, Duration duration2, String command2, Instant timestamp2,
			String invariant2, List<Scenario> previous2) {
		super(goal2, scenario2, duration2, command2, timestamp2);
	}

	@Override
	public Goal getGoal() {
		return goal;
	}

	@Override
	public Scenario getScenario() {
		return scenario;
	}

	@Override
	public Duration getDuration() {
		return duration;
	}

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public Instant getTimestamp() {
		return timestamp;
	}

	@Override
	public String getInvariant() {
		return invariant;
	}

	@Override
	public List<Scenario> getPrevious() {
		return previous;
	}

}
