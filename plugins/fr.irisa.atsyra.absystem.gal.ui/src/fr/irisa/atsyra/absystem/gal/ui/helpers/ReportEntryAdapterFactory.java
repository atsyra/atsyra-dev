/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.ui.helpers;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import fr.irisa.atsyra.absystem.gal.transfo.ReportEntry;
import fr.irisa.atsyra.absystem.gal.transfo.ReportEntryFactory;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.Scenario;

public class ReportEntryAdapterFactory implements ReportEntryFactory {

	@Override
	public ReportEntry createReportEntry(Goal goal2, Scenario scenario2, Duration duration2, String command2,
			Instant timestamp2) {
		return new ReportEntryAdapter(goal2, scenario2, duration2, command2, timestamp2);
	}

	@Override
	public ReportEntry createReportEntry(Goal goal2, Scenario scenario2, Duration duration2, String command2,
			Instant timestamp2, String invariant2, List<Scenario> previous2) {
		return new ReportEntryAdapter(goal2, scenario2, duration2, command2, timestamp2, invariant2, previous2);
	}

}
