/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.atsyra.absystem.gal.ui.helpers.ABS2ScenariosWorker;
import fr.irisa.atsyra.absystem.model.absystem.Goal;

public class ABS2GALScenariosHandler extends AbstractABSGoalSelectHandler {
	static final Logger logger = LoggerFactory.getLogger(ABS2GALScenariosHandler.class);

	public static void computeScenarios(IFile resource, Goal goal) {
		try {
			ABS2ScenariosWorker.abs2scenarios(resource, goal);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object executeForSelectedGoal(ExecutionEvent event, IProject updatedGemocGoalProject, Goal goal)
			throws ExecutionException {
		IFile goalFile = getGoalIFile(event, goal);
		computeScenarios(goalFile, goal);
		return null;
	}

	@Override
	public String getSelectionMessage() {
		return "Select a goal to check reachability for";
	}
}
