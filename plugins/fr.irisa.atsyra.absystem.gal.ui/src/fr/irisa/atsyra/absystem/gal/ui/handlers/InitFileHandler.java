/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.ui.handlers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.common.collect.Iterators;

import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem;
import fr.irisa.atsyra.absystem.model.absystem.AssetType;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect;
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup;
import fr.irisa.atsyra.absystem.model.absystem.impl.AssetTypeImpl;

public class InitFileHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			Object selectedElement = strucSelection.getFirstElement();
			IFile absModelFile = null;
			if (selectedElement instanceof IResource) {
				absModelFile = (IFile) selectedElement;
			} else if (selectedElement instanceof IAdaptable) {
				absModelFile = (IFile) ((IAdaptable) selectedElement).getAdapter(IFile.class);
			}
			if (absModelFile != null) {
				if (isABSModelFileContainsAssets(absModelFile)) {
					generateInitFile(absModelFile);
				} else {
					IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
					MessageDialog.openInformation(window.getShell(), "ATSyRA ABS",
							"There is no assets in this file, please select another one.");
				}
			}
		}
		return null;
	}

	private void generateInitFile(IFile absModelFile) {
		Job job = new Job("Generate init property file") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				AssetBasedSystem abs = getABSResourcesFromIFile(absModelFile);
				EcoreUtil.resolveAll(abs);

				Path outputPath = Paths.get(absModelFile.getLocation().removeFileExtension().toOSString() + "_"
						+ System.currentTimeMillis() + ".properties");

				abs.getAssetGroups().get(0).getAssets().forEach(asset -> {
					List<String> assetTypeNames = buildAssetTypeNamesHierarchy(asset.getAssetType(), new ArrayList<>());
					Iterators.filter(abs.eResource().getResourceSet().getAllContents(), DefinitionGroup.class).forEachRemaining(definitionGroup -> {
						for (AbstractAssetType abstractAssetType : definitionGroup.getAssetTypes()) {
							if (abstractAssetType instanceof AssetTypeAspect) {
								AssetTypeAspect assetTypeAspect = (AssetTypeAspect) abstractAssetType;
								AssetTypeImpl aspectType = (AssetTypeImpl) assetTypeAspect.getBaseAssetType();
								if (assetTypeNames.contains(aspectType.getName())) {
									assetTypeAspect.getAssetTypeAttributes().forEach(attribute -> {
										writeToFile(outputPath, asset.getName(), attribute.getName(),
												attribute.getAttributeType().getName(),
												attribute.getMultiplicity().toString());
									});
									assetTypeAspect.getAssetTypeProperties().forEach(property -> {
										writeToFile(outputPath, asset.getName(), property.getName(),
												property.getPropertyType().getName(),
												property.getMultiplicity().toString());
									});
								}
							}
						}
					});
				});
				return Status.OK_STATUS;
			}
		};
		job.setPriority(Job.SHORT);
		job.setRule(absModelFile.getProject());
		job.schedule();
	}

	private boolean isABSModelFileContainsAssets(IFile file) {
		AssetBasedSystem abs = getABSResourcesFromIFile(file);
		if (!abs.getAssetGroups().isEmpty() && !abs.getAssetGroups().get(0).getAssets().isEmpty()) {
			return true;
		}
		return false;
	}

	private AssetBasedSystem getABSResourcesFromIFile(IFile file) {
		final ResourceSet rs = new ResourceSetImpl();
		final URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
		Resource absRes = rs.getResource(uri, true);
		return (AssetBasedSystem) absRes.getContents().get(0);
	}

	private void writeToFile(Path filePath, String assetName, String attributeName, String attributeType,
			String attributeMultiplicity) {
		try {
			Files.writeString(filePath,
					"# type: " + attributeType + ", multiplicity: " + attributeMultiplicity + System.lineSeparator(),
					StandardOpenOption.CREATE, StandardOpenOption.APPEND);
			Files.writeString(filePath,
					assetName + "." + attributeName + " = " + System.lineSeparator() + System.lineSeparator(),
					StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private List<String> buildAssetTypeNamesHierarchy(AssetType assetType, List<String> assetTypesNames) {
		assetTypesNames.add(assetType.getName());
		if (assetType.getExtends() != null && !assetType.getExtends().isEmpty()) {
			buildAssetTypeNamesHierarchy(assetType.getExtends().get(0), assetTypesNames);
		}
		return assetTypesNames;
	}
}