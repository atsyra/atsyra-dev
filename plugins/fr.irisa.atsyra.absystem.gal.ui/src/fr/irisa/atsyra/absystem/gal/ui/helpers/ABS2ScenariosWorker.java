/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.ui.helpers;

import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import fr.irisa.atsyra.absreport.helpers.AbsreportHelper;
import fr.irisa.atsyra.absystem.gal.transfo.ABS2Scenarios;
import fr.irisa.atsyra.absystem.gal.transfo.ABS2Scenarios.ABS2ScenariosContext;
import fr.irisa.atsyra.absystem.gal.transfo.ReportEntry;
import fr.irisa.atsyra.absystem.gal.ui.Activator;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandler;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandlerImpl;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotraceFactory;
import fr.irisa.atsyra.absystem.transfo.ui.helpers.EclipseFileHelper;
import fr.irisa.atsyra.gal.UnableToFindITSToolException;
import fr.irisa.atsyra.ide.ui.preferences.EclipsePreferenceService;

public class ABS2ScenariosWorker {
	private ABS2ScenariosWorker() {
		throw new IllegalStateException("Utility class");
	}

	public static Job abs2scenarios(IFile resource, Goal goal) {
		if (resource.getName().endsWith(".abs")) {
			Job job = new Job("Computing scenarios for goal " + goal.getName() + " in file " + resource.getName()) {
				protected IStatus run(IProgressMonitor monitor) {
					SubMonitor submonitor = SubMonitor.convert(monitor, 1000);
					Clock clock = Clock.systemDefaultZone();
					Instant start = clock.instant();
					EclipsePreferenceService.register();
					Activator.eclipseInfo("Computing scenarios for goal \"" + goal.getName() + "\" in file \""
							+ resource.getName() + "\"...");
					IFolder genFolder = resource.getProject().getFolder(EclipseFileHelper.DEFAULT_GEN_FOLDER_NAME);
					try {
						EclipseFileHelper.setupGenFolder(genFolder, submonitor.split(1));
						IFile traceabilityFile = genFolder
								.getFile(resource.getFullPath().removeFileExtension().lastSegment() + "_"
										+ goal.getName() + ".traceabilityMap.xmi");
						EclipseFileHelper.createFileIfNeeded(traceabilityFile, submonitor.split(1));
						ResourceSet rs = goal.eResource().getResourceSet();
						Resource traceResource = rs.createResource(
								URI.createPlatformResourceURI(traceabilityFile.getFullPath().toString(), false));
						TransfoTraceModel tracemodel = TransfotraceFactory.eINSTANCE.createTransfoTraceModel();
						traceResource.getContents().add(tracemodel);
						traceResource.save(null);
						GeneratedFilesHandler generatedFilesHandler = new GeneratedFilesHandlerImpl(genFolder,
								resource.getLocation().removeFileExtension().lastSegment() + "_" + goal.getName());
						ABS2Scenarios abs2Scenarios = new ABS2Scenarios(new ReportEntryAdapterFactory(),
								Activator.getMessagingSystem());
						List<ReportEntry> reported = abs2Scenarios
								.abs2Scenarios(new ABS2ScenariosContext(generatedFilesHandler, goal.eResource().getResourceSet(), goal,
										tracemodel, start, getTimeout(), getNbWitnesses()), submonitor.split(998));
						IFile reportFile = genFolder.getFile("report.absreport");
						EclipseFileHelper.createFileIfNeeded(reportFile, monitor);
						AbsreportHelper report = new AbsreportHelper(rs, genFolder);
						for (ReportEntry entry : reported) {
							report.addScenario((ReportEntryAdapter) entry);
						}
						report.serialize();
						Activator.eclipseImportant("Found " + reported.size()
								+ " scenarios. See \"gen/report.absreport\" for more details.");
					} catch (IOException | CoreException e) {
						Activator.eclipseError("Error during abs2scenarios.", e);
					} catch (UnableToFindITSToolException e) {
						String toolFullPath = Platform.getPreferencesService()
								.getString("fr.lip6.move.gal.itstools.preference", "its-reach", null, null);
						if (toolFullPath == null || toolFullPath.isEmpty()) {
							Activator.eclipseError(
									"Please specify the absolute path to the tool in the preferences page, ITS Modeler->ITS Path.",
									e);
						} else {
							Activator.eclipseError("Could not find ITS tool at \"" + toolFullPath + "\"", e);
						}
					}
					Instant end = clock.instant();
					Activator.eclipseInfo("Scenarios computation time: "
							+ Long.toString(Duration.between(start, end).getSeconds()) + " seconds.");
					submonitor.done();
					return Status.OK_STATUS;
				}
			};
			job.setPriority(Job.LONG);
			// prevent concurrent job
			job.setRule(resource.getProject());
			job.schedule();
			return job;
		}
		return null;
	}

	private static long getTimeout() {
		return Platform.getPreferencesService().getInt("fr.irisa.atsyra.ide.ui", "maxExecTimePreference", 60, null);
	}

	private static int getNbWitnesses() {
		return Platform.getPreferencesService().getInt("fr.irisa.atsyra.ide.ui", "manyPreference", 1, null);
	}
}
