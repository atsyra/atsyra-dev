/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.atsyra.absystem.gal.ui.helpers.ABS2GALWorker;
import fr.irisa.atsyra.absystem.model.absystem.Goal;

public class ABS2GALHandler extends AbstractABSGoalSelectHandler {
	static final Logger logger = LoggerFactory.getLogger(ABS2GALHandler.class);

	@Override
	public Object executeForSelectedGoal(ExecutionEvent event, IProject updatedGemocGoalProject, Goal goal)
			throws ExecutionException {
		IFile goalFile = getGoalIFile(event, goal);
		try {
			ABS2GALWorker.abs2gal(goalFile, goal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getSelectionMessage() {
		return "Select a goal for ABS2GAL";
	}
}
