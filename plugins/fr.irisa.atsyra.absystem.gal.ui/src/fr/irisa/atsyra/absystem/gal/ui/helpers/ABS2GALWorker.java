/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.ui.helpers;

import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import fr.irisa.atsyra.absystem.gal.transfo.ABS2GAL;
import fr.irisa.atsyra.absystem.gal.transfo.ABS2GAL.ABS2GALContext;
import fr.irisa.atsyra.absystem.gal.ui.Activator;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandler;
import fr.irisa.atsyra.absystem.transfo.files.GeneratedFilesHandlerImpl;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfotraceFactory;
import fr.irisa.atsyra.absystem.transfo.ui.helpers.EclipseFileHelper;
import fr.irisa.atsyra.ide.ui.preferences.EclipsePreferenceService;

public class ABS2GALWorker {
	private ABS2GALWorker() {
		throw new IllegalStateException("Utility class");
	}

	public static Job abs2gal(IFile resource, Goal goal) {
		if (resource.getName().endsWith(".abs")) {
			Job job = new Job("Create gal for file " + resource.getName() + " and goal " + goal.getName()) {
				protected IStatus run(IProgressMonitor monitor) {
					SubMonitor submonitor = SubMonitor.convert(monitor, 1000);
					Clock clock = Clock.systemDefaultZone();
					Instant start = clock.instant();
					EclipsePreferenceService.register();
					IFolder genFolder = resource.getProject().getFolder(EclipseFileHelper.DEFAULT_GEN_FOLDER_NAME);
					try {
						EclipseFileHelper.setupGenFolder(genFolder, submonitor.split(1));
						IFile traceabilityFile = genFolder
								.getFile(resource.getFullPath().removeFileExtension().lastSegment() + "_"
										+ goal.getName() + ".traceabilityMap.xmi");
						EclipseFileHelper.createFileIfNeeded(traceabilityFile, submonitor.split(1));
						ResourceSet rs = goal.eResource().getResourceSet();
						Resource traceResource = rs.createResource(
								URI.createPlatformResourceURI(traceabilityFile.getFullPath().toString(), false));
						TransfoTraceModel tracemodel = TransfotraceFactory.eINSTANCE.createTransfoTraceModel();
						traceResource.getContents().add(tracemodel);
						traceResource.save(null);
						GeneratedFilesHandler generatedFilesHandler = new GeneratedFilesHandlerImpl(genFolder,
								resource.getLocation().removeFileExtension().lastSegment() + "_" + goal.getName());
						ABS2GAL abs2gal = new ABS2GAL();
						abs2gal.abs2gal(new ABS2GALContext(generatedFilesHandler, goal.eResource().getResourceSet(), goal, start), tracemodel,
								submonitor.split(998));
						Activator.eclipseImportant("GAL file written.");
					} catch (IOException | CoreException e) {
						Activator.eclipseError("Error during abs2gal.", e);
					}
					Instant end = clock.instant();
					Activator.eclipseInfo("ABS2GAL execution time: "
							+ Long.toString(Duration.between(start, end).getSeconds()) + " seconds.");
					submonitor.done();
					return Status.OK_STATUS;
				}
			};
			job.setPriority(Job.LONG);
			// prevent concurrent job
			job.setRule(resource.getProject());
			job.schedule();
			return job;
		}
		return null;
	}
}
