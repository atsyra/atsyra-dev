/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.gal.ui.handlers;

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.irisa.atsyra.absreport.ABSReportRuntimeModule;
import fr.irisa.atsyra.absystem.gal.transfo.GAL2Scenario;
import fr.irisa.atsyra.absystem.gal.ui.Activator;
import fr.irisa.atsyra.absystem.transfo.trace.transfotrace.TransfoTraceModel;

public class GalOutput2ScenariosHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IFile tmFile = getFileFromSelection(event);
		if (tmFile.getFileExtension() == null || !tmFile.getFileExtension().equals("traceabilityMap")) {
			return null;
		}
		String galOutput = inputString();
		if (galOutput == null) {
			return null;
		}
		Job job = new Job("Create scenario file") {
			protected IStatus run(IProgressMonitor monitor) {
				TransfoTraceModel traceModel = getTraceModelFromFile(tmFile);
				GAL2Scenario g2s = new GAL2Scenario();
				String scenarios = g2s.gal2scenario(galOutput, traceModel,
						monitor.slice(1));
				Activator.eclipseInfo(scenarios);
				return Status.OK_STATUS;
			}
		};
		job.setPriority(Job.LONG);
		// prevent concurrent job
		job.setRule(tmFile.getProject());
		job.schedule();
		return null;
	}

	protected TransfoTraceModel getTraceModelFromFile(IFile tmFile) {
		Injector inj = Guice.createInjector(new ABSReportRuntimeModule());
		ResourceSet resourceSet = inj.getProvider(ResourceSet.class).get();
		Resource resource = resourceSet
				.getResource(URI.createPlatformResourceURI(tmFile.getFullPath().toString(), false), true);
		return (TransfoTraceModel) resource.getContents().get(0);
	}

	protected IFile getFileFromSelection(ExecutionEvent event) {
		IFile selectedIFile = null;
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (@SuppressWarnings("unchecked")
			Iterator<Object> iterator = strucSelection.iterator(); iterator.hasNext();) {

				Object element = iterator.next();

				if (element instanceof IFile) {
					selectedIFile = (IFile) element;

				}
				if (element instanceof IAdaptable) {
					IFile res = ((IAdaptable) element).getAdapter(IFile.class);
					if (res != null) {
						selectedIFile = res;
					}
				}
			}
		}
		return selectedIFile;
	}

	protected String inputString() {
		InputDialog dialog = new InputDialog(null, "its-reach output", "Please paste its-reach output:", null, null);
		dialog.open();
		return dialog.getValue();
	}

}