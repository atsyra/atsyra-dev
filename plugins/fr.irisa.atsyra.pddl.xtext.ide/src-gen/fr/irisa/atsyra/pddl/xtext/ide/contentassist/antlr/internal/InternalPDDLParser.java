/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.pddl.xtext.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.irisa.atsyra.pddl.xtext.services.PDDLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPDDLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_OP", "RULE_CP", "RULE_NAME", "RULE_IDWITHTWOPOINTBEFOREANDDASHES", "RULE_IDWITHQUESTIONMARKBEFORE", "RULE_LETTER", "RULE_ANY_CHAR", "RULE_SL_COMMENT", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'define'", "'domain'", "'problem'", "':extends'", "':requirements'", "':types'", "'-'", "':predicates'", "'either'", "':functions'", "':action'", "':parameters'", "':precondition'", "':effect'", "'='", "'not'", "'and'", "'or'", "'imply'", "'forall'", "'exists'", "'when'", "':axiom'", "':vars'", "':context'", "':implies'", "':domain'", "':objects'", "':init'", "':goal'"
    };
    public static final int RULE_CP=5;
    public static final int T__19=19;
    public static final int T__18=18;
    public static final int RULE_OP=4;
    public static final int RULE_IDWITHTWOPOINTBEFOREANDDASHES=7;
    public static final int RULE_ID=12;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=13;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=15;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_NAME=6;
    public static final int RULE_STRING=14;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int RULE_ANY_CHAR=10;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=16;
    public static final int RULE_ANY_OTHER=17;
    public static final int RULE_LETTER=9;
    public static final int RULE_IDWITHQUESTIONMARKBEFORE=8;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalPDDLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPDDLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPDDLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPDDL.g"; }


    	private PDDLGrammarAccess grammarAccess;

    	public void setGrammarAccess(PDDLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRulePDDLModel"
    // InternalPDDL.g:53:1: entryRulePDDLModel : rulePDDLModel EOF ;
    public final void entryRulePDDLModel() throws RecognitionException {
        try {
            // InternalPDDL.g:54:1: ( rulePDDLModel EOF )
            // InternalPDDL.g:55:1: rulePDDLModel EOF
            {
             before(grammarAccess.getPDDLModelRule()); 
            pushFollow(FOLLOW_1);
            rulePDDLModel();

            state._fsp--;

             after(grammarAccess.getPDDLModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePDDLModel"


    // $ANTLR start "rulePDDLModel"
    // InternalPDDL.g:62:1: rulePDDLModel : ( ( rule__PDDLModel__Group__0 ) ) ;
    public final void rulePDDLModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:66:2: ( ( ( rule__PDDLModel__Group__0 ) ) )
            // InternalPDDL.g:67:2: ( ( rule__PDDLModel__Group__0 ) )
            {
            // InternalPDDL.g:67:2: ( ( rule__PDDLModel__Group__0 ) )
            // InternalPDDL.g:68:3: ( rule__PDDLModel__Group__0 )
            {
             before(grammarAccess.getPDDLModelAccess().getGroup()); 
            // InternalPDDL.g:69:3: ( rule__PDDLModel__Group__0 )
            // InternalPDDL.g:69:4: rule__PDDLModel__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PDDLModel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPDDLModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePDDLModel"


    // $ANTLR start "entryRulePDDLFile"
    // InternalPDDL.g:78:1: entryRulePDDLFile : rulePDDLFile EOF ;
    public final void entryRulePDDLFile() throws RecognitionException {
        try {
            // InternalPDDL.g:79:1: ( rulePDDLFile EOF )
            // InternalPDDL.g:80:1: rulePDDLFile EOF
            {
             before(grammarAccess.getPDDLFileRule()); 
            pushFollow(FOLLOW_1);
            rulePDDLFile();

            state._fsp--;

             after(grammarAccess.getPDDLFileRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePDDLFile"


    // $ANTLR start "rulePDDLFile"
    // InternalPDDL.g:87:1: rulePDDLFile : ( ( rule__PDDLFile__Alternatives ) ) ;
    public final void rulePDDLFile() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:91:2: ( ( ( rule__PDDLFile__Alternatives ) ) )
            // InternalPDDL.g:92:2: ( ( rule__PDDLFile__Alternatives ) )
            {
            // InternalPDDL.g:92:2: ( ( rule__PDDLFile__Alternatives ) )
            // InternalPDDL.g:93:3: ( rule__PDDLFile__Alternatives )
            {
             before(grammarAccess.getPDDLFileAccess().getAlternatives()); 
            // InternalPDDL.g:94:3: ( rule__PDDLFile__Alternatives )
            // InternalPDDL.g:94:4: rule__PDDLFile__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__PDDLFile__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPDDLFileAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePDDLFile"


    // $ANTLR start "entryRuleDomain"
    // InternalPDDL.g:103:1: entryRuleDomain : ruleDomain EOF ;
    public final void entryRuleDomain() throws RecognitionException {
        try {
            // InternalPDDL.g:104:1: ( ruleDomain EOF )
            // InternalPDDL.g:105:1: ruleDomain EOF
            {
             before(grammarAccess.getDomainRule()); 
            pushFollow(FOLLOW_1);
            ruleDomain();

            state._fsp--;

             after(grammarAccess.getDomainRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDomain"


    // $ANTLR start "ruleDomain"
    // InternalPDDL.g:112:1: ruleDomain : ( ( rule__Domain__Group__0 ) ) ;
    public final void ruleDomain() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:116:2: ( ( ( rule__Domain__Group__0 ) ) )
            // InternalPDDL.g:117:2: ( ( rule__Domain__Group__0 ) )
            {
            // InternalPDDL.g:117:2: ( ( rule__Domain__Group__0 ) )
            // InternalPDDL.g:118:3: ( rule__Domain__Group__0 )
            {
             before(grammarAccess.getDomainAccess().getGroup()); 
            // InternalPDDL.g:119:3: ( rule__Domain__Group__0 )
            // InternalPDDL.g:119:4: rule__Domain__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Domain__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDomainAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDomain"


    // $ANTLR start "entryRuleExtends"
    // InternalPDDL.g:128:1: entryRuleExtends : ruleExtends EOF ;
    public final void entryRuleExtends() throws RecognitionException {
        try {
            // InternalPDDL.g:129:1: ( ruleExtends EOF )
            // InternalPDDL.g:130:1: ruleExtends EOF
            {
             before(grammarAccess.getExtendsRule()); 
            pushFollow(FOLLOW_1);
            ruleExtends();

            state._fsp--;

             after(grammarAccess.getExtendsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExtends"


    // $ANTLR start "ruleExtends"
    // InternalPDDL.g:137:1: ruleExtends : ( ( rule__Extends__Group__0 ) ) ;
    public final void ruleExtends() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:141:2: ( ( ( rule__Extends__Group__0 ) ) )
            // InternalPDDL.g:142:2: ( ( rule__Extends__Group__0 ) )
            {
            // InternalPDDL.g:142:2: ( ( rule__Extends__Group__0 ) )
            // InternalPDDL.g:143:3: ( rule__Extends__Group__0 )
            {
             before(grammarAccess.getExtendsAccess().getGroup()); 
            // InternalPDDL.g:144:3: ( rule__Extends__Group__0 )
            // InternalPDDL.g:144:4: rule__Extends__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Extends__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExtendsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExtends"


    // $ANTLR start "entryRuleExtendsList"
    // InternalPDDL.g:153:1: entryRuleExtendsList : ruleExtendsList EOF ;
    public final void entryRuleExtendsList() throws RecognitionException {
        try {
            // InternalPDDL.g:154:1: ( ruleExtendsList EOF )
            // InternalPDDL.g:155:1: ruleExtendsList EOF
            {
             before(grammarAccess.getExtendsListRule()); 
            pushFollow(FOLLOW_1);
            ruleExtendsList();

            state._fsp--;

             after(grammarAccess.getExtendsListRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExtendsList"


    // $ANTLR start "ruleExtendsList"
    // InternalPDDL.g:162:1: ruleExtendsList : ( ( ( rule__ExtendsList__EElemAssignment ) ) ( ( rule__ExtendsList__EElemAssignment )* ) ) ;
    public final void ruleExtendsList() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:166:2: ( ( ( ( rule__ExtendsList__EElemAssignment ) ) ( ( rule__ExtendsList__EElemAssignment )* ) ) )
            // InternalPDDL.g:167:2: ( ( ( rule__ExtendsList__EElemAssignment ) ) ( ( rule__ExtendsList__EElemAssignment )* ) )
            {
            // InternalPDDL.g:167:2: ( ( ( rule__ExtendsList__EElemAssignment ) ) ( ( rule__ExtendsList__EElemAssignment )* ) )
            // InternalPDDL.g:168:3: ( ( rule__ExtendsList__EElemAssignment ) ) ( ( rule__ExtendsList__EElemAssignment )* )
            {
            // InternalPDDL.g:168:3: ( ( rule__ExtendsList__EElemAssignment ) )
            // InternalPDDL.g:169:4: ( rule__ExtendsList__EElemAssignment )
            {
             before(grammarAccess.getExtendsListAccess().getEElemAssignment()); 
            // InternalPDDL.g:170:4: ( rule__ExtendsList__EElemAssignment )
            // InternalPDDL.g:170:5: rule__ExtendsList__EElemAssignment
            {
            pushFollow(FOLLOW_3);
            rule__ExtendsList__EElemAssignment();

            state._fsp--;


            }

             after(grammarAccess.getExtendsListAccess().getEElemAssignment()); 

            }

            // InternalPDDL.g:173:3: ( ( rule__ExtendsList__EElemAssignment )* )
            // InternalPDDL.g:174:4: ( rule__ExtendsList__EElemAssignment )*
            {
             before(grammarAccess.getExtendsListAccess().getEElemAssignment()); 
            // InternalPDDL.g:175:4: ( rule__ExtendsList__EElemAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_NAME) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPDDL.g:175:5: rule__ExtendsList__EElemAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__ExtendsList__EElemAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getExtendsListAccess().getEElemAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExtendsList"


    // $ANTLR start "entryRuleExtend"
    // InternalPDDL.g:185:1: entryRuleExtend : ruleExtend EOF ;
    public final void entryRuleExtend() throws RecognitionException {
        try {
            // InternalPDDL.g:186:1: ( ruleExtend EOF )
            // InternalPDDL.g:187:1: ruleExtend EOF
            {
             before(grammarAccess.getExtendRule()); 
            pushFollow(FOLLOW_1);
            ruleExtend();

            state._fsp--;

             after(grammarAccess.getExtendRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExtend"


    // $ANTLR start "ruleExtend"
    // InternalPDDL.g:194:1: ruleExtend : ( ( rule__Extend__ExtendNameAssignment ) ) ;
    public final void ruleExtend() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:198:2: ( ( ( rule__Extend__ExtendNameAssignment ) ) )
            // InternalPDDL.g:199:2: ( ( rule__Extend__ExtendNameAssignment ) )
            {
            // InternalPDDL.g:199:2: ( ( rule__Extend__ExtendNameAssignment ) )
            // InternalPDDL.g:200:3: ( rule__Extend__ExtendNameAssignment )
            {
             before(grammarAccess.getExtendAccess().getExtendNameAssignment()); 
            // InternalPDDL.g:201:3: ( rule__Extend__ExtendNameAssignment )
            // InternalPDDL.g:201:4: rule__Extend__ExtendNameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Extend__ExtendNameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getExtendAccess().getExtendNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExtend"


    // $ANTLR start "entryRuleRequirements"
    // InternalPDDL.g:210:1: entryRuleRequirements : ruleRequirements EOF ;
    public final void entryRuleRequirements() throws RecognitionException {
        try {
            // InternalPDDL.g:211:1: ( ruleRequirements EOF )
            // InternalPDDL.g:212:1: ruleRequirements EOF
            {
             before(grammarAccess.getRequirementsRule()); 
            pushFollow(FOLLOW_1);
            ruleRequirements();

            state._fsp--;

             after(grammarAccess.getRequirementsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRequirements"


    // $ANTLR start "ruleRequirements"
    // InternalPDDL.g:219:1: ruleRequirements : ( ( rule__Requirements__Group__0 ) ) ;
    public final void ruleRequirements() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:223:2: ( ( ( rule__Requirements__Group__0 ) ) )
            // InternalPDDL.g:224:2: ( ( rule__Requirements__Group__0 ) )
            {
            // InternalPDDL.g:224:2: ( ( rule__Requirements__Group__0 ) )
            // InternalPDDL.g:225:3: ( rule__Requirements__Group__0 )
            {
             before(grammarAccess.getRequirementsAccess().getGroup()); 
            // InternalPDDL.g:226:3: ( rule__Requirements__Group__0 )
            // InternalPDDL.g:226:4: rule__Requirements__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Requirements__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRequirementsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRequirements"


    // $ANTLR start "entryRuleRequirementsList"
    // InternalPDDL.g:235:1: entryRuleRequirementsList : ruleRequirementsList EOF ;
    public final void entryRuleRequirementsList() throws RecognitionException {
        try {
            // InternalPDDL.g:236:1: ( ruleRequirementsList EOF )
            // InternalPDDL.g:237:1: ruleRequirementsList EOF
            {
             before(grammarAccess.getRequirementsListRule()); 
            pushFollow(FOLLOW_1);
            ruleRequirementsList();

            state._fsp--;

             after(grammarAccess.getRequirementsListRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRequirementsList"


    // $ANTLR start "ruleRequirementsList"
    // InternalPDDL.g:244:1: ruleRequirementsList : ( ( ( rule__RequirementsList__RElemAssignment ) ) ( ( rule__RequirementsList__RElemAssignment )* ) ) ;
    public final void ruleRequirementsList() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:248:2: ( ( ( ( rule__RequirementsList__RElemAssignment ) ) ( ( rule__RequirementsList__RElemAssignment )* ) ) )
            // InternalPDDL.g:249:2: ( ( ( rule__RequirementsList__RElemAssignment ) ) ( ( rule__RequirementsList__RElemAssignment )* ) )
            {
            // InternalPDDL.g:249:2: ( ( ( rule__RequirementsList__RElemAssignment ) ) ( ( rule__RequirementsList__RElemAssignment )* ) )
            // InternalPDDL.g:250:3: ( ( rule__RequirementsList__RElemAssignment ) ) ( ( rule__RequirementsList__RElemAssignment )* )
            {
            // InternalPDDL.g:250:3: ( ( rule__RequirementsList__RElemAssignment ) )
            // InternalPDDL.g:251:4: ( rule__RequirementsList__RElemAssignment )
            {
             before(grammarAccess.getRequirementsListAccess().getRElemAssignment()); 
            // InternalPDDL.g:252:4: ( rule__RequirementsList__RElemAssignment )
            // InternalPDDL.g:252:5: rule__RequirementsList__RElemAssignment
            {
            pushFollow(FOLLOW_4);
            rule__RequirementsList__RElemAssignment();

            state._fsp--;


            }

             after(grammarAccess.getRequirementsListAccess().getRElemAssignment()); 

            }

            // InternalPDDL.g:255:3: ( ( rule__RequirementsList__RElemAssignment )* )
            // InternalPDDL.g:256:4: ( rule__RequirementsList__RElemAssignment )*
            {
             before(grammarAccess.getRequirementsListAccess().getRElemAssignment()); 
            // InternalPDDL.g:257:4: ( rule__RequirementsList__RElemAssignment )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_IDWITHTWOPOINTBEFOREANDDASHES) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPDDL.g:257:5: rule__RequirementsList__RElemAssignment
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__RequirementsList__RElemAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getRequirementsListAccess().getRElemAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRequirementsList"


    // $ANTLR start "entryRuleRequirement"
    // InternalPDDL.g:267:1: entryRuleRequirement : ruleRequirement EOF ;
    public final void entryRuleRequirement() throws RecognitionException {
        try {
            // InternalPDDL.g:268:1: ( ruleRequirement EOF )
            // InternalPDDL.g:269:1: ruleRequirement EOF
            {
             before(grammarAccess.getRequirementRule()); 
            pushFollow(FOLLOW_1);
            ruleRequirement();

            state._fsp--;

             after(grammarAccess.getRequirementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRequirement"


    // $ANTLR start "ruleRequirement"
    // InternalPDDL.g:276:1: ruleRequirement : ( ( rule__Requirement__RequirementNameAssignment ) ) ;
    public final void ruleRequirement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:280:2: ( ( ( rule__Requirement__RequirementNameAssignment ) ) )
            // InternalPDDL.g:281:2: ( ( rule__Requirement__RequirementNameAssignment ) )
            {
            // InternalPDDL.g:281:2: ( ( rule__Requirement__RequirementNameAssignment ) )
            // InternalPDDL.g:282:3: ( rule__Requirement__RequirementNameAssignment )
            {
             before(grammarAccess.getRequirementAccess().getRequirementNameAssignment()); 
            // InternalPDDL.g:283:3: ( rule__Requirement__RequirementNameAssignment )
            // InternalPDDL.g:283:4: rule__Requirement__RequirementNameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Requirement__RequirementNameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getRequirementAccess().getRequirementNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRequirement"


    // $ANTLR start "entryRuleTypes"
    // InternalPDDL.g:292:1: entryRuleTypes : ruleTypes EOF ;
    public final void entryRuleTypes() throws RecognitionException {
        try {
            // InternalPDDL.g:293:1: ( ruleTypes EOF )
            // InternalPDDL.g:294:1: ruleTypes EOF
            {
             before(grammarAccess.getTypesRule()); 
            pushFollow(FOLLOW_1);
            ruleTypes();

            state._fsp--;

             after(grammarAccess.getTypesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypes"


    // $ANTLR start "ruleTypes"
    // InternalPDDL.g:301:1: ruleTypes : ( ( rule__Types__Group__0 ) ) ;
    public final void ruleTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:305:2: ( ( ( rule__Types__Group__0 ) ) )
            // InternalPDDL.g:306:2: ( ( rule__Types__Group__0 ) )
            {
            // InternalPDDL.g:306:2: ( ( rule__Types__Group__0 ) )
            // InternalPDDL.g:307:3: ( rule__Types__Group__0 )
            {
             before(grammarAccess.getTypesAccess().getGroup()); 
            // InternalPDDL.g:308:3: ( rule__Types__Group__0 )
            // InternalPDDL.g:308:4: rule__Types__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Types__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypes"


    // $ANTLR start "entryRuleNewTypes"
    // InternalPDDL.g:317:1: entryRuleNewTypes : ruleNewTypes EOF ;
    public final void entryRuleNewTypes() throws RecognitionException {
        try {
            // InternalPDDL.g:318:1: ( ruleNewTypes EOF )
            // InternalPDDL.g:319:1: ruleNewTypes EOF
            {
             before(grammarAccess.getNewTypesRule()); 
            pushFollow(FOLLOW_1);
            ruleNewTypes();

            state._fsp--;

             after(grammarAccess.getNewTypesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNewTypes"


    // $ANTLR start "ruleNewTypes"
    // InternalPDDL.g:326:1: ruleNewTypes : ( ( rule__NewTypes__Group__0 ) ) ;
    public final void ruleNewTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:330:2: ( ( ( rule__NewTypes__Group__0 ) ) )
            // InternalPDDL.g:331:2: ( ( rule__NewTypes__Group__0 ) )
            {
            // InternalPDDL.g:331:2: ( ( rule__NewTypes__Group__0 ) )
            // InternalPDDL.g:332:3: ( rule__NewTypes__Group__0 )
            {
             before(grammarAccess.getNewTypesAccess().getGroup()); 
            // InternalPDDL.g:333:3: ( rule__NewTypes__Group__0 )
            // InternalPDDL.g:333:4: rule__NewTypes__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NewTypes__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNewTypesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNewTypes"


    // $ANTLR start "entryRuleInheritorTypes"
    // InternalPDDL.g:342:1: entryRuleInheritorTypes : ruleInheritorTypes EOF ;
    public final void entryRuleInheritorTypes() throws RecognitionException {
        try {
            // InternalPDDL.g:343:1: ( ruleInheritorTypes EOF )
            // InternalPDDL.g:344:1: ruleInheritorTypes EOF
            {
             before(grammarAccess.getInheritorTypesRule()); 
            pushFollow(FOLLOW_1);
            ruleInheritorTypes();

            state._fsp--;

             after(grammarAccess.getInheritorTypesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInheritorTypes"


    // $ANTLR start "ruleInheritorTypes"
    // InternalPDDL.g:351:1: ruleInheritorTypes : ( ( rule__InheritorTypes__Group__0 ) ) ;
    public final void ruleInheritorTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:355:2: ( ( ( rule__InheritorTypes__Group__0 ) ) )
            // InternalPDDL.g:356:2: ( ( rule__InheritorTypes__Group__0 ) )
            {
            // InternalPDDL.g:356:2: ( ( rule__InheritorTypes__Group__0 ) )
            // InternalPDDL.g:357:3: ( rule__InheritorTypes__Group__0 )
            {
             before(grammarAccess.getInheritorTypesAccess().getGroup()); 
            // InternalPDDL.g:358:3: ( rule__InheritorTypes__Group__0 )
            // InternalPDDL.g:358:4: rule__InheritorTypes__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InheritorTypes__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInheritorTypesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInheritorTypes"


    // $ANTLR start "entryRuleDeclarationTypes"
    // InternalPDDL.g:367:1: entryRuleDeclarationTypes : ruleDeclarationTypes EOF ;
    public final void entryRuleDeclarationTypes() throws RecognitionException {
        try {
            // InternalPDDL.g:368:1: ( ruleDeclarationTypes EOF )
            // InternalPDDL.g:369:1: ruleDeclarationTypes EOF
            {
             before(grammarAccess.getDeclarationTypesRule()); 
            pushFollow(FOLLOW_1);
            ruleDeclarationTypes();

            state._fsp--;

             after(grammarAccess.getDeclarationTypesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeclarationTypes"


    // $ANTLR start "ruleDeclarationTypes"
    // InternalPDDL.g:376:1: ruleDeclarationTypes : ( ( ( rule__DeclarationTypes__DeclTAssignment ) ) ( ( rule__DeclarationTypes__DeclTAssignment )* ) ) ;
    public final void ruleDeclarationTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:380:2: ( ( ( ( rule__DeclarationTypes__DeclTAssignment ) ) ( ( rule__DeclarationTypes__DeclTAssignment )* ) ) )
            // InternalPDDL.g:381:2: ( ( ( rule__DeclarationTypes__DeclTAssignment ) ) ( ( rule__DeclarationTypes__DeclTAssignment )* ) )
            {
            // InternalPDDL.g:381:2: ( ( ( rule__DeclarationTypes__DeclTAssignment ) ) ( ( rule__DeclarationTypes__DeclTAssignment )* ) )
            // InternalPDDL.g:382:3: ( ( rule__DeclarationTypes__DeclTAssignment ) ) ( ( rule__DeclarationTypes__DeclTAssignment )* )
            {
            // InternalPDDL.g:382:3: ( ( rule__DeclarationTypes__DeclTAssignment ) )
            // InternalPDDL.g:383:4: ( rule__DeclarationTypes__DeclTAssignment )
            {
             before(grammarAccess.getDeclarationTypesAccess().getDeclTAssignment()); 
            // InternalPDDL.g:384:4: ( rule__DeclarationTypes__DeclTAssignment )
            // InternalPDDL.g:384:5: rule__DeclarationTypes__DeclTAssignment
            {
            pushFollow(FOLLOW_3);
            rule__DeclarationTypes__DeclTAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationTypesAccess().getDeclTAssignment()); 

            }

            // InternalPDDL.g:387:3: ( ( rule__DeclarationTypes__DeclTAssignment )* )
            // InternalPDDL.g:388:4: ( rule__DeclarationTypes__DeclTAssignment )*
            {
             before(grammarAccess.getDeclarationTypesAccess().getDeclTAssignment()); 
            // InternalPDDL.g:389:4: ( rule__DeclarationTypes__DeclTAssignment )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_NAME) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalPDDL.g:389:5: rule__DeclarationTypes__DeclTAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__DeclarationTypes__DeclTAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getDeclarationTypesAccess().getDeclTAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeclarationTypes"


    // $ANTLR start "entryRuleType"
    // InternalPDDL.g:399:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalPDDL.g:400:1: ( ruleType EOF )
            // InternalPDDL.g:401:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalPDDL.g:408:1: ruleType : ( ( rule__Type__TypeNameAssignment ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:412:2: ( ( ( rule__Type__TypeNameAssignment ) ) )
            // InternalPDDL.g:413:2: ( ( rule__Type__TypeNameAssignment ) )
            {
            // InternalPDDL.g:413:2: ( ( rule__Type__TypeNameAssignment ) )
            // InternalPDDL.g:414:3: ( rule__Type__TypeNameAssignment )
            {
             before(grammarAccess.getTypeAccess().getTypeNameAssignment()); 
            // InternalPDDL.g:415:3: ( rule__Type__TypeNameAssignment )
            // InternalPDDL.g:415:4: rule__Type__TypeNameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Type__TypeNameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getTypeNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRulePredicates"
    // InternalPDDL.g:424:1: entryRulePredicates : rulePredicates EOF ;
    public final void entryRulePredicates() throws RecognitionException {
        try {
            // InternalPDDL.g:425:1: ( rulePredicates EOF )
            // InternalPDDL.g:426:1: rulePredicates EOF
            {
             before(grammarAccess.getPredicatesRule()); 
            pushFollow(FOLLOW_1);
            rulePredicates();

            state._fsp--;

             after(grammarAccess.getPredicatesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePredicates"


    // $ANTLR start "rulePredicates"
    // InternalPDDL.g:433:1: rulePredicates : ( ( rule__Predicates__Group__0 ) ) ;
    public final void rulePredicates() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:437:2: ( ( ( rule__Predicates__Group__0 ) ) )
            // InternalPDDL.g:438:2: ( ( rule__Predicates__Group__0 ) )
            {
            // InternalPDDL.g:438:2: ( ( rule__Predicates__Group__0 ) )
            // InternalPDDL.g:439:3: ( rule__Predicates__Group__0 )
            {
             before(grammarAccess.getPredicatesAccess().getGroup()); 
            // InternalPDDL.g:440:3: ( rule__Predicates__Group__0 )
            // InternalPDDL.g:440:4: rule__Predicates__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Predicates__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPredicatesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePredicates"


    // $ANTLR start "entryRuleDeclPFList"
    // InternalPDDL.g:449:1: entryRuleDeclPFList : ruleDeclPFList EOF ;
    public final void entryRuleDeclPFList() throws RecognitionException {
        try {
            // InternalPDDL.g:450:1: ( ruleDeclPFList EOF )
            // InternalPDDL.g:451:1: ruleDeclPFList EOF
            {
             before(grammarAccess.getDeclPFListRule()); 
            pushFollow(FOLLOW_1);
            ruleDeclPFList();

            state._fsp--;

             after(grammarAccess.getDeclPFListRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeclPFList"


    // $ANTLR start "ruleDeclPFList"
    // InternalPDDL.g:458:1: ruleDeclPFList : ( ( ( rule__DeclPFList__PfListAssignment ) ) ( ( rule__DeclPFList__PfListAssignment )* ) ) ;
    public final void ruleDeclPFList() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:462:2: ( ( ( ( rule__DeclPFList__PfListAssignment ) ) ( ( rule__DeclPFList__PfListAssignment )* ) ) )
            // InternalPDDL.g:463:2: ( ( ( rule__DeclPFList__PfListAssignment ) ) ( ( rule__DeclPFList__PfListAssignment )* ) )
            {
            // InternalPDDL.g:463:2: ( ( ( rule__DeclPFList__PfListAssignment ) ) ( ( rule__DeclPFList__PfListAssignment )* ) )
            // InternalPDDL.g:464:3: ( ( rule__DeclPFList__PfListAssignment ) ) ( ( rule__DeclPFList__PfListAssignment )* )
            {
            // InternalPDDL.g:464:3: ( ( rule__DeclPFList__PfListAssignment ) )
            // InternalPDDL.g:465:4: ( rule__DeclPFList__PfListAssignment )
            {
             before(grammarAccess.getDeclPFListAccess().getPfListAssignment()); 
            // InternalPDDL.g:466:4: ( rule__DeclPFList__PfListAssignment )
            // InternalPDDL.g:466:5: rule__DeclPFList__PfListAssignment
            {
            pushFollow(FOLLOW_5);
            rule__DeclPFList__PfListAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDeclPFListAccess().getPfListAssignment()); 

            }

            // InternalPDDL.g:469:3: ( ( rule__DeclPFList__PfListAssignment )* )
            // InternalPDDL.g:470:4: ( rule__DeclPFList__PfListAssignment )*
            {
             before(grammarAccess.getDeclPFListAccess().getPfListAssignment()); 
            // InternalPDDL.g:471:4: ( rule__DeclPFList__PfListAssignment )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_OP) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalPDDL.g:471:5: rule__DeclPFList__PfListAssignment
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__DeclPFList__PfListAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getDeclPFListAccess().getPfListAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeclPFList"


    // $ANTLR start "entryRuleDeclPredicatOrFunction"
    // InternalPDDL.g:481:1: entryRuleDeclPredicatOrFunction : ruleDeclPredicatOrFunction EOF ;
    public final void entryRuleDeclPredicatOrFunction() throws RecognitionException {
        try {
            // InternalPDDL.g:482:1: ( ruleDeclPredicatOrFunction EOF )
            // InternalPDDL.g:483:1: ruleDeclPredicatOrFunction EOF
            {
             before(grammarAccess.getDeclPredicatOrFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleDeclPredicatOrFunction();

            state._fsp--;

             after(grammarAccess.getDeclPredicatOrFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeclPredicatOrFunction"


    // $ANTLR start "ruleDeclPredicatOrFunction"
    // InternalPDDL.g:490:1: ruleDeclPredicatOrFunction : ( ( rule__DeclPredicatOrFunction__Group__0 ) ) ;
    public final void ruleDeclPredicatOrFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:494:2: ( ( ( rule__DeclPredicatOrFunction__Group__0 ) ) )
            // InternalPDDL.g:495:2: ( ( rule__DeclPredicatOrFunction__Group__0 ) )
            {
            // InternalPDDL.g:495:2: ( ( rule__DeclPredicatOrFunction__Group__0 ) )
            // InternalPDDL.g:496:3: ( rule__DeclPredicatOrFunction__Group__0 )
            {
             before(grammarAccess.getDeclPredicatOrFunctionAccess().getGroup()); 
            // InternalPDDL.g:497:3: ( rule__DeclPredicatOrFunction__Group__0 )
            // InternalPDDL.g:497:4: rule__DeclPredicatOrFunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DeclPredicatOrFunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDeclPredicatOrFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeclPredicatOrFunction"


    // $ANTLR start "entryRuleTypedVariablesList"
    // InternalPDDL.g:506:1: entryRuleTypedVariablesList : ruleTypedVariablesList EOF ;
    public final void entryRuleTypedVariablesList() throws RecognitionException {
        try {
            // InternalPDDL.g:507:1: ( ruleTypedVariablesList EOF )
            // InternalPDDL.g:508:1: ruleTypedVariablesList EOF
            {
             before(grammarAccess.getTypedVariablesListRule()); 
            pushFollow(FOLLOW_1);
            ruleTypedVariablesList();

            state._fsp--;

             after(grammarAccess.getTypedVariablesListRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypedVariablesList"


    // $ANTLR start "ruleTypedVariablesList"
    // InternalPDDL.g:515:1: ruleTypedVariablesList : ( ( ( rule__TypedVariablesList__VariablesListAssignment ) ) ( ( rule__TypedVariablesList__VariablesListAssignment )* ) ) ;
    public final void ruleTypedVariablesList() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:519:2: ( ( ( ( rule__TypedVariablesList__VariablesListAssignment ) ) ( ( rule__TypedVariablesList__VariablesListAssignment )* ) ) )
            // InternalPDDL.g:520:2: ( ( ( rule__TypedVariablesList__VariablesListAssignment ) ) ( ( rule__TypedVariablesList__VariablesListAssignment )* ) )
            {
            // InternalPDDL.g:520:2: ( ( ( rule__TypedVariablesList__VariablesListAssignment ) ) ( ( rule__TypedVariablesList__VariablesListAssignment )* ) )
            // InternalPDDL.g:521:3: ( ( rule__TypedVariablesList__VariablesListAssignment ) ) ( ( rule__TypedVariablesList__VariablesListAssignment )* )
            {
            // InternalPDDL.g:521:3: ( ( rule__TypedVariablesList__VariablesListAssignment ) )
            // InternalPDDL.g:522:4: ( rule__TypedVariablesList__VariablesListAssignment )
            {
             before(grammarAccess.getTypedVariablesListAccess().getVariablesListAssignment()); 
            // InternalPDDL.g:523:4: ( rule__TypedVariablesList__VariablesListAssignment )
            // InternalPDDL.g:523:5: rule__TypedVariablesList__VariablesListAssignment
            {
            pushFollow(FOLLOW_6);
            rule__TypedVariablesList__VariablesListAssignment();

            state._fsp--;


            }

             after(grammarAccess.getTypedVariablesListAccess().getVariablesListAssignment()); 

            }

            // InternalPDDL.g:526:3: ( ( rule__TypedVariablesList__VariablesListAssignment )* )
            // InternalPDDL.g:527:4: ( rule__TypedVariablesList__VariablesListAssignment )*
            {
             before(grammarAccess.getTypedVariablesListAccess().getVariablesListAssignment()); 
            // InternalPDDL.g:528:4: ( rule__TypedVariablesList__VariablesListAssignment )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalPDDL.g:528:5: rule__TypedVariablesList__VariablesListAssignment
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__TypedVariablesList__VariablesListAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getTypedVariablesListAccess().getVariablesListAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypedVariablesList"


    // $ANTLR start "entryRuleTypedVariable"
    // InternalPDDL.g:538:1: entryRuleTypedVariable : ruleTypedVariable EOF ;
    public final void entryRuleTypedVariable() throws RecognitionException {
        try {
            // InternalPDDL.g:539:1: ( ruleTypedVariable EOF )
            // InternalPDDL.g:540:1: ruleTypedVariable EOF
            {
             before(grammarAccess.getTypedVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleTypedVariable();

            state._fsp--;

             after(grammarAccess.getTypedVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypedVariable"


    // $ANTLR start "ruleTypedVariable"
    // InternalPDDL.g:547:1: ruleTypedVariable : ( ( rule__TypedVariable__Group__0 ) ) ;
    public final void ruleTypedVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:551:2: ( ( ( rule__TypedVariable__Group__0 ) ) )
            // InternalPDDL.g:552:2: ( ( rule__TypedVariable__Group__0 ) )
            {
            // InternalPDDL.g:552:2: ( ( rule__TypedVariable__Group__0 ) )
            // InternalPDDL.g:553:3: ( rule__TypedVariable__Group__0 )
            {
             before(grammarAccess.getTypedVariableAccess().getGroup()); 
            // InternalPDDL.g:554:3: ( rule__TypedVariable__Group__0 )
            // InternalPDDL.g:554:4: rule__TypedVariable__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__TypedVariable__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypedVariableAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypedVariable"


    // $ANTLR start "entryRuleEither"
    // InternalPDDL.g:563:1: entryRuleEither : ruleEither EOF ;
    public final void entryRuleEither() throws RecognitionException {
        try {
            // InternalPDDL.g:564:1: ( ruleEither EOF )
            // InternalPDDL.g:565:1: ruleEither EOF
            {
             before(grammarAccess.getEitherRule()); 
            pushFollow(FOLLOW_1);
            ruleEither();

            state._fsp--;

             after(grammarAccess.getEitherRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEither"


    // $ANTLR start "ruleEither"
    // InternalPDDL.g:572:1: ruleEither : ( ( rule__Either__Group__0 ) ) ;
    public final void ruleEither() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:576:2: ( ( ( rule__Either__Group__0 ) ) )
            // InternalPDDL.g:577:2: ( ( rule__Either__Group__0 ) )
            {
            // InternalPDDL.g:577:2: ( ( rule__Either__Group__0 ) )
            // InternalPDDL.g:578:3: ( rule__Either__Group__0 )
            {
             before(grammarAccess.getEitherAccess().getGroup()); 
            // InternalPDDL.g:579:3: ( rule__Either__Group__0 )
            // InternalPDDL.g:579:4: rule__Either__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Either__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEitherAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEither"


    // $ANTLR start "entryRuleVariable"
    // InternalPDDL.g:588:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // InternalPDDL.g:589:1: ( ruleVariable EOF )
            // InternalPDDL.g:590:1: ruleVariable EOF
            {
             before(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalPDDL.g:597:1: ruleVariable : ( ( rule__Variable__VariableNameAssignment ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:601:2: ( ( ( rule__Variable__VariableNameAssignment ) ) )
            // InternalPDDL.g:602:2: ( ( rule__Variable__VariableNameAssignment ) )
            {
            // InternalPDDL.g:602:2: ( ( rule__Variable__VariableNameAssignment ) )
            // InternalPDDL.g:603:3: ( rule__Variable__VariableNameAssignment )
            {
             before(grammarAccess.getVariableAccess().getVariableNameAssignment()); 
            // InternalPDDL.g:604:3: ( rule__Variable__VariableNameAssignment )
            // InternalPDDL.g:604:4: rule__Variable__VariableNameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Variable__VariableNameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getVariableNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleFunctions"
    // InternalPDDL.g:613:1: entryRuleFunctions : ruleFunctions EOF ;
    public final void entryRuleFunctions() throws RecognitionException {
        try {
            // InternalPDDL.g:614:1: ( ruleFunctions EOF )
            // InternalPDDL.g:615:1: ruleFunctions EOF
            {
             before(grammarAccess.getFunctionsRule()); 
            pushFollow(FOLLOW_1);
            ruleFunctions();

            state._fsp--;

             after(grammarAccess.getFunctionsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunctions"


    // $ANTLR start "ruleFunctions"
    // InternalPDDL.g:622:1: ruleFunctions : ( ( rule__Functions__Group__0 ) ) ;
    public final void ruleFunctions() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:626:2: ( ( ( rule__Functions__Group__0 ) ) )
            // InternalPDDL.g:627:2: ( ( rule__Functions__Group__0 ) )
            {
            // InternalPDDL.g:627:2: ( ( rule__Functions__Group__0 ) )
            // InternalPDDL.g:628:3: ( rule__Functions__Group__0 )
            {
             before(grammarAccess.getFunctionsAccess().getGroup()); 
            // InternalPDDL.g:629:3: ( rule__Functions__Group__0 )
            // InternalPDDL.g:629:4: rule__Functions__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Functions__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunctions"


    // $ANTLR start "entryRulePDDLAction"
    // InternalPDDL.g:638:1: entryRulePDDLAction : rulePDDLAction EOF ;
    public final void entryRulePDDLAction() throws RecognitionException {
        try {
            // InternalPDDL.g:639:1: ( rulePDDLAction EOF )
            // InternalPDDL.g:640:1: rulePDDLAction EOF
            {
             before(grammarAccess.getPDDLActionRule()); 
            pushFollow(FOLLOW_1);
            rulePDDLAction();

            state._fsp--;

             after(grammarAccess.getPDDLActionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePDDLAction"


    // $ANTLR start "rulePDDLAction"
    // InternalPDDL.g:647:1: rulePDDLAction : ( ( rule__PDDLAction__Group__0 ) ) ;
    public final void rulePDDLAction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:651:2: ( ( ( rule__PDDLAction__Group__0 ) ) )
            // InternalPDDL.g:652:2: ( ( rule__PDDLAction__Group__0 ) )
            {
            // InternalPDDL.g:652:2: ( ( rule__PDDLAction__Group__0 ) )
            // InternalPDDL.g:653:3: ( rule__PDDLAction__Group__0 )
            {
             before(grammarAccess.getPDDLActionAccess().getGroup()); 
            // InternalPDDL.g:654:3: ( rule__PDDLAction__Group__0 )
            // InternalPDDL.g:654:4: rule__PDDLAction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PDDLAction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPDDLActionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePDDLAction"


    // $ANTLR start "entryRuleParameters"
    // InternalPDDL.g:663:1: entryRuleParameters : ruleParameters EOF ;
    public final void entryRuleParameters() throws RecognitionException {
        try {
            // InternalPDDL.g:664:1: ( ruleParameters EOF )
            // InternalPDDL.g:665:1: ruleParameters EOF
            {
             before(grammarAccess.getParametersRule()); 
            pushFollow(FOLLOW_1);
            ruleParameters();

            state._fsp--;

             after(grammarAccess.getParametersRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameters"


    // $ANTLR start "ruleParameters"
    // InternalPDDL.g:672:1: ruleParameters : ( ( rule__Parameters__Group__0 ) ) ;
    public final void ruleParameters() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:676:2: ( ( ( rule__Parameters__Group__0 ) ) )
            // InternalPDDL.g:677:2: ( ( rule__Parameters__Group__0 ) )
            {
            // InternalPDDL.g:677:2: ( ( rule__Parameters__Group__0 ) )
            // InternalPDDL.g:678:3: ( rule__Parameters__Group__0 )
            {
             before(grammarAccess.getParametersAccess().getGroup()); 
            // InternalPDDL.g:679:3: ( rule__Parameters__Group__0 )
            // InternalPDDL.g:679:4: rule__Parameters__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Parameters__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParametersAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameters"


    // $ANTLR start "entryRulePreconditions"
    // InternalPDDL.g:688:1: entryRulePreconditions : rulePreconditions EOF ;
    public final void entryRulePreconditions() throws RecognitionException {
        try {
            // InternalPDDL.g:689:1: ( rulePreconditions EOF )
            // InternalPDDL.g:690:1: rulePreconditions EOF
            {
             before(grammarAccess.getPreconditionsRule()); 
            pushFollow(FOLLOW_1);
            rulePreconditions();

            state._fsp--;

             after(grammarAccess.getPreconditionsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePreconditions"


    // $ANTLR start "rulePreconditions"
    // InternalPDDL.g:697:1: rulePreconditions : ( ( rule__Preconditions__Group__0 ) ) ;
    public final void rulePreconditions() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:701:2: ( ( ( rule__Preconditions__Group__0 ) ) )
            // InternalPDDL.g:702:2: ( ( rule__Preconditions__Group__0 ) )
            {
            // InternalPDDL.g:702:2: ( ( rule__Preconditions__Group__0 ) )
            // InternalPDDL.g:703:3: ( rule__Preconditions__Group__0 )
            {
             before(grammarAccess.getPreconditionsAccess().getGroup()); 
            // InternalPDDL.g:704:3: ( rule__Preconditions__Group__0 )
            // InternalPDDL.g:704:4: rule__Preconditions__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Preconditions__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPreconditionsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePreconditions"


    // $ANTLR start "entryRuleEffects"
    // InternalPDDL.g:713:1: entryRuleEffects : ruleEffects EOF ;
    public final void entryRuleEffects() throws RecognitionException {
        try {
            // InternalPDDL.g:714:1: ( ruleEffects EOF )
            // InternalPDDL.g:715:1: ruleEffects EOF
            {
             before(grammarAccess.getEffectsRule()); 
            pushFollow(FOLLOW_1);
            ruleEffects();

            state._fsp--;

             after(grammarAccess.getEffectsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEffects"


    // $ANTLR start "ruleEffects"
    // InternalPDDL.g:722:1: ruleEffects : ( ( rule__Effects__Group__0 ) ) ;
    public final void ruleEffects() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:726:2: ( ( ( rule__Effects__Group__0 ) ) )
            // InternalPDDL.g:727:2: ( ( rule__Effects__Group__0 ) )
            {
            // InternalPDDL.g:727:2: ( ( rule__Effects__Group__0 ) )
            // InternalPDDL.g:728:3: ( rule__Effects__Group__0 )
            {
             before(grammarAccess.getEffectsAccess().getGroup()); 
            // InternalPDDL.g:729:3: ( rule__Effects__Group__0 )
            // InternalPDDL.g:729:4: rule__Effects__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Effects__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEffectsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEffects"


    // $ANTLR start "entryRuleLogicalExpressionDM"
    // InternalPDDL.g:738:1: entryRuleLogicalExpressionDM : ruleLogicalExpressionDM EOF ;
    public final void entryRuleLogicalExpressionDM() throws RecognitionException {
        try {
            // InternalPDDL.g:739:1: ( ruleLogicalExpressionDM EOF )
            // InternalPDDL.g:740:1: ruleLogicalExpressionDM EOF
            {
             before(grammarAccess.getLogicalExpressionDMRule()); 
            pushFollow(FOLLOW_1);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLogicalExpressionDM"


    // $ANTLR start "ruleLogicalExpressionDM"
    // InternalPDDL.g:747:1: ruleLogicalExpressionDM : ( ( rule__LogicalExpressionDM__Alternatives ) ) ;
    public final void ruleLogicalExpressionDM() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:751:2: ( ( ( rule__LogicalExpressionDM__Alternatives ) ) )
            // InternalPDDL.g:752:2: ( ( rule__LogicalExpressionDM__Alternatives ) )
            {
            // InternalPDDL.g:752:2: ( ( rule__LogicalExpressionDM__Alternatives ) )
            // InternalPDDL.g:753:3: ( rule__LogicalExpressionDM__Alternatives )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getAlternatives()); 
            // InternalPDDL.g:754:3: ( rule__LogicalExpressionDM__Alternatives )
            // InternalPDDL.g:754:4: rule__LogicalExpressionDM__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLogicalExpressionDM"


    // $ANTLR start "entryRulePredicatDM"
    // InternalPDDL.g:763:1: entryRulePredicatDM : rulePredicatDM EOF ;
    public final void entryRulePredicatDM() throws RecognitionException {
        try {
            // InternalPDDL.g:764:1: ( rulePredicatDM EOF )
            // InternalPDDL.g:765:1: rulePredicatDM EOF
            {
             before(grammarAccess.getPredicatDMRule()); 
            pushFollow(FOLLOW_1);
            rulePredicatDM();

            state._fsp--;

             after(grammarAccess.getPredicatDMRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePredicatDM"


    // $ANTLR start "rulePredicatDM"
    // InternalPDDL.g:772:1: rulePredicatDM : ( ( rule__PredicatDM__Group__0 ) ) ;
    public final void rulePredicatDM() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:776:2: ( ( ( rule__PredicatDM__Group__0 ) ) )
            // InternalPDDL.g:777:2: ( ( rule__PredicatDM__Group__0 ) )
            {
            // InternalPDDL.g:777:2: ( ( rule__PredicatDM__Group__0 ) )
            // InternalPDDL.g:778:3: ( rule__PredicatDM__Group__0 )
            {
             before(grammarAccess.getPredicatDMAccess().getGroup()); 
            // InternalPDDL.g:779:3: ( rule__PredicatDM__Group__0 )
            // InternalPDDL.g:779:4: rule__PredicatDM__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PredicatDM__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPredicatDMAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePredicatDM"


    // $ANTLR start "entryRuleTypeDescr"
    // InternalPDDL.g:788:1: entryRuleTypeDescr : ruleTypeDescr EOF ;
    public final void entryRuleTypeDescr() throws RecognitionException {
        try {
            // InternalPDDL.g:789:1: ( ruleTypeDescr EOF )
            // InternalPDDL.g:790:1: ruleTypeDescr EOF
            {
             before(grammarAccess.getTypeDescrRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeDescr();

            state._fsp--;

             after(grammarAccess.getTypeDescrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeDescr"


    // $ANTLR start "ruleTypeDescr"
    // InternalPDDL.g:797:1: ruleTypeDescr : ( ( rule__TypeDescr__Group__0 ) ) ;
    public final void ruleTypeDescr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:801:2: ( ( ( rule__TypeDescr__Group__0 ) ) )
            // InternalPDDL.g:802:2: ( ( rule__TypeDescr__Group__0 ) )
            {
            // InternalPDDL.g:802:2: ( ( rule__TypeDescr__Group__0 ) )
            // InternalPDDL.g:803:3: ( rule__TypeDescr__Group__0 )
            {
             before(grammarAccess.getTypeDescrAccess().getGroup()); 
            // InternalPDDL.g:804:3: ( rule__TypeDescr__Group__0 )
            // InternalPDDL.g:804:4: rule__TypeDescr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__TypeDescr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeDescrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeDescr"


    // $ANTLR start "entryRuleAxiom"
    // InternalPDDL.g:813:1: entryRuleAxiom : ruleAxiom EOF ;
    public final void entryRuleAxiom() throws RecognitionException {
        try {
            // InternalPDDL.g:814:1: ( ruleAxiom EOF )
            // InternalPDDL.g:815:1: ruleAxiom EOF
            {
             before(grammarAccess.getAxiomRule()); 
            pushFollow(FOLLOW_1);
            ruleAxiom();

            state._fsp--;

             after(grammarAccess.getAxiomRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAxiom"


    // $ANTLR start "ruleAxiom"
    // InternalPDDL.g:822:1: ruleAxiom : ( ( rule__Axiom__Group__0 ) ) ;
    public final void ruleAxiom() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:826:2: ( ( ( rule__Axiom__Group__0 ) ) )
            // InternalPDDL.g:827:2: ( ( rule__Axiom__Group__0 ) )
            {
            // InternalPDDL.g:827:2: ( ( rule__Axiom__Group__0 ) )
            // InternalPDDL.g:828:3: ( rule__Axiom__Group__0 )
            {
             before(grammarAccess.getAxiomAccess().getGroup()); 
            // InternalPDDL.g:829:3: ( rule__Axiom__Group__0 )
            // InternalPDDL.g:829:4: rule__Axiom__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Axiom__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAxiomAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAxiom"


    // $ANTLR start "entryRuleVars"
    // InternalPDDL.g:838:1: entryRuleVars : ruleVars EOF ;
    public final void entryRuleVars() throws RecognitionException {
        try {
            // InternalPDDL.g:839:1: ( ruleVars EOF )
            // InternalPDDL.g:840:1: ruleVars EOF
            {
             before(grammarAccess.getVarsRule()); 
            pushFollow(FOLLOW_1);
            ruleVars();

            state._fsp--;

             after(grammarAccess.getVarsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVars"


    // $ANTLR start "ruleVars"
    // InternalPDDL.g:847:1: ruleVars : ( ( rule__Vars__Group__0 ) ) ;
    public final void ruleVars() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:851:2: ( ( ( rule__Vars__Group__0 ) ) )
            // InternalPDDL.g:852:2: ( ( rule__Vars__Group__0 ) )
            {
            // InternalPDDL.g:852:2: ( ( rule__Vars__Group__0 ) )
            // InternalPDDL.g:853:3: ( rule__Vars__Group__0 )
            {
             before(grammarAccess.getVarsAccess().getGroup()); 
            // InternalPDDL.g:854:3: ( rule__Vars__Group__0 )
            // InternalPDDL.g:854:4: rule__Vars__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Vars__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVarsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVars"


    // $ANTLR start "entryRuleContext"
    // InternalPDDL.g:863:1: entryRuleContext : ruleContext EOF ;
    public final void entryRuleContext() throws RecognitionException {
        try {
            // InternalPDDL.g:864:1: ( ruleContext EOF )
            // InternalPDDL.g:865:1: ruleContext EOF
            {
             before(grammarAccess.getContextRule()); 
            pushFollow(FOLLOW_1);
            ruleContext();

            state._fsp--;

             after(grammarAccess.getContextRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleContext"


    // $ANTLR start "ruleContext"
    // InternalPDDL.g:872:1: ruleContext : ( ( rule__Context__Group__0 ) ) ;
    public final void ruleContext() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:876:2: ( ( ( rule__Context__Group__0 ) ) )
            // InternalPDDL.g:877:2: ( ( rule__Context__Group__0 ) )
            {
            // InternalPDDL.g:877:2: ( ( rule__Context__Group__0 ) )
            // InternalPDDL.g:878:3: ( rule__Context__Group__0 )
            {
             before(grammarAccess.getContextAccess().getGroup()); 
            // InternalPDDL.g:879:3: ( rule__Context__Group__0 )
            // InternalPDDL.g:879:4: rule__Context__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Context__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getContextAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleContext"


    // $ANTLR start "entryRuleImplies"
    // InternalPDDL.g:888:1: entryRuleImplies : ruleImplies EOF ;
    public final void entryRuleImplies() throws RecognitionException {
        try {
            // InternalPDDL.g:889:1: ( ruleImplies EOF )
            // InternalPDDL.g:890:1: ruleImplies EOF
            {
             before(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getImpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalPDDL.g:897:1: ruleImplies : ( ( rule__Implies__Group__0 ) ) ;
    public final void ruleImplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:901:2: ( ( ( rule__Implies__Group__0 ) ) )
            // InternalPDDL.g:902:2: ( ( rule__Implies__Group__0 ) )
            {
            // InternalPDDL.g:902:2: ( ( rule__Implies__Group__0 ) )
            // InternalPDDL.g:903:3: ( rule__Implies__Group__0 )
            {
             before(grammarAccess.getImpliesAccess().getGroup()); 
            // InternalPDDL.g:904:3: ( rule__Implies__Group__0 )
            // InternalPDDL.g:904:4: rule__Implies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleProblem"
    // InternalPDDL.g:913:1: entryRuleProblem : ruleProblem EOF ;
    public final void entryRuleProblem() throws RecognitionException {
        try {
            // InternalPDDL.g:914:1: ( ruleProblem EOF )
            // InternalPDDL.g:915:1: ruleProblem EOF
            {
             before(grammarAccess.getProblemRule()); 
            pushFollow(FOLLOW_1);
            ruleProblem();

            state._fsp--;

             after(grammarAccess.getProblemRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProblem"


    // $ANTLR start "ruleProblem"
    // InternalPDDL.g:922:1: ruleProblem : ( ( rule__Problem__Group__0 ) ) ;
    public final void ruleProblem() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:926:2: ( ( ( rule__Problem__Group__0 ) ) )
            // InternalPDDL.g:927:2: ( ( rule__Problem__Group__0 ) )
            {
            // InternalPDDL.g:927:2: ( ( rule__Problem__Group__0 ) )
            // InternalPDDL.g:928:3: ( rule__Problem__Group__0 )
            {
             before(grammarAccess.getProblemAccess().getGroup()); 
            // InternalPDDL.g:929:3: ( rule__Problem__Group__0 )
            // InternalPDDL.g:929:4: rule__Problem__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Problem__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getProblemAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProblem"


    // $ANTLR start "entryRuleDomainRef"
    // InternalPDDL.g:938:1: entryRuleDomainRef : ruleDomainRef EOF ;
    public final void entryRuleDomainRef() throws RecognitionException {
        try {
            // InternalPDDL.g:939:1: ( ruleDomainRef EOF )
            // InternalPDDL.g:940:1: ruleDomainRef EOF
            {
             before(grammarAccess.getDomainRefRule()); 
            pushFollow(FOLLOW_1);
            ruleDomainRef();

            state._fsp--;

             after(grammarAccess.getDomainRefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDomainRef"


    // $ANTLR start "ruleDomainRef"
    // InternalPDDL.g:947:1: ruleDomainRef : ( ( rule__DomainRef__Group__0 ) ) ;
    public final void ruleDomainRef() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:951:2: ( ( ( rule__DomainRef__Group__0 ) ) )
            // InternalPDDL.g:952:2: ( ( rule__DomainRef__Group__0 ) )
            {
            // InternalPDDL.g:952:2: ( ( rule__DomainRef__Group__0 ) )
            // InternalPDDL.g:953:3: ( rule__DomainRef__Group__0 )
            {
             before(grammarAccess.getDomainRefAccess().getGroup()); 
            // InternalPDDL.g:954:3: ( rule__DomainRef__Group__0 )
            // InternalPDDL.g:954:4: rule__DomainRef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DomainRef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDomainRefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDomainRef"


    // $ANTLR start "entryRuleObjects"
    // InternalPDDL.g:963:1: entryRuleObjects : ruleObjects EOF ;
    public final void entryRuleObjects() throws RecognitionException {
        try {
            // InternalPDDL.g:964:1: ( ruleObjects EOF )
            // InternalPDDL.g:965:1: ruleObjects EOF
            {
             before(grammarAccess.getObjectsRule()); 
            pushFollow(FOLLOW_1);
            ruleObjects();

            state._fsp--;

             after(grammarAccess.getObjectsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObjects"


    // $ANTLR start "ruleObjects"
    // InternalPDDL.g:972:1: ruleObjects : ( ( rule__Objects__Group__0 ) ) ;
    public final void ruleObjects() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:976:2: ( ( ( rule__Objects__Group__0 ) ) )
            // InternalPDDL.g:977:2: ( ( rule__Objects__Group__0 ) )
            {
            // InternalPDDL.g:977:2: ( ( rule__Objects__Group__0 ) )
            // InternalPDDL.g:978:3: ( rule__Objects__Group__0 )
            {
             before(grammarAccess.getObjectsAccess().getGroup()); 
            // InternalPDDL.g:979:3: ( rule__Objects__Group__0 )
            // InternalPDDL.g:979:4: rule__Objects__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Objects__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getObjectsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjects"


    // $ANTLR start "entryRuleObjectsList"
    // InternalPDDL.g:988:1: entryRuleObjectsList : ruleObjectsList EOF ;
    public final void entryRuleObjectsList() throws RecognitionException {
        try {
            // InternalPDDL.g:989:1: ( ruleObjectsList EOF )
            // InternalPDDL.g:990:1: ruleObjectsList EOF
            {
             before(grammarAccess.getObjectsListRule()); 
            pushFollow(FOLLOW_1);
            ruleObjectsList();

            state._fsp--;

             after(grammarAccess.getObjectsListRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObjectsList"


    // $ANTLR start "ruleObjectsList"
    // InternalPDDL.g:997:1: ruleObjectsList : ( ( ( rule__ObjectsList__NewObjAssignment ) ) ( ( rule__ObjectsList__NewObjAssignment )* ) ) ;
    public final void ruleObjectsList() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1001:2: ( ( ( ( rule__ObjectsList__NewObjAssignment ) ) ( ( rule__ObjectsList__NewObjAssignment )* ) ) )
            // InternalPDDL.g:1002:2: ( ( ( rule__ObjectsList__NewObjAssignment ) ) ( ( rule__ObjectsList__NewObjAssignment )* ) )
            {
            // InternalPDDL.g:1002:2: ( ( ( rule__ObjectsList__NewObjAssignment ) ) ( ( rule__ObjectsList__NewObjAssignment )* ) )
            // InternalPDDL.g:1003:3: ( ( rule__ObjectsList__NewObjAssignment ) ) ( ( rule__ObjectsList__NewObjAssignment )* )
            {
            // InternalPDDL.g:1003:3: ( ( rule__ObjectsList__NewObjAssignment ) )
            // InternalPDDL.g:1004:4: ( rule__ObjectsList__NewObjAssignment )
            {
             before(grammarAccess.getObjectsListAccess().getNewObjAssignment()); 
            // InternalPDDL.g:1005:4: ( rule__ObjectsList__NewObjAssignment )
            // InternalPDDL.g:1005:5: rule__ObjectsList__NewObjAssignment
            {
            pushFollow(FOLLOW_3);
            rule__ObjectsList__NewObjAssignment();

            state._fsp--;


            }

             after(grammarAccess.getObjectsListAccess().getNewObjAssignment()); 

            }

            // InternalPDDL.g:1008:3: ( ( rule__ObjectsList__NewObjAssignment )* )
            // InternalPDDL.g:1009:4: ( rule__ObjectsList__NewObjAssignment )*
            {
             before(grammarAccess.getObjectsListAccess().getNewObjAssignment()); 
            // InternalPDDL.g:1010:4: ( rule__ObjectsList__NewObjAssignment )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_NAME) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalPDDL.g:1010:5: rule__ObjectsList__NewObjAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__ObjectsList__NewObjAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getObjectsListAccess().getNewObjAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjectsList"


    // $ANTLR start "entryRuleNewObjects"
    // InternalPDDL.g:1020:1: entryRuleNewObjects : ruleNewObjects EOF ;
    public final void entryRuleNewObjects() throws RecognitionException {
        try {
            // InternalPDDL.g:1021:1: ( ruleNewObjects EOF )
            // InternalPDDL.g:1022:1: ruleNewObjects EOF
            {
             before(grammarAccess.getNewObjectsRule()); 
            pushFollow(FOLLOW_1);
            ruleNewObjects();

            state._fsp--;

             after(grammarAccess.getNewObjectsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNewObjects"


    // $ANTLR start "ruleNewObjects"
    // InternalPDDL.g:1029:1: ruleNewObjects : ( ( rule__NewObjects__Group__0 ) ) ;
    public final void ruleNewObjects() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1033:2: ( ( ( rule__NewObjects__Group__0 ) ) )
            // InternalPDDL.g:1034:2: ( ( rule__NewObjects__Group__0 ) )
            {
            // InternalPDDL.g:1034:2: ( ( rule__NewObjects__Group__0 ) )
            // InternalPDDL.g:1035:3: ( rule__NewObjects__Group__0 )
            {
             before(grammarAccess.getNewObjectsAccess().getGroup()); 
            // InternalPDDL.g:1036:3: ( rule__NewObjects__Group__0 )
            // InternalPDDL.g:1036:4: rule__NewObjects__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NewObjects__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNewObjectsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNewObjects"


    // $ANTLR start "entryRuleInit"
    // InternalPDDL.g:1045:1: entryRuleInit : ruleInit EOF ;
    public final void entryRuleInit() throws RecognitionException {
        try {
            // InternalPDDL.g:1046:1: ( ruleInit EOF )
            // InternalPDDL.g:1047:1: ruleInit EOF
            {
             before(grammarAccess.getInitRule()); 
            pushFollow(FOLLOW_1);
            ruleInit();

            state._fsp--;

             after(grammarAccess.getInitRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInit"


    // $ANTLR start "ruleInit"
    // InternalPDDL.g:1054:1: ruleInit : ( ( rule__Init__Group__0 ) ) ;
    public final void ruleInit() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1058:2: ( ( ( rule__Init__Group__0 ) ) )
            // InternalPDDL.g:1059:2: ( ( rule__Init__Group__0 ) )
            {
            // InternalPDDL.g:1059:2: ( ( rule__Init__Group__0 ) )
            // InternalPDDL.g:1060:3: ( rule__Init__Group__0 )
            {
             before(grammarAccess.getInitAccess().getGroup()); 
            // InternalPDDL.g:1061:3: ( rule__Init__Group__0 )
            // InternalPDDL.g:1061:4: rule__Init__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Init__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInitAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInit"


    // $ANTLR start "entryRulePredicatPB"
    // InternalPDDL.g:1070:1: entryRulePredicatPB : rulePredicatPB EOF ;
    public final void entryRulePredicatPB() throws RecognitionException {
        try {
            // InternalPDDL.g:1071:1: ( rulePredicatPB EOF )
            // InternalPDDL.g:1072:1: rulePredicatPB EOF
            {
             before(grammarAccess.getPredicatPBRule()); 
            pushFollow(FOLLOW_1);
            rulePredicatPB();

            state._fsp--;

             after(grammarAccess.getPredicatPBRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePredicatPB"


    // $ANTLR start "rulePredicatPB"
    // InternalPDDL.g:1079:1: rulePredicatPB : ( ( rule__PredicatPB__Group__0 ) ) ;
    public final void rulePredicatPB() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1083:2: ( ( ( rule__PredicatPB__Group__0 ) ) )
            // InternalPDDL.g:1084:2: ( ( rule__PredicatPB__Group__0 ) )
            {
            // InternalPDDL.g:1084:2: ( ( rule__PredicatPB__Group__0 ) )
            // InternalPDDL.g:1085:3: ( rule__PredicatPB__Group__0 )
            {
             before(grammarAccess.getPredicatPBAccess().getGroup()); 
            // InternalPDDL.g:1086:3: ( rule__PredicatPB__Group__0 )
            // InternalPDDL.g:1086:4: rule__PredicatPB__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PredicatPB__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPredicatPBAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePredicatPB"


    // $ANTLR start "entryRulePDDLObject"
    // InternalPDDL.g:1095:1: entryRulePDDLObject : rulePDDLObject EOF ;
    public final void entryRulePDDLObject() throws RecognitionException {
        try {
            // InternalPDDL.g:1096:1: ( rulePDDLObject EOF )
            // InternalPDDL.g:1097:1: rulePDDLObject EOF
            {
             before(grammarAccess.getPDDLObjectRule()); 
            pushFollow(FOLLOW_1);
            rulePDDLObject();

            state._fsp--;

             after(grammarAccess.getPDDLObjectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePDDLObject"


    // $ANTLR start "rulePDDLObject"
    // InternalPDDL.g:1104:1: rulePDDLObject : ( ( rule__PDDLObject__ObjectNameAssignment ) ) ;
    public final void rulePDDLObject() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1108:2: ( ( ( rule__PDDLObject__ObjectNameAssignment ) ) )
            // InternalPDDL.g:1109:2: ( ( rule__PDDLObject__ObjectNameAssignment ) )
            {
            // InternalPDDL.g:1109:2: ( ( rule__PDDLObject__ObjectNameAssignment ) )
            // InternalPDDL.g:1110:3: ( rule__PDDLObject__ObjectNameAssignment )
            {
             before(grammarAccess.getPDDLObjectAccess().getObjectNameAssignment()); 
            // InternalPDDL.g:1111:3: ( rule__PDDLObject__ObjectNameAssignment )
            // InternalPDDL.g:1111:4: rule__PDDLObject__ObjectNameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__PDDLObject__ObjectNameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getPDDLObjectAccess().getObjectNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePDDLObject"


    // $ANTLR start "entryRuleGoal"
    // InternalPDDL.g:1120:1: entryRuleGoal : ruleGoal EOF ;
    public final void entryRuleGoal() throws RecognitionException {
        try {
            // InternalPDDL.g:1121:1: ( ruleGoal EOF )
            // InternalPDDL.g:1122:1: ruleGoal EOF
            {
             before(grammarAccess.getGoalRule()); 
            pushFollow(FOLLOW_1);
            ruleGoal();

            state._fsp--;

             after(grammarAccess.getGoalRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGoal"


    // $ANTLR start "ruleGoal"
    // InternalPDDL.g:1129:1: ruleGoal : ( ( rule__Goal__Group__0 ) ) ;
    public final void ruleGoal() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1133:2: ( ( ( rule__Goal__Group__0 ) ) )
            // InternalPDDL.g:1134:2: ( ( rule__Goal__Group__0 ) )
            {
            // InternalPDDL.g:1134:2: ( ( rule__Goal__Group__0 ) )
            // InternalPDDL.g:1135:3: ( rule__Goal__Group__0 )
            {
             before(grammarAccess.getGoalAccess().getGroup()); 
            // InternalPDDL.g:1136:3: ( rule__Goal__Group__0 )
            // InternalPDDL.g:1136:4: rule__Goal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Goal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGoalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGoal"


    // $ANTLR start "entryRuleLogicalExpressionPB"
    // InternalPDDL.g:1145:1: entryRuleLogicalExpressionPB : ruleLogicalExpressionPB EOF ;
    public final void entryRuleLogicalExpressionPB() throws RecognitionException {
        try {
            // InternalPDDL.g:1146:1: ( ruleLogicalExpressionPB EOF )
            // InternalPDDL.g:1147:1: ruleLogicalExpressionPB EOF
            {
             before(grammarAccess.getLogicalExpressionPBRule()); 
            pushFollow(FOLLOW_1);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLogicalExpressionPB"


    // $ANTLR start "ruleLogicalExpressionPB"
    // InternalPDDL.g:1154:1: ruleLogicalExpressionPB : ( ( rule__LogicalExpressionPB__Alternatives ) ) ;
    public final void ruleLogicalExpressionPB() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1158:2: ( ( ( rule__LogicalExpressionPB__Alternatives ) ) )
            // InternalPDDL.g:1159:2: ( ( rule__LogicalExpressionPB__Alternatives ) )
            {
            // InternalPDDL.g:1159:2: ( ( rule__LogicalExpressionPB__Alternatives ) )
            // InternalPDDL.g:1160:3: ( rule__LogicalExpressionPB__Alternatives )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getAlternatives()); 
            // InternalPDDL.g:1161:3: ( rule__LogicalExpressionPB__Alternatives )
            // InternalPDDL.g:1161:4: rule__LogicalExpressionPB__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLogicalExpressionPB"


    // $ANTLR start "rule__PDDLFile__Alternatives"
    // InternalPDDL.g:1169:1: rule__PDDLFile__Alternatives : ( ( ( rule__PDDLFile__Group_0__0 ) ) | ( ( rule__PDDLFile__Group_1__0 ) ) );
    public final void rule__PDDLFile__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1173:1: ( ( ( rule__PDDLFile__Group_0__0 ) ) | ( ( rule__PDDLFile__Group_1__0 ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_OP) ) {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==20) ) {
                    alt7=2;
                }
                else if ( (LA7_1==19) ) {
                    alt7=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 7, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalPDDL.g:1174:2: ( ( rule__PDDLFile__Group_0__0 ) )
                    {
                    // InternalPDDL.g:1174:2: ( ( rule__PDDLFile__Group_0__0 ) )
                    // InternalPDDL.g:1175:3: ( rule__PDDLFile__Group_0__0 )
                    {
                     before(grammarAccess.getPDDLFileAccess().getGroup_0()); 
                    // InternalPDDL.g:1176:3: ( rule__PDDLFile__Group_0__0 )
                    // InternalPDDL.g:1176:4: rule__PDDLFile__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__PDDLFile__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPDDLFileAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:1180:2: ( ( rule__PDDLFile__Group_1__0 ) )
                    {
                    // InternalPDDL.g:1180:2: ( ( rule__PDDLFile__Group_1__0 ) )
                    // InternalPDDL.g:1181:3: ( rule__PDDLFile__Group_1__0 )
                    {
                     before(grammarAccess.getPDDLFileAccess().getGroup_1()); 
                    // InternalPDDL.g:1182:3: ( rule__PDDLFile__Group_1__0 )
                    // InternalPDDL.g:1182:4: rule__PDDLFile__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__PDDLFile__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPDDLFileAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Alternatives"


    // $ANTLR start "rule__Domain__Alternatives_6"
    // InternalPDDL.g:1190:1: rule__Domain__Alternatives_6 : ( ( ( rule__Domain__ActionsAssignment_6_0 ) ) | ( ( rule__Domain__AxiomsAssignment_6_1 ) ) );
    public final void rule__Domain__Alternatives_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1194:1: ( ( ( rule__Domain__ActionsAssignment_6_0 ) ) | ( ( rule__Domain__AxiomsAssignment_6_1 ) ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_OP) ) {
                int LA8_1 = input.LA(2);

                if ( (LA8_1==28) ) {
                    alt8=1;
                }
                else if ( (LA8_1==40) ) {
                    alt8=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalPDDL.g:1195:2: ( ( rule__Domain__ActionsAssignment_6_0 ) )
                    {
                    // InternalPDDL.g:1195:2: ( ( rule__Domain__ActionsAssignment_6_0 ) )
                    // InternalPDDL.g:1196:3: ( rule__Domain__ActionsAssignment_6_0 )
                    {
                     before(grammarAccess.getDomainAccess().getActionsAssignment_6_0()); 
                    // InternalPDDL.g:1197:3: ( rule__Domain__ActionsAssignment_6_0 )
                    // InternalPDDL.g:1197:4: rule__Domain__ActionsAssignment_6_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Domain__ActionsAssignment_6_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getDomainAccess().getActionsAssignment_6_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:1201:2: ( ( rule__Domain__AxiomsAssignment_6_1 ) )
                    {
                    // InternalPDDL.g:1201:2: ( ( rule__Domain__AxiomsAssignment_6_1 ) )
                    // InternalPDDL.g:1202:3: ( rule__Domain__AxiomsAssignment_6_1 )
                    {
                     before(grammarAccess.getDomainAccess().getAxiomsAssignment_6_1()); 
                    // InternalPDDL.g:1203:3: ( rule__Domain__AxiomsAssignment_6_1 )
                    // InternalPDDL.g:1203:4: rule__Domain__AxiomsAssignment_6_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Domain__AxiomsAssignment_6_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getDomainAccess().getAxiomsAssignment_6_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Alternatives_6"


    // $ANTLR start "rule__TypedVariable__Alternatives_2"
    // InternalPDDL.g:1211:1: rule__TypedVariable__Alternatives_2 : ( ( ( rule__TypedVariable__TypeAssignment_2_0 ) ) | ( ( rule__TypedVariable__EitherAssignment_2_1 ) ) );
    public final void rule__TypedVariable__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1215:1: ( ( ( rule__TypedVariable__TypeAssignment_2_0 ) ) | ( ( rule__TypedVariable__EitherAssignment_2_1 ) ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_NAME) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_OP) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalPDDL.g:1216:2: ( ( rule__TypedVariable__TypeAssignment_2_0 ) )
                    {
                    // InternalPDDL.g:1216:2: ( ( rule__TypedVariable__TypeAssignment_2_0 ) )
                    // InternalPDDL.g:1217:3: ( rule__TypedVariable__TypeAssignment_2_0 )
                    {
                     before(grammarAccess.getTypedVariableAccess().getTypeAssignment_2_0()); 
                    // InternalPDDL.g:1218:3: ( rule__TypedVariable__TypeAssignment_2_0 )
                    // InternalPDDL.g:1218:4: rule__TypedVariable__TypeAssignment_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypedVariable__TypeAssignment_2_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypedVariableAccess().getTypeAssignment_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:1222:2: ( ( rule__TypedVariable__EitherAssignment_2_1 ) )
                    {
                    // InternalPDDL.g:1222:2: ( ( rule__TypedVariable__EitherAssignment_2_1 ) )
                    // InternalPDDL.g:1223:3: ( rule__TypedVariable__EitherAssignment_2_1 )
                    {
                     before(grammarAccess.getTypedVariableAccess().getEitherAssignment_2_1()); 
                    // InternalPDDL.g:1224:3: ( rule__TypedVariable__EitherAssignment_2_1 )
                    // InternalPDDL.g:1224:4: rule__TypedVariable__EitherAssignment_2_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypedVariable__EitherAssignment_2_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypedVariableAccess().getEitherAssignment_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__Alternatives_2"


    // $ANTLR start "rule__LogicalExpressionDM__Alternatives"
    // InternalPDDL.g:1232:1: rule__LogicalExpressionDM__Alternatives : ( ( ( rule__LogicalExpressionDM__Group_0__0 ) ) | ( ( rule__LogicalExpressionDM__VAssignment_1 ) ) | ( ( rule__LogicalExpressionDM__Group_2__0 ) ) | ( ( rule__LogicalExpressionDM__Group_3__0 ) ) | ( ( rule__LogicalExpressionDM__Group_4__0 ) ) | ( ( rule__LogicalExpressionDM__Group_5__0 ) ) | ( ( rule__LogicalExpressionDM__Group_6__0 ) ) | ( ( rule__LogicalExpressionDM__Group_7__0 ) ) | ( ( rule__LogicalExpressionDM__Group_8__0 ) ) | ( ( rule__LogicalExpressionDM__Group_9__0 ) ) | ( ( rule__LogicalExpressionDM__Group_10__0 ) ) );
    public final void rule__LogicalExpressionDM__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1236:1: ( ( ( rule__LogicalExpressionDM__Group_0__0 ) ) | ( ( rule__LogicalExpressionDM__VAssignment_1 ) ) | ( ( rule__LogicalExpressionDM__Group_2__0 ) ) | ( ( rule__LogicalExpressionDM__Group_3__0 ) ) | ( ( rule__LogicalExpressionDM__Group_4__0 ) ) | ( ( rule__LogicalExpressionDM__Group_5__0 ) ) | ( ( rule__LogicalExpressionDM__Group_6__0 ) ) | ( ( rule__LogicalExpressionDM__Group_7__0 ) ) | ( ( rule__LogicalExpressionDM__Group_8__0 ) ) | ( ( rule__LogicalExpressionDM__Group_9__0 ) ) | ( ( rule__LogicalExpressionDM__Group_10__0 ) ) )
            int alt10=11;
            alt10 = dfa10.predict(input);
            switch (alt10) {
                case 1 :
                    // InternalPDDL.g:1237:2: ( ( rule__LogicalExpressionDM__Group_0__0 ) )
                    {
                    // InternalPDDL.g:1237:2: ( ( rule__LogicalExpressionDM__Group_0__0 ) )
                    // InternalPDDL.g:1238:3: ( rule__LogicalExpressionDM__Group_0__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_0()); 
                    // InternalPDDL.g:1239:3: ( rule__LogicalExpressionDM__Group_0__0 )
                    // InternalPDDL.g:1239:4: rule__LogicalExpressionDM__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:1243:2: ( ( rule__LogicalExpressionDM__VAssignment_1 ) )
                    {
                    // InternalPDDL.g:1243:2: ( ( rule__LogicalExpressionDM__VAssignment_1 ) )
                    // InternalPDDL.g:1244:3: ( rule__LogicalExpressionDM__VAssignment_1 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getVAssignment_1()); 
                    // InternalPDDL.g:1245:3: ( rule__LogicalExpressionDM__VAssignment_1 )
                    // InternalPDDL.g:1245:4: rule__LogicalExpressionDM__VAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__VAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getVAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPDDL.g:1249:2: ( ( rule__LogicalExpressionDM__Group_2__0 ) )
                    {
                    // InternalPDDL.g:1249:2: ( ( rule__LogicalExpressionDM__Group_2__0 ) )
                    // InternalPDDL.g:1250:3: ( rule__LogicalExpressionDM__Group_2__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_2()); 
                    // InternalPDDL.g:1251:3: ( rule__LogicalExpressionDM__Group_2__0 )
                    // InternalPDDL.g:1251:4: rule__LogicalExpressionDM__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalPDDL.g:1255:2: ( ( rule__LogicalExpressionDM__Group_3__0 ) )
                    {
                    // InternalPDDL.g:1255:2: ( ( rule__LogicalExpressionDM__Group_3__0 ) )
                    // InternalPDDL.g:1256:3: ( rule__LogicalExpressionDM__Group_3__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_3()); 
                    // InternalPDDL.g:1257:3: ( rule__LogicalExpressionDM__Group_3__0 )
                    // InternalPDDL.g:1257:4: rule__LogicalExpressionDM__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalPDDL.g:1261:2: ( ( rule__LogicalExpressionDM__Group_4__0 ) )
                    {
                    // InternalPDDL.g:1261:2: ( ( rule__LogicalExpressionDM__Group_4__0 ) )
                    // InternalPDDL.g:1262:3: ( rule__LogicalExpressionDM__Group_4__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_4()); 
                    // InternalPDDL.g:1263:3: ( rule__LogicalExpressionDM__Group_4__0 )
                    // InternalPDDL.g:1263:4: rule__LogicalExpressionDM__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalPDDL.g:1267:2: ( ( rule__LogicalExpressionDM__Group_5__0 ) )
                    {
                    // InternalPDDL.g:1267:2: ( ( rule__LogicalExpressionDM__Group_5__0 ) )
                    // InternalPDDL.g:1268:3: ( rule__LogicalExpressionDM__Group_5__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_5()); 
                    // InternalPDDL.g:1269:3: ( rule__LogicalExpressionDM__Group_5__0 )
                    // InternalPDDL.g:1269:4: rule__LogicalExpressionDM__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalPDDL.g:1273:2: ( ( rule__LogicalExpressionDM__Group_6__0 ) )
                    {
                    // InternalPDDL.g:1273:2: ( ( rule__LogicalExpressionDM__Group_6__0 ) )
                    // InternalPDDL.g:1274:3: ( rule__LogicalExpressionDM__Group_6__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_6()); 
                    // InternalPDDL.g:1275:3: ( rule__LogicalExpressionDM__Group_6__0 )
                    // InternalPDDL.g:1275:4: rule__LogicalExpressionDM__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_6__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalPDDL.g:1279:2: ( ( rule__LogicalExpressionDM__Group_7__0 ) )
                    {
                    // InternalPDDL.g:1279:2: ( ( rule__LogicalExpressionDM__Group_7__0 ) )
                    // InternalPDDL.g:1280:3: ( rule__LogicalExpressionDM__Group_7__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_7()); 
                    // InternalPDDL.g:1281:3: ( rule__LogicalExpressionDM__Group_7__0 )
                    // InternalPDDL.g:1281:4: rule__LogicalExpressionDM__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_7__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalPDDL.g:1285:2: ( ( rule__LogicalExpressionDM__Group_8__0 ) )
                    {
                    // InternalPDDL.g:1285:2: ( ( rule__LogicalExpressionDM__Group_8__0 ) )
                    // InternalPDDL.g:1286:3: ( rule__LogicalExpressionDM__Group_8__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_8()); 
                    // InternalPDDL.g:1287:3: ( rule__LogicalExpressionDM__Group_8__0 )
                    // InternalPDDL.g:1287:4: rule__LogicalExpressionDM__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_8__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalPDDL.g:1291:2: ( ( rule__LogicalExpressionDM__Group_9__0 ) )
                    {
                    // InternalPDDL.g:1291:2: ( ( rule__LogicalExpressionDM__Group_9__0 ) )
                    // InternalPDDL.g:1292:3: ( rule__LogicalExpressionDM__Group_9__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_9()); 
                    // InternalPDDL.g:1293:3: ( rule__LogicalExpressionDM__Group_9__0 )
                    // InternalPDDL.g:1293:4: rule__LogicalExpressionDM__Group_9__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_9__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_9()); 

                    }


                    }
                    break;
                case 11 :
                    // InternalPDDL.g:1297:2: ( ( rule__LogicalExpressionDM__Group_10__0 ) )
                    {
                    // InternalPDDL.g:1297:2: ( ( rule__LogicalExpressionDM__Group_10__0 ) )
                    // InternalPDDL.g:1298:3: ( rule__LogicalExpressionDM__Group_10__0 )
                    {
                     before(grammarAccess.getLogicalExpressionDMAccess().getGroup_10()); 
                    // InternalPDDL.g:1299:3: ( rule__LogicalExpressionDM__Group_10__0 )
                    // InternalPDDL.g:1299:4: rule__LogicalExpressionDM__Group_10__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionDM__Group_10__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionDMAccess().getGroup_10()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Alternatives"


    // $ANTLR start "rule__Init__Alternatives_3"
    // InternalPDDL.g:1307:1: rule__Init__Alternatives_3 : ( ( ( rule__Init__PredicatAssignment_3_0 ) ) | ( ( rule__Init__Group_3_1__0 ) ) );
    public final void rule__Init__Alternatives_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1311:1: ( ( ( rule__Init__PredicatAssignment_3_0 ) ) | ( ( rule__Init__Group_3_1__0 ) ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_OP) ) {
                alt11=1;
            }
            else if ( (LA11_0==33) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalPDDL.g:1312:2: ( ( rule__Init__PredicatAssignment_3_0 ) )
                    {
                    // InternalPDDL.g:1312:2: ( ( rule__Init__PredicatAssignment_3_0 ) )
                    // InternalPDDL.g:1313:3: ( rule__Init__PredicatAssignment_3_0 )
                    {
                     before(grammarAccess.getInitAccess().getPredicatAssignment_3_0()); 
                    // InternalPDDL.g:1314:3: ( rule__Init__PredicatAssignment_3_0 )
                    // InternalPDDL.g:1314:4: rule__Init__PredicatAssignment_3_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Init__PredicatAssignment_3_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getInitAccess().getPredicatAssignment_3_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:1318:2: ( ( rule__Init__Group_3_1__0 ) )
                    {
                    // InternalPDDL.g:1318:2: ( ( rule__Init__Group_3_1__0 ) )
                    // InternalPDDL.g:1319:3: ( rule__Init__Group_3_1__0 )
                    {
                     before(grammarAccess.getInitAccess().getGroup_3_1()); 
                    // InternalPDDL.g:1320:3: ( rule__Init__Group_3_1__0 )
                    // InternalPDDL.g:1320:4: rule__Init__Group_3_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Init__Group_3_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getInitAccess().getGroup_3_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Alternatives_3"


    // $ANTLR start "rule__LogicalExpressionPB__Alternatives"
    // InternalPDDL.g:1328:1: rule__LogicalExpressionPB__Alternatives : ( ( ( rule__LogicalExpressionPB__PAssignment_0 ) ) | ( ( rule__LogicalExpressionPB__VAssignment_1 ) ) | ( ( rule__LogicalExpressionPB__Group_2__0 ) ) | ( ( rule__LogicalExpressionPB__Group_3__0 ) ) | ( ( rule__LogicalExpressionPB__Group_4__0 ) ) | ( ( rule__LogicalExpressionPB__Group_5__0 ) ) | ( ( rule__LogicalExpressionPB__Group_6__0 ) ) | ( ( rule__LogicalExpressionPB__Group_7__0 ) ) | ( ( rule__LogicalExpressionPB__Group_8__0 ) ) | ( ( rule__LogicalExpressionPB__Group_9__0 ) ) | ( ( rule__LogicalExpressionPB__Group_10__0 ) ) );
    public final void rule__LogicalExpressionPB__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1332:1: ( ( ( rule__LogicalExpressionPB__PAssignment_0 ) ) | ( ( rule__LogicalExpressionPB__VAssignment_1 ) ) | ( ( rule__LogicalExpressionPB__Group_2__0 ) ) | ( ( rule__LogicalExpressionPB__Group_3__0 ) ) | ( ( rule__LogicalExpressionPB__Group_4__0 ) ) | ( ( rule__LogicalExpressionPB__Group_5__0 ) ) | ( ( rule__LogicalExpressionPB__Group_6__0 ) ) | ( ( rule__LogicalExpressionPB__Group_7__0 ) ) | ( ( rule__LogicalExpressionPB__Group_8__0 ) ) | ( ( rule__LogicalExpressionPB__Group_9__0 ) ) | ( ( rule__LogicalExpressionPB__Group_10__0 ) ) )
            int alt12=11;
            alt12 = dfa12.predict(input);
            switch (alt12) {
                case 1 :
                    // InternalPDDL.g:1333:2: ( ( rule__LogicalExpressionPB__PAssignment_0 ) )
                    {
                    // InternalPDDL.g:1333:2: ( ( rule__LogicalExpressionPB__PAssignment_0 ) )
                    // InternalPDDL.g:1334:3: ( rule__LogicalExpressionPB__PAssignment_0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getPAssignment_0()); 
                    // InternalPDDL.g:1335:3: ( rule__LogicalExpressionPB__PAssignment_0 )
                    // InternalPDDL.g:1335:4: rule__LogicalExpressionPB__PAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__PAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getPAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:1339:2: ( ( rule__LogicalExpressionPB__VAssignment_1 ) )
                    {
                    // InternalPDDL.g:1339:2: ( ( rule__LogicalExpressionPB__VAssignment_1 ) )
                    // InternalPDDL.g:1340:3: ( rule__LogicalExpressionPB__VAssignment_1 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getVAssignment_1()); 
                    // InternalPDDL.g:1341:3: ( rule__LogicalExpressionPB__VAssignment_1 )
                    // InternalPDDL.g:1341:4: rule__LogicalExpressionPB__VAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__VAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getVAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPDDL.g:1345:2: ( ( rule__LogicalExpressionPB__Group_2__0 ) )
                    {
                    // InternalPDDL.g:1345:2: ( ( rule__LogicalExpressionPB__Group_2__0 ) )
                    // InternalPDDL.g:1346:3: ( rule__LogicalExpressionPB__Group_2__0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getGroup_2()); 
                    // InternalPDDL.g:1347:3: ( rule__LogicalExpressionPB__Group_2__0 )
                    // InternalPDDL.g:1347:4: rule__LogicalExpressionPB__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalPDDL.g:1351:2: ( ( rule__LogicalExpressionPB__Group_3__0 ) )
                    {
                    // InternalPDDL.g:1351:2: ( ( rule__LogicalExpressionPB__Group_3__0 ) )
                    // InternalPDDL.g:1352:3: ( rule__LogicalExpressionPB__Group_3__0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getGroup_3()); 
                    // InternalPDDL.g:1353:3: ( rule__LogicalExpressionPB__Group_3__0 )
                    // InternalPDDL.g:1353:4: rule__LogicalExpressionPB__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalPDDL.g:1357:2: ( ( rule__LogicalExpressionPB__Group_4__0 ) )
                    {
                    // InternalPDDL.g:1357:2: ( ( rule__LogicalExpressionPB__Group_4__0 ) )
                    // InternalPDDL.g:1358:3: ( rule__LogicalExpressionPB__Group_4__0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getGroup_4()); 
                    // InternalPDDL.g:1359:3: ( rule__LogicalExpressionPB__Group_4__0 )
                    // InternalPDDL.g:1359:4: rule__LogicalExpressionPB__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalPDDL.g:1363:2: ( ( rule__LogicalExpressionPB__Group_5__0 ) )
                    {
                    // InternalPDDL.g:1363:2: ( ( rule__LogicalExpressionPB__Group_5__0 ) )
                    // InternalPDDL.g:1364:3: ( rule__LogicalExpressionPB__Group_5__0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getGroup_5()); 
                    // InternalPDDL.g:1365:3: ( rule__LogicalExpressionPB__Group_5__0 )
                    // InternalPDDL.g:1365:4: rule__LogicalExpressionPB__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getGroup_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalPDDL.g:1369:2: ( ( rule__LogicalExpressionPB__Group_6__0 ) )
                    {
                    // InternalPDDL.g:1369:2: ( ( rule__LogicalExpressionPB__Group_6__0 ) )
                    // InternalPDDL.g:1370:3: ( rule__LogicalExpressionPB__Group_6__0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getGroup_6()); 
                    // InternalPDDL.g:1371:3: ( rule__LogicalExpressionPB__Group_6__0 )
                    // InternalPDDL.g:1371:4: rule__LogicalExpressionPB__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__Group_6__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getGroup_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalPDDL.g:1375:2: ( ( rule__LogicalExpressionPB__Group_7__0 ) )
                    {
                    // InternalPDDL.g:1375:2: ( ( rule__LogicalExpressionPB__Group_7__0 ) )
                    // InternalPDDL.g:1376:3: ( rule__LogicalExpressionPB__Group_7__0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getGroup_7()); 
                    // InternalPDDL.g:1377:3: ( rule__LogicalExpressionPB__Group_7__0 )
                    // InternalPDDL.g:1377:4: rule__LogicalExpressionPB__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__Group_7__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getGroup_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalPDDL.g:1381:2: ( ( rule__LogicalExpressionPB__Group_8__0 ) )
                    {
                    // InternalPDDL.g:1381:2: ( ( rule__LogicalExpressionPB__Group_8__0 ) )
                    // InternalPDDL.g:1382:3: ( rule__LogicalExpressionPB__Group_8__0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getGroup_8()); 
                    // InternalPDDL.g:1383:3: ( rule__LogicalExpressionPB__Group_8__0 )
                    // InternalPDDL.g:1383:4: rule__LogicalExpressionPB__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__Group_8__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getGroup_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalPDDL.g:1387:2: ( ( rule__LogicalExpressionPB__Group_9__0 ) )
                    {
                    // InternalPDDL.g:1387:2: ( ( rule__LogicalExpressionPB__Group_9__0 ) )
                    // InternalPDDL.g:1388:3: ( rule__LogicalExpressionPB__Group_9__0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getGroup_9()); 
                    // InternalPDDL.g:1389:3: ( rule__LogicalExpressionPB__Group_9__0 )
                    // InternalPDDL.g:1389:4: rule__LogicalExpressionPB__Group_9__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__Group_9__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getGroup_9()); 

                    }


                    }
                    break;
                case 11 :
                    // InternalPDDL.g:1393:2: ( ( rule__LogicalExpressionPB__Group_10__0 ) )
                    {
                    // InternalPDDL.g:1393:2: ( ( rule__LogicalExpressionPB__Group_10__0 ) )
                    // InternalPDDL.g:1394:3: ( rule__LogicalExpressionPB__Group_10__0 )
                    {
                     before(grammarAccess.getLogicalExpressionPBAccess().getGroup_10()); 
                    // InternalPDDL.g:1395:3: ( rule__LogicalExpressionPB__Group_10__0 )
                    // InternalPDDL.g:1395:4: rule__LogicalExpressionPB__Group_10__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LogicalExpressionPB__Group_10__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogicalExpressionPBAccess().getGroup_10()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Alternatives"


    // $ANTLR start "rule__PDDLModel__Group__0"
    // InternalPDDL.g:1403:1: rule__PDDLModel__Group__0 : rule__PDDLModel__Group__0__Impl rule__PDDLModel__Group__1 ;
    public final void rule__PDDLModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1407:1: ( rule__PDDLModel__Group__0__Impl rule__PDDLModel__Group__1 )
            // InternalPDDL.g:1408:2: rule__PDDLModel__Group__0__Impl rule__PDDLModel__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__PDDLModel__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLModel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group__0"


    // $ANTLR start "rule__PDDLModel__Group__0__Impl"
    // InternalPDDL.g:1415:1: rule__PDDLModel__Group__0__Impl : ( () ) ;
    public final void rule__PDDLModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1419:1: ( ( () ) )
            // InternalPDDL.g:1420:1: ( () )
            {
            // InternalPDDL.g:1420:1: ( () )
            // InternalPDDL.g:1421:2: ()
            {
             before(grammarAccess.getPDDLModelAccess().getPDDLModelAction_0()); 
            // InternalPDDL.g:1422:2: ()
            // InternalPDDL.g:1422:3: 
            {
            }

             after(grammarAccess.getPDDLModelAccess().getPDDLModelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group__0__Impl"


    // $ANTLR start "rule__PDDLModel__Group__1"
    // InternalPDDL.g:1430:1: rule__PDDLModel__Group__1 : rule__PDDLModel__Group__1__Impl ;
    public final void rule__PDDLModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1434:1: ( rule__PDDLModel__Group__1__Impl )
            // InternalPDDL.g:1435:2: rule__PDDLModel__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PDDLModel__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group__1"


    // $ANTLR start "rule__PDDLModel__Group__1__Impl"
    // InternalPDDL.g:1441:1: rule__PDDLModel__Group__1__Impl : ( ( rule__PDDLModel__Group_1__0 )? ) ;
    public final void rule__PDDLModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1445:1: ( ( ( rule__PDDLModel__Group_1__0 )? ) )
            // InternalPDDL.g:1446:1: ( ( rule__PDDLModel__Group_1__0 )? )
            {
            // InternalPDDL.g:1446:1: ( ( rule__PDDLModel__Group_1__0 )? )
            // InternalPDDL.g:1447:2: ( rule__PDDLModel__Group_1__0 )?
            {
             before(grammarAccess.getPDDLModelAccess().getGroup_1()); 
            // InternalPDDL.g:1448:2: ( rule__PDDLModel__Group_1__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_OP) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalPDDL.g:1448:3: rule__PDDLModel__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__PDDLModel__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPDDLModelAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group__1__Impl"


    // $ANTLR start "rule__PDDLModel__Group_1__0"
    // InternalPDDL.g:1457:1: rule__PDDLModel__Group_1__0 : rule__PDDLModel__Group_1__0__Impl rule__PDDLModel__Group_1__1 ;
    public final void rule__PDDLModel__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1461:1: ( rule__PDDLModel__Group_1__0__Impl rule__PDDLModel__Group_1__1 )
            // InternalPDDL.g:1462:2: rule__PDDLModel__Group_1__0__Impl rule__PDDLModel__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__PDDLModel__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLModel__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group_1__0"


    // $ANTLR start "rule__PDDLModel__Group_1__0__Impl"
    // InternalPDDL.g:1469:1: rule__PDDLModel__Group_1__0__Impl : ( RULE_OP ) ;
    public final void rule__PDDLModel__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1473:1: ( ( RULE_OP ) )
            // InternalPDDL.g:1474:1: ( RULE_OP )
            {
            // InternalPDDL.g:1474:1: ( RULE_OP )
            // InternalPDDL.g:1475:2: RULE_OP
            {
             before(grammarAccess.getPDDLModelAccess().getOPTerminalRuleCall_1_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getPDDLModelAccess().getOPTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group_1__0__Impl"


    // $ANTLR start "rule__PDDLModel__Group_1__1"
    // InternalPDDL.g:1484:1: rule__PDDLModel__Group_1__1 : rule__PDDLModel__Group_1__1__Impl rule__PDDLModel__Group_1__2 ;
    public final void rule__PDDLModel__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1488:1: ( rule__PDDLModel__Group_1__1__Impl rule__PDDLModel__Group_1__2 )
            // InternalPDDL.g:1489:2: rule__PDDLModel__Group_1__1__Impl rule__PDDLModel__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__PDDLModel__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLModel__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group_1__1"


    // $ANTLR start "rule__PDDLModel__Group_1__1__Impl"
    // InternalPDDL.g:1496:1: rule__PDDLModel__Group_1__1__Impl : ( 'define' ) ;
    public final void rule__PDDLModel__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1500:1: ( ( 'define' ) )
            // InternalPDDL.g:1501:1: ( 'define' )
            {
            // InternalPDDL.g:1501:1: ( 'define' )
            // InternalPDDL.g:1502:2: 'define'
            {
             before(grammarAccess.getPDDLModelAccess().getDefineKeyword_1_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getPDDLModelAccess().getDefineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group_1__1__Impl"


    // $ANTLR start "rule__PDDLModel__Group_1__2"
    // InternalPDDL.g:1511:1: rule__PDDLModel__Group_1__2 : rule__PDDLModel__Group_1__2__Impl rule__PDDLModel__Group_1__3 ;
    public final void rule__PDDLModel__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1515:1: ( rule__PDDLModel__Group_1__2__Impl rule__PDDLModel__Group_1__3 )
            // InternalPDDL.g:1516:2: rule__PDDLModel__Group_1__2__Impl rule__PDDLModel__Group_1__3
            {
            pushFollow(FOLLOW_9);
            rule__PDDLModel__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLModel__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group_1__2"


    // $ANTLR start "rule__PDDLModel__Group_1__2__Impl"
    // InternalPDDL.g:1523:1: rule__PDDLModel__Group_1__2__Impl : ( ( rule__PDDLModel__FileAssignment_1_2 ) ) ;
    public final void rule__PDDLModel__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1527:1: ( ( ( rule__PDDLModel__FileAssignment_1_2 ) ) )
            // InternalPDDL.g:1528:1: ( ( rule__PDDLModel__FileAssignment_1_2 ) )
            {
            // InternalPDDL.g:1528:1: ( ( rule__PDDLModel__FileAssignment_1_2 ) )
            // InternalPDDL.g:1529:2: ( rule__PDDLModel__FileAssignment_1_2 )
            {
             before(grammarAccess.getPDDLModelAccess().getFileAssignment_1_2()); 
            // InternalPDDL.g:1530:2: ( rule__PDDLModel__FileAssignment_1_2 )
            // InternalPDDL.g:1530:3: rule__PDDLModel__FileAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__PDDLModel__FileAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getPDDLModelAccess().getFileAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group_1__2__Impl"


    // $ANTLR start "rule__PDDLModel__Group_1__3"
    // InternalPDDL.g:1538:1: rule__PDDLModel__Group_1__3 : rule__PDDLModel__Group_1__3__Impl ;
    public final void rule__PDDLModel__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1542:1: ( rule__PDDLModel__Group_1__3__Impl )
            // InternalPDDL.g:1543:2: rule__PDDLModel__Group_1__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PDDLModel__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group_1__3"


    // $ANTLR start "rule__PDDLModel__Group_1__3__Impl"
    // InternalPDDL.g:1549:1: rule__PDDLModel__Group_1__3__Impl : ( RULE_CP ) ;
    public final void rule__PDDLModel__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1553:1: ( ( RULE_CP ) )
            // InternalPDDL.g:1554:1: ( RULE_CP )
            {
            // InternalPDDL.g:1554:1: ( RULE_CP )
            // InternalPDDL.g:1555:2: RULE_CP
            {
             before(grammarAccess.getPDDLModelAccess().getCPTerminalRuleCall_1_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getPDDLModelAccess().getCPTerminalRuleCall_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__Group_1__3__Impl"


    // $ANTLR start "rule__PDDLFile__Group_0__0"
    // InternalPDDL.g:1565:1: rule__PDDLFile__Group_0__0 : rule__PDDLFile__Group_0__0__Impl rule__PDDLFile__Group_0__1 ;
    public final void rule__PDDLFile__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1569:1: ( rule__PDDLFile__Group_0__0__Impl rule__PDDLFile__Group_0__1 )
            // InternalPDDL.g:1570:2: rule__PDDLFile__Group_0__0__Impl rule__PDDLFile__Group_0__1
            {
            pushFollow(FOLLOW_10);
            rule__PDDLFile__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__0"


    // $ANTLR start "rule__PDDLFile__Group_0__0__Impl"
    // InternalPDDL.g:1577:1: rule__PDDLFile__Group_0__0__Impl : ( RULE_OP ) ;
    public final void rule__PDDLFile__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1581:1: ( ( RULE_OP ) )
            // InternalPDDL.g:1582:1: ( RULE_OP )
            {
            // InternalPDDL.g:1582:1: ( RULE_OP )
            // InternalPDDL.g:1583:2: RULE_OP
            {
             before(grammarAccess.getPDDLFileAccess().getOPTerminalRuleCall_0_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getPDDLFileAccess().getOPTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__0__Impl"


    // $ANTLR start "rule__PDDLFile__Group_0__1"
    // InternalPDDL.g:1592:1: rule__PDDLFile__Group_0__1 : rule__PDDLFile__Group_0__1__Impl rule__PDDLFile__Group_0__2 ;
    public final void rule__PDDLFile__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1596:1: ( rule__PDDLFile__Group_0__1__Impl rule__PDDLFile__Group_0__2 )
            // InternalPDDL.g:1597:2: rule__PDDLFile__Group_0__1__Impl rule__PDDLFile__Group_0__2
            {
            pushFollow(FOLLOW_11);
            rule__PDDLFile__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__1"


    // $ANTLR start "rule__PDDLFile__Group_0__1__Impl"
    // InternalPDDL.g:1604:1: rule__PDDLFile__Group_0__1__Impl : ( 'domain' ) ;
    public final void rule__PDDLFile__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1608:1: ( ( 'domain' ) )
            // InternalPDDL.g:1609:1: ( 'domain' )
            {
            // InternalPDDL.g:1609:1: ( 'domain' )
            // InternalPDDL.g:1610:2: 'domain'
            {
             before(grammarAccess.getPDDLFileAccess().getDomainKeyword_0_1()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getPDDLFileAccess().getDomainKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__1__Impl"


    // $ANTLR start "rule__PDDLFile__Group_0__2"
    // InternalPDDL.g:1619:1: rule__PDDLFile__Group_0__2 : rule__PDDLFile__Group_0__2__Impl rule__PDDLFile__Group_0__3 ;
    public final void rule__PDDLFile__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1623:1: ( rule__PDDLFile__Group_0__2__Impl rule__PDDLFile__Group_0__3 )
            // InternalPDDL.g:1624:2: rule__PDDLFile__Group_0__2__Impl rule__PDDLFile__Group_0__3
            {
            pushFollow(FOLLOW_9);
            rule__PDDLFile__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__2"


    // $ANTLR start "rule__PDDLFile__Group_0__2__Impl"
    // InternalPDDL.g:1631:1: rule__PDDLFile__Group_0__2__Impl : ( ( rule__PDDLFile__NameDAssignment_0_2 ) ) ;
    public final void rule__PDDLFile__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1635:1: ( ( ( rule__PDDLFile__NameDAssignment_0_2 ) ) )
            // InternalPDDL.g:1636:1: ( ( rule__PDDLFile__NameDAssignment_0_2 ) )
            {
            // InternalPDDL.g:1636:1: ( ( rule__PDDLFile__NameDAssignment_0_2 ) )
            // InternalPDDL.g:1637:2: ( rule__PDDLFile__NameDAssignment_0_2 )
            {
             before(grammarAccess.getPDDLFileAccess().getNameDAssignment_0_2()); 
            // InternalPDDL.g:1638:2: ( rule__PDDLFile__NameDAssignment_0_2 )
            // InternalPDDL.g:1638:3: rule__PDDLFile__NameDAssignment_0_2
            {
            pushFollow(FOLLOW_2);
            rule__PDDLFile__NameDAssignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getPDDLFileAccess().getNameDAssignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__2__Impl"


    // $ANTLR start "rule__PDDLFile__Group_0__3"
    // InternalPDDL.g:1646:1: rule__PDDLFile__Group_0__3 : rule__PDDLFile__Group_0__3__Impl rule__PDDLFile__Group_0__4 ;
    public final void rule__PDDLFile__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1650:1: ( rule__PDDLFile__Group_0__3__Impl rule__PDDLFile__Group_0__4 )
            // InternalPDDL.g:1651:2: rule__PDDLFile__Group_0__3__Impl rule__PDDLFile__Group_0__4
            {
            pushFollow(FOLLOW_7);
            rule__PDDLFile__Group_0__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_0__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__3"


    // $ANTLR start "rule__PDDLFile__Group_0__3__Impl"
    // InternalPDDL.g:1658:1: rule__PDDLFile__Group_0__3__Impl : ( RULE_CP ) ;
    public final void rule__PDDLFile__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1662:1: ( ( RULE_CP ) )
            // InternalPDDL.g:1663:1: ( RULE_CP )
            {
            // InternalPDDL.g:1663:1: ( RULE_CP )
            // InternalPDDL.g:1664:2: RULE_CP
            {
             before(grammarAccess.getPDDLFileAccess().getCPTerminalRuleCall_0_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getPDDLFileAccess().getCPTerminalRuleCall_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__3__Impl"


    // $ANTLR start "rule__PDDLFile__Group_0__4"
    // InternalPDDL.g:1673:1: rule__PDDLFile__Group_0__4 : rule__PDDLFile__Group_0__4__Impl ;
    public final void rule__PDDLFile__Group_0__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1677:1: ( rule__PDDLFile__Group_0__4__Impl )
            // InternalPDDL.g:1678:2: rule__PDDLFile__Group_0__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_0__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__4"


    // $ANTLR start "rule__PDDLFile__Group_0__4__Impl"
    // InternalPDDL.g:1684:1: rule__PDDLFile__Group_0__4__Impl : ( ( rule__PDDLFile__ContentDAssignment_0_4 ) ) ;
    public final void rule__PDDLFile__Group_0__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1688:1: ( ( ( rule__PDDLFile__ContentDAssignment_0_4 ) ) )
            // InternalPDDL.g:1689:1: ( ( rule__PDDLFile__ContentDAssignment_0_4 ) )
            {
            // InternalPDDL.g:1689:1: ( ( rule__PDDLFile__ContentDAssignment_0_4 ) )
            // InternalPDDL.g:1690:2: ( rule__PDDLFile__ContentDAssignment_0_4 )
            {
             before(grammarAccess.getPDDLFileAccess().getContentDAssignment_0_4()); 
            // InternalPDDL.g:1691:2: ( rule__PDDLFile__ContentDAssignment_0_4 )
            // InternalPDDL.g:1691:3: rule__PDDLFile__ContentDAssignment_0_4
            {
            pushFollow(FOLLOW_2);
            rule__PDDLFile__ContentDAssignment_0_4();

            state._fsp--;


            }

             after(grammarAccess.getPDDLFileAccess().getContentDAssignment_0_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_0__4__Impl"


    // $ANTLR start "rule__PDDLFile__Group_1__0"
    // InternalPDDL.g:1700:1: rule__PDDLFile__Group_1__0 : rule__PDDLFile__Group_1__0__Impl rule__PDDLFile__Group_1__1 ;
    public final void rule__PDDLFile__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1704:1: ( rule__PDDLFile__Group_1__0__Impl rule__PDDLFile__Group_1__1 )
            // InternalPDDL.g:1705:2: rule__PDDLFile__Group_1__0__Impl rule__PDDLFile__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__PDDLFile__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__0"


    // $ANTLR start "rule__PDDLFile__Group_1__0__Impl"
    // InternalPDDL.g:1712:1: rule__PDDLFile__Group_1__0__Impl : ( RULE_OP ) ;
    public final void rule__PDDLFile__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1716:1: ( ( RULE_OP ) )
            // InternalPDDL.g:1717:1: ( RULE_OP )
            {
            // InternalPDDL.g:1717:1: ( RULE_OP )
            // InternalPDDL.g:1718:2: RULE_OP
            {
             before(grammarAccess.getPDDLFileAccess().getOPTerminalRuleCall_1_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getPDDLFileAccess().getOPTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__0__Impl"


    // $ANTLR start "rule__PDDLFile__Group_1__1"
    // InternalPDDL.g:1727:1: rule__PDDLFile__Group_1__1 : rule__PDDLFile__Group_1__1__Impl rule__PDDLFile__Group_1__2 ;
    public final void rule__PDDLFile__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1731:1: ( rule__PDDLFile__Group_1__1__Impl rule__PDDLFile__Group_1__2 )
            // InternalPDDL.g:1732:2: rule__PDDLFile__Group_1__1__Impl rule__PDDLFile__Group_1__2
            {
            pushFollow(FOLLOW_11);
            rule__PDDLFile__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__1"


    // $ANTLR start "rule__PDDLFile__Group_1__1__Impl"
    // InternalPDDL.g:1739:1: rule__PDDLFile__Group_1__1__Impl : ( 'problem' ) ;
    public final void rule__PDDLFile__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1743:1: ( ( 'problem' ) )
            // InternalPDDL.g:1744:1: ( 'problem' )
            {
            // InternalPDDL.g:1744:1: ( 'problem' )
            // InternalPDDL.g:1745:2: 'problem'
            {
             before(grammarAccess.getPDDLFileAccess().getProblemKeyword_1_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getPDDLFileAccess().getProblemKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__1__Impl"


    // $ANTLR start "rule__PDDLFile__Group_1__2"
    // InternalPDDL.g:1754:1: rule__PDDLFile__Group_1__2 : rule__PDDLFile__Group_1__2__Impl rule__PDDLFile__Group_1__3 ;
    public final void rule__PDDLFile__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1758:1: ( rule__PDDLFile__Group_1__2__Impl rule__PDDLFile__Group_1__3 )
            // InternalPDDL.g:1759:2: rule__PDDLFile__Group_1__2__Impl rule__PDDLFile__Group_1__3
            {
            pushFollow(FOLLOW_9);
            rule__PDDLFile__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__2"


    // $ANTLR start "rule__PDDLFile__Group_1__2__Impl"
    // InternalPDDL.g:1766:1: rule__PDDLFile__Group_1__2__Impl : ( ( rule__PDDLFile__NamePAssignment_1_2 ) ) ;
    public final void rule__PDDLFile__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1770:1: ( ( ( rule__PDDLFile__NamePAssignment_1_2 ) ) )
            // InternalPDDL.g:1771:1: ( ( rule__PDDLFile__NamePAssignment_1_2 ) )
            {
            // InternalPDDL.g:1771:1: ( ( rule__PDDLFile__NamePAssignment_1_2 ) )
            // InternalPDDL.g:1772:2: ( rule__PDDLFile__NamePAssignment_1_2 )
            {
             before(grammarAccess.getPDDLFileAccess().getNamePAssignment_1_2()); 
            // InternalPDDL.g:1773:2: ( rule__PDDLFile__NamePAssignment_1_2 )
            // InternalPDDL.g:1773:3: rule__PDDLFile__NamePAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__PDDLFile__NamePAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getPDDLFileAccess().getNamePAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__2__Impl"


    // $ANTLR start "rule__PDDLFile__Group_1__3"
    // InternalPDDL.g:1781:1: rule__PDDLFile__Group_1__3 : rule__PDDLFile__Group_1__3__Impl rule__PDDLFile__Group_1__4 ;
    public final void rule__PDDLFile__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1785:1: ( rule__PDDLFile__Group_1__3__Impl rule__PDDLFile__Group_1__4 )
            // InternalPDDL.g:1786:2: rule__PDDLFile__Group_1__3__Impl rule__PDDLFile__Group_1__4
            {
            pushFollow(FOLLOW_7);
            rule__PDDLFile__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__3"


    // $ANTLR start "rule__PDDLFile__Group_1__3__Impl"
    // InternalPDDL.g:1793:1: rule__PDDLFile__Group_1__3__Impl : ( RULE_CP ) ;
    public final void rule__PDDLFile__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1797:1: ( ( RULE_CP ) )
            // InternalPDDL.g:1798:1: ( RULE_CP )
            {
            // InternalPDDL.g:1798:1: ( RULE_CP )
            // InternalPDDL.g:1799:2: RULE_CP
            {
             before(grammarAccess.getPDDLFileAccess().getCPTerminalRuleCall_1_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getPDDLFileAccess().getCPTerminalRuleCall_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__3__Impl"


    // $ANTLR start "rule__PDDLFile__Group_1__4"
    // InternalPDDL.g:1808:1: rule__PDDLFile__Group_1__4 : rule__PDDLFile__Group_1__4__Impl ;
    public final void rule__PDDLFile__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1812:1: ( rule__PDDLFile__Group_1__4__Impl )
            // InternalPDDL.g:1813:2: rule__PDDLFile__Group_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PDDLFile__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__4"


    // $ANTLR start "rule__PDDLFile__Group_1__4__Impl"
    // InternalPDDL.g:1819:1: rule__PDDLFile__Group_1__4__Impl : ( ( rule__PDDLFile__ContentPAssignment_1_4 ) ) ;
    public final void rule__PDDLFile__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1823:1: ( ( ( rule__PDDLFile__ContentPAssignment_1_4 ) ) )
            // InternalPDDL.g:1824:1: ( ( rule__PDDLFile__ContentPAssignment_1_4 ) )
            {
            // InternalPDDL.g:1824:1: ( ( rule__PDDLFile__ContentPAssignment_1_4 ) )
            // InternalPDDL.g:1825:2: ( rule__PDDLFile__ContentPAssignment_1_4 )
            {
             before(grammarAccess.getPDDLFileAccess().getContentPAssignment_1_4()); 
            // InternalPDDL.g:1826:2: ( rule__PDDLFile__ContentPAssignment_1_4 )
            // InternalPDDL.g:1826:3: rule__PDDLFile__ContentPAssignment_1_4
            {
            pushFollow(FOLLOW_2);
            rule__PDDLFile__ContentPAssignment_1_4();

            state._fsp--;


            }

             after(grammarAccess.getPDDLFileAccess().getContentPAssignment_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__Group_1__4__Impl"


    // $ANTLR start "rule__Domain__Group__0"
    // InternalPDDL.g:1835:1: rule__Domain__Group__0 : rule__Domain__Group__0__Impl rule__Domain__Group__1 ;
    public final void rule__Domain__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1839:1: ( rule__Domain__Group__0__Impl rule__Domain__Group__1 )
            // InternalPDDL.g:1840:2: rule__Domain__Group__0__Impl rule__Domain__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Domain__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Domain__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__0"


    // $ANTLR start "rule__Domain__Group__0__Impl"
    // InternalPDDL.g:1847:1: rule__Domain__Group__0__Impl : ( () ) ;
    public final void rule__Domain__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1851:1: ( ( () ) )
            // InternalPDDL.g:1852:1: ( () )
            {
            // InternalPDDL.g:1852:1: ( () )
            // InternalPDDL.g:1853:2: ()
            {
             before(grammarAccess.getDomainAccess().getDomainAction_0()); 
            // InternalPDDL.g:1854:2: ()
            // InternalPDDL.g:1854:3: 
            {
            }

             after(grammarAccess.getDomainAccess().getDomainAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__0__Impl"


    // $ANTLR start "rule__Domain__Group__1"
    // InternalPDDL.g:1862:1: rule__Domain__Group__1 : rule__Domain__Group__1__Impl rule__Domain__Group__2 ;
    public final void rule__Domain__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1866:1: ( rule__Domain__Group__1__Impl rule__Domain__Group__2 )
            // InternalPDDL.g:1867:2: rule__Domain__Group__1__Impl rule__Domain__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Domain__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Domain__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__1"


    // $ANTLR start "rule__Domain__Group__1__Impl"
    // InternalPDDL.g:1874:1: rule__Domain__Group__1__Impl : ( ( rule__Domain__ExtendsAssignment_1 )? ) ;
    public final void rule__Domain__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1878:1: ( ( ( rule__Domain__ExtendsAssignment_1 )? ) )
            // InternalPDDL.g:1879:1: ( ( rule__Domain__ExtendsAssignment_1 )? )
            {
            // InternalPDDL.g:1879:1: ( ( rule__Domain__ExtendsAssignment_1 )? )
            // InternalPDDL.g:1880:2: ( rule__Domain__ExtendsAssignment_1 )?
            {
             before(grammarAccess.getDomainAccess().getExtendsAssignment_1()); 
            // InternalPDDL.g:1881:2: ( rule__Domain__ExtendsAssignment_1 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_OP) ) {
                int LA14_1 = input.LA(2);

                if ( (LA14_1==21) ) {
                    alt14=1;
                }
            }
            switch (alt14) {
                case 1 :
                    // InternalPDDL.g:1881:3: rule__Domain__ExtendsAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Domain__ExtendsAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDomainAccess().getExtendsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__1__Impl"


    // $ANTLR start "rule__Domain__Group__2"
    // InternalPDDL.g:1889:1: rule__Domain__Group__2 : rule__Domain__Group__2__Impl rule__Domain__Group__3 ;
    public final void rule__Domain__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1893:1: ( rule__Domain__Group__2__Impl rule__Domain__Group__3 )
            // InternalPDDL.g:1894:2: rule__Domain__Group__2__Impl rule__Domain__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Domain__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Domain__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__2"


    // $ANTLR start "rule__Domain__Group__2__Impl"
    // InternalPDDL.g:1901:1: rule__Domain__Group__2__Impl : ( ( rule__Domain__RequirementsAssignment_2 )? ) ;
    public final void rule__Domain__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1905:1: ( ( ( rule__Domain__RequirementsAssignment_2 )? ) )
            // InternalPDDL.g:1906:1: ( ( rule__Domain__RequirementsAssignment_2 )? )
            {
            // InternalPDDL.g:1906:1: ( ( rule__Domain__RequirementsAssignment_2 )? )
            // InternalPDDL.g:1907:2: ( rule__Domain__RequirementsAssignment_2 )?
            {
             before(grammarAccess.getDomainAccess().getRequirementsAssignment_2()); 
            // InternalPDDL.g:1908:2: ( rule__Domain__RequirementsAssignment_2 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_OP) ) {
                int LA15_1 = input.LA(2);

                if ( (LA15_1==22) ) {
                    alt15=1;
                }
            }
            switch (alt15) {
                case 1 :
                    // InternalPDDL.g:1908:3: rule__Domain__RequirementsAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Domain__RequirementsAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDomainAccess().getRequirementsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__2__Impl"


    // $ANTLR start "rule__Domain__Group__3"
    // InternalPDDL.g:1916:1: rule__Domain__Group__3 : rule__Domain__Group__3__Impl rule__Domain__Group__4 ;
    public final void rule__Domain__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1920:1: ( rule__Domain__Group__3__Impl rule__Domain__Group__4 )
            // InternalPDDL.g:1921:2: rule__Domain__Group__3__Impl rule__Domain__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__Domain__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Domain__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__3"


    // $ANTLR start "rule__Domain__Group__3__Impl"
    // InternalPDDL.g:1928:1: rule__Domain__Group__3__Impl : ( ( rule__Domain__TypesAssignment_3 )? ) ;
    public final void rule__Domain__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1932:1: ( ( ( rule__Domain__TypesAssignment_3 )? ) )
            // InternalPDDL.g:1933:1: ( ( rule__Domain__TypesAssignment_3 )? )
            {
            // InternalPDDL.g:1933:1: ( ( rule__Domain__TypesAssignment_3 )? )
            // InternalPDDL.g:1934:2: ( rule__Domain__TypesAssignment_3 )?
            {
             before(grammarAccess.getDomainAccess().getTypesAssignment_3()); 
            // InternalPDDL.g:1935:2: ( rule__Domain__TypesAssignment_3 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_OP) ) {
                int LA16_1 = input.LA(2);

                if ( (LA16_1==23) ) {
                    alt16=1;
                }
            }
            switch (alt16) {
                case 1 :
                    // InternalPDDL.g:1935:3: rule__Domain__TypesAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Domain__TypesAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDomainAccess().getTypesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__3__Impl"


    // $ANTLR start "rule__Domain__Group__4"
    // InternalPDDL.g:1943:1: rule__Domain__Group__4 : rule__Domain__Group__4__Impl rule__Domain__Group__5 ;
    public final void rule__Domain__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1947:1: ( rule__Domain__Group__4__Impl rule__Domain__Group__5 )
            // InternalPDDL.g:1948:2: rule__Domain__Group__4__Impl rule__Domain__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Domain__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Domain__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__4"


    // $ANTLR start "rule__Domain__Group__4__Impl"
    // InternalPDDL.g:1955:1: rule__Domain__Group__4__Impl : ( ( rule__Domain__PredicatesAssignment_4 )? ) ;
    public final void rule__Domain__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1959:1: ( ( ( rule__Domain__PredicatesAssignment_4 )? ) )
            // InternalPDDL.g:1960:1: ( ( rule__Domain__PredicatesAssignment_4 )? )
            {
            // InternalPDDL.g:1960:1: ( ( rule__Domain__PredicatesAssignment_4 )? )
            // InternalPDDL.g:1961:2: ( rule__Domain__PredicatesAssignment_4 )?
            {
             before(grammarAccess.getDomainAccess().getPredicatesAssignment_4()); 
            // InternalPDDL.g:1962:2: ( rule__Domain__PredicatesAssignment_4 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_OP) ) {
                int LA17_1 = input.LA(2);

                if ( (LA17_1==25) ) {
                    alt17=1;
                }
            }
            switch (alt17) {
                case 1 :
                    // InternalPDDL.g:1962:3: rule__Domain__PredicatesAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Domain__PredicatesAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDomainAccess().getPredicatesAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__4__Impl"


    // $ANTLR start "rule__Domain__Group__5"
    // InternalPDDL.g:1970:1: rule__Domain__Group__5 : rule__Domain__Group__5__Impl rule__Domain__Group__6 ;
    public final void rule__Domain__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1974:1: ( rule__Domain__Group__5__Impl rule__Domain__Group__6 )
            // InternalPDDL.g:1975:2: rule__Domain__Group__5__Impl rule__Domain__Group__6
            {
            pushFollow(FOLLOW_7);
            rule__Domain__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Domain__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__5"


    // $ANTLR start "rule__Domain__Group__5__Impl"
    // InternalPDDL.g:1982:1: rule__Domain__Group__5__Impl : ( ( rule__Domain__FunctionsAssignment_5 )? ) ;
    public final void rule__Domain__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:1986:1: ( ( ( rule__Domain__FunctionsAssignment_5 )? ) )
            // InternalPDDL.g:1987:1: ( ( rule__Domain__FunctionsAssignment_5 )? )
            {
            // InternalPDDL.g:1987:1: ( ( rule__Domain__FunctionsAssignment_5 )? )
            // InternalPDDL.g:1988:2: ( rule__Domain__FunctionsAssignment_5 )?
            {
             before(grammarAccess.getDomainAccess().getFunctionsAssignment_5()); 
            // InternalPDDL.g:1989:2: ( rule__Domain__FunctionsAssignment_5 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==RULE_OP) ) {
                int LA18_1 = input.LA(2);

                if ( (LA18_1==27) ) {
                    alt18=1;
                }
            }
            switch (alt18) {
                case 1 :
                    // InternalPDDL.g:1989:3: rule__Domain__FunctionsAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__Domain__FunctionsAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDomainAccess().getFunctionsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__5__Impl"


    // $ANTLR start "rule__Domain__Group__6"
    // InternalPDDL.g:1997:1: rule__Domain__Group__6 : rule__Domain__Group__6__Impl ;
    public final void rule__Domain__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2001:1: ( rule__Domain__Group__6__Impl )
            // InternalPDDL.g:2002:2: rule__Domain__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Domain__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__6"


    // $ANTLR start "rule__Domain__Group__6__Impl"
    // InternalPDDL.g:2008:1: rule__Domain__Group__6__Impl : ( ( rule__Domain__Alternatives_6 )* ) ;
    public final void rule__Domain__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2012:1: ( ( ( rule__Domain__Alternatives_6 )* ) )
            // InternalPDDL.g:2013:1: ( ( rule__Domain__Alternatives_6 )* )
            {
            // InternalPDDL.g:2013:1: ( ( rule__Domain__Alternatives_6 )* )
            // InternalPDDL.g:2014:2: ( rule__Domain__Alternatives_6 )*
            {
             before(grammarAccess.getDomainAccess().getAlternatives_6()); 
            // InternalPDDL.g:2015:2: ( rule__Domain__Alternatives_6 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==RULE_OP) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalPDDL.g:2015:3: rule__Domain__Alternatives_6
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Domain__Alternatives_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getDomainAccess().getAlternatives_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__Group__6__Impl"


    // $ANTLR start "rule__Extends__Group__0"
    // InternalPDDL.g:2024:1: rule__Extends__Group__0 : rule__Extends__Group__0__Impl rule__Extends__Group__1 ;
    public final void rule__Extends__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2028:1: ( rule__Extends__Group__0__Impl rule__Extends__Group__1 )
            // InternalPDDL.g:2029:2: rule__Extends__Group__0__Impl rule__Extends__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Extends__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Extends__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extends__Group__0"


    // $ANTLR start "rule__Extends__Group__0__Impl"
    // InternalPDDL.g:2036:1: rule__Extends__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__Extends__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2040:1: ( ( RULE_OP ) )
            // InternalPDDL.g:2041:1: ( RULE_OP )
            {
            // InternalPDDL.g:2041:1: ( RULE_OP )
            // InternalPDDL.g:2042:2: RULE_OP
            {
             before(grammarAccess.getExtendsAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getExtendsAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extends__Group__0__Impl"


    // $ANTLR start "rule__Extends__Group__1"
    // InternalPDDL.g:2051:1: rule__Extends__Group__1 : rule__Extends__Group__1__Impl rule__Extends__Group__2 ;
    public final void rule__Extends__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2055:1: ( rule__Extends__Group__1__Impl rule__Extends__Group__2 )
            // InternalPDDL.g:2056:2: rule__Extends__Group__1__Impl rule__Extends__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__Extends__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Extends__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extends__Group__1"


    // $ANTLR start "rule__Extends__Group__1__Impl"
    // InternalPDDL.g:2063:1: rule__Extends__Group__1__Impl : ( ':extends' ) ;
    public final void rule__Extends__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2067:1: ( ( ':extends' ) )
            // InternalPDDL.g:2068:1: ( ':extends' )
            {
            // InternalPDDL.g:2068:1: ( ':extends' )
            // InternalPDDL.g:2069:2: ':extends'
            {
             before(grammarAccess.getExtendsAccess().getExtendsKeyword_1()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getExtendsAccess().getExtendsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extends__Group__1__Impl"


    // $ANTLR start "rule__Extends__Group__2"
    // InternalPDDL.g:2078:1: rule__Extends__Group__2 : rule__Extends__Group__2__Impl rule__Extends__Group__3 ;
    public final void rule__Extends__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2082:1: ( rule__Extends__Group__2__Impl rule__Extends__Group__3 )
            // InternalPDDL.g:2083:2: rule__Extends__Group__2__Impl rule__Extends__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Extends__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Extends__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extends__Group__2"


    // $ANTLR start "rule__Extends__Group__2__Impl"
    // InternalPDDL.g:2090:1: rule__Extends__Group__2__Impl : ( ( rule__Extends__EListAssignment_2 ) ) ;
    public final void rule__Extends__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2094:1: ( ( ( rule__Extends__EListAssignment_2 ) ) )
            // InternalPDDL.g:2095:1: ( ( rule__Extends__EListAssignment_2 ) )
            {
            // InternalPDDL.g:2095:1: ( ( rule__Extends__EListAssignment_2 ) )
            // InternalPDDL.g:2096:2: ( rule__Extends__EListAssignment_2 )
            {
             before(grammarAccess.getExtendsAccess().getEListAssignment_2()); 
            // InternalPDDL.g:2097:2: ( rule__Extends__EListAssignment_2 )
            // InternalPDDL.g:2097:3: rule__Extends__EListAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Extends__EListAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getExtendsAccess().getEListAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extends__Group__2__Impl"


    // $ANTLR start "rule__Extends__Group__3"
    // InternalPDDL.g:2105:1: rule__Extends__Group__3 : rule__Extends__Group__3__Impl ;
    public final void rule__Extends__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2109:1: ( rule__Extends__Group__3__Impl )
            // InternalPDDL.g:2110:2: rule__Extends__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Extends__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extends__Group__3"


    // $ANTLR start "rule__Extends__Group__3__Impl"
    // InternalPDDL.g:2116:1: rule__Extends__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Extends__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2120:1: ( ( RULE_CP ) )
            // InternalPDDL.g:2121:1: ( RULE_CP )
            {
            // InternalPDDL.g:2121:1: ( RULE_CP )
            // InternalPDDL.g:2122:2: RULE_CP
            {
             before(grammarAccess.getExtendsAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getExtendsAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extends__Group__3__Impl"


    // $ANTLR start "rule__Requirements__Group__0"
    // InternalPDDL.g:2132:1: rule__Requirements__Group__0 : rule__Requirements__Group__0__Impl rule__Requirements__Group__1 ;
    public final void rule__Requirements__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2136:1: ( rule__Requirements__Group__0__Impl rule__Requirements__Group__1 )
            // InternalPDDL.g:2137:2: rule__Requirements__Group__0__Impl rule__Requirements__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Requirements__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Requirements__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirements__Group__0"


    // $ANTLR start "rule__Requirements__Group__0__Impl"
    // InternalPDDL.g:2144:1: rule__Requirements__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__Requirements__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2148:1: ( ( RULE_OP ) )
            // InternalPDDL.g:2149:1: ( RULE_OP )
            {
            // InternalPDDL.g:2149:1: ( RULE_OP )
            // InternalPDDL.g:2150:2: RULE_OP
            {
             before(grammarAccess.getRequirementsAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getRequirementsAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirements__Group__0__Impl"


    // $ANTLR start "rule__Requirements__Group__1"
    // InternalPDDL.g:2159:1: rule__Requirements__Group__1 : rule__Requirements__Group__1__Impl rule__Requirements__Group__2 ;
    public final void rule__Requirements__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2163:1: ( rule__Requirements__Group__1__Impl rule__Requirements__Group__2 )
            // InternalPDDL.g:2164:2: rule__Requirements__Group__1__Impl rule__Requirements__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__Requirements__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Requirements__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirements__Group__1"


    // $ANTLR start "rule__Requirements__Group__1__Impl"
    // InternalPDDL.g:2171:1: rule__Requirements__Group__1__Impl : ( ':requirements' ) ;
    public final void rule__Requirements__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2175:1: ( ( ':requirements' ) )
            // InternalPDDL.g:2176:1: ( ':requirements' )
            {
            // InternalPDDL.g:2176:1: ( ':requirements' )
            // InternalPDDL.g:2177:2: ':requirements'
            {
             before(grammarAccess.getRequirementsAccess().getRequirementsKeyword_1()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getRequirementsAccess().getRequirementsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirements__Group__1__Impl"


    // $ANTLR start "rule__Requirements__Group__2"
    // InternalPDDL.g:2186:1: rule__Requirements__Group__2 : rule__Requirements__Group__2__Impl rule__Requirements__Group__3 ;
    public final void rule__Requirements__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2190:1: ( rule__Requirements__Group__2__Impl rule__Requirements__Group__3 )
            // InternalPDDL.g:2191:2: rule__Requirements__Group__2__Impl rule__Requirements__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Requirements__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Requirements__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirements__Group__2"


    // $ANTLR start "rule__Requirements__Group__2__Impl"
    // InternalPDDL.g:2198:1: rule__Requirements__Group__2__Impl : ( ( rule__Requirements__RListAssignment_2 ) ) ;
    public final void rule__Requirements__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2202:1: ( ( ( rule__Requirements__RListAssignment_2 ) ) )
            // InternalPDDL.g:2203:1: ( ( rule__Requirements__RListAssignment_2 ) )
            {
            // InternalPDDL.g:2203:1: ( ( rule__Requirements__RListAssignment_2 ) )
            // InternalPDDL.g:2204:2: ( rule__Requirements__RListAssignment_2 )
            {
             before(grammarAccess.getRequirementsAccess().getRListAssignment_2()); 
            // InternalPDDL.g:2205:2: ( rule__Requirements__RListAssignment_2 )
            // InternalPDDL.g:2205:3: rule__Requirements__RListAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Requirements__RListAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getRequirementsAccess().getRListAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirements__Group__2__Impl"


    // $ANTLR start "rule__Requirements__Group__3"
    // InternalPDDL.g:2213:1: rule__Requirements__Group__3 : rule__Requirements__Group__3__Impl ;
    public final void rule__Requirements__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2217:1: ( rule__Requirements__Group__3__Impl )
            // InternalPDDL.g:2218:2: rule__Requirements__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Requirements__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirements__Group__3"


    // $ANTLR start "rule__Requirements__Group__3__Impl"
    // InternalPDDL.g:2224:1: rule__Requirements__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Requirements__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2228:1: ( ( RULE_CP ) )
            // InternalPDDL.g:2229:1: ( RULE_CP )
            {
            // InternalPDDL.g:2229:1: ( RULE_CP )
            // InternalPDDL.g:2230:2: RULE_CP
            {
             before(grammarAccess.getRequirementsAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getRequirementsAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirements__Group__3__Impl"


    // $ANTLR start "rule__Types__Group__0"
    // InternalPDDL.g:2240:1: rule__Types__Group__0 : rule__Types__Group__0__Impl rule__Types__Group__1 ;
    public final void rule__Types__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2244:1: ( rule__Types__Group__0__Impl rule__Types__Group__1 )
            // InternalPDDL.g:2245:2: rule__Types__Group__0__Impl rule__Types__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Types__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Types__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Types__Group__0"


    // $ANTLR start "rule__Types__Group__0__Impl"
    // InternalPDDL.g:2252:1: rule__Types__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__Types__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2256:1: ( ( RULE_OP ) )
            // InternalPDDL.g:2257:1: ( RULE_OP )
            {
            // InternalPDDL.g:2257:1: ( RULE_OP )
            // InternalPDDL.g:2258:2: RULE_OP
            {
             before(grammarAccess.getTypesAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getTypesAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Types__Group__0__Impl"


    // $ANTLR start "rule__Types__Group__1"
    // InternalPDDL.g:2267:1: rule__Types__Group__1 : rule__Types__Group__1__Impl rule__Types__Group__2 ;
    public final void rule__Types__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2271:1: ( rule__Types__Group__1__Impl rule__Types__Group__2 )
            // InternalPDDL.g:2272:2: rule__Types__Group__1__Impl rule__Types__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__Types__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Types__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Types__Group__1"


    // $ANTLR start "rule__Types__Group__1__Impl"
    // InternalPDDL.g:2279:1: rule__Types__Group__1__Impl : ( ':types' ) ;
    public final void rule__Types__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2283:1: ( ( ':types' ) )
            // InternalPDDL.g:2284:1: ( ':types' )
            {
            // InternalPDDL.g:2284:1: ( ':types' )
            // InternalPDDL.g:2285:2: ':types'
            {
             before(grammarAccess.getTypesAccess().getTypesKeyword_1()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getTypesAccess().getTypesKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Types__Group__1__Impl"


    // $ANTLR start "rule__Types__Group__2"
    // InternalPDDL.g:2294:1: rule__Types__Group__2 : rule__Types__Group__2__Impl rule__Types__Group__3 ;
    public final void rule__Types__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2298:1: ( rule__Types__Group__2__Impl rule__Types__Group__3 )
            // InternalPDDL.g:2299:2: rule__Types__Group__2__Impl rule__Types__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Types__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Types__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Types__Group__2"


    // $ANTLR start "rule__Types__Group__2__Impl"
    // InternalPDDL.g:2306:1: rule__Types__Group__2__Impl : ( ( rule__Types__NewTypesAssignment_2 ) ) ;
    public final void rule__Types__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2310:1: ( ( ( rule__Types__NewTypesAssignment_2 ) ) )
            // InternalPDDL.g:2311:1: ( ( rule__Types__NewTypesAssignment_2 ) )
            {
            // InternalPDDL.g:2311:1: ( ( rule__Types__NewTypesAssignment_2 ) )
            // InternalPDDL.g:2312:2: ( rule__Types__NewTypesAssignment_2 )
            {
             before(grammarAccess.getTypesAccess().getNewTypesAssignment_2()); 
            // InternalPDDL.g:2313:2: ( rule__Types__NewTypesAssignment_2 )
            // InternalPDDL.g:2313:3: rule__Types__NewTypesAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Types__NewTypesAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getTypesAccess().getNewTypesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Types__Group__2__Impl"


    // $ANTLR start "rule__Types__Group__3"
    // InternalPDDL.g:2321:1: rule__Types__Group__3 : rule__Types__Group__3__Impl ;
    public final void rule__Types__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2325:1: ( rule__Types__Group__3__Impl )
            // InternalPDDL.g:2326:2: rule__Types__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Types__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Types__Group__3"


    // $ANTLR start "rule__Types__Group__3__Impl"
    // InternalPDDL.g:2332:1: rule__Types__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Types__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2336:1: ( ( RULE_CP ) )
            // InternalPDDL.g:2337:1: ( RULE_CP )
            {
            // InternalPDDL.g:2337:1: ( RULE_CP )
            // InternalPDDL.g:2338:2: RULE_CP
            {
             before(grammarAccess.getTypesAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getTypesAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Types__Group__3__Impl"


    // $ANTLR start "rule__NewTypes__Group__0"
    // InternalPDDL.g:2348:1: rule__NewTypes__Group__0 : rule__NewTypes__Group__0__Impl rule__NewTypes__Group__1 ;
    public final void rule__NewTypes__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2352:1: ( rule__NewTypes__Group__0__Impl rule__NewTypes__Group__1 )
            // InternalPDDL.g:2353:2: rule__NewTypes__Group__0__Impl rule__NewTypes__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__NewTypes__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NewTypes__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewTypes__Group__0"


    // $ANTLR start "rule__NewTypes__Group__0__Impl"
    // InternalPDDL.g:2360:1: rule__NewTypes__Group__0__Impl : ( () ) ;
    public final void rule__NewTypes__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2364:1: ( ( () ) )
            // InternalPDDL.g:2365:1: ( () )
            {
            // InternalPDDL.g:2365:1: ( () )
            // InternalPDDL.g:2366:2: ()
            {
             before(grammarAccess.getNewTypesAccess().getNewTypesAction_0()); 
            // InternalPDDL.g:2367:2: ()
            // InternalPDDL.g:2367:3: 
            {
            }

             after(grammarAccess.getNewTypesAccess().getNewTypesAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewTypes__Group__0__Impl"


    // $ANTLR start "rule__NewTypes__Group__1"
    // InternalPDDL.g:2375:1: rule__NewTypes__Group__1 : rule__NewTypes__Group__1__Impl rule__NewTypes__Group__2 ;
    public final void rule__NewTypes__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2379:1: ( rule__NewTypes__Group__1__Impl rule__NewTypes__Group__2 )
            // InternalPDDL.g:2380:2: rule__NewTypes__Group__1__Impl rule__NewTypes__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__NewTypes__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NewTypes__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewTypes__Group__1"


    // $ANTLR start "rule__NewTypes__Group__1__Impl"
    // InternalPDDL.g:2387:1: rule__NewTypes__Group__1__Impl : ( ( rule__NewTypes__DeclInheritorTypesAssignment_1 )* ) ;
    public final void rule__NewTypes__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2391:1: ( ( ( rule__NewTypes__DeclInheritorTypesAssignment_1 )* ) )
            // InternalPDDL.g:2392:1: ( ( rule__NewTypes__DeclInheritorTypesAssignment_1 )* )
            {
            // InternalPDDL.g:2392:1: ( ( rule__NewTypes__DeclInheritorTypesAssignment_1 )* )
            // InternalPDDL.g:2393:2: ( rule__NewTypes__DeclInheritorTypesAssignment_1 )*
            {
             before(grammarAccess.getNewTypesAccess().getDeclInheritorTypesAssignment_1()); 
            // InternalPDDL.g:2394:2: ( rule__NewTypes__DeclInheritorTypesAssignment_1 )*
            loop20:
            do {
                int alt20=2;
                alt20 = dfa20.predict(input);
                switch (alt20) {
            	case 1 :
            	    // InternalPDDL.g:2394:3: rule__NewTypes__DeclInheritorTypesAssignment_1
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__NewTypes__DeclInheritorTypesAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getNewTypesAccess().getDeclInheritorTypesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewTypes__Group__1__Impl"


    // $ANTLR start "rule__NewTypes__Group__2"
    // InternalPDDL.g:2402:1: rule__NewTypes__Group__2 : rule__NewTypes__Group__2__Impl ;
    public final void rule__NewTypes__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2406:1: ( rule__NewTypes__Group__2__Impl )
            // InternalPDDL.g:2407:2: rule__NewTypes__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NewTypes__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewTypes__Group__2"


    // $ANTLR start "rule__NewTypes__Group__2__Impl"
    // InternalPDDL.g:2413:1: rule__NewTypes__Group__2__Impl : ( ( rule__NewTypes__DeclOtherTypesAssignment_2 )? ) ;
    public final void rule__NewTypes__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2417:1: ( ( ( rule__NewTypes__DeclOtherTypesAssignment_2 )? ) )
            // InternalPDDL.g:2418:1: ( ( rule__NewTypes__DeclOtherTypesAssignment_2 )? )
            {
            // InternalPDDL.g:2418:1: ( ( rule__NewTypes__DeclOtherTypesAssignment_2 )? )
            // InternalPDDL.g:2419:2: ( rule__NewTypes__DeclOtherTypesAssignment_2 )?
            {
             before(grammarAccess.getNewTypesAccess().getDeclOtherTypesAssignment_2()); 
            // InternalPDDL.g:2420:2: ( rule__NewTypes__DeclOtherTypesAssignment_2 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==RULE_NAME) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalPDDL.g:2420:3: rule__NewTypes__DeclOtherTypesAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__NewTypes__DeclOtherTypesAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNewTypesAccess().getDeclOtherTypesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewTypes__Group__2__Impl"


    // $ANTLR start "rule__InheritorTypes__Group__0"
    // InternalPDDL.g:2429:1: rule__InheritorTypes__Group__0 : rule__InheritorTypes__Group__0__Impl rule__InheritorTypes__Group__1 ;
    public final void rule__InheritorTypes__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2433:1: ( rule__InheritorTypes__Group__0__Impl rule__InheritorTypes__Group__1 )
            // InternalPDDL.g:2434:2: rule__InheritorTypes__Group__0__Impl rule__InheritorTypes__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__InheritorTypes__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InheritorTypes__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InheritorTypes__Group__0"


    // $ANTLR start "rule__InheritorTypes__Group__0__Impl"
    // InternalPDDL.g:2441:1: rule__InheritorTypes__Group__0__Impl : ( ( rule__InheritorTypes__SubTypesAssignment_0 ) ) ;
    public final void rule__InheritorTypes__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2445:1: ( ( ( rule__InheritorTypes__SubTypesAssignment_0 ) ) )
            // InternalPDDL.g:2446:1: ( ( rule__InheritorTypes__SubTypesAssignment_0 ) )
            {
            // InternalPDDL.g:2446:1: ( ( rule__InheritorTypes__SubTypesAssignment_0 ) )
            // InternalPDDL.g:2447:2: ( rule__InheritorTypes__SubTypesAssignment_0 )
            {
             before(grammarAccess.getInheritorTypesAccess().getSubTypesAssignment_0()); 
            // InternalPDDL.g:2448:2: ( rule__InheritorTypes__SubTypesAssignment_0 )
            // InternalPDDL.g:2448:3: rule__InheritorTypes__SubTypesAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__InheritorTypes__SubTypesAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getInheritorTypesAccess().getSubTypesAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InheritorTypes__Group__0__Impl"


    // $ANTLR start "rule__InheritorTypes__Group__1"
    // InternalPDDL.g:2456:1: rule__InheritorTypes__Group__1 : rule__InheritorTypes__Group__1__Impl rule__InheritorTypes__Group__2 ;
    public final void rule__InheritorTypes__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2460:1: ( rule__InheritorTypes__Group__1__Impl rule__InheritorTypes__Group__2 )
            // InternalPDDL.g:2461:2: rule__InheritorTypes__Group__1__Impl rule__InheritorTypes__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__InheritorTypes__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InheritorTypes__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InheritorTypes__Group__1"


    // $ANTLR start "rule__InheritorTypes__Group__1__Impl"
    // InternalPDDL.g:2468:1: rule__InheritorTypes__Group__1__Impl : ( '-' ) ;
    public final void rule__InheritorTypes__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2472:1: ( ( '-' ) )
            // InternalPDDL.g:2473:1: ( '-' )
            {
            // InternalPDDL.g:2473:1: ( '-' )
            // InternalPDDL.g:2474:2: '-'
            {
             before(grammarAccess.getInheritorTypesAccess().getHyphenMinusKeyword_1()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getInheritorTypesAccess().getHyphenMinusKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InheritorTypes__Group__1__Impl"


    // $ANTLR start "rule__InheritorTypes__Group__2"
    // InternalPDDL.g:2483:1: rule__InheritorTypes__Group__2 : rule__InheritorTypes__Group__2__Impl ;
    public final void rule__InheritorTypes__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2487:1: ( rule__InheritorTypes__Group__2__Impl )
            // InternalPDDL.g:2488:2: rule__InheritorTypes__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InheritorTypes__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InheritorTypes__Group__2"


    // $ANTLR start "rule__InheritorTypes__Group__2__Impl"
    // InternalPDDL.g:2494:1: rule__InheritorTypes__Group__2__Impl : ( ( rule__InheritorTypes__SupTypeAssignment_2 ) ) ;
    public final void rule__InheritorTypes__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2498:1: ( ( ( rule__InheritorTypes__SupTypeAssignment_2 ) ) )
            // InternalPDDL.g:2499:1: ( ( rule__InheritorTypes__SupTypeAssignment_2 ) )
            {
            // InternalPDDL.g:2499:1: ( ( rule__InheritorTypes__SupTypeAssignment_2 ) )
            // InternalPDDL.g:2500:2: ( rule__InheritorTypes__SupTypeAssignment_2 )
            {
             before(grammarAccess.getInheritorTypesAccess().getSupTypeAssignment_2()); 
            // InternalPDDL.g:2501:2: ( rule__InheritorTypes__SupTypeAssignment_2 )
            // InternalPDDL.g:2501:3: rule__InheritorTypes__SupTypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__InheritorTypes__SupTypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getInheritorTypesAccess().getSupTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InheritorTypes__Group__2__Impl"


    // $ANTLR start "rule__Predicates__Group__0"
    // InternalPDDL.g:2510:1: rule__Predicates__Group__0 : rule__Predicates__Group__0__Impl rule__Predicates__Group__1 ;
    public final void rule__Predicates__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2514:1: ( rule__Predicates__Group__0__Impl rule__Predicates__Group__1 )
            // InternalPDDL.g:2515:2: rule__Predicates__Group__0__Impl rule__Predicates__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__Predicates__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Predicates__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predicates__Group__0"


    // $ANTLR start "rule__Predicates__Group__0__Impl"
    // InternalPDDL.g:2522:1: rule__Predicates__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__Predicates__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2526:1: ( ( RULE_OP ) )
            // InternalPDDL.g:2527:1: ( RULE_OP )
            {
            // InternalPDDL.g:2527:1: ( RULE_OP )
            // InternalPDDL.g:2528:2: RULE_OP
            {
             before(grammarAccess.getPredicatesAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getPredicatesAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predicates__Group__0__Impl"


    // $ANTLR start "rule__Predicates__Group__1"
    // InternalPDDL.g:2537:1: rule__Predicates__Group__1 : rule__Predicates__Group__1__Impl rule__Predicates__Group__2 ;
    public final void rule__Predicates__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2541:1: ( rule__Predicates__Group__1__Impl rule__Predicates__Group__2 )
            // InternalPDDL.g:2542:2: rule__Predicates__Group__1__Impl rule__Predicates__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Predicates__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Predicates__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predicates__Group__1"


    // $ANTLR start "rule__Predicates__Group__1__Impl"
    // InternalPDDL.g:2549:1: rule__Predicates__Group__1__Impl : ( ':predicates' ) ;
    public final void rule__Predicates__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2553:1: ( ( ':predicates' ) )
            // InternalPDDL.g:2554:1: ( ':predicates' )
            {
            // InternalPDDL.g:2554:1: ( ':predicates' )
            // InternalPDDL.g:2555:2: ':predicates'
            {
             before(grammarAccess.getPredicatesAccess().getPredicatesKeyword_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getPredicatesAccess().getPredicatesKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predicates__Group__1__Impl"


    // $ANTLR start "rule__Predicates__Group__2"
    // InternalPDDL.g:2564:1: rule__Predicates__Group__2 : rule__Predicates__Group__2__Impl rule__Predicates__Group__3 ;
    public final void rule__Predicates__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2568:1: ( rule__Predicates__Group__2__Impl rule__Predicates__Group__3 )
            // InternalPDDL.g:2569:2: rule__Predicates__Group__2__Impl rule__Predicates__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Predicates__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Predicates__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predicates__Group__2"


    // $ANTLR start "rule__Predicates__Group__2__Impl"
    // InternalPDDL.g:2576:1: rule__Predicates__Group__2__Impl : ( ( rule__Predicates__PredicatListAssignment_2 ) ) ;
    public final void rule__Predicates__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2580:1: ( ( ( rule__Predicates__PredicatListAssignment_2 ) ) )
            // InternalPDDL.g:2581:1: ( ( rule__Predicates__PredicatListAssignment_2 ) )
            {
            // InternalPDDL.g:2581:1: ( ( rule__Predicates__PredicatListAssignment_2 ) )
            // InternalPDDL.g:2582:2: ( rule__Predicates__PredicatListAssignment_2 )
            {
             before(grammarAccess.getPredicatesAccess().getPredicatListAssignment_2()); 
            // InternalPDDL.g:2583:2: ( rule__Predicates__PredicatListAssignment_2 )
            // InternalPDDL.g:2583:3: rule__Predicates__PredicatListAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Predicates__PredicatListAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getPredicatesAccess().getPredicatListAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predicates__Group__2__Impl"


    // $ANTLR start "rule__Predicates__Group__3"
    // InternalPDDL.g:2591:1: rule__Predicates__Group__3 : rule__Predicates__Group__3__Impl ;
    public final void rule__Predicates__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2595:1: ( rule__Predicates__Group__3__Impl )
            // InternalPDDL.g:2596:2: rule__Predicates__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Predicates__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predicates__Group__3"


    // $ANTLR start "rule__Predicates__Group__3__Impl"
    // InternalPDDL.g:2602:1: rule__Predicates__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Predicates__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2606:1: ( ( RULE_CP ) )
            // InternalPDDL.g:2607:1: ( RULE_CP )
            {
            // InternalPDDL.g:2607:1: ( RULE_CP )
            // InternalPDDL.g:2608:2: RULE_CP
            {
             before(grammarAccess.getPredicatesAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getPredicatesAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predicates__Group__3__Impl"


    // $ANTLR start "rule__DeclPredicatOrFunction__Group__0"
    // InternalPDDL.g:2618:1: rule__DeclPredicatOrFunction__Group__0 : rule__DeclPredicatOrFunction__Group__0__Impl rule__DeclPredicatOrFunction__Group__1 ;
    public final void rule__DeclPredicatOrFunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2622:1: ( rule__DeclPredicatOrFunction__Group__0__Impl rule__DeclPredicatOrFunction__Group__1 )
            // InternalPDDL.g:2623:2: rule__DeclPredicatOrFunction__Group__0__Impl rule__DeclPredicatOrFunction__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__DeclPredicatOrFunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeclPredicatOrFunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__Group__0"


    // $ANTLR start "rule__DeclPredicatOrFunction__Group__0__Impl"
    // InternalPDDL.g:2630:1: rule__DeclPredicatOrFunction__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__DeclPredicatOrFunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2634:1: ( ( RULE_OP ) )
            // InternalPDDL.g:2635:1: ( RULE_OP )
            {
            // InternalPDDL.g:2635:1: ( RULE_OP )
            // InternalPDDL.g:2636:2: RULE_OP
            {
             before(grammarAccess.getDeclPredicatOrFunctionAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getDeclPredicatOrFunctionAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__Group__0__Impl"


    // $ANTLR start "rule__DeclPredicatOrFunction__Group__1"
    // InternalPDDL.g:2645:1: rule__DeclPredicatOrFunction__Group__1 : rule__DeclPredicatOrFunction__Group__1__Impl rule__DeclPredicatOrFunction__Group__2 ;
    public final void rule__DeclPredicatOrFunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2649:1: ( rule__DeclPredicatOrFunction__Group__1__Impl rule__DeclPredicatOrFunction__Group__2 )
            // InternalPDDL.g:2650:2: rule__DeclPredicatOrFunction__Group__1__Impl rule__DeclPredicatOrFunction__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__DeclPredicatOrFunction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeclPredicatOrFunction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__Group__1"


    // $ANTLR start "rule__DeclPredicatOrFunction__Group__1__Impl"
    // InternalPDDL.g:2657:1: rule__DeclPredicatOrFunction__Group__1__Impl : ( ( rule__DeclPredicatOrFunction__PfnameAssignment_1 ) ) ;
    public final void rule__DeclPredicatOrFunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2661:1: ( ( ( rule__DeclPredicatOrFunction__PfnameAssignment_1 ) ) )
            // InternalPDDL.g:2662:1: ( ( rule__DeclPredicatOrFunction__PfnameAssignment_1 ) )
            {
            // InternalPDDL.g:2662:1: ( ( rule__DeclPredicatOrFunction__PfnameAssignment_1 ) )
            // InternalPDDL.g:2663:2: ( rule__DeclPredicatOrFunction__PfnameAssignment_1 )
            {
             before(grammarAccess.getDeclPredicatOrFunctionAccess().getPfnameAssignment_1()); 
            // InternalPDDL.g:2664:2: ( rule__DeclPredicatOrFunction__PfnameAssignment_1 )
            // InternalPDDL.g:2664:3: rule__DeclPredicatOrFunction__PfnameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DeclPredicatOrFunction__PfnameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDeclPredicatOrFunctionAccess().getPfnameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__Group__1__Impl"


    // $ANTLR start "rule__DeclPredicatOrFunction__Group__2"
    // InternalPDDL.g:2672:1: rule__DeclPredicatOrFunction__Group__2 : rule__DeclPredicatOrFunction__Group__2__Impl rule__DeclPredicatOrFunction__Group__3 ;
    public final void rule__DeclPredicatOrFunction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2676:1: ( rule__DeclPredicatOrFunction__Group__2__Impl rule__DeclPredicatOrFunction__Group__3 )
            // InternalPDDL.g:2677:2: rule__DeclPredicatOrFunction__Group__2__Impl rule__DeclPredicatOrFunction__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__DeclPredicatOrFunction__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeclPredicatOrFunction__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__Group__2"


    // $ANTLR start "rule__DeclPredicatOrFunction__Group__2__Impl"
    // InternalPDDL.g:2684:1: rule__DeclPredicatOrFunction__Group__2__Impl : ( ( rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2 ) ) ;
    public final void rule__DeclPredicatOrFunction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2688:1: ( ( ( rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2 ) ) )
            // InternalPDDL.g:2689:1: ( ( rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2 ) )
            {
            // InternalPDDL.g:2689:1: ( ( rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2 ) )
            // InternalPDDL.g:2690:2: ( rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2 )
            {
             before(grammarAccess.getDeclPredicatOrFunctionAccess().getTypedVariablesListAssignment_2()); 
            // InternalPDDL.g:2691:2: ( rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2 )
            // InternalPDDL.g:2691:3: rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDeclPredicatOrFunctionAccess().getTypedVariablesListAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__Group__2__Impl"


    // $ANTLR start "rule__DeclPredicatOrFunction__Group__3"
    // InternalPDDL.g:2699:1: rule__DeclPredicatOrFunction__Group__3 : rule__DeclPredicatOrFunction__Group__3__Impl ;
    public final void rule__DeclPredicatOrFunction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2703:1: ( rule__DeclPredicatOrFunction__Group__3__Impl )
            // InternalPDDL.g:2704:2: rule__DeclPredicatOrFunction__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DeclPredicatOrFunction__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__Group__3"


    // $ANTLR start "rule__DeclPredicatOrFunction__Group__3__Impl"
    // InternalPDDL.g:2710:1: rule__DeclPredicatOrFunction__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__DeclPredicatOrFunction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2714:1: ( ( RULE_CP ) )
            // InternalPDDL.g:2715:1: ( RULE_CP )
            {
            // InternalPDDL.g:2715:1: ( RULE_CP )
            // InternalPDDL.g:2716:2: RULE_CP
            {
             before(grammarAccess.getDeclPredicatOrFunctionAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getDeclPredicatOrFunctionAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__Group__3__Impl"


    // $ANTLR start "rule__TypedVariable__Group__0"
    // InternalPDDL.g:2726:1: rule__TypedVariable__Group__0 : rule__TypedVariable__Group__0__Impl rule__TypedVariable__Group__1 ;
    public final void rule__TypedVariable__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2730:1: ( rule__TypedVariable__Group__0__Impl rule__TypedVariable__Group__1 )
            // InternalPDDL.g:2731:2: rule__TypedVariable__Group__0__Impl rule__TypedVariable__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__TypedVariable__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypedVariable__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__Group__0"


    // $ANTLR start "rule__TypedVariable__Group__0__Impl"
    // InternalPDDL.g:2738:1: rule__TypedVariable__Group__0__Impl : ( ( ( rule__TypedVariable__VariablesAssignment_0 ) ) ( ( rule__TypedVariable__VariablesAssignment_0 )* ) ) ;
    public final void rule__TypedVariable__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2742:1: ( ( ( ( rule__TypedVariable__VariablesAssignment_0 ) ) ( ( rule__TypedVariable__VariablesAssignment_0 )* ) ) )
            // InternalPDDL.g:2743:1: ( ( ( rule__TypedVariable__VariablesAssignment_0 ) ) ( ( rule__TypedVariable__VariablesAssignment_0 )* ) )
            {
            // InternalPDDL.g:2743:1: ( ( ( rule__TypedVariable__VariablesAssignment_0 ) ) ( ( rule__TypedVariable__VariablesAssignment_0 )* ) )
            // InternalPDDL.g:2744:2: ( ( rule__TypedVariable__VariablesAssignment_0 ) ) ( ( rule__TypedVariable__VariablesAssignment_0 )* )
            {
            // InternalPDDL.g:2744:2: ( ( rule__TypedVariable__VariablesAssignment_0 ) )
            // InternalPDDL.g:2745:3: ( rule__TypedVariable__VariablesAssignment_0 )
            {
             before(grammarAccess.getTypedVariableAccess().getVariablesAssignment_0()); 
            // InternalPDDL.g:2746:3: ( rule__TypedVariable__VariablesAssignment_0 )
            // InternalPDDL.g:2746:4: rule__TypedVariable__VariablesAssignment_0
            {
            pushFollow(FOLLOW_6);
            rule__TypedVariable__VariablesAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTypedVariableAccess().getVariablesAssignment_0()); 

            }

            // InternalPDDL.g:2749:2: ( ( rule__TypedVariable__VariablesAssignment_0 )* )
            // InternalPDDL.g:2750:3: ( rule__TypedVariable__VariablesAssignment_0 )*
            {
             before(grammarAccess.getTypedVariableAccess().getVariablesAssignment_0()); 
            // InternalPDDL.g:2751:3: ( rule__TypedVariable__VariablesAssignment_0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalPDDL.g:2751:4: rule__TypedVariable__VariablesAssignment_0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__TypedVariable__VariablesAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getTypedVariableAccess().getVariablesAssignment_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__Group__0__Impl"


    // $ANTLR start "rule__TypedVariable__Group__1"
    // InternalPDDL.g:2760:1: rule__TypedVariable__Group__1 : rule__TypedVariable__Group__1__Impl rule__TypedVariable__Group__2 ;
    public final void rule__TypedVariable__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2764:1: ( rule__TypedVariable__Group__1__Impl rule__TypedVariable__Group__2 )
            // InternalPDDL.g:2765:2: rule__TypedVariable__Group__1__Impl rule__TypedVariable__Group__2
            {
            pushFollow(FOLLOW_20);
            rule__TypedVariable__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypedVariable__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__Group__1"


    // $ANTLR start "rule__TypedVariable__Group__1__Impl"
    // InternalPDDL.g:2772:1: rule__TypedVariable__Group__1__Impl : ( '-' ) ;
    public final void rule__TypedVariable__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2776:1: ( ( '-' ) )
            // InternalPDDL.g:2777:1: ( '-' )
            {
            // InternalPDDL.g:2777:1: ( '-' )
            // InternalPDDL.g:2778:2: '-'
            {
             before(grammarAccess.getTypedVariableAccess().getHyphenMinusKeyword_1()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getTypedVariableAccess().getHyphenMinusKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__Group__1__Impl"


    // $ANTLR start "rule__TypedVariable__Group__2"
    // InternalPDDL.g:2787:1: rule__TypedVariable__Group__2 : rule__TypedVariable__Group__2__Impl ;
    public final void rule__TypedVariable__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2791:1: ( rule__TypedVariable__Group__2__Impl )
            // InternalPDDL.g:2792:2: rule__TypedVariable__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypedVariable__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__Group__2"


    // $ANTLR start "rule__TypedVariable__Group__2__Impl"
    // InternalPDDL.g:2798:1: rule__TypedVariable__Group__2__Impl : ( ( rule__TypedVariable__Alternatives_2 ) ) ;
    public final void rule__TypedVariable__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2802:1: ( ( ( rule__TypedVariable__Alternatives_2 ) ) )
            // InternalPDDL.g:2803:1: ( ( rule__TypedVariable__Alternatives_2 ) )
            {
            // InternalPDDL.g:2803:1: ( ( rule__TypedVariable__Alternatives_2 ) )
            // InternalPDDL.g:2804:2: ( rule__TypedVariable__Alternatives_2 )
            {
             before(grammarAccess.getTypedVariableAccess().getAlternatives_2()); 
            // InternalPDDL.g:2805:2: ( rule__TypedVariable__Alternatives_2 )
            // InternalPDDL.g:2805:3: rule__TypedVariable__Alternatives_2
            {
            pushFollow(FOLLOW_2);
            rule__TypedVariable__Alternatives_2();

            state._fsp--;


            }

             after(grammarAccess.getTypedVariableAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__Group__2__Impl"


    // $ANTLR start "rule__Either__Group__0"
    // InternalPDDL.g:2814:1: rule__Either__Group__0 : rule__Either__Group__0__Impl rule__Either__Group__1 ;
    public final void rule__Either__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2818:1: ( rule__Either__Group__0__Impl rule__Either__Group__1 )
            // InternalPDDL.g:2819:2: rule__Either__Group__0__Impl rule__Either__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__Either__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Either__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__0"


    // $ANTLR start "rule__Either__Group__0__Impl"
    // InternalPDDL.g:2826:1: rule__Either__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__Either__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2830:1: ( ( RULE_OP ) )
            // InternalPDDL.g:2831:1: ( RULE_OP )
            {
            // InternalPDDL.g:2831:1: ( RULE_OP )
            // InternalPDDL.g:2832:2: RULE_OP
            {
             before(grammarAccess.getEitherAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getEitherAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__0__Impl"


    // $ANTLR start "rule__Either__Group__1"
    // InternalPDDL.g:2841:1: rule__Either__Group__1 : rule__Either__Group__1__Impl rule__Either__Group__2 ;
    public final void rule__Either__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2845:1: ( rule__Either__Group__1__Impl rule__Either__Group__2 )
            // InternalPDDL.g:2846:2: rule__Either__Group__1__Impl rule__Either__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__Either__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Either__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__1"


    // $ANTLR start "rule__Either__Group__1__Impl"
    // InternalPDDL.g:2853:1: rule__Either__Group__1__Impl : ( 'either' ) ;
    public final void rule__Either__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2857:1: ( ( 'either' ) )
            // InternalPDDL.g:2858:1: ( 'either' )
            {
            // InternalPDDL.g:2858:1: ( 'either' )
            // InternalPDDL.g:2859:2: 'either'
            {
             before(grammarAccess.getEitherAccess().getEitherKeyword_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getEitherAccess().getEitherKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__1__Impl"


    // $ANTLR start "rule__Either__Group__2"
    // InternalPDDL.g:2868:1: rule__Either__Group__2 : rule__Either__Group__2__Impl rule__Either__Group__3 ;
    public final void rule__Either__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2872:1: ( rule__Either__Group__2__Impl rule__Either__Group__3 )
            // InternalPDDL.g:2873:2: rule__Either__Group__2__Impl rule__Either__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__Either__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Either__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__2"


    // $ANTLR start "rule__Either__Group__2__Impl"
    // InternalPDDL.g:2880:1: rule__Either__Group__2__Impl : ( ( rule__Either__EitherType1Assignment_2 ) ) ;
    public final void rule__Either__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2884:1: ( ( ( rule__Either__EitherType1Assignment_2 ) ) )
            // InternalPDDL.g:2885:1: ( ( rule__Either__EitherType1Assignment_2 ) )
            {
            // InternalPDDL.g:2885:1: ( ( rule__Either__EitherType1Assignment_2 ) )
            // InternalPDDL.g:2886:2: ( rule__Either__EitherType1Assignment_2 )
            {
             before(grammarAccess.getEitherAccess().getEitherType1Assignment_2()); 
            // InternalPDDL.g:2887:2: ( rule__Either__EitherType1Assignment_2 )
            // InternalPDDL.g:2887:3: rule__Either__EitherType1Assignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Either__EitherType1Assignment_2();

            state._fsp--;


            }

             after(grammarAccess.getEitherAccess().getEitherType1Assignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__2__Impl"


    // $ANTLR start "rule__Either__Group__3"
    // InternalPDDL.g:2895:1: rule__Either__Group__3 : rule__Either__Group__3__Impl rule__Either__Group__4 ;
    public final void rule__Either__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2899:1: ( rule__Either__Group__3__Impl rule__Either__Group__4 )
            // InternalPDDL.g:2900:2: rule__Either__Group__3__Impl rule__Either__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Either__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Either__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__3"


    // $ANTLR start "rule__Either__Group__3__Impl"
    // InternalPDDL.g:2907:1: rule__Either__Group__3__Impl : ( ( ( rule__Either__EitherType2Assignment_3 ) ) ( ( rule__Either__EitherType2Assignment_3 )* ) ) ;
    public final void rule__Either__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2911:1: ( ( ( ( rule__Either__EitherType2Assignment_3 ) ) ( ( rule__Either__EitherType2Assignment_3 )* ) ) )
            // InternalPDDL.g:2912:1: ( ( ( rule__Either__EitherType2Assignment_3 ) ) ( ( rule__Either__EitherType2Assignment_3 )* ) )
            {
            // InternalPDDL.g:2912:1: ( ( ( rule__Either__EitherType2Assignment_3 ) ) ( ( rule__Either__EitherType2Assignment_3 )* ) )
            // InternalPDDL.g:2913:2: ( ( rule__Either__EitherType2Assignment_3 ) ) ( ( rule__Either__EitherType2Assignment_3 )* )
            {
            // InternalPDDL.g:2913:2: ( ( rule__Either__EitherType2Assignment_3 ) )
            // InternalPDDL.g:2914:3: ( rule__Either__EitherType2Assignment_3 )
            {
             before(grammarAccess.getEitherAccess().getEitherType2Assignment_3()); 
            // InternalPDDL.g:2915:3: ( rule__Either__EitherType2Assignment_3 )
            // InternalPDDL.g:2915:4: rule__Either__EitherType2Assignment_3
            {
            pushFollow(FOLLOW_3);
            rule__Either__EitherType2Assignment_3();

            state._fsp--;


            }

             after(grammarAccess.getEitherAccess().getEitherType2Assignment_3()); 

            }

            // InternalPDDL.g:2918:2: ( ( rule__Either__EitherType2Assignment_3 )* )
            // InternalPDDL.g:2919:3: ( rule__Either__EitherType2Assignment_3 )*
            {
             before(grammarAccess.getEitherAccess().getEitherType2Assignment_3()); 
            // InternalPDDL.g:2920:3: ( rule__Either__EitherType2Assignment_3 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==RULE_NAME) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalPDDL.g:2920:4: rule__Either__EitherType2Assignment_3
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Either__EitherType2Assignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getEitherAccess().getEitherType2Assignment_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__3__Impl"


    // $ANTLR start "rule__Either__Group__4"
    // InternalPDDL.g:2929:1: rule__Either__Group__4 : rule__Either__Group__4__Impl ;
    public final void rule__Either__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2933:1: ( rule__Either__Group__4__Impl )
            // InternalPDDL.g:2934:2: rule__Either__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Either__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__4"


    // $ANTLR start "rule__Either__Group__4__Impl"
    // InternalPDDL.g:2940:1: rule__Either__Group__4__Impl : ( RULE_CP ) ;
    public final void rule__Either__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2944:1: ( ( RULE_CP ) )
            // InternalPDDL.g:2945:1: ( RULE_CP )
            {
            // InternalPDDL.g:2945:1: ( RULE_CP )
            // InternalPDDL.g:2946:2: RULE_CP
            {
             before(grammarAccess.getEitherAccess().getCPTerminalRuleCall_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getEitherAccess().getCPTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__Group__4__Impl"


    // $ANTLR start "rule__Functions__Group__0"
    // InternalPDDL.g:2956:1: rule__Functions__Group__0 : rule__Functions__Group__0__Impl rule__Functions__Group__1 ;
    public final void rule__Functions__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2960:1: ( rule__Functions__Group__0__Impl rule__Functions__Group__1 )
            // InternalPDDL.g:2961:2: rule__Functions__Group__0__Impl rule__Functions__Group__1
            {
            pushFollow(FOLLOW_22);
            rule__Functions__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Functions__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Functions__Group__0"


    // $ANTLR start "rule__Functions__Group__0__Impl"
    // InternalPDDL.g:2968:1: rule__Functions__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__Functions__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2972:1: ( ( RULE_OP ) )
            // InternalPDDL.g:2973:1: ( RULE_OP )
            {
            // InternalPDDL.g:2973:1: ( RULE_OP )
            // InternalPDDL.g:2974:2: RULE_OP
            {
             before(grammarAccess.getFunctionsAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getFunctionsAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Functions__Group__0__Impl"


    // $ANTLR start "rule__Functions__Group__1"
    // InternalPDDL.g:2983:1: rule__Functions__Group__1 : rule__Functions__Group__1__Impl rule__Functions__Group__2 ;
    public final void rule__Functions__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2987:1: ( rule__Functions__Group__1__Impl rule__Functions__Group__2 )
            // InternalPDDL.g:2988:2: rule__Functions__Group__1__Impl rule__Functions__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Functions__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Functions__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Functions__Group__1"


    // $ANTLR start "rule__Functions__Group__1__Impl"
    // InternalPDDL.g:2995:1: rule__Functions__Group__1__Impl : ( ':functions' ) ;
    public final void rule__Functions__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:2999:1: ( ( ':functions' ) )
            // InternalPDDL.g:3000:1: ( ':functions' )
            {
            // InternalPDDL.g:3000:1: ( ':functions' )
            // InternalPDDL.g:3001:2: ':functions'
            {
             before(grammarAccess.getFunctionsAccess().getFunctionsKeyword_1()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getFunctionsAccess().getFunctionsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Functions__Group__1__Impl"


    // $ANTLR start "rule__Functions__Group__2"
    // InternalPDDL.g:3010:1: rule__Functions__Group__2 : rule__Functions__Group__2__Impl rule__Functions__Group__3 ;
    public final void rule__Functions__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3014:1: ( rule__Functions__Group__2__Impl rule__Functions__Group__3 )
            // InternalPDDL.g:3015:2: rule__Functions__Group__2__Impl rule__Functions__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Functions__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Functions__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Functions__Group__2"


    // $ANTLR start "rule__Functions__Group__2__Impl"
    // InternalPDDL.g:3022:1: rule__Functions__Group__2__Impl : ( ( rule__Functions__FunctionListAssignment_2 ) ) ;
    public final void rule__Functions__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3026:1: ( ( ( rule__Functions__FunctionListAssignment_2 ) ) )
            // InternalPDDL.g:3027:1: ( ( rule__Functions__FunctionListAssignment_2 ) )
            {
            // InternalPDDL.g:3027:1: ( ( rule__Functions__FunctionListAssignment_2 ) )
            // InternalPDDL.g:3028:2: ( rule__Functions__FunctionListAssignment_2 )
            {
             before(grammarAccess.getFunctionsAccess().getFunctionListAssignment_2()); 
            // InternalPDDL.g:3029:2: ( rule__Functions__FunctionListAssignment_2 )
            // InternalPDDL.g:3029:3: rule__Functions__FunctionListAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Functions__FunctionListAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFunctionsAccess().getFunctionListAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Functions__Group__2__Impl"


    // $ANTLR start "rule__Functions__Group__3"
    // InternalPDDL.g:3037:1: rule__Functions__Group__3 : rule__Functions__Group__3__Impl ;
    public final void rule__Functions__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3041:1: ( rule__Functions__Group__3__Impl )
            // InternalPDDL.g:3042:2: rule__Functions__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Functions__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Functions__Group__3"


    // $ANTLR start "rule__Functions__Group__3__Impl"
    // InternalPDDL.g:3048:1: rule__Functions__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Functions__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3052:1: ( ( RULE_CP ) )
            // InternalPDDL.g:3053:1: ( RULE_CP )
            {
            // InternalPDDL.g:3053:1: ( RULE_CP )
            // InternalPDDL.g:3054:2: RULE_CP
            {
             before(grammarAccess.getFunctionsAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getFunctionsAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Functions__Group__3__Impl"


    // $ANTLR start "rule__PDDLAction__Group__0"
    // InternalPDDL.g:3064:1: rule__PDDLAction__Group__0 : rule__PDDLAction__Group__0__Impl rule__PDDLAction__Group__1 ;
    public final void rule__PDDLAction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3068:1: ( rule__PDDLAction__Group__0__Impl rule__PDDLAction__Group__1 )
            // InternalPDDL.g:3069:2: rule__PDDLAction__Group__0__Impl rule__PDDLAction__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__PDDLAction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLAction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__0"


    // $ANTLR start "rule__PDDLAction__Group__0__Impl"
    // InternalPDDL.g:3076:1: rule__PDDLAction__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__PDDLAction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3080:1: ( ( RULE_OP ) )
            // InternalPDDL.g:3081:1: ( RULE_OP )
            {
            // InternalPDDL.g:3081:1: ( RULE_OP )
            // InternalPDDL.g:3082:2: RULE_OP
            {
             before(grammarAccess.getPDDLActionAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getPDDLActionAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__0__Impl"


    // $ANTLR start "rule__PDDLAction__Group__1"
    // InternalPDDL.g:3091:1: rule__PDDLAction__Group__1 : rule__PDDLAction__Group__1__Impl rule__PDDLAction__Group__2 ;
    public final void rule__PDDLAction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3095:1: ( rule__PDDLAction__Group__1__Impl rule__PDDLAction__Group__2 )
            // InternalPDDL.g:3096:2: rule__PDDLAction__Group__1__Impl rule__PDDLAction__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__PDDLAction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLAction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__1"


    // $ANTLR start "rule__PDDLAction__Group__1__Impl"
    // InternalPDDL.g:3103:1: rule__PDDLAction__Group__1__Impl : ( ':action' ) ;
    public final void rule__PDDLAction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3107:1: ( ( ':action' ) )
            // InternalPDDL.g:3108:1: ( ':action' )
            {
            // InternalPDDL.g:3108:1: ( ':action' )
            // InternalPDDL.g:3109:2: ':action'
            {
             before(grammarAccess.getPDDLActionAccess().getActionKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getPDDLActionAccess().getActionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__1__Impl"


    // $ANTLR start "rule__PDDLAction__Group__2"
    // InternalPDDL.g:3118:1: rule__PDDLAction__Group__2 : rule__PDDLAction__Group__2__Impl rule__PDDLAction__Group__3 ;
    public final void rule__PDDLAction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3122:1: ( rule__PDDLAction__Group__2__Impl rule__PDDLAction__Group__3 )
            // InternalPDDL.g:3123:2: rule__PDDLAction__Group__2__Impl rule__PDDLAction__Group__3
            {
            pushFollow(FOLLOW_24);
            rule__PDDLAction__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLAction__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__2"


    // $ANTLR start "rule__PDDLAction__Group__2__Impl"
    // InternalPDDL.g:3130:1: rule__PDDLAction__Group__2__Impl : ( ( rule__PDDLAction__ActionNameAssignment_2 ) ) ;
    public final void rule__PDDLAction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3134:1: ( ( ( rule__PDDLAction__ActionNameAssignment_2 ) ) )
            // InternalPDDL.g:3135:1: ( ( rule__PDDLAction__ActionNameAssignment_2 ) )
            {
            // InternalPDDL.g:3135:1: ( ( rule__PDDLAction__ActionNameAssignment_2 ) )
            // InternalPDDL.g:3136:2: ( rule__PDDLAction__ActionNameAssignment_2 )
            {
             before(grammarAccess.getPDDLActionAccess().getActionNameAssignment_2()); 
            // InternalPDDL.g:3137:2: ( rule__PDDLAction__ActionNameAssignment_2 )
            // InternalPDDL.g:3137:3: rule__PDDLAction__ActionNameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__PDDLAction__ActionNameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getPDDLActionAccess().getActionNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__2__Impl"


    // $ANTLR start "rule__PDDLAction__Group__3"
    // InternalPDDL.g:3145:1: rule__PDDLAction__Group__3 : rule__PDDLAction__Group__3__Impl rule__PDDLAction__Group__4 ;
    public final void rule__PDDLAction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3149:1: ( rule__PDDLAction__Group__3__Impl rule__PDDLAction__Group__4 )
            // InternalPDDL.g:3150:2: rule__PDDLAction__Group__3__Impl rule__PDDLAction__Group__4
            {
            pushFollow(FOLLOW_25);
            rule__PDDLAction__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLAction__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__3"


    // $ANTLR start "rule__PDDLAction__Group__3__Impl"
    // InternalPDDL.g:3157:1: rule__PDDLAction__Group__3__Impl : ( ( rule__PDDLAction__ParametersAssignment_3 ) ) ;
    public final void rule__PDDLAction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3161:1: ( ( ( rule__PDDLAction__ParametersAssignment_3 ) ) )
            // InternalPDDL.g:3162:1: ( ( rule__PDDLAction__ParametersAssignment_3 ) )
            {
            // InternalPDDL.g:3162:1: ( ( rule__PDDLAction__ParametersAssignment_3 ) )
            // InternalPDDL.g:3163:2: ( rule__PDDLAction__ParametersAssignment_3 )
            {
             before(grammarAccess.getPDDLActionAccess().getParametersAssignment_3()); 
            // InternalPDDL.g:3164:2: ( rule__PDDLAction__ParametersAssignment_3 )
            // InternalPDDL.g:3164:3: rule__PDDLAction__ParametersAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__PDDLAction__ParametersAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getPDDLActionAccess().getParametersAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__3__Impl"


    // $ANTLR start "rule__PDDLAction__Group__4"
    // InternalPDDL.g:3172:1: rule__PDDLAction__Group__4 : rule__PDDLAction__Group__4__Impl rule__PDDLAction__Group__5 ;
    public final void rule__PDDLAction__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3176:1: ( rule__PDDLAction__Group__4__Impl rule__PDDLAction__Group__5 )
            // InternalPDDL.g:3177:2: rule__PDDLAction__Group__4__Impl rule__PDDLAction__Group__5
            {
            pushFollow(FOLLOW_25);
            rule__PDDLAction__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLAction__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__4"


    // $ANTLR start "rule__PDDLAction__Group__4__Impl"
    // InternalPDDL.g:3184:1: rule__PDDLAction__Group__4__Impl : ( ( rule__PDDLAction__PreconditionsAssignment_4 )? ) ;
    public final void rule__PDDLAction__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3188:1: ( ( ( rule__PDDLAction__PreconditionsAssignment_4 )? ) )
            // InternalPDDL.g:3189:1: ( ( rule__PDDLAction__PreconditionsAssignment_4 )? )
            {
            // InternalPDDL.g:3189:1: ( ( rule__PDDLAction__PreconditionsAssignment_4 )? )
            // InternalPDDL.g:3190:2: ( rule__PDDLAction__PreconditionsAssignment_4 )?
            {
             before(grammarAccess.getPDDLActionAccess().getPreconditionsAssignment_4()); 
            // InternalPDDL.g:3191:2: ( rule__PDDLAction__PreconditionsAssignment_4 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==30) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalPDDL.g:3191:3: rule__PDDLAction__PreconditionsAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__PDDLAction__PreconditionsAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPDDLActionAccess().getPreconditionsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__4__Impl"


    // $ANTLR start "rule__PDDLAction__Group__5"
    // InternalPDDL.g:3199:1: rule__PDDLAction__Group__5 : rule__PDDLAction__Group__5__Impl rule__PDDLAction__Group__6 ;
    public final void rule__PDDLAction__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3203:1: ( rule__PDDLAction__Group__5__Impl rule__PDDLAction__Group__6 )
            // InternalPDDL.g:3204:2: rule__PDDLAction__Group__5__Impl rule__PDDLAction__Group__6
            {
            pushFollow(FOLLOW_9);
            rule__PDDLAction__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PDDLAction__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__5"


    // $ANTLR start "rule__PDDLAction__Group__5__Impl"
    // InternalPDDL.g:3211:1: rule__PDDLAction__Group__5__Impl : ( ( rule__PDDLAction__EffectsAssignment_5 ) ) ;
    public final void rule__PDDLAction__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3215:1: ( ( ( rule__PDDLAction__EffectsAssignment_5 ) ) )
            // InternalPDDL.g:3216:1: ( ( rule__PDDLAction__EffectsAssignment_5 ) )
            {
            // InternalPDDL.g:3216:1: ( ( rule__PDDLAction__EffectsAssignment_5 ) )
            // InternalPDDL.g:3217:2: ( rule__PDDLAction__EffectsAssignment_5 )
            {
             before(grammarAccess.getPDDLActionAccess().getEffectsAssignment_5()); 
            // InternalPDDL.g:3218:2: ( rule__PDDLAction__EffectsAssignment_5 )
            // InternalPDDL.g:3218:3: rule__PDDLAction__EffectsAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__PDDLAction__EffectsAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getPDDLActionAccess().getEffectsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__5__Impl"


    // $ANTLR start "rule__PDDLAction__Group__6"
    // InternalPDDL.g:3226:1: rule__PDDLAction__Group__6 : rule__PDDLAction__Group__6__Impl ;
    public final void rule__PDDLAction__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3230:1: ( rule__PDDLAction__Group__6__Impl )
            // InternalPDDL.g:3231:2: rule__PDDLAction__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PDDLAction__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__6"


    // $ANTLR start "rule__PDDLAction__Group__6__Impl"
    // InternalPDDL.g:3237:1: rule__PDDLAction__Group__6__Impl : ( RULE_CP ) ;
    public final void rule__PDDLAction__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3241:1: ( ( RULE_CP ) )
            // InternalPDDL.g:3242:1: ( RULE_CP )
            {
            // InternalPDDL.g:3242:1: ( RULE_CP )
            // InternalPDDL.g:3243:2: RULE_CP
            {
             before(grammarAccess.getPDDLActionAccess().getCPTerminalRuleCall_6()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getPDDLActionAccess().getCPTerminalRuleCall_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__Group__6__Impl"


    // $ANTLR start "rule__Parameters__Group__0"
    // InternalPDDL.g:3253:1: rule__Parameters__Group__0 : rule__Parameters__Group__0__Impl rule__Parameters__Group__1 ;
    public final void rule__Parameters__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3257:1: ( rule__Parameters__Group__0__Impl rule__Parameters__Group__1 )
            // InternalPDDL.g:3258:2: rule__Parameters__Group__0__Impl rule__Parameters__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Parameters__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameters__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameters__Group__0"


    // $ANTLR start "rule__Parameters__Group__0__Impl"
    // InternalPDDL.g:3265:1: rule__Parameters__Group__0__Impl : ( ':parameters' ) ;
    public final void rule__Parameters__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3269:1: ( ( ':parameters' ) )
            // InternalPDDL.g:3270:1: ( ':parameters' )
            {
            // InternalPDDL.g:3270:1: ( ':parameters' )
            // InternalPDDL.g:3271:2: ':parameters'
            {
             before(grammarAccess.getParametersAccess().getParametersKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getParametersAccess().getParametersKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameters__Group__0__Impl"


    // $ANTLR start "rule__Parameters__Group__1"
    // InternalPDDL.g:3280:1: rule__Parameters__Group__1 : rule__Parameters__Group__1__Impl rule__Parameters__Group__2 ;
    public final void rule__Parameters__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3284:1: ( rule__Parameters__Group__1__Impl rule__Parameters__Group__2 )
            // InternalPDDL.g:3285:2: rule__Parameters__Group__1__Impl rule__Parameters__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__Parameters__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameters__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameters__Group__1"


    // $ANTLR start "rule__Parameters__Group__1__Impl"
    // InternalPDDL.g:3292:1: rule__Parameters__Group__1__Impl : ( RULE_OP ) ;
    public final void rule__Parameters__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3296:1: ( ( RULE_OP ) )
            // InternalPDDL.g:3297:1: ( RULE_OP )
            {
            // InternalPDDL.g:3297:1: ( RULE_OP )
            // InternalPDDL.g:3298:2: RULE_OP
            {
             before(grammarAccess.getParametersAccess().getOPTerminalRuleCall_1()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getParametersAccess().getOPTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameters__Group__1__Impl"


    // $ANTLR start "rule__Parameters__Group__2"
    // InternalPDDL.g:3307:1: rule__Parameters__Group__2 : rule__Parameters__Group__2__Impl rule__Parameters__Group__3 ;
    public final void rule__Parameters__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3311:1: ( rule__Parameters__Group__2__Impl rule__Parameters__Group__3 )
            // InternalPDDL.g:3312:2: rule__Parameters__Group__2__Impl rule__Parameters__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Parameters__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameters__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameters__Group__2"


    // $ANTLR start "rule__Parameters__Group__2__Impl"
    // InternalPDDL.g:3319:1: rule__Parameters__Group__2__Impl : ( ( rule__Parameters__ParametersListAssignment_2 ) ) ;
    public final void rule__Parameters__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3323:1: ( ( ( rule__Parameters__ParametersListAssignment_2 ) ) )
            // InternalPDDL.g:3324:1: ( ( rule__Parameters__ParametersListAssignment_2 ) )
            {
            // InternalPDDL.g:3324:1: ( ( rule__Parameters__ParametersListAssignment_2 ) )
            // InternalPDDL.g:3325:2: ( rule__Parameters__ParametersListAssignment_2 )
            {
             before(grammarAccess.getParametersAccess().getParametersListAssignment_2()); 
            // InternalPDDL.g:3326:2: ( rule__Parameters__ParametersListAssignment_2 )
            // InternalPDDL.g:3326:3: rule__Parameters__ParametersListAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Parameters__ParametersListAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getParametersAccess().getParametersListAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameters__Group__2__Impl"


    // $ANTLR start "rule__Parameters__Group__3"
    // InternalPDDL.g:3334:1: rule__Parameters__Group__3 : rule__Parameters__Group__3__Impl ;
    public final void rule__Parameters__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3338:1: ( rule__Parameters__Group__3__Impl )
            // InternalPDDL.g:3339:2: rule__Parameters__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Parameters__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameters__Group__3"


    // $ANTLR start "rule__Parameters__Group__3__Impl"
    // InternalPDDL.g:3345:1: rule__Parameters__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Parameters__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3349:1: ( ( RULE_CP ) )
            // InternalPDDL.g:3350:1: ( RULE_CP )
            {
            // InternalPDDL.g:3350:1: ( RULE_CP )
            // InternalPDDL.g:3351:2: RULE_CP
            {
             before(grammarAccess.getParametersAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getParametersAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameters__Group__3__Impl"


    // $ANTLR start "rule__Preconditions__Group__0"
    // InternalPDDL.g:3361:1: rule__Preconditions__Group__0 : rule__Preconditions__Group__0__Impl rule__Preconditions__Group__1 ;
    public final void rule__Preconditions__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3365:1: ( rule__Preconditions__Group__0__Impl rule__Preconditions__Group__1 )
            // InternalPDDL.g:3366:2: rule__Preconditions__Group__0__Impl rule__Preconditions__Group__1
            {
            pushFollow(FOLLOW_26);
            rule__Preconditions__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Preconditions__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Preconditions__Group__0"


    // $ANTLR start "rule__Preconditions__Group__0__Impl"
    // InternalPDDL.g:3373:1: rule__Preconditions__Group__0__Impl : ( ':precondition' ) ;
    public final void rule__Preconditions__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3377:1: ( ( ':precondition' ) )
            // InternalPDDL.g:3378:1: ( ':precondition' )
            {
            // InternalPDDL.g:3378:1: ( ':precondition' )
            // InternalPDDL.g:3379:2: ':precondition'
            {
             before(grammarAccess.getPreconditionsAccess().getPreconditionKeyword_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getPreconditionsAccess().getPreconditionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Preconditions__Group__0__Impl"


    // $ANTLR start "rule__Preconditions__Group__1"
    // InternalPDDL.g:3388:1: rule__Preconditions__Group__1 : rule__Preconditions__Group__1__Impl ;
    public final void rule__Preconditions__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3392:1: ( rule__Preconditions__Group__1__Impl )
            // InternalPDDL.g:3393:2: rule__Preconditions__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Preconditions__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Preconditions__Group__1"


    // $ANTLR start "rule__Preconditions__Group__1__Impl"
    // InternalPDDL.g:3399:1: rule__Preconditions__Group__1__Impl : ( ( rule__Preconditions__LeAssignment_1 ) ) ;
    public final void rule__Preconditions__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3403:1: ( ( ( rule__Preconditions__LeAssignment_1 ) ) )
            // InternalPDDL.g:3404:1: ( ( rule__Preconditions__LeAssignment_1 ) )
            {
            // InternalPDDL.g:3404:1: ( ( rule__Preconditions__LeAssignment_1 ) )
            // InternalPDDL.g:3405:2: ( rule__Preconditions__LeAssignment_1 )
            {
             before(grammarAccess.getPreconditionsAccess().getLeAssignment_1()); 
            // InternalPDDL.g:3406:2: ( rule__Preconditions__LeAssignment_1 )
            // InternalPDDL.g:3406:3: rule__Preconditions__LeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Preconditions__LeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPreconditionsAccess().getLeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Preconditions__Group__1__Impl"


    // $ANTLR start "rule__Effects__Group__0"
    // InternalPDDL.g:3415:1: rule__Effects__Group__0 : rule__Effects__Group__0__Impl rule__Effects__Group__1 ;
    public final void rule__Effects__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3419:1: ( rule__Effects__Group__0__Impl rule__Effects__Group__1 )
            // InternalPDDL.g:3420:2: rule__Effects__Group__0__Impl rule__Effects__Group__1
            {
            pushFollow(FOLLOW_26);
            rule__Effects__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effects__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effects__Group__0"


    // $ANTLR start "rule__Effects__Group__0__Impl"
    // InternalPDDL.g:3427:1: rule__Effects__Group__0__Impl : ( ':effect' ) ;
    public final void rule__Effects__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3431:1: ( ( ':effect' ) )
            // InternalPDDL.g:3432:1: ( ':effect' )
            {
            // InternalPDDL.g:3432:1: ( ':effect' )
            // InternalPDDL.g:3433:2: ':effect'
            {
             before(grammarAccess.getEffectsAccess().getEffectKeyword_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getEffectsAccess().getEffectKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effects__Group__0__Impl"


    // $ANTLR start "rule__Effects__Group__1"
    // InternalPDDL.g:3442:1: rule__Effects__Group__1 : rule__Effects__Group__1__Impl ;
    public final void rule__Effects__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3446:1: ( rule__Effects__Group__1__Impl )
            // InternalPDDL.g:3447:2: rule__Effects__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Effects__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effects__Group__1"


    // $ANTLR start "rule__Effects__Group__1__Impl"
    // InternalPDDL.g:3453:1: rule__Effects__Group__1__Impl : ( ( rule__Effects__LeAssignment_1 ) ) ;
    public final void rule__Effects__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3457:1: ( ( ( rule__Effects__LeAssignment_1 ) ) )
            // InternalPDDL.g:3458:1: ( ( rule__Effects__LeAssignment_1 ) )
            {
            // InternalPDDL.g:3458:1: ( ( rule__Effects__LeAssignment_1 ) )
            // InternalPDDL.g:3459:2: ( rule__Effects__LeAssignment_1 )
            {
             before(grammarAccess.getEffectsAccess().getLeAssignment_1()); 
            // InternalPDDL.g:3460:2: ( rule__Effects__LeAssignment_1 )
            // InternalPDDL.g:3460:3: rule__Effects__LeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Effects__LeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEffectsAccess().getLeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effects__Group__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0__0"
    // InternalPDDL.g:3469:1: rule__LogicalExpressionDM__Group_0__0 : rule__LogicalExpressionDM__Group_0__0__Impl rule__LogicalExpressionDM__Group_0__1 ;
    public final void rule__LogicalExpressionDM__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3473:1: ( rule__LogicalExpressionDM__Group_0__0__Impl rule__LogicalExpressionDM__Group_0__1 )
            // InternalPDDL.g:3474:2: rule__LogicalExpressionDM__Group_0__0__Impl rule__LogicalExpressionDM__Group_0__1
            {
            pushFollow(FOLLOW_7);
            rule__LogicalExpressionDM__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0__0__Impl"
    // InternalPDDL.g:3481:1: rule__LogicalExpressionDM__Group_0__0__Impl : ( () ) ;
    public final void rule__LogicalExpressionDM__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3485:1: ( ( () ) )
            // InternalPDDL.g:3486:1: ( () )
            {
            // InternalPDDL.g:3486:1: ( () )
            // InternalPDDL.g:3487:2: ()
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLogicalExpressionDMAction_0_0()); 
            // InternalPDDL.g:3488:2: ()
            // InternalPDDL.g:3488:3: 
            {
            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLogicalExpressionDMAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0__1"
    // InternalPDDL.g:3496:1: rule__LogicalExpressionDM__Group_0__1 : rule__LogicalExpressionDM__Group_0__1__Impl ;
    public final void rule__LogicalExpressionDM__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3500:1: ( rule__LogicalExpressionDM__Group_0__1__Impl )
            // InternalPDDL.g:3501:2: rule__LogicalExpressionDM__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0__1__Impl"
    // InternalPDDL.g:3507:1: rule__LogicalExpressionDM__Group_0__1__Impl : ( ( rule__LogicalExpressionDM__Group_0_1__0 ) ) ;
    public final void rule__LogicalExpressionDM__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3511:1: ( ( ( rule__LogicalExpressionDM__Group_0_1__0 ) ) )
            // InternalPDDL.g:3512:1: ( ( rule__LogicalExpressionDM__Group_0_1__0 ) )
            {
            // InternalPDDL.g:3512:1: ( ( rule__LogicalExpressionDM__Group_0_1__0 ) )
            // InternalPDDL.g:3513:2: ( rule__LogicalExpressionDM__Group_0_1__0 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getGroup_0_1()); 
            // InternalPDDL.g:3514:2: ( rule__LogicalExpressionDM__Group_0_1__0 )
            // InternalPDDL.g:3514:3: rule__LogicalExpressionDM__Group_0_1__0
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_0_1__0();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getGroup_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0_1__0"
    // InternalPDDL.g:3523:1: rule__LogicalExpressionDM__Group_0_1__0 : rule__LogicalExpressionDM__Group_0_1__0__Impl rule__LogicalExpressionDM__Group_0_1__1 ;
    public final void rule__LogicalExpressionDM__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3527:1: ( rule__LogicalExpressionDM__Group_0_1__0__Impl rule__LogicalExpressionDM__Group_0_1__1 )
            // InternalPDDL.g:3528:2: rule__LogicalExpressionDM__Group_0_1__0__Impl rule__LogicalExpressionDM__Group_0_1__1
            {
            pushFollow(FOLLOW_11);
            rule__LogicalExpressionDM__Group_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0_1__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0_1__0__Impl"
    // InternalPDDL.g:3535:1: rule__LogicalExpressionDM__Group_0_1__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3539:1: ( ( RULE_OP ) )
            // InternalPDDL.g:3540:1: ( RULE_OP )
            {
            // InternalPDDL.g:3540:1: ( RULE_OP )
            // InternalPDDL.g:3541:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_0_1_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0_1__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0_1__1"
    // InternalPDDL.g:3550:1: rule__LogicalExpressionDM__Group_0_1__1 : rule__LogicalExpressionDM__Group_0_1__1__Impl rule__LogicalExpressionDM__Group_0_1__2 ;
    public final void rule__LogicalExpressionDM__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3554:1: ( rule__LogicalExpressionDM__Group_0_1__1__Impl rule__LogicalExpressionDM__Group_0_1__2 )
            // InternalPDDL.g:3555:2: rule__LogicalExpressionDM__Group_0_1__1__Impl rule__LogicalExpressionDM__Group_0_1__2
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_0_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_0_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0_1__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0_1__1__Impl"
    // InternalPDDL.g:3562:1: rule__LogicalExpressionDM__Group_0_1__1__Impl : ( ( rule__LogicalExpressionDM__PAssignment_0_1_1 ) ) ;
    public final void rule__LogicalExpressionDM__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3566:1: ( ( ( rule__LogicalExpressionDM__PAssignment_0_1_1 ) ) )
            // InternalPDDL.g:3567:1: ( ( rule__LogicalExpressionDM__PAssignment_0_1_1 ) )
            {
            // InternalPDDL.g:3567:1: ( ( rule__LogicalExpressionDM__PAssignment_0_1_1 ) )
            // InternalPDDL.g:3568:2: ( rule__LogicalExpressionDM__PAssignment_0_1_1 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getPAssignment_0_1_1()); 
            // InternalPDDL.g:3569:2: ( rule__LogicalExpressionDM__PAssignment_0_1_1 )
            // InternalPDDL.g:3569:3: rule__LogicalExpressionDM__PAssignment_0_1_1
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__PAssignment_0_1_1();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getPAssignment_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0_1__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0_1__2"
    // InternalPDDL.g:3577:1: rule__LogicalExpressionDM__Group_0_1__2 : rule__LogicalExpressionDM__Group_0_1__2__Impl ;
    public final void rule__LogicalExpressionDM__Group_0_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3581:1: ( rule__LogicalExpressionDM__Group_0_1__2__Impl )
            // InternalPDDL.g:3582:2: rule__LogicalExpressionDM__Group_0_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_0_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0_1__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_0_1__2__Impl"
    // InternalPDDL.g:3588:1: rule__LogicalExpressionDM__Group_0_1__2__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_0_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3592:1: ( ( RULE_CP ) )
            // InternalPDDL.g:3593:1: ( RULE_CP )
            {
            // InternalPDDL.g:3593:1: ( RULE_CP )
            // InternalPDDL.g:3594:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_0_1_2()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_0_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_0_1__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_2__0"
    // InternalPDDL.g:3604:1: rule__LogicalExpressionDM__Group_2__0 : rule__LogicalExpressionDM__Group_2__0__Impl rule__LogicalExpressionDM__Group_2__1 ;
    public final void rule__LogicalExpressionDM__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3608:1: ( rule__LogicalExpressionDM__Group_2__0__Impl rule__LogicalExpressionDM__Group_2__1 )
            // InternalPDDL.g:3609:2: rule__LogicalExpressionDM__Group_2__0__Impl rule__LogicalExpressionDM__Group_2__1
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_2__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_2__0__Impl"
    // InternalPDDL.g:3616:1: rule__LogicalExpressionDM__Group_2__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3620:1: ( ( RULE_OP ) )
            // InternalPDDL.g:3621:1: ( RULE_OP )
            {
            // InternalPDDL.g:3621:1: ( RULE_OP )
            // InternalPDDL.g:3622:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_2_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_2__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_2__1"
    // InternalPDDL.g:3631:1: rule__LogicalExpressionDM__Group_2__1 : rule__LogicalExpressionDM__Group_2__1__Impl rule__LogicalExpressionDM__Group_2__2 ;
    public final void rule__LogicalExpressionDM__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3635:1: ( rule__LogicalExpressionDM__Group_2__1__Impl rule__LogicalExpressionDM__Group_2__2 )
            // InternalPDDL.g:3636:2: rule__LogicalExpressionDM__Group_2__1__Impl rule__LogicalExpressionDM__Group_2__2
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_2__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_2__1__Impl"
    // InternalPDDL.g:3643:1: rule__LogicalExpressionDM__Group_2__1__Impl : ( ( rule__LogicalExpressionDM__LeAssignment_2_1 ) ) ;
    public final void rule__LogicalExpressionDM__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3647:1: ( ( ( rule__LogicalExpressionDM__LeAssignment_2_1 ) ) )
            // InternalPDDL.g:3648:1: ( ( rule__LogicalExpressionDM__LeAssignment_2_1 ) )
            {
            // InternalPDDL.g:3648:1: ( ( rule__LogicalExpressionDM__LeAssignment_2_1 ) )
            // InternalPDDL.g:3649:2: ( rule__LogicalExpressionDM__LeAssignment_2_1 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeAssignment_2_1()); 
            // InternalPDDL.g:3650:2: ( rule__LogicalExpressionDM__LeAssignment_2_1 )
            // InternalPDDL.g:3650:3: rule__LogicalExpressionDM__LeAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_2__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_2__2"
    // InternalPDDL.g:3658:1: rule__LogicalExpressionDM__Group_2__2 : rule__LogicalExpressionDM__Group_2__2__Impl ;
    public final void rule__LogicalExpressionDM__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3662:1: ( rule__LogicalExpressionDM__Group_2__2__Impl )
            // InternalPDDL.g:3663:2: rule__LogicalExpressionDM__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_2__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_2__2__Impl"
    // InternalPDDL.g:3669:1: rule__LogicalExpressionDM__Group_2__2__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3673:1: ( ( RULE_CP ) )
            // InternalPDDL.g:3674:1: ( RULE_CP )
            {
            // InternalPDDL.g:3674:1: ( RULE_CP )
            // InternalPDDL.g:3675:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_2_2()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_2__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__0"
    // InternalPDDL.g:3685:1: rule__LogicalExpressionDM__Group_3__0 : rule__LogicalExpressionDM__Group_3__0__Impl rule__LogicalExpressionDM__Group_3__1 ;
    public final void rule__LogicalExpressionDM__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3689:1: ( rule__LogicalExpressionDM__Group_3__0__Impl rule__LogicalExpressionDM__Group_3__1 )
            // InternalPDDL.g:3690:2: rule__LogicalExpressionDM__Group_3__0__Impl rule__LogicalExpressionDM__Group_3__1
            {
            pushFollow(FOLLOW_27);
            rule__LogicalExpressionDM__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__0__Impl"
    // InternalPDDL.g:3697:1: rule__LogicalExpressionDM__Group_3__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3701:1: ( ( RULE_OP ) )
            // InternalPDDL.g:3702:1: ( RULE_OP )
            {
            // InternalPDDL.g:3702:1: ( RULE_OP )
            // InternalPDDL.g:3703:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_3_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__1"
    // InternalPDDL.g:3712:1: rule__LogicalExpressionDM__Group_3__1 : rule__LogicalExpressionDM__Group_3__1__Impl rule__LogicalExpressionDM__Group_3__2 ;
    public final void rule__LogicalExpressionDM__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3716:1: ( rule__LogicalExpressionDM__Group_3__1__Impl rule__LogicalExpressionDM__Group_3__2 )
            // InternalPDDL.g:3717:2: rule__LogicalExpressionDM__Group_3__1__Impl rule__LogicalExpressionDM__Group_3__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__1__Impl"
    // InternalPDDL.g:3724:1: rule__LogicalExpressionDM__Group_3__1__Impl : ( '=' ) ;
    public final void rule__LogicalExpressionDM__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3728:1: ( ( '=' ) )
            // InternalPDDL.g:3729:1: ( '=' )
            {
            // InternalPDDL.g:3729:1: ( '=' )
            // InternalPDDL.g:3730:2: '='
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getEqualsSignKeyword_3_1()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getEqualsSignKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__2"
    // InternalPDDL.g:3739:1: rule__LogicalExpressionDM__Group_3__2 : rule__LogicalExpressionDM__Group_3__2__Impl rule__LogicalExpressionDM__Group_3__3 ;
    public final void rule__LogicalExpressionDM__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3743:1: ( rule__LogicalExpressionDM__Group_3__2__Impl rule__LogicalExpressionDM__Group_3__3 )
            // InternalPDDL.g:3744:2: rule__LogicalExpressionDM__Group_3__2__Impl rule__LogicalExpressionDM__Group_3__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__2__Impl"
    // InternalPDDL.g:3751:1: rule__LogicalExpressionDM__Group_3__2__Impl : ( ( rule__LogicalExpressionDM__VEqual1Assignment_3_2 ) ) ;
    public final void rule__LogicalExpressionDM__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3755:1: ( ( ( rule__LogicalExpressionDM__VEqual1Assignment_3_2 ) ) )
            // InternalPDDL.g:3756:1: ( ( rule__LogicalExpressionDM__VEqual1Assignment_3_2 ) )
            {
            // InternalPDDL.g:3756:1: ( ( rule__LogicalExpressionDM__VEqual1Assignment_3_2 ) )
            // InternalPDDL.g:3757:2: ( rule__LogicalExpressionDM__VEqual1Assignment_3_2 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getVEqual1Assignment_3_2()); 
            // InternalPDDL.g:3758:2: ( rule__LogicalExpressionDM__VEqual1Assignment_3_2 )
            // InternalPDDL.g:3758:3: rule__LogicalExpressionDM__VEqual1Assignment_3_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__VEqual1Assignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getVEqual1Assignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__3"
    // InternalPDDL.g:3766:1: rule__LogicalExpressionDM__Group_3__3 : rule__LogicalExpressionDM__Group_3__3__Impl rule__LogicalExpressionDM__Group_3__4 ;
    public final void rule__LogicalExpressionDM__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3770:1: ( rule__LogicalExpressionDM__Group_3__3__Impl rule__LogicalExpressionDM__Group_3__4 )
            // InternalPDDL.g:3771:2: rule__LogicalExpressionDM__Group_3__3__Impl rule__LogicalExpressionDM__Group_3__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__3"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__3__Impl"
    // InternalPDDL.g:3778:1: rule__LogicalExpressionDM__Group_3__3__Impl : ( ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )* ) ) ;
    public final void rule__LogicalExpressionDM__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3782:1: ( ( ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )* ) ) )
            // InternalPDDL.g:3783:1: ( ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )* ) )
            {
            // InternalPDDL.g:3783:1: ( ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )* ) )
            // InternalPDDL.g:3784:2: ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )* )
            {
            // InternalPDDL.g:3784:2: ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 ) )
            // InternalPDDL.g:3785:3: ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getVEqual2Assignment_3_3()); 
            // InternalPDDL.g:3786:3: ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )
            // InternalPDDL.g:3786:4: rule__LogicalExpressionDM__VEqual2Assignment_3_3
            {
            pushFollow(FOLLOW_28);
            rule__LogicalExpressionDM__VEqual2Assignment_3_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getVEqual2Assignment_3_3()); 

            }

            // InternalPDDL.g:3789:2: ( ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )* )
            // InternalPDDL.g:3790:3: ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )*
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getVEqual2Assignment_3_3()); 
            // InternalPDDL.g:3791:3: ( rule__LogicalExpressionDM__VEqual2Assignment_3_3 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==RULE_OP||LA25_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalPDDL.g:3791:4: rule__LogicalExpressionDM__VEqual2Assignment_3_3
            	    {
            	    pushFollow(FOLLOW_28);
            	    rule__LogicalExpressionDM__VEqual2Assignment_3_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getLogicalExpressionDMAccess().getVEqual2Assignment_3_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__3__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__4"
    // InternalPDDL.g:3800:1: rule__LogicalExpressionDM__Group_3__4 : rule__LogicalExpressionDM__Group_3__4__Impl ;
    public final void rule__LogicalExpressionDM__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3804:1: ( rule__LogicalExpressionDM__Group_3__4__Impl )
            // InternalPDDL.g:3805:2: rule__LogicalExpressionDM__Group_3__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_3__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__4"


    // $ANTLR start "rule__LogicalExpressionDM__Group_3__4__Impl"
    // InternalPDDL.g:3811:1: rule__LogicalExpressionDM__Group_3__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3815:1: ( ( RULE_CP ) )
            // InternalPDDL.g:3816:1: ( RULE_CP )
            {
            // InternalPDDL.g:3816:1: ( RULE_CP )
            // InternalPDDL.g:3817:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_3_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_3_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_3__4__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_4__0"
    // InternalPDDL.g:3827:1: rule__LogicalExpressionDM__Group_4__0 : rule__LogicalExpressionDM__Group_4__0__Impl rule__LogicalExpressionDM__Group_4__1 ;
    public final void rule__LogicalExpressionDM__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3831:1: ( rule__LogicalExpressionDM__Group_4__0__Impl rule__LogicalExpressionDM__Group_4__1 )
            // InternalPDDL.g:3832:2: rule__LogicalExpressionDM__Group_4__0__Impl rule__LogicalExpressionDM__Group_4__1
            {
            pushFollow(FOLLOW_29);
            rule__LogicalExpressionDM__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_4__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_4__0__Impl"
    // InternalPDDL.g:3839:1: rule__LogicalExpressionDM__Group_4__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3843:1: ( ( RULE_OP ) )
            // InternalPDDL.g:3844:1: ( RULE_OP )
            {
            // InternalPDDL.g:3844:1: ( RULE_OP )
            // InternalPDDL.g:3845:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_4_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_4__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_4__1"
    // InternalPDDL.g:3854:1: rule__LogicalExpressionDM__Group_4__1 : rule__LogicalExpressionDM__Group_4__1__Impl rule__LogicalExpressionDM__Group_4__2 ;
    public final void rule__LogicalExpressionDM__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3858:1: ( rule__LogicalExpressionDM__Group_4__1__Impl rule__LogicalExpressionDM__Group_4__2 )
            // InternalPDDL.g:3859:2: rule__LogicalExpressionDM__Group_4__1__Impl rule__LogicalExpressionDM__Group_4__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_4__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_4__1__Impl"
    // InternalPDDL.g:3866:1: rule__LogicalExpressionDM__Group_4__1__Impl : ( 'not' ) ;
    public final void rule__LogicalExpressionDM__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3870:1: ( ( 'not' ) )
            // InternalPDDL.g:3871:1: ( 'not' )
            {
            // InternalPDDL.g:3871:1: ( 'not' )
            // InternalPDDL.g:3872:2: 'not'
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getNotKeyword_4_1()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getNotKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_4__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_4__2"
    // InternalPDDL.g:3881:1: rule__LogicalExpressionDM__Group_4__2 : rule__LogicalExpressionDM__Group_4__2__Impl rule__LogicalExpressionDM__Group_4__3 ;
    public final void rule__LogicalExpressionDM__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3885:1: ( rule__LogicalExpressionDM__Group_4__2__Impl rule__LogicalExpressionDM__Group_4__3 )
            // InternalPDDL.g:3886:2: rule__LogicalExpressionDM__Group_4__2__Impl rule__LogicalExpressionDM__Group_4__3
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_4__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_4__2__Impl"
    // InternalPDDL.g:3893:1: rule__LogicalExpressionDM__Group_4__2__Impl : ( ( rule__LogicalExpressionDM__LeNotAssignment_4_2 ) ) ;
    public final void rule__LogicalExpressionDM__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3897:1: ( ( ( rule__LogicalExpressionDM__LeNotAssignment_4_2 ) ) )
            // InternalPDDL.g:3898:1: ( ( rule__LogicalExpressionDM__LeNotAssignment_4_2 ) )
            {
            // InternalPDDL.g:3898:1: ( ( rule__LogicalExpressionDM__LeNotAssignment_4_2 ) )
            // InternalPDDL.g:3899:2: ( rule__LogicalExpressionDM__LeNotAssignment_4_2 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeNotAssignment_4_2()); 
            // InternalPDDL.g:3900:2: ( rule__LogicalExpressionDM__LeNotAssignment_4_2 )
            // InternalPDDL.g:3900:3: rule__LogicalExpressionDM__LeNotAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeNotAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeNotAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_4__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_4__3"
    // InternalPDDL.g:3908:1: rule__LogicalExpressionDM__Group_4__3 : rule__LogicalExpressionDM__Group_4__3__Impl ;
    public final void rule__LogicalExpressionDM__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3912:1: ( rule__LogicalExpressionDM__Group_4__3__Impl )
            // InternalPDDL.g:3913:2: rule__LogicalExpressionDM__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_4__3"


    // $ANTLR start "rule__LogicalExpressionDM__Group_4__3__Impl"
    // InternalPDDL.g:3919:1: rule__LogicalExpressionDM__Group_4__3__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3923:1: ( ( RULE_CP ) )
            // InternalPDDL.g:3924:1: ( RULE_CP )
            {
            // InternalPDDL.g:3924:1: ( RULE_CP )
            // InternalPDDL.g:3925:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_4_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_4__3__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__0"
    // InternalPDDL.g:3935:1: rule__LogicalExpressionDM__Group_5__0 : rule__LogicalExpressionDM__Group_5__0__Impl rule__LogicalExpressionDM__Group_5__1 ;
    public final void rule__LogicalExpressionDM__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3939:1: ( rule__LogicalExpressionDM__Group_5__0__Impl rule__LogicalExpressionDM__Group_5__1 )
            // InternalPDDL.g:3940:2: rule__LogicalExpressionDM__Group_5__0__Impl rule__LogicalExpressionDM__Group_5__1
            {
            pushFollow(FOLLOW_30);
            rule__LogicalExpressionDM__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__0__Impl"
    // InternalPDDL.g:3947:1: rule__LogicalExpressionDM__Group_5__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3951:1: ( ( RULE_OP ) )
            // InternalPDDL.g:3952:1: ( RULE_OP )
            {
            // InternalPDDL.g:3952:1: ( RULE_OP )
            // InternalPDDL.g:3953:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_5_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__1"
    // InternalPDDL.g:3962:1: rule__LogicalExpressionDM__Group_5__1 : rule__LogicalExpressionDM__Group_5__1__Impl rule__LogicalExpressionDM__Group_5__2 ;
    public final void rule__LogicalExpressionDM__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3966:1: ( rule__LogicalExpressionDM__Group_5__1__Impl rule__LogicalExpressionDM__Group_5__2 )
            // InternalPDDL.g:3967:2: rule__LogicalExpressionDM__Group_5__1__Impl rule__LogicalExpressionDM__Group_5__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__1__Impl"
    // InternalPDDL.g:3974:1: rule__LogicalExpressionDM__Group_5__1__Impl : ( 'and' ) ;
    public final void rule__LogicalExpressionDM__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3978:1: ( ( 'and' ) )
            // InternalPDDL.g:3979:1: ( 'and' )
            {
            // InternalPDDL.g:3979:1: ( 'and' )
            // InternalPDDL.g:3980:2: 'and'
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getAndKeyword_5_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getAndKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__2"
    // InternalPDDL.g:3989:1: rule__LogicalExpressionDM__Group_5__2 : rule__LogicalExpressionDM__Group_5__2__Impl rule__LogicalExpressionDM__Group_5__3 ;
    public final void rule__LogicalExpressionDM__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:3993:1: ( rule__LogicalExpressionDM__Group_5__2__Impl rule__LogicalExpressionDM__Group_5__3 )
            // InternalPDDL.g:3994:2: rule__LogicalExpressionDM__Group_5__2__Impl rule__LogicalExpressionDM__Group_5__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__2__Impl"
    // InternalPDDL.g:4001:1: rule__LogicalExpressionDM__Group_5__2__Impl : ( ( rule__LogicalExpressionDM__LeAnd1Assignment_5_2 ) ) ;
    public final void rule__LogicalExpressionDM__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4005:1: ( ( ( rule__LogicalExpressionDM__LeAnd1Assignment_5_2 ) ) )
            // InternalPDDL.g:4006:1: ( ( rule__LogicalExpressionDM__LeAnd1Assignment_5_2 ) )
            {
            // InternalPDDL.g:4006:1: ( ( rule__LogicalExpressionDM__LeAnd1Assignment_5_2 ) )
            // InternalPDDL.g:4007:2: ( rule__LogicalExpressionDM__LeAnd1Assignment_5_2 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeAnd1Assignment_5_2()); 
            // InternalPDDL.g:4008:2: ( rule__LogicalExpressionDM__LeAnd1Assignment_5_2 )
            // InternalPDDL.g:4008:3: rule__LogicalExpressionDM__LeAnd1Assignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeAnd1Assignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeAnd1Assignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__3"
    // InternalPDDL.g:4016:1: rule__LogicalExpressionDM__Group_5__3 : rule__LogicalExpressionDM__Group_5__3__Impl rule__LogicalExpressionDM__Group_5__4 ;
    public final void rule__LogicalExpressionDM__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4020:1: ( rule__LogicalExpressionDM__Group_5__3__Impl rule__LogicalExpressionDM__Group_5__4 )
            // InternalPDDL.g:4021:2: rule__LogicalExpressionDM__Group_5__3__Impl rule__LogicalExpressionDM__Group_5__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__3"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__3__Impl"
    // InternalPDDL.g:4028:1: rule__LogicalExpressionDM__Group_5__3__Impl : ( ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )* ) ) ;
    public final void rule__LogicalExpressionDM__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4032:1: ( ( ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )* ) ) )
            // InternalPDDL.g:4033:1: ( ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )* ) )
            {
            // InternalPDDL.g:4033:1: ( ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )* ) )
            // InternalPDDL.g:4034:2: ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )* )
            {
            // InternalPDDL.g:4034:2: ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 ) )
            // InternalPDDL.g:4035:3: ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeAnd2Assignment_5_3()); 
            // InternalPDDL.g:4036:3: ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )
            // InternalPDDL.g:4036:4: rule__LogicalExpressionDM__LeAnd2Assignment_5_3
            {
            pushFollow(FOLLOW_28);
            rule__LogicalExpressionDM__LeAnd2Assignment_5_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeAnd2Assignment_5_3()); 

            }

            // InternalPDDL.g:4039:2: ( ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )* )
            // InternalPDDL.g:4040:3: ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )*
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeAnd2Assignment_5_3()); 
            // InternalPDDL.g:4041:3: ( rule__LogicalExpressionDM__LeAnd2Assignment_5_3 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==RULE_OP||LA26_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalPDDL.g:4041:4: rule__LogicalExpressionDM__LeAnd2Assignment_5_3
            	    {
            	    pushFollow(FOLLOW_28);
            	    rule__LogicalExpressionDM__LeAnd2Assignment_5_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getLogicalExpressionDMAccess().getLeAnd2Assignment_5_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__3__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__4"
    // InternalPDDL.g:4050:1: rule__LogicalExpressionDM__Group_5__4 : rule__LogicalExpressionDM__Group_5__4__Impl ;
    public final void rule__LogicalExpressionDM__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4054:1: ( rule__LogicalExpressionDM__Group_5__4__Impl )
            // InternalPDDL.g:4055:2: rule__LogicalExpressionDM__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__4"


    // $ANTLR start "rule__LogicalExpressionDM__Group_5__4__Impl"
    // InternalPDDL.g:4061:1: rule__LogicalExpressionDM__Group_5__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4065:1: ( ( RULE_CP ) )
            // InternalPDDL.g:4066:1: ( RULE_CP )
            {
            // InternalPDDL.g:4066:1: ( RULE_CP )
            // InternalPDDL.g:4067:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_5_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_5__4__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__0"
    // InternalPDDL.g:4077:1: rule__LogicalExpressionDM__Group_6__0 : rule__LogicalExpressionDM__Group_6__0__Impl rule__LogicalExpressionDM__Group_6__1 ;
    public final void rule__LogicalExpressionDM__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4081:1: ( rule__LogicalExpressionDM__Group_6__0__Impl rule__LogicalExpressionDM__Group_6__1 )
            // InternalPDDL.g:4082:2: rule__LogicalExpressionDM__Group_6__0__Impl rule__LogicalExpressionDM__Group_6__1
            {
            pushFollow(FOLLOW_31);
            rule__LogicalExpressionDM__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__0__Impl"
    // InternalPDDL.g:4089:1: rule__LogicalExpressionDM__Group_6__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4093:1: ( ( RULE_OP ) )
            // InternalPDDL.g:4094:1: ( RULE_OP )
            {
            // InternalPDDL.g:4094:1: ( RULE_OP )
            // InternalPDDL.g:4095:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_6_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__1"
    // InternalPDDL.g:4104:1: rule__LogicalExpressionDM__Group_6__1 : rule__LogicalExpressionDM__Group_6__1__Impl rule__LogicalExpressionDM__Group_6__2 ;
    public final void rule__LogicalExpressionDM__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4108:1: ( rule__LogicalExpressionDM__Group_6__1__Impl rule__LogicalExpressionDM__Group_6__2 )
            // InternalPDDL.g:4109:2: rule__LogicalExpressionDM__Group_6__1__Impl rule__LogicalExpressionDM__Group_6__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__1__Impl"
    // InternalPDDL.g:4116:1: rule__LogicalExpressionDM__Group_6__1__Impl : ( 'or' ) ;
    public final void rule__LogicalExpressionDM__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4120:1: ( ( 'or' ) )
            // InternalPDDL.g:4121:1: ( 'or' )
            {
            // InternalPDDL.g:4121:1: ( 'or' )
            // InternalPDDL.g:4122:2: 'or'
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOrKeyword_6_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOrKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__2"
    // InternalPDDL.g:4131:1: rule__LogicalExpressionDM__Group_6__2 : rule__LogicalExpressionDM__Group_6__2__Impl rule__LogicalExpressionDM__Group_6__3 ;
    public final void rule__LogicalExpressionDM__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4135:1: ( rule__LogicalExpressionDM__Group_6__2__Impl rule__LogicalExpressionDM__Group_6__3 )
            // InternalPDDL.g:4136:2: rule__LogicalExpressionDM__Group_6__2__Impl rule__LogicalExpressionDM__Group_6__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__2__Impl"
    // InternalPDDL.g:4143:1: rule__LogicalExpressionDM__Group_6__2__Impl : ( ( rule__LogicalExpressionDM__LeOr1Assignment_6_2 ) ) ;
    public final void rule__LogicalExpressionDM__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4147:1: ( ( ( rule__LogicalExpressionDM__LeOr1Assignment_6_2 ) ) )
            // InternalPDDL.g:4148:1: ( ( rule__LogicalExpressionDM__LeOr1Assignment_6_2 ) )
            {
            // InternalPDDL.g:4148:1: ( ( rule__LogicalExpressionDM__LeOr1Assignment_6_2 ) )
            // InternalPDDL.g:4149:2: ( rule__LogicalExpressionDM__LeOr1Assignment_6_2 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeOr1Assignment_6_2()); 
            // InternalPDDL.g:4150:2: ( rule__LogicalExpressionDM__LeOr1Assignment_6_2 )
            // InternalPDDL.g:4150:3: rule__LogicalExpressionDM__LeOr1Assignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeOr1Assignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeOr1Assignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__3"
    // InternalPDDL.g:4158:1: rule__LogicalExpressionDM__Group_6__3 : rule__LogicalExpressionDM__Group_6__3__Impl rule__LogicalExpressionDM__Group_6__4 ;
    public final void rule__LogicalExpressionDM__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4162:1: ( rule__LogicalExpressionDM__Group_6__3__Impl rule__LogicalExpressionDM__Group_6__4 )
            // InternalPDDL.g:4163:2: rule__LogicalExpressionDM__Group_6__3__Impl rule__LogicalExpressionDM__Group_6__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__3"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__3__Impl"
    // InternalPDDL.g:4170:1: rule__LogicalExpressionDM__Group_6__3__Impl : ( ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )* ) ) ;
    public final void rule__LogicalExpressionDM__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4174:1: ( ( ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )* ) ) )
            // InternalPDDL.g:4175:1: ( ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )* ) )
            {
            // InternalPDDL.g:4175:1: ( ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )* ) )
            // InternalPDDL.g:4176:2: ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )* )
            {
            // InternalPDDL.g:4176:2: ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 ) )
            // InternalPDDL.g:4177:3: ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeOr2Assignment_6_3()); 
            // InternalPDDL.g:4178:3: ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )
            // InternalPDDL.g:4178:4: rule__LogicalExpressionDM__LeOr2Assignment_6_3
            {
            pushFollow(FOLLOW_28);
            rule__LogicalExpressionDM__LeOr2Assignment_6_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeOr2Assignment_6_3()); 

            }

            // InternalPDDL.g:4181:2: ( ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )* )
            // InternalPDDL.g:4182:3: ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )*
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeOr2Assignment_6_3()); 
            // InternalPDDL.g:4183:3: ( rule__LogicalExpressionDM__LeOr2Assignment_6_3 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==RULE_OP||LA27_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalPDDL.g:4183:4: rule__LogicalExpressionDM__LeOr2Assignment_6_3
            	    {
            	    pushFollow(FOLLOW_28);
            	    rule__LogicalExpressionDM__LeOr2Assignment_6_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getLogicalExpressionDMAccess().getLeOr2Assignment_6_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__3__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__4"
    // InternalPDDL.g:4192:1: rule__LogicalExpressionDM__Group_6__4 : rule__LogicalExpressionDM__Group_6__4__Impl ;
    public final void rule__LogicalExpressionDM__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4196:1: ( rule__LogicalExpressionDM__Group_6__4__Impl )
            // InternalPDDL.g:4197:2: rule__LogicalExpressionDM__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__4"


    // $ANTLR start "rule__LogicalExpressionDM__Group_6__4__Impl"
    // InternalPDDL.g:4203:1: rule__LogicalExpressionDM__Group_6__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4207:1: ( ( RULE_CP ) )
            // InternalPDDL.g:4208:1: ( RULE_CP )
            {
            // InternalPDDL.g:4208:1: ( RULE_CP )
            // InternalPDDL.g:4209:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_6_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_6__4__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__0"
    // InternalPDDL.g:4219:1: rule__LogicalExpressionDM__Group_7__0 : rule__LogicalExpressionDM__Group_7__0__Impl rule__LogicalExpressionDM__Group_7__1 ;
    public final void rule__LogicalExpressionDM__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4223:1: ( rule__LogicalExpressionDM__Group_7__0__Impl rule__LogicalExpressionDM__Group_7__1 )
            // InternalPDDL.g:4224:2: rule__LogicalExpressionDM__Group_7__0__Impl rule__LogicalExpressionDM__Group_7__1
            {
            pushFollow(FOLLOW_32);
            rule__LogicalExpressionDM__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__0__Impl"
    // InternalPDDL.g:4231:1: rule__LogicalExpressionDM__Group_7__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4235:1: ( ( RULE_OP ) )
            // InternalPDDL.g:4236:1: ( RULE_OP )
            {
            // InternalPDDL.g:4236:1: ( RULE_OP )
            // InternalPDDL.g:4237:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_7_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__1"
    // InternalPDDL.g:4246:1: rule__LogicalExpressionDM__Group_7__1 : rule__LogicalExpressionDM__Group_7__1__Impl rule__LogicalExpressionDM__Group_7__2 ;
    public final void rule__LogicalExpressionDM__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4250:1: ( rule__LogicalExpressionDM__Group_7__1__Impl rule__LogicalExpressionDM__Group_7__2 )
            // InternalPDDL.g:4251:2: rule__LogicalExpressionDM__Group_7__1__Impl rule__LogicalExpressionDM__Group_7__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__1__Impl"
    // InternalPDDL.g:4258:1: rule__LogicalExpressionDM__Group_7__1__Impl : ( 'imply' ) ;
    public final void rule__LogicalExpressionDM__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4262:1: ( ( 'imply' ) )
            // InternalPDDL.g:4263:1: ( 'imply' )
            {
            // InternalPDDL.g:4263:1: ( 'imply' )
            // InternalPDDL.g:4264:2: 'imply'
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getImplyKeyword_7_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getImplyKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__2"
    // InternalPDDL.g:4273:1: rule__LogicalExpressionDM__Group_7__2 : rule__LogicalExpressionDM__Group_7__2__Impl rule__LogicalExpressionDM__Group_7__3 ;
    public final void rule__LogicalExpressionDM__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4277:1: ( rule__LogicalExpressionDM__Group_7__2__Impl rule__LogicalExpressionDM__Group_7__3 )
            // InternalPDDL.g:4278:2: rule__LogicalExpressionDM__Group_7__2__Impl rule__LogicalExpressionDM__Group_7__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_7__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_7__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__2__Impl"
    // InternalPDDL.g:4285:1: rule__LogicalExpressionDM__Group_7__2__Impl : ( ( rule__LogicalExpressionDM__LeImply1Assignment_7_2 ) ) ;
    public final void rule__LogicalExpressionDM__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4289:1: ( ( ( rule__LogicalExpressionDM__LeImply1Assignment_7_2 ) ) )
            // InternalPDDL.g:4290:1: ( ( rule__LogicalExpressionDM__LeImply1Assignment_7_2 ) )
            {
            // InternalPDDL.g:4290:1: ( ( rule__LogicalExpressionDM__LeImply1Assignment_7_2 ) )
            // InternalPDDL.g:4291:2: ( rule__LogicalExpressionDM__LeImply1Assignment_7_2 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeImply1Assignment_7_2()); 
            // InternalPDDL.g:4292:2: ( rule__LogicalExpressionDM__LeImply1Assignment_7_2 )
            // InternalPDDL.g:4292:3: rule__LogicalExpressionDM__LeImply1Assignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeImply1Assignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeImply1Assignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__3"
    // InternalPDDL.g:4300:1: rule__LogicalExpressionDM__Group_7__3 : rule__LogicalExpressionDM__Group_7__3__Impl rule__LogicalExpressionDM__Group_7__4 ;
    public final void rule__LogicalExpressionDM__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4304:1: ( rule__LogicalExpressionDM__Group_7__3__Impl rule__LogicalExpressionDM__Group_7__4 )
            // InternalPDDL.g:4305:2: rule__LogicalExpressionDM__Group_7__3__Impl rule__LogicalExpressionDM__Group_7__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_7__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_7__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__3"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__3__Impl"
    // InternalPDDL.g:4312:1: rule__LogicalExpressionDM__Group_7__3__Impl : ( ( rule__LogicalExpressionDM__LeImply2Assignment_7_3 ) ) ;
    public final void rule__LogicalExpressionDM__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4316:1: ( ( ( rule__LogicalExpressionDM__LeImply2Assignment_7_3 ) ) )
            // InternalPDDL.g:4317:1: ( ( rule__LogicalExpressionDM__LeImply2Assignment_7_3 ) )
            {
            // InternalPDDL.g:4317:1: ( ( rule__LogicalExpressionDM__LeImply2Assignment_7_3 ) )
            // InternalPDDL.g:4318:2: ( rule__LogicalExpressionDM__LeImply2Assignment_7_3 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeImply2Assignment_7_3()); 
            // InternalPDDL.g:4319:2: ( rule__LogicalExpressionDM__LeImply2Assignment_7_3 )
            // InternalPDDL.g:4319:3: rule__LogicalExpressionDM__LeImply2Assignment_7_3
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeImply2Assignment_7_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeImply2Assignment_7_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__3__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__4"
    // InternalPDDL.g:4327:1: rule__LogicalExpressionDM__Group_7__4 : rule__LogicalExpressionDM__Group_7__4__Impl ;
    public final void rule__LogicalExpressionDM__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4331:1: ( rule__LogicalExpressionDM__Group_7__4__Impl )
            // InternalPDDL.g:4332:2: rule__LogicalExpressionDM__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_7__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__4"


    // $ANTLR start "rule__LogicalExpressionDM__Group_7__4__Impl"
    // InternalPDDL.g:4338:1: rule__LogicalExpressionDM__Group_7__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4342:1: ( ( RULE_CP ) )
            // InternalPDDL.g:4343:1: ( RULE_CP )
            {
            // InternalPDDL.g:4343:1: ( RULE_CP )
            // InternalPDDL.g:4344:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_7_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_7_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_7__4__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__0"
    // InternalPDDL.g:4354:1: rule__LogicalExpressionDM__Group_8__0 : rule__LogicalExpressionDM__Group_8__0__Impl rule__LogicalExpressionDM__Group_8__1 ;
    public final void rule__LogicalExpressionDM__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4358:1: ( rule__LogicalExpressionDM__Group_8__0__Impl rule__LogicalExpressionDM__Group_8__1 )
            // InternalPDDL.g:4359:2: rule__LogicalExpressionDM__Group_8__0__Impl rule__LogicalExpressionDM__Group_8__1
            {
            pushFollow(FOLLOW_33);
            rule__LogicalExpressionDM__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__0__Impl"
    // InternalPDDL.g:4366:1: rule__LogicalExpressionDM__Group_8__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4370:1: ( ( RULE_OP ) )
            // InternalPDDL.g:4371:1: ( RULE_OP )
            {
            // InternalPDDL.g:4371:1: ( RULE_OP )
            // InternalPDDL.g:4372:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_8_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__1"
    // InternalPDDL.g:4381:1: rule__LogicalExpressionDM__Group_8__1 : rule__LogicalExpressionDM__Group_8__1__Impl rule__LogicalExpressionDM__Group_8__2 ;
    public final void rule__LogicalExpressionDM__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4385:1: ( rule__LogicalExpressionDM__Group_8__1__Impl rule__LogicalExpressionDM__Group_8__2 )
            // InternalPDDL.g:4386:2: rule__LogicalExpressionDM__Group_8__1__Impl rule__LogicalExpressionDM__Group_8__2
            {
            pushFollow(FOLLOW_7);
            rule__LogicalExpressionDM__Group_8__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_8__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__1__Impl"
    // InternalPDDL.g:4393:1: rule__LogicalExpressionDM__Group_8__1__Impl : ( 'forall' ) ;
    public final void rule__LogicalExpressionDM__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4397:1: ( ( 'forall' ) )
            // InternalPDDL.g:4398:1: ( 'forall' )
            {
            // InternalPDDL.g:4398:1: ( 'forall' )
            // InternalPDDL.g:4399:2: 'forall'
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getForallKeyword_8_1()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getForallKeyword_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__2"
    // InternalPDDL.g:4408:1: rule__LogicalExpressionDM__Group_8__2 : rule__LogicalExpressionDM__Group_8__2__Impl rule__LogicalExpressionDM__Group_8__3 ;
    public final void rule__LogicalExpressionDM__Group_8__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4412:1: ( rule__LogicalExpressionDM__Group_8__2__Impl rule__LogicalExpressionDM__Group_8__3 )
            // InternalPDDL.g:4413:2: rule__LogicalExpressionDM__Group_8__2__Impl rule__LogicalExpressionDM__Group_8__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_8__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_8__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__2__Impl"
    // InternalPDDL.g:4420:1: rule__LogicalExpressionDM__Group_8__2__Impl : ( ( rule__LogicalExpressionDM__TdForallAssignment_8_2 ) ) ;
    public final void rule__LogicalExpressionDM__Group_8__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4424:1: ( ( ( rule__LogicalExpressionDM__TdForallAssignment_8_2 ) ) )
            // InternalPDDL.g:4425:1: ( ( rule__LogicalExpressionDM__TdForallAssignment_8_2 ) )
            {
            // InternalPDDL.g:4425:1: ( ( rule__LogicalExpressionDM__TdForallAssignment_8_2 ) )
            // InternalPDDL.g:4426:2: ( rule__LogicalExpressionDM__TdForallAssignment_8_2 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getTdForallAssignment_8_2()); 
            // InternalPDDL.g:4427:2: ( rule__LogicalExpressionDM__TdForallAssignment_8_2 )
            // InternalPDDL.g:4427:3: rule__LogicalExpressionDM__TdForallAssignment_8_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__TdForallAssignment_8_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getTdForallAssignment_8_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__3"
    // InternalPDDL.g:4435:1: rule__LogicalExpressionDM__Group_8__3 : rule__LogicalExpressionDM__Group_8__3__Impl rule__LogicalExpressionDM__Group_8__4 ;
    public final void rule__LogicalExpressionDM__Group_8__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4439:1: ( rule__LogicalExpressionDM__Group_8__3__Impl rule__LogicalExpressionDM__Group_8__4 )
            // InternalPDDL.g:4440:2: rule__LogicalExpressionDM__Group_8__3__Impl rule__LogicalExpressionDM__Group_8__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_8__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_8__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__3"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__3__Impl"
    // InternalPDDL.g:4447:1: rule__LogicalExpressionDM__Group_8__3__Impl : ( ( rule__LogicalExpressionDM__LeForallAssignment_8_3 ) ) ;
    public final void rule__LogicalExpressionDM__Group_8__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4451:1: ( ( ( rule__LogicalExpressionDM__LeForallAssignment_8_3 ) ) )
            // InternalPDDL.g:4452:1: ( ( rule__LogicalExpressionDM__LeForallAssignment_8_3 ) )
            {
            // InternalPDDL.g:4452:1: ( ( rule__LogicalExpressionDM__LeForallAssignment_8_3 ) )
            // InternalPDDL.g:4453:2: ( rule__LogicalExpressionDM__LeForallAssignment_8_3 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeForallAssignment_8_3()); 
            // InternalPDDL.g:4454:2: ( rule__LogicalExpressionDM__LeForallAssignment_8_3 )
            // InternalPDDL.g:4454:3: rule__LogicalExpressionDM__LeForallAssignment_8_3
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeForallAssignment_8_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeForallAssignment_8_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__3__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__4"
    // InternalPDDL.g:4462:1: rule__LogicalExpressionDM__Group_8__4 : rule__LogicalExpressionDM__Group_8__4__Impl ;
    public final void rule__LogicalExpressionDM__Group_8__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4466:1: ( rule__LogicalExpressionDM__Group_8__4__Impl )
            // InternalPDDL.g:4467:2: rule__LogicalExpressionDM__Group_8__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_8__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__4"


    // $ANTLR start "rule__LogicalExpressionDM__Group_8__4__Impl"
    // InternalPDDL.g:4473:1: rule__LogicalExpressionDM__Group_8__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_8__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4477:1: ( ( RULE_CP ) )
            // InternalPDDL.g:4478:1: ( RULE_CP )
            {
            // InternalPDDL.g:4478:1: ( RULE_CP )
            // InternalPDDL.g:4479:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_8_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_8_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_8__4__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__0"
    // InternalPDDL.g:4489:1: rule__LogicalExpressionDM__Group_9__0 : rule__LogicalExpressionDM__Group_9__0__Impl rule__LogicalExpressionDM__Group_9__1 ;
    public final void rule__LogicalExpressionDM__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4493:1: ( rule__LogicalExpressionDM__Group_9__0__Impl rule__LogicalExpressionDM__Group_9__1 )
            // InternalPDDL.g:4494:2: rule__LogicalExpressionDM__Group_9__0__Impl rule__LogicalExpressionDM__Group_9__1
            {
            pushFollow(FOLLOW_34);
            rule__LogicalExpressionDM__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__0__Impl"
    // InternalPDDL.g:4501:1: rule__LogicalExpressionDM__Group_9__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4505:1: ( ( RULE_OP ) )
            // InternalPDDL.g:4506:1: ( RULE_OP )
            {
            // InternalPDDL.g:4506:1: ( RULE_OP )
            // InternalPDDL.g:4507:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_9_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__1"
    // InternalPDDL.g:4516:1: rule__LogicalExpressionDM__Group_9__1 : rule__LogicalExpressionDM__Group_9__1__Impl rule__LogicalExpressionDM__Group_9__2 ;
    public final void rule__LogicalExpressionDM__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4520:1: ( rule__LogicalExpressionDM__Group_9__1__Impl rule__LogicalExpressionDM__Group_9__2 )
            // InternalPDDL.g:4521:2: rule__LogicalExpressionDM__Group_9__1__Impl rule__LogicalExpressionDM__Group_9__2
            {
            pushFollow(FOLLOW_7);
            rule__LogicalExpressionDM__Group_9__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_9__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__1__Impl"
    // InternalPDDL.g:4528:1: rule__LogicalExpressionDM__Group_9__1__Impl : ( 'exists' ) ;
    public final void rule__LogicalExpressionDM__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4532:1: ( ( 'exists' ) )
            // InternalPDDL.g:4533:1: ( 'exists' )
            {
            // InternalPDDL.g:4533:1: ( 'exists' )
            // InternalPDDL.g:4534:2: 'exists'
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getExistsKeyword_9_1()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getExistsKeyword_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__2"
    // InternalPDDL.g:4543:1: rule__LogicalExpressionDM__Group_9__2 : rule__LogicalExpressionDM__Group_9__2__Impl rule__LogicalExpressionDM__Group_9__3 ;
    public final void rule__LogicalExpressionDM__Group_9__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4547:1: ( rule__LogicalExpressionDM__Group_9__2__Impl rule__LogicalExpressionDM__Group_9__3 )
            // InternalPDDL.g:4548:2: rule__LogicalExpressionDM__Group_9__2__Impl rule__LogicalExpressionDM__Group_9__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_9__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_9__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__2__Impl"
    // InternalPDDL.g:4555:1: rule__LogicalExpressionDM__Group_9__2__Impl : ( ( rule__LogicalExpressionDM__TdExistsAssignment_9_2 ) ) ;
    public final void rule__LogicalExpressionDM__Group_9__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4559:1: ( ( ( rule__LogicalExpressionDM__TdExistsAssignment_9_2 ) ) )
            // InternalPDDL.g:4560:1: ( ( rule__LogicalExpressionDM__TdExistsAssignment_9_2 ) )
            {
            // InternalPDDL.g:4560:1: ( ( rule__LogicalExpressionDM__TdExistsAssignment_9_2 ) )
            // InternalPDDL.g:4561:2: ( rule__LogicalExpressionDM__TdExistsAssignment_9_2 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getTdExistsAssignment_9_2()); 
            // InternalPDDL.g:4562:2: ( rule__LogicalExpressionDM__TdExistsAssignment_9_2 )
            // InternalPDDL.g:4562:3: rule__LogicalExpressionDM__TdExistsAssignment_9_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__TdExistsAssignment_9_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getTdExistsAssignment_9_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__3"
    // InternalPDDL.g:4570:1: rule__LogicalExpressionDM__Group_9__3 : rule__LogicalExpressionDM__Group_9__3__Impl rule__LogicalExpressionDM__Group_9__4 ;
    public final void rule__LogicalExpressionDM__Group_9__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4574:1: ( rule__LogicalExpressionDM__Group_9__3__Impl rule__LogicalExpressionDM__Group_9__4 )
            // InternalPDDL.g:4575:2: rule__LogicalExpressionDM__Group_9__3__Impl rule__LogicalExpressionDM__Group_9__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_9__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_9__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__3"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__3__Impl"
    // InternalPDDL.g:4582:1: rule__LogicalExpressionDM__Group_9__3__Impl : ( ( rule__LogicalExpressionDM__LeExistsAssignment_9_3 ) ) ;
    public final void rule__LogicalExpressionDM__Group_9__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4586:1: ( ( ( rule__LogicalExpressionDM__LeExistsAssignment_9_3 ) ) )
            // InternalPDDL.g:4587:1: ( ( rule__LogicalExpressionDM__LeExistsAssignment_9_3 ) )
            {
            // InternalPDDL.g:4587:1: ( ( rule__LogicalExpressionDM__LeExistsAssignment_9_3 ) )
            // InternalPDDL.g:4588:2: ( rule__LogicalExpressionDM__LeExistsAssignment_9_3 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeExistsAssignment_9_3()); 
            // InternalPDDL.g:4589:2: ( rule__LogicalExpressionDM__LeExistsAssignment_9_3 )
            // InternalPDDL.g:4589:3: rule__LogicalExpressionDM__LeExistsAssignment_9_3
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeExistsAssignment_9_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeExistsAssignment_9_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__3__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__4"
    // InternalPDDL.g:4597:1: rule__LogicalExpressionDM__Group_9__4 : rule__LogicalExpressionDM__Group_9__4__Impl ;
    public final void rule__LogicalExpressionDM__Group_9__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4601:1: ( rule__LogicalExpressionDM__Group_9__4__Impl )
            // InternalPDDL.g:4602:2: rule__LogicalExpressionDM__Group_9__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_9__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__4"


    // $ANTLR start "rule__LogicalExpressionDM__Group_9__4__Impl"
    // InternalPDDL.g:4608:1: rule__LogicalExpressionDM__Group_9__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_9__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4612:1: ( ( RULE_CP ) )
            // InternalPDDL.g:4613:1: ( RULE_CP )
            {
            // InternalPDDL.g:4613:1: ( RULE_CP )
            // InternalPDDL.g:4614:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_9_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_9_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_9__4__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__0"
    // InternalPDDL.g:4624:1: rule__LogicalExpressionDM__Group_10__0 : rule__LogicalExpressionDM__Group_10__0__Impl rule__LogicalExpressionDM__Group_10__1 ;
    public final void rule__LogicalExpressionDM__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4628:1: ( rule__LogicalExpressionDM__Group_10__0__Impl rule__LogicalExpressionDM__Group_10__1 )
            // InternalPDDL.g:4629:2: rule__LogicalExpressionDM__Group_10__0__Impl rule__LogicalExpressionDM__Group_10__1
            {
            pushFollow(FOLLOW_35);
            rule__LogicalExpressionDM__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__0"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__0__Impl"
    // InternalPDDL.g:4636:1: rule__LogicalExpressionDM__Group_10__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionDM__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4640:1: ( ( RULE_OP ) )
            // InternalPDDL.g:4641:1: ( RULE_OP )
            {
            // InternalPDDL.g:4641:1: ( RULE_OP )
            // InternalPDDL.g:4642:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_10_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__0__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__1"
    // InternalPDDL.g:4651:1: rule__LogicalExpressionDM__Group_10__1 : rule__LogicalExpressionDM__Group_10__1__Impl rule__LogicalExpressionDM__Group_10__2 ;
    public final void rule__LogicalExpressionDM__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4655:1: ( rule__LogicalExpressionDM__Group_10__1__Impl rule__LogicalExpressionDM__Group_10__2 )
            // InternalPDDL.g:4656:2: rule__LogicalExpressionDM__Group_10__1__Impl rule__LogicalExpressionDM__Group_10__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_10__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_10__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__1"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__1__Impl"
    // InternalPDDL.g:4663:1: rule__LogicalExpressionDM__Group_10__1__Impl : ( 'when' ) ;
    public final void rule__LogicalExpressionDM__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4667:1: ( ( 'when' ) )
            // InternalPDDL.g:4668:1: ( 'when' )
            {
            // InternalPDDL.g:4668:1: ( 'when' )
            // InternalPDDL.g:4669:2: 'when'
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getWhenKeyword_10_1()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getWhenKeyword_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__1__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__2"
    // InternalPDDL.g:4678:1: rule__LogicalExpressionDM__Group_10__2 : rule__LogicalExpressionDM__Group_10__2__Impl rule__LogicalExpressionDM__Group_10__3 ;
    public final void rule__LogicalExpressionDM__Group_10__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4682:1: ( rule__LogicalExpressionDM__Group_10__2__Impl rule__LogicalExpressionDM__Group_10__3 )
            // InternalPDDL.g:4683:2: rule__LogicalExpressionDM__Group_10__2__Impl rule__LogicalExpressionDM__Group_10__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionDM__Group_10__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_10__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__2"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__2__Impl"
    // InternalPDDL.g:4690:1: rule__LogicalExpressionDM__Group_10__2__Impl : ( ( rule__LogicalExpressionDM__LeWhen1Assignment_10_2 ) ) ;
    public final void rule__LogicalExpressionDM__Group_10__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4694:1: ( ( ( rule__LogicalExpressionDM__LeWhen1Assignment_10_2 ) ) )
            // InternalPDDL.g:4695:1: ( ( rule__LogicalExpressionDM__LeWhen1Assignment_10_2 ) )
            {
            // InternalPDDL.g:4695:1: ( ( rule__LogicalExpressionDM__LeWhen1Assignment_10_2 ) )
            // InternalPDDL.g:4696:2: ( rule__LogicalExpressionDM__LeWhen1Assignment_10_2 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeWhen1Assignment_10_2()); 
            // InternalPDDL.g:4697:2: ( rule__LogicalExpressionDM__LeWhen1Assignment_10_2 )
            // InternalPDDL.g:4697:3: rule__LogicalExpressionDM__LeWhen1Assignment_10_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeWhen1Assignment_10_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeWhen1Assignment_10_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__2__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__3"
    // InternalPDDL.g:4705:1: rule__LogicalExpressionDM__Group_10__3 : rule__LogicalExpressionDM__Group_10__3__Impl rule__LogicalExpressionDM__Group_10__4 ;
    public final void rule__LogicalExpressionDM__Group_10__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4709:1: ( rule__LogicalExpressionDM__Group_10__3__Impl rule__LogicalExpressionDM__Group_10__4 )
            // InternalPDDL.g:4710:2: rule__LogicalExpressionDM__Group_10__3__Impl rule__LogicalExpressionDM__Group_10__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionDM__Group_10__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_10__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__3"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__3__Impl"
    // InternalPDDL.g:4717:1: rule__LogicalExpressionDM__Group_10__3__Impl : ( ( rule__LogicalExpressionDM__LeWhen2Assignment_10_3 ) ) ;
    public final void rule__LogicalExpressionDM__Group_10__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4721:1: ( ( ( rule__LogicalExpressionDM__LeWhen2Assignment_10_3 ) ) )
            // InternalPDDL.g:4722:1: ( ( rule__LogicalExpressionDM__LeWhen2Assignment_10_3 ) )
            {
            // InternalPDDL.g:4722:1: ( ( rule__LogicalExpressionDM__LeWhen2Assignment_10_3 ) )
            // InternalPDDL.g:4723:2: ( rule__LogicalExpressionDM__LeWhen2Assignment_10_3 )
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeWhen2Assignment_10_3()); 
            // InternalPDDL.g:4724:2: ( rule__LogicalExpressionDM__LeWhen2Assignment_10_3 )
            // InternalPDDL.g:4724:3: rule__LogicalExpressionDM__LeWhen2Assignment_10_3
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__LeWhen2Assignment_10_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionDMAccess().getLeWhen2Assignment_10_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__3__Impl"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__4"
    // InternalPDDL.g:4732:1: rule__LogicalExpressionDM__Group_10__4 : rule__LogicalExpressionDM__Group_10__4__Impl ;
    public final void rule__LogicalExpressionDM__Group_10__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4736:1: ( rule__LogicalExpressionDM__Group_10__4__Impl )
            // InternalPDDL.g:4737:2: rule__LogicalExpressionDM__Group_10__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionDM__Group_10__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__4"


    // $ANTLR start "rule__LogicalExpressionDM__Group_10__4__Impl"
    // InternalPDDL.g:4743:1: rule__LogicalExpressionDM__Group_10__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionDM__Group_10__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4747:1: ( ( RULE_CP ) )
            // InternalPDDL.g:4748:1: ( RULE_CP )
            {
            // InternalPDDL.g:4748:1: ( RULE_CP )
            // InternalPDDL.g:4749:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_10_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_10_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__Group_10__4__Impl"


    // $ANTLR start "rule__PredicatDM__Group__0"
    // InternalPDDL.g:4759:1: rule__PredicatDM__Group__0 : rule__PredicatDM__Group__0__Impl rule__PredicatDM__Group__1 ;
    public final void rule__PredicatDM__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4763:1: ( rule__PredicatDM__Group__0__Impl rule__PredicatDM__Group__1 )
            // InternalPDDL.g:4764:2: rule__PredicatDM__Group__0__Impl rule__PredicatDM__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__PredicatDM__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PredicatDM__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatDM__Group__0"


    // $ANTLR start "rule__PredicatDM__Group__0__Impl"
    // InternalPDDL.g:4771:1: rule__PredicatDM__Group__0__Impl : ( ( rule__PredicatDM__PredicatNameAssignment_0 ) ) ;
    public final void rule__PredicatDM__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4775:1: ( ( ( rule__PredicatDM__PredicatNameAssignment_0 ) ) )
            // InternalPDDL.g:4776:1: ( ( rule__PredicatDM__PredicatNameAssignment_0 ) )
            {
            // InternalPDDL.g:4776:1: ( ( rule__PredicatDM__PredicatNameAssignment_0 ) )
            // InternalPDDL.g:4777:2: ( rule__PredicatDM__PredicatNameAssignment_0 )
            {
             before(grammarAccess.getPredicatDMAccess().getPredicatNameAssignment_0()); 
            // InternalPDDL.g:4778:2: ( rule__PredicatDM__PredicatNameAssignment_0 )
            // InternalPDDL.g:4778:3: rule__PredicatDM__PredicatNameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__PredicatDM__PredicatNameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPredicatDMAccess().getPredicatNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatDM__Group__0__Impl"


    // $ANTLR start "rule__PredicatDM__Group__1"
    // InternalPDDL.g:4786:1: rule__PredicatDM__Group__1 : rule__PredicatDM__Group__1__Impl ;
    public final void rule__PredicatDM__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4790:1: ( rule__PredicatDM__Group__1__Impl )
            // InternalPDDL.g:4791:2: rule__PredicatDM__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PredicatDM__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatDM__Group__1"


    // $ANTLR start "rule__PredicatDM__Group__1__Impl"
    // InternalPDDL.g:4797:1: rule__PredicatDM__Group__1__Impl : ( ( ( rule__PredicatDM__VariablesListAssignment_1 ) ) ( ( rule__PredicatDM__VariablesListAssignment_1 )* ) ) ;
    public final void rule__PredicatDM__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4801:1: ( ( ( ( rule__PredicatDM__VariablesListAssignment_1 ) ) ( ( rule__PredicatDM__VariablesListAssignment_1 )* ) ) )
            // InternalPDDL.g:4802:1: ( ( ( rule__PredicatDM__VariablesListAssignment_1 ) ) ( ( rule__PredicatDM__VariablesListAssignment_1 )* ) )
            {
            // InternalPDDL.g:4802:1: ( ( ( rule__PredicatDM__VariablesListAssignment_1 ) ) ( ( rule__PredicatDM__VariablesListAssignment_1 )* ) )
            // InternalPDDL.g:4803:2: ( ( rule__PredicatDM__VariablesListAssignment_1 ) ) ( ( rule__PredicatDM__VariablesListAssignment_1 )* )
            {
            // InternalPDDL.g:4803:2: ( ( rule__PredicatDM__VariablesListAssignment_1 ) )
            // InternalPDDL.g:4804:3: ( rule__PredicatDM__VariablesListAssignment_1 )
            {
             before(grammarAccess.getPredicatDMAccess().getVariablesListAssignment_1()); 
            // InternalPDDL.g:4805:3: ( rule__PredicatDM__VariablesListAssignment_1 )
            // InternalPDDL.g:4805:4: rule__PredicatDM__VariablesListAssignment_1
            {
            pushFollow(FOLLOW_6);
            rule__PredicatDM__VariablesListAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPredicatDMAccess().getVariablesListAssignment_1()); 

            }

            // InternalPDDL.g:4808:2: ( ( rule__PredicatDM__VariablesListAssignment_1 )* )
            // InternalPDDL.g:4809:3: ( rule__PredicatDM__VariablesListAssignment_1 )*
            {
             before(grammarAccess.getPredicatDMAccess().getVariablesListAssignment_1()); 
            // InternalPDDL.g:4810:3: ( rule__PredicatDM__VariablesListAssignment_1 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalPDDL.g:4810:4: rule__PredicatDM__VariablesListAssignment_1
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__PredicatDM__VariablesListAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getPredicatDMAccess().getVariablesListAssignment_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatDM__Group__1__Impl"


    // $ANTLR start "rule__TypeDescr__Group__0"
    // InternalPDDL.g:4820:1: rule__TypeDescr__Group__0 : rule__TypeDescr__Group__0__Impl rule__TypeDescr__Group__1 ;
    public final void rule__TypeDescr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4824:1: ( rule__TypeDescr__Group__0__Impl rule__TypeDescr__Group__1 )
            // InternalPDDL.g:4825:2: rule__TypeDescr__Group__0__Impl rule__TypeDescr__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__TypeDescr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeDescr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__0"


    // $ANTLR start "rule__TypeDescr__Group__0__Impl"
    // InternalPDDL.g:4832:1: rule__TypeDescr__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__TypeDescr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4836:1: ( ( RULE_OP ) )
            // InternalPDDL.g:4837:1: ( RULE_OP )
            {
            // InternalPDDL.g:4837:1: ( RULE_OP )
            // InternalPDDL.g:4838:2: RULE_OP
            {
             before(grammarAccess.getTypeDescrAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getTypeDescrAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__0__Impl"


    // $ANTLR start "rule__TypeDescr__Group__1"
    // InternalPDDL.g:4847:1: rule__TypeDescr__Group__1 : rule__TypeDescr__Group__1__Impl rule__TypeDescr__Group__2 ;
    public final void rule__TypeDescr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4851:1: ( rule__TypeDescr__Group__1__Impl rule__TypeDescr__Group__2 )
            // InternalPDDL.g:4852:2: rule__TypeDescr__Group__1__Impl rule__TypeDescr__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__TypeDescr__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeDescr__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__1"


    // $ANTLR start "rule__TypeDescr__Group__1__Impl"
    // InternalPDDL.g:4859:1: rule__TypeDescr__Group__1__Impl : ( ( rule__TypeDescr__VarAssignment_1 ) ) ;
    public final void rule__TypeDescr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4863:1: ( ( ( rule__TypeDescr__VarAssignment_1 ) ) )
            // InternalPDDL.g:4864:1: ( ( rule__TypeDescr__VarAssignment_1 ) )
            {
            // InternalPDDL.g:4864:1: ( ( rule__TypeDescr__VarAssignment_1 ) )
            // InternalPDDL.g:4865:2: ( rule__TypeDescr__VarAssignment_1 )
            {
             before(grammarAccess.getTypeDescrAccess().getVarAssignment_1()); 
            // InternalPDDL.g:4866:2: ( rule__TypeDescr__VarAssignment_1 )
            // InternalPDDL.g:4866:3: rule__TypeDescr__VarAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__TypeDescr__VarAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeDescrAccess().getVarAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__1__Impl"


    // $ANTLR start "rule__TypeDescr__Group__2"
    // InternalPDDL.g:4874:1: rule__TypeDescr__Group__2 : rule__TypeDescr__Group__2__Impl rule__TypeDescr__Group__3 ;
    public final void rule__TypeDescr__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4878:1: ( rule__TypeDescr__Group__2__Impl rule__TypeDescr__Group__3 )
            // InternalPDDL.g:4879:2: rule__TypeDescr__Group__2__Impl rule__TypeDescr__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__TypeDescr__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeDescr__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__2"


    // $ANTLR start "rule__TypeDescr__Group__2__Impl"
    // InternalPDDL.g:4886:1: rule__TypeDescr__Group__2__Impl : ( '-' ) ;
    public final void rule__TypeDescr__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4890:1: ( ( '-' ) )
            // InternalPDDL.g:4891:1: ( '-' )
            {
            // InternalPDDL.g:4891:1: ( '-' )
            // InternalPDDL.g:4892:2: '-'
            {
             before(grammarAccess.getTypeDescrAccess().getHyphenMinusKeyword_2()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getTypeDescrAccess().getHyphenMinusKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__2__Impl"


    // $ANTLR start "rule__TypeDescr__Group__3"
    // InternalPDDL.g:4901:1: rule__TypeDescr__Group__3 : rule__TypeDescr__Group__3__Impl rule__TypeDescr__Group__4 ;
    public final void rule__TypeDescr__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4905:1: ( rule__TypeDescr__Group__3__Impl rule__TypeDescr__Group__4 )
            // InternalPDDL.g:4906:2: rule__TypeDescr__Group__3__Impl rule__TypeDescr__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__TypeDescr__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeDescr__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__3"


    // $ANTLR start "rule__TypeDescr__Group__3__Impl"
    // InternalPDDL.g:4913:1: rule__TypeDescr__Group__3__Impl : ( ( rule__TypeDescr__TypeAssignment_3 ) ) ;
    public final void rule__TypeDescr__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4917:1: ( ( ( rule__TypeDescr__TypeAssignment_3 ) ) )
            // InternalPDDL.g:4918:1: ( ( rule__TypeDescr__TypeAssignment_3 ) )
            {
            // InternalPDDL.g:4918:1: ( ( rule__TypeDescr__TypeAssignment_3 ) )
            // InternalPDDL.g:4919:2: ( rule__TypeDescr__TypeAssignment_3 )
            {
             before(grammarAccess.getTypeDescrAccess().getTypeAssignment_3()); 
            // InternalPDDL.g:4920:2: ( rule__TypeDescr__TypeAssignment_3 )
            // InternalPDDL.g:4920:3: rule__TypeDescr__TypeAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__TypeDescr__TypeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTypeDescrAccess().getTypeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__3__Impl"


    // $ANTLR start "rule__TypeDescr__Group__4"
    // InternalPDDL.g:4928:1: rule__TypeDescr__Group__4 : rule__TypeDescr__Group__4__Impl ;
    public final void rule__TypeDescr__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4932:1: ( rule__TypeDescr__Group__4__Impl )
            // InternalPDDL.g:4933:2: rule__TypeDescr__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeDescr__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__4"


    // $ANTLR start "rule__TypeDescr__Group__4__Impl"
    // InternalPDDL.g:4939:1: rule__TypeDescr__Group__4__Impl : ( RULE_CP ) ;
    public final void rule__TypeDescr__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4943:1: ( ( RULE_CP ) )
            // InternalPDDL.g:4944:1: ( RULE_CP )
            {
            // InternalPDDL.g:4944:1: ( RULE_CP )
            // InternalPDDL.g:4945:2: RULE_CP
            {
             before(grammarAccess.getTypeDescrAccess().getCPTerminalRuleCall_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getTypeDescrAccess().getCPTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__Group__4__Impl"


    // $ANTLR start "rule__Axiom__Group__0"
    // InternalPDDL.g:4955:1: rule__Axiom__Group__0 : rule__Axiom__Group__0__Impl rule__Axiom__Group__1 ;
    public final void rule__Axiom__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4959:1: ( rule__Axiom__Group__0__Impl rule__Axiom__Group__1 )
            // InternalPDDL.g:4960:2: rule__Axiom__Group__0__Impl rule__Axiom__Group__1
            {
            pushFollow(FOLLOW_36);
            rule__Axiom__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Axiom__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__0"


    // $ANTLR start "rule__Axiom__Group__0__Impl"
    // InternalPDDL.g:4967:1: rule__Axiom__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__Axiom__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4971:1: ( ( RULE_OP ) )
            // InternalPDDL.g:4972:1: ( RULE_OP )
            {
            // InternalPDDL.g:4972:1: ( RULE_OP )
            // InternalPDDL.g:4973:2: RULE_OP
            {
             before(grammarAccess.getAxiomAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getAxiomAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__0__Impl"


    // $ANTLR start "rule__Axiom__Group__1"
    // InternalPDDL.g:4982:1: rule__Axiom__Group__1 : rule__Axiom__Group__1__Impl rule__Axiom__Group__2 ;
    public final void rule__Axiom__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4986:1: ( rule__Axiom__Group__1__Impl rule__Axiom__Group__2 )
            // InternalPDDL.g:4987:2: rule__Axiom__Group__1__Impl rule__Axiom__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__Axiom__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Axiom__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__1"


    // $ANTLR start "rule__Axiom__Group__1__Impl"
    // InternalPDDL.g:4994:1: rule__Axiom__Group__1__Impl : ( ':axiom' ) ;
    public final void rule__Axiom__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:4998:1: ( ( ':axiom' ) )
            // InternalPDDL.g:4999:1: ( ':axiom' )
            {
            // InternalPDDL.g:4999:1: ( ':axiom' )
            // InternalPDDL.g:5000:2: ':axiom'
            {
             before(grammarAccess.getAxiomAccess().getAxiomKeyword_1()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getAxiomAccess().getAxiomKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__1__Impl"


    // $ANTLR start "rule__Axiom__Group__2"
    // InternalPDDL.g:5009:1: rule__Axiom__Group__2 : rule__Axiom__Group__2__Impl rule__Axiom__Group__3 ;
    public final void rule__Axiom__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5013:1: ( rule__Axiom__Group__2__Impl rule__Axiom__Group__3 )
            // InternalPDDL.g:5014:2: rule__Axiom__Group__2__Impl rule__Axiom__Group__3
            {
            pushFollow(FOLLOW_37);
            rule__Axiom__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Axiom__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__2"


    // $ANTLR start "rule__Axiom__Group__2__Impl"
    // InternalPDDL.g:5021:1: rule__Axiom__Group__2__Impl : ( ( rule__Axiom__AxiomNameAssignment_2 ) ) ;
    public final void rule__Axiom__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5025:1: ( ( ( rule__Axiom__AxiomNameAssignment_2 ) ) )
            // InternalPDDL.g:5026:1: ( ( rule__Axiom__AxiomNameAssignment_2 ) )
            {
            // InternalPDDL.g:5026:1: ( ( rule__Axiom__AxiomNameAssignment_2 ) )
            // InternalPDDL.g:5027:2: ( rule__Axiom__AxiomNameAssignment_2 )
            {
             before(grammarAccess.getAxiomAccess().getAxiomNameAssignment_2()); 
            // InternalPDDL.g:5028:2: ( rule__Axiom__AxiomNameAssignment_2 )
            // InternalPDDL.g:5028:3: rule__Axiom__AxiomNameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Axiom__AxiomNameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAxiomAccess().getAxiomNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__2__Impl"


    // $ANTLR start "rule__Axiom__Group__3"
    // InternalPDDL.g:5036:1: rule__Axiom__Group__3 : rule__Axiom__Group__3__Impl rule__Axiom__Group__4 ;
    public final void rule__Axiom__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5040:1: ( rule__Axiom__Group__3__Impl rule__Axiom__Group__4 )
            // InternalPDDL.g:5041:2: rule__Axiom__Group__3__Impl rule__Axiom__Group__4
            {
            pushFollow(FOLLOW_38);
            rule__Axiom__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Axiom__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__3"


    // $ANTLR start "rule__Axiom__Group__3__Impl"
    // InternalPDDL.g:5048:1: rule__Axiom__Group__3__Impl : ( ( rule__Axiom__VarsAssignment_3 ) ) ;
    public final void rule__Axiom__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5052:1: ( ( ( rule__Axiom__VarsAssignment_3 ) ) )
            // InternalPDDL.g:5053:1: ( ( rule__Axiom__VarsAssignment_3 ) )
            {
            // InternalPDDL.g:5053:1: ( ( rule__Axiom__VarsAssignment_3 ) )
            // InternalPDDL.g:5054:2: ( rule__Axiom__VarsAssignment_3 )
            {
             before(grammarAccess.getAxiomAccess().getVarsAssignment_3()); 
            // InternalPDDL.g:5055:2: ( rule__Axiom__VarsAssignment_3 )
            // InternalPDDL.g:5055:3: rule__Axiom__VarsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Axiom__VarsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAxiomAccess().getVarsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__3__Impl"


    // $ANTLR start "rule__Axiom__Group__4"
    // InternalPDDL.g:5063:1: rule__Axiom__Group__4 : rule__Axiom__Group__4__Impl rule__Axiom__Group__5 ;
    public final void rule__Axiom__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5067:1: ( rule__Axiom__Group__4__Impl rule__Axiom__Group__5 )
            // InternalPDDL.g:5068:2: rule__Axiom__Group__4__Impl rule__Axiom__Group__5
            {
            pushFollow(FOLLOW_39);
            rule__Axiom__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Axiom__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__4"


    // $ANTLR start "rule__Axiom__Group__4__Impl"
    // InternalPDDL.g:5075:1: rule__Axiom__Group__4__Impl : ( ( rule__Axiom__ContextAssignment_4 ) ) ;
    public final void rule__Axiom__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5079:1: ( ( ( rule__Axiom__ContextAssignment_4 ) ) )
            // InternalPDDL.g:5080:1: ( ( rule__Axiom__ContextAssignment_4 ) )
            {
            // InternalPDDL.g:5080:1: ( ( rule__Axiom__ContextAssignment_4 ) )
            // InternalPDDL.g:5081:2: ( rule__Axiom__ContextAssignment_4 )
            {
             before(grammarAccess.getAxiomAccess().getContextAssignment_4()); 
            // InternalPDDL.g:5082:2: ( rule__Axiom__ContextAssignment_4 )
            // InternalPDDL.g:5082:3: rule__Axiom__ContextAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Axiom__ContextAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getAxiomAccess().getContextAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__4__Impl"


    // $ANTLR start "rule__Axiom__Group__5"
    // InternalPDDL.g:5090:1: rule__Axiom__Group__5 : rule__Axiom__Group__5__Impl rule__Axiom__Group__6 ;
    public final void rule__Axiom__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5094:1: ( rule__Axiom__Group__5__Impl rule__Axiom__Group__6 )
            // InternalPDDL.g:5095:2: rule__Axiom__Group__5__Impl rule__Axiom__Group__6
            {
            pushFollow(FOLLOW_9);
            rule__Axiom__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Axiom__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__5"


    // $ANTLR start "rule__Axiom__Group__5__Impl"
    // InternalPDDL.g:5102:1: rule__Axiom__Group__5__Impl : ( ( rule__Axiom__ImpliesAssignment_5 ) ) ;
    public final void rule__Axiom__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5106:1: ( ( ( rule__Axiom__ImpliesAssignment_5 ) ) )
            // InternalPDDL.g:5107:1: ( ( rule__Axiom__ImpliesAssignment_5 ) )
            {
            // InternalPDDL.g:5107:1: ( ( rule__Axiom__ImpliesAssignment_5 ) )
            // InternalPDDL.g:5108:2: ( rule__Axiom__ImpliesAssignment_5 )
            {
             before(grammarAccess.getAxiomAccess().getImpliesAssignment_5()); 
            // InternalPDDL.g:5109:2: ( rule__Axiom__ImpliesAssignment_5 )
            // InternalPDDL.g:5109:3: rule__Axiom__ImpliesAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Axiom__ImpliesAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getAxiomAccess().getImpliesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__5__Impl"


    // $ANTLR start "rule__Axiom__Group__6"
    // InternalPDDL.g:5117:1: rule__Axiom__Group__6 : rule__Axiom__Group__6__Impl ;
    public final void rule__Axiom__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5121:1: ( rule__Axiom__Group__6__Impl )
            // InternalPDDL.g:5122:2: rule__Axiom__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Axiom__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__6"


    // $ANTLR start "rule__Axiom__Group__6__Impl"
    // InternalPDDL.g:5128:1: rule__Axiom__Group__6__Impl : ( RULE_CP ) ;
    public final void rule__Axiom__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5132:1: ( ( RULE_CP ) )
            // InternalPDDL.g:5133:1: ( RULE_CP )
            {
            // InternalPDDL.g:5133:1: ( RULE_CP )
            // InternalPDDL.g:5134:2: RULE_CP
            {
             before(grammarAccess.getAxiomAccess().getCPTerminalRuleCall_6()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getAxiomAccess().getCPTerminalRuleCall_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__Group__6__Impl"


    // $ANTLR start "rule__Vars__Group__0"
    // InternalPDDL.g:5144:1: rule__Vars__Group__0 : rule__Vars__Group__0__Impl rule__Vars__Group__1 ;
    public final void rule__Vars__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5148:1: ( rule__Vars__Group__0__Impl rule__Vars__Group__1 )
            // InternalPDDL.g:5149:2: rule__Vars__Group__0__Impl rule__Vars__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Vars__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Vars__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vars__Group__0"


    // $ANTLR start "rule__Vars__Group__0__Impl"
    // InternalPDDL.g:5156:1: rule__Vars__Group__0__Impl : ( ':vars' ) ;
    public final void rule__Vars__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5160:1: ( ( ':vars' ) )
            // InternalPDDL.g:5161:1: ( ':vars' )
            {
            // InternalPDDL.g:5161:1: ( ':vars' )
            // InternalPDDL.g:5162:2: ':vars'
            {
             before(grammarAccess.getVarsAccess().getVarsKeyword_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getVarsAccess().getVarsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vars__Group__0__Impl"


    // $ANTLR start "rule__Vars__Group__1"
    // InternalPDDL.g:5171:1: rule__Vars__Group__1 : rule__Vars__Group__1__Impl rule__Vars__Group__2 ;
    public final void rule__Vars__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5175:1: ( rule__Vars__Group__1__Impl rule__Vars__Group__2 )
            // InternalPDDL.g:5176:2: rule__Vars__Group__1__Impl rule__Vars__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__Vars__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Vars__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vars__Group__1"


    // $ANTLR start "rule__Vars__Group__1__Impl"
    // InternalPDDL.g:5183:1: rule__Vars__Group__1__Impl : ( RULE_OP ) ;
    public final void rule__Vars__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5187:1: ( ( RULE_OP ) )
            // InternalPDDL.g:5188:1: ( RULE_OP )
            {
            // InternalPDDL.g:5188:1: ( RULE_OP )
            // InternalPDDL.g:5189:2: RULE_OP
            {
             before(grammarAccess.getVarsAccess().getOPTerminalRuleCall_1()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getVarsAccess().getOPTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vars__Group__1__Impl"


    // $ANTLR start "rule__Vars__Group__2"
    // InternalPDDL.g:5198:1: rule__Vars__Group__2 : rule__Vars__Group__2__Impl rule__Vars__Group__3 ;
    public final void rule__Vars__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5202:1: ( rule__Vars__Group__2__Impl rule__Vars__Group__3 )
            // InternalPDDL.g:5203:2: rule__Vars__Group__2__Impl rule__Vars__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Vars__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Vars__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vars__Group__2"


    // $ANTLR start "rule__Vars__Group__2__Impl"
    // InternalPDDL.g:5210:1: rule__Vars__Group__2__Impl : ( ruleTypedVariablesList ) ;
    public final void rule__Vars__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5214:1: ( ( ruleTypedVariablesList ) )
            // InternalPDDL.g:5215:1: ( ruleTypedVariablesList )
            {
            // InternalPDDL.g:5215:1: ( ruleTypedVariablesList )
            // InternalPDDL.g:5216:2: ruleTypedVariablesList
            {
             before(grammarAccess.getVarsAccess().getTypedVariablesListParserRuleCall_2()); 
            pushFollow(FOLLOW_2);
            ruleTypedVariablesList();

            state._fsp--;

             after(grammarAccess.getVarsAccess().getTypedVariablesListParserRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vars__Group__2__Impl"


    // $ANTLR start "rule__Vars__Group__3"
    // InternalPDDL.g:5225:1: rule__Vars__Group__3 : rule__Vars__Group__3__Impl ;
    public final void rule__Vars__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5229:1: ( rule__Vars__Group__3__Impl )
            // InternalPDDL.g:5230:2: rule__Vars__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Vars__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vars__Group__3"


    // $ANTLR start "rule__Vars__Group__3__Impl"
    // InternalPDDL.g:5236:1: rule__Vars__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Vars__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5240:1: ( ( RULE_CP ) )
            // InternalPDDL.g:5241:1: ( RULE_CP )
            {
            // InternalPDDL.g:5241:1: ( RULE_CP )
            // InternalPDDL.g:5242:2: RULE_CP
            {
             before(grammarAccess.getVarsAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getVarsAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vars__Group__3__Impl"


    // $ANTLR start "rule__Context__Group__0"
    // InternalPDDL.g:5252:1: rule__Context__Group__0 : rule__Context__Group__0__Impl rule__Context__Group__1 ;
    public final void rule__Context__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5256:1: ( rule__Context__Group__0__Impl rule__Context__Group__1 )
            // InternalPDDL.g:5257:2: rule__Context__Group__0__Impl rule__Context__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Context__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Context__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context__Group__0"


    // $ANTLR start "rule__Context__Group__0__Impl"
    // InternalPDDL.g:5264:1: rule__Context__Group__0__Impl : ( ':context' ) ;
    public final void rule__Context__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5268:1: ( ( ':context' ) )
            // InternalPDDL.g:5269:1: ( ':context' )
            {
            // InternalPDDL.g:5269:1: ( ':context' )
            // InternalPDDL.g:5270:2: ':context'
            {
             before(grammarAccess.getContextAccess().getContextKeyword_0()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getContextAccess().getContextKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context__Group__0__Impl"


    // $ANTLR start "rule__Context__Group__1"
    // InternalPDDL.g:5279:1: rule__Context__Group__1 : rule__Context__Group__1__Impl rule__Context__Group__2 ;
    public final void rule__Context__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5283:1: ( rule__Context__Group__1__Impl rule__Context__Group__2 )
            // InternalPDDL.g:5284:2: rule__Context__Group__1__Impl rule__Context__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__Context__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Context__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context__Group__1"


    // $ANTLR start "rule__Context__Group__1__Impl"
    // InternalPDDL.g:5291:1: rule__Context__Group__1__Impl : ( RULE_OP ) ;
    public final void rule__Context__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5295:1: ( ( RULE_OP ) )
            // InternalPDDL.g:5296:1: ( RULE_OP )
            {
            // InternalPDDL.g:5296:1: ( RULE_OP )
            // InternalPDDL.g:5297:2: RULE_OP
            {
             before(grammarAccess.getContextAccess().getOPTerminalRuleCall_1()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getContextAccess().getOPTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context__Group__1__Impl"


    // $ANTLR start "rule__Context__Group__2"
    // InternalPDDL.g:5306:1: rule__Context__Group__2 : rule__Context__Group__2__Impl rule__Context__Group__3 ;
    public final void rule__Context__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5310:1: ( rule__Context__Group__2__Impl rule__Context__Group__3 )
            // InternalPDDL.g:5311:2: rule__Context__Group__2__Impl rule__Context__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Context__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Context__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context__Group__2"


    // $ANTLR start "rule__Context__Group__2__Impl"
    // InternalPDDL.g:5318:1: rule__Context__Group__2__Impl : ( ( rule__Context__LeAssignment_2 ) ) ;
    public final void rule__Context__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5322:1: ( ( ( rule__Context__LeAssignment_2 ) ) )
            // InternalPDDL.g:5323:1: ( ( rule__Context__LeAssignment_2 ) )
            {
            // InternalPDDL.g:5323:1: ( ( rule__Context__LeAssignment_2 ) )
            // InternalPDDL.g:5324:2: ( rule__Context__LeAssignment_2 )
            {
             before(grammarAccess.getContextAccess().getLeAssignment_2()); 
            // InternalPDDL.g:5325:2: ( rule__Context__LeAssignment_2 )
            // InternalPDDL.g:5325:3: rule__Context__LeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Context__LeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getContextAccess().getLeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context__Group__2__Impl"


    // $ANTLR start "rule__Context__Group__3"
    // InternalPDDL.g:5333:1: rule__Context__Group__3 : rule__Context__Group__3__Impl ;
    public final void rule__Context__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5337:1: ( rule__Context__Group__3__Impl )
            // InternalPDDL.g:5338:2: rule__Context__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Context__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context__Group__3"


    // $ANTLR start "rule__Context__Group__3__Impl"
    // InternalPDDL.g:5344:1: rule__Context__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Context__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5348:1: ( ( RULE_CP ) )
            // InternalPDDL.g:5349:1: ( RULE_CP )
            {
            // InternalPDDL.g:5349:1: ( RULE_CP )
            // InternalPDDL.g:5350:2: RULE_CP
            {
             before(grammarAccess.getContextAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getContextAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context__Group__3__Impl"


    // $ANTLR start "rule__Implies__Group__0"
    // InternalPDDL.g:5360:1: rule__Implies__Group__0 : rule__Implies__Group__0__Impl rule__Implies__Group__1 ;
    public final void rule__Implies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5364:1: ( rule__Implies__Group__0__Impl rule__Implies__Group__1 )
            // InternalPDDL.g:5365:2: rule__Implies__Group__0__Impl rule__Implies__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Implies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0"


    // $ANTLR start "rule__Implies__Group__0__Impl"
    // InternalPDDL.g:5372:1: rule__Implies__Group__0__Impl : ( ':implies' ) ;
    public final void rule__Implies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5376:1: ( ( ':implies' ) )
            // InternalPDDL.g:5377:1: ( ':implies' )
            {
            // InternalPDDL.g:5377:1: ( ':implies' )
            // InternalPDDL.g:5378:2: ':implies'
            {
             before(grammarAccess.getImpliesAccess().getImpliesKeyword_0()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getImpliesKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0__Impl"


    // $ANTLR start "rule__Implies__Group__1"
    // InternalPDDL.g:5387:1: rule__Implies__Group__1 : rule__Implies__Group__1__Impl rule__Implies__Group__2 ;
    public final void rule__Implies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5391:1: ( rule__Implies__Group__1__Impl rule__Implies__Group__2 )
            // InternalPDDL.g:5392:2: rule__Implies__Group__1__Impl rule__Implies__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__Implies__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1"


    // $ANTLR start "rule__Implies__Group__1__Impl"
    // InternalPDDL.g:5399:1: rule__Implies__Group__1__Impl : ( RULE_OP ) ;
    public final void rule__Implies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5403:1: ( ( RULE_OP ) )
            // InternalPDDL.g:5404:1: ( RULE_OP )
            {
            // InternalPDDL.g:5404:1: ( RULE_OP )
            // InternalPDDL.g:5405:2: RULE_OP
            {
             before(grammarAccess.getImpliesAccess().getOPTerminalRuleCall_1()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getOPTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1__Impl"


    // $ANTLR start "rule__Implies__Group__2"
    // InternalPDDL.g:5414:1: rule__Implies__Group__2 : rule__Implies__Group__2__Impl rule__Implies__Group__3 ;
    public final void rule__Implies__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5418:1: ( rule__Implies__Group__2__Impl rule__Implies__Group__3 )
            // InternalPDDL.g:5419:2: rule__Implies__Group__2__Impl rule__Implies__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Implies__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__2"


    // $ANTLR start "rule__Implies__Group__2__Impl"
    // InternalPDDL.g:5426:1: rule__Implies__Group__2__Impl : ( ( rule__Implies__LeAssignment_2 ) ) ;
    public final void rule__Implies__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5430:1: ( ( ( rule__Implies__LeAssignment_2 ) ) )
            // InternalPDDL.g:5431:1: ( ( rule__Implies__LeAssignment_2 ) )
            {
            // InternalPDDL.g:5431:1: ( ( rule__Implies__LeAssignment_2 ) )
            // InternalPDDL.g:5432:2: ( rule__Implies__LeAssignment_2 )
            {
             before(grammarAccess.getImpliesAccess().getLeAssignment_2()); 
            // InternalPDDL.g:5433:2: ( rule__Implies__LeAssignment_2 )
            // InternalPDDL.g:5433:3: rule__Implies__LeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Implies__LeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getLeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__2__Impl"


    // $ANTLR start "rule__Implies__Group__3"
    // InternalPDDL.g:5441:1: rule__Implies__Group__3 : rule__Implies__Group__3__Impl ;
    public final void rule__Implies__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5445:1: ( rule__Implies__Group__3__Impl )
            // InternalPDDL.g:5446:2: rule__Implies__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__3"


    // $ANTLR start "rule__Implies__Group__3__Impl"
    // InternalPDDL.g:5452:1: rule__Implies__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Implies__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5456:1: ( ( RULE_CP ) )
            // InternalPDDL.g:5457:1: ( RULE_CP )
            {
            // InternalPDDL.g:5457:1: ( RULE_CP )
            // InternalPDDL.g:5458:2: RULE_CP
            {
             before(grammarAccess.getImpliesAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__3__Impl"


    // $ANTLR start "rule__Problem__Group__0"
    // InternalPDDL.g:5468:1: rule__Problem__Group__0 : rule__Problem__Group__0__Impl rule__Problem__Group__1 ;
    public final void rule__Problem__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5472:1: ( rule__Problem__Group__0__Impl rule__Problem__Group__1 )
            // InternalPDDL.g:5473:2: rule__Problem__Group__0__Impl rule__Problem__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Problem__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Problem__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__Group__0"


    // $ANTLR start "rule__Problem__Group__0__Impl"
    // InternalPDDL.g:5480:1: rule__Problem__Group__0__Impl : ( ( rule__Problem__DomainRefAssignment_0 ) ) ;
    public final void rule__Problem__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5484:1: ( ( ( rule__Problem__DomainRefAssignment_0 ) ) )
            // InternalPDDL.g:5485:1: ( ( rule__Problem__DomainRefAssignment_0 ) )
            {
            // InternalPDDL.g:5485:1: ( ( rule__Problem__DomainRefAssignment_0 ) )
            // InternalPDDL.g:5486:2: ( rule__Problem__DomainRefAssignment_0 )
            {
             before(grammarAccess.getProblemAccess().getDomainRefAssignment_0()); 
            // InternalPDDL.g:5487:2: ( rule__Problem__DomainRefAssignment_0 )
            // InternalPDDL.g:5487:3: rule__Problem__DomainRefAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Problem__DomainRefAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getProblemAccess().getDomainRefAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__Group__0__Impl"


    // $ANTLR start "rule__Problem__Group__1"
    // InternalPDDL.g:5495:1: rule__Problem__Group__1 : rule__Problem__Group__1__Impl rule__Problem__Group__2 ;
    public final void rule__Problem__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5499:1: ( rule__Problem__Group__1__Impl rule__Problem__Group__2 )
            // InternalPDDL.g:5500:2: rule__Problem__Group__1__Impl rule__Problem__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Problem__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Problem__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__Group__1"


    // $ANTLR start "rule__Problem__Group__1__Impl"
    // InternalPDDL.g:5507:1: rule__Problem__Group__1__Impl : ( ( rule__Problem__ObjectsAssignment_1 )? ) ;
    public final void rule__Problem__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5511:1: ( ( ( rule__Problem__ObjectsAssignment_1 )? ) )
            // InternalPDDL.g:5512:1: ( ( rule__Problem__ObjectsAssignment_1 )? )
            {
            // InternalPDDL.g:5512:1: ( ( rule__Problem__ObjectsAssignment_1 )? )
            // InternalPDDL.g:5513:2: ( rule__Problem__ObjectsAssignment_1 )?
            {
             before(grammarAccess.getProblemAccess().getObjectsAssignment_1()); 
            // InternalPDDL.g:5514:2: ( rule__Problem__ObjectsAssignment_1 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==RULE_OP) ) {
                int LA29_1 = input.LA(2);

                if ( (LA29_1==45) ) {
                    alt29=1;
                }
            }
            switch (alt29) {
                case 1 :
                    // InternalPDDL.g:5514:3: rule__Problem__ObjectsAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Problem__ObjectsAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getProblemAccess().getObjectsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__Group__1__Impl"


    // $ANTLR start "rule__Problem__Group__2"
    // InternalPDDL.g:5522:1: rule__Problem__Group__2 : rule__Problem__Group__2__Impl rule__Problem__Group__3 ;
    public final void rule__Problem__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5526:1: ( rule__Problem__Group__2__Impl rule__Problem__Group__3 )
            // InternalPDDL.g:5527:2: rule__Problem__Group__2__Impl rule__Problem__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Problem__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Problem__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__Group__2"


    // $ANTLR start "rule__Problem__Group__2__Impl"
    // InternalPDDL.g:5534:1: rule__Problem__Group__2__Impl : ( ( rule__Problem__InitAssignment_2 )? ) ;
    public final void rule__Problem__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5538:1: ( ( ( rule__Problem__InitAssignment_2 )? ) )
            // InternalPDDL.g:5539:1: ( ( rule__Problem__InitAssignment_2 )? )
            {
            // InternalPDDL.g:5539:1: ( ( rule__Problem__InitAssignment_2 )? )
            // InternalPDDL.g:5540:2: ( rule__Problem__InitAssignment_2 )?
            {
             before(grammarAccess.getProblemAccess().getInitAssignment_2()); 
            // InternalPDDL.g:5541:2: ( rule__Problem__InitAssignment_2 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==RULE_OP) ) {
                int LA30_1 = input.LA(2);

                if ( (LA30_1==46) ) {
                    alt30=1;
                }
            }
            switch (alt30) {
                case 1 :
                    // InternalPDDL.g:5541:3: rule__Problem__InitAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Problem__InitAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getProblemAccess().getInitAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__Group__2__Impl"


    // $ANTLR start "rule__Problem__Group__3"
    // InternalPDDL.g:5549:1: rule__Problem__Group__3 : rule__Problem__Group__3__Impl ;
    public final void rule__Problem__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5553:1: ( rule__Problem__Group__3__Impl )
            // InternalPDDL.g:5554:2: rule__Problem__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Problem__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__Group__3"


    // $ANTLR start "rule__Problem__Group__3__Impl"
    // InternalPDDL.g:5560:1: rule__Problem__Group__3__Impl : ( ( rule__Problem__GoalAssignment_3 )? ) ;
    public final void rule__Problem__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5564:1: ( ( ( rule__Problem__GoalAssignment_3 )? ) )
            // InternalPDDL.g:5565:1: ( ( rule__Problem__GoalAssignment_3 )? )
            {
            // InternalPDDL.g:5565:1: ( ( rule__Problem__GoalAssignment_3 )? )
            // InternalPDDL.g:5566:2: ( rule__Problem__GoalAssignment_3 )?
            {
             before(grammarAccess.getProblemAccess().getGoalAssignment_3()); 
            // InternalPDDL.g:5567:2: ( rule__Problem__GoalAssignment_3 )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==RULE_OP) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalPDDL.g:5567:3: rule__Problem__GoalAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Problem__GoalAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getProblemAccess().getGoalAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__Group__3__Impl"


    // $ANTLR start "rule__DomainRef__Group__0"
    // InternalPDDL.g:5576:1: rule__DomainRef__Group__0 : rule__DomainRef__Group__0__Impl rule__DomainRef__Group__1 ;
    public final void rule__DomainRef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5580:1: ( rule__DomainRef__Group__0__Impl rule__DomainRef__Group__1 )
            // InternalPDDL.g:5581:2: rule__DomainRef__Group__0__Impl rule__DomainRef__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__DomainRef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DomainRef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DomainRef__Group__0"


    // $ANTLR start "rule__DomainRef__Group__0__Impl"
    // InternalPDDL.g:5588:1: rule__DomainRef__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__DomainRef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5592:1: ( ( RULE_OP ) )
            // InternalPDDL.g:5593:1: ( RULE_OP )
            {
            // InternalPDDL.g:5593:1: ( RULE_OP )
            // InternalPDDL.g:5594:2: RULE_OP
            {
             before(grammarAccess.getDomainRefAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getDomainRefAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DomainRef__Group__0__Impl"


    // $ANTLR start "rule__DomainRef__Group__1"
    // InternalPDDL.g:5603:1: rule__DomainRef__Group__1 : rule__DomainRef__Group__1__Impl rule__DomainRef__Group__2 ;
    public final void rule__DomainRef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5607:1: ( rule__DomainRef__Group__1__Impl rule__DomainRef__Group__2 )
            // InternalPDDL.g:5608:2: rule__DomainRef__Group__1__Impl rule__DomainRef__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__DomainRef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DomainRef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DomainRef__Group__1"


    // $ANTLR start "rule__DomainRef__Group__1__Impl"
    // InternalPDDL.g:5615:1: rule__DomainRef__Group__1__Impl : ( ':domain' ) ;
    public final void rule__DomainRef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5619:1: ( ( ':domain' ) )
            // InternalPDDL.g:5620:1: ( ':domain' )
            {
            // InternalPDDL.g:5620:1: ( ':domain' )
            // InternalPDDL.g:5621:2: ':domain'
            {
             before(grammarAccess.getDomainRefAccess().getDomainKeyword_1()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getDomainRefAccess().getDomainKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DomainRef__Group__1__Impl"


    // $ANTLR start "rule__DomainRef__Group__2"
    // InternalPDDL.g:5630:1: rule__DomainRef__Group__2 : rule__DomainRef__Group__2__Impl rule__DomainRef__Group__3 ;
    public final void rule__DomainRef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5634:1: ( rule__DomainRef__Group__2__Impl rule__DomainRef__Group__3 )
            // InternalPDDL.g:5635:2: rule__DomainRef__Group__2__Impl rule__DomainRef__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__DomainRef__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DomainRef__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DomainRef__Group__2"


    // $ANTLR start "rule__DomainRef__Group__2__Impl"
    // InternalPDDL.g:5642:1: rule__DomainRef__Group__2__Impl : ( ( rule__DomainRef__DomainNameAssignment_2 ) ) ;
    public final void rule__DomainRef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5646:1: ( ( ( rule__DomainRef__DomainNameAssignment_2 ) ) )
            // InternalPDDL.g:5647:1: ( ( rule__DomainRef__DomainNameAssignment_2 ) )
            {
            // InternalPDDL.g:5647:1: ( ( rule__DomainRef__DomainNameAssignment_2 ) )
            // InternalPDDL.g:5648:2: ( rule__DomainRef__DomainNameAssignment_2 )
            {
             before(grammarAccess.getDomainRefAccess().getDomainNameAssignment_2()); 
            // InternalPDDL.g:5649:2: ( rule__DomainRef__DomainNameAssignment_2 )
            // InternalPDDL.g:5649:3: rule__DomainRef__DomainNameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__DomainRef__DomainNameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDomainRefAccess().getDomainNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DomainRef__Group__2__Impl"


    // $ANTLR start "rule__DomainRef__Group__3"
    // InternalPDDL.g:5657:1: rule__DomainRef__Group__3 : rule__DomainRef__Group__3__Impl ;
    public final void rule__DomainRef__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5661:1: ( rule__DomainRef__Group__3__Impl )
            // InternalPDDL.g:5662:2: rule__DomainRef__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DomainRef__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DomainRef__Group__3"


    // $ANTLR start "rule__DomainRef__Group__3__Impl"
    // InternalPDDL.g:5668:1: rule__DomainRef__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__DomainRef__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5672:1: ( ( RULE_CP ) )
            // InternalPDDL.g:5673:1: ( RULE_CP )
            {
            // InternalPDDL.g:5673:1: ( RULE_CP )
            // InternalPDDL.g:5674:2: RULE_CP
            {
             before(grammarAccess.getDomainRefAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getDomainRefAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DomainRef__Group__3__Impl"


    // $ANTLR start "rule__Objects__Group__0"
    // InternalPDDL.g:5684:1: rule__Objects__Group__0 : rule__Objects__Group__0__Impl rule__Objects__Group__1 ;
    public final void rule__Objects__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5688:1: ( rule__Objects__Group__0__Impl rule__Objects__Group__1 )
            // InternalPDDL.g:5689:2: rule__Objects__Group__0__Impl rule__Objects__Group__1
            {
            pushFollow(FOLLOW_41);
            rule__Objects__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objects__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objects__Group__0"


    // $ANTLR start "rule__Objects__Group__0__Impl"
    // InternalPDDL.g:5696:1: rule__Objects__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__Objects__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5700:1: ( ( RULE_OP ) )
            // InternalPDDL.g:5701:1: ( RULE_OP )
            {
            // InternalPDDL.g:5701:1: ( RULE_OP )
            // InternalPDDL.g:5702:2: RULE_OP
            {
             before(grammarAccess.getObjectsAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getObjectsAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objects__Group__0__Impl"


    // $ANTLR start "rule__Objects__Group__1"
    // InternalPDDL.g:5711:1: rule__Objects__Group__1 : rule__Objects__Group__1__Impl rule__Objects__Group__2 ;
    public final void rule__Objects__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5715:1: ( rule__Objects__Group__1__Impl rule__Objects__Group__2 )
            // InternalPDDL.g:5716:2: rule__Objects__Group__1__Impl rule__Objects__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__Objects__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objects__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objects__Group__1"


    // $ANTLR start "rule__Objects__Group__1__Impl"
    // InternalPDDL.g:5723:1: rule__Objects__Group__1__Impl : ( ':objects' ) ;
    public final void rule__Objects__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5727:1: ( ( ':objects' ) )
            // InternalPDDL.g:5728:1: ( ':objects' )
            {
            // InternalPDDL.g:5728:1: ( ':objects' )
            // InternalPDDL.g:5729:2: ':objects'
            {
             before(grammarAccess.getObjectsAccess().getObjectsKeyword_1()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getObjectsAccess().getObjectsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objects__Group__1__Impl"


    // $ANTLR start "rule__Objects__Group__2"
    // InternalPDDL.g:5738:1: rule__Objects__Group__2 : rule__Objects__Group__2__Impl rule__Objects__Group__3 ;
    public final void rule__Objects__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5742:1: ( rule__Objects__Group__2__Impl rule__Objects__Group__3 )
            // InternalPDDL.g:5743:2: rule__Objects__Group__2__Impl rule__Objects__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Objects__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objects__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objects__Group__2"


    // $ANTLR start "rule__Objects__Group__2__Impl"
    // InternalPDDL.g:5750:1: rule__Objects__Group__2__Impl : ( ( rule__Objects__NewObjectsListAssignment_2 ) ) ;
    public final void rule__Objects__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5754:1: ( ( ( rule__Objects__NewObjectsListAssignment_2 ) ) )
            // InternalPDDL.g:5755:1: ( ( rule__Objects__NewObjectsListAssignment_2 ) )
            {
            // InternalPDDL.g:5755:1: ( ( rule__Objects__NewObjectsListAssignment_2 ) )
            // InternalPDDL.g:5756:2: ( rule__Objects__NewObjectsListAssignment_2 )
            {
             before(grammarAccess.getObjectsAccess().getNewObjectsListAssignment_2()); 
            // InternalPDDL.g:5757:2: ( rule__Objects__NewObjectsListAssignment_2 )
            // InternalPDDL.g:5757:3: rule__Objects__NewObjectsListAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Objects__NewObjectsListAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getObjectsAccess().getNewObjectsListAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objects__Group__2__Impl"


    // $ANTLR start "rule__Objects__Group__3"
    // InternalPDDL.g:5765:1: rule__Objects__Group__3 : rule__Objects__Group__3__Impl ;
    public final void rule__Objects__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5769:1: ( rule__Objects__Group__3__Impl )
            // InternalPDDL.g:5770:2: rule__Objects__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Objects__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objects__Group__3"


    // $ANTLR start "rule__Objects__Group__3__Impl"
    // InternalPDDL.g:5776:1: rule__Objects__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Objects__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5780:1: ( ( RULE_CP ) )
            // InternalPDDL.g:5781:1: ( RULE_CP )
            {
            // InternalPDDL.g:5781:1: ( RULE_CP )
            // InternalPDDL.g:5782:2: RULE_CP
            {
             before(grammarAccess.getObjectsAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getObjectsAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objects__Group__3__Impl"


    // $ANTLR start "rule__NewObjects__Group__0"
    // InternalPDDL.g:5792:1: rule__NewObjects__Group__0 : rule__NewObjects__Group__0__Impl rule__NewObjects__Group__1 ;
    public final void rule__NewObjects__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5796:1: ( rule__NewObjects__Group__0__Impl rule__NewObjects__Group__1 )
            // InternalPDDL.g:5797:2: rule__NewObjects__Group__0__Impl rule__NewObjects__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__NewObjects__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NewObjects__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewObjects__Group__0"


    // $ANTLR start "rule__NewObjects__Group__0__Impl"
    // InternalPDDL.g:5804:1: rule__NewObjects__Group__0__Impl : ( ( ( rule__NewObjects__NewObjectAssignment_0 ) ) ( ( rule__NewObjects__NewObjectAssignment_0 )* ) ) ;
    public final void rule__NewObjects__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5808:1: ( ( ( ( rule__NewObjects__NewObjectAssignment_0 ) ) ( ( rule__NewObjects__NewObjectAssignment_0 )* ) ) )
            // InternalPDDL.g:5809:1: ( ( ( rule__NewObjects__NewObjectAssignment_0 ) ) ( ( rule__NewObjects__NewObjectAssignment_0 )* ) )
            {
            // InternalPDDL.g:5809:1: ( ( ( rule__NewObjects__NewObjectAssignment_0 ) ) ( ( rule__NewObjects__NewObjectAssignment_0 )* ) )
            // InternalPDDL.g:5810:2: ( ( rule__NewObjects__NewObjectAssignment_0 ) ) ( ( rule__NewObjects__NewObjectAssignment_0 )* )
            {
            // InternalPDDL.g:5810:2: ( ( rule__NewObjects__NewObjectAssignment_0 ) )
            // InternalPDDL.g:5811:3: ( rule__NewObjects__NewObjectAssignment_0 )
            {
             before(grammarAccess.getNewObjectsAccess().getNewObjectAssignment_0()); 
            // InternalPDDL.g:5812:3: ( rule__NewObjects__NewObjectAssignment_0 )
            // InternalPDDL.g:5812:4: rule__NewObjects__NewObjectAssignment_0
            {
            pushFollow(FOLLOW_3);
            rule__NewObjects__NewObjectAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getNewObjectsAccess().getNewObjectAssignment_0()); 

            }

            // InternalPDDL.g:5815:2: ( ( rule__NewObjects__NewObjectAssignment_0 )* )
            // InternalPDDL.g:5816:3: ( rule__NewObjects__NewObjectAssignment_0 )*
            {
             before(grammarAccess.getNewObjectsAccess().getNewObjectAssignment_0()); 
            // InternalPDDL.g:5817:3: ( rule__NewObjects__NewObjectAssignment_0 )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==RULE_NAME) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalPDDL.g:5817:4: rule__NewObjects__NewObjectAssignment_0
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__NewObjects__NewObjectAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

             after(grammarAccess.getNewObjectsAccess().getNewObjectAssignment_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewObjects__Group__0__Impl"


    // $ANTLR start "rule__NewObjects__Group__1"
    // InternalPDDL.g:5826:1: rule__NewObjects__Group__1 : rule__NewObjects__Group__1__Impl rule__NewObjects__Group__2 ;
    public final void rule__NewObjects__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5830:1: ( rule__NewObjects__Group__1__Impl rule__NewObjects__Group__2 )
            // InternalPDDL.g:5831:2: rule__NewObjects__Group__1__Impl rule__NewObjects__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__NewObjects__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NewObjects__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewObjects__Group__1"


    // $ANTLR start "rule__NewObjects__Group__1__Impl"
    // InternalPDDL.g:5838:1: rule__NewObjects__Group__1__Impl : ( '-' ) ;
    public final void rule__NewObjects__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5842:1: ( ( '-' ) )
            // InternalPDDL.g:5843:1: ( '-' )
            {
            // InternalPDDL.g:5843:1: ( '-' )
            // InternalPDDL.g:5844:2: '-'
            {
             before(grammarAccess.getNewObjectsAccess().getHyphenMinusKeyword_1()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getNewObjectsAccess().getHyphenMinusKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewObjects__Group__1__Impl"


    // $ANTLR start "rule__NewObjects__Group__2"
    // InternalPDDL.g:5853:1: rule__NewObjects__Group__2 : rule__NewObjects__Group__2__Impl ;
    public final void rule__NewObjects__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5857:1: ( rule__NewObjects__Group__2__Impl )
            // InternalPDDL.g:5858:2: rule__NewObjects__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NewObjects__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewObjects__Group__2"


    // $ANTLR start "rule__NewObjects__Group__2__Impl"
    // InternalPDDL.g:5864:1: rule__NewObjects__Group__2__Impl : ( ( rule__NewObjects__ObjectTypeAssignment_2 ) ) ;
    public final void rule__NewObjects__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5868:1: ( ( ( rule__NewObjects__ObjectTypeAssignment_2 ) ) )
            // InternalPDDL.g:5869:1: ( ( rule__NewObjects__ObjectTypeAssignment_2 ) )
            {
            // InternalPDDL.g:5869:1: ( ( rule__NewObjects__ObjectTypeAssignment_2 ) )
            // InternalPDDL.g:5870:2: ( rule__NewObjects__ObjectTypeAssignment_2 )
            {
             before(grammarAccess.getNewObjectsAccess().getObjectTypeAssignment_2()); 
            // InternalPDDL.g:5871:2: ( rule__NewObjects__ObjectTypeAssignment_2 )
            // InternalPDDL.g:5871:3: rule__NewObjects__ObjectTypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__NewObjects__ObjectTypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getNewObjectsAccess().getObjectTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewObjects__Group__2__Impl"


    // $ANTLR start "rule__Init__Group__0"
    // InternalPDDL.g:5880:1: rule__Init__Group__0 : rule__Init__Group__0__Impl rule__Init__Group__1 ;
    public final void rule__Init__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5884:1: ( rule__Init__Group__0__Impl rule__Init__Group__1 )
            // InternalPDDL.g:5885:2: rule__Init__Group__0__Impl rule__Init__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Init__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Init__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__0"


    // $ANTLR start "rule__Init__Group__0__Impl"
    // InternalPDDL.g:5892:1: rule__Init__Group__0__Impl : ( () ) ;
    public final void rule__Init__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5896:1: ( ( () ) )
            // InternalPDDL.g:5897:1: ( () )
            {
            // InternalPDDL.g:5897:1: ( () )
            // InternalPDDL.g:5898:2: ()
            {
             before(grammarAccess.getInitAccess().getInitAction_0()); 
            // InternalPDDL.g:5899:2: ()
            // InternalPDDL.g:5899:3: 
            {
            }

             after(grammarAccess.getInitAccess().getInitAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__0__Impl"


    // $ANTLR start "rule__Init__Group__1"
    // InternalPDDL.g:5907:1: rule__Init__Group__1 : rule__Init__Group__1__Impl rule__Init__Group__2 ;
    public final void rule__Init__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5911:1: ( rule__Init__Group__1__Impl rule__Init__Group__2 )
            // InternalPDDL.g:5912:2: rule__Init__Group__1__Impl rule__Init__Group__2
            {
            pushFollow(FOLLOW_42);
            rule__Init__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Init__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__1"


    // $ANTLR start "rule__Init__Group__1__Impl"
    // InternalPDDL.g:5919:1: rule__Init__Group__1__Impl : ( RULE_OP ) ;
    public final void rule__Init__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5923:1: ( ( RULE_OP ) )
            // InternalPDDL.g:5924:1: ( RULE_OP )
            {
            // InternalPDDL.g:5924:1: ( RULE_OP )
            // InternalPDDL.g:5925:2: RULE_OP
            {
             before(grammarAccess.getInitAccess().getOPTerminalRuleCall_1()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getInitAccess().getOPTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__1__Impl"


    // $ANTLR start "rule__Init__Group__2"
    // InternalPDDL.g:5934:1: rule__Init__Group__2 : rule__Init__Group__2__Impl rule__Init__Group__3 ;
    public final void rule__Init__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5938:1: ( rule__Init__Group__2__Impl rule__Init__Group__3 )
            // InternalPDDL.g:5939:2: rule__Init__Group__2__Impl rule__Init__Group__3
            {
            pushFollow(FOLLOW_43);
            rule__Init__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Init__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__2"


    // $ANTLR start "rule__Init__Group__2__Impl"
    // InternalPDDL.g:5946:1: rule__Init__Group__2__Impl : ( ':init' ) ;
    public final void rule__Init__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5950:1: ( ( ':init' ) )
            // InternalPDDL.g:5951:1: ( ':init' )
            {
            // InternalPDDL.g:5951:1: ( ':init' )
            // InternalPDDL.g:5952:2: ':init'
            {
             before(grammarAccess.getInitAccess().getInitKeyword_2()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getInitAccess().getInitKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__2__Impl"


    // $ANTLR start "rule__Init__Group__3"
    // InternalPDDL.g:5961:1: rule__Init__Group__3 : rule__Init__Group__3__Impl rule__Init__Group__4 ;
    public final void rule__Init__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5965:1: ( rule__Init__Group__3__Impl rule__Init__Group__4 )
            // InternalPDDL.g:5966:2: rule__Init__Group__3__Impl rule__Init__Group__4
            {
            pushFollow(FOLLOW_43);
            rule__Init__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Init__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__3"


    // $ANTLR start "rule__Init__Group__3__Impl"
    // InternalPDDL.g:5973:1: rule__Init__Group__3__Impl : ( ( rule__Init__Alternatives_3 )* ) ;
    public final void rule__Init__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5977:1: ( ( ( rule__Init__Alternatives_3 )* ) )
            // InternalPDDL.g:5978:1: ( ( rule__Init__Alternatives_3 )* )
            {
            // InternalPDDL.g:5978:1: ( ( rule__Init__Alternatives_3 )* )
            // InternalPDDL.g:5979:2: ( rule__Init__Alternatives_3 )*
            {
             before(grammarAccess.getInitAccess().getAlternatives_3()); 
            // InternalPDDL.g:5980:2: ( rule__Init__Alternatives_3 )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==RULE_OP||LA33_0==33) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalPDDL.g:5980:3: rule__Init__Alternatives_3
            	    {
            	    pushFollow(FOLLOW_44);
            	    rule__Init__Alternatives_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);

             after(grammarAccess.getInitAccess().getAlternatives_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__3__Impl"


    // $ANTLR start "rule__Init__Group__4"
    // InternalPDDL.g:5988:1: rule__Init__Group__4 : rule__Init__Group__4__Impl ;
    public final void rule__Init__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:5992:1: ( rule__Init__Group__4__Impl )
            // InternalPDDL.g:5993:2: rule__Init__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Init__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__4"


    // $ANTLR start "rule__Init__Group__4__Impl"
    // InternalPDDL.g:5999:1: rule__Init__Group__4__Impl : ( RULE_CP ) ;
    public final void rule__Init__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6003:1: ( ( RULE_CP ) )
            // InternalPDDL.g:6004:1: ( RULE_CP )
            {
            // InternalPDDL.g:6004:1: ( RULE_CP )
            // InternalPDDL.g:6005:2: RULE_CP
            {
             before(grammarAccess.getInitAccess().getCPTerminalRuleCall_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getInitAccess().getCPTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group__4__Impl"


    // $ANTLR start "rule__Init__Group_3_1__0"
    // InternalPDDL.g:6015:1: rule__Init__Group_3_1__0 : rule__Init__Group_3_1__0__Impl rule__Init__Group_3_1__1 ;
    public final void rule__Init__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6019:1: ( rule__Init__Group_3_1__0__Impl rule__Init__Group_3_1__1 )
            // InternalPDDL.g:6020:2: rule__Init__Group_3_1__0__Impl rule__Init__Group_3_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Init__Group_3_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Init__Group_3_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group_3_1__0"


    // $ANTLR start "rule__Init__Group_3_1__0__Impl"
    // InternalPDDL.g:6027:1: rule__Init__Group_3_1__0__Impl : ( 'not' ) ;
    public final void rule__Init__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6031:1: ( ( 'not' ) )
            // InternalPDDL.g:6032:1: ( 'not' )
            {
            // InternalPDDL.g:6032:1: ( 'not' )
            // InternalPDDL.g:6033:2: 'not'
            {
             before(grammarAccess.getInitAccess().getNotKeyword_3_1_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getInitAccess().getNotKeyword_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group_3_1__0__Impl"


    // $ANTLR start "rule__Init__Group_3_1__1"
    // InternalPDDL.g:6042:1: rule__Init__Group_3_1__1 : rule__Init__Group_3_1__1__Impl ;
    public final void rule__Init__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6046:1: ( rule__Init__Group_3_1__1__Impl )
            // InternalPDDL.g:6047:2: rule__Init__Group_3_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Init__Group_3_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group_3_1__1"


    // $ANTLR start "rule__Init__Group_3_1__1__Impl"
    // InternalPDDL.g:6053:1: rule__Init__Group_3_1__1__Impl : ( ( rule__Init__NegativePredicatAssignment_3_1_1 ) ) ;
    public final void rule__Init__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6057:1: ( ( ( rule__Init__NegativePredicatAssignment_3_1_1 ) ) )
            // InternalPDDL.g:6058:1: ( ( rule__Init__NegativePredicatAssignment_3_1_1 ) )
            {
            // InternalPDDL.g:6058:1: ( ( rule__Init__NegativePredicatAssignment_3_1_1 ) )
            // InternalPDDL.g:6059:2: ( rule__Init__NegativePredicatAssignment_3_1_1 )
            {
             before(grammarAccess.getInitAccess().getNegativePredicatAssignment_3_1_1()); 
            // InternalPDDL.g:6060:2: ( rule__Init__NegativePredicatAssignment_3_1_1 )
            // InternalPDDL.g:6060:3: rule__Init__NegativePredicatAssignment_3_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Init__NegativePredicatAssignment_3_1_1();

            state._fsp--;


            }

             after(grammarAccess.getInitAccess().getNegativePredicatAssignment_3_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__Group_3_1__1__Impl"


    // $ANTLR start "rule__PredicatPB__Group__0"
    // InternalPDDL.g:6069:1: rule__PredicatPB__Group__0 : rule__PredicatPB__Group__0__Impl rule__PredicatPB__Group__1 ;
    public final void rule__PredicatPB__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6073:1: ( rule__PredicatPB__Group__0__Impl rule__PredicatPB__Group__1 )
            // InternalPDDL.g:6074:2: rule__PredicatPB__Group__0__Impl rule__PredicatPB__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__PredicatPB__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PredicatPB__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__Group__0"


    // $ANTLR start "rule__PredicatPB__Group__0__Impl"
    // InternalPDDL.g:6081:1: rule__PredicatPB__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__PredicatPB__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6085:1: ( ( RULE_OP ) )
            // InternalPDDL.g:6086:1: ( RULE_OP )
            {
            // InternalPDDL.g:6086:1: ( RULE_OP )
            // InternalPDDL.g:6087:2: RULE_OP
            {
             before(grammarAccess.getPredicatPBAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getPredicatPBAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__Group__0__Impl"


    // $ANTLR start "rule__PredicatPB__Group__1"
    // InternalPDDL.g:6096:1: rule__PredicatPB__Group__1 : rule__PredicatPB__Group__1__Impl rule__PredicatPB__Group__2 ;
    public final void rule__PredicatPB__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6100:1: ( rule__PredicatPB__Group__1__Impl rule__PredicatPB__Group__2 )
            // InternalPDDL.g:6101:2: rule__PredicatPB__Group__1__Impl rule__PredicatPB__Group__2
            {
            pushFollow(FOLLOW_45);
            rule__PredicatPB__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PredicatPB__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__Group__1"


    // $ANTLR start "rule__PredicatPB__Group__1__Impl"
    // InternalPDDL.g:6108:1: rule__PredicatPB__Group__1__Impl : ( ( rule__PredicatPB__PredicatNameAssignment_1 ) ) ;
    public final void rule__PredicatPB__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6112:1: ( ( ( rule__PredicatPB__PredicatNameAssignment_1 ) ) )
            // InternalPDDL.g:6113:1: ( ( rule__PredicatPB__PredicatNameAssignment_1 ) )
            {
            // InternalPDDL.g:6113:1: ( ( rule__PredicatPB__PredicatNameAssignment_1 ) )
            // InternalPDDL.g:6114:2: ( rule__PredicatPB__PredicatNameAssignment_1 )
            {
             before(grammarAccess.getPredicatPBAccess().getPredicatNameAssignment_1()); 
            // InternalPDDL.g:6115:2: ( rule__PredicatPB__PredicatNameAssignment_1 )
            // InternalPDDL.g:6115:3: rule__PredicatPB__PredicatNameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__PredicatPB__PredicatNameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPredicatPBAccess().getPredicatNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__Group__1__Impl"


    // $ANTLR start "rule__PredicatPB__Group__2"
    // InternalPDDL.g:6123:1: rule__PredicatPB__Group__2 : rule__PredicatPB__Group__2__Impl rule__PredicatPB__Group__3 ;
    public final void rule__PredicatPB__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6127:1: ( rule__PredicatPB__Group__2__Impl rule__PredicatPB__Group__3 )
            // InternalPDDL.g:6128:2: rule__PredicatPB__Group__2__Impl rule__PredicatPB__Group__3
            {
            pushFollow(FOLLOW_45);
            rule__PredicatPB__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PredicatPB__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__Group__2"


    // $ANTLR start "rule__PredicatPB__Group__2__Impl"
    // InternalPDDL.g:6135:1: rule__PredicatPB__Group__2__Impl : ( ( rule__PredicatPB__ObjectsAssignment_2 )* ) ;
    public final void rule__PredicatPB__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6139:1: ( ( ( rule__PredicatPB__ObjectsAssignment_2 )* ) )
            // InternalPDDL.g:6140:1: ( ( rule__PredicatPB__ObjectsAssignment_2 )* )
            {
            // InternalPDDL.g:6140:1: ( ( rule__PredicatPB__ObjectsAssignment_2 )* )
            // InternalPDDL.g:6141:2: ( rule__PredicatPB__ObjectsAssignment_2 )*
            {
             before(grammarAccess.getPredicatPBAccess().getObjectsAssignment_2()); 
            // InternalPDDL.g:6142:2: ( rule__PredicatPB__ObjectsAssignment_2 )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==RULE_NAME) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // InternalPDDL.g:6142:3: rule__PredicatPB__ObjectsAssignment_2
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__PredicatPB__ObjectsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

             after(grammarAccess.getPredicatPBAccess().getObjectsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__Group__2__Impl"


    // $ANTLR start "rule__PredicatPB__Group__3"
    // InternalPDDL.g:6150:1: rule__PredicatPB__Group__3 : rule__PredicatPB__Group__3__Impl ;
    public final void rule__PredicatPB__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6154:1: ( rule__PredicatPB__Group__3__Impl )
            // InternalPDDL.g:6155:2: rule__PredicatPB__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PredicatPB__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__Group__3"


    // $ANTLR start "rule__PredicatPB__Group__3__Impl"
    // InternalPDDL.g:6161:1: rule__PredicatPB__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__PredicatPB__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6165:1: ( ( RULE_CP ) )
            // InternalPDDL.g:6166:1: ( RULE_CP )
            {
            // InternalPDDL.g:6166:1: ( RULE_CP )
            // InternalPDDL.g:6167:2: RULE_CP
            {
             before(grammarAccess.getPredicatPBAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getPredicatPBAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__Group__3__Impl"


    // $ANTLR start "rule__Goal__Group__0"
    // InternalPDDL.g:6177:1: rule__Goal__Group__0 : rule__Goal__Group__0__Impl rule__Goal__Group__1 ;
    public final void rule__Goal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6181:1: ( rule__Goal__Group__0__Impl rule__Goal__Group__1 )
            // InternalPDDL.g:6182:2: rule__Goal__Group__0__Impl rule__Goal__Group__1
            {
            pushFollow(FOLLOW_46);
            rule__Goal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__0"


    // $ANTLR start "rule__Goal__Group__0__Impl"
    // InternalPDDL.g:6189:1: rule__Goal__Group__0__Impl : ( RULE_OP ) ;
    public final void rule__Goal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6193:1: ( ( RULE_OP ) )
            // InternalPDDL.g:6194:1: ( RULE_OP )
            {
            // InternalPDDL.g:6194:1: ( RULE_OP )
            // InternalPDDL.g:6195:2: RULE_OP
            {
             before(grammarAccess.getGoalAccess().getOPTerminalRuleCall_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getOPTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__0__Impl"


    // $ANTLR start "rule__Goal__Group__1"
    // InternalPDDL.g:6204:1: rule__Goal__Group__1 : rule__Goal__Group__1__Impl rule__Goal__Group__2 ;
    public final void rule__Goal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6208:1: ( rule__Goal__Group__1__Impl rule__Goal__Group__2 )
            // InternalPDDL.g:6209:2: rule__Goal__Group__1__Impl rule__Goal__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__Goal__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__1"


    // $ANTLR start "rule__Goal__Group__1__Impl"
    // InternalPDDL.g:6216:1: rule__Goal__Group__1__Impl : ( ':goal' ) ;
    public final void rule__Goal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6220:1: ( ( ':goal' ) )
            // InternalPDDL.g:6221:1: ( ':goal' )
            {
            // InternalPDDL.g:6221:1: ( ':goal' )
            // InternalPDDL.g:6222:2: ':goal'
            {
             before(grammarAccess.getGoalAccess().getGoalKeyword_1()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getGoalKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__1__Impl"


    // $ANTLR start "rule__Goal__Group__2"
    // InternalPDDL.g:6231:1: rule__Goal__Group__2 : rule__Goal__Group__2__Impl rule__Goal__Group__3 ;
    public final void rule__Goal__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6235:1: ( rule__Goal__Group__2__Impl rule__Goal__Group__3 )
            // InternalPDDL.g:6236:2: rule__Goal__Group__2__Impl rule__Goal__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Goal__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Goal__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__2"


    // $ANTLR start "rule__Goal__Group__2__Impl"
    // InternalPDDL.g:6243:1: rule__Goal__Group__2__Impl : ( ( rule__Goal__LeGoalAssignment_2 ) ) ;
    public final void rule__Goal__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6247:1: ( ( ( rule__Goal__LeGoalAssignment_2 ) ) )
            // InternalPDDL.g:6248:1: ( ( rule__Goal__LeGoalAssignment_2 ) )
            {
            // InternalPDDL.g:6248:1: ( ( rule__Goal__LeGoalAssignment_2 ) )
            // InternalPDDL.g:6249:2: ( rule__Goal__LeGoalAssignment_2 )
            {
             before(grammarAccess.getGoalAccess().getLeGoalAssignment_2()); 
            // InternalPDDL.g:6250:2: ( rule__Goal__LeGoalAssignment_2 )
            // InternalPDDL.g:6250:3: rule__Goal__LeGoalAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Goal__LeGoalAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getGoalAccess().getLeGoalAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__2__Impl"


    // $ANTLR start "rule__Goal__Group__3"
    // InternalPDDL.g:6258:1: rule__Goal__Group__3 : rule__Goal__Group__3__Impl ;
    public final void rule__Goal__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6262:1: ( rule__Goal__Group__3__Impl )
            // InternalPDDL.g:6263:2: rule__Goal__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Goal__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__3"


    // $ANTLR start "rule__Goal__Group__3__Impl"
    // InternalPDDL.g:6269:1: rule__Goal__Group__3__Impl : ( RULE_CP ) ;
    public final void rule__Goal__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6273:1: ( ( RULE_CP ) )
            // InternalPDDL.g:6274:1: ( RULE_CP )
            {
            // InternalPDDL.g:6274:1: ( RULE_CP )
            // InternalPDDL.g:6275:2: RULE_CP
            {
             before(grammarAccess.getGoalAccess().getCPTerminalRuleCall_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getGoalAccess().getCPTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__Group__3__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_2__0"
    // InternalPDDL.g:6285:1: rule__LogicalExpressionPB__Group_2__0 : rule__LogicalExpressionPB__Group_2__0__Impl rule__LogicalExpressionPB__Group_2__1 ;
    public final void rule__LogicalExpressionPB__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6289:1: ( rule__LogicalExpressionPB__Group_2__0__Impl rule__LogicalExpressionPB__Group_2__1 )
            // InternalPDDL.g:6290:2: rule__LogicalExpressionPB__Group_2__0__Impl rule__LogicalExpressionPB__Group_2__1
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_2__0"


    // $ANTLR start "rule__LogicalExpressionPB__Group_2__0__Impl"
    // InternalPDDL.g:6297:1: rule__LogicalExpressionPB__Group_2__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionPB__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6301:1: ( ( RULE_OP ) )
            // InternalPDDL.g:6302:1: ( RULE_OP )
            {
            // InternalPDDL.g:6302:1: ( RULE_OP )
            // InternalPDDL.g:6303:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_2_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_2__0__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_2__1"
    // InternalPDDL.g:6312:1: rule__LogicalExpressionPB__Group_2__1 : rule__LogicalExpressionPB__Group_2__1__Impl rule__LogicalExpressionPB__Group_2__2 ;
    public final void rule__LogicalExpressionPB__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6316:1: ( rule__LogicalExpressionPB__Group_2__1__Impl rule__LogicalExpressionPB__Group_2__2 )
            // InternalPDDL.g:6317:2: rule__LogicalExpressionPB__Group_2__1__Impl rule__LogicalExpressionPB__Group_2__2
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionPB__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_2__1"


    // $ANTLR start "rule__LogicalExpressionPB__Group_2__1__Impl"
    // InternalPDDL.g:6324:1: rule__LogicalExpressionPB__Group_2__1__Impl : ( ( rule__LogicalExpressionPB__LeAssignment_2_1 ) ) ;
    public final void rule__LogicalExpressionPB__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6328:1: ( ( ( rule__LogicalExpressionPB__LeAssignment_2_1 ) ) )
            // InternalPDDL.g:6329:1: ( ( rule__LogicalExpressionPB__LeAssignment_2_1 ) )
            {
            // InternalPDDL.g:6329:1: ( ( rule__LogicalExpressionPB__LeAssignment_2_1 ) )
            // InternalPDDL.g:6330:2: ( rule__LogicalExpressionPB__LeAssignment_2_1 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeAssignment_2_1()); 
            // InternalPDDL.g:6331:2: ( rule__LogicalExpressionPB__LeAssignment_2_1 )
            // InternalPDDL.g:6331:3: rule__LogicalExpressionPB__LeAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_2__1__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_2__2"
    // InternalPDDL.g:6339:1: rule__LogicalExpressionPB__Group_2__2 : rule__LogicalExpressionPB__Group_2__2__Impl ;
    public final void rule__LogicalExpressionPB__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6343:1: ( rule__LogicalExpressionPB__Group_2__2__Impl )
            // InternalPDDL.g:6344:2: rule__LogicalExpressionPB__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_2__2"


    // $ANTLR start "rule__LogicalExpressionPB__Group_2__2__Impl"
    // InternalPDDL.g:6350:1: rule__LogicalExpressionPB__Group_2__2__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionPB__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6354:1: ( ( RULE_CP ) )
            // InternalPDDL.g:6355:1: ( RULE_CP )
            {
            // InternalPDDL.g:6355:1: ( RULE_CP )
            // InternalPDDL.g:6356:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_2_2()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_2__2__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__0"
    // InternalPDDL.g:6366:1: rule__LogicalExpressionPB__Group_3__0 : rule__LogicalExpressionPB__Group_3__0__Impl rule__LogicalExpressionPB__Group_3__1 ;
    public final void rule__LogicalExpressionPB__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6370:1: ( rule__LogicalExpressionPB__Group_3__0__Impl rule__LogicalExpressionPB__Group_3__1 )
            // InternalPDDL.g:6371:2: rule__LogicalExpressionPB__Group_3__0__Impl rule__LogicalExpressionPB__Group_3__1
            {
            pushFollow(FOLLOW_27);
            rule__LogicalExpressionPB__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__0"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__0__Impl"
    // InternalPDDL.g:6378:1: rule__LogicalExpressionPB__Group_3__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionPB__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6382:1: ( ( RULE_OP ) )
            // InternalPDDL.g:6383:1: ( RULE_OP )
            {
            // InternalPDDL.g:6383:1: ( RULE_OP )
            // InternalPDDL.g:6384:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_3_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__0__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__1"
    // InternalPDDL.g:6393:1: rule__LogicalExpressionPB__Group_3__1 : rule__LogicalExpressionPB__Group_3__1__Impl rule__LogicalExpressionPB__Group_3__2 ;
    public final void rule__LogicalExpressionPB__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6397:1: ( rule__LogicalExpressionPB__Group_3__1__Impl rule__LogicalExpressionPB__Group_3__2 )
            // InternalPDDL.g:6398:2: rule__LogicalExpressionPB__Group_3__1__Impl rule__LogicalExpressionPB__Group_3__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__1"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__1__Impl"
    // InternalPDDL.g:6405:1: rule__LogicalExpressionPB__Group_3__1__Impl : ( '=' ) ;
    public final void rule__LogicalExpressionPB__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6409:1: ( ( '=' ) )
            // InternalPDDL.g:6410:1: ( '=' )
            {
            // InternalPDDL.g:6410:1: ( '=' )
            // InternalPDDL.g:6411:2: '='
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getEqualsSignKeyword_3_1()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getEqualsSignKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__1__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__2"
    // InternalPDDL.g:6420:1: rule__LogicalExpressionPB__Group_3__2 : rule__LogicalExpressionPB__Group_3__2__Impl rule__LogicalExpressionPB__Group_3__3 ;
    public final void rule__LogicalExpressionPB__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6424:1: ( rule__LogicalExpressionPB__Group_3__2__Impl rule__LogicalExpressionPB__Group_3__3 )
            // InternalPDDL.g:6425:2: rule__LogicalExpressionPB__Group_3__2__Impl rule__LogicalExpressionPB__Group_3__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__2"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__2__Impl"
    // InternalPDDL.g:6432:1: rule__LogicalExpressionPB__Group_3__2__Impl : ( ( rule__LogicalExpressionPB__VEqual1Assignment_3_2 ) ) ;
    public final void rule__LogicalExpressionPB__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6436:1: ( ( ( rule__LogicalExpressionPB__VEqual1Assignment_3_2 ) ) )
            // InternalPDDL.g:6437:1: ( ( rule__LogicalExpressionPB__VEqual1Assignment_3_2 ) )
            {
            // InternalPDDL.g:6437:1: ( ( rule__LogicalExpressionPB__VEqual1Assignment_3_2 ) )
            // InternalPDDL.g:6438:2: ( rule__LogicalExpressionPB__VEqual1Assignment_3_2 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getVEqual1Assignment_3_2()); 
            // InternalPDDL.g:6439:2: ( rule__LogicalExpressionPB__VEqual1Assignment_3_2 )
            // InternalPDDL.g:6439:3: rule__LogicalExpressionPB__VEqual1Assignment_3_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__VEqual1Assignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getVEqual1Assignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__2__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__3"
    // InternalPDDL.g:6447:1: rule__LogicalExpressionPB__Group_3__3 : rule__LogicalExpressionPB__Group_3__3__Impl rule__LogicalExpressionPB__Group_3__4 ;
    public final void rule__LogicalExpressionPB__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6451:1: ( rule__LogicalExpressionPB__Group_3__3__Impl rule__LogicalExpressionPB__Group_3__4 )
            // InternalPDDL.g:6452:2: rule__LogicalExpressionPB__Group_3__3__Impl rule__LogicalExpressionPB__Group_3__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionPB__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__3"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__3__Impl"
    // InternalPDDL.g:6459:1: rule__LogicalExpressionPB__Group_3__3__Impl : ( ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )* ) ) ;
    public final void rule__LogicalExpressionPB__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6463:1: ( ( ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )* ) ) )
            // InternalPDDL.g:6464:1: ( ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )* ) )
            {
            // InternalPDDL.g:6464:1: ( ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )* ) )
            // InternalPDDL.g:6465:2: ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 ) ) ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )* )
            {
            // InternalPDDL.g:6465:2: ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 ) )
            // InternalPDDL.g:6466:3: ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getVEqual2Assignment_3_3()); 
            // InternalPDDL.g:6467:3: ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )
            // InternalPDDL.g:6467:4: rule__LogicalExpressionPB__VEqual2Assignment_3_3
            {
            pushFollow(FOLLOW_28);
            rule__LogicalExpressionPB__VEqual2Assignment_3_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getVEqual2Assignment_3_3()); 

            }

            // InternalPDDL.g:6470:2: ( ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )* )
            // InternalPDDL.g:6471:3: ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )*
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getVEqual2Assignment_3_3()); 
            // InternalPDDL.g:6472:3: ( rule__LogicalExpressionPB__VEqual2Assignment_3_3 )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==RULE_OP||LA35_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalPDDL.g:6472:4: rule__LogicalExpressionPB__VEqual2Assignment_3_3
            	    {
            	    pushFollow(FOLLOW_28);
            	    rule__LogicalExpressionPB__VEqual2Assignment_3_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

             after(grammarAccess.getLogicalExpressionPBAccess().getVEqual2Assignment_3_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__3__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__4"
    // InternalPDDL.g:6481:1: rule__LogicalExpressionPB__Group_3__4 : rule__LogicalExpressionPB__Group_3__4__Impl ;
    public final void rule__LogicalExpressionPB__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6485:1: ( rule__LogicalExpressionPB__Group_3__4__Impl )
            // InternalPDDL.g:6486:2: rule__LogicalExpressionPB__Group_3__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_3__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__4"


    // $ANTLR start "rule__LogicalExpressionPB__Group_3__4__Impl"
    // InternalPDDL.g:6492:1: rule__LogicalExpressionPB__Group_3__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionPB__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6496:1: ( ( RULE_CP ) )
            // InternalPDDL.g:6497:1: ( RULE_CP )
            {
            // InternalPDDL.g:6497:1: ( RULE_CP )
            // InternalPDDL.g:6498:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_3_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_3_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_3__4__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_4__0"
    // InternalPDDL.g:6508:1: rule__LogicalExpressionPB__Group_4__0 : rule__LogicalExpressionPB__Group_4__0__Impl rule__LogicalExpressionPB__Group_4__1 ;
    public final void rule__LogicalExpressionPB__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6512:1: ( rule__LogicalExpressionPB__Group_4__0__Impl rule__LogicalExpressionPB__Group_4__1 )
            // InternalPDDL.g:6513:2: rule__LogicalExpressionPB__Group_4__0__Impl rule__LogicalExpressionPB__Group_4__1
            {
            pushFollow(FOLLOW_29);
            rule__LogicalExpressionPB__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_4__0"


    // $ANTLR start "rule__LogicalExpressionPB__Group_4__0__Impl"
    // InternalPDDL.g:6520:1: rule__LogicalExpressionPB__Group_4__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionPB__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6524:1: ( ( RULE_OP ) )
            // InternalPDDL.g:6525:1: ( RULE_OP )
            {
            // InternalPDDL.g:6525:1: ( RULE_OP )
            // InternalPDDL.g:6526:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_4_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_4__0__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_4__1"
    // InternalPDDL.g:6535:1: rule__LogicalExpressionPB__Group_4__1 : rule__LogicalExpressionPB__Group_4__1__Impl rule__LogicalExpressionPB__Group_4__2 ;
    public final void rule__LogicalExpressionPB__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6539:1: ( rule__LogicalExpressionPB__Group_4__1__Impl rule__LogicalExpressionPB__Group_4__2 )
            // InternalPDDL.g:6540:2: rule__LogicalExpressionPB__Group_4__1__Impl rule__LogicalExpressionPB__Group_4__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_4__1"


    // $ANTLR start "rule__LogicalExpressionPB__Group_4__1__Impl"
    // InternalPDDL.g:6547:1: rule__LogicalExpressionPB__Group_4__1__Impl : ( 'not' ) ;
    public final void rule__LogicalExpressionPB__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6551:1: ( ( 'not' ) )
            // InternalPDDL.g:6552:1: ( 'not' )
            {
            // InternalPDDL.g:6552:1: ( 'not' )
            // InternalPDDL.g:6553:2: 'not'
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getNotKeyword_4_1()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getNotKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_4__1__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_4__2"
    // InternalPDDL.g:6562:1: rule__LogicalExpressionPB__Group_4__2 : rule__LogicalExpressionPB__Group_4__2__Impl rule__LogicalExpressionPB__Group_4__3 ;
    public final void rule__LogicalExpressionPB__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6566:1: ( rule__LogicalExpressionPB__Group_4__2__Impl rule__LogicalExpressionPB__Group_4__3 )
            // InternalPDDL.g:6567:2: rule__LogicalExpressionPB__Group_4__2__Impl rule__LogicalExpressionPB__Group_4__3
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionPB__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_4__2"


    // $ANTLR start "rule__LogicalExpressionPB__Group_4__2__Impl"
    // InternalPDDL.g:6574:1: rule__LogicalExpressionPB__Group_4__2__Impl : ( ( rule__LogicalExpressionPB__LeNotAssignment_4_2 ) ) ;
    public final void rule__LogicalExpressionPB__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6578:1: ( ( ( rule__LogicalExpressionPB__LeNotAssignment_4_2 ) ) )
            // InternalPDDL.g:6579:1: ( ( rule__LogicalExpressionPB__LeNotAssignment_4_2 ) )
            {
            // InternalPDDL.g:6579:1: ( ( rule__LogicalExpressionPB__LeNotAssignment_4_2 ) )
            // InternalPDDL.g:6580:2: ( rule__LogicalExpressionPB__LeNotAssignment_4_2 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeNotAssignment_4_2()); 
            // InternalPDDL.g:6581:2: ( rule__LogicalExpressionPB__LeNotAssignment_4_2 )
            // InternalPDDL.g:6581:3: rule__LogicalExpressionPB__LeNotAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeNotAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeNotAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_4__2__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_4__3"
    // InternalPDDL.g:6589:1: rule__LogicalExpressionPB__Group_4__3 : rule__LogicalExpressionPB__Group_4__3__Impl ;
    public final void rule__LogicalExpressionPB__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6593:1: ( rule__LogicalExpressionPB__Group_4__3__Impl )
            // InternalPDDL.g:6594:2: rule__LogicalExpressionPB__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_4__3"


    // $ANTLR start "rule__LogicalExpressionPB__Group_4__3__Impl"
    // InternalPDDL.g:6600:1: rule__LogicalExpressionPB__Group_4__3__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionPB__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6604:1: ( ( RULE_CP ) )
            // InternalPDDL.g:6605:1: ( RULE_CP )
            {
            // InternalPDDL.g:6605:1: ( RULE_CP )
            // InternalPDDL.g:6606:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_4_3()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_4__3__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__0"
    // InternalPDDL.g:6616:1: rule__LogicalExpressionPB__Group_5__0 : rule__LogicalExpressionPB__Group_5__0__Impl rule__LogicalExpressionPB__Group_5__1 ;
    public final void rule__LogicalExpressionPB__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6620:1: ( rule__LogicalExpressionPB__Group_5__0__Impl rule__LogicalExpressionPB__Group_5__1 )
            // InternalPDDL.g:6621:2: rule__LogicalExpressionPB__Group_5__0__Impl rule__LogicalExpressionPB__Group_5__1
            {
            pushFollow(FOLLOW_30);
            rule__LogicalExpressionPB__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__0"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__0__Impl"
    // InternalPDDL.g:6628:1: rule__LogicalExpressionPB__Group_5__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionPB__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6632:1: ( ( RULE_OP ) )
            // InternalPDDL.g:6633:1: ( RULE_OP )
            {
            // InternalPDDL.g:6633:1: ( RULE_OP )
            // InternalPDDL.g:6634:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_5_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__0__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__1"
    // InternalPDDL.g:6643:1: rule__LogicalExpressionPB__Group_5__1 : rule__LogicalExpressionPB__Group_5__1__Impl rule__LogicalExpressionPB__Group_5__2 ;
    public final void rule__LogicalExpressionPB__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6647:1: ( rule__LogicalExpressionPB__Group_5__1__Impl rule__LogicalExpressionPB__Group_5__2 )
            // InternalPDDL.g:6648:2: rule__LogicalExpressionPB__Group_5__1__Impl rule__LogicalExpressionPB__Group_5__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__1"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__1__Impl"
    // InternalPDDL.g:6655:1: rule__LogicalExpressionPB__Group_5__1__Impl : ( 'and' ) ;
    public final void rule__LogicalExpressionPB__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6659:1: ( ( 'and' ) )
            // InternalPDDL.g:6660:1: ( 'and' )
            {
            // InternalPDDL.g:6660:1: ( 'and' )
            // InternalPDDL.g:6661:2: 'and'
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getAndKeyword_5_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getAndKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__1__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__2"
    // InternalPDDL.g:6670:1: rule__LogicalExpressionPB__Group_5__2 : rule__LogicalExpressionPB__Group_5__2__Impl rule__LogicalExpressionPB__Group_5__3 ;
    public final void rule__LogicalExpressionPB__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6674:1: ( rule__LogicalExpressionPB__Group_5__2__Impl rule__LogicalExpressionPB__Group_5__3 )
            // InternalPDDL.g:6675:2: rule__LogicalExpressionPB__Group_5__2__Impl rule__LogicalExpressionPB__Group_5__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__2"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__2__Impl"
    // InternalPDDL.g:6682:1: rule__LogicalExpressionPB__Group_5__2__Impl : ( ( rule__LogicalExpressionPB__LeAnd1Assignment_5_2 ) ) ;
    public final void rule__LogicalExpressionPB__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6686:1: ( ( ( rule__LogicalExpressionPB__LeAnd1Assignment_5_2 ) ) )
            // InternalPDDL.g:6687:1: ( ( rule__LogicalExpressionPB__LeAnd1Assignment_5_2 ) )
            {
            // InternalPDDL.g:6687:1: ( ( rule__LogicalExpressionPB__LeAnd1Assignment_5_2 ) )
            // InternalPDDL.g:6688:2: ( rule__LogicalExpressionPB__LeAnd1Assignment_5_2 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeAnd1Assignment_5_2()); 
            // InternalPDDL.g:6689:2: ( rule__LogicalExpressionPB__LeAnd1Assignment_5_2 )
            // InternalPDDL.g:6689:3: rule__LogicalExpressionPB__LeAnd1Assignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeAnd1Assignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeAnd1Assignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__2__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__3"
    // InternalPDDL.g:6697:1: rule__LogicalExpressionPB__Group_5__3 : rule__LogicalExpressionPB__Group_5__3__Impl rule__LogicalExpressionPB__Group_5__4 ;
    public final void rule__LogicalExpressionPB__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6701:1: ( rule__LogicalExpressionPB__Group_5__3__Impl rule__LogicalExpressionPB__Group_5__4 )
            // InternalPDDL.g:6702:2: rule__LogicalExpressionPB__Group_5__3__Impl rule__LogicalExpressionPB__Group_5__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionPB__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__3"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__3__Impl"
    // InternalPDDL.g:6709:1: rule__LogicalExpressionPB__Group_5__3__Impl : ( ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )* ) ) ;
    public final void rule__LogicalExpressionPB__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6713:1: ( ( ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )* ) ) )
            // InternalPDDL.g:6714:1: ( ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )* ) )
            {
            // InternalPDDL.g:6714:1: ( ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )* ) )
            // InternalPDDL.g:6715:2: ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 ) ) ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )* )
            {
            // InternalPDDL.g:6715:2: ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 ) )
            // InternalPDDL.g:6716:3: ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeAnd2Assignment_5_3()); 
            // InternalPDDL.g:6717:3: ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )
            // InternalPDDL.g:6717:4: rule__LogicalExpressionPB__LeAnd2Assignment_5_3
            {
            pushFollow(FOLLOW_28);
            rule__LogicalExpressionPB__LeAnd2Assignment_5_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeAnd2Assignment_5_3()); 

            }

            // InternalPDDL.g:6720:2: ( ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )* )
            // InternalPDDL.g:6721:3: ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )*
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeAnd2Assignment_5_3()); 
            // InternalPDDL.g:6722:3: ( rule__LogicalExpressionPB__LeAnd2Assignment_5_3 )*
            loop36:
            do {
                int alt36=2;
                int LA36_0 = input.LA(1);

                if ( (LA36_0==RULE_OP||LA36_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt36=1;
                }


                switch (alt36) {
            	case 1 :
            	    // InternalPDDL.g:6722:4: rule__LogicalExpressionPB__LeAnd2Assignment_5_3
            	    {
            	    pushFollow(FOLLOW_28);
            	    rule__LogicalExpressionPB__LeAnd2Assignment_5_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop36;
                }
            } while (true);

             after(grammarAccess.getLogicalExpressionPBAccess().getLeAnd2Assignment_5_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__3__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__4"
    // InternalPDDL.g:6731:1: rule__LogicalExpressionPB__Group_5__4 : rule__LogicalExpressionPB__Group_5__4__Impl ;
    public final void rule__LogicalExpressionPB__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6735:1: ( rule__LogicalExpressionPB__Group_5__4__Impl )
            // InternalPDDL.g:6736:2: rule__LogicalExpressionPB__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__4"


    // $ANTLR start "rule__LogicalExpressionPB__Group_5__4__Impl"
    // InternalPDDL.g:6742:1: rule__LogicalExpressionPB__Group_5__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionPB__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6746:1: ( ( RULE_CP ) )
            // InternalPDDL.g:6747:1: ( RULE_CP )
            {
            // InternalPDDL.g:6747:1: ( RULE_CP )
            // InternalPDDL.g:6748:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_5_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_5__4__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__0"
    // InternalPDDL.g:6758:1: rule__LogicalExpressionPB__Group_6__0 : rule__LogicalExpressionPB__Group_6__0__Impl rule__LogicalExpressionPB__Group_6__1 ;
    public final void rule__LogicalExpressionPB__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6762:1: ( rule__LogicalExpressionPB__Group_6__0__Impl rule__LogicalExpressionPB__Group_6__1 )
            // InternalPDDL.g:6763:2: rule__LogicalExpressionPB__Group_6__0__Impl rule__LogicalExpressionPB__Group_6__1
            {
            pushFollow(FOLLOW_31);
            rule__LogicalExpressionPB__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__0"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__0__Impl"
    // InternalPDDL.g:6770:1: rule__LogicalExpressionPB__Group_6__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionPB__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6774:1: ( ( RULE_OP ) )
            // InternalPDDL.g:6775:1: ( RULE_OP )
            {
            // InternalPDDL.g:6775:1: ( RULE_OP )
            // InternalPDDL.g:6776:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_6_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__0__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__1"
    // InternalPDDL.g:6785:1: rule__LogicalExpressionPB__Group_6__1 : rule__LogicalExpressionPB__Group_6__1__Impl rule__LogicalExpressionPB__Group_6__2 ;
    public final void rule__LogicalExpressionPB__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6789:1: ( rule__LogicalExpressionPB__Group_6__1__Impl rule__LogicalExpressionPB__Group_6__2 )
            // InternalPDDL.g:6790:2: rule__LogicalExpressionPB__Group_6__1__Impl rule__LogicalExpressionPB__Group_6__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__1"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__1__Impl"
    // InternalPDDL.g:6797:1: rule__LogicalExpressionPB__Group_6__1__Impl : ( 'or' ) ;
    public final void rule__LogicalExpressionPB__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6801:1: ( ( 'or' ) )
            // InternalPDDL.g:6802:1: ( 'or' )
            {
            // InternalPDDL.g:6802:1: ( 'or' )
            // InternalPDDL.g:6803:2: 'or'
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOrKeyword_6_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOrKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__1__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__2"
    // InternalPDDL.g:6812:1: rule__LogicalExpressionPB__Group_6__2 : rule__LogicalExpressionPB__Group_6__2__Impl rule__LogicalExpressionPB__Group_6__3 ;
    public final void rule__LogicalExpressionPB__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6816:1: ( rule__LogicalExpressionPB__Group_6__2__Impl rule__LogicalExpressionPB__Group_6__3 )
            // InternalPDDL.g:6817:2: rule__LogicalExpressionPB__Group_6__2__Impl rule__LogicalExpressionPB__Group_6__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__2"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__2__Impl"
    // InternalPDDL.g:6824:1: rule__LogicalExpressionPB__Group_6__2__Impl : ( ( rule__LogicalExpressionPB__LeOr1Assignment_6_2 ) ) ;
    public final void rule__LogicalExpressionPB__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6828:1: ( ( ( rule__LogicalExpressionPB__LeOr1Assignment_6_2 ) ) )
            // InternalPDDL.g:6829:1: ( ( rule__LogicalExpressionPB__LeOr1Assignment_6_2 ) )
            {
            // InternalPDDL.g:6829:1: ( ( rule__LogicalExpressionPB__LeOr1Assignment_6_2 ) )
            // InternalPDDL.g:6830:2: ( rule__LogicalExpressionPB__LeOr1Assignment_6_2 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeOr1Assignment_6_2()); 
            // InternalPDDL.g:6831:2: ( rule__LogicalExpressionPB__LeOr1Assignment_6_2 )
            // InternalPDDL.g:6831:3: rule__LogicalExpressionPB__LeOr1Assignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeOr1Assignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeOr1Assignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__2__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__3"
    // InternalPDDL.g:6839:1: rule__LogicalExpressionPB__Group_6__3 : rule__LogicalExpressionPB__Group_6__3__Impl rule__LogicalExpressionPB__Group_6__4 ;
    public final void rule__LogicalExpressionPB__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6843:1: ( rule__LogicalExpressionPB__Group_6__3__Impl rule__LogicalExpressionPB__Group_6__4 )
            // InternalPDDL.g:6844:2: rule__LogicalExpressionPB__Group_6__3__Impl rule__LogicalExpressionPB__Group_6__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionPB__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__3"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__3__Impl"
    // InternalPDDL.g:6851:1: rule__LogicalExpressionPB__Group_6__3__Impl : ( ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )* ) ) ;
    public final void rule__LogicalExpressionPB__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6855:1: ( ( ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )* ) ) )
            // InternalPDDL.g:6856:1: ( ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )* ) )
            {
            // InternalPDDL.g:6856:1: ( ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )* ) )
            // InternalPDDL.g:6857:2: ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 ) ) ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )* )
            {
            // InternalPDDL.g:6857:2: ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 ) )
            // InternalPDDL.g:6858:3: ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeOr2Assignment_6_3()); 
            // InternalPDDL.g:6859:3: ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )
            // InternalPDDL.g:6859:4: rule__LogicalExpressionPB__LeOr2Assignment_6_3
            {
            pushFollow(FOLLOW_28);
            rule__LogicalExpressionPB__LeOr2Assignment_6_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeOr2Assignment_6_3()); 

            }

            // InternalPDDL.g:6862:2: ( ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )* )
            // InternalPDDL.g:6863:3: ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )*
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeOr2Assignment_6_3()); 
            // InternalPDDL.g:6864:3: ( rule__LogicalExpressionPB__LeOr2Assignment_6_3 )*
            loop37:
            do {
                int alt37=2;
                int LA37_0 = input.LA(1);

                if ( (LA37_0==RULE_OP||LA37_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt37=1;
                }


                switch (alt37) {
            	case 1 :
            	    // InternalPDDL.g:6864:4: rule__LogicalExpressionPB__LeOr2Assignment_6_3
            	    {
            	    pushFollow(FOLLOW_28);
            	    rule__LogicalExpressionPB__LeOr2Assignment_6_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);

             after(grammarAccess.getLogicalExpressionPBAccess().getLeOr2Assignment_6_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__3__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__4"
    // InternalPDDL.g:6873:1: rule__LogicalExpressionPB__Group_6__4 : rule__LogicalExpressionPB__Group_6__4__Impl ;
    public final void rule__LogicalExpressionPB__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6877:1: ( rule__LogicalExpressionPB__Group_6__4__Impl )
            // InternalPDDL.g:6878:2: rule__LogicalExpressionPB__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__4"


    // $ANTLR start "rule__LogicalExpressionPB__Group_6__4__Impl"
    // InternalPDDL.g:6884:1: rule__LogicalExpressionPB__Group_6__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionPB__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6888:1: ( ( RULE_CP ) )
            // InternalPDDL.g:6889:1: ( RULE_CP )
            {
            // InternalPDDL.g:6889:1: ( RULE_CP )
            // InternalPDDL.g:6890:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_6_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_6__4__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__0"
    // InternalPDDL.g:6900:1: rule__LogicalExpressionPB__Group_7__0 : rule__LogicalExpressionPB__Group_7__0__Impl rule__LogicalExpressionPB__Group_7__1 ;
    public final void rule__LogicalExpressionPB__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6904:1: ( rule__LogicalExpressionPB__Group_7__0__Impl rule__LogicalExpressionPB__Group_7__1 )
            // InternalPDDL.g:6905:2: rule__LogicalExpressionPB__Group_7__0__Impl rule__LogicalExpressionPB__Group_7__1
            {
            pushFollow(FOLLOW_32);
            rule__LogicalExpressionPB__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__0"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__0__Impl"
    // InternalPDDL.g:6912:1: rule__LogicalExpressionPB__Group_7__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionPB__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6916:1: ( ( RULE_OP ) )
            // InternalPDDL.g:6917:1: ( RULE_OP )
            {
            // InternalPDDL.g:6917:1: ( RULE_OP )
            // InternalPDDL.g:6918:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_7_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__0__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__1"
    // InternalPDDL.g:6927:1: rule__LogicalExpressionPB__Group_7__1 : rule__LogicalExpressionPB__Group_7__1__Impl rule__LogicalExpressionPB__Group_7__2 ;
    public final void rule__LogicalExpressionPB__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6931:1: ( rule__LogicalExpressionPB__Group_7__1__Impl rule__LogicalExpressionPB__Group_7__2 )
            // InternalPDDL.g:6932:2: rule__LogicalExpressionPB__Group_7__1__Impl rule__LogicalExpressionPB__Group_7__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__1"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__1__Impl"
    // InternalPDDL.g:6939:1: rule__LogicalExpressionPB__Group_7__1__Impl : ( 'imply' ) ;
    public final void rule__LogicalExpressionPB__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6943:1: ( ( 'imply' ) )
            // InternalPDDL.g:6944:1: ( 'imply' )
            {
            // InternalPDDL.g:6944:1: ( 'imply' )
            // InternalPDDL.g:6945:2: 'imply'
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getImplyKeyword_7_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getImplyKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__1__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__2"
    // InternalPDDL.g:6954:1: rule__LogicalExpressionPB__Group_7__2 : rule__LogicalExpressionPB__Group_7__2__Impl rule__LogicalExpressionPB__Group_7__3 ;
    public final void rule__LogicalExpressionPB__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6958:1: ( rule__LogicalExpressionPB__Group_7__2__Impl rule__LogicalExpressionPB__Group_7__3 )
            // InternalPDDL.g:6959:2: rule__LogicalExpressionPB__Group_7__2__Impl rule__LogicalExpressionPB__Group_7__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_7__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_7__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__2"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__2__Impl"
    // InternalPDDL.g:6966:1: rule__LogicalExpressionPB__Group_7__2__Impl : ( ( rule__LogicalExpressionPB__LeImply1Assignment_7_2 ) ) ;
    public final void rule__LogicalExpressionPB__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6970:1: ( ( ( rule__LogicalExpressionPB__LeImply1Assignment_7_2 ) ) )
            // InternalPDDL.g:6971:1: ( ( rule__LogicalExpressionPB__LeImply1Assignment_7_2 ) )
            {
            // InternalPDDL.g:6971:1: ( ( rule__LogicalExpressionPB__LeImply1Assignment_7_2 ) )
            // InternalPDDL.g:6972:2: ( rule__LogicalExpressionPB__LeImply1Assignment_7_2 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeImply1Assignment_7_2()); 
            // InternalPDDL.g:6973:2: ( rule__LogicalExpressionPB__LeImply1Assignment_7_2 )
            // InternalPDDL.g:6973:3: rule__LogicalExpressionPB__LeImply1Assignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeImply1Assignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeImply1Assignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__2__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__3"
    // InternalPDDL.g:6981:1: rule__LogicalExpressionPB__Group_7__3 : rule__LogicalExpressionPB__Group_7__3__Impl rule__LogicalExpressionPB__Group_7__4 ;
    public final void rule__LogicalExpressionPB__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6985:1: ( rule__LogicalExpressionPB__Group_7__3__Impl rule__LogicalExpressionPB__Group_7__4 )
            // InternalPDDL.g:6986:2: rule__LogicalExpressionPB__Group_7__3__Impl rule__LogicalExpressionPB__Group_7__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionPB__Group_7__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_7__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__3"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__3__Impl"
    // InternalPDDL.g:6993:1: rule__LogicalExpressionPB__Group_7__3__Impl : ( ( rule__LogicalExpressionPB__LeImply2Assignment_7_3 ) ) ;
    public final void rule__LogicalExpressionPB__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:6997:1: ( ( ( rule__LogicalExpressionPB__LeImply2Assignment_7_3 ) ) )
            // InternalPDDL.g:6998:1: ( ( rule__LogicalExpressionPB__LeImply2Assignment_7_3 ) )
            {
            // InternalPDDL.g:6998:1: ( ( rule__LogicalExpressionPB__LeImply2Assignment_7_3 ) )
            // InternalPDDL.g:6999:2: ( rule__LogicalExpressionPB__LeImply2Assignment_7_3 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeImply2Assignment_7_3()); 
            // InternalPDDL.g:7000:2: ( rule__LogicalExpressionPB__LeImply2Assignment_7_3 )
            // InternalPDDL.g:7000:3: rule__LogicalExpressionPB__LeImply2Assignment_7_3
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeImply2Assignment_7_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeImply2Assignment_7_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__3__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__4"
    // InternalPDDL.g:7008:1: rule__LogicalExpressionPB__Group_7__4 : rule__LogicalExpressionPB__Group_7__4__Impl ;
    public final void rule__LogicalExpressionPB__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7012:1: ( rule__LogicalExpressionPB__Group_7__4__Impl )
            // InternalPDDL.g:7013:2: rule__LogicalExpressionPB__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_7__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__4"


    // $ANTLR start "rule__LogicalExpressionPB__Group_7__4__Impl"
    // InternalPDDL.g:7019:1: rule__LogicalExpressionPB__Group_7__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionPB__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7023:1: ( ( RULE_CP ) )
            // InternalPDDL.g:7024:1: ( RULE_CP )
            {
            // InternalPDDL.g:7024:1: ( RULE_CP )
            // InternalPDDL.g:7025:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_7_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_7_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_7__4__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__0"
    // InternalPDDL.g:7035:1: rule__LogicalExpressionPB__Group_8__0 : rule__LogicalExpressionPB__Group_8__0__Impl rule__LogicalExpressionPB__Group_8__1 ;
    public final void rule__LogicalExpressionPB__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7039:1: ( rule__LogicalExpressionPB__Group_8__0__Impl rule__LogicalExpressionPB__Group_8__1 )
            // InternalPDDL.g:7040:2: rule__LogicalExpressionPB__Group_8__0__Impl rule__LogicalExpressionPB__Group_8__1
            {
            pushFollow(FOLLOW_33);
            rule__LogicalExpressionPB__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__0"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__0__Impl"
    // InternalPDDL.g:7047:1: rule__LogicalExpressionPB__Group_8__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionPB__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7051:1: ( ( RULE_OP ) )
            // InternalPDDL.g:7052:1: ( RULE_OP )
            {
            // InternalPDDL.g:7052:1: ( RULE_OP )
            // InternalPDDL.g:7053:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_8_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__0__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__1"
    // InternalPDDL.g:7062:1: rule__LogicalExpressionPB__Group_8__1 : rule__LogicalExpressionPB__Group_8__1__Impl rule__LogicalExpressionPB__Group_8__2 ;
    public final void rule__LogicalExpressionPB__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7066:1: ( rule__LogicalExpressionPB__Group_8__1__Impl rule__LogicalExpressionPB__Group_8__2 )
            // InternalPDDL.g:7067:2: rule__LogicalExpressionPB__Group_8__1__Impl rule__LogicalExpressionPB__Group_8__2
            {
            pushFollow(FOLLOW_7);
            rule__LogicalExpressionPB__Group_8__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_8__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__1"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__1__Impl"
    // InternalPDDL.g:7074:1: rule__LogicalExpressionPB__Group_8__1__Impl : ( 'forall' ) ;
    public final void rule__LogicalExpressionPB__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7078:1: ( ( 'forall' ) )
            // InternalPDDL.g:7079:1: ( 'forall' )
            {
            // InternalPDDL.g:7079:1: ( 'forall' )
            // InternalPDDL.g:7080:2: 'forall'
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getForallKeyword_8_1()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getForallKeyword_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__1__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__2"
    // InternalPDDL.g:7089:1: rule__LogicalExpressionPB__Group_8__2 : rule__LogicalExpressionPB__Group_8__2__Impl rule__LogicalExpressionPB__Group_8__3 ;
    public final void rule__LogicalExpressionPB__Group_8__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7093:1: ( rule__LogicalExpressionPB__Group_8__2__Impl rule__LogicalExpressionPB__Group_8__3 )
            // InternalPDDL.g:7094:2: rule__LogicalExpressionPB__Group_8__2__Impl rule__LogicalExpressionPB__Group_8__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_8__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_8__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__2"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__2__Impl"
    // InternalPDDL.g:7101:1: rule__LogicalExpressionPB__Group_8__2__Impl : ( ( rule__LogicalExpressionPB__TdForallAssignment_8_2 ) ) ;
    public final void rule__LogicalExpressionPB__Group_8__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7105:1: ( ( ( rule__LogicalExpressionPB__TdForallAssignment_8_2 ) ) )
            // InternalPDDL.g:7106:1: ( ( rule__LogicalExpressionPB__TdForallAssignment_8_2 ) )
            {
            // InternalPDDL.g:7106:1: ( ( rule__LogicalExpressionPB__TdForallAssignment_8_2 ) )
            // InternalPDDL.g:7107:2: ( rule__LogicalExpressionPB__TdForallAssignment_8_2 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getTdForallAssignment_8_2()); 
            // InternalPDDL.g:7108:2: ( rule__LogicalExpressionPB__TdForallAssignment_8_2 )
            // InternalPDDL.g:7108:3: rule__LogicalExpressionPB__TdForallAssignment_8_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__TdForallAssignment_8_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getTdForallAssignment_8_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__2__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__3"
    // InternalPDDL.g:7116:1: rule__LogicalExpressionPB__Group_8__3 : rule__LogicalExpressionPB__Group_8__3__Impl rule__LogicalExpressionPB__Group_8__4 ;
    public final void rule__LogicalExpressionPB__Group_8__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7120:1: ( rule__LogicalExpressionPB__Group_8__3__Impl rule__LogicalExpressionPB__Group_8__4 )
            // InternalPDDL.g:7121:2: rule__LogicalExpressionPB__Group_8__3__Impl rule__LogicalExpressionPB__Group_8__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionPB__Group_8__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_8__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__3"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__3__Impl"
    // InternalPDDL.g:7128:1: rule__LogicalExpressionPB__Group_8__3__Impl : ( ( rule__LogicalExpressionPB__LeForallAssignment_8_3 ) ) ;
    public final void rule__LogicalExpressionPB__Group_8__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7132:1: ( ( ( rule__LogicalExpressionPB__LeForallAssignment_8_3 ) ) )
            // InternalPDDL.g:7133:1: ( ( rule__LogicalExpressionPB__LeForallAssignment_8_3 ) )
            {
            // InternalPDDL.g:7133:1: ( ( rule__LogicalExpressionPB__LeForallAssignment_8_3 ) )
            // InternalPDDL.g:7134:2: ( rule__LogicalExpressionPB__LeForallAssignment_8_3 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeForallAssignment_8_3()); 
            // InternalPDDL.g:7135:2: ( rule__LogicalExpressionPB__LeForallAssignment_8_3 )
            // InternalPDDL.g:7135:3: rule__LogicalExpressionPB__LeForallAssignment_8_3
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeForallAssignment_8_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeForallAssignment_8_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__3__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__4"
    // InternalPDDL.g:7143:1: rule__LogicalExpressionPB__Group_8__4 : rule__LogicalExpressionPB__Group_8__4__Impl ;
    public final void rule__LogicalExpressionPB__Group_8__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7147:1: ( rule__LogicalExpressionPB__Group_8__4__Impl )
            // InternalPDDL.g:7148:2: rule__LogicalExpressionPB__Group_8__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_8__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__4"


    // $ANTLR start "rule__LogicalExpressionPB__Group_8__4__Impl"
    // InternalPDDL.g:7154:1: rule__LogicalExpressionPB__Group_8__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionPB__Group_8__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7158:1: ( ( RULE_CP ) )
            // InternalPDDL.g:7159:1: ( RULE_CP )
            {
            // InternalPDDL.g:7159:1: ( RULE_CP )
            // InternalPDDL.g:7160:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_8_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_8_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_8__4__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__0"
    // InternalPDDL.g:7170:1: rule__LogicalExpressionPB__Group_9__0 : rule__LogicalExpressionPB__Group_9__0__Impl rule__LogicalExpressionPB__Group_9__1 ;
    public final void rule__LogicalExpressionPB__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7174:1: ( rule__LogicalExpressionPB__Group_9__0__Impl rule__LogicalExpressionPB__Group_9__1 )
            // InternalPDDL.g:7175:2: rule__LogicalExpressionPB__Group_9__0__Impl rule__LogicalExpressionPB__Group_9__1
            {
            pushFollow(FOLLOW_34);
            rule__LogicalExpressionPB__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__0"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__0__Impl"
    // InternalPDDL.g:7182:1: rule__LogicalExpressionPB__Group_9__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionPB__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7186:1: ( ( RULE_OP ) )
            // InternalPDDL.g:7187:1: ( RULE_OP )
            {
            // InternalPDDL.g:7187:1: ( RULE_OP )
            // InternalPDDL.g:7188:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_9_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__0__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__1"
    // InternalPDDL.g:7197:1: rule__LogicalExpressionPB__Group_9__1 : rule__LogicalExpressionPB__Group_9__1__Impl rule__LogicalExpressionPB__Group_9__2 ;
    public final void rule__LogicalExpressionPB__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7201:1: ( rule__LogicalExpressionPB__Group_9__1__Impl rule__LogicalExpressionPB__Group_9__2 )
            // InternalPDDL.g:7202:2: rule__LogicalExpressionPB__Group_9__1__Impl rule__LogicalExpressionPB__Group_9__2
            {
            pushFollow(FOLLOW_7);
            rule__LogicalExpressionPB__Group_9__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_9__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__1"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__1__Impl"
    // InternalPDDL.g:7209:1: rule__LogicalExpressionPB__Group_9__1__Impl : ( 'exists' ) ;
    public final void rule__LogicalExpressionPB__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7213:1: ( ( 'exists' ) )
            // InternalPDDL.g:7214:1: ( 'exists' )
            {
            // InternalPDDL.g:7214:1: ( 'exists' )
            // InternalPDDL.g:7215:2: 'exists'
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getExistsKeyword_9_1()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getExistsKeyword_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__1__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__2"
    // InternalPDDL.g:7224:1: rule__LogicalExpressionPB__Group_9__2 : rule__LogicalExpressionPB__Group_9__2__Impl rule__LogicalExpressionPB__Group_9__3 ;
    public final void rule__LogicalExpressionPB__Group_9__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7228:1: ( rule__LogicalExpressionPB__Group_9__2__Impl rule__LogicalExpressionPB__Group_9__3 )
            // InternalPDDL.g:7229:2: rule__LogicalExpressionPB__Group_9__2__Impl rule__LogicalExpressionPB__Group_9__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_9__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_9__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__2"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__2__Impl"
    // InternalPDDL.g:7236:1: rule__LogicalExpressionPB__Group_9__2__Impl : ( ( rule__LogicalExpressionPB__TdExistsAssignment_9_2 ) ) ;
    public final void rule__LogicalExpressionPB__Group_9__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7240:1: ( ( ( rule__LogicalExpressionPB__TdExistsAssignment_9_2 ) ) )
            // InternalPDDL.g:7241:1: ( ( rule__LogicalExpressionPB__TdExistsAssignment_9_2 ) )
            {
            // InternalPDDL.g:7241:1: ( ( rule__LogicalExpressionPB__TdExistsAssignment_9_2 ) )
            // InternalPDDL.g:7242:2: ( rule__LogicalExpressionPB__TdExistsAssignment_9_2 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getTdExistsAssignment_9_2()); 
            // InternalPDDL.g:7243:2: ( rule__LogicalExpressionPB__TdExistsAssignment_9_2 )
            // InternalPDDL.g:7243:3: rule__LogicalExpressionPB__TdExistsAssignment_9_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__TdExistsAssignment_9_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getTdExistsAssignment_9_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__2__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__3"
    // InternalPDDL.g:7251:1: rule__LogicalExpressionPB__Group_9__3 : rule__LogicalExpressionPB__Group_9__3__Impl rule__LogicalExpressionPB__Group_9__4 ;
    public final void rule__LogicalExpressionPB__Group_9__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7255:1: ( rule__LogicalExpressionPB__Group_9__3__Impl rule__LogicalExpressionPB__Group_9__4 )
            // InternalPDDL.g:7256:2: rule__LogicalExpressionPB__Group_9__3__Impl rule__LogicalExpressionPB__Group_9__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionPB__Group_9__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_9__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__3"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__3__Impl"
    // InternalPDDL.g:7263:1: rule__LogicalExpressionPB__Group_9__3__Impl : ( ( rule__LogicalExpressionPB__LeExistsAssignment_9_3 ) ) ;
    public final void rule__LogicalExpressionPB__Group_9__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7267:1: ( ( ( rule__LogicalExpressionPB__LeExistsAssignment_9_3 ) ) )
            // InternalPDDL.g:7268:1: ( ( rule__LogicalExpressionPB__LeExistsAssignment_9_3 ) )
            {
            // InternalPDDL.g:7268:1: ( ( rule__LogicalExpressionPB__LeExistsAssignment_9_3 ) )
            // InternalPDDL.g:7269:2: ( rule__LogicalExpressionPB__LeExistsAssignment_9_3 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeExistsAssignment_9_3()); 
            // InternalPDDL.g:7270:2: ( rule__LogicalExpressionPB__LeExistsAssignment_9_3 )
            // InternalPDDL.g:7270:3: rule__LogicalExpressionPB__LeExistsAssignment_9_3
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeExistsAssignment_9_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeExistsAssignment_9_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__3__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__4"
    // InternalPDDL.g:7278:1: rule__LogicalExpressionPB__Group_9__4 : rule__LogicalExpressionPB__Group_9__4__Impl ;
    public final void rule__LogicalExpressionPB__Group_9__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7282:1: ( rule__LogicalExpressionPB__Group_9__4__Impl )
            // InternalPDDL.g:7283:2: rule__LogicalExpressionPB__Group_9__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_9__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__4"


    // $ANTLR start "rule__LogicalExpressionPB__Group_9__4__Impl"
    // InternalPDDL.g:7289:1: rule__LogicalExpressionPB__Group_9__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionPB__Group_9__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7293:1: ( ( RULE_CP ) )
            // InternalPDDL.g:7294:1: ( RULE_CP )
            {
            // InternalPDDL.g:7294:1: ( RULE_CP )
            // InternalPDDL.g:7295:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_9_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_9_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_9__4__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__0"
    // InternalPDDL.g:7305:1: rule__LogicalExpressionPB__Group_10__0 : rule__LogicalExpressionPB__Group_10__0__Impl rule__LogicalExpressionPB__Group_10__1 ;
    public final void rule__LogicalExpressionPB__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7309:1: ( rule__LogicalExpressionPB__Group_10__0__Impl rule__LogicalExpressionPB__Group_10__1 )
            // InternalPDDL.g:7310:2: rule__LogicalExpressionPB__Group_10__0__Impl rule__LogicalExpressionPB__Group_10__1
            {
            pushFollow(FOLLOW_35);
            rule__LogicalExpressionPB__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__0"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__0__Impl"
    // InternalPDDL.g:7317:1: rule__LogicalExpressionPB__Group_10__0__Impl : ( RULE_OP ) ;
    public final void rule__LogicalExpressionPB__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7321:1: ( ( RULE_OP ) )
            // InternalPDDL.g:7322:1: ( RULE_OP )
            {
            // InternalPDDL.g:7322:1: ( RULE_OP )
            // InternalPDDL.g:7323:2: RULE_OP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_10_0()); 
            match(input,RULE_OP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__0__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__1"
    // InternalPDDL.g:7332:1: rule__LogicalExpressionPB__Group_10__1 : rule__LogicalExpressionPB__Group_10__1__Impl rule__LogicalExpressionPB__Group_10__2 ;
    public final void rule__LogicalExpressionPB__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7336:1: ( rule__LogicalExpressionPB__Group_10__1__Impl rule__LogicalExpressionPB__Group_10__2 )
            // InternalPDDL.g:7337:2: rule__LogicalExpressionPB__Group_10__1__Impl rule__LogicalExpressionPB__Group_10__2
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_10__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_10__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__1"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__1__Impl"
    // InternalPDDL.g:7344:1: rule__LogicalExpressionPB__Group_10__1__Impl : ( 'when' ) ;
    public final void rule__LogicalExpressionPB__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7348:1: ( ( 'when' ) )
            // InternalPDDL.g:7349:1: ( 'when' )
            {
            // InternalPDDL.g:7349:1: ( 'when' )
            // InternalPDDL.g:7350:2: 'when'
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getWhenKeyword_10_1()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getWhenKeyword_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__1__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__2"
    // InternalPDDL.g:7359:1: rule__LogicalExpressionPB__Group_10__2 : rule__LogicalExpressionPB__Group_10__2__Impl rule__LogicalExpressionPB__Group_10__3 ;
    public final void rule__LogicalExpressionPB__Group_10__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7363:1: ( rule__LogicalExpressionPB__Group_10__2__Impl rule__LogicalExpressionPB__Group_10__3 )
            // InternalPDDL.g:7364:2: rule__LogicalExpressionPB__Group_10__2__Impl rule__LogicalExpressionPB__Group_10__3
            {
            pushFollow(FOLLOW_26);
            rule__LogicalExpressionPB__Group_10__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_10__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__2"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__2__Impl"
    // InternalPDDL.g:7371:1: rule__LogicalExpressionPB__Group_10__2__Impl : ( ( rule__LogicalExpressionPB__LeWhen1Assignment_10_2 ) ) ;
    public final void rule__LogicalExpressionPB__Group_10__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7375:1: ( ( ( rule__LogicalExpressionPB__LeWhen1Assignment_10_2 ) ) )
            // InternalPDDL.g:7376:1: ( ( rule__LogicalExpressionPB__LeWhen1Assignment_10_2 ) )
            {
            // InternalPDDL.g:7376:1: ( ( rule__LogicalExpressionPB__LeWhen1Assignment_10_2 ) )
            // InternalPDDL.g:7377:2: ( rule__LogicalExpressionPB__LeWhen1Assignment_10_2 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeWhen1Assignment_10_2()); 
            // InternalPDDL.g:7378:2: ( rule__LogicalExpressionPB__LeWhen1Assignment_10_2 )
            // InternalPDDL.g:7378:3: rule__LogicalExpressionPB__LeWhen1Assignment_10_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeWhen1Assignment_10_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeWhen1Assignment_10_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__2__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__3"
    // InternalPDDL.g:7386:1: rule__LogicalExpressionPB__Group_10__3 : rule__LogicalExpressionPB__Group_10__3__Impl rule__LogicalExpressionPB__Group_10__4 ;
    public final void rule__LogicalExpressionPB__Group_10__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7390:1: ( rule__LogicalExpressionPB__Group_10__3__Impl rule__LogicalExpressionPB__Group_10__4 )
            // InternalPDDL.g:7391:2: rule__LogicalExpressionPB__Group_10__3__Impl rule__LogicalExpressionPB__Group_10__4
            {
            pushFollow(FOLLOW_9);
            rule__LogicalExpressionPB__Group_10__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_10__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__3"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__3__Impl"
    // InternalPDDL.g:7398:1: rule__LogicalExpressionPB__Group_10__3__Impl : ( ( rule__LogicalExpressionPB__LeWhen2Assignment_10_3 ) ) ;
    public final void rule__LogicalExpressionPB__Group_10__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7402:1: ( ( ( rule__LogicalExpressionPB__LeWhen2Assignment_10_3 ) ) )
            // InternalPDDL.g:7403:1: ( ( rule__LogicalExpressionPB__LeWhen2Assignment_10_3 ) )
            {
            // InternalPDDL.g:7403:1: ( ( rule__LogicalExpressionPB__LeWhen2Assignment_10_3 ) )
            // InternalPDDL.g:7404:2: ( rule__LogicalExpressionPB__LeWhen2Assignment_10_3 )
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeWhen2Assignment_10_3()); 
            // InternalPDDL.g:7405:2: ( rule__LogicalExpressionPB__LeWhen2Assignment_10_3 )
            // InternalPDDL.g:7405:3: rule__LogicalExpressionPB__LeWhen2Assignment_10_3
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__LeWhen2Assignment_10_3();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionPBAccess().getLeWhen2Assignment_10_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__3__Impl"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__4"
    // InternalPDDL.g:7413:1: rule__LogicalExpressionPB__Group_10__4 : rule__LogicalExpressionPB__Group_10__4__Impl ;
    public final void rule__LogicalExpressionPB__Group_10__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7417:1: ( rule__LogicalExpressionPB__Group_10__4__Impl )
            // InternalPDDL.g:7418:2: rule__LogicalExpressionPB__Group_10__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpressionPB__Group_10__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__4"


    // $ANTLR start "rule__LogicalExpressionPB__Group_10__4__Impl"
    // InternalPDDL.g:7424:1: rule__LogicalExpressionPB__Group_10__4__Impl : ( RULE_CP ) ;
    public final void rule__LogicalExpressionPB__Group_10__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7428:1: ( ( RULE_CP ) )
            // InternalPDDL.g:7429:1: ( RULE_CP )
            {
            // InternalPDDL.g:7429:1: ( RULE_CP )
            // InternalPDDL.g:7430:2: RULE_CP
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_10_4()); 
            match(input,RULE_CP,FOLLOW_2); 
             after(grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_10_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__Group_10__4__Impl"


    // $ANTLR start "rule__PDDLModel__FileAssignment_1_2"
    // InternalPDDL.g:7440:1: rule__PDDLModel__FileAssignment_1_2 : ( rulePDDLFile ) ;
    public final void rule__PDDLModel__FileAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7444:1: ( ( rulePDDLFile ) )
            // InternalPDDL.g:7445:2: ( rulePDDLFile )
            {
            // InternalPDDL.g:7445:2: ( rulePDDLFile )
            // InternalPDDL.g:7446:3: rulePDDLFile
            {
             before(grammarAccess.getPDDLModelAccess().getFilePDDLFileParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePDDLFile();

            state._fsp--;

             after(grammarAccess.getPDDLModelAccess().getFilePDDLFileParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLModel__FileAssignment_1_2"


    // $ANTLR start "rule__PDDLFile__NameDAssignment_0_2"
    // InternalPDDL.g:7455:1: rule__PDDLFile__NameDAssignment_0_2 : ( RULE_NAME ) ;
    public final void rule__PDDLFile__NameDAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7459:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:7460:2: ( RULE_NAME )
            {
            // InternalPDDL.g:7460:2: ( RULE_NAME )
            // InternalPDDL.g:7461:3: RULE_NAME
            {
             before(grammarAccess.getPDDLFileAccess().getNameDNAMETerminalRuleCall_0_2_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getPDDLFileAccess().getNameDNAMETerminalRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__NameDAssignment_0_2"


    // $ANTLR start "rule__PDDLFile__ContentDAssignment_0_4"
    // InternalPDDL.g:7470:1: rule__PDDLFile__ContentDAssignment_0_4 : ( ruleDomain ) ;
    public final void rule__PDDLFile__ContentDAssignment_0_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7474:1: ( ( ruleDomain ) )
            // InternalPDDL.g:7475:2: ( ruleDomain )
            {
            // InternalPDDL.g:7475:2: ( ruleDomain )
            // InternalPDDL.g:7476:3: ruleDomain
            {
             before(grammarAccess.getPDDLFileAccess().getContentDDomainParserRuleCall_0_4_0()); 
            pushFollow(FOLLOW_2);
            ruleDomain();

            state._fsp--;

             after(grammarAccess.getPDDLFileAccess().getContentDDomainParserRuleCall_0_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__ContentDAssignment_0_4"


    // $ANTLR start "rule__PDDLFile__NamePAssignment_1_2"
    // InternalPDDL.g:7485:1: rule__PDDLFile__NamePAssignment_1_2 : ( RULE_NAME ) ;
    public final void rule__PDDLFile__NamePAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7489:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:7490:2: ( RULE_NAME )
            {
            // InternalPDDL.g:7490:2: ( RULE_NAME )
            // InternalPDDL.g:7491:3: RULE_NAME
            {
             before(grammarAccess.getPDDLFileAccess().getNamePNAMETerminalRuleCall_1_2_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getPDDLFileAccess().getNamePNAMETerminalRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__NamePAssignment_1_2"


    // $ANTLR start "rule__PDDLFile__ContentPAssignment_1_4"
    // InternalPDDL.g:7500:1: rule__PDDLFile__ContentPAssignment_1_4 : ( ruleProblem ) ;
    public final void rule__PDDLFile__ContentPAssignment_1_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7504:1: ( ( ruleProblem ) )
            // InternalPDDL.g:7505:2: ( ruleProblem )
            {
            // InternalPDDL.g:7505:2: ( ruleProblem )
            // InternalPDDL.g:7506:3: ruleProblem
            {
             before(grammarAccess.getPDDLFileAccess().getContentPProblemParserRuleCall_1_4_0()); 
            pushFollow(FOLLOW_2);
            ruleProblem();

            state._fsp--;

             after(grammarAccess.getPDDLFileAccess().getContentPProblemParserRuleCall_1_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLFile__ContentPAssignment_1_4"


    // $ANTLR start "rule__Domain__ExtendsAssignment_1"
    // InternalPDDL.g:7515:1: rule__Domain__ExtendsAssignment_1 : ( ruleExtends ) ;
    public final void rule__Domain__ExtendsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7519:1: ( ( ruleExtends ) )
            // InternalPDDL.g:7520:2: ( ruleExtends )
            {
            // InternalPDDL.g:7520:2: ( ruleExtends )
            // InternalPDDL.g:7521:3: ruleExtends
            {
             before(grammarAccess.getDomainAccess().getExtendsExtendsParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExtends();

            state._fsp--;

             after(grammarAccess.getDomainAccess().getExtendsExtendsParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__ExtendsAssignment_1"


    // $ANTLR start "rule__Domain__RequirementsAssignment_2"
    // InternalPDDL.g:7530:1: rule__Domain__RequirementsAssignment_2 : ( ruleRequirements ) ;
    public final void rule__Domain__RequirementsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7534:1: ( ( ruleRequirements ) )
            // InternalPDDL.g:7535:2: ( ruleRequirements )
            {
            // InternalPDDL.g:7535:2: ( ruleRequirements )
            // InternalPDDL.g:7536:3: ruleRequirements
            {
             before(grammarAccess.getDomainAccess().getRequirementsRequirementsParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleRequirements();

            state._fsp--;

             after(grammarAccess.getDomainAccess().getRequirementsRequirementsParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__RequirementsAssignment_2"


    // $ANTLR start "rule__Domain__TypesAssignment_3"
    // InternalPDDL.g:7545:1: rule__Domain__TypesAssignment_3 : ( ruleTypes ) ;
    public final void rule__Domain__TypesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7549:1: ( ( ruleTypes ) )
            // InternalPDDL.g:7550:2: ( ruleTypes )
            {
            // InternalPDDL.g:7550:2: ( ruleTypes )
            // InternalPDDL.g:7551:3: ruleTypes
            {
             before(grammarAccess.getDomainAccess().getTypesTypesParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleTypes();

            state._fsp--;

             after(grammarAccess.getDomainAccess().getTypesTypesParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__TypesAssignment_3"


    // $ANTLR start "rule__Domain__PredicatesAssignment_4"
    // InternalPDDL.g:7560:1: rule__Domain__PredicatesAssignment_4 : ( rulePredicates ) ;
    public final void rule__Domain__PredicatesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7564:1: ( ( rulePredicates ) )
            // InternalPDDL.g:7565:2: ( rulePredicates )
            {
            // InternalPDDL.g:7565:2: ( rulePredicates )
            // InternalPDDL.g:7566:3: rulePredicates
            {
             before(grammarAccess.getDomainAccess().getPredicatesPredicatesParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            rulePredicates();

            state._fsp--;

             after(grammarAccess.getDomainAccess().getPredicatesPredicatesParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__PredicatesAssignment_4"


    // $ANTLR start "rule__Domain__FunctionsAssignment_5"
    // InternalPDDL.g:7575:1: rule__Domain__FunctionsAssignment_5 : ( ruleFunctions ) ;
    public final void rule__Domain__FunctionsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7579:1: ( ( ruleFunctions ) )
            // InternalPDDL.g:7580:2: ( ruleFunctions )
            {
            // InternalPDDL.g:7580:2: ( ruleFunctions )
            // InternalPDDL.g:7581:3: ruleFunctions
            {
             before(grammarAccess.getDomainAccess().getFunctionsFunctionsParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleFunctions();

            state._fsp--;

             after(grammarAccess.getDomainAccess().getFunctionsFunctionsParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__FunctionsAssignment_5"


    // $ANTLR start "rule__Domain__ActionsAssignment_6_0"
    // InternalPDDL.g:7590:1: rule__Domain__ActionsAssignment_6_0 : ( rulePDDLAction ) ;
    public final void rule__Domain__ActionsAssignment_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7594:1: ( ( rulePDDLAction ) )
            // InternalPDDL.g:7595:2: ( rulePDDLAction )
            {
            // InternalPDDL.g:7595:2: ( rulePDDLAction )
            // InternalPDDL.g:7596:3: rulePDDLAction
            {
             before(grammarAccess.getDomainAccess().getActionsPDDLActionParserRuleCall_6_0_0()); 
            pushFollow(FOLLOW_2);
            rulePDDLAction();

            state._fsp--;

             after(grammarAccess.getDomainAccess().getActionsPDDLActionParserRuleCall_6_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__ActionsAssignment_6_0"


    // $ANTLR start "rule__Domain__AxiomsAssignment_6_1"
    // InternalPDDL.g:7605:1: rule__Domain__AxiomsAssignment_6_1 : ( ruleAxiom ) ;
    public final void rule__Domain__AxiomsAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7609:1: ( ( ruleAxiom ) )
            // InternalPDDL.g:7610:2: ( ruleAxiom )
            {
            // InternalPDDL.g:7610:2: ( ruleAxiom )
            // InternalPDDL.g:7611:3: ruleAxiom
            {
             before(grammarAccess.getDomainAccess().getAxiomsAxiomParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAxiom();

            state._fsp--;

             after(grammarAccess.getDomainAccess().getAxiomsAxiomParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domain__AxiomsAssignment_6_1"


    // $ANTLR start "rule__Extends__EListAssignment_2"
    // InternalPDDL.g:7620:1: rule__Extends__EListAssignment_2 : ( ruleExtendsList ) ;
    public final void rule__Extends__EListAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7624:1: ( ( ruleExtendsList ) )
            // InternalPDDL.g:7625:2: ( ruleExtendsList )
            {
            // InternalPDDL.g:7625:2: ( ruleExtendsList )
            // InternalPDDL.g:7626:3: ruleExtendsList
            {
             before(grammarAccess.getExtendsAccess().getEListExtendsListParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExtendsList();

            state._fsp--;

             after(grammarAccess.getExtendsAccess().getEListExtendsListParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extends__EListAssignment_2"


    // $ANTLR start "rule__ExtendsList__EElemAssignment"
    // InternalPDDL.g:7635:1: rule__ExtendsList__EElemAssignment : ( ruleExtend ) ;
    public final void rule__ExtendsList__EElemAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7639:1: ( ( ruleExtend ) )
            // InternalPDDL.g:7640:2: ( ruleExtend )
            {
            // InternalPDDL.g:7640:2: ( ruleExtend )
            // InternalPDDL.g:7641:3: ruleExtend
            {
             before(grammarAccess.getExtendsListAccess().getEElemExtendParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleExtend();

            state._fsp--;

             after(grammarAccess.getExtendsListAccess().getEElemExtendParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExtendsList__EElemAssignment"


    // $ANTLR start "rule__Extend__ExtendNameAssignment"
    // InternalPDDL.g:7650:1: rule__Extend__ExtendNameAssignment : ( RULE_NAME ) ;
    public final void rule__Extend__ExtendNameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7654:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:7655:2: ( RULE_NAME )
            {
            // InternalPDDL.g:7655:2: ( RULE_NAME )
            // InternalPDDL.g:7656:3: RULE_NAME
            {
             before(grammarAccess.getExtendAccess().getExtendNameNAMETerminalRuleCall_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getExtendAccess().getExtendNameNAMETerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extend__ExtendNameAssignment"


    // $ANTLR start "rule__Requirements__RListAssignment_2"
    // InternalPDDL.g:7665:1: rule__Requirements__RListAssignment_2 : ( ruleRequirementsList ) ;
    public final void rule__Requirements__RListAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7669:1: ( ( ruleRequirementsList ) )
            // InternalPDDL.g:7670:2: ( ruleRequirementsList )
            {
            // InternalPDDL.g:7670:2: ( ruleRequirementsList )
            // InternalPDDL.g:7671:3: ruleRequirementsList
            {
             before(grammarAccess.getRequirementsAccess().getRListRequirementsListParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleRequirementsList();

            state._fsp--;

             after(grammarAccess.getRequirementsAccess().getRListRequirementsListParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirements__RListAssignment_2"


    // $ANTLR start "rule__RequirementsList__RElemAssignment"
    // InternalPDDL.g:7680:1: rule__RequirementsList__RElemAssignment : ( ruleRequirement ) ;
    public final void rule__RequirementsList__RElemAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7684:1: ( ( ruleRequirement ) )
            // InternalPDDL.g:7685:2: ( ruleRequirement )
            {
            // InternalPDDL.g:7685:2: ( ruleRequirement )
            // InternalPDDL.g:7686:3: ruleRequirement
            {
             before(grammarAccess.getRequirementsListAccess().getRElemRequirementParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleRequirement();

            state._fsp--;

             after(grammarAccess.getRequirementsListAccess().getRElemRequirementParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RequirementsList__RElemAssignment"


    // $ANTLR start "rule__Requirement__RequirementNameAssignment"
    // InternalPDDL.g:7695:1: rule__Requirement__RequirementNameAssignment : ( RULE_IDWITHTWOPOINTBEFOREANDDASHES ) ;
    public final void rule__Requirement__RequirementNameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7699:1: ( ( RULE_IDWITHTWOPOINTBEFOREANDDASHES ) )
            // InternalPDDL.g:7700:2: ( RULE_IDWITHTWOPOINTBEFOREANDDASHES )
            {
            // InternalPDDL.g:7700:2: ( RULE_IDWITHTWOPOINTBEFOREANDDASHES )
            // InternalPDDL.g:7701:3: RULE_IDWITHTWOPOINTBEFOREANDDASHES
            {
             before(grammarAccess.getRequirementAccess().getRequirementNameIDWITHTWOPOINTBEFOREANDDASHESTerminalRuleCall_0()); 
            match(input,RULE_IDWITHTWOPOINTBEFOREANDDASHES,FOLLOW_2); 
             after(grammarAccess.getRequirementAccess().getRequirementNameIDWITHTWOPOINTBEFOREANDDASHESTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Requirement__RequirementNameAssignment"


    // $ANTLR start "rule__Types__NewTypesAssignment_2"
    // InternalPDDL.g:7710:1: rule__Types__NewTypesAssignment_2 : ( ruleNewTypes ) ;
    public final void rule__Types__NewTypesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7714:1: ( ( ruleNewTypes ) )
            // InternalPDDL.g:7715:2: ( ruleNewTypes )
            {
            // InternalPDDL.g:7715:2: ( ruleNewTypes )
            // InternalPDDL.g:7716:3: ruleNewTypes
            {
             before(grammarAccess.getTypesAccess().getNewTypesNewTypesParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNewTypes();

            state._fsp--;

             after(grammarAccess.getTypesAccess().getNewTypesNewTypesParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Types__NewTypesAssignment_2"


    // $ANTLR start "rule__NewTypes__DeclInheritorTypesAssignment_1"
    // InternalPDDL.g:7725:1: rule__NewTypes__DeclInheritorTypesAssignment_1 : ( ruleInheritorTypes ) ;
    public final void rule__NewTypes__DeclInheritorTypesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7729:1: ( ( ruleInheritorTypes ) )
            // InternalPDDL.g:7730:2: ( ruleInheritorTypes )
            {
            // InternalPDDL.g:7730:2: ( ruleInheritorTypes )
            // InternalPDDL.g:7731:3: ruleInheritorTypes
            {
             before(grammarAccess.getNewTypesAccess().getDeclInheritorTypesInheritorTypesParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInheritorTypes();

            state._fsp--;

             after(grammarAccess.getNewTypesAccess().getDeclInheritorTypesInheritorTypesParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewTypes__DeclInheritorTypesAssignment_1"


    // $ANTLR start "rule__NewTypes__DeclOtherTypesAssignment_2"
    // InternalPDDL.g:7740:1: rule__NewTypes__DeclOtherTypesAssignment_2 : ( ruleDeclarationTypes ) ;
    public final void rule__NewTypes__DeclOtherTypesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7744:1: ( ( ruleDeclarationTypes ) )
            // InternalPDDL.g:7745:2: ( ruleDeclarationTypes )
            {
            // InternalPDDL.g:7745:2: ( ruleDeclarationTypes )
            // InternalPDDL.g:7746:3: ruleDeclarationTypes
            {
             before(grammarAccess.getNewTypesAccess().getDeclOtherTypesDeclarationTypesParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDeclarationTypes();

            state._fsp--;

             after(grammarAccess.getNewTypesAccess().getDeclOtherTypesDeclarationTypesParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewTypes__DeclOtherTypesAssignment_2"


    // $ANTLR start "rule__InheritorTypes__SubTypesAssignment_0"
    // InternalPDDL.g:7755:1: rule__InheritorTypes__SubTypesAssignment_0 : ( ruleDeclarationTypes ) ;
    public final void rule__InheritorTypes__SubTypesAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7759:1: ( ( ruleDeclarationTypes ) )
            // InternalPDDL.g:7760:2: ( ruleDeclarationTypes )
            {
            // InternalPDDL.g:7760:2: ( ruleDeclarationTypes )
            // InternalPDDL.g:7761:3: ruleDeclarationTypes
            {
             before(grammarAccess.getInheritorTypesAccess().getSubTypesDeclarationTypesParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDeclarationTypes();

            state._fsp--;

             after(grammarAccess.getInheritorTypesAccess().getSubTypesDeclarationTypesParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InheritorTypes__SubTypesAssignment_0"


    // $ANTLR start "rule__InheritorTypes__SupTypeAssignment_2"
    // InternalPDDL.g:7770:1: rule__InheritorTypes__SupTypeAssignment_2 : ( ruleType ) ;
    public final void rule__InheritorTypes__SupTypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7774:1: ( ( ruleType ) )
            // InternalPDDL.g:7775:2: ( ruleType )
            {
            // InternalPDDL.g:7775:2: ( ruleType )
            // InternalPDDL.g:7776:3: ruleType
            {
             before(grammarAccess.getInheritorTypesAccess().getSupTypeTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getInheritorTypesAccess().getSupTypeTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InheritorTypes__SupTypeAssignment_2"


    // $ANTLR start "rule__DeclarationTypes__DeclTAssignment"
    // InternalPDDL.g:7785:1: rule__DeclarationTypes__DeclTAssignment : ( ruleType ) ;
    public final void rule__DeclarationTypes__DeclTAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7789:1: ( ( ruleType ) )
            // InternalPDDL.g:7790:2: ( ruleType )
            {
            // InternalPDDL.g:7790:2: ( ruleType )
            // InternalPDDL.g:7791:3: ruleType
            {
             before(grammarAccess.getDeclarationTypesAccess().getDeclTTypeParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getDeclarationTypesAccess().getDeclTTypeParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationTypes__DeclTAssignment"


    // $ANTLR start "rule__Type__TypeNameAssignment"
    // InternalPDDL.g:7800:1: rule__Type__TypeNameAssignment : ( RULE_NAME ) ;
    public final void rule__Type__TypeNameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7804:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:7805:2: ( RULE_NAME )
            {
            // InternalPDDL.g:7805:2: ( RULE_NAME )
            // InternalPDDL.g:7806:3: RULE_NAME
            {
             before(grammarAccess.getTypeAccess().getTypeNameNAMETerminalRuleCall_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getTypeAccess().getTypeNameNAMETerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__TypeNameAssignment"


    // $ANTLR start "rule__Predicates__PredicatListAssignment_2"
    // InternalPDDL.g:7815:1: rule__Predicates__PredicatListAssignment_2 : ( ruleDeclPFList ) ;
    public final void rule__Predicates__PredicatListAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7819:1: ( ( ruleDeclPFList ) )
            // InternalPDDL.g:7820:2: ( ruleDeclPFList )
            {
            // InternalPDDL.g:7820:2: ( ruleDeclPFList )
            // InternalPDDL.g:7821:3: ruleDeclPFList
            {
             before(grammarAccess.getPredicatesAccess().getPredicatListDeclPFListParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDeclPFList();

            state._fsp--;

             after(grammarAccess.getPredicatesAccess().getPredicatListDeclPFListParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predicates__PredicatListAssignment_2"


    // $ANTLR start "rule__DeclPFList__PfListAssignment"
    // InternalPDDL.g:7830:1: rule__DeclPFList__PfListAssignment : ( ruleDeclPredicatOrFunction ) ;
    public final void rule__DeclPFList__PfListAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7834:1: ( ( ruleDeclPredicatOrFunction ) )
            // InternalPDDL.g:7835:2: ( ruleDeclPredicatOrFunction )
            {
            // InternalPDDL.g:7835:2: ( ruleDeclPredicatOrFunction )
            // InternalPDDL.g:7836:3: ruleDeclPredicatOrFunction
            {
             before(grammarAccess.getDeclPFListAccess().getPfListDeclPredicatOrFunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleDeclPredicatOrFunction();

            state._fsp--;

             after(grammarAccess.getDeclPFListAccess().getPfListDeclPredicatOrFunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPFList__PfListAssignment"


    // $ANTLR start "rule__DeclPredicatOrFunction__PfnameAssignment_1"
    // InternalPDDL.g:7845:1: rule__DeclPredicatOrFunction__PfnameAssignment_1 : ( RULE_NAME ) ;
    public final void rule__DeclPredicatOrFunction__PfnameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7849:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:7850:2: ( RULE_NAME )
            {
            // InternalPDDL.g:7850:2: ( RULE_NAME )
            // InternalPDDL.g:7851:3: RULE_NAME
            {
             before(grammarAccess.getDeclPredicatOrFunctionAccess().getPfnameNAMETerminalRuleCall_1_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getDeclPredicatOrFunctionAccess().getPfnameNAMETerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__PfnameAssignment_1"


    // $ANTLR start "rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2"
    // InternalPDDL.g:7860:1: rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2 : ( ruleTypedVariablesList ) ;
    public final void rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7864:1: ( ( ruleTypedVariablesList ) )
            // InternalPDDL.g:7865:2: ( ruleTypedVariablesList )
            {
            // InternalPDDL.g:7865:2: ( ruleTypedVariablesList )
            // InternalPDDL.g:7866:3: ruleTypedVariablesList
            {
             before(grammarAccess.getDeclPredicatOrFunctionAccess().getTypedVariablesListTypedVariablesListParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTypedVariablesList();

            state._fsp--;

             after(grammarAccess.getDeclPredicatOrFunctionAccess().getTypedVariablesListTypedVariablesListParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclPredicatOrFunction__TypedVariablesListAssignment_2"


    // $ANTLR start "rule__TypedVariablesList__VariablesListAssignment"
    // InternalPDDL.g:7875:1: rule__TypedVariablesList__VariablesListAssignment : ( ruleTypedVariable ) ;
    public final void rule__TypedVariablesList__VariablesListAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7879:1: ( ( ruleTypedVariable ) )
            // InternalPDDL.g:7880:2: ( ruleTypedVariable )
            {
            // InternalPDDL.g:7880:2: ( ruleTypedVariable )
            // InternalPDDL.g:7881:3: ruleTypedVariable
            {
             before(grammarAccess.getTypedVariablesListAccess().getVariablesListTypedVariableParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleTypedVariable();

            state._fsp--;

             after(grammarAccess.getTypedVariablesListAccess().getVariablesListTypedVariableParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariablesList__VariablesListAssignment"


    // $ANTLR start "rule__TypedVariable__VariablesAssignment_0"
    // InternalPDDL.g:7890:1: rule__TypedVariable__VariablesAssignment_0 : ( ruleVariable ) ;
    public final void rule__TypedVariable__VariablesAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7894:1: ( ( ruleVariable ) )
            // InternalPDDL.g:7895:2: ( ruleVariable )
            {
            // InternalPDDL.g:7895:2: ( ruleVariable )
            // InternalPDDL.g:7896:3: ruleVariable
            {
             before(grammarAccess.getTypedVariableAccess().getVariablesVariableParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getTypedVariableAccess().getVariablesVariableParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__VariablesAssignment_0"


    // $ANTLR start "rule__TypedVariable__TypeAssignment_2_0"
    // InternalPDDL.g:7905:1: rule__TypedVariable__TypeAssignment_2_0 : ( ruleType ) ;
    public final void rule__TypedVariable__TypeAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7909:1: ( ( ruleType ) )
            // InternalPDDL.g:7910:2: ( ruleType )
            {
            // InternalPDDL.g:7910:2: ( ruleType )
            // InternalPDDL.g:7911:3: ruleType
            {
             before(grammarAccess.getTypedVariableAccess().getTypeTypeParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypedVariableAccess().getTypeTypeParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__TypeAssignment_2_0"


    // $ANTLR start "rule__TypedVariable__EitherAssignment_2_1"
    // InternalPDDL.g:7920:1: rule__TypedVariable__EitherAssignment_2_1 : ( ruleEither ) ;
    public final void rule__TypedVariable__EitherAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7924:1: ( ( ruleEither ) )
            // InternalPDDL.g:7925:2: ( ruleEither )
            {
            // InternalPDDL.g:7925:2: ( ruleEither )
            // InternalPDDL.g:7926:3: ruleEither
            {
             before(grammarAccess.getTypedVariableAccess().getEitherEitherParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEither();

            state._fsp--;

             after(grammarAccess.getTypedVariableAccess().getEitherEitherParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariable__EitherAssignment_2_1"


    // $ANTLR start "rule__Either__EitherType1Assignment_2"
    // InternalPDDL.g:7935:1: rule__Either__EitherType1Assignment_2 : ( ruleType ) ;
    public final void rule__Either__EitherType1Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7939:1: ( ( ruleType ) )
            // InternalPDDL.g:7940:2: ( ruleType )
            {
            // InternalPDDL.g:7940:2: ( ruleType )
            // InternalPDDL.g:7941:3: ruleType
            {
             before(grammarAccess.getEitherAccess().getEitherType1TypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getEitherAccess().getEitherType1TypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__EitherType1Assignment_2"


    // $ANTLR start "rule__Either__EitherType2Assignment_3"
    // InternalPDDL.g:7950:1: rule__Either__EitherType2Assignment_3 : ( ruleType ) ;
    public final void rule__Either__EitherType2Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7954:1: ( ( ruleType ) )
            // InternalPDDL.g:7955:2: ( ruleType )
            {
            // InternalPDDL.g:7955:2: ( ruleType )
            // InternalPDDL.g:7956:3: ruleType
            {
             before(grammarAccess.getEitherAccess().getEitherType2TypeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getEitherAccess().getEitherType2TypeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Either__EitherType2Assignment_3"


    // $ANTLR start "rule__Variable__VariableNameAssignment"
    // InternalPDDL.g:7965:1: rule__Variable__VariableNameAssignment : ( RULE_IDWITHQUESTIONMARKBEFORE ) ;
    public final void rule__Variable__VariableNameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7969:1: ( ( RULE_IDWITHQUESTIONMARKBEFORE ) )
            // InternalPDDL.g:7970:2: ( RULE_IDWITHQUESTIONMARKBEFORE )
            {
            // InternalPDDL.g:7970:2: ( RULE_IDWITHQUESTIONMARKBEFORE )
            // InternalPDDL.g:7971:3: RULE_IDWITHQUESTIONMARKBEFORE
            {
             before(grammarAccess.getVariableAccess().getVariableNameIDWITHQUESTIONMARKBEFORETerminalRuleCall_0()); 
            match(input,RULE_IDWITHQUESTIONMARKBEFORE,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getVariableNameIDWITHQUESTIONMARKBEFORETerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__VariableNameAssignment"


    // $ANTLR start "rule__Functions__FunctionListAssignment_2"
    // InternalPDDL.g:7980:1: rule__Functions__FunctionListAssignment_2 : ( ruleDeclPFList ) ;
    public final void rule__Functions__FunctionListAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7984:1: ( ( ruleDeclPFList ) )
            // InternalPDDL.g:7985:2: ( ruleDeclPFList )
            {
            // InternalPDDL.g:7985:2: ( ruleDeclPFList )
            // InternalPDDL.g:7986:3: ruleDeclPFList
            {
             before(grammarAccess.getFunctionsAccess().getFunctionListDeclPFListParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDeclPFList();

            state._fsp--;

             after(grammarAccess.getFunctionsAccess().getFunctionListDeclPFListParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Functions__FunctionListAssignment_2"


    // $ANTLR start "rule__PDDLAction__ActionNameAssignment_2"
    // InternalPDDL.g:7995:1: rule__PDDLAction__ActionNameAssignment_2 : ( RULE_NAME ) ;
    public final void rule__PDDLAction__ActionNameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:7999:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:8000:2: ( RULE_NAME )
            {
            // InternalPDDL.g:8000:2: ( RULE_NAME )
            // InternalPDDL.g:8001:3: RULE_NAME
            {
             before(grammarAccess.getPDDLActionAccess().getActionNameNAMETerminalRuleCall_2_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getPDDLActionAccess().getActionNameNAMETerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__ActionNameAssignment_2"


    // $ANTLR start "rule__PDDLAction__ParametersAssignment_3"
    // InternalPDDL.g:8010:1: rule__PDDLAction__ParametersAssignment_3 : ( ruleParameters ) ;
    public final void rule__PDDLAction__ParametersAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8014:1: ( ( ruleParameters ) )
            // InternalPDDL.g:8015:2: ( ruleParameters )
            {
            // InternalPDDL.g:8015:2: ( ruleParameters )
            // InternalPDDL.g:8016:3: ruleParameters
            {
             before(grammarAccess.getPDDLActionAccess().getParametersParametersParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleParameters();

            state._fsp--;

             after(grammarAccess.getPDDLActionAccess().getParametersParametersParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__ParametersAssignment_3"


    // $ANTLR start "rule__PDDLAction__PreconditionsAssignment_4"
    // InternalPDDL.g:8025:1: rule__PDDLAction__PreconditionsAssignment_4 : ( rulePreconditions ) ;
    public final void rule__PDDLAction__PreconditionsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8029:1: ( ( rulePreconditions ) )
            // InternalPDDL.g:8030:2: ( rulePreconditions )
            {
            // InternalPDDL.g:8030:2: ( rulePreconditions )
            // InternalPDDL.g:8031:3: rulePreconditions
            {
             before(grammarAccess.getPDDLActionAccess().getPreconditionsPreconditionsParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            rulePreconditions();

            state._fsp--;

             after(grammarAccess.getPDDLActionAccess().getPreconditionsPreconditionsParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__PreconditionsAssignment_4"


    // $ANTLR start "rule__PDDLAction__EffectsAssignment_5"
    // InternalPDDL.g:8040:1: rule__PDDLAction__EffectsAssignment_5 : ( ruleEffects ) ;
    public final void rule__PDDLAction__EffectsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8044:1: ( ( ruleEffects ) )
            // InternalPDDL.g:8045:2: ( ruleEffects )
            {
            // InternalPDDL.g:8045:2: ( ruleEffects )
            // InternalPDDL.g:8046:3: ruleEffects
            {
             before(grammarAccess.getPDDLActionAccess().getEffectsEffectsParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleEffects();

            state._fsp--;

             after(grammarAccess.getPDDLActionAccess().getEffectsEffectsParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLAction__EffectsAssignment_5"


    // $ANTLR start "rule__Parameters__ParametersListAssignment_2"
    // InternalPDDL.g:8055:1: rule__Parameters__ParametersListAssignment_2 : ( ruleTypedVariablesList ) ;
    public final void rule__Parameters__ParametersListAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8059:1: ( ( ruleTypedVariablesList ) )
            // InternalPDDL.g:8060:2: ( ruleTypedVariablesList )
            {
            // InternalPDDL.g:8060:2: ( ruleTypedVariablesList )
            // InternalPDDL.g:8061:3: ruleTypedVariablesList
            {
             before(grammarAccess.getParametersAccess().getParametersListTypedVariablesListParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTypedVariablesList();

            state._fsp--;

             after(grammarAccess.getParametersAccess().getParametersListTypedVariablesListParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameters__ParametersListAssignment_2"


    // $ANTLR start "rule__Preconditions__LeAssignment_1"
    // InternalPDDL.g:8070:1: rule__Preconditions__LeAssignment_1 : ( ruleLogicalExpressionDM ) ;
    public final void rule__Preconditions__LeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8074:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8075:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8075:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8076:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getPreconditionsAccess().getLeLogicalExpressionDMParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getPreconditionsAccess().getLeLogicalExpressionDMParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Preconditions__LeAssignment_1"


    // $ANTLR start "rule__Effects__LeAssignment_1"
    // InternalPDDL.g:8085:1: rule__Effects__LeAssignment_1 : ( ruleLogicalExpressionDM ) ;
    public final void rule__Effects__LeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8089:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8090:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8090:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8091:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getEffectsAccess().getLeLogicalExpressionDMParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getEffectsAccess().getLeLogicalExpressionDMParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effects__LeAssignment_1"


    // $ANTLR start "rule__LogicalExpressionDM__PAssignment_0_1_1"
    // InternalPDDL.g:8100:1: rule__LogicalExpressionDM__PAssignment_0_1_1 : ( rulePredicatDM ) ;
    public final void rule__LogicalExpressionDM__PAssignment_0_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8104:1: ( ( rulePredicatDM ) )
            // InternalPDDL.g:8105:2: ( rulePredicatDM )
            {
            // InternalPDDL.g:8105:2: ( rulePredicatDM )
            // InternalPDDL.g:8106:3: rulePredicatDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getPPredicatDMParserRuleCall_0_1_1_0()); 
            pushFollow(FOLLOW_2);
            rulePredicatDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getPPredicatDMParserRuleCall_0_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__PAssignment_0_1_1"


    // $ANTLR start "rule__LogicalExpressionDM__VAssignment_1"
    // InternalPDDL.g:8115:1: rule__LogicalExpressionDM__VAssignment_1 : ( ruleVariable ) ;
    public final void rule__LogicalExpressionDM__VAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8119:1: ( ( ruleVariable ) )
            // InternalPDDL.g:8120:2: ( ruleVariable )
            {
            // InternalPDDL.g:8120:2: ( ruleVariable )
            // InternalPDDL.g:8121:3: ruleVariable
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getVVariableParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getVVariableParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__VAssignment_1"


    // $ANTLR start "rule__LogicalExpressionDM__LeAssignment_2_1"
    // InternalPDDL.g:8130:1: rule__LogicalExpressionDM__LeAssignment_2_1 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8134:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8135:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8135:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8136:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeLogicalExpressionDMParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeLogicalExpressionDMParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeAssignment_2_1"


    // $ANTLR start "rule__LogicalExpressionDM__VEqual1Assignment_3_2"
    // InternalPDDL.g:8145:1: rule__LogicalExpressionDM__VEqual1Assignment_3_2 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__VEqual1Assignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8149:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8150:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8150:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8151:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getVEqual1LogicalExpressionDMParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getVEqual1LogicalExpressionDMParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__VEqual1Assignment_3_2"


    // $ANTLR start "rule__LogicalExpressionDM__VEqual2Assignment_3_3"
    // InternalPDDL.g:8160:1: rule__LogicalExpressionDM__VEqual2Assignment_3_3 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__VEqual2Assignment_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8164:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8165:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8165:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8166:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getVEqual2LogicalExpressionDMParserRuleCall_3_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getVEqual2LogicalExpressionDMParserRuleCall_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__VEqual2Assignment_3_3"


    // $ANTLR start "rule__LogicalExpressionDM__LeNotAssignment_4_2"
    // InternalPDDL.g:8175:1: rule__LogicalExpressionDM__LeNotAssignment_4_2 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeNotAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8179:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8180:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8180:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8181:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeNotLogicalExpressionDMParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeNotLogicalExpressionDMParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeNotAssignment_4_2"


    // $ANTLR start "rule__LogicalExpressionDM__LeAnd1Assignment_5_2"
    // InternalPDDL.g:8190:1: rule__LogicalExpressionDM__LeAnd1Assignment_5_2 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeAnd1Assignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8194:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8195:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8195:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8196:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeAnd1LogicalExpressionDMParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeAnd1LogicalExpressionDMParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeAnd1Assignment_5_2"


    // $ANTLR start "rule__LogicalExpressionDM__LeAnd2Assignment_5_3"
    // InternalPDDL.g:8205:1: rule__LogicalExpressionDM__LeAnd2Assignment_5_3 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeAnd2Assignment_5_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8209:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8210:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8210:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8211:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeAnd2LogicalExpressionDMParserRuleCall_5_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeAnd2LogicalExpressionDMParserRuleCall_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeAnd2Assignment_5_3"


    // $ANTLR start "rule__LogicalExpressionDM__LeOr1Assignment_6_2"
    // InternalPDDL.g:8220:1: rule__LogicalExpressionDM__LeOr1Assignment_6_2 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeOr1Assignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8224:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8225:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8225:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8226:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeOr1LogicalExpressionDMParserRuleCall_6_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeOr1LogicalExpressionDMParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeOr1Assignment_6_2"


    // $ANTLR start "rule__LogicalExpressionDM__LeOr2Assignment_6_3"
    // InternalPDDL.g:8235:1: rule__LogicalExpressionDM__LeOr2Assignment_6_3 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeOr2Assignment_6_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8239:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8240:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8240:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8241:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeOr2LogicalExpressionDMParserRuleCall_6_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeOr2LogicalExpressionDMParserRuleCall_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeOr2Assignment_6_3"


    // $ANTLR start "rule__LogicalExpressionDM__LeImply1Assignment_7_2"
    // InternalPDDL.g:8250:1: rule__LogicalExpressionDM__LeImply1Assignment_7_2 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeImply1Assignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8254:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8255:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8255:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8256:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeImply1LogicalExpressionDMParserRuleCall_7_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeImply1LogicalExpressionDMParserRuleCall_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeImply1Assignment_7_2"


    // $ANTLR start "rule__LogicalExpressionDM__LeImply2Assignment_7_3"
    // InternalPDDL.g:8265:1: rule__LogicalExpressionDM__LeImply2Assignment_7_3 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeImply2Assignment_7_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8269:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8270:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8270:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8271:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeImply2LogicalExpressionDMParserRuleCall_7_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeImply2LogicalExpressionDMParserRuleCall_7_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeImply2Assignment_7_3"


    // $ANTLR start "rule__LogicalExpressionDM__TdForallAssignment_8_2"
    // InternalPDDL.g:8280:1: rule__LogicalExpressionDM__TdForallAssignment_8_2 : ( ruleTypeDescr ) ;
    public final void rule__LogicalExpressionDM__TdForallAssignment_8_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8284:1: ( ( ruleTypeDescr ) )
            // InternalPDDL.g:8285:2: ( ruleTypeDescr )
            {
            // InternalPDDL.g:8285:2: ( ruleTypeDescr )
            // InternalPDDL.g:8286:3: ruleTypeDescr
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getTdForallTypeDescrParserRuleCall_8_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeDescr();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getTdForallTypeDescrParserRuleCall_8_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__TdForallAssignment_8_2"


    // $ANTLR start "rule__LogicalExpressionDM__LeForallAssignment_8_3"
    // InternalPDDL.g:8295:1: rule__LogicalExpressionDM__LeForallAssignment_8_3 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeForallAssignment_8_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8299:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8300:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8300:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8301:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeForallLogicalExpressionDMParserRuleCall_8_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeForallLogicalExpressionDMParserRuleCall_8_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeForallAssignment_8_3"


    // $ANTLR start "rule__LogicalExpressionDM__TdExistsAssignment_9_2"
    // InternalPDDL.g:8310:1: rule__LogicalExpressionDM__TdExistsAssignment_9_2 : ( ruleTypeDescr ) ;
    public final void rule__LogicalExpressionDM__TdExistsAssignment_9_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8314:1: ( ( ruleTypeDescr ) )
            // InternalPDDL.g:8315:2: ( ruleTypeDescr )
            {
            // InternalPDDL.g:8315:2: ( ruleTypeDescr )
            // InternalPDDL.g:8316:3: ruleTypeDescr
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getTdExistsTypeDescrParserRuleCall_9_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeDescr();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getTdExistsTypeDescrParserRuleCall_9_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__TdExistsAssignment_9_2"


    // $ANTLR start "rule__LogicalExpressionDM__LeExistsAssignment_9_3"
    // InternalPDDL.g:8325:1: rule__LogicalExpressionDM__LeExistsAssignment_9_3 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeExistsAssignment_9_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8329:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8330:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8330:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8331:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeExistsLogicalExpressionDMParserRuleCall_9_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeExistsLogicalExpressionDMParserRuleCall_9_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeExistsAssignment_9_3"


    // $ANTLR start "rule__LogicalExpressionDM__LeWhen1Assignment_10_2"
    // InternalPDDL.g:8340:1: rule__LogicalExpressionDM__LeWhen1Assignment_10_2 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeWhen1Assignment_10_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8344:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8345:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8345:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8346:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeWhen1LogicalExpressionDMParserRuleCall_10_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeWhen1LogicalExpressionDMParserRuleCall_10_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeWhen1Assignment_10_2"


    // $ANTLR start "rule__LogicalExpressionDM__LeWhen2Assignment_10_3"
    // InternalPDDL.g:8355:1: rule__LogicalExpressionDM__LeWhen2Assignment_10_3 : ( ruleLogicalExpressionDM ) ;
    public final void rule__LogicalExpressionDM__LeWhen2Assignment_10_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8359:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8360:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8360:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8361:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getLogicalExpressionDMAccess().getLeWhen2LogicalExpressionDMParserRuleCall_10_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionDMAccess().getLeWhen2LogicalExpressionDMParserRuleCall_10_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionDM__LeWhen2Assignment_10_3"


    // $ANTLR start "rule__PredicatDM__PredicatNameAssignment_0"
    // InternalPDDL.g:8370:1: rule__PredicatDM__PredicatNameAssignment_0 : ( RULE_NAME ) ;
    public final void rule__PredicatDM__PredicatNameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8374:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:8375:2: ( RULE_NAME )
            {
            // InternalPDDL.g:8375:2: ( RULE_NAME )
            // InternalPDDL.g:8376:3: RULE_NAME
            {
             before(grammarAccess.getPredicatDMAccess().getPredicatNameNAMETerminalRuleCall_0_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getPredicatDMAccess().getPredicatNameNAMETerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatDM__PredicatNameAssignment_0"


    // $ANTLR start "rule__PredicatDM__VariablesListAssignment_1"
    // InternalPDDL.g:8385:1: rule__PredicatDM__VariablesListAssignment_1 : ( ruleVariable ) ;
    public final void rule__PredicatDM__VariablesListAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8389:1: ( ( ruleVariable ) )
            // InternalPDDL.g:8390:2: ( ruleVariable )
            {
            // InternalPDDL.g:8390:2: ( ruleVariable )
            // InternalPDDL.g:8391:3: ruleVariable
            {
             before(grammarAccess.getPredicatDMAccess().getVariablesListVariableParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getPredicatDMAccess().getVariablesListVariableParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatDM__VariablesListAssignment_1"


    // $ANTLR start "rule__TypeDescr__VarAssignment_1"
    // InternalPDDL.g:8400:1: rule__TypeDescr__VarAssignment_1 : ( ruleVariable ) ;
    public final void rule__TypeDescr__VarAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8404:1: ( ( ruleVariable ) )
            // InternalPDDL.g:8405:2: ( ruleVariable )
            {
            // InternalPDDL.g:8405:2: ( ruleVariable )
            // InternalPDDL.g:8406:3: ruleVariable
            {
             before(grammarAccess.getTypeDescrAccess().getVarVariableParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getTypeDescrAccess().getVarVariableParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__VarAssignment_1"


    // $ANTLR start "rule__TypeDescr__TypeAssignment_3"
    // InternalPDDL.g:8415:1: rule__TypeDescr__TypeAssignment_3 : ( ruleType ) ;
    public final void rule__TypeDescr__TypeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8419:1: ( ( ruleType ) )
            // InternalPDDL.g:8420:2: ( ruleType )
            {
            // InternalPDDL.g:8420:2: ( ruleType )
            // InternalPDDL.g:8421:3: ruleType
            {
             before(grammarAccess.getTypeDescrAccess().getTypeTypeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeDescrAccess().getTypeTypeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeDescr__TypeAssignment_3"


    // $ANTLR start "rule__Axiom__AxiomNameAssignment_2"
    // InternalPDDL.g:8430:1: rule__Axiom__AxiomNameAssignment_2 : ( RULE_NAME ) ;
    public final void rule__Axiom__AxiomNameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8434:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:8435:2: ( RULE_NAME )
            {
            // InternalPDDL.g:8435:2: ( RULE_NAME )
            // InternalPDDL.g:8436:3: RULE_NAME
            {
             before(grammarAccess.getAxiomAccess().getAxiomNameNAMETerminalRuleCall_2_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getAxiomAccess().getAxiomNameNAMETerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__AxiomNameAssignment_2"


    // $ANTLR start "rule__Axiom__VarsAssignment_3"
    // InternalPDDL.g:8445:1: rule__Axiom__VarsAssignment_3 : ( ruleVars ) ;
    public final void rule__Axiom__VarsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8449:1: ( ( ruleVars ) )
            // InternalPDDL.g:8450:2: ( ruleVars )
            {
            // InternalPDDL.g:8450:2: ( ruleVars )
            // InternalPDDL.g:8451:3: ruleVars
            {
             before(grammarAccess.getAxiomAccess().getVarsVarsParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleVars();

            state._fsp--;

             after(grammarAccess.getAxiomAccess().getVarsVarsParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__VarsAssignment_3"


    // $ANTLR start "rule__Axiom__ContextAssignment_4"
    // InternalPDDL.g:8460:1: rule__Axiom__ContextAssignment_4 : ( ruleContext ) ;
    public final void rule__Axiom__ContextAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8464:1: ( ( ruleContext ) )
            // InternalPDDL.g:8465:2: ( ruleContext )
            {
            // InternalPDDL.g:8465:2: ( ruleContext )
            // InternalPDDL.g:8466:3: ruleContext
            {
             before(grammarAccess.getAxiomAccess().getContextContextParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleContext();

            state._fsp--;

             after(grammarAccess.getAxiomAccess().getContextContextParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__ContextAssignment_4"


    // $ANTLR start "rule__Axiom__ImpliesAssignment_5"
    // InternalPDDL.g:8475:1: rule__Axiom__ImpliesAssignment_5 : ( ruleImplies ) ;
    public final void rule__Axiom__ImpliesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8479:1: ( ( ruleImplies ) )
            // InternalPDDL.g:8480:2: ( ruleImplies )
            {
            // InternalPDDL.g:8480:2: ( ruleImplies )
            // InternalPDDL.g:8481:3: ruleImplies
            {
             before(grammarAccess.getAxiomAccess().getImpliesImpliesParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getAxiomAccess().getImpliesImpliesParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Axiom__ImpliesAssignment_5"


    // $ANTLR start "rule__Context__LeAssignment_2"
    // InternalPDDL.g:8490:1: rule__Context__LeAssignment_2 : ( ruleLogicalExpressionDM ) ;
    public final void rule__Context__LeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8494:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8495:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8495:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8496:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getContextAccess().getLeLogicalExpressionDMParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getContextAccess().getLeLogicalExpressionDMParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context__LeAssignment_2"


    // $ANTLR start "rule__Implies__LeAssignment_2"
    // InternalPDDL.g:8505:1: rule__Implies__LeAssignment_2 : ( ruleLogicalExpressionDM ) ;
    public final void rule__Implies__LeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8509:1: ( ( ruleLogicalExpressionDM ) )
            // InternalPDDL.g:8510:2: ( ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:8510:2: ( ruleLogicalExpressionDM )
            // InternalPDDL.g:8511:3: ruleLogicalExpressionDM
            {
             before(grammarAccess.getImpliesAccess().getLeLogicalExpressionDMParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionDM();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getLeLogicalExpressionDMParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__LeAssignment_2"


    // $ANTLR start "rule__Problem__DomainRefAssignment_0"
    // InternalPDDL.g:8520:1: rule__Problem__DomainRefAssignment_0 : ( ruleDomainRef ) ;
    public final void rule__Problem__DomainRefAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8524:1: ( ( ruleDomainRef ) )
            // InternalPDDL.g:8525:2: ( ruleDomainRef )
            {
            // InternalPDDL.g:8525:2: ( ruleDomainRef )
            // InternalPDDL.g:8526:3: ruleDomainRef
            {
             before(grammarAccess.getProblemAccess().getDomainRefDomainRefParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDomainRef();

            state._fsp--;

             after(grammarAccess.getProblemAccess().getDomainRefDomainRefParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__DomainRefAssignment_0"


    // $ANTLR start "rule__Problem__ObjectsAssignment_1"
    // InternalPDDL.g:8535:1: rule__Problem__ObjectsAssignment_1 : ( ruleObjects ) ;
    public final void rule__Problem__ObjectsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8539:1: ( ( ruleObjects ) )
            // InternalPDDL.g:8540:2: ( ruleObjects )
            {
            // InternalPDDL.g:8540:2: ( ruleObjects )
            // InternalPDDL.g:8541:3: ruleObjects
            {
             before(grammarAccess.getProblemAccess().getObjectsObjectsParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleObjects();

            state._fsp--;

             after(grammarAccess.getProblemAccess().getObjectsObjectsParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__ObjectsAssignment_1"


    // $ANTLR start "rule__Problem__InitAssignment_2"
    // InternalPDDL.g:8550:1: rule__Problem__InitAssignment_2 : ( ruleInit ) ;
    public final void rule__Problem__InitAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8554:1: ( ( ruleInit ) )
            // InternalPDDL.g:8555:2: ( ruleInit )
            {
            // InternalPDDL.g:8555:2: ( ruleInit )
            // InternalPDDL.g:8556:3: ruleInit
            {
             before(grammarAccess.getProblemAccess().getInitInitParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInit();

            state._fsp--;

             after(grammarAccess.getProblemAccess().getInitInitParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__InitAssignment_2"


    // $ANTLR start "rule__Problem__GoalAssignment_3"
    // InternalPDDL.g:8565:1: rule__Problem__GoalAssignment_3 : ( ruleGoal ) ;
    public final void rule__Problem__GoalAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8569:1: ( ( ruleGoal ) )
            // InternalPDDL.g:8570:2: ( ruleGoal )
            {
            // InternalPDDL.g:8570:2: ( ruleGoal )
            // InternalPDDL.g:8571:3: ruleGoal
            {
             before(grammarAccess.getProblemAccess().getGoalGoalParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleGoal();

            state._fsp--;

             after(grammarAccess.getProblemAccess().getGoalGoalParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Problem__GoalAssignment_3"


    // $ANTLR start "rule__DomainRef__DomainNameAssignment_2"
    // InternalPDDL.g:8580:1: rule__DomainRef__DomainNameAssignment_2 : ( RULE_NAME ) ;
    public final void rule__DomainRef__DomainNameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8584:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:8585:2: ( RULE_NAME )
            {
            // InternalPDDL.g:8585:2: ( RULE_NAME )
            // InternalPDDL.g:8586:3: RULE_NAME
            {
             before(grammarAccess.getDomainRefAccess().getDomainNameNAMETerminalRuleCall_2_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getDomainRefAccess().getDomainNameNAMETerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DomainRef__DomainNameAssignment_2"


    // $ANTLR start "rule__Objects__NewObjectsListAssignment_2"
    // InternalPDDL.g:8595:1: rule__Objects__NewObjectsListAssignment_2 : ( ruleObjectsList ) ;
    public final void rule__Objects__NewObjectsListAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8599:1: ( ( ruleObjectsList ) )
            // InternalPDDL.g:8600:2: ( ruleObjectsList )
            {
            // InternalPDDL.g:8600:2: ( ruleObjectsList )
            // InternalPDDL.g:8601:3: ruleObjectsList
            {
             before(grammarAccess.getObjectsAccess().getNewObjectsListObjectsListParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleObjectsList();

            state._fsp--;

             after(grammarAccess.getObjectsAccess().getNewObjectsListObjectsListParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objects__NewObjectsListAssignment_2"


    // $ANTLR start "rule__ObjectsList__NewObjAssignment"
    // InternalPDDL.g:8610:1: rule__ObjectsList__NewObjAssignment : ( ruleNewObjects ) ;
    public final void rule__ObjectsList__NewObjAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8614:1: ( ( ruleNewObjects ) )
            // InternalPDDL.g:8615:2: ( ruleNewObjects )
            {
            // InternalPDDL.g:8615:2: ( ruleNewObjects )
            // InternalPDDL.g:8616:3: ruleNewObjects
            {
             before(grammarAccess.getObjectsListAccess().getNewObjNewObjectsParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleNewObjects();

            state._fsp--;

             after(grammarAccess.getObjectsListAccess().getNewObjNewObjectsParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectsList__NewObjAssignment"


    // $ANTLR start "rule__NewObjects__NewObjectAssignment_0"
    // InternalPDDL.g:8625:1: rule__NewObjects__NewObjectAssignment_0 : ( rulePDDLObject ) ;
    public final void rule__NewObjects__NewObjectAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8629:1: ( ( rulePDDLObject ) )
            // InternalPDDL.g:8630:2: ( rulePDDLObject )
            {
            // InternalPDDL.g:8630:2: ( rulePDDLObject )
            // InternalPDDL.g:8631:3: rulePDDLObject
            {
             before(grammarAccess.getNewObjectsAccess().getNewObjectPDDLObjectParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            rulePDDLObject();

            state._fsp--;

             after(grammarAccess.getNewObjectsAccess().getNewObjectPDDLObjectParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewObjects__NewObjectAssignment_0"


    // $ANTLR start "rule__NewObjects__ObjectTypeAssignment_2"
    // InternalPDDL.g:8640:1: rule__NewObjects__ObjectTypeAssignment_2 : ( ruleType ) ;
    public final void rule__NewObjects__ObjectTypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8644:1: ( ( ruleType ) )
            // InternalPDDL.g:8645:2: ( ruleType )
            {
            // InternalPDDL.g:8645:2: ( ruleType )
            // InternalPDDL.g:8646:3: ruleType
            {
             before(grammarAccess.getNewObjectsAccess().getObjectTypeTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getNewObjectsAccess().getObjectTypeTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NewObjects__ObjectTypeAssignment_2"


    // $ANTLR start "rule__Init__PredicatAssignment_3_0"
    // InternalPDDL.g:8655:1: rule__Init__PredicatAssignment_3_0 : ( rulePredicatPB ) ;
    public final void rule__Init__PredicatAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8659:1: ( ( rulePredicatPB ) )
            // InternalPDDL.g:8660:2: ( rulePredicatPB )
            {
            // InternalPDDL.g:8660:2: ( rulePredicatPB )
            // InternalPDDL.g:8661:3: rulePredicatPB
            {
             before(grammarAccess.getInitAccess().getPredicatPredicatPBParserRuleCall_3_0_0()); 
            pushFollow(FOLLOW_2);
            rulePredicatPB();

            state._fsp--;

             after(grammarAccess.getInitAccess().getPredicatPredicatPBParserRuleCall_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__PredicatAssignment_3_0"


    // $ANTLR start "rule__Init__NegativePredicatAssignment_3_1_1"
    // InternalPDDL.g:8670:1: rule__Init__NegativePredicatAssignment_3_1_1 : ( rulePredicatPB ) ;
    public final void rule__Init__NegativePredicatAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8674:1: ( ( rulePredicatPB ) )
            // InternalPDDL.g:8675:2: ( rulePredicatPB )
            {
            // InternalPDDL.g:8675:2: ( rulePredicatPB )
            // InternalPDDL.g:8676:3: rulePredicatPB
            {
             before(grammarAccess.getInitAccess().getNegativePredicatPredicatPBParserRuleCall_3_1_1_0()); 
            pushFollow(FOLLOW_2);
            rulePredicatPB();

            state._fsp--;

             after(grammarAccess.getInitAccess().getNegativePredicatPredicatPBParserRuleCall_3_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Init__NegativePredicatAssignment_3_1_1"


    // $ANTLR start "rule__PredicatPB__PredicatNameAssignment_1"
    // InternalPDDL.g:8685:1: rule__PredicatPB__PredicatNameAssignment_1 : ( RULE_NAME ) ;
    public final void rule__PredicatPB__PredicatNameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8689:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:8690:2: ( RULE_NAME )
            {
            // InternalPDDL.g:8690:2: ( RULE_NAME )
            // InternalPDDL.g:8691:3: RULE_NAME
            {
             before(grammarAccess.getPredicatPBAccess().getPredicatNameNAMETerminalRuleCall_1_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getPredicatPBAccess().getPredicatNameNAMETerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__PredicatNameAssignment_1"


    // $ANTLR start "rule__PredicatPB__ObjectsAssignment_2"
    // InternalPDDL.g:8700:1: rule__PredicatPB__ObjectsAssignment_2 : ( rulePDDLObject ) ;
    public final void rule__PredicatPB__ObjectsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8704:1: ( ( rulePDDLObject ) )
            // InternalPDDL.g:8705:2: ( rulePDDLObject )
            {
            // InternalPDDL.g:8705:2: ( rulePDDLObject )
            // InternalPDDL.g:8706:3: rulePDDLObject
            {
             before(grammarAccess.getPredicatPBAccess().getObjectsPDDLObjectParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            rulePDDLObject();

            state._fsp--;

             after(grammarAccess.getPredicatPBAccess().getObjectsPDDLObjectParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredicatPB__ObjectsAssignment_2"


    // $ANTLR start "rule__PDDLObject__ObjectNameAssignment"
    // InternalPDDL.g:8715:1: rule__PDDLObject__ObjectNameAssignment : ( RULE_NAME ) ;
    public final void rule__PDDLObject__ObjectNameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8719:1: ( ( RULE_NAME ) )
            // InternalPDDL.g:8720:2: ( RULE_NAME )
            {
            // InternalPDDL.g:8720:2: ( RULE_NAME )
            // InternalPDDL.g:8721:3: RULE_NAME
            {
             before(grammarAccess.getPDDLObjectAccess().getObjectNameNAMETerminalRuleCall_0()); 
            match(input,RULE_NAME,FOLLOW_2); 
             after(grammarAccess.getPDDLObjectAccess().getObjectNameNAMETerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PDDLObject__ObjectNameAssignment"


    // $ANTLR start "rule__Goal__LeGoalAssignment_2"
    // InternalPDDL.g:8730:1: rule__Goal__LeGoalAssignment_2 : ( ruleLogicalExpressionPB ) ;
    public final void rule__Goal__LeGoalAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8734:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8735:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8735:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8736:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getGoalAccess().getLeGoalLogicalExpressionPBParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getGoalAccess().getLeGoalLogicalExpressionPBParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Goal__LeGoalAssignment_2"


    // $ANTLR start "rule__LogicalExpressionPB__PAssignment_0"
    // InternalPDDL.g:8745:1: rule__LogicalExpressionPB__PAssignment_0 : ( rulePredicatPB ) ;
    public final void rule__LogicalExpressionPB__PAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8749:1: ( ( rulePredicatPB ) )
            // InternalPDDL.g:8750:2: ( rulePredicatPB )
            {
            // InternalPDDL.g:8750:2: ( rulePredicatPB )
            // InternalPDDL.g:8751:3: rulePredicatPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getPPredicatPBParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            rulePredicatPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getPPredicatPBParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__PAssignment_0"


    // $ANTLR start "rule__LogicalExpressionPB__VAssignment_1"
    // InternalPDDL.g:8760:1: rule__LogicalExpressionPB__VAssignment_1 : ( ruleVariable ) ;
    public final void rule__LogicalExpressionPB__VAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8764:1: ( ( ruleVariable ) )
            // InternalPDDL.g:8765:2: ( ruleVariable )
            {
            // InternalPDDL.g:8765:2: ( ruleVariable )
            // InternalPDDL.g:8766:3: ruleVariable
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getVVariableParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getVVariableParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__VAssignment_1"


    // $ANTLR start "rule__LogicalExpressionPB__LeAssignment_2_1"
    // InternalPDDL.g:8775:1: rule__LogicalExpressionPB__LeAssignment_2_1 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8779:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8780:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8780:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8781:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeLogicalExpressionPBParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeLogicalExpressionPBParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeAssignment_2_1"


    // $ANTLR start "rule__LogicalExpressionPB__VEqual1Assignment_3_2"
    // InternalPDDL.g:8790:1: rule__LogicalExpressionPB__VEqual1Assignment_3_2 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__VEqual1Assignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8794:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8795:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8795:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8796:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getVEqual1LogicalExpressionPBParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getVEqual1LogicalExpressionPBParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__VEqual1Assignment_3_2"


    // $ANTLR start "rule__LogicalExpressionPB__VEqual2Assignment_3_3"
    // InternalPDDL.g:8805:1: rule__LogicalExpressionPB__VEqual2Assignment_3_3 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__VEqual2Assignment_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8809:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8810:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8810:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8811:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getVEqual2LogicalExpressionPBParserRuleCall_3_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getVEqual2LogicalExpressionPBParserRuleCall_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__VEqual2Assignment_3_3"


    // $ANTLR start "rule__LogicalExpressionPB__LeNotAssignment_4_2"
    // InternalPDDL.g:8820:1: rule__LogicalExpressionPB__LeNotAssignment_4_2 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeNotAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8824:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8825:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8825:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8826:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeNotLogicalExpressionPBParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeNotLogicalExpressionPBParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeNotAssignment_4_2"


    // $ANTLR start "rule__LogicalExpressionPB__LeAnd1Assignment_5_2"
    // InternalPDDL.g:8835:1: rule__LogicalExpressionPB__LeAnd1Assignment_5_2 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeAnd1Assignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8839:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8840:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8840:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8841:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeAnd1LogicalExpressionPBParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeAnd1LogicalExpressionPBParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeAnd1Assignment_5_2"


    // $ANTLR start "rule__LogicalExpressionPB__LeAnd2Assignment_5_3"
    // InternalPDDL.g:8850:1: rule__LogicalExpressionPB__LeAnd2Assignment_5_3 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeAnd2Assignment_5_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8854:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8855:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8855:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8856:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeAnd2LogicalExpressionPBParserRuleCall_5_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeAnd2LogicalExpressionPBParserRuleCall_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeAnd2Assignment_5_3"


    // $ANTLR start "rule__LogicalExpressionPB__LeOr1Assignment_6_2"
    // InternalPDDL.g:8865:1: rule__LogicalExpressionPB__LeOr1Assignment_6_2 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeOr1Assignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8869:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8870:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8870:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8871:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeOr1LogicalExpressionPBParserRuleCall_6_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeOr1LogicalExpressionPBParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeOr1Assignment_6_2"


    // $ANTLR start "rule__LogicalExpressionPB__LeOr2Assignment_6_3"
    // InternalPDDL.g:8880:1: rule__LogicalExpressionPB__LeOr2Assignment_6_3 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeOr2Assignment_6_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8884:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8885:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8885:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8886:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeOr2LogicalExpressionPBParserRuleCall_6_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeOr2LogicalExpressionPBParserRuleCall_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeOr2Assignment_6_3"


    // $ANTLR start "rule__LogicalExpressionPB__LeImply1Assignment_7_2"
    // InternalPDDL.g:8895:1: rule__LogicalExpressionPB__LeImply1Assignment_7_2 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeImply1Assignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8899:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8900:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8900:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8901:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeImply1LogicalExpressionPBParserRuleCall_7_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeImply1LogicalExpressionPBParserRuleCall_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeImply1Assignment_7_2"


    // $ANTLR start "rule__LogicalExpressionPB__LeImply2Assignment_7_3"
    // InternalPDDL.g:8910:1: rule__LogicalExpressionPB__LeImply2Assignment_7_3 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeImply2Assignment_7_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8914:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8915:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8915:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8916:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeImply2LogicalExpressionPBParserRuleCall_7_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeImply2LogicalExpressionPBParserRuleCall_7_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeImply2Assignment_7_3"


    // $ANTLR start "rule__LogicalExpressionPB__TdForallAssignment_8_2"
    // InternalPDDL.g:8925:1: rule__LogicalExpressionPB__TdForallAssignment_8_2 : ( ruleTypeDescr ) ;
    public final void rule__LogicalExpressionPB__TdForallAssignment_8_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8929:1: ( ( ruleTypeDescr ) )
            // InternalPDDL.g:8930:2: ( ruleTypeDescr )
            {
            // InternalPDDL.g:8930:2: ( ruleTypeDescr )
            // InternalPDDL.g:8931:3: ruleTypeDescr
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getTdForallTypeDescrParserRuleCall_8_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeDescr();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getTdForallTypeDescrParserRuleCall_8_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__TdForallAssignment_8_2"


    // $ANTLR start "rule__LogicalExpressionPB__LeForallAssignment_8_3"
    // InternalPDDL.g:8940:1: rule__LogicalExpressionPB__LeForallAssignment_8_3 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeForallAssignment_8_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8944:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8945:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8945:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8946:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeForallLogicalExpressionPBParserRuleCall_8_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeForallLogicalExpressionPBParserRuleCall_8_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeForallAssignment_8_3"


    // $ANTLR start "rule__LogicalExpressionPB__TdExistsAssignment_9_2"
    // InternalPDDL.g:8955:1: rule__LogicalExpressionPB__TdExistsAssignment_9_2 : ( ruleTypeDescr ) ;
    public final void rule__LogicalExpressionPB__TdExistsAssignment_9_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8959:1: ( ( ruleTypeDescr ) )
            // InternalPDDL.g:8960:2: ( ruleTypeDescr )
            {
            // InternalPDDL.g:8960:2: ( ruleTypeDescr )
            // InternalPDDL.g:8961:3: ruleTypeDescr
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getTdExistsTypeDescrParserRuleCall_9_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeDescr();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getTdExistsTypeDescrParserRuleCall_9_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__TdExistsAssignment_9_2"


    // $ANTLR start "rule__LogicalExpressionPB__LeExistsAssignment_9_3"
    // InternalPDDL.g:8970:1: rule__LogicalExpressionPB__LeExistsAssignment_9_3 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeExistsAssignment_9_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8974:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8975:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8975:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8976:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeExistsLogicalExpressionPBParserRuleCall_9_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeExistsLogicalExpressionPBParserRuleCall_9_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeExistsAssignment_9_3"


    // $ANTLR start "rule__LogicalExpressionPB__LeWhen1Assignment_10_2"
    // InternalPDDL.g:8985:1: rule__LogicalExpressionPB__LeWhen1Assignment_10_2 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeWhen1Assignment_10_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:8989:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:8990:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:8990:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:8991:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeWhen1LogicalExpressionPBParserRuleCall_10_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeWhen1LogicalExpressionPBParserRuleCall_10_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeWhen1Assignment_10_2"


    // $ANTLR start "rule__LogicalExpressionPB__LeWhen2Assignment_10_3"
    // InternalPDDL.g:9000:1: rule__LogicalExpressionPB__LeWhen2Assignment_10_3 : ( ruleLogicalExpressionPB ) ;
    public final void rule__LogicalExpressionPB__LeWhen2Assignment_10_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPDDL.g:9004:1: ( ( ruleLogicalExpressionPB ) )
            // InternalPDDL.g:9005:2: ( ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:9005:2: ( ruleLogicalExpressionPB )
            // InternalPDDL.g:9006:3: ruleLogicalExpressionPB
            {
             before(grammarAccess.getLogicalExpressionPBAccess().getLeWhen2LogicalExpressionPBParserRuleCall_10_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpressionPB();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionPBAccess().getLeWhen2LogicalExpressionPBParserRuleCall_10_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpressionPB__LeWhen2Assignment_10_3"

    // Delegated rules


    protected DFA10 dfa10 = new DFA10(this);
    protected DFA12 dfa12 = new DFA12(this);
    protected DFA20 dfa20 = new DFA20(this);
    static final String dfa_1s = "\15\uffff";
    static final String dfa_2s = "\2\4\13\uffff";
    static final String dfa_3s = "\1\10\1\47\13\uffff";
    static final String dfa_4s = "\2\uffff\1\2\1\6\1\12\1\5\1\7\1\11\1\3\1\10\1\13\1\4\1\1";
    static final String dfa_5s = "\15\uffff}>";
    static final String[] dfa_6s = {
            "\1\1\3\uffff\1\2",
            "\1\10\1\uffff\1\14\1\uffff\1\10\27\uffff\1\13\1\5\1\3\1\6\1\11\1\7\1\4\1\12",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA10 extends DFA {

        public DFA10(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 10;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "1232:1: rule__LogicalExpressionDM__Alternatives : ( ( ( rule__LogicalExpressionDM__Group_0__0 ) ) | ( ( rule__LogicalExpressionDM__VAssignment_1 ) ) | ( ( rule__LogicalExpressionDM__Group_2__0 ) ) | ( ( rule__LogicalExpressionDM__Group_3__0 ) ) | ( ( rule__LogicalExpressionDM__Group_4__0 ) ) | ( ( rule__LogicalExpressionDM__Group_5__0 ) ) | ( ( rule__LogicalExpressionDM__Group_6__0 ) ) | ( ( rule__LogicalExpressionDM__Group_7__0 ) ) | ( ( rule__LogicalExpressionDM__Group_8__0 ) ) | ( ( rule__LogicalExpressionDM__Group_9__0 ) ) | ( ( rule__LogicalExpressionDM__Group_10__0 ) ) );";
        }
    }
    static final String dfa_7s = "\2\uffff\1\2\1\7\1\11\1\3\1\10\1\13\1\4\1\6\1\12\1\5\1\1";
    static final String[] dfa_8s = {
            "\1\1\3\uffff\1\2",
            "\1\5\1\uffff\1\14\1\uffff\1\5\27\uffff\1\10\1\13\1\11\1\3\1\6\1\4\1\12\1\7",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };
    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final short[][] dfa_8 = unpackEncodedStringArray(dfa_8s);

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_7;
            this.special = dfa_5;
            this.transition = dfa_8;
        }
        public String getDescription() {
            return "1328:1: rule__LogicalExpressionPB__Alternatives : ( ( ( rule__LogicalExpressionPB__PAssignment_0 ) ) | ( ( rule__LogicalExpressionPB__VAssignment_1 ) ) | ( ( rule__LogicalExpressionPB__Group_2__0 ) ) | ( ( rule__LogicalExpressionPB__Group_3__0 ) ) | ( ( rule__LogicalExpressionPB__Group_4__0 ) ) | ( ( rule__LogicalExpressionPB__Group_5__0 ) ) | ( ( rule__LogicalExpressionPB__Group_6__0 ) ) | ( ( rule__LogicalExpressionPB__Group_7__0 ) ) | ( ( rule__LogicalExpressionPB__Group_8__0 ) ) | ( ( rule__LogicalExpressionPB__Group_9__0 ) ) | ( ( rule__LogicalExpressionPB__Group_10__0 ) ) );";
        }
    }
    static final String dfa_9s = "\5\uffff";
    static final String dfa_10s = "\2\2\1\uffff\1\2\1\uffff";
    static final String dfa_11s = "\2\5\1\uffff\1\5\1\uffff";
    static final String dfa_12s = "\1\6\1\30\1\uffff\1\30\1\uffff";
    static final String dfa_13s = "\2\uffff\1\2\1\uffff\1\1";
    static final String dfa_14s = "\5\uffff}>";
    static final String[] dfa_15s = {
            "\1\2\1\1",
            "\1\2\1\3\21\uffff\1\4",
            "",
            "\1\2\1\3\21\uffff\1\4",
            ""
    };

    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final short[] dfa_10 = DFA.unpackEncodedString(dfa_10s);
    static final char[] dfa_11 = DFA.unpackEncodedStringToUnsignedChars(dfa_11s);
    static final char[] dfa_12 = DFA.unpackEncodedStringToUnsignedChars(dfa_12s);
    static final short[] dfa_13 = DFA.unpackEncodedString(dfa_13s);
    static final short[] dfa_14 = DFA.unpackEncodedString(dfa_14s);
    static final short[][] dfa_15 = unpackEncodedStringArray(dfa_15s);

    class DFA20 extends DFA {

        public DFA20(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 20;
            this.eot = dfa_9;
            this.eof = dfa_10;
            this.min = dfa_11;
            this.max = dfa_12;
            this.accept = dfa_13;
            this.special = dfa_14;
            this.transition = dfa_15;
        }
        public String getDescription() {
            return "()* loopback of 2394:2: ( rule__NewTypes__DeclInheritorTypesAssignment_1 )*";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000082L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x00000000C0000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000110L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000000112L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000200000030L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000200000012L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000800000000000L});

}