/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyragoal.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.atsyra.atsyragoal.xtext.services.AtsyRAGoalGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalAtsyRAGoalParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'AtsyraGoalModel'", "'{'", "'defaults'", "'}'", "'types'", "'features'", "'atsyragoals'", "'trees'", "'import'", "'or'", "'OR'", "'and'", "'AND'", "'('", "')'", "'not'", "'NOT'", "'='", "'const'", "'true'", "'false'", "'Goal'", "'pre'", "'with'", "':'", "'post'", "','", "'['", "']'", "'InternalType'", "'SystemType'", "'SAND'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalAtsyRAGoalParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalAtsyRAGoalParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalAtsyRAGoalParser.tokenNames; }
    public String getGrammarFileName() { return "InternalAtsyRAGoal.g"; }



     	private AtsyRAGoalGrammarAccess grammarAccess;

        public InternalAtsyRAGoalParser(TokenStream input, AtsyRAGoalGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "AtsyraGoalModel";
       	}

       	@Override
       	protected AtsyRAGoalGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleAtsyraGoalModel"
    // InternalAtsyRAGoal.g:65:1: entryRuleAtsyraGoalModel returns [EObject current=null] : iv_ruleAtsyraGoalModel= ruleAtsyraGoalModel EOF ;
    public final EObject entryRuleAtsyraGoalModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtsyraGoalModel = null;


        try {
            // InternalAtsyRAGoal.g:65:56: (iv_ruleAtsyraGoalModel= ruleAtsyraGoalModel EOF )
            // InternalAtsyRAGoal.g:66:2: iv_ruleAtsyraGoalModel= ruleAtsyraGoalModel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAtsyraGoalModelRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAtsyraGoalModel=ruleAtsyraGoalModel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAtsyraGoalModel; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtsyraGoalModel"


    // $ANTLR start "ruleAtsyraGoalModel"
    // InternalAtsyRAGoal.g:72:1: ruleAtsyraGoalModel returns [EObject current=null] : ( () ( (lv_imports_1_0= ruleImport ) )* otherlv_2= 'AtsyraGoalModel' otherlv_3= '{' (otherlv_4= 'defaults' otherlv_5= '{' ( (lv_defaultValues_6_0= ruleDefaultValues ) ) ( (lv_defaultValues_7_0= ruleDefaultValues ) )* otherlv_8= '}' )? (otherlv_9= 'types' otherlv_10= '{' ( (lv_types_11_0= ruleType ) ) ( (lv_types_12_0= ruleType ) )* otherlv_13= '}' )? (otherlv_14= 'features' otherlv_15= '{' ( (lv_typedElements_16_0= ruleTypedElementDecl ) ) ( (lv_typedElements_17_0= ruleTypedElementDecl ) )* otherlv_18= '}' )? (otherlv_19= 'atsyragoals' otherlv_20= '{' ( (lv_atsyragoals_21_0= ruleAtsyraGoal ) ) ( (lv_atsyragoals_22_0= ruleAtsyraGoal ) )* otherlv_23= '}' )? (otherlv_24= 'trees' otherlv_25= '{' ( (lv_trees_26_0= ruleAtsyraTree ) ) ( (lv_trees_27_0= ruleAtsyraTree ) )* otherlv_28= '}' )? otherlv_29= '}' ) ;
    public final EObject ruleAtsyraGoalModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        EObject lv_imports_1_0 = null;

        EObject lv_defaultValues_6_0 = null;

        EObject lv_defaultValues_7_0 = null;

        EObject lv_types_11_0 = null;

        EObject lv_types_12_0 = null;

        EObject lv_typedElements_16_0 = null;

        EObject lv_typedElements_17_0 = null;

        EObject lv_atsyragoals_21_0 = null;

        EObject lv_atsyragoals_22_0 = null;

        EObject lv_trees_26_0 = null;

        EObject lv_trees_27_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:78:2: ( ( () ( (lv_imports_1_0= ruleImport ) )* otherlv_2= 'AtsyraGoalModel' otherlv_3= '{' (otherlv_4= 'defaults' otherlv_5= '{' ( (lv_defaultValues_6_0= ruleDefaultValues ) ) ( (lv_defaultValues_7_0= ruleDefaultValues ) )* otherlv_8= '}' )? (otherlv_9= 'types' otherlv_10= '{' ( (lv_types_11_0= ruleType ) ) ( (lv_types_12_0= ruleType ) )* otherlv_13= '}' )? (otherlv_14= 'features' otherlv_15= '{' ( (lv_typedElements_16_0= ruleTypedElementDecl ) ) ( (lv_typedElements_17_0= ruleTypedElementDecl ) )* otherlv_18= '}' )? (otherlv_19= 'atsyragoals' otherlv_20= '{' ( (lv_atsyragoals_21_0= ruleAtsyraGoal ) ) ( (lv_atsyragoals_22_0= ruleAtsyraGoal ) )* otherlv_23= '}' )? (otherlv_24= 'trees' otherlv_25= '{' ( (lv_trees_26_0= ruleAtsyraTree ) ) ( (lv_trees_27_0= ruleAtsyraTree ) )* otherlv_28= '}' )? otherlv_29= '}' ) )
            // InternalAtsyRAGoal.g:79:2: ( () ( (lv_imports_1_0= ruleImport ) )* otherlv_2= 'AtsyraGoalModel' otherlv_3= '{' (otherlv_4= 'defaults' otherlv_5= '{' ( (lv_defaultValues_6_0= ruleDefaultValues ) ) ( (lv_defaultValues_7_0= ruleDefaultValues ) )* otherlv_8= '}' )? (otherlv_9= 'types' otherlv_10= '{' ( (lv_types_11_0= ruleType ) ) ( (lv_types_12_0= ruleType ) )* otherlv_13= '}' )? (otherlv_14= 'features' otherlv_15= '{' ( (lv_typedElements_16_0= ruleTypedElementDecl ) ) ( (lv_typedElements_17_0= ruleTypedElementDecl ) )* otherlv_18= '}' )? (otherlv_19= 'atsyragoals' otherlv_20= '{' ( (lv_atsyragoals_21_0= ruleAtsyraGoal ) ) ( (lv_atsyragoals_22_0= ruleAtsyraGoal ) )* otherlv_23= '}' )? (otherlv_24= 'trees' otherlv_25= '{' ( (lv_trees_26_0= ruleAtsyraTree ) ) ( (lv_trees_27_0= ruleAtsyraTree ) )* otherlv_28= '}' )? otherlv_29= '}' )
            {
            // InternalAtsyRAGoal.g:79:2: ( () ( (lv_imports_1_0= ruleImport ) )* otherlv_2= 'AtsyraGoalModel' otherlv_3= '{' (otherlv_4= 'defaults' otherlv_5= '{' ( (lv_defaultValues_6_0= ruleDefaultValues ) ) ( (lv_defaultValues_7_0= ruleDefaultValues ) )* otherlv_8= '}' )? (otherlv_9= 'types' otherlv_10= '{' ( (lv_types_11_0= ruleType ) ) ( (lv_types_12_0= ruleType ) )* otherlv_13= '}' )? (otherlv_14= 'features' otherlv_15= '{' ( (lv_typedElements_16_0= ruleTypedElementDecl ) ) ( (lv_typedElements_17_0= ruleTypedElementDecl ) )* otherlv_18= '}' )? (otherlv_19= 'atsyragoals' otherlv_20= '{' ( (lv_atsyragoals_21_0= ruleAtsyraGoal ) ) ( (lv_atsyragoals_22_0= ruleAtsyraGoal ) )* otherlv_23= '}' )? (otherlv_24= 'trees' otherlv_25= '{' ( (lv_trees_26_0= ruleAtsyraTree ) ) ( (lv_trees_27_0= ruleAtsyraTree ) )* otherlv_28= '}' )? otherlv_29= '}' )
            // InternalAtsyRAGoal.g:80:3: () ( (lv_imports_1_0= ruleImport ) )* otherlv_2= 'AtsyraGoalModel' otherlv_3= '{' (otherlv_4= 'defaults' otherlv_5= '{' ( (lv_defaultValues_6_0= ruleDefaultValues ) ) ( (lv_defaultValues_7_0= ruleDefaultValues ) )* otherlv_8= '}' )? (otherlv_9= 'types' otherlv_10= '{' ( (lv_types_11_0= ruleType ) ) ( (lv_types_12_0= ruleType ) )* otherlv_13= '}' )? (otherlv_14= 'features' otherlv_15= '{' ( (lv_typedElements_16_0= ruleTypedElementDecl ) ) ( (lv_typedElements_17_0= ruleTypedElementDecl ) )* otherlv_18= '}' )? (otherlv_19= 'atsyragoals' otherlv_20= '{' ( (lv_atsyragoals_21_0= ruleAtsyraGoal ) ) ( (lv_atsyragoals_22_0= ruleAtsyraGoal ) )* otherlv_23= '}' )? (otherlv_24= 'trees' otherlv_25= '{' ( (lv_trees_26_0= ruleAtsyraTree ) ) ( (lv_trees_27_0= ruleAtsyraTree ) )* otherlv_28= '}' )? otherlv_29= '}'
            {
            // InternalAtsyRAGoal.g:80:3: ()
            // InternalAtsyRAGoal.g:81:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getAtsyraGoalModelAccess().getAtsyraGoalModelAction_0(),
              					current);
              			
            }

            }

            // InternalAtsyRAGoal.g:87:3: ( (lv_imports_1_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==19) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:88:4: (lv_imports_1_0= ruleImport )
            	    {
            	    // InternalAtsyRAGoal.g:88:4: (lv_imports_1_0= ruleImport )
            	    // InternalAtsyRAGoal.g:89:5: lv_imports_1_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getImportsImportParserRuleCall_1_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_3);
            	    lv_imports_1_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
            	      					}
            	      					add(
            	      						current,
            	      						"imports",
            	      						lv_imports_1_0,
            	      						"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.Import");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_2=(Token)match(input,11,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getAtsyraGoalModelAccess().getAtsyraGoalModelKeyword_2());
              		
            }
            otherlv_3=(Token)match(input,12,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalAtsyRAGoal.g:114:3: (otherlv_4= 'defaults' otherlv_5= '{' ( (lv_defaultValues_6_0= ruleDefaultValues ) ) ( (lv_defaultValues_7_0= ruleDefaultValues ) )* otherlv_8= '}' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalAtsyRAGoal.g:115:4: otherlv_4= 'defaults' otherlv_5= '{' ( (lv_defaultValues_6_0= ruleDefaultValues ) ) ( (lv_defaultValues_7_0= ruleDefaultValues ) )* otherlv_8= '}'
                    {
                    otherlv_4=(Token)match(input,13,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getAtsyraGoalModelAccess().getDefaultsKeyword_4_0());
                      			
                    }
                    otherlv_5=(Token)match(input,12,FOLLOW_6); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_4_1());
                      			
                    }
                    // InternalAtsyRAGoal.g:123:4: ( (lv_defaultValues_6_0= ruleDefaultValues ) )
                    // InternalAtsyRAGoal.g:124:5: (lv_defaultValues_6_0= ruleDefaultValues )
                    {
                    // InternalAtsyRAGoal.g:124:5: (lv_defaultValues_6_0= ruleDefaultValues )
                    // InternalAtsyRAGoal.g:125:6: lv_defaultValues_6_0= ruleDefaultValues
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesDefaultValuesParserRuleCall_4_2_0());
                      					
                    }
                    pushFollow(FOLLOW_7);
                    lv_defaultValues_6_0=ruleDefaultValues();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                      						}
                      						add(
                      							current,
                      							"defaultValues",
                      							lv_defaultValues_6_0,
                      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.DefaultValues");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalAtsyRAGoal.g:142:4: ( (lv_defaultValues_7_0= ruleDefaultValues ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( ((LA2_0>=RULE_STRING && LA2_0<=RULE_ID)) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // InternalAtsyRAGoal.g:143:5: (lv_defaultValues_7_0= ruleDefaultValues )
                    	    {
                    	    // InternalAtsyRAGoal.g:143:5: (lv_defaultValues_7_0= ruleDefaultValues )
                    	    // InternalAtsyRAGoal.g:144:6: lv_defaultValues_7_0= ruleDefaultValues
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getDefaultValuesDefaultValuesParserRuleCall_4_3_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_7);
                    	    lv_defaultValues_7_0=ruleDefaultValues();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"defaultValues",
                    	      							lv_defaultValues_7_0,
                    	      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.DefaultValues");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,14,FOLLOW_8); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_8, grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_4_4());
                      			
                    }

                    }
                    break;

            }

            // InternalAtsyRAGoal.g:166:3: (otherlv_9= 'types' otherlv_10= '{' ( (lv_types_11_0= ruleType ) ) ( (lv_types_12_0= ruleType ) )* otherlv_13= '}' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalAtsyRAGoal.g:167:4: otherlv_9= 'types' otherlv_10= '{' ( (lv_types_11_0= ruleType ) ) ( (lv_types_12_0= ruleType ) )* otherlv_13= '}'
                    {
                    otherlv_9=(Token)match(input,15,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_9, grammarAccess.getAtsyraGoalModelAccess().getTypesKeyword_5_0());
                      			
                    }
                    otherlv_10=(Token)match(input,12,FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_10, grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_5_1());
                      			
                    }
                    // InternalAtsyRAGoal.g:175:4: ( (lv_types_11_0= ruleType ) )
                    // InternalAtsyRAGoal.g:176:5: (lv_types_11_0= ruleType )
                    {
                    // InternalAtsyRAGoal.g:176:5: (lv_types_11_0= ruleType )
                    // InternalAtsyRAGoal.g:177:6: lv_types_11_0= ruleType
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getTypesTypeParserRuleCall_5_2_0());
                      					
                    }
                    pushFollow(FOLLOW_10);
                    lv_types_11_0=ruleType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                      						}
                      						add(
                      							current,
                      							"types",
                      							lv_types_11_0,
                      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.Type");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalAtsyRAGoal.g:194:4: ( (lv_types_12_0= ruleType ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( ((LA4_0>=40 && LA4_0<=41)) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalAtsyRAGoal.g:195:5: (lv_types_12_0= ruleType )
                    	    {
                    	    // InternalAtsyRAGoal.g:195:5: (lv_types_12_0= ruleType )
                    	    // InternalAtsyRAGoal.g:196:6: lv_types_12_0= ruleType
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getTypesTypeParserRuleCall_5_3_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_10);
                    	    lv_types_12_0=ruleType();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"types",
                    	      							lv_types_12_0,
                    	      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.Type");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    otherlv_13=(Token)match(input,14,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_13, grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_5_4());
                      			
                    }

                    }
                    break;

            }

            // InternalAtsyRAGoal.g:218:3: (otherlv_14= 'features' otherlv_15= '{' ( (lv_typedElements_16_0= ruleTypedElementDecl ) ) ( (lv_typedElements_17_0= ruleTypedElementDecl ) )* otherlv_18= '}' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==16) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalAtsyRAGoal.g:219:4: otherlv_14= 'features' otherlv_15= '{' ( (lv_typedElements_16_0= ruleTypedElementDecl ) ) ( (lv_typedElements_17_0= ruleTypedElementDecl ) )* otherlv_18= '}'
                    {
                    otherlv_14=(Token)match(input,16,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_14, grammarAccess.getAtsyraGoalModelAccess().getFeaturesKeyword_6_0());
                      			
                    }
                    otherlv_15=(Token)match(input,12,FOLLOW_12); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_15, grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_6_1());
                      			
                    }
                    // InternalAtsyRAGoal.g:227:4: ( (lv_typedElements_16_0= ruleTypedElementDecl ) )
                    // InternalAtsyRAGoal.g:228:5: (lv_typedElements_16_0= ruleTypedElementDecl )
                    {
                    // InternalAtsyRAGoal.g:228:5: (lv_typedElements_16_0= ruleTypedElementDecl )
                    // InternalAtsyRAGoal.g:229:6: lv_typedElements_16_0= ruleTypedElementDecl
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsTypedElementDeclParserRuleCall_6_2_0());
                      					
                    }
                    pushFollow(FOLLOW_13);
                    lv_typedElements_16_0=ruleTypedElementDecl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                      						}
                      						add(
                      							current,
                      							"typedElements",
                      							lv_typedElements_16_0,
                      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.TypedElementDecl");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalAtsyRAGoal.g:246:4: ( (lv_typedElements_17_0= ruleTypedElementDecl ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0>=RULE_STRING && LA6_0<=RULE_ID)||LA6_0==29) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalAtsyRAGoal.g:247:5: (lv_typedElements_17_0= ruleTypedElementDecl )
                    	    {
                    	    // InternalAtsyRAGoal.g:247:5: (lv_typedElements_17_0= ruleTypedElementDecl )
                    	    // InternalAtsyRAGoal.g:248:6: lv_typedElements_17_0= ruleTypedElementDecl
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getTypedElementsTypedElementDeclParserRuleCall_6_3_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_13);
                    	    lv_typedElements_17_0=ruleTypedElementDecl();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"typedElements",
                    	      							lv_typedElements_17_0,
                    	      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.TypedElementDecl");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    otherlv_18=(Token)match(input,14,FOLLOW_14); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_18, grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_6_4());
                      			
                    }

                    }
                    break;

            }

            // InternalAtsyRAGoal.g:270:3: (otherlv_19= 'atsyragoals' otherlv_20= '{' ( (lv_atsyragoals_21_0= ruleAtsyraGoal ) ) ( (lv_atsyragoals_22_0= ruleAtsyraGoal ) )* otherlv_23= '}' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==17) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalAtsyRAGoal.g:271:4: otherlv_19= 'atsyragoals' otherlv_20= '{' ( (lv_atsyragoals_21_0= ruleAtsyraGoal ) ) ( (lv_atsyragoals_22_0= ruleAtsyraGoal ) )* otherlv_23= '}'
                    {
                    otherlv_19=(Token)match(input,17,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_19, grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsKeyword_7_0());
                      			
                    }
                    otherlv_20=(Token)match(input,12,FOLLOW_15); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_20, grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_7_1());
                      			
                    }
                    // InternalAtsyRAGoal.g:279:4: ( (lv_atsyragoals_21_0= ruleAtsyraGoal ) )
                    // InternalAtsyRAGoal.g:280:5: (lv_atsyragoals_21_0= ruleAtsyraGoal )
                    {
                    // InternalAtsyRAGoal.g:280:5: (lv_atsyragoals_21_0= ruleAtsyraGoal )
                    // InternalAtsyRAGoal.g:281:6: lv_atsyragoals_21_0= ruleAtsyraGoal
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAtsyraGoalParserRuleCall_7_2_0());
                      					
                    }
                    pushFollow(FOLLOW_15);
                    lv_atsyragoals_21_0=ruleAtsyraGoal();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                      						}
                      						add(
                      							current,
                      							"atsyragoals",
                      							lv_atsyragoals_21_0,
                      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.AtsyraGoal");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalAtsyRAGoal.g:298:4: ( (lv_atsyragoals_22_0= ruleAtsyraGoal ) )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==14) ) {
                            int LA8_1 = input.LA(2);

                            if ( (LA8_1==14) ) {
                                int LA8_3 = input.LA(3);

                                if ( (LA8_3==14||LA8_3==18||LA8_3==32||LA8_3==36) ) {
                                    alt8=1;
                                }


                            }
                            else if ( (LA8_1==32||LA8_1==36) ) {
                                alt8=1;
                            }


                        }
                        else if ( (LA8_0==32||LA8_0==36) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalAtsyRAGoal.g:299:5: (lv_atsyragoals_22_0= ruleAtsyraGoal )
                    	    {
                    	    // InternalAtsyRAGoal.g:299:5: (lv_atsyragoals_22_0= ruleAtsyraGoal )
                    	    // InternalAtsyRAGoal.g:300:6: lv_atsyragoals_22_0= ruleAtsyraGoal
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getAtsyragoalsAtsyraGoalParserRuleCall_7_3_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_15);
                    	    lv_atsyragoals_22_0=ruleAtsyraGoal();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"atsyragoals",
                    	      							lv_atsyragoals_22_0,
                    	      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.AtsyraGoal");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    otherlv_23=(Token)match(input,14,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_23, grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_7_4());
                      			
                    }

                    }
                    break;

            }

            // InternalAtsyRAGoal.g:322:3: (otherlv_24= 'trees' otherlv_25= '{' ( (lv_trees_26_0= ruleAtsyraTree ) ) ( (lv_trees_27_0= ruleAtsyraTree ) )* otherlv_28= '}' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==18) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalAtsyRAGoal.g:323:4: otherlv_24= 'trees' otherlv_25= '{' ( (lv_trees_26_0= ruleAtsyraTree ) ) ( (lv_trees_27_0= ruleAtsyraTree ) )* otherlv_28= '}'
                    {
                    otherlv_24=(Token)match(input,18,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_24, grammarAccess.getAtsyraGoalModelAccess().getTreesKeyword_8_0());
                      			
                    }
                    otherlv_25=(Token)match(input,12,FOLLOW_6); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_25, grammarAccess.getAtsyraGoalModelAccess().getLeftCurlyBracketKeyword_8_1());
                      			
                    }
                    // InternalAtsyRAGoal.g:331:4: ( (lv_trees_26_0= ruleAtsyraTree ) )
                    // InternalAtsyRAGoal.g:332:5: (lv_trees_26_0= ruleAtsyraTree )
                    {
                    // InternalAtsyRAGoal.g:332:5: (lv_trees_26_0= ruleAtsyraTree )
                    // InternalAtsyRAGoal.g:333:6: lv_trees_26_0= ruleAtsyraTree
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getTreesAtsyraTreeParserRuleCall_8_2_0());
                      					
                    }
                    pushFollow(FOLLOW_7);
                    lv_trees_26_0=ruleAtsyraTree();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                      						}
                      						add(
                      							current,
                      							"trees",
                      							lv_trees_26_0,
                      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.AtsyraTree");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalAtsyRAGoal.g:350:4: ( (lv_trees_27_0= ruleAtsyraTree ) )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( ((LA10_0>=RULE_STRING && LA10_0<=RULE_ID)) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalAtsyRAGoal.g:351:5: (lv_trees_27_0= ruleAtsyraTree )
                    	    {
                    	    // InternalAtsyRAGoal.g:351:5: (lv_trees_27_0= ruleAtsyraTree )
                    	    // InternalAtsyRAGoal.g:352:6: lv_trees_27_0= ruleAtsyraTree
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getAtsyraGoalModelAccess().getTreesAtsyraTreeParserRuleCall_8_3_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_7);
                    	    lv_trees_27_0=ruleAtsyraTree();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getAtsyraGoalModelRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"trees",
                    	      							lv_trees_27_0,
                    	      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.AtsyraTree");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    otherlv_28=(Token)match(input,14,FOLLOW_17); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_28, grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_8_4());
                      			
                    }

                    }
                    break;

            }

            otherlv_29=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_29, grammarAccess.getAtsyraGoalModelAccess().getRightCurlyBracketKeyword_9());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtsyraGoalModel"


    // $ANTLR start "entryRuleImport"
    // InternalAtsyRAGoal.g:382:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalAtsyRAGoal.g:382:47: (iv_ruleImport= ruleImport EOF )
            // InternalAtsyRAGoal.g:383:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalAtsyRAGoal.g:389:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importURI_1_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:395:2: ( (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) ) )
            // InternalAtsyRAGoal.g:396:2: (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) )
            {
            // InternalAtsyRAGoal.g:396:2: (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) )
            // InternalAtsyRAGoal.g:397:3: otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
              		
            }
            // InternalAtsyRAGoal.g:401:3: ( (lv_importURI_1_0= ruleEString ) )
            // InternalAtsyRAGoal.g:402:4: (lv_importURI_1_0= ruleEString )
            {
            // InternalAtsyRAGoal.g:402:4: (lv_importURI_1_0= ruleEString )
            // InternalAtsyRAGoal.g:403:5: lv_importURI_1_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getImportAccess().getImportURIEStringParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_importURI_1_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getImportRule());
              					}
              					set(
              						current,
              						"importURI",
              						lv_importURI_1_0,
              						"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleType"
    // InternalAtsyRAGoal.g:424:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalAtsyRAGoal.g:424:45: (iv_ruleType= ruleType EOF )
            // InternalAtsyRAGoal.g:425:2: iv_ruleType= ruleType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalAtsyRAGoal.g:431:1: ruleType returns [EObject current=null] : (this_InternalType_0= ruleInternalType | this_SystemType_1= ruleSystemType ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_InternalType_0 = null;

        EObject this_SystemType_1 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:437:2: ( (this_InternalType_0= ruleInternalType | this_SystemType_1= ruleSystemType ) )
            // InternalAtsyRAGoal.g:438:2: (this_InternalType_0= ruleInternalType | this_SystemType_1= ruleSystemType )
            {
            // InternalAtsyRAGoal.g:438:2: (this_InternalType_0= ruleInternalType | this_SystemType_1= ruleSystemType )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==40) ) {
                alt12=1;
            }
            else if ( (LA12_0==41) ) {
                alt12=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalAtsyRAGoal.g:439:3: this_InternalType_0= ruleInternalType
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getInternalTypeParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_InternalType_0=ruleInternalType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_InternalType_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:448:3: this_SystemType_1= ruleSystemType
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getSystemTypeParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_SystemType_1=ruleSystemType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SystemType_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleGoalCondition"
    // InternalAtsyRAGoal.g:460:1: entryRuleGoalCondition returns [EObject current=null] : iv_ruleGoalCondition= ruleGoalCondition EOF ;
    public final EObject entryRuleGoalCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGoalCondition = null;


        try {
            // InternalAtsyRAGoal.g:460:54: (iv_ruleGoalCondition= ruleGoalCondition EOF )
            // InternalAtsyRAGoal.g:461:2: iv_ruleGoalCondition= ruleGoalCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGoalConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGoalCondition=ruleGoalCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGoalCondition; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGoalCondition"


    // $ANTLR start "ruleGoalCondition"
    // InternalAtsyRAGoal.g:467:1: ruleGoalCondition returns [EObject current=null] : this_OrCondition_0= ruleOrCondition ;
    public final EObject ruleGoalCondition() throws RecognitionException {
        EObject current = null;

        EObject this_OrCondition_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:473:2: (this_OrCondition_0= ruleOrCondition )
            // InternalAtsyRAGoal.g:474:2: this_OrCondition_0= ruleOrCondition
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getGoalConditionAccess().getOrConditionParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_OrCondition_0=ruleOrCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_OrCondition_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGoalCondition"


    // $ANTLR start "entryRuleOrCondition"
    // InternalAtsyRAGoal.g:485:1: entryRuleOrCondition returns [EObject current=null] : iv_ruleOrCondition= ruleOrCondition EOF ;
    public final EObject entryRuleOrCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrCondition = null;


        try {
            // InternalAtsyRAGoal.g:485:52: (iv_ruleOrCondition= ruleOrCondition EOF )
            // InternalAtsyRAGoal.g:486:2: iv_ruleOrCondition= ruleOrCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOrConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOrCondition=ruleOrCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOrCondition; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrCondition"


    // $ANTLR start "ruleOrCondition"
    // InternalAtsyRAGoal.g:492:1: ruleOrCondition returns [EObject current=null] : (this_AndCondition_0= ruleAndCondition ( () (otherlv_2= 'or' | otherlv_3= 'OR' ) ( (lv_operands_4_0= ruleAndCondition ) ) )* ) ;
    public final EObject ruleOrCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_AndCondition_0 = null;

        EObject lv_operands_4_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:498:2: ( (this_AndCondition_0= ruleAndCondition ( () (otherlv_2= 'or' | otherlv_3= 'OR' ) ( (lv_operands_4_0= ruleAndCondition ) ) )* ) )
            // InternalAtsyRAGoal.g:499:2: (this_AndCondition_0= ruleAndCondition ( () (otherlv_2= 'or' | otherlv_3= 'OR' ) ( (lv_operands_4_0= ruleAndCondition ) ) )* )
            {
            // InternalAtsyRAGoal.g:499:2: (this_AndCondition_0= ruleAndCondition ( () (otherlv_2= 'or' | otherlv_3= 'OR' ) ( (lv_operands_4_0= ruleAndCondition ) ) )* )
            // InternalAtsyRAGoal.g:500:3: this_AndCondition_0= ruleAndCondition ( () (otherlv_2= 'or' | otherlv_3= 'OR' ) ( (lv_operands_4_0= ruleAndCondition ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getOrConditionAccess().getAndConditionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_18);
            this_AndCondition_0=ruleAndCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_AndCondition_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalAtsyRAGoal.g:508:3: ( () (otherlv_2= 'or' | otherlv_3= 'OR' ) ( (lv_operands_4_0= ruleAndCondition ) ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=20 && LA14_0<=21)) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:509:4: () (otherlv_2= 'or' | otherlv_3= 'OR' ) ( (lv_operands_4_0= ruleAndCondition ) )
            	    {
            	    // InternalAtsyRAGoal.g:509:4: ()
            	    // InternalAtsyRAGoal.g:510:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndAdd(
            	      						grammarAccess.getOrConditionAccess().getOrConditionOperandsAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalAtsyRAGoal.g:516:4: (otherlv_2= 'or' | otherlv_3= 'OR' )
            	    int alt13=2;
            	    int LA13_0 = input.LA(1);

            	    if ( (LA13_0==20) ) {
            	        alt13=1;
            	    }
            	    else if ( (LA13_0==21) ) {
            	        alt13=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 13, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt13) {
            	        case 1 :
            	            // InternalAtsyRAGoal.g:517:5: otherlv_2= 'or'
            	            {
            	            otherlv_2=(Token)match(input,20,FOLLOW_19); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              					newLeafNode(otherlv_2, grammarAccess.getOrConditionAccess().getOrKeyword_1_1_0());
            	              				
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // InternalAtsyRAGoal.g:522:5: otherlv_3= 'OR'
            	            {
            	            otherlv_3=(Token)match(input,21,FOLLOW_19); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              					newLeafNode(otherlv_3, grammarAccess.getOrConditionAccess().getORKeyword_1_1_1());
            	              				
            	            }

            	            }
            	            break;

            	    }

            	    // InternalAtsyRAGoal.g:527:4: ( (lv_operands_4_0= ruleAndCondition ) )
            	    // InternalAtsyRAGoal.g:528:5: (lv_operands_4_0= ruleAndCondition )
            	    {
            	    // InternalAtsyRAGoal.g:528:5: (lv_operands_4_0= ruleAndCondition )
            	    // InternalAtsyRAGoal.g:529:6: lv_operands_4_0= ruleAndCondition
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getOrConditionAccess().getOperandsAndConditionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_18);
            	    lv_operands_4_0=ruleAndCondition();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getOrConditionRule());
            	      						}
            	      						add(
            	      							current,
            	      							"operands",
            	      							lv_operands_4_0,
            	      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.AndCondition");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrCondition"


    // $ANTLR start "entryRuleAndCondition"
    // InternalAtsyRAGoal.g:551:1: entryRuleAndCondition returns [EObject current=null] : iv_ruleAndCondition= ruleAndCondition EOF ;
    public final EObject entryRuleAndCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndCondition = null;


        try {
            // InternalAtsyRAGoal.g:551:53: (iv_ruleAndCondition= ruleAndCondition EOF )
            // InternalAtsyRAGoal.g:552:2: iv_ruleAndCondition= ruleAndCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAndCondition=ruleAndCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAndCondition; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndCondition"


    // $ANTLR start "ruleAndCondition"
    // InternalAtsyRAGoal.g:558:1: ruleAndCondition returns [EObject current=null] : (this_TerminalCondition_0= ruleTerminalCondition ( () (otherlv_2= 'and' | otherlv_3= 'AND' ) ( (lv_operands_4_0= ruleTerminalCondition ) ) )* ) ;
    public final EObject ruleAndCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_TerminalCondition_0 = null;

        EObject lv_operands_4_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:564:2: ( (this_TerminalCondition_0= ruleTerminalCondition ( () (otherlv_2= 'and' | otherlv_3= 'AND' ) ( (lv_operands_4_0= ruleTerminalCondition ) ) )* ) )
            // InternalAtsyRAGoal.g:565:2: (this_TerminalCondition_0= ruleTerminalCondition ( () (otherlv_2= 'and' | otherlv_3= 'AND' ) ( (lv_operands_4_0= ruleTerminalCondition ) ) )* )
            {
            // InternalAtsyRAGoal.g:565:2: (this_TerminalCondition_0= ruleTerminalCondition ( () (otherlv_2= 'and' | otherlv_3= 'AND' ) ( (lv_operands_4_0= ruleTerminalCondition ) ) )* )
            // InternalAtsyRAGoal.g:566:3: this_TerminalCondition_0= ruleTerminalCondition ( () (otherlv_2= 'and' | otherlv_3= 'AND' ) ( (lv_operands_4_0= ruleTerminalCondition ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getAndConditionAccess().getTerminalConditionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_20);
            this_TerminalCondition_0=ruleTerminalCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_TerminalCondition_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalAtsyRAGoal.g:574:3: ( () (otherlv_2= 'and' | otherlv_3= 'AND' ) ( (lv_operands_4_0= ruleTerminalCondition ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>=22 && LA16_0<=23)) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:575:4: () (otherlv_2= 'and' | otherlv_3= 'AND' ) ( (lv_operands_4_0= ruleTerminalCondition ) )
            	    {
            	    // InternalAtsyRAGoal.g:575:4: ()
            	    // InternalAtsyRAGoal.g:576:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndAdd(
            	      						grammarAccess.getAndConditionAccess().getAndConditionOperandsAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalAtsyRAGoal.g:582:4: (otherlv_2= 'and' | otherlv_3= 'AND' )
            	    int alt15=2;
            	    int LA15_0 = input.LA(1);

            	    if ( (LA15_0==22) ) {
            	        alt15=1;
            	    }
            	    else if ( (LA15_0==23) ) {
            	        alt15=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 15, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt15) {
            	        case 1 :
            	            // InternalAtsyRAGoal.g:583:5: otherlv_2= 'and'
            	            {
            	            otherlv_2=(Token)match(input,22,FOLLOW_19); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              					newLeafNode(otherlv_2, grammarAccess.getAndConditionAccess().getAndKeyword_1_1_0());
            	              				
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // InternalAtsyRAGoal.g:588:5: otherlv_3= 'AND'
            	            {
            	            otherlv_3=(Token)match(input,23,FOLLOW_19); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              					newLeafNode(otherlv_3, grammarAccess.getAndConditionAccess().getANDKeyword_1_1_1());
            	              				
            	            }

            	            }
            	            break;

            	    }

            	    // InternalAtsyRAGoal.g:593:4: ( (lv_operands_4_0= ruleTerminalCondition ) )
            	    // InternalAtsyRAGoal.g:594:5: (lv_operands_4_0= ruleTerminalCondition )
            	    {
            	    // InternalAtsyRAGoal.g:594:5: (lv_operands_4_0= ruleTerminalCondition )
            	    // InternalAtsyRAGoal.g:595:6: lv_operands_4_0= ruleTerminalCondition
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getAndConditionAccess().getOperandsTerminalConditionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_20);
            	    lv_operands_4_0=ruleTerminalCondition();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getAndConditionRule());
            	      						}
            	      						add(
            	      							current,
            	      							"operands",
            	      							lv_operands_4_0,
            	      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.TerminalCondition");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndCondition"


    // $ANTLR start "entryRuleTerminalCondition"
    // InternalAtsyRAGoal.g:617:1: entryRuleTerminalCondition returns [EObject current=null] : iv_ruleTerminalCondition= ruleTerminalCondition EOF ;
    public final EObject entryRuleTerminalCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerminalCondition = null;


        try {
            // InternalAtsyRAGoal.g:617:58: (iv_ruleTerminalCondition= ruleTerminalCondition EOF )
            // InternalAtsyRAGoal.g:618:2: iv_ruleTerminalCondition= ruleTerminalCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTerminalConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTerminalCondition=ruleTerminalCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTerminalCondition; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerminalCondition"


    // $ANTLR start "ruleTerminalCondition"
    // InternalAtsyRAGoal.g:624:1: ruleTerminalCondition returns [EObject current=null] : ( (otherlv_0= '(' this_GoalCondition_1= ruleGoalCondition otherlv_2= ')' ) | ( () (otherlv_4= 'not' | otherlv_5= 'NOT' ) ( (lv_operands_6_0= ruleTerminalCondition ) ) ) | this_EqualOrBooleanCond_7= ruleEqualOrBooleanCond ) ;
    public final EObject ruleTerminalCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject this_GoalCondition_1 = null;

        EObject lv_operands_6_0 = null;

        EObject this_EqualOrBooleanCond_7 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:630:2: ( ( (otherlv_0= '(' this_GoalCondition_1= ruleGoalCondition otherlv_2= ')' ) | ( () (otherlv_4= 'not' | otherlv_5= 'NOT' ) ( (lv_operands_6_0= ruleTerminalCondition ) ) ) | this_EqualOrBooleanCond_7= ruleEqualOrBooleanCond ) )
            // InternalAtsyRAGoal.g:631:2: ( (otherlv_0= '(' this_GoalCondition_1= ruleGoalCondition otherlv_2= ')' ) | ( () (otherlv_4= 'not' | otherlv_5= 'NOT' ) ( (lv_operands_6_0= ruleTerminalCondition ) ) ) | this_EqualOrBooleanCond_7= ruleEqualOrBooleanCond )
            {
            // InternalAtsyRAGoal.g:631:2: ( (otherlv_0= '(' this_GoalCondition_1= ruleGoalCondition otherlv_2= ')' ) | ( () (otherlv_4= 'not' | otherlv_5= 'NOT' ) ( (lv_operands_6_0= ruleTerminalCondition ) ) ) | this_EqualOrBooleanCond_7= ruleEqualOrBooleanCond )
            int alt18=3;
            switch ( input.LA(1) ) {
            case 24:
                {
                alt18=1;
                }
                break;
            case 26:
            case 27:
                {
                alt18=2;
                }
                break;
            case RULE_STRING:
            case RULE_ID:
            case 30:
            case 31:
                {
                alt18=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalAtsyRAGoal.g:632:3: (otherlv_0= '(' this_GoalCondition_1= ruleGoalCondition otherlv_2= ')' )
                    {
                    // InternalAtsyRAGoal.g:632:3: (otherlv_0= '(' this_GoalCondition_1= ruleGoalCondition otherlv_2= ')' )
                    // InternalAtsyRAGoal.g:633:4: otherlv_0= '(' this_GoalCondition_1= ruleGoalCondition otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,24,FOLLOW_19); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_0, grammarAccess.getTerminalConditionAccess().getLeftParenthesisKeyword_0_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getTerminalConditionAccess().getGoalConditionParserRuleCall_0_1());
                      			
                    }
                    pushFollow(FOLLOW_21);
                    this_GoalCondition_1=ruleGoalCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_GoalCondition_1;
                      				afterParserOrEnumRuleCall();
                      			
                    }
                    otherlv_2=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getTerminalConditionAccess().getRightParenthesisKeyword_0_2());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:651:3: ( () (otherlv_4= 'not' | otherlv_5= 'NOT' ) ( (lv_operands_6_0= ruleTerminalCondition ) ) )
                    {
                    // InternalAtsyRAGoal.g:651:3: ( () (otherlv_4= 'not' | otherlv_5= 'NOT' ) ( (lv_operands_6_0= ruleTerminalCondition ) ) )
                    // InternalAtsyRAGoal.g:652:4: () (otherlv_4= 'not' | otherlv_5= 'NOT' ) ( (lv_operands_6_0= ruleTerminalCondition ) )
                    {
                    // InternalAtsyRAGoal.g:652:4: ()
                    // InternalAtsyRAGoal.g:653:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getTerminalConditionAccess().getNotConditionAction_1_0(),
                      						current);
                      				
                    }

                    }

                    // InternalAtsyRAGoal.g:659:4: (otherlv_4= 'not' | otherlv_5= 'NOT' )
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0==26) ) {
                        alt17=1;
                    }
                    else if ( (LA17_0==27) ) {
                        alt17=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 17, 0, input);

                        throw nvae;
                    }
                    switch (alt17) {
                        case 1 :
                            // InternalAtsyRAGoal.g:660:5: otherlv_4= 'not'
                            {
                            otherlv_4=(Token)match(input,26,FOLLOW_19); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_4, grammarAccess.getTerminalConditionAccess().getNotKeyword_1_1_0());
                              				
                            }

                            }
                            break;
                        case 2 :
                            // InternalAtsyRAGoal.g:665:5: otherlv_5= 'NOT'
                            {
                            otherlv_5=(Token)match(input,27,FOLLOW_19); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_5, grammarAccess.getTerminalConditionAccess().getNOTKeyword_1_1_1());
                              				
                            }

                            }
                            break;

                    }

                    // InternalAtsyRAGoal.g:670:4: ( (lv_operands_6_0= ruleTerminalCondition ) )
                    // InternalAtsyRAGoal.g:671:5: (lv_operands_6_0= ruleTerminalCondition )
                    {
                    // InternalAtsyRAGoal.g:671:5: (lv_operands_6_0= ruleTerminalCondition )
                    // InternalAtsyRAGoal.g:672:6: lv_operands_6_0= ruleTerminalCondition
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getTerminalConditionAccess().getOperandsTerminalConditionParserRuleCall_1_2_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_operands_6_0=ruleTerminalCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getTerminalConditionRule());
                      						}
                      						add(
                      							current,
                      							"operands",
                      							lv_operands_6_0,
                      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.TerminalCondition");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalAtsyRAGoal.g:691:3: this_EqualOrBooleanCond_7= ruleEqualOrBooleanCond
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTerminalConditionAccess().getEqualOrBooleanCondParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_EqualOrBooleanCond_7=ruleEqualOrBooleanCond();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_EqualOrBooleanCond_7;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerminalCondition"


    // $ANTLR start "entryRuleEqualOrBooleanCond"
    // InternalAtsyRAGoal.g:703:1: entryRuleEqualOrBooleanCond returns [EObject current=null] : iv_ruleEqualOrBooleanCond= ruleEqualOrBooleanCond EOF ;
    public final EObject entryRuleEqualOrBooleanCond() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEqualOrBooleanCond = null;


        try {
            // InternalAtsyRAGoal.g:703:59: (iv_ruleEqualOrBooleanCond= ruleEqualOrBooleanCond EOF )
            // InternalAtsyRAGoal.g:704:2: iv_ruleEqualOrBooleanCond= ruleEqualOrBooleanCond EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEqualOrBooleanCondRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEqualOrBooleanCond=ruleEqualOrBooleanCond();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEqualOrBooleanCond; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEqualOrBooleanCond"


    // $ANTLR start "ruleEqualOrBooleanCond"
    // InternalAtsyRAGoal.g:710:1: ruleEqualOrBooleanCond returns [EObject current=null] : ( ( () ( ( ruleTypedElementString ) ) ) | ( () ( ( ruleTypedElementString ) ) ( ( ( '=' )=>otherlv_4= '=' ) ( ( ruleTypedElementString ) ) ) ) ) ;
    public final EObject ruleEqualOrBooleanCond() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalAtsyRAGoal.g:716:2: ( ( ( () ( ( ruleTypedElementString ) ) ) | ( () ( ( ruleTypedElementString ) ) ( ( ( '=' )=>otherlv_4= '=' ) ( ( ruleTypedElementString ) ) ) ) ) )
            // InternalAtsyRAGoal.g:717:2: ( ( () ( ( ruleTypedElementString ) ) ) | ( () ( ( ruleTypedElementString ) ) ( ( ( '=' )=>otherlv_4= '=' ) ( ( ruleTypedElementString ) ) ) ) )
            {
            // InternalAtsyRAGoal.g:717:2: ( ( () ( ( ruleTypedElementString ) ) ) | ( () ( ( ruleTypedElementString ) ) ( ( ( '=' )=>otherlv_4= '=' ) ( ( ruleTypedElementString ) ) ) ) )
            int alt19=2;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                int LA19_1 = input.LA(2);

                if ( (LA19_1==28) ) {
                    alt19=2;
                }
                else if ( (LA19_1==EOF||LA19_1==14||(LA19_1>=20 && LA19_1<=23)||LA19_1==25||LA19_1==32||LA19_1==36) ) {
                    alt19=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 19, 1, input);

                    throw nvae;
                }
                }
                break;
            case RULE_ID:
                {
                int LA19_2 = input.LA(2);

                if ( (LA19_2==28) ) {
                    alt19=2;
                }
                else if ( (LA19_2==EOF||LA19_2==14||(LA19_2>=20 && LA19_2<=23)||LA19_2==25||LA19_2==32||LA19_2==36) ) {
                    alt19=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 19, 2, input);

                    throw nvae;
                }
                }
                break;
            case 30:
                {
                int LA19_3 = input.LA(2);

                if ( (LA19_3==28) ) {
                    alt19=2;
                }
                else if ( (LA19_3==EOF||LA19_3==14||(LA19_3>=20 && LA19_3<=23)||LA19_3==25||LA19_3==32||LA19_3==36) ) {
                    alt19=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 19, 3, input);

                    throw nvae;
                }
                }
                break;
            case 31:
                {
                int LA19_4 = input.LA(2);

                if ( (LA19_4==28) ) {
                    alt19=2;
                }
                else if ( (LA19_4==EOF||LA19_4==14||(LA19_4>=20 && LA19_4<=23)||LA19_4==25||LA19_4==32||LA19_4==36) ) {
                    alt19=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 19, 4, input);

                    throw nvae;
                }
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }

            switch (alt19) {
                case 1 :
                    // InternalAtsyRAGoal.g:718:3: ( () ( ( ruleTypedElementString ) ) )
                    {
                    // InternalAtsyRAGoal.g:718:3: ( () ( ( ruleTypedElementString ) ) )
                    // InternalAtsyRAGoal.g:719:4: () ( ( ruleTypedElementString ) )
                    {
                    // InternalAtsyRAGoal.g:719:4: ()
                    // InternalAtsyRAGoal.g:720:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getEqualOrBooleanCondAccess().getBooleanSystemConditionAction_0_0(),
                      						current);
                      				
                    }

                    }

                    // InternalAtsyRAGoal.g:726:4: ( ( ruleTypedElementString ) )
                    // InternalAtsyRAGoal.g:727:5: ( ruleTypedElementString )
                    {
                    // InternalAtsyRAGoal.g:727:5: ( ruleTypedElementString )
                    // InternalAtsyRAGoal.g:728:6: ruleTypedElementString
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getEqualOrBooleanCondRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementCrossReference_0_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    ruleTypedElementString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:744:3: ( () ( ( ruleTypedElementString ) ) ( ( ( '=' )=>otherlv_4= '=' ) ( ( ruleTypedElementString ) ) ) )
                    {
                    // InternalAtsyRAGoal.g:744:3: ( () ( ( ruleTypedElementString ) ) ( ( ( '=' )=>otherlv_4= '=' ) ( ( ruleTypedElementString ) ) ) )
                    // InternalAtsyRAGoal.g:745:4: () ( ( ruleTypedElementString ) ) ( ( ( '=' )=>otherlv_4= '=' ) ( ( ruleTypedElementString ) ) )
                    {
                    // InternalAtsyRAGoal.g:745:4: ()
                    // InternalAtsyRAGoal.g:746:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getEqualOrBooleanCondAccess().getEqualsConditionAction_1_0(),
                      						current);
                      				
                    }

                    }

                    // InternalAtsyRAGoal.g:752:4: ( ( ruleTypedElementString ) )
                    // InternalAtsyRAGoal.g:753:5: ( ruleTypedElementString )
                    {
                    // InternalAtsyRAGoal.g:753:5: ( ruleTypedElementString )
                    // InternalAtsyRAGoal.g:754:6: ruleTypedElementString
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getEqualOrBooleanCondRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getEqualOrBooleanCondAccess().getSourceTypedElementCrossReference_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_22);
                    ruleTypedElementString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalAtsyRAGoal.g:768:4: ( ( ( '=' )=>otherlv_4= '=' ) ( ( ruleTypedElementString ) ) )
                    // InternalAtsyRAGoal.g:769:5: ( ( '=' )=>otherlv_4= '=' ) ( ( ruleTypedElementString ) )
                    {
                    // InternalAtsyRAGoal.g:769:5: ( ( '=' )=>otherlv_4= '=' )
                    // InternalAtsyRAGoal.g:770:6: ( '=' )=>otherlv_4= '='
                    {
                    otherlv_4=(Token)match(input,28,FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_4, grammarAccess.getEqualOrBooleanCondAccess().getEqualsSignKeyword_1_2_0());
                      					
                    }

                    }

                    // InternalAtsyRAGoal.g:776:5: ( ( ruleTypedElementString ) )
                    // InternalAtsyRAGoal.g:777:6: ( ruleTypedElementString )
                    {
                    // InternalAtsyRAGoal.g:777:6: ( ruleTypedElementString )
                    // InternalAtsyRAGoal.g:778:7: ruleTypedElementString
                    {
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElement(grammarAccess.getEqualOrBooleanCondRule());
                      							}
                      						
                    }
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getEqualOrBooleanCondAccess().getTargetTypedElementCrossReference_1_2_1_0());
                      						
                    }
                    pushFollow(FOLLOW_2);
                    ruleTypedElementString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEqualOrBooleanCond"


    // $ANTLR start "entryRuleTypedElementDecl"
    // InternalAtsyRAGoal.g:798:1: entryRuleTypedElementDecl returns [EObject current=null] : iv_ruleTypedElementDecl= ruleTypedElementDecl EOF ;
    public final EObject entryRuleTypedElementDecl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypedElementDecl = null;


        try {
            // InternalAtsyRAGoal.g:798:57: (iv_ruleTypedElementDecl= ruleTypedElementDecl EOF )
            // InternalAtsyRAGoal.g:799:2: iv_ruleTypedElementDecl= ruleTypedElementDecl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypedElementDeclRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTypedElementDecl=ruleTypedElementDecl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypedElementDecl; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedElementDecl"


    // $ANTLR start "ruleTypedElementDecl"
    // InternalAtsyRAGoal.g:805:1: ruleTypedElementDecl returns [EObject current=null] : ( (otherlv_0= 'const' this_BooleanLiteral_1= ruleBooleanLiteral ) | this_BuildingSystemFeature_2= ruleBuildingSystemFeature | (otherlv_3= 'const' this_BuildingSystemConstFeature_4= ruleBuildingSystemConstFeature ) ) ;
    public final EObject ruleTypedElementDecl() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        EObject this_BooleanLiteral_1 = null;

        EObject this_BuildingSystemFeature_2 = null;

        EObject this_BuildingSystemConstFeature_4 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:811:2: ( ( (otherlv_0= 'const' this_BooleanLiteral_1= ruleBooleanLiteral ) | this_BuildingSystemFeature_2= ruleBuildingSystemFeature | (otherlv_3= 'const' this_BuildingSystemConstFeature_4= ruleBuildingSystemConstFeature ) ) )
            // InternalAtsyRAGoal.g:812:2: ( (otherlv_0= 'const' this_BooleanLiteral_1= ruleBooleanLiteral ) | this_BuildingSystemFeature_2= ruleBuildingSystemFeature | (otherlv_3= 'const' this_BuildingSystemConstFeature_4= ruleBuildingSystemConstFeature ) )
            {
            // InternalAtsyRAGoal.g:812:2: ( (otherlv_0= 'const' this_BooleanLiteral_1= ruleBooleanLiteral ) | this_BuildingSystemFeature_2= ruleBuildingSystemFeature | (otherlv_3= 'const' this_BuildingSystemConstFeature_4= ruleBuildingSystemConstFeature ) )
            int alt20=3;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==29) ) {
                int LA20_1 = input.LA(2);

                if ( ((LA20_1>=30 && LA20_1<=31)) ) {
                    alt20=1;
                }
                else if ( ((LA20_1>=RULE_STRING && LA20_1<=RULE_ID)) ) {
                    alt20=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 20, 1, input);

                    throw nvae;
                }
            }
            else if ( ((LA20_0>=RULE_STRING && LA20_0<=RULE_ID)) ) {
                alt20=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // InternalAtsyRAGoal.g:813:3: (otherlv_0= 'const' this_BooleanLiteral_1= ruleBooleanLiteral )
                    {
                    // InternalAtsyRAGoal.g:813:3: (otherlv_0= 'const' this_BooleanLiteral_1= ruleBooleanLiteral )
                    // InternalAtsyRAGoal.g:814:4: otherlv_0= 'const' this_BooleanLiteral_1= ruleBooleanLiteral
                    {
                    otherlv_0=(Token)match(input,29,FOLLOW_24); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_0, grammarAccess.getTypedElementDeclAccess().getConstKeyword_0_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getTypedElementDeclAccess().getBooleanLiteralParserRuleCall_0_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_BooleanLiteral_1=ruleBooleanLiteral();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_BooleanLiteral_1;
                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:828:3: this_BuildingSystemFeature_2= ruleBuildingSystemFeature
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypedElementDeclAccess().getBuildingSystemFeatureParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_BuildingSystemFeature_2=ruleBuildingSystemFeature();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_BuildingSystemFeature_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalAtsyRAGoal.g:837:3: (otherlv_3= 'const' this_BuildingSystemConstFeature_4= ruleBuildingSystemConstFeature )
                    {
                    // InternalAtsyRAGoal.g:837:3: (otherlv_3= 'const' this_BuildingSystemConstFeature_4= ruleBuildingSystemConstFeature )
                    // InternalAtsyRAGoal.g:838:4: otherlv_3= 'const' this_BuildingSystemConstFeature_4= ruleBuildingSystemConstFeature
                    {
                    otherlv_3=(Token)match(input,29,FOLLOW_6); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getTypedElementDeclAccess().getConstKeyword_2_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getTypedElementDeclAccess().getBuildingSystemConstFeatureParserRuleCall_2_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_BuildingSystemConstFeature_4=ruleBuildingSystemConstFeature();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_BuildingSystemConstFeature_4;
                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedElementDecl"


    // $ANTLR start "entryRuleEString"
    // InternalAtsyRAGoal.g:855:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalAtsyRAGoal.g:855:47: (iv_ruleEString= ruleEString EOF )
            // InternalAtsyRAGoal.g:856:2: iv_ruleEString= ruleEString EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEStringRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEString.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalAtsyRAGoal.g:862:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalAtsyRAGoal.g:868:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalAtsyRAGoal.g:869:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalAtsyRAGoal.g:869:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==RULE_STRING) ) {
                alt21=1;
            }
            else if ( (LA21_0==RULE_ID) ) {
                alt21=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalAtsyRAGoal.g:870:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_STRING_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:878:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_ID_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleTypedElementString"
    // InternalAtsyRAGoal.g:889:1: entryRuleTypedElementString returns [String current=null] : iv_ruleTypedElementString= ruleTypedElementString EOF ;
    public final String entryRuleTypedElementString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTypedElementString = null;


        try {
            // InternalAtsyRAGoal.g:889:58: (iv_ruleTypedElementString= ruleTypedElementString EOF )
            // InternalAtsyRAGoal.g:890:2: iv_ruleTypedElementString= ruleTypedElementString EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypedElementStringRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTypedElementString=ruleTypedElementString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypedElementString.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedElementString"


    // $ANTLR start "ruleTypedElementString"
    // InternalAtsyRAGoal.g:896:1: ruleTypedElementString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleTypedElementString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;
        Token kw=null;


        	enterRule();

        try {
            // InternalAtsyRAGoal.g:902:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | kw= 'true' | kw= 'false' ) )
            // InternalAtsyRAGoal.g:903:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | kw= 'true' | kw= 'false' )
            {
            // InternalAtsyRAGoal.g:903:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | kw= 'true' | kw= 'false' )
            int alt22=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt22=1;
                }
                break;
            case RULE_ID:
                {
                alt22=2;
                }
                break;
            case 30:
                {
                alt22=3;
                }
                break;
            case 31:
                {
                alt22=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // InternalAtsyRAGoal.g:904:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_STRING_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_STRING_0, grammarAccess.getTypedElementStringAccess().getSTRINGTerminalRuleCall_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:912:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_ID_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_ID_1, grammarAccess.getTypedElementStringAccess().getIDTerminalRuleCall_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalAtsyRAGoal.g:920:3: kw= 'true'
                    {
                    kw=(Token)match(input,30,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getTypedElementStringAccess().getTrueKeyword_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalAtsyRAGoal.g:926:3: kw= 'false'
                    {
                    kw=(Token)match(input,31,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getTypedElementStringAccess().getFalseKeyword_3());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedElementString"


    // $ANTLR start "entryRuleAtsyraGoal"
    // InternalAtsyRAGoal.g:935:1: entryRuleAtsyraGoal returns [EObject current=null] : iv_ruleAtsyraGoal= ruleAtsyraGoal EOF ;
    public final EObject entryRuleAtsyraGoal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtsyraGoal = null;


        try {
            // InternalAtsyRAGoal.g:935:51: (iv_ruleAtsyraGoal= ruleAtsyraGoal EOF )
            // InternalAtsyRAGoal.g:936:2: iv_ruleAtsyraGoal= ruleAtsyraGoal EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAtsyraGoalRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAtsyraGoal=ruleAtsyraGoal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAtsyraGoal; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtsyraGoal"


    // $ANTLR start "ruleAtsyraGoal"
    // InternalAtsyRAGoal.g:942:1: ruleAtsyraGoal returns [EObject current=null] : ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+ {...}?) ) ) ;
    public final EObject ruleAtsyraGoal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_precondition_8_0 = null;

        EObject lv_postcondition_13_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:948:2: ( ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+ {...}?) ) ) )
            // InternalAtsyRAGoal.g:949:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+ {...}?) ) )
            {
            // InternalAtsyRAGoal.g:949:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+ {...}?) ) )
            // InternalAtsyRAGoal.g:950:3: ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+ {...}?) )
            {
            // InternalAtsyRAGoal.g:950:3: ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+ {...}?) )
            // InternalAtsyRAGoal.g:951:4: ( ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+ {...}?)
            {
            getUnorderedGroupHelper().enter(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup());
            // InternalAtsyRAGoal.g:954:4: ( ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+ {...}?)
            // InternalAtsyRAGoal.g:955:5: ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+ {...}?
            {
            // InternalAtsyRAGoal.g:955:5: ( ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) ) )+
            int cnt27=0;
            loop27:
            do {
                int alt27=3;
                switch ( input.LA(1) ) {
                case 32:
                    {
                    int LA27_1 = input.LA(2);

                    if ( getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0) ) {
                        alt27=1;
                    }


                    }
                    break;
                case 36:
                    {
                    int LA27_2 = input.LA(2);

                    if ( getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1) ) {
                        alt27=2;
                    }


                    }
                    break;
                case 14:
                    {
                    int LA27_3 = input.LA(2);

                    if ( getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1) ) {
                        alt27=2;
                    }


                    }
                    break;

                }

                switch (alt27) {
            	case 1 :
            	    // InternalAtsyRAGoal.g:956:3: ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) )
            	    {
            	    // InternalAtsyRAGoal.g:956:3: ({...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) ) )
            	    // InternalAtsyRAGoal.g:957:4: {...}? => ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleAtsyraGoal", "getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0)");
            	    }
            	    // InternalAtsyRAGoal.g:957:104: ( ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) ) )
            	    // InternalAtsyRAGoal.g:958:5: ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 0);
            	    // InternalAtsyRAGoal.g:961:8: ({...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? ) )
            	    // InternalAtsyRAGoal.g:961:9: {...}? => (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleAtsyraGoal", "true");
            	    }
            	    // InternalAtsyRAGoal.g:961:18: (otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )? )
            	    // InternalAtsyRAGoal.g:961:19: otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )?
            	    {
            	    otherlv_1=(Token)match(input,32,FOLLOW_6); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_1, grammarAccess.getAtsyraGoalAccess().getGoalKeyword_0_0());
            	      							
            	    }
            	    // InternalAtsyRAGoal.g:965:8: ( (lv_name_2_0= ruleEString ) )
            	    // InternalAtsyRAGoal.g:966:9: (lv_name_2_0= ruleEString )
            	    {
            	    // InternalAtsyRAGoal.g:966:9: (lv_name_2_0= ruleEString )
            	    // InternalAtsyRAGoal.g:967:10: lv_name_2_0= ruleEString
            	    {
            	    if ( state.backtracking==0 ) {

            	      										newCompositeNode(grammarAccess.getAtsyraGoalAccess().getNameEStringParserRuleCall_0_1_0());
            	      									
            	    }
            	    pushFollow(FOLLOW_4);
            	    lv_name_2_0=ruleEString();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      										if (current==null) {
            	      											current = createModelElementForParent(grammarAccess.getAtsyraGoalRule());
            	      										}
            	      										set(
            	      											current,
            	      											"name",
            	      											lv_name_2_0,
            	      											"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.EString");
            	      										afterParserOrEnumRuleCall();
            	      									
            	    }

            	    }


            	    }

            	    otherlv_3=(Token)match(input,12,FOLLOW_25); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_3, grammarAccess.getAtsyraGoalAccess().getLeftCurlyBracketKeyword_0_2());
            	      							
            	    }
            	    // InternalAtsyRAGoal.g:988:8: (otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) ) )?
            	    int alt24=2;
            	    int LA24_0 = input.LA(1);

            	    if ( (LA24_0==33) ) {
            	        alt24=1;
            	    }
            	    switch (alt24) {
            	        case 1 :
            	            // InternalAtsyRAGoal.g:989:9: otherlv_4= 'pre' (otherlv_5= 'with' ( ( ruleEString ) ) )? otherlv_7= ':' ( (lv_precondition_8_0= ruleGoalCondition ) )
            	            {
            	            otherlv_4=(Token)match(input,33,FOLLOW_26); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              									newLeafNode(otherlv_4, grammarAccess.getAtsyraGoalAccess().getPreKeyword_0_3_0());
            	              								
            	            }
            	            // InternalAtsyRAGoal.g:993:9: (otherlv_5= 'with' ( ( ruleEString ) ) )?
            	            int alt23=2;
            	            int LA23_0 = input.LA(1);

            	            if ( (LA23_0==34) ) {
            	                alt23=1;
            	            }
            	            switch (alt23) {
            	                case 1 :
            	                    // InternalAtsyRAGoal.g:994:10: otherlv_5= 'with' ( ( ruleEString ) )
            	                    {
            	                    otherlv_5=(Token)match(input,34,FOLLOW_6); if (state.failed) return current;
            	                    if ( state.backtracking==0 ) {

            	                      										newLeafNode(otherlv_5, grammarAccess.getAtsyraGoalAccess().getWithKeyword_0_3_1_0());
            	                      									
            	                    }
            	                    // InternalAtsyRAGoal.g:998:10: ( ( ruleEString ) )
            	                    // InternalAtsyRAGoal.g:999:11: ( ruleEString )
            	                    {
            	                    // InternalAtsyRAGoal.g:999:11: ( ruleEString )
            	                    // InternalAtsyRAGoal.g:1000:12: ruleEString
            	                    {
            	                    if ( state.backtracking==0 ) {

            	                      												if (current==null) {
            	                      													current = createModelElement(grammarAccess.getAtsyraGoalRule());
            	                      												}
            	                      											
            	                    }
            	                    if ( state.backtracking==0 ) {

            	                      												newCompositeNode(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPreDefaultValuesCrossReference_0_3_1_1_0());
            	                      											
            	                    }
            	                    pushFollow(FOLLOW_27);
            	                    ruleEString();

            	                    state._fsp--;
            	                    if (state.failed) return current;
            	                    if ( state.backtracking==0 ) {

            	                      												afterParserOrEnumRuleCall();
            	                      											
            	                    }

            	                    }


            	                    }


            	                    }
            	                    break;

            	            }

            	            otherlv_7=(Token)match(input,35,FOLLOW_19); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              									newLeafNode(otherlv_7, grammarAccess.getAtsyraGoalAccess().getColonKeyword_0_3_2());
            	              								
            	            }
            	            // InternalAtsyRAGoal.g:1019:9: ( (lv_precondition_8_0= ruleGoalCondition ) )
            	            // InternalAtsyRAGoal.g:1020:10: (lv_precondition_8_0= ruleGoalCondition )
            	            {
            	            // InternalAtsyRAGoal.g:1020:10: (lv_precondition_8_0= ruleGoalCondition )
            	            // InternalAtsyRAGoal.g:1021:11: lv_precondition_8_0= ruleGoalCondition
            	            {
            	            if ( state.backtracking==0 ) {

            	              											newCompositeNode(grammarAccess.getAtsyraGoalAccess().getPreconditionGoalConditionParserRuleCall_0_3_3_0());
            	              										
            	            }
            	            pushFollow(FOLLOW_28);
            	            lv_precondition_8_0=ruleGoalCondition();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              											if (current==null) {
            	              												current = createModelElementForParent(grammarAccess.getAtsyraGoalRule());
            	              											}
            	              											set(
            	              												current,
            	              												"precondition",
            	              												lv_precondition_8_0,
            	              												"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.GoalCondition");
            	              											afterParserOrEnumRuleCall();
            	              										
            	            }

            	            }


            	            }


            	            }
            	            break;

            	    }


            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup());

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalAtsyRAGoal.g:1045:3: ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) )
            	    {
            	    // InternalAtsyRAGoal.g:1045:3: ({...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) ) )
            	    // InternalAtsyRAGoal.g:1046:4: {...}? => ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleAtsyraGoal", "getUnorderedGroupHelper().canSelect(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1)");
            	    }
            	    // InternalAtsyRAGoal.g:1046:104: ( ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) ) )
            	    // InternalAtsyRAGoal.g:1047:5: ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup(), 1);
            	    // InternalAtsyRAGoal.g:1050:8: ({...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' ) )
            	    // InternalAtsyRAGoal.g:1050:9: {...}? => ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleAtsyraGoal", "true");
            	    }
            	    // InternalAtsyRAGoal.g:1050:18: ( (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}' )
            	    // InternalAtsyRAGoal.g:1050:19: (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )? otherlv_14= '}'
            	    {
            	    // InternalAtsyRAGoal.g:1050:19: (otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) ) )?
            	    int alt26=2;
            	    int LA26_0 = input.LA(1);

            	    if ( (LA26_0==36) ) {
            	        alt26=1;
            	    }
            	    switch (alt26) {
            	        case 1 :
            	            // InternalAtsyRAGoal.g:1051:9: otherlv_9= 'post' (otherlv_10= 'with' ( ( ruleEString ) ) )? otherlv_12= ':' ( (lv_postcondition_13_0= ruleGoalCondition ) )
            	            {
            	            otherlv_9=(Token)match(input,36,FOLLOW_26); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              									newLeafNode(otherlv_9, grammarAccess.getAtsyraGoalAccess().getPostKeyword_1_0_0());
            	              								
            	            }
            	            // InternalAtsyRAGoal.g:1055:9: (otherlv_10= 'with' ( ( ruleEString ) ) )?
            	            int alt25=2;
            	            int LA25_0 = input.LA(1);

            	            if ( (LA25_0==34) ) {
            	                alt25=1;
            	            }
            	            switch (alt25) {
            	                case 1 :
            	                    // InternalAtsyRAGoal.g:1056:10: otherlv_10= 'with' ( ( ruleEString ) )
            	                    {
            	                    otherlv_10=(Token)match(input,34,FOLLOW_6); if (state.failed) return current;
            	                    if ( state.backtracking==0 ) {

            	                      										newLeafNode(otherlv_10, grammarAccess.getAtsyraGoalAccess().getWithKeyword_1_0_1_0());
            	                      									
            	                    }
            	                    // InternalAtsyRAGoal.g:1060:10: ( ( ruleEString ) )
            	                    // InternalAtsyRAGoal.g:1061:11: ( ruleEString )
            	                    {
            	                    // InternalAtsyRAGoal.g:1061:11: ( ruleEString )
            	                    // InternalAtsyRAGoal.g:1062:12: ruleEString
            	                    {
            	                    if ( state.backtracking==0 ) {

            	                      												if (current==null) {
            	                      													current = createModelElement(grammarAccess.getAtsyraGoalRule());
            	                      												}
            	                      											
            	                    }
            	                    if ( state.backtracking==0 ) {

            	                      												newCompositeNode(grammarAccess.getAtsyraGoalAccess().getDefaultUsedInPostDefaultValuesCrossReference_1_0_1_1_0());
            	                      											
            	                    }
            	                    pushFollow(FOLLOW_27);
            	                    ruleEString();

            	                    state._fsp--;
            	                    if (state.failed) return current;
            	                    if ( state.backtracking==0 ) {

            	                      												afterParserOrEnumRuleCall();
            	                      											
            	                    }

            	                    }


            	                    }


            	                    }
            	                    break;

            	            }

            	            otherlv_12=(Token)match(input,35,FOLLOW_19); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              									newLeafNode(otherlv_12, grammarAccess.getAtsyraGoalAccess().getColonKeyword_1_0_2());
            	              								
            	            }
            	            // InternalAtsyRAGoal.g:1081:9: ( (lv_postcondition_13_0= ruleGoalCondition ) )
            	            // InternalAtsyRAGoal.g:1082:10: (lv_postcondition_13_0= ruleGoalCondition )
            	            {
            	            // InternalAtsyRAGoal.g:1082:10: (lv_postcondition_13_0= ruleGoalCondition )
            	            // InternalAtsyRAGoal.g:1083:11: lv_postcondition_13_0= ruleGoalCondition
            	            {
            	            if ( state.backtracking==0 ) {

            	              											newCompositeNode(grammarAccess.getAtsyraGoalAccess().getPostconditionGoalConditionParserRuleCall_1_0_3_0());
            	              										
            	            }
            	            pushFollow(FOLLOW_17);
            	            lv_postcondition_13_0=ruleGoalCondition();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              											if (current==null) {
            	              												current = createModelElementForParent(grammarAccess.getAtsyraGoalRule());
            	              											}
            	              											set(
            	              												current,
            	              												"postcondition",
            	              												lv_postcondition_13_0,
            	              												"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.GoalCondition");
            	              											afterParserOrEnumRuleCall();
            	              										
            	            }

            	            }


            	            }


            	            }
            	            break;

            	    }

            	    otherlv_14=(Token)match(input,14,FOLLOW_28); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_14, grammarAccess.getAtsyraGoalAccess().getRightCurlyBracketKeyword_1_1());
            	      							
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup());

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt27 >= 1 ) break loop27;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(27, input);
                        throw eee;
                }
                cnt27++;
            } while (true);

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup()) ) {
                if (state.backtracking>0) {state.failed=true; return current;}
                throw new FailedPredicateException(input, "ruleAtsyraGoal", "getUnorderedGroupHelper().canLeave(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup())");
            }

            }


            }

            getUnorderedGroupHelper().leave(grammarAccess.getAtsyraGoalAccess().getUnorderedGroup());

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtsyraGoal"


    // $ANTLR start "entryRuleDefaultValues"
    // InternalAtsyRAGoal.g:1122:1: entryRuleDefaultValues returns [EObject current=null] : iv_ruleDefaultValues= ruleDefaultValues EOF ;
    public final EObject entryRuleDefaultValues() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefaultValues = null;


        try {
            // InternalAtsyRAGoal.g:1122:54: (iv_ruleDefaultValues= ruleDefaultValues EOF )
            // InternalAtsyRAGoal.g:1123:2: iv_ruleDefaultValues= ruleDefaultValues EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDefaultValuesRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDefaultValues=ruleDefaultValues();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDefaultValues; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefaultValues"


    // $ANTLR start "ruleDefaultValues"
    // InternalAtsyRAGoal.g:1129:1: ruleDefaultValues returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments ) ) (otherlv_4= ',' ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) ) )* )? otherlv_6= '}' ) ;
    public final EObject ruleDefaultValues() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_defaultValueAssignments_3_0 = null;

        EObject lv_defaultValueAssignments_5_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1135:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments ) ) (otherlv_4= ',' ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) ) )* )? otherlv_6= '}' ) )
            // InternalAtsyRAGoal.g:1136:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments ) ) (otherlv_4= ',' ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) ) )* )? otherlv_6= '}' )
            {
            // InternalAtsyRAGoal.g:1136:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments ) ) (otherlv_4= ',' ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) ) )* )? otherlv_6= '}' )
            // InternalAtsyRAGoal.g:1137:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments ) ) (otherlv_4= ',' ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) ) )* )? otherlv_6= '}'
            {
            // InternalAtsyRAGoal.g:1137:3: ()
            // InternalAtsyRAGoal.g:1138:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getDefaultValuesAccess().getDefaultValuesAction_0(),
              					current);
              			
            }

            }

            // InternalAtsyRAGoal.g:1144:3: ( (lv_name_1_0= ruleEString ) )
            // InternalAtsyRAGoal.g:1145:4: (lv_name_1_0= ruleEString )
            {
            // InternalAtsyRAGoal.g:1145:4: (lv_name_1_0= ruleEString )
            // InternalAtsyRAGoal.g:1146:5: lv_name_1_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDefaultValuesAccess().getNameEStringParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getDefaultValuesRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getDefaultValuesAccess().getLeftCurlyBracketKeyword_2());
              		
            }
            // InternalAtsyRAGoal.g:1167:3: ( ( (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments ) ) (otherlv_4= ',' ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) ) )* )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( ((LA29_0>=RULE_STRING && LA29_0<=RULE_ID)) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalAtsyRAGoal.g:1168:4: ( (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments ) ) (otherlv_4= ',' ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) ) )*
                    {
                    // InternalAtsyRAGoal.g:1168:4: ( (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments ) )
                    // InternalAtsyRAGoal.g:1169:5: (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments )
                    {
                    // InternalAtsyRAGoal.g:1169:5: (lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments )
                    // InternalAtsyRAGoal.g:1170:6: lv_defaultValueAssignments_3_0= ruleDefaultValueAssignments
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsDefaultValueAssignmentsParserRuleCall_3_0_0());
                      					
                    }
                    pushFollow(FOLLOW_29);
                    lv_defaultValueAssignments_3_0=ruleDefaultValueAssignments();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getDefaultValuesRule());
                      						}
                      						add(
                      							current,
                      							"defaultValueAssignments",
                      							lv_defaultValueAssignments_3_0,
                      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.DefaultValueAssignments");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalAtsyRAGoal.g:1187:4: (otherlv_4= ',' ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) ) )*
                    loop28:
                    do {
                        int alt28=2;
                        int LA28_0 = input.LA(1);

                        if ( (LA28_0==37) ) {
                            alt28=1;
                        }


                        switch (alt28) {
                    	case 1 :
                    	    // InternalAtsyRAGoal.g:1188:5: otherlv_4= ',' ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) )
                    	    {
                    	    otherlv_4=(Token)match(input,37,FOLLOW_6); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_4, grammarAccess.getDefaultValuesAccess().getCommaKeyword_3_1_0());
                    	      				
                    	    }
                    	    // InternalAtsyRAGoal.g:1192:5: ( (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments ) )
                    	    // InternalAtsyRAGoal.g:1193:6: (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments )
                    	    {
                    	    // InternalAtsyRAGoal.g:1193:6: (lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments )
                    	    // InternalAtsyRAGoal.g:1194:7: lv_defaultValueAssignments_5_0= ruleDefaultValueAssignments
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getDefaultValuesAccess().getDefaultValueAssignmentsDefaultValueAssignmentsParserRuleCall_3_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_29);
                    	    lv_defaultValueAssignments_5_0=ruleDefaultValueAssignments();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getDefaultValuesRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"defaultValueAssignments",
                    	      								lv_defaultValueAssignments_5_0,
                    	      								"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.DefaultValueAssignments");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop28;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getDefaultValuesAccess().getRightCurlyBracketKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefaultValues"


    // $ANTLR start "entryRuleDefaultValueAssignments"
    // InternalAtsyRAGoal.g:1221:1: entryRuleDefaultValueAssignments returns [EObject current=null] : iv_ruleDefaultValueAssignments= ruleDefaultValueAssignments EOF ;
    public final EObject entryRuleDefaultValueAssignments() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefaultValueAssignments = null;


        try {
            // InternalAtsyRAGoal.g:1221:64: (iv_ruleDefaultValueAssignments= ruleDefaultValueAssignments EOF )
            // InternalAtsyRAGoal.g:1222:2: iv_ruleDefaultValueAssignments= ruleDefaultValueAssignments EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDefaultValueAssignmentsRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDefaultValueAssignments=ruleDefaultValueAssignments();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDefaultValueAssignments; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefaultValueAssignments"


    // $ANTLR start "ruleDefaultValueAssignments"
    // InternalAtsyRAGoal.g:1228:1: ruleDefaultValueAssignments returns [EObject current=null] : ( () ( ( ruleEString ) ) otherlv_2= '=' ( ( ruleTypedElementString ) ) ) ;
    public final EObject ruleDefaultValueAssignments() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1234:2: ( ( () ( ( ruleEString ) ) otherlv_2= '=' ( ( ruleTypedElementString ) ) ) )
            // InternalAtsyRAGoal.g:1235:2: ( () ( ( ruleEString ) ) otherlv_2= '=' ( ( ruleTypedElementString ) ) )
            {
            // InternalAtsyRAGoal.g:1235:2: ( () ( ( ruleEString ) ) otherlv_2= '=' ( ( ruleTypedElementString ) ) )
            // InternalAtsyRAGoal.g:1236:3: () ( ( ruleEString ) ) otherlv_2= '=' ( ( ruleTypedElementString ) )
            {
            // InternalAtsyRAGoal.g:1236:3: ()
            // InternalAtsyRAGoal.g:1237:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getDefaultValueAssignmentsAccess().getDefaultAssignmentAction_0(),
              					current);
              			
            }

            }

            // InternalAtsyRAGoal.g:1243:3: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:1244:4: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:1244:4: ( ruleEString )
            // InternalAtsyRAGoal.g:1245:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDefaultValueAssignmentsRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDefaultValueAssignmentsAccess().getTargetSystemFeatureCrossReference_1_0());
              				
            }
            pushFollow(FOLLOW_22);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,28,FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getDefaultValueAssignmentsAccess().getEqualsSignKeyword_2());
              		
            }
            // InternalAtsyRAGoal.g:1263:3: ( ( ruleTypedElementString ) )
            // InternalAtsyRAGoal.g:1264:4: ( ruleTypedElementString )
            {
            // InternalAtsyRAGoal.g:1264:4: ( ruleTypedElementString )
            // InternalAtsyRAGoal.g:1265:5: ruleTypedElementString
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDefaultValueAssignmentsRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDefaultValueAssignmentsAccess().getAssignedValueTypedElementCrossReference_3_0());
              				
            }
            pushFollow(FOLLOW_2);
            ruleTypedElementString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefaultValueAssignments"


    // $ANTLR start "entryRuleAbstractAtsyraTree"
    // InternalAtsyRAGoal.g:1283:1: entryRuleAbstractAtsyraTree returns [EObject current=null] : iv_ruleAbstractAtsyraTree= ruleAbstractAtsyraTree EOF ;
    public final EObject entryRuleAbstractAtsyraTree() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractAtsyraTree = null;


        try {
            // InternalAtsyRAGoal.g:1283:59: (iv_ruleAbstractAtsyraTree= ruleAbstractAtsyraTree EOF )
            // InternalAtsyRAGoal.g:1284:2: iv_ruleAbstractAtsyraTree= ruleAbstractAtsyraTree EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAbstractAtsyraTreeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAbstractAtsyraTree=ruleAbstractAtsyraTree();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAbstractAtsyraTree; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractAtsyraTree"


    // $ANTLR start "ruleAbstractAtsyraTree"
    // InternalAtsyRAGoal.g:1290:1: ruleAbstractAtsyraTree returns [EObject current=null] : (this_AtsyraTree_0= ruleAtsyraTree | this_AtsyraTreeReference_1= ruleAtsyraTreeReference ) ;
    public final EObject ruleAbstractAtsyraTree() throws RecognitionException {
        EObject current = null;

        EObject this_AtsyraTree_0 = null;

        EObject this_AtsyraTreeReference_1 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1296:2: ( (this_AtsyraTree_0= ruleAtsyraTree | this_AtsyraTreeReference_1= ruleAtsyraTreeReference ) )
            // InternalAtsyRAGoal.g:1297:2: (this_AtsyraTree_0= ruleAtsyraTree | this_AtsyraTreeReference_1= ruleAtsyraTreeReference )
            {
            // InternalAtsyRAGoal.g:1297:2: (this_AtsyraTree_0= ruleAtsyraTree | this_AtsyraTreeReference_1= ruleAtsyraTreeReference )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==RULE_STRING) ) {
                int LA30_1 = input.LA(2);

                if ( (LA30_1==38) ) {
                    alt30=1;
                }
                else if ( (LA30_1==EOF||LA30_1==25||LA30_1==37) ) {
                    alt30=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 30, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA30_0==RULE_ID) ) {
                int LA30_2 = input.LA(2);

                if ( (LA30_2==38) ) {
                    alt30=1;
                }
                else if ( (LA30_2==EOF||LA30_2==25||LA30_2==37) ) {
                    alt30=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 30, 2, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }
            switch (alt30) {
                case 1 :
                    // InternalAtsyRAGoal.g:1298:3: this_AtsyraTree_0= ruleAtsyraTree
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAbstractAtsyraTreeAccess().getAtsyraTreeParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_AtsyraTree_0=ruleAtsyraTree();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_AtsyraTree_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:1307:3: this_AtsyraTreeReference_1= ruleAtsyraTreeReference
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAbstractAtsyraTreeAccess().getAtsyraTreeReferenceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_AtsyraTreeReference_1=ruleAtsyraTreeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_AtsyraTreeReference_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractAtsyraTree"


    // $ANTLR start "entryRuleAtsyraTreeReference"
    // InternalAtsyRAGoal.g:1319:1: entryRuleAtsyraTreeReference returns [EObject current=null] : iv_ruleAtsyraTreeReference= ruleAtsyraTreeReference EOF ;
    public final EObject entryRuleAtsyraTreeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtsyraTreeReference = null;


        try {
            // InternalAtsyRAGoal.g:1319:60: (iv_ruleAtsyraTreeReference= ruleAtsyraTreeReference EOF )
            // InternalAtsyRAGoal.g:1320:2: iv_ruleAtsyraTreeReference= ruleAtsyraTreeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAtsyraTreeReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAtsyraTreeReference=ruleAtsyraTreeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAtsyraTreeReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtsyraTreeReference"


    // $ANTLR start "ruleAtsyraTreeReference"
    // InternalAtsyRAGoal.g:1326:1: ruleAtsyraTreeReference returns [EObject current=null] : ( ( ruleEString ) ) ;
    public final EObject ruleAtsyraTreeReference() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1332:2: ( ( ( ruleEString ) ) )
            // InternalAtsyRAGoal.g:1333:2: ( ( ruleEString ) )
            {
            // InternalAtsyRAGoal.g:1333:2: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:1334:3: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:1334:3: ( ruleEString )
            // InternalAtsyRAGoal.g:1335:4: ruleEString
            {
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getAtsyraTreeReferenceRule());
              				}
              			
            }
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getAtsyraTreeReferenceAccess().getReferencedTreeAtsyraTreeCrossReference_0());
              			
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtsyraTreeReference"


    // $ANTLR start "entryRuleAtsyraTree"
    // InternalAtsyRAGoal.g:1352:1: entryRuleAtsyraTree returns [EObject current=null] : iv_ruleAtsyraTree= ruleAtsyraTree EOF ;
    public final EObject entryRuleAtsyraTree() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtsyraTree = null;


        try {
            // InternalAtsyRAGoal.g:1352:51: (iv_ruleAtsyraTree= ruleAtsyraTree EOF )
            // InternalAtsyRAGoal.g:1353:2: iv_ruleAtsyraTree= ruleAtsyraTree EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAtsyraTreeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAtsyraTree=ruleAtsyraTree();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAtsyraTree; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtsyraTree"


    // $ANTLR start "ruleAtsyraTree"
    // InternalAtsyRAGoal.g:1359:1: ruleAtsyraTree returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '[' ( (otherlv_3= RULE_ID ) )? otherlv_4= ']' ( ( (lv_operator_5_0= ruleAtsyraTreeOperator ) ) otherlv_6= '(' ( (lv_operands_7_0= ruleAbstractAtsyraTree ) ) (otherlv_8= ',' ( (lv_operands_9_0= ruleAbstractAtsyraTree ) ) )* otherlv_10= ')' )? ) ;
    public final EObject ruleAtsyraTree() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        Enumerator lv_operator_5_0 = null;

        EObject lv_operands_7_0 = null;

        EObject lv_operands_9_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1365:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '[' ( (otherlv_3= RULE_ID ) )? otherlv_4= ']' ( ( (lv_operator_5_0= ruleAtsyraTreeOperator ) ) otherlv_6= '(' ( (lv_operands_7_0= ruleAbstractAtsyraTree ) ) (otherlv_8= ',' ( (lv_operands_9_0= ruleAbstractAtsyraTree ) ) )* otherlv_10= ')' )? ) )
            // InternalAtsyRAGoal.g:1366:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '[' ( (otherlv_3= RULE_ID ) )? otherlv_4= ']' ( ( (lv_operator_5_0= ruleAtsyraTreeOperator ) ) otherlv_6= '(' ( (lv_operands_7_0= ruleAbstractAtsyraTree ) ) (otherlv_8= ',' ( (lv_operands_9_0= ruleAbstractAtsyraTree ) ) )* otherlv_10= ')' )? )
            {
            // InternalAtsyRAGoal.g:1366:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '[' ( (otherlv_3= RULE_ID ) )? otherlv_4= ']' ( ( (lv_operator_5_0= ruleAtsyraTreeOperator ) ) otherlv_6= '(' ( (lv_operands_7_0= ruleAbstractAtsyraTree ) ) (otherlv_8= ',' ( (lv_operands_9_0= ruleAbstractAtsyraTree ) ) )* otherlv_10= ')' )? )
            // InternalAtsyRAGoal.g:1367:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '[' ( (otherlv_3= RULE_ID ) )? otherlv_4= ']' ( ( (lv_operator_5_0= ruleAtsyraTreeOperator ) ) otherlv_6= '(' ( (lv_operands_7_0= ruleAbstractAtsyraTree ) ) (otherlv_8= ',' ( (lv_operands_9_0= ruleAbstractAtsyraTree ) ) )* otherlv_10= ')' )?
            {
            // InternalAtsyRAGoal.g:1367:3: ()
            // InternalAtsyRAGoal.g:1368:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getAtsyraTreeAccess().getAtsyraTreeAction_0(),
              					current);
              			
            }

            }

            // InternalAtsyRAGoal.g:1374:3: ( (lv_name_1_0= ruleEString ) )
            // InternalAtsyRAGoal.g:1375:4: (lv_name_1_0= ruleEString )
            {
            // InternalAtsyRAGoal.g:1375:4: (lv_name_1_0= ruleEString )
            // InternalAtsyRAGoal.g:1376:5: lv_name_1_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAtsyraTreeAccess().getNameEStringParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_30);
            lv_name_1_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getAtsyraTreeRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,38,FOLLOW_31); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getAtsyraTreeAccess().getLeftSquareBracketKeyword_2());
              		
            }
            // InternalAtsyRAGoal.g:1397:3: ( (otherlv_3= RULE_ID ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==RULE_ID) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalAtsyRAGoal.g:1398:4: (otherlv_3= RULE_ID )
                    {
                    // InternalAtsyRAGoal.g:1398:4: (otherlv_3= RULE_ID )
                    // InternalAtsyRAGoal.g:1399:5: otherlv_3= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getAtsyraTreeRule());
                      					}
                      				
                    }
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_3, grammarAccess.getAtsyraTreeAccess().getMainGoalAtsyraGoalCrossReference_3_0());
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,39,FOLLOW_33); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getAtsyraTreeAccess().getRightSquareBracketKeyword_4());
              		
            }
            // InternalAtsyRAGoal.g:1414:3: ( ( (lv_operator_5_0= ruleAtsyraTreeOperator ) ) otherlv_6= '(' ( (lv_operands_7_0= ruleAbstractAtsyraTree ) ) (otherlv_8= ',' ( (lv_operands_9_0= ruleAbstractAtsyraTree ) ) )* otherlv_10= ')' )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==21||LA33_0==23||LA33_0==42) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalAtsyRAGoal.g:1415:4: ( (lv_operator_5_0= ruleAtsyraTreeOperator ) ) otherlv_6= '(' ( (lv_operands_7_0= ruleAbstractAtsyraTree ) ) (otherlv_8= ',' ( (lv_operands_9_0= ruleAbstractAtsyraTree ) ) )* otherlv_10= ')'
                    {
                    // InternalAtsyRAGoal.g:1415:4: ( (lv_operator_5_0= ruleAtsyraTreeOperator ) )
                    // InternalAtsyRAGoal.g:1416:5: (lv_operator_5_0= ruleAtsyraTreeOperator )
                    {
                    // InternalAtsyRAGoal.g:1416:5: (lv_operator_5_0= ruleAtsyraTreeOperator )
                    // InternalAtsyRAGoal.g:1417:6: lv_operator_5_0= ruleAtsyraTreeOperator
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAtsyraTreeAccess().getOperatorAtsyraTreeOperatorEnumRuleCall_5_0_0());
                      					
                    }
                    pushFollow(FOLLOW_34);
                    lv_operator_5_0=ruleAtsyraTreeOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAtsyraTreeRule());
                      						}
                      						set(
                      							current,
                      							"operator",
                      							lv_operator_5_0,
                      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.AtsyraTreeOperator");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_6=(Token)match(input,24,FOLLOW_6); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getAtsyraTreeAccess().getLeftParenthesisKeyword_5_1());
                      			
                    }
                    // InternalAtsyRAGoal.g:1438:4: ( (lv_operands_7_0= ruleAbstractAtsyraTree ) )
                    // InternalAtsyRAGoal.g:1439:5: (lv_operands_7_0= ruleAbstractAtsyraTree )
                    {
                    // InternalAtsyRAGoal.g:1439:5: (lv_operands_7_0= ruleAbstractAtsyraTree )
                    // InternalAtsyRAGoal.g:1440:6: lv_operands_7_0= ruleAbstractAtsyraTree
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAtsyraTreeAccess().getOperandsAbstractAtsyraTreeParserRuleCall_5_2_0());
                      					
                    }
                    pushFollow(FOLLOW_35);
                    lv_operands_7_0=ruleAbstractAtsyraTree();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAtsyraTreeRule());
                      						}
                      						add(
                      							current,
                      							"operands",
                      							lv_operands_7_0,
                      							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.AbstractAtsyraTree");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalAtsyRAGoal.g:1457:4: (otherlv_8= ',' ( (lv_operands_9_0= ruleAbstractAtsyraTree ) ) )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==37) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalAtsyRAGoal.g:1458:5: otherlv_8= ',' ( (lv_operands_9_0= ruleAbstractAtsyraTree ) )
                    	    {
                    	    otherlv_8=(Token)match(input,37,FOLLOW_6); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_8, grammarAccess.getAtsyraTreeAccess().getCommaKeyword_5_3_0());
                    	      				
                    	    }
                    	    // InternalAtsyRAGoal.g:1462:5: ( (lv_operands_9_0= ruleAbstractAtsyraTree ) )
                    	    // InternalAtsyRAGoal.g:1463:6: (lv_operands_9_0= ruleAbstractAtsyraTree )
                    	    {
                    	    // InternalAtsyRAGoal.g:1463:6: (lv_operands_9_0= ruleAbstractAtsyraTree )
                    	    // InternalAtsyRAGoal.g:1464:7: lv_operands_9_0= ruleAbstractAtsyraTree
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getAtsyraTreeAccess().getOperandsAbstractAtsyraTreeParserRuleCall_5_3_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_35);
                    	    lv_operands_9_0=ruleAbstractAtsyraTree();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getAtsyraTreeRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"operands",
                    	      								lv_operands_9_0,
                    	      								"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.AbstractAtsyraTree");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);

                    otherlv_10=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_10, grammarAccess.getAtsyraTreeAccess().getRightParenthesisKeyword_5_4());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtsyraTree"


    // $ANTLR start "entryRuleBooleanLiteral"
    // InternalAtsyRAGoal.g:1491:1: entryRuleBooleanLiteral returns [EObject current=null] : iv_ruleBooleanLiteral= ruleBooleanLiteral EOF ;
    public final EObject entryRuleBooleanLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanLiteral = null;


        try {
            // InternalAtsyRAGoal.g:1491:55: (iv_ruleBooleanLiteral= ruleBooleanLiteral EOF )
            // InternalAtsyRAGoal.g:1492:2: iv_ruleBooleanLiteral= ruleBooleanLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanLiteralRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBooleanLiteral=ruleBooleanLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanLiteral; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanLiteral"


    // $ANTLR start "ruleBooleanLiteral"
    // InternalAtsyRAGoal.g:1498:1: ruleBooleanLiteral returns [EObject current=null] : ( () ( ( (lv_name_1_1= 'true' | lv_name_1_2= 'false' ) ) ) otherlv_2= '(' ( ( ruleEString ) ) otherlv_4= ')' ) ;
    public final EObject ruleBooleanLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_1=null;
        Token lv_name_1_2=null;
        Token otherlv_2=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1504:2: ( ( () ( ( (lv_name_1_1= 'true' | lv_name_1_2= 'false' ) ) ) otherlv_2= '(' ( ( ruleEString ) ) otherlv_4= ')' ) )
            // InternalAtsyRAGoal.g:1505:2: ( () ( ( (lv_name_1_1= 'true' | lv_name_1_2= 'false' ) ) ) otherlv_2= '(' ( ( ruleEString ) ) otherlv_4= ')' )
            {
            // InternalAtsyRAGoal.g:1505:2: ( () ( ( (lv_name_1_1= 'true' | lv_name_1_2= 'false' ) ) ) otherlv_2= '(' ( ( ruleEString ) ) otherlv_4= ')' )
            // InternalAtsyRAGoal.g:1506:3: () ( ( (lv_name_1_1= 'true' | lv_name_1_2= 'false' ) ) ) otherlv_2= '(' ( ( ruleEString ) ) otherlv_4= ')'
            {
            // InternalAtsyRAGoal.g:1506:3: ()
            // InternalAtsyRAGoal.g:1507:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getBooleanLiteralAccess().getBooleanLiteralAction_0(),
              					current);
              			
            }

            }

            // InternalAtsyRAGoal.g:1513:3: ( ( (lv_name_1_1= 'true' | lv_name_1_2= 'false' ) ) )
            // InternalAtsyRAGoal.g:1514:4: ( (lv_name_1_1= 'true' | lv_name_1_2= 'false' ) )
            {
            // InternalAtsyRAGoal.g:1514:4: ( (lv_name_1_1= 'true' | lv_name_1_2= 'false' ) )
            // InternalAtsyRAGoal.g:1515:5: (lv_name_1_1= 'true' | lv_name_1_2= 'false' )
            {
            // InternalAtsyRAGoal.g:1515:5: (lv_name_1_1= 'true' | lv_name_1_2= 'false' )
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==30) ) {
                alt34=1;
            }
            else if ( (LA34_0==31) ) {
                alt34=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }
            switch (alt34) {
                case 1 :
                    // InternalAtsyRAGoal.g:1516:6: lv_name_1_1= 'true'
                    {
                    lv_name_1_1=(Token)match(input,30,FOLLOW_34); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_1_1, grammarAccess.getBooleanLiteralAccess().getNameTrueKeyword_1_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getBooleanLiteralRule());
                      						}
                      						setWithLastConsumed(current, "name", lv_name_1_1, null);
                      					
                    }

                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:1527:6: lv_name_1_2= 'false'
                    {
                    lv_name_1_2=(Token)match(input,31,FOLLOW_34); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_1_2, grammarAccess.getBooleanLiteralAccess().getNameFalseKeyword_1_0_1());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getBooleanLiteralRule());
                      						}
                      						setWithLastConsumed(current, "name", lv_name_1_2, null);
                      					
                    }

                    }
                    break;

            }


            }


            }

            otherlv_2=(Token)match(input,24,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getBooleanLiteralAccess().getLeftParenthesisKeyword_2());
              		
            }
            // InternalAtsyRAGoal.g:1544:3: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:1545:4: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:1545:4: ( ruleEString )
            // InternalAtsyRAGoal.g:1546:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBooleanLiteralRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBooleanLiteralAccess().getTypeTypeCrossReference_3_0());
              				
            }
            pushFollow(FOLLOW_21);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getBooleanLiteralAccess().getRightParenthesisKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanLiteral"


    // $ANTLR start "entryRuleBuildingSystemFeature"
    // InternalAtsyRAGoal.g:1568:1: entryRuleBuildingSystemFeature returns [EObject current=null] : iv_ruleBuildingSystemFeature= ruleBuildingSystemFeature EOF ;
    public final EObject entryRuleBuildingSystemFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBuildingSystemFeature = null;


        try {
            // InternalAtsyRAGoal.g:1568:62: (iv_ruleBuildingSystemFeature= ruleBuildingSystemFeature EOF )
            // InternalAtsyRAGoal.g:1569:2: iv_ruleBuildingSystemFeature= ruleBuildingSystemFeature EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBuildingSystemFeatureRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBuildingSystemFeature=ruleBuildingSystemFeature();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBuildingSystemFeature; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBuildingSystemFeature"


    // $ANTLR start "ruleBuildingSystemFeature"
    // InternalAtsyRAGoal.g:1575:1: ruleBuildingSystemFeature returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')' ) ;
    public final EObject ruleBuildingSystemFeature() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1581:2: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')' ) )
            // InternalAtsyRAGoal.g:1582:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')' )
            {
            // InternalAtsyRAGoal.g:1582:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')' )
            // InternalAtsyRAGoal.g:1583:3: ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')'
            {
            // InternalAtsyRAGoal.g:1583:3: ( (lv_name_0_0= ruleEString ) )
            // InternalAtsyRAGoal.g:1584:4: (lv_name_0_0= ruleEString )
            {
            // InternalAtsyRAGoal.g:1584:4: (lv_name_0_0= ruleEString )
            // InternalAtsyRAGoal.g:1585:5: lv_name_0_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBuildingSystemFeatureAccess().getNameEStringParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_34);
            lv_name_0_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBuildingSystemFeatureRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_0_0,
              						"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,24,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getBuildingSystemFeatureAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalAtsyRAGoal.g:1606:3: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:1607:4: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:1607:4: ( ruleEString )
            // InternalAtsyRAGoal.g:1608:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBuildingSystemFeatureRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBuildingSystemFeatureAccess().getTypeTypeCrossReference_2_0());
              				
            }
            pushFollow(FOLLOW_21);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getBuildingSystemFeatureAccess().getRightParenthesisKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBuildingSystemFeature"


    // $ANTLR start "entryRuleBuildingSystemConstFeature"
    // InternalAtsyRAGoal.g:1630:1: entryRuleBuildingSystemConstFeature returns [EObject current=null] : iv_ruleBuildingSystemConstFeature= ruleBuildingSystemConstFeature EOF ;
    public final EObject entryRuleBuildingSystemConstFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBuildingSystemConstFeature = null;


        try {
            // InternalAtsyRAGoal.g:1630:67: (iv_ruleBuildingSystemConstFeature= ruleBuildingSystemConstFeature EOF )
            // InternalAtsyRAGoal.g:1631:2: iv_ruleBuildingSystemConstFeature= ruleBuildingSystemConstFeature EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBuildingSystemConstFeatureRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBuildingSystemConstFeature=ruleBuildingSystemConstFeature();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBuildingSystemConstFeature; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBuildingSystemConstFeature"


    // $ANTLR start "ruleBuildingSystemConstFeature"
    // InternalAtsyRAGoal.g:1637:1: ruleBuildingSystemConstFeature returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')' ) ;
    public final EObject ruleBuildingSystemConstFeature() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1643:2: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')' ) )
            // InternalAtsyRAGoal.g:1644:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')' )
            {
            // InternalAtsyRAGoal.g:1644:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')' )
            // InternalAtsyRAGoal.g:1645:3: ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( ruleEString ) ) otherlv_3= ')'
            {
            // InternalAtsyRAGoal.g:1645:3: ( (lv_name_0_0= ruleEString ) )
            // InternalAtsyRAGoal.g:1646:4: (lv_name_0_0= ruleEString )
            {
            // InternalAtsyRAGoal.g:1646:4: (lv_name_0_0= ruleEString )
            // InternalAtsyRAGoal.g:1647:5: lv_name_0_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBuildingSystemConstFeatureAccess().getNameEStringParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_34);
            lv_name_0_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBuildingSystemConstFeatureRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_0_0,
              						"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,24,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getBuildingSystemConstFeatureAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalAtsyRAGoal.g:1668:3: ( ( ruleEString ) )
            // InternalAtsyRAGoal.g:1669:4: ( ruleEString )
            {
            // InternalAtsyRAGoal.g:1669:4: ( ruleEString )
            // InternalAtsyRAGoal.g:1670:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBuildingSystemConstFeatureRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBuildingSystemConstFeatureAccess().getTypeTypeCrossReference_2_0());
              				
            }
            pushFollow(FOLLOW_21);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getBuildingSystemConstFeatureAccess().getRightParenthesisKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBuildingSystemConstFeature"


    // $ANTLR start "entryRuleInternalType"
    // InternalAtsyRAGoal.g:1692:1: entryRuleInternalType returns [EObject current=null] : iv_ruleInternalType= ruleInternalType EOF ;
    public final EObject entryRuleInternalType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInternalType = null;


        try {
            // InternalAtsyRAGoal.g:1692:53: (iv_ruleInternalType= ruleInternalType EOF )
            // InternalAtsyRAGoal.g:1693:2: iv_ruleInternalType= ruleInternalType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInternalTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInternalType=ruleInternalType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInternalType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInternalType"


    // $ANTLR start "ruleInternalType"
    // InternalAtsyRAGoal.g:1699:1: ruleInternalType returns [EObject current=null] : ( () otherlv_1= 'InternalType' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleInternalType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1705:2: ( ( () otherlv_1= 'InternalType' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalAtsyRAGoal.g:1706:2: ( () otherlv_1= 'InternalType' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalAtsyRAGoal.g:1706:2: ( () otherlv_1= 'InternalType' ( (lv_name_2_0= ruleEString ) ) )
            // InternalAtsyRAGoal.g:1707:3: () otherlv_1= 'InternalType' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalAtsyRAGoal.g:1707:3: ()
            // InternalAtsyRAGoal.g:1708:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getInternalTypeAccess().getInternalTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,40,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getInternalTypeAccess().getInternalTypeKeyword_1());
              		
            }
            // InternalAtsyRAGoal.g:1718:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAtsyRAGoal.g:1719:4: (lv_name_2_0= ruleEString )
            {
            // InternalAtsyRAGoal.g:1719:4: (lv_name_2_0= ruleEString )
            // InternalAtsyRAGoal.g:1720:5: lv_name_2_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInternalTypeAccess().getNameEStringParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInternalTypeRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_2_0,
              						"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInternalType"


    // $ANTLR start "entryRuleSystemType"
    // InternalAtsyRAGoal.g:1741:1: entryRuleSystemType returns [EObject current=null] : iv_ruleSystemType= ruleSystemType EOF ;
    public final EObject entryRuleSystemType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSystemType = null;


        try {
            // InternalAtsyRAGoal.g:1741:51: (iv_ruleSystemType= ruleSystemType EOF )
            // InternalAtsyRAGoal.g:1742:2: iv_ruleSystemType= ruleSystemType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSystemTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSystemType=ruleSystemType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSystemType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSystemType"


    // $ANTLR start "ruleSystemType"
    // InternalAtsyRAGoal.g:1748:1: ruleSystemType returns [EObject current=null] : ( () otherlv_1= 'SystemType' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleSystemType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1754:2: ( ( () otherlv_1= 'SystemType' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalAtsyRAGoal.g:1755:2: ( () otherlv_1= 'SystemType' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalAtsyRAGoal.g:1755:2: ( () otherlv_1= 'SystemType' ( (lv_name_2_0= ruleEString ) ) )
            // InternalAtsyRAGoal.g:1756:3: () otherlv_1= 'SystemType' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalAtsyRAGoal.g:1756:3: ()
            // InternalAtsyRAGoal.g:1757:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getSystemTypeAccess().getSystemTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,41,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getSystemTypeAccess().getSystemTypeKeyword_1());
              		
            }
            // InternalAtsyRAGoal.g:1767:3: ( (lv_name_2_0= ruleEString ) )
            // InternalAtsyRAGoal.g:1768:4: (lv_name_2_0= ruleEString )
            {
            // InternalAtsyRAGoal.g:1768:4: (lv_name_2_0= ruleEString )
            // InternalAtsyRAGoal.g:1769:5: lv_name_2_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSystemTypeAccess().getNameEStringParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSystemTypeRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_2_0,
              						"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSystemType"


    // $ANTLR start "ruleAtsyraTreeOperator"
    // InternalAtsyRAGoal.g:1790:1: ruleAtsyraTreeOperator returns [Enumerator current=null] : ( (enumLiteral_0= 'OR' ) | (enumLiteral_1= 'SAND' ) | (enumLiteral_2= 'AND' ) ) ;
    public final Enumerator ruleAtsyraTreeOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalAtsyRAGoal.g:1796:2: ( ( (enumLiteral_0= 'OR' ) | (enumLiteral_1= 'SAND' ) | (enumLiteral_2= 'AND' ) ) )
            // InternalAtsyRAGoal.g:1797:2: ( (enumLiteral_0= 'OR' ) | (enumLiteral_1= 'SAND' ) | (enumLiteral_2= 'AND' ) )
            {
            // InternalAtsyRAGoal.g:1797:2: ( (enumLiteral_0= 'OR' ) | (enumLiteral_1= 'SAND' ) | (enumLiteral_2= 'AND' ) )
            int alt35=3;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt35=1;
                }
                break;
            case 42:
                {
                alt35=2;
                }
                break;
            case 23:
                {
                alt35=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }

            switch (alt35) {
                case 1 :
                    // InternalAtsyRAGoal.g:1798:3: (enumLiteral_0= 'OR' )
                    {
                    // InternalAtsyRAGoal.g:1798:3: (enumLiteral_0= 'OR' )
                    // InternalAtsyRAGoal.g:1799:4: enumLiteral_0= 'OR'
                    {
                    enumLiteral_0=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getAtsyraTreeOperatorAccess().getOREnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getAtsyraTreeOperatorAccess().getOREnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalAtsyRAGoal.g:1806:3: (enumLiteral_1= 'SAND' )
                    {
                    // InternalAtsyRAGoal.g:1806:3: (enumLiteral_1= 'SAND' )
                    // InternalAtsyRAGoal.g:1807:4: enumLiteral_1= 'SAND'
                    {
                    enumLiteral_1=(Token)match(input,42,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getAtsyraTreeOperatorAccess().getSANDEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getAtsyraTreeOperatorAccess().getSANDEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalAtsyRAGoal.g:1814:3: (enumLiteral_2= 'AND' )
                    {
                    // InternalAtsyRAGoal.g:1814:3: (enumLiteral_2= 'AND' )
                    // InternalAtsyRAGoal.g:1815:4: enumLiteral_2= 'AND'
                    {
                    enumLiteral_2=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getAtsyraTreeOperatorAccess().getANDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getAtsyraTreeOperatorAccess().getANDEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtsyraTreeOperator"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000080800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000007E000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004030L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000007C000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000030000000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000030000004000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000074000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000020000030L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000020004030L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000064000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000001100004000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000044000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000300002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x00000000CD000030L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000C00002L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x00000000C0000030L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x00000000C0000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000001300004002L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000C00000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000001100004002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000002000004000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000008000000020L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000040000A00002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000002002000000L});

}
