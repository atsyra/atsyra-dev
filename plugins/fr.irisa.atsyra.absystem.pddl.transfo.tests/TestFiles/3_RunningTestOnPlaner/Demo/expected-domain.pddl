( define

	( domain definitionGroup )

	( :types
		City
		Object
		Traveler
	)

	( :predicates
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City ?c - City ?c1 - City )
		( location-OneMutableReference_From_Traveler_To_City ?t - Traveler ?c - City )
		( ownedObjects-ZeroOrManyMutableReference_From_Traveler_To_Object ?t - Traveler ?o - Object )
		( itemsInCity-ZeroOrManyMutableReference_From_City_To_Object ?c - City ?o - Object )
	)

	( :action picks_item_in_city
		:parameters ( ?aTraveler - Traveler ?currentCity - City ?item - Object )
		:precondition ( and
			( location-OneMutableReference_From_Traveler_To_City ?aTraveler ?currentCity )
			( itemsInCity-ZeroOrManyMutableReference_From_City_To_Object ?currentCity ?item )
		)
		:effect ( and
			( ownedObjects-ZeroOrManyMutableReference_From_Traveler_To_Object ?aTraveler ?item )
			( not ( itemsInCity-ZeroOrManyMutableReference_From_City_To_Object ?currentCity ?item ) )
		)
	)

	( :action drops_item_in_city
		:parameters ( ?aTraveler - Traveler ?currentCity - City ?item - Object )
		:precondition ( and
			( location-OneMutableReference_From_Traveler_To_City ?aTraveler ?currentCity )
			( ownedObjects-ZeroOrManyMutableReference_From_Traveler_To_Object ?aTraveler ?item )
		)
		:effect ( and
			( not ( ownedObjects-ZeroOrManyMutableReference_From_Traveler_To_Object ?aTraveler ?item ) )
			( itemsInCity-ZeroOrManyMutableReference_From_City_To_Object ?currentCity ?item )
		)
	)

	( :action goes_from_to
		:parameters ( ?aTraveler - Traveler ?startCity - City ?destCity - City )
		:precondition ( and
			( location-OneMutableReference_From_Traveler_To_City ?aTraveler ?startCity )
			( roadsTo-ZeroOrManyImmutableReference_From_City_To_City ?startCity ?destCity )
		)
		:effect ( and
			( not ( location-OneMutableReference_From_Traveler_To_City ?aTraveler ?startCity ) )
			( location-OneMutableReference_From_Traveler_To_City ?aTraveler ?destCity )
		)
	)

)