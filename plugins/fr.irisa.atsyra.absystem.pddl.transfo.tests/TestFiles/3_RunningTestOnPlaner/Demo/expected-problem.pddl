( define

	( problem DeModel )

	( :domain definitionGroup )

	( :objects
		Timi - Traveler
		TravelCase Gift - Object
		Vitregunc Ploermel Guer Comblessac Rennes Chicou Paris - City
	)

	( :init
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Vitregunc Ploermel )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Ploermel Vitregunc )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Ploermel Guer )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Guer Ploermel )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Guer Comblessac )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Comblessac Guer )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Ploermel Rennes )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Rennes Ploermel )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Comblessac Rennes )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Rennes Comblessac )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Rennes Chicou )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Chicou Rennes )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Rennes Paris )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Paris Rennes )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Chicou Paris )
		( roadsTo-ZeroOrManyImmutableReference_From_City_To_City Paris Chicou )
		( location-OneMutableReference_From_Traveler_To_City Timi Guer )
		( itemsInCity-ZeroOrManyMutableReference_From_City_To_Object Vitregunc Gift )
		( itemsInCity-ZeroOrManyMutableReference_From_City_To_Object Ploermel TravelCase )
	)

	( :goal
		( and
			( location-OneMutableReference_From_Traveler_To_City Timi Chicou )
			( ownedObjects-ZeroOrManyMutableReference_From_Traveler_To_Object Timi TravelCase )
			( itemsInCity-ZeroOrManyMutableReference_From_City_To_Object Rennes Gift )
		)
	)

)