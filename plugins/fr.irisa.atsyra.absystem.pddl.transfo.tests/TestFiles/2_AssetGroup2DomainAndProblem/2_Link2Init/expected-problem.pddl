( define

	( problem agenceModel )

	( :domain definitionGroup )

	( :objects
		SENSIBLE STANDARD - EDataSensibility
		WINDOWS LINUX MACOS - EPlateform
		NONE PASSWORD MFA TOKEN BIOMETRIC - EAuthenticationType
		USER ADMIN ROOT - EPermission
		DEFAULT LOCAL DOMAIN CLOUD - EAccountType
		rootAccountWebServer userAccountPc1 userAccountPc2 commonFileSharingAccount - Account
		credentialRootAccountWebServer credentialUserAccountPc1 credentialUserAccountPc2 - Credential
		robinHood - Attacker
		webServer nas - Server
		osWebServer osNas osPc1Bureau osPc2Openspace - OS
		exterieur patio reunion bureau couloir cageEscalier openspace accueil parking localTechnique - Zone
		commonFileSharing - FileSharing
		porteLocalTechniqueCouloir porteAccueilPrincipale - BadgeDoor
		dataWebServer dataNAS dataPc1Bureau dataPc2Openspace - Data
		fenetreReunion fenetreBureau fenetreCouloir fenetreOpenspace fenetreAccueilParking fenetreAccueilExterieur - Windows
		portePatioCouloir portePatioExterieur porteReunion porteBureau porteCageEscalier porteOpenspace porteAccueil - Door
		pc1Bureau pc2Openspace - Workstation
		badgeAccueil documentSecret badgeLocalTechnique cleBureau - Item
		hddWebServer hddNas hddPc1Bureau hddPc2Openspace - Storage
		escalier barriereParking - FreeAccess
		alarmPatio alarmCouloir alarmAccueil alarmParking1 alarmParking2 - Alarm
		agenceLocalNetwork - Network
	)

	( :init
		( EDataSensibility-SENSIBLE SENSIBLE )
		( EDataSensibility-STANDARD STANDARD )
		( EPlateform-WINDOWS WINDOWS )
		( EPlateform-LINUX LINUX )
		( EPlateform-MACOS MACOS )
		( EAuthenticationType-NONE NONE )
		( EAuthenticationType-PASSWORD PASSWORD )
		( EAuthenticationType-MFA MFA )
		( EAuthenticationType-TOKEN TOKEN )
		( EAuthenticationType-BIOMETRIC BIOMETRIC )
		( EPermission-USER USER )
		( EPermission-ADMIN ADMIN )
		( EPermission-ROOT ROOT )
		( EAccountType-DEFAULT DEFAULT )
		( EAccountType-LOCAL LOCAL )
		( EAccountType-DOMAIN DOMAIN )
		( EAccountType-CLOUD CLOUD )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone portePatioCouloir patio )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone portePatioCouloir couloir )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone portePatioExterieur patio )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone portePatioExterieur exterieur )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreBureau patio )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreBureau bureau )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreReunion patio )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreReunion reunion )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone porteBureau bureau )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone porteBureau couloir )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone porteReunion reunion )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone porteReunion couloir )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone escalier exterieur )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone escalier cageEscalier )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone porteCageEscalier cageEscalier )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone porteCageEscalier couloir )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreCouloir couloir )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreCouloir exterieur )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone porteOpenspace openspace )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone porteOpenspace couloir )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreOpenspace openspace )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreOpenspace parking )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone porteAccueil accueil )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone porteAccueil couloir )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreAccueilParking accueil )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreAccueilParking parking )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone barriereParking parking )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone barriereParking exterieur )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreAccueilExterieur accueil )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone fenetreAccueilExterieur exterieur )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone porteAccueilPrincipale accueil )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone porteAccueilPrincipale parking )
		( alarms-ZeroOrManyImmutableReference_From_Zone_To_Alarm patio alarmPatio )
		( alarms-ZeroOrManyImmutableReference_From_Zone_To_Alarm couloir alarmCouloir )
		( alarms-ZeroOrManyImmutableReference_From_Zone_To_Alarm parking alarmParking1 )
		( alarms-ZeroOrManyImmutableReference_From_Zone_To_Alarm parking alarmParking2 )
		( alarms-ZeroOrManyImmutableReference_From_Zone_To_Alarm accueil alarmAccueil )
		( badges-ZeroOrManyImmutableReference_From_BadgeDoor_To_Item porteAccueilPrincipale badgeAccueil )
		( keys-ZeroOrManyImmutableReference_From_Door_To_Item porteBureau cleBureau )
		( control-ZeroOrOneImmutableReference_From_Alarm_To_Zone alarmAccueil bureau )
		( control-ZeroOrOneImmutableReference_From_Alarm_To_Zone alarmCouloir accueil )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone porteLocalTechniqueCouloir localTechnique )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone porteLocalTechniqueCouloir couloir )
		( badges-ZeroOrManyImmutableReference_From_BadgeDoor_To_Item porteLocalTechniqueCouloir badgeLocalTechnique )
		( location-OneImmutableReference_From_Machine_To_Zone webServer localTechnique )
		( location-OneImmutableReference_From_Machine_To_Zone nas localTechnique )
		( location-OneImmutableReference_From_Machine_To_Zone pc1Bureau bureau )
		( location-OneImmutableReference_From_Machine_To_Zone pc2Openspace openspace )
		( storages-ZeroOrManyImmutableReference_From_Machine_To_Storage webServer hddWebServer )
		( storages-ZeroOrManyImmutableReference_From_Machine_To_Storage nas hddNas )
		( storages-ZeroOrManyImmutableReference_From_Machine_To_Storage pc1Bureau hddPc1Bureau )
		( storages-ZeroOrManyImmutableReference_From_Machine_To_Storage pc2Openspace hddPc2Openspace )
		( hostedBy-OneImmutableReference_From_Storage_To_Machine hddWebServer webServer )
		( hostedBy-OneImmutableReference_From_Storage_To_Machine hddNas nas )
		( hostedBy-OneImmutableReference_From_Storage_To_Machine hddPc1Bureau pc1Bureau )
		( hostedBy-OneImmutableReference_From_Storage_To_Machine hddPc2Openspace pc2Openspace )
		( data-ZeroOrManyImmutableReference_From_Storage_To_Data hddWebServer dataWebServer )
		( data-ZeroOrManyImmutableReference_From_Storage_To_Data hddNas dataNAS )
		( data-ZeroOrManyImmutableReference_From_Storage_To_Data hddPc1Bureau dataPc1Bureau )
		( data-ZeroOrManyImmutableReference_From_Storage_To_Data hddPc2Openspace dataPc2Openspace )
		( storedOn-OneImmutableReference_From_Data_To_Storage dataWebServer hddWebServer )
		( storedOn-OneImmutableReference_From_Data_To_Storage dataNAS hddNas )
		( storedOn-OneImmutableReference_From_Data_To_Storage dataPc1Bureau hddPc1Bureau )
		( storedOn-OneImmutableReference_From_Data_To_Storage dataPc2Openspace hddPc2Openspace )
		( exploitedBy-OneImmutableReference_From_Machine_To_OS webServer osWebServer )
		( exploitedBy-OneImmutableReference_From_Machine_To_OS nas osNas )
		( exploitedBy-OneImmutableReference_From_Machine_To_OS pc1Bureau osPc1Bureau )
		( exploitedBy-OneImmutableReference_From_Machine_To_OS pc2Openspace osPc2Openspace )
		( runsOn-OneImmutableReference_From_OS_To_Machine osWebServer webServer )
		( runsOn-OneImmutableReference_From_OS_To_Machine osNas nas )
		( runsOn-OneImmutableReference_From_OS_To_Machine osPc1Bureau pc1Bureau )
		( runsOn-OneImmutableReference_From_OS_To_Machine osPc2Openspace pc2Openspace )
		( accounts-ZeroOrManyImmutableReference_From_OS_To_Account osWebServer rootAccountWebServer )
		( accounts-ZeroOrManyImmutableReference_From_OS_To_Account osPc1Bureau userAccountPc1 )
		( accounts-ZeroOrManyImmutableReference_From_OS_To_Account osPc2Openspace userAccountPc2 )
		( validFor-ZeroOrOneImmutableReference_From_Account_To_OS rootAccountWebServer osWebServer )
		( validFor-ZeroOrOneImmutableReference_From_Account_To_OS userAccountPc1 osPc1Bureau )
		( validFor-ZeroOrOneImmutableReference_From_Account_To_OS userAccountPc2 osPc2Openspace )
		( credential-ZeroOrOneImmutableReference_From_Account_To_Credential rootAccountWebServer credentialRootAccountWebServer )
		( credential-ZeroOrOneImmutableReference_From_Account_To_Credential userAccountPc1 credentialUserAccountPc1 )
		( credential-ZeroOrOneImmutableReference_From_Account_To_Credential userAccountPc2 credentialUserAccountPc2 )
		( machines-OneOrManyImmutableReference_From_Network_To_Machine agenceLocalNetwork webServer )
		( machines-OneOrManyImmutableReference_From_Network_To_Machine agenceLocalNetwork nas )
		( machines-OneOrManyImmutableReference_From_Network_To_Machine agenceLocalNetwork pc1Bureau )
		( machines-OneOrManyImmutableReference_From_Network_To_Machine agenceLocalNetwork pc2Openspace )
		( connectedTo-ZeroOrManyImmutableReference_From_Machine_To_Network webServer agenceLocalNetwork )
		( connectedTo-ZeroOrManyImmutableReference_From_Machine_To_Network nas agenceLocalNetwork )
		( connectedTo-ZeroOrManyImmutableReference_From_Machine_To_Network pc1Bureau agenceLocalNetwork )
		( connectedTo-ZeroOrManyImmutableReference_From_Machine_To_Network pc2Openspace agenceLocalNetwork )
		( shares-ZeroOrManyImmutableReference_From_Machine_To_FileSharing nas commonFileSharing )
		( sharedBy-OneImmutableReference_From_FileSharing_To_Machine commonFileSharing nas )
		( dataShared-ZeroOrManyImmutableReference_From_FileSharing_To_Data commonFileSharing dataNAS )
		( authorizedAccount-ZeroOrManyImmutableReference_From_FileSharing_To_Account commonFileSharing commonFileSharingAccount )
	)

)