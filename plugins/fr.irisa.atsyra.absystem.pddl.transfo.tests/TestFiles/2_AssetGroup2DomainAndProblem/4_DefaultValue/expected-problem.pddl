( define

	( problem agenceModel )

	( :domain definitionGroup )

	( :objects
		SENSIBLE STANDARD - EDataSensibility
		WINDOWS LINUX MACOS - EPlateform
		NONE PASSWORD MFA TOKEN BIOMETRIC - EAuthenticationType
		USER ADMIN ROOT - EPermission
		DEFAULT LOCAL DOMAIN CLOUD - EAccountType
		documentSecret - Item
		credentialUserAccountPc1 credentialUserAccountPc2 - Credential
		robinHood - Attacker
		exterieur bureau - Zone
		barriereParking - FreeAccess
		alarmPatio alarmCouloir - Alarm
		porteAccueilPrincipale - BadgeDoor
		fenetreAccueilParking - Windows
		porteAccueil - Door
	)

	( :init
		( EDataSensibility-SENSIBLE SENSIBLE )
		( EDataSensibility-STANDARD STANDARD )
		( EPlateform-WINDOWS WINDOWS )
		( EPlateform-LINUX LINUX )
		( EPlateform-MACOS MACOS )
		( EAuthenticationType-NONE NONE )
		( EAuthenticationType-PASSWORD PASSWORD )
		( EAuthenticationType-MFA MFA )
		( EAuthenticationType-TOKEN TOKEN )
		( EAuthenticationType-BIOMETRIC BIOMETRIC )
		( EPermission-USER USER )
		( EPermission-ADMIN ADMIN )
		( EPermission-ROOT ROOT )
		( EAccountType-DEFAULT DEFAULT )
		( EAccountType-LOCAL LOCAL )
		( EAccountType-DOMAIN DOMAIN )
		( EAccountType-CLOUD CLOUD )
		( location-ZeroOrOneMutableReference_From_Attacker_To_Zone robinHood exterieur )
		( itemsInZone-ZeroOrManyMutableReference_From_Zone_To_Item bureau documentSecret )
	)

	( :goal
		( and
			( ownedItems-ZeroOrManyMutableReference_From_Attacker_To_Item robinHood documentSecret )
			( location-ZeroOrOneMutableReference_From_Attacker_To_Zone robinHood exterieur )
		)
	)

)