( define

	( problem agenceModel )

	( :domain definitionGroup )

	( :objects
		SENSIBLE STANDARD - EDataSensibility
		WINDOWS LINUX MACOS - EPlateform
		NONE PASSWORD MFA TOKEN BIOMETRIC - EAuthenticationType
		USER ADMIN ROOT - EPermission
		DEFAULT LOCAL DOMAIN CLOUD - EAccountType
		exterieur - Zone
	)

	( :init
		( EDataSensibility-SENSIBLE SENSIBLE )
		( EDataSensibility-STANDARD STANDARD )
		( EPlateform-WINDOWS WINDOWS )
		( EPlateform-LINUX LINUX )
		( EPlateform-MACOS MACOS )
		( EAuthenticationType-NONE NONE )
		( EAuthenticationType-PASSWORD PASSWORD )
		( EAuthenticationType-MFA MFA )
		( EAuthenticationType-TOKEN TOKEN )
		( EAuthenticationType-BIOMETRIC BIOMETRIC )
		( EPermission-USER USER )
		( EPermission-ADMIN ADMIN )
		( EPermission-ROOT ROOT )
		( EAccountType-DEFAULT DEFAULT )
		( EAccountType-LOCAL LOCAL )
		( EAccountType-DOMAIN DOMAIN )
		( EAccountType-CLOUD CLOUD )
	)

)