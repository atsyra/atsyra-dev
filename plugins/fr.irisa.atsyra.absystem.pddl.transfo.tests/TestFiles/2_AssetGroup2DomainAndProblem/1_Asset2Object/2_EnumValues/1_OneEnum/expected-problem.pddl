( define

	( problem agenceModel )

	( :domain definitionGroup )

	( :objects
		WINDOWS LINUX MACOS - EPlateform
		exterieur - Zone
	)

	( :init
		( EPlateform-WINDOWS WINDOWS )
		( EPlateform-LINUX LINUX )
		( EPlateform-MACOS MACOS )
	)

)