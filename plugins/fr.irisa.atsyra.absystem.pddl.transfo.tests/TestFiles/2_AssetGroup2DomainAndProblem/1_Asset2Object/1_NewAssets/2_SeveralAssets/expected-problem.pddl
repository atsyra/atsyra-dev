( define

	( problem agenceModel )

	( :domain definitionGroup )

	( :objects
		rootAccountWebServer userAccountPc1 userAccountPc2 commonFileSharingAccount - Account
		credentialRootAccountWebServer credentialUserAccountPc1 credentialUserAccountPc2 - Credential
		robinHood - Attacker
		webServer nas - Server
		osWebServer osNas osPc1Bureau osPc2Openspace - OS
		exterieur patio reunion bureau couloir cageEscalier openspace accueil parking localTechnique - Zone
		commonFileSharing - FileSharing
		porteLocalTechniqueCouloir porteAccueilPrincipale - BadgeDoor
		dataWebServer dataNAS dataPc1Bureau dataPc2Openspace - Data
		fenetreReunion fenetreBureau fenetreCouloir fenetreOpenspace fenetreAccueilParking fenetreAccueilExterieur - Windows
		portePatioCouloir portePatioExterieur porteReunion porteBureau porteCageEscalier porteOpenspace porteAccueil - Door
		pc1Bureau pc2Openspace - Workstation
		badgeAccueil documentSecret badgeLocalTechnique cleBureau - Item
		hddWebServer hddNas hddPc1Bureau hddPc2Openspace - Storage
		escalier barriereParking - FreeAccess
		alarmPatio alarmCouloir alarmAccueil alarmParking1 alarmParking2 - Alarm
		agenceLocalNetwork - Network
	)

)