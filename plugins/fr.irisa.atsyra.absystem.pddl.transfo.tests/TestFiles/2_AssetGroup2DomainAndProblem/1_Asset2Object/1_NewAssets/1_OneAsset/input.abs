AssetBasedSystem
with definitionGroup::*;
DefinitionGroup definitionGroup {
	PrimitiveDataType String
	PrimitiveDataType Boolean
	PrimitiveDataType Integer
	PrimitiveDataType Version

	StaticMethod contains
	StaticMethod containsAll
	StaticMethod containsAny
	StaticMethod filter
	StaticMethod isEmpty

	Tag Physical
	Tag Logical
	Tag InformationSystem
	Tag Building
	Tag Human

	AssetType Zone {
		level "1"
		tags (Building)
		// reference nestedInformation : AbstractInformation [*]
		reference alarms : Alarm [*]
		reference hasMachines : Machine[*]
	}

	AssetType Access {
		level "2"
		tags (Building)
		reference inside : Zone
		reference outside : Zone
		reference alarms : Alarm [*]
	}
	AssetType FreeAccess extends (Access) {
		level "2"
		tags (Building)
	}
	AssetType CloseableAccess extends (Access) {
		level "2"
		tags (Building)
	}

	AssetType Door  extends (CloseableAccess) {
		level "2"
		tags (Building)
		reference keys : Item[*]
	}
	AssetType BadgeDoor extends (CloseableAccess) {
		level "2"
		tags (Building)
		reference badges : Item[*]
	}
	AssetType Windows  extends (CloseableAccess) {
		level "2"
		tags (Building)

	}
	AssetType Alarm {
		level "3"
		tags (Building)
		reference control : Zone
	}

	AssetType Item {
		level "3"
	}

	AssetType Attacker {
		tags (Human)
	// reference testRef : Information [*]
	}
	AssetType Machine {
		attribute name : String
		attribute cableLocked : Boolean default false
		reference storages : Storage[*]
		reference location : Zone[1]
		reference exploitedBy : OS[1]
		reference connectedTo : Network[*]
		reference shares : FileSharing[*]
	}
	AssetType Workstation extends (Machine) {

	}
	AssetType Server extends (Machine) {

	}
	AssetType Storage {
		attribute encrypted : Boolean default false
		reference hostedBy : Machine[1]
		reference encryptedWith : Credential[0..1]
		reference data : Data[*]
	}
	AssetType Data {
		attribute encrypted : Boolean default false
		reference encryptedWith : Credential[0..1]
		reference storedOn : Storage[1]
	}
	AssetType OS {
		attribute version : Version
		reference runsOn : Machine[1]
		reference accounts : Account[*]
	}
	AssetType Account {
		reference credential : Credential[0..1]
		reference validFor : OS[0..1]
	}
	AssetType Credential {
		reference worksWith : Account[*]
	}
	AssetType Network {
		reference machines : Machine[1..*]
	}
	AssetType FileSharing {
		reference sharedBy : Machine[1]
		reference dataShared : Data[*]
		reference authorizedAccount : Account[*]
	}
}
DefinitionGroup OtherOne {
	AssetTypeAspect Attacker {
		// @runtime
		reference location : Zone
		reference ownedItems : Item [*] default []
		reference knownCredentials : Credential[*] default []
		reference knownSharedFolders : FileSharing[*] default []
		reference knownMachines : Machine[*] default []
		reference stolenData : Data[*] default []
		reference stolenStorage : Storage[*] default []
		reference stolenMachine : Machine[*] default []
		reference compromizedMachine : Machine[*] default []
	}

	AssetTypeAspect CloseableAccess {
		// @runtime
		attribute isOpen : Boolean default false
	}
	AssetTypeAspect Door {
		// @runtime
		attribute isLocked : Boolean
	}
	AssetTypeAspect BadgeDoor {
		// @runtime
		attribute isLocked : Boolean
	}
	AssetTypeAspect Alarm {
		// @runtime
		attribute isActive : Boolean
		attribute isTriggered : Boolean default false
	}
	AssetTypeAspect Zone {
		reference itemsInZone : Item [*] default []
	}
}

AssetGroup agenceModel {

	Asset exterieur : Zone {
	}

}