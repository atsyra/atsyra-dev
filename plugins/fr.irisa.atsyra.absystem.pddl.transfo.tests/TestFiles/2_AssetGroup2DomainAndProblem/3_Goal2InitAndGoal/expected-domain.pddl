( define

	( domain definitionGroup )

	( :types
		Door BadgeDoor Windows - CloseableAccess
		Workstation Server - Machine
		FreeAccess CloseableAccess - Access
		EDataSensibility
		EPlateform
		EAuthenticationType
		EPermission
		EAccountType
		Zone
		Access
		Alarm
		Item
		Attacker
		Machine
		Storage
		Data
		OS
		Account
		Credential
		Network
		FileSharing
	)

	( :predicates
		( EDataSensibility-SENSIBLE ?value - EDataSensibility )
		( EDataSensibility-STANDARD ?value - EDataSensibility )
		( EPlateform-WINDOWS ?value - EPlateform )
		( EPlateform-LINUX ?value - EPlateform )
		( EPlateform-MACOS ?value - EPlateform )
		( EAuthenticationType-NONE ?value - EAuthenticationType )
		( EAuthenticationType-PASSWORD ?value - EAuthenticationType )
		( EAuthenticationType-MFA ?value - EAuthenticationType )
		( EAuthenticationType-TOKEN ?value - EAuthenticationType )
		( EAuthenticationType-BIOMETRIC ?value - EAuthenticationType )
		( EPermission-USER ?value - EPermission )
		( EPermission-ADMIN ?value - EPermission )
		( EPermission-ROOT ?value - EPermission )
		( EAccountType-DEFAULT ?value - EAccountType )
		( EAccountType-LOCAL ?value - EAccountType )
		( EAccountType-DOMAIN ?value - EAccountType )
		( EAccountType-CLOUD ?value - EAccountType )
		( alarms-ZeroOrManyImmutableReference_From_Zone_To_Alarm ?z - Zone ?a - Alarm )
		( hasMachines-ZeroOrManyImmutableReference_From_Zone_To_Machine ?z - Zone ?m - Machine )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone ?a - Access ?z - Zone )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone ?a - Access ?z - Zone )
		( alarms-ZeroOrManyImmutableReference_From_Access_To_Alarm ?a - Access ?a1 - Alarm )
		( keys-ZeroOrManyImmutableReference_From_Door_To_Item ?d - Door ?i - Item )
		( badges-ZeroOrManyImmutableReference_From_BadgeDoor_To_Item ?b - BadgeDoor ?i - Item )
		( control-ZeroOrOneImmutableReference_From_Alarm_To_Zone ?a - Alarm ?z - Zone )
		( storages-ZeroOrManyImmutableReference_From_Machine_To_Storage ?m - Machine ?s - Storage )
		( location-OneImmutableReference_From_Machine_To_Zone ?m - Machine ?z - Zone )
		( exploitedBy-OneImmutableReference_From_Machine_To_OS ?m - Machine ?o - OS )
		( connectedTo-ZeroOrManyImmutableReference_From_Machine_To_Network ?m - Machine ?n - Network )
		( shares-ZeroOrManyImmutableReference_From_Machine_To_FileSharing ?m - Machine ?f - FileSharing )
		( cableLocked-ZeroOrOneImmutableAttribute_From_Machine_To_Boolean ?m - Machine )
		( hostedBy-OneImmutableReference_From_Storage_To_Machine ?s - Storage ?m - Machine )
		( encryptedWith-ZeroOrOneImmutableReference_From_Storage_To_Credential ?s - Storage ?c - Credential )
		( data-ZeroOrManyImmutableReference_From_Storage_To_Data ?s - Storage ?d - Data )
		( encrypted-ZeroOrOneImmutableAttribute_From_Storage_To_Boolean ?s - Storage )
		( encryptedWith-ZeroOrOneImmutableReference_From_Data_To_Credential ?d - Data ?c - Credential )
		( storedOn-OneImmutableReference_From_Data_To_Storage ?d - Data ?s - Storage )
		( type-ZeroOrOneImmutableAttribute_From_Data_To_EDataSensibility ?d - Data ?value - EDataSensibility )
		( encrypted-ZeroOrOneImmutableAttribute_From_Data_To_Boolean ?d - Data )
		( runsOn-OneImmutableReference_From_OS_To_Machine ?o - OS ?m - Machine )
		( accounts-ZeroOrManyImmutableReference_From_OS_To_Account ?o - OS ?a - Account )
		( plateform-ZeroOrOneImmutableAttribute_From_OS_To_EPlateform ?o - OS ?value - EPlateform )
		( credential-ZeroOrOneImmutableReference_From_Account_To_Credential ?a - Account ?c - Credential )
		( validFor-ZeroOrOneImmutableReference_From_Account_To_OS ?a - Account ?o - OS )
		( accountType-ZeroOrOneImmutableAttribute_From_Account_To_EAccountType ?a - Account ?value - EAccountType )
		( authenticationType-ZeroOrOneImmutableAttribute_From_Account_To_EAuthenticationType ?a - Account ?value - EAuthenticationType )
		( permission-ZeroOrOneImmutableAttribute_From_Account_To_EPermission ?a - Account ?value - EPermission )
		( worksWith-ZeroOrManyImmutableReference_From_Credential_To_Account ?c - Credential ?a - Account )
		( machines-OneOrManyImmutableReference_From_Network_To_Machine ?n - Network ?m - Machine )
		( sharedBy-OneImmutableReference_From_FileSharing_To_Machine ?f - FileSharing ?m - Machine )
		( dataShared-ZeroOrManyImmutableReference_From_FileSharing_To_Data ?f - FileSharing ?d - Data )
		( authorizedAccount-ZeroOrManyImmutableReference_From_FileSharing_To_Account ?f - FileSharing ?a - Account )
		( location-ZeroOrOneMutableReference_From_Attacker_To_Zone ?a - Attacker ?z - Zone )
		( ownedItems-ZeroOrManyMutableReference_From_Attacker_To_Item ?a - Attacker ?i - Item )
		( knownCredentials-ZeroOrManyMutableReference_From_Attacker_To_Credential ?a - Attacker ?c - Credential )
		( knownSharedFolders-ZeroOrManyMutableReference_From_Attacker_To_FileSharing ?a - Attacker ?f - FileSharing )
		( knownMachines-ZeroOrManyMutableReference_From_Attacker_To_Machine ?a - Attacker ?m - Machine )
		( stolenData-ZeroOrManyMutableReference_From_Attacker_To_Data ?a - Attacker ?d - Data )
		( stolenStorage-ZeroOrManyMutableReference_From_Attacker_To_Storage ?a - Attacker ?s - Storage )
		( stolenMachine-ZeroOrManyMutableReference_From_Attacker_To_Machine ?a - Attacker ?m - Machine )
		( compromizedMachine-ZeroOrManyMutableReference_From_Attacker_To_Machine ?a - Attacker ?m - Machine )
		( isOpen-ZeroOrOneMutableAttribute_From_CloseableAccess_To_Boolean ?c - CloseableAccess )
		( isLocked-ZeroOrOneMutableAttribute_From_Door_To_Boolean ?d - Door )
		( isLocked-ZeroOrOneMutableAttribute_From_BadgeDoor_To_Boolean ?b - BadgeDoor )
		( isActive-ZeroOrOneMutableAttribute_From_Alarm_To_Boolean ?a - Alarm )
		( isTriggered-ZeroOrOneMutableAttribute_From_Alarm_To_Boolean ?a - Alarm )
		( itemsInZone-ZeroOrManyMutableReference_From_Zone_To_Item ?z - Zone ?i - Item )
	)

)