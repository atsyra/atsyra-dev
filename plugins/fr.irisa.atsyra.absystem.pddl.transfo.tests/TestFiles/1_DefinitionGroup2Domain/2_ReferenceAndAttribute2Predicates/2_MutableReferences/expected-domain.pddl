( define

	( domain OtherOne )

	( :types
		Workstation Server - Machine
		FreeAccess CloseableAccess - Access
		Door BadgeDoor Windows - CloseableAccess
		Zone
		Access
		Alarm
		Item
		Attacker
		Machine
		Storage
		Data
		OS
		Account
		Credential
		Network
		FileSharing
	)

	( :predicates
		( alarms-ZeroOrManyImmutableReference_From_Zone_To_Alarm ?z - Zone ?a - Alarm )
		( hasMachines-ZeroOrManyImmutableReference_From_Zone_To_Machine ?z - Zone ?m - Machine )
		( inside-ZeroOrOneImmutableReference_From_Access_To_Zone ?a - Access ?z - Zone )
		( outside-ZeroOrOneImmutableReference_From_Access_To_Zone ?a - Access ?z - Zone )
		( alarms-ZeroOrManyImmutableReference_From_Access_To_Alarm ?a - Access ?a1 - Alarm )
		( keys-ZeroOrManyImmutableReference_From_Door_To_Item ?d - Door ?i - Item )
		( badges-ZeroOrManyImmutableReference_From_BadgeDoor_To_Item ?b - BadgeDoor ?i - Item )
		( control-ZeroOrOneImmutableReference_From_Alarm_To_Zone ?a - Alarm ?z - Zone )
		( storages-ZeroOrManyImmutableReference_From_Machine_To_Storage ?m - Machine ?s - Storage )
		( location-OneImmutableReference_From_Machine_To_Zone ?m - Machine ?z - Zone )
		( exploitedBy-OneImmutableReference_From_Machine_To_OS ?m - Machine ?o - OS )
		( connectedTo-ZeroOrManyImmutableReference_From_Machine_To_Network ?m - Machine ?n - Network )
		( shares-ZeroOrManyImmutableReference_From_Machine_To_FileSharing ?m - Machine ?f - FileSharing )
		( hostedBy-OneImmutableReference_From_Storage_To_Machine ?s - Storage ?m - Machine )
		( encryptedWith-ZeroOrOneImmutableReference_From_Storage_To_Credential ?s - Storage ?c - Credential )
		( data-ZeroOrManyImmutableReference_From_Storage_To_Data ?s - Storage ?d - Data )
		( encryptedWith-ZeroOrOneImmutableReference_From_Data_To_Credential ?d - Data ?c - Credential )
		( storedOn-OneImmutableReference_From_Data_To_Storage ?d - Data ?s - Storage )
		( runsOn-OneImmutableReference_From_OS_To_Machine ?o - OS ?m - Machine )
		( accounts-ZeroOrManyImmutableReference_From_OS_To_Account ?o - OS ?a - Account )
		( credential-ZeroOrOneImmutableReference_From_Account_To_Credential ?a - Account ?c - Credential )
		( validFor-ZeroOrOneImmutableReference_From_Account_To_OS ?a - Account ?o - OS )
		( worksWith-ZeroOrManyImmutableReference_From_Credential_To_Account ?c - Credential ?a - Account )
		( machines-OneOrManyImmutableReference_From_Network_To_Machine ?n - Network ?m - Machine )
		( sharedBy-OneImmutableReference_From_FileSharing_To_Machine ?f - FileSharing ?m - Machine )
		( dataShared-ZeroOrManyImmutableReference_From_FileSharing_To_Data ?f - FileSharing ?d - Data )
		( authorizedAccount-ZeroOrManyImmutableReference_From_FileSharing_To_Account ?f - FileSharing ?a - Account )
		( location-ZeroOrOneMutableReference_From_Attacker_To_Zone ?a - Attacker ?z - Zone )
		( ownedItems-ZeroOrManyMutableReference_From_Attacker_To_Item ?a - Attacker ?i - Item )
		( knownCredentials-ZeroOrManyMutableReference_From_Attacker_To_Credential ?a - Attacker ?c - Credential )
		( knownSharedFolders-ZeroOrManyMutableReference_From_Attacker_To_FileSharing ?a - Attacker ?f - FileSharing )
		( knownMachines-ZeroOrManyMutableReference_From_Attacker_To_Machine ?a - Attacker ?m - Machine )
		( stolenData-ZeroOrManyMutableReference_From_Attacker_To_Data ?a - Attacker ?d - Data )
		( stolenStorage-ZeroOrManyMutableReference_From_Attacker_To_Storage ?a - Attacker ?s - Storage )
		( stolenMachine-ZeroOrManyMutableReference_From_Attacker_To_Machine ?a - Attacker ?m - Machine )
		( compromizedMachine-ZeroOrManyMutableReference_From_Attacker_To_Machine ?a - Attacker ?m - Machine )
		( itemsInZone-ZeroOrManyMutableReference_From_Zone_To_Item ?z - Zone ?i - Item )
	)

)