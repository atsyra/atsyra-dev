( define

	( domain definitionGroup )

	( :types
		FreeAccess CloseableAccess - Access
		Door BadgeDoor Windows - CloseableAccess
		Workstation Server - Machine
		Zone
		Access
		Alarm
		Item
		Attacker
		Machine
		Storage
		Data
		OS
		Account
		Credential
		Network
		FileSharing
	)

)