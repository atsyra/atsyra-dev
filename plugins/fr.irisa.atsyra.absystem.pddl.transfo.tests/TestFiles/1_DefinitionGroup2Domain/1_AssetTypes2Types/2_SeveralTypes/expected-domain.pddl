( define

	( domain definitionGroup )

	( :types
		Zone
		Access
		FreeAccess
		CloseableAccess
		Door
		BadgeDoor
		Windows
		Alarm
		Item
		Attacker
		Machine
		Workstation
		Server
		Storage
		Data
		OS
		Account
		Credential
		Network
		FileSharing
	)

)
