package fr.irisa.atsyra.absystem.pddl.transfo.tests

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.eclipse.xtext.serializer.ISerializer
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import org.eclipse.xtext.testing.extensions.InjectionExtension
import fr.irisa.atsyra.absystem.gal.transfo.tests.AssetBasedSystemDslInjectorProvider
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.InjectWith
import fr.irisa.atsyra.pddl.xtext.PDDLRuntimeModule
import com.google.inject.Guice
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.junit.jupiter.api.BeforeEach
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage
import org.junit.jupiter.api.BeforeAll
import org.eclipse.xtext.resource.SaveOptions
import fr.irisa.atsyra.pddl.xtext.PDDLStandaloneSetup
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.EcoreUtil

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABS2PDDL_SmallAgence_TransfoTest {
	
	@Inject
	ParseHelper<AssetBasedSystem> parseHelper
	
	@BeforeEach
	def void setup() {
		AbsystemPackage.eINSTANCE.eClass()
		PDDLPackage.eINSTANCE.eClass()
	}
	
	@Test
	def void testTypes() {
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem
			with definitionGroup::*;
			with agenceModel::*;
			DefinitionGroup definitionGroup {
				primitiveDataTypes {
					PrimitiveDataType String
					PrimitiveDataType Boolean
					PrimitiveDataType Integer
					PrimitiveDataType Version
				}
				staticMethods {
					StaticMethod contains
					StaticMethod containsAll
					StaticMethod containsAny
					StaticMethod isEmpty
				}
				tags {
					Tag Physical
					Tag Logical
					Tag InformationSystem
					Tag Building
					Tag Human
				}
				assetTypes {
					AssetType Zone {
						level "1"
						tags (Building)
						// reference nestedInformation : AbstractInformation [*]
						reference alarms : Alarm [*]
						reference hasMachines : Machine[*]
					}
			
					AssetType Access {
						level "2"
						tags (Building)
						reference inside : Zone
						reference outside : Zone
						reference alarms : Alarm [*]
					}
					AssetType FreeAccess extends (Access) {
						level "2"
						tags (Building)
					}
					AssetType CloseableAccess extends (Access) {
						level "2"
						tags (Building)
					}
			
					AssetType Door  extends (CloseableAccess) {
						level "2"
						tags (Building)
						reference keys : Item[*]
					}
					AssetType BadgeDoor extends (CloseableAccess) {
						level "2"
						tags (Building)
						reference badges : Item[*]
					}
					AssetType Windows  extends (CloseableAccess) {
						level "2"
						tags (Building)
			
					}
					AssetType Alarm {
						level "3"
						tags (Building)
						reference control : Zone
					}
			
					AssetType Item {
						level "3"
					}
			
					AssetType Attacker {
						tags (Human)
					// reference testRef : Information [*]
					}
					AssetType Machine {
						attribute name : String
						attribute cableLocked : Boolean default false
						reference storages : Storage[*]
						reference location : Zone[1]
						reference exploitedBy : OS[1]
						reference connectedTo : Network[*]
						reference shares : FileSharing[*]
					}
					AssetType Workstation extends (Machine) {
			
					}
					AssetType Server extends (Machine) {
			
					}
					AssetType Storage {
						attribute encrypted : Boolean default false
						reference hostedBy : Machine[1]
						reference data : Data[*]
					}
					AssetType Data {
						attribute encrypted : Boolean default false
						reference storedOn : Storage[1]
					}
					AssetType OS {
						attribute version : Version
						reference runsOn : Machine[1]
					}
					AssetType Network {
						reference machines : Machine[1..*]
					}
					AssetType FileSharing {
						reference sharedBy : Machine[1]
						reference dataShared : Data[*]
					}
					AssetTypeAspect Attacker {
						// @runtime
						reference location : Zone
						reference ownedItems : Item [*] default []
						reference knownSharedFolders : FileSharing[*] default []
						reference knownMachines : Machine[*] default []
						reference stolenData : Data[*] default []
						reference stolenStorage : Storage[*] default []
						reference stolenMachine : Machine[*] default []
						reference compromizedMachine : Machine[*] default []
					}
			
					AssetTypeAspect CloseableAccess {
						// @runtime
						attribute isOpen : Boolean default false
					}
					AssetTypeAspect Door {
						// @runtime
						attribute isLocked : Boolean default true
					}
					AssetTypeAspect BadgeDoor {
						// @runtime
						attribute isLocked : Boolean default true
					}
					AssetTypeAspect Alarm {
						// @runtime
						attribute isActive : Boolean default true
						attribute isTriggered : Boolean default false
					}
					AssetTypeAspect Zone {
						reference itemsInZone : Item [*] default []
					}
				}
				guardedActions {
					GuardedAction goes_from_to_via_freeaccess( anAttacker : Attacker, startZone : Zone, destZone : Zone, usedAccess : FreeAccess ) {
						guard = anAttacker.location == startZone &&
							(( usedAccess.inside == startZone &&  usedAccess.outside == destZone) || 
						     ( usedAccess.outside == startZone &&  usedAccess.inside == destZone)
							)  && 
							( startZone != destZone)
						action {
							anAttacker.location.assign(destZone);
						}
					}
			
					GuardedAction goes_from_to_via_closeableaccess(anAttacker : Attacker, startZone : Zone, destZone : Zone, access : CloseableAccess ) {
						guard = anAttacker.location == startZone &&
							((access.inside == startZone && access.outside == destZone) || (access.outside == startZone &&  access.inside == destZone)) && 
							(startZone != destZone) &&
							access.isOpen == true
						action {
							anAttacker.location.assign(destZone);
						}
					}
			
					GuardedAction open_door( anAttacker : Attacker, usedDoor : Door ) {
						guard = (anAttacker.location == usedDoor.inside || anAttacker.location == usedDoor.outside) &&
							usedDoor.isOpen == false && usedDoor.isLocked == false
						action {
							usedDoor.isOpen.assign(true);
						}
					}
					GuardedAction close_door( anAttacker : Attacker, usedDoor : Door ) {
						guard = (anAttacker.location == usedDoor.inside || anAttacker.location == usedDoor.outside) &&
							usedDoor.isOpen == true && usedDoor.isLocked == false
						action {
							usedDoor.isOpen.assign(false);
						}
					}
					GuardedAction open_badgedoor( anAttacker : Attacker, usedDoor : BadgeDoor ) {
						guard = (anAttacker.location == usedDoor.inside || (anAttacker.location == usedDoor.outside && usedDoor.isLocked == false)) &&
							usedDoor.isOpen == false
						action {
							usedDoor.isOpen.assign(true);
						}
					}
					GuardedAction close_badgedoor( anAttacker : Attacker, usedDoor : BadgeDoor ) {
						guard = (anAttacker.location == usedDoor.inside || anAttacker.location == usedDoor.outside) &&
							usedDoor.isOpen == true
						action {
							usedDoor.isOpen.assign(false);
						}
					}
					GuardedAction open_window(anAttacker: Attacker, window : Windows) {
						guard = anAttacker.location == window.inside && window.isOpen == false
						action {
							window.isOpen.assign(true);
						}
					}
					GuardedAction close_window(anAttacker: Attacker, window : Windows) {
						guard = (anAttacker.location == window.inside || anAttacker.location == window.outside) && window.isOpen == true
						action {
							window.isOpen.assign(false);
						}
					}
					GuardedAction unlockDoor( anAttacker : Attacker, usedDoor : Door ) {
						guard = (anAttacker.location == usedDoor.inside || anAttacker.location == usedDoor.outside) &&
							anAttacker.ownedItems.containsAny(usedDoor.keys) &&
							usedDoor.isLocked == true
						action {
							usedDoor.isLocked.assign(false);
						}
					}
					GuardedAction unlockBadgeDoor( anAttacker : Attacker, usedDoor : BadgeDoor ) {
						guard = anAttacker.location == usedDoor.outside &&
							anAttacker.ownedItems.containsAny(usedDoor.badges) && 
							usedDoor.isLocked == true
						action {
							usedDoor.isLocked.assign(false);
						}
					}
					GuardedAction pickItemInZone(anAttacker : Attacker, currentZone : Zone, item : Item) {
						guard = anAttacker.location == currentZone && 
			                !anAttacker.ownedItems.contains(item) && 
			                currentZone.itemsInZone.contains (item)
						action {
							anAttacker.ownedItems.add(item);
							currentZone.itemsInZone.remove(item);
						}
					}
					GuardedAction dropItemInZone(anAttacker : Attacker, currentZone : Zone, item : Item) {
						guard = anAttacker.location == currentZone && 
			                anAttacker.ownedItems.contains(item) && 
			                !currentZone.itemsInZone.contains(item)
						action {
							anAttacker.ownedItems.remove(item);
							currentZone.itemsInZone.add(item);
						}
					}
				}
			}
			AssetGroup GoalGroup {
				goals {
					Goal goal1 {
						description "attacker should steal DocumentSecret via Acceuil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur
					}
					Goal goal2 {
						description "attacker should steal DocumentSecret via Patio OR Accueil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur
					}
					Goal goal3 {
						description "attacker should steal DocumentSecret via Patio and Reunion"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							portePatioExterieur.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							porteOpenspace.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur
					}
					Goal goal4 {
						description "attacker should steal DocumentSecret via Patio OR Patio and Reunion OR Accueil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur
					}
					Goal goal1NoAlarms {
						description "attacker would steal DocumentSecret via Acceuil, not triggering any alarms (not possible)"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur && 
							alarmPatio.isTriggered == false && 
							alarmCouloir.isTriggered == false && 
							alarmAccueil.isTriggered == false && 
							alarmParking1.isTriggered == false && 
							alarmParking2.isTriggered == false
					}
					Goal goal5 {
						description "attacker should enter via Patio, steal DocumentSecret, and leave either via Patio OR Accueil after disabling alarmAccueil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							alarmPatio.isActive == false && 
							alarmCouloir.isActive == false && 
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur && 
							alarmPatio.isTriggered == false && 
							alarmCouloir.isTriggered == false && 
							alarmAccueil.isTriggered == false && 
							alarmParking1.isTriggered == false && 
							alarmParking2.isTriggered == false
					}
					Goal goal6 {
						description "attacker should enter via Accueil, disable alarmCouloir, steal DocumentSecret, and leave either via Patio OR Accueil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							alarmAccueil.isActive == false && 
							alarmParking1.isActive == false && 
							alarmParking2.isActive == false && 
							alarmPatio.isActive == false && 
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur && 
							alarmPatio.isTriggered == false && 
							alarmCouloir.isTriggered == false && 
							alarmAccueil.isTriggered == false && 
							alarmParking1.isTriggered == false && 
							alarmParking2.isTriggered == false
					}
					Goal goal7 {
						description "attacker should enter via Accueil, disable alarmCouloir, steal DocumentSecret, and leave either via Patio OR Accueil, but also all doors & windows must be closed"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							alarmAccueil.isActive == false && 
							alarmParking1.isActive == false && 
							alarmParking2.isActive == false && 
							alarmPatio.isActive == false && 
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur && 
							alarmPatio.isTriggered == false && 
							alarmCouloir.isTriggered == false && 
							alarmAccueil.isTriggered == false && 
							alarmParking1.isTriggered == false && 
							alarmParking2.isTriggered == false && 
							porteAccueil.isOpen == false && 
							porteAccueilPrincipale.isOpen == false && 
							porteBureau.isOpen == false && 
							porteCageEscalier.isOpen == false && 
							porteOpenspace.isOpen == false && 
							portePatioCouloir.isOpen == false && 
							portePatioExterieur.isOpen == false && 
							porteReunion.isOpen == false && 
							fenetreAccueilExterieur.isOpen == false && 
							fenetreAccueilParking.isOpen == false && 
							fenetreBureau.isOpen == false && 
							fenetreCouloir.isOpen == false && 
							fenetreOpenspace.isOpen == false && 
							fenetreReunion.isOpen == false
					}
				}
			}
			
			AssetGroup agenceModel {
			
				Asset exterieur : Zone {
				}
				Asset patio : Zone {
				}
				Asset reunion : Zone {
				}
				Asset bureau : Zone {
				}
				Asset couloir : Zone {
				}
				Asset cageEscalier : Zone {
				}
				Asset openspace : Zone {
				}
				Asset accueil : Zone {
				}
				Asset parking : Zone {
				}
				Asset localTechnique : Zone {
				}
			
				Asset porteLocalTechniqueCouloir : BadgeDoor {
				}
				Asset portePatioCouloir : Door {
				}
				Asset portePatioExterieur : Door {
				}
				Asset fenetreReunion : Windows {
				}
				Asset fenetreBureau : Windows {
				}
			
				Asset porteReunion : Door {
				}
				Asset porteBureau : Door {
				}
			
				Asset escalier : FreeAccess {
				}
				Asset porteCageEscalier : Door {
				}
			
				Asset fenetreCouloir : Windows {
				}
			
				Asset porteOpenspace : Door {
				}
				Asset fenetreOpenspace : Windows {
				}
			
				Asset porteAccueil : Door {
				}
				Asset fenetreAccueilParking : Windows {
				}
				Asset fenetreAccueilExterieur : Windows {
				}
				Asset porteAccueilPrincipale : BadgeDoor {
				}
				Asset barriereParking : FreeAccess {
				}
			
				Asset robinHood : Attacker {
				}
			
				Asset alarmPatio : Alarm {
				}
				Asset alarmCouloir : Alarm {
				}
				Asset alarmAccueil : Alarm {
				}
				Asset alarmParking1 : Alarm {
				}
				Asset alarmParking2 : Alarm {
				}
			
				Asset badgeAccueil : Item {
				}
				Asset documentSecret : Item {
				}
				Asset badgeLocalTechnique : Item {
				}
			
				Asset cleBureau : Item {
				}
			
				link portePatioCouloir to patio as Access::inside
				link portePatioCouloir to couloir as Access::outside
				link portePatioExterieur to patio as Access::inside
				link portePatioExterieur to exterieur as Access::outside
			
				link fenetreBureau to patio as Access::outside
				link fenetreBureau to bureau as Access::inside
			
				link fenetreReunion to patio as Access::outside
				link fenetreReunion to reunion as Access::inside
			
				link porteBureau to bureau as Access::inside
				link porteBureau to couloir as Access::outside
			
				link porteReunion to reunion as Access::inside
				link porteReunion to couloir as Access::outside
			
				link escalier to exterieur as Access::outside
				link escalier to cageEscalier as Access::inside
			
				link porteCageEscalier to cageEscalier as Access::inside
				link porteCageEscalier to couloir as Access::outside
			
				link fenetreCouloir to couloir as Access::inside
				link fenetreCouloir to exterieur as Access::outside
			
				link porteOpenspace to openspace as Access::inside
				link porteOpenspace to couloir as Access::outside
			
				link fenetreOpenspace to openspace as Access::inside
				link fenetreOpenspace to parking as Access::outside
			
				link porteAccueil to accueil as Access::inside
				link porteAccueil to couloir as Access::outside
				link fenetreAccueilParking to accueil as Access::inside
				link fenetreAccueilParking to parking as Access::outside
				link barriereParking to parking as Access::inside
				link barriereParking to exterieur as Access::outside
				link fenetreAccueilExterieur to accueil as Access::inside
				link fenetreAccueilExterieur to exterieur as Access::outside
				link porteAccueilPrincipale to accueil as Access::inside
				link porteAccueilPrincipale to parking as Access::outside
			
				link patio to alarmPatio as Zone::alarms
				link couloir to alarmCouloir as Zone::alarms
				link parking to alarmParking1 as Zone::alarms
				link parking to alarmParking2 as Zone::alarms
				link accueil to alarmAccueil as Zone::alarms
			
				link porteAccueilPrincipale to badgeAccueil as BadgeDoor::badges
				link porteBureau to cleBureau as Door::keys
			
				link alarmAccueil to bureau as Alarm::control
				link alarmCouloir to accueil as Alarm::control
			
				link porteLocalTechniqueCouloir to localTechnique as Access::inside
				link porteLocalTechniqueCouloir to couloir as Access::outside
			
				link porteLocalTechniqueCouloir to badgeLocalTechnique as BadgeDoor::badges
			}
			''')
		
		EcoreUtil.resolveAll(abs)
		
		val injector = new PDDLStandaloneSetup().createInjectorAndDoEMFRegistration()

		val PDDLModel pddl = abs.ABS2PDDL().domain
		        
		val rsp = injector.getProvider(ResourceSet)
		val rs = rsp.get
		val res = rs.createResource(URI.createFileURI("dummy.pddl"))
		res.contents.add(pddl)
		val serializer = injector.getInstance(ISerializer)
		        
		val optionsbuilder = SaveOptions.newBuilder()
		optionsbuilder.format;
		val serialized = serializer.serialize(pddl, optionsbuilder.options)
		        
		System.out.println(serialized)
	}
	
}