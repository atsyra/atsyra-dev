package fr.irisa.atsyra.absystem.pddl.transfo.tests

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.eclipse.xtext.serializer.ISerializer
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import org.eclipse.xtext.testing.extensions.InjectionExtension
import fr.irisa.atsyra.absystem.gal.transfo.tests.AssetBasedSystemDslInjectorProvider
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.InjectWith
import fr.irisa.atsyra.pddl.xtext.PDDLRuntimeModule
import com.google.inject.Guice
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.junit.jupiter.api.BeforeEach
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage
import org.junit.jupiter.api.BeforeAll
import org.eclipse.xtext.resource.SaveOptions
import fr.irisa.atsyra.pddl.xtext.PDDLStandaloneSetup
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.EcoreUtil

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABS2PDDLUnassignerTransfoTest {
	
	@Inject
	ParseHelper<AssetBasedSystem> parseHelper
	
	@BeforeEach
	def void setup() {
		AbsystemPackage.eINSTANCE.eClass()
		PDDLPackage.eINSTANCE.eClass()
	}
	
	@Test
	def void testTypes() {
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem
			with Default::*;
			with GoodNamesWereTaken::*;
			with VSE_Definition::*;
			with VSE_Aspect::*;
			with VSE_Declaration::*;
			
			DefinitionGroup GoodNamesWereTaken {
				primitiveDataTypes {
					PrimitiveDataType String
					PrimitiveDataType Boolean
					PrimitiveDataType Integer
					EnumDataType AstrologicalSign {
						isTaurus,
						isNotTaurus
					}
					EnumDataType Day {
						Monday,
						Tuesday,
						Wednesday,
						Thursday,
						Friday,
						Saturday,
						Sunday
					}
				}
				staticMethods {
					StaticMethod contains
					StaticMethod containsAll
					StaticMethod containsAny
					StaticMethod filter
					StaticMethod isEmpty
				}
			}
			
			DefinitionGroup VSE_Definition {
				assetTypes {
					AssetType Zone {
						// Passages/Liens entre les salles
			            reference accesses : Zone [*] default []
			        }
			        
			        // Méchant
					AssetType Attacker {}
			        
					AssetType Item {}
				}
			}
			
			DefinitionGroup VSE_Aspect {
				assetTypes {
					AssetTypeAspect Attacker {
						reference location : Zone
						reference ownedItems : Item [*] default []
						attribute name : String [1]
						attribute isHidden : Boolean [1] default false
						attribute personalAstrologicalSign : AstrologicalSign [1] default AstrologicalSign::isTaurus
						attribute personalAstrologicalSignList : AstrologicalSign [*]
					}
			        AssetTypeAspect Zone {
						reference itemsInZone : Item [*] default []
					}
				}
				guardedActions {
					GuardedAction goes_from_to1 ( anAttacker : Attacker, startZone : Zone, destZone : Zone ) {
						guard = ( anAttacker.location == startZone ) && ( startZone.accesses.contains(destZone) ) && ( startZone != destZone )
						action {
							anAttacker.location.assign(destZone);
						}
					}
					GuardedAction goes_from_to2 ( anAttacker : Attacker, startZone : Zone, destZone : Zone ) {
						guard = ( startZone.accesses.contains(destZone) ) && ( startZone != destZone )
						action {
							anAttacker.location.assign(destZone);
						}
					}
					GuardedAction goes_from_to3 ( unassigner : Attacker, startZone : Zone, destZone : Zone ) {
						guard = ( startZone.accesses.contains(destZone) ) && ( startZone != destZone )
						action {
							unassigner.location.assign(destZone);
						}
					}
				}
			}
			
			AssetGroup  VSE_Declaration {
				Asset Entrance : Zone {}
				Asset Bedroom : Zone {}
				Asset LivingRoom : Zone {}
				Asset Bathroom : Zone {}
				Asset Exit : Zone {}
			
				Asset MATHIS : Attacker {}
			
				Asset VeryImportantDocument : Item {}
				Asset UselessDocument : Item {}
			
			    link Entrance to Bedroom as Zone::accesses
			    link Bedroom to Entrance as Zone::accesses
			    link Bedroom to LivingRoom as Zone::accesses
			    link LivingRoom to Bedroom as Zone::accesses
			    link Bedroom to Bathroom as Zone::accesses
			    link Bathroom to Bedroom as Zone::accesses
			    link Bathroom to Exit as Zone::accesses
			    link Exit to Bathroom as Zone::accesses
			}
			
			AssetGroup VSE_Goal {
				goals {
					Goal from_Entrance_to_Exit_with_VID  {
						pre =
							//attacker location
							MATHIS.location == Entrance
							//items are in their initial location (ie. not owned by the attacker)
							&& Bathroom.itemsInZone.contains(VeryImportantDocument)
							&& Bathroom.itemsInZone.contains(UselessDocument)
						post = 
							MATHIS.location == LivingRoom
							// attacker has the document
							&& MATHIS.ownedItems.contains(VeryImportantDocument)
					}
				}
			}
			''')
		
		EcoreUtil.resolveAll(abs)
		
		val injector = new PDDLStandaloneSetup().createInjectorAndDoEMFRegistration()

		val PDDLModel pddl = abs.ABS2PDDL().domain
		        
		val rsp = injector.getProvider(ResourceSet)
		val rs = rsp.get
		val res = rs.createResource(URI.createFileURI("dummy.pddl"))
		res.contents.add(pddl)
		val serializer = injector.getInstance(ISerializer)
		        
		val optionsbuilder = SaveOptions.newBuilder()
		optionsbuilder.format;
		val serialized = serializer.serialize(pddl, optionsbuilder.options)
		        
		System.out.println(serialized)
	}
	
}