package fr.irisa.atsyra.absystem.pddl.transfo.tests

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.gal.transfo.tests.AssetBasedSystemDslInjectorProvider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.pddl.xtext.PDDLStandaloneSetup
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.resource.SaveOptions
import org.eclipse.xtext.serializer.ISerializer
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import org.eclipse.emf.ecore.util.EcoreUtil

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABS2PDDLPredicatesTransfoTest {
	
	@Inject
	ParseHelper<AssetBasedSystem> parseHelper
	
	@BeforeEach
	def void setup() {
		AbsystemPackage.eINSTANCE.eClass()
		PDDLPackage.eINSTANCE.eClass()
	}
	
	@Test
	def void testPredicates() {
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem 
			with Type_def::*;
			with Default_def::*;
			DefinitionGroup Default_def {
				assetTypes {
					// root of all assetType ?
					AssetType Any {}
					
				}
				primitiveDataTypes {
					PrimitiveDataType String
					PrimitiveDataType Boolean
					PrimitiveDataType Integer
				}
				staticMethods {
					StaticMethod contains
					StaticMethod containsAll
					StaticMethod containsAny
					StaticMethod filter
				}
			}
			DefinitionGroup Type_def {
				assetTypes {
					AssetType T1 {
						attribute value : Boolean
						attribute isNeverWatch : Boolean
						reference thisIsAReferenceToSeeIfItWorks : T1 [*]
					}
					AssetType T2  {
						reference contained : T1 [*]
						attribute isNeverLocked : Boolean
					}
				}
			}
			''')
		
		EcoreUtil.resolveAll(abs)
		
		val injector = new PDDLStandaloneSetup().createInjectorAndDoEMFRegistration()

		val PDDLModel pddl = abs.ABS2PDDL().domain
		        
		val rsp = injector.getProvider(ResourceSet)
		val rs = rsp.get
		val res = rs.createResource(URI.createFileURI("dummy.pddl"))
		res.contents.add(pddl)
		val serializer = injector.getInstance(ISerializer)
		        
		val optionsbuilder = SaveOptions.newBuilder()
		optionsbuilder.format;
		val serialized = serializer.serialize(pddl, optionsbuilder.options)
		        
		System.out.println(serialized)
	}
	
}