package fr.irisa.atsyra.absystem.pddl.transfo.tests

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.eclipse.xtext.serializer.ISerializer
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import org.eclipse.xtext.testing.extensions.InjectionExtension
import fr.irisa.atsyra.absystem.gal.transfo.tests.AssetBasedSystemDslInjectorProvider
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.InjectWith
import fr.irisa.atsyra.pddl.xtext.PDDLRuntimeModule
import com.google.inject.Guice
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.junit.jupiter.api.BeforeEach
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage
import org.junit.jupiter.api.BeforeAll
import org.eclipse.xtext.resource.SaveOptions
import fr.irisa.atsyra.pddl.xtext.PDDLStandaloneSetup
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.EcoreUtil

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABS2PDDLActionsTransfoTest {
	
	@Inject
	ParseHelper<AssetBasedSystem> parseHelper
	
	@BeforeEach
	def void setup() {
		AbsystemPackage.eINSTANCE.eClass()
		PDDLPackage.eINSTANCE.eClass()
	}
	
	@Test
	def void testTypes() {
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem 
			with Type_def::*;
			with Default_def::*;
			DefinitionGroup Default_def {
				primitiveDataTypes {
					PrimitiveDataType Boolean
					PrimitiveDataType String
				}
			}
			DefinitionGroup Type_def {
				assetTypes {
					AssetType T1 {
						reference other : T1
					}
					AssetType T2 extends (T1) {
					}
					AssetType T3 extends (T2) {
					}
				}
				guardedActions {
					GuardedAction do(t1 : T1, t2 : T2) {
						guard = true
						action {
							t2.other.assign(t1);
							t2.other.add(t1);
							t2.other.remove(t1);
						}
					}
				}
			} 
			AssetGroup example {
				Asset a1 : T1 {}
				Asset a2 : T2 {}
			}
			''')
		/*
							//t1.value.assign(true);
							//t2.contained.forAll({t -> t.value.assign(true)});
		 */
		
		EcoreUtil.resolveAll(abs)
		
		val injector = new PDDLStandaloneSetup().createInjectorAndDoEMFRegistration()

		val PDDLModel pddl = abs.ABS2PDDL().domain
		        
		val rsp = injector.getProvider(ResourceSet)
		val rs = rsp.get
		val res = rs.createResource(URI.createFileURI("dummy.pddl"))
		res.contents.add(pddl)
		val serializer = injector.getInstance(ISerializer)
		        
		val optionsbuilder = SaveOptions.newBuilder()
		optionsbuilder.format;
		val serialized = serializer.serialize(pddl, optionsbuilder.options)
		        
		System.out.println(serialized)
	}
	
}