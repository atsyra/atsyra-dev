package fr.irisa.atsyra.absystem.pddl.transfo.tests

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.eclipse.xtext.serializer.ISerializer
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import org.eclipse.xtext.testing.extensions.InjectionExtension
import fr.irisa.atsyra.absystem.gal.transfo.tests.AssetBasedSystemDslInjectorProvider
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.InjectWith
import fr.irisa.atsyra.pddl.xtext.PDDLRuntimeModule
import com.google.inject.Guice
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.junit.jupiter.api.BeforeEach
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage
import org.junit.jupiter.api.BeforeAll
import org.eclipse.xtext.resource.SaveOptions
import fr.irisa.atsyra.pddl.xtext.PDDLStandaloneSetup
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.EcoreUtil

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABS2PDDLObjectsTransfoTest {
	
	@Inject
	ParseHelper<AssetBasedSystem> parseHelper
	
	@BeforeEach
	def void setup() {
		AbsystemPackage.eINSTANCE.eClass()
		PDDLPackage.eINSTANCE.eClass()
	}
	
	@Test
	def void testTypes() {
		/*
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem 
			with Type_def::*;
			with Default_def::*;
			DefinitionGroup Default_def {
				assetTypes {
					AssetType Any {}
					AssetType T1 {}
					AssetType T2 {}
					AssetType Zone {
						reference alarms : Alarm [*]
					}
					AssetType Access {
						reference outside : Zone
						reference inside : Zone
					}
					AssetType Alarm {}
					AssetType Door extends (Access) {
						reference keys : Item [*]
					}
					AssetType BadgeDoor extends (Access) {
						reference badges : Item [*]
					}
					AssetType FreeAccess extends (Access) {}
					AssetType Windows extends (Access) {}
					AssetType Attacker {}
					AssetType Item {}
				}
			}
			DefinitionGroup ReferenceDeclarations {
				assetTypes {
					AssetTypeAspect Zone {
					}
					AssetTypeAspect Access {
						reference outside : Zone
						reference inside : Zone
					}
					AssetTypeAspect Door {
						reference keys : Item [*]
					}
					AssetTypeAspect BadgeDoor {
						reference badges : Item [*]
					}
				}
			}
			AssetGroup  SmallReception_model {
				Asset Exterior : Zone {}
				Asset Lobby : Zone {}
				Asset WelcomeDesk : Zone {}
				Asset Corridor1 : Zone {}
				Asset Corridor2 : Zone {}
				Asset LuggageStorage : Zone {}
				
				Asset Alarm1 : Alarm {}
				
				Asset FrontDoor : Door {}
				Asset WelcomeDeskGate : Door {}
				Asset welcomeDeskToStorageAccess : BadgeDoor {}		
				Asset SideDoor1 : Door {}
				Asset SideAccess2 : FreeAccess {}
				Asset Window1 : Windows {}
				Asset Window2 : Windows {}
				Asset StorageSideDoor1 : Door {}
				Asset StorageSideDoor2 : Door {}
				
				Asset ArseneLupin : Attacker {}
				
				Asset receptionistBadge : Item {}
				Asset key1 : Item {}
				Asset document : Item {}
				
				link FrontDoor to Exterior as Access::outside
				link FrontDoor to Lobby as Access::inside
				link welcomeDeskToStorageAccess to LuggageStorage as Access::inside
				link welcomeDeskToStorageAccess to WelcomeDesk as Access::outside
				link welcomeDeskToStorageAccess to receptionistBadge as BadgeDoor::badges
				link WelcomeDeskGate to Lobby as Access::outside
				link WelcomeDeskGate to WelcomeDesk as Access::inside
				link SideDoor1 to Lobby as Access::inside
				link SideDoor1 to Corridor1 as Access::outside
				link Corridor1 to Alarm1 as Zone::alarms
				link SideDoor1 to key1 as Door::keys
				link SideAccess2 to Lobby as Access::inside
				link SideAccess2 to Corridor2 as Access::outside
				link Window1 to Corridor1 as Access::inside
				link Window1 to Exterior as Access::outside
				link Window2 to Corridor2 as Access::inside
				link Window2 to Exterior as Access::outside
				link StorageSideDoor1 to Corridor1 as Access::inside
				link StorageSideDoor1 to LuggageStorage as Access::outside
				link StorageSideDoor2 to Corridor2 as Access::inside
				link StorageSideDoor2 to LuggageStorage as Access::outside
				link StorageSideDoor2 to key1 as Door::keys
			}
			''')
		*/
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem 
			with Type_def::*;
			with Default_def::*;
			DefinitionGroup Default_def {
				assetTypes {
					AssetType Any {}
					AssetType T1 {}
					AssetType T2 {}
					AssetType Zone {
						reference alarms : Alarm [*]
					}
					AssetType Access {
						reference outside : Zone
						reference inside : Zone
					}
					AssetType Alarm {}
					AssetType Door extends (Access) {
						reference keys : Item [*]
					}
					AssetType BadgeDoor extends (Access) {
						reference badges : Item [*]
					}
					AssetType FreeAccess extends (Access) {}
					AssetType Windows extends (Access) {}
					AssetType Attacker {}
					AssetType Item {}
				}
			}
			AssetGroup  SmallReception_model {
				Asset Exterior : Zone {}
				Asset Lobby : Zone {}
				Asset WelcomeDesk : Zone {}
				Asset Corridor1 : Zone {}
				Asset Corridor2 : Zone {}
				Asset LuggageStorage : Zone {}
				
				Asset Alarm1 : Alarm {}
				
				Asset FrontDoor : Door {}
				Asset WelcomeDeskGate : Door {}
				Asset welcomeDeskToStorageAccess : BadgeDoor {}		
				Asset SideDoor1 : Door {}
				Asset SideAccess2 : FreeAccess {}
				Asset Window1 : Windows {}
				Asset Window2 : Windows {}
				Asset StorageSideDoor1 : Door {}
				Asset StorageSideDoor2 : Door {}
				
				Asset ArseneLupin : Attacker {}
				
				Asset receptionistBadge : Item {}
				Asset key1 : Item {}
				Asset document : Item {}
				
				link FrontDoor to Exterior as Access::outside
				link FrontDoor to Lobby as Access::inside
				link welcomeDeskToStorageAccess to LuggageStorage as Access::inside
				link welcomeDeskToStorageAccess to WelcomeDesk as Access::outside
				link welcomeDeskToStorageAccess to receptionistBadge as BadgeDoor::badges
				link WelcomeDeskGate to Lobby as Access::outside
				link WelcomeDeskGate to WelcomeDesk as Access::inside
				link SideDoor1 to Lobby as Access::inside
				link SideDoor1 to Corridor1 as Access::outside
				link Corridor1 to Alarm1 as Zone::alarms
				link SideDoor1 to key1 as Door::keys
				link SideAccess2 to Lobby as Access::inside
				link SideAccess2 to Corridor2 as Access::outside
				link Window1 to Corridor1 as Access::inside
				link Window1 to Exterior as Access::outside
				link Window2 to Corridor2 as Access::inside
				link Window2 to Exterior as Access::outside
				link StorageSideDoor1 to Corridor1 as Access::inside
				link StorageSideDoor1 to LuggageStorage as Access::outside
				link StorageSideDoor2 to Corridor2 as Access::inside
				link StorageSideDoor2 to LuggageStorage as Access::outside
				link StorageSideDoor2 to key1 as Door::keys
			}
			''')
		
		EcoreUtil.resolveAll(abs)
		
		val injector = new PDDLStandaloneSetup().createInjectorAndDoEMFRegistration()

		val PDDLModel pddl = abs.ABS2PDDL().problem
		        
		val rsp = injector.getProvider(ResourceSet)
		val rs = rsp.get
		val res = rs.createResource(URI.createFileURI("dummy.pddl"))
		res.contents.add(pddl)
		val serializer = injector.getInstance(ISerializer)
		        
		val optionsbuilder = SaveOptions.newBuilder()
		optionsbuilder.format;
		val serialized = serializer.serialize(pddl, optionsbuilder.options)
		        
		System.out.println(serialized)
	}
	
}