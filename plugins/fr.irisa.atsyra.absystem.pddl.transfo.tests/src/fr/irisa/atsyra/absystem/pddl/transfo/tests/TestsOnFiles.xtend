package fr.irisa.atsyra.absystem.pddl.transfo.tests

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.eclipse.xtext.serializer.ISerializer
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import org.eclipse.xtext.testing.extensions.InjectionExtension
import fr.irisa.atsyra.absystem.gal.transfo.tests.AssetBasedSystemDslInjectorProvider
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.InjectWith
import fr.irisa.atsyra.pddl.xtext.PDDLRuntimeModule
import com.google.inject.Guice
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.junit.jupiter.api.BeforeEach
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage
import org.junit.jupiter.api.BeforeAll
import org.eclipse.xtext.resource.SaveOptions

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class TestsOnFiles {
	
	@Inject
	ParseHelper<AssetBasedSystem> parseHelper
	
	//ISerializer serializer
	
	//Injector injector
	
	/*
	String[] listsOfTests = { "global" , "other" }
	
	def void testsFiles() {
		
	}
	
	def AssetBasedSystem loadTestCase(String testFileName) {
		
	}
	@BeforeAll
	def void globalSetup() {
		injector = Guice.createInjector(new PDDLRuntimeModule)
		
		serializer = injector.getInstance(ISerializer)
		
	}
	*/
	
	@BeforeEach
	def void setup() {
		AbsystemPackage.eINSTANCE.eClass()
		PDDLPackage.eINSTANCE.eClass()
	}
	/*
	@Test
	def void testTypes() {
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem 
			with Type_def::*;
			with Default_def::*;
			DefinitionGroup Default_def {
				assetTypes {
					// root of all assetType ?
					AssetType Any {}
					
				}
				primitiveDataTypes {
					PrimitiveDataType String
					PrimitiveDataType Boolean
					PrimitiveDataType Integer
				}
				staticMethods {
					StaticMethod contains
					StaticMethod containsAll
					StaticMethod containsAny
					StaticMethod filter
				}
			}
			DefinitionGroup Type_def {
				assetTypes {
					AssetType T1 {
					}
					AssetType T2  {
					}
				}
			}
			DefinitionGroup Behavior_def {
				assetTypes {
					AssetTypeAspect T1 {
						attribute value : Boolean
					}
					AssetTypeAspect T2 {
						reference contained : T1 [*]
					}
				}
				guardedActions {
					GuardedAction transi(t1 : T1, t2 : T2, t3 : T2) {
						guard = true
						action {
							t2.contained.add(t1);
							t2.contained.addAll(t3.contained);
							t1.value.assign(true);
							t2.contained.clear();
							t2.contained.forAll({t -> t.value.assign(true)});
							t2.contained.remove(t1);
							t2.contained.removeAll(t3.contained);
						} 
					}
				}
			}
			AssetGroup example {
				Asset a1 : T1 {}
				Asset a2 : T2 {}
				Asset a3 : T2 {}
			}
			''')
		
		//Assertions.assertNotNull(abs)
		
		//val Injector injector = Guice.createInjector(new PDDLRuntimeModule)
		
		val PDDLModel pddl = abs.ABS2PDDL()
		
		//val serializer = injector.getInstance(ISerializer)
		
		//Assertions.assertLines('''jdhi''')
		val serialized = serializer.serialize(pddl)
		System.out.println(serialized)
	}*/
	
	@Test
	def void testPredicates() {
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem 
			with Type_def::*;
			with Default_def::*;
			DefinitionGroup Default_def {
				assetTypes {
					// root of all assetType ?
					AssetType Any {}
					
				}
				primitiveDataTypes {
					PrimitiveDataType String
					PrimitiveDataType Boolean
					PrimitiveDataType Integer
				}
				staticMethods {
					StaticMethod contains
					StaticMethod containsAll
					StaticMethod containsAny
					StaticMethod filter
				}
			}
			DefinitionGroup Type_def {
				assetTypes {
					AssetType T1 {
						attribute value : Boolean
						attribute isNeverWatch : Boolean
						reference thisIsAReferenceToSeeIfItWorks : T1 [*]
					}
					AssetType T2  {
						reference contained : T1 [*]
						attribute isNeverLocked : Boolean
					}
				}
			}
			''')
		
		val Injector injector = Guice.createInjector(new PDDLRuntimeModule) // PDDLInjectorProvider.getInjector()
		val serializer = injector.getInstance(ISerializer)
		
		val optionsbuilder = SaveOptions.newBuilder()
		optionsbuilder.format;
		val PDDLModel pddl = abs.ABS2PDDL().domain
		val serialized = serializer.serialize(pddl, optionsbuilder.options)
		
		System.out.println(serialized)
	}
	
}