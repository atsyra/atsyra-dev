package fr.irisa.atsyra.absystem.pddl.transfo.tests

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.eclipse.xtext.serializer.ISerializer
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import org.eclipse.xtext.testing.extensions.InjectionExtension
import fr.irisa.atsyra.absystem.gal.transfo.tests.AssetBasedSystemDslInjectorProvider
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.InjectWith
import fr.irisa.atsyra.pddl.xtext.PDDLRuntimeModule
import com.google.inject.Guice
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.junit.jupiter.api.BeforeEach
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage
import org.junit.jupiter.api.BeforeAll
import org.eclipse.xtext.resource.SaveOptions
import fr.irisa.atsyra.pddl.xtext.PDDLStandaloneSetup
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.EcoreUtil

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABS2PDDL_Agence_TransfoTest {
	
	@Inject
	ParseHelper<AssetBasedSystem> parseHelper
	
	@BeforeEach
	def void setup() {
		AbsystemPackage.eINSTANCE.eClass()
		PDDLPackage.eINSTANCE.eClass()
	}
	
	@Test
	def void testTypes() {
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem
			with definitionGroup::*;
			with agenceModel::*;
			DefinitionGroup definitionGroup {
				primitiveDataTypes {
					PrimitiveDataType String
					PrimitiveDataType Boolean
					PrimitiveDataType Integer
					PrimitiveDataType Version
					EnumDataType EDataSensibility {
						SENSIBLE,
						STANDARD
					}
					EnumDataType EPlateform {
						WINDOWS,
						LINUX,
						MACOS
					}
					EnumDataType EAuthenticationType {
						NONE,
						PASSWORD,
						MFA,
						TOKEN,
						BIOMETRIC
					}
					EnumDataType EPermission {
						USER,
						ADMIN,
						ROOT
					}
					EnumDataType EAccountType {
						DEFAULT,
						LOCAL,
						DOMAIN,
						CLOUD
					}
				}
				staticMethods {
					StaticMethod contains
					StaticMethod containsAll
					StaticMethod containsAny
					StaticMethod filter
					StaticMethod isEmpty
				}
				tags {
					Tag Physical
					Tag Logical
					Tag InformationSystem
					Tag Building
					Tag Human
				}
				assetTypes {
					AssetType Zone {
						level "1"
						tags (Building)
						// reference nestedInformation : AbstractInformation [*]
						reference alarms : Alarm [*]
						reference hasMachines : Machine[*]
					}
			
					AssetType Access {
						level "2"
						tags (Building)
						reference inside : Zone
						reference outside : Zone
						reference alarms : Alarm [*]
					}
					AssetType FreeAccess extends (Access) {
						level "2"
						tags (Building)
					}
					AssetType CloseableAccess extends (Access) {
						level "2"
						tags (Building)
					}
			
					AssetType Door  extends (CloseableAccess) {
						level "2"
						tags (Building)
						reference keys : Item[*]
					}
					AssetType BadgeDoor extends (CloseableAccess) {
						level "2"
						tags (Building)
						reference badges : Item[*]
					}
					AssetType Windows  extends (CloseableAccess) {
						level "2"
						tags (Building)
			
					}
					AssetType Alarm {
						level "3"
						tags (Building)
						reference control : Zone
					}
			
					AssetType Item {
						level "3"
					}
			
					AssetType Attacker {
						tags (Human)
					// reference testRef : Information [*]
					}
					AssetType Machine {
						attribute name : String
						attribute cableLocked : Boolean default false
						reference storages : Storage[*]
						reference location : Zone[1]
						reference exploitedBy : OS[1]
						reference connectedTo : Network[*]
						reference shares : FileSharing[*]
					}
					AssetType Workstation extends (Machine) {
			
					}
					AssetType Server extends (Machine) {
			
					}
					AssetType Storage {
						attribute encrypted : Boolean default false
						reference hostedBy : Machine[1]
						reference encryptedWith : Credential[0..1]
						reference data : Data[*]
					}
					AssetType Data {
						attribute type : EDataSensibility default EDataSensibility::STANDARD
						attribute encrypted : Boolean default false
						reference encryptedWith : Credential[0..1]
						reference storedOn : Storage[1]
					}
					AssetType OS {
						attribute plateform : EPlateform
						attribute version : Version
						reference runsOn : Machine[1]
						reference accounts : Account[*]
					}
					AssetType Account {
						attribute accountType : EAccountType
						attribute authenticationType : EAuthenticationType default EAuthenticationType::PASSWORD
						attribute permission : EPermission default EPermission::USER
						reference credential : Credential[0..1]
						reference validFor : OS[0..1]
					}
					AssetType Credential {
						reference worksWith : Account[*]
					}
					AssetType Network {
						reference machines : Machine[1..*]
					}
					AssetType FileSharing {
						reference sharedBy : Machine[1]
						reference dataShared : Data[*]
						reference authorizedAccount : Account[*]
					}
					AssetTypeAspect Attacker {
						// @runtime
						reference location : Zone
						reference ownedItems : Item [*] default []
						reference knownCredentials : Credential[*] default []
						reference knownSharedFolders : FileSharing[*] default []
						reference knownMachines : Machine[*] default []
						reference stolenData : Data[*] default []
						reference stolenStorage : Storage[*] default []
						reference stolenMachine : Machine[*] default []
						reference compromizedMachine : Machine[*] default []
					}
			
					AssetTypeAspect CloseableAccess {
						// @runtime
						attribute isOpen : Boolean default false
					}
					AssetTypeAspect Door {
						// @runtime
						attribute isLocked : Boolean default true
					}
					AssetTypeAspect BadgeDoor {
						// @runtime
						attribute isLocked : Boolean default true
					}
					AssetTypeAspect Alarm {
						// @runtime
						attribute isActive : Boolean default true
						attribute isTriggered : Boolean default false
					}
					AssetTypeAspect Zone {
						reference itemsInZone : Item [*] default []
					}
				}
				guardedActions {
					GuardedAction goes_from_to_via_freeaccess( anAttacker : Attacker, startZone : Zone, destZone : Zone, usedAccess : FreeAccess ) {
						guard = anAttacker.location == startZone &&
							(( usedAccess.inside == startZone &&  usedAccess.outside == destZone) || 
						     ( usedAccess.outside == startZone &&  usedAccess.inside == destZone)
							)  && 
							( startZone != destZone)
						action {
							anAttacker.location.assign(destZone);
							usedAccess.alarms.filter({alarm  -> alarm.isActive}).forAll({ alarm -> alarm.isTriggered.assign(true)});
							destZone.alarms.filter({alarm  -> alarm.isActive}).forAll({ alarm -> alarm.isTriggered.assign(true)});
						}
					}
			
					GuardedAction goes_from_to_via_closeableaccess(anAttacker : Attacker, startZone : Zone, destZone : Zone, access : CloseableAccess ) {
						guard = anAttacker.location == startZone &&
							((access.inside == startZone && access.outside == destZone) || (access.outside == startZone &&  access.inside == destZone)) && 
							(startZone != destZone) &&
							access.isOpen == true
						action {
							anAttacker.location.assign(destZone);
							access.alarms.filter({alarm  -> alarm.isActive}).forAll({ alarm -> alarm.isTriggered.assign(true)});
							destZone.alarms.filter({alarm  -> alarm.isActive}).forAll({ alarm -> alarm.isTriggered.assign(true)});
						}
					}
			
					GuardedAction open_door( anAttacker : Attacker, usedDoor : Door ) {
						guard = (anAttacker.location == usedDoor.inside || anAttacker.location == usedDoor.outside) &&
							usedDoor.isOpen == false && usedDoor.isLocked == false
						action {
							usedDoor.isOpen.assign(true);
							usedDoor.alarms.filter({alarm  -> alarm.isActive}).forAll({ alarm -> alarm.isTriggered.assign(true)});
						}
					}
					GuardedAction close_door( anAttacker : Attacker, usedDoor : Door ) {
						guard = (anAttacker.location == usedDoor.inside || anAttacker.location == usedDoor.outside) &&
							usedDoor.isOpen == true && usedDoor.isLocked == false
						action {
							usedDoor.isOpen.assign(false);
						// usedDoor.alarms.filter({alarm  -> alarm.isActive}).forAll({ alarm -> alarm.isTriggered.assign(true)});
						}
					}
					GuardedAction open_badgedoor( anAttacker : Attacker, usedDoor : BadgeDoor ) {
						guard = (anAttacker.location == usedDoor.inside || (anAttacker.location == usedDoor.outside && usedDoor.isLocked == false)) &&
							usedDoor.isOpen == false
						action {
							usedDoor.isOpen.assign(true);
							usedDoor.alarms.filter({alarm  -> alarm.isActive}).forAll({ alarm -> alarm.isTriggered.assign(true)});
						}
					}
					GuardedAction close_badgedoor( anAttacker : Attacker, usedDoor : BadgeDoor ) {
						guard = (anAttacker.location == usedDoor.inside || anAttacker.location == usedDoor.outside) &&
							usedDoor.isOpen == true
						action {
							usedDoor.isOpen.assign(false);
						// usedDoor.alarms.filter({alarm  -> alarm.isActive}).forAll({ alarm -> alarm.isTriggered.assign(true)});
						}
					}
					GuardedAction open_window(anAttacker: Attacker, window : Windows) {
						guard = anAttacker.location == window.inside && window.isOpen == false
						action {
							window.isOpen.assign(true);
						}
					}
					GuardedAction close_window(anAttacker: Attacker, window : Windows) {
						guard = (anAttacker.location == window.inside || anAttacker.location == window.outside) && window.isOpen == true
						action {
							window.isOpen.assign(false);
						}
					}
					GuardedAction unlockDoor( anAttacker : Attacker, usedDoor : Door ) {
						guard = (anAttacker.location == usedDoor.inside || anAttacker.location == usedDoor.outside) &&
							anAttacker.ownedItems.containsAny(usedDoor.keys) &&
							usedDoor.isLocked == true
						action {
							usedDoor.isLocked.assign(false);
						}
					}
					GuardedAction unlockBadgeDoor( anAttacker : Attacker, usedDoor : BadgeDoor ) {
						guard = anAttacker.location == usedDoor.outside &&
							anAttacker.ownedItems.containsAny(usedDoor.badges) && 
							usedDoor.isLocked == true
						action {
							usedDoor.isLocked.assign(false);
						}
					}
					GuardedAction pickItemInZone(anAttacker : Attacker, currentZone : Zone, item : Item) {
						guard = anAttacker.location == currentZone && 
			                !anAttacker.ownedItems.contains(item) && 
			                currentZone.itemsInZone.contains (item)
						action {
							anAttacker.ownedItems.add(item);
							currentZone.itemsInZone.remove(item);
						}
					}
					GuardedAction dropItemInZone(anAttacker : Attacker, currentZone : Zone, item : Item) {
						guard = anAttacker.location == currentZone && 
			                anAttacker.ownedItems.contains(item) && 
			                !currentZone.itemsInZone.contains(item)
						action {
							anAttacker.ownedItems.remove(item);
							currentZone.itemsInZone.add(item);
						}
					}
					GuardedAction disableAlarm(anAttacker : Attacker, alarm : Alarm ) {
						guard = anAttacker.location == alarm.control &&
							alarm.isActive == true
						action {
							alarm.isActive.assign(false);
						}
					}
					GuardedAction stealMachine(attacker : Attacker, machine : Machine) {
						guard = attacker.location == machine.location && machine.cableLocked == false
						action {
							attacker.stolenMachine.add(machine);
							attacker.stolenStorage.addAll(machine.storages);
						// machine.storages.forAll({storage -> attacker.stolenData.addAll(storage.data)});
						}
					}
					GuardedAction stealStorage(attacker : Attacker, storage : Storage) {
						guard = storage.hostedBy.location == attacker.location
						action {
							attacker.stolenStorage.add(storage);
							attacker.stolenData.addAll(storage.data);
						}
					}
			
					GuardedAction stealData(attacker : Attacker, data : Data, accountUsed : Account) {
						guard = attacker.location == data.storedOn.hostedBy.location && data.storedOn.hostedBy.exploitedBy.accounts.contains(accountUsed) && attacker.knownCredentials.contains(accountUsed.credential)
						action {
							attacker.stolenData.add(data);
						}
					}
			
					GuardedAction bruteForceOSAuthentication(attacker : Attacker, account : Account) {
						guard = attacker.location == account.validFor.runsOn.location && account.authenticationType == EAuthenticationType::PASSWORD
						action {
							attacker.knownCredentials.add(account.credential);
							attacker.compromizedMachine.add(account.validFor.runsOn);
						}
					}
			
					GuardedAction discoverNetworkFromCompromizedMachine(attacker : Attacker, compromizedMachine : Machine) {
						guard = attacker.compromizedMachine.contains(compromizedMachine) && !compromizedMachine.connectedTo.isEmpty()
						action {
							compromizedMachine.connectedTo.forAll({network -> attacker.knownMachines.addAll(network.machines)});
							attacker.knownMachines.forAll({machine -> attacker.knownSharedFolders.addAll(machine.shares)});
						}
					}
			
					GuardedAction stealDataOnShareForlderViaCompromizedMachine(attacker : Attacker, sharedFolder : FileSharing, sharedFolderAccount : Account, compromizedMachine : Machine) {
						guard = attacker.compromizedMachine.contains(compromizedMachine) &&
						  attacker.knownSharedFolders.contains(sharedFolder) &&
						  sharedFolder.authorizedAccount.contains(sharedFolderAccount) &&
						  (attacker.knownCredentials.contains(sharedFolderAccount.credential) || sharedFolderAccount.authenticationType == EAuthenticationType::NONE)
						action {
							attacker.stolenData.addAll(sharedFolder.dataShared);
						}
					}
				}
			}
			AssetGroup GoalGroup {
				goals {
					Goal goal1 {
						description "attacker should steal DocumentSecret via Acceuil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur
					}
					Goal goal2 {
						description "attacker should steal DocumentSecret via Patio OR Accueil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur
					}
					Goal goal3 {
						description "attacker should steal DocumentSecret via Patio and Reunion"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							portePatioExterieur.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							porteOpenspace.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur
					}
					Goal goal4 {
						description "attacker should steal DocumentSecret via Patio OR Patio and Reunion OR Accueil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur
					}
					Goal goal1NoAlarms {
						description "attacker would steal DocumentSecret via Acceuil, not triggering any alarms (not possible)"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur && 
							alarmPatio.isTriggered == false && 
							alarmCouloir.isTriggered == false && 
							alarmAccueil.isTriggered == false && 
							alarmParking1.isTriggered == false && 
							alarmParking2.isTriggered == false
					}
					Goal goal5 {
						description "attacker should enter via Patio, steal DocumentSecret, and leave either via Patio OR Accueil after disabling alarmAccueil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							alarmPatio.isActive == false && 
							alarmCouloir.isActive == false && 
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur && 
							alarmPatio.isTriggered == false && 
							alarmCouloir.isTriggered == false && 
							alarmAccueil.isTriggered == false && 
							alarmParking1.isTriggered == false && 
							alarmParking2.isTriggered == false
					}
					Goal goal6 {
						description "attacker should enter via Accueil, disable alarmCouloir, steal DocumentSecret, and leave either via Patio OR Accueil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							alarmAccueil.isActive == false && 
							alarmParking1.isActive == false && 
							alarmParking2.isActive == false && 
							alarmPatio.isActive == false && 
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur && 
							alarmPatio.isTriggered == false && 
							alarmCouloir.isTriggered == false && 
							alarmAccueil.isTriggered == false && 
							alarmParking1.isTriggered == false && 
							alarmParking2.isTriggered == false
					}
					Goal goal7 {
						description "attacker should enter via Accueil, disable alarmCouloir, steal DocumentSecret, and leave either via Patio OR Accueil, but also all doors & windows must be closed"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							portePatioExterieur.isLocked == false &&
							portePatioCouloir.isLocked == false &&
							fenetreReunion.isOpen == true &&
							porteReunion.isLocked == false &&
							alarmAccueil.isActive == false && 
							alarmParking1.isActive == false && 
							alarmParking2.isActive == false && 
							alarmPatio.isActive == false && 
							robinHood.ownedItems.contains(badgeAccueil)
						post = robinHood.ownedItems.contains(documentSecret) && robinHood.location == exterieur && 
							alarmPatio.isTriggered == false && 
							alarmCouloir.isTriggered == false && 
							alarmAccueil.isTriggered == false && 
							alarmParking1.isTriggered == false && 
							alarmParking2.isTriggered == false && 
							porteAccueil.isOpen == false && 
							porteAccueilPrincipale.isOpen == false && 
							porteBureau.isOpen == false && 
							porteCageEscalier.isOpen == false && 
							porteOpenspace.isOpen == false && 
							portePatioCouloir.isOpen == false && 
							portePatioExterieur.isOpen == false && 
							porteReunion.isOpen == false && 
							fenetreAccueilExterieur.isOpen == false && 
							fenetreAccueilParking.isOpen == false && 
							fenetreBureau.isOpen == false && 
							fenetreCouloir.isOpen == false && 
							fenetreOpenspace.isOpen == false && 
							fenetreReunion.isOpen == false
					}
					Goal DEgoal {
						description "attacker should steal DocumentSecret via Acceuil"
						pre = robinHood.location == exterieur &&
							bureau.itemsInZone.contains(documentSecret) &&
							openspace.itemsInZone.contains(cleBureau) &&
							porteOpenspace.isLocked == false &&
							porteAccueil.isLocked == false &&
							robinHood.ownedItems.contains(badgeAccueil) &&
							!robinHood.ownedItems.contains(badgeLocalTechnique)
						post = robinHood.location == exterieur && !robinHood.stolenData.filter({d -> d.type == EDataSensibility::SENSIBLE}).isEmpty()
					}
				}
			}
			
			AssetGroup agenceModel {
			
				Asset exterieur : Zone {
				}
				Asset patio : Zone {
				}
				Asset reunion : Zone {
				}
				Asset bureau : Zone {
				}
				Asset couloir : Zone {
				}
				Asset cageEscalier : Zone {
				}
				Asset openspace : Zone {
				}
				Asset accueil : Zone {
				}
				Asset parking : Zone {
				}
				Asset localTechnique : Zone {
				}
			
				Asset porteLocalTechniqueCouloir : BadgeDoor {
				}
				Asset portePatioCouloir : Door {
				}
				Asset portePatioExterieur : Door {
				}
				Asset fenetreReunion : Windows {
				}
				Asset fenetreBureau : Windows {
				}
			
				Asset porteReunion : Door {
				}
				Asset porteBureau : Door {
				}
			
				Asset escalier : FreeAccess {
				}
				Asset porteCageEscalier : Door {
				}
			
				Asset fenetreCouloir : Windows {
				}
			
				Asset porteOpenspace : Door {
				}
				Asset fenetreOpenspace : Windows {
				}
			
				Asset porteAccueil : Door {
				}
				Asset fenetreAccueilParking : Windows {
				}
				Asset fenetreAccueilExterieur : Windows {
				}
				Asset porteAccueilPrincipale : BadgeDoor {
				}
				Asset barriereParking : FreeAccess {
				}
			
				Asset robinHood : Attacker {
				}
			
				Asset alarmPatio : Alarm {
				}
				Asset alarmCouloir : Alarm {
				}
				Asset alarmAccueil : Alarm {
				}
				Asset alarmParking1 : Alarm {
				}
				Asset alarmParking2 : Alarm {
				}
			
				Asset badgeAccueil : Item {
				}
				Asset documentSecret : Item {
				}
				Asset badgeLocalTechnique : Item {
				}
			
				Asset cleBureau : Item {
				}
			
				Asset webServer : Server {
				}
				Asset nas : Server {
				}
				Asset pc1Bureau : Workstation {
				}
				Asset pc2Openspace : Workstation {
				}
			
				Asset hddWebServer : Storage {
					attribute Storage::encrypted := true
				}
				Asset hddNas : Storage {
				}
				Asset hddPc1Bureau : Storage {
				}
				Asset hddPc2Openspace : Storage {
				}
			
				Asset dataWebServer : Data {
					attribute Data::type := definitionGroup::EDataSensibility::SENSIBLE
				}
				Asset dataNAS : Data {
					attribute Data::type := definitionGroup::EDataSensibility::SENSIBLE
				}
				Asset dataPc1Bureau : Data {
				}
				Asset dataPc2Openspace : Data {
				}
			
				Asset osWebServer : OS {
					attribute OS::plateform := EPlateform::LINUX
					attribute OS::version := 5.17.4
				}
				Asset osNas : OS {
					attribute OS::plateform := definitionGroup::EPlateform::LINUX
				}
				Asset osPc1Bureau : OS {
					attribute OS::plateform := definitionGroup::EPlateform::WINDOWS
					attribute OS::version := 10.0.0
				}
				Asset osPc2Openspace : OS {
					attribute OS::plateform := EPlateform::WINDOWS
					attribute OS::version := 7.0.0
				}
			
				Asset rootAccountWebServer : Account {
				}
				Asset userAccountPc1 : Account {
					attribute Account::accountType := EAccountType::LOCAL
				}
				Asset userAccountPc2 : Account {
					attribute Account::accountType := EAccountType::LOCAL
				}
				Asset commonFileSharingAccount : Account {
					attribute Account::authenticationType := EAuthenticationType::NONE
					attribute Account::accountType := EAccountType::LOCAL
				}
				Asset credentialRootAccountWebServer : Credential {
				}
				Asset credentialUserAccountPc1 : Credential {
				}
				Asset credentialUserAccountPc2 : Credential {
				}
				Asset agenceLocalNetwork : Network {
				}
				Asset commonFileSharing : FileSharing {
				}
			
				link portePatioCouloir to patio as Access::inside
				link portePatioCouloir to couloir as Access::outside
				link portePatioExterieur to patio as Access::inside
				link portePatioExterieur to exterieur as Access::outside
			
				link fenetreBureau to patio as Access::outside
				link fenetreBureau to bureau as Access::inside
			
				link fenetreReunion to patio as Access::outside
				link fenetreReunion to reunion as Access::inside
			
				link porteBureau to bureau as Access::inside
				link porteBureau to couloir as Access::outside
			
				link porteReunion to reunion as Access::inside
				link porteReunion to couloir as Access::outside
			
				link escalier to exterieur as Access::outside
				link escalier to cageEscalier as Access::inside
			
				link porteCageEscalier to cageEscalier as Access::inside
				link porteCageEscalier to couloir as Access::outside
			
				link fenetreCouloir to couloir as Access::inside
				link fenetreCouloir to exterieur as Access::outside
			
				link porteOpenspace to openspace as Access::inside
				link porteOpenspace to couloir as Access::outside
			
				link fenetreOpenspace to openspace as Access::inside
				link fenetreOpenspace to parking as Access::outside
			
				link porteAccueil to accueil as Access::inside
				link porteAccueil to couloir as Access::outside
				link fenetreAccueilParking to accueil as Access::inside
				link fenetreAccueilParking to parking as Access::outside
				link barriereParking to parking as Access::inside
				link barriereParking to exterieur as Access::outside
				link fenetreAccueilExterieur to accueil as Access::inside
				link fenetreAccueilExterieur to exterieur as Access::outside
				link porteAccueilPrincipale to accueil as Access::inside
				link porteAccueilPrincipale to parking as Access::outside
			
				link patio to alarmPatio as Zone::alarms
				link couloir to alarmCouloir as Zone::alarms
				link parking to alarmParking1 as Zone::alarms
				link parking to alarmParking2 as Zone::alarms
				link accueil to alarmAccueil as Zone::alarms
			
				link porteAccueilPrincipale to badgeAccueil as BadgeDoor::badges
				link porteBureau to cleBureau as Door::keys
			
				link alarmAccueil to bureau as Alarm::control
				link alarmCouloir to accueil as Alarm::control
			
				link porteLocalTechniqueCouloir to localTechnique as Access::inside
				link porteLocalTechniqueCouloir to couloir as Access::outside
			
				link porteLocalTechniqueCouloir to badgeLocalTechnique as BadgeDoor::badges
				link webServer to localTechnique as Machine::location
				link nas to localTechnique as Machine::location
				link pc1Bureau to bureau as Machine::location
				link pc2Openspace to openspace as Machine::location
			
				link webServer to hddWebServer as Machine::storages
				link nas to hddNas as Machine::storages
				link pc1Bureau to hddPc1Bureau as Machine::storages
				link pc2Openspace to hddPc2Openspace as Machine::storages
				// reverse, because it's mandatory 
				link hddWebServer to webServer as Storage::hostedBy
				link hddNas to nas as Storage::hostedBy
				link hddPc1Bureau to pc1Bureau as Storage::hostedBy
				link hddPc2Openspace to pc2Openspace as Storage::hostedBy
			
				link hddWebServer to dataWebServer as Storage::data
				link hddNas to dataNAS as Storage::data
				link hddPc1Bureau to dataPc1Bureau as Storage::data
				link hddPc2Openspace to dataPc2Openspace as Storage::data
				// reverse
				link dataWebServer to hddWebServer as Data::storedOn
				link dataNAS to hddNas as Data::storedOn
				link dataPc1Bureau to hddPc1Bureau as Data::storedOn
				link dataPc2Openspace to hddPc2Openspace as Data::storedOn
			
				link webServer to osWebServer as Machine::exploitedBy
				link nas to osNas as Machine::exploitedBy
				link pc1Bureau to osPc1Bureau as Machine::exploitedBy
				link pc2Openspace to osPc2Openspace as Machine::exploitedBy
				// reverse
				link osWebServer to webServer as OS::runsOn
				link osNas to nas as OS::runsOn
				link osPc1Bureau to pc1Bureau as OS::runsOn
				link osPc2Openspace to pc2Openspace as OS::runsOn
			
				link osWebServer to rootAccountWebServer as OS::accounts
				link osPc1Bureau to userAccountPc1 as OS::accounts
				link osPc2Openspace to userAccountPc2 as OS::accounts
				// reverse
				link rootAccountWebServer to osWebServer as Account::validFor
				link userAccountPc1 to osPc1Bureau as Account::validFor
				link userAccountPc2 to osPc2Openspace as Account::validFor
			
				link rootAccountWebServer to credentialRootAccountWebServer as Account::credential
				link userAccountPc1 to credentialUserAccountPc1 as Account::credential
				link userAccountPc2 to credentialUserAccountPc2 as Account::credential
			
				link agenceLocalNetwork to webServer as Network::machines
				link agenceLocalNetwork to nas as Network::machines
				link agenceLocalNetwork to pc1Bureau as Network::machines
				link agenceLocalNetwork to pc2Openspace as Network::machines
				// reverse
				link webServer to agenceLocalNetwork as Machine::connectedTo
				link nas to agenceLocalNetwork as Machine::connectedTo
				link pc1Bureau to agenceLocalNetwork as Machine::connectedTo
				link pc2Openspace to agenceLocalNetwork as Machine::connectedTo
			
				link nas to commonFileSharing as Machine::shares
				// reverse
				link commonFileSharing to nas as FileSharing::sharedBy
			
				link commonFileSharing to dataNAS as FileSharing::dataShared
				link commonFileSharing to commonFileSharingAccount as FileSharing::authorizedAccount
			}
			''')
		
		EcoreUtil.resolveAll(abs)
		
		val injector = new PDDLStandaloneSetup().createInjectorAndDoEMFRegistration()

		val PDDLModel pddl = abs.ABS2PDDL().domain
		        
		val rsp = injector.getProvider(ResourceSet)
		val rs = rsp.get
		val res = rs.createResource(URI.createFileURI("dummy.pddl"))
		res.contents.add(pddl)
		val serializer = injector.getInstance(ISerializer)
		        
		val optionsbuilder = SaveOptions.newBuilder()
		optionsbuilder.format;
		val serialized = serializer.serialize(pddl, optionsbuilder.options)
		        
		System.out.println(serialized)
	}
	
}