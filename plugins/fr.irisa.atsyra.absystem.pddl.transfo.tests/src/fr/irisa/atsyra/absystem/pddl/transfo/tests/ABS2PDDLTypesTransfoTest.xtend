package fr.irisa.atsyra.absystem.pddl.transfo.tests

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.gal.transfo.tests.AssetBasedSystemDslInjectorProvider
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.pddl.xtext.PDDLStandaloneSetup
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.resource.SaveOptions
import org.eclipse.xtext.serializer.ISerializer
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import org.eclipse.emf.ecore.util.EcoreUtil

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABS2PDDLTypesTransfoTest {
	
	@Inject
	ParseHelper<AssetBasedSystem> parseHelper
	
	@BeforeEach
	def void setup() {
		AbsystemPackage.eINSTANCE.eClass()
		PDDLPackage.eINSTANCE.eClass()
	}
	
	@Test
	def void testTypes() {
		val AssetBasedSystem abs = parseHelper.parse('''
			AssetBasedSystem 
			with Type_def::*;
			with Default_def::*;
			DefinitionGroup Default_def {
				assetTypes {
					// root of all assetType ?
					AssetType Any {}
					
				}
				primitiveDataTypes {
					PrimitiveDataType String
					PrimitiveDataType Boolean
					PrimitiveDataType Integer
				}
				staticMethods {
					StaticMethod contains
					StaticMethod containsAll
					StaticMethod containsAny
					StaticMethod filter
				}
			}
			DefinitionGroup Type_def {
				assetTypes {
					AssetType T1 {
					}
					AssetType T2  {
					}
				}
			}
			DefinitionGroup Behavior_def {
				assetTypes {
					AssetTypeAspect T1 {
						attribute value : Boolean
					}
					AssetTypeAspect T2 {
						reference contained : T1 [*]
						reference contained2 : T1 [1]
					}
				}
				guardedActions {
					GuardedAction transi(t1 : T1, t2 : T2, t3 : T2) {
						guard = t2 != t3
						action {
							t2.contained.add(t1);
							t2.contained.addAll(t3.contained);
							//t1.value.assign(true);
							t2.contained.clear();
							//t2.contained.forAll({t -> t.value.assign(true)});
							t2.contained.remove(t1);
							t2.contained.removeAll(t3.contained);
						} 
					}
					GuardedAction addTest(t1 : T1, t2 : T2) {
						guard = true
						action {
							t2.contained.add(t1);
						}
					}
					GuardedAction clearTest(t2 : T2) {
						guard = true
						action {
							t2.contained.clear();
						}
					}
					GuardedAction addAllTest(t2 : T2, t3 : T2) {
						guard = true
						action {
							t2.contained.addAll(t3.contained);
						}
					}
					GuardedAction removeTest(t1 : T1, t2 : T2) {
						guard = true
						action {
							t2.contained.remove(t1);
						}
					}
					GuardedAction removeAllTest(t2 : T2, t3 : T2) {
						guard = true
						action {
							t2.contained.removeAll(t3.contained);
						}
					}
				}
			}
			AssetGroup example {
				Asset a1 : T1 {}
				Asset a2 : T2 {}
				Asset a3 : T2 {}
			}
			''')
		
		EcoreUtil.resolveAll(abs)
		
		val injector = new PDDLStandaloneSetup().createInjectorAndDoEMFRegistration()

		val PDDLModel pddl = abs.ABS2PDDL().domain
		        
		val rsp = injector.getProvider(ResourceSet)
		val rs = rsp.get
		val res = rs.createResource(URI.createFileURI("dummy.pddl"))
		res.contents.add(pddl)
		val serializer = injector.getInstance(ISerializer)
		        
		val optionsbuilder = SaveOptions.newBuilder()
		optionsbuilder.format;
		val serialized = serializer.serialize(pddl, optionsbuilder.options)
		        
		System.out.println(serialized)
	}
	
}