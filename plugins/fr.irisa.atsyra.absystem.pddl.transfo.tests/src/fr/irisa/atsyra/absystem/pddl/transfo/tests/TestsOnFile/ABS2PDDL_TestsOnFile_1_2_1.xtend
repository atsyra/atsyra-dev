package fr.irisa.atsyra.absystem.pddl.transfo.tests.TestsOnFile

import static extension fr.irisa.atsyra.absystem.pddl.transfo.aspects.AssetBasedSystemAspect.*
import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.eclipse.xtext.serializer.ISerializer
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLModel
import org.eclipse.xtext.testing.extensions.InjectionExtension
import fr.irisa.atsyra.absystem.gal.transfo.tests.AssetBasedSystemDslInjectorProvider
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.InjectWith
import fr.irisa.atsyra.pddl.xtext.PDDLRuntimeModule
import com.google.inject.Guice
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import org.junit.jupiter.api.BeforeEach
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage
import org.junit.jupiter.api.BeforeAll
import org.eclipse.xtext.resource.SaveOptions
import fr.irisa.atsyra.pddl.xtext.PDDLStandaloneSetup
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.EcoreUtil
import java.util.Scanner
import java.io.File
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.nodemodel.serialization.SerializationUtil
import fr.irisa.atsyra.absystem.pddl.transfo.aspects.DataOut

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ABS2PDDL_TestsOnFile_1_2_1 {
	@Inject
	Injector injector

	AssetBasedSystem abs
	XtextResourceSet absresourceSet;
	
	@BeforeEach
	def void SetUpBeforeClass() {
		AbsystemPackage.eINSTANCE.eClass()
		absresourceSet = injector.getInstance(typeof(XtextResourceSet))
		absresourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE)
	}
	
	@Test
	def void testTypes() {
		val String location = "1_DefinitionGroup2Domain/2_ReferenceAndAttribute2Predicates/1_ImmutableReferences/"
		
		// Load ABSTest to test
		val absuri = URI.createFileURI("TestFiles/" + location + "input.abs")
		val absRes = absresourceSet.getResource(absuri, true)
		abs =  absRes.getContents().get(0) as AssetBasedSystem
		
		// Load expected PDDLText
		val scannerDM = new Scanner(new File("TestFiles/" + location + "expected-domain.pddl"))
		
		// Translate ABS File into PDDL text
		Assertions.assertNotNull(abs)
		try {
			EcoreUtil.resolveAll(abs)
			
			val injector = new PDDLStandaloneSetup().createInjectorAndDoEMFRegistration()
			
			val DataOut out = abs.ABS2PDDL()
			
			
			System.out.println("\n\n----------DOMAIN----------")
			val PDDLModel pddlDM = out.domain
			Assertions.assertNotNull(pddlDM)
			
			val rspDM = injector.getProvider(ResourceSet)
			val rsDM = rspDM.get
			val resDM = rsDM.createResource(URI.createFileURI("dummyDM.pddl"))
			resDM.contents.add(pddlDM)
			val serializerDM = injector.getInstance(ISerializer)
			
			val optionsbuilderDM = SaveOptions.newBuilder()
			optionsbuilderDM.format;
			val serializedDM = serializerDM.serialize(pddlDM, optionsbuilderDM.options)
			
			// Print Result and Expected PDDL Text translated from ABS File
			System.out.println(serializedDM)
			
			// Compare transformed PDDL from ABS File AND PDDL expected text
			Assertions.assertLinesMatch(scannerDM.useDelimiter("\n").tokens, serializedDM.lines)
			
		} catch (RuntimeException e) {
			Assertions.fail(e.message)
			return
		} finally {
			scannerDM.close()
		}
		
	}
	
}