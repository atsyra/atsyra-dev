/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.topoplan.infos.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.topoplan.infos.services.TpiGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalTpiParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Infos'", "'for'", "'Info'", "'{'", "';'", "'}'", "'Zone'", "'Ext'", "'Access'", "'Window'", "'Elevator'", "'Outside'", "'Inside'", "'Alarms'", "'Keys'", "'key'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalTpiParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalTpiParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalTpiParser.tokenNames; }
    public String getGrammarFileName() { return "InternalTpi.g"; }



     	private TpiGrammarAccess grammarAccess;

        public InternalTpiParser(TokenStream input, TpiGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected TpiGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalTpi.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalTpi.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalTpi.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalTpi.g:71:1: ruleModel returns [EObject current=null] : ( () otherlv_1= 'Infos' otherlv_2= 'for' ( (lv_exportName_3_0= ruleEString ) ) ( ( (lv_info_4_0= ruleInfo ) ) | ( (lv_room_5_0= ruleRoom ) ) | ( (lv_access_6_0= ruleAccess ) ) | ( (lv_window_7_0= ruleWindow ) ) | ( (lv_elevators_8_0= ruleElevator ) ) | ( (lv_inside_9_0= ruleInside ) ) | ( (lv_outside_10_0= ruleOutside ) ) )* ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_exportName_3_0 = null;

        EObject lv_info_4_0 = null;

        EObject lv_room_5_0 = null;

        EObject lv_access_6_0 = null;

        EObject lv_window_7_0 = null;

        EObject lv_elevators_8_0 = null;

        EObject lv_inside_9_0 = null;

        EObject lv_outside_10_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:77:2: ( ( () otherlv_1= 'Infos' otherlv_2= 'for' ( (lv_exportName_3_0= ruleEString ) ) ( ( (lv_info_4_0= ruleInfo ) ) | ( (lv_room_5_0= ruleRoom ) ) | ( (lv_access_6_0= ruleAccess ) ) | ( (lv_window_7_0= ruleWindow ) ) | ( (lv_elevators_8_0= ruleElevator ) ) | ( (lv_inside_9_0= ruleInside ) ) | ( (lv_outside_10_0= ruleOutside ) ) )* ) )
            // InternalTpi.g:78:2: ( () otherlv_1= 'Infos' otherlv_2= 'for' ( (lv_exportName_3_0= ruleEString ) ) ( ( (lv_info_4_0= ruleInfo ) ) | ( (lv_room_5_0= ruleRoom ) ) | ( (lv_access_6_0= ruleAccess ) ) | ( (lv_window_7_0= ruleWindow ) ) | ( (lv_elevators_8_0= ruleElevator ) ) | ( (lv_inside_9_0= ruleInside ) ) | ( (lv_outside_10_0= ruleOutside ) ) )* )
            {
            // InternalTpi.g:78:2: ( () otherlv_1= 'Infos' otherlv_2= 'for' ( (lv_exportName_3_0= ruleEString ) ) ( ( (lv_info_4_0= ruleInfo ) ) | ( (lv_room_5_0= ruleRoom ) ) | ( (lv_access_6_0= ruleAccess ) ) | ( (lv_window_7_0= ruleWindow ) ) | ( (lv_elevators_8_0= ruleElevator ) ) | ( (lv_inside_9_0= ruleInside ) ) | ( (lv_outside_10_0= ruleOutside ) ) )* )
            // InternalTpi.g:79:3: () otherlv_1= 'Infos' otherlv_2= 'for' ( (lv_exportName_3_0= ruleEString ) ) ( ( (lv_info_4_0= ruleInfo ) ) | ( (lv_room_5_0= ruleRoom ) ) | ( (lv_access_6_0= ruleAccess ) ) | ( (lv_window_7_0= ruleWindow ) ) | ( (lv_elevators_8_0= ruleElevator ) ) | ( (lv_inside_9_0= ruleInside ) ) | ( (lv_outside_10_0= ruleOutside ) ) )*
            {
            // InternalTpi.g:79:3: ()
            // InternalTpi.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getModelAccess().getModelAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getModelAccess().getInfosKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_2, grammarAccess.getModelAccess().getForKeyword_2());
            		
            // InternalTpi.g:94:3: ( (lv_exportName_3_0= ruleEString ) )
            // InternalTpi.g:95:4: (lv_exportName_3_0= ruleEString )
            {
            // InternalTpi.g:95:4: (lv_exportName_3_0= ruleEString )
            // InternalTpi.g:96:5: lv_exportName_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getModelAccess().getExportNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_5);
            lv_exportName_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModelRule());
            					}
            					set(
            						current,
            						"exportName",
            						lv_exportName_3_0,
            						"fr.irisa.topoplan.infos.Tpi.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTpi.g:113:3: ( ( (lv_info_4_0= ruleInfo ) ) | ( (lv_room_5_0= ruleRoom ) ) | ( (lv_access_6_0= ruleAccess ) ) | ( (lv_window_7_0= ruleWindow ) ) | ( (lv_elevators_8_0= ruleElevator ) ) | ( (lv_inside_9_0= ruleInside ) ) | ( (lv_outside_10_0= ruleOutside ) ) )*
            loop1:
            do {
                int alt1=8;
                switch ( input.LA(1) ) {
                case 13:
                    {
                    alt1=1;
                    }
                    break;
                case 17:
                    {
                    alt1=2;
                    }
                    break;
                case 19:
                    {
                    alt1=3;
                    }
                    break;
                case 20:
                    {
                    alt1=4;
                    }
                    break;
                case 21:
                    {
                    alt1=5;
                    }
                    break;
                case 23:
                    {
                    alt1=6;
                    }
                    break;
                case 22:
                    {
                    alt1=7;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // InternalTpi.g:114:4: ( (lv_info_4_0= ruleInfo ) )
            	    {
            	    // InternalTpi.g:114:4: ( (lv_info_4_0= ruleInfo ) )
            	    // InternalTpi.g:115:5: (lv_info_4_0= ruleInfo )
            	    {
            	    // InternalTpi.g:115:5: (lv_info_4_0= ruleInfo )
            	    // InternalTpi.g:116:6: lv_info_4_0= ruleInfo
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getInfoInfoParserRuleCall_4_0_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_info_4_0=ruleInfo();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"info",
            	    							lv_info_4_0,
            	    							"fr.irisa.topoplan.infos.Tpi.Info");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalTpi.g:134:4: ( (lv_room_5_0= ruleRoom ) )
            	    {
            	    // InternalTpi.g:134:4: ( (lv_room_5_0= ruleRoom ) )
            	    // InternalTpi.g:135:5: (lv_room_5_0= ruleRoom )
            	    {
            	    // InternalTpi.g:135:5: (lv_room_5_0= ruleRoom )
            	    // InternalTpi.g:136:6: lv_room_5_0= ruleRoom
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getRoomRoomParserRuleCall_4_1_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_room_5_0=ruleRoom();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"room",
            	    							lv_room_5_0,
            	    							"fr.irisa.topoplan.infos.Tpi.Room");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalTpi.g:154:4: ( (lv_access_6_0= ruleAccess ) )
            	    {
            	    // InternalTpi.g:154:4: ( (lv_access_6_0= ruleAccess ) )
            	    // InternalTpi.g:155:5: (lv_access_6_0= ruleAccess )
            	    {
            	    // InternalTpi.g:155:5: (lv_access_6_0= ruleAccess )
            	    // InternalTpi.g:156:6: lv_access_6_0= ruleAccess
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getAccessAccessParserRuleCall_4_2_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_access_6_0=ruleAccess();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"access",
            	    							lv_access_6_0,
            	    							"fr.irisa.topoplan.infos.Tpi.Access");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalTpi.g:174:4: ( (lv_window_7_0= ruleWindow ) )
            	    {
            	    // InternalTpi.g:174:4: ( (lv_window_7_0= ruleWindow ) )
            	    // InternalTpi.g:175:5: (lv_window_7_0= ruleWindow )
            	    {
            	    // InternalTpi.g:175:5: (lv_window_7_0= ruleWindow )
            	    // InternalTpi.g:176:6: lv_window_7_0= ruleWindow
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getWindowWindowParserRuleCall_4_3_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_window_7_0=ruleWindow();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"window",
            	    							lv_window_7_0,
            	    							"fr.irisa.topoplan.infos.Tpi.Window");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalTpi.g:194:4: ( (lv_elevators_8_0= ruleElevator ) )
            	    {
            	    // InternalTpi.g:194:4: ( (lv_elevators_8_0= ruleElevator ) )
            	    // InternalTpi.g:195:5: (lv_elevators_8_0= ruleElevator )
            	    {
            	    // InternalTpi.g:195:5: (lv_elevators_8_0= ruleElevator )
            	    // InternalTpi.g:196:6: lv_elevators_8_0= ruleElevator
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getElevatorsElevatorParserRuleCall_4_4_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_elevators_8_0=ruleElevator();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"elevators",
            	    							lv_elevators_8_0,
            	    							"fr.irisa.topoplan.infos.Tpi.Elevator");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 6 :
            	    // InternalTpi.g:214:4: ( (lv_inside_9_0= ruleInside ) )
            	    {
            	    // InternalTpi.g:214:4: ( (lv_inside_9_0= ruleInside ) )
            	    // InternalTpi.g:215:5: (lv_inside_9_0= ruleInside )
            	    {
            	    // InternalTpi.g:215:5: (lv_inside_9_0= ruleInside )
            	    // InternalTpi.g:216:6: lv_inside_9_0= ruleInside
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getInsideInsideParserRuleCall_4_5_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_inside_9_0=ruleInside();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"inside",
            	    							lv_inside_9_0,
            	    							"fr.irisa.topoplan.infos.Tpi.Inside");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 7 :
            	    // InternalTpi.g:234:4: ( (lv_outside_10_0= ruleOutside ) )
            	    {
            	    // InternalTpi.g:234:4: ( (lv_outside_10_0= ruleOutside ) )
            	    // InternalTpi.g:235:5: (lv_outside_10_0= ruleOutside )
            	    {
            	    // InternalTpi.g:235:5: (lv_outside_10_0= ruleOutside )
            	    // InternalTpi.g:236:6: lv_outside_10_0= ruleOutside
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getOutsideOutsideParserRuleCall_4_6_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_outside_10_0=ruleOutside();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"outside",
            	    							lv_outside_10_0,
            	    							"fr.irisa.topoplan.infos.Tpi.Outside");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleInfo"
    // InternalTpi.g:258:1: entryRuleInfo returns [EObject current=null] : iv_ruleInfo= ruleInfo EOF ;
    public final EObject entryRuleInfo() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInfo = null;


        try {
            // InternalTpi.g:258:45: (iv_ruleInfo= ruleInfo EOF )
            // InternalTpi.g:259:2: iv_ruleInfo= ruleInfo EOF
            {
             newCompositeNode(grammarAccess.getInfoRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInfo=ruleInfo();

            state._fsp--;

             current =iv_ruleInfo; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInfo"


    // $ANTLR start "ruleInfo"
    // InternalTpi.g:265:1: ruleInfo returns [EObject current=null] : ( () otherlv_1= 'Info' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_type_4_0= ruleType ) ) otherlv_5= ';' ( ( (lv_alarms_6_0= ruleAlarms ) ) otherlv_7= ';' )? ( ( (lv_keys_8_0= ruleKeys ) ) otherlv_9= ';' )? otherlv_10= '}' ) ;
    public final EObject ruleInfo() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_type_4_0 = null;

        EObject lv_alarms_6_0 = null;

        EObject lv_keys_8_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:271:2: ( ( () otherlv_1= 'Info' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_type_4_0= ruleType ) ) otherlv_5= ';' ( ( (lv_alarms_6_0= ruleAlarms ) ) otherlv_7= ';' )? ( ( (lv_keys_8_0= ruleKeys ) ) otherlv_9= ';' )? otherlv_10= '}' ) )
            // InternalTpi.g:272:2: ( () otherlv_1= 'Info' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_type_4_0= ruleType ) ) otherlv_5= ';' ( ( (lv_alarms_6_0= ruleAlarms ) ) otherlv_7= ';' )? ( ( (lv_keys_8_0= ruleKeys ) ) otherlv_9= ';' )? otherlv_10= '}' )
            {
            // InternalTpi.g:272:2: ( () otherlv_1= 'Info' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_type_4_0= ruleType ) ) otherlv_5= ';' ( ( (lv_alarms_6_0= ruleAlarms ) ) otherlv_7= ';' )? ( ( (lv_keys_8_0= ruleKeys ) ) otherlv_9= ';' )? otherlv_10= '}' )
            // InternalTpi.g:273:3: () otherlv_1= 'Info' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_type_4_0= ruleType ) ) otherlv_5= ';' ( ( (lv_alarms_6_0= ruleAlarms ) ) otherlv_7= ';' )? ( ( (lv_keys_8_0= ruleKeys ) ) otherlv_9= ';' )? otherlv_10= '}'
            {
            // InternalTpi.g:273:3: ()
            // InternalTpi.g:274:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInfoAccess().getInfoAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,13,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getInfoAccess().getInfoKeyword_1());
            		
            // InternalTpi.g:284:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalTpi.g:285:4: (lv_name_2_0= RULE_ID )
            {
            // InternalTpi.g:285:4: (lv_name_2_0= RULE_ID )
            // InternalTpi.g:286:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getInfoAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInfoRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_7); 

            			newLeafNode(otherlv_3, grammarAccess.getInfoAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalTpi.g:306:3: ( (lv_type_4_0= ruleType ) )
            // InternalTpi.g:307:4: (lv_type_4_0= ruleType )
            {
            // InternalTpi.g:307:4: (lv_type_4_0= ruleType )
            // InternalTpi.g:308:5: lv_type_4_0= ruleType
            {

            					newCompositeNode(grammarAccess.getInfoAccess().getTypeTypeParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_type_4_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInfoRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_4_0,
            						"fr.irisa.topoplan.infos.Tpi.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,15,FOLLOW_9); 

            			newLeafNode(otherlv_5, grammarAccess.getInfoAccess().getSemicolonKeyword_5());
            		
            // InternalTpi.g:329:3: ( ( (lv_alarms_6_0= ruleAlarms ) ) otherlv_7= ';' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==24) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalTpi.g:330:4: ( (lv_alarms_6_0= ruleAlarms ) ) otherlv_7= ';'
                    {
                    // InternalTpi.g:330:4: ( (lv_alarms_6_0= ruleAlarms ) )
                    // InternalTpi.g:331:5: (lv_alarms_6_0= ruleAlarms )
                    {
                    // InternalTpi.g:331:5: (lv_alarms_6_0= ruleAlarms )
                    // InternalTpi.g:332:6: lv_alarms_6_0= ruleAlarms
                    {

                    						newCompositeNode(grammarAccess.getInfoAccess().getAlarmsAlarmsParserRuleCall_6_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_alarms_6_0=ruleAlarms();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInfoRule());
                    						}
                    						set(
                    							current,
                    							"alarms",
                    							lv_alarms_6_0,
                    							"fr.irisa.topoplan.infos.Tpi.Alarms");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_7=(Token)match(input,15,FOLLOW_10); 

                    				newLeafNode(otherlv_7, grammarAccess.getInfoAccess().getSemicolonKeyword_6_1());
                    			

                    }
                    break;

            }

            // InternalTpi.g:354:3: ( ( (lv_keys_8_0= ruleKeys ) ) otherlv_9= ';' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==25) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalTpi.g:355:4: ( (lv_keys_8_0= ruleKeys ) ) otherlv_9= ';'
                    {
                    // InternalTpi.g:355:4: ( (lv_keys_8_0= ruleKeys ) )
                    // InternalTpi.g:356:5: (lv_keys_8_0= ruleKeys )
                    {
                    // InternalTpi.g:356:5: (lv_keys_8_0= ruleKeys )
                    // InternalTpi.g:357:6: lv_keys_8_0= ruleKeys
                    {

                    						newCompositeNode(grammarAccess.getInfoAccess().getKeysKeysParserRuleCall_7_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_keys_8_0=ruleKeys();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInfoRule());
                    						}
                    						set(
                    							current,
                    							"keys",
                    							lv_keys_8_0,
                    							"fr.irisa.topoplan.infos.Tpi.Keys");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_9=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_9, grammarAccess.getInfoAccess().getSemicolonKeyword_7_1());
                    			

                    }
                    break;

            }

            otherlv_10=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getInfoAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInfo"


    // $ANTLR start "entryRuleRoom"
    // InternalTpi.g:387:1: entryRuleRoom returns [EObject current=null] : iv_ruleRoom= ruleRoom EOF ;
    public final EObject entryRuleRoom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoom = null;


        try {
            // InternalTpi.g:387:45: (iv_ruleRoom= ruleRoom EOF )
            // InternalTpi.g:388:2: iv_ruleRoom= ruleRoom EOF
            {
             newCompositeNode(grammarAccess.getRoomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRoom=ruleRoom();

            state._fsp--;

             current =iv_ruleRoom; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoom"


    // $ANTLR start "ruleRoom"
    // InternalTpi.g:394:1: ruleRoom returns [EObject current=null] : ( () otherlv_1= 'Zone' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_ext_4_0= 'Ext' ) )? ( ( (lv_alarms_5_0= ruleAlarms ) ) otherlv_6= ';' )? otherlv_7= '}' ) ;
    public final EObject ruleRoom() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token lv_ext_4_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_alarms_5_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:400:2: ( ( () otherlv_1= 'Zone' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_ext_4_0= 'Ext' ) )? ( ( (lv_alarms_5_0= ruleAlarms ) ) otherlv_6= ';' )? otherlv_7= '}' ) )
            // InternalTpi.g:401:2: ( () otherlv_1= 'Zone' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_ext_4_0= 'Ext' ) )? ( ( (lv_alarms_5_0= ruleAlarms ) ) otherlv_6= ';' )? otherlv_7= '}' )
            {
            // InternalTpi.g:401:2: ( () otherlv_1= 'Zone' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_ext_4_0= 'Ext' ) )? ( ( (lv_alarms_5_0= ruleAlarms ) ) otherlv_6= ';' )? otherlv_7= '}' )
            // InternalTpi.g:402:3: () otherlv_1= 'Zone' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_ext_4_0= 'Ext' ) )? ( ( (lv_alarms_5_0= ruleAlarms ) ) otherlv_6= ';' )? otherlv_7= '}'
            {
            // InternalTpi.g:402:3: ()
            // InternalTpi.g:403:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRoomAccess().getRoomAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,17,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getRoomAccess().getZoneKeyword_1());
            		
            // InternalTpi.g:413:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalTpi.g:414:4: (lv_name_2_0= RULE_ID )
            {
            // InternalTpi.g:414:4: (lv_name_2_0= RULE_ID )
            // InternalTpi.g:415:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getRoomAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRoomRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_12); 

            			newLeafNode(otherlv_3, grammarAccess.getRoomAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalTpi.g:435:3: ( (lv_ext_4_0= 'Ext' ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==18) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalTpi.g:436:4: (lv_ext_4_0= 'Ext' )
                    {
                    // InternalTpi.g:436:4: (lv_ext_4_0= 'Ext' )
                    // InternalTpi.g:437:5: lv_ext_4_0= 'Ext'
                    {
                    lv_ext_4_0=(Token)match(input,18,FOLLOW_13); 

                    					newLeafNode(lv_ext_4_0, grammarAccess.getRoomAccess().getExtExtKeyword_4_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getRoomRule());
                    					}
                    					setWithLastConsumed(current, "ext", true, "Ext");
                    				

                    }


                    }
                    break;

            }

            // InternalTpi.g:449:3: ( ( (lv_alarms_5_0= ruleAlarms ) ) otherlv_6= ';' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==24) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalTpi.g:450:4: ( (lv_alarms_5_0= ruleAlarms ) ) otherlv_6= ';'
                    {
                    // InternalTpi.g:450:4: ( (lv_alarms_5_0= ruleAlarms ) )
                    // InternalTpi.g:451:5: (lv_alarms_5_0= ruleAlarms )
                    {
                    // InternalTpi.g:451:5: (lv_alarms_5_0= ruleAlarms )
                    // InternalTpi.g:452:6: lv_alarms_5_0= ruleAlarms
                    {

                    						newCompositeNode(grammarAccess.getRoomAccess().getAlarmsAlarmsParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_alarms_5_0=ruleAlarms();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRoomRule());
                    						}
                    						set(
                    							current,
                    							"alarms",
                    							lv_alarms_5_0,
                    							"fr.irisa.topoplan.infos.Tpi.Alarms");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_6=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_6, grammarAccess.getRoomAccess().getSemicolonKeyword_5_1());
                    			

                    }
                    break;

            }

            otherlv_7=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getRoomAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoom"


    // $ANTLR start "entryRuleAccess"
    // InternalTpi.g:482:1: entryRuleAccess returns [EObject current=null] : iv_ruleAccess= ruleAccess EOF ;
    public final EObject entryRuleAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAccess = null;


        try {
            // InternalTpi.g:482:47: (iv_ruleAccess= ruleAccess EOF )
            // InternalTpi.g:483:2: iv_ruleAccess= ruleAccess EOF
            {
             newCompositeNode(grammarAccess.getAccessRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAccess=ruleAccess();

            state._fsp--;

             current =iv_ruleAccess; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAccess"


    // $ANTLR start "ruleAccess"
    // InternalTpi.g:489:1: ruleAccess returns [EObject current=null] : ( () otherlv_1= 'Access' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' ) ;
    public final EObject ruleAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_alarms_4_0 = null;

        EObject lv_keys_6_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:495:2: ( ( () otherlv_1= 'Access' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' ) )
            // InternalTpi.g:496:2: ( () otherlv_1= 'Access' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' )
            {
            // InternalTpi.g:496:2: ( () otherlv_1= 'Access' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' )
            // InternalTpi.g:497:3: () otherlv_1= 'Access' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}'
            {
            // InternalTpi.g:497:3: ()
            // InternalTpi.g:498:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAccessAccess().getAccessAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,19,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getAccessAccess().getAccessKeyword_1());
            		
            // InternalTpi.g:508:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalTpi.g:509:4: (lv_name_2_0= RULE_ID )
            {
            // InternalTpi.g:509:4: (lv_name_2_0= RULE_ID )
            // InternalTpi.g:510:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getAccessAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAccessRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_9); 

            			newLeafNode(otherlv_3, grammarAccess.getAccessAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalTpi.g:530:3: ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==24) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalTpi.g:531:4: ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';'
                    {
                    // InternalTpi.g:531:4: ( (lv_alarms_4_0= ruleAlarms ) )
                    // InternalTpi.g:532:5: (lv_alarms_4_0= ruleAlarms )
                    {
                    // InternalTpi.g:532:5: (lv_alarms_4_0= ruleAlarms )
                    // InternalTpi.g:533:6: lv_alarms_4_0= ruleAlarms
                    {

                    						newCompositeNode(grammarAccess.getAccessAccess().getAlarmsAlarmsParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_alarms_4_0=ruleAlarms();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAccessRule());
                    						}
                    						set(
                    							current,
                    							"alarms",
                    							lv_alarms_4_0,
                    							"fr.irisa.topoplan.infos.Tpi.Alarms");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,15,FOLLOW_10); 

                    				newLeafNode(otherlv_5, grammarAccess.getAccessAccess().getSemicolonKeyword_4_1());
                    			

                    }
                    break;

            }

            // InternalTpi.g:555:3: ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==25) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalTpi.g:556:4: ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';'
                    {
                    // InternalTpi.g:556:4: ( (lv_keys_6_0= ruleKeys ) )
                    // InternalTpi.g:557:5: (lv_keys_6_0= ruleKeys )
                    {
                    // InternalTpi.g:557:5: (lv_keys_6_0= ruleKeys )
                    // InternalTpi.g:558:6: lv_keys_6_0= ruleKeys
                    {

                    						newCompositeNode(grammarAccess.getAccessAccess().getKeysKeysParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_keys_6_0=ruleKeys();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAccessRule());
                    						}
                    						set(
                    							current,
                    							"keys",
                    							lv_keys_6_0,
                    							"fr.irisa.topoplan.infos.Tpi.Keys");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_7=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_7, grammarAccess.getAccessAccess().getSemicolonKeyword_5_1());
                    			

                    }
                    break;

            }

            otherlv_8=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getAccessAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAccess"


    // $ANTLR start "entryRuleWindow"
    // InternalTpi.g:588:1: entryRuleWindow returns [EObject current=null] : iv_ruleWindow= ruleWindow EOF ;
    public final EObject entryRuleWindow() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWindow = null;


        try {
            // InternalTpi.g:588:47: (iv_ruleWindow= ruleWindow EOF )
            // InternalTpi.g:589:2: iv_ruleWindow= ruleWindow EOF
            {
             newCompositeNode(grammarAccess.getWindowRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWindow=ruleWindow();

            state._fsp--;

             current =iv_ruleWindow; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWindow"


    // $ANTLR start "ruleWindow"
    // InternalTpi.g:595:1: ruleWindow returns [EObject current=null] : ( () otherlv_1= 'Window' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' ) ;
    public final EObject ruleWindow() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_alarms_4_0 = null;

        EObject lv_keys_6_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:601:2: ( ( () otherlv_1= 'Window' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' ) )
            // InternalTpi.g:602:2: ( () otherlv_1= 'Window' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' )
            {
            // InternalTpi.g:602:2: ( () otherlv_1= 'Window' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' )
            // InternalTpi.g:603:3: () otherlv_1= 'Window' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}'
            {
            // InternalTpi.g:603:3: ()
            // InternalTpi.g:604:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getWindowAccess().getWindowAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,20,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getWindowAccess().getWindowKeyword_1());
            		
            // InternalTpi.g:614:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalTpi.g:615:4: (lv_name_2_0= RULE_ID )
            {
            // InternalTpi.g:615:4: (lv_name_2_0= RULE_ID )
            // InternalTpi.g:616:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getWindowAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWindowRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_9); 

            			newLeafNode(otherlv_3, grammarAccess.getWindowAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalTpi.g:636:3: ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==24) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalTpi.g:637:4: ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';'
                    {
                    // InternalTpi.g:637:4: ( (lv_alarms_4_0= ruleAlarms ) )
                    // InternalTpi.g:638:5: (lv_alarms_4_0= ruleAlarms )
                    {
                    // InternalTpi.g:638:5: (lv_alarms_4_0= ruleAlarms )
                    // InternalTpi.g:639:6: lv_alarms_4_0= ruleAlarms
                    {

                    						newCompositeNode(grammarAccess.getWindowAccess().getAlarmsAlarmsParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_alarms_4_0=ruleAlarms();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWindowRule());
                    						}
                    						set(
                    							current,
                    							"alarms",
                    							lv_alarms_4_0,
                    							"fr.irisa.topoplan.infos.Tpi.Alarms");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,15,FOLLOW_10); 

                    				newLeafNode(otherlv_5, grammarAccess.getWindowAccess().getSemicolonKeyword_4_1());
                    			

                    }
                    break;

            }

            // InternalTpi.g:661:3: ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==25) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalTpi.g:662:4: ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';'
                    {
                    // InternalTpi.g:662:4: ( (lv_keys_6_0= ruleKeys ) )
                    // InternalTpi.g:663:5: (lv_keys_6_0= ruleKeys )
                    {
                    // InternalTpi.g:663:5: (lv_keys_6_0= ruleKeys )
                    // InternalTpi.g:664:6: lv_keys_6_0= ruleKeys
                    {

                    						newCompositeNode(grammarAccess.getWindowAccess().getKeysKeysParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_keys_6_0=ruleKeys();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWindowRule());
                    						}
                    						set(
                    							current,
                    							"keys",
                    							lv_keys_6_0,
                    							"fr.irisa.topoplan.infos.Tpi.Keys");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_7=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_7, grammarAccess.getWindowAccess().getSemicolonKeyword_5_1());
                    			

                    }
                    break;

            }

            otherlv_8=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getWindowAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWindow"


    // $ANTLR start "entryRuleElevator"
    // InternalTpi.g:694:1: entryRuleElevator returns [EObject current=null] : iv_ruleElevator= ruleElevator EOF ;
    public final EObject entryRuleElevator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElevator = null;


        try {
            // InternalTpi.g:694:49: (iv_ruleElevator= ruleElevator EOF )
            // InternalTpi.g:695:2: iv_ruleElevator= ruleElevator EOF
            {
             newCompositeNode(grammarAccess.getElevatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleElevator=ruleElevator();

            state._fsp--;

             current =iv_ruleElevator; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElevator"


    // $ANTLR start "ruleElevator"
    // InternalTpi.g:701:1: ruleElevator returns [EObject current=null] : ( () otherlv_1= 'Elevator' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' ) ;
    public final EObject ruleElevator() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_alarms_4_0 = null;

        EObject lv_keys_6_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:707:2: ( ( () otherlv_1= 'Elevator' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' ) )
            // InternalTpi.g:708:2: ( () otherlv_1= 'Elevator' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' )
            {
            // InternalTpi.g:708:2: ( () otherlv_1= 'Elevator' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' )
            // InternalTpi.g:709:3: () otherlv_1= 'Elevator' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}'
            {
            // InternalTpi.g:709:3: ()
            // InternalTpi.g:710:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getElevatorAccess().getElevatorAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,21,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getElevatorAccess().getElevatorKeyword_1());
            		
            // InternalTpi.g:720:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalTpi.g:721:4: (lv_name_2_0= RULE_ID )
            {
            // InternalTpi.g:721:4: (lv_name_2_0= RULE_ID )
            // InternalTpi.g:722:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getElevatorAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getElevatorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_9); 

            			newLeafNode(otherlv_3, grammarAccess.getElevatorAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalTpi.g:742:3: ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==24) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalTpi.g:743:4: ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';'
                    {
                    // InternalTpi.g:743:4: ( (lv_alarms_4_0= ruleAlarms ) )
                    // InternalTpi.g:744:5: (lv_alarms_4_0= ruleAlarms )
                    {
                    // InternalTpi.g:744:5: (lv_alarms_4_0= ruleAlarms )
                    // InternalTpi.g:745:6: lv_alarms_4_0= ruleAlarms
                    {

                    						newCompositeNode(grammarAccess.getElevatorAccess().getAlarmsAlarmsParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_alarms_4_0=ruleAlarms();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getElevatorRule());
                    						}
                    						set(
                    							current,
                    							"alarms",
                    							lv_alarms_4_0,
                    							"fr.irisa.topoplan.infos.Tpi.Alarms");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,15,FOLLOW_10); 

                    				newLeafNode(otherlv_5, grammarAccess.getElevatorAccess().getSemicolonKeyword_4_1());
                    			

                    }
                    break;

            }

            // InternalTpi.g:767:3: ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==25) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalTpi.g:768:4: ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';'
                    {
                    // InternalTpi.g:768:4: ( (lv_keys_6_0= ruleKeys ) )
                    // InternalTpi.g:769:5: (lv_keys_6_0= ruleKeys )
                    {
                    // InternalTpi.g:769:5: (lv_keys_6_0= ruleKeys )
                    // InternalTpi.g:770:6: lv_keys_6_0= ruleKeys
                    {

                    						newCompositeNode(grammarAccess.getElevatorAccess().getKeysKeysParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_keys_6_0=ruleKeys();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getElevatorRule());
                    						}
                    						set(
                    							current,
                    							"keys",
                    							lv_keys_6_0,
                    							"fr.irisa.topoplan.infos.Tpi.Keys");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_7=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_7, grammarAccess.getElevatorAccess().getSemicolonKeyword_5_1());
                    			

                    }
                    break;

            }

            otherlv_8=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getElevatorAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElevator"


    // $ANTLR start "entryRuleOutside"
    // InternalTpi.g:800:1: entryRuleOutside returns [EObject current=null] : iv_ruleOutside= ruleOutside EOF ;
    public final EObject entryRuleOutside() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutside = null;


        try {
            // InternalTpi.g:800:48: (iv_ruleOutside= ruleOutside EOF )
            // InternalTpi.g:801:2: iv_ruleOutside= ruleOutside EOF
            {
             newCompositeNode(grammarAccess.getOutsideRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutside=ruleOutside();

            state._fsp--;

             current =iv_ruleOutside; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutside"


    // $ANTLR start "ruleOutside"
    // InternalTpi.g:807:1: ruleOutside returns [EObject current=null] : ( () otherlv_1= 'Outside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' ) ;
    public final EObject ruleOutside() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_alarms_4_0 = null;

        EObject lv_keys_6_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:813:2: ( ( () otherlv_1= 'Outside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' ) )
            // InternalTpi.g:814:2: ( () otherlv_1= 'Outside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' )
            {
            // InternalTpi.g:814:2: ( () otherlv_1= 'Outside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}' )
            // InternalTpi.g:815:3: () otherlv_1= 'Outside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )? ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )? otherlv_8= '}'
            {
            // InternalTpi.g:815:3: ()
            // InternalTpi.g:816:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getOutsideAccess().getOutsideAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,22,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getOutsideAccess().getOutsideKeyword_1());
            		
            // InternalTpi.g:826:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalTpi.g:827:4: (lv_name_2_0= RULE_ID )
            {
            // InternalTpi.g:827:4: (lv_name_2_0= RULE_ID )
            // InternalTpi.g:828:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getOutsideAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOutsideRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_9); 

            			newLeafNode(otherlv_3, grammarAccess.getOutsideAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalTpi.g:848:3: ( ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';' )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==24) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalTpi.g:849:4: ( (lv_alarms_4_0= ruleAlarms ) ) otherlv_5= ';'
                    {
                    // InternalTpi.g:849:4: ( (lv_alarms_4_0= ruleAlarms ) )
                    // InternalTpi.g:850:5: (lv_alarms_4_0= ruleAlarms )
                    {
                    // InternalTpi.g:850:5: (lv_alarms_4_0= ruleAlarms )
                    // InternalTpi.g:851:6: lv_alarms_4_0= ruleAlarms
                    {

                    						newCompositeNode(grammarAccess.getOutsideAccess().getAlarmsAlarmsParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_alarms_4_0=ruleAlarms();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOutsideRule());
                    						}
                    						set(
                    							current,
                    							"alarms",
                    							lv_alarms_4_0,
                    							"fr.irisa.topoplan.infos.Tpi.Alarms");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,15,FOLLOW_10); 

                    				newLeafNode(otherlv_5, grammarAccess.getOutsideAccess().getSemicolonKeyword_4_1());
                    			

                    }
                    break;

            }

            // InternalTpi.g:873:3: ( ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==25) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalTpi.g:874:4: ( (lv_keys_6_0= ruleKeys ) ) otherlv_7= ';'
                    {
                    // InternalTpi.g:874:4: ( (lv_keys_6_0= ruleKeys ) )
                    // InternalTpi.g:875:5: (lv_keys_6_0= ruleKeys )
                    {
                    // InternalTpi.g:875:5: (lv_keys_6_0= ruleKeys )
                    // InternalTpi.g:876:6: lv_keys_6_0= ruleKeys
                    {

                    						newCompositeNode(grammarAccess.getOutsideAccess().getKeysKeysParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_keys_6_0=ruleKeys();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOutsideRule());
                    						}
                    						set(
                    							current,
                    							"keys",
                    							lv_keys_6_0,
                    							"fr.irisa.topoplan.infos.Tpi.Keys");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_7=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_7, grammarAccess.getOutsideAccess().getSemicolonKeyword_5_1());
                    			

                    }
                    break;

            }

            otherlv_8=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getOutsideAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutside"


    // $ANTLR start "entryRuleInside"
    // InternalTpi.g:906:1: entryRuleInside returns [EObject current=null] : iv_ruleInside= ruleInside EOF ;
    public final EObject entryRuleInside() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInside = null;


        try {
            // InternalTpi.g:906:47: (iv_ruleInside= ruleInside EOF )
            // InternalTpi.g:907:2: iv_ruleInside= ruleInside EOF
            {
             newCompositeNode(grammarAccess.getInsideRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInside=ruleInside();

            state._fsp--;

             current =iv_ruleInside; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInside"


    // $ANTLR start "ruleInside"
    // InternalTpi.g:913:1: ruleInside returns [EObject current=null] : ( () otherlv_1= 'Inside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= '}' ) ;
    public final EObject ruleInside() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalTpi.g:919:2: ( ( () otherlv_1= 'Inside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= '}' ) )
            // InternalTpi.g:920:2: ( () otherlv_1= 'Inside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= '}' )
            {
            // InternalTpi.g:920:2: ( () otherlv_1= 'Inside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= '}' )
            // InternalTpi.g:921:3: () otherlv_1= 'Inside' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' otherlv_4= '}'
            {
            // InternalTpi.g:921:3: ()
            // InternalTpi.g:922:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInsideAccess().getInsideAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,23,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getInsideAccess().getInsideKeyword_1());
            		
            // InternalTpi.g:932:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalTpi.g:933:4: (lv_name_2_0= RULE_ID )
            {
            // InternalTpi.g:933:4: (lv_name_2_0= RULE_ID )
            // InternalTpi.g:934:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getInsideAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInsideRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_11); 

            			newLeafNode(otherlv_3, grammarAccess.getInsideAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getInsideAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInside"


    // $ANTLR start "entryRuleType"
    // InternalTpi.g:962:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalTpi.g:962:45: (iv_ruleType= ruleType EOF )
            // InternalTpi.g:963:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalTpi.g:969:1: ruleType returns [EObject current=null] : ( ( ( (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' ) ) ) ( (lv_v_1_0= ruleEString ) )? ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        Token lv_k_0_1=null;
        Token lv_k_0_2=null;
        Token lv_k_0_3=null;
        Token lv_k_0_4=null;
        Token lv_k_0_5=null;
        AntlrDatatypeRuleToken lv_v_1_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:975:2: ( ( ( ( (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' ) ) ) ( (lv_v_1_0= ruleEString ) )? ) )
            // InternalTpi.g:976:2: ( ( ( (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' ) ) ) ( (lv_v_1_0= ruleEString ) )? )
            {
            // InternalTpi.g:976:2: ( ( ( (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' ) ) ) ( (lv_v_1_0= ruleEString ) )? )
            // InternalTpi.g:977:3: ( ( (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' ) ) ) ( (lv_v_1_0= ruleEString ) )?
            {
            // InternalTpi.g:977:3: ( ( (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' ) ) )
            // InternalTpi.g:978:4: ( (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' ) )
            {
            // InternalTpi.g:978:4: ( (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' ) )
            // InternalTpi.g:979:5: (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' )
            {
            // InternalTpi.g:979:5: (lv_k_0_1= 'Zone' | lv_k_0_2= 'Access' | lv_k_0_3= 'Window' | lv_k_0_4= 'Elevator' | lv_k_0_5= 'Outside' )
            int alt14=5;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt14=1;
                }
                break;
            case 19:
                {
                alt14=2;
                }
                break;
            case 20:
                {
                alt14=3;
                }
                break;
            case 21:
                {
                alt14=4;
                }
                break;
            case 22:
                {
                alt14=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalTpi.g:980:6: lv_k_0_1= 'Zone'
                    {
                    lv_k_0_1=(Token)match(input,17,FOLLOW_14); 

                    						newLeafNode(lv_k_0_1, grammarAccess.getTypeAccess().getKZoneKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTypeRule());
                    						}
                    						setWithLastConsumed(current, "k", lv_k_0_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalTpi.g:991:6: lv_k_0_2= 'Access'
                    {
                    lv_k_0_2=(Token)match(input,19,FOLLOW_14); 

                    						newLeafNode(lv_k_0_2, grammarAccess.getTypeAccess().getKAccessKeyword_0_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTypeRule());
                    						}
                    						setWithLastConsumed(current, "k", lv_k_0_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalTpi.g:1002:6: lv_k_0_3= 'Window'
                    {
                    lv_k_0_3=(Token)match(input,20,FOLLOW_14); 

                    						newLeafNode(lv_k_0_3, grammarAccess.getTypeAccess().getKWindowKeyword_0_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTypeRule());
                    						}
                    						setWithLastConsumed(current, "k", lv_k_0_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalTpi.g:1013:6: lv_k_0_4= 'Elevator'
                    {
                    lv_k_0_4=(Token)match(input,21,FOLLOW_14); 

                    						newLeafNode(lv_k_0_4, grammarAccess.getTypeAccess().getKElevatorKeyword_0_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTypeRule());
                    						}
                    						setWithLastConsumed(current, "k", lv_k_0_4, null);
                    					

                    }
                    break;
                case 5 :
                    // InternalTpi.g:1024:6: lv_k_0_5= 'Outside'
                    {
                    lv_k_0_5=(Token)match(input,22,FOLLOW_14); 

                    						newLeafNode(lv_k_0_5, grammarAccess.getTypeAccess().getKOutsideKeyword_0_0_4());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTypeRule());
                    						}
                    						setWithLastConsumed(current, "k", lv_k_0_5, null);
                    					

                    }
                    break;

            }


            }


            }

            // InternalTpi.g:1037:3: ( (lv_v_1_0= ruleEString ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_ID) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalTpi.g:1038:4: (lv_v_1_0= ruleEString )
                    {
                    // InternalTpi.g:1038:4: (lv_v_1_0= ruleEString )
                    // InternalTpi.g:1039:5: lv_v_1_0= ruleEString
                    {

                    					newCompositeNode(grammarAccess.getTypeAccess().getVEStringParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_v_1_0=ruleEString();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getTypeRule());
                    					}
                    					set(
                    						current,
                    						"v",
                    						lv_v_1_0,
                    						"fr.irisa.topoplan.infos.Tpi.EString");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleAlarms"
    // InternalTpi.g:1060:1: entryRuleAlarms returns [EObject current=null] : iv_ruleAlarms= ruleAlarms EOF ;
    public final EObject entryRuleAlarms() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlarms = null;


        try {
            // InternalTpi.g:1060:47: (iv_ruleAlarms= ruleAlarms EOF )
            // InternalTpi.g:1061:2: iv_ruleAlarms= ruleAlarms EOF
            {
             newCompositeNode(grammarAccess.getAlarmsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAlarms=ruleAlarms();

            state._fsp--;

             current =iv_ruleAlarms; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlarms"


    // $ANTLR start "ruleAlarms"
    // InternalTpi.g:1067:1: ruleAlarms returns [EObject current=null] : ( () otherlv_1= 'Alarms' ( (lv_v_2_0= ruleEString ) )+ ) ;
    public final EObject ruleAlarms() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_v_2_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:1073:2: ( ( () otherlv_1= 'Alarms' ( (lv_v_2_0= ruleEString ) )+ ) )
            // InternalTpi.g:1074:2: ( () otherlv_1= 'Alarms' ( (lv_v_2_0= ruleEString ) )+ )
            {
            // InternalTpi.g:1074:2: ( () otherlv_1= 'Alarms' ( (lv_v_2_0= ruleEString ) )+ )
            // InternalTpi.g:1075:3: () otherlv_1= 'Alarms' ( (lv_v_2_0= ruleEString ) )+
            {
            // InternalTpi.g:1075:3: ()
            // InternalTpi.g:1076:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAlarmsAccess().getAlarmsAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,24,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getAlarmsAccess().getAlarmsKeyword_1());
            		
            // InternalTpi.g:1086:3: ( (lv_v_2_0= ruleEString ) )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalTpi.g:1087:4: (lv_v_2_0= ruleEString )
            	    {
            	    // InternalTpi.g:1087:4: (lv_v_2_0= ruleEString )
            	    // InternalTpi.g:1088:5: lv_v_2_0= ruleEString
            	    {

            	    					newCompositeNode(grammarAccess.getAlarmsAccess().getVEStringParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_14);
            	    lv_v_2_0=ruleEString();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAlarmsRule());
            	    					}
            	    					add(
            	    						current,
            	    						"v",
            	    						lv_v_2_0,
            	    						"fr.irisa.topoplan.infos.Tpi.EString");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlarms"


    // $ANTLR start "entryRuleKeys"
    // InternalTpi.g:1109:1: entryRuleKeys returns [EObject current=null] : iv_ruleKeys= ruleKeys EOF ;
    public final EObject entryRuleKeys() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleKeys = null;


        try {
            // InternalTpi.g:1109:45: (iv_ruleKeys= ruleKeys EOF )
            // InternalTpi.g:1110:2: iv_ruleKeys= ruleKeys EOF
            {
             newCompositeNode(grammarAccess.getKeysRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleKeys=ruleKeys();

            state._fsp--;

             current =iv_ruleKeys; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleKeys"


    // $ANTLR start "ruleKeys"
    // InternalTpi.g:1116:1: ruleKeys returns [EObject current=null] : ( () otherlv_1= 'Keys' ( (lv_k_2_0= 'key' ) )? ( (lv_v_3_0= ruleEString ) )* ) ;
    public final EObject ruleKeys() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_k_2_0=null;
        AntlrDatatypeRuleToken lv_v_3_0 = null;



        	enterRule();

        try {
            // InternalTpi.g:1122:2: ( ( () otherlv_1= 'Keys' ( (lv_k_2_0= 'key' ) )? ( (lv_v_3_0= ruleEString ) )* ) )
            // InternalTpi.g:1123:2: ( () otherlv_1= 'Keys' ( (lv_k_2_0= 'key' ) )? ( (lv_v_3_0= ruleEString ) )* )
            {
            // InternalTpi.g:1123:2: ( () otherlv_1= 'Keys' ( (lv_k_2_0= 'key' ) )? ( (lv_v_3_0= ruleEString ) )* )
            // InternalTpi.g:1124:3: () otherlv_1= 'Keys' ( (lv_k_2_0= 'key' ) )? ( (lv_v_3_0= ruleEString ) )*
            {
            // InternalTpi.g:1124:3: ()
            // InternalTpi.g:1125:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getKeysAccess().getKeysAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,25,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getKeysAccess().getKeysKeyword_1());
            		
            // InternalTpi.g:1135:3: ( (lv_k_2_0= 'key' ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==26) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalTpi.g:1136:4: (lv_k_2_0= 'key' )
                    {
                    // InternalTpi.g:1136:4: (lv_k_2_0= 'key' )
                    // InternalTpi.g:1137:5: lv_k_2_0= 'key'
                    {
                    lv_k_2_0=(Token)match(input,26,FOLLOW_14); 

                    					newLeafNode(lv_k_2_0, grammarAccess.getKeysAccess().getKKeyKeyword_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getKeysRule());
                    					}
                    					setWithLastConsumed(current, "k", true, "key");
                    				

                    }


                    }
                    break;

            }

            // InternalTpi.g:1149:3: ( (lv_v_3_0= ruleEString ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==RULE_ID) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalTpi.g:1150:4: (lv_v_3_0= ruleEString )
            	    {
            	    // InternalTpi.g:1150:4: (lv_v_3_0= ruleEString )
            	    // InternalTpi.g:1151:5: lv_v_3_0= ruleEString
            	    {

            	    					newCompositeNode(grammarAccess.getKeysAccess().getVEStringParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_14);
            	    lv_v_3_0=ruleEString();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getKeysRule());
            	    					}
            	    					add(
            	    						current,
            	    						"v",
            	    						lv_v_3_0,
            	    						"fr.irisa.topoplan.infos.Tpi.EString");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleKeys"


    // $ANTLR start "entryRuleEString"
    // InternalTpi.g:1172:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalTpi.g:1172:47: (iv_ruleEString= ruleEString EOF )
            // InternalTpi.g:1173:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalTpi.g:1179:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_ID_0= RULE_ID ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;


        	enterRule();

        try {
            // InternalTpi.g:1185:2: (this_ID_0= RULE_ID )
            // InternalTpi.g:1186:2: this_ID_0= RULE_ID
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            		current.merge(this_ID_0);
            	

            		newLeafNode(this_ID_0, grammarAccess.getEStringAccess().getIDTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000FA2002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x00000000007A0000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000003010000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000002010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000001050000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000001010000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000004000012L});

}