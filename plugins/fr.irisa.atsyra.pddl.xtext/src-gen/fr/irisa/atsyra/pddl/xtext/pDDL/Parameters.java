/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * generated by Xtext 2.25.0
 */
package fr.irisa.atsyra.pddl.xtext.pDDL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameters</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.Parameters#getParametersList <em>Parameters List</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage#getParameters()
 * @model
 * @generated
 */
public interface Parameters extends EObject
{
  /**
   * Returns the value of the '<em><b>Parameters List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parameters List</em>' containment reference.
   * @see #setParametersList(TypedVariablesList)
   * @see fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage#getParameters_ParametersList()
   * @model containment="true"
   * @generated
   */
  TypedVariablesList getParametersList();

  /**
   * Sets the value of the '{@link fr.irisa.atsyra.pddl.xtext.pDDL.Parameters#getParametersList <em>Parameters List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parameters List</em>' containment reference.
   * @see #getParametersList()
   * @generated
   */
  void setParametersList(TypedVariablesList value);

} // Parameters
