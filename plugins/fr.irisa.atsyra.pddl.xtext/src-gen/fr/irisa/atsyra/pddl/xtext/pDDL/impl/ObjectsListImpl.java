/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * generated by Xtext 2.25.0
 */
package fr.irisa.atsyra.pddl.xtext.pDDL.impl;

import fr.irisa.atsyra.pddl.xtext.pDDL.NewObjects;
import fr.irisa.atsyra.pddl.xtext.pDDL.ObjectsList;
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Objects List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.impl.ObjectsListImpl#getNewObj <em>New Obj</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObjectsListImpl extends MinimalEObjectImpl.Container implements ObjectsList
{
  /**
   * The cached value of the '{@link #getNewObj() <em>New Obj</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNewObj()
   * @generated
   * @ordered
   */
  protected EList<NewObjects> newObj;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ObjectsListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PDDLPackage.Literals.OBJECTS_LIST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<NewObjects> getNewObj()
  {
    if (newObj == null)
    {
      newObj = new EObjectContainmentEList<NewObjects>(NewObjects.class, this, PDDLPackage.OBJECTS_LIST__NEW_OBJ);
    }
    return newObj;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case PDDLPackage.OBJECTS_LIST__NEW_OBJ:
        return ((InternalEList<?>)getNewObj()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PDDLPackage.OBJECTS_LIST__NEW_OBJ:
        return getNewObj();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PDDLPackage.OBJECTS_LIST__NEW_OBJ:
        getNewObj().clear();
        getNewObj().addAll((Collection<? extends NewObjects>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.OBJECTS_LIST__NEW_OBJ:
        getNewObj().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.OBJECTS_LIST__NEW_OBJ:
        return newObj != null && !newObj.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ObjectsListImpl
