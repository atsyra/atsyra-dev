/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * generated by Xtext 2.25.0
 */
package fr.irisa.atsyra.pddl.xtext.pDDL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decl Predicat Or Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.DeclPredicatOrFunction#getPfname <em>Pfname</em>}</li>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.DeclPredicatOrFunction#getTypedVariablesList <em>Typed Variables List</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage#getDeclPredicatOrFunction()
 * @model
 * @generated
 */
public interface DeclPredicatOrFunction extends EObject
{
  /**
   * Returns the value of the '<em><b>Pfname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pfname</em>' attribute.
   * @see #setPfname(String)
   * @see fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage#getDeclPredicatOrFunction_Pfname()
   * @model
   * @generated
   */
  String getPfname();

  /**
   * Sets the value of the '{@link fr.irisa.atsyra.pddl.xtext.pDDL.DeclPredicatOrFunction#getPfname <em>Pfname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pfname</em>' attribute.
   * @see #getPfname()
   * @generated
   */
  void setPfname(String value);

  /**
   * Returns the value of the '<em><b>Typed Variables List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Typed Variables List</em>' containment reference.
   * @see #setTypedVariablesList(TypedVariablesList)
   * @see fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage#getDeclPredicatOrFunction_TypedVariablesList()
   * @model containment="true"
   * @generated
   */
  TypedVariablesList getTypedVariablesList();

  /**
   * Sets the value of the '{@link fr.irisa.atsyra.pddl.xtext.pDDL.DeclPredicatOrFunction#getTypedVariablesList <em>Typed Variables List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Typed Variables List</em>' containment reference.
   * @see #getTypedVariablesList()
   * @generated
   */
  void setTypedVariablesList(TypedVariablesList value);

} // DeclPredicatOrFunction
