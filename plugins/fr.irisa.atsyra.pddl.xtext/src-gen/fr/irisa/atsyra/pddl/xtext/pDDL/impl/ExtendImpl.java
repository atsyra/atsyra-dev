/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * generated by Xtext 2.25.0
 */
package fr.irisa.atsyra.pddl.xtext.pDDL.impl;

import fr.irisa.atsyra.pddl.xtext.pDDL.Extend;
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extend</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.impl.ExtendImpl#getExtendName <em>Extend Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExtendImpl extends MinimalEObjectImpl.Container implements Extend
{
  /**
   * The default value of the '{@link #getExtendName() <em>Extend Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtendName()
   * @generated
   * @ordered
   */
  protected static final String EXTEND_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getExtendName() <em>Extend Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtendName()
   * @generated
   * @ordered
   */
  protected String extendName = EXTEND_NAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExtendImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PDDLPackage.Literals.EXTEND;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getExtendName()
  {
    return extendName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setExtendName(String newExtendName)
  {
    String oldExtendName = extendName;
    extendName = newExtendName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PDDLPackage.EXTEND__EXTEND_NAME, oldExtendName, extendName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PDDLPackage.EXTEND__EXTEND_NAME:
        return getExtendName();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PDDLPackage.EXTEND__EXTEND_NAME:
        setExtendName((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.EXTEND__EXTEND_NAME:
        setExtendName(EXTEND_NAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.EXTEND__EXTEND_NAME:
        return EXTEND_NAME_EDEFAULT == null ? extendName != null : !EXTEND_NAME_EDEFAULT.equals(extendName);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (extendName: ");
    result.append(extendName);
    result.append(')');
    return result.toString();
  }

} //ExtendImpl
