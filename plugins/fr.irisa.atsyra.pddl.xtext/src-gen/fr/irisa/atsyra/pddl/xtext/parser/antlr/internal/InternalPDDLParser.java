/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.pddl.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.atsyra.pddl.xtext.services.PDDLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPDDLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_OP", "RULE_CP", "RULE_NAME", "RULE_IDWITHTWOPOINTBEFOREANDDASHES", "RULE_IDWITHQUESTIONMARKBEFORE", "RULE_LETTER", "RULE_ANY_CHAR", "RULE_SL_COMMENT", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'define'", "'domain'", "'problem'", "':extends'", "':requirements'", "':types'", "'-'", "':predicates'", "'either'", "':functions'", "':action'", "':parameters'", "':precondition'", "':effect'", "'='", "'not'", "'and'", "'or'", "'imply'", "'forall'", "'exists'", "'when'", "':axiom'", "':vars'", "':context'", "':implies'", "':domain'", "':objects'", "':init'", "':goal'"
    };
    public static final int RULE_CP=5;
    public static final int T__19=19;
    public static final int T__18=18;
    public static final int RULE_OP=4;
    public static final int RULE_IDWITHTWOPOINTBEFOREANDDASHES=7;
    public static final int RULE_ID=12;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=13;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=15;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_NAME=6;
    public static final int RULE_STRING=14;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int RULE_ANY_CHAR=10;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=16;
    public static final int RULE_ANY_OTHER=17;
    public static final int RULE_LETTER=9;
    public static final int RULE_IDWITHQUESTIONMARKBEFORE=8;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalPDDLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPDDLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPDDLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPDDL.g"; }



     	private PDDLGrammarAccess grammarAccess;

        public InternalPDDLParser(TokenStream input, PDDLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "PDDLModel";
       	}

       	@Override
       	protected PDDLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRulePDDLModel"
    // InternalPDDL.g:64:1: entryRulePDDLModel returns [EObject current=null] : iv_rulePDDLModel= rulePDDLModel EOF ;
    public final EObject entryRulePDDLModel() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePDDLModel = null;


        try {
            // InternalPDDL.g:64:50: (iv_rulePDDLModel= rulePDDLModel EOF )
            // InternalPDDL.g:65:2: iv_rulePDDLModel= rulePDDLModel EOF
            {
             newCompositeNode(grammarAccess.getPDDLModelRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePDDLModel=rulePDDLModel();

            state._fsp--;

             current =iv_rulePDDLModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePDDLModel"


    // $ANTLR start "rulePDDLModel"
    // InternalPDDL.g:71:1: rulePDDLModel returns [EObject current=null] : ( () (this_OP_1= RULE_OP otherlv_2= 'define' ( (lv_file_3_0= rulePDDLFile ) ) this_CP_4= RULE_CP )? ) ;
    public final EObject rulePDDLModel() throws RecognitionException {
        EObject current = null;

        Token this_OP_1=null;
        Token otherlv_2=null;
        Token this_CP_4=null;
        EObject lv_file_3_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:77:2: ( ( () (this_OP_1= RULE_OP otherlv_2= 'define' ( (lv_file_3_0= rulePDDLFile ) ) this_CP_4= RULE_CP )? ) )
            // InternalPDDL.g:78:2: ( () (this_OP_1= RULE_OP otherlv_2= 'define' ( (lv_file_3_0= rulePDDLFile ) ) this_CP_4= RULE_CP )? )
            {
            // InternalPDDL.g:78:2: ( () (this_OP_1= RULE_OP otherlv_2= 'define' ( (lv_file_3_0= rulePDDLFile ) ) this_CP_4= RULE_CP )? )
            // InternalPDDL.g:79:3: () (this_OP_1= RULE_OP otherlv_2= 'define' ( (lv_file_3_0= rulePDDLFile ) ) this_CP_4= RULE_CP )?
            {
            // InternalPDDL.g:79:3: ()
            // InternalPDDL.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPDDLModelAccess().getPDDLModelAction_0(),
            					current);
            			

            }

            // InternalPDDL.g:86:3: (this_OP_1= RULE_OP otherlv_2= 'define' ( (lv_file_3_0= rulePDDLFile ) ) this_CP_4= RULE_CP )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_OP) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalPDDL.g:87:4: this_OP_1= RULE_OP otherlv_2= 'define' ( (lv_file_3_0= rulePDDLFile ) ) this_CP_4= RULE_CP
                    {
                    this_OP_1=(Token)match(input,RULE_OP,FOLLOW_3); 

                    				newLeafNode(this_OP_1, grammarAccess.getPDDLModelAccess().getOPTerminalRuleCall_1_0());
                    			
                    otherlv_2=(Token)match(input,18,FOLLOW_4); 

                    				newLeafNode(otherlv_2, grammarAccess.getPDDLModelAccess().getDefineKeyword_1_1());
                    			
                    // InternalPDDL.g:95:4: ( (lv_file_3_0= rulePDDLFile ) )
                    // InternalPDDL.g:96:5: (lv_file_3_0= rulePDDLFile )
                    {
                    // InternalPDDL.g:96:5: (lv_file_3_0= rulePDDLFile )
                    // InternalPDDL.g:97:6: lv_file_3_0= rulePDDLFile
                    {

                    						newCompositeNode(grammarAccess.getPDDLModelAccess().getFilePDDLFileParserRuleCall_1_2_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_file_3_0=rulePDDLFile();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPDDLModelRule());
                    						}
                    						set(
                    							current,
                    							"file",
                    							lv_file_3_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.PDDLFile");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_4=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_4, grammarAccess.getPDDLModelAccess().getCPTerminalRuleCall_1_3());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePDDLModel"


    // $ANTLR start "entryRulePDDLFile"
    // InternalPDDL.g:123:1: entryRulePDDLFile returns [EObject current=null] : iv_rulePDDLFile= rulePDDLFile EOF ;
    public final EObject entryRulePDDLFile() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePDDLFile = null;


        try {
            // InternalPDDL.g:123:49: (iv_rulePDDLFile= rulePDDLFile EOF )
            // InternalPDDL.g:124:2: iv_rulePDDLFile= rulePDDLFile EOF
            {
             newCompositeNode(grammarAccess.getPDDLFileRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePDDLFile=rulePDDLFile();

            state._fsp--;

             current =iv_rulePDDLFile; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePDDLFile"


    // $ANTLR start "rulePDDLFile"
    // InternalPDDL.g:130:1: rulePDDLFile returns [EObject current=null] : ( (this_OP_0= RULE_OP otherlv_1= 'domain' ( (lv_nameD_2_0= RULE_NAME ) ) this_CP_3= RULE_CP ( (lv_contentD_4_0= ruleDomain ) ) ) | (this_OP_5= RULE_OP otherlv_6= 'problem' ( (lv_nameP_7_0= RULE_NAME ) ) this_CP_8= RULE_CP ( (lv_contentP_9_0= ruleProblem ) ) ) ) ;
    public final EObject rulePDDLFile() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token lv_nameD_2_0=null;
        Token this_CP_3=null;
        Token this_OP_5=null;
        Token otherlv_6=null;
        Token lv_nameP_7_0=null;
        Token this_CP_8=null;
        EObject lv_contentD_4_0 = null;

        EObject lv_contentP_9_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:136:2: ( ( (this_OP_0= RULE_OP otherlv_1= 'domain' ( (lv_nameD_2_0= RULE_NAME ) ) this_CP_3= RULE_CP ( (lv_contentD_4_0= ruleDomain ) ) ) | (this_OP_5= RULE_OP otherlv_6= 'problem' ( (lv_nameP_7_0= RULE_NAME ) ) this_CP_8= RULE_CP ( (lv_contentP_9_0= ruleProblem ) ) ) ) )
            // InternalPDDL.g:137:2: ( (this_OP_0= RULE_OP otherlv_1= 'domain' ( (lv_nameD_2_0= RULE_NAME ) ) this_CP_3= RULE_CP ( (lv_contentD_4_0= ruleDomain ) ) ) | (this_OP_5= RULE_OP otherlv_6= 'problem' ( (lv_nameP_7_0= RULE_NAME ) ) this_CP_8= RULE_CP ( (lv_contentP_9_0= ruleProblem ) ) ) )
            {
            // InternalPDDL.g:137:2: ( (this_OP_0= RULE_OP otherlv_1= 'domain' ( (lv_nameD_2_0= RULE_NAME ) ) this_CP_3= RULE_CP ( (lv_contentD_4_0= ruleDomain ) ) ) | (this_OP_5= RULE_OP otherlv_6= 'problem' ( (lv_nameP_7_0= RULE_NAME ) ) this_CP_8= RULE_CP ( (lv_contentP_9_0= ruleProblem ) ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_OP) ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1==20) ) {
                    alt2=2;
                }
                else if ( (LA2_1==19) ) {
                    alt2=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalPDDL.g:138:3: (this_OP_0= RULE_OP otherlv_1= 'domain' ( (lv_nameD_2_0= RULE_NAME ) ) this_CP_3= RULE_CP ( (lv_contentD_4_0= ruleDomain ) ) )
                    {
                    // InternalPDDL.g:138:3: (this_OP_0= RULE_OP otherlv_1= 'domain' ( (lv_nameD_2_0= RULE_NAME ) ) this_CP_3= RULE_CP ( (lv_contentD_4_0= ruleDomain ) ) )
                    // InternalPDDL.g:139:4: this_OP_0= RULE_OP otherlv_1= 'domain' ( (lv_nameD_2_0= RULE_NAME ) ) this_CP_3= RULE_CP ( (lv_contentD_4_0= ruleDomain ) )
                    {
                    this_OP_0=(Token)match(input,RULE_OP,FOLLOW_6); 

                    				newLeafNode(this_OP_0, grammarAccess.getPDDLFileAccess().getOPTerminalRuleCall_0_0());
                    			
                    otherlv_1=(Token)match(input,19,FOLLOW_7); 

                    				newLeafNode(otherlv_1, grammarAccess.getPDDLFileAccess().getDomainKeyword_0_1());
                    			
                    // InternalPDDL.g:147:4: ( (lv_nameD_2_0= RULE_NAME ) )
                    // InternalPDDL.g:148:5: (lv_nameD_2_0= RULE_NAME )
                    {
                    // InternalPDDL.g:148:5: (lv_nameD_2_0= RULE_NAME )
                    // InternalPDDL.g:149:6: lv_nameD_2_0= RULE_NAME
                    {
                    lv_nameD_2_0=(Token)match(input,RULE_NAME,FOLLOW_5); 

                    						newLeafNode(lv_nameD_2_0, grammarAccess.getPDDLFileAccess().getNameDNAMETerminalRuleCall_0_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPDDLFileRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"nameD",
                    							lv_nameD_2_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
                    					

                    }


                    }

                    this_CP_3=(Token)match(input,RULE_CP,FOLLOW_4); 

                    				newLeafNode(this_CP_3, grammarAccess.getPDDLFileAccess().getCPTerminalRuleCall_0_3());
                    			
                    // InternalPDDL.g:169:4: ( (lv_contentD_4_0= ruleDomain ) )
                    // InternalPDDL.g:170:5: (lv_contentD_4_0= ruleDomain )
                    {
                    // InternalPDDL.g:170:5: (lv_contentD_4_0= ruleDomain )
                    // InternalPDDL.g:171:6: lv_contentD_4_0= ruleDomain
                    {

                    						newCompositeNode(grammarAccess.getPDDLFileAccess().getContentDDomainParserRuleCall_0_4_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_contentD_4_0=ruleDomain();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPDDLFileRule());
                    						}
                    						set(
                    							current,
                    							"contentD",
                    							lv_contentD_4_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.Domain");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:190:3: (this_OP_5= RULE_OP otherlv_6= 'problem' ( (lv_nameP_7_0= RULE_NAME ) ) this_CP_8= RULE_CP ( (lv_contentP_9_0= ruleProblem ) ) )
                    {
                    // InternalPDDL.g:190:3: (this_OP_5= RULE_OP otherlv_6= 'problem' ( (lv_nameP_7_0= RULE_NAME ) ) this_CP_8= RULE_CP ( (lv_contentP_9_0= ruleProblem ) ) )
                    // InternalPDDL.g:191:4: this_OP_5= RULE_OP otherlv_6= 'problem' ( (lv_nameP_7_0= RULE_NAME ) ) this_CP_8= RULE_CP ( (lv_contentP_9_0= ruleProblem ) )
                    {
                    this_OP_5=(Token)match(input,RULE_OP,FOLLOW_8); 

                    				newLeafNode(this_OP_5, grammarAccess.getPDDLFileAccess().getOPTerminalRuleCall_1_0());
                    			
                    otherlv_6=(Token)match(input,20,FOLLOW_7); 

                    				newLeafNode(otherlv_6, grammarAccess.getPDDLFileAccess().getProblemKeyword_1_1());
                    			
                    // InternalPDDL.g:199:4: ( (lv_nameP_7_0= RULE_NAME ) )
                    // InternalPDDL.g:200:5: (lv_nameP_7_0= RULE_NAME )
                    {
                    // InternalPDDL.g:200:5: (lv_nameP_7_0= RULE_NAME )
                    // InternalPDDL.g:201:6: lv_nameP_7_0= RULE_NAME
                    {
                    lv_nameP_7_0=(Token)match(input,RULE_NAME,FOLLOW_5); 

                    						newLeafNode(lv_nameP_7_0, grammarAccess.getPDDLFileAccess().getNamePNAMETerminalRuleCall_1_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPDDLFileRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"nameP",
                    							lv_nameP_7_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
                    					

                    }


                    }

                    this_CP_8=(Token)match(input,RULE_CP,FOLLOW_4); 

                    				newLeafNode(this_CP_8, grammarAccess.getPDDLFileAccess().getCPTerminalRuleCall_1_3());
                    			
                    // InternalPDDL.g:221:4: ( (lv_contentP_9_0= ruleProblem ) )
                    // InternalPDDL.g:222:5: (lv_contentP_9_0= ruleProblem )
                    {
                    // InternalPDDL.g:222:5: (lv_contentP_9_0= ruleProblem )
                    // InternalPDDL.g:223:6: lv_contentP_9_0= ruleProblem
                    {

                    						newCompositeNode(grammarAccess.getPDDLFileAccess().getContentPProblemParserRuleCall_1_4_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_contentP_9_0=ruleProblem();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPDDLFileRule());
                    						}
                    						set(
                    							current,
                    							"contentP",
                    							lv_contentP_9_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.Problem");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePDDLFile"


    // $ANTLR start "entryRuleDomain"
    // InternalPDDL.g:245:1: entryRuleDomain returns [EObject current=null] : iv_ruleDomain= ruleDomain EOF ;
    public final EObject entryRuleDomain() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomain = null;


        try {
            // InternalPDDL.g:245:47: (iv_ruleDomain= ruleDomain EOF )
            // InternalPDDL.g:246:2: iv_ruleDomain= ruleDomain EOF
            {
             newCompositeNode(grammarAccess.getDomainRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomain=ruleDomain();

            state._fsp--;

             current =iv_ruleDomain; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomain"


    // $ANTLR start "ruleDomain"
    // InternalPDDL.g:252:1: ruleDomain returns [EObject current=null] : ( () ( (lv_extends_1_0= ruleExtends ) )? ( (lv_requirements_2_0= ruleRequirements ) )? ( (lv_types_3_0= ruleTypes ) )? ( (lv_predicates_4_0= rulePredicates ) )? ( (lv_functions_5_0= ruleFunctions ) )? ( ( (lv_actions_6_0= rulePDDLAction ) ) | ( (lv_axioms_7_0= ruleAxiom ) ) )* ) ;
    public final EObject ruleDomain() throws RecognitionException {
        EObject current = null;

        EObject lv_extends_1_0 = null;

        EObject lv_requirements_2_0 = null;

        EObject lv_types_3_0 = null;

        EObject lv_predicates_4_0 = null;

        EObject lv_functions_5_0 = null;

        EObject lv_actions_6_0 = null;

        EObject lv_axioms_7_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:258:2: ( ( () ( (lv_extends_1_0= ruleExtends ) )? ( (lv_requirements_2_0= ruleRequirements ) )? ( (lv_types_3_0= ruleTypes ) )? ( (lv_predicates_4_0= rulePredicates ) )? ( (lv_functions_5_0= ruleFunctions ) )? ( ( (lv_actions_6_0= rulePDDLAction ) ) | ( (lv_axioms_7_0= ruleAxiom ) ) )* ) )
            // InternalPDDL.g:259:2: ( () ( (lv_extends_1_0= ruleExtends ) )? ( (lv_requirements_2_0= ruleRequirements ) )? ( (lv_types_3_0= ruleTypes ) )? ( (lv_predicates_4_0= rulePredicates ) )? ( (lv_functions_5_0= ruleFunctions ) )? ( ( (lv_actions_6_0= rulePDDLAction ) ) | ( (lv_axioms_7_0= ruleAxiom ) ) )* )
            {
            // InternalPDDL.g:259:2: ( () ( (lv_extends_1_0= ruleExtends ) )? ( (lv_requirements_2_0= ruleRequirements ) )? ( (lv_types_3_0= ruleTypes ) )? ( (lv_predicates_4_0= rulePredicates ) )? ( (lv_functions_5_0= ruleFunctions ) )? ( ( (lv_actions_6_0= rulePDDLAction ) ) | ( (lv_axioms_7_0= ruleAxiom ) ) )* )
            // InternalPDDL.g:260:3: () ( (lv_extends_1_0= ruleExtends ) )? ( (lv_requirements_2_0= ruleRequirements ) )? ( (lv_types_3_0= ruleTypes ) )? ( (lv_predicates_4_0= rulePredicates ) )? ( (lv_functions_5_0= ruleFunctions ) )? ( ( (lv_actions_6_0= rulePDDLAction ) ) | ( (lv_axioms_7_0= ruleAxiom ) ) )*
            {
            // InternalPDDL.g:260:3: ()
            // InternalPDDL.g:261:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDomainAccess().getDomainAction_0(),
            					current);
            			

            }

            // InternalPDDL.g:267:3: ( (lv_extends_1_0= ruleExtends ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_OP) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==21) ) {
                    alt3=1;
                }
            }
            switch (alt3) {
                case 1 :
                    // InternalPDDL.g:268:4: (lv_extends_1_0= ruleExtends )
                    {
                    // InternalPDDL.g:268:4: (lv_extends_1_0= ruleExtends )
                    // InternalPDDL.g:269:5: lv_extends_1_0= ruleExtends
                    {

                    					newCompositeNode(grammarAccess.getDomainAccess().getExtendsExtendsParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_extends_1_0=ruleExtends();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDomainRule());
                    					}
                    					set(
                    						current,
                    						"extends",
                    						lv_extends_1_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Extends");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPDDL.g:286:3: ( (lv_requirements_2_0= ruleRequirements ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_OP) ) {
                int LA4_1 = input.LA(2);

                if ( (LA4_1==22) ) {
                    alt4=1;
                }
            }
            switch (alt4) {
                case 1 :
                    // InternalPDDL.g:287:4: (lv_requirements_2_0= ruleRequirements )
                    {
                    // InternalPDDL.g:287:4: (lv_requirements_2_0= ruleRequirements )
                    // InternalPDDL.g:288:5: lv_requirements_2_0= ruleRequirements
                    {

                    					newCompositeNode(grammarAccess.getDomainAccess().getRequirementsRequirementsParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_requirements_2_0=ruleRequirements();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDomainRule());
                    					}
                    					set(
                    						current,
                    						"requirements",
                    						lv_requirements_2_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Requirements");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPDDL.g:305:3: ( (lv_types_3_0= ruleTypes ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_OP) ) {
                int LA5_1 = input.LA(2);

                if ( (LA5_1==23) ) {
                    alt5=1;
                }
            }
            switch (alt5) {
                case 1 :
                    // InternalPDDL.g:306:4: (lv_types_3_0= ruleTypes )
                    {
                    // InternalPDDL.g:306:4: (lv_types_3_0= ruleTypes )
                    // InternalPDDL.g:307:5: lv_types_3_0= ruleTypes
                    {

                    					newCompositeNode(grammarAccess.getDomainAccess().getTypesTypesParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_types_3_0=ruleTypes();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDomainRule());
                    					}
                    					set(
                    						current,
                    						"types",
                    						lv_types_3_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Types");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPDDL.g:324:3: ( (lv_predicates_4_0= rulePredicates ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_OP) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==25) ) {
                    alt6=1;
                }
            }
            switch (alt6) {
                case 1 :
                    // InternalPDDL.g:325:4: (lv_predicates_4_0= rulePredicates )
                    {
                    // InternalPDDL.g:325:4: (lv_predicates_4_0= rulePredicates )
                    // InternalPDDL.g:326:5: lv_predicates_4_0= rulePredicates
                    {

                    					newCompositeNode(grammarAccess.getDomainAccess().getPredicatesPredicatesParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_predicates_4_0=rulePredicates();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDomainRule());
                    					}
                    					set(
                    						current,
                    						"predicates",
                    						lv_predicates_4_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Predicates");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPDDL.g:343:3: ( (lv_functions_5_0= ruleFunctions ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_OP) ) {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==27) ) {
                    alt7=1;
                }
            }
            switch (alt7) {
                case 1 :
                    // InternalPDDL.g:344:4: (lv_functions_5_0= ruleFunctions )
                    {
                    // InternalPDDL.g:344:4: (lv_functions_5_0= ruleFunctions )
                    // InternalPDDL.g:345:5: lv_functions_5_0= ruleFunctions
                    {

                    					newCompositeNode(grammarAccess.getDomainAccess().getFunctionsFunctionsParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_functions_5_0=ruleFunctions();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDomainRule());
                    					}
                    					set(
                    						current,
                    						"functions",
                    						lv_functions_5_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Functions");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPDDL.g:362:3: ( ( (lv_actions_6_0= rulePDDLAction ) ) | ( (lv_axioms_7_0= ruleAxiom ) ) )*
            loop8:
            do {
                int alt8=3;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_OP) ) {
                    int LA8_2 = input.LA(2);

                    if ( (LA8_2==40) ) {
                        alt8=2;
                    }
                    else if ( (LA8_2==28) ) {
                        alt8=1;
                    }


                }


                switch (alt8) {
            	case 1 :
            	    // InternalPDDL.g:363:4: ( (lv_actions_6_0= rulePDDLAction ) )
            	    {
            	    // InternalPDDL.g:363:4: ( (lv_actions_6_0= rulePDDLAction ) )
            	    // InternalPDDL.g:364:5: (lv_actions_6_0= rulePDDLAction )
            	    {
            	    // InternalPDDL.g:364:5: (lv_actions_6_0= rulePDDLAction )
            	    // InternalPDDL.g:365:6: lv_actions_6_0= rulePDDLAction
            	    {

            	    						newCompositeNode(grammarAccess.getDomainAccess().getActionsPDDLActionParserRuleCall_6_0_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_actions_6_0=rulePDDLAction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getDomainRule());
            	    						}
            	    						add(
            	    							current,
            	    							"actions",
            	    							lv_actions_6_0,
            	    							"fr.irisa.atsyra.pddl.xtext.PDDL.PDDLAction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPDDL.g:383:4: ( (lv_axioms_7_0= ruleAxiom ) )
            	    {
            	    // InternalPDDL.g:383:4: ( (lv_axioms_7_0= ruleAxiom ) )
            	    // InternalPDDL.g:384:5: (lv_axioms_7_0= ruleAxiom )
            	    {
            	    // InternalPDDL.g:384:5: (lv_axioms_7_0= ruleAxiom )
            	    // InternalPDDL.g:385:6: lv_axioms_7_0= ruleAxiom
            	    {

            	    						newCompositeNode(grammarAccess.getDomainAccess().getAxiomsAxiomParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_axioms_7_0=ruleAxiom();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getDomainRule());
            	    						}
            	    						add(
            	    							current,
            	    							"axioms",
            	    							lv_axioms_7_0,
            	    							"fr.irisa.atsyra.pddl.xtext.PDDL.Axiom");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomain"


    // $ANTLR start "entryRuleExtends"
    // InternalPDDL.g:407:1: entryRuleExtends returns [EObject current=null] : iv_ruleExtends= ruleExtends EOF ;
    public final EObject entryRuleExtends() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExtends = null;


        try {
            // InternalPDDL.g:407:48: (iv_ruleExtends= ruleExtends EOF )
            // InternalPDDL.g:408:2: iv_ruleExtends= ruleExtends EOF
            {
             newCompositeNode(grammarAccess.getExtendsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExtends=ruleExtends();

            state._fsp--;

             current =iv_ruleExtends; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtends"


    // $ANTLR start "ruleExtends"
    // InternalPDDL.g:414:1: ruleExtends returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':extends' ( (lv_eList_2_0= ruleExtendsList ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleExtends() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token this_CP_3=null;
        EObject lv_eList_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:420:2: ( (this_OP_0= RULE_OP otherlv_1= ':extends' ( (lv_eList_2_0= ruleExtendsList ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:421:2: (this_OP_0= RULE_OP otherlv_1= ':extends' ( (lv_eList_2_0= ruleExtendsList ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:421:2: (this_OP_0= RULE_OP otherlv_1= ':extends' ( (lv_eList_2_0= ruleExtendsList ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:422:3: this_OP_0= RULE_OP otherlv_1= ':extends' ( (lv_eList_2_0= ruleExtendsList ) ) this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_10); 

            			newLeafNode(this_OP_0, grammarAccess.getExtendsAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,21,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getExtendsAccess().getExtendsKeyword_1());
            		
            // InternalPDDL.g:430:3: ( (lv_eList_2_0= ruleExtendsList ) )
            // InternalPDDL.g:431:4: (lv_eList_2_0= ruleExtendsList )
            {
            // InternalPDDL.g:431:4: (lv_eList_2_0= ruleExtendsList )
            // InternalPDDL.g:432:5: lv_eList_2_0= ruleExtendsList
            {

            					newCompositeNode(grammarAccess.getExtendsAccess().getEListExtendsListParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_eList_2_0=ruleExtendsList();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getExtendsRule());
            					}
            					set(
            						current,
            						"eList",
            						lv_eList_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.ExtendsList");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getExtendsAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtends"


    // $ANTLR start "entryRuleExtendsList"
    // InternalPDDL.g:457:1: entryRuleExtendsList returns [EObject current=null] : iv_ruleExtendsList= ruleExtendsList EOF ;
    public final EObject entryRuleExtendsList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExtendsList = null;


        try {
            // InternalPDDL.g:457:52: (iv_ruleExtendsList= ruleExtendsList EOF )
            // InternalPDDL.g:458:2: iv_ruleExtendsList= ruleExtendsList EOF
            {
             newCompositeNode(grammarAccess.getExtendsListRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExtendsList=ruleExtendsList();

            state._fsp--;

             current =iv_ruleExtendsList; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtendsList"


    // $ANTLR start "ruleExtendsList"
    // InternalPDDL.g:464:1: ruleExtendsList returns [EObject current=null] : ( (lv_eElem_0_0= ruleExtend ) )+ ;
    public final EObject ruleExtendsList() throws RecognitionException {
        EObject current = null;

        EObject lv_eElem_0_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:470:2: ( ( (lv_eElem_0_0= ruleExtend ) )+ )
            // InternalPDDL.g:471:2: ( (lv_eElem_0_0= ruleExtend ) )+
            {
            // InternalPDDL.g:471:2: ( (lv_eElem_0_0= ruleExtend ) )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_NAME) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalPDDL.g:472:3: (lv_eElem_0_0= ruleExtend )
            	    {
            	    // InternalPDDL.g:472:3: (lv_eElem_0_0= ruleExtend )
            	    // InternalPDDL.g:473:4: lv_eElem_0_0= ruleExtend
            	    {

            	    				newCompositeNode(grammarAccess.getExtendsListAccess().getEElemExtendParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_11);
            	    lv_eElem_0_0=ruleExtend();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getExtendsListRule());
            	    				}
            	    				add(
            	    					current,
            	    					"eElem",
            	    					lv_eElem_0_0,
            	    					"fr.irisa.atsyra.pddl.xtext.PDDL.Extend");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtendsList"


    // $ANTLR start "entryRuleExtend"
    // InternalPDDL.g:493:1: entryRuleExtend returns [EObject current=null] : iv_ruleExtend= ruleExtend EOF ;
    public final EObject entryRuleExtend() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExtend = null;


        try {
            // InternalPDDL.g:493:47: (iv_ruleExtend= ruleExtend EOF )
            // InternalPDDL.g:494:2: iv_ruleExtend= ruleExtend EOF
            {
             newCompositeNode(grammarAccess.getExtendRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExtend=ruleExtend();

            state._fsp--;

             current =iv_ruleExtend; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtend"


    // $ANTLR start "ruleExtend"
    // InternalPDDL.g:500:1: ruleExtend returns [EObject current=null] : ( (lv_extendName_0_0= RULE_NAME ) ) ;
    public final EObject ruleExtend() throws RecognitionException {
        EObject current = null;

        Token lv_extendName_0_0=null;


        	enterRule();

        try {
            // InternalPDDL.g:506:2: ( ( (lv_extendName_0_0= RULE_NAME ) ) )
            // InternalPDDL.g:507:2: ( (lv_extendName_0_0= RULE_NAME ) )
            {
            // InternalPDDL.g:507:2: ( (lv_extendName_0_0= RULE_NAME ) )
            // InternalPDDL.g:508:3: (lv_extendName_0_0= RULE_NAME )
            {
            // InternalPDDL.g:508:3: (lv_extendName_0_0= RULE_NAME )
            // InternalPDDL.g:509:4: lv_extendName_0_0= RULE_NAME
            {
            lv_extendName_0_0=(Token)match(input,RULE_NAME,FOLLOW_2); 

            				newLeafNode(lv_extendName_0_0, grammarAccess.getExtendAccess().getExtendNameNAMETerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getExtendRule());
            				}
            				setWithLastConsumed(
            					current,
            					"extendName",
            					lv_extendName_0_0,
            					"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtend"


    // $ANTLR start "entryRuleRequirements"
    // InternalPDDL.g:528:1: entryRuleRequirements returns [EObject current=null] : iv_ruleRequirements= ruleRequirements EOF ;
    public final EObject entryRuleRequirements() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequirements = null;


        try {
            // InternalPDDL.g:528:53: (iv_ruleRequirements= ruleRequirements EOF )
            // InternalPDDL.g:529:2: iv_ruleRequirements= ruleRequirements EOF
            {
             newCompositeNode(grammarAccess.getRequirementsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRequirements=ruleRequirements();

            state._fsp--;

             current =iv_ruleRequirements; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequirements"


    // $ANTLR start "ruleRequirements"
    // InternalPDDL.g:535:1: ruleRequirements returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':requirements' ( (lv_rList_2_0= ruleRequirementsList ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleRequirements() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token this_CP_3=null;
        EObject lv_rList_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:541:2: ( (this_OP_0= RULE_OP otherlv_1= ':requirements' ( (lv_rList_2_0= ruleRequirementsList ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:542:2: (this_OP_0= RULE_OP otherlv_1= ':requirements' ( (lv_rList_2_0= ruleRequirementsList ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:542:2: (this_OP_0= RULE_OP otherlv_1= ':requirements' ( (lv_rList_2_0= ruleRequirementsList ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:543:3: this_OP_0= RULE_OP otherlv_1= ':requirements' ( (lv_rList_2_0= ruleRequirementsList ) ) this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_12); 

            			newLeafNode(this_OP_0, grammarAccess.getRequirementsAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,22,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getRequirementsAccess().getRequirementsKeyword_1());
            		
            // InternalPDDL.g:551:3: ( (lv_rList_2_0= ruleRequirementsList ) )
            // InternalPDDL.g:552:4: (lv_rList_2_0= ruleRequirementsList )
            {
            // InternalPDDL.g:552:4: (lv_rList_2_0= ruleRequirementsList )
            // InternalPDDL.g:553:5: lv_rList_2_0= ruleRequirementsList
            {

            					newCompositeNode(grammarAccess.getRequirementsAccess().getRListRequirementsListParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_rList_2_0=ruleRequirementsList();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRequirementsRule());
            					}
            					set(
            						current,
            						"rList",
            						lv_rList_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.RequirementsList");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getRequirementsAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequirements"


    // $ANTLR start "entryRuleRequirementsList"
    // InternalPDDL.g:578:1: entryRuleRequirementsList returns [EObject current=null] : iv_ruleRequirementsList= ruleRequirementsList EOF ;
    public final EObject entryRuleRequirementsList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequirementsList = null;


        try {
            // InternalPDDL.g:578:57: (iv_ruleRequirementsList= ruleRequirementsList EOF )
            // InternalPDDL.g:579:2: iv_ruleRequirementsList= ruleRequirementsList EOF
            {
             newCompositeNode(grammarAccess.getRequirementsListRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRequirementsList=ruleRequirementsList();

            state._fsp--;

             current =iv_ruleRequirementsList; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequirementsList"


    // $ANTLR start "ruleRequirementsList"
    // InternalPDDL.g:585:1: ruleRequirementsList returns [EObject current=null] : ( (lv_rElem_0_0= ruleRequirement ) )+ ;
    public final EObject ruleRequirementsList() throws RecognitionException {
        EObject current = null;

        EObject lv_rElem_0_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:591:2: ( ( (lv_rElem_0_0= ruleRequirement ) )+ )
            // InternalPDDL.g:592:2: ( (lv_rElem_0_0= ruleRequirement ) )+
            {
            // InternalPDDL.g:592:2: ( (lv_rElem_0_0= ruleRequirement ) )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==RULE_IDWITHTWOPOINTBEFOREANDDASHES) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalPDDL.g:593:3: (lv_rElem_0_0= ruleRequirement )
            	    {
            	    // InternalPDDL.g:593:3: (lv_rElem_0_0= ruleRequirement )
            	    // InternalPDDL.g:594:4: lv_rElem_0_0= ruleRequirement
            	    {

            	    				newCompositeNode(grammarAccess.getRequirementsListAccess().getRElemRequirementParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_14);
            	    lv_rElem_0_0=ruleRequirement();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getRequirementsListRule());
            	    				}
            	    				add(
            	    					current,
            	    					"rElem",
            	    					lv_rElem_0_0,
            	    					"fr.irisa.atsyra.pddl.xtext.PDDL.Requirement");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequirementsList"


    // $ANTLR start "entryRuleRequirement"
    // InternalPDDL.g:614:1: entryRuleRequirement returns [EObject current=null] : iv_ruleRequirement= ruleRequirement EOF ;
    public final EObject entryRuleRequirement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequirement = null;


        try {
            // InternalPDDL.g:614:52: (iv_ruleRequirement= ruleRequirement EOF )
            // InternalPDDL.g:615:2: iv_ruleRequirement= ruleRequirement EOF
            {
             newCompositeNode(grammarAccess.getRequirementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRequirement=ruleRequirement();

            state._fsp--;

             current =iv_ruleRequirement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequirement"


    // $ANTLR start "ruleRequirement"
    // InternalPDDL.g:621:1: ruleRequirement returns [EObject current=null] : ( (lv_requirementName_0_0= RULE_IDWITHTWOPOINTBEFOREANDDASHES ) ) ;
    public final EObject ruleRequirement() throws RecognitionException {
        EObject current = null;

        Token lv_requirementName_0_0=null;


        	enterRule();

        try {
            // InternalPDDL.g:627:2: ( ( (lv_requirementName_0_0= RULE_IDWITHTWOPOINTBEFOREANDDASHES ) ) )
            // InternalPDDL.g:628:2: ( (lv_requirementName_0_0= RULE_IDWITHTWOPOINTBEFOREANDDASHES ) )
            {
            // InternalPDDL.g:628:2: ( (lv_requirementName_0_0= RULE_IDWITHTWOPOINTBEFOREANDDASHES ) )
            // InternalPDDL.g:629:3: (lv_requirementName_0_0= RULE_IDWITHTWOPOINTBEFOREANDDASHES )
            {
            // InternalPDDL.g:629:3: (lv_requirementName_0_0= RULE_IDWITHTWOPOINTBEFOREANDDASHES )
            // InternalPDDL.g:630:4: lv_requirementName_0_0= RULE_IDWITHTWOPOINTBEFOREANDDASHES
            {
            lv_requirementName_0_0=(Token)match(input,RULE_IDWITHTWOPOINTBEFOREANDDASHES,FOLLOW_2); 

            				newLeafNode(lv_requirementName_0_0, grammarAccess.getRequirementAccess().getRequirementNameIDWITHTWOPOINTBEFOREANDDASHESTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getRequirementRule());
            				}
            				setWithLastConsumed(
            					current,
            					"requirementName",
            					lv_requirementName_0_0,
            					"fr.irisa.atsyra.pddl.xtext.PDDL.IDWITHTWOPOINTBEFOREANDDASHES");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequirement"


    // $ANTLR start "entryRuleTypes"
    // InternalPDDL.g:649:1: entryRuleTypes returns [EObject current=null] : iv_ruleTypes= ruleTypes EOF ;
    public final EObject entryRuleTypes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypes = null;


        try {
            // InternalPDDL.g:649:46: (iv_ruleTypes= ruleTypes EOF )
            // InternalPDDL.g:650:2: iv_ruleTypes= ruleTypes EOF
            {
             newCompositeNode(grammarAccess.getTypesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypes=ruleTypes();

            state._fsp--;

             current =iv_ruleTypes; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypes"


    // $ANTLR start "ruleTypes"
    // InternalPDDL.g:656:1: ruleTypes returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':types' ( (lv_newTypes_2_0= ruleNewTypes ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleTypes() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token this_CP_3=null;
        EObject lv_newTypes_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:662:2: ( (this_OP_0= RULE_OP otherlv_1= ':types' ( (lv_newTypes_2_0= ruleNewTypes ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:663:2: (this_OP_0= RULE_OP otherlv_1= ':types' ( (lv_newTypes_2_0= ruleNewTypes ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:663:2: (this_OP_0= RULE_OP otherlv_1= ':types' ( (lv_newTypes_2_0= ruleNewTypes ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:664:3: this_OP_0= RULE_OP otherlv_1= ':types' ( (lv_newTypes_2_0= ruleNewTypes ) ) this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_15); 

            			newLeafNode(this_OP_0, grammarAccess.getTypesAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,23,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getTypesAccess().getTypesKeyword_1());
            		
            // InternalPDDL.g:672:3: ( (lv_newTypes_2_0= ruleNewTypes ) )
            // InternalPDDL.g:673:4: (lv_newTypes_2_0= ruleNewTypes )
            {
            // InternalPDDL.g:673:4: (lv_newTypes_2_0= ruleNewTypes )
            // InternalPDDL.g:674:5: lv_newTypes_2_0= ruleNewTypes
            {

            					newCompositeNode(grammarAccess.getTypesAccess().getNewTypesNewTypesParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_newTypes_2_0=ruleNewTypes();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTypesRule());
            					}
            					set(
            						current,
            						"newTypes",
            						lv_newTypes_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.NewTypes");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getTypesAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypes"


    // $ANTLR start "entryRuleNewTypes"
    // InternalPDDL.g:699:1: entryRuleNewTypes returns [EObject current=null] : iv_ruleNewTypes= ruleNewTypes EOF ;
    public final EObject entryRuleNewTypes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNewTypes = null;


        try {
            // InternalPDDL.g:699:49: (iv_ruleNewTypes= ruleNewTypes EOF )
            // InternalPDDL.g:700:2: iv_ruleNewTypes= ruleNewTypes EOF
            {
             newCompositeNode(grammarAccess.getNewTypesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNewTypes=ruleNewTypes();

            state._fsp--;

             current =iv_ruleNewTypes; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNewTypes"


    // $ANTLR start "ruleNewTypes"
    // InternalPDDL.g:706:1: ruleNewTypes returns [EObject current=null] : ( () ( (lv_declInheritorTypes_1_0= ruleInheritorTypes ) )* ( (lv_declOtherTypes_2_0= ruleDeclarationTypes ) )? ) ;
    public final EObject ruleNewTypes() throws RecognitionException {
        EObject current = null;

        EObject lv_declInheritorTypes_1_0 = null;

        EObject lv_declOtherTypes_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:712:2: ( ( () ( (lv_declInheritorTypes_1_0= ruleInheritorTypes ) )* ( (lv_declOtherTypes_2_0= ruleDeclarationTypes ) )? ) )
            // InternalPDDL.g:713:2: ( () ( (lv_declInheritorTypes_1_0= ruleInheritorTypes ) )* ( (lv_declOtherTypes_2_0= ruleDeclarationTypes ) )? )
            {
            // InternalPDDL.g:713:2: ( () ( (lv_declInheritorTypes_1_0= ruleInheritorTypes ) )* ( (lv_declOtherTypes_2_0= ruleDeclarationTypes ) )? )
            // InternalPDDL.g:714:3: () ( (lv_declInheritorTypes_1_0= ruleInheritorTypes ) )* ( (lv_declOtherTypes_2_0= ruleDeclarationTypes ) )?
            {
            // InternalPDDL.g:714:3: ()
            // InternalPDDL.g:715:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getNewTypesAccess().getNewTypesAction_0(),
            					current);
            			

            }

            // InternalPDDL.g:721:3: ( (lv_declInheritorTypes_1_0= ruleInheritorTypes ) )*
            loop11:
            do {
                int alt11=2;
                alt11 = dfa11.predict(input);
                switch (alt11) {
            	case 1 :
            	    // InternalPDDL.g:722:4: (lv_declInheritorTypes_1_0= ruleInheritorTypes )
            	    {
            	    // InternalPDDL.g:722:4: (lv_declInheritorTypes_1_0= ruleInheritorTypes )
            	    // InternalPDDL.g:723:5: lv_declInheritorTypes_1_0= ruleInheritorTypes
            	    {

            	    					newCompositeNode(grammarAccess.getNewTypesAccess().getDeclInheritorTypesInheritorTypesParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_11);
            	    lv_declInheritorTypes_1_0=ruleInheritorTypes();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getNewTypesRule());
            	    					}
            	    					add(
            	    						current,
            	    						"declInheritorTypes",
            	    						lv_declInheritorTypes_1_0,
            	    						"fr.irisa.atsyra.pddl.xtext.PDDL.InheritorTypes");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // InternalPDDL.g:740:3: ( (lv_declOtherTypes_2_0= ruleDeclarationTypes ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_NAME) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalPDDL.g:741:4: (lv_declOtherTypes_2_0= ruleDeclarationTypes )
                    {
                    // InternalPDDL.g:741:4: (lv_declOtherTypes_2_0= ruleDeclarationTypes )
                    // InternalPDDL.g:742:5: lv_declOtherTypes_2_0= ruleDeclarationTypes
                    {

                    					newCompositeNode(grammarAccess.getNewTypesAccess().getDeclOtherTypesDeclarationTypesParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_declOtherTypes_2_0=ruleDeclarationTypes();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getNewTypesRule());
                    					}
                    					set(
                    						current,
                    						"declOtherTypes",
                    						lv_declOtherTypes_2_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.DeclarationTypes");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNewTypes"


    // $ANTLR start "entryRuleInheritorTypes"
    // InternalPDDL.g:763:1: entryRuleInheritorTypes returns [EObject current=null] : iv_ruleInheritorTypes= ruleInheritorTypes EOF ;
    public final EObject entryRuleInheritorTypes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInheritorTypes = null;


        try {
            // InternalPDDL.g:763:55: (iv_ruleInheritorTypes= ruleInheritorTypes EOF )
            // InternalPDDL.g:764:2: iv_ruleInheritorTypes= ruleInheritorTypes EOF
            {
             newCompositeNode(grammarAccess.getInheritorTypesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInheritorTypes=ruleInheritorTypes();

            state._fsp--;

             current =iv_ruleInheritorTypes; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInheritorTypes"


    // $ANTLR start "ruleInheritorTypes"
    // InternalPDDL.g:770:1: ruleInheritorTypes returns [EObject current=null] : ( ( (lv_subTypes_0_0= ruleDeclarationTypes ) ) otherlv_1= '-' ( (lv_supType_2_0= ruleType ) ) ) ;
    public final EObject ruleInheritorTypes() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_subTypes_0_0 = null;

        EObject lv_supType_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:776:2: ( ( ( (lv_subTypes_0_0= ruleDeclarationTypes ) ) otherlv_1= '-' ( (lv_supType_2_0= ruleType ) ) ) )
            // InternalPDDL.g:777:2: ( ( (lv_subTypes_0_0= ruleDeclarationTypes ) ) otherlv_1= '-' ( (lv_supType_2_0= ruleType ) ) )
            {
            // InternalPDDL.g:777:2: ( ( (lv_subTypes_0_0= ruleDeclarationTypes ) ) otherlv_1= '-' ( (lv_supType_2_0= ruleType ) ) )
            // InternalPDDL.g:778:3: ( (lv_subTypes_0_0= ruleDeclarationTypes ) ) otherlv_1= '-' ( (lv_supType_2_0= ruleType ) )
            {
            // InternalPDDL.g:778:3: ( (lv_subTypes_0_0= ruleDeclarationTypes ) )
            // InternalPDDL.g:779:4: (lv_subTypes_0_0= ruleDeclarationTypes )
            {
            // InternalPDDL.g:779:4: (lv_subTypes_0_0= ruleDeclarationTypes )
            // InternalPDDL.g:780:5: lv_subTypes_0_0= ruleDeclarationTypes
            {

            					newCompositeNode(grammarAccess.getInheritorTypesAccess().getSubTypesDeclarationTypesParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_17);
            lv_subTypes_0_0=ruleDeclarationTypes();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInheritorTypesRule());
            					}
            					set(
            						current,
            						"subTypes",
            						lv_subTypes_0_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.DeclarationTypes");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,24,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getInheritorTypesAccess().getHyphenMinusKeyword_1());
            		
            // InternalPDDL.g:801:3: ( (lv_supType_2_0= ruleType ) )
            // InternalPDDL.g:802:4: (lv_supType_2_0= ruleType )
            {
            // InternalPDDL.g:802:4: (lv_supType_2_0= ruleType )
            // InternalPDDL.g:803:5: lv_supType_2_0= ruleType
            {

            					newCompositeNode(grammarAccess.getInheritorTypesAccess().getSupTypeTypeParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_supType_2_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInheritorTypesRule());
            					}
            					set(
            						current,
            						"supType",
            						lv_supType_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInheritorTypes"


    // $ANTLR start "entryRuleDeclarationTypes"
    // InternalPDDL.g:824:1: entryRuleDeclarationTypes returns [EObject current=null] : iv_ruleDeclarationTypes= ruleDeclarationTypes EOF ;
    public final EObject entryRuleDeclarationTypes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclarationTypes = null;


        try {
            // InternalPDDL.g:824:57: (iv_ruleDeclarationTypes= ruleDeclarationTypes EOF )
            // InternalPDDL.g:825:2: iv_ruleDeclarationTypes= ruleDeclarationTypes EOF
            {
             newCompositeNode(grammarAccess.getDeclarationTypesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeclarationTypes=ruleDeclarationTypes();

            state._fsp--;

             current =iv_ruleDeclarationTypes; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclarationTypes"


    // $ANTLR start "ruleDeclarationTypes"
    // InternalPDDL.g:831:1: ruleDeclarationTypes returns [EObject current=null] : ( (lv_declT_0_0= ruleType ) )+ ;
    public final EObject ruleDeclarationTypes() throws RecognitionException {
        EObject current = null;

        EObject lv_declT_0_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:837:2: ( ( (lv_declT_0_0= ruleType ) )+ )
            // InternalPDDL.g:838:2: ( (lv_declT_0_0= ruleType ) )+
            {
            // InternalPDDL.g:838:2: ( (lv_declT_0_0= ruleType ) )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_NAME) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalPDDL.g:839:3: (lv_declT_0_0= ruleType )
            	    {
            	    // InternalPDDL.g:839:3: (lv_declT_0_0= ruleType )
            	    // InternalPDDL.g:840:4: lv_declT_0_0= ruleType
            	    {

            	    				newCompositeNode(grammarAccess.getDeclarationTypesAccess().getDeclTTypeParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_11);
            	    lv_declT_0_0=ruleType();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getDeclarationTypesRule());
            	    				}
            	    				add(
            	    					current,
            	    					"declT",
            	    					lv_declT_0_0,
            	    					"fr.irisa.atsyra.pddl.xtext.PDDL.Type");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclarationTypes"


    // $ANTLR start "entryRuleType"
    // InternalPDDL.g:860:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalPDDL.g:860:45: (iv_ruleType= ruleType EOF )
            // InternalPDDL.g:861:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalPDDL.g:867:1: ruleType returns [EObject current=null] : ( (lv_typeName_0_0= RULE_NAME ) ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        Token lv_typeName_0_0=null;


        	enterRule();

        try {
            // InternalPDDL.g:873:2: ( ( (lv_typeName_0_0= RULE_NAME ) ) )
            // InternalPDDL.g:874:2: ( (lv_typeName_0_0= RULE_NAME ) )
            {
            // InternalPDDL.g:874:2: ( (lv_typeName_0_0= RULE_NAME ) )
            // InternalPDDL.g:875:3: (lv_typeName_0_0= RULE_NAME )
            {
            // InternalPDDL.g:875:3: (lv_typeName_0_0= RULE_NAME )
            // InternalPDDL.g:876:4: lv_typeName_0_0= RULE_NAME
            {
            lv_typeName_0_0=(Token)match(input,RULE_NAME,FOLLOW_2); 

            				newLeafNode(lv_typeName_0_0, grammarAccess.getTypeAccess().getTypeNameNAMETerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getTypeRule());
            				}
            				setWithLastConsumed(
            					current,
            					"typeName",
            					lv_typeName_0_0,
            					"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRulePredicates"
    // InternalPDDL.g:895:1: entryRulePredicates returns [EObject current=null] : iv_rulePredicates= rulePredicates EOF ;
    public final EObject entryRulePredicates() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePredicates = null;


        try {
            // InternalPDDL.g:895:51: (iv_rulePredicates= rulePredicates EOF )
            // InternalPDDL.g:896:2: iv_rulePredicates= rulePredicates EOF
            {
             newCompositeNode(grammarAccess.getPredicatesRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePredicates=rulePredicates();

            state._fsp--;

             current =iv_rulePredicates; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePredicates"


    // $ANTLR start "rulePredicates"
    // InternalPDDL.g:902:1: rulePredicates returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':predicates' ( (lv_predicatList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP ) ;
    public final EObject rulePredicates() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token this_CP_3=null;
        EObject lv_predicatList_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:908:2: ( (this_OP_0= RULE_OP otherlv_1= ':predicates' ( (lv_predicatList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:909:2: (this_OP_0= RULE_OP otherlv_1= ':predicates' ( (lv_predicatList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:909:2: (this_OP_0= RULE_OP otherlv_1= ':predicates' ( (lv_predicatList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:910:3: this_OP_0= RULE_OP otherlv_1= ':predicates' ( (lv_predicatList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_18); 

            			newLeafNode(this_OP_0, grammarAccess.getPredicatesAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,25,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getPredicatesAccess().getPredicatesKeyword_1());
            		
            // InternalPDDL.g:918:3: ( (lv_predicatList_2_0= ruleDeclPFList ) )
            // InternalPDDL.g:919:4: (lv_predicatList_2_0= ruleDeclPFList )
            {
            // InternalPDDL.g:919:4: (lv_predicatList_2_0= ruleDeclPFList )
            // InternalPDDL.g:920:5: lv_predicatList_2_0= ruleDeclPFList
            {

            					newCompositeNode(grammarAccess.getPredicatesAccess().getPredicatListDeclPFListParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_predicatList_2_0=ruleDeclPFList();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPredicatesRule());
            					}
            					set(
            						current,
            						"predicatList",
            						lv_predicatList_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.DeclPFList");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getPredicatesAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePredicates"


    // $ANTLR start "entryRuleDeclPFList"
    // InternalPDDL.g:945:1: entryRuleDeclPFList returns [EObject current=null] : iv_ruleDeclPFList= ruleDeclPFList EOF ;
    public final EObject entryRuleDeclPFList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclPFList = null;


        try {
            // InternalPDDL.g:945:51: (iv_ruleDeclPFList= ruleDeclPFList EOF )
            // InternalPDDL.g:946:2: iv_ruleDeclPFList= ruleDeclPFList EOF
            {
             newCompositeNode(grammarAccess.getDeclPFListRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeclPFList=ruleDeclPFList();

            state._fsp--;

             current =iv_ruleDeclPFList; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclPFList"


    // $ANTLR start "ruleDeclPFList"
    // InternalPDDL.g:952:1: ruleDeclPFList returns [EObject current=null] : ( (lv_pfList_0_0= ruleDeclPredicatOrFunction ) )+ ;
    public final EObject ruleDeclPFList() throws RecognitionException {
        EObject current = null;

        EObject lv_pfList_0_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:958:2: ( ( (lv_pfList_0_0= ruleDeclPredicatOrFunction ) )+ )
            // InternalPDDL.g:959:2: ( (lv_pfList_0_0= ruleDeclPredicatOrFunction ) )+
            {
            // InternalPDDL.g:959:2: ( (lv_pfList_0_0= ruleDeclPredicatOrFunction ) )+
            int cnt14=0;
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_OP) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalPDDL.g:960:3: (lv_pfList_0_0= ruleDeclPredicatOrFunction )
            	    {
            	    // InternalPDDL.g:960:3: (lv_pfList_0_0= ruleDeclPredicatOrFunction )
            	    // InternalPDDL.g:961:4: lv_pfList_0_0= ruleDeclPredicatOrFunction
            	    {

            	    				newCompositeNode(grammarAccess.getDeclPFListAccess().getPfListDeclPredicatOrFunctionParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_9);
            	    lv_pfList_0_0=ruleDeclPredicatOrFunction();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getDeclPFListRule());
            	    				}
            	    				add(
            	    					current,
            	    					"pfList",
            	    					lv_pfList_0_0,
            	    					"fr.irisa.atsyra.pddl.xtext.PDDL.DeclPredicatOrFunction");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt14 >= 1 ) break loop14;
                        EarlyExitException eee =
                            new EarlyExitException(14, input);
                        throw eee;
                }
                cnt14++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclPFList"


    // $ANTLR start "entryRuleDeclPredicatOrFunction"
    // InternalPDDL.g:981:1: entryRuleDeclPredicatOrFunction returns [EObject current=null] : iv_ruleDeclPredicatOrFunction= ruleDeclPredicatOrFunction EOF ;
    public final EObject entryRuleDeclPredicatOrFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclPredicatOrFunction = null;


        try {
            // InternalPDDL.g:981:63: (iv_ruleDeclPredicatOrFunction= ruleDeclPredicatOrFunction EOF )
            // InternalPDDL.g:982:2: iv_ruleDeclPredicatOrFunction= ruleDeclPredicatOrFunction EOF
            {
             newCompositeNode(grammarAccess.getDeclPredicatOrFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeclPredicatOrFunction=ruleDeclPredicatOrFunction();

            state._fsp--;

             current =iv_ruleDeclPredicatOrFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclPredicatOrFunction"


    // $ANTLR start "ruleDeclPredicatOrFunction"
    // InternalPDDL.g:988:1: ruleDeclPredicatOrFunction returns [EObject current=null] : (this_OP_0= RULE_OP ( (lv_pfname_1_0= RULE_NAME ) ) ( (lv_typedVariablesList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleDeclPredicatOrFunction() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token lv_pfname_1_0=null;
        Token this_CP_3=null;
        EObject lv_typedVariablesList_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:994:2: ( (this_OP_0= RULE_OP ( (lv_pfname_1_0= RULE_NAME ) ) ( (lv_typedVariablesList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:995:2: (this_OP_0= RULE_OP ( (lv_pfname_1_0= RULE_NAME ) ) ( (lv_typedVariablesList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:995:2: (this_OP_0= RULE_OP ( (lv_pfname_1_0= RULE_NAME ) ) ( (lv_typedVariablesList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:996:3: this_OP_0= RULE_OP ( (lv_pfname_1_0= RULE_NAME ) ) ( (lv_typedVariablesList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_7); 

            			newLeafNode(this_OP_0, grammarAccess.getDeclPredicatOrFunctionAccess().getOPTerminalRuleCall_0());
            		
            // InternalPDDL.g:1000:3: ( (lv_pfname_1_0= RULE_NAME ) )
            // InternalPDDL.g:1001:4: (lv_pfname_1_0= RULE_NAME )
            {
            // InternalPDDL.g:1001:4: (lv_pfname_1_0= RULE_NAME )
            // InternalPDDL.g:1002:5: lv_pfname_1_0= RULE_NAME
            {
            lv_pfname_1_0=(Token)match(input,RULE_NAME,FOLLOW_19); 

            					newLeafNode(lv_pfname_1_0, grammarAccess.getDeclPredicatOrFunctionAccess().getPfnameNAMETerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDeclPredicatOrFunctionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"pfname",
            						lv_pfname_1_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
            				

            }


            }

            // InternalPDDL.g:1018:3: ( (lv_typedVariablesList_2_0= ruleTypedVariablesList ) )
            // InternalPDDL.g:1019:4: (lv_typedVariablesList_2_0= ruleTypedVariablesList )
            {
            // InternalPDDL.g:1019:4: (lv_typedVariablesList_2_0= ruleTypedVariablesList )
            // InternalPDDL.g:1020:5: lv_typedVariablesList_2_0= ruleTypedVariablesList
            {

            					newCompositeNode(grammarAccess.getDeclPredicatOrFunctionAccess().getTypedVariablesListTypedVariablesListParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_typedVariablesList_2_0=ruleTypedVariablesList();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDeclPredicatOrFunctionRule());
            					}
            					set(
            						current,
            						"typedVariablesList",
            						lv_typedVariablesList_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.TypedVariablesList");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getDeclPredicatOrFunctionAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclPredicatOrFunction"


    // $ANTLR start "entryRuleTypedVariablesList"
    // InternalPDDL.g:1045:1: entryRuleTypedVariablesList returns [EObject current=null] : iv_ruleTypedVariablesList= ruleTypedVariablesList EOF ;
    public final EObject entryRuleTypedVariablesList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypedVariablesList = null;


        try {
            // InternalPDDL.g:1045:59: (iv_ruleTypedVariablesList= ruleTypedVariablesList EOF )
            // InternalPDDL.g:1046:2: iv_ruleTypedVariablesList= ruleTypedVariablesList EOF
            {
             newCompositeNode(grammarAccess.getTypedVariablesListRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypedVariablesList=ruleTypedVariablesList();

            state._fsp--;

             current =iv_ruleTypedVariablesList; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedVariablesList"


    // $ANTLR start "ruleTypedVariablesList"
    // InternalPDDL.g:1052:1: ruleTypedVariablesList returns [EObject current=null] : ( (lv_variablesList_0_0= ruleTypedVariable ) )+ ;
    public final EObject ruleTypedVariablesList() throws RecognitionException {
        EObject current = null;

        EObject lv_variablesList_0_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:1058:2: ( ( (lv_variablesList_0_0= ruleTypedVariable ) )+ )
            // InternalPDDL.g:1059:2: ( (lv_variablesList_0_0= ruleTypedVariable ) )+
            {
            // InternalPDDL.g:1059:2: ( (lv_variablesList_0_0= ruleTypedVariable ) )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalPDDL.g:1060:3: (lv_variablesList_0_0= ruleTypedVariable )
            	    {
            	    // InternalPDDL.g:1060:3: (lv_variablesList_0_0= ruleTypedVariable )
            	    // InternalPDDL.g:1061:4: lv_variablesList_0_0= ruleTypedVariable
            	    {

            	    				newCompositeNode(grammarAccess.getTypedVariablesListAccess().getVariablesListTypedVariableParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_20);
            	    lv_variablesList_0_0=ruleTypedVariable();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getTypedVariablesListRule());
            	    				}
            	    				add(
            	    					current,
            	    					"variablesList",
            	    					lv_variablesList_0_0,
            	    					"fr.irisa.atsyra.pddl.xtext.PDDL.TypedVariable");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedVariablesList"


    // $ANTLR start "entryRuleTypedVariable"
    // InternalPDDL.g:1081:1: entryRuleTypedVariable returns [EObject current=null] : iv_ruleTypedVariable= ruleTypedVariable EOF ;
    public final EObject entryRuleTypedVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypedVariable = null;


        try {
            // InternalPDDL.g:1081:54: (iv_ruleTypedVariable= ruleTypedVariable EOF )
            // InternalPDDL.g:1082:2: iv_ruleTypedVariable= ruleTypedVariable EOF
            {
             newCompositeNode(grammarAccess.getTypedVariableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypedVariable=ruleTypedVariable();

            state._fsp--;

             current =iv_ruleTypedVariable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedVariable"


    // $ANTLR start "ruleTypedVariable"
    // InternalPDDL.g:1088:1: ruleTypedVariable returns [EObject current=null] : ( ( (lv_variables_0_0= ruleVariable ) )+ otherlv_1= '-' ( ( (lv_type_2_0= ruleType ) ) | ( (lv_either_3_0= ruleEither ) ) ) ) ;
    public final EObject ruleTypedVariable() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_variables_0_0 = null;

        EObject lv_type_2_0 = null;

        EObject lv_either_3_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:1094:2: ( ( ( (lv_variables_0_0= ruleVariable ) )+ otherlv_1= '-' ( ( (lv_type_2_0= ruleType ) ) | ( (lv_either_3_0= ruleEither ) ) ) ) )
            // InternalPDDL.g:1095:2: ( ( (lv_variables_0_0= ruleVariable ) )+ otherlv_1= '-' ( ( (lv_type_2_0= ruleType ) ) | ( (lv_either_3_0= ruleEither ) ) ) )
            {
            // InternalPDDL.g:1095:2: ( ( (lv_variables_0_0= ruleVariable ) )+ otherlv_1= '-' ( ( (lv_type_2_0= ruleType ) ) | ( (lv_either_3_0= ruleEither ) ) ) )
            // InternalPDDL.g:1096:3: ( (lv_variables_0_0= ruleVariable ) )+ otherlv_1= '-' ( ( (lv_type_2_0= ruleType ) ) | ( (lv_either_3_0= ruleEither ) ) )
            {
            // InternalPDDL.g:1096:3: ( (lv_variables_0_0= ruleVariable ) )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalPDDL.g:1097:4: (lv_variables_0_0= ruleVariable )
            	    {
            	    // InternalPDDL.g:1097:4: (lv_variables_0_0= ruleVariable )
            	    // InternalPDDL.g:1098:5: lv_variables_0_0= ruleVariable
            	    {

            	    					newCompositeNode(grammarAccess.getTypedVariableAccess().getVariablesVariableParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_21);
            	    lv_variables_0_0=ruleVariable();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTypedVariableRule());
            	    					}
            	    					add(
            	    						current,
            	    						"variables",
            	    						lv_variables_0_0,
            	    						"fr.irisa.atsyra.pddl.xtext.PDDL.Variable");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);

            otherlv_1=(Token)match(input,24,FOLLOW_22); 

            			newLeafNode(otherlv_1, grammarAccess.getTypedVariableAccess().getHyphenMinusKeyword_1());
            		
            // InternalPDDL.g:1119:3: ( ( (lv_type_2_0= ruleType ) ) | ( (lv_either_3_0= ruleEither ) ) )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_NAME) ) {
                alt17=1;
            }
            else if ( (LA17_0==RULE_OP) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalPDDL.g:1120:4: ( (lv_type_2_0= ruleType ) )
                    {
                    // InternalPDDL.g:1120:4: ( (lv_type_2_0= ruleType ) )
                    // InternalPDDL.g:1121:5: (lv_type_2_0= ruleType )
                    {
                    // InternalPDDL.g:1121:5: (lv_type_2_0= ruleType )
                    // InternalPDDL.g:1122:6: lv_type_2_0= ruleType
                    {

                    						newCompositeNode(grammarAccess.getTypedVariableAccess().getTypeTypeParserRuleCall_2_0_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_type_2_0=ruleType();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTypedVariableRule());
                    						}
                    						set(
                    							current,
                    							"type",
                    							lv_type_2_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.Type");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:1140:4: ( (lv_either_3_0= ruleEither ) )
                    {
                    // InternalPDDL.g:1140:4: ( (lv_either_3_0= ruleEither ) )
                    // InternalPDDL.g:1141:5: (lv_either_3_0= ruleEither )
                    {
                    // InternalPDDL.g:1141:5: (lv_either_3_0= ruleEither )
                    // InternalPDDL.g:1142:6: lv_either_3_0= ruleEither
                    {

                    						newCompositeNode(grammarAccess.getTypedVariableAccess().getEitherEitherParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_either_3_0=ruleEither();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTypedVariableRule());
                    						}
                    						set(
                    							current,
                    							"either",
                    							lv_either_3_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.Either");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedVariable"


    // $ANTLR start "entryRuleEither"
    // InternalPDDL.g:1164:1: entryRuleEither returns [EObject current=null] : iv_ruleEither= ruleEither EOF ;
    public final EObject entryRuleEither() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEither = null;


        try {
            // InternalPDDL.g:1164:47: (iv_ruleEither= ruleEither EOF )
            // InternalPDDL.g:1165:2: iv_ruleEither= ruleEither EOF
            {
             newCompositeNode(grammarAccess.getEitherRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEither=ruleEither();

            state._fsp--;

             current =iv_ruleEither; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEither"


    // $ANTLR start "ruleEither"
    // InternalPDDL.g:1171:1: ruleEither returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= 'either' ( (lv_eitherType1_2_0= ruleType ) ) ( (lv_eitherType2_3_0= ruleType ) )+ this_CP_4= RULE_CP ) ;
    public final EObject ruleEither() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token this_CP_4=null;
        EObject lv_eitherType1_2_0 = null;

        EObject lv_eitherType2_3_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:1177:2: ( (this_OP_0= RULE_OP otherlv_1= 'either' ( (lv_eitherType1_2_0= ruleType ) ) ( (lv_eitherType2_3_0= ruleType ) )+ this_CP_4= RULE_CP ) )
            // InternalPDDL.g:1178:2: (this_OP_0= RULE_OP otherlv_1= 'either' ( (lv_eitherType1_2_0= ruleType ) ) ( (lv_eitherType2_3_0= ruleType ) )+ this_CP_4= RULE_CP )
            {
            // InternalPDDL.g:1178:2: (this_OP_0= RULE_OP otherlv_1= 'either' ( (lv_eitherType1_2_0= ruleType ) ) ( (lv_eitherType2_3_0= ruleType ) )+ this_CP_4= RULE_CP )
            // InternalPDDL.g:1179:3: this_OP_0= RULE_OP otherlv_1= 'either' ( (lv_eitherType1_2_0= ruleType ) ) ( (lv_eitherType2_3_0= ruleType ) )+ this_CP_4= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_23); 

            			newLeafNode(this_OP_0, grammarAccess.getEitherAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,26,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getEitherAccess().getEitherKeyword_1());
            		
            // InternalPDDL.g:1187:3: ( (lv_eitherType1_2_0= ruleType ) )
            // InternalPDDL.g:1188:4: (lv_eitherType1_2_0= ruleType )
            {
            // InternalPDDL.g:1188:4: (lv_eitherType1_2_0= ruleType )
            // InternalPDDL.g:1189:5: lv_eitherType1_2_0= ruleType
            {

            					newCompositeNode(grammarAccess.getEitherAccess().getEitherType1TypeParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_7);
            lv_eitherType1_2_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEitherRule());
            					}
            					set(
            						current,
            						"eitherType1",
            						lv_eitherType1_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPDDL.g:1206:3: ( (lv_eitherType2_3_0= ruleType ) )+
            int cnt18=0;
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==RULE_NAME) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalPDDL.g:1207:4: (lv_eitherType2_3_0= ruleType )
            	    {
            	    // InternalPDDL.g:1207:4: (lv_eitherType2_3_0= ruleType )
            	    // InternalPDDL.g:1208:5: lv_eitherType2_3_0= ruleType
            	    {

            	    					newCompositeNode(grammarAccess.getEitherAccess().getEitherType2TypeParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_16);
            	    lv_eitherType2_3_0=ruleType();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEitherRule());
            	    					}
            	    					add(
            	    						current,
            	    						"eitherType2",
            	    						lv_eitherType2_3_0,
            	    						"fr.irisa.atsyra.pddl.xtext.PDDL.Type");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt18 >= 1 ) break loop18;
                        EarlyExitException eee =
                            new EarlyExitException(18, input);
                        throw eee;
                }
                cnt18++;
            } while (true);

            this_CP_4=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_4, grammarAccess.getEitherAccess().getCPTerminalRuleCall_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEither"


    // $ANTLR start "entryRuleVariable"
    // InternalPDDL.g:1233:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalPDDL.g:1233:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalPDDL.g:1234:2: iv_ruleVariable= ruleVariable EOF
            {
             newCompositeNode(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;

             current =iv_ruleVariable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalPDDL.g:1240:1: ruleVariable returns [EObject current=null] : ( (lv_variableName_0_0= RULE_IDWITHQUESTIONMARKBEFORE ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_variableName_0_0=null;


        	enterRule();

        try {
            // InternalPDDL.g:1246:2: ( ( (lv_variableName_0_0= RULE_IDWITHQUESTIONMARKBEFORE ) ) )
            // InternalPDDL.g:1247:2: ( (lv_variableName_0_0= RULE_IDWITHQUESTIONMARKBEFORE ) )
            {
            // InternalPDDL.g:1247:2: ( (lv_variableName_0_0= RULE_IDWITHQUESTIONMARKBEFORE ) )
            // InternalPDDL.g:1248:3: (lv_variableName_0_0= RULE_IDWITHQUESTIONMARKBEFORE )
            {
            // InternalPDDL.g:1248:3: (lv_variableName_0_0= RULE_IDWITHQUESTIONMARKBEFORE )
            // InternalPDDL.g:1249:4: lv_variableName_0_0= RULE_IDWITHQUESTIONMARKBEFORE
            {
            lv_variableName_0_0=(Token)match(input,RULE_IDWITHQUESTIONMARKBEFORE,FOLLOW_2); 

            				newLeafNode(lv_variableName_0_0, grammarAccess.getVariableAccess().getVariableNameIDWITHQUESTIONMARKBEFORETerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getVariableRule());
            				}
            				setWithLastConsumed(
            					current,
            					"variableName",
            					lv_variableName_0_0,
            					"fr.irisa.atsyra.pddl.xtext.PDDL.IDWITHQUESTIONMARKBEFORE");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleFunctions"
    // InternalPDDL.g:1268:1: entryRuleFunctions returns [EObject current=null] : iv_ruleFunctions= ruleFunctions EOF ;
    public final EObject entryRuleFunctions() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctions = null;


        try {
            // InternalPDDL.g:1268:50: (iv_ruleFunctions= ruleFunctions EOF )
            // InternalPDDL.g:1269:2: iv_ruleFunctions= ruleFunctions EOF
            {
             newCompositeNode(grammarAccess.getFunctionsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctions=ruleFunctions();

            state._fsp--;

             current =iv_ruleFunctions; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctions"


    // $ANTLR start "ruleFunctions"
    // InternalPDDL.g:1275:1: ruleFunctions returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':functions' ( (lv_functionList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleFunctions() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token this_CP_3=null;
        EObject lv_functionList_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:1281:2: ( (this_OP_0= RULE_OP otherlv_1= ':functions' ( (lv_functionList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:1282:2: (this_OP_0= RULE_OP otherlv_1= ':functions' ( (lv_functionList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:1282:2: (this_OP_0= RULE_OP otherlv_1= ':functions' ( (lv_functionList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:1283:3: this_OP_0= RULE_OP otherlv_1= ':functions' ( (lv_functionList_2_0= ruleDeclPFList ) ) this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_24); 

            			newLeafNode(this_OP_0, grammarAccess.getFunctionsAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,27,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getFunctionsAccess().getFunctionsKeyword_1());
            		
            // InternalPDDL.g:1291:3: ( (lv_functionList_2_0= ruleDeclPFList ) )
            // InternalPDDL.g:1292:4: (lv_functionList_2_0= ruleDeclPFList )
            {
            // InternalPDDL.g:1292:4: (lv_functionList_2_0= ruleDeclPFList )
            // InternalPDDL.g:1293:5: lv_functionList_2_0= ruleDeclPFList
            {

            					newCompositeNode(grammarAccess.getFunctionsAccess().getFunctionListDeclPFListParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_functionList_2_0=ruleDeclPFList();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFunctionsRule());
            					}
            					set(
            						current,
            						"functionList",
            						lv_functionList_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.DeclPFList");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getFunctionsAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctions"


    // $ANTLR start "entryRulePDDLAction"
    // InternalPDDL.g:1318:1: entryRulePDDLAction returns [EObject current=null] : iv_rulePDDLAction= rulePDDLAction EOF ;
    public final EObject entryRulePDDLAction() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePDDLAction = null;


        try {
            // InternalPDDL.g:1318:51: (iv_rulePDDLAction= rulePDDLAction EOF )
            // InternalPDDL.g:1319:2: iv_rulePDDLAction= rulePDDLAction EOF
            {
             newCompositeNode(grammarAccess.getPDDLActionRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePDDLAction=rulePDDLAction();

            state._fsp--;

             current =iv_rulePDDLAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePDDLAction"


    // $ANTLR start "rulePDDLAction"
    // InternalPDDL.g:1325:1: rulePDDLAction returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':action' ( (lv_actionName_2_0= RULE_NAME ) ) ( (lv_parameters_3_0= ruleParameters ) ) ( (lv_preconditions_4_0= rulePreconditions ) )? ( (lv_effects_5_0= ruleEffects ) ) this_CP_6= RULE_CP ) ;
    public final EObject rulePDDLAction() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token lv_actionName_2_0=null;
        Token this_CP_6=null;
        EObject lv_parameters_3_0 = null;

        EObject lv_preconditions_4_0 = null;

        EObject lv_effects_5_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:1331:2: ( (this_OP_0= RULE_OP otherlv_1= ':action' ( (lv_actionName_2_0= RULE_NAME ) ) ( (lv_parameters_3_0= ruleParameters ) ) ( (lv_preconditions_4_0= rulePreconditions ) )? ( (lv_effects_5_0= ruleEffects ) ) this_CP_6= RULE_CP ) )
            // InternalPDDL.g:1332:2: (this_OP_0= RULE_OP otherlv_1= ':action' ( (lv_actionName_2_0= RULE_NAME ) ) ( (lv_parameters_3_0= ruleParameters ) ) ( (lv_preconditions_4_0= rulePreconditions ) )? ( (lv_effects_5_0= ruleEffects ) ) this_CP_6= RULE_CP )
            {
            // InternalPDDL.g:1332:2: (this_OP_0= RULE_OP otherlv_1= ':action' ( (lv_actionName_2_0= RULE_NAME ) ) ( (lv_parameters_3_0= ruleParameters ) ) ( (lv_preconditions_4_0= rulePreconditions ) )? ( (lv_effects_5_0= ruleEffects ) ) this_CP_6= RULE_CP )
            // InternalPDDL.g:1333:3: this_OP_0= RULE_OP otherlv_1= ':action' ( (lv_actionName_2_0= RULE_NAME ) ) ( (lv_parameters_3_0= ruleParameters ) ) ( (lv_preconditions_4_0= rulePreconditions ) )? ( (lv_effects_5_0= ruleEffects ) ) this_CP_6= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_25); 

            			newLeafNode(this_OP_0, grammarAccess.getPDDLActionAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,28,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getPDDLActionAccess().getActionKeyword_1());
            		
            // InternalPDDL.g:1341:3: ( (lv_actionName_2_0= RULE_NAME ) )
            // InternalPDDL.g:1342:4: (lv_actionName_2_0= RULE_NAME )
            {
            // InternalPDDL.g:1342:4: (lv_actionName_2_0= RULE_NAME )
            // InternalPDDL.g:1343:5: lv_actionName_2_0= RULE_NAME
            {
            lv_actionName_2_0=(Token)match(input,RULE_NAME,FOLLOW_26); 

            					newLeafNode(lv_actionName_2_0, grammarAccess.getPDDLActionAccess().getActionNameNAMETerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPDDLActionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"actionName",
            						lv_actionName_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
            				

            }


            }

            // InternalPDDL.g:1359:3: ( (lv_parameters_3_0= ruleParameters ) )
            // InternalPDDL.g:1360:4: (lv_parameters_3_0= ruleParameters )
            {
            // InternalPDDL.g:1360:4: (lv_parameters_3_0= ruleParameters )
            // InternalPDDL.g:1361:5: lv_parameters_3_0= ruleParameters
            {

            					newCompositeNode(grammarAccess.getPDDLActionAccess().getParametersParametersParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_27);
            lv_parameters_3_0=ruleParameters();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPDDLActionRule());
            					}
            					set(
            						current,
            						"parameters",
            						lv_parameters_3_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Parameters");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPDDL.g:1378:3: ( (lv_preconditions_4_0= rulePreconditions ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==30) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalPDDL.g:1379:4: (lv_preconditions_4_0= rulePreconditions )
                    {
                    // InternalPDDL.g:1379:4: (lv_preconditions_4_0= rulePreconditions )
                    // InternalPDDL.g:1380:5: lv_preconditions_4_0= rulePreconditions
                    {

                    					newCompositeNode(grammarAccess.getPDDLActionAccess().getPreconditionsPreconditionsParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_27);
                    lv_preconditions_4_0=rulePreconditions();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getPDDLActionRule());
                    					}
                    					set(
                    						current,
                    						"preconditions",
                    						lv_preconditions_4_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Preconditions");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPDDL.g:1397:3: ( (lv_effects_5_0= ruleEffects ) )
            // InternalPDDL.g:1398:4: (lv_effects_5_0= ruleEffects )
            {
            // InternalPDDL.g:1398:4: (lv_effects_5_0= ruleEffects )
            // InternalPDDL.g:1399:5: lv_effects_5_0= ruleEffects
            {

            					newCompositeNode(grammarAccess.getPDDLActionAccess().getEffectsEffectsParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_5);
            lv_effects_5_0=ruleEffects();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPDDLActionRule());
            					}
            					set(
            						current,
            						"effects",
            						lv_effects_5_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Effects");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_6=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_6, grammarAccess.getPDDLActionAccess().getCPTerminalRuleCall_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePDDLAction"


    // $ANTLR start "entryRuleParameters"
    // InternalPDDL.g:1424:1: entryRuleParameters returns [EObject current=null] : iv_ruleParameters= ruleParameters EOF ;
    public final EObject entryRuleParameters() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameters = null;


        try {
            // InternalPDDL.g:1424:51: (iv_ruleParameters= ruleParameters EOF )
            // InternalPDDL.g:1425:2: iv_ruleParameters= ruleParameters EOF
            {
             newCompositeNode(grammarAccess.getParametersRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameters=ruleParameters();

            state._fsp--;

             current =iv_ruleParameters; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameters"


    // $ANTLR start "ruleParameters"
    // InternalPDDL.g:1431:1: ruleParameters returns [EObject current=null] : (otherlv_0= ':parameters' this_OP_1= RULE_OP ( (lv_parametersList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleParameters() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token this_OP_1=null;
        Token this_CP_3=null;
        EObject lv_parametersList_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:1437:2: ( (otherlv_0= ':parameters' this_OP_1= RULE_OP ( (lv_parametersList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:1438:2: (otherlv_0= ':parameters' this_OP_1= RULE_OP ( (lv_parametersList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:1438:2: (otherlv_0= ':parameters' this_OP_1= RULE_OP ( (lv_parametersList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:1439:3: otherlv_0= ':parameters' this_OP_1= RULE_OP ( (lv_parametersList_2_0= ruleTypedVariablesList ) ) this_CP_3= RULE_CP
            {
            otherlv_0=(Token)match(input,29,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getParametersAccess().getParametersKeyword_0());
            		
            this_OP_1=(Token)match(input,RULE_OP,FOLLOW_19); 

            			newLeafNode(this_OP_1, grammarAccess.getParametersAccess().getOPTerminalRuleCall_1());
            		
            // InternalPDDL.g:1447:3: ( (lv_parametersList_2_0= ruleTypedVariablesList ) )
            // InternalPDDL.g:1448:4: (lv_parametersList_2_0= ruleTypedVariablesList )
            {
            // InternalPDDL.g:1448:4: (lv_parametersList_2_0= ruleTypedVariablesList )
            // InternalPDDL.g:1449:5: lv_parametersList_2_0= ruleTypedVariablesList
            {

            					newCompositeNode(grammarAccess.getParametersAccess().getParametersListTypedVariablesListParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_parametersList_2_0=ruleTypedVariablesList();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getParametersRule());
            					}
            					set(
            						current,
            						"parametersList",
            						lv_parametersList_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.TypedVariablesList");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getParametersAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameters"


    // $ANTLR start "entryRulePreconditions"
    // InternalPDDL.g:1474:1: entryRulePreconditions returns [EObject current=null] : iv_rulePreconditions= rulePreconditions EOF ;
    public final EObject entryRulePreconditions() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePreconditions = null;


        try {
            // InternalPDDL.g:1474:54: (iv_rulePreconditions= rulePreconditions EOF )
            // InternalPDDL.g:1475:2: iv_rulePreconditions= rulePreconditions EOF
            {
             newCompositeNode(grammarAccess.getPreconditionsRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePreconditions=rulePreconditions();

            state._fsp--;

             current =iv_rulePreconditions; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePreconditions"


    // $ANTLR start "rulePreconditions"
    // InternalPDDL.g:1481:1: rulePreconditions returns [EObject current=null] : (otherlv_0= ':precondition' ( (lv_le_1_0= ruleLogicalExpressionDM ) ) ) ;
    public final EObject rulePreconditions() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_le_1_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:1487:2: ( (otherlv_0= ':precondition' ( (lv_le_1_0= ruleLogicalExpressionDM ) ) ) )
            // InternalPDDL.g:1488:2: (otherlv_0= ':precondition' ( (lv_le_1_0= ruleLogicalExpressionDM ) ) )
            {
            // InternalPDDL.g:1488:2: (otherlv_0= ':precondition' ( (lv_le_1_0= ruleLogicalExpressionDM ) ) )
            // InternalPDDL.g:1489:3: otherlv_0= ':precondition' ( (lv_le_1_0= ruleLogicalExpressionDM ) )
            {
            otherlv_0=(Token)match(input,30,FOLLOW_28); 

            			newLeafNode(otherlv_0, grammarAccess.getPreconditionsAccess().getPreconditionKeyword_0());
            		
            // InternalPDDL.g:1493:3: ( (lv_le_1_0= ruleLogicalExpressionDM ) )
            // InternalPDDL.g:1494:4: (lv_le_1_0= ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:1494:4: (lv_le_1_0= ruleLogicalExpressionDM )
            // InternalPDDL.g:1495:5: lv_le_1_0= ruleLogicalExpressionDM
            {

            					newCompositeNode(grammarAccess.getPreconditionsAccess().getLeLogicalExpressionDMParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_le_1_0=ruleLogicalExpressionDM();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPreconditionsRule());
            					}
            					set(
            						current,
            						"le",
            						lv_le_1_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePreconditions"


    // $ANTLR start "entryRuleEffects"
    // InternalPDDL.g:1516:1: entryRuleEffects returns [EObject current=null] : iv_ruleEffects= ruleEffects EOF ;
    public final EObject entryRuleEffects() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEffects = null;


        try {
            // InternalPDDL.g:1516:48: (iv_ruleEffects= ruleEffects EOF )
            // InternalPDDL.g:1517:2: iv_ruleEffects= ruleEffects EOF
            {
             newCompositeNode(grammarAccess.getEffectsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEffects=ruleEffects();

            state._fsp--;

             current =iv_ruleEffects; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEffects"


    // $ANTLR start "ruleEffects"
    // InternalPDDL.g:1523:1: ruleEffects returns [EObject current=null] : (otherlv_0= ':effect' ( (lv_le_1_0= ruleLogicalExpressionDM ) ) ) ;
    public final EObject ruleEffects() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_le_1_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:1529:2: ( (otherlv_0= ':effect' ( (lv_le_1_0= ruleLogicalExpressionDM ) ) ) )
            // InternalPDDL.g:1530:2: (otherlv_0= ':effect' ( (lv_le_1_0= ruleLogicalExpressionDM ) ) )
            {
            // InternalPDDL.g:1530:2: (otherlv_0= ':effect' ( (lv_le_1_0= ruleLogicalExpressionDM ) ) )
            // InternalPDDL.g:1531:3: otherlv_0= ':effect' ( (lv_le_1_0= ruleLogicalExpressionDM ) )
            {
            otherlv_0=(Token)match(input,31,FOLLOW_28); 

            			newLeafNode(otherlv_0, grammarAccess.getEffectsAccess().getEffectKeyword_0());
            		
            // InternalPDDL.g:1535:3: ( (lv_le_1_0= ruleLogicalExpressionDM ) )
            // InternalPDDL.g:1536:4: (lv_le_1_0= ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:1536:4: (lv_le_1_0= ruleLogicalExpressionDM )
            // InternalPDDL.g:1537:5: lv_le_1_0= ruleLogicalExpressionDM
            {

            					newCompositeNode(grammarAccess.getEffectsAccess().getLeLogicalExpressionDMParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_le_1_0=ruleLogicalExpressionDM();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEffectsRule());
            					}
            					set(
            						current,
            						"le",
            						lv_le_1_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEffects"


    // $ANTLR start "entryRuleLogicalExpressionDM"
    // InternalPDDL.g:1558:1: entryRuleLogicalExpressionDM returns [EObject current=null] : iv_ruleLogicalExpressionDM= ruleLogicalExpressionDM EOF ;
    public final EObject entryRuleLogicalExpressionDM() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogicalExpressionDM = null;


        try {
            // InternalPDDL.g:1558:60: (iv_ruleLogicalExpressionDM= ruleLogicalExpressionDM EOF )
            // InternalPDDL.g:1559:2: iv_ruleLogicalExpressionDM= ruleLogicalExpressionDM EOF
            {
             newCompositeNode(grammarAccess.getLogicalExpressionDMRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogicalExpressionDM=ruleLogicalExpressionDM();

            state._fsp--;

             current =iv_ruleLogicalExpressionDM; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogicalExpressionDM"


    // $ANTLR start "ruleLogicalExpressionDM"
    // InternalPDDL.g:1565:1: ruleLogicalExpressionDM returns [EObject current=null] : ( ( () (this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP ) ) | ( (lv_v_4_0= ruleVariable ) ) | (this_OP_5= RULE_OP ( (lv_le_6_0= ruleLogicalExpressionDM ) ) this_CP_7= RULE_CP ) | (this_OP_8= RULE_OP otherlv_9= '=' ( (lv_vEqual1_10_0= ruleLogicalExpressionDM ) ) ( (lv_vEqual2_11_0= ruleLogicalExpressionDM ) )+ this_CP_12= RULE_CP ) | (this_OP_13= RULE_OP otherlv_14= 'not' ( (lv_leNot_15_0= ruleLogicalExpressionDM ) ) this_CP_16= RULE_CP ) | (this_OP_17= RULE_OP otherlv_18= 'and' ( (lv_leAnd1_19_0= ruleLogicalExpressionDM ) ) ( (lv_leAnd2_20_0= ruleLogicalExpressionDM ) )+ this_CP_21= RULE_CP ) | (this_OP_22= RULE_OP otherlv_23= 'or' ( (lv_leOr1_24_0= ruleLogicalExpressionDM ) ) ( (lv_leOr2_25_0= ruleLogicalExpressionDM ) )+ this_CP_26= RULE_CP ) | (this_OP_27= RULE_OP otherlv_28= 'imply' ( (lv_leImply1_29_0= ruleLogicalExpressionDM ) ) ( (lv_leImply2_30_0= ruleLogicalExpressionDM ) ) this_CP_31= RULE_CP ) | (this_OP_32= RULE_OP otherlv_33= 'forall' ( (lv_tdForall_34_0= ruleTypeDescr ) ) ( (lv_leForall_35_0= ruleLogicalExpressionDM ) ) this_CP_36= RULE_CP ) | (this_OP_37= RULE_OP otherlv_38= 'exists' ( (lv_tdExists_39_0= ruleTypeDescr ) ) ( (lv_leExists_40_0= ruleLogicalExpressionDM ) ) this_CP_41= RULE_CP ) | (this_OP_42= RULE_OP otherlv_43= 'when' ( (lv_leWhen1_44_0= ruleLogicalExpressionDM ) ) ( (lv_leWhen2_45_0= ruleLogicalExpressionDM ) ) this_CP_46= RULE_CP ) ) ;
    public final EObject ruleLogicalExpressionDM() throws RecognitionException {
        EObject current = null;

        Token this_OP_1=null;
        Token this_CP_3=null;
        Token this_OP_5=null;
        Token this_CP_7=null;
        Token this_OP_8=null;
        Token otherlv_9=null;
        Token this_CP_12=null;
        Token this_OP_13=null;
        Token otherlv_14=null;
        Token this_CP_16=null;
        Token this_OP_17=null;
        Token otherlv_18=null;
        Token this_CP_21=null;
        Token this_OP_22=null;
        Token otherlv_23=null;
        Token this_CP_26=null;
        Token this_OP_27=null;
        Token otherlv_28=null;
        Token this_CP_31=null;
        Token this_OP_32=null;
        Token otherlv_33=null;
        Token this_CP_36=null;
        Token this_OP_37=null;
        Token otherlv_38=null;
        Token this_CP_41=null;
        Token this_OP_42=null;
        Token otherlv_43=null;
        Token this_CP_46=null;
        EObject lv_p_2_0 = null;

        EObject lv_v_4_0 = null;

        EObject lv_le_6_0 = null;

        EObject lv_vEqual1_10_0 = null;

        EObject lv_vEqual2_11_0 = null;

        EObject lv_leNot_15_0 = null;

        EObject lv_leAnd1_19_0 = null;

        EObject lv_leAnd2_20_0 = null;

        EObject lv_leOr1_24_0 = null;

        EObject lv_leOr2_25_0 = null;

        EObject lv_leImply1_29_0 = null;

        EObject lv_leImply2_30_0 = null;

        EObject lv_tdForall_34_0 = null;

        EObject lv_leForall_35_0 = null;

        EObject lv_tdExists_39_0 = null;

        EObject lv_leExists_40_0 = null;

        EObject lv_leWhen1_44_0 = null;

        EObject lv_leWhen2_45_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:1571:2: ( ( ( () (this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP ) ) | ( (lv_v_4_0= ruleVariable ) ) | (this_OP_5= RULE_OP ( (lv_le_6_0= ruleLogicalExpressionDM ) ) this_CP_7= RULE_CP ) | (this_OP_8= RULE_OP otherlv_9= '=' ( (lv_vEqual1_10_0= ruleLogicalExpressionDM ) ) ( (lv_vEqual2_11_0= ruleLogicalExpressionDM ) )+ this_CP_12= RULE_CP ) | (this_OP_13= RULE_OP otherlv_14= 'not' ( (lv_leNot_15_0= ruleLogicalExpressionDM ) ) this_CP_16= RULE_CP ) | (this_OP_17= RULE_OP otherlv_18= 'and' ( (lv_leAnd1_19_0= ruleLogicalExpressionDM ) ) ( (lv_leAnd2_20_0= ruleLogicalExpressionDM ) )+ this_CP_21= RULE_CP ) | (this_OP_22= RULE_OP otherlv_23= 'or' ( (lv_leOr1_24_0= ruleLogicalExpressionDM ) ) ( (lv_leOr2_25_0= ruleLogicalExpressionDM ) )+ this_CP_26= RULE_CP ) | (this_OP_27= RULE_OP otherlv_28= 'imply' ( (lv_leImply1_29_0= ruleLogicalExpressionDM ) ) ( (lv_leImply2_30_0= ruleLogicalExpressionDM ) ) this_CP_31= RULE_CP ) | (this_OP_32= RULE_OP otherlv_33= 'forall' ( (lv_tdForall_34_0= ruleTypeDescr ) ) ( (lv_leForall_35_0= ruleLogicalExpressionDM ) ) this_CP_36= RULE_CP ) | (this_OP_37= RULE_OP otherlv_38= 'exists' ( (lv_tdExists_39_0= ruleTypeDescr ) ) ( (lv_leExists_40_0= ruleLogicalExpressionDM ) ) this_CP_41= RULE_CP ) | (this_OP_42= RULE_OP otherlv_43= 'when' ( (lv_leWhen1_44_0= ruleLogicalExpressionDM ) ) ( (lv_leWhen2_45_0= ruleLogicalExpressionDM ) ) this_CP_46= RULE_CP ) ) )
            // InternalPDDL.g:1572:2: ( ( () (this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP ) ) | ( (lv_v_4_0= ruleVariable ) ) | (this_OP_5= RULE_OP ( (lv_le_6_0= ruleLogicalExpressionDM ) ) this_CP_7= RULE_CP ) | (this_OP_8= RULE_OP otherlv_9= '=' ( (lv_vEqual1_10_0= ruleLogicalExpressionDM ) ) ( (lv_vEqual2_11_0= ruleLogicalExpressionDM ) )+ this_CP_12= RULE_CP ) | (this_OP_13= RULE_OP otherlv_14= 'not' ( (lv_leNot_15_0= ruleLogicalExpressionDM ) ) this_CP_16= RULE_CP ) | (this_OP_17= RULE_OP otherlv_18= 'and' ( (lv_leAnd1_19_0= ruleLogicalExpressionDM ) ) ( (lv_leAnd2_20_0= ruleLogicalExpressionDM ) )+ this_CP_21= RULE_CP ) | (this_OP_22= RULE_OP otherlv_23= 'or' ( (lv_leOr1_24_0= ruleLogicalExpressionDM ) ) ( (lv_leOr2_25_0= ruleLogicalExpressionDM ) )+ this_CP_26= RULE_CP ) | (this_OP_27= RULE_OP otherlv_28= 'imply' ( (lv_leImply1_29_0= ruleLogicalExpressionDM ) ) ( (lv_leImply2_30_0= ruleLogicalExpressionDM ) ) this_CP_31= RULE_CP ) | (this_OP_32= RULE_OP otherlv_33= 'forall' ( (lv_tdForall_34_0= ruleTypeDescr ) ) ( (lv_leForall_35_0= ruleLogicalExpressionDM ) ) this_CP_36= RULE_CP ) | (this_OP_37= RULE_OP otherlv_38= 'exists' ( (lv_tdExists_39_0= ruleTypeDescr ) ) ( (lv_leExists_40_0= ruleLogicalExpressionDM ) ) this_CP_41= RULE_CP ) | (this_OP_42= RULE_OP otherlv_43= 'when' ( (lv_leWhen1_44_0= ruleLogicalExpressionDM ) ) ( (lv_leWhen2_45_0= ruleLogicalExpressionDM ) ) this_CP_46= RULE_CP ) )
            {
            // InternalPDDL.g:1572:2: ( ( () (this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP ) ) | ( (lv_v_4_0= ruleVariable ) ) | (this_OP_5= RULE_OP ( (lv_le_6_0= ruleLogicalExpressionDM ) ) this_CP_7= RULE_CP ) | (this_OP_8= RULE_OP otherlv_9= '=' ( (lv_vEqual1_10_0= ruleLogicalExpressionDM ) ) ( (lv_vEqual2_11_0= ruleLogicalExpressionDM ) )+ this_CP_12= RULE_CP ) | (this_OP_13= RULE_OP otherlv_14= 'not' ( (lv_leNot_15_0= ruleLogicalExpressionDM ) ) this_CP_16= RULE_CP ) | (this_OP_17= RULE_OP otherlv_18= 'and' ( (lv_leAnd1_19_0= ruleLogicalExpressionDM ) ) ( (lv_leAnd2_20_0= ruleLogicalExpressionDM ) )+ this_CP_21= RULE_CP ) | (this_OP_22= RULE_OP otherlv_23= 'or' ( (lv_leOr1_24_0= ruleLogicalExpressionDM ) ) ( (lv_leOr2_25_0= ruleLogicalExpressionDM ) )+ this_CP_26= RULE_CP ) | (this_OP_27= RULE_OP otherlv_28= 'imply' ( (lv_leImply1_29_0= ruleLogicalExpressionDM ) ) ( (lv_leImply2_30_0= ruleLogicalExpressionDM ) ) this_CP_31= RULE_CP ) | (this_OP_32= RULE_OP otherlv_33= 'forall' ( (lv_tdForall_34_0= ruleTypeDescr ) ) ( (lv_leForall_35_0= ruleLogicalExpressionDM ) ) this_CP_36= RULE_CP ) | (this_OP_37= RULE_OP otherlv_38= 'exists' ( (lv_tdExists_39_0= ruleTypeDescr ) ) ( (lv_leExists_40_0= ruleLogicalExpressionDM ) ) this_CP_41= RULE_CP ) | (this_OP_42= RULE_OP otherlv_43= 'when' ( (lv_leWhen1_44_0= ruleLogicalExpressionDM ) ) ( (lv_leWhen2_45_0= ruleLogicalExpressionDM ) ) this_CP_46= RULE_CP ) )
            int alt23=11;
            alt23 = dfa23.predict(input);
            switch (alt23) {
                case 1 :
                    // InternalPDDL.g:1573:3: ( () (this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP ) )
                    {
                    // InternalPDDL.g:1573:3: ( () (this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP ) )
                    // InternalPDDL.g:1574:4: () (this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP )
                    {
                    // InternalPDDL.g:1574:4: ()
                    // InternalPDDL.g:1575:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getLogicalExpressionDMAccess().getLogicalExpressionDMAction_0_0(),
                    						current);
                    				

                    }

                    // InternalPDDL.g:1581:4: (this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP )
                    // InternalPDDL.g:1582:5: this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP
                    {
                    this_OP_1=(Token)match(input,RULE_OP,FOLLOW_7); 

                    					newLeafNode(this_OP_1, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_0_1_0());
                    				
                    // InternalPDDL.g:1586:5: ( (lv_p_2_0= rulePredicatDM ) )
                    // InternalPDDL.g:1587:6: (lv_p_2_0= rulePredicatDM )
                    {
                    // InternalPDDL.g:1587:6: (lv_p_2_0= rulePredicatDM )
                    // InternalPDDL.g:1588:7: lv_p_2_0= rulePredicatDM
                    {

                    							newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getPPredicatDMParserRuleCall_0_1_1_0());
                    						
                    pushFollow(FOLLOW_5);
                    lv_p_2_0=rulePredicatDM();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    							}
                    							set(
                    								current,
                    								"p",
                    								lv_p_2_0,
                    								"fr.irisa.atsyra.pddl.xtext.PDDL.PredicatDM");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

                    					newLeafNode(this_CP_3, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_0_1_2());
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:1612:3: ( (lv_v_4_0= ruleVariable ) )
                    {
                    // InternalPDDL.g:1612:3: ( (lv_v_4_0= ruleVariable ) )
                    // InternalPDDL.g:1613:4: (lv_v_4_0= ruleVariable )
                    {
                    // InternalPDDL.g:1613:4: (lv_v_4_0= ruleVariable )
                    // InternalPDDL.g:1614:5: lv_v_4_0= ruleVariable
                    {

                    					newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getVVariableParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_v_4_0=ruleVariable();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    					}
                    					set(
                    						current,
                    						"v",
                    						lv_v_4_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Variable");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPDDL.g:1632:3: (this_OP_5= RULE_OP ( (lv_le_6_0= ruleLogicalExpressionDM ) ) this_CP_7= RULE_CP )
                    {
                    // InternalPDDL.g:1632:3: (this_OP_5= RULE_OP ( (lv_le_6_0= ruleLogicalExpressionDM ) ) this_CP_7= RULE_CP )
                    // InternalPDDL.g:1633:4: this_OP_5= RULE_OP ( (lv_le_6_0= ruleLogicalExpressionDM ) ) this_CP_7= RULE_CP
                    {
                    this_OP_5=(Token)match(input,RULE_OP,FOLLOW_28); 

                    				newLeafNode(this_OP_5, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_2_0());
                    			
                    // InternalPDDL.g:1637:4: ( (lv_le_6_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:1638:5: (lv_le_6_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:1638:5: (lv_le_6_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:1639:6: lv_le_6_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeLogicalExpressionDMParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_le_6_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"le",
                    							lv_le_6_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_7=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_7, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_2_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalPDDL.g:1662:3: (this_OP_8= RULE_OP otherlv_9= '=' ( (lv_vEqual1_10_0= ruleLogicalExpressionDM ) ) ( (lv_vEqual2_11_0= ruleLogicalExpressionDM ) )+ this_CP_12= RULE_CP )
                    {
                    // InternalPDDL.g:1662:3: (this_OP_8= RULE_OP otherlv_9= '=' ( (lv_vEqual1_10_0= ruleLogicalExpressionDM ) ) ( (lv_vEqual2_11_0= ruleLogicalExpressionDM ) )+ this_CP_12= RULE_CP )
                    // InternalPDDL.g:1663:4: this_OP_8= RULE_OP otherlv_9= '=' ( (lv_vEqual1_10_0= ruleLogicalExpressionDM ) ) ( (lv_vEqual2_11_0= ruleLogicalExpressionDM ) )+ this_CP_12= RULE_CP
                    {
                    this_OP_8=(Token)match(input,RULE_OP,FOLLOW_29); 

                    				newLeafNode(this_OP_8, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_3_0());
                    			
                    otherlv_9=(Token)match(input,32,FOLLOW_28); 

                    				newLeafNode(otherlv_9, grammarAccess.getLogicalExpressionDMAccess().getEqualsSignKeyword_3_1());
                    			
                    // InternalPDDL.g:1671:4: ( (lv_vEqual1_10_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:1672:5: (lv_vEqual1_10_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:1672:5: (lv_vEqual1_10_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:1673:6: lv_vEqual1_10_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getVEqual1LogicalExpressionDMParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_vEqual1_10_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"vEqual1",
                    							lv_vEqual1_10_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:1690:4: ( (lv_vEqual2_11_0= ruleLogicalExpressionDM ) )+
                    int cnt20=0;
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==RULE_OP||LA20_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // InternalPDDL.g:1691:5: (lv_vEqual2_11_0= ruleLogicalExpressionDM )
                    	    {
                    	    // InternalPDDL.g:1691:5: (lv_vEqual2_11_0= ruleLogicalExpressionDM )
                    	    // InternalPDDL.g:1692:6: lv_vEqual2_11_0= ruleLogicalExpressionDM
                    	    {

                    	    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getVEqual2LogicalExpressionDMParserRuleCall_3_3_0());
                    	    					
                    	    pushFollow(FOLLOW_30);
                    	    lv_vEqual2_11_0=ruleLogicalExpressionDM();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"vEqual2",
                    	    							lv_vEqual2_11_0,
                    	    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt20 >= 1 ) break loop20;
                                EarlyExitException eee =
                                    new EarlyExitException(20, input);
                                throw eee;
                        }
                        cnt20++;
                    } while (true);

                    this_CP_12=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_12, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_3_4());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalPDDL.g:1715:3: (this_OP_13= RULE_OP otherlv_14= 'not' ( (lv_leNot_15_0= ruleLogicalExpressionDM ) ) this_CP_16= RULE_CP )
                    {
                    // InternalPDDL.g:1715:3: (this_OP_13= RULE_OP otherlv_14= 'not' ( (lv_leNot_15_0= ruleLogicalExpressionDM ) ) this_CP_16= RULE_CP )
                    // InternalPDDL.g:1716:4: this_OP_13= RULE_OP otherlv_14= 'not' ( (lv_leNot_15_0= ruleLogicalExpressionDM ) ) this_CP_16= RULE_CP
                    {
                    this_OP_13=(Token)match(input,RULE_OP,FOLLOW_31); 

                    				newLeafNode(this_OP_13, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_4_0());
                    			
                    otherlv_14=(Token)match(input,33,FOLLOW_28); 

                    				newLeafNode(otherlv_14, grammarAccess.getLogicalExpressionDMAccess().getNotKeyword_4_1());
                    			
                    // InternalPDDL.g:1724:4: ( (lv_leNot_15_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:1725:5: (lv_leNot_15_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:1725:5: (lv_leNot_15_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:1726:6: lv_leNot_15_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeNotLogicalExpressionDMParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leNot_15_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"leNot",
                    							lv_leNot_15_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_16=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_16, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_4_3());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalPDDL.g:1749:3: (this_OP_17= RULE_OP otherlv_18= 'and' ( (lv_leAnd1_19_0= ruleLogicalExpressionDM ) ) ( (lv_leAnd2_20_0= ruleLogicalExpressionDM ) )+ this_CP_21= RULE_CP )
                    {
                    // InternalPDDL.g:1749:3: (this_OP_17= RULE_OP otherlv_18= 'and' ( (lv_leAnd1_19_0= ruleLogicalExpressionDM ) ) ( (lv_leAnd2_20_0= ruleLogicalExpressionDM ) )+ this_CP_21= RULE_CP )
                    // InternalPDDL.g:1750:4: this_OP_17= RULE_OP otherlv_18= 'and' ( (lv_leAnd1_19_0= ruleLogicalExpressionDM ) ) ( (lv_leAnd2_20_0= ruleLogicalExpressionDM ) )+ this_CP_21= RULE_CP
                    {
                    this_OP_17=(Token)match(input,RULE_OP,FOLLOW_32); 

                    				newLeafNode(this_OP_17, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_5_0());
                    			
                    otherlv_18=(Token)match(input,34,FOLLOW_28); 

                    				newLeafNode(otherlv_18, grammarAccess.getLogicalExpressionDMAccess().getAndKeyword_5_1());
                    			
                    // InternalPDDL.g:1758:4: ( (lv_leAnd1_19_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:1759:5: (lv_leAnd1_19_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:1759:5: (lv_leAnd1_19_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:1760:6: lv_leAnd1_19_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeAnd1LogicalExpressionDMParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_leAnd1_19_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"leAnd1",
                    							lv_leAnd1_19_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:1777:4: ( (lv_leAnd2_20_0= ruleLogicalExpressionDM ) )+
                    int cnt21=0;
                    loop21:
                    do {
                        int alt21=2;
                        int LA21_0 = input.LA(1);

                        if ( (LA21_0==RULE_OP||LA21_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                            alt21=1;
                        }


                        switch (alt21) {
                    	case 1 :
                    	    // InternalPDDL.g:1778:5: (lv_leAnd2_20_0= ruleLogicalExpressionDM )
                    	    {
                    	    // InternalPDDL.g:1778:5: (lv_leAnd2_20_0= ruleLogicalExpressionDM )
                    	    // InternalPDDL.g:1779:6: lv_leAnd2_20_0= ruleLogicalExpressionDM
                    	    {

                    	    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeAnd2LogicalExpressionDMParserRuleCall_5_3_0());
                    	    					
                    	    pushFollow(FOLLOW_30);
                    	    lv_leAnd2_20_0=ruleLogicalExpressionDM();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"leAnd2",
                    	    							lv_leAnd2_20_0,
                    	    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt21 >= 1 ) break loop21;
                                EarlyExitException eee =
                                    new EarlyExitException(21, input);
                                throw eee;
                        }
                        cnt21++;
                    } while (true);

                    this_CP_21=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_21, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_5_4());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalPDDL.g:1802:3: (this_OP_22= RULE_OP otherlv_23= 'or' ( (lv_leOr1_24_0= ruleLogicalExpressionDM ) ) ( (lv_leOr2_25_0= ruleLogicalExpressionDM ) )+ this_CP_26= RULE_CP )
                    {
                    // InternalPDDL.g:1802:3: (this_OP_22= RULE_OP otherlv_23= 'or' ( (lv_leOr1_24_0= ruleLogicalExpressionDM ) ) ( (lv_leOr2_25_0= ruleLogicalExpressionDM ) )+ this_CP_26= RULE_CP )
                    // InternalPDDL.g:1803:4: this_OP_22= RULE_OP otherlv_23= 'or' ( (lv_leOr1_24_0= ruleLogicalExpressionDM ) ) ( (lv_leOr2_25_0= ruleLogicalExpressionDM ) )+ this_CP_26= RULE_CP
                    {
                    this_OP_22=(Token)match(input,RULE_OP,FOLLOW_33); 

                    				newLeafNode(this_OP_22, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_6_0());
                    			
                    otherlv_23=(Token)match(input,35,FOLLOW_28); 

                    				newLeafNode(otherlv_23, grammarAccess.getLogicalExpressionDMAccess().getOrKeyword_6_1());
                    			
                    // InternalPDDL.g:1811:4: ( (lv_leOr1_24_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:1812:5: (lv_leOr1_24_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:1812:5: (lv_leOr1_24_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:1813:6: lv_leOr1_24_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeOr1LogicalExpressionDMParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_leOr1_24_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"leOr1",
                    							lv_leOr1_24_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:1830:4: ( (lv_leOr2_25_0= ruleLogicalExpressionDM ) )+
                    int cnt22=0;
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==RULE_OP||LA22_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // InternalPDDL.g:1831:5: (lv_leOr2_25_0= ruleLogicalExpressionDM )
                    	    {
                    	    // InternalPDDL.g:1831:5: (lv_leOr2_25_0= ruleLogicalExpressionDM )
                    	    // InternalPDDL.g:1832:6: lv_leOr2_25_0= ruleLogicalExpressionDM
                    	    {

                    	    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeOr2LogicalExpressionDMParserRuleCall_6_3_0());
                    	    					
                    	    pushFollow(FOLLOW_30);
                    	    lv_leOr2_25_0=ruleLogicalExpressionDM();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"leOr2",
                    	    							lv_leOr2_25_0,
                    	    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt22 >= 1 ) break loop22;
                                EarlyExitException eee =
                                    new EarlyExitException(22, input);
                                throw eee;
                        }
                        cnt22++;
                    } while (true);

                    this_CP_26=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_26, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_6_4());
                    			

                    }


                    }
                    break;
                case 8 :
                    // InternalPDDL.g:1855:3: (this_OP_27= RULE_OP otherlv_28= 'imply' ( (lv_leImply1_29_0= ruleLogicalExpressionDM ) ) ( (lv_leImply2_30_0= ruleLogicalExpressionDM ) ) this_CP_31= RULE_CP )
                    {
                    // InternalPDDL.g:1855:3: (this_OP_27= RULE_OP otherlv_28= 'imply' ( (lv_leImply1_29_0= ruleLogicalExpressionDM ) ) ( (lv_leImply2_30_0= ruleLogicalExpressionDM ) ) this_CP_31= RULE_CP )
                    // InternalPDDL.g:1856:4: this_OP_27= RULE_OP otherlv_28= 'imply' ( (lv_leImply1_29_0= ruleLogicalExpressionDM ) ) ( (lv_leImply2_30_0= ruleLogicalExpressionDM ) ) this_CP_31= RULE_CP
                    {
                    this_OP_27=(Token)match(input,RULE_OP,FOLLOW_34); 

                    				newLeafNode(this_OP_27, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_7_0());
                    			
                    otherlv_28=(Token)match(input,36,FOLLOW_28); 

                    				newLeafNode(otherlv_28, grammarAccess.getLogicalExpressionDMAccess().getImplyKeyword_7_1());
                    			
                    // InternalPDDL.g:1864:4: ( (lv_leImply1_29_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:1865:5: (lv_leImply1_29_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:1865:5: (lv_leImply1_29_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:1866:6: lv_leImply1_29_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeImply1LogicalExpressionDMParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_leImply1_29_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"leImply1",
                    							lv_leImply1_29_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:1883:4: ( (lv_leImply2_30_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:1884:5: (lv_leImply2_30_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:1884:5: (lv_leImply2_30_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:1885:6: lv_leImply2_30_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeImply2LogicalExpressionDMParserRuleCall_7_3_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leImply2_30_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"leImply2",
                    							lv_leImply2_30_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_31=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_31, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_7_4());
                    			

                    }


                    }
                    break;
                case 9 :
                    // InternalPDDL.g:1908:3: (this_OP_32= RULE_OP otherlv_33= 'forall' ( (lv_tdForall_34_0= ruleTypeDescr ) ) ( (lv_leForall_35_0= ruleLogicalExpressionDM ) ) this_CP_36= RULE_CP )
                    {
                    // InternalPDDL.g:1908:3: (this_OP_32= RULE_OP otherlv_33= 'forall' ( (lv_tdForall_34_0= ruleTypeDescr ) ) ( (lv_leForall_35_0= ruleLogicalExpressionDM ) ) this_CP_36= RULE_CP )
                    // InternalPDDL.g:1909:4: this_OP_32= RULE_OP otherlv_33= 'forall' ( (lv_tdForall_34_0= ruleTypeDescr ) ) ( (lv_leForall_35_0= ruleLogicalExpressionDM ) ) this_CP_36= RULE_CP
                    {
                    this_OP_32=(Token)match(input,RULE_OP,FOLLOW_35); 

                    				newLeafNode(this_OP_32, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_8_0());
                    			
                    otherlv_33=(Token)match(input,37,FOLLOW_4); 

                    				newLeafNode(otherlv_33, grammarAccess.getLogicalExpressionDMAccess().getForallKeyword_8_1());
                    			
                    // InternalPDDL.g:1917:4: ( (lv_tdForall_34_0= ruleTypeDescr ) )
                    // InternalPDDL.g:1918:5: (lv_tdForall_34_0= ruleTypeDescr )
                    {
                    // InternalPDDL.g:1918:5: (lv_tdForall_34_0= ruleTypeDescr )
                    // InternalPDDL.g:1919:6: lv_tdForall_34_0= ruleTypeDescr
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getTdForallTypeDescrParserRuleCall_8_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_tdForall_34_0=ruleTypeDescr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"tdForall",
                    							lv_tdForall_34_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.TypeDescr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:1936:4: ( (lv_leForall_35_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:1937:5: (lv_leForall_35_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:1937:5: (lv_leForall_35_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:1938:6: lv_leForall_35_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeForallLogicalExpressionDMParserRuleCall_8_3_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leForall_35_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"leForall",
                    							lv_leForall_35_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_36=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_36, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_8_4());
                    			

                    }


                    }
                    break;
                case 10 :
                    // InternalPDDL.g:1961:3: (this_OP_37= RULE_OP otherlv_38= 'exists' ( (lv_tdExists_39_0= ruleTypeDescr ) ) ( (lv_leExists_40_0= ruleLogicalExpressionDM ) ) this_CP_41= RULE_CP )
                    {
                    // InternalPDDL.g:1961:3: (this_OP_37= RULE_OP otherlv_38= 'exists' ( (lv_tdExists_39_0= ruleTypeDescr ) ) ( (lv_leExists_40_0= ruleLogicalExpressionDM ) ) this_CP_41= RULE_CP )
                    // InternalPDDL.g:1962:4: this_OP_37= RULE_OP otherlv_38= 'exists' ( (lv_tdExists_39_0= ruleTypeDescr ) ) ( (lv_leExists_40_0= ruleLogicalExpressionDM ) ) this_CP_41= RULE_CP
                    {
                    this_OP_37=(Token)match(input,RULE_OP,FOLLOW_36); 

                    				newLeafNode(this_OP_37, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_9_0());
                    			
                    otherlv_38=(Token)match(input,38,FOLLOW_4); 

                    				newLeafNode(otherlv_38, grammarAccess.getLogicalExpressionDMAccess().getExistsKeyword_9_1());
                    			
                    // InternalPDDL.g:1970:4: ( (lv_tdExists_39_0= ruleTypeDescr ) )
                    // InternalPDDL.g:1971:5: (lv_tdExists_39_0= ruleTypeDescr )
                    {
                    // InternalPDDL.g:1971:5: (lv_tdExists_39_0= ruleTypeDescr )
                    // InternalPDDL.g:1972:6: lv_tdExists_39_0= ruleTypeDescr
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getTdExistsTypeDescrParserRuleCall_9_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_tdExists_39_0=ruleTypeDescr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"tdExists",
                    							lv_tdExists_39_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.TypeDescr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:1989:4: ( (lv_leExists_40_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:1990:5: (lv_leExists_40_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:1990:5: (lv_leExists_40_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:1991:6: lv_leExists_40_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeExistsLogicalExpressionDMParserRuleCall_9_3_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leExists_40_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"leExists",
                    							lv_leExists_40_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_41=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_41, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_9_4());
                    			

                    }


                    }
                    break;
                case 11 :
                    // InternalPDDL.g:2014:3: (this_OP_42= RULE_OP otherlv_43= 'when' ( (lv_leWhen1_44_0= ruleLogicalExpressionDM ) ) ( (lv_leWhen2_45_0= ruleLogicalExpressionDM ) ) this_CP_46= RULE_CP )
                    {
                    // InternalPDDL.g:2014:3: (this_OP_42= RULE_OP otherlv_43= 'when' ( (lv_leWhen1_44_0= ruleLogicalExpressionDM ) ) ( (lv_leWhen2_45_0= ruleLogicalExpressionDM ) ) this_CP_46= RULE_CP )
                    // InternalPDDL.g:2015:4: this_OP_42= RULE_OP otherlv_43= 'when' ( (lv_leWhen1_44_0= ruleLogicalExpressionDM ) ) ( (lv_leWhen2_45_0= ruleLogicalExpressionDM ) ) this_CP_46= RULE_CP
                    {
                    this_OP_42=(Token)match(input,RULE_OP,FOLLOW_37); 

                    				newLeafNode(this_OP_42, grammarAccess.getLogicalExpressionDMAccess().getOPTerminalRuleCall_10_0());
                    			
                    otherlv_43=(Token)match(input,39,FOLLOW_28); 

                    				newLeafNode(otherlv_43, grammarAccess.getLogicalExpressionDMAccess().getWhenKeyword_10_1());
                    			
                    // InternalPDDL.g:2023:4: ( (lv_leWhen1_44_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:2024:5: (lv_leWhen1_44_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:2024:5: (lv_leWhen1_44_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:2025:6: lv_leWhen1_44_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeWhen1LogicalExpressionDMParserRuleCall_10_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_leWhen1_44_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"leWhen1",
                    							lv_leWhen1_44_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:2042:4: ( (lv_leWhen2_45_0= ruleLogicalExpressionDM ) )
                    // InternalPDDL.g:2043:5: (lv_leWhen2_45_0= ruleLogicalExpressionDM )
                    {
                    // InternalPDDL.g:2043:5: (lv_leWhen2_45_0= ruleLogicalExpressionDM )
                    // InternalPDDL.g:2044:6: lv_leWhen2_45_0= ruleLogicalExpressionDM
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionDMAccess().getLeWhen2LogicalExpressionDMParserRuleCall_10_3_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leWhen2_45_0=ruleLogicalExpressionDM();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionDMRule());
                    						}
                    						set(
                    							current,
                    							"leWhen2",
                    							lv_leWhen2_45_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_46=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_46, grammarAccess.getLogicalExpressionDMAccess().getCPTerminalRuleCall_10_4());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicalExpressionDM"


    // $ANTLR start "entryRulePredicatDM"
    // InternalPDDL.g:2070:1: entryRulePredicatDM returns [EObject current=null] : iv_rulePredicatDM= rulePredicatDM EOF ;
    public final EObject entryRulePredicatDM() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePredicatDM = null;


        try {
            // InternalPDDL.g:2070:51: (iv_rulePredicatDM= rulePredicatDM EOF )
            // InternalPDDL.g:2071:2: iv_rulePredicatDM= rulePredicatDM EOF
            {
             newCompositeNode(grammarAccess.getPredicatDMRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePredicatDM=rulePredicatDM();

            state._fsp--;

             current =iv_rulePredicatDM; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePredicatDM"


    // $ANTLR start "rulePredicatDM"
    // InternalPDDL.g:2077:1: rulePredicatDM returns [EObject current=null] : ( ( (lv_predicatName_0_0= RULE_NAME ) ) ( (lv_variablesList_1_0= ruleVariable ) )+ ) ;
    public final EObject rulePredicatDM() throws RecognitionException {
        EObject current = null;

        Token lv_predicatName_0_0=null;
        EObject lv_variablesList_1_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2083:2: ( ( ( (lv_predicatName_0_0= RULE_NAME ) ) ( (lv_variablesList_1_0= ruleVariable ) )+ ) )
            // InternalPDDL.g:2084:2: ( ( (lv_predicatName_0_0= RULE_NAME ) ) ( (lv_variablesList_1_0= ruleVariable ) )+ )
            {
            // InternalPDDL.g:2084:2: ( ( (lv_predicatName_0_0= RULE_NAME ) ) ( (lv_variablesList_1_0= ruleVariable ) )+ )
            // InternalPDDL.g:2085:3: ( (lv_predicatName_0_0= RULE_NAME ) ) ( (lv_variablesList_1_0= ruleVariable ) )+
            {
            // InternalPDDL.g:2085:3: ( (lv_predicatName_0_0= RULE_NAME ) )
            // InternalPDDL.g:2086:4: (lv_predicatName_0_0= RULE_NAME )
            {
            // InternalPDDL.g:2086:4: (lv_predicatName_0_0= RULE_NAME )
            // InternalPDDL.g:2087:5: lv_predicatName_0_0= RULE_NAME
            {
            lv_predicatName_0_0=(Token)match(input,RULE_NAME,FOLLOW_19); 

            					newLeafNode(lv_predicatName_0_0, grammarAccess.getPredicatDMAccess().getPredicatNameNAMETerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPredicatDMRule());
            					}
            					setWithLastConsumed(
            						current,
            						"predicatName",
            						lv_predicatName_0_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
            				

            }


            }

            // InternalPDDL.g:2103:3: ( (lv_variablesList_1_0= ruleVariable ) )+
            int cnt24=0;
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalPDDL.g:2104:4: (lv_variablesList_1_0= ruleVariable )
            	    {
            	    // InternalPDDL.g:2104:4: (lv_variablesList_1_0= ruleVariable )
            	    // InternalPDDL.g:2105:5: lv_variablesList_1_0= ruleVariable
            	    {

            	    					newCompositeNode(grammarAccess.getPredicatDMAccess().getVariablesListVariableParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_20);
            	    lv_variablesList_1_0=ruleVariable();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPredicatDMRule());
            	    					}
            	    					add(
            	    						current,
            	    						"variablesList",
            	    						lv_variablesList_1_0,
            	    						"fr.irisa.atsyra.pddl.xtext.PDDL.Variable");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt24 >= 1 ) break loop24;
                        EarlyExitException eee =
                            new EarlyExitException(24, input);
                        throw eee;
                }
                cnt24++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePredicatDM"


    // $ANTLR start "entryRuleTypeDescr"
    // InternalPDDL.g:2126:1: entryRuleTypeDescr returns [EObject current=null] : iv_ruleTypeDescr= ruleTypeDescr EOF ;
    public final EObject entryRuleTypeDescr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeDescr = null;


        try {
            // InternalPDDL.g:2126:50: (iv_ruleTypeDescr= ruleTypeDescr EOF )
            // InternalPDDL.g:2127:2: iv_ruleTypeDescr= ruleTypeDescr EOF
            {
             newCompositeNode(grammarAccess.getTypeDescrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeDescr=ruleTypeDescr();

            state._fsp--;

             current =iv_ruleTypeDescr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeDescr"


    // $ANTLR start "ruleTypeDescr"
    // InternalPDDL.g:2133:1: ruleTypeDescr returns [EObject current=null] : (this_OP_0= RULE_OP ( (lv_var_1_0= ruleVariable ) ) otherlv_2= '-' ( (lv_type_3_0= ruleType ) ) this_CP_4= RULE_CP ) ;
    public final EObject ruleTypeDescr() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_2=null;
        Token this_CP_4=null;
        EObject lv_var_1_0 = null;

        EObject lv_type_3_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2139:2: ( (this_OP_0= RULE_OP ( (lv_var_1_0= ruleVariable ) ) otherlv_2= '-' ( (lv_type_3_0= ruleType ) ) this_CP_4= RULE_CP ) )
            // InternalPDDL.g:2140:2: (this_OP_0= RULE_OP ( (lv_var_1_0= ruleVariable ) ) otherlv_2= '-' ( (lv_type_3_0= ruleType ) ) this_CP_4= RULE_CP )
            {
            // InternalPDDL.g:2140:2: (this_OP_0= RULE_OP ( (lv_var_1_0= ruleVariable ) ) otherlv_2= '-' ( (lv_type_3_0= ruleType ) ) this_CP_4= RULE_CP )
            // InternalPDDL.g:2141:3: this_OP_0= RULE_OP ( (lv_var_1_0= ruleVariable ) ) otherlv_2= '-' ( (lv_type_3_0= ruleType ) ) this_CP_4= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_19); 

            			newLeafNode(this_OP_0, grammarAccess.getTypeDescrAccess().getOPTerminalRuleCall_0());
            		
            // InternalPDDL.g:2145:3: ( (lv_var_1_0= ruleVariable ) )
            // InternalPDDL.g:2146:4: (lv_var_1_0= ruleVariable )
            {
            // InternalPDDL.g:2146:4: (lv_var_1_0= ruleVariable )
            // InternalPDDL.g:2147:5: lv_var_1_0= ruleVariable
            {

            					newCompositeNode(grammarAccess.getTypeDescrAccess().getVarVariableParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_17);
            lv_var_1_0=ruleVariable();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTypeDescrRule());
            					}
            					set(
            						current,
            						"var",
            						lv_var_1_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Variable");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,24,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getTypeDescrAccess().getHyphenMinusKeyword_2());
            		
            // InternalPDDL.g:2168:3: ( (lv_type_3_0= ruleType ) )
            // InternalPDDL.g:2169:4: (lv_type_3_0= ruleType )
            {
            // InternalPDDL.g:2169:4: (lv_type_3_0= ruleType )
            // InternalPDDL.g:2170:5: lv_type_3_0= ruleType
            {

            					newCompositeNode(grammarAccess.getTypeDescrAccess().getTypeTypeParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_5);
            lv_type_3_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTypeDescrRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_3_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_4=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_4, grammarAccess.getTypeDescrAccess().getCPTerminalRuleCall_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeDescr"


    // $ANTLR start "entryRuleAxiom"
    // InternalPDDL.g:2195:1: entryRuleAxiom returns [EObject current=null] : iv_ruleAxiom= ruleAxiom EOF ;
    public final EObject entryRuleAxiom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAxiom = null;


        try {
            // InternalPDDL.g:2195:46: (iv_ruleAxiom= ruleAxiom EOF )
            // InternalPDDL.g:2196:2: iv_ruleAxiom= ruleAxiom EOF
            {
             newCompositeNode(grammarAccess.getAxiomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAxiom=ruleAxiom();

            state._fsp--;

             current =iv_ruleAxiom; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAxiom"


    // $ANTLR start "ruleAxiom"
    // InternalPDDL.g:2202:1: ruleAxiom returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':axiom' ( (lv_axiomName_2_0= RULE_NAME ) ) ( (lv_vars_3_0= ruleVars ) ) ( (lv_context_4_0= ruleContext ) ) ( (lv_implies_5_0= ruleImplies ) ) this_CP_6= RULE_CP ) ;
    public final EObject ruleAxiom() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token lv_axiomName_2_0=null;
        Token this_CP_6=null;
        EObject lv_vars_3_0 = null;

        EObject lv_context_4_0 = null;

        EObject lv_implies_5_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2208:2: ( (this_OP_0= RULE_OP otherlv_1= ':axiom' ( (lv_axiomName_2_0= RULE_NAME ) ) ( (lv_vars_3_0= ruleVars ) ) ( (lv_context_4_0= ruleContext ) ) ( (lv_implies_5_0= ruleImplies ) ) this_CP_6= RULE_CP ) )
            // InternalPDDL.g:2209:2: (this_OP_0= RULE_OP otherlv_1= ':axiom' ( (lv_axiomName_2_0= RULE_NAME ) ) ( (lv_vars_3_0= ruleVars ) ) ( (lv_context_4_0= ruleContext ) ) ( (lv_implies_5_0= ruleImplies ) ) this_CP_6= RULE_CP )
            {
            // InternalPDDL.g:2209:2: (this_OP_0= RULE_OP otherlv_1= ':axiom' ( (lv_axiomName_2_0= RULE_NAME ) ) ( (lv_vars_3_0= ruleVars ) ) ( (lv_context_4_0= ruleContext ) ) ( (lv_implies_5_0= ruleImplies ) ) this_CP_6= RULE_CP )
            // InternalPDDL.g:2210:3: this_OP_0= RULE_OP otherlv_1= ':axiom' ( (lv_axiomName_2_0= RULE_NAME ) ) ( (lv_vars_3_0= ruleVars ) ) ( (lv_context_4_0= ruleContext ) ) ( (lv_implies_5_0= ruleImplies ) ) this_CP_6= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_38); 

            			newLeafNode(this_OP_0, grammarAccess.getAxiomAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,40,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getAxiomAccess().getAxiomKeyword_1());
            		
            // InternalPDDL.g:2218:3: ( (lv_axiomName_2_0= RULE_NAME ) )
            // InternalPDDL.g:2219:4: (lv_axiomName_2_0= RULE_NAME )
            {
            // InternalPDDL.g:2219:4: (lv_axiomName_2_0= RULE_NAME )
            // InternalPDDL.g:2220:5: lv_axiomName_2_0= RULE_NAME
            {
            lv_axiomName_2_0=(Token)match(input,RULE_NAME,FOLLOW_39); 

            					newLeafNode(lv_axiomName_2_0, grammarAccess.getAxiomAccess().getAxiomNameNAMETerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAxiomRule());
            					}
            					setWithLastConsumed(
            						current,
            						"axiomName",
            						lv_axiomName_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
            				

            }


            }

            // InternalPDDL.g:2236:3: ( (lv_vars_3_0= ruleVars ) )
            // InternalPDDL.g:2237:4: (lv_vars_3_0= ruleVars )
            {
            // InternalPDDL.g:2237:4: (lv_vars_3_0= ruleVars )
            // InternalPDDL.g:2238:5: lv_vars_3_0= ruleVars
            {

            					newCompositeNode(grammarAccess.getAxiomAccess().getVarsVarsParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_40);
            lv_vars_3_0=ruleVars();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAxiomRule());
            					}
            					set(
            						current,
            						"vars",
            						lv_vars_3_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Vars");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPDDL.g:2255:3: ( (lv_context_4_0= ruleContext ) )
            // InternalPDDL.g:2256:4: (lv_context_4_0= ruleContext )
            {
            // InternalPDDL.g:2256:4: (lv_context_4_0= ruleContext )
            // InternalPDDL.g:2257:5: lv_context_4_0= ruleContext
            {

            					newCompositeNode(grammarAccess.getAxiomAccess().getContextContextParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_41);
            lv_context_4_0=ruleContext();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAxiomRule());
            					}
            					set(
            						current,
            						"context",
            						lv_context_4_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Context");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPDDL.g:2274:3: ( (lv_implies_5_0= ruleImplies ) )
            // InternalPDDL.g:2275:4: (lv_implies_5_0= ruleImplies )
            {
            // InternalPDDL.g:2275:4: (lv_implies_5_0= ruleImplies )
            // InternalPDDL.g:2276:5: lv_implies_5_0= ruleImplies
            {

            					newCompositeNode(grammarAccess.getAxiomAccess().getImpliesImpliesParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_5);
            lv_implies_5_0=ruleImplies();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAxiomRule());
            					}
            					set(
            						current,
            						"implies",
            						lv_implies_5_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Implies");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_6=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_6, grammarAccess.getAxiomAccess().getCPTerminalRuleCall_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAxiom"


    // $ANTLR start "entryRuleVars"
    // InternalPDDL.g:2301:1: entryRuleVars returns [EObject current=null] : iv_ruleVars= ruleVars EOF ;
    public final EObject entryRuleVars() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVars = null;


        try {
            // InternalPDDL.g:2301:45: (iv_ruleVars= ruleVars EOF )
            // InternalPDDL.g:2302:2: iv_ruleVars= ruleVars EOF
            {
             newCompositeNode(grammarAccess.getVarsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVars=ruleVars();

            state._fsp--;

             current =iv_ruleVars; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVars"


    // $ANTLR start "ruleVars"
    // InternalPDDL.g:2308:1: ruleVars returns [EObject current=null] : (otherlv_0= ':vars' this_OP_1= RULE_OP this_TypedVariablesList_2= ruleTypedVariablesList this_CP_3= RULE_CP ) ;
    public final EObject ruleVars() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token this_OP_1=null;
        Token this_CP_3=null;
        EObject this_TypedVariablesList_2 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2314:2: ( (otherlv_0= ':vars' this_OP_1= RULE_OP this_TypedVariablesList_2= ruleTypedVariablesList this_CP_3= RULE_CP ) )
            // InternalPDDL.g:2315:2: (otherlv_0= ':vars' this_OP_1= RULE_OP this_TypedVariablesList_2= ruleTypedVariablesList this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:2315:2: (otherlv_0= ':vars' this_OP_1= RULE_OP this_TypedVariablesList_2= ruleTypedVariablesList this_CP_3= RULE_CP )
            // InternalPDDL.g:2316:3: otherlv_0= ':vars' this_OP_1= RULE_OP this_TypedVariablesList_2= ruleTypedVariablesList this_CP_3= RULE_CP
            {
            otherlv_0=(Token)match(input,41,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getVarsAccess().getVarsKeyword_0());
            		
            this_OP_1=(Token)match(input,RULE_OP,FOLLOW_19); 

            			newLeafNode(this_OP_1, grammarAccess.getVarsAccess().getOPTerminalRuleCall_1());
            		

            			newCompositeNode(grammarAccess.getVarsAccess().getTypedVariablesListParserRuleCall_2());
            		
            pushFollow(FOLLOW_5);
            this_TypedVariablesList_2=ruleTypedVariablesList();

            state._fsp--;


            			current = this_TypedVariablesList_2;
            			afterParserOrEnumRuleCall();
            		
            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getVarsAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVars"


    // $ANTLR start "entryRuleContext"
    // InternalPDDL.g:2340:1: entryRuleContext returns [EObject current=null] : iv_ruleContext= ruleContext EOF ;
    public final EObject entryRuleContext() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContext = null;


        try {
            // InternalPDDL.g:2340:48: (iv_ruleContext= ruleContext EOF )
            // InternalPDDL.g:2341:2: iv_ruleContext= ruleContext EOF
            {
             newCompositeNode(grammarAccess.getContextRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleContext=ruleContext();

            state._fsp--;

             current =iv_ruleContext; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContext"


    // $ANTLR start "ruleContext"
    // InternalPDDL.g:2347:1: ruleContext returns [EObject current=null] : (otherlv_0= ':context' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleContext() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token this_OP_1=null;
        Token this_CP_3=null;
        EObject lv_le_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2353:2: ( (otherlv_0= ':context' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:2354:2: (otherlv_0= ':context' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:2354:2: (otherlv_0= ':context' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:2355:3: otherlv_0= ':context' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP
            {
            otherlv_0=(Token)match(input,42,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getContextAccess().getContextKeyword_0());
            		
            this_OP_1=(Token)match(input,RULE_OP,FOLLOW_28); 

            			newLeafNode(this_OP_1, grammarAccess.getContextAccess().getOPTerminalRuleCall_1());
            		
            // InternalPDDL.g:2363:3: ( (lv_le_2_0= ruleLogicalExpressionDM ) )
            // InternalPDDL.g:2364:4: (lv_le_2_0= ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:2364:4: (lv_le_2_0= ruleLogicalExpressionDM )
            // InternalPDDL.g:2365:5: lv_le_2_0= ruleLogicalExpressionDM
            {

            					newCompositeNode(grammarAccess.getContextAccess().getLeLogicalExpressionDMParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_le_2_0=ruleLogicalExpressionDM();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getContextRule());
            					}
            					set(
            						current,
            						"le",
            						lv_le_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getContextAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContext"


    // $ANTLR start "entryRuleImplies"
    // InternalPDDL.g:2390:1: entryRuleImplies returns [EObject current=null] : iv_ruleImplies= ruleImplies EOF ;
    public final EObject entryRuleImplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplies = null;


        try {
            // InternalPDDL.g:2390:48: (iv_ruleImplies= ruleImplies EOF )
            // InternalPDDL.g:2391:2: iv_ruleImplies= ruleImplies EOF
            {
             newCompositeNode(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImplies=ruleImplies();

            state._fsp--;

             current =iv_ruleImplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalPDDL.g:2397:1: ruleImplies returns [EObject current=null] : (otherlv_0= ':implies' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleImplies() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token this_OP_1=null;
        Token this_CP_3=null;
        EObject lv_le_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2403:2: ( (otherlv_0= ':implies' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:2404:2: (otherlv_0= ':implies' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:2404:2: (otherlv_0= ':implies' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:2405:3: otherlv_0= ':implies' this_OP_1= RULE_OP ( (lv_le_2_0= ruleLogicalExpressionDM ) ) this_CP_3= RULE_CP
            {
            otherlv_0=(Token)match(input,43,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getImpliesAccess().getImpliesKeyword_0());
            		
            this_OP_1=(Token)match(input,RULE_OP,FOLLOW_28); 

            			newLeafNode(this_OP_1, grammarAccess.getImpliesAccess().getOPTerminalRuleCall_1());
            		
            // InternalPDDL.g:2413:3: ( (lv_le_2_0= ruleLogicalExpressionDM ) )
            // InternalPDDL.g:2414:4: (lv_le_2_0= ruleLogicalExpressionDM )
            {
            // InternalPDDL.g:2414:4: (lv_le_2_0= ruleLogicalExpressionDM )
            // InternalPDDL.g:2415:5: lv_le_2_0= ruleLogicalExpressionDM
            {

            					newCompositeNode(grammarAccess.getImpliesAccess().getLeLogicalExpressionDMParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_le_2_0=ruleLogicalExpressionDM();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImpliesRule());
            					}
            					set(
            						current,
            						"le",
            						lv_le_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionDM");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getImpliesAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleProblem"
    // InternalPDDL.g:2440:1: entryRuleProblem returns [EObject current=null] : iv_ruleProblem= ruleProblem EOF ;
    public final EObject entryRuleProblem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProblem = null;


        try {
            // InternalPDDL.g:2440:48: (iv_ruleProblem= ruleProblem EOF )
            // InternalPDDL.g:2441:2: iv_ruleProblem= ruleProblem EOF
            {
             newCompositeNode(grammarAccess.getProblemRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProblem=ruleProblem();

            state._fsp--;

             current =iv_ruleProblem; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProblem"


    // $ANTLR start "ruleProblem"
    // InternalPDDL.g:2447:1: ruleProblem returns [EObject current=null] : ( ( (lv_domainRef_0_0= ruleDomainRef ) ) ( (lv_objects_1_0= ruleObjects ) )? ( (lv_init_2_0= ruleInit ) )? ( (lv_goal_3_0= ruleGoal ) )? ) ;
    public final EObject ruleProblem() throws RecognitionException {
        EObject current = null;

        EObject lv_domainRef_0_0 = null;

        EObject lv_objects_1_0 = null;

        EObject lv_init_2_0 = null;

        EObject lv_goal_3_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2453:2: ( ( ( (lv_domainRef_0_0= ruleDomainRef ) ) ( (lv_objects_1_0= ruleObjects ) )? ( (lv_init_2_0= ruleInit ) )? ( (lv_goal_3_0= ruleGoal ) )? ) )
            // InternalPDDL.g:2454:2: ( ( (lv_domainRef_0_0= ruleDomainRef ) ) ( (lv_objects_1_0= ruleObjects ) )? ( (lv_init_2_0= ruleInit ) )? ( (lv_goal_3_0= ruleGoal ) )? )
            {
            // InternalPDDL.g:2454:2: ( ( (lv_domainRef_0_0= ruleDomainRef ) ) ( (lv_objects_1_0= ruleObjects ) )? ( (lv_init_2_0= ruleInit ) )? ( (lv_goal_3_0= ruleGoal ) )? )
            // InternalPDDL.g:2455:3: ( (lv_domainRef_0_0= ruleDomainRef ) ) ( (lv_objects_1_0= ruleObjects ) )? ( (lv_init_2_0= ruleInit ) )? ( (lv_goal_3_0= ruleGoal ) )?
            {
            // InternalPDDL.g:2455:3: ( (lv_domainRef_0_0= ruleDomainRef ) )
            // InternalPDDL.g:2456:4: (lv_domainRef_0_0= ruleDomainRef )
            {
            // InternalPDDL.g:2456:4: (lv_domainRef_0_0= ruleDomainRef )
            // InternalPDDL.g:2457:5: lv_domainRef_0_0= ruleDomainRef
            {

            					newCompositeNode(grammarAccess.getProblemAccess().getDomainRefDomainRefParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_9);
            lv_domainRef_0_0=ruleDomainRef();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getProblemRule());
            					}
            					set(
            						current,
            						"domainRef",
            						lv_domainRef_0_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.DomainRef");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPDDL.g:2474:3: ( (lv_objects_1_0= ruleObjects ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==RULE_OP) ) {
                int LA25_1 = input.LA(2);

                if ( (LA25_1==45) ) {
                    alt25=1;
                }
            }
            switch (alt25) {
                case 1 :
                    // InternalPDDL.g:2475:4: (lv_objects_1_0= ruleObjects )
                    {
                    // InternalPDDL.g:2475:4: (lv_objects_1_0= ruleObjects )
                    // InternalPDDL.g:2476:5: lv_objects_1_0= ruleObjects
                    {

                    					newCompositeNode(grammarAccess.getProblemAccess().getObjectsObjectsParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_objects_1_0=ruleObjects();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getProblemRule());
                    					}
                    					set(
                    						current,
                    						"objects",
                    						lv_objects_1_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Objects");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPDDL.g:2493:3: ( (lv_init_2_0= ruleInit ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==RULE_OP) ) {
                int LA26_1 = input.LA(2);

                if ( (LA26_1==46) ) {
                    alt26=1;
                }
            }
            switch (alt26) {
                case 1 :
                    // InternalPDDL.g:2494:4: (lv_init_2_0= ruleInit )
                    {
                    // InternalPDDL.g:2494:4: (lv_init_2_0= ruleInit )
                    // InternalPDDL.g:2495:5: lv_init_2_0= ruleInit
                    {

                    					newCompositeNode(grammarAccess.getProblemAccess().getInitInitParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_init_2_0=ruleInit();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getProblemRule());
                    					}
                    					set(
                    						current,
                    						"init",
                    						lv_init_2_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Init");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPDDL.g:2512:3: ( (lv_goal_3_0= ruleGoal ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==RULE_OP) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalPDDL.g:2513:4: (lv_goal_3_0= ruleGoal )
                    {
                    // InternalPDDL.g:2513:4: (lv_goal_3_0= ruleGoal )
                    // InternalPDDL.g:2514:5: lv_goal_3_0= ruleGoal
                    {

                    					newCompositeNode(grammarAccess.getProblemAccess().getGoalGoalParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_goal_3_0=ruleGoal();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getProblemRule());
                    					}
                    					set(
                    						current,
                    						"goal",
                    						lv_goal_3_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Goal");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProblem"


    // $ANTLR start "entryRuleDomainRef"
    // InternalPDDL.g:2535:1: entryRuleDomainRef returns [EObject current=null] : iv_ruleDomainRef= ruleDomainRef EOF ;
    public final EObject entryRuleDomainRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainRef = null;


        try {
            // InternalPDDL.g:2535:50: (iv_ruleDomainRef= ruleDomainRef EOF )
            // InternalPDDL.g:2536:2: iv_ruleDomainRef= ruleDomainRef EOF
            {
             newCompositeNode(grammarAccess.getDomainRefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainRef=ruleDomainRef();

            state._fsp--;

             current =iv_ruleDomainRef; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainRef"


    // $ANTLR start "ruleDomainRef"
    // InternalPDDL.g:2542:1: ruleDomainRef returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':domain' ( (lv_domainName_2_0= RULE_NAME ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleDomainRef() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token lv_domainName_2_0=null;
        Token this_CP_3=null;


        	enterRule();

        try {
            // InternalPDDL.g:2548:2: ( (this_OP_0= RULE_OP otherlv_1= ':domain' ( (lv_domainName_2_0= RULE_NAME ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:2549:2: (this_OP_0= RULE_OP otherlv_1= ':domain' ( (lv_domainName_2_0= RULE_NAME ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:2549:2: (this_OP_0= RULE_OP otherlv_1= ':domain' ( (lv_domainName_2_0= RULE_NAME ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:2550:3: this_OP_0= RULE_OP otherlv_1= ':domain' ( (lv_domainName_2_0= RULE_NAME ) ) this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_42); 

            			newLeafNode(this_OP_0, grammarAccess.getDomainRefAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,44,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getDomainRefAccess().getDomainKeyword_1());
            		
            // InternalPDDL.g:2558:3: ( (lv_domainName_2_0= RULE_NAME ) )
            // InternalPDDL.g:2559:4: (lv_domainName_2_0= RULE_NAME )
            {
            // InternalPDDL.g:2559:4: (lv_domainName_2_0= RULE_NAME )
            // InternalPDDL.g:2560:5: lv_domainName_2_0= RULE_NAME
            {
            lv_domainName_2_0=(Token)match(input,RULE_NAME,FOLLOW_5); 

            					newLeafNode(lv_domainName_2_0, grammarAccess.getDomainRefAccess().getDomainNameNAMETerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDomainRefRule());
            					}
            					setWithLastConsumed(
            						current,
            						"domainName",
            						lv_domainName_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getDomainRefAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainRef"


    // $ANTLR start "entryRuleObjects"
    // InternalPDDL.g:2584:1: entryRuleObjects returns [EObject current=null] : iv_ruleObjects= ruleObjects EOF ;
    public final EObject entryRuleObjects() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjects = null;


        try {
            // InternalPDDL.g:2584:48: (iv_ruleObjects= ruleObjects EOF )
            // InternalPDDL.g:2585:2: iv_ruleObjects= ruleObjects EOF
            {
             newCompositeNode(grammarAccess.getObjectsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleObjects=ruleObjects();

            state._fsp--;

             current =iv_ruleObjects; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjects"


    // $ANTLR start "ruleObjects"
    // InternalPDDL.g:2591:1: ruleObjects returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':objects' ( (lv_newObjectsList_2_0= ruleObjectsList ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleObjects() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token this_CP_3=null;
        EObject lv_newObjectsList_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2597:2: ( (this_OP_0= RULE_OP otherlv_1= ':objects' ( (lv_newObjectsList_2_0= ruleObjectsList ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:2598:2: (this_OP_0= RULE_OP otherlv_1= ':objects' ( (lv_newObjectsList_2_0= ruleObjectsList ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:2598:2: (this_OP_0= RULE_OP otherlv_1= ':objects' ( (lv_newObjectsList_2_0= ruleObjectsList ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:2599:3: this_OP_0= RULE_OP otherlv_1= ':objects' ( (lv_newObjectsList_2_0= ruleObjectsList ) ) this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_43); 

            			newLeafNode(this_OP_0, grammarAccess.getObjectsAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,45,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getObjectsAccess().getObjectsKeyword_1());
            		
            // InternalPDDL.g:2607:3: ( (lv_newObjectsList_2_0= ruleObjectsList ) )
            // InternalPDDL.g:2608:4: (lv_newObjectsList_2_0= ruleObjectsList )
            {
            // InternalPDDL.g:2608:4: (lv_newObjectsList_2_0= ruleObjectsList )
            // InternalPDDL.g:2609:5: lv_newObjectsList_2_0= ruleObjectsList
            {

            					newCompositeNode(grammarAccess.getObjectsAccess().getNewObjectsListObjectsListParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_newObjectsList_2_0=ruleObjectsList();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getObjectsRule());
            					}
            					set(
            						current,
            						"newObjectsList",
            						lv_newObjectsList_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.ObjectsList");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getObjectsAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjects"


    // $ANTLR start "entryRuleObjectsList"
    // InternalPDDL.g:2634:1: entryRuleObjectsList returns [EObject current=null] : iv_ruleObjectsList= ruleObjectsList EOF ;
    public final EObject entryRuleObjectsList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectsList = null;


        try {
            // InternalPDDL.g:2634:52: (iv_ruleObjectsList= ruleObjectsList EOF )
            // InternalPDDL.g:2635:2: iv_ruleObjectsList= ruleObjectsList EOF
            {
             newCompositeNode(grammarAccess.getObjectsListRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleObjectsList=ruleObjectsList();

            state._fsp--;

             current =iv_ruleObjectsList; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectsList"


    // $ANTLR start "ruleObjectsList"
    // InternalPDDL.g:2641:1: ruleObjectsList returns [EObject current=null] : ( (lv_newObj_0_0= ruleNewObjects ) )+ ;
    public final EObject ruleObjectsList() throws RecognitionException {
        EObject current = null;

        EObject lv_newObj_0_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2647:2: ( ( (lv_newObj_0_0= ruleNewObjects ) )+ )
            // InternalPDDL.g:2648:2: ( (lv_newObj_0_0= ruleNewObjects ) )+
            {
            // InternalPDDL.g:2648:2: ( (lv_newObj_0_0= ruleNewObjects ) )+
            int cnt28=0;
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==RULE_NAME) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalPDDL.g:2649:3: (lv_newObj_0_0= ruleNewObjects )
            	    {
            	    // InternalPDDL.g:2649:3: (lv_newObj_0_0= ruleNewObjects )
            	    // InternalPDDL.g:2650:4: lv_newObj_0_0= ruleNewObjects
            	    {

            	    				newCompositeNode(grammarAccess.getObjectsListAccess().getNewObjNewObjectsParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_11);
            	    lv_newObj_0_0=ruleNewObjects();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getObjectsListRule());
            	    				}
            	    				add(
            	    					current,
            	    					"newObj",
            	    					lv_newObj_0_0,
            	    					"fr.irisa.atsyra.pddl.xtext.PDDL.NewObjects");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt28 >= 1 ) break loop28;
                        EarlyExitException eee =
                            new EarlyExitException(28, input);
                        throw eee;
                }
                cnt28++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectsList"


    // $ANTLR start "entryRuleNewObjects"
    // InternalPDDL.g:2670:1: entryRuleNewObjects returns [EObject current=null] : iv_ruleNewObjects= ruleNewObjects EOF ;
    public final EObject entryRuleNewObjects() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNewObjects = null;


        try {
            // InternalPDDL.g:2670:51: (iv_ruleNewObjects= ruleNewObjects EOF )
            // InternalPDDL.g:2671:2: iv_ruleNewObjects= ruleNewObjects EOF
            {
             newCompositeNode(grammarAccess.getNewObjectsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNewObjects=ruleNewObjects();

            state._fsp--;

             current =iv_ruleNewObjects; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNewObjects"


    // $ANTLR start "ruleNewObjects"
    // InternalPDDL.g:2677:1: ruleNewObjects returns [EObject current=null] : ( ( (lv_newObject_0_0= rulePDDLObject ) )+ otherlv_1= '-' ( (lv_objectType_2_0= ruleType ) ) ) ;
    public final EObject ruleNewObjects() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_newObject_0_0 = null;

        EObject lv_objectType_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2683:2: ( ( ( (lv_newObject_0_0= rulePDDLObject ) )+ otherlv_1= '-' ( (lv_objectType_2_0= ruleType ) ) ) )
            // InternalPDDL.g:2684:2: ( ( (lv_newObject_0_0= rulePDDLObject ) )+ otherlv_1= '-' ( (lv_objectType_2_0= ruleType ) ) )
            {
            // InternalPDDL.g:2684:2: ( ( (lv_newObject_0_0= rulePDDLObject ) )+ otherlv_1= '-' ( (lv_objectType_2_0= ruleType ) ) )
            // InternalPDDL.g:2685:3: ( (lv_newObject_0_0= rulePDDLObject ) )+ otherlv_1= '-' ( (lv_objectType_2_0= ruleType ) )
            {
            // InternalPDDL.g:2685:3: ( (lv_newObject_0_0= rulePDDLObject ) )+
            int cnt29=0;
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==RULE_NAME) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalPDDL.g:2686:4: (lv_newObject_0_0= rulePDDLObject )
            	    {
            	    // InternalPDDL.g:2686:4: (lv_newObject_0_0= rulePDDLObject )
            	    // InternalPDDL.g:2687:5: lv_newObject_0_0= rulePDDLObject
            	    {

            	    					newCompositeNode(grammarAccess.getNewObjectsAccess().getNewObjectPDDLObjectParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_44);
            	    lv_newObject_0_0=rulePDDLObject();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getNewObjectsRule());
            	    					}
            	    					add(
            	    						current,
            	    						"newObject",
            	    						lv_newObject_0_0,
            	    						"fr.irisa.atsyra.pddl.xtext.PDDL.PDDLObject");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt29 >= 1 ) break loop29;
                        EarlyExitException eee =
                            new EarlyExitException(29, input);
                        throw eee;
                }
                cnt29++;
            } while (true);

            otherlv_1=(Token)match(input,24,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getNewObjectsAccess().getHyphenMinusKeyword_1());
            		
            // InternalPDDL.g:2708:3: ( (lv_objectType_2_0= ruleType ) )
            // InternalPDDL.g:2709:4: (lv_objectType_2_0= ruleType )
            {
            // InternalPDDL.g:2709:4: (lv_objectType_2_0= ruleType )
            // InternalPDDL.g:2710:5: lv_objectType_2_0= ruleType
            {

            					newCompositeNode(grammarAccess.getNewObjectsAccess().getObjectTypeTypeParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_objectType_2_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNewObjectsRule());
            					}
            					set(
            						current,
            						"objectType",
            						lv_objectType_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNewObjects"


    // $ANTLR start "entryRuleInit"
    // InternalPDDL.g:2731:1: entryRuleInit returns [EObject current=null] : iv_ruleInit= ruleInit EOF ;
    public final EObject entryRuleInit() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInit = null;


        try {
            // InternalPDDL.g:2731:45: (iv_ruleInit= ruleInit EOF )
            // InternalPDDL.g:2732:2: iv_ruleInit= ruleInit EOF
            {
             newCompositeNode(grammarAccess.getInitRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInit=ruleInit();

            state._fsp--;

             current =iv_ruleInit; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInit"


    // $ANTLR start "ruleInit"
    // InternalPDDL.g:2738:1: ruleInit returns [EObject current=null] : ( () this_OP_1= RULE_OP otherlv_2= ':init' ( ( (lv_predicat_3_0= rulePredicatPB ) ) | (otherlv_4= 'not' ( (lv_negativePredicat_5_0= rulePredicatPB ) ) ) )* this_CP_6= RULE_CP ) ;
    public final EObject ruleInit() throws RecognitionException {
        EObject current = null;

        Token this_OP_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token this_CP_6=null;
        EObject lv_predicat_3_0 = null;

        EObject lv_negativePredicat_5_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2744:2: ( ( () this_OP_1= RULE_OP otherlv_2= ':init' ( ( (lv_predicat_3_0= rulePredicatPB ) ) | (otherlv_4= 'not' ( (lv_negativePredicat_5_0= rulePredicatPB ) ) ) )* this_CP_6= RULE_CP ) )
            // InternalPDDL.g:2745:2: ( () this_OP_1= RULE_OP otherlv_2= ':init' ( ( (lv_predicat_3_0= rulePredicatPB ) ) | (otherlv_4= 'not' ( (lv_negativePredicat_5_0= rulePredicatPB ) ) ) )* this_CP_6= RULE_CP )
            {
            // InternalPDDL.g:2745:2: ( () this_OP_1= RULE_OP otherlv_2= ':init' ( ( (lv_predicat_3_0= rulePredicatPB ) ) | (otherlv_4= 'not' ( (lv_negativePredicat_5_0= rulePredicatPB ) ) ) )* this_CP_6= RULE_CP )
            // InternalPDDL.g:2746:3: () this_OP_1= RULE_OP otherlv_2= ':init' ( ( (lv_predicat_3_0= rulePredicatPB ) ) | (otherlv_4= 'not' ( (lv_negativePredicat_5_0= rulePredicatPB ) ) ) )* this_CP_6= RULE_CP
            {
            // InternalPDDL.g:2746:3: ()
            // InternalPDDL.g:2747:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInitAccess().getInitAction_0(),
            					current);
            			

            }

            this_OP_1=(Token)match(input,RULE_OP,FOLLOW_45); 

            			newLeafNode(this_OP_1, grammarAccess.getInitAccess().getOPTerminalRuleCall_1());
            		
            otherlv_2=(Token)match(input,46,FOLLOW_46); 

            			newLeafNode(otherlv_2, grammarAccess.getInitAccess().getInitKeyword_2());
            		
            // InternalPDDL.g:2761:3: ( ( (lv_predicat_3_0= rulePredicatPB ) ) | (otherlv_4= 'not' ( (lv_negativePredicat_5_0= rulePredicatPB ) ) ) )*
            loop30:
            do {
                int alt30=3;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==RULE_OP) ) {
                    alt30=1;
                }
                else if ( (LA30_0==33) ) {
                    alt30=2;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalPDDL.g:2762:4: ( (lv_predicat_3_0= rulePredicatPB ) )
            	    {
            	    // InternalPDDL.g:2762:4: ( (lv_predicat_3_0= rulePredicatPB ) )
            	    // InternalPDDL.g:2763:5: (lv_predicat_3_0= rulePredicatPB )
            	    {
            	    // InternalPDDL.g:2763:5: (lv_predicat_3_0= rulePredicatPB )
            	    // InternalPDDL.g:2764:6: lv_predicat_3_0= rulePredicatPB
            	    {

            	    						newCompositeNode(grammarAccess.getInitAccess().getPredicatPredicatPBParserRuleCall_3_0_0());
            	    					
            	    pushFollow(FOLLOW_46);
            	    lv_predicat_3_0=rulePredicatPB();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getInitRule());
            	    						}
            	    						add(
            	    							current,
            	    							"predicat",
            	    							lv_predicat_3_0,
            	    							"fr.irisa.atsyra.pddl.xtext.PDDL.PredicatPB");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPDDL.g:2782:4: (otherlv_4= 'not' ( (lv_negativePredicat_5_0= rulePredicatPB ) ) )
            	    {
            	    // InternalPDDL.g:2782:4: (otherlv_4= 'not' ( (lv_negativePredicat_5_0= rulePredicatPB ) ) )
            	    // InternalPDDL.g:2783:5: otherlv_4= 'not' ( (lv_negativePredicat_5_0= rulePredicatPB ) )
            	    {
            	    otherlv_4=(Token)match(input,33,FOLLOW_4); 

            	    					newLeafNode(otherlv_4, grammarAccess.getInitAccess().getNotKeyword_3_1_0());
            	    				
            	    // InternalPDDL.g:2787:5: ( (lv_negativePredicat_5_0= rulePredicatPB ) )
            	    // InternalPDDL.g:2788:6: (lv_negativePredicat_5_0= rulePredicatPB )
            	    {
            	    // InternalPDDL.g:2788:6: (lv_negativePredicat_5_0= rulePredicatPB )
            	    // InternalPDDL.g:2789:7: lv_negativePredicat_5_0= rulePredicatPB
            	    {

            	    							newCompositeNode(grammarAccess.getInitAccess().getNegativePredicatPredicatPBParserRuleCall_3_1_1_0());
            	    						
            	    pushFollow(FOLLOW_46);
            	    lv_negativePredicat_5_0=rulePredicatPB();

            	    state._fsp--;


            	    							if (current==null) {
            	    								current = createModelElementForParent(grammarAccess.getInitRule());
            	    							}
            	    							add(
            	    								current,
            	    								"negativePredicat",
            	    								lv_negativePredicat_5_0,
            	    								"fr.irisa.atsyra.pddl.xtext.PDDL.PredicatPB");
            	    							afterParserOrEnumRuleCall();
            	    						

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            this_CP_6=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_6, grammarAccess.getInitAccess().getCPTerminalRuleCall_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInit"


    // $ANTLR start "entryRulePredicatPB"
    // InternalPDDL.g:2816:1: entryRulePredicatPB returns [EObject current=null] : iv_rulePredicatPB= rulePredicatPB EOF ;
    public final EObject entryRulePredicatPB() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePredicatPB = null;


        try {
            // InternalPDDL.g:2816:51: (iv_rulePredicatPB= rulePredicatPB EOF )
            // InternalPDDL.g:2817:2: iv_rulePredicatPB= rulePredicatPB EOF
            {
             newCompositeNode(grammarAccess.getPredicatPBRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePredicatPB=rulePredicatPB();

            state._fsp--;

             current =iv_rulePredicatPB; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePredicatPB"


    // $ANTLR start "rulePredicatPB"
    // InternalPDDL.g:2823:1: rulePredicatPB returns [EObject current=null] : (this_OP_0= RULE_OP ( (lv_predicatName_1_0= RULE_NAME ) ) ( (lv_objects_2_0= rulePDDLObject ) )* this_CP_3= RULE_CP ) ;
    public final EObject rulePredicatPB() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token lv_predicatName_1_0=null;
        Token this_CP_3=null;
        EObject lv_objects_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2829:2: ( (this_OP_0= RULE_OP ( (lv_predicatName_1_0= RULE_NAME ) ) ( (lv_objects_2_0= rulePDDLObject ) )* this_CP_3= RULE_CP ) )
            // InternalPDDL.g:2830:2: (this_OP_0= RULE_OP ( (lv_predicatName_1_0= RULE_NAME ) ) ( (lv_objects_2_0= rulePDDLObject ) )* this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:2830:2: (this_OP_0= RULE_OP ( (lv_predicatName_1_0= RULE_NAME ) ) ( (lv_objects_2_0= rulePDDLObject ) )* this_CP_3= RULE_CP )
            // InternalPDDL.g:2831:3: this_OP_0= RULE_OP ( (lv_predicatName_1_0= RULE_NAME ) ) ( (lv_objects_2_0= rulePDDLObject ) )* this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_7); 

            			newLeafNode(this_OP_0, grammarAccess.getPredicatPBAccess().getOPTerminalRuleCall_0());
            		
            // InternalPDDL.g:2835:3: ( (lv_predicatName_1_0= RULE_NAME ) )
            // InternalPDDL.g:2836:4: (lv_predicatName_1_0= RULE_NAME )
            {
            // InternalPDDL.g:2836:4: (lv_predicatName_1_0= RULE_NAME )
            // InternalPDDL.g:2837:5: lv_predicatName_1_0= RULE_NAME
            {
            lv_predicatName_1_0=(Token)match(input,RULE_NAME,FOLLOW_16); 

            					newLeafNode(lv_predicatName_1_0, grammarAccess.getPredicatPBAccess().getPredicatNameNAMETerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPredicatPBRule());
            					}
            					setWithLastConsumed(
            						current,
            						"predicatName",
            						lv_predicatName_1_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
            				

            }


            }

            // InternalPDDL.g:2853:3: ( (lv_objects_2_0= rulePDDLObject ) )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==RULE_NAME) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalPDDL.g:2854:4: (lv_objects_2_0= rulePDDLObject )
            	    {
            	    // InternalPDDL.g:2854:4: (lv_objects_2_0= rulePDDLObject )
            	    // InternalPDDL.g:2855:5: lv_objects_2_0= rulePDDLObject
            	    {

            	    					newCompositeNode(grammarAccess.getPredicatPBAccess().getObjectsPDDLObjectParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_16);
            	    lv_objects_2_0=rulePDDLObject();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPredicatPBRule());
            	    					}
            	    					add(
            	    						current,
            	    						"objects",
            	    						lv_objects_2_0,
            	    						"fr.irisa.atsyra.pddl.xtext.PDDL.PDDLObject");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getPredicatPBAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePredicatPB"


    // $ANTLR start "entryRulePDDLObject"
    // InternalPDDL.g:2880:1: entryRulePDDLObject returns [EObject current=null] : iv_rulePDDLObject= rulePDDLObject EOF ;
    public final EObject entryRulePDDLObject() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePDDLObject = null;


        try {
            // InternalPDDL.g:2880:51: (iv_rulePDDLObject= rulePDDLObject EOF )
            // InternalPDDL.g:2881:2: iv_rulePDDLObject= rulePDDLObject EOF
            {
             newCompositeNode(grammarAccess.getPDDLObjectRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePDDLObject=rulePDDLObject();

            state._fsp--;

             current =iv_rulePDDLObject; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePDDLObject"


    // $ANTLR start "rulePDDLObject"
    // InternalPDDL.g:2887:1: rulePDDLObject returns [EObject current=null] : ( (lv_objectName_0_0= RULE_NAME ) ) ;
    public final EObject rulePDDLObject() throws RecognitionException {
        EObject current = null;

        Token lv_objectName_0_0=null;


        	enterRule();

        try {
            // InternalPDDL.g:2893:2: ( ( (lv_objectName_0_0= RULE_NAME ) ) )
            // InternalPDDL.g:2894:2: ( (lv_objectName_0_0= RULE_NAME ) )
            {
            // InternalPDDL.g:2894:2: ( (lv_objectName_0_0= RULE_NAME ) )
            // InternalPDDL.g:2895:3: (lv_objectName_0_0= RULE_NAME )
            {
            // InternalPDDL.g:2895:3: (lv_objectName_0_0= RULE_NAME )
            // InternalPDDL.g:2896:4: lv_objectName_0_0= RULE_NAME
            {
            lv_objectName_0_0=(Token)match(input,RULE_NAME,FOLLOW_2); 

            				newLeafNode(lv_objectName_0_0, grammarAccess.getPDDLObjectAccess().getObjectNameNAMETerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getPDDLObjectRule());
            				}
            				setWithLastConsumed(
            					current,
            					"objectName",
            					lv_objectName_0_0,
            					"fr.irisa.atsyra.pddl.xtext.PDDL.NAME");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePDDLObject"


    // $ANTLR start "entryRuleGoal"
    // InternalPDDL.g:2915:1: entryRuleGoal returns [EObject current=null] : iv_ruleGoal= ruleGoal EOF ;
    public final EObject entryRuleGoal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGoal = null;


        try {
            // InternalPDDL.g:2915:45: (iv_ruleGoal= ruleGoal EOF )
            // InternalPDDL.g:2916:2: iv_ruleGoal= ruleGoal EOF
            {
             newCompositeNode(grammarAccess.getGoalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGoal=ruleGoal();

            state._fsp--;

             current =iv_ruleGoal; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGoal"


    // $ANTLR start "ruleGoal"
    // InternalPDDL.g:2922:1: ruleGoal returns [EObject current=null] : (this_OP_0= RULE_OP otherlv_1= ':goal' ( (lv_leGoal_2_0= ruleLogicalExpressionPB ) ) this_CP_3= RULE_CP ) ;
    public final EObject ruleGoal() throws RecognitionException {
        EObject current = null;

        Token this_OP_0=null;
        Token otherlv_1=null;
        Token this_CP_3=null;
        EObject lv_leGoal_2_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2928:2: ( (this_OP_0= RULE_OP otherlv_1= ':goal' ( (lv_leGoal_2_0= ruleLogicalExpressionPB ) ) this_CP_3= RULE_CP ) )
            // InternalPDDL.g:2929:2: (this_OP_0= RULE_OP otherlv_1= ':goal' ( (lv_leGoal_2_0= ruleLogicalExpressionPB ) ) this_CP_3= RULE_CP )
            {
            // InternalPDDL.g:2929:2: (this_OP_0= RULE_OP otherlv_1= ':goal' ( (lv_leGoal_2_0= ruleLogicalExpressionPB ) ) this_CP_3= RULE_CP )
            // InternalPDDL.g:2930:3: this_OP_0= RULE_OP otherlv_1= ':goal' ( (lv_leGoal_2_0= ruleLogicalExpressionPB ) ) this_CP_3= RULE_CP
            {
            this_OP_0=(Token)match(input,RULE_OP,FOLLOW_47); 

            			newLeafNode(this_OP_0, grammarAccess.getGoalAccess().getOPTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,47,FOLLOW_28); 

            			newLeafNode(otherlv_1, grammarAccess.getGoalAccess().getGoalKeyword_1());
            		
            // InternalPDDL.g:2938:3: ( (lv_leGoal_2_0= ruleLogicalExpressionPB ) )
            // InternalPDDL.g:2939:4: (lv_leGoal_2_0= ruleLogicalExpressionPB )
            {
            // InternalPDDL.g:2939:4: (lv_leGoal_2_0= ruleLogicalExpressionPB )
            // InternalPDDL.g:2940:5: lv_leGoal_2_0= ruleLogicalExpressionPB
            {

            					newCompositeNode(grammarAccess.getGoalAccess().getLeGoalLogicalExpressionPBParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_leGoal_2_0=ruleLogicalExpressionPB();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGoalRule());
            					}
            					set(
            						current,
            						"leGoal",
            						lv_leGoal_2_0,
            						"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_CP_3=(Token)match(input,RULE_CP,FOLLOW_2); 

            			newLeafNode(this_CP_3, grammarAccess.getGoalAccess().getCPTerminalRuleCall_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGoal"


    // $ANTLR start "entryRuleLogicalExpressionPB"
    // InternalPDDL.g:2965:1: entryRuleLogicalExpressionPB returns [EObject current=null] : iv_ruleLogicalExpressionPB= ruleLogicalExpressionPB EOF ;
    public final EObject entryRuleLogicalExpressionPB() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogicalExpressionPB = null;


        try {
            // InternalPDDL.g:2965:60: (iv_ruleLogicalExpressionPB= ruleLogicalExpressionPB EOF )
            // InternalPDDL.g:2966:2: iv_ruleLogicalExpressionPB= ruleLogicalExpressionPB EOF
            {
             newCompositeNode(grammarAccess.getLogicalExpressionPBRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogicalExpressionPB=ruleLogicalExpressionPB();

            state._fsp--;

             current =iv_ruleLogicalExpressionPB; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogicalExpressionPB"


    // $ANTLR start "ruleLogicalExpressionPB"
    // InternalPDDL.g:2972:1: ruleLogicalExpressionPB returns [EObject current=null] : ( ( (lv_p_0_0= rulePredicatPB ) ) | ( (lv_v_1_0= ruleVariable ) ) | (this_OP_2= RULE_OP ( (lv_le_3_0= ruleLogicalExpressionPB ) ) this_CP_4= RULE_CP ) | (this_OP_5= RULE_OP otherlv_6= '=' ( (lv_vEqual1_7_0= ruleLogicalExpressionPB ) ) ( (lv_vEqual2_8_0= ruleLogicalExpressionPB ) )+ this_CP_9= RULE_CP ) | (this_OP_10= RULE_OP otherlv_11= 'not' ( (lv_leNot_12_0= ruleLogicalExpressionPB ) ) this_CP_13= RULE_CP ) | (this_OP_14= RULE_OP otherlv_15= 'and' ( (lv_leAnd1_16_0= ruleLogicalExpressionPB ) ) ( (lv_leAnd2_17_0= ruleLogicalExpressionPB ) )+ this_CP_18= RULE_CP ) | (this_OP_19= RULE_OP otherlv_20= 'or' ( (lv_leOr1_21_0= ruleLogicalExpressionPB ) ) ( (lv_leOr2_22_0= ruleLogicalExpressionPB ) )+ this_CP_23= RULE_CP ) | (this_OP_24= RULE_OP otherlv_25= 'imply' ( (lv_leImply1_26_0= ruleLogicalExpressionPB ) ) ( (lv_leImply2_27_0= ruleLogicalExpressionPB ) ) this_CP_28= RULE_CP ) | (this_OP_29= RULE_OP otherlv_30= 'forall' ( (lv_tdForall_31_0= ruleTypeDescr ) ) ( (lv_leForall_32_0= ruleLogicalExpressionPB ) ) this_CP_33= RULE_CP ) | (this_OP_34= RULE_OP otherlv_35= 'exists' ( (lv_tdExists_36_0= ruleTypeDescr ) ) ( (lv_leExists_37_0= ruleLogicalExpressionPB ) ) this_CP_38= RULE_CP ) | (this_OP_39= RULE_OP otherlv_40= 'when' ( (lv_leWhen1_41_0= ruleLogicalExpressionPB ) ) ( (lv_leWhen2_42_0= ruleLogicalExpressionPB ) ) this_CP_43= RULE_CP ) ) ;
    public final EObject ruleLogicalExpressionPB() throws RecognitionException {
        EObject current = null;

        Token this_OP_2=null;
        Token this_CP_4=null;
        Token this_OP_5=null;
        Token otherlv_6=null;
        Token this_CP_9=null;
        Token this_OP_10=null;
        Token otherlv_11=null;
        Token this_CP_13=null;
        Token this_OP_14=null;
        Token otherlv_15=null;
        Token this_CP_18=null;
        Token this_OP_19=null;
        Token otherlv_20=null;
        Token this_CP_23=null;
        Token this_OP_24=null;
        Token otherlv_25=null;
        Token this_CP_28=null;
        Token this_OP_29=null;
        Token otherlv_30=null;
        Token this_CP_33=null;
        Token this_OP_34=null;
        Token otherlv_35=null;
        Token this_CP_38=null;
        Token this_OP_39=null;
        Token otherlv_40=null;
        Token this_CP_43=null;
        EObject lv_p_0_0 = null;

        EObject lv_v_1_0 = null;

        EObject lv_le_3_0 = null;

        EObject lv_vEqual1_7_0 = null;

        EObject lv_vEqual2_8_0 = null;

        EObject lv_leNot_12_0 = null;

        EObject lv_leAnd1_16_0 = null;

        EObject lv_leAnd2_17_0 = null;

        EObject lv_leOr1_21_0 = null;

        EObject lv_leOr2_22_0 = null;

        EObject lv_leImply1_26_0 = null;

        EObject lv_leImply2_27_0 = null;

        EObject lv_tdForall_31_0 = null;

        EObject lv_leForall_32_0 = null;

        EObject lv_tdExists_36_0 = null;

        EObject lv_leExists_37_0 = null;

        EObject lv_leWhen1_41_0 = null;

        EObject lv_leWhen2_42_0 = null;



        	enterRule();

        try {
            // InternalPDDL.g:2978:2: ( ( ( (lv_p_0_0= rulePredicatPB ) ) | ( (lv_v_1_0= ruleVariable ) ) | (this_OP_2= RULE_OP ( (lv_le_3_0= ruleLogicalExpressionPB ) ) this_CP_4= RULE_CP ) | (this_OP_5= RULE_OP otherlv_6= '=' ( (lv_vEqual1_7_0= ruleLogicalExpressionPB ) ) ( (lv_vEqual2_8_0= ruleLogicalExpressionPB ) )+ this_CP_9= RULE_CP ) | (this_OP_10= RULE_OP otherlv_11= 'not' ( (lv_leNot_12_0= ruleLogicalExpressionPB ) ) this_CP_13= RULE_CP ) | (this_OP_14= RULE_OP otherlv_15= 'and' ( (lv_leAnd1_16_0= ruleLogicalExpressionPB ) ) ( (lv_leAnd2_17_0= ruleLogicalExpressionPB ) )+ this_CP_18= RULE_CP ) | (this_OP_19= RULE_OP otherlv_20= 'or' ( (lv_leOr1_21_0= ruleLogicalExpressionPB ) ) ( (lv_leOr2_22_0= ruleLogicalExpressionPB ) )+ this_CP_23= RULE_CP ) | (this_OP_24= RULE_OP otherlv_25= 'imply' ( (lv_leImply1_26_0= ruleLogicalExpressionPB ) ) ( (lv_leImply2_27_0= ruleLogicalExpressionPB ) ) this_CP_28= RULE_CP ) | (this_OP_29= RULE_OP otherlv_30= 'forall' ( (lv_tdForall_31_0= ruleTypeDescr ) ) ( (lv_leForall_32_0= ruleLogicalExpressionPB ) ) this_CP_33= RULE_CP ) | (this_OP_34= RULE_OP otherlv_35= 'exists' ( (lv_tdExists_36_0= ruleTypeDescr ) ) ( (lv_leExists_37_0= ruleLogicalExpressionPB ) ) this_CP_38= RULE_CP ) | (this_OP_39= RULE_OP otherlv_40= 'when' ( (lv_leWhen1_41_0= ruleLogicalExpressionPB ) ) ( (lv_leWhen2_42_0= ruleLogicalExpressionPB ) ) this_CP_43= RULE_CP ) ) )
            // InternalPDDL.g:2979:2: ( ( (lv_p_0_0= rulePredicatPB ) ) | ( (lv_v_1_0= ruleVariable ) ) | (this_OP_2= RULE_OP ( (lv_le_3_0= ruleLogicalExpressionPB ) ) this_CP_4= RULE_CP ) | (this_OP_5= RULE_OP otherlv_6= '=' ( (lv_vEqual1_7_0= ruleLogicalExpressionPB ) ) ( (lv_vEqual2_8_0= ruleLogicalExpressionPB ) )+ this_CP_9= RULE_CP ) | (this_OP_10= RULE_OP otherlv_11= 'not' ( (lv_leNot_12_0= ruleLogicalExpressionPB ) ) this_CP_13= RULE_CP ) | (this_OP_14= RULE_OP otherlv_15= 'and' ( (lv_leAnd1_16_0= ruleLogicalExpressionPB ) ) ( (lv_leAnd2_17_0= ruleLogicalExpressionPB ) )+ this_CP_18= RULE_CP ) | (this_OP_19= RULE_OP otherlv_20= 'or' ( (lv_leOr1_21_0= ruleLogicalExpressionPB ) ) ( (lv_leOr2_22_0= ruleLogicalExpressionPB ) )+ this_CP_23= RULE_CP ) | (this_OP_24= RULE_OP otherlv_25= 'imply' ( (lv_leImply1_26_0= ruleLogicalExpressionPB ) ) ( (lv_leImply2_27_0= ruleLogicalExpressionPB ) ) this_CP_28= RULE_CP ) | (this_OP_29= RULE_OP otherlv_30= 'forall' ( (lv_tdForall_31_0= ruleTypeDescr ) ) ( (lv_leForall_32_0= ruleLogicalExpressionPB ) ) this_CP_33= RULE_CP ) | (this_OP_34= RULE_OP otherlv_35= 'exists' ( (lv_tdExists_36_0= ruleTypeDescr ) ) ( (lv_leExists_37_0= ruleLogicalExpressionPB ) ) this_CP_38= RULE_CP ) | (this_OP_39= RULE_OP otherlv_40= 'when' ( (lv_leWhen1_41_0= ruleLogicalExpressionPB ) ) ( (lv_leWhen2_42_0= ruleLogicalExpressionPB ) ) this_CP_43= RULE_CP ) )
            {
            // InternalPDDL.g:2979:2: ( ( (lv_p_0_0= rulePredicatPB ) ) | ( (lv_v_1_0= ruleVariable ) ) | (this_OP_2= RULE_OP ( (lv_le_3_0= ruleLogicalExpressionPB ) ) this_CP_4= RULE_CP ) | (this_OP_5= RULE_OP otherlv_6= '=' ( (lv_vEqual1_7_0= ruleLogicalExpressionPB ) ) ( (lv_vEqual2_8_0= ruleLogicalExpressionPB ) )+ this_CP_9= RULE_CP ) | (this_OP_10= RULE_OP otherlv_11= 'not' ( (lv_leNot_12_0= ruleLogicalExpressionPB ) ) this_CP_13= RULE_CP ) | (this_OP_14= RULE_OP otherlv_15= 'and' ( (lv_leAnd1_16_0= ruleLogicalExpressionPB ) ) ( (lv_leAnd2_17_0= ruleLogicalExpressionPB ) )+ this_CP_18= RULE_CP ) | (this_OP_19= RULE_OP otherlv_20= 'or' ( (lv_leOr1_21_0= ruleLogicalExpressionPB ) ) ( (lv_leOr2_22_0= ruleLogicalExpressionPB ) )+ this_CP_23= RULE_CP ) | (this_OP_24= RULE_OP otherlv_25= 'imply' ( (lv_leImply1_26_0= ruleLogicalExpressionPB ) ) ( (lv_leImply2_27_0= ruleLogicalExpressionPB ) ) this_CP_28= RULE_CP ) | (this_OP_29= RULE_OP otherlv_30= 'forall' ( (lv_tdForall_31_0= ruleTypeDescr ) ) ( (lv_leForall_32_0= ruleLogicalExpressionPB ) ) this_CP_33= RULE_CP ) | (this_OP_34= RULE_OP otherlv_35= 'exists' ( (lv_tdExists_36_0= ruleTypeDescr ) ) ( (lv_leExists_37_0= ruleLogicalExpressionPB ) ) this_CP_38= RULE_CP ) | (this_OP_39= RULE_OP otherlv_40= 'when' ( (lv_leWhen1_41_0= ruleLogicalExpressionPB ) ) ( (lv_leWhen2_42_0= ruleLogicalExpressionPB ) ) this_CP_43= RULE_CP ) )
            int alt35=11;
            alt35 = dfa35.predict(input);
            switch (alt35) {
                case 1 :
                    // InternalPDDL.g:2980:3: ( (lv_p_0_0= rulePredicatPB ) )
                    {
                    // InternalPDDL.g:2980:3: ( (lv_p_0_0= rulePredicatPB ) )
                    // InternalPDDL.g:2981:4: (lv_p_0_0= rulePredicatPB )
                    {
                    // InternalPDDL.g:2981:4: (lv_p_0_0= rulePredicatPB )
                    // InternalPDDL.g:2982:5: lv_p_0_0= rulePredicatPB
                    {

                    					newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getPPredicatPBParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_p_0_0=rulePredicatPB();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    					}
                    					set(
                    						current,
                    						"p",
                    						lv_p_0_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.PredicatPB");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPDDL.g:3000:3: ( (lv_v_1_0= ruleVariable ) )
                    {
                    // InternalPDDL.g:3000:3: ( (lv_v_1_0= ruleVariable ) )
                    // InternalPDDL.g:3001:4: (lv_v_1_0= ruleVariable )
                    {
                    // InternalPDDL.g:3001:4: (lv_v_1_0= ruleVariable )
                    // InternalPDDL.g:3002:5: lv_v_1_0= ruleVariable
                    {

                    					newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getVVariableParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_v_1_0=ruleVariable();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    					}
                    					set(
                    						current,
                    						"v",
                    						lv_v_1_0,
                    						"fr.irisa.atsyra.pddl.xtext.PDDL.Variable");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPDDL.g:3020:3: (this_OP_2= RULE_OP ( (lv_le_3_0= ruleLogicalExpressionPB ) ) this_CP_4= RULE_CP )
                    {
                    // InternalPDDL.g:3020:3: (this_OP_2= RULE_OP ( (lv_le_3_0= ruleLogicalExpressionPB ) ) this_CP_4= RULE_CP )
                    // InternalPDDL.g:3021:4: this_OP_2= RULE_OP ( (lv_le_3_0= ruleLogicalExpressionPB ) ) this_CP_4= RULE_CP
                    {
                    this_OP_2=(Token)match(input,RULE_OP,FOLLOW_28); 

                    				newLeafNode(this_OP_2, grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_2_0());
                    			
                    // InternalPDDL.g:3025:4: ( (lv_le_3_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3026:5: (lv_le_3_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3026:5: (lv_le_3_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3027:6: lv_le_3_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeLogicalExpressionPBParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_le_3_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"le",
                    							lv_le_3_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_4=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_4, grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_2_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalPDDL.g:3050:3: (this_OP_5= RULE_OP otherlv_6= '=' ( (lv_vEqual1_7_0= ruleLogicalExpressionPB ) ) ( (lv_vEqual2_8_0= ruleLogicalExpressionPB ) )+ this_CP_9= RULE_CP )
                    {
                    // InternalPDDL.g:3050:3: (this_OP_5= RULE_OP otherlv_6= '=' ( (lv_vEqual1_7_0= ruleLogicalExpressionPB ) ) ( (lv_vEqual2_8_0= ruleLogicalExpressionPB ) )+ this_CP_9= RULE_CP )
                    // InternalPDDL.g:3051:4: this_OP_5= RULE_OP otherlv_6= '=' ( (lv_vEqual1_7_0= ruleLogicalExpressionPB ) ) ( (lv_vEqual2_8_0= ruleLogicalExpressionPB ) )+ this_CP_9= RULE_CP
                    {
                    this_OP_5=(Token)match(input,RULE_OP,FOLLOW_29); 

                    				newLeafNode(this_OP_5, grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_3_0());
                    			
                    otherlv_6=(Token)match(input,32,FOLLOW_28); 

                    				newLeafNode(otherlv_6, grammarAccess.getLogicalExpressionPBAccess().getEqualsSignKeyword_3_1());
                    			
                    // InternalPDDL.g:3059:4: ( (lv_vEqual1_7_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3060:5: (lv_vEqual1_7_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3060:5: (lv_vEqual1_7_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3061:6: lv_vEqual1_7_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getVEqual1LogicalExpressionPBParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_vEqual1_7_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"vEqual1",
                    							lv_vEqual1_7_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:3078:4: ( (lv_vEqual2_8_0= ruleLogicalExpressionPB ) )+
                    int cnt32=0;
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==RULE_OP||LA32_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalPDDL.g:3079:5: (lv_vEqual2_8_0= ruleLogicalExpressionPB )
                    	    {
                    	    // InternalPDDL.g:3079:5: (lv_vEqual2_8_0= ruleLogicalExpressionPB )
                    	    // InternalPDDL.g:3080:6: lv_vEqual2_8_0= ruleLogicalExpressionPB
                    	    {

                    	    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getVEqual2LogicalExpressionPBParserRuleCall_3_3_0());
                    	    					
                    	    pushFollow(FOLLOW_30);
                    	    lv_vEqual2_8_0=ruleLogicalExpressionPB();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"vEqual2",
                    	    							lv_vEqual2_8_0,
                    	    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt32 >= 1 ) break loop32;
                                EarlyExitException eee =
                                    new EarlyExitException(32, input);
                                throw eee;
                        }
                        cnt32++;
                    } while (true);

                    this_CP_9=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_9, grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_3_4());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalPDDL.g:3103:3: (this_OP_10= RULE_OP otherlv_11= 'not' ( (lv_leNot_12_0= ruleLogicalExpressionPB ) ) this_CP_13= RULE_CP )
                    {
                    // InternalPDDL.g:3103:3: (this_OP_10= RULE_OP otherlv_11= 'not' ( (lv_leNot_12_0= ruleLogicalExpressionPB ) ) this_CP_13= RULE_CP )
                    // InternalPDDL.g:3104:4: this_OP_10= RULE_OP otherlv_11= 'not' ( (lv_leNot_12_0= ruleLogicalExpressionPB ) ) this_CP_13= RULE_CP
                    {
                    this_OP_10=(Token)match(input,RULE_OP,FOLLOW_31); 

                    				newLeafNode(this_OP_10, grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_4_0());
                    			
                    otherlv_11=(Token)match(input,33,FOLLOW_28); 

                    				newLeafNode(otherlv_11, grammarAccess.getLogicalExpressionPBAccess().getNotKeyword_4_1());
                    			
                    // InternalPDDL.g:3112:4: ( (lv_leNot_12_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3113:5: (lv_leNot_12_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3113:5: (lv_leNot_12_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3114:6: lv_leNot_12_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeNotLogicalExpressionPBParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leNot_12_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"leNot",
                    							lv_leNot_12_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_13=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_13, grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_4_3());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalPDDL.g:3137:3: (this_OP_14= RULE_OP otherlv_15= 'and' ( (lv_leAnd1_16_0= ruleLogicalExpressionPB ) ) ( (lv_leAnd2_17_0= ruleLogicalExpressionPB ) )+ this_CP_18= RULE_CP )
                    {
                    // InternalPDDL.g:3137:3: (this_OP_14= RULE_OP otherlv_15= 'and' ( (lv_leAnd1_16_0= ruleLogicalExpressionPB ) ) ( (lv_leAnd2_17_0= ruleLogicalExpressionPB ) )+ this_CP_18= RULE_CP )
                    // InternalPDDL.g:3138:4: this_OP_14= RULE_OP otherlv_15= 'and' ( (lv_leAnd1_16_0= ruleLogicalExpressionPB ) ) ( (lv_leAnd2_17_0= ruleLogicalExpressionPB ) )+ this_CP_18= RULE_CP
                    {
                    this_OP_14=(Token)match(input,RULE_OP,FOLLOW_32); 

                    				newLeafNode(this_OP_14, grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_5_0());
                    			
                    otherlv_15=(Token)match(input,34,FOLLOW_28); 

                    				newLeafNode(otherlv_15, grammarAccess.getLogicalExpressionPBAccess().getAndKeyword_5_1());
                    			
                    // InternalPDDL.g:3146:4: ( (lv_leAnd1_16_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3147:5: (lv_leAnd1_16_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3147:5: (lv_leAnd1_16_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3148:6: lv_leAnd1_16_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeAnd1LogicalExpressionPBParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_leAnd1_16_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"leAnd1",
                    							lv_leAnd1_16_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:3165:4: ( (lv_leAnd2_17_0= ruleLogicalExpressionPB ) )+
                    int cnt33=0;
                    loop33:
                    do {
                        int alt33=2;
                        int LA33_0 = input.LA(1);

                        if ( (LA33_0==RULE_OP||LA33_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                            alt33=1;
                        }


                        switch (alt33) {
                    	case 1 :
                    	    // InternalPDDL.g:3166:5: (lv_leAnd2_17_0= ruleLogicalExpressionPB )
                    	    {
                    	    // InternalPDDL.g:3166:5: (lv_leAnd2_17_0= ruleLogicalExpressionPB )
                    	    // InternalPDDL.g:3167:6: lv_leAnd2_17_0= ruleLogicalExpressionPB
                    	    {

                    	    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeAnd2LogicalExpressionPBParserRuleCall_5_3_0());
                    	    					
                    	    pushFollow(FOLLOW_30);
                    	    lv_leAnd2_17_0=ruleLogicalExpressionPB();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"leAnd2",
                    	    							lv_leAnd2_17_0,
                    	    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt33 >= 1 ) break loop33;
                                EarlyExitException eee =
                                    new EarlyExitException(33, input);
                                throw eee;
                        }
                        cnt33++;
                    } while (true);

                    this_CP_18=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_18, grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_5_4());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalPDDL.g:3190:3: (this_OP_19= RULE_OP otherlv_20= 'or' ( (lv_leOr1_21_0= ruleLogicalExpressionPB ) ) ( (lv_leOr2_22_0= ruleLogicalExpressionPB ) )+ this_CP_23= RULE_CP )
                    {
                    // InternalPDDL.g:3190:3: (this_OP_19= RULE_OP otherlv_20= 'or' ( (lv_leOr1_21_0= ruleLogicalExpressionPB ) ) ( (lv_leOr2_22_0= ruleLogicalExpressionPB ) )+ this_CP_23= RULE_CP )
                    // InternalPDDL.g:3191:4: this_OP_19= RULE_OP otherlv_20= 'or' ( (lv_leOr1_21_0= ruleLogicalExpressionPB ) ) ( (lv_leOr2_22_0= ruleLogicalExpressionPB ) )+ this_CP_23= RULE_CP
                    {
                    this_OP_19=(Token)match(input,RULE_OP,FOLLOW_33); 

                    				newLeafNode(this_OP_19, grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_6_0());
                    			
                    otherlv_20=(Token)match(input,35,FOLLOW_28); 

                    				newLeafNode(otherlv_20, grammarAccess.getLogicalExpressionPBAccess().getOrKeyword_6_1());
                    			
                    // InternalPDDL.g:3199:4: ( (lv_leOr1_21_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3200:5: (lv_leOr1_21_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3200:5: (lv_leOr1_21_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3201:6: lv_leOr1_21_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeOr1LogicalExpressionPBParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_leOr1_21_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"leOr1",
                    							lv_leOr1_21_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:3218:4: ( (lv_leOr2_22_0= ruleLogicalExpressionPB ) )+
                    int cnt34=0;
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==RULE_OP||LA34_0==RULE_IDWITHQUESTIONMARKBEFORE) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // InternalPDDL.g:3219:5: (lv_leOr2_22_0= ruleLogicalExpressionPB )
                    	    {
                    	    // InternalPDDL.g:3219:5: (lv_leOr2_22_0= ruleLogicalExpressionPB )
                    	    // InternalPDDL.g:3220:6: lv_leOr2_22_0= ruleLogicalExpressionPB
                    	    {

                    	    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeOr2LogicalExpressionPBParserRuleCall_6_3_0());
                    	    					
                    	    pushFollow(FOLLOW_30);
                    	    lv_leOr2_22_0=ruleLogicalExpressionPB();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"leOr2",
                    	    							lv_leOr2_22_0,
                    	    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt34 >= 1 ) break loop34;
                                EarlyExitException eee =
                                    new EarlyExitException(34, input);
                                throw eee;
                        }
                        cnt34++;
                    } while (true);

                    this_CP_23=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_23, grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_6_4());
                    			

                    }


                    }
                    break;
                case 8 :
                    // InternalPDDL.g:3243:3: (this_OP_24= RULE_OP otherlv_25= 'imply' ( (lv_leImply1_26_0= ruleLogicalExpressionPB ) ) ( (lv_leImply2_27_0= ruleLogicalExpressionPB ) ) this_CP_28= RULE_CP )
                    {
                    // InternalPDDL.g:3243:3: (this_OP_24= RULE_OP otherlv_25= 'imply' ( (lv_leImply1_26_0= ruleLogicalExpressionPB ) ) ( (lv_leImply2_27_0= ruleLogicalExpressionPB ) ) this_CP_28= RULE_CP )
                    // InternalPDDL.g:3244:4: this_OP_24= RULE_OP otherlv_25= 'imply' ( (lv_leImply1_26_0= ruleLogicalExpressionPB ) ) ( (lv_leImply2_27_0= ruleLogicalExpressionPB ) ) this_CP_28= RULE_CP
                    {
                    this_OP_24=(Token)match(input,RULE_OP,FOLLOW_34); 

                    				newLeafNode(this_OP_24, grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_7_0());
                    			
                    otherlv_25=(Token)match(input,36,FOLLOW_28); 

                    				newLeafNode(otherlv_25, grammarAccess.getLogicalExpressionPBAccess().getImplyKeyword_7_1());
                    			
                    // InternalPDDL.g:3252:4: ( (lv_leImply1_26_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3253:5: (lv_leImply1_26_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3253:5: (lv_leImply1_26_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3254:6: lv_leImply1_26_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeImply1LogicalExpressionPBParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_leImply1_26_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"leImply1",
                    							lv_leImply1_26_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:3271:4: ( (lv_leImply2_27_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3272:5: (lv_leImply2_27_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3272:5: (lv_leImply2_27_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3273:6: lv_leImply2_27_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeImply2LogicalExpressionPBParserRuleCall_7_3_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leImply2_27_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"leImply2",
                    							lv_leImply2_27_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_28=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_28, grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_7_4());
                    			

                    }


                    }
                    break;
                case 9 :
                    // InternalPDDL.g:3296:3: (this_OP_29= RULE_OP otherlv_30= 'forall' ( (lv_tdForall_31_0= ruleTypeDescr ) ) ( (lv_leForall_32_0= ruleLogicalExpressionPB ) ) this_CP_33= RULE_CP )
                    {
                    // InternalPDDL.g:3296:3: (this_OP_29= RULE_OP otherlv_30= 'forall' ( (lv_tdForall_31_0= ruleTypeDescr ) ) ( (lv_leForall_32_0= ruleLogicalExpressionPB ) ) this_CP_33= RULE_CP )
                    // InternalPDDL.g:3297:4: this_OP_29= RULE_OP otherlv_30= 'forall' ( (lv_tdForall_31_0= ruleTypeDescr ) ) ( (lv_leForall_32_0= ruleLogicalExpressionPB ) ) this_CP_33= RULE_CP
                    {
                    this_OP_29=(Token)match(input,RULE_OP,FOLLOW_35); 

                    				newLeafNode(this_OP_29, grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_8_0());
                    			
                    otherlv_30=(Token)match(input,37,FOLLOW_4); 

                    				newLeafNode(otherlv_30, grammarAccess.getLogicalExpressionPBAccess().getForallKeyword_8_1());
                    			
                    // InternalPDDL.g:3305:4: ( (lv_tdForall_31_0= ruleTypeDescr ) )
                    // InternalPDDL.g:3306:5: (lv_tdForall_31_0= ruleTypeDescr )
                    {
                    // InternalPDDL.g:3306:5: (lv_tdForall_31_0= ruleTypeDescr )
                    // InternalPDDL.g:3307:6: lv_tdForall_31_0= ruleTypeDescr
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getTdForallTypeDescrParserRuleCall_8_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_tdForall_31_0=ruleTypeDescr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"tdForall",
                    							lv_tdForall_31_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.TypeDescr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:3324:4: ( (lv_leForall_32_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3325:5: (lv_leForall_32_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3325:5: (lv_leForall_32_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3326:6: lv_leForall_32_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeForallLogicalExpressionPBParserRuleCall_8_3_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leForall_32_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"leForall",
                    							lv_leForall_32_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_33=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_33, grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_8_4());
                    			

                    }


                    }
                    break;
                case 10 :
                    // InternalPDDL.g:3349:3: (this_OP_34= RULE_OP otherlv_35= 'exists' ( (lv_tdExists_36_0= ruleTypeDescr ) ) ( (lv_leExists_37_0= ruleLogicalExpressionPB ) ) this_CP_38= RULE_CP )
                    {
                    // InternalPDDL.g:3349:3: (this_OP_34= RULE_OP otherlv_35= 'exists' ( (lv_tdExists_36_0= ruleTypeDescr ) ) ( (lv_leExists_37_0= ruleLogicalExpressionPB ) ) this_CP_38= RULE_CP )
                    // InternalPDDL.g:3350:4: this_OP_34= RULE_OP otherlv_35= 'exists' ( (lv_tdExists_36_0= ruleTypeDescr ) ) ( (lv_leExists_37_0= ruleLogicalExpressionPB ) ) this_CP_38= RULE_CP
                    {
                    this_OP_34=(Token)match(input,RULE_OP,FOLLOW_36); 

                    				newLeafNode(this_OP_34, grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_9_0());
                    			
                    otherlv_35=(Token)match(input,38,FOLLOW_4); 

                    				newLeafNode(otherlv_35, grammarAccess.getLogicalExpressionPBAccess().getExistsKeyword_9_1());
                    			
                    // InternalPDDL.g:3358:4: ( (lv_tdExists_36_0= ruleTypeDescr ) )
                    // InternalPDDL.g:3359:5: (lv_tdExists_36_0= ruleTypeDescr )
                    {
                    // InternalPDDL.g:3359:5: (lv_tdExists_36_0= ruleTypeDescr )
                    // InternalPDDL.g:3360:6: lv_tdExists_36_0= ruleTypeDescr
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getTdExistsTypeDescrParserRuleCall_9_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_tdExists_36_0=ruleTypeDescr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"tdExists",
                    							lv_tdExists_36_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.TypeDescr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:3377:4: ( (lv_leExists_37_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3378:5: (lv_leExists_37_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3378:5: (lv_leExists_37_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3379:6: lv_leExists_37_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeExistsLogicalExpressionPBParserRuleCall_9_3_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leExists_37_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"leExists",
                    							lv_leExists_37_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_38=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_38, grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_9_4());
                    			

                    }


                    }
                    break;
                case 11 :
                    // InternalPDDL.g:3402:3: (this_OP_39= RULE_OP otherlv_40= 'when' ( (lv_leWhen1_41_0= ruleLogicalExpressionPB ) ) ( (lv_leWhen2_42_0= ruleLogicalExpressionPB ) ) this_CP_43= RULE_CP )
                    {
                    // InternalPDDL.g:3402:3: (this_OP_39= RULE_OP otherlv_40= 'when' ( (lv_leWhen1_41_0= ruleLogicalExpressionPB ) ) ( (lv_leWhen2_42_0= ruleLogicalExpressionPB ) ) this_CP_43= RULE_CP )
                    // InternalPDDL.g:3403:4: this_OP_39= RULE_OP otherlv_40= 'when' ( (lv_leWhen1_41_0= ruleLogicalExpressionPB ) ) ( (lv_leWhen2_42_0= ruleLogicalExpressionPB ) ) this_CP_43= RULE_CP
                    {
                    this_OP_39=(Token)match(input,RULE_OP,FOLLOW_37); 

                    				newLeafNode(this_OP_39, grammarAccess.getLogicalExpressionPBAccess().getOPTerminalRuleCall_10_0());
                    			
                    otherlv_40=(Token)match(input,39,FOLLOW_28); 

                    				newLeafNode(otherlv_40, grammarAccess.getLogicalExpressionPBAccess().getWhenKeyword_10_1());
                    			
                    // InternalPDDL.g:3411:4: ( (lv_leWhen1_41_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3412:5: (lv_leWhen1_41_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3412:5: (lv_leWhen1_41_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3413:6: lv_leWhen1_41_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeWhen1LogicalExpressionPBParserRuleCall_10_2_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_leWhen1_41_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"leWhen1",
                    							lv_leWhen1_41_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDDL.g:3430:4: ( (lv_leWhen2_42_0= ruleLogicalExpressionPB ) )
                    // InternalPDDL.g:3431:5: (lv_leWhen2_42_0= ruleLogicalExpressionPB )
                    {
                    // InternalPDDL.g:3431:5: (lv_leWhen2_42_0= ruleLogicalExpressionPB )
                    // InternalPDDL.g:3432:6: lv_leWhen2_42_0= ruleLogicalExpressionPB
                    {

                    						newCompositeNode(grammarAccess.getLogicalExpressionPBAccess().getLeWhen2LogicalExpressionPBParserRuleCall_10_3_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_leWhen2_42_0=ruleLogicalExpressionPB();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogicalExpressionPBRule());
                    						}
                    						set(
                    							current,
                    							"leWhen2",
                    							lv_leWhen2_42_0,
                    							"fr.irisa.atsyra.pddl.xtext.PDDL.LogicalExpressionPB");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_CP_43=(Token)match(input,RULE_CP,FOLLOW_2); 

                    				newLeafNode(this_CP_43, grammarAccess.getLogicalExpressionPBAccess().getCPTerminalRuleCall_10_4());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicalExpressionPB"

    // Delegated rules


    protected DFA11 dfa11 = new DFA11(this);
    protected DFA23 dfa23 = new DFA23(this);
    protected DFA35 dfa35 = new DFA35(this);
    static final String dfa_1s = "\4\uffff";
    static final String dfa_2s = "\2\2\2\uffff";
    static final String dfa_3s = "\2\5\2\uffff";
    static final String dfa_4s = "\1\6\1\30\2\uffff";
    static final String dfa_5s = "\2\uffff\1\2\1\1";
    static final String dfa_6s = "\4\uffff}>";
    static final String[] dfa_7s = {
            "\1\2\1\1",
            "\1\2\1\1\21\uffff\1\3",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA11 extends DFA {

        public DFA11(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 11;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "()* loopback of 721:3: ( (lv_declInheritorTypes_1_0= ruleInheritorTypes ) )*";
        }
    }
    static final String dfa_8s = "\15\uffff";
    static final String dfa_9s = "\2\4\13\uffff";
    static final String dfa_10s = "\1\10\1\47\13\uffff";
    static final String dfa_11s = "\2\uffff\1\2\1\7\1\12\1\13\1\3\1\10\1\11\1\1\1\4\1\5\1\6";
    static final String dfa_12s = "\15\uffff}>";
    static final String[] dfa_13s = {
            "\1\1\3\uffff\1\2",
            "\1\6\1\uffff\1\11\1\uffff\1\6\27\uffff\1\12\1\13\1\14\1\3\1\7\1\10\1\4\1\5",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[][] dfa_13 = unpackEncodedStringArray(dfa_13s);

    class DFA23 extends DFA {

        public DFA23(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 23;
            this.eot = dfa_8;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_11;
            this.special = dfa_12;
            this.transition = dfa_13;
        }
        public String getDescription() {
            return "1572:2: ( ( () (this_OP_1= RULE_OP ( (lv_p_2_0= rulePredicatDM ) ) this_CP_3= RULE_CP ) ) | ( (lv_v_4_0= ruleVariable ) ) | (this_OP_5= RULE_OP ( (lv_le_6_0= ruleLogicalExpressionDM ) ) this_CP_7= RULE_CP ) | (this_OP_8= RULE_OP otherlv_9= '=' ( (lv_vEqual1_10_0= ruleLogicalExpressionDM ) ) ( (lv_vEqual2_11_0= ruleLogicalExpressionDM ) )+ this_CP_12= RULE_CP ) | (this_OP_13= RULE_OP otherlv_14= 'not' ( (lv_leNot_15_0= ruleLogicalExpressionDM ) ) this_CP_16= RULE_CP ) | (this_OP_17= RULE_OP otherlv_18= 'and' ( (lv_leAnd1_19_0= ruleLogicalExpressionDM ) ) ( (lv_leAnd2_20_0= ruleLogicalExpressionDM ) )+ this_CP_21= RULE_CP ) | (this_OP_22= RULE_OP otherlv_23= 'or' ( (lv_leOr1_24_0= ruleLogicalExpressionDM ) ) ( (lv_leOr2_25_0= ruleLogicalExpressionDM ) )+ this_CP_26= RULE_CP ) | (this_OP_27= RULE_OP otherlv_28= 'imply' ( (lv_leImply1_29_0= ruleLogicalExpressionDM ) ) ( (lv_leImply2_30_0= ruleLogicalExpressionDM ) ) this_CP_31= RULE_CP ) | (this_OP_32= RULE_OP otherlv_33= 'forall' ( (lv_tdForall_34_0= ruleTypeDescr ) ) ( (lv_leForall_35_0= ruleLogicalExpressionDM ) ) this_CP_36= RULE_CP ) | (this_OP_37= RULE_OP otherlv_38= 'exists' ( (lv_tdExists_39_0= ruleTypeDescr ) ) ( (lv_leExists_40_0= ruleLogicalExpressionDM ) ) this_CP_41= RULE_CP ) | (this_OP_42= RULE_OP otherlv_43= 'when' ( (lv_leWhen1_44_0= ruleLogicalExpressionDM ) ) ( (lv_leWhen2_45_0= ruleLogicalExpressionDM ) ) this_CP_46= RULE_CP ) )";
        }
    }
    static final String dfa_14s = "\2\uffff\1\2\1\4\1\5\1\1\1\6\1\7\1\3\1\10\1\11\1\12\1\13";
    static final String[] dfa_15s = {
            "\1\1\3\uffff\1\2",
            "\1\10\1\uffff\1\5\1\uffff\1\10\27\uffff\1\3\1\4\1\6\1\7\1\11\1\12\1\13\1\14",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };
    static final short[] dfa_14 = DFA.unpackEncodedString(dfa_14s);
    static final short[][] dfa_15 = unpackEncodedStringArray(dfa_15s);

    class DFA35 extends DFA {

        public DFA35(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 35;
            this.eot = dfa_8;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_14;
            this.special = dfa_12;
            this.transition = dfa_15;
        }
        public String getDescription() {
            return "2979:2: ( ( (lv_p_0_0= rulePredicatPB ) ) | ( (lv_v_1_0= ruleVariable ) ) | (this_OP_2= RULE_OP ( (lv_le_3_0= ruleLogicalExpressionPB ) ) this_CP_4= RULE_CP ) | (this_OP_5= RULE_OP otherlv_6= '=' ( (lv_vEqual1_7_0= ruleLogicalExpressionPB ) ) ( (lv_vEqual2_8_0= ruleLogicalExpressionPB ) )+ this_CP_9= RULE_CP ) | (this_OP_10= RULE_OP otherlv_11= 'not' ( (lv_leNot_12_0= ruleLogicalExpressionPB ) ) this_CP_13= RULE_CP ) | (this_OP_14= RULE_OP otherlv_15= 'and' ( (lv_leAnd1_16_0= ruleLogicalExpressionPB ) ) ( (lv_leAnd2_17_0= ruleLogicalExpressionPB ) )+ this_CP_18= RULE_CP ) | (this_OP_19= RULE_OP otherlv_20= 'or' ( (lv_leOr1_21_0= ruleLogicalExpressionPB ) ) ( (lv_leOr2_22_0= ruleLogicalExpressionPB ) )+ this_CP_23= RULE_CP ) | (this_OP_24= RULE_OP otherlv_25= 'imply' ( (lv_leImply1_26_0= ruleLogicalExpressionPB ) ) ( (lv_leImply2_27_0= ruleLogicalExpressionPB ) ) this_CP_28= RULE_CP ) | (this_OP_29= RULE_OP otherlv_30= 'forall' ( (lv_tdForall_31_0= ruleTypeDescr ) ) ( (lv_leForall_32_0= ruleLogicalExpressionPB ) ) this_CP_33= RULE_CP ) | (this_OP_34= RULE_OP otherlv_35= 'exists' ( (lv_tdExists_36_0= ruleTypeDescr ) ) ( (lv_leExists_37_0= ruleLogicalExpressionPB ) ) this_CP_38= RULE_CP ) | (this_OP_39= RULE_OP otherlv_40= 'when' ( (lv_leWhen1_41_0= ruleLogicalExpressionPB ) ) ( (lv_leWhen2_42_0= ruleLogicalExpressionPB ) ) this_CP_43= RULE_CP ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000082L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000001000100L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x00000000C0000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000000110L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000130L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000001000040L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000200000030L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000800000000000L});

}