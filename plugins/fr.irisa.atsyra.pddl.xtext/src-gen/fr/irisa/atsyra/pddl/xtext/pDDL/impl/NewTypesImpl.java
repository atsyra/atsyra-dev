/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * generated by Xtext 2.25.0
 */
package fr.irisa.atsyra.pddl.xtext.pDDL.impl;

import fr.irisa.atsyra.pddl.xtext.pDDL.DeclarationTypes;
import fr.irisa.atsyra.pddl.xtext.pDDL.InheritorTypes;
import fr.irisa.atsyra.pddl.xtext.pDDL.NewTypes;
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>New Types</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.impl.NewTypesImpl#getDeclInheritorTypes <em>Decl Inheritor Types</em>}</li>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.impl.NewTypesImpl#getDeclOtherTypes <em>Decl Other Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NewTypesImpl extends MinimalEObjectImpl.Container implements NewTypes
{
  /**
   * The cached value of the '{@link #getDeclInheritorTypes() <em>Decl Inheritor Types</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDeclInheritorTypes()
   * @generated
   * @ordered
   */
  protected EList<InheritorTypes> declInheritorTypes;

  /**
   * The cached value of the '{@link #getDeclOtherTypes() <em>Decl Other Types</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDeclOtherTypes()
   * @generated
   * @ordered
   */
  protected DeclarationTypes declOtherTypes;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NewTypesImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PDDLPackage.Literals.NEW_TYPES;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<InheritorTypes> getDeclInheritorTypes()
  {
    if (declInheritorTypes == null)
    {
      declInheritorTypes = new EObjectContainmentEList<InheritorTypes>(InheritorTypes.class, this, PDDLPackage.NEW_TYPES__DECL_INHERITOR_TYPES);
    }
    return declInheritorTypes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public DeclarationTypes getDeclOtherTypes()
  {
    return declOtherTypes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDeclOtherTypes(DeclarationTypes newDeclOtherTypes, NotificationChain msgs)
  {
    DeclarationTypes oldDeclOtherTypes = declOtherTypes;
    declOtherTypes = newDeclOtherTypes;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PDDLPackage.NEW_TYPES__DECL_OTHER_TYPES, oldDeclOtherTypes, newDeclOtherTypes);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setDeclOtherTypes(DeclarationTypes newDeclOtherTypes)
  {
    if (newDeclOtherTypes != declOtherTypes)
    {
      NotificationChain msgs = null;
      if (declOtherTypes != null)
        msgs = ((InternalEObject)declOtherTypes).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PDDLPackage.NEW_TYPES__DECL_OTHER_TYPES, null, msgs);
      if (newDeclOtherTypes != null)
        msgs = ((InternalEObject)newDeclOtherTypes).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PDDLPackage.NEW_TYPES__DECL_OTHER_TYPES, null, msgs);
      msgs = basicSetDeclOtherTypes(newDeclOtherTypes, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PDDLPackage.NEW_TYPES__DECL_OTHER_TYPES, newDeclOtherTypes, newDeclOtherTypes));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case PDDLPackage.NEW_TYPES__DECL_INHERITOR_TYPES:
        return ((InternalEList<?>)getDeclInheritorTypes()).basicRemove(otherEnd, msgs);
      case PDDLPackage.NEW_TYPES__DECL_OTHER_TYPES:
        return basicSetDeclOtherTypes(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PDDLPackage.NEW_TYPES__DECL_INHERITOR_TYPES:
        return getDeclInheritorTypes();
      case PDDLPackage.NEW_TYPES__DECL_OTHER_TYPES:
        return getDeclOtherTypes();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PDDLPackage.NEW_TYPES__DECL_INHERITOR_TYPES:
        getDeclInheritorTypes().clear();
        getDeclInheritorTypes().addAll((Collection<? extends InheritorTypes>)newValue);
        return;
      case PDDLPackage.NEW_TYPES__DECL_OTHER_TYPES:
        setDeclOtherTypes((DeclarationTypes)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.NEW_TYPES__DECL_INHERITOR_TYPES:
        getDeclInheritorTypes().clear();
        return;
      case PDDLPackage.NEW_TYPES__DECL_OTHER_TYPES:
        setDeclOtherTypes((DeclarationTypes)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.NEW_TYPES__DECL_INHERITOR_TYPES:
        return declInheritorTypes != null && !declInheritorTypes.isEmpty();
      case PDDLPackage.NEW_TYPES__DECL_OTHER_TYPES:
        return declOtherTypes != null;
    }
    return super.eIsSet(featureID);
  }

} //NewTypesImpl
