/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * generated by Xtext 2.25.0
 */
package fr.irisa.atsyra.pddl.xtext.pDDL.impl;

import fr.irisa.atsyra.pddl.xtext.pDDL.LogicalExpressionDM;
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage;
import fr.irisa.atsyra.pddl.xtext.pDDL.Preconditions;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Preconditions</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.impl.PreconditionsImpl#getLe <em>Le</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PreconditionsImpl extends MinimalEObjectImpl.Container implements Preconditions
{
  /**
   * The cached value of the '{@link #getLe() <em>Le</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLe()
   * @generated
   * @ordered
   */
  protected LogicalExpressionDM le;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PreconditionsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PDDLPackage.Literals.PRECONDITIONS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public LogicalExpressionDM getLe()
  {
    return le;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLe(LogicalExpressionDM newLe, NotificationChain msgs)
  {
    LogicalExpressionDM oldLe = le;
    le = newLe;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PDDLPackage.PRECONDITIONS__LE, oldLe, newLe);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setLe(LogicalExpressionDM newLe)
  {
    if (newLe != le)
    {
      NotificationChain msgs = null;
      if (le != null)
        msgs = ((InternalEObject)le).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PDDLPackage.PRECONDITIONS__LE, null, msgs);
      if (newLe != null)
        msgs = ((InternalEObject)newLe).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PDDLPackage.PRECONDITIONS__LE, null, msgs);
      msgs = basicSetLe(newLe, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PDDLPackage.PRECONDITIONS__LE, newLe, newLe));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case PDDLPackage.PRECONDITIONS__LE:
        return basicSetLe(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PDDLPackage.PRECONDITIONS__LE:
        return getLe();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PDDLPackage.PRECONDITIONS__LE:
        setLe((LogicalExpressionDM)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.PRECONDITIONS__LE:
        setLe((LogicalExpressionDM)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.PRECONDITIONS__LE:
        return le != null;
    }
    return super.eIsSet(featureID);
  }

} //PreconditionsImpl
