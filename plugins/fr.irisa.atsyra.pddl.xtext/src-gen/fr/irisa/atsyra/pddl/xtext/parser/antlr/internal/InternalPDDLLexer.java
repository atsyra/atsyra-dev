/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.pddl.xtext.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPDDLLexer extends Lexer {
    public static final int RULE_CP=5;
    public static final int T__19=19;
    public static final int T__18=18;
    public static final int RULE_OP=4;
    public static final int RULE_IDWITHTWOPOINTBEFOREANDDASHES=7;
    public static final int RULE_ID=12;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=13;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=15;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_NAME=6;
    public static final int RULE_STRING=14;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int RULE_ANY_CHAR=10;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=16;
    public static final int RULE_ANY_OTHER=17;
    public static final int RULE_LETTER=9;
    public static final int RULE_IDWITHQUESTIONMARKBEFORE=8;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators

    public InternalPDDLLexer() {;} 
    public InternalPDDLLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalPDDLLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalPDDL.g"; }

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:11:7: ( 'define' )
            // InternalPDDL.g:11:9: 'define'
            {
            match("define"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:12:7: ( 'domain' )
            // InternalPDDL.g:12:9: 'domain'
            {
            match("domain"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:13:7: ( 'problem' )
            // InternalPDDL.g:13:9: 'problem'
            {
            match("problem"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:14:7: ( ':extends' )
            // InternalPDDL.g:14:9: ':extends'
            {
            match(":extends"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:15:7: ( ':requirements' )
            // InternalPDDL.g:15:9: ':requirements'
            {
            match(":requirements"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:16:7: ( ':types' )
            // InternalPDDL.g:16:9: ':types'
            {
            match(":types"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:17:7: ( '-' )
            // InternalPDDL.g:17:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:18:7: ( ':predicates' )
            // InternalPDDL.g:18:9: ':predicates'
            {
            match(":predicates"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:19:7: ( 'either' )
            // InternalPDDL.g:19:9: 'either'
            {
            match("either"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:20:7: ( ':functions' )
            // InternalPDDL.g:20:9: ':functions'
            {
            match(":functions"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:21:7: ( ':action' )
            // InternalPDDL.g:21:9: ':action'
            {
            match(":action"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:22:7: ( ':parameters' )
            // InternalPDDL.g:22:9: ':parameters'
            {
            match(":parameters"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:23:7: ( ':precondition' )
            // InternalPDDL.g:23:9: ':precondition'
            {
            match(":precondition"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:24:7: ( ':effect' )
            // InternalPDDL.g:24:9: ':effect'
            {
            match(":effect"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:25:7: ( '=' )
            // InternalPDDL.g:25:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:26:7: ( 'not' )
            // InternalPDDL.g:26:9: 'not'
            {
            match("not"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:27:7: ( 'and' )
            // InternalPDDL.g:27:9: 'and'
            {
            match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:28:7: ( 'or' )
            // InternalPDDL.g:28:9: 'or'
            {
            match("or"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:29:7: ( 'imply' )
            // InternalPDDL.g:29:9: 'imply'
            {
            match("imply"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:30:7: ( 'forall' )
            // InternalPDDL.g:30:9: 'forall'
            {
            match("forall"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:31:7: ( 'exists' )
            // InternalPDDL.g:31:9: 'exists'
            {
            match("exists"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:32:7: ( 'when' )
            // InternalPDDL.g:32:9: 'when'
            {
            match("when"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:33:7: ( ':axiom' )
            // InternalPDDL.g:33:9: ':axiom'
            {
            match(":axiom"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:34:7: ( ':vars' )
            // InternalPDDL.g:34:9: ':vars'
            {
            match(":vars"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:35:7: ( ':context' )
            // InternalPDDL.g:35:9: ':context'
            {
            match(":context"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:36:7: ( ':implies' )
            // InternalPDDL.g:36:9: ':implies'
            {
            match(":implies"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:37:7: ( ':domain' )
            // InternalPDDL.g:37:9: ':domain'
            {
            match(":domain"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:38:7: ( ':objects' )
            // InternalPDDL.g:38:9: ':objects'
            {
            match(":objects"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:39:7: ( ':init' )
            // InternalPDDL.g:39:9: ':init'
            {
            match(":init"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:40:7: ( ':goal' )
            // InternalPDDL.g:40:9: ':goal'
            {
            match(":goal"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "RULE_NAME"
    public final void mRULE_NAME() throws RecognitionException {
        try {
            int _type = RULE_NAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3457:11: ( RULE_LETTER ( RULE_ANY_CHAR )* )
            // InternalPDDL.g:3457:13: RULE_LETTER ( RULE_ANY_CHAR )*
            {
            mRULE_LETTER(); 
            // InternalPDDL.g:3457:25: ( RULE_ANY_CHAR )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='-'||(LA1_0>='0' && LA1_0<='9')||(LA1_0>='A' && LA1_0<='Z')||LA1_0=='_'||(LA1_0>='a' && LA1_0<='z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPDDL.g:3457:25: RULE_ANY_CHAR
            	    {
            	    mRULE_ANY_CHAR(); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NAME"

    // $ANTLR start "RULE_IDWITHTWOPOINTBEFOREANDDASHES"
    public final void mRULE_IDWITHTWOPOINTBEFOREANDDASHES() throws RecognitionException {
        try {
            int _type = RULE_IDWITHTWOPOINTBEFOREANDDASHES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3459:36: ( ':' RULE_LETTER ( RULE_ANY_CHAR )* )
            // InternalPDDL.g:3459:38: ':' RULE_LETTER ( RULE_ANY_CHAR )*
            {
            match(':'); 
            mRULE_LETTER(); 
            // InternalPDDL.g:3459:54: ( RULE_ANY_CHAR )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0=='-'||(LA2_0>='0' && LA2_0<='9')||(LA2_0>='A' && LA2_0<='Z')||LA2_0=='_'||(LA2_0>='a' && LA2_0<='z')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPDDL.g:3459:54: RULE_ANY_CHAR
            	    {
            	    mRULE_ANY_CHAR(); 

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IDWITHTWOPOINTBEFOREANDDASHES"

    // $ANTLR start "RULE_IDWITHQUESTIONMARKBEFORE"
    public final void mRULE_IDWITHQUESTIONMARKBEFORE() throws RecognitionException {
        try {
            int _type = RULE_IDWITHQUESTIONMARKBEFORE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3461:31: ( '?' RULE_LETTER ( RULE_ANY_CHAR )* )
            // InternalPDDL.g:3461:33: '?' RULE_LETTER ( RULE_ANY_CHAR )*
            {
            match('?'); 
            mRULE_LETTER(); 
            // InternalPDDL.g:3461:49: ( RULE_ANY_CHAR )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0=='-'||(LA3_0>='0' && LA3_0<='9')||(LA3_0>='A' && LA3_0<='Z')||LA3_0=='_'||(LA3_0>='a' && LA3_0<='z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalPDDL.g:3461:49: RULE_ANY_CHAR
            	    {
            	    mRULE_ANY_CHAR(); 

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IDWITHQUESTIONMARKBEFORE"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3463:17: ( ';' ( options {greedy=false; } : . )* ( '\\n' | '\\r' ) )
            // InternalPDDL.g:3463:19: ';' ( options {greedy=false; } : . )* ( '\\n' | '\\r' )
            {
            match(';'); 
            // InternalPDDL.g:3463:23: ( options {greedy=false; } : . )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0=='\n'||LA4_0=='\r') ) {
                    alt4=2;
                }
                else if ( ((LA4_0>='\u0000' && LA4_0<='\t')||(LA4_0>='\u000B' && LA4_0<='\f')||(LA4_0>='\u000E' && LA4_0<='\uFFFF')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalPDDL.g:3463:51: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_OP"
    public final void mRULE_OP() throws RecognitionException {
        try {
            int _type = RULE_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3465:9: ( '(' )
            // InternalPDDL.g:3465:11: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OP"

    // $ANTLR start "RULE_CP"
    public final void mRULE_CP() throws RecognitionException {
        try {
            int _type = RULE_CP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3467:9: ( ')' )
            // InternalPDDL.g:3467:11: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CP"

    // $ANTLR start "RULE_LETTER"
    public final void mRULE_LETTER() throws RecognitionException {
        try {
            // InternalPDDL.g:3469:22: ( ( 'a' .. 'z' | 'A' .. 'Z' ) )
            // InternalPDDL.g:3469:24: ( 'a' .. 'z' | 'A' .. 'Z' )
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_LETTER"

    // $ANTLR start "RULE_ANY_CHAR"
    public final void mRULE_ANY_CHAR() throws RecognitionException {
        try {
            // InternalPDDL.g:3471:24: ( ( RULE_LETTER | '0' .. '9' | '-' | '_' ) )
            // InternalPDDL.g:3471:26: ( RULE_LETTER | '0' .. '9' | '-' | '_' )
            {
            if ( input.LA(1)=='-'||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_CHAR"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3473:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalPDDL.g:3473:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalPDDL.g:3473:11: ( '^' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='^') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalPDDL.g:3473:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalPDDL.g:3473:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')||(LA6_0>='A' && LA6_0<='Z')||LA6_0=='_'||(LA6_0>='a' && LA6_0<='z')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalPDDL.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3475:10: ( ( '0' .. '9' )+ )
            // InternalPDDL.g:3475:12: ( '0' .. '9' )+
            {
            // InternalPDDL.g:3475:12: ( '0' .. '9' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='0' && LA7_0<='9')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalPDDL.g:3475:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3477:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalPDDL.g:3477:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalPDDL.g:3477:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\"') ) {
                alt10=1;
            }
            else if ( (LA10_0=='\'') ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalPDDL.g:3477:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalPDDL.g:3477:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop8:
                    do {
                        int alt8=3;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0=='\\') ) {
                            alt8=1;
                        }
                        else if ( ((LA8_0>='\u0000' && LA8_0<='!')||(LA8_0>='#' && LA8_0<='[')||(LA8_0>=']' && LA8_0<='\uFFFF')) ) {
                            alt8=2;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalPDDL.g:3477:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalPDDL.g:3477:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalPDDL.g:3477:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalPDDL.g:3477:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop9:
                    do {
                        int alt9=3;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0=='\\') ) {
                            alt9=1;
                        }
                        else if ( ((LA9_0>='\u0000' && LA9_0<='&')||(LA9_0>='(' && LA9_0<='[')||(LA9_0>=']' && LA9_0<='\uFFFF')) ) {
                            alt9=2;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalPDDL.g:3477:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalPDDL.g:3477:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3479:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalPDDL.g:3479:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalPDDL.g:3479:24: ( options {greedy=false; } : . )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0=='*') ) {
                    int LA11_1 = input.LA(2);

                    if ( (LA11_1=='/') ) {
                        alt11=2;
                    }
                    else if ( ((LA11_1>='\u0000' && LA11_1<='.')||(LA11_1>='0' && LA11_1<='\uFFFF')) ) {
                        alt11=1;
                    }


                }
                else if ( ((LA11_0>='\u0000' && LA11_0<=')')||(LA11_0>='+' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalPDDL.g:3479:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3481:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalPDDL.g:3481:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalPDDL.g:3481:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='\t' && LA12_0<='\n')||LA12_0=='\r'||LA12_0==' ') ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalPDDL.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPDDL.g:3483:16: ( . )
            // InternalPDDL.g:3483:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalPDDL.g:1:8: ( T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | RULE_NAME | RULE_IDWITHTWOPOINTBEFOREANDDASHES | RULE_IDWITHQUESTIONMARKBEFORE | RULE_SL_COMMENT | RULE_OP | RULE_CP | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt13=42;
        alt13 = dfa13.predict(input);
        switch (alt13) {
            case 1 :
                // InternalPDDL.g:1:10: T__18
                {
                mT__18(); 

                }
                break;
            case 2 :
                // InternalPDDL.g:1:16: T__19
                {
                mT__19(); 

                }
                break;
            case 3 :
                // InternalPDDL.g:1:22: T__20
                {
                mT__20(); 

                }
                break;
            case 4 :
                // InternalPDDL.g:1:28: T__21
                {
                mT__21(); 

                }
                break;
            case 5 :
                // InternalPDDL.g:1:34: T__22
                {
                mT__22(); 

                }
                break;
            case 6 :
                // InternalPDDL.g:1:40: T__23
                {
                mT__23(); 

                }
                break;
            case 7 :
                // InternalPDDL.g:1:46: T__24
                {
                mT__24(); 

                }
                break;
            case 8 :
                // InternalPDDL.g:1:52: T__25
                {
                mT__25(); 

                }
                break;
            case 9 :
                // InternalPDDL.g:1:58: T__26
                {
                mT__26(); 

                }
                break;
            case 10 :
                // InternalPDDL.g:1:64: T__27
                {
                mT__27(); 

                }
                break;
            case 11 :
                // InternalPDDL.g:1:70: T__28
                {
                mT__28(); 

                }
                break;
            case 12 :
                // InternalPDDL.g:1:76: T__29
                {
                mT__29(); 

                }
                break;
            case 13 :
                // InternalPDDL.g:1:82: T__30
                {
                mT__30(); 

                }
                break;
            case 14 :
                // InternalPDDL.g:1:88: T__31
                {
                mT__31(); 

                }
                break;
            case 15 :
                // InternalPDDL.g:1:94: T__32
                {
                mT__32(); 

                }
                break;
            case 16 :
                // InternalPDDL.g:1:100: T__33
                {
                mT__33(); 

                }
                break;
            case 17 :
                // InternalPDDL.g:1:106: T__34
                {
                mT__34(); 

                }
                break;
            case 18 :
                // InternalPDDL.g:1:112: T__35
                {
                mT__35(); 

                }
                break;
            case 19 :
                // InternalPDDL.g:1:118: T__36
                {
                mT__36(); 

                }
                break;
            case 20 :
                // InternalPDDL.g:1:124: T__37
                {
                mT__37(); 

                }
                break;
            case 21 :
                // InternalPDDL.g:1:130: T__38
                {
                mT__38(); 

                }
                break;
            case 22 :
                // InternalPDDL.g:1:136: T__39
                {
                mT__39(); 

                }
                break;
            case 23 :
                // InternalPDDL.g:1:142: T__40
                {
                mT__40(); 

                }
                break;
            case 24 :
                // InternalPDDL.g:1:148: T__41
                {
                mT__41(); 

                }
                break;
            case 25 :
                // InternalPDDL.g:1:154: T__42
                {
                mT__42(); 

                }
                break;
            case 26 :
                // InternalPDDL.g:1:160: T__43
                {
                mT__43(); 

                }
                break;
            case 27 :
                // InternalPDDL.g:1:166: T__44
                {
                mT__44(); 

                }
                break;
            case 28 :
                // InternalPDDL.g:1:172: T__45
                {
                mT__45(); 

                }
                break;
            case 29 :
                // InternalPDDL.g:1:178: T__46
                {
                mT__46(); 

                }
                break;
            case 30 :
                // InternalPDDL.g:1:184: T__47
                {
                mT__47(); 

                }
                break;
            case 31 :
                // InternalPDDL.g:1:190: RULE_NAME
                {
                mRULE_NAME(); 

                }
                break;
            case 32 :
                // InternalPDDL.g:1:200: RULE_IDWITHTWOPOINTBEFOREANDDASHES
                {
                mRULE_IDWITHTWOPOINTBEFOREANDDASHES(); 

                }
                break;
            case 33 :
                // InternalPDDL.g:1:235: RULE_IDWITHQUESTIONMARKBEFORE
                {
                mRULE_IDWITHQUESTIONMARKBEFORE(); 

                }
                break;
            case 34 :
                // InternalPDDL.g:1:265: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 35 :
                // InternalPDDL.g:1:281: RULE_OP
                {
                mRULE_OP(); 

                }
                break;
            case 36 :
                // InternalPDDL.g:1:289: RULE_CP
                {
                mRULE_CP(); 

                }
                break;
            case 37 :
                // InternalPDDL.g:1:297: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 38 :
                // InternalPDDL.g:1:305: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 39 :
                // InternalPDDL.g:1:314: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 40 :
                // InternalPDDL.g:1:326: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 41 :
                // InternalPDDL.g:1:342: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 42 :
                // InternalPDDL.g:1:350: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA13 dfa13 = new DFA13(this);
    static final String DFA13_eotS =
        "\1\uffff\2\34\1\31\1\uffff\1\34\1\uffff\7\34\2\31\2\uffff\1\31\2\uffff\3\31\2\uffff\2\34\1\uffff\2\34\14\53\2\uffff\2\34\1\uffff\2\34\1\126\3\34\11\uffff\3\34\20\53\2\34\1\157\1\160\1\uffff\6\34\20\53\2\34\2\uffff\2\34\1\u008c\3\34\12\53\1\u009a\2\53\1\u009d\2\53\1\u00a0\2\34\1\u00a3\1\34\1\uffff\1\u00a5\1\u00a6\1\34\3\53\1\u00ab\5\53\1\u00b1\1\uffff\2\53\1\uffff\2\53\1\uffff\1\u00b6\1\u00b7\1\uffff\1\u00b8\2\uffff\1\u00b9\1\53\1\u00bb\1\53\1\uffff\4\53\1\u00c1\1\uffff\2\53\1\u00c4\1\53\4\uffff\1\u00c6\1\uffff\5\53\1\uffff\1\u00cc\1\u00cd\1\uffff\1\u00ce\1\uffff\5\53\3\uffff\4\53\1\u00d8\1\53\1\u00da\1\53\1\u00dc\1\uffff\1\53\1\uffff\1\53\1\uffff\1\u00df\1\u00e0\2\uffff";
    static final String DFA13_eofS =
        "\u00e1\uffff";
    static final String DFA13_minS =
        "\1\0\2\60\1\101\1\uffff\1\60\1\uffff\7\60\1\101\1\0\2\uffff\1\101\2\uffff\2\0\1\52\2\uffff\2\60\1\uffff\2\60\1\146\1\145\1\171\1\141\1\165\1\143\1\141\1\157\1\155\1\157\1\142\1\157\2\uffff\2\60\1\uffff\2\60\1\55\3\60\11\uffff\3\60\1\164\1\146\1\161\1\160\1\145\1\162\1\156\1\164\1\151\1\162\1\156\1\160\1\151\1\155\1\152\1\141\2\60\2\55\1\uffff\6\60\2\145\1\165\1\145\1\143\1\141\1\143\1\151\1\157\1\163\1\164\1\154\1\164\1\141\1\145\1\154\2\60\2\uffff\2\60\1\55\3\60\1\156\1\143\1\151\1\163\1\151\1\157\1\155\1\164\1\157\1\155\1\55\1\145\1\151\1\55\1\151\1\143\1\55\2\60\1\55\1\60\1\uffff\2\55\1\60\1\144\1\164\1\162\1\55\1\143\1\156\1\145\1\151\1\156\1\55\1\uffff\1\170\1\145\1\uffff\1\156\1\164\1\uffff\2\55\1\uffff\1\55\2\uffff\1\55\1\163\1\55\1\145\1\uffff\1\141\1\144\1\164\1\157\1\55\1\uffff\1\164\1\163\1\55\1\163\4\uffff\1\55\1\uffff\1\155\1\164\1\151\1\145\1\156\1\uffff\2\55\1\uffff\1\55\1\uffff\2\145\1\164\1\162\1\163\3\uffff\1\156\1\163\1\151\1\163\1\55\1\164\1\55\1\157\1\55\1\uffff\1\163\1\uffff\1\156\1\uffff\2\55\2\uffff";
    static final String DFA13_maxS =
        "\1\uffff\3\172\1\uffff\1\172\1\uffff\10\172\1\uffff\2\uffff\1\172\2\uffff\2\uffff\1\52\2\uffff\2\172\1\uffff\2\172\1\170\1\145\1\171\1\162\1\165\1\170\1\141\1\157\1\156\1\157\1\142\1\157\2\uffff\2\172\1\uffff\6\172\11\uffff\3\172\1\164\1\146\1\161\1\160\1\145\1\162\1\156\1\164\1\151\1\162\1\156\1\160\1\151\1\155\1\152\1\141\4\172\1\uffff\6\172\2\145\1\165\1\145\1\144\1\141\1\143\1\151\1\157\1\163\1\164\1\154\1\164\1\141\1\145\1\154\2\172\2\uffff\6\172\1\156\1\143\1\151\1\163\1\151\1\157\1\155\1\164\1\157\1\155\1\172\1\145\1\151\1\172\1\151\1\143\5\172\1\uffff\3\172\1\144\1\164\1\162\1\172\1\143\1\156\1\145\1\151\1\156\1\172\1\uffff\1\170\1\145\1\uffff\1\156\1\164\1\uffff\2\172\1\uffff\1\172\2\uffff\1\172\1\163\1\172\1\145\1\uffff\1\141\1\144\1\164\1\157\1\172\1\uffff\1\164\1\163\1\172\1\163\4\uffff\1\172\1\uffff\1\155\1\164\1\151\1\145\1\156\1\uffff\2\172\1\uffff\1\172\1\uffff\2\145\1\164\1\162\1\163\3\uffff\1\156\1\163\1\151\1\163\1\172\1\164\1\172\1\157\1\172\1\uffff\1\163\1\uffff\1\156\1\uffff\2\172\2\uffff";
    static final String DFA13_acceptS =
        "\4\uffff\1\7\1\uffff\1\17\11\uffff\1\43\1\44\1\uffff\1\45\1\46\3\uffff\1\51\1\52\2\uffff\1\37\16\uffff\1\40\1\7\2\uffff\1\17\6\uffff\1\41\1\42\1\43\1\44\1\45\1\46\1\47\1\50\1\51\27\uffff\1\22\30\uffff\1\20\1\21\33\uffff\1\26\15\uffff\1\30\2\uffff\1\35\2\uffff\1\36\2\uffff\1\23\1\uffff\1\1\1\2\4\uffff\1\6\5\uffff\1\27\4\uffff\1\11\1\25\1\24\1\3\1\uffff\1\16\5\uffff\1\13\2\uffff\1\33\1\uffff\1\4\5\uffff\1\31\1\32\1\34\11\uffff\1\12\1\uffff\1\10\1\uffff\1\14\2\uffff\1\5\1\15";
    static final String DFA13_specialS =
        "\1\0\16\uffff\1\3\5\uffff\1\1\1\2\u00ca\uffff}>";
    static final String[] DFA13_transitionS = {
            "\11\31\2\30\2\31\1\30\22\31\1\30\1\31\1\25\4\31\1\26\1\20\1\21\3\31\1\4\1\31\1\27\12\24\1\3\1\17\1\31\1\6\1\31\1\16\1\31\32\15\3\31\1\22\1\23\1\31\1\10\2\15\1\1\1\5\1\13\2\15\1\12\4\15\1\7\1\11\1\2\6\15\1\14\3\15\uff85\31",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\4\35\1\32\11\35\1\33\13\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\21\35\1\36\10\35",
            "\32\53\6\uffff\1\44\1\53\1\46\1\50\1\37\1\43\1\52\1\53\1\47\5\53\1\51\1\42\1\53\1\40\1\53\1\41\1\53\1\45\4\53",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\10\35\1\55\16\35\1\56\2\35",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\16\35\1\60\13\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\15\35\1\61\14\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\21\35\1\62\10\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\14\35\1\63\15\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\16\35\1\64\13\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\7\35\1\65\22\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\32\66\6\uffff\32\66",
            "\0\67",
            "",
            "",
            "\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "",
            "\0\74",
            "\0\74",
            "\1\75",
            "",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\5\35\1\77\24\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\14\35\1\100\15\35",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\16\35\1\101\13\35",
            "\1\103\21\uffff\1\102",
            "\1\104",
            "\1\105",
            "\1\107\20\uffff\1\106",
            "\1\110",
            "\1\111\24\uffff\1\112",
            "\1\113",
            "\1\114",
            "\1\115\1\116",
            "\1\117",
            "\1\120",
            "\1\121",
            "",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\23\35\1\122\6\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\10\35\1\123\21\35",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\23\35\1\124\6\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\3\35\1\125\26\35",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\17\35\1\127\12\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\21\35\1\130\10\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\4\35\1\131\25\35",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\10\35\1\132\21\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\1\133\31\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\1\35\1\134\30\35",
            "\1\135",
            "\1\136",
            "\1\137",
            "\1\140",
            "\1\141",
            "\1\142",
            "\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\147",
            "\1\150",
            "\1\151",
            "\1\152",
            "\1\153",
            "\1\154",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\7\35\1\155\22\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\22\35\1\156\7\35",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\13\35\1\161\16\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\1\162\31\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\15\35\1\163\14\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\15\35\1\164\14\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\10\35\1\165\21\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\13\35\1\166\16\35",
            "\1\167",
            "\1\170",
            "\1\171",
            "\1\172",
            "\1\174\1\173",
            "\1\175",
            "\1\176",
            "\1\177",
            "\1\u0080",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\1\u0087",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\4\35\1\u0088\25\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\23\35\1\u0089\6\35",
            "",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\30\35\1\u008a\1\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\13\35\1\u008b\16\35",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\4\35\1\u008d\25\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\15\35\1\u008e\14\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\4\35\1\u008f\25\35",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "\1\u0096",
            "\1\u0097",
            "\1\u0098",
            "\1\u0099",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\1\u009b",
            "\1\u009c",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\1\u009e",
            "\1\u009f",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\21\35\1\u00a1\10\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\22\35\1\u00a2\7\35",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\13\35\1\u00a4\16\35",
            "",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\14\35\1\u00a7\15\35",
            "\1\u00a8",
            "\1\u00a9",
            "\1\u00aa",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\1\u00ac",
            "\1\u00ad",
            "\1\u00ae",
            "\1\u00af",
            "\1\u00b0",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "",
            "\1\u00b2",
            "\1\u00b3",
            "",
            "\1\u00b4",
            "\1\u00b5",
            "",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            "",
            "\1\34\2\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\1\u00ba",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\1\u00bc",
            "",
            "\1\u00bd",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "",
            "\1\u00c2",
            "\1\u00c3",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\1\u00c5",
            "",
            "",
            "",
            "",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "",
            "\1\u00c7",
            "\1\u00c8",
            "\1\u00c9",
            "\1\u00ca",
            "\1\u00cb",
            "",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "",
            "\1\u00cf",
            "\1\u00d0",
            "\1\u00d1",
            "\1\u00d2",
            "\1\u00d3",
            "",
            "",
            "",
            "\1\u00d4",
            "\1\u00d5",
            "\1\u00d6",
            "\1\u00d7",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\1\u00d9",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\1\u00db",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "",
            "\1\u00dd",
            "",
            "\1\u00de",
            "",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "\1\53\2\uffff\12\53\7\uffff\32\53\4\uffff\1\53\1\uffff\32\53",
            "",
            ""
    };

    static final short[] DFA13_eot = DFA.unpackEncodedString(DFA13_eotS);
    static final short[] DFA13_eof = DFA.unpackEncodedString(DFA13_eofS);
    static final char[] DFA13_min = DFA.unpackEncodedStringToUnsignedChars(DFA13_minS);
    static final char[] DFA13_max = DFA.unpackEncodedStringToUnsignedChars(DFA13_maxS);
    static final short[] DFA13_accept = DFA.unpackEncodedString(DFA13_acceptS);
    static final short[] DFA13_special = DFA.unpackEncodedString(DFA13_specialS);
    static final short[][] DFA13_transition;

    static {
        int numStates = DFA13_transitionS.length;
        DFA13_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA13_transition[i] = DFA.unpackEncodedString(DFA13_transitionS[i]);
        }
    }

    class DFA13 extends DFA {

        public DFA13(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 13;
            this.eot = DFA13_eot;
            this.eof = DFA13_eof;
            this.min = DFA13_min;
            this.max = DFA13_max;
            this.accept = DFA13_accept;
            this.special = DFA13_special;
            this.transition = DFA13_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | RULE_NAME | RULE_IDWITHTWOPOINTBEFOREANDDASHES | RULE_IDWITHQUESTIONMARKBEFORE | RULE_SL_COMMENT | RULE_OP | RULE_CP | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA13_0 = input.LA(1);

                        s = -1;
                        if ( (LA13_0=='d') ) {s = 1;}

                        else if ( (LA13_0=='p') ) {s = 2;}

                        else if ( (LA13_0==':') ) {s = 3;}

                        else if ( (LA13_0=='-') ) {s = 4;}

                        else if ( (LA13_0=='e') ) {s = 5;}

                        else if ( (LA13_0=='=') ) {s = 6;}

                        else if ( (LA13_0=='n') ) {s = 7;}

                        else if ( (LA13_0=='a') ) {s = 8;}

                        else if ( (LA13_0=='o') ) {s = 9;}

                        else if ( (LA13_0=='i') ) {s = 10;}

                        else if ( (LA13_0=='f') ) {s = 11;}

                        else if ( (LA13_0=='w') ) {s = 12;}

                        else if ( ((LA13_0>='A' && LA13_0<='Z')||(LA13_0>='b' && LA13_0<='c')||(LA13_0>='g' && LA13_0<='h')||(LA13_0>='j' && LA13_0<='m')||(LA13_0>='q' && LA13_0<='v')||(LA13_0>='x' && LA13_0<='z')) ) {s = 13;}

                        else if ( (LA13_0=='?') ) {s = 14;}

                        else if ( (LA13_0==';') ) {s = 15;}

                        else if ( (LA13_0=='(') ) {s = 16;}

                        else if ( (LA13_0==')') ) {s = 17;}

                        else if ( (LA13_0=='^') ) {s = 18;}

                        else if ( (LA13_0=='_') ) {s = 19;}

                        else if ( ((LA13_0>='0' && LA13_0<='9')) ) {s = 20;}

                        else if ( (LA13_0=='\"') ) {s = 21;}

                        else if ( (LA13_0=='\'') ) {s = 22;}

                        else if ( (LA13_0=='/') ) {s = 23;}

                        else if ( ((LA13_0>='\t' && LA13_0<='\n')||LA13_0=='\r'||LA13_0==' ') ) {s = 24;}

                        else if ( ((LA13_0>='\u0000' && LA13_0<='\b')||(LA13_0>='\u000B' && LA13_0<='\f')||(LA13_0>='\u000E' && LA13_0<='\u001F')||LA13_0=='!'||(LA13_0>='#' && LA13_0<='&')||(LA13_0>='*' && LA13_0<=',')||LA13_0=='.'||LA13_0=='<'||LA13_0=='>'||LA13_0=='@'||(LA13_0>='[' && LA13_0<=']')||LA13_0=='`'||(LA13_0>='{' && LA13_0<='\uFFFF')) ) {s = 25;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA13_21 = input.LA(1);

                        s = -1;
                        if ( ((LA13_21>='\u0000' && LA13_21<='\uFFFF')) ) {s = 60;}

                        else s = 25;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA13_22 = input.LA(1);

                        s = -1;
                        if ( ((LA13_22>='\u0000' && LA13_22<='\uFFFF')) ) {s = 60;}

                        else s = 25;

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA13_15 = input.LA(1);

                        s = -1;
                        if ( ((LA13_15>='\u0000' && LA13_15<='\uFFFF')) ) {s = 55;}

                        else s = 25;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 13, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}