/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * generated by Xtext 2.25.0
 */
package fr.irisa.atsyra.pddl.xtext.pDDL.impl;

import fr.irisa.atsyra.pddl.xtext.pDDL.Either;
import fr.irisa.atsyra.pddl.xtext.pDDL.PDDLPackage;
import fr.irisa.atsyra.pddl.xtext.pDDL.Type;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Either</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.impl.EitherImpl#getEitherType1 <em>Either Type1</em>}</li>
 *   <li>{@link fr.irisa.atsyra.pddl.xtext.pDDL.impl.EitherImpl#getEitherType2 <em>Either Type2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EitherImpl extends MinimalEObjectImpl.Container implements Either
{
  /**
   * The cached value of the '{@link #getEitherType1() <em>Either Type1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEitherType1()
   * @generated
   * @ordered
   */
  protected Type eitherType1;

  /**
   * The cached value of the '{@link #getEitherType2() <em>Either Type2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEitherType2()
   * @generated
   * @ordered
   */
  protected EList<Type> eitherType2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EitherImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PDDLPackage.Literals.EITHER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Type getEitherType1()
  {
    return eitherType1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEitherType1(Type newEitherType1, NotificationChain msgs)
  {
    Type oldEitherType1 = eitherType1;
    eitherType1 = newEitherType1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PDDLPackage.EITHER__EITHER_TYPE1, oldEitherType1, newEitherType1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setEitherType1(Type newEitherType1)
  {
    if (newEitherType1 != eitherType1)
    {
      NotificationChain msgs = null;
      if (eitherType1 != null)
        msgs = ((InternalEObject)eitherType1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PDDLPackage.EITHER__EITHER_TYPE1, null, msgs);
      if (newEitherType1 != null)
        msgs = ((InternalEObject)newEitherType1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PDDLPackage.EITHER__EITHER_TYPE1, null, msgs);
      msgs = basicSetEitherType1(newEitherType1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PDDLPackage.EITHER__EITHER_TYPE1, newEitherType1, newEitherType1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Type> getEitherType2()
  {
    if (eitherType2 == null)
    {
      eitherType2 = new EObjectContainmentEList<Type>(Type.class, this, PDDLPackage.EITHER__EITHER_TYPE2);
    }
    return eitherType2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case PDDLPackage.EITHER__EITHER_TYPE1:
        return basicSetEitherType1(null, msgs);
      case PDDLPackage.EITHER__EITHER_TYPE2:
        return ((InternalEList<?>)getEitherType2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PDDLPackage.EITHER__EITHER_TYPE1:
        return getEitherType1();
      case PDDLPackage.EITHER__EITHER_TYPE2:
        return getEitherType2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PDDLPackage.EITHER__EITHER_TYPE1:
        setEitherType1((Type)newValue);
        return;
      case PDDLPackage.EITHER__EITHER_TYPE2:
        getEitherType2().clear();
        getEitherType2().addAll((Collection<? extends Type>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.EITHER__EITHER_TYPE1:
        setEitherType1((Type)null);
        return;
      case PDDLPackage.EITHER__EITHER_TYPE2:
        getEitherType2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PDDLPackage.EITHER__EITHER_TYPE1:
        return eitherType1 != null;
      case PDDLPackage.EITHER__EITHER_TYPE2:
        return eitherType2 != null && !eitherType2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //EitherImpl
