package fr.irisa.atsyra.absystem.gal.transfo.tests

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetBasedSystemAspect
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class AssetBasedSystemAspectTest {

	@Inject
	ParseHelper<AssetBasedSystem> parseHelper

	AssetBasedSystem model;

	@BeforeEach
	def void loadModel() {
		AbsystemPackage.eINSTANCE.eClass()
		model = parseHelper.parse('''
			AssetBasedSystem 
			with Type_def::*;
			with Default_def::*;
			DefinitionGroup Default_def {
				AssetType Any {}
				PrimitiveDataType String
				PrimitiveDataType Boolean
				PrimitiveDataType Integer
				StaticMethod contains
				StaticMethod containsAll
				StaticMethod containsAny
				StaticMethod filter
			}
			DefinitionGroup Type_def {
				AssetType T1 {
				}
				AssetType T2  {
				}
			}
			DefinitionGroup Behavior_def {
				AssetTypeAspect T2 {
					reference contained : T1 [*]
				}
				GuardedAction goes_from_to_via(t1 : T1, t2 : T2) {
					guard = true
					action {
						t2.contained.add(t1);
					} 
				}
			}
			AssetGroup example {
				Asset a1 : T1 {}
				Asset a2 : T2 {}
			}
		''')
	}

	@Test
	def void should_convert_one_asset_type_aspect_reference_to_one_array() {
		Assertions.assertNotNull(model);
		val gal = AssetBasedSystemAspect.convertSystem2GAL(model, null);
		// 3 arrays for subtypes (Any, T1, T2) + one for the reference = 4
		Assertions.assertEquals(4, gal.arrays.length)
	}

}
