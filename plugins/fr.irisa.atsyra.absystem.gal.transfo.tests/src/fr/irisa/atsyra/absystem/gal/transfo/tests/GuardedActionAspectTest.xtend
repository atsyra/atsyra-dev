package fr.irisa.atsyra.absystem.gal.transfo.tests

import com.google.inject.Inject
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.jupiter.api.Assertions.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetBasedSystemAspect.*

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class GuardedActionAspectTest {
	@Inject
	Injector injector

	AssetBasedSystem abs
	XtextResourceSet absresourceSet;
	
	@BeforeEach
	def void SetUpBeforeClass() {
		AbsystemPackage.eINSTANCE.eClass()
		absresourceSet = injector.getInstance(typeof(XtextResourceSet))
		absresourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE)
	}
	
	private def void LoadTestCase(String caseName) {
		val absuri = URI.createFileURI("testfiles/GuardedAction/" + caseName + "/input.abs")
		val absRes = absresourceSet.getResource(absuri, true)
		abs =  absRes.getContents().get(0) as AssetBasedSystem
	}

	@Test
	def test_parameters() {
		LoadTestCase("Parameters")
		Assertions.assertNotNull(abs)
		val gal = abs.convertSystem2GAL(null)
		Assertions.assertNotNull(gal)
		assertEquals(2, gal.transitions.size)
		var test = gal.transitions.get(0).name == "Type_def_do__1__Type_def_T1__Type_def_T2"
		val t1 = if(test) {
			gal.transitions.get(0)
		} else { 
			gal.transitions.get(1)
		} // do_1_T1_T2(T1 $t1, T2 $t2)
		val t2 = if(test) {
			gal.transitions.get(1)
		} else { 
			gal.transitions.get(0)
		} //do_1_T2_T2(T2 $t1, T2 $t2)
		assertEquals("Type_def_do__1__Type_def_T1__Type_def_T2", t1.name)
		assertEquals("Type_def_T1",t1.params.get(0).type.name)
		assertEquals("$t1",t1.params.get(0).name)
		assertEquals("Type_def_T2",t1.params.get(1).type.name)
		assertEquals("$t2",t1.params.get(1).name)
		assertEquals("Type_def_do__1__Type_def_T2__Type_def_T2", t2.name)
		assertEquals("Type_def_T2",t2.params.get(0).type.name)
		assertEquals("$t1",t2.params.get(0).name)
		assertEquals("Type_def_T2",t2.params.get(1).type.name)
		assertEquals("$t2",t2.params.get(1).name)
	}

}
