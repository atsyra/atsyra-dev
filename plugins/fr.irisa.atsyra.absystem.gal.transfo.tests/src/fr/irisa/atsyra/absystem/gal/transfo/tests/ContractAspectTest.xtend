package fr.irisa.atsyra.absystem.gal.transfo.tests

import com.google.inject.Inject
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.lip6.move.gal.And
import fr.lip6.move.serialization.SerializationUtil
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.Assert.assertEquals

import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetBasedSystemAspect.*

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ContractAspectTest {
	@Inject
	Injector injector

	AssetBasedSystem abs
	XtextResourceSet absresourceSet;

	@BeforeEach
	def void SetUpBeforeClass() {
		AbsystemPackage.eINSTANCE.eClass()
		absresourceSet = injector.getInstance(typeof(XtextResourceSet))
		absresourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE)
	}

	private def void LoadTestCase(String caseName) {
		val absuri = URI.createFileURI("testfiles/Contracts/" + caseName + "/input.abs")
		val absRes = absresourceSet.getResource(absuri, true)
		abs = absRes.getContents().get(0) as AssetBasedSystem
	}

	@Test
	def test_dynamic_contract_in_precondition() {
		LoadTestCase("dynamic")
		Assertions.assertNotNull(abs)

		abs.convertAllGoals2GAL(null)
		val e1 = abs.contracts.get(0)
		val e1text = SerializationUtil.getText(e1, true)
		assertEquals("(Type_def_T1.inherited_dynamic_attribute[((0*2)+T1_types[1])]!=Type_def_T2.dynamic_attribute[((0*1)+T2_types[1])])&&(Type_def_T1.inherited_dynamic_attribute[((1*2)+T1_types[1])]!=Type_def_T2.dynamic_attribute[((1*1)+T2_types[1])])", e1text)

		val testorder = (abs.contracts.get(1) as And).left instanceof And
		val e2 = if (testorder)
				abs.contracts.get(1) as And
			else
				abs.contracts.get(2) as And
		val e3 = if (!testorder)
				abs.contracts.get(1) as And
			else
				abs.contracts.get(2) as And

		val e2text = SerializationUtil.getText(e2, true)
		assertEquals("((((!(Type_def_T2.owner[((0*1)+T2_types[1])]==((0*2)+1)))||(((0*2)+1)!=((0*2)+1)))&&((!(Type_def_T2.owner[((1*1)+T2_types[1])]==((0*2)+1)))||(((1*2)+1)!=((0*2)+1))))&&((!(Type_def_T2.owner[((0*1)+T2_types[1])]==((1*2)+1)))||(((0*2)+1)!=((1*2)+1))))&&((!(Type_def_T2.owner[((1*1)+T2_types[1])]==((1*2)+1)))||(((1*2)+1)!=((1*2)+1)))", e2text)
		val e3text = SerializationUtil.getText(e3, true)
		assertEquals("((!(Type_def_T2.owner[((0*1)+T2_types[1])]==((0*2)+0)))||(((0*2)+1)!=((0*2)+0)))&&((!(Type_def_T2.owner[((1*1)+T2_types[1])]==((0*2)+0)))||(((1*2)+1)!=((0*2)+0)))", e3text)
	}

}
