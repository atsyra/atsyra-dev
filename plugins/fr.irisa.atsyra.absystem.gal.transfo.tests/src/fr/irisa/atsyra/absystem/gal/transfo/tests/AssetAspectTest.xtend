package fr.irisa.atsyra.absystem.gal.transfo.tests

import com.google.inject.Inject
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.lip6.move.serialization.SerializationUtil
import java.io.File
import java.util.Scanner
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetBasedSystemAspect.*

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class AssetAspectTest {
	@Inject
	Injector injector

	AssetBasedSystem abs
	XtextResourceSet absresourceSet;
	File galFile
	
	@BeforeEach
	def void SetUpBeforeClass() {
		AbsystemPackage.eINSTANCE.eClass()
		absresourceSet = injector.getInstance(typeof(XtextResourceSet))
		absresourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE)
	}
	
	private def void LoadTestCase(String caseName) {
		val absuri = URI.createFileURI("testfiles/Assets/" + caseName + "/input.abs")
		val absRes = absresourceSet.getResource(absuri, true)
		abs =  absRes.getContents().get(0) as AssetBasedSystem
		galFile = new File("testfiles/Assets/" + caseName + "/expected.gal")
	}
	
	private def void TestByText() {
		Assertions.assertNotNull(abs)
		val scanner = new Scanner(galFile)
		try {
			val gal = abs.convertSystem2GAL(null)
			Assertions.assertNotNull(gal)
			val testText = SerializationUtil.getText(gal, true)
			Assertions.assertLinesMatch(scanner.useDelimiter("\n").tokens, testText.lines)
		} catch (RuntimeException e) {
			Assertions.fail(e.message)
			return
		} finally {
			scanner.close()
		}
	}

	@Test
	def test_assetAttributeValues() {
		LoadTestCase("assetAttributeValues")
		TestByText()
	}
	
	@Test
	def test_oppositeLinks() {
		LoadTestCase("oppositeLinks")
		TestByText()
	}

}
