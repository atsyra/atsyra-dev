package fr.irisa.atsyra.absystem.gal.transfo.tests

import com.google.inject.Inject
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.lip6.move.gal.GALTypeDeclaration
import fr.lip6.move.serialization.SerializationUtil
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle
import org.junit.jupiter.api.TestMethodOrder
import org.junit.jupiter.api.^extension.ExtendWith

import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetBasedSystemAspect.*

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation)
class ActionAspectTest {
	@Inject
	ParseHelper<AssetBasedSystem> parseHelper

	AssetBasedSystem abs
	GALTypeDeclaration gal

	@BeforeAll
	def void loadModel() {

		AbsystemPackage.eINSTANCE.eClass()
		abs = parseHelper.parse('''
			AssetBasedSystem 
			with Type_def::*;
			with Default_def::*;
			DefinitionGroup Default_def {
				AssetType Any {}
				PrimitiveDataType String
				PrimitiveDataType Boolean
				PrimitiveDataType Integer
				StaticMethod contains
				StaticMethod containsAll
				StaticMethod containsAny
				StaticMethod filter
			}
			DefinitionGroup Type_def {
				AssetType T1 {
				}
				AssetType T2  {
				}
			}
			DefinitionGroup Behavior_def {
				AssetTypeAspect T1 {
					attribute value : Boolean
				}
				AssetTypeAspect T2 {
					reference contained : T1 [*]
				}
				GuardedAction transi(t1 : T1, t2 : T2, t3 : T2) {
					guard = true
					action {
						t2.contained.add(t1);
						t2.contained.addAll(t3.contained);
						t1.value.assign(true);
						t2.contained.clear();
						t2.contained.forAll({t -> t.value.assign(true)});
						t2.contained.remove(t1);
						t2.contained.removeAll(t3.contained);
					} 
				}
			}
			AssetGroup example {
				Asset a1 : T1 {}
				Asset a2 : T2 {}
				Asset a3 : T2 {}
			}
		''')
	}

	@Test
	@Order(0)
	def test_to_gal() {
		Assertions.assertNotNull(abs)
		gal = abs.convertSystem2GAL(null)
		Assertions.assertNotNull(gal)
		Assertions.assertTrue(!gal.transitions.empty, "Transitions missing.")
		val transition = gal.transitions.get(0)
		Assertions.assertTrue(!transition.actions.empty, "Actions missing.")
	}

	@Test
	@Order(1)
	def test_add() {
		Assertions.assertNotNull(gal)
		val action = gal.transitions.get(0).actions.get(0)
		val actionText = SerializationUtil.getText(action, true)
		Assertions.assertEquals('''if (((($t1*3)+1)%3)==1) {
  Type_def_T2.Type_def_T1.contained[(((($t2*1)+T2_types[2])*1)+((($t1*3)+1)/3))] = 1  ;
}
'''.toString, actionText)
	}

	@Test
	@Order(1)
	def test_addAll() {
		Assertions.assertNotNull(gal)
		val action = gal.transitions.get(0).actions.get(1)
		val actionText = SerializationUtil.getText(action, true)
		Assertions.assertEquals('''if (Type_def_T2.Type_def_T1.contained[(((($t3*1)+T2_types[2])*1)+0)]==1) {
  Type_def_T2.Type_def_T1.contained[(((($t2*1)+T2_types[2])*1)+0)] = 1  ;
}
'''.toString, actionText)
	}

	@Test
	@Order(1)
	def test_assign() {
		Assertions.assertNotNull(gal)
		val action = gal.transitions.get(0).actions.get(2)
		val actionText = SerializationUtil.getText(action, true)
		Assertions.assertEquals('''Type_def_T1.value[(($t1*1)+T1_types[1])] = 1;
		'''.toString, actionText)
	}

	@Test
	@Order(1)
	def test_clear() {
		Assertions.assertNotNull(gal)
		val action = gal.transitions.get(0).actions.get(3)
		val actionText = SerializationUtil.getText(action, true)
		Assertions.assertEquals('''Type_def_T2.Type_def_T1.contained[(((($t2*1)+T2_types[2])*1)+0)] = 0;
		'''.toString, actionText)
	}

	@Test
	@Order(1)
	def test_forAll() {
		Assertions.assertNotNull(gal)
		val action = gal.transitions.get(0).actions.get(4)
		val actionText = SerializationUtil.getText(action, true)
		Assertions.assertEquals('''if (Type_def_T2.Type_def_T1.contained[(((($t2*1)+T2_types[2])*1)+0)]==1) {
  Type_def_T1.value[((0*1)+T1_types[1])] = 1  ;
}
'''.toString, actionText)
	}

	@Test
	@Order(1)
	def test_remove() {
		Assertions.assertNotNull(gal)
		val action = gal.transitions.get(0).actions.get(5)
		val actionText = SerializationUtil.getText(action, true)
		Assertions.assertEquals('''if (((($t1*3)+1)%3)==1) {
  Type_def_T2.Type_def_T1.contained[(((($t2*1)+T2_types[2])*1)+((($t1*3)+1)/3))] = 0  ;
}
'''.toString, actionText)
	}

	@Test
	@Order(1)
	def test_removeAll() {
		Assertions.assertNotNull(gal)
		val action = gal.transitions.get(0).actions.get(6)
		val actionText = SerializationUtil.getText(action, true)
		Assertions.assertEquals('''if (Type_def_T2.Type_def_T1.contained[(((($t3*1)+T2_types[2])*1)+0)]==1) {
  Type_def_T2.Type_def_T1.contained[(((($t2*1)+T2_types[2])*1)+0)] = 0  ;
}
'''.toString, actionText)
	}

}
