package fr.irisa.atsyra.absystem.gal.transfo.tests

import com.google.common.collect.Comparators
import com.google.common.collect.Ordering
import com.google.inject.Inject
import com.google.inject.Injector
import fr.irisa.atsyra.absystem.gal.transfo.aspects.Context
import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.IntConstant
import fr.lip6.move.gal.Not
import fr.lip6.move.gal.Or
import fr.lip6.move.serialization.SerializationUtil
import java.io.File
import java.util.Scanner
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.Assert.assertTrue
import static org.junit.jupiter.api.Assertions.*
import static org.junit.jupiter.api.Assumptions.*

import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.AssetBasedSystemAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.ExpressionAspect.*
import static extension fr.irisa.atsyra.absystem.gal.transfo.aspects.IntConstantAspect.*

@ExtendWith(InjectionExtension)
@InjectWith(AssetBasedSystemDslInjectorProvider)
class ExpressionAspectTest {
	@Inject
	Injector injector

	AssetBasedSystem abs
	XtextResourceSet absresourceSet;
	File galFile

	@BeforeEach
	def void SetUpBeforeClass() {
		AbsystemPackage.eINSTANCE.eClass()
		absresourceSet = injector.getInstance(typeof(XtextResourceSet))
		absresourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE)
	}

	private def void LoadTestCase(String caseName) {
		val absuri = URI.createFileURI("testfiles/Expressions/" + caseName + "/input.abs")
		val absRes = absresourceSet.getResource(absuri, true)
		abs = absRes.getContents().get(0) as AssetBasedSystem
		galFile = new File("testfiles/Expressions/" + caseName + "/expected.gal")
	}

	private def void TestByText() {
		Assertions.assertNotNull(abs)
		val scanner = new Scanner(galFile)
		val gal = abs.convertSystem2GAL(null)
		Assertions.assertNotNull(gal)
		val testText = SerializationUtil.getText(gal, true)
		Assertions.assertLinesMatch(scanner.useDelimiter("\n").tokens, testText.lines)
		scanner.close()
	}
	
	private def void TestByTextWithFirstGoal() {
		Assertions.assertNotNull(abs)
		val goal = abs.assetGroups.stream.flatMap[goals.stream].findFirst.orElseThrow
		val scanner = new Scanner(galFile)
		val gal = abs.convertGoal2GAL(goal, null)
		Assertions.assertNotNull(gal)
		val testText = SerializationUtil.getText(gal, true)
		Assertions.assertLinesMatch(scanner.useDelimiter("\n").tokens, testText.lines)
		scanner.close()
	}

	@Test
	def test_contains() {
		LoadTestCase("contains")
		TestByText()
	}

	@Test
	def test_containsAll() {
		LoadTestCase("containsAll")
		TestByText()
	}

	@Test
	def test_containsAny() {
		LoadTestCase("containsAny")
		TestByText()
	}

	@Test
	def test_filter() {
		LoadTestCase("filter")
		TestByText()
	}

	@Test
	def test_isEmpty() {
		LoadTestCase("isEmpty")
		TestByText()
	}

	@Test
	def test_filter_isEmpty() {
		LoadTestCase("filter_isEmpty")
		TestByText()
	}

	@Test
	def void test_implies() {
		val left = AbsystemFactory.eINSTANCE.createBooleanConstant
		left.value = "true"
		val right = AbsystemFactory.eINSTANCE.createBooleanConstant
		right.value = "false"
		val test = AbsystemFactory.eINSTANCE.createImpliesExpression
		test.lhs = left
		test.rhs = right
		val context = new Context()
		try {
			val left2gal = left.expr2boolExpr(context)
			val right2gal = right.expr2boolExpr(context)
			try {
				val test2gal = test.expr2boolExpr(context)
				assertNotNull(test2gal)
				val test2gal_or = test2gal as Or
				val test2gal_left_not = test2gal_or.left as Not
				assertTrue(EcoreUtil.equals(left2gal, test2gal_left_not.value))
				assertTrue(EcoreUtil.equals(right2gal, test2gal_or.right))
			} catch (RuntimeException e) {
				fail(e)
			}
		} catch (RuntimeException e) {
			assumeTrue(false, e.message)
		}
	}

	@Test
	def test_intconstant() {
		LoadTestCase("IntConstant")
		TestByText()
		val intmap = abs.eAllContents.filter(IntConstant).map[it.value -> it.const2int()].toList.sortBy[it.key]
		assertTrue(Comparators.isInOrder(intmap, [p1, p2|Ordering.natural().compare(p1.value, p2.value)]))
	}

	@Test
	def test_enumconstant() {
		LoadTestCase("EnumConstant")
		TestByText()
	}

	@Test
	def void test_inequality() {
		LoadTestCase("Inequality")
		TestByText()
	}
	
	@Test
	def test_versionconstant() {
		LoadTestCase("VersionConstant")
		TestByText()
	}
	
	@Test
	def test_collection() {
		LoadTestCase("Collection")
		TestByTextWithFirstGoal()
	}
	
}
