/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyragoal.design;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.XtextResource;

import atsyragoal.AbstractAtsyraTree;
import atsyragoal.AtsyraGoal;
import atsyragoal.AtsyraGoalModel;
import atsyragoal.AtsyraTree;
import atsyragoal.AtsyraTreeOperator;
import atsyragoal.AtsyragoalFactory;
import atsyragoal.BooleanLiteral;
import atsyragoal.BooleanSystemCondition;
import atsyragoal.util.AtsyragoalHelper;
import fr.irisa.atsyra.atsyra2.ide.ui.views.correctnessresult.CorrectnessResultView;
import fr.irisa.atsyra.atsyra2.ide.ui.views.correctnessresult.ViewLabelProvider;
import fr.irisa.atsyra.resultstore.Result;
import fr.irisa.atsyra.resultstore.ResultStore;
import fr.irisa.atsyra.resultstore.ResultstoreFactory;
import fr.irisa.atsyra.resultstore.TreeResult;
import fr.irisa.atsyra.resultstore.util.SyntheticResultHelper;
import fr.irisa.atsyra.resultstore.util.SyntheticResultHelper.SyntheticLocalRefinementResult;

/**
 * The services class used by VSM.
 */
public class Services {

	/**
	 * See
	 * http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.sirius.doc%2Fdoc%2Findex.html&cp=24
	 * for documentation on how to write service methods.
	 */
	public EObject myService(EObject self, String arg) {
		// TODO Auto-generated code
		return self;
	}

	public AbstractAtsyraTree nextInOperands(AbstractAtsyraTree self) {
		if (self.eContainer() != null && self.eContainer() instanceof AtsyraTree) {
			AtsyraTree parent = (AtsyraTree) self.eContainer();
			EList<AbstractAtsyraTree> operands = parent.getOperands();
			int index = operands.indexOf(self);
			if (index + 1 < operands.size()) {
				return operands.get(index + 1);
			}
		}
		return null;
	}

	public AbstractAtsyraTree nextInSandOperands(AbstractAtsyraTree self) {
		if (self.eContainer() != null && self.eContainer() instanceof AtsyraTree) {
			AtsyraTree parent = (AtsyraTree) self.eContainer();
			if (parent.getOperator().equals(AtsyraTreeOperator.SAND)) {
				EList<AbstractAtsyraTree> operands = parent.getOperands();
				int index = operands.indexOf(self);
				if (index + 1 < operands.size()) {
					return operands.get(index + 1);
				}
			}
		}
		return null;
	}

	public AbstractAtsyraTree nextInAndOperands(AbstractAtsyraTree self) {
		if (self.eContainer() != null && self.eContainer() instanceof AtsyraTree) {
			AtsyraTree parent = (AtsyraTree) self.eContainer();
			if (parent.getOperator().equals(AtsyraTreeOperator.AND)) {
				EList<AbstractAtsyraTree> operands = parent.getOperands();
				int index = operands.indexOf(self);
				if (index + 1 < operands.size()) {
					return operands.get(index + 1);
				}
			}
		}
		return null;
	}

	public AbstractAtsyraTree nextInOrOperands(AbstractAtsyraTree self) {
		if (self.eContainer() != null && self.eContainer() instanceof AtsyraTree) {
			AtsyraTree parent = (AtsyraTree) self.eContainer();
			if (parent.getOperator().equals(AtsyraTreeOperator.OR)) {
				EList<AbstractAtsyraTree> operands = parent.getOperands();
				int index = operands.indexOf(self);
				if (index + 1 < operands.size()) {
					return operands.get(index + 1);
				}
			}
		}
		return null;
	}

	
	public List<AbstractAtsyraTree> firstAndSandOperands(AtsyraTree self) {
		List<AbstractAtsyraTree> res = new ArrayList<AbstractAtsyraTree>();
		if(self.getOperator().equals(AtsyraTreeOperator.AND)) {
			res.addAll(self.getOperands());
		}
		if(self.getOperator().equals(AtsyraTreeOperator.SAND)) {
			res.add(self.getOperands().get(0));
		}
		return res;
	}
	public List<AbstractAtsyraTree> lastAndSandOperands(AtsyraTree self) {
		List<AbstractAtsyraTree> res = new ArrayList<AbstractAtsyraTree>();
		if(self.getOperator().equals(AtsyraTreeOperator.AND)) {
			res.addAll(self.getOperands());
		}
		if(self.getOperator().equals(AtsyraTreeOperator.SAND) && self.getOperands().size() != 0) {
			res.add(self.getOperands().get(self.getOperands().size()-1));
		}
		return res;
	}
	
	public EObject openTextEditor(EObject any) {
		if (any != null && any.eResource() instanceof XtextResource && any.eResource().getURI() != null) {

			String fileURI = any.eResource().getURI().toPlatformString(true);
			IFile workspaceFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(fileURI));
			if (workspaceFile != null) {
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					IEditorPart openEditor = IDE.openEditor(page, workspaceFile,
							"fr.irisa.atsyra.atsyragoal.xtext.AtsyRAGoal", true);
					if (openEditor instanceof AbstractTextEditor) {
						ICompositeNode node = NodeModelUtils.findActualNodeFor(any);
						if (node != null) {
							int offset = node.getOffset();
							int length = node.getTotalEndOffset() - offset;
							((AbstractTextEditor) openEditor).selectAndReveal(offset, length);
						}
					}
					// editorInput.
				} catch (PartInitException e) {
					Activator.eclipseError(e.getMessage(), e);
				}
			}
		}
		System.out.println(any);
		return any;
	}
	
	public EObject openBasicHoveringDialog(EObject any) {
		if (any != null && any.eResource() instanceof XtextResource && any.eResource().getURI() != null) {

			String fileURI = any.eResource().getURI().toPlatformString(true);
			IFile workspaceFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(fileURI));
			if (workspaceFile != null) {
				ICompositeNode node = NodeModelUtils.findActualNodeFor(any);
				if (node != null) {
					String allText = node.getText();
					IEditorPart part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();							
					InfoPopUp pop = new InfoPopUp( part.getSite().getShell() , "Goal details","press ESC to close");
					pop.setText(allText.trim());
					pop.open();
				}
			}
		}
		return any;
	}
	
	/**
	 * create a new goal with precondition/postcondition = true
	 * and with a name based on the tree name
	 * @param tree
	 * @return
	 */
	public AtsyraGoal createNewGoal(EObject any) {
		
		if(any instanceof AtsyraTree) {
			AtsyraTree tree = (AtsyraTree) any;
		
			AtsyraGoal goal = AtsyragoalFactory.eINSTANCE.createAtsyraGoal();
			tree.setMainGoal(goal);
			AtsyraGoalModel model = (AtsyraGoalModel) EcoreUtil.getRootContainer(tree);
			model.getAtsyragoals().add(goal);
			goal.setName(AtsyragoalHelper.getFullQualifiedName(tree)+"_goal");
			
			// find System type true in referenced resources
			EcoreUtil.resolveAll(tree.eResource().getResourceSet());
			Optional<BooleanLiteral> trueLiteral = findTrueLiteral(tree.eResource().getResourceSet());
			if(trueLiteral.isPresent()) {
				BooleanSystemCondition true1 = AtsyragoalFactory.eINSTANCE.createBooleanSystemCondition();
				true1.setSource(trueLiteral.get());
				BooleanSystemCondition true2 = AtsyragoalFactory.eINSTANCE.createBooleanSystemCondition();
				true2.setSource(trueLiteral.get());
			
				goal.setPrecondition(true1);
				goal.setPostcondition(true2);
			}
			return goal;
		} else {
			return null;
		}
		
		
	}
	
	public static Optional<BooleanLiteral> findTrueLiteral(ResourceSet resSet) {
		for(Resource res : resSet.getResources()) {
			for(EObject robj : res.getContents()) {
				Optional<BooleanLiteral> trueLit = EcoreUtil2.getAllContentsOfType(robj, BooleanLiteral.class).stream().filter(lit -> lit.getName().equalsIgnoreCase("true")).findFirst();
				if(trueLit.isPresent()) { 
					return trueLit; 
				}
			}
		}
		return Optional.empty();
	}
	
	public static boolean isPerfectLocalMatch(AtsyraTree tree) {
		ResultStore resStore = CorrectnessResultView.getResultStoreModel(tree);
		Optional<TreeResult> treeRes = resStore.getTreeResults().stream().filter(tr -> tr.getTree().getName().equals(tree.getName())).findFirst();
		if(treeRes.isPresent()) {		
			return SyntheticResultHelper.getSyntheticLocalRefinementTreeResult(treeRes.get()) == SyntheticLocalRefinementResult.PERFECT_LOCAL_MATCH;
		}
		return false;
	}
	
	public static Image getCorrectnessImageString(AtsyraTree tree) {
		ResultStore resStore = CorrectnessResultView.getResultStoreModel(tree);
		Optional<TreeResult> treeRes = resStore.getTreeResults().stream().filter(tr -> tr.getTree().getName().equals(tree.getName())).findFirst();
		TreeResult tr;
		if(treeRes.isPresent()) {	
			tr = treeRes.get();			
		} else {
			tr =ResultstoreFactory.eINSTANCE.createTreeResult();
			tr.setTree(tree);
		}
		ViewLabelProvider labelProvider = new ViewLabelProvider(null);
		return labelProvider.getImage(tr);
	}
	public static TreeResult getTreeResult(AtsyraTree tree) {
		ResultStore resStore = CorrectnessResultView.getResultStoreModel(tree);
		Optional<TreeResult> treeRes = resStore.getTreeResults().stream().filter(tr -> tr.getTree().getName().equals(tree.getName())).findFirst();
		if(treeRes.isPresent()) {	
			return treeRes.get();			
		} 

		return null;
		/* else {
			TreeResult tr =ResultstoreFactory.eINSTANCE.createTreeResult();
			tr.setTree(tree);
			return tr;
		} */
	}
	
	public static Result getTreeAdmissibilityResult(AtsyraTree tree) {
		ResultStore resStore = CorrectnessResultView.getResultStoreModel(tree);
		Optional<TreeResult> treeRes = resStore.getTreeResults().stream().filter(tr -> tr.getTree().getName().equals(tree.getName())).findFirst();
		if(treeRes.isPresent()) {	
			return treeRes.get().getAdmissibilityResult();			
		}
		return null;
		/*else {
			TreeResult tr =ResultstoreFactory.eINSTANCE.createTreeResult();
			tr.setTree(tree);
			return tr.getAdmissibilityResult();
		}*/
	}
	
	public static boolean hasLocalRefinement(AtsyraTree tree) {
		return !tree.getOperands().isEmpty();
	}
}
