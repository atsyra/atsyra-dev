/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.atsyragoal.design;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystemManager;
import org.eclipse.sirius.business.api.componentization.ViewpointRegistry;
import org.eclipse.sirius.viewpoint.description.Viewpoint;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {
    // The plug-in ID
    public static final String PLUGIN_ID = "fr.irisa.atsyra.atsyragoal.design";

    // The shared instance
    private static Activator plugin;

    private static Set<Viewpoint> viewpoints; 

    /**
     * The constructor
     */
    public Activator() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    public void start(BundleContext context) throws Exception {
      super.start(context);
	  plugin = this;
	  viewpoints = new HashSet<Viewpoint>();
	  viewpoints.addAll(ViewpointRegistry.getInstance().registerFromPlugin(PLUGIN_ID + "/description/atsyragoal.odesign")); 
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    public void stop(BundleContext context) throws Exception {
	plugin = null;
	if (viewpoints != null) {
	    for (final Viewpoint viewpoint: viewpoints) {
		ViewpointRegistry.getInstance().disposeFromPlugin(viewpoint);
	    }
	    viewpoints.clear();
	    viewpoints = null; 
	}
	super.stop(context);
    }

    
	/**
	 * Use this when something went wrong in the plugin itself (internal warning)
	 * @param msg
	 * @param e
	 */
	public static void eclipseWarn(String msg, Throwable e){
		getMessagingSystem().warn(msg, msgGroup, e);
		getMessagingSystem().focus();
	}
	/**
	 * Use this when something went wrong in the plugin itself (internal error)
	 * @param msg
	 * @param e
	 */
	public static void eclipseError(String msg, Throwable e){
		getMessagingSystem().error(msg, msgGroup, e);
		getMessagingSystem().focus();
	}
	public static void debug(String msg){
		//System.out.println(msg);
		getMessagingSystem().debug(msg, msgGroup);
	}
	public static void error(String msg){
		getMessagingSystem().error(msg, msgGroup);
		getMessagingSystem().focus();
	}
	public static void info(String str) {
		getMessagingSystem().info(str, msgGroup);
		getMessagingSystem().focus();
	}
	public static void warn(String str) {
		getMessagingSystem().warn(str, msgGroup);
		getMessagingSystem().focus();
	}
	public static void important(String str) {
		getMessagingSystem().important(str, msgGroup);
		getMessagingSystem().focus();
	}
    
    
    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static Activator getDefault() {
	return plugin;
    }
    
    public static String baseMsgGroup = "fr.irisa.atsyra";
	public static String msgGroup = baseMsgGroup+".ide.ui";
	protected static MessagingSystem messagingSystem = null;
	public static MessagingSystem getMessagingSystem() {
		if(messagingSystem == null) {
			MessagingSystemManager msm = new MessagingSystemManager();
			messagingSystem = msm.createBestPlatformMessagingSystem(baseMsgGroup, "ATSyRA");
		}
		return messagingSystem;
	}
}
