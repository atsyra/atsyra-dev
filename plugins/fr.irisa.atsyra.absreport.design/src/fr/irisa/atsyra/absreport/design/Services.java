package fr.irisa.atsyra.absreport.design;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import fr.irisa.atsyra.absreport.aBSReport.ABSGoalWitness;
import fr.irisa.atsyra.absreport.aBSReport.GoalReport;
import fr.irisa.atsyra.absreport.aBSReport.Step;
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;

/**
 * The services class used by VSM.
 */
public class Services {

	/**
	 * See
	 * http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.sirius.doc%2Fdoc%2Findex.html&cp=24
	 * for documentation on how to write service methods.
	 */
	public EObject myService(EObject self, String arg) {
		// TODO Auto-generated code
		return self;
	}

	public String getWitnessID(ABSGoalWitness witness) {
		GoalReport report = (GoalReport) witness.eContainer();

		return String.format("%s(%d)", report.getGoal().getName(), report.getWitnesses().indexOf(witness));
	}

	public int stepIndex(Step step) {
		ABSGoalWitness witness = (ABSGoalWitness) step.eContainer();
		EList<Step> steps = witness.getSteps();
		return steps.indexOf(step);
	}
	public int scenarioIndex(Step step) {
		ABSGoalWitness witness = (ABSGoalWitness) step.eContainer();
		GoalReport report = (GoalReport) witness.eContainer();
		return report.getWitnesses().indexOf(witness);
	}
	
	public Step nextStep(Step step) {
		ABSGoalWitness witness = (ABSGoalWitness) step.eContainer();
		EList<Step> steps = witness.getSteps();
		int index = steps.indexOf(step);
		if (steps.size() > index+1) {
			return steps.get(index + 1);
		} else {
			return null;
		}
	}

	public String getStepID(Step step) {
		ABSGoalWitness witness = (ABSGoalWitness) step.eContainer();
		EList<Step> steps = witness.getSteps();
		GoalReport report = (GoalReport) witness.eContainer();

		return String.format("%d::%d ", report.getWitnesses().indexOf(witness), steps.indexOf(step));
	}
	
	public String getStepLongID(Step step) {
		ABSGoalWitness witness = (ABSGoalWitness) step.eContainer();
		EList<Step> steps = witness.getSteps();
		GoalReport report = (GoalReport) witness.eContainer();

		String params = step.getParameters().stream().map(p -> p.getName()).collect(Collectors.joining(", "));
		return String.format("%d::%d - %s", report.getWitnesses().indexOf(witness), steps.indexOf(step), params);
	}

	public List<GuardedAction> getWitnessGuardedActions(ABSGoalWitness witness) {
		Set<GuardedAction> gaSet = witness.getSteps().stream().map(s -> s.getGuardedAction())
				.collect(Collectors.toSet());
		return gaSet.stream().collect(Collectors.toList());
	}

	public List<Step> getUnifiedSteps(ABSGoalWitness witness) {
		return getUnifiedSteps(witness.getSteps());
	}

	public List<Step> getUnifiedSteps(List<Step> inputSteps) {

		HashMap<String, Step> hashMap = getUnifiedStepsMap(inputSteps);
		return hashMap.values().stream().collect(Collectors.toList());
	}

	protected HashMap<String, Step> getUnifiedStepsMap(List<Step> inputSteps) {
		HashMap<String, Step> hashMap = new HashMap<String, Step>();
		for (Step step : inputSteps) {
			String key = getUnifiedStepKey(step);
			if (!hashMap.containsKey(key)) {
				hashMap.put(key, step);
			}
		}
		return hashMap;
	}

	public String getUnifiedStepKey(Step step) {
		String params = step.getParameters().stream().map(p -> p.getName()).collect(Collectors.joining(", "));
		return String.format("%s(%s)", step.getGuardedAction().getName(), params);
	}

	public Step getUnifiedStepReference(Step step, List<Step> inputSteps) {
		HashMap<String, Step> hashMap = getUnifiedStepsMap(inputSteps);
		return hashMap.get(getUnifiedStepKey(step));
	}

	public List<Step> nextUnifiedSteps(Step step) {
		List<Step> allUnifiedSteps = getUnifiedSteps(((ABSGoalWitness) step.eContainer()));
		return nextUnifiedStepsFromList(step, allUnifiedSteps);
	}

	public Step nextUnifiedStepsFromList2(Step step, List<Step> inputSteps) {
		
		return getUnifiedStepReference(nextStep(step), inputSteps);
	}

	public List<Step> nextUnifiedStepsInGoal2(Step step) {
		GoalReport goalReport = (GoalReport) step.eContainer().eContainer();
		List<Step> allSteps = goalReport.getWitnesses().stream().flatMap(w -> w.getSteps().stream())
				.collect(Collectors.toList());
		System.out.println("allUnifiedSteps.size()="+ allSteps.size());
		Step nextStep = nextStep(step);
		if(nextStep != null) {
			Step res = getUnifiedStepReference(nextStep, allSteps);
			return Arrays.asList(res);
		} else return null;
	}
	
	public List<Step> getAllEquivalentStepsInGoalReport(Step step) {
		GoalReport goalReport = (GoalReport) step.eContainer().eContainer();
		String searchKey = getUnifiedStepKey(step);
		List<Step> allSteps = goalReport.getWitnesses().stream().flatMap(w -> w.getSteps().stream())
				.collect(Collectors.toList());
		List<Step> equivSteps = allSteps.stream().filter(s -> getUnifiedStepKey(s).equals(searchKey))
				.collect(Collectors.toList());
		return equivSteps;
	}
	
	public List<Step> nextUnifiedStepsFromList(Step step, List<Step> inputSteps) {
		// find all equivalent Steps
		String searchKey = getUnifiedStepKey(step);
		List<Step> equivSteps = inputSteps.stream().filter(s -> getUnifiedStepKey(s).equals(searchKey))
				.collect(Collectors.toList());
		System.out.println("inputSteps.size()="+inputSteps.size()+"  equivSteps.size()="+ equivSteps.size());
		// return the next of each of them if not null
		Set<Step> nextSteps = new HashSet<Step>();
		for (Step equivStep : equivSteps) {
			nextSteps.add(getUnifiedStepReference(nextStep(equivStep), inputSteps));
		}
		return nextSteps.stream().collect(Collectors.toList());
	}

	public List<Step> nextUnifiedStepsInGoal(Step step) {
		GoalReport goalReport = (GoalReport) step.eContainer().eContainer();
		List<Step> allSteps = goalReport.getWitnesses().stream().flatMap(w -> w.getSteps().stream())
				.collect(Collectors.toList());
		
		return nextUnifiedStepsFromList(step, allSteps);
	}
	
	public Step previousStep(Step step) {
		ABSGoalWitness witness = (ABSGoalWitness) step.eContainer();
		EList<Step> steps = witness.getSteps();
		int index = steps.indexOf(step);
		if (index > 0) {
			return steps.get(index - 1);
		} else {
			return null;
		}
	}
	
	public List<Step> childrenStepsInGoal(Step step) {
		GoalReport goalReport = (GoalReport) step.eContainer().eContainer();
		List<Step> allSteps = goalReport.getWitnesses().stream().flatMap(w -> w.getSteps().stream())
				.collect(Collectors.toList());
		
		List<Step> allUnifiedSteps = getUnifiedSteps(allSteps);
		return nextUnifiedStepsFromList(step, allSteps).stream().filter(s -> step.equals(previousStep(s)) || !allUnifiedSteps.contains(previousStep(s))).collect(Collectors.toList());
	}
	
	public List<Step> getUnifiedSteps(GoalReport goalReport) {
		List<Step> allSteps = goalReport.getWitnesses().stream().flatMap(w -> w.getSteps().stream())
				.collect(Collectors.toList());
		System.out.println("allSteps.size()="+ allSteps.size());
		return getUnifiedSteps(allSteps);
	}

}
