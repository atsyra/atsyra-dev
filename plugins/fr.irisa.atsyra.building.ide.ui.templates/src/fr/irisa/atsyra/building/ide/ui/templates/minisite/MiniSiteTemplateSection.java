/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.templates.minisite;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;

import org.eclipse.gemoc.commons.eclipse.pde.wizards.pages.pde.ui.BaseProjectWizardFields;
import fr.irisa.atsyra.building.ide.ui.templates.AtsyraBuildingTemplateSection;
import fr.irisa.atsyra.building.ide.ui.templates.IHelpContextIds;
import fr.irisa.atsyra.building.ide.ui.templates.TemplateMessages;
import fr.irisa.atsyra.building.ide.ui.wizards.pages.NewAtsyraBuildingProjectWizardFields;

public class MiniSiteTemplateSection extends AtsyraBuildingTemplateSection {
	public static final String KEY_BUILDING_NAME = "buildingName"; //$NON-NLS-1$
	public static final String KEY_ZONE1_NAME = "zone1Name"; //$NON-NLS-1$
	public static final String KEY_ZONE2_NAME = "zone2Name"; //$NON-NLS-1$
	public static final String KEY_ACCESS_NAME = "accessName"; //$NON-NLS-1$
	public static final String BUILDING_NAME = "MyBuilding"; //$NON-NLS-1$
	public static final String ZONE1_NAME = "exterior"; //$NON-NLS-1$
	public static final String ZONE2_NAME = "interior"; //$NON-NLS-1$
	public static final String ACCESS_NAME = "frontdoor"; //$NON-NLS-1$

	/**
	 * Constructor for HelloWorldTemplate.
	 */
	public MiniSiteTemplateSection() {
		setPageCount(1);
		createOptions();
	}

	/** 
	 * used to retrieve the template folder
	 */
	public String getSectionId() {
		return "miniSite"; //$NON-NLS-1$
	}

	/*
	 * @see ITemplateSection#getNumberOfWorkUnits()
	 */
	public int getNumberOfWorkUnits() {
		return super.getNumberOfWorkUnits() + 1;
	}

	private void createOptions() {
		addOption(KEY_BUILDING_NAME, TemplateMessages.MiniSiteTemplate_buildingName, BUILDING_NAME, 0);
		//addOption(KEY_PACKAGE_NAME, TemplateMessages.MiniAspectSampleTemplate_packageName, (String) null, 0);
		addOption(KEY_ZONE1_NAME, TemplateMessages.MiniSiteTemplate_zone1Name, ZONE1_NAME, 0);
		addOption(KEY_ZONE2_NAME, TemplateMessages.MiniSiteTemplate_zone2Name, ZONE2_NAME, 0);
		addOption(KEY_ACCESS_NAME, TemplateMessages.MiniSiteTemplate_accessName, ACCESS_NAME, 0);
	}

	public void addPages(Wizard wizard) {
		WizardPage page = createPage(0, IHelpContextIds.TEMPLATE_MINI_SITE);
		page.setTitle(TemplateMessages.MiniSiteTemplate_title);
		page.setDescription(TemplateMessages.MiniSiteTemplate_desc);
		wizard.addPage(page);
		markPagesAdded();
	}

	public boolean isDependentOnParentWizard() {
		return true;
	}

	protected void initializeFields(BaseProjectWizardFields data) {
		String buildingName = getFormattedPackageName(((NewAtsyraBuildingProjectWizardFields)data).projectName);
		initializeOption(KEY_BUILDING_NAME, buildingName);
	}


	public String getUsedExtensionPoint() {
		return "org.eclipse.ui.actionSets"; //$NON-NLS-1$
	}


	/* (non-Javadoc)
	 * @see org.eclipse.pde.ui.templates.ITemplateSection#getFoldersToInclude()
	 */
	public String[] getNewFiles() {
		return new String[] {"icons/"}; //$NON-NLS-1$
	}
}
