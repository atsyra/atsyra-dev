/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.templates;

public interface IHelpContextIds {

	public static final String PREFIX = "org.eclipse.pde.doc.user."; //$NON-NLS-1$	
	public static final String TEMPLATE_MINI_SITE = PREFIX + "template_mini_site"; //$NON-NLS-1$
	public static final String TEMPLATE_SMALL_RECEPTION = PREFIX + "template_small_reception"; //$NON-NLS-1$
	

}
