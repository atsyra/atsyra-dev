/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

package fr.irisa.atsyra.building.ide.ui.templates.smallreception;


import org.eclipse.gemoc.commons.eclipse.pde.wizards.pages.pde.ui.BaseProjectWizardFields;
import org.eclipse.gemoc.commons.eclipse.pde.wizards.pages.pde.ui.templates.ITemplateSection;
import org.eclipse.gemoc.commons.eclipse.pde.wizards.pages.pde.ui.templates.NewProjectTemplateWizard;
import fr.irisa.atsyra.building.ide.ui.templates.TemplateMessages;

public class SmallReceptionProjectContentWizard extends NewProjectTemplateWizard {
	/**
	 * Constructor for HelloWorldNewWizard.
	 */
	public SmallReceptionProjectContentWizard() {
		super();
	}

	public void init(BaseProjectWizardFields data) {
		super.init(data);
		setWindowTitle(TemplateMessages.SmallReceptionNewWizard_wtitle);
	}

	/*
	 * @see NewExtensionTemplateWizard#createTemplateSections()
	 */
	public ITemplateSection[] createTemplateSections() {
		return new ITemplateSection[] {new SmallReceptionTemplateSection()};
	}
}
