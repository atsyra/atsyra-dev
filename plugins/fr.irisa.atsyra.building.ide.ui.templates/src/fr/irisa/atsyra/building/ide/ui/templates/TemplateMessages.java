/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.ide.ui.templates;

import org.eclipse.osgi.util.NLS;

public class TemplateMessages extends NLS {
	private static final String BUNDLE_NAME = "fr.irisa.atsyra.building.ide.ui.templates.templatemessages"; //$NON-NLS-1$

	static {
		// load message values from bundle file
		NLS.initializeMessages(BUNDLE_NAME, TemplateMessages.class);
	}
		
	// MiniSite constants
	public static String MiniSiteNewWizard_wtitle;
	public static String MiniSiteTemplate_packageName;
	public static String MiniSiteTemplate_buildingName;
	public static String MiniSiteTemplate_zone1Name;
	public static String MiniSiteTemplate_zone2Name;
	public static String MiniSiteTemplate_accessName;
	public static String MiniSiteTemplate_title;
	public static String MiniSiteTemplate_desc;

	// SmallReception constants
	public static String SmallReceptionNewWizard_wtitle;
	public static String SmallReceptionTemplate_packageName;
	public static String SmallReceptionTemplate_buildingName;
	public static String SmallReceptionTemplate_zone1Name;
	public static String SmallReceptionTemplate_zone2Name;
	public static String SmallReceptionTemplate_accessName;
	public static String SmallReceptionTemplate_title;
	public static String SmallReceptionTemplate_desc;	
	

}