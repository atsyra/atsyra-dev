package fr.irisa.atsyra.absreport.generator

import org.eclipse.xtext.generator.IOutputConfigurationProvider
import org.eclipse.xtext.generator.OutputConfiguration
import org.eclipse.xtext.generator.IFileSystemAccess
import java.util.Set

class ABSReportOutputConfigurationProvider implements IOutputConfigurationProvider {
	public static final String DOC_GEN_OUTPUT = "doc-gen";
	
	override getOutputConfigurations() {
		val defaultOutput = new OutputConfiguration(IFileSystemAccess.DEFAULT_OUTPUT);
	    defaultOutput.setDescription("Output Folder");
	    defaultOutput.setOutputDirectory("./gen");
	    defaultOutput.setOverrideExistingResources(true);
	    defaultOutput.setCreateOutputDirectory(true);
	    defaultOutput.setCleanUpDerivedResources(true);
	    defaultOutput.setSetDerivedProperty(true);
	    
	   val OutputConfiguration docgenOutput = new OutputConfiguration(DOC_GEN_OUTPUT);
	    docgenOutput.setDescription("Doc Output Folder");
	    docgenOutput.setOutputDirectory("./doc-gen");
	    docgenOutput.setOverrideExistingResources(true);
	    docgenOutput.setCreateOutputDirectory(true);
	    docgenOutput.setCleanUpDerivedResources(true);
	    docgenOutput.setSetDerivedProperty(true);
	    
	    
	    Set.of(defaultOutput, docgenOutput)
	}
	
}