/*******************************************************************************
 * Copyright (c) 2014, 2022 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absreport.helpers;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFolder;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtend.lib.annotations.Data;
import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.resource.SaveOptions.Builder;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Pair;

import fr.irisa.atsyra.absreport.aBSReport.ABSGoalWitness;
import fr.irisa.atsyra.absreport.aBSReport.ABSReportFactory;
import fr.irisa.atsyra.absreport.aBSReport.ABSReportModel;
import fr.irisa.atsyra.absreport.aBSReport.ABSReportPackage;
import fr.irisa.atsyra.absreport.aBSReport.AssetState;
import fr.irisa.atsyra.absreport.aBSReport.AttributeState;
import fr.irisa.atsyra.absreport.aBSReport.GoalReport;
import fr.irisa.atsyra.absreport.aBSReport.ReferenceState;
import fr.irisa.atsyra.absreport.aBSReport.ReportMetadata;
import fr.irisa.atsyra.absreport.aBSReport.Step;
import fr.irisa.atsyra.absreport.aBSReport.StepState;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemFactory;
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage;
import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant;
import fr.irisa.atsyra.absystem.model.absystem.EnumConstant;
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral;
import fr.irisa.atsyra.absystem.model.absystem.Goal;
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction;
import fr.irisa.atsyra.absystem.model.absystem.Import;
import fr.irisa.atsyra.absystem.model.absystem.IntConstant;
import fr.irisa.atsyra.absystem.model.absystem.Scenario;
import fr.irisa.atsyra.absystem.model.absystem.State;
import fr.irisa.atsyra.absystem.model.absystem.StringConstant;
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant;
import fr.irisa.atsyra.absystem.model.absystem.Version;
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetArgument;
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence;

public class AbsreportHelper {
	@Data
	public static class ReportEntry {
		public ReportEntry(Goal goal2, Scenario scenario2, Duration duration2, String command2, Instant timestamp2) {
			goal = goal2;
			scenario = scenario2;
			duration = duration2;
			command = command2;
			timestamp = timestamp2;
		}

		public ReportEntry(Goal goal2, Scenario scenario2, Duration duration2, String command2, Instant timestamp2,
				String invariant2, List<Scenario> previous2) {
			goal = goal2;
			scenario = scenario2;
			duration = duration2;
			command = command2;
			timestamp = timestamp2;
			invariant = invariant2;
			previous = previous2;
		}

		public Goal goal;
		public Scenario scenario;
		public Duration duration;
		public String command;
		public Instant timestamp;
		public String invariant;
		public List<Scenario> previous;
	}

	Resource reportRes;
	ABSReportModel absreport;

	public AbsreportHelper(ResourceSet resourceSet, IFolder genFolder) {
		ABSReportPackage.eINSTANCE.eClass();
		URI reporturi = URI.createPlatformResourceURI(genFolder.getFullPath() + "/report.absreport", true);
		reportRes = resourceSet.getResource(reporturi, true);
		if (reportRes.getContents().isEmpty()) {
			absreport = ABSReportFactory.eINSTANCE.createABSReportModel();
			absreport.getReports();
			reportRes.getContents().add(absreport);
		} else {
			absreport = (ABSReportModel) reportRes.getContents().get(0);
		}
	}

	public void addScenario(ReportEntry entry) {
		addImportIfNecessary(entry.goal);
		final GoalReport report = getGoalReport(entry.goal);
		final ABSGoalWitness witness = getWitness(report, entry.scenario);
		final ReportMetadata metadata = witness.getMetadata();
		metadata.setDuration((int) entry.duration.toMillis());
		metadata.setRawCommand(entry.command);
		metadata.setTimestamp(timestampFromInstant(entry.timestamp));
		metadata.setNbSteps(witness.getSteps().size());
		metadata.setObsolete(false);
		if (entry.invariant != null) {
			metadata.setInvariant(entry.invariant);
		}
		metadata.getPrevious().clear();
		if (entry.previous != null) {
			entry.previous.forEach((s) -> {
				Optional<ABSGoalWitness> witnessRef = getWitnessRef(report, s);
				if (!witnessRef.isEmpty())
					metadata.getPrevious().add(witnessRef.get());
			});
		}
		setStates(witness.getInitialState(), entry.scenario.getInitialStates());
		setStates(witness.getFinalState(), entry.scenario.getFinalStates());
	}

	private void addImportIfNecessary(Goal goal) {
		if(absreport.getImports().stream().noneMatch(imp -> Objects.equals(imp.getImportURI(), goal.eResource().getURI().toString()))) {
			Import imp = AbsystemFactory.eINSTANCE.createImport();
			imp.setImportURI(goal.eResource().getURI().toString());
			absreport.getImports().add(imp);
		}
	}

	private void setStates(EList<StepState> stepStates, List<State> initialStates) {
		stepStates.clear();
		List<StepState> mapped = ListExtensions.map(initialStates, state -> {
			StepState stepstate = ABSReportFactory.eINSTANCE.createStepState();
			state.getValuation().forEach((asset, assetinstance) -> {
				AssetState assetstate = ABSReportFactory.eINSTANCE.createAssetState();
				assetstate.setAsset(asset);
				assetinstance.getRefvalues().forEach((ref, orefasset) -> {
					ReferenceState referencestate = ABSReportFactory.eINSTANCE.createReferenceState();
					referencestate.setReference(ref);
					assetstate.getFeatureStates().add(referencestate);
					if (orefasset.isPresent()) {
						Asset refasset = orefasset.get();
						referencestate.getValues().add(refasset);
					}
				});
				assetinstance.getBoolvalues().forEach((attribute, obool) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					assetstate.getFeatureStates().add(attributestate);
					if (obool.isPresent()) {
						boolean bool = obool.get();
						BooleanConstant bcst = AbsystemFactory.eINSTANCE.createBooleanConstant();
						bcst.setValue(bool ? "true" : "false");
						attributestate.getValues().add(bcst);
					} else {
						UndefinedConstant undefined = AbsystemFactory.eINSTANCE.createUndefinedConstant();
						attributestate.getValues().add(undefined);
					}
				});
				assetinstance.getIntvalues().forEach((attribute, ointeger) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					assetstate.getFeatureStates().add(attributestate);
					if (ointeger.isPresent()) {
						int integer = ointeger.get();
						IntConstant icst = AbsystemFactory.eINSTANCE.createIntConstant();
						icst.setValue(integer);
						attributestate.getValues().add(icst);
					} else {
						UndefinedConstant undefined = AbsystemFactory.eINSTANCE.createUndefinedConstant();
						attributestate.getValues().add(undefined);
					}
				});
				assetinstance.getStringvalues().forEach((attribute, ostr) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					assetstate.getFeatureStates().add(attributestate);
					if (ostr.isPresent()) {
						String str = ostr.get();
						StringConstant scst = AbsystemFactory.eINSTANCE.createStringConstant();
						scst.setValue(str);
						attributestate.getValues().add(scst);
					} else {
						UndefinedConstant undefined = AbsystemFactory.eINSTANCE.createUndefinedConstant();
						attributestate.getValues().add(undefined);
					}
				});
				assetinstance.getEnumvalues().forEach((attribute, oen) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					assetstate.getFeatureStates().add(attributestate);
					if (oen.isPresent()) {
						EnumLiteral en = oen.get();
						EnumConstant encst = AbsystemFactory.eINSTANCE.createEnumConstant();
						encst.setValue(en);
						attributestate.getValues().add(encst);
					} else {
						UndefinedConstant undefined = AbsystemFactory.eINSTANCE.createUndefinedConstant();
						attributestate.getValues().add(undefined);
					}
				});
				assetinstance.getVersionvalues().forEach((attribute, over) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					assetstate.getFeatureStates().add(attributestate);
					if (over.isPresent()) {
						Version ver = over.get();
						VersionConstant vercst = AbsystemFactory.eINSTANCE.createVersionConstant();
						vercst.setValue(ver);
						attributestate.getValues().add(vercst);
					} else {
						UndefinedConstant undefined = AbsystemFactory.eINSTANCE.createUndefinedConstant();
						attributestate.getValues().add(undefined);
					}
				});
				assetinstance.getRefcollections().asMap().forEach((ref, refassetcollection) -> {
					ReferenceState referencestate = ABSReportFactory.eINSTANCE.createReferenceState();
					referencestate.setReference(ref);
					referencestate.getValues().addAll(refassetcollection);
					assetstate.getFeatureStates().add(referencestate);
				});
				assetinstance.getBoolcollections().asMap().forEach((attribute, boolcollection) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					List<BooleanConstant> values = boolcollection.stream().map(bool -> {
						BooleanConstant bcst = AbsystemFactory.eINSTANCE.createBooleanConstant();
						bcst.setValue(bool ? "true" : "false");
						return bcst;
					}).collect(Collectors.toList());
					attributestate.getValues().addAll(values);
					assetstate.getFeatureStates().add(attributestate);
				});
				assetinstance.getIntcollections().asMap().forEach((attribute, integercollection) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					List<IntConstant> values = integercollection.stream().map(integer -> {
						IntConstant icst = AbsystemFactory.eINSTANCE.createIntConstant();
						icst.setValue(integer);
						return icst;
					}).collect(Collectors.toList());
					attributestate.getValues().addAll(values);
					assetstate.getFeatureStates().add(attributestate);
				});
				assetinstance.getStringcollections().asMap().forEach((attribute, strcollection) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					List<StringConstant> values = strcollection.stream().map(str -> {
						StringConstant scst = AbsystemFactory.eINSTANCE.createStringConstant();
						scst.setValue(str);
						return scst;
					}).collect(Collectors.toList());
					attributestate.getValues().addAll(values);
					assetstate.getFeatureStates().add(attributestate);
				});
				assetinstance.getEnumcollections().asMap().forEach((attribute, encollection) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					List<EnumConstant> values = encollection.stream().map(en -> {
						EnumConstant encst = AbsystemFactory.eINSTANCE.createEnumConstant();
						encst.setValue(en);
						return encst;
					}).collect(Collectors.toList());
					attributestate.getValues().addAll(values);
					assetstate.getFeatureStates().add(attributestate);
				});
				assetinstance.getVersioncollections().asMap().forEach((attribute, vercollection) -> {
					AttributeState attributestate = ABSReportFactory.eINSTANCE.createAttributeState();
					attributestate.setAttribute(attribute);
					List<VersionConstant> values = vercollection.stream().map(ver -> {
						VersionConstant vercst = AbsystemFactory.eINSTANCE.createVersionConstant();
						vercst.setValue(ver);
						return vercst;
					}).collect(Collectors.toList());
					attributestate.getValues().addAll(values);
					assetstate.getFeatureStates().add(attributestate);
				});
				stepstate.getAssetState().add(assetstate);
			});
			return stepstate;
		});
		stepStates.addAll(mapped);
	}

	private Optional<ABSGoalWitness> getWitnessRef(GoalReport report, Scenario scenario) {
		return report.getWitnesses().stream()
				.filter(witness -> scenario.sameAs(Scenario.fromSteps(witness.getSteps().stream().map(
						step -> new Pair<GuardedAction, List<Asset>>(step.getGuardedAction(), step.getParameters()))
						.collect(Collectors.toList()))))
				.findAny();
	}

	private ABSGoalWitness getWitness(GoalReport report, Scenario scenario) {
		final Optional<ABSGoalWitness> alreadyPresentWitness = getWitnessRef(report, scenario);
		if (alreadyPresentWitness.isEmpty()) {
			final ABSGoalWitness added = ABSReportFactory.eINSTANCE.createABSGoalWitness();
			final ReportMetadata metadata = ABSReportFactory.eINSTANCE.createReportMetadata();
			added.setMetadata(metadata);
			IterableExtensions.map(scenario.getActions(), this::fromGuardOcucrence)
					.forEach(step -> added.getSteps().add(step));
			report.getWitnesses().add(added);
			return added;
		} else {
			return alreadyPresentWitness.get();
		}
	}

	private String timestampFromInstant(Instant instant) {
		return instant.toString();
	}

	private Step fromGuardOcucrence(GuardOccurence go) {
		final Step res = ABSReportFactory.eINSTANCE.createStep();
		res.setGuardedAction((GuardedAction) go.getGuard());
		IterableExtensions.map(go.getGuardOccurenceArguments(), goa -> ((AssetArgument) goa).getAsset())
				.forEach(asset -> res.getParameters().add(asset));
		return res;
	}

	private GoalReport getGoalReport(Goal goal) {
		final Optional<GoalReport> res = absreport.getReports().stream()
				.filter(g -> goal.getName().equals(g.getGoal().getName())).findFirst();
		if (res.isEmpty()) {
			final GoalReport added = ABSReportFactory.eINSTANCE.createGoalReport();
			added.setGoal(goal);
			absreport.getReports().add(added);
			return added;
		} else {
			return res.get();
		}
	}

	public void serialize() throws IOException {
		if (!absreport.getReports().isEmpty()) {
			Builder saveOptionBuilder = SaveOptions.newBuilder();
			saveOptionBuilder.format();
			reportRes.save(saveOptionBuilder.getOptions().toOptionsMap());
		}
	}
}
