package fr.irisa.atsyra.absreport.naming

import org.eclipse.xtext.naming.IQualifiedNameConverter

class ABSReportQualifiedNameConverter extends IQualifiedNameConverter.DefaultImpl {
	
	override String getDelimiter() {
		return "::"
	}
}
