package fr.irisa.atsyra.absreport.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.atsyra.absreport.services.ABSReportGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalABSReportParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_INT", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Goal'", "'{'", "'}'", "'witness'", "'steps'", "'initialStates'", "'finalStates'", "'metadata'", "'timestamp'", "'obsolete'", "'duration'", "'rawCommand'", "'nbSteps'", "'invariant'", "'previousWitnesses'", "','", "'('", "')'", "'state'", "'attribute'", "'='", "'['", "']'", "'reference'", "'undefined'", "'DefinitionGroup'", "'tags'", "'annotations'", "'AssetGroup'", "'annotation'", "'abstract'", "'AssetType'", "'extends'", "'level'", "'description'", "'AssetTypeAspect'", "'PrimitiveDataType'", "'StaticMethod'", "'Tag'", "'container'", "':'", "'opposite'", "'default'", "'EnumDataType'", "'Asset'", "'link'", "'to'", "'as'", "'#'", "':='", "'true'", "'false'", "'dynamic'", "'static'", "'contract'", "'severity'", "'guard'", "'GuardedAction'", "'action'", "';'", "'pre'", "'post'", "'=>'", "'||'", "'&&'", "'!'", "'=='", "'!='", "'<'", "'<='", "'>'", "'>='", "'.'", "'->'", "'-'", "'+'", "'with'", "'from'", "'import'", "'::'", "'*'", "'Locale'", "'locale'", "'EnumLiteral'", "'Guard'", "'Requirement'", "'title'", "'contracts'", "'[1]'", "'[0..1]'", "'[*]'", "'[1..*]'", "'ERROR'", "'WARNING'", "'assign'", "'add'", "'addAll'", "'clear'", "'remove'", "'removeAll'", "'forAll'", "'plaintext'", "'html'", "'markdown'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=6;
    public static final int RULE_INT=5;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__99=99;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__114=114;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__111=111;
    public static final int T__81=81;
    public static final int T__110=110;
    public static final int T__82=82;
    public static final int T__113=113;
    public static final int T__83=83;
    public static final int T__112=112;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int T__87=87;
    public static final int T__105=105;

    // delegates
    // delegators


        public InternalABSReportParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalABSReportParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalABSReportParser.tokenNames; }
    public String getGrammarFileName() { return "InternalABSReport.g"; }



     	private ABSReportGrammarAccess grammarAccess;

        public InternalABSReportParser(TokenStream input, ABSReportGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "ABSReportModel";
       	}

       	@Override
       	protected ABSReportGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleABSReportModel"
    // InternalABSReport.g:65:1: entryRuleABSReportModel returns [EObject current=null] : iv_ruleABSReportModel= ruleABSReportModel EOF ;
    public final EObject entryRuleABSReportModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleABSReportModel = null;


        try {
            // InternalABSReport.g:65:55: (iv_ruleABSReportModel= ruleABSReportModel EOF )
            // InternalABSReport.g:66:2: iv_ruleABSReportModel= ruleABSReportModel EOF
            {
             newCompositeNode(grammarAccess.getABSReportModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleABSReportModel=ruleABSReportModel();

            state._fsp--;

             current =iv_ruleABSReportModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleABSReportModel"


    // $ANTLR start "ruleABSReportModel"
    // InternalABSReport.g:72:1: ruleABSReportModel returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_reports_1_0= ruleGoalReport ) )* ) ;
    public final EObject ruleABSReportModel() throws RecognitionException {
        EObject current = null;

        EObject lv_imports_0_0 = null;

        EObject lv_reports_1_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:78:2: ( ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_reports_1_0= ruleGoalReport ) )* ) )
            // InternalABSReport.g:79:2: ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_reports_1_0= ruleGoalReport ) )* )
            {
            // InternalABSReport.g:79:2: ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_reports_1_0= ruleGoalReport ) )* )
            // InternalABSReport.g:80:3: ( (lv_imports_0_0= ruleImport ) )* ( (lv_reports_1_0= ruleGoalReport ) )*
            {
            // InternalABSReport.g:80:3: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==87||LA1_0==89) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalABSReport.g:81:4: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalABSReport.g:81:4: (lv_imports_0_0= ruleImport )
            	    // InternalABSReport.g:82:5: lv_imports_0_0= ruleImport
            	    {

            	    					newCompositeNode(grammarAccess.getABSReportModelAccess().getImportsImportParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getABSReportModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_0_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Import");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalABSReport.g:99:3: ( (lv_reports_1_0= ruleGoalReport ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==11) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalABSReport.g:100:4: (lv_reports_1_0= ruleGoalReport )
            	    {
            	    // InternalABSReport.g:100:4: (lv_reports_1_0= ruleGoalReport )
            	    // InternalABSReport.g:101:5: lv_reports_1_0= ruleGoalReport
            	    {

            	    					newCompositeNode(grammarAccess.getABSReportModelAccess().getReportsGoalReportParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_reports_1_0=ruleGoalReport();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getABSReportModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"reports",
            	    						lv_reports_1_0,
            	    						"fr.irisa.atsyra.absreport.ABSReport.GoalReport");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleABSReportModel"


    // $ANTLR start "entryRuleGoalReport"
    // InternalABSReport.g:122:1: entryRuleGoalReport returns [EObject current=null] : iv_ruleGoalReport= ruleGoalReport EOF ;
    public final EObject entryRuleGoalReport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGoalReport = null;


        try {
            // InternalABSReport.g:122:51: (iv_ruleGoalReport= ruleGoalReport EOF )
            // InternalABSReport.g:123:2: iv_ruleGoalReport= ruleGoalReport EOF
            {
             newCompositeNode(grammarAccess.getGoalReportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGoalReport=ruleGoalReport();

            state._fsp--;

             current =iv_ruleGoalReport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGoalReport"


    // $ANTLR start "ruleGoalReport"
    // InternalABSReport.g:129:1: ruleGoalReport returns [EObject current=null] : (otherlv_0= 'Goal' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( (lv_witnesses_3_0= ruleABSGoalWitness ) )* otherlv_4= '}' ) ;
    public final EObject ruleGoalReport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_witnesses_3_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:135:2: ( (otherlv_0= 'Goal' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( (lv_witnesses_3_0= ruleABSGoalWitness ) )* otherlv_4= '}' ) )
            // InternalABSReport.g:136:2: (otherlv_0= 'Goal' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( (lv_witnesses_3_0= ruleABSGoalWitness ) )* otherlv_4= '}' )
            {
            // InternalABSReport.g:136:2: (otherlv_0= 'Goal' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( (lv_witnesses_3_0= ruleABSGoalWitness ) )* otherlv_4= '}' )
            // InternalABSReport.g:137:3: otherlv_0= 'Goal' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( (lv_witnesses_3_0= ruleABSGoalWitness ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getGoalReportAccess().getGoalKeyword_0());
            		
            // InternalABSReport.g:141:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:142:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:142:4: ( ruleQualifiedName )
            // InternalABSReport.g:143:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGoalReportRule());
            					}
            				

            					newCompositeNode(grammarAccess.getGoalReportAccess().getGoalGoalCrossReference_1_0());
            				
            pushFollow(FOLLOW_6);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getGoalReportAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalABSReport.g:161:3: ( (lv_witnesses_3_0= ruleABSGoalWitness ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==14) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalABSReport.g:162:4: (lv_witnesses_3_0= ruleABSGoalWitness )
            	    {
            	    // InternalABSReport.g:162:4: (lv_witnesses_3_0= ruleABSGoalWitness )
            	    // InternalABSReport.g:163:5: lv_witnesses_3_0= ruleABSGoalWitness
            	    {

            	    					newCompositeNode(grammarAccess.getGoalReportAccess().getWitnessesABSGoalWitnessParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_witnesses_3_0=ruleABSGoalWitness();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getGoalReportRule());
            	    					}
            	    					add(
            	    						current,
            	    						"witnesses",
            	    						lv_witnesses_3_0,
            	    						"fr.irisa.atsyra.absreport.ABSReport.ABSGoalWitness");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getGoalReportAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGoalReport"


    // $ANTLR start "entryRuleABSGoalWitness"
    // InternalABSReport.g:188:1: entryRuleABSGoalWitness returns [EObject current=null] : iv_ruleABSGoalWitness= ruleABSGoalWitness EOF ;
    public final EObject entryRuleABSGoalWitness() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleABSGoalWitness = null;


        try {
            // InternalABSReport.g:188:55: (iv_ruleABSGoalWitness= ruleABSGoalWitness EOF )
            // InternalABSReport.g:189:2: iv_ruleABSGoalWitness= ruleABSGoalWitness EOF
            {
             newCompositeNode(grammarAccess.getABSGoalWitnessRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleABSGoalWitness=ruleABSGoalWitness();

            state._fsp--;

             current =iv_ruleABSGoalWitness; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleABSGoalWitness"


    // $ANTLR start "ruleABSGoalWitness"
    // InternalABSReport.g:195:1: ruleABSGoalWitness returns [EObject current=null] : ( () otherlv_1= 'witness' otherlv_2= '{' ( (lv_metadata_3_0= ruleReportMetadata ) )? (otherlv_4= 'steps' otherlv_5= '{' ( (lv_steps_6_0= ruleStep ) )* otherlv_7= '}' )? (otherlv_8= 'initialStates' otherlv_9= '{' ( (lv_initialState_10_0= ruleStepState ) )* otherlv_11= '}' )? (otherlv_12= 'finalStates' otherlv_13= '{' ( (lv_finalState_14_0= ruleStepState ) )* otherlv_15= '}' )? otherlv_16= '}' ) ;
    public final EObject ruleABSGoalWitness() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        EObject lv_metadata_3_0 = null;

        EObject lv_steps_6_0 = null;

        EObject lv_initialState_10_0 = null;

        EObject lv_finalState_14_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:201:2: ( ( () otherlv_1= 'witness' otherlv_2= '{' ( (lv_metadata_3_0= ruleReportMetadata ) )? (otherlv_4= 'steps' otherlv_5= '{' ( (lv_steps_6_0= ruleStep ) )* otherlv_7= '}' )? (otherlv_8= 'initialStates' otherlv_9= '{' ( (lv_initialState_10_0= ruleStepState ) )* otherlv_11= '}' )? (otherlv_12= 'finalStates' otherlv_13= '{' ( (lv_finalState_14_0= ruleStepState ) )* otherlv_15= '}' )? otherlv_16= '}' ) )
            // InternalABSReport.g:202:2: ( () otherlv_1= 'witness' otherlv_2= '{' ( (lv_metadata_3_0= ruleReportMetadata ) )? (otherlv_4= 'steps' otherlv_5= '{' ( (lv_steps_6_0= ruleStep ) )* otherlv_7= '}' )? (otherlv_8= 'initialStates' otherlv_9= '{' ( (lv_initialState_10_0= ruleStepState ) )* otherlv_11= '}' )? (otherlv_12= 'finalStates' otherlv_13= '{' ( (lv_finalState_14_0= ruleStepState ) )* otherlv_15= '}' )? otherlv_16= '}' )
            {
            // InternalABSReport.g:202:2: ( () otherlv_1= 'witness' otherlv_2= '{' ( (lv_metadata_3_0= ruleReportMetadata ) )? (otherlv_4= 'steps' otherlv_5= '{' ( (lv_steps_6_0= ruleStep ) )* otherlv_7= '}' )? (otherlv_8= 'initialStates' otherlv_9= '{' ( (lv_initialState_10_0= ruleStepState ) )* otherlv_11= '}' )? (otherlv_12= 'finalStates' otherlv_13= '{' ( (lv_finalState_14_0= ruleStepState ) )* otherlv_15= '}' )? otherlv_16= '}' )
            // InternalABSReport.g:203:3: () otherlv_1= 'witness' otherlv_2= '{' ( (lv_metadata_3_0= ruleReportMetadata ) )? (otherlv_4= 'steps' otherlv_5= '{' ( (lv_steps_6_0= ruleStep ) )* otherlv_7= '}' )? (otherlv_8= 'initialStates' otherlv_9= '{' ( (lv_initialState_10_0= ruleStepState ) )* otherlv_11= '}' )? (otherlv_12= 'finalStates' otherlv_13= '{' ( (lv_finalState_14_0= ruleStepState ) )* otherlv_15= '}' )? otherlv_16= '}'
            {
            // InternalABSReport.g:203:3: ()
            // InternalABSReport.g:204:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getABSGoalWitnessAccess().getABSGoalWitnessAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,14,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getABSGoalWitnessAccess().getWitnessKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_8); 

            			newLeafNode(otherlv_2, grammarAccess.getABSGoalWitnessAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalABSReport.g:218:3: ( (lv_metadata_3_0= ruleReportMetadata ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==18) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalABSReport.g:219:4: (lv_metadata_3_0= ruleReportMetadata )
                    {
                    // InternalABSReport.g:219:4: (lv_metadata_3_0= ruleReportMetadata )
                    // InternalABSReport.g:220:5: lv_metadata_3_0= ruleReportMetadata
                    {

                    					newCompositeNode(grammarAccess.getABSGoalWitnessAccess().getMetadataReportMetadataParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_metadata_3_0=ruleReportMetadata();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getABSGoalWitnessRule());
                    					}
                    					set(
                    						current,
                    						"metadata",
                    						lv_metadata_3_0,
                    						"fr.irisa.atsyra.absreport.ABSReport.ReportMetadata");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalABSReport.g:237:3: (otherlv_4= 'steps' otherlv_5= '{' ( (lv_steps_6_0= ruleStep ) )* otherlv_7= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==15) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalABSReport.g:238:4: otherlv_4= 'steps' otherlv_5= '{' ( (lv_steps_6_0= ruleStep ) )* otherlv_7= '}'
                    {
                    otherlv_4=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_4, grammarAccess.getABSGoalWitnessAccess().getStepsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,12,FOLLOW_10); 

                    				newLeafNode(otherlv_5, grammarAccess.getABSGoalWitnessAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalABSReport.g:246:4: ( (lv_steps_6_0= ruleStep ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==RULE_ID) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalABSReport.g:247:5: (lv_steps_6_0= ruleStep )
                    	    {
                    	    // InternalABSReport.g:247:5: (lv_steps_6_0= ruleStep )
                    	    // InternalABSReport.g:248:6: lv_steps_6_0= ruleStep
                    	    {

                    	    						newCompositeNode(grammarAccess.getABSGoalWitnessAccess().getStepsStepParserRuleCall_4_2_0());
                    	    					
                    	    pushFollow(FOLLOW_10);
                    	    lv_steps_6_0=ruleStep();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getABSGoalWitnessRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"steps",
                    	    							lv_steps_6_0,
                    	    							"fr.irisa.atsyra.absreport.ABSReport.Step");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,13,FOLLOW_11); 

                    				newLeafNode(otherlv_7, grammarAccess.getABSGoalWitnessAccess().getRightCurlyBracketKeyword_4_3());
                    			

                    }
                    break;

            }

            // InternalABSReport.g:270:3: (otherlv_8= 'initialStates' otherlv_9= '{' ( (lv_initialState_10_0= ruleStepState ) )* otherlv_11= '}' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==16) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalABSReport.g:271:4: otherlv_8= 'initialStates' otherlv_9= '{' ( (lv_initialState_10_0= ruleStepState ) )* otherlv_11= '}'
                    {
                    otherlv_8=(Token)match(input,16,FOLLOW_6); 

                    				newLeafNode(otherlv_8, grammarAccess.getABSGoalWitnessAccess().getInitialStatesKeyword_5_0());
                    			
                    otherlv_9=(Token)match(input,12,FOLLOW_12); 

                    				newLeafNode(otherlv_9, grammarAccess.getABSGoalWitnessAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalABSReport.g:279:4: ( (lv_initialState_10_0= ruleStepState ) )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==29) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // InternalABSReport.g:280:5: (lv_initialState_10_0= ruleStepState )
                    	    {
                    	    // InternalABSReport.g:280:5: (lv_initialState_10_0= ruleStepState )
                    	    // InternalABSReport.g:281:6: lv_initialState_10_0= ruleStepState
                    	    {

                    	    						newCompositeNode(grammarAccess.getABSGoalWitnessAccess().getInitialStateStepStateParserRuleCall_5_2_0());
                    	    					
                    	    pushFollow(FOLLOW_12);
                    	    lv_initialState_10_0=ruleStepState();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getABSGoalWitnessRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"initialState",
                    	    							lv_initialState_10_0,
                    	    							"fr.irisa.atsyra.absreport.ABSReport.StepState");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,13,FOLLOW_13); 

                    				newLeafNode(otherlv_11, grammarAccess.getABSGoalWitnessAccess().getRightCurlyBracketKeyword_5_3());
                    			

                    }
                    break;

            }

            // InternalABSReport.g:303:3: (otherlv_12= 'finalStates' otherlv_13= '{' ( (lv_finalState_14_0= ruleStepState ) )* otherlv_15= '}' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==17) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalABSReport.g:304:4: otherlv_12= 'finalStates' otherlv_13= '{' ( (lv_finalState_14_0= ruleStepState ) )* otherlv_15= '}'
                    {
                    otherlv_12=(Token)match(input,17,FOLLOW_6); 

                    				newLeafNode(otherlv_12, grammarAccess.getABSGoalWitnessAccess().getFinalStatesKeyword_6_0());
                    			
                    otherlv_13=(Token)match(input,12,FOLLOW_12); 

                    				newLeafNode(otherlv_13, grammarAccess.getABSGoalWitnessAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalABSReport.g:312:4: ( (lv_finalState_14_0= ruleStepState ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==29) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalABSReport.g:313:5: (lv_finalState_14_0= ruleStepState )
                    	    {
                    	    // InternalABSReport.g:313:5: (lv_finalState_14_0= ruleStepState )
                    	    // InternalABSReport.g:314:6: lv_finalState_14_0= ruleStepState
                    	    {

                    	    						newCompositeNode(grammarAccess.getABSGoalWitnessAccess().getFinalStateStepStateParserRuleCall_6_2_0());
                    	    					
                    	    pushFollow(FOLLOW_12);
                    	    lv_finalState_14_0=ruleStepState();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getABSGoalWitnessRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"finalState",
                    	    							lv_finalState_14_0,
                    	    							"fr.irisa.atsyra.absreport.ABSReport.StepState");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,13,FOLLOW_14); 

                    				newLeafNode(otherlv_15, grammarAccess.getABSGoalWitnessAccess().getRightCurlyBracketKeyword_6_3());
                    			

                    }
                    break;

            }

            otherlv_16=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_16, grammarAccess.getABSGoalWitnessAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleABSGoalWitness"


    // $ANTLR start "entryRuleReportMetadata"
    // InternalABSReport.g:344:1: entryRuleReportMetadata returns [EObject current=null] : iv_ruleReportMetadata= ruleReportMetadata EOF ;
    public final EObject entryRuleReportMetadata() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReportMetadata = null;


        try {
            // InternalABSReport.g:344:55: (iv_ruleReportMetadata= ruleReportMetadata EOF )
            // InternalABSReport.g:345:2: iv_ruleReportMetadata= ruleReportMetadata EOF
            {
             newCompositeNode(grammarAccess.getReportMetadataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReportMetadata=ruleReportMetadata();

            state._fsp--;

             current =iv_ruleReportMetadata; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReportMetadata"


    // $ANTLR start "ruleReportMetadata"
    // InternalABSReport.g:351:1: ruleReportMetadata returns [EObject current=null] : ( () otherlv_1= 'metadata' otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* ) ) ) otherlv_19= '}' ) ;
    public final EObject ruleReportMetadata() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_timestamp_5_0=null;
        Token lv_obsolete_6_0=null;
        Token otherlv_7=null;
        Token lv_duration_8_0=null;
        Token otherlv_9=null;
        Token lv_rawCommand_10_0=null;
        Token otherlv_11=null;
        Token lv_nbSteps_12_0=null;
        Token otherlv_13=null;
        Token lv_invariant_14_0=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;


        	enterRule();

        try {
            // InternalABSReport.g:357:2: ( ( () otherlv_1= 'metadata' otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* ) ) ) otherlv_19= '}' ) )
            // InternalABSReport.g:358:2: ( () otherlv_1= 'metadata' otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* ) ) ) otherlv_19= '}' )
            {
            // InternalABSReport.g:358:2: ( () otherlv_1= 'metadata' otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* ) ) ) otherlv_19= '}' )
            // InternalABSReport.g:359:3: () otherlv_1= 'metadata' otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* ) ) ) otherlv_19= '}'
            {
            // InternalABSReport.g:359:3: ()
            // InternalABSReport.g:360:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getReportMetadataAccess().getReportMetadataAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,18,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getReportMetadataAccess().getMetadataKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getReportMetadataAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalABSReport.g:374:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* ) ) )
            // InternalABSReport.g:375:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* ) )
            {
            // InternalABSReport.g:375:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* ) )
            // InternalABSReport.g:376:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3());
            				
            // InternalABSReport.g:379:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )* )
            // InternalABSReport.g:380:6: ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )*
            {
            // InternalABSReport.g:380:6: ( ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) ) )*
            loop12:
            do {
                int alt12=8;
                int LA12_0 = input.LA(1);

                if ( LA12_0 == 19 && getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 0) ) {
                    alt12=1;
                }
                else if ( LA12_0 == 20 && getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 1) ) {
                    alt12=2;
                }
                else if ( LA12_0 == 21 && getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 2) ) {
                    alt12=3;
                }
                else if ( LA12_0 == 22 && getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 3) ) {
                    alt12=4;
                }
                else if ( LA12_0 == 23 && getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 4) ) {
                    alt12=5;
                }
                else if ( LA12_0 == 24 && getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 5) ) {
                    alt12=6;
                }
                else if ( LA12_0 == 25 && getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 6) ) {
                    alt12=7;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalABSReport.g:381:4: ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:381:4: ({...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) ) )
            	    // InternalABSReport.g:382:5: {...}? => ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 0)");
            	    }
            	    // InternalABSReport.g:382:111: ( ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) ) )
            	    // InternalABSReport.g:383:6: ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 0);
            	    					
            	    // InternalABSReport.g:386:9: ({...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) ) )
            	    // InternalABSReport.g:386:10: {...}? => (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "true");
            	    }
            	    // InternalABSReport.g:386:19: (otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) ) )
            	    // InternalABSReport.g:386:20: otherlv_4= 'timestamp' ( (lv_timestamp_5_0= RULE_STRING ) )
            	    {
            	    otherlv_4=(Token)match(input,19,FOLLOW_16); 

            	    									newLeafNode(otherlv_4, grammarAccess.getReportMetadataAccess().getTimestampKeyword_3_0_0());
            	    								
            	    // InternalABSReport.g:390:9: ( (lv_timestamp_5_0= RULE_STRING ) )
            	    // InternalABSReport.g:391:10: (lv_timestamp_5_0= RULE_STRING )
            	    {
            	    // InternalABSReport.g:391:10: (lv_timestamp_5_0= RULE_STRING )
            	    // InternalABSReport.g:392:11: lv_timestamp_5_0= RULE_STRING
            	    {
            	    lv_timestamp_5_0=(Token)match(input,RULE_STRING,FOLLOW_15); 

            	    											newLeafNode(lv_timestamp_5_0, grammarAccess.getReportMetadataAccess().getTimestampSTRINGTerminalRuleCall_3_0_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getReportMetadataRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"timestamp",
            	    												lv_timestamp_5_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalABSReport.g:414:4: ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) )
            	    {
            	    // InternalABSReport.g:414:4: ({...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) ) )
            	    // InternalABSReport.g:415:5: {...}? => ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 1)");
            	    }
            	    // InternalABSReport.g:415:111: ( ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) ) )
            	    // InternalABSReport.g:416:6: ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 1);
            	    					
            	    // InternalABSReport.g:419:9: ({...}? => ( (lv_obsolete_6_0= 'obsolete' ) ) )
            	    // InternalABSReport.g:419:10: {...}? => ( (lv_obsolete_6_0= 'obsolete' ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "true");
            	    }
            	    // InternalABSReport.g:419:19: ( (lv_obsolete_6_0= 'obsolete' ) )
            	    // InternalABSReport.g:419:20: (lv_obsolete_6_0= 'obsolete' )
            	    {
            	    // InternalABSReport.g:419:20: (lv_obsolete_6_0= 'obsolete' )
            	    // InternalABSReport.g:420:10: lv_obsolete_6_0= 'obsolete'
            	    {
            	    lv_obsolete_6_0=(Token)match(input,20,FOLLOW_15); 

            	    										newLeafNode(lv_obsolete_6_0, grammarAccess.getReportMetadataAccess().getObsoleteObsoleteKeyword_3_1_0());
            	    									

            	    										if (current==null) {
            	    											current = createModelElement(grammarAccess.getReportMetadataRule());
            	    										}
            	    										setWithLastConsumed(current, "obsolete", lv_obsolete_6_0 != null, "obsolete");
            	    									

            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalABSReport.g:437:4: ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:437:4: ({...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) ) )
            	    // InternalABSReport.g:438:5: {...}? => ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 2)");
            	    }
            	    // InternalABSReport.g:438:111: ( ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) ) )
            	    // InternalABSReport.g:439:6: ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 2);
            	    					
            	    // InternalABSReport.g:442:9: ({...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) ) )
            	    // InternalABSReport.g:442:10: {...}? => (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "true");
            	    }
            	    // InternalABSReport.g:442:19: (otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) ) )
            	    // InternalABSReport.g:442:20: otherlv_7= 'duration' ( (lv_duration_8_0= RULE_INT ) )
            	    {
            	    otherlv_7=(Token)match(input,21,FOLLOW_17); 

            	    									newLeafNode(otherlv_7, grammarAccess.getReportMetadataAccess().getDurationKeyword_3_2_0());
            	    								
            	    // InternalABSReport.g:446:9: ( (lv_duration_8_0= RULE_INT ) )
            	    // InternalABSReport.g:447:10: (lv_duration_8_0= RULE_INT )
            	    {
            	    // InternalABSReport.g:447:10: (lv_duration_8_0= RULE_INT )
            	    // InternalABSReport.g:448:11: lv_duration_8_0= RULE_INT
            	    {
            	    lv_duration_8_0=(Token)match(input,RULE_INT,FOLLOW_15); 

            	    											newLeafNode(lv_duration_8_0, grammarAccess.getReportMetadataAccess().getDurationINTTerminalRuleCall_3_2_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getReportMetadataRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"duration",
            	    												lv_duration_8_0,
            	    												"org.eclipse.xtext.common.Terminals.INT");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalABSReport.g:470:4: ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:470:4: ({...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) ) )
            	    // InternalABSReport.g:471:5: {...}? => ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 3)");
            	    }
            	    // InternalABSReport.g:471:111: ( ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) ) )
            	    // InternalABSReport.g:472:6: ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 3);
            	    					
            	    // InternalABSReport.g:475:9: ({...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) ) )
            	    // InternalABSReport.g:475:10: {...}? => (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "true");
            	    }
            	    // InternalABSReport.g:475:19: (otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) ) )
            	    // InternalABSReport.g:475:20: otherlv_9= 'rawCommand' ( (lv_rawCommand_10_0= RULE_STRING ) )
            	    {
            	    otherlv_9=(Token)match(input,22,FOLLOW_16); 

            	    									newLeafNode(otherlv_9, grammarAccess.getReportMetadataAccess().getRawCommandKeyword_3_3_0());
            	    								
            	    // InternalABSReport.g:479:9: ( (lv_rawCommand_10_0= RULE_STRING ) )
            	    // InternalABSReport.g:480:10: (lv_rawCommand_10_0= RULE_STRING )
            	    {
            	    // InternalABSReport.g:480:10: (lv_rawCommand_10_0= RULE_STRING )
            	    // InternalABSReport.g:481:11: lv_rawCommand_10_0= RULE_STRING
            	    {
            	    lv_rawCommand_10_0=(Token)match(input,RULE_STRING,FOLLOW_15); 

            	    											newLeafNode(lv_rawCommand_10_0, grammarAccess.getReportMetadataAccess().getRawCommandSTRINGTerminalRuleCall_3_3_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getReportMetadataRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"rawCommand",
            	    												lv_rawCommand_10_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalABSReport.g:503:4: ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:503:4: ({...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) ) )
            	    // InternalABSReport.g:504:5: {...}? => ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 4) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 4)");
            	    }
            	    // InternalABSReport.g:504:111: ( ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) ) )
            	    // InternalABSReport.g:505:6: ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 4);
            	    					
            	    // InternalABSReport.g:508:9: ({...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) ) )
            	    // InternalABSReport.g:508:10: {...}? => (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "true");
            	    }
            	    // InternalABSReport.g:508:19: (otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) ) )
            	    // InternalABSReport.g:508:20: otherlv_11= 'nbSteps' ( (lv_nbSteps_12_0= RULE_INT ) )
            	    {
            	    otherlv_11=(Token)match(input,23,FOLLOW_17); 

            	    									newLeafNode(otherlv_11, grammarAccess.getReportMetadataAccess().getNbStepsKeyword_3_4_0());
            	    								
            	    // InternalABSReport.g:512:9: ( (lv_nbSteps_12_0= RULE_INT ) )
            	    // InternalABSReport.g:513:10: (lv_nbSteps_12_0= RULE_INT )
            	    {
            	    // InternalABSReport.g:513:10: (lv_nbSteps_12_0= RULE_INT )
            	    // InternalABSReport.g:514:11: lv_nbSteps_12_0= RULE_INT
            	    {
            	    lv_nbSteps_12_0=(Token)match(input,RULE_INT,FOLLOW_15); 

            	    											newLeafNode(lv_nbSteps_12_0, grammarAccess.getReportMetadataAccess().getNbStepsINTTerminalRuleCall_3_4_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getReportMetadataRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"nbSteps",
            	    												lv_nbSteps_12_0,
            	    												"org.eclipse.xtext.common.Terminals.INT");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 6 :
            	    // InternalABSReport.g:536:4: ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:536:4: ({...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) ) )
            	    // InternalABSReport.g:537:5: {...}? => ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 5) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 5)");
            	    }
            	    // InternalABSReport.g:537:111: ( ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) ) )
            	    // InternalABSReport.g:538:6: ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 5);
            	    					
            	    // InternalABSReport.g:541:9: ({...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) ) )
            	    // InternalABSReport.g:541:10: {...}? => (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "true");
            	    }
            	    // InternalABSReport.g:541:19: (otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) ) )
            	    // InternalABSReport.g:541:20: otherlv_13= 'invariant' ( (lv_invariant_14_0= RULE_STRING ) )
            	    {
            	    otherlv_13=(Token)match(input,24,FOLLOW_16); 

            	    									newLeafNode(otherlv_13, grammarAccess.getReportMetadataAccess().getInvariantKeyword_3_5_0());
            	    								
            	    // InternalABSReport.g:545:9: ( (lv_invariant_14_0= RULE_STRING ) )
            	    // InternalABSReport.g:546:10: (lv_invariant_14_0= RULE_STRING )
            	    {
            	    // InternalABSReport.g:546:10: (lv_invariant_14_0= RULE_STRING )
            	    // InternalABSReport.g:547:11: lv_invariant_14_0= RULE_STRING
            	    {
            	    lv_invariant_14_0=(Token)match(input,RULE_STRING,FOLLOW_15); 

            	    											newLeafNode(lv_invariant_14_0, grammarAccess.getReportMetadataAccess().getInvariantSTRINGTerminalRuleCall_3_5_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getReportMetadataRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"invariant",
            	    												lv_invariant_14_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 7 :
            	    // InternalABSReport.g:569:4: ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) )
            	    {
            	    // InternalABSReport.g:569:4: ({...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) ) )
            	    // InternalABSReport.g:570:5: {...}? => ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 6) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "getUnorderedGroupHelper().canSelect(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 6)");
            	    }
            	    // InternalABSReport.g:570:111: ( ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) ) )
            	    // InternalABSReport.g:571:6: ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3(), 6);
            	    					
            	    // InternalABSReport.g:574:9: ({...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* ) )
            	    // InternalABSReport.g:574:10: {...}? => (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleReportMetadata", "true");
            	    }
            	    // InternalABSReport.g:574:19: (otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )* )
            	    // InternalABSReport.g:574:20: otherlv_15= 'previousWitnesses' ( (otherlv_16= RULE_STRING ) ) (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )*
            	    {
            	    otherlv_15=(Token)match(input,25,FOLLOW_16); 

            	    									newLeafNode(otherlv_15, grammarAccess.getReportMetadataAccess().getPreviousWitnessesKeyword_3_6_0());
            	    								
            	    // InternalABSReport.g:578:9: ( (otherlv_16= RULE_STRING ) )
            	    // InternalABSReport.g:579:10: (otherlv_16= RULE_STRING )
            	    {
            	    // InternalABSReport.g:579:10: (otherlv_16= RULE_STRING )
            	    // InternalABSReport.g:580:11: otherlv_16= RULE_STRING
            	    {

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getReportMetadataRule());
            	    											}
            	    										
            	    otherlv_16=(Token)match(input,RULE_STRING,FOLLOW_18); 

            	    											newLeafNode(otherlv_16, grammarAccess.getReportMetadataAccess().getPreviousABSGoalWitnessCrossReference_3_6_1_0());
            	    										

            	    }


            	    }

            	    // InternalABSReport.g:591:9: (otherlv_17= ',' ( (otherlv_18= RULE_STRING ) ) )*
            	    loop11:
            	    do {
            	        int alt11=2;
            	        int LA11_0 = input.LA(1);

            	        if ( (LA11_0==26) ) {
            	            alt11=1;
            	        }


            	        switch (alt11) {
            	    	case 1 :
            	    	    // InternalABSReport.g:592:10: otherlv_17= ',' ( (otherlv_18= RULE_STRING ) )
            	    	    {
            	    	    otherlv_17=(Token)match(input,26,FOLLOW_16); 

            	    	    										newLeafNode(otherlv_17, grammarAccess.getReportMetadataAccess().getCommaKeyword_3_6_2_0());
            	    	    									
            	    	    // InternalABSReport.g:596:10: ( (otherlv_18= RULE_STRING ) )
            	    	    // InternalABSReport.g:597:11: (otherlv_18= RULE_STRING )
            	    	    {
            	    	    // InternalABSReport.g:597:11: (otherlv_18= RULE_STRING )
            	    	    // InternalABSReport.g:598:12: otherlv_18= RULE_STRING
            	    	    {

            	    	    												if (current==null) {
            	    	    													current = createModelElement(grammarAccess.getReportMetadataRule());
            	    	    												}
            	    	    											
            	    	    otherlv_18=(Token)match(input,RULE_STRING,FOLLOW_18); 

            	    	    												newLeafNode(otherlv_18, grammarAccess.getReportMetadataAccess().getPreviousABSGoalWitnessCrossReference_3_6_2_1_0());
            	    	    											

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop11;
            	        }
            	    } while (true);


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getReportMetadataAccess().getUnorderedGroup_3());
            				

            }

            otherlv_19=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_19, grammarAccess.getReportMetadataAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReportMetadata"


    // $ANTLR start "entryRuleStep"
    // InternalABSReport.g:631:1: entryRuleStep returns [EObject current=null] : iv_ruleStep= ruleStep EOF ;
    public final EObject entryRuleStep() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStep = null;


        try {
            // InternalABSReport.g:631:45: (iv_ruleStep= ruleStep EOF )
            // InternalABSReport.g:632:2: iv_ruleStep= ruleStep EOF
            {
             newCompositeNode(grammarAccess.getStepRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStep=ruleStep();

            state._fsp--;

             current =iv_ruleStep; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStep"


    // $ANTLR start "ruleStep"
    // InternalABSReport.g:638:1: ruleStep returns [EObject current=null] : ( ( ( ruleQualifiedName ) ) otherlv_1= '(' ( ( ( ruleQualifiedName ) ) (otherlv_3= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_5= ')' ) ;
    public final EObject ruleStep() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalABSReport.g:644:2: ( ( ( ( ruleQualifiedName ) ) otherlv_1= '(' ( ( ( ruleQualifiedName ) ) (otherlv_3= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_5= ')' ) )
            // InternalABSReport.g:645:2: ( ( ( ruleQualifiedName ) ) otherlv_1= '(' ( ( ( ruleQualifiedName ) ) (otherlv_3= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_5= ')' )
            {
            // InternalABSReport.g:645:2: ( ( ( ruleQualifiedName ) ) otherlv_1= '(' ( ( ( ruleQualifiedName ) ) (otherlv_3= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_5= ')' )
            // InternalABSReport.g:646:3: ( ( ruleQualifiedName ) ) otherlv_1= '(' ( ( ( ruleQualifiedName ) ) (otherlv_3= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_5= ')'
            {
            // InternalABSReport.g:646:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:647:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:647:4: ( ruleQualifiedName )
            // InternalABSReport.g:648:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStepRule());
            					}
            				

            					newCompositeNode(grammarAccess.getStepAccess().getGuardedActionGuardedActionCrossReference_0_0());
            				
            pushFollow(FOLLOW_19);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,27,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getStepAccess().getLeftParenthesisKeyword_1());
            		
            // InternalABSReport.g:666:3: ( ( ( ruleQualifiedName ) ) (otherlv_3= ',' ( ( ruleQualifiedName ) ) )* )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_ID) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalABSReport.g:667:4: ( ( ruleQualifiedName ) ) (otherlv_3= ',' ( ( ruleQualifiedName ) ) )*
                    {
                    // InternalABSReport.g:667:4: ( ( ruleQualifiedName ) )
                    // InternalABSReport.g:668:5: ( ruleQualifiedName )
                    {
                    // InternalABSReport.g:668:5: ( ruleQualifiedName )
                    // InternalABSReport.g:669:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getStepRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getStepAccess().getParametersAssetCrossReference_2_0_0());
                    					
                    pushFollow(FOLLOW_21);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalABSReport.g:683:4: (otherlv_3= ',' ( ( ruleQualifiedName ) ) )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==26) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // InternalABSReport.g:684:5: otherlv_3= ',' ( ( ruleQualifiedName ) )
                    	    {
                    	    otherlv_3=(Token)match(input,26,FOLLOW_5); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getStepAccess().getCommaKeyword_2_1_0());
                    	    				
                    	    // InternalABSReport.g:688:5: ( ( ruleQualifiedName ) )
                    	    // InternalABSReport.g:689:6: ( ruleQualifiedName )
                    	    {
                    	    // InternalABSReport.g:689:6: ( ruleQualifiedName )
                    	    // InternalABSReport.g:690:7: ruleQualifiedName
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getStepRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getStepAccess().getParametersAssetCrossReference_2_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_21);
                    	    ruleQualifiedName();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,28,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getStepAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStep"


    // $ANTLR start "entryRuleStepState"
    // InternalABSReport.g:714:1: entryRuleStepState returns [EObject current=null] : iv_ruleStepState= ruleStepState EOF ;
    public final EObject entryRuleStepState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStepState = null;


        try {
            // InternalABSReport.g:714:50: (iv_ruleStepState= ruleStepState EOF )
            // InternalABSReport.g:715:2: iv_ruleStepState= ruleStepState EOF
            {
             newCompositeNode(grammarAccess.getStepStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStepState=ruleStepState();

            state._fsp--;

             current =iv_ruleStepState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStepState"


    // $ANTLR start "ruleStepState"
    // InternalABSReport.g:721:1: ruleStepState returns [EObject current=null] : ( () otherlv_1= 'state' otherlv_2= '{' ( ( (lv_assetState_3_0= ruleAssetState ) ) (otherlv_4= ',' ( (lv_assetState_5_0= ruleAssetState ) ) )* )? otherlv_6= '}' ) ;
    public final EObject ruleStepState() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_assetState_3_0 = null;

        EObject lv_assetState_5_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:727:2: ( ( () otherlv_1= 'state' otherlv_2= '{' ( ( (lv_assetState_3_0= ruleAssetState ) ) (otherlv_4= ',' ( (lv_assetState_5_0= ruleAssetState ) ) )* )? otherlv_6= '}' ) )
            // InternalABSReport.g:728:2: ( () otherlv_1= 'state' otherlv_2= '{' ( ( (lv_assetState_3_0= ruleAssetState ) ) (otherlv_4= ',' ( (lv_assetState_5_0= ruleAssetState ) ) )* )? otherlv_6= '}' )
            {
            // InternalABSReport.g:728:2: ( () otherlv_1= 'state' otherlv_2= '{' ( ( (lv_assetState_3_0= ruleAssetState ) ) (otherlv_4= ',' ( (lv_assetState_5_0= ruleAssetState ) ) )* )? otherlv_6= '}' )
            // InternalABSReport.g:729:3: () otherlv_1= 'state' otherlv_2= '{' ( ( (lv_assetState_3_0= ruleAssetState ) ) (otherlv_4= ',' ( (lv_assetState_5_0= ruleAssetState ) ) )* )? otherlv_6= '}'
            {
            // InternalABSReport.g:729:3: ()
            // InternalABSReport.g:730:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStepStateAccess().getStepStateAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,29,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getStepStateAccess().getStateKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_10); 

            			newLeafNode(otherlv_2, grammarAccess.getStepStateAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalABSReport.g:744:3: ( ( (lv_assetState_3_0= ruleAssetState ) ) (otherlv_4= ',' ( (lv_assetState_5_0= ruleAssetState ) ) )* )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalABSReport.g:745:4: ( (lv_assetState_3_0= ruleAssetState ) ) (otherlv_4= ',' ( (lv_assetState_5_0= ruleAssetState ) ) )*
                    {
                    // InternalABSReport.g:745:4: ( (lv_assetState_3_0= ruleAssetState ) )
                    // InternalABSReport.g:746:5: (lv_assetState_3_0= ruleAssetState )
                    {
                    // InternalABSReport.g:746:5: (lv_assetState_3_0= ruleAssetState )
                    // InternalABSReport.g:747:6: lv_assetState_3_0= ruleAssetState
                    {

                    						newCompositeNode(grammarAccess.getStepStateAccess().getAssetStateAssetStateParserRuleCall_3_0_0());
                    					
                    pushFollow(FOLLOW_22);
                    lv_assetState_3_0=ruleAssetState();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getStepStateRule());
                    						}
                    						add(
                    							current,
                    							"assetState",
                    							lv_assetState_3_0,
                    							"fr.irisa.atsyra.absreport.ABSReport.AssetState");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalABSReport.g:764:4: (otherlv_4= ',' ( (lv_assetState_5_0= ruleAssetState ) ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==26) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // InternalABSReport.g:765:5: otherlv_4= ',' ( (lv_assetState_5_0= ruleAssetState ) )
                    	    {
                    	    otherlv_4=(Token)match(input,26,FOLLOW_5); 

                    	    					newLeafNode(otherlv_4, grammarAccess.getStepStateAccess().getCommaKeyword_3_1_0());
                    	    				
                    	    // InternalABSReport.g:769:5: ( (lv_assetState_5_0= ruleAssetState ) )
                    	    // InternalABSReport.g:770:6: (lv_assetState_5_0= ruleAssetState )
                    	    {
                    	    // InternalABSReport.g:770:6: (lv_assetState_5_0= ruleAssetState )
                    	    // InternalABSReport.g:771:7: lv_assetState_5_0= ruleAssetState
                    	    {

                    	    							newCompositeNode(grammarAccess.getStepStateAccess().getAssetStateAssetStateParserRuleCall_3_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_22);
                    	    lv_assetState_5_0=ruleAssetState();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getStepStateRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"assetState",
                    	    								lv_assetState_5_0,
                    	    								"fr.irisa.atsyra.absreport.ABSReport.AssetState");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getStepStateAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStepState"


    // $ANTLR start "entryRuleAssetState"
    // InternalABSReport.g:798:1: entryRuleAssetState returns [EObject current=null] : iv_ruleAssetState= ruleAssetState EOF ;
    public final EObject entryRuleAssetState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetState = null;


        try {
            // InternalABSReport.g:798:51: (iv_ruleAssetState= ruleAssetState EOF )
            // InternalABSReport.g:799:2: iv_ruleAssetState= ruleAssetState EOF
            {
             newCompositeNode(grammarAccess.getAssetStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetState=ruleAssetState();

            state._fsp--;

             current =iv_ruleAssetState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetState"


    // $ANTLR start "ruleAssetState"
    // InternalABSReport.g:805:1: ruleAssetState returns [EObject current=null] : ( ( ( ruleQualifiedName ) ) otherlv_1= '{' ( ( (lv_featureStates_2_0= ruleFeatureState ) ) (otherlv_3= ',' ( (lv_featureStates_4_0= ruleFeatureState ) ) )* )? otherlv_5= '}' ) ;
    public final EObject ruleAssetState() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_featureStates_2_0 = null;

        EObject lv_featureStates_4_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:811:2: ( ( ( ( ruleQualifiedName ) ) otherlv_1= '{' ( ( (lv_featureStates_2_0= ruleFeatureState ) ) (otherlv_3= ',' ( (lv_featureStates_4_0= ruleFeatureState ) ) )* )? otherlv_5= '}' ) )
            // InternalABSReport.g:812:2: ( ( ( ruleQualifiedName ) ) otherlv_1= '{' ( ( (lv_featureStates_2_0= ruleFeatureState ) ) (otherlv_3= ',' ( (lv_featureStates_4_0= ruleFeatureState ) ) )* )? otherlv_5= '}' )
            {
            // InternalABSReport.g:812:2: ( ( ( ruleQualifiedName ) ) otherlv_1= '{' ( ( (lv_featureStates_2_0= ruleFeatureState ) ) (otherlv_3= ',' ( (lv_featureStates_4_0= ruleFeatureState ) ) )* )? otherlv_5= '}' )
            // InternalABSReport.g:813:3: ( ( ruleQualifiedName ) ) otherlv_1= '{' ( ( (lv_featureStates_2_0= ruleFeatureState ) ) (otherlv_3= ',' ( (lv_featureStates_4_0= ruleFeatureState ) ) )* )? otherlv_5= '}'
            {
            // InternalABSReport.g:813:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:814:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:814:4: ( ruleQualifiedName )
            // InternalABSReport.g:815:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetStateRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetStateAccess().getAssetAssetCrossReference_0_0());
            				
            pushFollow(FOLLOW_6);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,12,FOLLOW_23); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetStateAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalABSReport.g:833:3: ( ( (lv_featureStates_2_0= ruleFeatureState ) ) (otherlv_3= ',' ( (lv_featureStates_4_0= ruleFeatureState ) ) )* )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==30||LA18_0==34) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalABSReport.g:834:4: ( (lv_featureStates_2_0= ruleFeatureState ) ) (otherlv_3= ',' ( (lv_featureStates_4_0= ruleFeatureState ) ) )*
                    {
                    // InternalABSReport.g:834:4: ( (lv_featureStates_2_0= ruleFeatureState ) )
                    // InternalABSReport.g:835:5: (lv_featureStates_2_0= ruleFeatureState )
                    {
                    // InternalABSReport.g:835:5: (lv_featureStates_2_0= ruleFeatureState )
                    // InternalABSReport.g:836:6: lv_featureStates_2_0= ruleFeatureState
                    {

                    						newCompositeNode(grammarAccess.getAssetStateAccess().getFeatureStatesFeatureStateParserRuleCall_2_0_0());
                    					
                    pushFollow(FOLLOW_22);
                    lv_featureStates_2_0=ruleFeatureState();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAssetStateRule());
                    						}
                    						add(
                    							current,
                    							"featureStates",
                    							lv_featureStates_2_0,
                    							"fr.irisa.atsyra.absreport.ABSReport.FeatureState");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalABSReport.g:853:4: (otherlv_3= ',' ( (lv_featureStates_4_0= ruleFeatureState ) ) )*
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==26) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // InternalABSReport.g:854:5: otherlv_3= ',' ( (lv_featureStates_4_0= ruleFeatureState ) )
                    	    {
                    	    otherlv_3=(Token)match(input,26,FOLLOW_24); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getAssetStateAccess().getCommaKeyword_2_1_0());
                    	    				
                    	    // InternalABSReport.g:858:5: ( (lv_featureStates_4_0= ruleFeatureState ) )
                    	    // InternalABSReport.g:859:6: (lv_featureStates_4_0= ruleFeatureState )
                    	    {
                    	    // InternalABSReport.g:859:6: (lv_featureStates_4_0= ruleFeatureState )
                    	    // InternalABSReport.g:860:7: lv_featureStates_4_0= ruleFeatureState
                    	    {

                    	    							newCompositeNode(grammarAccess.getAssetStateAccess().getFeatureStatesFeatureStateParserRuleCall_2_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_22);
                    	    lv_featureStates_4_0=ruleFeatureState();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAssetStateRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"featureStates",
                    	    								lv_featureStates_4_0,
                    	    								"fr.irisa.atsyra.absreport.ABSReport.FeatureState");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop17;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getAssetStateAccess().getRightCurlyBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetState"


    // $ANTLR start "entryRuleFeatureState"
    // InternalABSReport.g:887:1: entryRuleFeatureState returns [EObject current=null] : iv_ruleFeatureState= ruleFeatureState EOF ;
    public final EObject entryRuleFeatureState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureState = null;


        try {
            // InternalABSReport.g:887:53: (iv_ruleFeatureState= ruleFeatureState EOF )
            // InternalABSReport.g:888:2: iv_ruleFeatureState= ruleFeatureState EOF
            {
             newCompositeNode(grammarAccess.getFeatureStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureState=ruleFeatureState();

            state._fsp--;

             current =iv_ruleFeatureState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureState"


    // $ANTLR start "ruleFeatureState"
    // InternalABSReport.g:894:1: ruleFeatureState returns [EObject current=null] : (this_AttributeState_0= ruleAttributeState | this_ReferenceState_1= ruleReferenceState ) ;
    public final EObject ruleFeatureState() throws RecognitionException {
        EObject current = null;

        EObject this_AttributeState_0 = null;

        EObject this_ReferenceState_1 = null;



        	enterRule();

        try {
            // InternalABSReport.g:900:2: ( (this_AttributeState_0= ruleAttributeState | this_ReferenceState_1= ruleReferenceState ) )
            // InternalABSReport.g:901:2: (this_AttributeState_0= ruleAttributeState | this_ReferenceState_1= ruleReferenceState )
            {
            // InternalABSReport.g:901:2: (this_AttributeState_0= ruleAttributeState | this_ReferenceState_1= ruleReferenceState )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==30) ) {
                alt19=1;
            }
            else if ( (LA19_0==34) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalABSReport.g:902:3: this_AttributeState_0= ruleAttributeState
                    {

                    			newCompositeNode(grammarAccess.getFeatureStateAccess().getAttributeStateParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AttributeState_0=ruleAttributeState();

                    state._fsp--;


                    			current = this_AttributeState_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalABSReport.g:911:3: this_ReferenceState_1= ruleReferenceState
                    {

                    			newCompositeNode(grammarAccess.getFeatureStateAccess().getReferenceStateParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ReferenceState_1=ruleReferenceState();

                    state._fsp--;


                    			current = this_ReferenceState_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureState"


    // $ANTLR start "entryRuleAttributeState"
    // InternalABSReport.g:923:1: entryRuleAttributeState returns [EObject current=null] : iv_ruleAttributeState= ruleAttributeState EOF ;
    public final EObject entryRuleAttributeState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeState = null;


        try {
            // InternalABSReport.g:923:55: (iv_ruleAttributeState= ruleAttributeState EOF )
            // InternalABSReport.g:924:2: iv_ruleAttributeState= ruleAttributeState EOF
            {
             newCompositeNode(grammarAccess.getAttributeStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeState=ruleAttributeState();

            state._fsp--;

             current =iv_ruleAttributeState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeState"


    // $ANTLR start "ruleAttributeState"
    // InternalABSReport.g:930:1: ruleAttributeState returns [EObject current=null] : (otherlv_0= 'attribute' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( (lv_values_3_0= ruleAttributeConstantExpression ) ) | (otherlv_4= '[' ( (lv_values_5_0= ruleAttributeConstantExpression ) ) (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )* otherlv_8= ']' ) ) ) ;
    public final EObject ruleAttributeState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_values_3_0 = null;

        EObject lv_values_5_0 = null;

        EObject lv_values_7_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:936:2: ( (otherlv_0= 'attribute' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( (lv_values_3_0= ruleAttributeConstantExpression ) ) | (otherlv_4= '[' ( (lv_values_5_0= ruleAttributeConstantExpression ) ) (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )* otherlv_8= ']' ) ) ) )
            // InternalABSReport.g:937:2: (otherlv_0= 'attribute' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( (lv_values_3_0= ruleAttributeConstantExpression ) ) | (otherlv_4= '[' ( (lv_values_5_0= ruleAttributeConstantExpression ) ) (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )* otherlv_8= ']' ) ) )
            {
            // InternalABSReport.g:937:2: (otherlv_0= 'attribute' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( (lv_values_3_0= ruleAttributeConstantExpression ) ) | (otherlv_4= '[' ( (lv_values_5_0= ruleAttributeConstantExpression ) ) (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )* otherlv_8= ']' ) ) )
            // InternalABSReport.g:938:3: otherlv_0= 'attribute' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( (lv_values_3_0= ruleAttributeConstantExpression ) ) | (otherlv_4= '[' ( (lv_values_5_0= ruleAttributeConstantExpression ) ) (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )* otherlv_8= ']' ) )
            {
            otherlv_0=(Token)match(input,30,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getAttributeStateAccess().getAttributeKeyword_0());
            		
            // InternalABSReport.g:942:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:943:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:943:4: ( ruleQualifiedName )
            // InternalABSReport.g:944:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeStateRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAttributeStateAccess().getAttributeAssetTypeAttributeCrossReference_1_0());
            				
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,31,FOLLOW_26); 

            			newLeafNode(otherlv_2, grammarAccess.getAttributeStateAccess().getEqualsSignKeyword_2());
            		
            // InternalABSReport.g:962:3: ( ( (lv_values_3_0= ruleAttributeConstantExpression ) ) | (otherlv_4= '[' ( (lv_values_5_0= ruleAttributeConstantExpression ) ) (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )* otherlv_8= ']' ) )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=RULE_STRING && LA21_0<=RULE_ID)||LA21_0==35||(LA21_0>=61 && LA21_0<=62)) ) {
                alt21=1;
            }
            else if ( (LA21_0==32) ) {
                alt21=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalABSReport.g:963:4: ( (lv_values_3_0= ruleAttributeConstantExpression ) )
                    {
                    // InternalABSReport.g:963:4: ( (lv_values_3_0= ruleAttributeConstantExpression ) )
                    // InternalABSReport.g:964:5: (lv_values_3_0= ruleAttributeConstantExpression )
                    {
                    // InternalABSReport.g:964:5: (lv_values_3_0= ruleAttributeConstantExpression )
                    // InternalABSReport.g:965:6: lv_values_3_0= ruleAttributeConstantExpression
                    {

                    						newCompositeNode(grammarAccess.getAttributeStateAccess().getValuesAttributeConstantExpressionParserRuleCall_3_0_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_values_3_0=ruleAttributeConstantExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeStateRule());
                    						}
                    						add(
                    							current,
                    							"values",
                    							lv_values_3_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:983:4: (otherlv_4= '[' ( (lv_values_5_0= ruleAttributeConstantExpression ) ) (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )* otherlv_8= ']' )
                    {
                    // InternalABSReport.g:983:4: (otherlv_4= '[' ( (lv_values_5_0= ruleAttributeConstantExpression ) ) (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )* otherlv_8= ']' )
                    // InternalABSReport.g:984:5: otherlv_4= '[' ( (lv_values_5_0= ruleAttributeConstantExpression ) ) (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )* otherlv_8= ']'
                    {
                    otherlv_4=(Token)match(input,32,FOLLOW_27); 

                    					newLeafNode(otherlv_4, grammarAccess.getAttributeStateAccess().getLeftSquareBracketKeyword_3_1_0());
                    				
                    // InternalABSReport.g:988:5: ( (lv_values_5_0= ruleAttributeConstantExpression ) )
                    // InternalABSReport.g:989:6: (lv_values_5_0= ruleAttributeConstantExpression )
                    {
                    // InternalABSReport.g:989:6: (lv_values_5_0= ruleAttributeConstantExpression )
                    // InternalABSReport.g:990:7: lv_values_5_0= ruleAttributeConstantExpression
                    {

                    							newCompositeNode(grammarAccess.getAttributeStateAccess().getValuesAttributeConstantExpressionParserRuleCall_3_1_1_0());
                    						
                    pushFollow(FOLLOW_28);
                    lv_values_5_0=ruleAttributeConstantExpression();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getAttributeStateRule());
                    							}
                    							add(
                    								current,
                    								"values",
                    								lv_values_5_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalABSReport.g:1007:5: (otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) ) )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==26) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // InternalABSReport.g:1008:6: otherlv_6= ',' ( (lv_values_7_0= ruleAttributeConstantExpression ) )
                    	    {
                    	    otherlv_6=(Token)match(input,26,FOLLOW_27); 

                    	    						newLeafNode(otherlv_6, grammarAccess.getAttributeStateAccess().getCommaKeyword_3_1_2_0());
                    	    					
                    	    // InternalABSReport.g:1012:6: ( (lv_values_7_0= ruleAttributeConstantExpression ) )
                    	    // InternalABSReport.g:1013:7: (lv_values_7_0= ruleAttributeConstantExpression )
                    	    {
                    	    // InternalABSReport.g:1013:7: (lv_values_7_0= ruleAttributeConstantExpression )
                    	    // InternalABSReport.g:1014:8: lv_values_7_0= ruleAttributeConstantExpression
                    	    {

                    	    								newCompositeNode(grammarAccess.getAttributeStateAccess().getValuesAttributeConstantExpressionParserRuleCall_3_1_2_1_0());
                    	    							
                    	    pushFollow(FOLLOW_28);
                    	    lv_values_7_0=ruleAttributeConstantExpression();

                    	    state._fsp--;


                    	    								if (current==null) {
                    	    									current = createModelElementForParent(grammarAccess.getAttributeStateRule());
                    	    								}
                    	    								add(
                    	    									current,
                    	    									"values",
                    	    									lv_values_7_0,
                    	    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                    	    								afterParserOrEnumRuleCall();
                    	    							

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,33,FOLLOW_2); 

                    					newLeafNode(otherlv_8, grammarAccess.getAttributeStateAccess().getRightSquareBracketKeyword_3_1_3());
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeState"


    // $ANTLR start "entryRuleReferenceState"
    // InternalABSReport.g:1042:1: entryRuleReferenceState returns [EObject current=null] : iv_ruleReferenceState= ruleReferenceState EOF ;
    public final EObject entryRuleReferenceState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferenceState = null;


        try {
            // InternalABSReport.g:1042:55: (iv_ruleReferenceState= ruleReferenceState EOF )
            // InternalABSReport.g:1043:2: iv_ruleReferenceState= ruleReferenceState EOF
            {
             newCompositeNode(grammarAccess.getReferenceStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReferenceState=ruleReferenceState();

            state._fsp--;

             current =iv_ruleReferenceState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferenceState"


    // $ANTLR start "ruleReferenceState"
    // InternalABSReport.g:1049:1: ruleReferenceState returns [EObject current=null] : (otherlv_0= 'reference' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( ( ruleQualifiedName ) ) | otherlv_4= 'undefined' | (otherlv_5= '[' ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* otherlv_9= ']' ) ) ) ;
    public final EObject ruleReferenceState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;


        	enterRule();

        try {
            // InternalABSReport.g:1055:2: ( (otherlv_0= 'reference' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( ( ruleQualifiedName ) ) | otherlv_4= 'undefined' | (otherlv_5= '[' ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* otherlv_9= ']' ) ) ) )
            // InternalABSReport.g:1056:2: (otherlv_0= 'reference' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( ( ruleQualifiedName ) ) | otherlv_4= 'undefined' | (otherlv_5= '[' ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* otherlv_9= ']' ) ) )
            {
            // InternalABSReport.g:1056:2: (otherlv_0= 'reference' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( ( ruleQualifiedName ) ) | otherlv_4= 'undefined' | (otherlv_5= '[' ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* otherlv_9= ']' ) ) )
            // InternalABSReport.g:1057:3: otherlv_0= 'reference' ( ( ruleQualifiedName ) ) otherlv_2= '=' ( ( ( ruleQualifiedName ) ) | otherlv_4= 'undefined' | (otherlv_5= '[' ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* otherlv_9= ']' ) )
            {
            otherlv_0=(Token)match(input,34,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getReferenceStateAccess().getReferenceKeyword_0());
            		
            // InternalABSReport.g:1061:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:1062:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:1062:4: ( ruleQualifiedName )
            // InternalABSReport.g:1063:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReferenceStateRule());
            					}
            				

            					newCompositeNode(grammarAccess.getReferenceStateAccess().getReferenceAssetTypeReferenceCrossReference_1_0());
            				
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,31,FOLLOW_29); 

            			newLeafNode(otherlv_2, grammarAccess.getReferenceStateAccess().getEqualsSignKeyword_2());
            		
            // InternalABSReport.g:1081:3: ( ( ( ruleQualifiedName ) ) | otherlv_4= 'undefined' | (otherlv_5= '[' ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* otherlv_9= ']' ) )
            int alt23=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt23=1;
                }
                break;
            case 35:
                {
                alt23=2;
                }
                break;
            case 32:
                {
                alt23=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // InternalABSReport.g:1082:4: ( ( ruleQualifiedName ) )
                    {
                    // InternalABSReport.g:1082:4: ( ( ruleQualifiedName ) )
                    // InternalABSReport.g:1083:5: ( ruleQualifiedName )
                    {
                    // InternalABSReport.g:1083:5: ( ruleQualifiedName )
                    // InternalABSReport.g:1084:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getReferenceStateRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getReferenceStateAccess().getValuesAssetCrossReference_3_0_0());
                    					
                    pushFollow(FOLLOW_2);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:1099:4: otherlv_4= 'undefined'
                    {
                    otherlv_4=(Token)match(input,35,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getReferenceStateAccess().getUndefinedKeyword_3_1());
                    			

                    }
                    break;
                case 3 :
                    // InternalABSReport.g:1104:4: (otherlv_5= '[' ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* otherlv_9= ']' )
                    {
                    // InternalABSReport.g:1104:4: (otherlv_5= '[' ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* otherlv_9= ']' )
                    // InternalABSReport.g:1105:5: otherlv_5= '[' ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* otherlv_9= ']'
                    {
                    otherlv_5=(Token)match(input,32,FOLLOW_5); 

                    					newLeafNode(otherlv_5, grammarAccess.getReferenceStateAccess().getLeftSquareBracketKeyword_3_2_0());
                    				
                    // InternalABSReport.g:1109:5: ( ( ruleQualifiedName ) )
                    // InternalABSReport.g:1110:6: ( ruleQualifiedName )
                    {
                    // InternalABSReport.g:1110:6: ( ruleQualifiedName )
                    // InternalABSReport.g:1111:7: ruleQualifiedName
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getReferenceStateRule());
                    							}
                    						

                    							newCompositeNode(grammarAccess.getReferenceStateAccess().getValuesAssetCrossReference_3_2_1_0());
                    						
                    pushFollow(FOLLOW_28);
                    ruleQualifiedName();

                    state._fsp--;


                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalABSReport.g:1125:5: (otherlv_7= ',' ( ( ruleQualifiedName ) ) )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==26) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // InternalABSReport.g:1126:6: otherlv_7= ',' ( ( ruleQualifiedName ) )
                    	    {
                    	    otherlv_7=(Token)match(input,26,FOLLOW_5); 

                    	    						newLeafNode(otherlv_7, grammarAccess.getReferenceStateAccess().getCommaKeyword_3_2_2_0());
                    	    					
                    	    // InternalABSReport.g:1130:6: ( ( ruleQualifiedName ) )
                    	    // InternalABSReport.g:1131:7: ( ruleQualifiedName )
                    	    {
                    	    // InternalABSReport.g:1131:7: ( ruleQualifiedName )
                    	    // InternalABSReport.g:1132:8: ruleQualifiedName
                    	    {

                    	    								if (current==null) {
                    	    									current = createModelElement(grammarAccess.getReferenceStateRule());
                    	    								}
                    	    							

                    	    								newCompositeNode(grammarAccess.getReferenceStateAccess().getValuesAssetCrossReference_3_2_2_1_0());
                    	    							
                    	    pushFollow(FOLLOW_28);
                    	    ruleQualifiedName();

                    	    state._fsp--;


                    	    								afterParserOrEnumRuleCall();
                    	    							

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,33,FOLLOW_2); 

                    					newLeafNode(otherlv_9, grammarAccess.getReferenceStateAccess().getRightSquareBracketKeyword_3_2_3());
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferenceState"


    // $ANTLR start "entryRuleAbstractAssetType"
    // InternalABSReport.g:1157:1: entryRuleAbstractAssetType returns [EObject current=null] : iv_ruleAbstractAssetType= ruleAbstractAssetType EOF ;
    public final EObject entryRuleAbstractAssetType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractAssetType = null;


        try {
            // InternalABSReport.g:1157:58: (iv_ruleAbstractAssetType= ruleAbstractAssetType EOF )
            // InternalABSReport.g:1158:2: iv_ruleAbstractAssetType= ruleAbstractAssetType EOF
            {
             newCompositeNode(grammarAccess.getAbstractAssetTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractAssetType=ruleAbstractAssetType();

            state._fsp--;

             current =iv_ruleAbstractAssetType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractAssetType"


    // $ANTLR start "ruleAbstractAssetType"
    // InternalABSReport.g:1164:1: ruleAbstractAssetType returns [EObject current=null] : (this_AssetType_0= ruleAssetType | this_AssetTypeAspect_1= ruleAssetTypeAspect ) ;
    public final EObject ruleAbstractAssetType() throws RecognitionException {
        EObject current = null;

        EObject this_AssetType_0 = null;

        EObject this_AssetTypeAspect_1 = null;



        	enterRule();

        try {
            // InternalABSReport.g:1170:2: ( (this_AssetType_0= ruleAssetType | this_AssetTypeAspect_1= ruleAssetTypeAspect ) )
            // InternalABSReport.g:1171:2: (this_AssetType_0= ruleAssetType | this_AssetTypeAspect_1= ruleAssetTypeAspect )
            {
            // InternalABSReport.g:1171:2: (this_AssetType_0= ruleAssetType | this_AssetTypeAspect_1= ruleAssetTypeAspect )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( ((LA24_0>=41 && LA24_0<=42)) ) {
                alt24=1;
            }
            else if ( (LA24_0==46) ) {
                alt24=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }
            switch (alt24) {
                case 1 :
                    // InternalABSReport.g:1172:3: this_AssetType_0= ruleAssetType
                    {

                    			newCompositeNode(grammarAccess.getAbstractAssetTypeAccess().getAssetTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AssetType_0=ruleAssetType();

                    state._fsp--;


                    			current = this_AssetType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalABSReport.g:1181:3: this_AssetTypeAspect_1= ruleAssetTypeAspect
                    {

                    			newCompositeNode(grammarAccess.getAbstractAssetTypeAccess().getAssetTypeAspectParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AssetTypeAspect_1=ruleAssetTypeAspect();

                    state._fsp--;


                    			current = this_AssetTypeAspect_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractAssetType"


    // $ANTLR start "entryRulePrimitiveDataType"
    // InternalABSReport.g:1193:1: entryRulePrimitiveDataType returns [EObject current=null] : iv_rulePrimitiveDataType= rulePrimitiveDataType EOF ;
    public final EObject entryRulePrimitiveDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveDataType = null;


        try {
            // InternalABSReport.g:1193:58: (iv_rulePrimitiveDataType= rulePrimitiveDataType EOF )
            // InternalABSReport.g:1194:2: iv_rulePrimitiveDataType= rulePrimitiveDataType EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimitiveDataType=rulePrimitiveDataType();

            state._fsp--;

             current =iv_rulePrimitiveDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveDataType"


    // $ANTLR start "rulePrimitiveDataType"
    // InternalABSReport.g:1200:1: rulePrimitiveDataType returns [EObject current=null] : (this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl | this_EnumDataType_1= ruleEnumDataType ) ;
    public final EObject rulePrimitiveDataType() throws RecognitionException {
        EObject current = null;

        EObject this_PrimitiveDataType_Impl_0 = null;

        EObject this_EnumDataType_1 = null;



        	enterRule();

        try {
            // InternalABSReport.g:1206:2: ( (this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl | this_EnumDataType_1= ruleEnumDataType ) )
            // InternalABSReport.g:1207:2: (this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl | this_EnumDataType_1= ruleEnumDataType )
            {
            // InternalABSReport.g:1207:2: (this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl | this_EnumDataType_1= ruleEnumDataType )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==47) ) {
                alt25=1;
            }
            else if ( (LA25_0==54) ) {
                alt25=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }
            switch (alt25) {
                case 1 :
                    // InternalABSReport.g:1208:3: this_PrimitiveDataType_Impl_0= rulePrimitiveDataType_Impl
                    {

                    			newCompositeNode(grammarAccess.getPrimitiveDataTypeAccess().getPrimitiveDataType_ImplParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_PrimitiveDataType_Impl_0=rulePrimitiveDataType_Impl();

                    state._fsp--;


                    			current = this_PrimitiveDataType_Impl_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalABSReport.g:1217:3: this_EnumDataType_1= ruleEnumDataType
                    {

                    			newCompositeNode(grammarAccess.getPrimitiveDataTypeAccess().getEnumDataTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_EnumDataType_1=ruleEnumDataType();

                    state._fsp--;


                    			current = this_EnumDataType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveDataType"


    // $ANTLR start "entryRuleDefinitionGroupRule"
    // InternalABSReport.g:1229:1: entryRuleDefinitionGroupRule returns [EObject current=null] : iv_ruleDefinitionGroupRule= ruleDefinitionGroupRule EOF ;
    public final EObject entryRuleDefinitionGroupRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinitionGroupRule = null;


        try {
            // InternalABSReport.g:1229:60: (iv_ruleDefinitionGroupRule= ruleDefinitionGroupRule EOF )
            // InternalABSReport.g:1230:2: iv_ruleDefinitionGroupRule= ruleDefinitionGroupRule EOF
            {
             newCompositeNode(grammarAccess.getDefinitionGroupRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefinitionGroupRule=ruleDefinitionGroupRule();

            state._fsp--;

             current =iv_ruleDefinitionGroupRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinitionGroupRule"


    // $ANTLR start "ruleDefinitionGroupRule"
    // InternalABSReport.g:1236:1: ruleDefinitionGroupRule returns [EObject current=null] : ( () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}' ) ;
    public final EObject ruleDefinitionGroupRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_annotations_13_0 = null;

        EObject lv_definitions_15_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:1242:2: ( ( () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}' ) )
            // InternalABSReport.g:1243:2: ( () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}' )
            {
            // InternalABSReport.g:1243:2: ( () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}' )
            // InternalABSReport.g:1244:3: () otherlv_1= 'DefinitionGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) ) ( (lv_definitions_15_0= ruleDefinition ) )* otherlv_16= '}'
            {
            // InternalABSReport.g:1244:3: ()
            // InternalABSReport.g:1245:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDefinitionGroupRuleAccess().getDefinitionGroupAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,36,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getDefinitionGroupRuleAccess().getDefinitionGroupKeyword_1());
            		
            // InternalABSReport.g:1255:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:1256:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:1256:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:1257:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDefinitionGroupRuleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_31); 

            			newLeafNode(otherlv_3, grammarAccess.getDefinitionGroupRuleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalABSReport.g:1278:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) ) )
            // InternalABSReport.g:1279:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) )
            {
            // InternalABSReport.g:1279:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* ) )
            // InternalABSReport.g:1280:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4());
            				
            // InternalABSReport.g:1283:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )* )
            // InternalABSReport.g:1284:6: ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )*
            {
            // InternalABSReport.g:1284:6: ( ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) ) )*
            loop29:
            do {
                int alt29=3;
                int LA29_0 = input.LA(1);

                if ( LA29_0 == 37 && getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 0) ) {
                    alt29=1;
                }
                else if ( LA29_0 == 38 && getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 1) ) {
                    alt29=2;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalABSReport.g:1285:4: ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) )
            	    {
            	    // InternalABSReport.g:1285:4: ({...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) ) )
            	    // InternalABSReport.g:1286:5: {...}? => ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleDefinitionGroupRule", "getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 0)");
            	    }
            	    // InternalABSReport.g:1286:116: ( ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) ) )
            	    // InternalABSReport.g:1287:6: ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 0);
            	    					
            	    // InternalABSReport.g:1290:9: ({...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' ) )
            	    // InternalABSReport.g:1290:10: {...}? => (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDefinitionGroupRule", "true");
            	    }
            	    // InternalABSReport.g:1290:19: (otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')' )
            	    // InternalABSReport.g:1290:20: otherlv_5= 'tags' otherlv_6= '(' ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_10= ')'
            	    {
            	    otherlv_5=(Token)match(input,37,FOLLOW_19); 

            	    									newLeafNode(otherlv_5, grammarAccess.getDefinitionGroupRuleAccess().getTagsKeyword_4_0_0());
            	    								
            	    otherlv_6=(Token)match(input,27,FOLLOW_20); 

            	    									newLeafNode(otherlv_6, grammarAccess.getDefinitionGroupRuleAccess().getLeftParenthesisKeyword_4_0_1());
            	    								
            	    // InternalABSReport.g:1298:9: ( ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )* )?
            	    int alt27=2;
            	    int LA27_0 = input.LA(1);

            	    if ( (LA27_0==RULE_ID) ) {
            	        alt27=1;
            	    }
            	    switch (alt27) {
            	        case 1 :
            	            // InternalABSReport.g:1299:10: ( ( ruleQualifiedName ) ) (otherlv_8= ',' ( ( ruleQualifiedName ) ) )*
            	            {
            	            // InternalABSReport.g:1299:10: ( ( ruleQualifiedName ) )
            	            // InternalABSReport.g:1300:11: ( ruleQualifiedName )
            	            {
            	            // InternalABSReport.g:1300:11: ( ruleQualifiedName )
            	            // InternalABSReport.g:1301:12: ruleQualifiedName
            	            {

            	            												if (current==null) {
            	            													current = createModelElement(grammarAccess.getDefinitionGroupRuleRule());
            	            												}
            	            											

            	            												newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getTagsTagCrossReference_4_0_2_0_0());
            	            											
            	            pushFollow(FOLLOW_21);
            	            ruleQualifiedName();

            	            state._fsp--;


            	            												afterParserOrEnumRuleCall();
            	            											

            	            }


            	            }

            	            // InternalABSReport.g:1315:10: (otherlv_8= ',' ( ( ruleQualifiedName ) ) )*
            	            loop26:
            	            do {
            	                int alt26=2;
            	                int LA26_0 = input.LA(1);

            	                if ( (LA26_0==26) ) {
            	                    alt26=1;
            	                }


            	                switch (alt26) {
            	            	case 1 :
            	            	    // InternalABSReport.g:1316:11: otherlv_8= ',' ( ( ruleQualifiedName ) )
            	            	    {
            	            	    otherlv_8=(Token)match(input,26,FOLLOW_5); 

            	            	    											newLeafNode(otherlv_8, grammarAccess.getDefinitionGroupRuleAccess().getCommaKeyword_4_0_2_1_0());
            	            	    										
            	            	    // InternalABSReport.g:1320:11: ( ( ruleQualifiedName ) )
            	            	    // InternalABSReport.g:1321:12: ( ruleQualifiedName )
            	            	    {
            	            	    // InternalABSReport.g:1321:12: ( ruleQualifiedName )
            	            	    // InternalABSReport.g:1322:13: ruleQualifiedName
            	            	    {

            	            	    													if (current==null) {
            	            	    														current = createModelElement(grammarAccess.getDefinitionGroupRuleRule());
            	            	    													}
            	            	    												

            	            	    													newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getTagsTagCrossReference_4_0_2_1_1_0());
            	            	    												
            	            	    pushFollow(FOLLOW_21);
            	            	    ruleQualifiedName();

            	            	    state._fsp--;


            	            	    													afterParserOrEnumRuleCall();
            	            	    												

            	            	    }


            	            	    }


            	            	    }
            	            	    break;

            	            	default :
            	            	    break loop26;
            	                }
            	            } while (true);


            	            }
            	            break;

            	    }

            	    otherlv_10=(Token)match(input,28,FOLLOW_31); 

            	    									newLeafNode(otherlv_10, grammarAccess.getDefinitionGroupRuleAccess().getRightParenthesisKeyword_4_0_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalABSReport.g:1348:4: ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) )
            	    {
            	    // InternalABSReport.g:1348:4: ({...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) ) )
            	    // InternalABSReport.g:1349:5: {...}? => ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleDefinitionGroupRule", "getUnorderedGroupHelper().canSelect(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 1)");
            	    }
            	    // InternalABSReport.g:1349:116: ( ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) ) )
            	    // InternalABSReport.g:1350:6: ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4(), 1);
            	    					
            	    // InternalABSReport.g:1353:9: ({...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' ) )
            	    // InternalABSReport.g:1353:10: {...}? => (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDefinitionGroupRule", "true");
            	    }
            	    // InternalABSReport.g:1353:19: (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )
            	    // InternalABSReport.g:1353:20: otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}'
            	    {
            	    otherlv_11=(Token)match(input,38,FOLLOW_6); 

            	    									newLeafNode(otherlv_11, grammarAccess.getDefinitionGroupRuleAccess().getAnnotationsKeyword_4_1_0());
            	    								
            	    otherlv_12=(Token)match(input,12,FOLLOW_32); 

            	    									newLeafNode(otherlv_12, grammarAccess.getDefinitionGroupRuleAccess().getLeftCurlyBracketKeyword_4_1_1());
            	    								
            	    // InternalABSReport.g:1361:9: ( (lv_annotations_13_0= ruleAnnotationEntry ) )*
            	    loop28:
            	    do {
            	        int alt28=2;
            	        int LA28_0 = input.LA(1);

            	        if ( (LA28_0==RULE_STRING||LA28_0==RULE_ID) ) {
            	            alt28=1;
            	        }


            	        switch (alt28) {
            	    	case 1 :
            	    	    // InternalABSReport.g:1362:10: (lv_annotations_13_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalABSReport.g:1362:10: (lv_annotations_13_0= ruleAnnotationEntry )
            	    	    // InternalABSReport.g:1363:11: lv_annotations_13_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getAnnotationsAnnotationEntryParserRuleCall_4_1_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_32);
            	    	    lv_annotations_13_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getDefinitionGroupRuleRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_13_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop28;
            	        }
            	    } while (true);

            	    otherlv_14=(Token)match(input,13,FOLLOW_31); 

            	    									newLeafNode(otherlv_14, grammarAccess.getDefinitionGroupRuleAccess().getRightCurlyBracketKeyword_4_1_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getDefinitionGroupRuleAccess().getUnorderedGroup_4());
            				

            }

            // InternalABSReport.g:1397:3: ( (lv_definitions_15_0= ruleDefinition ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==36||(LA30_0>=40 && LA30_0<=42)||(LA30_0>=46 && LA30_0<=49)||LA30_0==54||(LA30_0>=63 && LA30_0<=65)||LA30_0==68||LA30_0==96) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalABSReport.g:1398:4: (lv_definitions_15_0= ruleDefinition )
            	    {
            	    // InternalABSReport.g:1398:4: (lv_definitions_15_0= ruleDefinition )
            	    // InternalABSReport.g:1399:5: lv_definitions_15_0= ruleDefinition
            	    {

            	    					newCompositeNode(grammarAccess.getDefinitionGroupRuleAccess().getDefinitionsDefinitionParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_33);
            	    lv_definitions_15_0=ruleDefinition();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDefinitionGroupRuleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"definitions",
            	    						lv_definitions_15_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Definition");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            otherlv_16=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_16, grammarAccess.getDefinitionGroupRuleAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinitionGroupRule"


    // $ANTLR start "entryRuleDefinition"
    // InternalABSReport.g:1424:1: entryRuleDefinition returns [EObject current=null] : iv_ruleDefinition= ruleDefinition EOF ;
    public final EObject entryRuleDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinition = null;


        try {
            // InternalABSReport.g:1424:51: (iv_ruleDefinition= ruleDefinition EOF )
            // InternalABSReport.g:1425:2: iv_ruleDefinition= ruleDefinition EOF
            {
             newCompositeNode(grammarAccess.getDefinitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefinition=ruleDefinition();

            state._fsp--;

             current =iv_ruleDefinition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinition"


    // $ANTLR start "ruleDefinition"
    // InternalABSReport.g:1431:1: ruleDefinition returns [EObject current=null] : (this_DefinitionGroupRule_0= ruleDefinitionGroupRule | this_AbstractAssetType_1= ruleAbstractAssetType | this_Contract_2= ruleContract | this_PrimitiveDataType_3= rulePrimitiveDataType | this_StaticMethod_4= ruleStaticMethod | this_Tag_5= ruleTag | this_AnnotationKey_6= ruleAnnotationKey | this_GuardedAction_7= ruleGuardedAction | this_Requirement_8= ruleRequirement ) ;
    public final EObject ruleDefinition() throws RecognitionException {
        EObject current = null;

        EObject this_DefinitionGroupRule_0 = null;

        EObject this_AbstractAssetType_1 = null;

        EObject this_Contract_2 = null;

        EObject this_PrimitiveDataType_3 = null;

        EObject this_StaticMethod_4 = null;

        EObject this_Tag_5 = null;

        EObject this_AnnotationKey_6 = null;

        EObject this_GuardedAction_7 = null;

        EObject this_Requirement_8 = null;



        	enterRule();

        try {
            // InternalABSReport.g:1437:2: ( (this_DefinitionGroupRule_0= ruleDefinitionGroupRule | this_AbstractAssetType_1= ruleAbstractAssetType | this_Contract_2= ruleContract | this_PrimitiveDataType_3= rulePrimitiveDataType | this_StaticMethod_4= ruleStaticMethod | this_Tag_5= ruleTag | this_AnnotationKey_6= ruleAnnotationKey | this_GuardedAction_7= ruleGuardedAction | this_Requirement_8= ruleRequirement ) )
            // InternalABSReport.g:1438:2: (this_DefinitionGroupRule_0= ruleDefinitionGroupRule | this_AbstractAssetType_1= ruleAbstractAssetType | this_Contract_2= ruleContract | this_PrimitiveDataType_3= rulePrimitiveDataType | this_StaticMethod_4= ruleStaticMethod | this_Tag_5= ruleTag | this_AnnotationKey_6= ruleAnnotationKey | this_GuardedAction_7= ruleGuardedAction | this_Requirement_8= ruleRequirement )
            {
            // InternalABSReport.g:1438:2: (this_DefinitionGroupRule_0= ruleDefinitionGroupRule | this_AbstractAssetType_1= ruleAbstractAssetType | this_Contract_2= ruleContract | this_PrimitiveDataType_3= rulePrimitiveDataType | this_StaticMethod_4= ruleStaticMethod | this_Tag_5= ruleTag | this_AnnotationKey_6= ruleAnnotationKey | this_GuardedAction_7= ruleGuardedAction | this_Requirement_8= ruleRequirement )
            int alt31=9;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt31=1;
                }
                break;
            case 41:
            case 42:
            case 46:
                {
                alt31=2;
                }
                break;
            case 63:
            case 64:
            case 65:
                {
                alt31=3;
                }
                break;
            case 47:
            case 54:
                {
                alt31=4;
                }
                break;
            case 48:
                {
                alt31=5;
                }
                break;
            case 49:
                {
                alt31=6;
                }
                break;
            case 40:
                {
                alt31=7;
                }
                break;
            case 68:
                {
                alt31=8;
                }
                break;
            case 96:
                {
                alt31=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // InternalABSReport.g:1439:3: this_DefinitionGroupRule_0= ruleDefinitionGroupRule
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getDefinitionGroupRuleParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_DefinitionGroupRule_0=ruleDefinitionGroupRule();

                    state._fsp--;


                    			current = this_DefinitionGroupRule_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalABSReport.g:1448:3: this_AbstractAssetType_1= ruleAbstractAssetType
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getAbstractAssetTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AbstractAssetType_1=ruleAbstractAssetType();

                    state._fsp--;


                    			current = this_AbstractAssetType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalABSReport.g:1457:3: this_Contract_2= ruleContract
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getContractParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Contract_2=ruleContract();

                    state._fsp--;


                    			current = this_Contract_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalABSReport.g:1466:3: this_PrimitiveDataType_3= rulePrimitiveDataType
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getPrimitiveDataTypeParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_PrimitiveDataType_3=rulePrimitiveDataType();

                    state._fsp--;


                    			current = this_PrimitiveDataType_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalABSReport.g:1475:3: this_StaticMethod_4= ruleStaticMethod
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getStaticMethodParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_StaticMethod_4=ruleStaticMethod();

                    state._fsp--;


                    			current = this_StaticMethod_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalABSReport.g:1484:3: this_Tag_5= ruleTag
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getTagParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Tag_5=ruleTag();

                    state._fsp--;


                    			current = this_Tag_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalABSReport.g:1493:3: this_AnnotationKey_6= ruleAnnotationKey
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getAnnotationKeyParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_AnnotationKey_6=ruleAnnotationKey();

                    state._fsp--;


                    			current = this_AnnotationKey_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalABSReport.g:1502:3: this_GuardedAction_7= ruleGuardedAction
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getGuardedActionParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_GuardedAction_7=ruleGuardedAction();

                    state._fsp--;


                    			current = this_GuardedAction_7;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 9 :
                    // InternalABSReport.g:1511:3: this_Requirement_8= ruleRequirement
                    {

                    			newCompositeNode(grammarAccess.getDefinitionAccess().getRequirementParserRuleCall_8());
                    		
                    pushFollow(FOLLOW_2);
                    this_Requirement_8=ruleRequirement();

                    state._fsp--;


                    			current = this_Requirement_8;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinition"


    // $ANTLR start "entryRuleAssetGroup"
    // InternalABSReport.g:1523:1: entryRuleAssetGroup returns [EObject current=null] : iv_ruleAssetGroup= ruleAssetGroup EOF ;
    public final EObject entryRuleAssetGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetGroup = null;


        try {
            // InternalABSReport.g:1523:51: (iv_ruleAssetGroup= ruleAssetGroup EOF )
            // InternalABSReport.g:1524:2: iv_ruleAssetGroup= ruleAssetGroup EOF
            {
             newCompositeNode(grammarAccess.getAssetGroupRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetGroup=ruleAssetGroup();

            state._fsp--;

             current =iv_ruleAssetGroup; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetGroup"


    // $ANTLR start "ruleAssetGroup"
    // InternalABSReport.g:1530:1: ruleAssetGroup returns [EObject current=null] : ( () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}' ) ;
    public final EObject ruleAssetGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_annotations_6_0 = null;

        EObject lv_elements_8_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:1536:2: ( ( () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}' ) )
            // InternalABSReport.g:1537:2: ( () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}' )
            {
            // InternalABSReport.g:1537:2: ( () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}' )
            // InternalABSReport.g:1538:3: () otherlv_1= 'AssetGroup' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_elements_8_0= ruleAssetGroupContent ) )* otherlv_9= '}'
            {
            // InternalABSReport.g:1538:3: ()
            // InternalABSReport.g:1539:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetGroupAccess().getAssetGroupAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,39,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetGroupAccess().getAssetGroupKeyword_1());
            		
            // InternalABSReport.g:1549:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:1550:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:1550:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:1551:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetGroupAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetGroupRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_34); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetGroupAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalABSReport.g:1572:3: (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==38) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalABSReport.g:1573:4: otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}'
                    {
                    otherlv_4=(Token)match(input,38,FOLLOW_6); 

                    				newLeafNode(otherlv_4, grammarAccess.getAssetGroupAccess().getAnnotationsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,12,FOLLOW_32); 

                    				newLeafNode(otherlv_5, grammarAccess.getAssetGroupAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalABSReport.g:1581:4: ( (lv_annotations_6_0= ruleAnnotationEntry ) )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==RULE_STRING||LA32_0==RULE_ID) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalABSReport.g:1582:5: (lv_annotations_6_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalABSReport.g:1582:5: (lv_annotations_6_0= ruleAnnotationEntry )
                    	    // InternalABSReport.g:1583:6: lv_annotations_6_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getAssetGroupAccess().getAnnotationsAnnotationEntryParserRuleCall_4_2_0());
                    	    					
                    	    pushFollow(FOLLOW_32);
                    	    lv_annotations_6_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getAssetGroupRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_6_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,13,FOLLOW_35); 

                    				newLeafNode(otherlv_7, grammarAccess.getAssetGroupAccess().getRightCurlyBracketKeyword_4_3());
                    			

                    }
                    break;

            }

            // InternalABSReport.g:1605:3: ( (lv_elements_8_0= ruleAssetGroupContent ) )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==11||LA34_0==39||(LA34_0>=55 && LA34_0<=56)) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // InternalABSReport.g:1606:4: (lv_elements_8_0= ruleAssetGroupContent )
            	    {
            	    // InternalABSReport.g:1606:4: (lv_elements_8_0= ruleAssetGroupContent )
            	    // InternalABSReport.g:1607:5: lv_elements_8_0= ruleAssetGroupContent
            	    {

            	    					newCompositeNode(grammarAccess.getAssetGroupAccess().getElementsAssetGroupContentParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_35);
            	    lv_elements_8_0=ruleAssetGroupContent();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAssetGroupRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_8_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetGroupContent");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

            otherlv_9=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getAssetGroupAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetGroup"


    // $ANTLR start "entryRuleAssetGroupContent"
    // InternalABSReport.g:1632:1: entryRuleAssetGroupContent returns [EObject current=null] : iv_ruleAssetGroupContent= ruleAssetGroupContent EOF ;
    public final EObject entryRuleAssetGroupContent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetGroupContent = null;


        try {
            // InternalABSReport.g:1632:58: (iv_ruleAssetGroupContent= ruleAssetGroupContent EOF )
            // InternalABSReport.g:1633:2: iv_ruleAssetGroupContent= ruleAssetGroupContent EOF
            {
             newCompositeNode(grammarAccess.getAssetGroupContentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetGroupContent=ruleAssetGroupContent();

            state._fsp--;

             current =iv_ruleAssetGroupContent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetGroupContent"


    // $ANTLR start "ruleAssetGroupContent"
    // InternalABSReport.g:1639:1: ruleAssetGroupContent returns [EObject current=null] : (this_AssetGroup_0= ruleAssetGroup | this_Asset_1= ruleAsset | this_AssetLink_2= ruleAssetLink | this_Goal_3= ruleGoal ) ;
    public final EObject ruleAssetGroupContent() throws RecognitionException {
        EObject current = null;

        EObject this_AssetGroup_0 = null;

        EObject this_Asset_1 = null;

        EObject this_AssetLink_2 = null;

        EObject this_Goal_3 = null;



        	enterRule();

        try {
            // InternalABSReport.g:1645:2: ( (this_AssetGroup_0= ruleAssetGroup | this_Asset_1= ruleAsset | this_AssetLink_2= ruleAssetLink | this_Goal_3= ruleGoal ) )
            // InternalABSReport.g:1646:2: (this_AssetGroup_0= ruleAssetGroup | this_Asset_1= ruleAsset | this_AssetLink_2= ruleAssetLink | this_Goal_3= ruleGoal )
            {
            // InternalABSReport.g:1646:2: (this_AssetGroup_0= ruleAssetGroup | this_Asset_1= ruleAsset | this_AssetLink_2= ruleAssetLink | this_Goal_3= ruleGoal )
            int alt35=4;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt35=1;
                }
                break;
            case 55:
                {
                alt35=2;
                }
                break;
            case 56:
                {
                alt35=3;
                }
                break;
            case 11:
                {
                alt35=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }

            switch (alt35) {
                case 1 :
                    // InternalABSReport.g:1647:3: this_AssetGroup_0= ruleAssetGroup
                    {

                    			newCompositeNode(grammarAccess.getAssetGroupContentAccess().getAssetGroupParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AssetGroup_0=ruleAssetGroup();

                    state._fsp--;


                    			current = this_AssetGroup_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalABSReport.g:1656:3: this_Asset_1= ruleAsset
                    {

                    			newCompositeNode(grammarAccess.getAssetGroupContentAccess().getAssetParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Asset_1=ruleAsset();

                    state._fsp--;


                    			current = this_Asset_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalABSReport.g:1665:3: this_AssetLink_2= ruleAssetLink
                    {

                    			newCompositeNode(grammarAccess.getAssetGroupContentAccess().getAssetLinkParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_AssetLink_2=ruleAssetLink();

                    state._fsp--;


                    			current = this_AssetLink_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalABSReport.g:1674:3: this_Goal_3= ruleGoal
                    {

                    			newCompositeNode(grammarAccess.getAssetGroupContentAccess().getGoalParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Goal_3=ruleGoal();

                    state._fsp--;


                    			current = this_Goal_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetGroupContent"


    // $ANTLR start "entryRuleAnnotationKey"
    // InternalABSReport.g:1686:1: entryRuleAnnotationKey returns [EObject current=null] : iv_ruleAnnotationKey= ruleAnnotationKey EOF ;
    public final EObject entryRuleAnnotationKey() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotationKey = null;


        try {
            // InternalABSReport.g:1686:54: (iv_ruleAnnotationKey= ruleAnnotationKey EOF )
            // InternalABSReport.g:1687:2: iv_ruleAnnotationKey= ruleAnnotationKey EOF
            {
             newCompositeNode(grammarAccess.getAnnotationKeyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnnotationKey=ruleAnnotationKey();

            state._fsp--;

             current =iv_ruleAnnotationKey; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotationKey"


    // $ANTLR start "ruleAnnotationKey"
    // InternalABSReport.g:1693:1: ruleAnnotationKey returns [EObject current=null] : ( () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleAnnotationKey() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:1699:2: ( ( () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalABSReport.g:1700:2: ( () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalABSReport.g:1700:2: ( () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) ) )
            // InternalABSReport.g:1701:3: () otherlv_1= 'annotation' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalABSReport.g:1701:3: ()
            // InternalABSReport.g:1702:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAnnotationKeyAccess().getAnnotationKeyAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,40,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getAnnotationKeyAccess().getAnnotationKeyword_1());
            		
            // InternalABSReport.g:1712:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:1713:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:1713:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:1714:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAnnotationKeyAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAnnotationKeyRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotationKey"


    // $ANTLR start "entryRuleAssetType"
    // InternalABSReport.g:1735:1: entryRuleAssetType returns [EObject current=null] : iv_ruleAssetType= ruleAssetType EOF ;
    public final EObject entryRuleAssetType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetType = null;


        try {
            // InternalABSReport.g:1735:50: (iv_ruleAssetType= ruleAssetType EOF )
            // InternalABSReport.g:1736:2: iv_ruleAssetType= ruleAssetType EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetType=ruleAssetType();

            state._fsp--;

             current =iv_ruleAssetType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetType"


    // $ANTLR start "ruleAssetType"
    // InternalABSReport.g:1742:1: ruleAssetType returns [EObject current=null] : ( () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}' ) ;
    public final EObject ruleAssetType() throws RecognitionException {
        EObject current = null;

        Token lv_abstract_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token lv_description_22_0=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_29=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;

        AntlrDatatypeRuleToken lv_level_13_0 = null;

        Enumerator lv_descriptionFormat_21_0 = null;

        EObject lv_annotations_25_0 = null;

        EObject lv_assetTypeProperties_27_0 = null;

        EObject lv_assetTypeAttributes_28_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:1748:2: ( ( () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}' ) )
            // InternalABSReport.g:1749:2: ( () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}' )
            {
            // InternalABSReport.g:1749:2: ( () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}' )
            // InternalABSReport.g:1750:3: () ( (lv_abstract_1_0= 'abstract' ) )? otherlv_2= 'AssetType' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )? otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) ) ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )* otherlv_29= '}'
            {
            // InternalABSReport.g:1750:3: ()
            // InternalABSReport.g:1751:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeAccess().getAssetTypeAction_0(),
            					current);
            			

            }

            // InternalABSReport.g:1757:3: ( (lv_abstract_1_0= 'abstract' ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==41) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalABSReport.g:1758:4: (lv_abstract_1_0= 'abstract' )
                    {
                    // InternalABSReport.g:1758:4: (lv_abstract_1_0= 'abstract' )
                    // InternalABSReport.g:1759:5: lv_abstract_1_0= 'abstract'
                    {
                    lv_abstract_1_0=(Token)match(input,41,FOLLOW_36); 

                    					newLeafNode(lv_abstract_1_0, grammarAccess.getAssetTypeAccess().getAbstractAbstractKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAssetTypeRule());
                    					}
                    					setWithLastConsumed(current, "abstract", lv_abstract_1_0 != null, "abstract");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,42,FOLLOW_30); 

            			newLeafNode(otherlv_2, grammarAccess.getAssetTypeAccess().getAssetTypeKeyword_2());
            		
            // InternalABSReport.g:1775:3: ( (lv_name_3_0= ruleEString ) )
            // InternalABSReport.g:1776:4: (lv_name_3_0= ruleEString )
            {
            // InternalABSReport.g:1776:4: (lv_name_3_0= ruleEString )
            // InternalABSReport.g:1777:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetTypeAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_37);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalABSReport.g:1794:3: (otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')' )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==43) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalABSReport.g:1795:4: otherlv_4= 'extends' otherlv_5= '(' ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,43,FOLLOW_19); 

                    				newLeafNode(otherlv_4, grammarAccess.getAssetTypeAccess().getExtendsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,27,FOLLOW_20); 

                    				newLeafNode(otherlv_5, grammarAccess.getAssetTypeAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalABSReport.g:1803:4: ( ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )* )?
                    int alt38=2;
                    int LA38_0 = input.LA(1);

                    if ( (LA38_0==RULE_ID) ) {
                        alt38=1;
                    }
                    switch (alt38) {
                        case 1 :
                            // InternalABSReport.g:1804:5: ( ( ruleQualifiedName ) ) (otherlv_7= ',' ( ( ruleQualifiedName ) ) )*
                            {
                            // InternalABSReport.g:1804:5: ( ( ruleQualifiedName ) )
                            // InternalABSReport.g:1805:6: ( ruleQualifiedName )
                            {
                            // InternalABSReport.g:1805:6: ( ruleQualifiedName )
                            // InternalABSReport.g:1806:7: ruleQualifiedName
                            {

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAssetTypeRule());
                            							}
                            						

                            							newCompositeNode(grammarAccess.getAssetTypeAccess().getExtendsAssetTypeCrossReference_4_2_0_0());
                            						
                            pushFollow(FOLLOW_21);
                            ruleQualifiedName();

                            state._fsp--;


                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            // InternalABSReport.g:1820:5: (otherlv_7= ',' ( ( ruleQualifiedName ) ) )*
                            loop37:
                            do {
                                int alt37=2;
                                int LA37_0 = input.LA(1);

                                if ( (LA37_0==26) ) {
                                    alt37=1;
                                }


                                switch (alt37) {
                            	case 1 :
                            	    // InternalABSReport.g:1821:6: otherlv_7= ',' ( ( ruleQualifiedName ) )
                            	    {
                            	    otherlv_7=(Token)match(input,26,FOLLOW_5); 

                            	    						newLeafNode(otherlv_7, grammarAccess.getAssetTypeAccess().getCommaKeyword_4_2_1_0());
                            	    					
                            	    // InternalABSReport.g:1825:6: ( ( ruleQualifiedName ) )
                            	    // InternalABSReport.g:1826:7: ( ruleQualifiedName )
                            	    {
                            	    // InternalABSReport.g:1826:7: ( ruleQualifiedName )
                            	    // InternalABSReport.g:1827:8: ruleQualifiedName
                            	    {

                            	    								if (current==null) {
                            	    									current = createModelElement(grammarAccess.getAssetTypeRule());
                            	    								}
                            	    							

                            	    								newCompositeNode(grammarAccess.getAssetTypeAccess().getExtendsAssetTypeCrossReference_4_2_1_1_0());
                            	    							
                            	    pushFollow(FOLLOW_21);
                            	    ruleQualifiedName();

                            	    state._fsp--;


                            	    								afterParserOrEnumRuleCall();
                            	    							

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop37;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_9=(Token)match(input,28,FOLLOW_6); 

                    				newLeafNode(otherlv_9, grammarAccess.getAssetTypeAccess().getRightParenthesisKeyword_4_3());
                    			

                    }
                    break;

            }

            otherlv_10=(Token)match(input,12,FOLLOW_38); 

            			newLeafNode(otherlv_10, grammarAccess.getAssetTypeAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalABSReport.g:1852:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) ) )
            // InternalABSReport.g:1853:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) )
            {
            // InternalABSReport.g:1853:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* ) )
            // InternalABSReport.g:1854:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            				
            // InternalABSReport.g:1857:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )* )
            // InternalABSReport.g:1858:6: ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )*
            {
            // InternalABSReport.g:1858:6: ( ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) ) )*
            loop44:
            do {
                int alt44=5;
                int LA44_0 = input.LA(1);

                if ( LA44_0 == 44 && getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 0) ) {
                    alt44=1;
                }
                else if ( LA44_0 == 37 && getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 1) ) {
                    alt44=2;
                }
                else if ( LA44_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 2) ) {
                    alt44=3;
                }
                else if ( LA44_0 == 38 && getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 3) ) {
                    alt44=4;
                }


                switch (alt44) {
            	case 1 :
            	    // InternalABSReport.g:1859:4: ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:1859:4: ({...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) ) )
            	    // InternalABSReport.g:1860:5: {...}? => ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 0)");
            	    }
            	    // InternalABSReport.g:1860:106: ( ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) ) )
            	    // InternalABSReport.g:1861:6: ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 0);
            	    					
            	    // InternalABSReport.g:1864:9: ({...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) ) )
            	    // InternalABSReport.g:1864:10: {...}? => (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "true");
            	    }
            	    // InternalABSReport.g:1864:19: (otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) ) )
            	    // InternalABSReport.g:1864:20: otherlv_12= 'level' ( (lv_level_13_0= ruleEString ) )
            	    {
            	    otherlv_12=(Token)match(input,44,FOLLOW_30); 

            	    									newLeafNode(otherlv_12, grammarAccess.getAssetTypeAccess().getLevelKeyword_6_0_0());
            	    								
            	    // InternalABSReport.g:1868:9: ( (lv_level_13_0= ruleEString ) )
            	    // InternalABSReport.g:1869:10: (lv_level_13_0= ruleEString )
            	    {
            	    // InternalABSReport.g:1869:10: (lv_level_13_0= ruleEString )
            	    // InternalABSReport.g:1870:11: lv_level_13_0= ruleEString
            	    {

            	    											newCompositeNode(grammarAccess.getAssetTypeAccess().getLevelEStringParserRuleCall_6_0_1_0());
            	    										
            	    pushFollow(FOLLOW_38);
            	    lv_level_13_0=ruleEString();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	    											}
            	    											set(
            	    												current,
            	    												"level",
            	    												lv_level_13_0,
            	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalABSReport.g:1893:4: ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) )
            	    {
            	    // InternalABSReport.g:1893:4: ({...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) ) )
            	    // InternalABSReport.g:1894:5: {...}? => ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 1)");
            	    }
            	    // InternalABSReport.g:1894:106: ( ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) ) )
            	    // InternalABSReport.g:1895:6: ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 1);
            	    					
            	    // InternalABSReport.g:1898:9: ({...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' ) )
            	    // InternalABSReport.g:1898:10: {...}? => (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "true");
            	    }
            	    // InternalABSReport.g:1898:19: (otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')' )
            	    // InternalABSReport.g:1898:20: otherlv_14= 'tags' otherlv_15= '(' ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )? otherlv_19= ')'
            	    {
            	    otherlv_14=(Token)match(input,37,FOLLOW_19); 

            	    									newLeafNode(otherlv_14, grammarAccess.getAssetTypeAccess().getTagsKeyword_6_1_0());
            	    								
            	    otherlv_15=(Token)match(input,27,FOLLOW_20); 

            	    									newLeafNode(otherlv_15, grammarAccess.getAssetTypeAccess().getLeftParenthesisKeyword_6_1_1());
            	    								
            	    // InternalABSReport.g:1906:9: ( ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )* )?
            	    int alt41=2;
            	    int LA41_0 = input.LA(1);

            	    if ( (LA41_0==RULE_ID) ) {
            	        alt41=1;
            	    }
            	    switch (alt41) {
            	        case 1 :
            	            // InternalABSReport.g:1907:10: ( ( ruleQualifiedName ) ) (otherlv_17= ',' ( ( ruleQualifiedName ) ) )*
            	            {
            	            // InternalABSReport.g:1907:10: ( ( ruleQualifiedName ) )
            	            // InternalABSReport.g:1908:11: ( ruleQualifiedName )
            	            {
            	            // InternalABSReport.g:1908:11: ( ruleQualifiedName )
            	            // InternalABSReport.g:1909:12: ruleQualifiedName
            	            {

            	            												if (current==null) {
            	            													current = createModelElement(grammarAccess.getAssetTypeRule());
            	            												}
            	            											

            	            												newCompositeNode(grammarAccess.getAssetTypeAccess().getTagsTagCrossReference_6_1_2_0_0());
            	            											
            	            pushFollow(FOLLOW_21);
            	            ruleQualifiedName();

            	            state._fsp--;


            	            												afterParserOrEnumRuleCall();
            	            											

            	            }


            	            }

            	            // InternalABSReport.g:1923:10: (otherlv_17= ',' ( ( ruleQualifiedName ) ) )*
            	            loop40:
            	            do {
            	                int alt40=2;
            	                int LA40_0 = input.LA(1);

            	                if ( (LA40_0==26) ) {
            	                    alt40=1;
            	                }


            	                switch (alt40) {
            	            	case 1 :
            	            	    // InternalABSReport.g:1924:11: otherlv_17= ',' ( ( ruleQualifiedName ) )
            	            	    {
            	            	    otherlv_17=(Token)match(input,26,FOLLOW_5); 

            	            	    											newLeafNode(otherlv_17, grammarAccess.getAssetTypeAccess().getCommaKeyword_6_1_2_1_0());
            	            	    										
            	            	    // InternalABSReport.g:1928:11: ( ( ruleQualifiedName ) )
            	            	    // InternalABSReport.g:1929:12: ( ruleQualifiedName )
            	            	    {
            	            	    // InternalABSReport.g:1929:12: ( ruleQualifiedName )
            	            	    // InternalABSReport.g:1930:13: ruleQualifiedName
            	            	    {

            	            	    													if (current==null) {
            	            	    														current = createModelElement(grammarAccess.getAssetTypeRule());
            	            	    													}
            	            	    												

            	            	    													newCompositeNode(grammarAccess.getAssetTypeAccess().getTagsTagCrossReference_6_1_2_1_1_0());
            	            	    												
            	            	    pushFollow(FOLLOW_21);
            	            	    ruleQualifiedName();

            	            	    state._fsp--;


            	            	    													afterParserOrEnumRuleCall();
            	            	    												

            	            	    }


            	            	    }


            	            	    }
            	            	    break;

            	            	default :
            	            	    break loop40;
            	                }
            	            } while (true);


            	            }
            	            break;

            	    }

            	    otherlv_19=(Token)match(input,28,FOLLOW_38); 

            	    									newLeafNode(otherlv_19, grammarAccess.getAssetTypeAccess().getRightParenthesisKeyword_6_1_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalABSReport.g:1956:4: ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:1956:4: ({...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) ) )
            	    // InternalABSReport.g:1957:5: {...}? => ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 2)");
            	    }
            	    // InternalABSReport.g:1957:106: ( ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) ) )
            	    // InternalABSReport.g:1958:6: ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 2);
            	    					
            	    // InternalABSReport.g:1961:9: ({...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) ) )
            	    // InternalABSReport.g:1961:10: {...}? => (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "true");
            	    }
            	    // InternalABSReport.g:1961:19: (otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) ) )
            	    // InternalABSReport.g:1961:20: otherlv_20= 'description' ( (lv_descriptionFormat_21_0= ruleTextFormat ) )? ( (lv_description_22_0= RULE_STRING ) )
            	    {
            	    otherlv_20=(Token)match(input,45,FOLLOW_39); 

            	    									newLeafNode(otherlv_20, grammarAccess.getAssetTypeAccess().getDescriptionKeyword_6_2_0());
            	    								
            	    // InternalABSReport.g:1965:9: ( (lv_descriptionFormat_21_0= ruleTextFormat ) )?
            	    int alt42=2;
            	    int LA42_0 = input.LA(1);

            	    if ( ((LA42_0>=112 && LA42_0<=114)) ) {
            	        alt42=1;
            	    }
            	    switch (alt42) {
            	        case 1 :
            	            // InternalABSReport.g:1966:10: (lv_descriptionFormat_21_0= ruleTextFormat )
            	            {
            	            // InternalABSReport.g:1966:10: (lv_descriptionFormat_21_0= ruleTextFormat )
            	            // InternalABSReport.g:1967:11: lv_descriptionFormat_21_0= ruleTextFormat
            	            {

            	            											newCompositeNode(grammarAccess.getAssetTypeAccess().getDescriptionFormatTextFormatEnumRuleCall_6_2_1_0());
            	            										
            	            pushFollow(FOLLOW_16);
            	            lv_descriptionFormat_21_0=ruleTextFormat();

            	            state._fsp--;


            	            											if (current==null) {
            	            												current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	            											}
            	            											set(
            	            												current,
            	            												"descriptionFormat",
            	            												lv_descriptionFormat_21_0,
            	            												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
            	            											afterParserOrEnumRuleCall();
            	            										

            	            }


            	            }
            	            break;

            	    }

            	    // InternalABSReport.g:1984:9: ( (lv_description_22_0= RULE_STRING ) )
            	    // InternalABSReport.g:1985:10: (lv_description_22_0= RULE_STRING )
            	    {
            	    // InternalABSReport.g:1985:10: (lv_description_22_0= RULE_STRING )
            	    // InternalABSReport.g:1986:11: lv_description_22_0= RULE_STRING
            	    {
            	    lv_description_22_0=(Token)match(input,RULE_STRING,FOLLOW_38); 

            	    											newLeafNode(lv_description_22_0, grammarAccess.getAssetTypeAccess().getDescriptionSTRINGTerminalRuleCall_6_2_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getAssetTypeRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"description",
            	    												lv_description_22_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalABSReport.g:2008:4: ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) )
            	    {
            	    // InternalABSReport.g:2008:4: ({...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) ) )
            	    // InternalABSReport.g:2009:5: {...}? => ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "getUnorderedGroupHelper().canSelect(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 3)");
            	    }
            	    // InternalABSReport.g:2009:106: ( ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) ) )
            	    // InternalABSReport.g:2010:6: ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6(), 3);
            	    					
            	    // InternalABSReport.g:2013:9: ({...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' ) )
            	    // InternalABSReport.g:2013:10: {...}? => (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleAssetType", "true");
            	    }
            	    // InternalABSReport.g:2013:19: (otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}' )
            	    // InternalABSReport.g:2013:20: otherlv_23= 'annotations' otherlv_24= '{' ( (lv_annotations_25_0= ruleAnnotationEntry ) )* otherlv_26= '}'
            	    {
            	    otherlv_23=(Token)match(input,38,FOLLOW_6); 

            	    									newLeafNode(otherlv_23, grammarAccess.getAssetTypeAccess().getAnnotationsKeyword_6_3_0());
            	    								
            	    otherlv_24=(Token)match(input,12,FOLLOW_32); 

            	    									newLeafNode(otherlv_24, grammarAccess.getAssetTypeAccess().getLeftCurlyBracketKeyword_6_3_1());
            	    								
            	    // InternalABSReport.g:2021:9: ( (lv_annotations_25_0= ruleAnnotationEntry ) )*
            	    loop43:
            	    do {
            	        int alt43=2;
            	        int LA43_0 = input.LA(1);

            	        if ( (LA43_0==RULE_STRING||LA43_0==RULE_ID) ) {
            	            alt43=1;
            	        }


            	        switch (alt43) {
            	    	case 1 :
            	    	    // InternalABSReport.g:2022:10: (lv_annotations_25_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalABSReport.g:2022:10: (lv_annotations_25_0= ruleAnnotationEntry )
            	    	    // InternalABSReport.g:2023:11: lv_annotations_25_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getAssetTypeAccess().getAnnotationsAnnotationEntryParserRuleCall_6_3_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_32);
            	    	    lv_annotations_25_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_25_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop43;
            	        }
            	    } while (true);

            	    otherlv_26=(Token)match(input,13,FOLLOW_38); 

            	    									newLeafNode(otherlv_26, grammarAccess.getAssetTypeAccess().getRightCurlyBracketKeyword_6_3_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop44;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getAssetTypeAccess().getUnorderedGroup_6());
            				

            }

            // InternalABSReport.g:2057:3: ( ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) ) )*
            loop45:
            do {
                int alt45=3;
                int LA45_0 = input.LA(1);

                if ( (LA45_0==34||LA45_0==50) ) {
                    alt45=1;
                }
                else if ( (LA45_0==30) ) {
                    alt45=2;
                }


                switch (alt45) {
            	case 1 :
            	    // InternalABSReport.g:2058:4: ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) )
            	    {
            	    // InternalABSReport.g:2058:4: ( (lv_assetTypeProperties_27_0= ruleAssetTypeReference ) )
            	    // InternalABSReport.g:2059:5: (lv_assetTypeProperties_27_0= ruleAssetTypeReference )
            	    {
            	    // InternalABSReport.g:2059:5: (lv_assetTypeProperties_27_0= ruleAssetTypeReference )
            	    // InternalABSReport.g:2060:6: lv_assetTypeProperties_27_0= ruleAssetTypeReference
            	    {

            	    						newCompositeNode(grammarAccess.getAssetTypeAccess().getAssetTypePropertiesAssetTypeReferenceParserRuleCall_7_0_0());
            	    					
            	    pushFollow(FOLLOW_40);
            	    lv_assetTypeProperties_27_0=ruleAssetTypeReference();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"assetTypeProperties",
            	    							lv_assetTypeProperties_27_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeReference");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalABSReport.g:2078:4: ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) )
            	    {
            	    // InternalABSReport.g:2078:4: ( (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute ) )
            	    // InternalABSReport.g:2079:5: (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute )
            	    {
            	    // InternalABSReport.g:2079:5: (lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute )
            	    // InternalABSReport.g:2080:6: lv_assetTypeAttributes_28_0= ruleAssetTypeAttribute
            	    {

            	    						newCompositeNode(grammarAccess.getAssetTypeAccess().getAssetTypeAttributesAssetTypeAttributeParserRuleCall_7_1_0());
            	    					
            	    pushFollow(FOLLOW_40);
            	    lv_assetTypeAttributes_28_0=ruleAssetTypeAttribute();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetTypeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"assetTypeAttributes",
            	    							lv_assetTypeAttributes_28_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeAttribute");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop45;
                }
            } while (true);

            otherlv_29=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_29, grammarAccess.getAssetTypeAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetType"


    // $ANTLR start "entryRuleAssetTypeAspect"
    // InternalABSReport.g:2106:1: entryRuleAssetTypeAspect returns [EObject current=null] : iv_ruleAssetTypeAspect= ruleAssetTypeAspect EOF ;
    public final EObject entryRuleAssetTypeAspect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeAspect = null;


        try {
            // InternalABSReport.g:2106:56: (iv_ruleAssetTypeAspect= ruleAssetTypeAspect EOF )
            // InternalABSReport.g:2107:2: iv_ruleAssetTypeAspect= ruleAssetTypeAspect EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeAspectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeAspect=ruleAssetTypeAspect();

            state._fsp--;

             current =iv_ruleAssetTypeAspect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeAspect"


    // $ANTLR start "ruleAssetTypeAspect"
    // InternalABSReport.g:2113:1: ruleAssetTypeAspect returns [EObject current=null] : (otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}' ) ;
    public final EObject ruleAssetTypeAspect() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_assetTypeProperties_3_0 = null;

        EObject lv_assetTypeAttributes_4_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2119:2: ( (otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}' ) )
            // InternalABSReport.g:2120:2: (otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}' )
            {
            // InternalABSReport.g:2120:2: (otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}' )
            // InternalABSReport.g:2121:3: otherlv_0= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_2= '{' ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,46,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getAssetTypeAspectAccess().getAssetTypeAspectKeyword_0());
            		
            // InternalABSReport.g:2125:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:2126:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:2126:4: ( ruleQualifiedName )
            // InternalABSReport.g:2127:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeAspectRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeAspectAccess().getBaseAssetTypeAssetTypeCrossReference_1_0());
            				
            pushFollow(FOLLOW_6);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_40); 

            			newLeafNode(otherlv_2, grammarAccess.getAssetTypeAspectAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalABSReport.g:2145:3: ( ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) ) | ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) ) )*
            loop46:
            do {
                int alt46=3;
                int LA46_0 = input.LA(1);

                if ( (LA46_0==34||LA46_0==50) ) {
                    alt46=1;
                }
                else if ( (LA46_0==30) ) {
                    alt46=2;
                }


                switch (alt46) {
            	case 1 :
            	    // InternalABSReport.g:2146:4: ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) )
            	    {
            	    // InternalABSReport.g:2146:4: ( (lv_assetTypeProperties_3_0= ruleAssetTypeReference ) )
            	    // InternalABSReport.g:2147:5: (lv_assetTypeProperties_3_0= ruleAssetTypeReference )
            	    {
            	    // InternalABSReport.g:2147:5: (lv_assetTypeProperties_3_0= ruleAssetTypeReference )
            	    // InternalABSReport.g:2148:6: lv_assetTypeProperties_3_0= ruleAssetTypeReference
            	    {

            	    						newCompositeNode(grammarAccess.getAssetTypeAspectAccess().getAssetTypePropertiesAssetTypeReferenceParserRuleCall_3_0_0());
            	    					
            	    pushFollow(FOLLOW_40);
            	    lv_assetTypeProperties_3_0=ruleAssetTypeReference();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetTypeAspectRule());
            	    						}
            	    						add(
            	    							current,
            	    							"assetTypeProperties",
            	    							lv_assetTypeProperties_3_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeReference");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalABSReport.g:2166:4: ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) )
            	    {
            	    // InternalABSReport.g:2166:4: ( (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute ) )
            	    // InternalABSReport.g:2167:5: (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute )
            	    {
            	    // InternalABSReport.g:2167:5: (lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute )
            	    // InternalABSReport.g:2168:6: lv_assetTypeAttributes_4_0= ruleAssetTypeAttribute
            	    {

            	    						newCompositeNode(grammarAccess.getAssetTypeAspectAccess().getAssetTypeAttributesAssetTypeAttributeParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_40);
            	    lv_assetTypeAttributes_4_0=ruleAssetTypeAttribute();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAssetTypeAspectRule());
            	    						}
            	    						add(
            	    							current,
            	    							"assetTypeAttributes",
            	    							lv_assetTypeAttributes_4_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeAttribute");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop46;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getAssetTypeAspectAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeAspect"


    // $ANTLR start "entryRulePrimitiveDataType_Impl"
    // InternalABSReport.g:2194:1: entryRulePrimitiveDataType_Impl returns [EObject current=null] : iv_rulePrimitiveDataType_Impl= rulePrimitiveDataType_Impl EOF ;
    public final EObject entryRulePrimitiveDataType_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveDataType_Impl = null;


        try {
            // InternalABSReport.g:2194:63: (iv_rulePrimitiveDataType_Impl= rulePrimitiveDataType_Impl EOF )
            // InternalABSReport.g:2195:2: iv_rulePrimitiveDataType_Impl= rulePrimitiveDataType_Impl EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveDataType_ImplRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimitiveDataType_Impl=rulePrimitiveDataType_Impl();

            state._fsp--;

             current =iv_rulePrimitiveDataType_Impl; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveDataType_Impl"


    // $ANTLR start "rulePrimitiveDataType_Impl"
    // InternalABSReport.g:2201:1: rulePrimitiveDataType_Impl returns [EObject current=null] : ( () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject rulePrimitiveDataType_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2207:2: ( ( () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalABSReport.g:2208:2: ( () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalABSReport.g:2208:2: ( () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) ) )
            // InternalABSReport.g:2209:3: () otherlv_1= 'PrimitiveDataType' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalABSReport.g:2209:3: ()
            // InternalABSReport.g:2210:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPrimitiveDataType_ImplAccess().getPrimitiveDataTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,47,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getPrimitiveDataType_ImplAccess().getPrimitiveDataTypeKeyword_1());
            		
            // InternalABSReport.g:2220:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:2221:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:2221:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:2222:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPrimitiveDataType_ImplAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPrimitiveDataType_ImplRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveDataType_Impl"


    // $ANTLR start "entryRuleStaticMethod"
    // InternalABSReport.g:2243:1: entryRuleStaticMethod returns [EObject current=null] : iv_ruleStaticMethod= ruleStaticMethod EOF ;
    public final EObject entryRuleStaticMethod() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStaticMethod = null;


        try {
            // InternalABSReport.g:2243:53: (iv_ruleStaticMethod= ruleStaticMethod EOF )
            // InternalABSReport.g:2244:2: iv_ruleStaticMethod= ruleStaticMethod EOF
            {
             newCompositeNode(grammarAccess.getStaticMethodRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStaticMethod=ruleStaticMethod();

            state._fsp--;

             current =iv_ruleStaticMethod; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStaticMethod"


    // $ANTLR start "ruleStaticMethod"
    // InternalABSReport.g:2250:1: ruleStaticMethod returns [EObject current=null] : ( () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleStaticMethod() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2256:2: ( ( () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalABSReport.g:2257:2: ( () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalABSReport.g:2257:2: ( () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) ) )
            // InternalABSReport.g:2258:3: () otherlv_1= 'StaticMethod' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalABSReport.g:2258:3: ()
            // InternalABSReport.g:2259:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStaticMethodAccess().getStaticMethodAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,48,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getStaticMethodAccess().getStaticMethodKeyword_1());
            		
            // InternalABSReport.g:2269:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:2270:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:2270:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:2271:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStaticMethodAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStaticMethodRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStaticMethod"


    // $ANTLR start "entryRuleLambdaParameter"
    // InternalABSReport.g:2292:1: entryRuleLambdaParameter returns [EObject current=null] : iv_ruleLambdaParameter= ruleLambdaParameter EOF ;
    public final EObject entryRuleLambdaParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLambdaParameter = null;


        try {
            // InternalABSReport.g:2292:56: (iv_ruleLambdaParameter= ruleLambdaParameter EOF )
            // InternalABSReport.g:2293:2: iv_ruleLambdaParameter= ruleLambdaParameter EOF
            {
             newCompositeNode(grammarAccess.getLambdaParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLambdaParameter=ruleLambdaParameter();

            state._fsp--;

             current =iv_ruleLambdaParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLambdaParameter"


    // $ANTLR start "ruleLambdaParameter"
    // InternalABSReport.g:2299:1: ruleLambdaParameter returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ) ;
    public final EObject ruleLambdaParameter() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2305:2: ( ( () ( (lv_name_1_0= ruleEString ) ) ) )
            // InternalABSReport.g:2306:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            {
            // InternalABSReport.g:2306:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            // InternalABSReport.g:2307:3: () ( (lv_name_1_0= ruleEString ) )
            {
            // InternalABSReport.g:2307:3: ()
            // InternalABSReport.g:2308:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLambdaParameterAccess().getLambdaParameterAction_0(),
            					current);
            			

            }

            // InternalABSReport.g:2314:3: ( (lv_name_1_0= ruleEString ) )
            // InternalABSReport.g:2315:4: (lv_name_1_0= ruleEString )
            {
            // InternalABSReport.g:2315:4: (lv_name_1_0= ruleEString )
            // InternalABSReport.g:2316:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getLambdaParameterAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaParameterRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLambdaParameter"


    // $ANTLR start "entryRuleTag"
    // InternalABSReport.g:2337:1: entryRuleTag returns [EObject current=null] : iv_ruleTag= ruleTag EOF ;
    public final EObject entryRuleTag() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTag = null;


        try {
            // InternalABSReport.g:2337:44: (iv_ruleTag= ruleTag EOF )
            // InternalABSReport.g:2338:2: iv_ruleTag= ruleTag EOF
            {
             newCompositeNode(grammarAccess.getTagRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTag=ruleTag();

            state._fsp--;

             current =iv_ruleTag; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTag"


    // $ANTLR start "ruleTag"
    // InternalABSReport.g:2344:1: ruleTag returns [EObject current=null] : ( () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleTag() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2350:2: ( ( () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalABSReport.g:2351:2: ( () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalABSReport.g:2351:2: ( () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) ) )
            // InternalABSReport.g:2352:3: () otherlv_1= 'Tag' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalABSReport.g:2352:3: ()
            // InternalABSReport.g:2353:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTagAccess().getTagAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,49,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getTagAccess().getTagKeyword_1());
            		
            // InternalABSReport.g:2363:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:2364:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:2364:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:2365:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getTagAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTagRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTag"


    // $ANTLR start "entryRuleAssetTypeReference"
    // InternalABSReport.g:2386:1: entryRuleAssetTypeReference returns [EObject current=null] : iv_ruleAssetTypeReference= ruleAssetTypeReference EOF ;
    public final EObject entryRuleAssetTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeReference = null;


        try {
            // InternalABSReport.g:2386:59: (iv_ruleAssetTypeReference= ruleAssetTypeReference EOF )
            // InternalABSReport.g:2387:2: iv_ruleAssetTypeReference= ruleAssetTypeReference EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeReference=ruleAssetTypeReference();

            state._fsp--;

             current =iv_ruleAssetTypeReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeReference"


    // $ANTLR start "ruleAssetTypeReference"
    // InternalABSReport.g:2393:1: ruleAssetTypeReference returns [EObject current=null] : ( ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) ) ) ;
    public final EObject ruleAssetTypeReference() throws RecognitionException {
        EObject current = null;

        Token lv_isContainer_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_hasDefault_8_0=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token lv_hasDefault_13_0=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        Enumerator lv_multiplicity_5_0 = null;

        Enumerator lv_multiplicity_10_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2399:2: ( ( ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) ) ) )
            // InternalABSReport.g:2400:2: ( ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) ) )
            {
            // InternalABSReport.g:2400:2: ( ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) ) )
            // InternalABSReport.g:2401:3: ( (lv_isContainer_0_0= 'container' ) )? otherlv_1= 'reference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) )
            {
            // InternalABSReport.g:2401:3: ( (lv_isContainer_0_0= 'container' ) )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==50) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalABSReport.g:2402:4: (lv_isContainer_0_0= 'container' )
                    {
                    // InternalABSReport.g:2402:4: (lv_isContainer_0_0= 'container' )
                    // InternalABSReport.g:2403:5: lv_isContainer_0_0= 'container'
                    {
                    lv_isContainer_0_0=(Token)match(input,50,FOLLOW_41); 

                    					newLeafNode(lv_isContainer_0_0, grammarAccess.getAssetTypeReferenceAccess().getIsContainerContainerKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                    					}
                    					setWithLastConsumed(current, "isContainer", lv_isContainer_0_0 != null, "container");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,34,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetTypeReferenceAccess().getReferenceKeyword_1());
            		
            // InternalABSReport.g:2419:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:2420:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:2420:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:2421:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetTypeReferenceAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_42);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetTypeReferenceRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,51,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetTypeReferenceAccess().getColonKeyword_3());
            		
            // InternalABSReport.g:2442:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:2443:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:2443:4: ( ruleQualifiedName )
            // InternalABSReport.g:2444:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeReferenceAccess().getPropertyTypeAssetTypeCrossReference_4_0());
            				
            pushFollow(FOLLOW_43);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalABSReport.g:2458:3: ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? ) | ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? ) )
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==EOF||LA53_0==13||LA53_0==30||LA53_0==34||LA53_0==50||(LA53_0>=52 && LA53_0<=53)||(LA53_0>=99 && LA53_0<=100)) ) {
                alt53=1;
            }
            else if ( ((LA53_0>=101 && LA53_0<=102)) ) {
                alt53=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 53, 0, input);

                throw nvae;
            }
            switch (alt53) {
                case 1 :
                    // InternalABSReport.g:2459:4: ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? )
                    {
                    // InternalABSReport.g:2459:4: ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )? )
                    // InternalABSReport.g:2460:5: ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )? ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )?
                    {
                    // InternalABSReport.g:2460:5: ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )?
                    int alt48=2;
                    int LA48_0 = input.LA(1);

                    if ( ((LA48_0>=99 && LA48_0<=100)) ) {
                        alt48=1;
                    }
                    switch (alt48) {
                        case 1 :
                            // InternalABSReport.g:2461:6: (lv_multiplicity_5_0= ruleMultiplicitySingle )
                            {
                            // InternalABSReport.g:2461:6: (lv_multiplicity_5_0= ruleMultiplicitySingle )
                            // InternalABSReport.g:2462:7: lv_multiplicity_5_0= ruleMultiplicitySingle
                            {

                            							newCompositeNode(grammarAccess.getAssetTypeReferenceAccess().getMultiplicityMultiplicitySingleEnumRuleCall_5_0_0_0());
                            						
                            pushFollow(FOLLOW_44);
                            lv_multiplicity_5_0=ruleMultiplicitySingle();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getAssetTypeReferenceRule());
                            							}
                            							set(
                            								current,
                            								"multiplicity",
                            								lv_multiplicity_5_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.MultiplicitySingle");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:2479:5: (otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) ) )?
                    int alt49=2;
                    int LA49_0 = input.LA(1);

                    if ( (LA49_0==52) ) {
                        alt49=1;
                    }
                    switch (alt49) {
                        case 1 :
                            // InternalABSReport.g:2480:6: otherlv_6= 'opposite' ( (otherlv_7= RULE_ID ) )
                            {
                            otherlv_6=(Token)match(input,52,FOLLOW_5); 

                            						newLeafNode(otherlv_6, grammarAccess.getAssetTypeReferenceAccess().getOppositeKeyword_5_0_1_0());
                            					
                            // InternalABSReport.g:2484:6: ( (otherlv_7= RULE_ID ) )
                            // InternalABSReport.g:2485:7: (otherlv_7= RULE_ID )
                            {
                            // InternalABSReport.g:2485:7: (otherlv_7= RULE_ID )
                            // InternalABSReport.g:2486:8: otherlv_7= RULE_ID
                            {

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                            								}
                            							
                            otherlv_7=(Token)match(input,RULE_ID,FOLLOW_45); 

                            								newLeafNode(otherlv_7, grammarAccess.getAssetTypeReferenceAccess().getOppositeTypeReferenceAssetTypeReferenceCrossReference_5_0_1_1_0());
                            							

                            }


                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:2498:5: ( ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined' )?
                    int alt50=2;
                    int LA50_0 = input.LA(1);

                    if ( (LA50_0==53) ) {
                        alt50=1;
                    }
                    switch (alt50) {
                        case 1 :
                            // InternalABSReport.g:2499:6: ( (lv_hasDefault_8_0= 'default' ) ) otherlv_9= 'undefined'
                            {
                            // InternalABSReport.g:2499:6: ( (lv_hasDefault_8_0= 'default' ) )
                            // InternalABSReport.g:2500:7: (lv_hasDefault_8_0= 'default' )
                            {
                            // InternalABSReport.g:2500:7: (lv_hasDefault_8_0= 'default' )
                            // InternalABSReport.g:2501:8: lv_hasDefault_8_0= 'default'
                            {
                            lv_hasDefault_8_0=(Token)match(input,53,FOLLOW_46); 

                            								newLeafNode(lv_hasDefault_8_0, grammarAccess.getAssetTypeReferenceAccess().getHasDefaultDefaultKeyword_5_0_2_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                            								}
                            								setWithLastConsumed(current, "hasDefault", lv_hasDefault_8_0 != null, "default");
                            							

                            }


                            }

                            otherlv_9=(Token)match(input,35,FOLLOW_2); 

                            						newLeafNode(otherlv_9, grammarAccess.getAssetTypeReferenceAccess().getUndefinedKeyword_5_0_2_1());
                            					

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:2520:4: ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? )
                    {
                    // InternalABSReport.g:2520:4: ( ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )? )
                    // InternalABSReport.g:2521:5: ( (lv_multiplicity_10_0= ruleMultiplicityMany ) ) (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )? ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )?
                    {
                    // InternalABSReport.g:2521:5: ( (lv_multiplicity_10_0= ruleMultiplicityMany ) )
                    // InternalABSReport.g:2522:6: (lv_multiplicity_10_0= ruleMultiplicityMany )
                    {
                    // InternalABSReport.g:2522:6: (lv_multiplicity_10_0= ruleMultiplicityMany )
                    // InternalABSReport.g:2523:7: lv_multiplicity_10_0= ruleMultiplicityMany
                    {

                    							newCompositeNode(grammarAccess.getAssetTypeReferenceAccess().getMultiplicityMultiplicityManyEnumRuleCall_5_1_0_0());
                    						
                    pushFollow(FOLLOW_44);
                    lv_multiplicity_10_0=ruleMultiplicityMany();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getAssetTypeReferenceRule());
                    							}
                    							set(
                    								current,
                    								"multiplicity",
                    								lv_multiplicity_10_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.MultiplicityMany");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalABSReport.g:2540:5: (otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) ) )?
                    int alt51=2;
                    int LA51_0 = input.LA(1);

                    if ( (LA51_0==52) ) {
                        alt51=1;
                    }
                    switch (alt51) {
                        case 1 :
                            // InternalABSReport.g:2541:6: otherlv_11= 'opposite' ( (otherlv_12= RULE_ID ) )
                            {
                            otherlv_11=(Token)match(input,52,FOLLOW_5); 

                            						newLeafNode(otherlv_11, grammarAccess.getAssetTypeReferenceAccess().getOppositeKeyword_5_1_1_0());
                            					
                            // InternalABSReport.g:2545:6: ( (otherlv_12= RULE_ID ) )
                            // InternalABSReport.g:2546:7: (otherlv_12= RULE_ID )
                            {
                            // InternalABSReport.g:2546:7: (otherlv_12= RULE_ID )
                            // InternalABSReport.g:2547:8: otherlv_12= RULE_ID
                            {

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                            								}
                            							
                            otherlv_12=(Token)match(input,RULE_ID,FOLLOW_45); 

                            								newLeafNode(otherlv_12, grammarAccess.getAssetTypeReferenceAccess().getOppositeTypeReferenceAssetTypeReferenceCrossReference_5_1_1_1_0());
                            							

                            }


                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:2559:5: ( ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']' )?
                    int alt52=2;
                    int LA52_0 = input.LA(1);

                    if ( (LA52_0==53) ) {
                        alt52=1;
                    }
                    switch (alt52) {
                        case 1 :
                            // InternalABSReport.g:2560:6: ( (lv_hasDefault_13_0= 'default' ) ) otherlv_14= '[' otherlv_15= ']'
                            {
                            // InternalABSReport.g:2560:6: ( (lv_hasDefault_13_0= 'default' ) )
                            // InternalABSReport.g:2561:7: (lv_hasDefault_13_0= 'default' )
                            {
                            // InternalABSReport.g:2561:7: (lv_hasDefault_13_0= 'default' )
                            // InternalABSReport.g:2562:8: lv_hasDefault_13_0= 'default'
                            {
                            lv_hasDefault_13_0=(Token)match(input,53,FOLLOW_47); 

                            								newLeafNode(lv_hasDefault_13_0, grammarAccess.getAssetTypeReferenceAccess().getHasDefaultDefaultKeyword_5_1_2_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeReferenceRule());
                            								}
                            								setWithLastConsumed(current, "hasDefault", lv_hasDefault_13_0 != null, "default");
                            							

                            }


                            }

                            otherlv_14=(Token)match(input,32,FOLLOW_48); 

                            						newLeafNode(otherlv_14, grammarAccess.getAssetTypeReferenceAccess().getLeftSquareBracketKeyword_5_1_2_1());
                            					
                            otherlv_15=(Token)match(input,33,FOLLOW_2); 

                            						newLeafNode(otherlv_15, grammarAccess.getAssetTypeReferenceAccess().getRightSquareBracketKeyword_5_1_2_2());
                            					

                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeReference"


    // $ANTLR start "entryRuleAssetTypeAttribute"
    // InternalABSReport.g:2589:1: entryRuleAssetTypeAttribute returns [EObject current=null] : iv_ruleAssetTypeAttribute= ruleAssetTypeAttribute EOF ;
    public final EObject entryRuleAssetTypeAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeAttribute = null;


        try {
            // InternalABSReport.g:2589:59: (iv_ruleAssetTypeAttribute= ruleAssetTypeAttribute EOF )
            // InternalABSReport.g:2590:2: iv_ruleAssetTypeAttribute= ruleAssetTypeAttribute EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeAttribute=ruleAssetTypeAttribute();

            state._fsp--;

             current =iv_ruleAssetTypeAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeAttribute"


    // $ANTLR start "ruleAssetTypeAttribute"
    // InternalABSReport.g:2596:1: ruleAssetTypeAttribute returns [EObject current=null] : ( () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) ) ) ;
    public final EObject ruleAssetTypeAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_hasDefault_6_0=null;
        Token lv_hasDefault_9_0=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        Enumerator lv_multiplicity_5_0 = null;

        EObject lv_defaultValues_7_0 = null;

        Enumerator lv_multiplicity_8_0 = null;

        EObject lv_defaultValues_11_0 = null;

        EObject lv_defaultValues_13_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2602:2: ( ( () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) ) ) )
            // InternalABSReport.g:2603:2: ( () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) ) )
            {
            // InternalABSReport.g:2603:2: ( () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) ) )
            // InternalABSReport.g:2604:3: () otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) )
            {
            // InternalABSReport.g:2604:3: ()
            // InternalABSReport.g:2605:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeAttributeAccess().getAssetTypeAttributeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,30,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetTypeAttributeAccess().getAttributeKeyword_1());
            		
            // InternalABSReport.g:2615:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:2616:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:2616:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:2617:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_42);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,51,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetTypeAttributeAccess().getColonKeyword_3());
            		
            // InternalABSReport.g:2638:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:2639:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:2639:4: ( ruleQualifiedName )
            // InternalABSReport.g:2640:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeAttributeRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getAttributeTypePrimitiveDataTypeCrossReference_4_0());
            				
            pushFollow(FOLLOW_43);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalABSReport.g:2654:3: ( ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? ) | ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? ) )
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==EOF||LA59_0==13||LA59_0==30||LA59_0==34||LA59_0==50||LA59_0==53||(LA59_0>=99 && LA59_0<=100)) ) {
                alt59=1;
            }
            else if ( ((LA59_0>=101 && LA59_0<=102)) ) {
                alt59=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 59, 0, input);

                throw nvae;
            }
            switch (alt59) {
                case 1 :
                    // InternalABSReport.g:2655:4: ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? )
                    {
                    // InternalABSReport.g:2655:4: ( ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )? )
                    // InternalABSReport.g:2656:5: ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )? ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )?
                    {
                    // InternalABSReport.g:2656:5: ( (lv_multiplicity_5_0= ruleMultiplicitySingle ) )?
                    int alt54=2;
                    int LA54_0 = input.LA(1);

                    if ( ((LA54_0>=99 && LA54_0<=100)) ) {
                        alt54=1;
                    }
                    switch (alt54) {
                        case 1 :
                            // InternalABSReport.g:2657:6: (lv_multiplicity_5_0= ruleMultiplicitySingle )
                            {
                            // InternalABSReport.g:2657:6: (lv_multiplicity_5_0= ruleMultiplicitySingle )
                            // InternalABSReport.g:2658:7: lv_multiplicity_5_0= ruleMultiplicitySingle
                            {

                            							newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getMultiplicityMultiplicitySingleEnumRuleCall_5_0_0_0());
                            						
                            pushFollow(FOLLOW_45);
                            lv_multiplicity_5_0=ruleMultiplicitySingle();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                            							}
                            							set(
                            								current,
                            								"multiplicity",
                            								lv_multiplicity_5_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.MultiplicitySingle");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:2675:5: ( ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) ) )?
                    int alt55=2;
                    int LA55_0 = input.LA(1);

                    if ( (LA55_0==53) ) {
                        alt55=1;
                    }
                    switch (alt55) {
                        case 1 :
                            // InternalABSReport.g:2676:6: ( (lv_hasDefault_6_0= 'default' ) ) ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) )
                            {
                            // InternalABSReport.g:2676:6: ( (lv_hasDefault_6_0= 'default' ) )
                            // InternalABSReport.g:2677:7: (lv_hasDefault_6_0= 'default' )
                            {
                            // InternalABSReport.g:2677:7: (lv_hasDefault_6_0= 'default' )
                            // InternalABSReport.g:2678:8: lv_hasDefault_6_0= 'default'
                            {
                            lv_hasDefault_6_0=(Token)match(input,53,FOLLOW_27); 

                            								newLeafNode(lv_hasDefault_6_0, grammarAccess.getAssetTypeAttributeAccess().getHasDefaultDefaultKeyword_5_0_1_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeAttributeRule());
                            								}
                            								setWithLastConsumed(current, "hasDefault", lv_hasDefault_6_0 != null, "default");
                            							

                            }


                            }

                            // InternalABSReport.g:2690:6: ( (lv_defaultValues_7_0= ruleAttributeConstantExpression ) )
                            // InternalABSReport.g:2691:7: (lv_defaultValues_7_0= ruleAttributeConstantExpression )
                            {
                            // InternalABSReport.g:2691:7: (lv_defaultValues_7_0= ruleAttributeConstantExpression )
                            // InternalABSReport.g:2692:8: lv_defaultValues_7_0= ruleAttributeConstantExpression
                            {

                            								newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getDefaultValuesAttributeConstantExpressionParserRuleCall_5_0_1_1_0());
                            							
                            pushFollow(FOLLOW_2);
                            lv_defaultValues_7_0=ruleAttributeConstantExpression();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                            								}
                            								add(
                            									current,
                            									"defaultValues",
                            									lv_defaultValues_7_0,
                            									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                            								afterParserOrEnumRuleCall();
                            							

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:2712:4: ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? )
                    {
                    // InternalABSReport.g:2712:4: ( ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )? )
                    // InternalABSReport.g:2713:5: ( (lv_multiplicity_8_0= ruleMultiplicityMany ) ) ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )?
                    {
                    // InternalABSReport.g:2713:5: ( (lv_multiplicity_8_0= ruleMultiplicityMany ) )
                    // InternalABSReport.g:2714:6: (lv_multiplicity_8_0= ruleMultiplicityMany )
                    {
                    // InternalABSReport.g:2714:6: (lv_multiplicity_8_0= ruleMultiplicityMany )
                    // InternalABSReport.g:2715:7: lv_multiplicity_8_0= ruleMultiplicityMany
                    {

                    							newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getMultiplicityMultiplicityManyEnumRuleCall_5_1_0_0());
                    						
                    pushFollow(FOLLOW_45);
                    lv_multiplicity_8_0=ruleMultiplicityMany();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                    							}
                    							set(
                    								current,
                    								"multiplicity",
                    								lv_multiplicity_8_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.MultiplicityMany");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalABSReport.g:2732:5: ( ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']' )?
                    int alt58=2;
                    int LA58_0 = input.LA(1);

                    if ( (LA58_0==53) ) {
                        alt58=1;
                    }
                    switch (alt58) {
                        case 1 :
                            // InternalABSReport.g:2733:6: ( (lv_hasDefault_9_0= 'default' ) ) otherlv_10= '[' ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )? (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )* otherlv_14= ']'
                            {
                            // InternalABSReport.g:2733:6: ( (lv_hasDefault_9_0= 'default' ) )
                            // InternalABSReport.g:2734:7: (lv_hasDefault_9_0= 'default' )
                            {
                            // InternalABSReport.g:2734:7: (lv_hasDefault_9_0= 'default' )
                            // InternalABSReport.g:2735:8: lv_hasDefault_9_0= 'default'
                            {
                            lv_hasDefault_9_0=(Token)match(input,53,FOLLOW_47); 

                            								newLeafNode(lv_hasDefault_9_0, grammarAccess.getAssetTypeAttributeAccess().getHasDefaultDefaultKeyword_5_1_1_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getAssetTypeAttributeRule());
                            								}
                            								setWithLastConsumed(current, "hasDefault", lv_hasDefault_9_0 != null, "default");
                            							

                            }


                            }

                            otherlv_10=(Token)match(input,32,FOLLOW_49); 

                            						newLeafNode(otherlv_10, grammarAccess.getAssetTypeAttributeAccess().getLeftSquareBracketKeyword_5_1_1_1());
                            					
                            // InternalABSReport.g:2751:6: ( (lv_defaultValues_11_0= ruleAttributeConstantExpression ) )?
                            int alt56=2;
                            int LA56_0 = input.LA(1);

                            if ( ((LA56_0>=RULE_STRING && LA56_0<=RULE_ID)||LA56_0==35||(LA56_0>=61 && LA56_0<=62)) ) {
                                alt56=1;
                            }
                            switch (alt56) {
                                case 1 :
                                    // InternalABSReport.g:2752:7: (lv_defaultValues_11_0= ruleAttributeConstantExpression )
                                    {
                                    // InternalABSReport.g:2752:7: (lv_defaultValues_11_0= ruleAttributeConstantExpression )
                                    // InternalABSReport.g:2753:8: lv_defaultValues_11_0= ruleAttributeConstantExpression
                                    {

                                    								newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getDefaultValuesAttributeConstantExpressionParserRuleCall_5_1_1_2_0());
                                    							
                                    pushFollow(FOLLOW_28);
                                    lv_defaultValues_11_0=ruleAttributeConstantExpression();

                                    state._fsp--;


                                    								if (current==null) {
                                    									current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                                    								}
                                    								add(
                                    									current,
                                    									"defaultValues",
                                    									lv_defaultValues_11_0,
                                    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                                    								afterParserOrEnumRuleCall();
                                    							

                                    }


                                    }
                                    break;

                            }

                            // InternalABSReport.g:2770:6: (otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) ) )*
                            loop57:
                            do {
                                int alt57=2;
                                int LA57_0 = input.LA(1);

                                if ( (LA57_0==26) ) {
                                    alt57=1;
                                }


                                switch (alt57) {
                            	case 1 :
                            	    // InternalABSReport.g:2771:7: otherlv_12= ',' ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) )
                            	    {
                            	    otherlv_12=(Token)match(input,26,FOLLOW_27); 

                            	    							newLeafNode(otherlv_12, grammarAccess.getAssetTypeAttributeAccess().getCommaKeyword_5_1_1_3_0());
                            	    						
                            	    // InternalABSReport.g:2775:7: ( (lv_defaultValues_13_0= ruleAttributeConstantExpression ) )
                            	    // InternalABSReport.g:2776:8: (lv_defaultValues_13_0= ruleAttributeConstantExpression )
                            	    {
                            	    // InternalABSReport.g:2776:8: (lv_defaultValues_13_0= ruleAttributeConstantExpression )
                            	    // InternalABSReport.g:2777:9: lv_defaultValues_13_0= ruleAttributeConstantExpression
                            	    {

                            	    									newCompositeNode(grammarAccess.getAssetTypeAttributeAccess().getDefaultValuesAttributeConstantExpressionParserRuleCall_5_1_1_3_1_0());
                            	    								
                            	    pushFollow(FOLLOW_28);
                            	    lv_defaultValues_13_0=ruleAttributeConstantExpression();

                            	    state._fsp--;


                            	    									if (current==null) {
                            	    										current = createModelElementForParent(grammarAccess.getAssetTypeAttributeRule());
                            	    									}
                            	    									add(
                            	    										current,
                            	    										"defaultValues",
                            	    										lv_defaultValues_13_0,
                            	    										"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                            	    									afterParserOrEnumRuleCall();
                            	    								

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop57;
                                }
                            } while (true);

                            otherlv_14=(Token)match(input,33,FOLLOW_2); 

                            						newLeafNode(otherlv_14, grammarAccess.getAssetTypeAttributeAccess().getRightSquareBracketKeyword_5_1_1_4());
                            					

                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeAttribute"


    // $ANTLR start "entryRuleEnumDataType"
    // InternalABSReport.g:2806:1: entryRuleEnumDataType returns [EObject current=null] : iv_ruleEnumDataType= ruleEnumDataType EOF ;
    public final EObject entryRuleEnumDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumDataType = null;


        try {
            // InternalABSReport.g:2806:53: (iv_ruleEnumDataType= ruleEnumDataType EOF )
            // InternalABSReport.g:2807:2: iv_ruleEnumDataType= ruleEnumDataType EOF
            {
             newCompositeNode(grammarAccess.getEnumDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEnumDataType=ruleEnumDataType();

            state._fsp--;

             current =iv_ruleEnumDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumDataType"


    // $ANTLR start "ruleEnumDataType"
    // InternalABSReport.g:2813:1: ruleEnumDataType returns [EObject current=null] : ( () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}' ) ;
    public final EObject ruleEnumDataType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_annotations_6_0 = null;

        EObject lv_enumLiteral_8_0 = null;

        EObject lv_enumLiteral_10_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2819:2: ( ( () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}' ) )
            // InternalABSReport.g:2820:2: ( () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}' )
            {
            // InternalABSReport.g:2820:2: ( () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}' )
            // InternalABSReport.g:2821:3: () otherlv_1= 'EnumDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )? ( (lv_enumLiteral_8_0= ruleEnumLiteral ) ) (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )* otherlv_11= '}'
            {
            // InternalABSReport.g:2821:3: ()
            // InternalABSReport.g:2822:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEnumDataTypeAccess().getEnumDataTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,54,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getEnumDataTypeAccess().getEnumDataTypeKeyword_1());
            		
            // InternalABSReport.g:2832:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:2833:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:2833:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:2834:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getEnumDataTypeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEnumDataTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_50); 

            			newLeafNode(otherlv_3, grammarAccess.getEnumDataTypeAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalABSReport.g:2855:3: (otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}' )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==38) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // InternalABSReport.g:2856:4: otherlv_4= 'annotations' otherlv_5= '{' ( (lv_annotations_6_0= ruleAnnotationEntry ) )* otherlv_7= '}'
                    {
                    otherlv_4=(Token)match(input,38,FOLLOW_6); 

                    				newLeafNode(otherlv_4, grammarAccess.getEnumDataTypeAccess().getAnnotationsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,12,FOLLOW_32); 

                    				newLeafNode(otherlv_5, grammarAccess.getEnumDataTypeAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalABSReport.g:2864:4: ( (lv_annotations_6_0= ruleAnnotationEntry ) )*
                    loop60:
                    do {
                        int alt60=2;
                        int LA60_0 = input.LA(1);

                        if ( (LA60_0==RULE_STRING||LA60_0==RULE_ID) ) {
                            alt60=1;
                        }


                        switch (alt60) {
                    	case 1 :
                    	    // InternalABSReport.g:2865:5: (lv_annotations_6_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalABSReport.g:2865:5: (lv_annotations_6_0= ruleAnnotationEntry )
                    	    // InternalABSReport.g:2866:6: lv_annotations_6_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getEnumDataTypeAccess().getAnnotationsAnnotationEntryParserRuleCall_4_2_0());
                    	    					
                    	    pushFollow(FOLLOW_32);
                    	    lv_annotations_6_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getEnumDataTypeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_6_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop60;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,13,FOLLOW_50); 

                    				newLeafNode(otherlv_7, grammarAccess.getEnumDataTypeAccess().getRightCurlyBracketKeyword_4_3());
                    			

                    }
                    break;

            }

            // InternalABSReport.g:2888:3: ( (lv_enumLiteral_8_0= ruleEnumLiteral ) )
            // InternalABSReport.g:2889:4: (lv_enumLiteral_8_0= ruleEnumLiteral )
            {
            // InternalABSReport.g:2889:4: (lv_enumLiteral_8_0= ruleEnumLiteral )
            // InternalABSReport.g:2890:5: lv_enumLiteral_8_0= ruleEnumLiteral
            {

            					newCompositeNode(grammarAccess.getEnumDataTypeAccess().getEnumLiteralEnumLiteralParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_22);
            lv_enumLiteral_8_0=ruleEnumLiteral();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEnumDataTypeRule());
            					}
            					add(
            						current,
            						"enumLiteral",
            						lv_enumLiteral_8_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EnumLiteral");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalABSReport.g:2907:3: (otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) ) )*
            loop62:
            do {
                int alt62=2;
                int LA62_0 = input.LA(1);

                if ( (LA62_0==26) ) {
                    alt62=1;
                }


                switch (alt62) {
            	case 1 :
            	    // InternalABSReport.g:2908:4: otherlv_9= ',' ( (lv_enumLiteral_10_0= ruleEnumLiteral ) )
            	    {
            	    otherlv_9=(Token)match(input,26,FOLLOW_50); 

            	    				newLeafNode(otherlv_9, grammarAccess.getEnumDataTypeAccess().getCommaKeyword_6_0());
            	    			
            	    // InternalABSReport.g:2912:4: ( (lv_enumLiteral_10_0= ruleEnumLiteral ) )
            	    // InternalABSReport.g:2913:5: (lv_enumLiteral_10_0= ruleEnumLiteral )
            	    {
            	    // InternalABSReport.g:2913:5: (lv_enumLiteral_10_0= ruleEnumLiteral )
            	    // InternalABSReport.g:2914:6: lv_enumLiteral_10_0= ruleEnumLiteral
            	    {

            	    						newCompositeNode(grammarAccess.getEnumDataTypeAccess().getEnumLiteralEnumLiteralParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_22);
            	    lv_enumLiteral_10_0=ruleEnumLiteral();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getEnumDataTypeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"enumLiteral",
            	    							lv_enumLiteral_10_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EnumLiteral");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop62;
                }
            } while (true);

            otherlv_11=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getEnumDataTypeAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumDataType"


    // $ANTLR start "entryRuleEnumLiteral"
    // InternalABSReport.g:2940:1: entryRuleEnumLiteral returns [EObject current=null] : iv_ruleEnumLiteral= ruleEnumLiteral EOF ;
    public final EObject entryRuleEnumLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumLiteral = null;


        try {
            // InternalABSReport.g:2940:52: (iv_ruleEnumLiteral= ruleEnumLiteral EOF )
            // InternalABSReport.g:2941:2: iv_ruleEnumLiteral= ruleEnumLiteral EOF
            {
             newCompositeNode(grammarAccess.getEnumLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEnumLiteral=ruleEnumLiteral();

            state._fsp--;

             current =iv_ruleEnumLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumLiteral"


    // $ANTLR start "ruleEnumLiteral"
    // InternalABSReport.g:2947:1: ruleEnumLiteral returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ) ;
    public final EObject ruleEnumLiteral() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2953:2: ( ( () ( (lv_name_1_0= ruleEString ) ) ) )
            // InternalABSReport.g:2954:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            {
            // InternalABSReport.g:2954:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            // InternalABSReport.g:2955:3: () ( (lv_name_1_0= ruleEString ) )
            {
            // InternalABSReport.g:2955:3: ()
            // InternalABSReport.g:2956:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEnumLiteralAccess().getEnumLiteralAction_0(),
            					current);
            			

            }

            // InternalABSReport.g:2962:3: ( (lv_name_1_0= ruleEString ) )
            // InternalABSReport.g:2963:4: (lv_name_1_0= ruleEString )
            {
            // InternalABSReport.g:2963:4: (lv_name_1_0= ruleEString )
            // InternalABSReport.g:2964:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getEnumLiteralAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEnumLiteralRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumLiteral"


    // $ANTLR start "entryRuleAsset"
    // InternalABSReport.g:2985:1: entryRuleAsset returns [EObject current=null] : iv_ruleAsset= ruleAsset EOF ;
    public final EObject entryRuleAsset() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAsset = null;


        try {
            // InternalABSReport.g:2985:46: (iv_ruleAsset= ruleAsset EOF )
            // InternalABSReport.g:2986:2: iv_ruleAsset= ruleAsset EOF
            {
             newCompositeNode(grammarAccess.getAssetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAsset=ruleAsset();

            state._fsp--;

             current =iv_ruleAsset; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAsset"


    // $ANTLR start "ruleAsset"
    // InternalABSReport.g:2992:1: ruleAsset returns [EObject current=null] : (otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}' ) ;
    public final EObject ruleAsset() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_description_7_0=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        Enumerator lv_descriptionFormat_6_0 = null;

        EObject lv_assetAttributeValues_8_0 = null;

        EObject lv_assetAttributeValues_9_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:2998:2: ( (otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}' ) )
            // InternalABSReport.g:2999:2: (otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}' )
            {
            // InternalABSReport.g:2999:2: (otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}' )
            // InternalABSReport.g:3000:3: otherlv_0= 'Asset' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) otherlv_4= '{' (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )? otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,55,FOLLOW_30); 

            			newLeafNode(otherlv_0, grammarAccess.getAssetAccess().getAssetKeyword_0());
            		
            // InternalABSReport.g:3004:3: ( (lv_name_1_0= ruleEString ) )
            // InternalABSReport.g:3005:4: (lv_name_1_0= ruleEString )
            {
            // InternalABSReport.g:3005:4: (lv_name_1_0= ruleEString )
            // InternalABSReport.g:3006:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAssetAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_42);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssetRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,51,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getAssetAccess().getColonKeyword_2());
            		
            // InternalABSReport.g:3027:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:3028:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:3028:4: ( ruleQualifiedName )
            // InternalABSReport.g:3029:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetAccess().getAssetTypeAssetTypeCrossReference_3_0());
            				
            pushFollow(FOLLOW_6);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,12,FOLLOW_51); 

            			newLeafNode(otherlv_4, grammarAccess.getAssetAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalABSReport.g:3047:3: (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==45) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalABSReport.g:3048:4: otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,45,FOLLOW_39); 

                    				newLeafNode(otherlv_5, grammarAccess.getAssetAccess().getDescriptionKeyword_5_0());
                    			
                    // InternalABSReport.g:3052:4: ( (lv_descriptionFormat_6_0= ruleTextFormat ) )?
                    int alt63=2;
                    int LA63_0 = input.LA(1);

                    if ( ((LA63_0>=112 && LA63_0<=114)) ) {
                        alt63=1;
                    }
                    switch (alt63) {
                        case 1 :
                            // InternalABSReport.g:3053:5: (lv_descriptionFormat_6_0= ruleTextFormat )
                            {
                            // InternalABSReport.g:3053:5: (lv_descriptionFormat_6_0= ruleTextFormat )
                            // InternalABSReport.g:3054:6: lv_descriptionFormat_6_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getAssetAccess().getDescriptionFormatTextFormatEnumRuleCall_5_1_0());
                            					
                            pushFollow(FOLLOW_16);
                            lv_descriptionFormat_6_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getAssetRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_6_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:3071:4: ( (lv_description_7_0= RULE_STRING ) )
                    // InternalABSReport.g:3072:5: (lv_description_7_0= RULE_STRING )
                    {
                    // InternalABSReport.g:3072:5: (lv_description_7_0= RULE_STRING )
                    // InternalABSReport.g:3073:6: lv_description_7_0= RULE_STRING
                    {
                    lv_description_7_0=(Token)match(input,RULE_STRING,FOLLOW_52); 

                    						newLeafNode(lv_description_7_0, grammarAccess.getAssetAccess().getDescriptionSTRINGTerminalRuleCall_5_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAssetRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_7_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalABSReport.g:3090:3: ( ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )* )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==30) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalABSReport.g:3091:4: ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) ) ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )*
                    {
                    // InternalABSReport.g:3091:4: ( (lv_assetAttributeValues_8_0= ruleAssetAttributeValue ) )
                    // InternalABSReport.g:3092:5: (lv_assetAttributeValues_8_0= ruleAssetAttributeValue )
                    {
                    // InternalABSReport.g:3092:5: (lv_assetAttributeValues_8_0= ruleAssetAttributeValue )
                    // InternalABSReport.g:3093:6: lv_assetAttributeValues_8_0= ruleAssetAttributeValue
                    {

                    						newCompositeNode(grammarAccess.getAssetAccess().getAssetAttributeValuesAssetAttributeValueParserRuleCall_6_0_0());
                    					
                    pushFollow(FOLLOW_52);
                    lv_assetAttributeValues_8_0=ruleAssetAttributeValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAssetRule());
                    						}
                    						add(
                    							current,
                    							"assetAttributeValues",
                    							lv_assetAttributeValues_8_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetAttributeValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalABSReport.g:3110:4: ( (lv_assetAttributeValues_9_0= ruleAssetAttributeValue ) )*
                    loop65:
                    do {
                        int alt65=2;
                        int LA65_0 = input.LA(1);

                        if ( (LA65_0==30) ) {
                            alt65=1;
                        }


                        switch (alt65) {
                    	case 1 :
                    	    // InternalABSReport.g:3111:5: (lv_assetAttributeValues_9_0= ruleAssetAttributeValue )
                    	    {
                    	    // InternalABSReport.g:3111:5: (lv_assetAttributeValues_9_0= ruleAssetAttributeValue )
                    	    // InternalABSReport.g:3112:6: lv_assetAttributeValues_9_0= ruleAssetAttributeValue
                    	    {

                    	    						newCompositeNode(grammarAccess.getAssetAccess().getAssetAttributeValuesAssetAttributeValueParserRuleCall_6_1_0());
                    	    					
                    	    pushFollow(FOLLOW_52);
                    	    lv_assetAttributeValues_9_0=ruleAssetAttributeValue();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getAssetRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"assetAttributeValues",
                    	    							lv_assetAttributeValues_9_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetAttributeValue");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop65;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getAssetAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAsset"


    // $ANTLR start "entryRuleAssetLink"
    // InternalABSReport.g:3138:1: entryRuleAssetLink returns [EObject current=null] : iv_ruleAssetLink= ruleAssetLink EOF ;
    public final EObject entryRuleAssetLink() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetLink = null;


        try {
            // InternalABSReport.g:3138:50: (iv_ruleAssetLink= ruleAssetLink EOF )
            // InternalABSReport.g:3139:2: iv_ruleAssetLink= ruleAssetLink EOF
            {
             newCompositeNode(grammarAccess.getAssetLinkRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetLink=ruleAssetLink();

            state._fsp--;

             current =iv_ruleAssetLink; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetLink"


    // $ANTLR start "ruleAssetLink"
    // InternalABSReport.g:3145:1: ruleAssetLink returns [EObject current=null] : (otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )? ) ;
    public final EObject ruleAssetLink() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalABSReport.g:3151:2: ( (otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )? ) )
            // InternalABSReport.g:3152:2: (otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )? )
            {
            // InternalABSReport.g:3152:2: (otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )? )
            // InternalABSReport.g:3153:3: otherlv_0= 'link' ( ( ruleQualifiedName ) ) otherlv_2= 'to' ( ( ruleQualifiedName ) ) (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )?
            {
            otherlv_0=(Token)match(input,56,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getAssetLinkAccess().getLinkKeyword_0());
            		
            // InternalABSReport.g:3157:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:3158:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:3158:4: ( ruleQualifiedName )
            // InternalABSReport.g:3159:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetLinkRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetLinkAccess().getSourceAssetAssetCrossReference_1_0());
            				
            pushFollow(FOLLOW_53);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,57,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getAssetLinkAccess().getToKeyword_2());
            		
            // InternalABSReport.g:3177:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:3178:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:3178:4: ( ruleQualifiedName )
            // InternalABSReport.g:3179:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetLinkRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetLinkAccess().getTargetAssetAssetCrossReference_3_0());
            				
            pushFollow(FOLLOW_54);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalABSReport.g:3193:3: (otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )? )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==58) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalABSReport.g:3194:4: otherlv_4= 'as' ( ( ruleQualifiedName ) ) (otherlv_6= '#' ( ( ruleQualifiedName ) ) )?
                    {
                    otherlv_4=(Token)match(input,58,FOLLOW_5); 

                    				newLeafNode(otherlv_4, grammarAccess.getAssetLinkAccess().getAsKeyword_4_0());
                    			
                    // InternalABSReport.g:3198:4: ( ( ruleQualifiedName ) )
                    // InternalABSReport.g:3199:5: ( ruleQualifiedName )
                    {
                    // InternalABSReport.g:3199:5: ( ruleQualifiedName )
                    // InternalABSReport.g:3200:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAssetLinkRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getAssetLinkAccess().getReferenceTypeAssetTypeReferenceCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_55);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalABSReport.g:3214:4: (otherlv_6= '#' ( ( ruleQualifiedName ) ) )?
                    int alt67=2;
                    int LA67_0 = input.LA(1);

                    if ( (LA67_0==59) ) {
                        alt67=1;
                    }
                    switch (alt67) {
                        case 1 :
                            // InternalABSReport.g:3215:5: otherlv_6= '#' ( ( ruleQualifiedName ) )
                            {
                            otherlv_6=(Token)match(input,59,FOLLOW_5); 

                            					newLeafNode(otherlv_6, grammarAccess.getAssetLinkAccess().getNumberSignKeyword_4_2_0());
                            				
                            // InternalABSReport.g:3219:5: ( ( ruleQualifiedName ) )
                            // InternalABSReport.g:3220:6: ( ruleQualifiedName )
                            {
                            // InternalABSReport.g:3220:6: ( ruleQualifiedName )
                            // InternalABSReport.g:3221:7: ruleQualifiedName
                            {

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAssetLinkRule());
                            							}
                            						

                            							newCompositeNode(grammarAccess.getAssetLinkAccess().getOppositeReferenceTypeAssetTypeReferenceCrossReference_4_2_1_0());
                            						
                            pushFollow(FOLLOW_2);
                            ruleQualifiedName();

                            state._fsp--;


                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetLink"


    // $ANTLR start "entryRuleAssetAttributeValue"
    // InternalABSReport.g:3241:1: entryRuleAssetAttributeValue returns [EObject current=null] : iv_ruleAssetAttributeValue= ruleAssetAttributeValue EOF ;
    public final EObject entryRuleAssetAttributeValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetAttributeValue = null;


        try {
            // InternalABSReport.g:3241:60: (iv_ruleAssetAttributeValue= ruleAssetAttributeValue EOF )
            // InternalABSReport.g:3242:2: iv_ruleAssetAttributeValue= ruleAssetAttributeValue EOF
            {
             newCompositeNode(grammarAccess.getAssetAttributeValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetAttributeValue=ruleAssetAttributeValue();

            state._fsp--;

             current =iv_ruleAssetAttributeValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetAttributeValue"


    // $ANTLR start "ruleAssetAttributeValue"
    // InternalABSReport.g:3248:1: ruleAssetAttributeValue returns [EObject current=null] : ( () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) ) ) ;
    public final EObject ruleAssetAttributeValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_collection_5_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_values_4_0 = null;

        EObject lv_values_6_0 = null;

        EObject lv_values_8_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:3254:2: ( ( () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) ) ) )
            // InternalABSReport.g:3255:2: ( () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) ) )
            {
            // InternalABSReport.g:3255:2: ( () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) ) )
            // InternalABSReport.g:3256:3: () otherlv_1= 'attribute' ( ( ruleQualifiedName ) ) otherlv_3= ':=' ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) )
            {
            // InternalABSReport.g:3256:3: ()
            // InternalABSReport.g:3257:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetAttributeValueAccess().getAssetAttributeValueAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,30,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetAttributeValueAccess().getAttributeKeyword_1());
            		
            // InternalABSReport.g:3267:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:3268:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:3268:4: ( ruleQualifiedName )
            // InternalABSReport.g:3269:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetAttributeValueRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetAttributeValueAccess().getAttributeTypeAssetTypeAttributeCrossReference_2_0());
            				
            pushFollow(FOLLOW_56);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,60,FOLLOW_26); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetAttributeValueAccess().getColonEqualsSignKeyword_3());
            		
            // InternalABSReport.g:3287:3: ( ( (lv_values_4_0= ruleAttributeConstantExpression ) ) | ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' ) )
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( ((LA70_0>=RULE_STRING && LA70_0<=RULE_ID)||LA70_0==35||(LA70_0>=61 && LA70_0<=62)) ) {
                alt70=1;
            }
            else if ( (LA70_0==32) ) {
                alt70=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 70, 0, input);

                throw nvae;
            }
            switch (alt70) {
                case 1 :
                    // InternalABSReport.g:3288:4: ( (lv_values_4_0= ruleAttributeConstantExpression ) )
                    {
                    // InternalABSReport.g:3288:4: ( (lv_values_4_0= ruleAttributeConstantExpression ) )
                    // InternalABSReport.g:3289:5: (lv_values_4_0= ruleAttributeConstantExpression )
                    {
                    // InternalABSReport.g:3289:5: (lv_values_4_0= ruleAttributeConstantExpression )
                    // InternalABSReport.g:3290:6: lv_values_4_0= ruleAttributeConstantExpression
                    {

                    						newCompositeNode(grammarAccess.getAssetAttributeValueAccess().getValuesAttributeConstantExpressionParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_values_4_0=ruleAttributeConstantExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAssetAttributeValueRule());
                    						}
                    						add(
                    							current,
                    							"values",
                    							lv_values_4_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:3308:4: ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' )
                    {
                    // InternalABSReport.g:3308:4: ( ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']' )
                    // InternalABSReport.g:3309:5: ( (lv_collection_5_0= '[' ) ) ( (lv_values_6_0= ruleAttributeConstantExpression ) ) (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )* otherlv_9= ']'
                    {
                    // InternalABSReport.g:3309:5: ( (lv_collection_5_0= '[' ) )
                    // InternalABSReport.g:3310:6: (lv_collection_5_0= '[' )
                    {
                    // InternalABSReport.g:3310:6: (lv_collection_5_0= '[' )
                    // InternalABSReport.g:3311:7: lv_collection_5_0= '['
                    {
                    lv_collection_5_0=(Token)match(input,32,FOLLOW_27); 

                    							newLeafNode(lv_collection_5_0, grammarAccess.getAssetAttributeValueAccess().getCollectionLeftSquareBracketKeyword_4_1_0_0());
                    						

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getAssetAttributeValueRule());
                    							}
                    							setWithLastConsumed(current, "collection", lv_collection_5_0 != null, "[");
                    						

                    }


                    }

                    // InternalABSReport.g:3323:5: ( (lv_values_6_0= ruleAttributeConstantExpression ) )
                    // InternalABSReport.g:3324:6: (lv_values_6_0= ruleAttributeConstantExpression )
                    {
                    // InternalABSReport.g:3324:6: (lv_values_6_0= ruleAttributeConstantExpression )
                    // InternalABSReport.g:3325:7: lv_values_6_0= ruleAttributeConstantExpression
                    {

                    							newCompositeNode(grammarAccess.getAssetAttributeValueAccess().getValuesAttributeConstantExpressionParserRuleCall_4_1_1_0());
                    						
                    pushFollow(FOLLOW_28);
                    lv_values_6_0=ruleAttributeConstantExpression();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getAssetAttributeValueRule());
                    							}
                    							add(
                    								current,
                    								"values",
                    								lv_values_6_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalABSReport.g:3342:5: (otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) ) )*
                    loop69:
                    do {
                        int alt69=2;
                        int LA69_0 = input.LA(1);

                        if ( (LA69_0==26) ) {
                            alt69=1;
                        }


                        switch (alt69) {
                    	case 1 :
                    	    // InternalABSReport.g:3343:6: otherlv_7= ',' ( (lv_values_8_0= ruleAttributeConstantExpression ) )
                    	    {
                    	    otherlv_7=(Token)match(input,26,FOLLOW_27); 

                    	    						newLeafNode(otherlv_7, grammarAccess.getAssetAttributeValueAccess().getCommaKeyword_4_1_2_0());
                    	    					
                    	    // InternalABSReport.g:3347:6: ( (lv_values_8_0= ruleAttributeConstantExpression ) )
                    	    // InternalABSReport.g:3348:7: (lv_values_8_0= ruleAttributeConstantExpression )
                    	    {
                    	    // InternalABSReport.g:3348:7: (lv_values_8_0= ruleAttributeConstantExpression )
                    	    // InternalABSReport.g:3349:8: lv_values_8_0= ruleAttributeConstantExpression
                    	    {

                    	    								newCompositeNode(grammarAccess.getAssetAttributeValueAccess().getValuesAttributeConstantExpressionParserRuleCall_4_1_2_1_0());
                    	    							
                    	    pushFollow(FOLLOW_28);
                    	    lv_values_8_0=ruleAttributeConstantExpression();

                    	    state._fsp--;


                    	    								if (current==null) {
                    	    									current = createModelElementForParent(grammarAccess.getAssetAttributeValueRule());
                    	    								}
                    	    								add(
                    	    									current,
                    	    									"values",
                    	    									lv_values_8_0,
                    	    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AttributeConstantExpression");
                    	    								afterParserOrEnumRuleCall();
                    	    							

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop69;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,33,FOLLOW_2); 

                    					newLeafNode(otherlv_9, grammarAccess.getAssetAttributeValueAccess().getRightSquareBracketKeyword_4_1_3());
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetAttributeValue"


    // $ANTLR start "entryRuleAttributeConstantExpression"
    // InternalABSReport.g:3377:1: entryRuleAttributeConstantExpression returns [EObject current=null] : iv_ruleAttributeConstantExpression= ruleAttributeConstantExpression EOF ;
    public final EObject entryRuleAttributeConstantExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeConstantExpression = null;


        try {
            // InternalABSReport.g:3377:68: (iv_ruleAttributeConstantExpression= ruleAttributeConstantExpression EOF )
            // InternalABSReport.g:3378:2: iv_ruleAttributeConstantExpression= ruleAttributeConstantExpression EOF
            {
             newCompositeNode(grammarAccess.getAttributeConstantExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeConstantExpression=ruleAttributeConstantExpression();

            state._fsp--;

             current =iv_ruleAttributeConstantExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeConstantExpression"


    // $ANTLR start "ruleAttributeConstantExpression"
    // InternalABSReport.g:3384:1: ruleAttributeConstantExpression returns [EObject current=null] : ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Undefined_10= ruleUndefined ) ;
    public final EObject ruleAttributeConstantExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token lv_value_3_0=null;
        Token lv_value_5_1=null;
        Token lv_value_5_2=null;
        AntlrDatatypeRuleToken lv_value_7_0 = null;

        EObject this_Undefined_10 = null;



        	enterRule();

        try {
            // InternalABSReport.g:3390:2: ( ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Undefined_10= ruleUndefined ) )
            // InternalABSReport.g:3391:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Undefined_10= ruleUndefined )
            {
            // InternalABSReport.g:3391:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Undefined_10= ruleUndefined )
            int alt72=6;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                int LA72_1 = input.LA(2);

                if ( (LA72_1==EOF||LA72_1==13||LA72_1==26||LA72_1==30||(LA72_1>=33 && LA72_1<=34)||LA72_1==50) ) {
                    alt72=1;
                }
                else if ( (LA72_1==83) ) {
                    alt72=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 72, 1, input);

                    throw nvae;
                }
                }
                break;
            case RULE_STRING:
                {
                alt72=2;
                }
                break;
            case 61:
            case 62:
                {
                alt72=3;
                }
                break;
            case RULE_ID:
                {
                alt72=5;
                }
                break;
            case 35:
                {
                alt72=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 72, 0, input);

                throw nvae;
            }

            switch (alt72) {
                case 1 :
                    // InternalABSReport.g:3392:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    {
                    // InternalABSReport.g:3392:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    // InternalABSReport.g:3393:4: () ( (lv_value_1_0= RULE_INT ) )
                    {
                    // InternalABSReport.g:3393:4: ()
                    // InternalABSReport.g:3394:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getIntConstantAction_0_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:3400:4: ( (lv_value_1_0= RULE_INT ) )
                    // InternalABSReport.g:3401:5: (lv_value_1_0= RULE_INT )
                    {
                    // InternalABSReport.g:3401:5: (lv_value_1_0= RULE_INT )
                    // InternalABSReport.g:3402:6: lv_value_1_0= RULE_INT
                    {
                    lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    						newLeafNode(lv_value_1_0, grammarAccess.getAttributeConstantExpressionAccess().getValueINTTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_1_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:3420:3: ( () ( (lv_value_3_0= RULE_STRING ) ) )
                    {
                    // InternalABSReport.g:3420:3: ( () ( (lv_value_3_0= RULE_STRING ) ) )
                    // InternalABSReport.g:3421:4: () ( (lv_value_3_0= RULE_STRING ) )
                    {
                    // InternalABSReport.g:3421:4: ()
                    // InternalABSReport.g:3422:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getStringConstantAction_1_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:3428:4: ( (lv_value_3_0= RULE_STRING ) )
                    // InternalABSReport.g:3429:5: (lv_value_3_0= RULE_STRING )
                    {
                    // InternalABSReport.g:3429:5: (lv_value_3_0= RULE_STRING )
                    // InternalABSReport.g:3430:6: lv_value_3_0= RULE_STRING
                    {
                    lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getAttributeConstantExpressionAccess().getValueSTRINGTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalABSReport.g:3448:3: ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) )
                    {
                    // InternalABSReport.g:3448:3: ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) )
                    // InternalABSReport.g:3449:4: () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) )
                    {
                    // InternalABSReport.g:3449:4: ()
                    // InternalABSReport.g:3450:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getBooleanConstantAction_2_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:3456:4: ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) )
                    // InternalABSReport.g:3457:5: ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) )
                    {
                    // InternalABSReport.g:3457:5: ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) )
                    // InternalABSReport.g:3458:6: (lv_value_5_1= 'true' | lv_value_5_2= 'false' )
                    {
                    // InternalABSReport.g:3458:6: (lv_value_5_1= 'true' | lv_value_5_2= 'false' )
                    int alt71=2;
                    int LA71_0 = input.LA(1);

                    if ( (LA71_0==61) ) {
                        alt71=1;
                    }
                    else if ( (LA71_0==62) ) {
                        alt71=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 71, 0, input);

                        throw nvae;
                    }
                    switch (alt71) {
                        case 1 :
                            // InternalABSReport.g:3459:7: lv_value_5_1= 'true'
                            {
                            lv_value_5_1=(Token)match(input,61,FOLLOW_2); 

                            							newLeafNode(lv_value_5_1, grammarAccess.getAttributeConstantExpressionAccess().getValueTrueKeyword_2_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_5_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalABSReport.g:3470:7: lv_value_5_2= 'false'
                            {
                            lv_value_5_2=(Token)match(input,62,FOLLOW_2); 

                            							newLeafNode(lv_value_5_2, grammarAccess.getAttributeConstantExpressionAccess().getValueFalseKeyword_2_1_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_5_2, null);
                            						

                            }
                            break;

                    }


                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalABSReport.g:3485:3: ( () ( (lv_value_7_0= ruleVersion ) ) )
                    {
                    // InternalABSReport.g:3485:3: ( () ( (lv_value_7_0= ruleVersion ) ) )
                    // InternalABSReport.g:3486:4: () ( (lv_value_7_0= ruleVersion ) )
                    {
                    // InternalABSReport.g:3486:4: ()
                    // InternalABSReport.g:3487:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getVersionConstantAction_3_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:3493:4: ( (lv_value_7_0= ruleVersion ) )
                    // InternalABSReport.g:3494:5: (lv_value_7_0= ruleVersion )
                    {
                    // InternalABSReport.g:3494:5: (lv_value_7_0= ruleVersion )
                    // InternalABSReport.g:3495:6: lv_value_7_0= ruleVersion
                    {

                    						newCompositeNode(grammarAccess.getAttributeConstantExpressionAccess().getValueVersionParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_7_0=ruleVersion();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeConstantExpressionRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Version");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalABSReport.g:3514:3: ( () ( ( ruleQualifiedName ) ) )
                    {
                    // InternalABSReport.g:3514:3: ( () ( ( ruleQualifiedName ) ) )
                    // InternalABSReport.g:3515:4: () ( ( ruleQualifiedName ) )
                    {
                    // InternalABSReport.g:3515:4: ()
                    // InternalABSReport.g:3516:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAttributeConstantExpressionAccess().getEnumConstantAction_4_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:3522:4: ( ( ruleQualifiedName ) )
                    // InternalABSReport.g:3523:5: ( ruleQualifiedName )
                    {
                    // InternalABSReport.g:3523:5: ( ruleQualifiedName )
                    // InternalABSReport.g:3524:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttributeConstantExpressionRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getAttributeConstantExpressionAccess().getValueEnumLiteralCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalABSReport.g:3540:3: this_Undefined_10= ruleUndefined
                    {

                    			newCompositeNode(grammarAccess.getAttributeConstantExpressionAccess().getUndefinedParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Undefined_10=ruleUndefined();

                    state._fsp--;


                    			current = this_Undefined_10;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeConstantExpression"


    // $ANTLR start "entryRuleContract"
    // InternalABSReport.g:3552:1: entryRuleContract returns [EObject current=null] : iv_ruleContract= ruleContract EOF ;
    public final EObject entryRuleContract() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContract = null;


        try {
            // InternalABSReport.g:3552:49: (iv_ruleContract= ruleContract EOF )
            // InternalABSReport.g:3553:2: iv_ruleContract= ruleContract EOF
            {
             newCompositeNode(grammarAccess.getContractRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleContract=ruleContract();

            state._fsp--;

             current =iv_ruleContract; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContract"


    // $ANTLR start "ruleContract"
    // InternalABSReport.g:3559:1: ruleContract returns [EObject current=null] : ( () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}' ) ;
    public final EObject ruleContract() throws RecognitionException {
        EObject current = null;

        Token lv_dynamic_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token lv_description_17_0=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        EObject lv_guardParameters_6_0 = null;

        EObject lv_guardParameters_8_0 = null;

        Enumerator lv_severity_14_0 = null;

        Enumerator lv_descriptionFormat_16_0 = null;

        EObject lv_annotations_20_0 = null;

        EObject lv_guardExpression_24_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:3565:2: ( ( () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}' ) )
            // InternalABSReport.g:3566:2: ( () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}' )
            {
            // InternalABSReport.g:3566:2: ( () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}' )
            // InternalABSReport.g:3567:3: () ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? ) otherlv_3= 'contract' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '(' ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) ) otherlv_22= 'guard' otherlv_23= '=' ( (lv_guardExpression_24_0= ruleExpression ) ) otherlv_25= '}'
            {
            // InternalABSReport.g:3567:3: ()
            // InternalABSReport.g:3568:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getContractAccess().getContractAction_0(),
            					current);
            			

            }

            // InternalABSReport.g:3574:3: ( ( (lv_dynamic_1_0= 'dynamic' ) ) | (otherlv_2= 'static' )? )
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==63) ) {
                alt74=1;
            }
            else if ( ((LA74_0>=64 && LA74_0<=65)) ) {
                alt74=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 74, 0, input);

                throw nvae;
            }
            switch (alt74) {
                case 1 :
                    // InternalABSReport.g:3575:4: ( (lv_dynamic_1_0= 'dynamic' ) )
                    {
                    // InternalABSReport.g:3575:4: ( (lv_dynamic_1_0= 'dynamic' ) )
                    // InternalABSReport.g:3576:5: (lv_dynamic_1_0= 'dynamic' )
                    {
                    // InternalABSReport.g:3576:5: (lv_dynamic_1_0= 'dynamic' )
                    // InternalABSReport.g:3577:6: lv_dynamic_1_0= 'dynamic'
                    {
                    lv_dynamic_1_0=(Token)match(input,63,FOLLOW_57); 

                    						newLeafNode(lv_dynamic_1_0, grammarAccess.getContractAccess().getDynamicDynamicKeyword_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getContractRule());
                    						}
                    						setWithLastConsumed(current, "dynamic", lv_dynamic_1_0 != null, "dynamic");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:3590:4: (otherlv_2= 'static' )?
                    {
                    // InternalABSReport.g:3590:4: (otherlv_2= 'static' )?
                    int alt73=2;
                    int LA73_0 = input.LA(1);

                    if ( (LA73_0==64) ) {
                        alt73=1;
                    }
                    switch (alt73) {
                        case 1 :
                            // InternalABSReport.g:3591:5: otherlv_2= 'static'
                            {
                            otherlv_2=(Token)match(input,64,FOLLOW_57); 

                            					newLeafNode(otherlv_2, grammarAccess.getContractAccess().getStaticKeyword_1_1());
                            				

                            }
                            break;

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,65,FOLLOW_30); 

            			newLeafNode(otherlv_3, grammarAccess.getContractAccess().getContractKeyword_2());
            		
            // InternalABSReport.g:3601:3: ( (lv_name_4_0= ruleEString ) )
            // InternalABSReport.g:3602:4: (lv_name_4_0= ruleEString )
            {
            // InternalABSReport.g:3602:4: (lv_name_4_0= ruleEString )
            // InternalABSReport.g:3603:5: lv_name_4_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getContractAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_19);
            lv_name_4_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getContractRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_4_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,27,FOLLOW_58); 

            			newLeafNode(otherlv_5, grammarAccess.getContractAccess().getLeftParenthesisKeyword_4());
            		
            // InternalABSReport.g:3624:3: ( ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )* )?
            int alt76=2;
            int LA76_0 = input.LA(1);

            if ( (LA76_0==RULE_STRING||LA76_0==RULE_ID) ) {
                alt76=1;
            }
            switch (alt76) {
                case 1 :
                    // InternalABSReport.g:3625:4: ( (lv_guardParameters_6_0= ruleGuardParameter ) ) (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )*
                    {
                    // InternalABSReport.g:3625:4: ( (lv_guardParameters_6_0= ruleGuardParameter ) )
                    // InternalABSReport.g:3626:5: (lv_guardParameters_6_0= ruleGuardParameter )
                    {
                    // InternalABSReport.g:3626:5: (lv_guardParameters_6_0= ruleGuardParameter )
                    // InternalABSReport.g:3627:6: lv_guardParameters_6_0= ruleGuardParameter
                    {

                    						newCompositeNode(grammarAccess.getContractAccess().getGuardParametersGuardParameterParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_21);
                    lv_guardParameters_6_0=ruleGuardParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getContractRule());
                    						}
                    						add(
                    							current,
                    							"guardParameters",
                    							lv_guardParameters_6_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardParameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalABSReport.g:3644:4: (otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) ) )*
                    loop75:
                    do {
                        int alt75=2;
                        int LA75_0 = input.LA(1);

                        if ( (LA75_0==26) ) {
                            alt75=1;
                        }


                        switch (alt75) {
                    	case 1 :
                    	    // InternalABSReport.g:3645:5: otherlv_7= ',' ( (lv_guardParameters_8_0= ruleGuardParameter ) )
                    	    {
                    	    otherlv_7=(Token)match(input,26,FOLLOW_30); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getContractAccess().getCommaKeyword_5_1_0());
                    	    				
                    	    // InternalABSReport.g:3649:5: ( (lv_guardParameters_8_0= ruleGuardParameter ) )
                    	    // InternalABSReport.g:3650:6: (lv_guardParameters_8_0= ruleGuardParameter )
                    	    {
                    	    // InternalABSReport.g:3650:6: (lv_guardParameters_8_0= ruleGuardParameter )
                    	    // InternalABSReport.g:3651:7: lv_guardParameters_8_0= ruleGuardParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getContractAccess().getGuardParametersGuardParameterParserRuleCall_5_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_21);
                    	    lv_guardParameters_8_0=ruleGuardParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getContractRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"guardParameters",
                    	    								lv_guardParameters_8_0,
                    	    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardParameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop75;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_9=(Token)match(input,28,FOLLOW_6); 

            			newLeafNode(otherlv_9, grammarAccess.getContractAccess().getRightParenthesisKeyword_6());
            		
            otherlv_10=(Token)match(input,12,FOLLOW_59); 

            			newLeafNode(otherlv_10, grammarAccess.getContractAccess().getLeftCurlyBracketKeyword_7());
            		
            // InternalABSReport.g:3678:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) ) )
            // InternalABSReport.g:3679:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) )
            {
            // InternalABSReport.g:3679:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* ) )
            // InternalABSReport.g:3680:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getContractAccess().getUnorderedGroup_8());
            				
            // InternalABSReport.g:3683:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )* )
            // InternalABSReport.g:3684:6: ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )*
            {
            // InternalABSReport.g:3684:6: ( ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) ) )*
            loop79:
            do {
                int alt79=4;
                int LA79_0 = input.LA(1);

                if ( LA79_0 == 66 && getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 0) ) {
                    alt79=1;
                }
                else if ( LA79_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 1) ) {
                    alt79=2;
                }
                else if ( LA79_0 == 38 && getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 2) ) {
                    alt79=3;
                }


                switch (alt79) {
            	case 1 :
            	    // InternalABSReport.g:3685:4: ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:3685:4: ({...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) ) )
            	    // InternalABSReport.g:3686:5: {...}? => ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 0)");
            	    }
            	    // InternalABSReport.g:3686:105: ( ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) ) )
            	    // InternalABSReport.g:3687:6: ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getContractAccess().getUnorderedGroup_8(), 0);
            	    					
            	    // InternalABSReport.g:3690:9: ({...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) ) )
            	    // InternalABSReport.g:3690:10: {...}? => (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "true");
            	    }
            	    // InternalABSReport.g:3690:19: (otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) ) )
            	    // InternalABSReport.g:3690:20: otherlv_12= 'severity' otherlv_13= '=' ( (lv_severity_14_0= ruleSeverity ) )
            	    {
            	    otherlv_12=(Token)match(input,66,FOLLOW_25); 

            	    									newLeafNode(otherlv_12, grammarAccess.getContractAccess().getSeverityKeyword_8_0_0());
            	    								
            	    otherlv_13=(Token)match(input,31,FOLLOW_60); 

            	    									newLeafNode(otherlv_13, grammarAccess.getContractAccess().getEqualsSignKeyword_8_0_1());
            	    								
            	    // InternalABSReport.g:3698:9: ( (lv_severity_14_0= ruleSeverity ) )
            	    // InternalABSReport.g:3699:10: (lv_severity_14_0= ruleSeverity )
            	    {
            	    // InternalABSReport.g:3699:10: (lv_severity_14_0= ruleSeverity )
            	    // InternalABSReport.g:3700:11: lv_severity_14_0= ruleSeverity
            	    {

            	    											newCompositeNode(grammarAccess.getContractAccess().getSeveritySeverityEnumRuleCall_8_0_2_0());
            	    										
            	    pushFollow(FOLLOW_59);
            	    lv_severity_14_0=ruleSeverity();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getContractRule());
            	    											}
            	    											set(
            	    												current,
            	    												"severity",
            	    												lv_severity_14_0,
            	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Severity");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getContractAccess().getUnorderedGroup_8());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalABSReport.g:3723:4: ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:3723:4: ({...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) ) )
            	    // InternalABSReport.g:3724:5: {...}? => ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 1)");
            	    }
            	    // InternalABSReport.g:3724:105: ( ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) ) )
            	    // InternalABSReport.g:3725:6: ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getContractAccess().getUnorderedGroup_8(), 1);
            	    					
            	    // InternalABSReport.g:3728:9: ({...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) ) )
            	    // InternalABSReport.g:3728:10: {...}? => (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "true");
            	    }
            	    // InternalABSReport.g:3728:19: (otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) ) )
            	    // InternalABSReport.g:3728:20: otherlv_15= 'description' ( (lv_descriptionFormat_16_0= ruleTextFormat ) )? ( (lv_description_17_0= RULE_STRING ) )
            	    {
            	    otherlv_15=(Token)match(input,45,FOLLOW_39); 

            	    									newLeafNode(otherlv_15, grammarAccess.getContractAccess().getDescriptionKeyword_8_1_0());
            	    								
            	    // InternalABSReport.g:3732:9: ( (lv_descriptionFormat_16_0= ruleTextFormat ) )?
            	    int alt77=2;
            	    int LA77_0 = input.LA(1);

            	    if ( ((LA77_0>=112 && LA77_0<=114)) ) {
            	        alt77=1;
            	    }
            	    switch (alt77) {
            	        case 1 :
            	            // InternalABSReport.g:3733:10: (lv_descriptionFormat_16_0= ruleTextFormat )
            	            {
            	            // InternalABSReport.g:3733:10: (lv_descriptionFormat_16_0= ruleTextFormat )
            	            // InternalABSReport.g:3734:11: lv_descriptionFormat_16_0= ruleTextFormat
            	            {

            	            											newCompositeNode(grammarAccess.getContractAccess().getDescriptionFormatTextFormatEnumRuleCall_8_1_1_0());
            	            										
            	            pushFollow(FOLLOW_16);
            	            lv_descriptionFormat_16_0=ruleTextFormat();

            	            state._fsp--;


            	            											if (current==null) {
            	            												current = createModelElementForParent(grammarAccess.getContractRule());
            	            											}
            	            											set(
            	            												current,
            	            												"descriptionFormat",
            	            												lv_descriptionFormat_16_0,
            	            												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
            	            											afterParserOrEnumRuleCall();
            	            										

            	            }


            	            }
            	            break;

            	    }

            	    // InternalABSReport.g:3751:9: ( (lv_description_17_0= RULE_STRING ) )
            	    // InternalABSReport.g:3752:10: (lv_description_17_0= RULE_STRING )
            	    {
            	    // InternalABSReport.g:3752:10: (lv_description_17_0= RULE_STRING )
            	    // InternalABSReport.g:3753:11: lv_description_17_0= RULE_STRING
            	    {
            	    lv_description_17_0=(Token)match(input,RULE_STRING,FOLLOW_59); 

            	    											newLeafNode(lv_description_17_0, grammarAccess.getContractAccess().getDescriptionSTRINGTerminalRuleCall_8_1_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getContractRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"description",
            	    												lv_description_17_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getContractAccess().getUnorderedGroup_8());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalABSReport.g:3775:4: ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) )
            	    {
            	    // InternalABSReport.g:3775:4: ({...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) ) )
            	    // InternalABSReport.g:3776:5: {...}? => ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "getUnorderedGroupHelper().canSelect(grammarAccess.getContractAccess().getUnorderedGroup_8(), 2)");
            	    }
            	    // InternalABSReport.g:3776:105: ( ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) ) )
            	    // InternalABSReport.g:3777:6: ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getContractAccess().getUnorderedGroup_8(), 2);
            	    					
            	    // InternalABSReport.g:3780:9: ({...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' ) )
            	    // InternalABSReport.g:3780:10: {...}? => (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleContract", "true");
            	    }
            	    // InternalABSReport.g:3780:19: (otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}' )
            	    // InternalABSReport.g:3780:20: otherlv_18= 'annotations' otherlv_19= '{' ( (lv_annotations_20_0= ruleAnnotationEntry ) )* otherlv_21= '}'
            	    {
            	    otherlv_18=(Token)match(input,38,FOLLOW_6); 

            	    									newLeafNode(otherlv_18, grammarAccess.getContractAccess().getAnnotationsKeyword_8_2_0());
            	    								
            	    otherlv_19=(Token)match(input,12,FOLLOW_32); 

            	    									newLeafNode(otherlv_19, grammarAccess.getContractAccess().getLeftCurlyBracketKeyword_8_2_1());
            	    								
            	    // InternalABSReport.g:3788:9: ( (lv_annotations_20_0= ruleAnnotationEntry ) )*
            	    loop78:
            	    do {
            	        int alt78=2;
            	        int LA78_0 = input.LA(1);

            	        if ( (LA78_0==RULE_STRING||LA78_0==RULE_ID) ) {
            	            alt78=1;
            	        }


            	        switch (alt78) {
            	    	case 1 :
            	    	    // InternalABSReport.g:3789:10: (lv_annotations_20_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalABSReport.g:3789:10: (lv_annotations_20_0= ruleAnnotationEntry )
            	    	    // InternalABSReport.g:3790:11: lv_annotations_20_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getContractAccess().getAnnotationsAnnotationEntryParserRuleCall_8_2_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_32);
            	    	    lv_annotations_20_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getContractRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_20_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop78;
            	        }
            	    } while (true);

            	    otherlv_21=(Token)match(input,13,FOLLOW_59); 

            	    									newLeafNode(otherlv_21, grammarAccess.getContractAccess().getRightCurlyBracketKeyword_8_2_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getContractAccess().getUnorderedGroup_8());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop79;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getContractAccess().getUnorderedGroup_8());
            				

            }

            otherlv_22=(Token)match(input,67,FOLLOW_25); 

            			newLeafNode(otherlv_22, grammarAccess.getContractAccess().getGuardKeyword_9());
            		
            otherlv_23=(Token)match(input,31,FOLLOW_61); 

            			newLeafNode(otherlv_23, grammarAccess.getContractAccess().getEqualsSignKeyword_10());
            		
            // InternalABSReport.g:3832:3: ( (lv_guardExpression_24_0= ruleExpression ) )
            // InternalABSReport.g:3833:4: (lv_guardExpression_24_0= ruleExpression )
            {
            // InternalABSReport.g:3833:4: (lv_guardExpression_24_0= ruleExpression )
            // InternalABSReport.g:3834:5: lv_guardExpression_24_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getContractAccess().getGuardExpressionExpressionParserRuleCall_11_0());
            				
            pushFollow(FOLLOW_14);
            lv_guardExpression_24_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getContractRule());
            					}
            					set(
            						current,
            						"guardExpression",
            						lv_guardExpression_24_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_25=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_25, grammarAccess.getContractAccess().getRightCurlyBracketKeyword_12());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContract"


    // $ANTLR start "entryRuleGuardedAction"
    // InternalABSReport.g:3859:1: entryRuleGuardedAction returns [EObject current=null] : iv_ruleGuardedAction= ruleGuardedAction EOF ;
    public final EObject entryRuleGuardedAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardedAction = null;


        try {
            // InternalABSReport.g:3859:54: (iv_ruleGuardedAction= ruleGuardedAction EOF )
            // InternalABSReport.g:3860:2: iv_ruleGuardedAction= ruleGuardedAction EOF
            {
             newCompositeNode(grammarAccess.getGuardedActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuardedAction=ruleGuardedAction();

            state._fsp--;

             current =iv_ruleGuardedAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardedAction"


    // $ANTLR start "ruleGuardedAction"
    // InternalABSReport.g:3866:1: ruleGuardedAction returns [EObject current=null] : ( () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}' ) ;
    public final EObject ruleGuardedAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token lv_description_12_0=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_guardParameters_4_0 = null;

        EObject lv_guardParameters_6_0 = null;

        Enumerator lv_descriptionFormat_11_0 = null;

        EObject lv_annotations_15_0 = null;

        EObject lv_guardExpression_19_0 = null;

        EObject lv_guardActions_22_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:3872:2: ( ( () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}' ) )
            // InternalABSReport.g:3873:2: ( () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}' )
            {
            // InternalABSReport.g:3873:2: ( () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}' )
            // InternalABSReport.g:3874:3: () otherlv_1= 'GuardedAction' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '(' ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )? otherlv_7= ')' otherlv_8= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) ) otherlv_17= 'guard' otherlv_18= '=' ( (lv_guardExpression_19_0= ruleExpression ) ) otherlv_20= 'action' otherlv_21= '{' ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )* otherlv_24= '}' otherlv_25= '}'
            {
            // InternalABSReport.g:3874:3: ()
            // InternalABSReport.g:3875:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGuardedActionAccess().getGuardedActionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,68,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getGuardedActionAccess().getGuardedActionKeyword_1());
            		
            // InternalABSReport.g:3885:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:3886:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:3886:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:3887:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getGuardedActionAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_19);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,27,FOLLOW_58); 

            			newLeafNode(otherlv_3, grammarAccess.getGuardedActionAccess().getLeftParenthesisKeyword_3());
            		
            // InternalABSReport.g:3908:3: ( ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )* )?
            int alt81=2;
            int LA81_0 = input.LA(1);

            if ( (LA81_0==RULE_STRING||LA81_0==RULE_ID) ) {
                alt81=1;
            }
            switch (alt81) {
                case 1 :
                    // InternalABSReport.g:3909:4: ( (lv_guardParameters_4_0= ruleGuardParameter ) ) (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )*
                    {
                    // InternalABSReport.g:3909:4: ( (lv_guardParameters_4_0= ruleGuardParameter ) )
                    // InternalABSReport.g:3910:5: (lv_guardParameters_4_0= ruleGuardParameter )
                    {
                    // InternalABSReport.g:3910:5: (lv_guardParameters_4_0= ruleGuardParameter )
                    // InternalABSReport.g:3911:6: lv_guardParameters_4_0= ruleGuardParameter
                    {

                    						newCompositeNode(grammarAccess.getGuardedActionAccess().getGuardParametersGuardParameterParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_21);
                    lv_guardParameters_4_0=ruleGuardParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGuardedActionRule());
                    						}
                    						add(
                    							current,
                    							"guardParameters",
                    							lv_guardParameters_4_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardParameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalABSReport.g:3928:4: (otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) ) )*
                    loop80:
                    do {
                        int alt80=2;
                        int LA80_0 = input.LA(1);

                        if ( (LA80_0==26) ) {
                            alt80=1;
                        }


                        switch (alt80) {
                    	case 1 :
                    	    // InternalABSReport.g:3929:5: otherlv_5= ',' ( (lv_guardParameters_6_0= ruleGuardParameter ) )
                    	    {
                    	    otherlv_5=(Token)match(input,26,FOLLOW_30); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getGuardedActionAccess().getCommaKeyword_4_1_0());
                    	    				
                    	    // InternalABSReport.g:3933:5: ( (lv_guardParameters_6_0= ruleGuardParameter ) )
                    	    // InternalABSReport.g:3934:6: (lv_guardParameters_6_0= ruleGuardParameter )
                    	    {
                    	    // InternalABSReport.g:3934:6: (lv_guardParameters_6_0= ruleGuardParameter )
                    	    // InternalABSReport.g:3935:7: lv_guardParameters_6_0= ruleGuardParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getGuardedActionAccess().getGuardParametersGuardParameterParserRuleCall_4_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_21);
                    	    lv_guardParameters_6_0=ruleGuardParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getGuardedActionRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"guardParameters",
                    	    								lv_guardParameters_6_0,
                    	    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardParameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop80;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,28,FOLLOW_6); 

            			newLeafNode(otherlv_7, grammarAccess.getGuardedActionAccess().getRightParenthesisKeyword_5());
            		
            otherlv_8=(Token)match(input,12,FOLLOW_62); 

            			newLeafNode(otherlv_8, grammarAccess.getGuardedActionAccess().getLeftCurlyBracketKeyword_6());
            		
            // InternalABSReport.g:3962:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) ) )
            // InternalABSReport.g:3963:4: ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) )
            {
            // InternalABSReport.g:3963:4: ( ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* ) )
            // InternalABSReport.g:3964:5: ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7());
            				
            // InternalABSReport.g:3967:5: ( ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )* )
            // InternalABSReport.g:3968:6: ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )*
            {
            // InternalABSReport.g:3968:6: ( ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) ) )*
            loop84:
            do {
                int alt84=3;
                int LA84_0 = input.LA(1);

                if ( LA84_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 0) ) {
                    alt84=1;
                }
                else if ( LA84_0 == 38 && getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 1) ) {
                    alt84=2;
                }


                switch (alt84) {
            	case 1 :
            	    // InternalABSReport.g:3969:4: ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:3969:4: ({...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) ) )
            	    // InternalABSReport.g:3970:5: {...}? => ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleGuardedAction", "getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 0)");
            	    }
            	    // InternalABSReport.g:3970:110: ( ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) ) )
            	    // InternalABSReport.g:3971:6: ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 0);
            	    					
            	    // InternalABSReport.g:3974:9: ({...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) ) )
            	    // InternalABSReport.g:3974:10: {...}? => (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGuardedAction", "true");
            	    }
            	    // InternalABSReport.g:3974:19: (otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) ) )
            	    // InternalABSReport.g:3974:20: otherlv_10= 'description' ( (lv_descriptionFormat_11_0= ruleTextFormat ) )? ( (lv_description_12_0= RULE_STRING ) )
            	    {
            	    otherlv_10=(Token)match(input,45,FOLLOW_39); 

            	    									newLeafNode(otherlv_10, grammarAccess.getGuardedActionAccess().getDescriptionKeyword_7_0_0());
            	    								
            	    // InternalABSReport.g:3978:9: ( (lv_descriptionFormat_11_0= ruleTextFormat ) )?
            	    int alt82=2;
            	    int LA82_0 = input.LA(1);

            	    if ( ((LA82_0>=112 && LA82_0<=114)) ) {
            	        alt82=1;
            	    }
            	    switch (alt82) {
            	        case 1 :
            	            // InternalABSReport.g:3979:10: (lv_descriptionFormat_11_0= ruleTextFormat )
            	            {
            	            // InternalABSReport.g:3979:10: (lv_descriptionFormat_11_0= ruleTextFormat )
            	            // InternalABSReport.g:3980:11: lv_descriptionFormat_11_0= ruleTextFormat
            	            {

            	            											newCompositeNode(grammarAccess.getGuardedActionAccess().getDescriptionFormatTextFormatEnumRuleCall_7_0_1_0());
            	            										
            	            pushFollow(FOLLOW_16);
            	            lv_descriptionFormat_11_0=ruleTextFormat();

            	            state._fsp--;


            	            											if (current==null) {
            	            												current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            	            											}
            	            											set(
            	            												current,
            	            												"descriptionFormat",
            	            												lv_descriptionFormat_11_0,
            	            												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
            	            											afterParserOrEnumRuleCall();
            	            										

            	            }


            	            }
            	            break;

            	    }

            	    // InternalABSReport.g:3997:9: ( (lv_description_12_0= RULE_STRING ) )
            	    // InternalABSReport.g:3998:10: (lv_description_12_0= RULE_STRING )
            	    {
            	    // InternalABSReport.g:3998:10: (lv_description_12_0= RULE_STRING )
            	    // InternalABSReport.g:3999:11: lv_description_12_0= RULE_STRING
            	    {
            	    lv_description_12_0=(Token)match(input,RULE_STRING,FOLLOW_62); 

            	    											newLeafNode(lv_description_12_0, grammarAccess.getGuardedActionAccess().getDescriptionSTRINGTerminalRuleCall_7_0_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getGuardedActionRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"description",
            	    												lv_description_12_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalABSReport.g:4021:4: ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) )
            	    {
            	    // InternalABSReport.g:4021:4: ({...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) ) )
            	    // InternalABSReport.g:4022:5: {...}? => ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleGuardedAction", "getUnorderedGroupHelper().canSelect(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 1)");
            	    }
            	    // InternalABSReport.g:4022:110: ( ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) ) )
            	    // InternalABSReport.g:4023:6: ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7(), 1);
            	    					
            	    // InternalABSReport.g:4026:9: ({...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' ) )
            	    // InternalABSReport.g:4026:10: {...}? => (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGuardedAction", "true");
            	    }
            	    // InternalABSReport.g:4026:19: (otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}' )
            	    // InternalABSReport.g:4026:20: otherlv_13= 'annotations' otherlv_14= '{' ( (lv_annotations_15_0= ruleAnnotationEntry ) )* otherlv_16= '}'
            	    {
            	    otherlv_13=(Token)match(input,38,FOLLOW_6); 

            	    									newLeafNode(otherlv_13, grammarAccess.getGuardedActionAccess().getAnnotationsKeyword_7_1_0());
            	    								
            	    otherlv_14=(Token)match(input,12,FOLLOW_32); 

            	    									newLeafNode(otherlv_14, grammarAccess.getGuardedActionAccess().getLeftCurlyBracketKeyword_7_1_1());
            	    								
            	    // InternalABSReport.g:4034:9: ( (lv_annotations_15_0= ruleAnnotationEntry ) )*
            	    loop83:
            	    do {
            	        int alt83=2;
            	        int LA83_0 = input.LA(1);

            	        if ( (LA83_0==RULE_STRING||LA83_0==RULE_ID) ) {
            	            alt83=1;
            	        }


            	        switch (alt83) {
            	    	case 1 :
            	    	    // InternalABSReport.g:4035:10: (lv_annotations_15_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalABSReport.g:4035:10: (lv_annotations_15_0= ruleAnnotationEntry )
            	    	    // InternalABSReport.g:4036:11: lv_annotations_15_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getGuardedActionAccess().getAnnotationsAnnotationEntryParserRuleCall_7_1_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_32);
            	    	    lv_annotations_15_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_15_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop83;
            	        }
            	    } while (true);

            	    otherlv_16=(Token)match(input,13,FOLLOW_62); 

            	    									newLeafNode(otherlv_16, grammarAccess.getGuardedActionAccess().getRightCurlyBracketKeyword_7_1_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop84;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getGuardedActionAccess().getUnorderedGroup_7());
            				

            }

            otherlv_17=(Token)match(input,67,FOLLOW_25); 

            			newLeafNode(otherlv_17, grammarAccess.getGuardedActionAccess().getGuardKeyword_8());
            		
            otherlv_18=(Token)match(input,31,FOLLOW_61); 

            			newLeafNode(otherlv_18, grammarAccess.getGuardedActionAccess().getEqualsSignKeyword_9());
            		
            // InternalABSReport.g:4078:3: ( (lv_guardExpression_19_0= ruleExpression ) )
            // InternalABSReport.g:4079:4: (lv_guardExpression_19_0= ruleExpression )
            {
            // InternalABSReport.g:4079:4: (lv_guardExpression_19_0= ruleExpression )
            // InternalABSReport.g:4080:5: lv_guardExpression_19_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getGuardedActionAccess().getGuardExpressionExpressionParserRuleCall_10_0());
            				
            pushFollow(FOLLOW_63);
            lv_guardExpression_19_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            					}
            					set(
            						current,
            						"guardExpression",
            						lv_guardExpression_19_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_20=(Token)match(input,69,FOLLOW_6); 

            			newLeafNode(otherlv_20, grammarAccess.getGuardedActionAccess().getActionKeyword_11());
            		
            otherlv_21=(Token)match(input,12,FOLLOW_64); 

            			newLeafNode(otherlv_21, grammarAccess.getGuardedActionAccess().getLeftCurlyBracketKeyword_12());
            		
            // InternalABSReport.g:4105:3: ( ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';' )*
            loop85:
            do {
                int alt85=2;
                int LA85_0 = input.LA(1);

                if ( ((LA85_0>=RULE_STRING && LA85_0<=RULE_ID)||LA85_0==27||LA85_0==32||LA85_0==35||(LA85_0>=61 && LA85_0<=62)) ) {
                    alt85=1;
                }


                switch (alt85) {
            	case 1 :
            	    // InternalABSReport.g:4106:4: ( (lv_guardActions_22_0= ruleAction ) ) otherlv_23= ';'
            	    {
            	    // InternalABSReport.g:4106:4: ( (lv_guardActions_22_0= ruleAction ) )
            	    // InternalABSReport.g:4107:5: (lv_guardActions_22_0= ruleAction )
            	    {
            	    // InternalABSReport.g:4107:5: (lv_guardActions_22_0= ruleAction )
            	    // InternalABSReport.g:4108:6: lv_guardActions_22_0= ruleAction
            	    {

            	    						newCompositeNode(grammarAccess.getGuardedActionAccess().getGuardActionsActionParserRuleCall_13_0_0());
            	    					
            	    pushFollow(FOLLOW_65);
            	    lv_guardActions_22_0=ruleAction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getGuardedActionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"guardActions",
            	    							lv_guardActions_22_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Action");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_23=(Token)match(input,70,FOLLOW_64); 

            	    				newLeafNode(otherlv_23, grammarAccess.getGuardedActionAccess().getSemicolonKeyword_13_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop85;
                }
            } while (true);

            otherlv_24=(Token)match(input,13,FOLLOW_14); 

            			newLeafNode(otherlv_24, grammarAccess.getGuardedActionAccess().getRightCurlyBracketKeyword_14());
            		
            otherlv_25=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_25, grammarAccess.getGuardedActionAccess().getRightCurlyBracketKeyword_15());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardedAction"


    // $ANTLR start "entryRuleGuardParameter"
    // InternalABSReport.g:4142:1: entryRuleGuardParameter returns [EObject current=null] : iv_ruleGuardParameter= ruleGuardParameter EOF ;
    public final EObject entryRuleGuardParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardParameter = null;


        try {
            // InternalABSReport.g:4142:55: (iv_ruleGuardParameter= ruleGuardParameter EOF )
            // InternalABSReport.g:4143:2: iv_ruleGuardParameter= ruleGuardParameter EOF
            {
             newCompositeNode(grammarAccess.getGuardParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuardParameter=ruleGuardParameter();

            state._fsp--;

             current =iv_ruleGuardParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardParameter"


    // $ANTLR start "ruleGuardParameter"
    // InternalABSReport.g:4149:1: ruleGuardParameter returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleGuardParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:4155:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) ) )
            // InternalABSReport.g:4156:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) )
            {
            // InternalABSReport.g:4156:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) )
            // InternalABSReport.g:4157:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( ( ruleQualifiedName ) )
            {
            // InternalABSReport.g:4157:3: ()
            // InternalABSReport.g:4158:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGuardParameterAccess().getGuardParameterAction_0(),
            					current);
            			

            }

            // InternalABSReport.g:4164:3: ( (lv_name_1_0= ruleEString ) )
            // InternalABSReport.g:4165:4: (lv_name_1_0= ruleEString )
            {
            // InternalABSReport.g:4165:4: (lv_name_1_0= ruleEString )
            // InternalABSReport.g:4166:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getGuardParameterAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_42);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGuardParameterRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,51,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getGuardParameterAccess().getColonKeyword_2());
            		
            // InternalABSReport.g:4187:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:4188:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:4188:4: ( ruleQualifiedName )
            // InternalABSReport.g:4189:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGuardParameterRule());
            					}
            				

            					newCompositeNode(grammarAccess.getGuardParameterAccess().getParameterTypeAssetTypeCrossReference_3_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardParameter"


    // $ANTLR start "entryRuleGoal"
    // InternalABSReport.g:4207:1: entryRuleGoal returns [EObject current=null] : iv_ruleGoal= ruleGoal EOF ;
    public final EObject entryRuleGoal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGoal = null;


        try {
            // InternalABSReport.g:4207:45: (iv_ruleGoal= ruleGoal EOF )
            // InternalABSReport.g:4208:2: iv_ruleGoal= ruleGoal EOF
            {
             newCompositeNode(grammarAccess.getGoalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGoal=ruleGoal();

            state._fsp--;

             current =iv_ruleGoal; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGoal"


    // $ANTLR start "ruleGoal"
    // InternalABSReport.g:4214:1: ruleGoal returns [EObject current=null] : ( () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}' ) ;
    public final EObject ruleGoal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_description_7_0=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        Enumerator lv_descriptionFormat_6_0 = null;

        EObject lv_annotations_10_0 = null;

        EObject lv_precondition_14_0 = null;

        EObject lv_postcondition_17_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:4220:2: ( ( () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}' ) )
            // InternalABSReport.g:4221:2: ( () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}' )
            {
            // InternalABSReport.g:4221:2: ( () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}' )
            // InternalABSReport.g:4222:3: () otherlv_1= 'Goal' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) ) (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )? (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )? otherlv_18= '}'
            {
            // InternalABSReport.g:4222:3: ()
            // InternalABSReport.g:4223:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGoalAccess().getGoalAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getGoalAccess().getGoalKeyword_1());
            		
            // InternalABSReport.g:4233:3: ( (lv_name_2_0= ruleEString ) )
            // InternalABSReport.g:4234:4: (lv_name_2_0= ruleEString )
            {
            // InternalABSReport.g:4234:4: (lv_name_2_0= ruleEString )
            // InternalABSReport.g:4235:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getGoalAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGoalRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_66); 

            			newLeafNode(otherlv_3, grammarAccess.getGoalAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalABSReport.g:4256:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) ) )
            // InternalABSReport.g:4257:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) )
            {
            // InternalABSReport.g:4257:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* ) )
            // InternalABSReport.g:4258:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getGoalAccess().getUnorderedGroup_4());
            				
            // InternalABSReport.g:4261:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )* )
            // InternalABSReport.g:4262:6: ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )*
            {
            // InternalABSReport.g:4262:6: ( ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) ) )*
            loop88:
            do {
                int alt88=3;
                int LA88_0 = input.LA(1);

                if ( LA88_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 0) ) {
                    alt88=1;
                }
                else if ( LA88_0 == 38 && getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 1) ) {
                    alt88=2;
                }


                switch (alt88) {
            	case 1 :
            	    // InternalABSReport.g:4263:4: ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalABSReport.g:4263:4: ({...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) ) )
            	    // InternalABSReport.g:4264:5: {...}? => ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleGoal", "getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 0)");
            	    }
            	    // InternalABSReport.g:4264:101: ( ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) ) )
            	    // InternalABSReport.g:4265:6: ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 0);
            	    					
            	    // InternalABSReport.g:4268:9: ({...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) ) )
            	    // InternalABSReport.g:4268:10: {...}? => (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGoal", "true");
            	    }
            	    // InternalABSReport.g:4268:19: (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )
            	    // InternalABSReport.g:4268:20: otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) )
            	    {
            	    otherlv_5=(Token)match(input,45,FOLLOW_39); 

            	    									newLeafNode(otherlv_5, grammarAccess.getGoalAccess().getDescriptionKeyword_4_0_0());
            	    								
            	    // InternalABSReport.g:4272:9: ( (lv_descriptionFormat_6_0= ruleTextFormat ) )?
            	    int alt86=2;
            	    int LA86_0 = input.LA(1);

            	    if ( ((LA86_0>=112 && LA86_0<=114)) ) {
            	        alt86=1;
            	    }
            	    switch (alt86) {
            	        case 1 :
            	            // InternalABSReport.g:4273:10: (lv_descriptionFormat_6_0= ruleTextFormat )
            	            {
            	            // InternalABSReport.g:4273:10: (lv_descriptionFormat_6_0= ruleTextFormat )
            	            // InternalABSReport.g:4274:11: lv_descriptionFormat_6_0= ruleTextFormat
            	            {

            	            											newCompositeNode(grammarAccess.getGoalAccess().getDescriptionFormatTextFormatEnumRuleCall_4_0_1_0());
            	            										
            	            pushFollow(FOLLOW_16);
            	            lv_descriptionFormat_6_0=ruleTextFormat();

            	            state._fsp--;


            	            											if (current==null) {
            	            												current = createModelElementForParent(grammarAccess.getGoalRule());
            	            											}
            	            											set(
            	            												current,
            	            												"descriptionFormat",
            	            												lv_descriptionFormat_6_0,
            	            												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
            	            											afterParserOrEnumRuleCall();
            	            										

            	            }


            	            }
            	            break;

            	    }

            	    // InternalABSReport.g:4291:9: ( (lv_description_7_0= RULE_STRING ) )
            	    // InternalABSReport.g:4292:10: (lv_description_7_0= RULE_STRING )
            	    {
            	    // InternalABSReport.g:4292:10: (lv_description_7_0= RULE_STRING )
            	    // InternalABSReport.g:4293:11: lv_description_7_0= RULE_STRING
            	    {
            	    lv_description_7_0=(Token)match(input,RULE_STRING,FOLLOW_66); 

            	    											newLeafNode(lv_description_7_0, grammarAccess.getGoalAccess().getDescriptionSTRINGTerminalRuleCall_4_0_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getGoalRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"description",
            	    												lv_description_7_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGoalAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalABSReport.g:4315:4: ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) )
            	    {
            	    // InternalABSReport.g:4315:4: ({...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) ) )
            	    // InternalABSReport.g:4316:5: {...}? => ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleGoal", "getUnorderedGroupHelper().canSelect(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 1)");
            	    }
            	    // InternalABSReport.g:4316:101: ( ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) ) )
            	    // InternalABSReport.g:4317:6: ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGoalAccess().getUnorderedGroup_4(), 1);
            	    					
            	    // InternalABSReport.g:4320:9: ({...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' ) )
            	    // InternalABSReport.g:4320:10: {...}? => (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGoal", "true");
            	    }
            	    // InternalABSReport.g:4320:19: (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )
            	    // InternalABSReport.g:4320:20: otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}'
            	    {
            	    otherlv_8=(Token)match(input,38,FOLLOW_6); 

            	    									newLeafNode(otherlv_8, grammarAccess.getGoalAccess().getAnnotationsKeyword_4_1_0());
            	    								
            	    otherlv_9=(Token)match(input,12,FOLLOW_32); 

            	    									newLeafNode(otherlv_9, grammarAccess.getGoalAccess().getLeftCurlyBracketKeyword_4_1_1());
            	    								
            	    // InternalABSReport.g:4328:9: ( (lv_annotations_10_0= ruleAnnotationEntry ) )*
            	    loop87:
            	    do {
            	        int alt87=2;
            	        int LA87_0 = input.LA(1);

            	        if ( (LA87_0==RULE_STRING||LA87_0==RULE_ID) ) {
            	            alt87=1;
            	        }


            	        switch (alt87) {
            	    	case 1 :
            	    	    // InternalABSReport.g:4329:10: (lv_annotations_10_0= ruleAnnotationEntry )
            	    	    {
            	    	    // InternalABSReport.g:4329:10: (lv_annotations_10_0= ruleAnnotationEntry )
            	    	    // InternalABSReport.g:4330:11: lv_annotations_10_0= ruleAnnotationEntry
            	    	    {

            	    	    											newCompositeNode(grammarAccess.getGoalAccess().getAnnotationsAnnotationEntryParserRuleCall_4_1_2_0());
            	    	    										
            	    	    pushFollow(FOLLOW_32);
            	    	    lv_annotations_10_0=ruleAnnotationEntry();

            	    	    state._fsp--;


            	    	    											if (current==null) {
            	    	    												current = createModelElementForParent(grammarAccess.getGoalRule());
            	    	    											}
            	    	    											add(
            	    	    												current,
            	    	    												"annotations",
            	    	    												lv_annotations_10_0,
            	    	    												"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
            	    	    											afterParserOrEnumRuleCall();
            	    	    										

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop87;
            	        }
            	    } while (true);

            	    otherlv_11=(Token)match(input,13,FOLLOW_66); 

            	    									newLeafNode(otherlv_11, grammarAccess.getGoalAccess().getRightCurlyBracketKeyword_4_1_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGoalAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop88;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getGoalAccess().getUnorderedGroup_4());
            				

            }

            // InternalABSReport.g:4364:3: (otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) ) )?
            int alt89=2;
            int LA89_0 = input.LA(1);

            if ( (LA89_0==71) ) {
                alt89=1;
            }
            switch (alt89) {
                case 1 :
                    // InternalABSReport.g:4365:4: otherlv_12= 'pre' otherlv_13= '=' ( (lv_precondition_14_0= ruleExpression ) )
                    {
                    otherlv_12=(Token)match(input,71,FOLLOW_25); 

                    				newLeafNode(otherlv_12, grammarAccess.getGoalAccess().getPreKeyword_5_0());
                    			
                    otherlv_13=(Token)match(input,31,FOLLOW_61); 

                    				newLeafNode(otherlv_13, grammarAccess.getGoalAccess().getEqualsSignKeyword_5_1());
                    			
                    // InternalABSReport.g:4373:4: ( (lv_precondition_14_0= ruleExpression ) )
                    // InternalABSReport.g:4374:5: (lv_precondition_14_0= ruleExpression )
                    {
                    // InternalABSReport.g:4374:5: (lv_precondition_14_0= ruleExpression )
                    // InternalABSReport.g:4375:6: lv_precondition_14_0= ruleExpression
                    {

                    						newCompositeNode(grammarAccess.getGoalAccess().getPreconditionExpressionParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_67);
                    lv_precondition_14_0=ruleExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGoalRule());
                    						}
                    						set(
                    							current,
                    							"precondition",
                    							lv_precondition_14_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalABSReport.g:4393:3: (otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) ) )?
            int alt90=2;
            int LA90_0 = input.LA(1);

            if ( (LA90_0==72) ) {
                alt90=1;
            }
            switch (alt90) {
                case 1 :
                    // InternalABSReport.g:4394:4: otherlv_15= 'post' otherlv_16= '=' ( (lv_postcondition_17_0= ruleExpression ) )
                    {
                    otherlv_15=(Token)match(input,72,FOLLOW_25); 

                    				newLeafNode(otherlv_15, grammarAccess.getGoalAccess().getPostKeyword_6_0());
                    			
                    otherlv_16=(Token)match(input,31,FOLLOW_61); 

                    				newLeafNode(otherlv_16, grammarAccess.getGoalAccess().getEqualsSignKeyword_6_1());
                    			
                    // InternalABSReport.g:4402:4: ( (lv_postcondition_17_0= ruleExpression ) )
                    // InternalABSReport.g:4403:5: (lv_postcondition_17_0= ruleExpression )
                    {
                    // InternalABSReport.g:4403:5: (lv_postcondition_17_0= ruleExpression )
                    // InternalABSReport.g:4404:6: lv_postcondition_17_0= ruleExpression
                    {

                    						newCompositeNode(grammarAccess.getGoalAccess().getPostconditionExpressionParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_14);
                    lv_postcondition_17_0=ruleExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGoalRule());
                    						}
                    						set(
                    							current,
                    							"postcondition",
                    							lv_postcondition_17_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_18=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_18, grammarAccess.getGoalAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGoal"


    // $ANTLR start "entryRuleAnnotationEntry"
    // InternalABSReport.g:4430:1: entryRuleAnnotationEntry returns [EObject current=null] : iv_ruleAnnotationEntry= ruleAnnotationEntry EOF ;
    public final EObject entryRuleAnnotationEntry() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotationEntry = null;


        try {
            // InternalABSReport.g:4430:56: (iv_ruleAnnotationEntry= ruleAnnotationEntry EOF )
            // InternalABSReport.g:4431:2: iv_ruleAnnotationEntry= ruleAnnotationEntry EOF
            {
             newCompositeNode(grammarAccess.getAnnotationEntryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnnotationEntry=ruleAnnotationEntry();

            state._fsp--;

             current =iv_ruleAnnotationEntry; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotationEntry"


    // $ANTLR start "ruleAnnotationEntry"
    // InternalABSReport.g:4437:1: ruleAnnotationEntry returns [EObject current=null] : ( () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) ;
    public final EObject ruleAnnotationEntry() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_value_3_0=null;


        	enterRule();

        try {
            // InternalABSReport.g:4443:2: ( ( () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) )
            // InternalABSReport.g:4444:2: ( () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
            {
            // InternalABSReport.g:4444:2: ( () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
            // InternalABSReport.g:4445:3: () ( ( ruleEString ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) )
            {
            // InternalABSReport.g:4445:3: ()
            // InternalABSReport.g:4446:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAnnotationEntryAccess().getAnnotationEntryAction_0(),
            					current);
            			

            }

            // InternalABSReport.g:4452:3: ( ( ruleEString ) )
            // InternalABSReport.g:4453:4: ( ruleEString )
            {
            // InternalABSReport.g:4453:4: ( ruleEString )
            // InternalABSReport.g:4454:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAnnotationEntryRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAnnotationEntryAccess().getKeyAnnotationKeyCrossReference_1_0());
            				
            pushFollow(FOLLOW_25);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,31,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getAnnotationEntryAccess().getEqualsSignKeyword_2());
            		
            // InternalABSReport.g:4472:3: ( (lv_value_3_0= RULE_STRING ) )
            // InternalABSReport.g:4473:4: (lv_value_3_0= RULE_STRING )
            {
            // InternalABSReport.g:4473:4: (lv_value_3_0= RULE_STRING )
            // InternalABSReport.g:4474:5: lv_value_3_0= RULE_STRING
            {
            lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_value_3_0, grammarAccess.getAnnotationEntryAccess().getValueSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAnnotationEntryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_3_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotationEntry"


    // $ANTLR start "entryRuleExpression"
    // InternalABSReport.g:4494:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalABSReport.g:4494:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalABSReport.g:4495:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalABSReport.g:4501:1: ruleExpression returns [EObject current=null] : this_ImpliesExpression_0= ruleImpliesExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ImpliesExpression_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:4507:2: (this_ImpliesExpression_0= ruleImpliesExpression )
            // InternalABSReport.g:4508:2: this_ImpliesExpression_0= ruleImpliesExpression
            {

            		newCompositeNode(grammarAccess.getExpressionAccess().getImpliesExpressionParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_ImpliesExpression_0=ruleImpliesExpression();

            state._fsp--;


            		current = this_ImpliesExpression_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleImpliesExpression"
    // InternalABSReport.g:4519:1: entryRuleImpliesExpression returns [EObject current=null] : iv_ruleImpliesExpression= ruleImpliesExpression EOF ;
    public final EObject entryRuleImpliesExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImpliesExpression = null;


        try {
            // InternalABSReport.g:4519:58: (iv_ruleImpliesExpression= ruleImpliesExpression EOF )
            // InternalABSReport.g:4520:2: iv_ruleImpliesExpression= ruleImpliesExpression EOF
            {
             newCompositeNode(grammarAccess.getImpliesExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImpliesExpression=ruleImpliesExpression();

            state._fsp--;

             current =iv_ruleImpliesExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImpliesExpression"


    // $ANTLR start "ruleImpliesExpression"
    // InternalABSReport.g:4526:1: ruleImpliesExpression returns [EObject current=null] : (this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )* ) ;
    public final EObject ruleImpliesExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_OrExpression_0 = null;

        EObject lv_rhs_3_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:4532:2: ( (this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )* ) )
            // InternalABSReport.g:4533:2: (this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )* )
            {
            // InternalABSReport.g:4533:2: (this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )* )
            // InternalABSReport.g:4534:3: this_OrExpression_0= ruleOrExpression ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getImpliesExpressionAccess().getOrExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_68);
            this_OrExpression_0=ruleOrExpression();

            state._fsp--;


            			current = this_OrExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalABSReport.g:4542:3: ( () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) ) )*
            loop91:
            do {
                int alt91=2;
                int LA91_0 = input.LA(1);

                if ( (LA91_0==73) ) {
                    alt91=1;
                }


                switch (alt91) {
            	case 1 :
            	    // InternalABSReport.g:4543:4: () otherlv_2= '=>' ( (lv_rhs_3_0= ruleOrExpression ) )
            	    {
            	    // InternalABSReport.g:4543:4: ()
            	    // InternalABSReport.g:4544:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLhsAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,73,FOLLOW_61); 

            	    				newLeafNode(otherlv_2, grammarAccess.getImpliesExpressionAccess().getEqualsSignGreaterThanSignKeyword_1_1());
            	    			
            	    // InternalABSReport.g:4554:4: ( (lv_rhs_3_0= ruleOrExpression ) )
            	    // InternalABSReport.g:4555:5: (lv_rhs_3_0= ruleOrExpression )
            	    {
            	    // InternalABSReport.g:4555:5: (lv_rhs_3_0= ruleOrExpression )
            	    // InternalABSReport.g:4556:6: lv_rhs_3_0= ruleOrExpression
            	    {

            	    						newCompositeNode(grammarAccess.getImpliesExpressionAccess().getRhsOrExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_68);
            	    lv_rhs_3_0=ruleOrExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getImpliesExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rhs",
            	    							lv_rhs_3_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.OrExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop91;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImpliesExpression"


    // $ANTLR start "entryRuleOrExpression"
    // InternalABSReport.g:4578:1: entryRuleOrExpression returns [EObject current=null] : iv_ruleOrExpression= ruleOrExpression EOF ;
    public final EObject entryRuleOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrExpression = null;


        try {
            // InternalABSReport.g:4578:53: (iv_ruleOrExpression= ruleOrExpression EOF )
            // InternalABSReport.g:4579:2: iv_ruleOrExpression= ruleOrExpression EOF
            {
             newCompositeNode(grammarAccess.getOrExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOrExpression=ruleOrExpression();

            state._fsp--;

             current =iv_ruleOrExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrExpression"


    // $ANTLR start "ruleOrExpression"
    // InternalABSReport.g:4585:1: ruleOrExpression returns [EObject current=null] : (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )* ) ;
    public final EObject ruleOrExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_AndExpression_0 = null;

        EObject lv_rhs_3_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:4591:2: ( (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )* ) )
            // InternalABSReport.g:4592:2: (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )* )
            {
            // InternalABSReport.g:4592:2: (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )* )
            // InternalABSReport.g:4593:3: this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrExpressionAccess().getAndExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_69);
            this_AndExpression_0=ruleAndExpression();

            state._fsp--;


            			current = this_AndExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalABSReport.g:4601:3: ( () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) ) )*
            loop92:
            do {
                int alt92=2;
                int LA92_0 = input.LA(1);

                if ( (LA92_0==74) ) {
                    alt92=1;
                }


                switch (alt92) {
            	case 1 :
            	    // InternalABSReport.g:4602:4: () otherlv_2= '||' ( (lv_rhs_3_0= ruleAndExpression ) )
            	    {
            	    // InternalABSReport.g:4602:4: ()
            	    // InternalABSReport.g:4603:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrExpressionAccess().getOrExpressionLhsAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,74,FOLLOW_61); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrExpressionAccess().getVerticalLineVerticalLineKeyword_1_1());
            	    			
            	    // InternalABSReport.g:4613:4: ( (lv_rhs_3_0= ruleAndExpression ) )
            	    // InternalABSReport.g:4614:5: (lv_rhs_3_0= ruleAndExpression )
            	    {
            	    // InternalABSReport.g:4614:5: (lv_rhs_3_0= ruleAndExpression )
            	    // InternalABSReport.g:4615:6: lv_rhs_3_0= ruleAndExpression
            	    {

            	    						newCompositeNode(grammarAccess.getOrExpressionAccess().getRhsAndExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_69);
            	    lv_rhs_3_0=ruleAndExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rhs",
            	    							lv_rhs_3_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AndExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop92;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrExpression"


    // $ANTLR start "entryRuleAndExpression"
    // InternalABSReport.g:4637:1: entryRuleAndExpression returns [EObject current=null] : iv_ruleAndExpression= ruleAndExpression EOF ;
    public final EObject entryRuleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpression = null;


        try {
            // InternalABSReport.g:4637:54: (iv_ruleAndExpression= ruleAndExpression EOF )
            // InternalABSReport.g:4638:2: iv_ruleAndExpression= ruleAndExpression EOF
            {
             newCompositeNode(grammarAccess.getAndExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAndExpression=ruleAndExpression();

            state._fsp--;

             current =iv_ruleAndExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpression"


    // $ANTLR start "ruleAndExpression"
    // InternalABSReport.g:4644:1: ruleAndExpression returns [EObject current=null] : (this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )* ) ;
    public final EObject ruleAndExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_NotExpression_0 = null;

        EObject lv_rhs_3_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:4650:2: ( (this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )* ) )
            // InternalABSReport.g:4651:2: (this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )* )
            {
            // InternalABSReport.g:4651:2: (this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )* )
            // InternalABSReport.g:4652:3: this_NotExpression_0= ruleNotExpression ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndExpressionAccess().getNotExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_70);
            this_NotExpression_0=ruleNotExpression();

            state._fsp--;


            			current = this_NotExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalABSReport.g:4660:3: ( () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) ) )*
            loop93:
            do {
                int alt93=2;
                int LA93_0 = input.LA(1);

                if ( (LA93_0==75) ) {
                    alt93=1;
                }


                switch (alt93) {
            	case 1 :
            	    // InternalABSReport.g:4661:4: () otherlv_2= '&&' ( (lv_rhs_3_0= ruleNotExpression ) )
            	    {
            	    // InternalABSReport.g:4661:4: ()
            	    // InternalABSReport.g:4662:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndExpressionAccess().getAndExpressionLhsAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,75,FOLLOW_61); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAndExpressionAccess().getAmpersandAmpersandKeyword_1_1());
            	    			
            	    // InternalABSReport.g:4672:4: ( (lv_rhs_3_0= ruleNotExpression ) )
            	    // InternalABSReport.g:4673:5: (lv_rhs_3_0= ruleNotExpression )
            	    {
            	    // InternalABSReport.g:4673:5: (lv_rhs_3_0= ruleNotExpression )
            	    // InternalABSReport.g:4674:6: lv_rhs_3_0= ruleNotExpression
            	    {

            	    						newCompositeNode(grammarAccess.getAndExpressionAccess().getRhsNotExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_70);
            	    lv_rhs_3_0=ruleNotExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rhs",
            	    							lv_rhs_3_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.NotExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop93;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpression"


    // $ANTLR start "entryRuleNotExpression"
    // InternalABSReport.g:4696:1: entryRuleNotExpression returns [EObject current=null] : iv_ruleNotExpression= ruleNotExpression EOF ;
    public final EObject entryRuleNotExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotExpression = null;


        try {
            // InternalABSReport.g:4696:54: (iv_ruleNotExpression= ruleNotExpression EOF )
            // InternalABSReport.g:4697:2: iv_ruleNotExpression= ruleNotExpression EOF
            {
             newCompositeNode(grammarAccess.getNotExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNotExpression=ruleNotExpression();

            state._fsp--;

             current =iv_ruleNotExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotExpression"


    // $ANTLR start "ruleNotExpression"
    // InternalABSReport.g:4703:1: ruleNotExpression returns [EObject current=null] : ( ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) ) | this_ComparisonExpression_3= ruleComparisonExpression ) ;
    public final EObject ruleNotExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;

        EObject this_ComparisonExpression_3 = null;



        	enterRule();

        try {
            // InternalABSReport.g:4709:2: ( ( ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) ) | this_ComparisonExpression_3= ruleComparisonExpression ) )
            // InternalABSReport.g:4710:2: ( ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) ) | this_ComparisonExpression_3= ruleComparisonExpression )
            {
            // InternalABSReport.g:4710:2: ( ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) ) | this_ComparisonExpression_3= ruleComparisonExpression )
            int alt94=2;
            int LA94_0 = input.LA(1);

            if ( (LA94_0==76) ) {
                alt94=1;
            }
            else if ( ((LA94_0>=RULE_STRING && LA94_0<=RULE_ID)||LA94_0==27||LA94_0==32||LA94_0==35||(LA94_0>=61 && LA94_0<=62)) ) {
                alt94=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 94, 0, input);

                throw nvae;
            }
            switch (alt94) {
                case 1 :
                    // InternalABSReport.g:4711:3: ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) )
                    {
                    // InternalABSReport.g:4711:3: ( () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) ) )
                    // InternalABSReport.g:4712:4: () otherlv_1= '!' ( (lv_expression_2_0= ruleNotExpression ) )
                    {
                    // InternalABSReport.g:4712:4: ()
                    // InternalABSReport.g:4713:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getNotExpressionAccess().getNotExpressionAction_0_0(),
                    						current);
                    				

                    }

                    otherlv_1=(Token)match(input,76,FOLLOW_61); 

                    				newLeafNode(otherlv_1, grammarAccess.getNotExpressionAccess().getExclamationMarkKeyword_0_1());
                    			
                    // InternalABSReport.g:4723:4: ( (lv_expression_2_0= ruleNotExpression ) )
                    // InternalABSReport.g:4724:5: (lv_expression_2_0= ruleNotExpression )
                    {
                    // InternalABSReport.g:4724:5: (lv_expression_2_0= ruleNotExpression )
                    // InternalABSReport.g:4725:6: lv_expression_2_0= ruleNotExpression
                    {

                    						newCompositeNode(grammarAccess.getNotExpressionAccess().getExpressionNotExpressionParserRuleCall_0_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_expression_2_0=ruleNotExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getNotExpressionRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_2_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.NotExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:4744:3: this_ComparisonExpression_3= ruleComparisonExpression
                    {

                    			newCompositeNode(grammarAccess.getNotExpressionAccess().getComparisonExpressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ComparisonExpression_3=ruleComparisonExpression();

                    state._fsp--;


                    			current = this_ComparisonExpression_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotExpression"


    // $ANTLR start "entryRuleComparisonExpression"
    // InternalABSReport.g:4756:1: entryRuleComparisonExpression returns [EObject current=null] : iv_ruleComparisonExpression= ruleComparisonExpression EOF ;
    public final EObject entryRuleComparisonExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparisonExpression = null;


        try {
            // InternalABSReport.g:4756:61: (iv_ruleComparisonExpression= ruleComparisonExpression EOF )
            // InternalABSReport.g:4757:2: iv_ruleComparisonExpression= ruleComparisonExpression EOF
            {
             newCompositeNode(grammarAccess.getComparisonExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComparisonExpression=ruleComparisonExpression();

            state._fsp--;

             current =iv_ruleComparisonExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparisonExpression"


    // $ANTLR start "ruleComparisonExpression"
    // InternalABSReport.g:4763:1: ruleComparisonExpression returns [EObject current=null] : (this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) ) ) ;
    public final EObject ruleComparisonExpression() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        Token lv_op_5_1=null;
        Token lv_op_5_2=null;
        Token lv_op_5_3=null;
        Token lv_op_5_4=null;
        EObject this_SelectionExpression_0 = null;

        EObject lv_rhs_3_0 = null;

        EObject lv_rhs_6_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:4769:2: ( (this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) ) ) )
            // InternalABSReport.g:4770:2: (this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) ) )
            {
            // InternalABSReport.g:4770:2: (this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) ) )
            // InternalABSReport.g:4771:3: this_SelectionExpression_0= ruleSelectionExpression ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) )
            {

            			newCompositeNode(grammarAccess.getComparisonExpressionAccess().getSelectionExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_71);
            this_SelectionExpression_0=ruleSelectionExpression();

            state._fsp--;


            			current = this_SelectionExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalABSReport.g:4779:3: ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )* | ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) ) )
            int alt98=2;
            int LA98_0 = input.LA(1);

            if ( (LA98_0==EOF||LA98_0==13||LA98_0==26||LA98_0==28||LA98_0==33||LA98_0==69||(LA98_0>=72 && LA98_0<=75)||(LA98_0>=77 && LA98_0<=78)) ) {
                alt98=1;
            }
            else if ( ((LA98_0>=79 && LA98_0<=82)) ) {
                alt98=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 98, 0, input);

                throw nvae;
            }
            switch (alt98) {
                case 1 :
                    // InternalABSReport.g:4780:4: ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )*
                    {
                    // InternalABSReport.g:4780:4: ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) ) )*
                    loop96:
                    do {
                        int alt96=2;
                        int LA96_0 = input.LA(1);

                        if ( ((LA96_0>=77 && LA96_0<=78)) ) {
                            alt96=1;
                        }


                        switch (alt96) {
                    	case 1 :
                    	    // InternalABSReport.g:4781:5: () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_rhs_3_0= ruleSelectionExpression ) )
                    	    {
                    	    // InternalABSReport.g:4781:5: ()
                    	    // InternalABSReport.g:4782:6: 
                    	    {

                    	    						current = forceCreateModelElementAndSet(
                    	    							grammarAccess.getComparisonExpressionAccess().getEqualityComparisonExpressionLhsAction_1_0_0(),
                    	    							current);
                    	    					

                    	    }

                    	    // InternalABSReport.g:4788:5: ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) )
                    	    // InternalABSReport.g:4789:6: ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) )
                    	    {
                    	    // InternalABSReport.g:4789:6: ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) )
                    	    // InternalABSReport.g:4790:7: (lv_op_2_1= '==' | lv_op_2_2= '!=' )
                    	    {
                    	    // InternalABSReport.g:4790:7: (lv_op_2_1= '==' | lv_op_2_2= '!=' )
                    	    int alt95=2;
                    	    int LA95_0 = input.LA(1);

                    	    if ( (LA95_0==77) ) {
                    	        alt95=1;
                    	    }
                    	    else if ( (LA95_0==78) ) {
                    	        alt95=2;
                    	    }
                    	    else {
                    	        NoViableAltException nvae =
                    	            new NoViableAltException("", 95, 0, input);

                    	        throw nvae;
                    	    }
                    	    switch (alt95) {
                    	        case 1 :
                    	            // InternalABSReport.g:4791:8: lv_op_2_1= '=='
                    	            {
                    	            lv_op_2_1=(Token)match(input,77,FOLLOW_61); 

                    	            								newLeafNode(lv_op_2_1, grammarAccess.getComparisonExpressionAccess().getOpEqualsSignEqualsSignKeyword_1_0_1_0_0());
                    	            							

                    	            								if (current==null) {
                    	            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                    	            								}
                    	            								setWithLastConsumed(current, "op", lv_op_2_1, null);
                    	            							

                    	            }
                    	            break;
                    	        case 2 :
                    	            // InternalABSReport.g:4802:8: lv_op_2_2= '!='
                    	            {
                    	            lv_op_2_2=(Token)match(input,78,FOLLOW_61); 

                    	            								newLeafNode(lv_op_2_2, grammarAccess.getComparisonExpressionAccess().getOpExclamationMarkEqualsSignKeyword_1_0_1_0_1());
                    	            							

                    	            								if (current==null) {
                    	            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                    	            								}
                    	            								setWithLastConsumed(current, "op", lv_op_2_2, null);
                    	            							

                    	            }
                    	            break;

                    	    }


                    	    }


                    	    }

                    	    // InternalABSReport.g:4815:5: ( (lv_rhs_3_0= ruleSelectionExpression ) )
                    	    // InternalABSReport.g:4816:6: (lv_rhs_3_0= ruleSelectionExpression )
                    	    {
                    	    // InternalABSReport.g:4816:6: (lv_rhs_3_0= ruleSelectionExpression )
                    	    // InternalABSReport.g:4817:7: lv_rhs_3_0= ruleSelectionExpression
                    	    {

                    	    							newCompositeNode(grammarAccess.getComparisonExpressionAccess().getRhsSelectionExpressionParserRuleCall_1_0_2_0());
                    	    						
                    	    pushFollow(FOLLOW_72);
                    	    lv_rhs_3_0=ruleSelectionExpression();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getComparisonExpressionRule());
                    	    							}
                    	    							set(
                    	    								current,
                    	    								"rhs",
                    	    								lv_rhs_3_0,
                    	    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.SelectionExpression");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop96;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:4836:4: ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) )
                    {
                    // InternalABSReport.g:4836:4: ( () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) ) )
                    // InternalABSReport.g:4837:5: () ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) ) ( (lv_rhs_6_0= ruleSelectionExpression ) )
                    {
                    // InternalABSReport.g:4837:5: ()
                    // InternalABSReport.g:4838:6: 
                    {

                    						current = forceCreateModelElementAndSet(
                    							grammarAccess.getComparisonExpressionAccess().getInequalityComparisonExpressionLhsAction_1_1_0(),
                    							current);
                    					

                    }

                    // InternalABSReport.g:4844:5: ( ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) ) )
                    // InternalABSReport.g:4845:6: ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) )
                    {
                    // InternalABSReport.g:4845:6: ( (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' ) )
                    // InternalABSReport.g:4846:7: (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' )
                    {
                    // InternalABSReport.g:4846:7: (lv_op_5_1= '<' | lv_op_5_2= '<=' | lv_op_5_3= '>' | lv_op_5_4= '>=' )
                    int alt97=4;
                    switch ( input.LA(1) ) {
                    case 79:
                        {
                        alt97=1;
                        }
                        break;
                    case 80:
                        {
                        alt97=2;
                        }
                        break;
                    case 81:
                        {
                        alt97=3;
                        }
                        break;
                    case 82:
                        {
                        alt97=4;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 97, 0, input);

                        throw nvae;
                    }

                    switch (alt97) {
                        case 1 :
                            // InternalABSReport.g:4847:8: lv_op_5_1= '<'
                            {
                            lv_op_5_1=(Token)match(input,79,FOLLOW_61); 

                            								newLeafNode(lv_op_5_1, grammarAccess.getComparisonExpressionAccess().getOpLessThanSignKeyword_1_1_1_0_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                            								}
                            								setWithLastConsumed(current, "op", lv_op_5_1, null);
                            							

                            }
                            break;
                        case 2 :
                            // InternalABSReport.g:4858:8: lv_op_5_2= '<='
                            {
                            lv_op_5_2=(Token)match(input,80,FOLLOW_61); 

                            								newLeafNode(lv_op_5_2, grammarAccess.getComparisonExpressionAccess().getOpLessThanSignEqualsSignKeyword_1_1_1_0_1());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                            								}
                            								setWithLastConsumed(current, "op", lv_op_5_2, null);
                            							

                            }
                            break;
                        case 3 :
                            // InternalABSReport.g:4869:8: lv_op_5_3= '>'
                            {
                            lv_op_5_3=(Token)match(input,81,FOLLOW_61); 

                            								newLeafNode(lv_op_5_3, grammarAccess.getComparisonExpressionAccess().getOpGreaterThanSignKeyword_1_1_1_0_2());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                            								}
                            								setWithLastConsumed(current, "op", lv_op_5_3, null);
                            							

                            }
                            break;
                        case 4 :
                            // InternalABSReport.g:4880:8: lv_op_5_4= '>='
                            {
                            lv_op_5_4=(Token)match(input,82,FOLLOW_61); 

                            								newLeafNode(lv_op_5_4, grammarAccess.getComparisonExpressionAccess().getOpGreaterThanSignEqualsSignKeyword_1_1_1_0_3());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getComparisonExpressionRule());
                            								}
                            								setWithLastConsumed(current, "op", lv_op_5_4, null);
                            							

                            }
                            break;

                    }


                    }


                    }

                    // InternalABSReport.g:4893:5: ( (lv_rhs_6_0= ruleSelectionExpression ) )
                    // InternalABSReport.g:4894:6: (lv_rhs_6_0= ruleSelectionExpression )
                    {
                    // InternalABSReport.g:4894:6: (lv_rhs_6_0= ruleSelectionExpression )
                    // InternalABSReport.g:4895:7: lv_rhs_6_0= ruleSelectionExpression
                    {

                    							newCompositeNode(grammarAccess.getComparisonExpressionAccess().getRhsSelectionExpressionParserRuleCall_1_1_2_0());
                    						
                    pushFollow(FOLLOW_2);
                    lv_rhs_6_0=ruleSelectionExpression();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getComparisonExpressionRule());
                    							}
                    							set(
                    								current,
                    								"rhs",
                    								lv_rhs_6_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.SelectionExpression");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparisonExpression"


    // $ANTLR start "entryRuleSelectionExpression"
    // InternalABSReport.g:4918:1: entryRuleSelectionExpression returns [EObject current=null] : iv_ruleSelectionExpression= ruleSelectionExpression EOF ;
    public final EObject entryRuleSelectionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSelectionExpression = null;


        try {
            // InternalABSReport.g:4918:60: (iv_ruleSelectionExpression= ruleSelectionExpression EOF )
            // InternalABSReport.g:4919:2: iv_ruleSelectionExpression= ruleSelectionExpression EOF
            {
             newCompositeNode(grammarAccess.getSelectionExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSelectionExpression=ruleSelectionExpression();

            state._fsp--;

             current =iv_ruleSelectionExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSelectionExpression"


    // $ANTLR start "ruleSelectionExpression"
    // InternalABSReport.g:4925:1: ruleSelectionExpression returns [EObject current=null] : (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )* ) ;
    public final EObject ruleSelectionExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_methodInvocation_4_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject this_TerminalExpression_0 = null;

        EObject lv_args_5_0 = null;

        EObject lv_args_7_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:4931:2: ( (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )* ) )
            // InternalABSReport.g:4932:2: (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )* )
            {
            // InternalABSReport.g:4932:2: (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )* )
            // InternalABSReport.g:4933:3: this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )*
            {

            			newCompositeNode(grammarAccess.getSelectionExpressionAccess().getTerminalExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_73);
            this_TerminalExpression_0=ruleTerminalExpression();

            state._fsp--;


            			current = this_TerminalExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalABSReport.g:4941:3: ( () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? )*
            loop102:
            do {
                int alt102=2;
                int LA102_0 = input.LA(1);

                if ( (LA102_0==83) ) {
                    alt102=1;
                }


                switch (alt102) {
            	case 1 :
            	    // InternalABSReport.g:4942:4: () otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )?
            	    {
            	    // InternalABSReport.g:4942:4: ()
            	    // InternalABSReport.g:4943:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getSelectionExpressionAccess().getMemberSelectionReceiverAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,83,FOLLOW_5); 

            	    				newLeafNode(otherlv_2, grammarAccess.getSelectionExpressionAccess().getFullStopKeyword_1_1());
            	    			
            	    // InternalABSReport.g:4953:4: ( ( ruleQualifiedName ) )
            	    // InternalABSReport.g:4954:5: ( ruleQualifiedName )
            	    {
            	    // InternalABSReport.g:4954:5: ( ruleQualifiedName )
            	    // InternalABSReport.g:4955:6: ruleQualifiedName
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getSelectionExpressionRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getSelectionExpressionAccess().getMemberMemberCrossReference_1_2_0());
            	    					
            	    pushFollow(FOLLOW_74);
            	    ruleQualifiedName();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalABSReport.g:4969:4: ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )?
            	    int alt101=2;
            	    int LA101_0 = input.LA(1);

            	    if ( (LA101_0==27) ) {
            	        alt101=1;
            	    }
            	    switch (alt101) {
            	        case 1 :
            	            // InternalABSReport.g:4970:5: ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')'
            	            {
            	            // InternalABSReport.g:4970:5: ( (lv_methodInvocation_4_0= '(' ) )
            	            // InternalABSReport.g:4971:6: (lv_methodInvocation_4_0= '(' )
            	            {
            	            // InternalABSReport.g:4971:6: (lv_methodInvocation_4_0= '(' )
            	            // InternalABSReport.g:4972:7: lv_methodInvocation_4_0= '('
            	            {
            	            lv_methodInvocation_4_0=(Token)match(input,27,FOLLOW_75); 

            	            							newLeafNode(lv_methodInvocation_4_0, grammarAccess.getSelectionExpressionAccess().getMethodInvocationLeftParenthesisKeyword_1_3_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getSelectionExpressionRule());
            	            							}
            	            							setWithLastConsumed(current, "methodInvocation", lv_methodInvocation_4_0 != null, "(");
            	            						

            	            }


            	            }

            	            // InternalABSReport.g:4984:5: ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )?
            	            int alt100=2;
            	            int LA100_0 = input.LA(1);

            	            if ( ((LA100_0>=RULE_STRING && LA100_0<=RULE_ID)||LA100_0==12||LA100_0==27||LA100_0==32||LA100_0==35||(LA100_0>=61 && LA100_0<=62)||LA100_0==76) ) {
            	                alt100=1;
            	            }
            	            switch (alt100) {
            	                case 1 :
            	                    // InternalABSReport.g:4985:6: ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )*
            	                    {
            	                    // InternalABSReport.g:4985:6: ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) )
            	                    // InternalABSReport.g:4986:7: (lv_args_5_0= ruleExpressionOrLambdaExpression )
            	                    {
            	                    // InternalABSReport.g:4986:7: (lv_args_5_0= ruleExpressionOrLambdaExpression )
            	                    // InternalABSReport.g:4987:8: lv_args_5_0= ruleExpressionOrLambdaExpression
            	                    {

            	                    								newCompositeNode(grammarAccess.getSelectionExpressionAccess().getArgsExpressionOrLambdaExpressionParserRuleCall_1_3_1_0_0());
            	                    							
            	                    pushFollow(FOLLOW_21);
            	                    lv_args_5_0=ruleExpressionOrLambdaExpression();

            	                    state._fsp--;


            	                    								if (current==null) {
            	                    									current = createModelElementForParent(grammarAccess.getSelectionExpressionRule());
            	                    								}
            	                    								add(
            	                    									current,
            	                    									"args",
            	                    									lv_args_5_0,
            	                    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ExpressionOrLambdaExpression");
            	                    								afterParserOrEnumRuleCall();
            	                    							

            	                    }


            	                    }

            	                    // InternalABSReport.g:5004:6: (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )*
            	                    loop99:
            	                    do {
            	                        int alt99=2;
            	                        int LA99_0 = input.LA(1);

            	                        if ( (LA99_0==26) ) {
            	                            alt99=1;
            	                        }


            	                        switch (alt99) {
            	                    	case 1 :
            	                    	    // InternalABSReport.g:5005:7: otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) )
            	                    	    {
            	                    	    otherlv_6=(Token)match(input,26,FOLLOW_61); 

            	                    	    							newLeafNode(otherlv_6, grammarAccess.getSelectionExpressionAccess().getCommaKeyword_1_3_1_1_0());
            	                    	    						
            	                    	    // InternalABSReport.g:5009:7: ( (lv_args_7_0= ruleExpression ) )
            	                    	    // InternalABSReport.g:5010:8: (lv_args_7_0= ruleExpression )
            	                    	    {
            	                    	    // InternalABSReport.g:5010:8: (lv_args_7_0= ruleExpression )
            	                    	    // InternalABSReport.g:5011:9: lv_args_7_0= ruleExpression
            	                    	    {

            	                    	    									newCompositeNode(grammarAccess.getSelectionExpressionAccess().getArgsExpressionParserRuleCall_1_3_1_1_1_0());
            	                    	    								
            	                    	    pushFollow(FOLLOW_21);
            	                    	    lv_args_7_0=ruleExpression();

            	                    	    state._fsp--;


            	                    	    									if (current==null) {
            	                    	    										current = createModelElementForParent(grammarAccess.getSelectionExpressionRule());
            	                    	    									}
            	                    	    									add(
            	                    	    										current,
            	                    	    										"args",
            	                    	    										lv_args_7_0,
            	                    	    										"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            	                    	    									afterParserOrEnumRuleCall();
            	                    	    								

            	                    	    }


            	                    	    }


            	                    	    }
            	                    	    break;

            	                    	default :
            	                    	    break loop99;
            	                        }
            	                    } while (true);


            	                    }
            	                    break;

            	            }

            	            otherlv_8=(Token)match(input,28,FOLLOW_73); 

            	            					newLeafNode(otherlv_8, grammarAccess.getSelectionExpressionAccess().getRightParenthesisKeyword_1_3_2());
            	            				

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop102;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSelectionExpression"


    // $ANTLR start "entryRuleExpressionOrLambdaExpression"
    // InternalABSReport.g:5040:1: entryRuleExpressionOrLambdaExpression returns [EObject current=null] : iv_ruleExpressionOrLambdaExpression= ruleExpressionOrLambdaExpression EOF ;
    public final EObject entryRuleExpressionOrLambdaExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionOrLambdaExpression = null;


        try {
            // InternalABSReport.g:5040:69: (iv_ruleExpressionOrLambdaExpression= ruleExpressionOrLambdaExpression EOF )
            // InternalABSReport.g:5041:2: iv_ruleExpressionOrLambdaExpression= ruleExpressionOrLambdaExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionOrLambdaExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpressionOrLambdaExpression=ruleExpressionOrLambdaExpression();

            state._fsp--;

             current =iv_ruleExpressionOrLambdaExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionOrLambdaExpression"


    // $ANTLR start "ruleExpressionOrLambdaExpression"
    // InternalABSReport.g:5047:1: ruleExpressionOrLambdaExpression returns [EObject current=null] : (this_LambdaExpression_0= ruleLambdaExpression | this_Expression_1= ruleExpression ) ;
    public final EObject ruleExpressionOrLambdaExpression() throws RecognitionException {
        EObject current = null;

        EObject this_LambdaExpression_0 = null;

        EObject this_Expression_1 = null;



        	enterRule();

        try {
            // InternalABSReport.g:5053:2: ( (this_LambdaExpression_0= ruleLambdaExpression | this_Expression_1= ruleExpression ) )
            // InternalABSReport.g:5054:2: (this_LambdaExpression_0= ruleLambdaExpression | this_Expression_1= ruleExpression )
            {
            // InternalABSReport.g:5054:2: (this_LambdaExpression_0= ruleLambdaExpression | this_Expression_1= ruleExpression )
            int alt103=2;
            int LA103_0 = input.LA(1);

            if ( (LA103_0==12) ) {
                alt103=1;
            }
            else if ( ((LA103_0>=RULE_STRING && LA103_0<=RULE_ID)||LA103_0==27||LA103_0==32||LA103_0==35||(LA103_0>=61 && LA103_0<=62)||LA103_0==76) ) {
                alt103=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 103, 0, input);

                throw nvae;
            }
            switch (alt103) {
                case 1 :
                    // InternalABSReport.g:5055:3: this_LambdaExpression_0= ruleLambdaExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionOrLambdaExpressionAccess().getLambdaExpressionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_LambdaExpression_0=ruleLambdaExpression();

                    state._fsp--;


                    			current = this_LambdaExpression_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalABSReport.g:5064:3: this_Expression_1= ruleExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionOrLambdaExpressionAccess().getExpressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Expression_1=ruleExpression();

                    state._fsp--;


                    			current = this_Expression_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionOrLambdaExpression"


    // $ANTLR start "entryRuleLambdaExpression"
    // InternalABSReport.g:5076:1: entryRuleLambdaExpression returns [EObject current=null] : iv_ruleLambdaExpression= ruleLambdaExpression EOF ;
    public final EObject entryRuleLambdaExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLambdaExpression = null;


        try {
            // InternalABSReport.g:5076:57: (iv_ruleLambdaExpression= ruleLambdaExpression EOF )
            // InternalABSReport.g:5077:2: iv_ruleLambdaExpression= ruleLambdaExpression EOF
            {
             newCompositeNode(grammarAccess.getLambdaExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLambdaExpression=ruleLambdaExpression();

            state._fsp--;

             current =iv_ruleLambdaExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLambdaExpression"


    // $ANTLR start "ruleLambdaExpression"
    // InternalABSReport.g:5083:1: ruleLambdaExpression returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}' ) ;
    public final EObject ruleLambdaExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_lambdaParameter_2_0 = null;

        EObject lv_body_4_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:5089:2: ( ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}' ) )
            // InternalABSReport.g:5090:2: ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}' )
            {
            // InternalABSReport.g:5090:2: ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}' )
            // InternalABSReport.g:5091:3: () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_body_4_0= ruleExpression ) ) otherlv_5= '}'
            {
            // InternalABSReport.g:5091:3: ()
            // InternalABSReport.g:5092:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLambdaExpressionAccess().getLambdaExpressionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,12,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getLambdaExpressionAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalABSReport.g:5102:3: ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) )
            // InternalABSReport.g:5103:4: (lv_lambdaParameter_2_0= ruleLambdaParameter )
            {
            // InternalABSReport.g:5103:4: (lv_lambdaParameter_2_0= ruleLambdaParameter )
            // InternalABSReport.g:5104:5: lv_lambdaParameter_2_0= ruleLambdaParameter
            {

            					newCompositeNode(grammarAccess.getLambdaExpressionAccess().getLambdaParameterLambdaParameterParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_76);
            lv_lambdaParameter_2_0=ruleLambdaParameter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaExpressionRule());
            					}
            					set(
            						current,
            						"lambdaParameter",
            						lv_lambdaParameter_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.LambdaParameter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,84,FOLLOW_61); 

            			newLeafNode(otherlv_3, grammarAccess.getLambdaExpressionAccess().getHyphenMinusGreaterThanSignKeyword_3());
            		
            // InternalABSReport.g:5125:3: ( (lv_body_4_0= ruleExpression ) )
            // InternalABSReport.g:5126:4: (lv_body_4_0= ruleExpression )
            {
            // InternalABSReport.g:5126:4: (lv_body_4_0= ruleExpression )
            // InternalABSReport.g:5127:5: lv_body_4_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getLambdaExpressionAccess().getBodyExpressionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_14);
            lv_body_4_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaExpressionRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_4_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getLambdaExpressionAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLambdaExpression"


    // $ANTLR start "entryRuleTerminalExpression"
    // InternalABSReport.g:5152:1: entryRuleTerminalExpression returns [EObject current=null] : iv_ruleTerminalExpression= ruleTerminalExpression EOF ;
    public final EObject entryRuleTerminalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerminalExpression = null;


        try {
            // InternalABSReport.g:5152:59: (iv_ruleTerminalExpression= ruleTerminalExpression EOF )
            // InternalABSReport.g:5153:2: iv_ruleTerminalExpression= ruleTerminalExpression EOF
            {
             newCompositeNode(grammarAccess.getTerminalExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTerminalExpression=ruleTerminalExpression();

            state._fsp--;

             current =iv_ruleTerminalExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerminalExpression"


    // $ANTLR start "ruleTerminalExpression"
    // InternalABSReport.g:5159:1: ruleTerminalExpression returns [EObject current=null] : ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) ) ;
    public final EObject ruleTerminalExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token lv_value_3_0=null;
        Token lv_value_5_1=null;
        Token lv_value_5_2=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        AntlrDatatypeRuleToken lv_value_7_0 = null;

        EObject this_Collection_10 = null;

        EObject this_Undefined_11 = null;

        EObject this_Expression_13 = null;



        	enterRule();

        try {
            // InternalABSReport.g:5165:2: ( ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) ) )
            // InternalABSReport.g:5166:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) )
            {
            // InternalABSReport.g:5166:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) )
            int alt105=8;
            alt105 = dfa105.predict(input);
            switch (alt105) {
                case 1 :
                    // InternalABSReport.g:5167:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    {
                    // InternalABSReport.g:5167:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    // InternalABSReport.g:5168:4: () ( (lv_value_1_0= RULE_INT ) )
                    {
                    // InternalABSReport.g:5168:4: ()
                    // InternalABSReport.g:5169:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getIntConstantAction_0_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:5175:4: ( (lv_value_1_0= RULE_INT ) )
                    // InternalABSReport.g:5176:5: (lv_value_1_0= RULE_INT )
                    {
                    // InternalABSReport.g:5176:5: (lv_value_1_0= RULE_INT )
                    // InternalABSReport.g:5177:6: lv_value_1_0= RULE_INT
                    {
                    lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    						newLeafNode(lv_value_1_0, grammarAccess.getTerminalExpressionAccess().getValueINTTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTerminalExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_1_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:5195:3: ( () ( (lv_value_3_0= RULE_STRING ) ) )
                    {
                    // InternalABSReport.g:5195:3: ( () ( (lv_value_3_0= RULE_STRING ) ) )
                    // InternalABSReport.g:5196:4: () ( (lv_value_3_0= RULE_STRING ) )
                    {
                    // InternalABSReport.g:5196:4: ()
                    // InternalABSReport.g:5197:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getStringConstantAction_1_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:5203:4: ( (lv_value_3_0= RULE_STRING ) )
                    // InternalABSReport.g:5204:5: (lv_value_3_0= RULE_STRING )
                    {
                    // InternalABSReport.g:5204:5: (lv_value_3_0= RULE_STRING )
                    // InternalABSReport.g:5205:6: lv_value_3_0= RULE_STRING
                    {
                    lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getTerminalExpressionAccess().getValueSTRINGTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTerminalExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalABSReport.g:5223:3: ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) )
                    {
                    // InternalABSReport.g:5223:3: ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) )
                    // InternalABSReport.g:5224:4: () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) )
                    {
                    // InternalABSReport.g:5224:4: ()
                    // InternalABSReport.g:5225:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getBooleanConstantAction_2_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:5231:4: ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) )
                    // InternalABSReport.g:5232:5: ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) )
                    {
                    // InternalABSReport.g:5232:5: ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) )
                    // InternalABSReport.g:5233:6: (lv_value_5_1= 'true' | lv_value_5_2= 'false' )
                    {
                    // InternalABSReport.g:5233:6: (lv_value_5_1= 'true' | lv_value_5_2= 'false' )
                    int alt104=2;
                    int LA104_0 = input.LA(1);

                    if ( (LA104_0==61) ) {
                        alt104=1;
                    }
                    else if ( (LA104_0==62) ) {
                        alt104=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 104, 0, input);

                        throw nvae;
                    }
                    switch (alt104) {
                        case 1 :
                            // InternalABSReport.g:5234:7: lv_value_5_1= 'true'
                            {
                            lv_value_5_1=(Token)match(input,61,FOLLOW_2); 

                            							newLeafNode(lv_value_5_1, grammarAccess.getTerminalExpressionAccess().getValueTrueKeyword_2_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getTerminalExpressionRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_5_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalABSReport.g:5245:7: lv_value_5_2= 'false'
                            {
                            lv_value_5_2=(Token)match(input,62,FOLLOW_2); 

                            							newLeafNode(lv_value_5_2, grammarAccess.getTerminalExpressionAccess().getValueFalseKeyword_2_1_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getTerminalExpressionRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_5_2, null);
                            						

                            }
                            break;

                    }


                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalABSReport.g:5260:3: ( () ( (lv_value_7_0= ruleVersion ) ) )
                    {
                    // InternalABSReport.g:5260:3: ( () ( (lv_value_7_0= ruleVersion ) ) )
                    // InternalABSReport.g:5261:4: () ( (lv_value_7_0= ruleVersion ) )
                    {
                    // InternalABSReport.g:5261:4: ()
                    // InternalABSReport.g:5262:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getVersionConstantAction_3_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:5268:4: ( (lv_value_7_0= ruleVersion ) )
                    // InternalABSReport.g:5269:5: (lv_value_7_0= ruleVersion )
                    {
                    // InternalABSReport.g:5269:5: (lv_value_7_0= ruleVersion )
                    // InternalABSReport.g:5270:6: lv_value_7_0= ruleVersion
                    {

                    						newCompositeNode(grammarAccess.getTerminalExpressionAccess().getValueVersionParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_7_0=ruleVersion();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTerminalExpressionRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Version");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalABSReport.g:5289:3: ( () ( ( ruleQualifiedName ) ) )
                    {
                    // InternalABSReport.g:5289:3: ( () ( ( ruleQualifiedName ) ) )
                    // InternalABSReport.g:5290:4: () ( ( ruleQualifiedName ) )
                    {
                    // InternalABSReport.g:5290:4: ()
                    // InternalABSReport.g:5291:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getTerminalExpressionAccess().getSymbolRefAction_4_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:5297:4: ( ( ruleQualifiedName ) )
                    // InternalABSReport.g:5298:5: ( ruleQualifiedName )
                    {
                    // InternalABSReport.g:5298:5: ( ruleQualifiedName )
                    // InternalABSReport.g:5299:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTerminalExpressionRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getTerminalExpressionAccess().getSymbolSymbolCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalABSReport.g:5315:3: this_Collection_10= ruleCollection
                    {

                    			newCompositeNode(grammarAccess.getTerminalExpressionAccess().getCollectionParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Collection_10=ruleCollection();

                    state._fsp--;


                    			current = this_Collection_10;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalABSReport.g:5324:3: this_Undefined_11= ruleUndefined
                    {

                    			newCompositeNode(grammarAccess.getTerminalExpressionAccess().getUndefinedParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_Undefined_11=ruleUndefined();

                    state._fsp--;


                    			current = this_Undefined_11;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalABSReport.g:5333:3: (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' )
                    {
                    // InternalABSReport.g:5333:3: (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' )
                    // InternalABSReport.g:5334:4: otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')'
                    {
                    otherlv_12=(Token)match(input,27,FOLLOW_61); 

                    				newLeafNode(otherlv_12, grammarAccess.getTerminalExpressionAccess().getLeftParenthesisKeyword_7_0());
                    			

                    				newCompositeNode(grammarAccess.getTerminalExpressionAccess().getExpressionParserRuleCall_7_1());
                    			
                    pushFollow(FOLLOW_77);
                    this_Expression_13=ruleExpression();

                    state._fsp--;


                    				current = this_Expression_13;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_14=(Token)match(input,28,FOLLOW_2); 

                    				newLeafNode(otherlv_14, grammarAccess.getTerminalExpressionAccess().getRightParenthesisKeyword_7_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerminalExpression"


    // $ANTLR start "entryRuleCollection"
    // InternalABSReport.g:5355:1: entryRuleCollection returns [EObject current=null] : iv_ruleCollection= ruleCollection EOF ;
    public final EObject entryRuleCollection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollection = null;


        try {
            // InternalABSReport.g:5355:51: (iv_ruleCollection= ruleCollection EOF )
            // InternalABSReport.g:5356:2: iv_ruleCollection= ruleCollection EOF
            {
             newCompositeNode(grammarAccess.getCollectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCollection=ruleCollection();

            state._fsp--;

             current =iv_ruleCollection; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollection"


    // $ANTLR start "ruleCollection"
    // InternalABSReport.g:5362:1: ruleCollection returns [EObject current=null] : ( () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']' ) ;
    public final EObject ruleCollection() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_elements_2_0 = null;

        EObject lv_elements_4_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:5368:2: ( ( () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']' ) )
            // InternalABSReport.g:5369:2: ( () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']' )
            {
            // InternalABSReport.g:5369:2: ( () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']' )
            // InternalABSReport.g:5370:3: () otherlv_1= '[' ( (lv_elements_2_0= ruleExpression ) )? (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )* otherlv_5= ']'
            {
            // InternalABSReport.g:5370:3: ()
            // InternalABSReport.g:5371:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCollectionAccess().getCollectionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,32,FOLLOW_78); 

            			newLeafNode(otherlv_1, grammarAccess.getCollectionAccess().getLeftSquareBracketKeyword_1());
            		
            // InternalABSReport.g:5381:3: ( (lv_elements_2_0= ruleExpression ) )?
            int alt106=2;
            int LA106_0 = input.LA(1);

            if ( ((LA106_0>=RULE_STRING && LA106_0<=RULE_ID)||LA106_0==27||LA106_0==32||LA106_0==35||(LA106_0>=61 && LA106_0<=62)||LA106_0==76) ) {
                alt106=1;
            }
            switch (alt106) {
                case 1 :
                    // InternalABSReport.g:5382:4: (lv_elements_2_0= ruleExpression )
                    {
                    // InternalABSReport.g:5382:4: (lv_elements_2_0= ruleExpression )
                    // InternalABSReport.g:5383:5: lv_elements_2_0= ruleExpression
                    {

                    					newCompositeNode(grammarAccess.getCollectionAccess().getElementsExpressionParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_28);
                    lv_elements_2_0=ruleExpression();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getCollectionRule());
                    					}
                    					add(
                    						current,
                    						"elements",
                    						lv_elements_2_0,
                    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalABSReport.g:5400:3: (otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) ) )*
            loop107:
            do {
                int alt107=2;
                int LA107_0 = input.LA(1);

                if ( (LA107_0==26) ) {
                    alt107=1;
                }


                switch (alt107) {
            	case 1 :
            	    // InternalABSReport.g:5401:4: otherlv_3= ',' ( (lv_elements_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,26,FOLLOW_61); 

            	    				newLeafNode(otherlv_3, grammarAccess.getCollectionAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalABSReport.g:5405:4: ( (lv_elements_4_0= ruleExpression ) )
            	    // InternalABSReport.g:5406:5: (lv_elements_4_0= ruleExpression )
            	    {
            	    // InternalABSReport.g:5406:5: (lv_elements_4_0= ruleExpression )
            	    // InternalABSReport.g:5407:6: lv_elements_4_0= ruleExpression
            	    {

            	    						newCompositeNode(grammarAccess.getCollectionAccess().getElementsExpressionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_28);
            	    lv_elements_4_0=ruleExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getCollectionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"elements",
            	    							lv_elements_4_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop107;
                }
            } while (true);

            otherlv_5=(Token)match(input,33,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCollectionAccess().getRightSquareBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollection"


    // $ANTLR start "entryRuleActionSelectionExpression"
    // InternalABSReport.g:5433:1: entryRuleActionSelectionExpression returns [EObject current=null] : iv_ruleActionSelectionExpression= ruleActionSelectionExpression EOF ;
    public final EObject entryRuleActionSelectionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActionSelectionExpression = null;


        try {
            // InternalABSReport.g:5433:66: (iv_ruleActionSelectionExpression= ruleActionSelectionExpression EOF )
            // InternalABSReport.g:5434:2: iv_ruleActionSelectionExpression= ruleActionSelectionExpression EOF
            {
             newCompositeNode(grammarAccess.getActionSelectionExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleActionSelectionExpression=ruleActionSelectionExpression();

            state._fsp--;

             current =iv_ruleActionSelectionExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActionSelectionExpression"


    // $ANTLR start "ruleActionSelectionExpression"
    // InternalABSReport.g:5440:1: ruleActionSelectionExpression returns [EObject current=null] : ( () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )* ) ;
    public final EObject ruleActionSelectionExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_methodInvocation_4_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token lv_methodInvocation_12_0=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        EObject lv_receiver_1_0 = null;

        EObject lv_args_5_0 = null;

        EObject lv_args_7_0 = null;

        EObject lv_args_13_0 = null;

        EObject lv_args_15_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:5446:2: ( ( () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )* ) )
            // InternalABSReport.g:5447:2: ( () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )* )
            {
            // InternalABSReport.g:5447:2: ( () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )* )
            // InternalABSReport.g:5448:3: () ( (lv_receiver_1_0= ruleTerminalExpression ) ) otherlv_2= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )? ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )*
            {
            // InternalABSReport.g:5448:3: ()
            // InternalABSReport.g:5449:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getActionSelectionExpressionAccess().getMemberSelectionAction_0(),
            					current);
            			

            }

            // InternalABSReport.g:5455:3: ( (lv_receiver_1_0= ruleTerminalExpression ) )
            // InternalABSReport.g:5456:4: (lv_receiver_1_0= ruleTerminalExpression )
            {
            // InternalABSReport.g:5456:4: (lv_receiver_1_0= ruleTerminalExpression )
            // InternalABSReport.g:5457:5: lv_receiver_1_0= ruleTerminalExpression
            {

            					newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getReceiverTerminalExpressionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_79);
            lv_receiver_1_0=ruleTerminalExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
            					}
            					set(
            						current,
            						"receiver",
            						lv_receiver_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TerminalExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,83,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getActionSelectionExpressionAccess().getFullStopKeyword_2());
            		
            // InternalABSReport.g:5478:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:5479:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:5479:4: ( ruleQualifiedName )
            // InternalABSReport.g:5480:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getActionSelectionExpressionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getMemberMemberCrossReference_3_0());
            				
            pushFollow(FOLLOW_74);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalABSReport.g:5494:3: ( ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')' )?
            int alt110=2;
            int LA110_0 = input.LA(1);

            if ( (LA110_0==27) ) {
                alt110=1;
            }
            switch (alt110) {
                case 1 :
                    // InternalABSReport.g:5495:4: ( (lv_methodInvocation_4_0= '(' ) ) ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )? otherlv_8= ')'
                    {
                    // InternalABSReport.g:5495:4: ( (lv_methodInvocation_4_0= '(' ) )
                    // InternalABSReport.g:5496:5: (lv_methodInvocation_4_0= '(' )
                    {
                    // InternalABSReport.g:5496:5: (lv_methodInvocation_4_0= '(' )
                    // InternalABSReport.g:5497:6: lv_methodInvocation_4_0= '('
                    {
                    lv_methodInvocation_4_0=(Token)match(input,27,FOLLOW_75); 

                    						newLeafNode(lv_methodInvocation_4_0, grammarAccess.getActionSelectionExpressionAccess().getMethodInvocationLeftParenthesisKeyword_4_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getActionSelectionExpressionRule());
                    						}
                    						setWithLastConsumed(current, "methodInvocation", lv_methodInvocation_4_0 != null, "(");
                    					

                    }


                    }

                    // InternalABSReport.g:5509:4: ( ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )* )?
                    int alt109=2;
                    int LA109_0 = input.LA(1);

                    if ( ((LA109_0>=RULE_STRING && LA109_0<=RULE_ID)||LA109_0==12||LA109_0==27||LA109_0==32||LA109_0==35||(LA109_0>=61 && LA109_0<=62)||LA109_0==76) ) {
                        alt109=1;
                    }
                    switch (alt109) {
                        case 1 :
                            // InternalABSReport.g:5510:5: ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) ) (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )*
                            {
                            // InternalABSReport.g:5510:5: ( (lv_args_5_0= ruleExpressionOrLambdaExpression ) )
                            // InternalABSReport.g:5511:6: (lv_args_5_0= ruleExpressionOrLambdaExpression )
                            {
                            // InternalABSReport.g:5511:6: (lv_args_5_0= ruleExpressionOrLambdaExpression )
                            // InternalABSReport.g:5512:7: lv_args_5_0= ruleExpressionOrLambdaExpression
                            {

                            							newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getArgsExpressionOrLambdaExpressionParserRuleCall_4_1_0_0());
                            						
                            pushFollow(FOLLOW_21);
                            lv_args_5_0=ruleExpressionOrLambdaExpression();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
                            							}
                            							add(
                            								current,
                            								"args",
                            								lv_args_5_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ExpressionOrLambdaExpression");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            // InternalABSReport.g:5529:5: (otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) ) )*
                            loop108:
                            do {
                                int alt108=2;
                                int LA108_0 = input.LA(1);

                                if ( (LA108_0==26) ) {
                                    alt108=1;
                                }


                                switch (alt108) {
                            	case 1 :
                            	    // InternalABSReport.g:5530:6: otherlv_6= ',' ( (lv_args_7_0= ruleExpression ) )
                            	    {
                            	    otherlv_6=(Token)match(input,26,FOLLOW_61); 

                            	    						newLeafNode(otherlv_6, grammarAccess.getActionSelectionExpressionAccess().getCommaKeyword_4_1_1_0());
                            	    					
                            	    // InternalABSReport.g:5534:6: ( (lv_args_7_0= ruleExpression ) )
                            	    // InternalABSReport.g:5535:7: (lv_args_7_0= ruleExpression )
                            	    {
                            	    // InternalABSReport.g:5535:7: (lv_args_7_0= ruleExpression )
                            	    // InternalABSReport.g:5536:8: lv_args_7_0= ruleExpression
                            	    {

                            	    								newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getArgsExpressionParserRuleCall_4_1_1_1_0());
                            	    							
                            	    pushFollow(FOLLOW_21);
                            	    lv_args_7_0=ruleExpression();

                            	    state._fsp--;


                            	    								if (current==null) {
                            	    									current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
                            	    								}
                            	    								add(
                            	    									current,
                            	    									"args",
                            	    									lv_args_7_0,
                            	    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                            	    								afterParserOrEnumRuleCall();
                            	    							

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop108;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_8=(Token)match(input,28,FOLLOW_73); 

                    				newLeafNode(otherlv_8, grammarAccess.getActionSelectionExpressionAccess().getRightParenthesisKeyword_4_2());
                    			

                    }
                    break;

            }

            // InternalABSReport.g:5560:3: ( () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )? )*
            loop114:
            do {
                int alt114=2;
                int LA114_0 = input.LA(1);

                if ( (LA114_0==83) ) {
                    int LA114_2 = input.LA(2);

                    if ( (LA114_2==RULE_ID) ) {
                        alt114=1;
                    }


                }


                switch (alt114) {
            	case 1 :
            	    // InternalABSReport.g:5561:4: () otherlv_10= '.' ( ( ruleQualifiedName ) ) ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )?
            	    {
            	    // InternalABSReport.g:5561:4: ()
            	    // InternalABSReport.g:5562:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getActionSelectionExpressionAccess().getMemberSelectionReceiverAction_5_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_10=(Token)match(input,83,FOLLOW_5); 

            	    				newLeafNode(otherlv_10, grammarAccess.getActionSelectionExpressionAccess().getFullStopKeyword_5_1());
            	    			
            	    // InternalABSReport.g:5572:4: ( ( ruleQualifiedName ) )
            	    // InternalABSReport.g:5573:5: ( ruleQualifiedName )
            	    {
            	    // InternalABSReport.g:5573:5: ( ruleQualifiedName )
            	    // InternalABSReport.g:5574:6: ruleQualifiedName
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getActionSelectionExpressionRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getMemberMemberCrossReference_5_2_0());
            	    					
            	    pushFollow(FOLLOW_74);
            	    ruleQualifiedName();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalABSReport.g:5588:4: ( ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')' )?
            	    int alt113=2;
            	    int LA113_0 = input.LA(1);

            	    if ( (LA113_0==27) ) {
            	        alt113=1;
            	    }
            	    switch (alt113) {
            	        case 1 :
            	            // InternalABSReport.g:5589:5: ( (lv_methodInvocation_12_0= '(' ) ) ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )? otherlv_16= ')'
            	            {
            	            // InternalABSReport.g:5589:5: ( (lv_methodInvocation_12_0= '(' ) )
            	            // InternalABSReport.g:5590:6: (lv_methodInvocation_12_0= '(' )
            	            {
            	            // InternalABSReport.g:5590:6: (lv_methodInvocation_12_0= '(' )
            	            // InternalABSReport.g:5591:7: lv_methodInvocation_12_0= '('
            	            {
            	            lv_methodInvocation_12_0=(Token)match(input,27,FOLLOW_75); 

            	            							newLeafNode(lv_methodInvocation_12_0, grammarAccess.getActionSelectionExpressionAccess().getMethodInvocationLeftParenthesisKeyword_5_3_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getActionSelectionExpressionRule());
            	            							}
            	            							setWithLastConsumed(current, "methodInvocation", lv_methodInvocation_12_0 != null, "(");
            	            						

            	            }


            	            }

            	            // InternalABSReport.g:5603:5: ( ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )* )?
            	            int alt112=2;
            	            int LA112_0 = input.LA(1);

            	            if ( ((LA112_0>=RULE_STRING && LA112_0<=RULE_ID)||LA112_0==12||LA112_0==27||LA112_0==32||LA112_0==35||(LA112_0>=61 && LA112_0<=62)||LA112_0==76) ) {
            	                alt112=1;
            	            }
            	            switch (alt112) {
            	                case 1 :
            	                    // InternalABSReport.g:5604:6: ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) ) (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )*
            	                    {
            	                    // InternalABSReport.g:5604:6: ( (lv_args_13_0= ruleExpressionOrLambdaExpression ) )
            	                    // InternalABSReport.g:5605:7: (lv_args_13_0= ruleExpressionOrLambdaExpression )
            	                    {
            	                    // InternalABSReport.g:5605:7: (lv_args_13_0= ruleExpressionOrLambdaExpression )
            	                    // InternalABSReport.g:5606:8: lv_args_13_0= ruleExpressionOrLambdaExpression
            	                    {

            	                    								newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getArgsExpressionOrLambdaExpressionParserRuleCall_5_3_1_0_0());
            	                    							
            	                    pushFollow(FOLLOW_21);
            	                    lv_args_13_0=ruleExpressionOrLambdaExpression();

            	                    state._fsp--;


            	                    								if (current==null) {
            	                    									current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
            	                    								}
            	                    								add(
            	                    									current,
            	                    									"args",
            	                    									lv_args_13_0,
            	                    									"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ExpressionOrLambdaExpression");
            	                    								afterParserOrEnumRuleCall();
            	                    							

            	                    }


            	                    }

            	                    // InternalABSReport.g:5623:6: (otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) ) )*
            	                    loop111:
            	                    do {
            	                        int alt111=2;
            	                        int LA111_0 = input.LA(1);

            	                        if ( (LA111_0==26) ) {
            	                            alt111=1;
            	                        }


            	                        switch (alt111) {
            	                    	case 1 :
            	                    	    // InternalABSReport.g:5624:7: otherlv_14= ',' ( (lv_args_15_0= ruleExpression ) )
            	                    	    {
            	                    	    otherlv_14=(Token)match(input,26,FOLLOW_61); 

            	                    	    							newLeafNode(otherlv_14, grammarAccess.getActionSelectionExpressionAccess().getCommaKeyword_5_3_1_1_0());
            	                    	    						
            	                    	    // InternalABSReport.g:5628:7: ( (lv_args_15_0= ruleExpression ) )
            	                    	    // InternalABSReport.g:5629:8: (lv_args_15_0= ruleExpression )
            	                    	    {
            	                    	    // InternalABSReport.g:5629:8: (lv_args_15_0= ruleExpression )
            	                    	    // InternalABSReport.g:5630:9: lv_args_15_0= ruleExpression
            	                    	    {

            	                    	    									newCompositeNode(grammarAccess.getActionSelectionExpressionAccess().getArgsExpressionParserRuleCall_5_3_1_1_1_0());
            	                    	    								
            	                    	    pushFollow(FOLLOW_21);
            	                    	    lv_args_15_0=ruleExpression();

            	                    	    state._fsp--;


            	                    	    									if (current==null) {
            	                    	    										current = createModelElementForParent(grammarAccess.getActionSelectionExpressionRule());
            	                    	    									}
            	                    	    									add(
            	                    	    										current,
            	                    	    										"args",
            	                    	    										lv_args_15_0,
            	                    	    										"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
            	                    	    									afterParserOrEnumRuleCall();
            	                    	    								

            	                    	    }


            	                    	    }


            	                    	    }
            	                    	    break;

            	                    	default :
            	                    	    break loop111;
            	                        }
            	                    } while (true);


            	                    }
            	                    break;

            	            }

            	            otherlv_16=(Token)match(input,28,FOLLOW_73); 

            	            					newLeafNode(otherlv_16, grammarAccess.getActionSelectionExpressionAccess().getRightParenthesisKeyword_5_3_2());
            	            				

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop114;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionSelectionExpression"


    // $ANTLR start "entryRuleAction"
    // InternalABSReport.g:5659:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalABSReport.g:5659:47: (iv_ruleAction= ruleAction EOF )
            // InternalABSReport.g:5660:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalABSReport.g:5666:1: ruleAction returns [EObject current=null] : ( () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')' ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_target_1_0 = null;

        Enumerator lv_actionType_3_0 = null;

        EObject lv_args_5_0 = null;

        EObject lv_lambdaAction_6_0 = null;

        EObject lv_args_8_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:5672:2: ( ( () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')' ) )
            // InternalABSReport.g:5673:2: ( () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')' )
            {
            // InternalABSReport.g:5673:2: ( () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')' )
            // InternalABSReport.g:5674:3: () ( (lv_target_1_0= ruleActionSelectionExpression ) ) otherlv_2= '.' ( (lv_actionType_3_0= ruleActionEnum ) ) otherlv_4= '(' ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )? otherlv_9= ')'
            {
            // InternalABSReport.g:5674:3: ()
            // InternalABSReport.g:5675:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getActionAccess().getActionAction_0(),
            					current);
            			

            }

            // InternalABSReport.g:5681:3: ( (lv_target_1_0= ruleActionSelectionExpression ) )
            // InternalABSReport.g:5682:4: (lv_target_1_0= ruleActionSelectionExpression )
            {
            // InternalABSReport.g:5682:4: (lv_target_1_0= ruleActionSelectionExpression )
            // InternalABSReport.g:5683:5: lv_target_1_0= ruleActionSelectionExpression
            {

            					newCompositeNode(grammarAccess.getActionAccess().getTargetActionSelectionExpressionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_79);
            lv_target_1_0=ruleActionSelectionExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					set(
            						current,
            						"target",
            						lv_target_1_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ActionSelectionExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,83,FOLLOW_80); 

            			newLeafNode(otherlv_2, grammarAccess.getActionAccess().getFullStopKeyword_2());
            		
            // InternalABSReport.g:5704:3: ( (lv_actionType_3_0= ruleActionEnum ) )
            // InternalABSReport.g:5705:4: (lv_actionType_3_0= ruleActionEnum )
            {
            // InternalABSReport.g:5705:4: (lv_actionType_3_0= ruleActionEnum )
            // InternalABSReport.g:5706:5: lv_actionType_3_0= ruleActionEnum
            {

            					newCompositeNode(grammarAccess.getActionAccess().getActionTypeActionEnumEnumRuleCall_3_0());
            				
            pushFollow(FOLLOW_19);
            lv_actionType_3_0=ruleActionEnum();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					set(
            						current,
            						"actionType",
            						lv_actionType_3_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ActionEnum");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,27,FOLLOW_75); 

            			newLeafNode(otherlv_4, grammarAccess.getActionAccess().getLeftParenthesisKeyword_4());
            		
            // InternalABSReport.g:5727:3: ( ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )* )?
            int alt117=2;
            int LA117_0 = input.LA(1);

            if ( ((LA117_0>=RULE_STRING && LA117_0<=RULE_ID)||LA117_0==12||LA117_0==27||LA117_0==32||LA117_0==35||(LA117_0>=61 && LA117_0<=62)||LA117_0==76) ) {
                alt117=1;
            }
            switch (alt117) {
                case 1 :
                    // InternalABSReport.g:5728:4: ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) ) (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )*
                    {
                    // InternalABSReport.g:5728:4: ( ( (lv_args_5_0= ruleExpression ) ) | ( (lv_lambdaAction_6_0= ruleLambdaAction ) ) )
                    int alt115=2;
                    int LA115_0 = input.LA(1);

                    if ( ((LA115_0>=RULE_STRING && LA115_0<=RULE_ID)||LA115_0==27||LA115_0==32||LA115_0==35||(LA115_0>=61 && LA115_0<=62)||LA115_0==76) ) {
                        alt115=1;
                    }
                    else if ( (LA115_0==12) ) {
                        alt115=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 115, 0, input);

                        throw nvae;
                    }
                    switch (alt115) {
                        case 1 :
                            // InternalABSReport.g:5729:5: ( (lv_args_5_0= ruleExpression ) )
                            {
                            // InternalABSReport.g:5729:5: ( (lv_args_5_0= ruleExpression ) )
                            // InternalABSReport.g:5730:6: (lv_args_5_0= ruleExpression )
                            {
                            // InternalABSReport.g:5730:6: (lv_args_5_0= ruleExpression )
                            // InternalABSReport.g:5731:7: lv_args_5_0= ruleExpression
                            {

                            							newCompositeNode(grammarAccess.getActionAccess().getArgsExpressionParserRuleCall_5_0_0_0());
                            						
                            pushFollow(FOLLOW_21);
                            lv_args_5_0=ruleExpression();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getActionRule());
                            							}
                            							add(
                            								current,
                            								"args",
                            								lv_args_5_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalABSReport.g:5749:5: ( (lv_lambdaAction_6_0= ruleLambdaAction ) )
                            {
                            // InternalABSReport.g:5749:5: ( (lv_lambdaAction_6_0= ruleLambdaAction ) )
                            // InternalABSReport.g:5750:6: (lv_lambdaAction_6_0= ruleLambdaAction )
                            {
                            // InternalABSReport.g:5750:6: (lv_lambdaAction_6_0= ruleLambdaAction )
                            // InternalABSReport.g:5751:7: lv_lambdaAction_6_0= ruleLambdaAction
                            {

                            							newCompositeNode(grammarAccess.getActionAccess().getLambdaActionLambdaActionParserRuleCall_5_0_1_0());
                            						
                            pushFollow(FOLLOW_21);
                            lv_lambdaAction_6_0=ruleLambdaAction();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getActionRule());
                            							}
                            							set(
                            								current,
                            								"lambdaAction",
                            								lv_lambdaAction_6_0,
                            								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.LambdaAction");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:5769:4: (otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) ) )*
                    loop116:
                    do {
                        int alt116=2;
                        int LA116_0 = input.LA(1);

                        if ( (LA116_0==26) ) {
                            alt116=1;
                        }


                        switch (alt116) {
                    	case 1 :
                    	    // InternalABSReport.g:5770:5: otherlv_7= ',' ( (lv_args_8_0= ruleExpression ) )
                    	    {
                    	    otherlv_7=(Token)match(input,26,FOLLOW_61); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getActionAccess().getCommaKeyword_5_1_0());
                    	    				
                    	    // InternalABSReport.g:5774:5: ( (lv_args_8_0= ruleExpression ) )
                    	    // InternalABSReport.g:5775:6: (lv_args_8_0= ruleExpression )
                    	    {
                    	    // InternalABSReport.g:5775:6: (lv_args_8_0= ruleExpression )
                    	    // InternalABSReport.g:5776:7: lv_args_8_0= ruleExpression
                    	    {

                    	    							newCompositeNode(grammarAccess.getActionAccess().getArgsExpressionParserRuleCall_5_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_21);
                    	    lv_args_8_0=ruleExpression();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getActionRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"args",
                    	    								lv_args_8_0,
                    	    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Expression");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop116;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_9=(Token)match(input,28,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getActionAccess().getRightParenthesisKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleLambdaAction"
    // InternalABSReport.g:5803:1: entryRuleLambdaAction returns [EObject current=null] : iv_ruleLambdaAction= ruleLambdaAction EOF ;
    public final EObject entryRuleLambdaAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLambdaAction = null;


        try {
            // InternalABSReport.g:5803:53: (iv_ruleLambdaAction= ruleLambdaAction EOF )
            // InternalABSReport.g:5804:2: iv_ruleLambdaAction= ruleLambdaAction EOF
            {
             newCompositeNode(grammarAccess.getLambdaActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLambdaAction=ruleLambdaAction();

            state._fsp--;

             current =iv_ruleLambdaAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLambdaAction"


    // $ANTLR start "ruleLambdaAction"
    // InternalABSReport.g:5810:1: ruleLambdaAction returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}' ) ;
    public final EObject ruleLambdaAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_lambdaParameter_2_0 = null;

        EObject lv_actions_4_0 = null;

        EObject lv_actions_6_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:5816:2: ( ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}' ) )
            // InternalABSReport.g:5817:2: ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}' )
            {
            // InternalABSReport.g:5817:2: ( () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}' )
            // InternalABSReport.g:5818:3: () otherlv_1= '{' ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) ) otherlv_3= '->' ( (lv_actions_4_0= ruleAction ) ) (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )* otherlv_7= '}'
            {
            // InternalABSReport.g:5818:3: ()
            // InternalABSReport.g:5819:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLambdaActionAccess().getLambdaActionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,12,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getLambdaActionAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalABSReport.g:5829:3: ( (lv_lambdaParameter_2_0= ruleLambdaParameter ) )
            // InternalABSReport.g:5830:4: (lv_lambdaParameter_2_0= ruleLambdaParameter )
            {
            // InternalABSReport.g:5830:4: (lv_lambdaParameter_2_0= ruleLambdaParameter )
            // InternalABSReport.g:5831:5: lv_lambdaParameter_2_0= ruleLambdaParameter
            {

            					newCompositeNode(grammarAccess.getLambdaActionAccess().getLambdaParameterLambdaParameterParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_76);
            lv_lambdaParameter_2_0=ruleLambdaParameter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaActionRule());
            					}
            					set(
            						current,
            						"lambdaParameter",
            						lv_lambdaParameter_2_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.LambdaParameter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,84,FOLLOW_61); 

            			newLeafNode(otherlv_3, grammarAccess.getLambdaActionAccess().getHyphenMinusGreaterThanSignKeyword_3());
            		
            // InternalABSReport.g:5852:3: ( (lv_actions_4_0= ruleAction ) )
            // InternalABSReport.g:5853:4: (lv_actions_4_0= ruleAction )
            {
            // InternalABSReport.g:5853:4: (lv_actions_4_0= ruleAction )
            // InternalABSReport.g:5854:5: lv_actions_4_0= ruleAction
            {

            					newCompositeNode(grammarAccess.getLambdaActionAccess().getActionsActionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_81);
            lv_actions_4_0=ruleAction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLambdaActionRule());
            					}
            					add(
            						current,
            						"actions",
            						lv_actions_4_0,
            						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Action");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalABSReport.g:5871:3: (otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) ) )*
            loop118:
            do {
                int alt118=2;
                int LA118_0 = input.LA(1);

                if ( (LA118_0==70) ) {
                    alt118=1;
                }


                switch (alt118) {
            	case 1 :
            	    // InternalABSReport.g:5872:4: otherlv_5= ';' ( (lv_actions_6_0= ruleAction ) )
            	    {
            	    otherlv_5=(Token)match(input,70,FOLLOW_61); 

            	    				newLeafNode(otherlv_5, grammarAccess.getLambdaActionAccess().getSemicolonKeyword_5_0());
            	    			
            	    // InternalABSReport.g:5876:4: ( (lv_actions_6_0= ruleAction ) )
            	    // InternalABSReport.g:5877:5: (lv_actions_6_0= ruleAction )
            	    {
            	    // InternalABSReport.g:5877:5: (lv_actions_6_0= ruleAction )
            	    // InternalABSReport.g:5878:6: lv_actions_6_0= ruleAction
            	    {

            	    						newCompositeNode(grammarAccess.getLambdaActionAccess().getActionsActionParserRuleCall_5_1_0());
            	    					
            	    pushFollow(FOLLOW_81);
            	    lv_actions_6_0=ruleAction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLambdaActionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"actions",
            	    							lv_actions_6_0,
            	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.Action");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop118;
                }
            } while (true);

            otherlv_7=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getLambdaActionAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLambdaAction"


    // $ANTLR start "entryRuleVersion"
    // InternalABSReport.g:5904:1: entryRuleVersion returns [String current=null] : iv_ruleVersion= ruleVersion EOF ;
    public final String entryRuleVersion() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleVersion = null;


        try {
            // InternalABSReport.g:5904:47: (iv_ruleVersion= ruleVersion EOF )
            // InternalABSReport.g:5905:2: iv_ruleVersion= ruleVersion EOF
            {
             newCompositeNode(grammarAccess.getVersionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVersion=ruleVersion();

            state._fsp--;

             current =iv_ruleVersion.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVersion"


    // $ANTLR start "ruleVersion"
    // InternalABSReport.g:5911:1: ruleVersion returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )? ) ;
    public final AntlrDatatypeRuleToken ruleVersion() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_INT_2=null;
        Token this_INT_4=null;
        AntlrDatatypeRuleToken this_VID_6 = null;

        AntlrDatatypeRuleToken this_VID_8 = null;



        	enterRule();

        try {
            // InternalABSReport.g:5917:2: ( (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )? ) )
            // InternalABSReport.g:5918:2: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )? )
            {
            // InternalABSReport.g:5918:2: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )? )
            // InternalABSReport.g:5919:3: this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT kw= '.' this_INT_4= RULE_INT (kw= '-' this_VID_6= ruleVID )? (kw= '+' this_VID_8= ruleVID )?
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_79); 

            			current.merge(this_INT_0);
            		

            			newLeafNode(this_INT_0, grammarAccess.getVersionAccess().getINTTerminalRuleCall_0());
            		
            kw=(Token)match(input,83,FOLLOW_17); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getVersionAccess().getFullStopKeyword_1());
            		
            this_INT_2=(Token)match(input,RULE_INT,FOLLOW_79); 

            			current.merge(this_INT_2);
            		

            			newLeafNode(this_INT_2, grammarAccess.getVersionAccess().getINTTerminalRuleCall_2());
            		
            kw=(Token)match(input,83,FOLLOW_17); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getVersionAccess().getFullStopKeyword_3());
            		
            this_INT_4=(Token)match(input,RULE_INT,FOLLOW_82); 

            			current.merge(this_INT_4);
            		

            			newLeafNode(this_INT_4, grammarAccess.getVersionAccess().getINTTerminalRuleCall_4());
            		
            // InternalABSReport.g:5950:3: (kw= '-' this_VID_6= ruleVID )?
            int alt119=2;
            int LA119_0 = input.LA(1);

            if ( (LA119_0==85) ) {
                alt119=1;
            }
            switch (alt119) {
                case 1 :
                    // InternalABSReport.g:5951:4: kw= '-' this_VID_6= ruleVID
                    {
                    kw=(Token)match(input,85,FOLLOW_83); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getVersionAccess().getHyphenMinusKeyword_5_0());
                    			

                    				newCompositeNode(grammarAccess.getVersionAccess().getVIDParserRuleCall_5_1());
                    			
                    pushFollow(FOLLOW_84);
                    this_VID_6=ruleVID();

                    state._fsp--;


                    				current.merge(this_VID_6);
                    			

                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalABSReport.g:5967:3: (kw= '+' this_VID_8= ruleVID )?
            int alt120=2;
            int LA120_0 = input.LA(1);

            if ( (LA120_0==86) ) {
                alt120=1;
            }
            switch (alt120) {
                case 1 :
                    // InternalABSReport.g:5968:4: kw= '+' this_VID_8= ruleVID
                    {
                    kw=(Token)match(input,86,FOLLOW_83); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getVersionAccess().getPlusSignKeyword_6_0());
                    			

                    				newCompositeNode(grammarAccess.getVersionAccess().getVIDParserRuleCall_6_1());
                    			
                    pushFollow(FOLLOW_2);
                    this_VID_8=ruleVID();

                    state._fsp--;


                    				current.merge(this_VID_8);
                    			

                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVersion"


    // $ANTLR start "entryRuleVID"
    // InternalABSReport.g:5988:1: entryRuleVID returns [String current=null] : iv_ruleVID= ruleVID EOF ;
    public final String entryRuleVID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleVID = null;


        try {
            // InternalABSReport.g:5988:43: (iv_ruleVID= ruleVID EOF )
            // InternalABSReport.g:5989:2: iv_ruleVID= ruleVID EOF
            {
             newCompositeNode(grammarAccess.getVIDRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVID=ruleVID();

            state._fsp--;

             current =iv_ruleVID.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVID"


    // $ANTLR start "ruleVID"
    // InternalABSReport.g:5995:1: ruleVID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID | this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleVID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalABSReport.g:6001:2: ( (this_ID_0= RULE_ID | this_INT_1= RULE_INT ) )
            // InternalABSReport.g:6002:2: (this_ID_0= RULE_ID | this_INT_1= RULE_INT )
            {
            // InternalABSReport.g:6002:2: (this_ID_0= RULE_ID | this_INT_1= RULE_INT )
            int alt121=2;
            int LA121_0 = input.LA(1);

            if ( (LA121_0==RULE_ID) ) {
                alt121=1;
            }
            else if ( (LA121_0==RULE_INT) ) {
                alt121=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 121, 0, input);

                throw nvae;
            }
            switch (alt121) {
                case 1 :
                    // InternalABSReport.g:6003:3: this_ID_0= RULE_ID
                    {
                    this_ID_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_0);
                    		

                    			newLeafNode(this_ID_0, grammarAccess.getVIDAccess().getIDTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalABSReport.g:6011:3: this_INT_1= RULE_INT
                    {
                    this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

                    			current.merge(this_INT_1);
                    		

                    			newLeafNode(this_INT_1, grammarAccess.getVIDAccess().getINTTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVID"


    // $ANTLR start "entryRuleUndefined"
    // InternalABSReport.g:6022:1: entryRuleUndefined returns [EObject current=null] : iv_ruleUndefined= ruleUndefined EOF ;
    public final EObject entryRuleUndefined() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUndefined = null;


        try {
            // InternalABSReport.g:6022:50: (iv_ruleUndefined= ruleUndefined EOF )
            // InternalABSReport.g:6023:2: iv_ruleUndefined= ruleUndefined EOF
            {
             newCompositeNode(grammarAccess.getUndefinedRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUndefined=ruleUndefined();

            state._fsp--;

             current =iv_ruleUndefined; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUndefined"


    // $ANTLR start "ruleUndefined"
    // InternalABSReport.g:6029:1: ruleUndefined returns [EObject current=null] : ( () otherlv_1= 'undefined' ) ;
    public final EObject ruleUndefined() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalABSReport.g:6035:2: ( ( () otherlv_1= 'undefined' ) )
            // InternalABSReport.g:6036:2: ( () otherlv_1= 'undefined' )
            {
            // InternalABSReport.g:6036:2: ( () otherlv_1= 'undefined' )
            // InternalABSReport.g:6037:3: () otherlv_1= 'undefined'
            {
            // InternalABSReport.g:6037:3: ()
            // InternalABSReport.g:6038:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getUndefinedAccess().getUndefinedConstantAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,35,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getUndefinedAccess().getUndefinedKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUndefined"


    // $ANTLR start "entryRuleEString"
    // InternalABSReport.g:6052:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalABSReport.g:6052:47: (iv_ruleEString= ruleEString EOF )
            // InternalABSReport.g:6053:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalABSReport.g:6059:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalABSReport.g:6065:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalABSReport.g:6066:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalABSReport.g:6066:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt122=2;
            int LA122_0 = input.LA(1);

            if ( (LA122_0==RULE_STRING) ) {
                alt122=1;
            }
            else if ( (LA122_0==RULE_ID) ) {
                alt122=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 122, 0, input);

                throw nvae;
            }
            switch (alt122) {
                case 1 :
                    // InternalABSReport.g:6067:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalABSReport.g:6075:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleImport"
    // InternalABSReport.g:6086:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalABSReport.g:6086:47: (iv_ruleImport= ruleImport EOF )
            // InternalABSReport.g:6087:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalABSReport.g:6093:1: ruleImport returns [EObject current=null] : ( ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) ) | (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_importURI_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token lv_importURI_7_0=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_importedNamespace_2_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:6099:2: ( ( ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) ) | (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' ) ) )
            // InternalABSReport.g:6100:2: ( ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) ) | (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' ) )
            {
            // InternalABSReport.g:6100:2: ( ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) ) | (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' ) )
            int alt124=2;
            int LA124_0 = input.LA(1);

            if ( (LA124_0==87) ) {
                alt124=1;
            }
            else if ( (LA124_0==89) ) {
                alt124=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 124, 0, input);

                throw nvae;
            }
            switch (alt124) {
                case 1 :
                    // InternalABSReport.g:6101:3: ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) )
                    {
                    // InternalABSReport.g:6101:3: ( () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' ) )
                    // InternalABSReport.g:6102:4: () (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' )
                    {
                    // InternalABSReport.g:6102:4: ()
                    // InternalABSReport.g:6103:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getImportAccess().getImportAction_0_0(),
                    						current);
                    				

                    }

                    // InternalABSReport.g:6109:4: (otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';' )
                    // InternalABSReport.g:6110:5: otherlv_1= 'with' ( (lv_importedNamespace_2_0= ruleImportedFQN ) ) (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )? otherlv_5= ';'
                    {
                    otherlv_1=(Token)match(input,87,FOLLOW_5); 

                    					newLeafNode(otherlv_1, grammarAccess.getImportAccess().getWithKeyword_0_1_0());
                    				
                    // InternalABSReport.g:6114:5: ( (lv_importedNamespace_2_0= ruleImportedFQN ) )
                    // InternalABSReport.g:6115:6: (lv_importedNamespace_2_0= ruleImportedFQN )
                    {
                    // InternalABSReport.g:6115:6: (lv_importedNamespace_2_0= ruleImportedFQN )
                    // InternalABSReport.g:6116:7: lv_importedNamespace_2_0= ruleImportedFQN
                    {

                    							newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceImportedFQNParserRuleCall_0_1_1_0());
                    						
                    pushFollow(FOLLOW_85);
                    lv_importedNamespace_2_0=ruleImportedFQN();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getImportRule());
                    							}
                    							set(
                    								current,
                    								"importedNamespace",
                    								lv_importedNamespace_2_0,
                    								"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.ImportedFQN");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalABSReport.g:6133:5: (otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) ) )?
                    int alt123=2;
                    int LA123_0 = input.LA(1);

                    if ( (LA123_0==88) ) {
                        alt123=1;
                    }
                    switch (alt123) {
                        case 1 :
                            // InternalABSReport.g:6134:6: otherlv_3= 'from' ( (lv_importURI_4_0= RULE_STRING ) )
                            {
                            otherlv_3=(Token)match(input,88,FOLLOW_16); 

                            						newLeafNode(otherlv_3, grammarAccess.getImportAccess().getFromKeyword_0_1_2_0());
                            					
                            // InternalABSReport.g:6138:6: ( (lv_importURI_4_0= RULE_STRING ) )
                            // InternalABSReport.g:6139:7: (lv_importURI_4_0= RULE_STRING )
                            {
                            // InternalABSReport.g:6139:7: (lv_importURI_4_0= RULE_STRING )
                            // InternalABSReport.g:6140:8: lv_importURI_4_0= RULE_STRING
                            {
                            lv_importURI_4_0=(Token)match(input,RULE_STRING,FOLLOW_65); 

                            								newLeafNode(lv_importURI_4_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_0_1_2_1_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getImportRule());
                            								}
                            								setWithLastConsumed(
                            									current,
                            									"importURI",
                            									lv_importURI_4_0,
                            									"org.eclipse.xtext.common.Terminals.STRING");
                            							

                            }


                            }


                            }
                            break;

                    }

                    otherlv_5=(Token)match(input,70,FOLLOW_2); 

                    					newLeafNode(otherlv_5, grammarAccess.getImportAccess().getSemicolonKeyword_0_1_3());
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:6164:3: (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' )
                    {
                    // InternalABSReport.g:6164:3: (otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';' )
                    // InternalABSReport.g:6165:4: otherlv_6= 'import' ( (lv_importURI_7_0= RULE_STRING ) ) otherlv_8= ';'
                    {
                    otherlv_6=(Token)match(input,89,FOLLOW_16); 

                    				newLeafNode(otherlv_6, grammarAccess.getImportAccess().getImportKeyword_1_0());
                    			
                    // InternalABSReport.g:6169:4: ( (lv_importURI_7_0= RULE_STRING ) )
                    // InternalABSReport.g:6170:5: (lv_importURI_7_0= RULE_STRING )
                    {
                    // InternalABSReport.g:6170:5: (lv_importURI_7_0= RULE_STRING )
                    // InternalABSReport.g:6171:6: lv_importURI_7_0= RULE_STRING
                    {
                    lv_importURI_7_0=(Token)match(input,RULE_STRING,FOLLOW_65); 

                    						newLeafNode(lv_importURI_7_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getImportRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"importURI",
                    							lv_importURI_7_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }

                    otherlv_8=(Token)match(input,70,FOLLOW_2); 

                    				newLeafNode(otherlv_8, grammarAccess.getImportAccess().getSemicolonKeyword_1_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalABSReport.g:6196:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalABSReport.g:6196:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalABSReport.g:6197:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalABSReport.g:6203:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalABSReport.g:6209:2: ( (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* ) )
            // InternalABSReport.g:6210:2: (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* )
            {
            // InternalABSReport.g:6210:2: (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* )
            // InternalABSReport.g:6211:3: this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_86); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalABSReport.g:6218:3: (kw= '::' this_ID_2= RULE_ID )*
            loop125:
            do {
                int alt125=2;
                int LA125_0 = input.LA(1);

                if ( (LA125_0==90) ) {
                    int LA125_2 = input.LA(2);

                    if ( (LA125_2==RULE_ID) ) {
                        alt125=1;
                    }


                }


                switch (alt125) {
            	case 1 :
            	    // InternalABSReport.g:6219:4: kw= '::' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,90,FOLLOW_5); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getColonColonKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_86); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop125;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleImportedFQN"
    // InternalABSReport.g:6236:1: entryRuleImportedFQN returns [String current=null] : iv_ruleImportedFQN= ruleImportedFQN EOF ;
    public final String entryRuleImportedFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleImportedFQN = null;


        try {
            // InternalABSReport.g:6236:51: (iv_ruleImportedFQN= ruleImportedFQN EOF )
            // InternalABSReport.g:6237:2: iv_ruleImportedFQN= ruleImportedFQN EOF
            {
             newCompositeNode(grammarAccess.getImportedFQNRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImportedFQN=ruleImportedFQN();

            state._fsp--;

             current =iv_ruleImportedFQN.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportedFQN"


    // $ANTLR start "ruleImportedFQN"
    // InternalABSReport.g:6243:1: ruleImportedFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )? ) ;
    public final AntlrDatatypeRuleToken ruleImportedFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:6249:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )? ) )
            // InternalABSReport.g:6250:2: (this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )? )
            {
            // InternalABSReport.g:6250:2: (this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )? )
            // InternalABSReport.g:6251:3: this_QualifiedName_0= ruleQualifiedName (kw= '::' kw= '*' )?
            {

            			newCompositeNode(grammarAccess.getImportedFQNAccess().getQualifiedNameParserRuleCall_0());
            		
            pushFollow(FOLLOW_86);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            			current.merge(this_QualifiedName_0);
            		

            			afterParserOrEnumRuleCall();
            		
            // InternalABSReport.g:6261:3: (kw= '::' kw= '*' )?
            int alt126=2;
            int LA126_0 = input.LA(1);

            if ( (LA126_0==90) ) {
                alt126=1;
            }
            switch (alt126) {
                case 1 :
                    // InternalABSReport.g:6262:4: kw= '::' kw= '*'
                    {
                    kw=(Token)match(input,90,FOLLOW_87); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getImportedFQNAccess().getColonColonKeyword_1_0());
                    			
                    kw=(Token)match(input,91,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getImportedFQNAccess().getAsteriskKeyword_1_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportedFQN"


    // $ANTLR start "entryRuleLocaleGroup"
    // InternalABSReport.g:6277:1: entryRuleLocaleGroup returns [EObject current=null] : iv_ruleLocaleGroup= ruleLocaleGroup EOF ;
    public final EObject entryRuleLocaleGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLocaleGroup = null;


        try {
            // InternalABSReport.g:6277:52: (iv_ruleLocaleGroup= ruleLocaleGroup EOF )
            // InternalABSReport.g:6278:2: iv_ruleLocaleGroup= ruleLocaleGroup EOF
            {
             newCompositeNode(grammarAccess.getLocaleGroupRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLocaleGroup=ruleLocaleGroup();

            state._fsp--;

             current =iv_ruleLocaleGroup; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLocaleGroup"


    // $ANTLR start "ruleLocaleGroup"
    // InternalABSReport.g:6284:1: ruleLocaleGroup returns [EObject current=null] : (otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}' ) ;
    public final EObject ruleLocaleGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_locale_4_0=null;
        Token otherlv_6=null;
        EObject lv_objectLocales_5_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:6290:2: ( (otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}' ) )
            // InternalABSReport.g:6291:2: (otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}' )
            {
            // InternalABSReport.g:6291:2: (otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}' )
            // InternalABSReport.g:6292:3: otherlv_0= 'Locale' otherlv_1= '{' otherlv_2= 'locale' otherlv_3= '=' ( (lv_locale_4_0= RULE_STRING ) ) ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,92,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getLocaleGroupAccess().getLocaleKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_88); 

            			newLeafNode(otherlv_1, grammarAccess.getLocaleGroupAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,93,FOLLOW_25); 

            			newLeafNode(otherlv_2, grammarAccess.getLocaleGroupAccess().getLocaleKeyword_2());
            		
            otherlv_3=(Token)match(input,31,FOLLOW_16); 

            			newLeafNode(otherlv_3, grammarAccess.getLocaleGroupAccess().getEqualsSignKeyword_3());
            		
            // InternalABSReport.g:6308:3: ( (lv_locale_4_0= RULE_STRING ) )
            // InternalABSReport.g:6309:4: (lv_locale_4_0= RULE_STRING )
            {
            // InternalABSReport.g:6309:4: (lv_locale_4_0= RULE_STRING )
            // InternalABSReport.g:6310:5: lv_locale_4_0= RULE_STRING
            {
            lv_locale_4_0=(Token)match(input,RULE_STRING,FOLLOW_89); 

            					newLeafNode(lv_locale_4_0, grammarAccess.getLocaleGroupAccess().getLocaleSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLocaleGroupRule());
            					}
            					setWithLastConsumed(
            						current,
            						"locale",
            						lv_locale_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalABSReport.g:6326:3: ( (lv_objectLocales_5_0= ruleDefinitionGroupLocale ) )*
            loop127:
            do {
                int alt127=2;
                int LA127_0 = input.LA(1);

                if ( (LA127_0==36) ) {
                    alt127=1;
                }


                switch (alt127) {
            	case 1 :
            	    // InternalABSReport.g:6327:4: (lv_objectLocales_5_0= ruleDefinitionGroupLocale )
            	    {
            	    // InternalABSReport.g:6327:4: (lv_objectLocales_5_0= ruleDefinitionGroupLocale )
            	    // InternalABSReport.g:6328:5: lv_objectLocales_5_0= ruleDefinitionGroupLocale
            	    {

            	    					newCompositeNode(grammarAccess.getLocaleGroupAccess().getObjectLocalesDefinitionGroupLocaleParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_89);
            	    lv_objectLocales_5_0=ruleDefinitionGroupLocale();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getLocaleGroupRule());
            	    					}
            	    					add(
            	    						current,
            	    						"objectLocales",
            	    						lv_objectLocales_5_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.DefinitionGroupLocale");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop127;
                }
            } while (true);

            otherlv_6=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getLocaleGroupAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLocaleGroup"


    // $ANTLR start "entryRuleDefinitionGroupLocale"
    // InternalABSReport.g:6353:1: entryRuleDefinitionGroupLocale returns [EObject current=null] : iv_ruleDefinitionGroupLocale= ruleDefinitionGroupLocale EOF ;
    public final EObject entryRuleDefinitionGroupLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinitionGroupLocale = null;


        try {
            // InternalABSReport.g:6353:62: (iv_ruleDefinitionGroupLocale= ruleDefinitionGroupLocale EOF )
            // InternalABSReport.g:6354:2: iv_ruleDefinitionGroupLocale= ruleDefinitionGroupLocale EOF
            {
             newCompositeNode(grammarAccess.getDefinitionGroupLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefinitionGroupLocale=ruleDefinitionGroupLocale();

            state._fsp--;

             current =iv_ruleDefinitionGroupLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinitionGroupLocale"


    // $ANTLR start "ruleDefinitionGroupLocale"
    // InternalABSReport.g:6360:1: ruleDefinitionGroupLocale returns [EObject current=null] : (otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}' ) ;
    public final EObject ruleDefinitionGroupLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_annotations_5_0 = null;

        EObject lv_definitionLocales_7_1 = null;

        EObject lv_definitionLocales_7_2 = null;

        EObject lv_definitionLocales_7_3 = null;

        EObject lv_definitionLocales_7_4 = null;

        EObject lv_definitionLocales_7_5 = null;

        EObject lv_definitionLocales_7_6 = null;



        	enterRule();

        try {
            // InternalABSReport.g:6366:2: ( (otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}' ) )
            // InternalABSReport.g:6367:2: (otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}' )
            {
            // InternalABSReport.g:6367:2: (otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}' )
            // InternalABSReport.g:6368:3: otherlv_0= 'DefinitionGroup' ( ( ruleQualifiedName ) ) otherlv_2= '{' (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )? ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )* otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,36,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionGroupKeyword_0());
            		
            // InternalABSReport.g:6372:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:6373:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:6373:4: ( ruleQualifiedName )
            // InternalABSReport.g:6374:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDefinitionGroupLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getRefDefinitionGroupCrossReference_1_0());
            				
            pushFollow(FOLLOW_6);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_90); 

            			newLeafNode(otherlv_2, grammarAccess.getDefinitionGroupLocaleAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalABSReport.g:6392:3: (otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}' )?
            int alt129=2;
            int LA129_0 = input.LA(1);

            if ( (LA129_0==38) ) {
                alt129=1;
            }
            switch (alt129) {
                case 1 :
                    // InternalABSReport.g:6393:4: otherlv_3= 'annotations' otherlv_4= '{' ( (lv_annotations_5_0= ruleAnnotationEntry ) )* otherlv_6= '}'
                    {
                    otherlv_3=(Token)match(input,38,FOLLOW_6); 

                    				newLeafNode(otherlv_3, grammarAccess.getDefinitionGroupLocaleAccess().getAnnotationsKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,12,FOLLOW_32); 

                    				newLeafNode(otherlv_4, grammarAccess.getDefinitionGroupLocaleAccess().getLeftCurlyBracketKeyword_3_1());
                    			
                    // InternalABSReport.g:6401:4: ( (lv_annotations_5_0= ruleAnnotationEntry ) )*
                    loop128:
                    do {
                        int alt128=2;
                        int LA128_0 = input.LA(1);

                        if ( (LA128_0==RULE_STRING||LA128_0==RULE_ID) ) {
                            alt128=1;
                        }


                        switch (alt128) {
                    	case 1 :
                    	    // InternalABSReport.g:6402:5: (lv_annotations_5_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalABSReport.g:6402:5: (lv_annotations_5_0= ruleAnnotationEntry )
                    	    // InternalABSReport.g:6403:6: lv_annotations_5_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getAnnotationsAnnotationEntryParserRuleCall_3_2_0());
                    	    					
                    	    pushFollow(FOLLOW_32);
                    	    lv_annotations_5_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_5_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop128;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,13,FOLLOW_91); 

                    				newLeafNode(otherlv_6, grammarAccess.getDefinitionGroupLocaleAccess().getRightCurlyBracketKeyword_3_3());
                    			

                    }
                    break;

            }

            // InternalABSReport.g:6425:3: ( ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) ) )*
            loop131:
            do {
                int alt131=2;
                int LA131_0 = input.LA(1);

                if ( (LA131_0==36||LA131_0==42||(LA131_0>=46 && LA131_0<=47)||(LA131_0>=95 && LA131_0<=96)) ) {
                    alt131=1;
                }


                switch (alt131) {
            	case 1 :
            	    // InternalABSReport.g:6426:4: ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) )
            	    {
            	    // InternalABSReport.g:6426:4: ( (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale ) )
            	    // InternalABSReport.g:6427:5: (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale )
            	    {
            	    // InternalABSReport.g:6427:5: (lv_definitionLocales_7_1= ruleDefinitionGroupLocale | lv_definitionLocales_7_2= ruleAssetTypeLocale | lv_definitionLocales_7_3= ruleAssetTypeAspectLocale | lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale | lv_definitionLocales_7_5= ruleGuardLocale | lv_definitionLocales_7_6= ruleRequirementLocale )
            	    int alt130=6;
            	    switch ( input.LA(1) ) {
            	    case 36:
            	        {
            	        alt130=1;
            	        }
            	        break;
            	    case 42:
            	        {
            	        alt130=2;
            	        }
            	        break;
            	    case 46:
            	        {
            	        alt130=3;
            	        }
            	        break;
            	    case 47:
            	        {
            	        alt130=4;
            	        }
            	        break;
            	    case 95:
            	        {
            	        alt130=5;
            	        }
            	        break;
            	    case 96:
            	        {
            	        alt130=6;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 130, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt130) {
            	        case 1 :
            	            // InternalABSReport.g:6428:6: lv_definitionLocales_7_1= ruleDefinitionGroupLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesDefinitionGroupLocaleParserRuleCall_4_0_0());
            	            					
            	            pushFollow(FOLLOW_91);
            	            lv_definitionLocales_7_1=ruleDefinitionGroupLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_1,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.DefinitionGroupLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 2 :
            	            // InternalABSReport.g:6444:6: lv_definitionLocales_7_2= ruleAssetTypeLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesAssetTypeLocaleParserRuleCall_4_0_1());
            	            					
            	            pushFollow(FOLLOW_91);
            	            lv_definitionLocales_7_2=ruleAssetTypeLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_2,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 3 :
            	            // InternalABSReport.g:6460:6: lv_definitionLocales_7_3= ruleAssetTypeAspectLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesAssetTypeAspectLocaleParserRuleCall_4_0_2());
            	            					
            	            pushFollow(FOLLOW_91);
            	            lv_definitionLocales_7_3=ruleAssetTypeAspectLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_3,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeAspectLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 4 :
            	            // InternalABSReport.g:6476:6: lv_definitionLocales_7_4= rulePrimitiveDataTypeLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesPrimitiveDataTypeLocaleParserRuleCall_4_0_3());
            	            					
            	            pushFollow(FOLLOW_91);
            	            lv_definitionLocales_7_4=rulePrimitiveDataTypeLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_4,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.PrimitiveDataTypeLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 5 :
            	            // InternalABSReport.g:6492:6: lv_definitionLocales_7_5= ruleGuardLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesGuardLocaleParserRuleCall_4_0_4());
            	            					
            	            pushFollow(FOLLOW_91);
            	            lv_definitionLocales_7_5=ruleGuardLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_5,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.GuardLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 6 :
            	            // InternalABSReport.g:6508:6: lv_definitionLocales_7_6= ruleRequirementLocale
            	            {

            	            						newCompositeNode(grammarAccess.getDefinitionGroupLocaleAccess().getDefinitionLocalesRequirementLocaleParserRuleCall_4_0_5());
            	            					
            	            pushFollow(FOLLOW_91);
            	            lv_definitionLocales_7_6=ruleRequirementLocale();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getDefinitionGroupLocaleRule());
            	            						}
            	            						add(
            	            							current,
            	            							"definitionLocales",
            	            							lv_definitionLocales_7_6,
            	            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.RequirementLocale");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop131;
                }
            } while (true);

            otherlv_8=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getDefinitionGroupLocaleAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinitionGroupLocale"


    // $ANTLR start "entryRuleAssetTypeLocale"
    // InternalABSReport.g:6534:1: entryRuleAssetTypeLocale returns [EObject current=null] : iv_ruleAssetTypeLocale= ruleAssetTypeLocale EOF ;
    public final EObject entryRuleAssetTypeLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeLocale = null;


        try {
            // InternalABSReport.g:6534:56: (iv_ruleAssetTypeLocale= ruleAssetTypeLocale EOF )
            // InternalABSReport.g:6535:2: iv_ruleAssetTypeLocale= ruleAssetTypeLocale EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeLocale=ruleAssetTypeLocale();

            state._fsp--;

             current =iv_ruleAssetTypeLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeLocale"


    // $ANTLR start "ruleAssetTypeLocale"
    // InternalABSReport.g:6541:1: ruleAssetTypeLocale returns [EObject current=null] : ( () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}' ) ;
    public final EObject ruleAssetTypeLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_description_9_0=null;
        Token otherlv_11=null;
        Enumerator lv_descriptionFormat_8_0 = null;

        EObject lv_features_10_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:6547:2: ( ( () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}' ) )
            // InternalABSReport.g:6548:2: ( () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}' )
            {
            // InternalABSReport.g:6548:2: ( () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}' )
            // InternalABSReport.g:6549:3: () otherlv_1= 'AssetType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )* otherlv_11= '}'
            {
            // InternalABSReport.g:6549:3: ()
            // InternalABSReport.g:6550:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeLocaleAccess().getAssetTypeLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,42,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetTypeLocaleAccess().getAssetTypeKeyword_1());
            		
            // InternalABSReport.g:6560:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:6561:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:6561:4: ( ruleQualifiedName )
            // InternalABSReport.g:6562:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeLocaleAccess().getRefAssetTypeCrossReference_2_0());
            				
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,31,FOLLOW_16); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetTypeLocaleAccess().getEqualsSignKeyword_3());
            		
            // InternalABSReport.g:6580:3: ( (lv_name_4_0= RULE_STRING ) )
            // InternalABSReport.g:6581:4: (lv_name_4_0= RULE_STRING )
            {
            // InternalABSReport.g:6581:4: (lv_name_4_0= RULE_STRING )
            // InternalABSReport.g:6582:5: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_6); 

            					newLeafNode(lv_name_4_0, grammarAccess.getAssetTypeLocaleAccess().getNameSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_5=(Token)match(input,12,FOLLOW_92); 

            			newLeafNode(otherlv_5, grammarAccess.getAssetTypeLocaleAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalABSReport.g:6602:3: (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )?
            int alt133=2;
            int LA133_0 = input.LA(1);

            if ( (LA133_0==45) ) {
                alt133=1;
            }
            switch (alt133) {
                case 1 :
                    // InternalABSReport.g:6603:4: otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) )
                    {
                    otherlv_6=(Token)match(input,45,FOLLOW_25); 

                    				newLeafNode(otherlv_6, grammarAccess.getAssetTypeLocaleAccess().getDescriptionKeyword_6_0());
                    			
                    otherlv_7=(Token)match(input,31,FOLLOW_39); 

                    				newLeafNode(otherlv_7, grammarAccess.getAssetTypeLocaleAccess().getEqualsSignKeyword_6_1());
                    			
                    // InternalABSReport.g:6611:4: ( (lv_descriptionFormat_8_0= ruleTextFormat ) )?
                    int alt132=2;
                    int LA132_0 = input.LA(1);

                    if ( ((LA132_0>=112 && LA132_0<=114)) ) {
                        alt132=1;
                    }
                    switch (alt132) {
                        case 1 :
                            // InternalABSReport.g:6612:5: (lv_descriptionFormat_8_0= ruleTextFormat )
                            {
                            // InternalABSReport.g:6612:5: (lv_descriptionFormat_8_0= ruleTextFormat )
                            // InternalABSReport.g:6613:6: lv_descriptionFormat_8_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getAssetTypeLocaleAccess().getDescriptionFormatTextFormatEnumRuleCall_6_2_0());
                            					
                            pushFollow(FOLLOW_16);
                            lv_descriptionFormat_8_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getAssetTypeLocaleRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_8_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:6630:4: ( (lv_description_9_0= RULE_STRING ) )
                    // InternalABSReport.g:6631:5: (lv_description_9_0= RULE_STRING )
                    {
                    // InternalABSReport.g:6631:5: (lv_description_9_0= RULE_STRING )
                    // InternalABSReport.g:6632:6: lv_description_9_0= RULE_STRING
                    {
                    lv_description_9_0=(Token)match(input,RULE_STRING,FOLLOW_23); 

                    						newLeafNode(lv_description_9_0, grammarAccess.getAssetTypeLocaleAccess().getDescriptionSTRINGTerminalRuleCall_6_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAssetTypeLocaleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_9_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalABSReport.g:6649:3: ( (lv_features_10_0= ruleAssetTypeFeatureLocale ) )*
            loop134:
            do {
                int alt134=2;
                int LA134_0 = input.LA(1);

                if ( (LA134_0==30||LA134_0==34) ) {
                    alt134=1;
                }


                switch (alt134) {
            	case 1 :
            	    // InternalABSReport.g:6650:4: (lv_features_10_0= ruleAssetTypeFeatureLocale )
            	    {
            	    // InternalABSReport.g:6650:4: (lv_features_10_0= ruleAssetTypeFeatureLocale )
            	    // InternalABSReport.g:6651:5: lv_features_10_0= ruleAssetTypeFeatureLocale
            	    {

            	    					newCompositeNode(grammarAccess.getAssetTypeLocaleAccess().getFeaturesAssetTypeFeatureLocaleParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_23);
            	    lv_features_10_0=ruleAssetTypeFeatureLocale();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAssetTypeLocaleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"features",
            	    						lv_features_10_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeFeatureLocale");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop134;
                }
            } while (true);

            otherlv_11=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getAssetTypeLocaleAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeLocale"


    // $ANTLR start "entryRuleAssetTypeAspectLocale"
    // InternalABSReport.g:6676:1: entryRuleAssetTypeAspectLocale returns [EObject current=null] : iv_ruleAssetTypeAspectLocale= ruleAssetTypeAspectLocale EOF ;
    public final EObject entryRuleAssetTypeAspectLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeAspectLocale = null;


        try {
            // InternalABSReport.g:6676:62: (iv_ruleAssetTypeAspectLocale= ruleAssetTypeAspectLocale EOF )
            // InternalABSReport.g:6677:2: iv_ruleAssetTypeAspectLocale= ruleAssetTypeAspectLocale EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeAspectLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeAspectLocale=ruleAssetTypeAspectLocale();

            state._fsp--;

             current =iv_ruleAssetTypeAspectLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeAspectLocale"


    // $ANTLR start "ruleAssetTypeAspectLocale"
    // InternalABSReport.g:6683:1: ruleAssetTypeAspectLocale returns [EObject current=null] : ( () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}' ) ;
    public final EObject ruleAssetTypeAspectLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_features_4_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:6689:2: ( ( () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}' ) )
            // InternalABSReport.g:6690:2: ( () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}' )
            {
            // InternalABSReport.g:6690:2: ( () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}' )
            // InternalABSReport.g:6691:3: () otherlv_1= 'AssetTypeAspect' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )* otherlv_5= '}'
            {
            // InternalABSReport.g:6691:3: ()
            // InternalABSReport.g:6692:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeAspectLocaleAccess().getAssetTypeAspectLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,46,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getAssetTypeAspectLocaleAccess().getAssetTypeAspectKeyword_1());
            		
            // InternalABSReport.g:6702:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:6703:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:6703:4: ( ruleQualifiedName )
            // InternalABSReport.g:6704:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeAspectLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssetTypeAspectLocaleAccess().getBaseAssetTypeAssetTypeCrossReference_2_0());
            				
            pushFollow(FOLLOW_6);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_23); 

            			newLeafNode(otherlv_3, grammarAccess.getAssetTypeAspectLocaleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalABSReport.g:6722:3: ( (lv_features_4_0= ruleAssetTypeFeatureLocale ) )*
            loop135:
            do {
                int alt135=2;
                int LA135_0 = input.LA(1);

                if ( (LA135_0==30||LA135_0==34) ) {
                    alt135=1;
                }


                switch (alt135) {
            	case 1 :
            	    // InternalABSReport.g:6723:4: (lv_features_4_0= ruleAssetTypeFeatureLocale )
            	    {
            	    // InternalABSReport.g:6723:4: (lv_features_4_0= ruleAssetTypeFeatureLocale )
            	    // InternalABSReport.g:6724:5: lv_features_4_0= ruleAssetTypeFeatureLocale
            	    {

            	    					newCompositeNode(grammarAccess.getAssetTypeAspectLocaleAccess().getFeaturesAssetTypeFeatureLocaleParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_23);
            	    lv_features_4_0=ruleAssetTypeFeatureLocale();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAssetTypeAspectLocaleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"features",
            	    						lv_features_4_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AssetTypeFeatureLocale");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop135;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getAssetTypeAspectLocaleAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeAspectLocale"


    // $ANTLR start "entryRuleAssetTypeFeatureLocale"
    // InternalABSReport.g:6749:1: entryRuleAssetTypeFeatureLocale returns [EObject current=null] : iv_ruleAssetTypeFeatureLocale= ruleAssetTypeFeatureLocale EOF ;
    public final EObject entryRuleAssetTypeFeatureLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssetTypeFeatureLocale = null;


        try {
            // InternalABSReport.g:6749:63: (iv_ruleAssetTypeFeatureLocale= ruleAssetTypeFeatureLocale EOF )
            // InternalABSReport.g:6750:2: iv_ruleAssetTypeFeatureLocale= ruleAssetTypeFeatureLocale EOF
            {
             newCompositeNode(grammarAccess.getAssetTypeFeatureLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssetTypeFeatureLocale=ruleAssetTypeFeatureLocale();

            state._fsp--;

             current =iv_ruleAssetTypeFeatureLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssetTypeFeatureLocale"


    // $ANTLR start "ruleAssetTypeFeatureLocale"
    // InternalABSReport.g:6756:1: ruleAssetTypeFeatureLocale returns [EObject current=null] : ( () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) ) ) ;
    public final EObject ruleAssetTypeFeatureLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_name_6_0=null;


        	enterRule();

        try {
            // InternalABSReport.g:6762:2: ( ( () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) ) ) )
            // InternalABSReport.g:6763:2: ( () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) ) )
            {
            // InternalABSReport.g:6763:2: ( () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) ) )
            // InternalABSReport.g:6764:3: () ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) ) otherlv_5= '=' ( (lv_name_6_0= RULE_STRING ) )
            {
            // InternalABSReport.g:6764:3: ()
            // InternalABSReport.g:6765:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAssetTypeFeatureLocaleAccess().getAssetTypeFeatureLocaleAction_0(),
            					current);
            			

            }

            // InternalABSReport.g:6771:3: ( (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) ) | (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) ) )
            int alt136=2;
            int LA136_0 = input.LA(1);

            if ( (LA136_0==34) ) {
                alt136=1;
            }
            else if ( (LA136_0==30) ) {
                alt136=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 136, 0, input);

                throw nvae;
            }
            switch (alt136) {
                case 1 :
                    // InternalABSReport.g:6772:4: (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) )
                    {
                    // InternalABSReport.g:6772:4: (otherlv_1= 'reference' ( ( ruleQualifiedName ) ) )
                    // InternalABSReport.g:6773:5: otherlv_1= 'reference' ( ( ruleQualifiedName ) )
                    {
                    otherlv_1=(Token)match(input,34,FOLLOW_5); 

                    					newLeafNode(otherlv_1, grammarAccess.getAssetTypeFeatureLocaleAccess().getReferenceKeyword_1_0_0());
                    				
                    // InternalABSReport.g:6777:5: ( ( ruleQualifiedName ) )
                    // InternalABSReport.g:6778:6: ( ruleQualifiedName )
                    {
                    // InternalABSReport.g:6778:6: ( ruleQualifiedName )
                    // InternalABSReport.g:6779:7: ruleQualifiedName
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getAssetTypeFeatureLocaleRule());
                    							}
                    						

                    							newCompositeNode(grammarAccess.getAssetTypeFeatureLocaleAccess().getRefAssetTypeReferenceCrossReference_1_0_1_0());
                    						
                    pushFollow(FOLLOW_25);
                    ruleQualifiedName();

                    state._fsp--;


                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:6795:4: (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) )
                    {
                    // InternalABSReport.g:6795:4: (otherlv_3= 'attribute' ( ( ruleQualifiedName ) ) )
                    // InternalABSReport.g:6796:5: otherlv_3= 'attribute' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,30,FOLLOW_5); 

                    					newLeafNode(otherlv_3, grammarAccess.getAssetTypeFeatureLocaleAccess().getAttributeKeyword_1_1_0());
                    				
                    // InternalABSReport.g:6800:5: ( ( ruleQualifiedName ) )
                    // InternalABSReport.g:6801:6: ( ruleQualifiedName )
                    {
                    // InternalABSReport.g:6801:6: ( ruleQualifiedName )
                    // InternalABSReport.g:6802:7: ruleQualifiedName
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getAssetTypeFeatureLocaleRule());
                    							}
                    						

                    							newCompositeNode(grammarAccess.getAssetTypeFeatureLocaleAccess().getRefAssetTypeAttributeCrossReference_1_1_1_0());
                    						
                    pushFollow(FOLLOW_25);
                    ruleQualifiedName();

                    state._fsp--;


                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,31,FOLLOW_16); 

            			newLeafNode(otherlv_5, grammarAccess.getAssetTypeFeatureLocaleAccess().getEqualsSignKeyword_2());
            		
            // InternalABSReport.g:6822:3: ( (lv_name_6_0= RULE_STRING ) )
            // InternalABSReport.g:6823:4: (lv_name_6_0= RULE_STRING )
            {
            // InternalABSReport.g:6823:4: (lv_name_6_0= RULE_STRING )
            // InternalABSReport.g:6824:5: lv_name_6_0= RULE_STRING
            {
            lv_name_6_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_6_0, grammarAccess.getAssetTypeFeatureLocaleAccess().getNameSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssetTypeFeatureLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_6_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssetTypeFeatureLocale"


    // $ANTLR start "entryRulePrimitiveDataTypeLocale"
    // InternalABSReport.g:6844:1: entryRulePrimitiveDataTypeLocale returns [EObject current=null] : iv_rulePrimitiveDataTypeLocale= rulePrimitiveDataTypeLocale EOF ;
    public final EObject entryRulePrimitiveDataTypeLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveDataTypeLocale = null;


        try {
            // InternalABSReport.g:6844:64: (iv_rulePrimitiveDataTypeLocale= rulePrimitiveDataTypeLocale EOF )
            // InternalABSReport.g:6845:2: iv_rulePrimitiveDataTypeLocale= rulePrimitiveDataTypeLocale EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveDataTypeLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimitiveDataTypeLocale=rulePrimitiveDataTypeLocale();

            state._fsp--;

             current =iv_rulePrimitiveDataTypeLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveDataTypeLocale"


    // $ANTLR start "rulePrimitiveDataTypeLocale"
    // InternalABSReport.g:6851:1: rulePrimitiveDataTypeLocale returns [EObject current=null] : ( () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}' ) ;
    public final EObject rulePrimitiveDataTypeLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_literals_6_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:6857:2: ( ( () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}' ) )
            // InternalABSReport.g:6858:2: ( () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}' )
            {
            // InternalABSReport.g:6858:2: ( () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}' )
            // InternalABSReport.g:6859:3: () otherlv_1= 'PrimitiveDataType' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' ( (lv_literals_6_0= ruleEnumLiteralLocale ) )* otherlv_7= '}'
            {
            // InternalABSReport.g:6859:3: ()
            // InternalABSReport.g:6860:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPrimitiveDataTypeLocaleAccess().getPrimitiveDataTypeLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,47,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getPrimitiveDataTypeLocaleAccess().getPrimitiveDataTypeKeyword_1());
            		
            // InternalABSReport.g:6870:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:6871:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:6871:4: ( ruleQualifiedName )
            // InternalABSReport.g:6872:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPrimitiveDataTypeLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getPrimitiveDataTypeLocaleAccess().getRefEnumDataTypeCrossReference_2_0());
            				
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,31,FOLLOW_16); 

            			newLeafNode(otherlv_3, grammarAccess.getPrimitiveDataTypeLocaleAccess().getEqualsSignKeyword_3());
            		
            // InternalABSReport.g:6890:3: ( (lv_name_4_0= RULE_STRING ) )
            // InternalABSReport.g:6891:4: (lv_name_4_0= RULE_STRING )
            {
            // InternalABSReport.g:6891:4: (lv_name_4_0= RULE_STRING )
            // InternalABSReport.g:6892:5: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_6); 

            					newLeafNode(lv_name_4_0, grammarAccess.getPrimitiveDataTypeLocaleAccess().getNameSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPrimitiveDataTypeLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_5=(Token)match(input,12,FOLLOW_93); 

            			newLeafNode(otherlv_5, grammarAccess.getPrimitiveDataTypeLocaleAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalABSReport.g:6912:3: ( (lv_literals_6_0= ruleEnumLiteralLocale ) )*
            loop137:
            do {
                int alt137=2;
                int LA137_0 = input.LA(1);

                if ( (LA137_0==94) ) {
                    alt137=1;
                }


                switch (alt137) {
            	case 1 :
            	    // InternalABSReport.g:6913:4: (lv_literals_6_0= ruleEnumLiteralLocale )
            	    {
            	    // InternalABSReport.g:6913:4: (lv_literals_6_0= ruleEnumLiteralLocale )
            	    // InternalABSReport.g:6914:5: lv_literals_6_0= ruleEnumLiteralLocale
            	    {

            	    					newCompositeNode(grammarAccess.getPrimitiveDataTypeLocaleAccess().getLiteralsEnumLiteralLocaleParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_93);
            	    lv_literals_6_0=ruleEnumLiteralLocale();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPrimitiveDataTypeLocaleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"literals",
            	    						lv_literals_6_0,
            	    						"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.EnumLiteralLocale");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop137;
                }
            } while (true);

            otherlv_7=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getPrimitiveDataTypeLocaleAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveDataTypeLocale"


    // $ANTLR start "entryRuleEnumLiteralLocale"
    // InternalABSReport.g:6939:1: entryRuleEnumLiteralLocale returns [EObject current=null] : iv_ruleEnumLiteralLocale= ruleEnumLiteralLocale EOF ;
    public final EObject entryRuleEnumLiteralLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumLiteralLocale = null;


        try {
            // InternalABSReport.g:6939:58: (iv_ruleEnumLiteralLocale= ruleEnumLiteralLocale EOF )
            // InternalABSReport.g:6940:2: iv_ruleEnumLiteralLocale= ruleEnumLiteralLocale EOF
            {
             newCompositeNode(grammarAccess.getEnumLiteralLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEnumLiteralLocale=ruleEnumLiteralLocale();

            state._fsp--;

             current =iv_ruleEnumLiteralLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumLiteralLocale"


    // $ANTLR start "ruleEnumLiteralLocale"
    // InternalABSReport.g:6946:1: ruleEnumLiteralLocale returns [EObject current=null] : ( () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) ) ;
    public final EObject ruleEnumLiteralLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;


        	enterRule();

        try {
            // InternalABSReport.g:6952:2: ( ( () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) ) )
            // InternalABSReport.g:6953:2: ( () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) )
            {
            // InternalABSReport.g:6953:2: ( () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) )
            // InternalABSReport.g:6954:3: () otherlv_1= 'EnumLiteral' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) )
            {
            // InternalABSReport.g:6954:3: ()
            // InternalABSReport.g:6955:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEnumLiteralLocaleAccess().getEnumLiteralLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,94,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getEnumLiteralLocaleAccess().getEnumLiteralKeyword_1());
            		
            // InternalABSReport.g:6965:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:6966:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:6966:4: ( ruleQualifiedName )
            // InternalABSReport.g:6967:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEnumLiteralLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getEnumLiteralLocaleAccess().getRefEnumLiteralCrossReference_2_0());
            				
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,31,FOLLOW_16); 

            			newLeafNode(otherlv_3, grammarAccess.getEnumLiteralLocaleAccess().getEqualsSignKeyword_3());
            		
            // InternalABSReport.g:6985:3: ( (lv_name_4_0= RULE_STRING ) )
            // InternalABSReport.g:6986:4: (lv_name_4_0= RULE_STRING )
            {
            // InternalABSReport.g:6986:4: (lv_name_4_0= RULE_STRING )
            // InternalABSReport.g:6987:5: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_4_0, grammarAccess.getEnumLiteralLocaleAccess().getNameSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEnumLiteralLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumLiteralLocale"


    // $ANTLR start "entryRuleGuardLocale"
    // InternalABSReport.g:7007:1: entryRuleGuardLocale returns [EObject current=null] : iv_ruleGuardLocale= ruleGuardLocale EOF ;
    public final EObject entryRuleGuardLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardLocale = null;


        try {
            // InternalABSReport.g:7007:52: (iv_ruleGuardLocale= ruleGuardLocale EOF )
            // InternalABSReport.g:7008:2: iv_ruleGuardLocale= ruleGuardLocale EOF
            {
             newCompositeNode(grammarAccess.getGuardLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuardLocale=ruleGuardLocale();

            state._fsp--;

             current =iv_ruleGuardLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardLocale"


    // $ANTLR start "ruleGuardLocale"
    // InternalABSReport.g:7014:1: ruleGuardLocale returns [EObject current=null] : ( () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}' ) ;
    public final EObject ruleGuardLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_description_9_0=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Enumerator lv_descriptionFormat_8_0 = null;

        EObject lv_annotations_12_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:7020:2: ( ( () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}' ) )
            // InternalABSReport.g:7021:2: ( () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}' )
            {
            // InternalABSReport.g:7021:2: ( () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}' )
            // InternalABSReport.g:7022:3: () otherlv_1= 'Guard' ( ( ruleQualifiedName ) ) otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= '{' (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )? (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )? otherlv_14= '}'
            {
            // InternalABSReport.g:7022:3: ()
            // InternalABSReport.g:7023:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGuardLocaleAccess().getGuardLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,95,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getGuardLocaleAccess().getGuardKeyword_1());
            		
            // InternalABSReport.g:7033:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:7034:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:7034:4: ( ruleQualifiedName )
            // InternalABSReport.g:7035:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGuardLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getGuardLocaleAccess().getRefGuardCrossReference_2_0());
            				
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,31,FOLLOW_16); 

            			newLeafNode(otherlv_3, grammarAccess.getGuardLocaleAccess().getEqualsSignKeyword_3());
            		
            // InternalABSReport.g:7053:3: ( (lv_name_4_0= RULE_STRING ) )
            // InternalABSReport.g:7054:4: (lv_name_4_0= RULE_STRING )
            {
            // InternalABSReport.g:7054:4: (lv_name_4_0= RULE_STRING )
            // InternalABSReport.g:7055:5: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_6); 

            					newLeafNode(lv_name_4_0, grammarAccess.getGuardLocaleAccess().getNameSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGuardLocaleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_5=(Token)match(input,12,FOLLOW_94); 

            			newLeafNode(otherlv_5, grammarAccess.getGuardLocaleAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalABSReport.g:7075:3: (otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) ) )?
            int alt139=2;
            int LA139_0 = input.LA(1);

            if ( (LA139_0==45) ) {
                alt139=1;
            }
            switch (alt139) {
                case 1 :
                    // InternalABSReport.g:7076:4: otherlv_6= 'description' otherlv_7= '=' ( (lv_descriptionFormat_8_0= ruleTextFormat ) )? ( (lv_description_9_0= RULE_STRING ) )
                    {
                    otherlv_6=(Token)match(input,45,FOLLOW_25); 

                    				newLeafNode(otherlv_6, grammarAccess.getGuardLocaleAccess().getDescriptionKeyword_6_0());
                    			
                    otherlv_7=(Token)match(input,31,FOLLOW_39); 

                    				newLeafNode(otherlv_7, grammarAccess.getGuardLocaleAccess().getEqualsSignKeyword_6_1());
                    			
                    // InternalABSReport.g:7084:4: ( (lv_descriptionFormat_8_0= ruleTextFormat ) )?
                    int alt138=2;
                    int LA138_0 = input.LA(1);

                    if ( ((LA138_0>=112 && LA138_0<=114)) ) {
                        alt138=1;
                    }
                    switch (alt138) {
                        case 1 :
                            // InternalABSReport.g:7085:5: (lv_descriptionFormat_8_0= ruleTextFormat )
                            {
                            // InternalABSReport.g:7085:5: (lv_descriptionFormat_8_0= ruleTextFormat )
                            // InternalABSReport.g:7086:6: lv_descriptionFormat_8_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getGuardLocaleAccess().getDescriptionFormatTextFormatEnumRuleCall_6_2_0());
                            					
                            pushFollow(FOLLOW_16);
                            lv_descriptionFormat_8_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getGuardLocaleRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_8_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:7103:4: ( (lv_description_9_0= RULE_STRING ) )
                    // InternalABSReport.g:7104:5: (lv_description_9_0= RULE_STRING )
                    {
                    // InternalABSReport.g:7104:5: (lv_description_9_0= RULE_STRING )
                    // InternalABSReport.g:7105:6: lv_description_9_0= RULE_STRING
                    {
                    lv_description_9_0=(Token)match(input,RULE_STRING,FOLLOW_95); 

                    						newLeafNode(lv_description_9_0, grammarAccess.getGuardLocaleAccess().getDescriptionSTRINGTerminalRuleCall_6_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getGuardLocaleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_9_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalABSReport.g:7122:3: (otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}' )?
            int alt141=2;
            int LA141_0 = input.LA(1);

            if ( (LA141_0==38) ) {
                alt141=1;
            }
            switch (alt141) {
                case 1 :
                    // InternalABSReport.g:7123:4: otherlv_10= 'annotations' otherlv_11= '{' ( (lv_annotations_12_0= ruleAnnotationEntry ) )* otherlv_13= '}'
                    {
                    otherlv_10=(Token)match(input,38,FOLLOW_6); 

                    				newLeafNode(otherlv_10, grammarAccess.getGuardLocaleAccess().getAnnotationsKeyword_7_0());
                    			
                    otherlv_11=(Token)match(input,12,FOLLOW_32); 

                    				newLeafNode(otherlv_11, grammarAccess.getGuardLocaleAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalABSReport.g:7131:4: ( (lv_annotations_12_0= ruleAnnotationEntry ) )*
                    loop140:
                    do {
                        int alt140=2;
                        int LA140_0 = input.LA(1);

                        if ( (LA140_0==RULE_STRING||LA140_0==RULE_ID) ) {
                            alt140=1;
                        }


                        switch (alt140) {
                    	case 1 :
                    	    // InternalABSReport.g:7132:5: (lv_annotations_12_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalABSReport.g:7132:5: (lv_annotations_12_0= ruleAnnotationEntry )
                    	    // InternalABSReport.g:7133:6: lv_annotations_12_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getGuardLocaleAccess().getAnnotationsAnnotationEntryParserRuleCall_7_2_0());
                    	    					
                    	    pushFollow(FOLLOW_32);
                    	    lv_annotations_12_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getGuardLocaleRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_12_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop140;
                        }
                    } while (true);

                    otherlv_13=(Token)match(input,13,FOLLOW_14); 

                    				newLeafNode(otherlv_13, grammarAccess.getGuardLocaleAccess().getRightCurlyBracketKeyword_7_3());
                    			

                    }
                    break;

            }

            otherlv_14=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_14, grammarAccess.getGuardLocaleAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardLocale"


    // $ANTLR start "entryRuleRequirement"
    // InternalABSReport.g:7163:1: entryRuleRequirement returns [EObject current=null] : iv_ruleRequirement= ruleRequirement EOF ;
    public final EObject entryRuleRequirement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequirement = null;


        try {
            // InternalABSReport.g:7163:52: (iv_ruleRequirement= ruleRequirement EOF )
            // InternalABSReport.g:7164:2: iv_ruleRequirement= ruleRequirement EOF
            {
             newCompositeNode(grammarAccess.getRequirementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRequirement=ruleRequirement();

            state._fsp--;

             current =iv_ruleRequirement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequirement"


    // $ANTLR start "ruleRequirement"
    // InternalABSReport.g:7170:1: ruleRequirement returns [EObject current=null] : (otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}' ) ;
    public final EObject ruleRequirement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_title_4_0=null;
        Token otherlv_5=null;
        Token lv_description_7_0=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Enumerator lv_descriptionFormat_6_0 = null;

        EObject lv_annotations_10_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:7176:2: ( (otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}' ) )
            // InternalABSReport.g:7177:2: (otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}' )
            {
            // InternalABSReport.g:7177:2: (otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}' )
            // InternalABSReport.g:7178:3: otherlv_0= 'Requirement' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )? (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )? (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )? otherlv_18= '}'
            {
            otherlv_0=(Token)match(input,96,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getRequirementAccess().getRequirementKeyword_0());
            		
            // InternalABSReport.g:7182:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalABSReport.g:7183:4: (lv_name_1_0= RULE_ID )
            {
            // InternalABSReport.g:7183:4: (lv_name_1_0= RULE_ID )
            // InternalABSReport.g:7184:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_1_0, grammarAccess.getRequirementAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRequirementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_96); 

            			newLeafNode(otherlv_2, grammarAccess.getRequirementAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalABSReport.g:7204:3: (otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) ) )?
            int alt142=2;
            int LA142_0 = input.LA(1);

            if ( (LA142_0==97) ) {
                alt142=1;
            }
            switch (alt142) {
                case 1 :
                    // InternalABSReport.g:7205:4: otherlv_3= 'title' ( (lv_title_4_0= RULE_STRING ) )
                    {
                    otherlv_3=(Token)match(input,97,FOLLOW_16); 

                    				newLeafNode(otherlv_3, grammarAccess.getRequirementAccess().getTitleKeyword_3_0());
                    			
                    // InternalABSReport.g:7209:4: ( (lv_title_4_0= RULE_STRING ) )
                    // InternalABSReport.g:7210:5: (lv_title_4_0= RULE_STRING )
                    {
                    // InternalABSReport.g:7210:5: (lv_title_4_0= RULE_STRING )
                    // InternalABSReport.g:7211:6: lv_title_4_0= RULE_STRING
                    {
                    lv_title_4_0=(Token)match(input,RULE_STRING,FOLLOW_97); 

                    						newLeafNode(lv_title_4_0, grammarAccess.getRequirementAccess().getTitleSTRINGTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"title",
                    							lv_title_4_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalABSReport.g:7228:3: (otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) ) )?
            int alt144=2;
            int LA144_0 = input.LA(1);

            if ( (LA144_0==45) ) {
                alt144=1;
            }
            switch (alt144) {
                case 1 :
                    // InternalABSReport.g:7229:4: otherlv_5= 'description' ( (lv_descriptionFormat_6_0= ruleTextFormat ) )? ( (lv_description_7_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,45,FOLLOW_39); 

                    				newLeafNode(otherlv_5, grammarAccess.getRequirementAccess().getDescriptionKeyword_4_0());
                    			
                    // InternalABSReport.g:7233:4: ( (lv_descriptionFormat_6_0= ruleTextFormat ) )?
                    int alt143=2;
                    int LA143_0 = input.LA(1);

                    if ( ((LA143_0>=112 && LA143_0<=114)) ) {
                        alt143=1;
                    }
                    switch (alt143) {
                        case 1 :
                            // InternalABSReport.g:7234:5: (lv_descriptionFormat_6_0= ruleTextFormat )
                            {
                            // InternalABSReport.g:7234:5: (lv_descriptionFormat_6_0= ruleTextFormat )
                            // InternalABSReport.g:7235:6: lv_descriptionFormat_6_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getRequirementAccess().getDescriptionFormatTextFormatEnumRuleCall_4_1_0());
                            					
                            pushFollow(FOLLOW_16);
                            lv_descriptionFormat_6_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getRequirementRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_6_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:7252:4: ( (lv_description_7_0= RULE_STRING ) )
                    // InternalABSReport.g:7253:5: (lv_description_7_0= RULE_STRING )
                    {
                    // InternalABSReport.g:7253:5: (lv_description_7_0= RULE_STRING )
                    // InternalABSReport.g:7254:6: lv_description_7_0= RULE_STRING
                    {
                    lv_description_7_0=(Token)match(input,RULE_STRING,FOLLOW_98); 

                    						newLeafNode(lv_description_7_0, grammarAccess.getRequirementAccess().getDescriptionSTRINGTerminalRuleCall_4_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_7_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalABSReport.g:7271:3: (otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}' )?
            int alt146=2;
            int LA146_0 = input.LA(1);

            if ( (LA146_0==38) ) {
                alt146=1;
            }
            switch (alt146) {
                case 1 :
                    // InternalABSReport.g:7272:4: otherlv_8= 'annotations' otherlv_9= '{' ( (lv_annotations_10_0= ruleAnnotationEntry ) )* otherlv_11= '}'
                    {
                    otherlv_8=(Token)match(input,38,FOLLOW_6); 

                    				newLeafNode(otherlv_8, grammarAccess.getRequirementAccess().getAnnotationsKeyword_5_0());
                    			
                    otherlv_9=(Token)match(input,12,FOLLOW_32); 

                    				newLeafNode(otherlv_9, grammarAccess.getRequirementAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalABSReport.g:7280:4: ( (lv_annotations_10_0= ruleAnnotationEntry ) )*
                    loop145:
                    do {
                        int alt145=2;
                        int LA145_0 = input.LA(1);

                        if ( (LA145_0==RULE_STRING||LA145_0==RULE_ID) ) {
                            alt145=1;
                        }


                        switch (alt145) {
                    	case 1 :
                    	    // InternalABSReport.g:7281:5: (lv_annotations_10_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalABSReport.g:7281:5: (lv_annotations_10_0= ruleAnnotationEntry )
                    	    // InternalABSReport.g:7282:6: lv_annotations_10_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getRequirementAccess().getAnnotationsAnnotationEntryParserRuleCall_5_2_0());
                    	    					
                    	    pushFollow(FOLLOW_32);
                    	    lv_annotations_10_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRequirementRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_10_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop145;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,13,FOLLOW_99); 

                    				newLeafNode(otherlv_11, grammarAccess.getRequirementAccess().getRightCurlyBracketKeyword_5_3());
                    			

                    }
                    break;

            }

            // InternalABSReport.g:7304:3: (otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')' )?
            int alt148=2;
            int LA148_0 = input.LA(1);

            if ( (LA148_0==98) ) {
                alt148=1;
            }
            switch (alt148) {
                case 1 :
                    // InternalABSReport.g:7305:4: otherlv_12= 'contracts' otherlv_13= '(' ( ( ruleQualifiedName ) ) (otherlv_15= ',' ( ( ruleQualifiedName ) ) )* otherlv_17= ')'
                    {
                    otherlv_12=(Token)match(input,98,FOLLOW_19); 

                    				newLeafNode(otherlv_12, grammarAccess.getRequirementAccess().getContractsKeyword_6_0());
                    			
                    otherlv_13=(Token)match(input,27,FOLLOW_5); 

                    				newLeafNode(otherlv_13, grammarAccess.getRequirementAccess().getLeftParenthesisKeyword_6_1());
                    			
                    // InternalABSReport.g:7313:4: ( ( ruleQualifiedName ) )
                    // InternalABSReport.g:7314:5: ( ruleQualifiedName )
                    {
                    // InternalABSReport.g:7314:5: ( ruleQualifiedName )
                    // InternalABSReport.g:7315:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getRequirementAccess().getContractsContractCrossReference_6_2_0());
                    					
                    pushFollow(FOLLOW_21);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalABSReport.g:7329:4: (otherlv_15= ',' ( ( ruleQualifiedName ) ) )*
                    loop147:
                    do {
                        int alt147=2;
                        int LA147_0 = input.LA(1);

                        if ( (LA147_0==26) ) {
                            alt147=1;
                        }


                        switch (alt147) {
                    	case 1 :
                    	    // InternalABSReport.g:7330:5: otherlv_15= ',' ( ( ruleQualifiedName ) )
                    	    {
                    	    otherlv_15=(Token)match(input,26,FOLLOW_5); 

                    	    					newLeafNode(otherlv_15, grammarAccess.getRequirementAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalABSReport.g:7334:5: ( ( ruleQualifiedName ) )
                    	    // InternalABSReport.g:7335:6: ( ruleQualifiedName )
                    	    {
                    	    // InternalABSReport.g:7335:6: ( ruleQualifiedName )
                    	    // InternalABSReport.g:7336:7: ruleQualifiedName
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getRequirementRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getRequirementAccess().getContractsContractCrossReference_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_21);
                    	    ruleQualifiedName();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop147;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,28,FOLLOW_14); 

                    				newLeafNode(otherlv_17, grammarAccess.getRequirementAccess().getRightParenthesisKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_18=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_18, grammarAccess.getRequirementAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequirement"


    // $ANTLR start "entryRuleRequirementLocale"
    // InternalABSReport.g:7364:1: entryRuleRequirementLocale returns [EObject current=null] : iv_ruleRequirementLocale= ruleRequirementLocale EOF ;
    public final EObject entryRuleRequirementLocale() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequirementLocale = null;


        try {
            // InternalABSReport.g:7364:58: (iv_ruleRequirementLocale= ruleRequirementLocale EOF )
            // InternalABSReport.g:7365:2: iv_ruleRequirementLocale= ruleRequirementLocale EOF
            {
             newCompositeNode(grammarAccess.getRequirementLocaleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRequirementLocale=ruleRequirementLocale();

            state._fsp--;

             current =iv_ruleRequirementLocale; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequirementLocale"


    // $ANTLR start "ruleRequirementLocale"
    // InternalABSReport.g:7371:1: ruleRequirementLocale returns [EObject current=null] : ( () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}' ) ;
    public final EObject ruleRequirementLocale() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_title_6_0=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token lv_description_10_0=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Enumerator lv_descriptionFormat_9_0 = null;

        EObject lv_annotations_13_0 = null;



        	enterRule();

        try {
            // InternalABSReport.g:7377:2: ( ( () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}' ) )
            // InternalABSReport.g:7378:2: ( () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}' )
            {
            // InternalABSReport.g:7378:2: ( () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}' )
            // InternalABSReport.g:7379:3: () otherlv_1= 'Requirement' ( ( ruleQualifiedName ) ) otherlv_3= '{' (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )? (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )? (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )? otherlv_15= '}'
            {
            // InternalABSReport.g:7379:3: ()
            // InternalABSReport.g:7380:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRequirementLocaleAccess().getRequirementLocaleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,96,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getRequirementLocaleAccess().getRequirementKeyword_1());
            		
            // InternalABSReport.g:7390:3: ( ( ruleQualifiedName ) )
            // InternalABSReport.g:7391:4: ( ruleQualifiedName )
            {
            // InternalABSReport.g:7391:4: ( ruleQualifiedName )
            // InternalABSReport.g:7392:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRequirementLocaleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getRequirementLocaleAccess().getRefRequirementCrossReference_2_0());
            				
            pushFollow(FOLLOW_6);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_100); 

            			newLeafNode(otherlv_3, grammarAccess.getRequirementLocaleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalABSReport.g:7410:3: (otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) ) )?
            int alt149=2;
            int LA149_0 = input.LA(1);

            if ( (LA149_0==97) ) {
                alt149=1;
            }
            switch (alt149) {
                case 1 :
                    // InternalABSReport.g:7411:4: otherlv_4= 'title' otherlv_5= '=' ( (lv_title_6_0= RULE_STRING ) )
                    {
                    otherlv_4=(Token)match(input,97,FOLLOW_25); 

                    				newLeafNode(otherlv_4, grammarAccess.getRequirementLocaleAccess().getTitleKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,31,FOLLOW_16); 

                    				newLeafNode(otherlv_5, grammarAccess.getRequirementLocaleAccess().getEqualsSignKeyword_4_1());
                    			
                    // InternalABSReport.g:7419:4: ( (lv_title_6_0= RULE_STRING ) )
                    // InternalABSReport.g:7420:5: (lv_title_6_0= RULE_STRING )
                    {
                    // InternalABSReport.g:7420:5: (lv_title_6_0= RULE_STRING )
                    // InternalABSReport.g:7421:6: lv_title_6_0= RULE_STRING
                    {
                    lv_title_6_0=(Token)match(input,RULE_STRING,FOLLOW_94); 

                    						newLeafNode(lv_title_6_0, grammarAccess.getRequirementLocaleAccess().getTitleSTRINGTerminalRuleCall_4_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementLocaleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"title",
                    							lv_title_6_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalABSReport.g:7438:3: (otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) ) )?
            int alt151=2;
            int LA151_0 = input.LA(1);

            if ( (LA151_0==45) ) {
                alt151=1;
            }
            switch (alt151) {
                case 1 :
                    // InternalABSReport.g:7439:4: otherlv_7= 'description' otherlv_8= '=' ( (lv_descriptionFormat_9_0= ruleTextFormat ) )? ( (lv_description_10_0= RULE_STRING ) )
                    {
                    otherlv_7=(Token)match(input,45,FOLLOW_25); 

                    				newLeafNode(otherlv_7, grammarAccess.getRequirementLocaleAccess().getDescriptionKeyword_5_0());
                    			
                    otherlv_8=(Token)match(input,31,FOLLOW_39); 

                    				newLeafNode(otherlv_8, grammarAccess.getRequirementLocaleAccess().getEqualsSignKeyword_5_1());
                    			
                    // InternalABSReport.g:7447:4: ( (lv_descriptionFormat_9_0= ruleTextFormat ) )?
                    int alt150=2;
                    int LA150_0 = input.LA(1);

                    if ( ((LA150_0>=112 && LA150_0<=114)) ) {
                        alt150=1;
                    }
                    switch (alt150) {
                        case 1 :
                            // InternalABSReport.g:7448:5: (lv_descriptionFormat_9_0= ruleTextFormat )
                            {
                            // InternalABSReport.g:7448:5: (lv_descriptionFormat_9_0= ruleTextFormat )
                            // InternalABSReport.g:7449:6: lv_descriptionFormat_9_0= ruleTextFormat
                            {

                            						newCompositeNode(grammarAccess.getRequirementLocaleAccess().getDescriptionFormatTextFormatEnumRuleCall_5_2_0());
                            					
                            pushFollow(FOLLOW_16);
                            lv_descriptionFormat_9_0=ruleTextFormat();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getRequirementLocaleRule());
                            						}
                            						set(
                            							current,
                            							"descriptionFormat",
                            							lv_descriptionFormat_9_0,
                            							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.TextFormat");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalABSReport.g:7466:4: ( (lv_description_10_0= RULE_STRING ) )
                    // InternalABSReport.g:7467:5: (lv_description_10_0= RULE_STRING )
                    {
                    // InternalABSReport.g:7467:5: (lv_description_10_0= RULE_STRING )
                    // InternalABSReport.g:7468:6: lv_description_10_0= RULE_STRING
                    {
                    lv_description_10_0=(Token)match(input,RULE_STRING,FOLLOW_95); 

                    						newLeafNode(lv_description_10_0, grammarAccess.getRequirementLocaleAccess().getDescriptionSTRINGTerminalRuleCall_5_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRequirementLocaleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"description",
                    							lv_description_10_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalABSReport.g:7485:3: (otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}' )?
            int alt153=2;
            int LA153_0 = input.LA(1);

            if ( (LA153_0==38) ) {
                alt153=1;
            }
            switch (alt153) {
                case 1 :
                    // InternalABSReport.g:7486:4: otherlv_11= 'annotations' otherlv_12= '{' ( (lv_annotations_13_0= ruleAnnotationEntry ) )* otherlv_14= '}'
                    {
                    otherlv_11=(Token)match(input,38,FOLLOW_6); 

                    				newLeafNode(otherlv_11, grammarAccess.getRequirementLocaleAccess().getAnnotationsKeyword_6_0());
                    			
                    otherlv_12=(Token)match(input,12,FOLLOW_32); 

                    				newLeafNode(otherlv_12, grammarAccess.getRequirementLocaleAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalABSReport.g:7494:4: ( (lv_annotations_13_0= ruleAnnotationEntry ) )*
                    loop152:
                    do {
                        int alt152=2;
                        int LA152_0 = input.LA(1);

                        if ( (LA152_0==RULE_STRING||LA152_0==RULE_ID) ) {
                            alt152=1;
                        }


                        switch (alt152) {
                    	case 1 :
                    	    // InternalABSReport.g:7495:5: (lv_annotations_13_0= ruleAnnotationEntry )
                    	    {
                    	    // InternalABSReport.g:7495:5: (lv_annotations_13_0= ruleAnnotationEntry )
                    	    // InternalABSReport.g:7496:6: lv_annotations_13_0= ruleAnnotationEntry
                    	    {

                    	    						newCompositeNode(grammarAccess.getRequirementLocaleAccess().getAnnotationsAnnotationEntryParserRuleCall_6_2_0());
                    	    					
                    	    pushFollow(FOLLOW_32);
                    	    lv_annotations_13_0=ruleAnnotationEntry();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRequirementLocaleRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"annotations",
                    	    							lv_annotations_13_0,
                    	    							"fr.irisa.atsyra.absystem.xtext.AssetBasedSystemDsl.AnnotationEntry");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop152;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,13,FOLLOW_14); 

                    				newLeafNode(otherlv_14, grammarAccess.getRequirementLocaleAccess().getRightCurlyBracketKeyword_6_3());
                    			

                    }
                    break;

            }

            otherlv_15=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getRequirementLocaleAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequirementLocale"


    // $ANTLR start "ruleMultiplicitySingle"
    // InternalABSReport.g:7526:1: ruleMultiplicitySingle returns [Enumerator current=null] : ( (enumLiteral_0= '[1]' ) | (enumLiteral_1= '[0..1]' ) ) ;
    public final Enumerator ruleMultiplicitySingle() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalABSReport.g:7532:2: ( ( (enumLiteral_0= '[1]' ) | (enumLiteral_1= '[0..1]' ) ) )
            // InternalABSReport.g:7533:2: ( (enumLiteral_0= '[1]' ) | (enumLiteral_1= '[0..1]' ) )
            {
            // InternalABSReport.g:7533:2: ( (enumLiteral_0= '[1]' ) | (enumLiteral_1= '[0..1]' ) )
            int alt154=2;
            int LA154_0 = input.LA(1);

            if ( (LA154_0==99) ) {
                alt154=1;
            }
            else if ( (LA154_0==100) ) {
                alt154=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 154, 0, input);

                throw nvae;
            }
            switch (alt154) {
                case 1 :
                    // InternalABSReport.g:7534:3: (enumLiteral_0= '[1]' )
                    {
                    // InternalABSReport.g:7534:3: (enumLiteral_0= '[1]' )
                    // InternalABSReport.g:7535:4: enumLiteral_0= '[1]'
                    {
                    enumLiteral_0=(Token)match(input,99,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicitySingleAccess().getOneEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getMultiplicitySingleAccess().getOneEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:7542:3: (enumLiteral_1= '[0..1]' )
                    {
                    // InternalABSReport.g:7542:3: (enumLiteral_1= '[0..1]' )
                    // InternalABSReport.g:7543:4: enumLiteral_1= '[0..1]'
                    {
                    enumLiteral_1=(Token)match(input,100,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicitySingleAccess().getZeroOrOneEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getMultiplicitySingleAccess().getZeroOrOneEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicitySingle"


    // $ANTLR start "ruleMultiplicityMany"
    // InternalABSReport.g:7553:1: ruleMultiplicityMany returns [Enumerator current=null] : ( (enumLiteral_0= '[*]' ) | (enumLiteral_1= '[1..*]' ) ) ;
    public final Enumerator ruleMultiplicityMany() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalABSReport.g:7559:2: ( ( (enumLiteral_0= '[*]' ) | (enumLiteral_1= '[1..*]' ) ) )
            // InternalABSReport.g:7560:2: ( (enumLiteral_0= '[*]' ) | (enumLiteral_1= '[1..*]' ) )
            {
            // InternalABSReport.g:7560:2: ( (enumLiteral_0= '[*]' ) | (enumLiteral_1= '[1..*]' ) )
            int alt155=2;
            int LA155_0 = input.LA(1);

            if ( (LA155_0==101) ) {
                alt155=1;
            }
            else if ( (LA155_0==102) ) {
                alt155=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 155, 0, input);

                throw nvae;
            }
            switch (alt155) {
                case 1 :
                    // InternalABSReport.g:7561:3: (enumLiteral_0= '[*]' )
                    {
                    // InternalABSReport.g:7561:3: (enumLiteral_0= '[*]' )
                    // InternalABSReport.g:7562:4: enumLiteral_0= '[*]'
                    {
                    enumLiteral_0=(Token)match(input,101,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicityManyAccess().getZeroOrManyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getMultiplicityManyAccess().getZeroOrManyEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:7569:3: (enumLiteral_1= '[1..*]' )
                    {
                    // InternalABSReport.g:7569:3: (enumLiteral_1= '[1..*]' )
                    // InternalABSReport.g:7570:4: enumLiteral_1= '[1..*]'
                    {
                    enumLiteral_1=(Token)match(input,102,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicityManyAccess().getOneOrManyEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getMultiplicityManyAccess().getOneOrManyEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicityMany"


    // $ANTLR start "ruleSeverity"
    // InternalABSReport.g:7580:1: ruleSeverity returns [Enumerator current=null] : ( (enumLiteral_0= 'ERROR' ) | (enumLiteral_1= 'WARNING' ) ) ;
    public final Enumerator ruleSeverity() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalABSReport.g:7586:2: ( ( (enumLiteral_0= 'ERROR' ) | (enumLiteral_1= 'WARNING' ) ) )
            // InternalABSReport.g:7587:2: ( (enumLiteral_0= 'ERROR' ) | (enumLiteral_1= 'WARNING' ) )
            {
            // InternalABSReport.g:7587:2: ( (enumLiteral_0= 'ERROR' ) | (enumLiteral_1= 'WARNING' ) )
            int alt156=2;
            int LA156_0 = input.LA(1);

            if ( (LA156_0==103) ) {
                alt156=1;
            }
            else if ( (LA156_0==104) ) {
                alt156=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 156, 0, input);

                throw nvae;
            }
            switch (alt156) {
                case 1 :
                    // InternalABSReport.g:7588:3: (enumLiteral_0= 'ERROR' )
                    {
                    // InternalABSReport.g:7588:3: (enumLiteral_0= 'ERROR' )
                    // InternalABSReport.g:7589:4: enumLiteral_0= 'ERROR'
                    {
                    enumLiteral_0=(Token)match(input,103,FOLLOW_2); 

                    				current = grammarAccess.getSeverityAccess().getERROREnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getSeverityAccess().getERROREnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:7596:3: (enumLiteral_1= 'WARNING' )
                    {
                    // InternalABSReport.g:7596:3: (enumLiteral_1= 'WARNING' )
                    // InternalABSReport.g:7597:4: enumLiteral_1= 'WARNING'
                    {
                    enumLiteral_1=(Token)match(input,104,FOLLOW_2); 

                    				current = grammarAccess.getSeverityAccess().getWARNINGEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getSeverityAccess().getWARNINGEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeverity"


    // $ANTLR start "ruleActionEnum"
    // InternalABSReport.g:7607:1: ruleActionEnum returns [Enumerator current=null] : ( (enumLiteral_0= 'assign' ) | (enumLiteral_1= 'add' ) | (enumLiteral_2= 'addAll' ) | (enumLiteral_3= 'clear' ) | (enumLiteral_4= 'remove' ) | (enumLiteral_5= 'removeAll' ) | (enumLiteral_6= 'forAll' ) ) ;
    public final Enumerator ruleActionEnum() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;


        	enterRule();

        try {
            // InternalABSReport.g:7613:2: ( ( (enumLiteral_0= 'assign' ) | (enumLiteral_1= 'add' ) | (enumLiteral_2= 'addAll' ) | (enumLiteral_3= 'clear' ) | (enumLiteral_4= 'remove' ) | (enumLiteral_5= 'removeAll' ) | (enumLiteral_6= 'forAll' ) ) )
            // InternalABSReport.g:7614:2: ( (enumLiteral_0= 'assign' ) | (enumLiteral_1= 'add' ) | (enumLiteral_2= 'addAll' ) | (enumLiteral_3= 'clear' ) | (enumLiteral_4= 'remove' ) | (enumLiteral_5= 'removeAll' ) | (enumLiteral_6= 'forAll' ) )
            {
            // InternalABSReport.g:7614:2: ( (enumLiteral_0= 'assign' ) | (enumLiteral_1= 'add' ) | (enumLiteral_2= 'addAll' ) | (enumLiteral_3= 'clear' ) | (enumLiteral_4= 'remove' ) | (enumLiteral_5= 'removeAll' ) | (enumLiteral_6= 'forAll' ) )
            int alt157=7;
            switch ( input.LA(1) ) {
            case 105:
                {
                alt157=1;
                }
                break;
            case 106:
                {
                alt157=2;
                }
                break;
            case 107:
                {
                alt157=3;
                }
                break;
            case 108:
                {
                alt157=4;
                }
                break;
            case 109:
                {
                alt157=5;
                }
                break;
            case 110:
                {
                alt157=6;
                }
                break;
            case 111:
                {
                alt157=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 157, 0, input);

                throw nvae;
            }

            switch (alt157) {
                case 1 :
                    // InternalABSReport.g:7615:3: (enumLiteral_0= 'assign' )
                    {
                    // InternalABSReport.g:7615:3: (enumLiteral_0= 'assign' )
                    // InternalABSReport.g:7616:4: enumLiteral_0= 'assign'
                    {
                    enumLiteral_0=(Token)match(input,105,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getAssignEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getActionEnumAccess().getAssignEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:7623:3: (enumLiteral_1= 'add' )
                    {
                    // InternalABSReport.g:7623:3: (enumLiteral_1= 'add' )
                    // InternalABSReport.g:7624:4: enumLiteral_1= 'add'
                    {
                    enumLiteral_1=(Token)match(input,106,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getAddEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getActionEnumAccess().getAddEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalABSReport.g:7631:3: (enumLiteral_2= 'addAll' )
                    {
                    // InternalABSReport.g:7631:3: (enumLiteral_2= 'addAll' )
                    // InternalABSReport.g:7632:4: enumLiteral_2= 'addAll'
                    {
                    enumLiteral_2=(Token)match(input,107,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getAddAllEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getActionEnumAccess().getAddAllEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalABSReport.g:7639:3: (enumLiteral_3= 'clear' )
                    {
                    // InternalABSReport.g:7639:3: (enumLiteral_3= 'clear' )
                    // InternalABSReport.g:7640:4: enumLiteral_3= 'clear'
                    {
                    enumLiteral_3=(Token)match(input,108,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getClearEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getActionEnumAccess().getClearEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalABSReport.g:7647:3: (enumLiteral_4= 'remove' )
                    {
                    // InternalABSReport.g:7647:3: (enumLiteral_4= 'remove' )
                    // InternalABSReport.g:7648:4: enumLiteral_4= 'remove'
                    {
                    enumLiteral_4=(Token)match(input,109,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getRemoveEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getActionEnumAccess().getRemoveEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalABSReport.g:7655:3: (enumLiteral_5= 'removeAll' )
                    {
                    // InternalABSReport.g:7655:3: (enumLiteral_5= 'removeAll' )
                    // InternalABSReport.g:7656:4: enumLiteral_5= 'removeAll'
                    {
                    enumLiteral_5=(Token)match(input,110,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getRemoveAllEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getActionEnumAccess().getRemoveAllEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalABSReport.g:7663:3: (enumLiteral_6= 'forAll' )
                    {
                    // InternalABSReport.g:7663:3: (enumLiteral_6= 'forAll' )
                    // InternalABSReport.g:7664:4: enumLiteral_6= 'forAll'
                    {
                    enumLiteral_6=(Token)match(input,111,FOLLOW_2); 

                    				current = grammarAccess.getActionEnumAccess().getForAllEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_6, grammarAccess.getActionEnumAccess().getForAllEnumLiteralDeclaration_6());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionEnum"


    // $ANTLR start "ruleTextFormat"
    // InternalABSReport.g:7674:1: ruleTextFormat returns [Enumerator current=null] : ( (enumLiteral_0= 'plaintext' ) | (enumLiteral_1= 'html' ) | (enumLiteral_2= 'markdown' ) ) ;
    public final Enumerator ruleTextFormat() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalABSReport.g:7680:2: ( ( (enumLiteral_0= 'plaintext' ) | (enumLiteral_1= 'html' ) | (enumLiteral_2= 'markdown' ) ) )
            // InternalABSReport.g:7681:2: ( (enumLiteral_0= 'plaintext' ) | (enumLiteral_1= 'html' ) | (enumLiteral_2= 'markdown' ) )
            {
            // InternalABSReport.g:7681:2: ( (enumLiteral_0= 'plaintext' ) | (enumLiteral_1= 'html' ) | (enumLiteral_2= 'markdown' ) )
            int alt158=3;
            switch ( input.LA(1) ) {
            case 112:
                {
                alt158=1;
                }
                break;
            case 113:
                {
                alt158=2;
                }
                break;
            case 114:
                {
                alt158=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 158, 0, input);

                throw nvae;
            }

            switch (alt158) {
                case 1 :
                    // InternalABSReport.g:7682:3: (enumLiteral_0= 'plaintext' )
                    {
                    // InternalABSReport.g:7682:3: (enumLiteral_0= 'plaintext' )
                    // InternalABSReport.g:7683:4: enumLiteral_0= 'plaintext'
                    {
                    enumLiteral_0=(Token)match(input,112,FOLLOW_2); 

                    				current = grammarAccess.getTextFormatAccess().getPlaintextEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTextFormatAccess().getPlaintextEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalABSReport.g:7690:3: (enumLiteral_1= 'html' )
                    {
                    // InternalABSReport.g:7690:3: (enumLiteral_1= 'html' )
                    // InternalABSReport.g:7691:4: enumLiteral_1= 'html'
                    {
                    enumLiteral_1=(Token)match(input,113,FOLLOW_2); 

                    				current = grammarAccess.getTextFormatAccess().getHTMLEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTextFormatAccess().getHTMLEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalABSReport.g:7698:3: (enumLiteral_2= 'markdown' )
                    {
                    // InternalABSReport.g:7698:3: (enumLiteral_2= 'markdown' )
                    // InternalABSReport.g:7699:4: enumLiteral_2= 'markdown'
                    {
                    enumLiteral_2=(Token)match(input,114,FOLLOW_2); 

                    				current = grammarAccess.getTextFormatAccess().getMarkdownEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getTextFormatAccess().getMarkdownEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTextFormat"

    // Delegated rules


    protected DFA105 dfa105 = new DFA105(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\1\uffff\1\11\11\uffff";
    static final String dfa_3s = "\1\4\1\15\6\uffff\1\5\2\uffff";
    static final String dfa_4s = "\1\76\1\123\6\uffff\1\6\2\uffff";
    static final String dfa_5s = "\2\uffff\1\2\1\3\1\5\1\6\1\7\1\10\1\uffff\1\1\1\4";
    static final String dfa_6s = "\13\uffff}>";
    static final String[] dfa_7s = {
            "\1\2\1\1\1\4\24\uffff\1\7\4\uffff\1\5\2\uffff\1\6\31\uffff\2\3",
            "\1\11\14\uffff\1\11\1\uffff\1\11\4\uffff\1\11\43\uffff\1\11\2\uffff\4\11\1\uffff\6\11\1\10",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\12\1\11",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA105 extends DFA {

        public DFA105(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 105;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "5166:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_5_1= 'true' | lv_value_5_2= 'false' ) ) ) ) | ( () ( (lv_value_7_0= ruleVersion ) ) ) | ( () ( ( ruleQualifiedName ) ) ) | this_Collection_10= ruleCollection | this_Undefined_11= ruleUndefined | (otherlv_12= '(' this_Expression_13= ruleExpression otherlv_14= ')' ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L,0x0000000002800000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000007A000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000000003A000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000002040L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000032000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000020002000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000022000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000003F82000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000007F82000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000010000040L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000014000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000004002000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000440002000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000440000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x6000000900000070L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x6000000800000070L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000204000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000900000040L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x8043C77000002000L,0x0000000100000013L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000002050L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x8043C71000002000L,0x0000000100000013L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x018000C000002800L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0180008000002800L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000080000001000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0004306440002000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000000000000010L,0x0007000000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0004000440002000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0030000000000002L,0x0000007800000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0030000000000002L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0020000000000002L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x6000000A04000070L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000004000000050L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000200040002000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000040002000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0400000000000002L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0800000000000002L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000002L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000010000050L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000204000000000L,0x000000000000000CL});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000000000000000L,0x0000018000000000L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x6000000908000070L,0x0000000000001000L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0000204000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
    public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x6000000908002070L,0x0000000000001000L});
    public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0000204000002000L,0x0000000000000180L});
    public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x0000000000002000L,0x0000000000000100L});
    public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000200L});
    public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000400L});
    public static final BitSet FOLLOW_70 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});
    public static final BitSet FOLLOW_71 = new BitSet(new long[]{0x0000000000000002L,0x000000000007E000L});
    public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x0000000000000002L,0x0000000000006000L});
    public static final BitSet FOLLOW_73 = new BitSet(new long[]{0x0000000000000002L,0x0000000000080000L});
    public static final BitSet FOLLOW_74 = new BitSet(new long[]{0x0000000008000002L,0x0000000000080000L});
    public static final BitSet FOLLOW_75 = new BitSet(new long[]{0x6000000918001070L,0x0000000000001000L});
    public static final BitSet FOLLOW_76 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_77 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_78 = new BitSet(new long[]{0x6000000B0C000070L,0x0000000000001000L});
    public static final BitSet FOLLOW_79 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
    public static final BitSet FOLLOW_80 = new BitSet(new long[]{0x0000000000000000L,0x0000FE0000000000L});
    public static final BitSet FOLLOW_81 = new BitSet(new long[]{0x0000000000002000L,0x0000000000000040L});
    public static final BitSet FOLLOW_82 = new BitSet(new long[]{0x0000000000000002L,0x0000000000600000L});
    public static final BitSet FOLLOW_83 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_84 = new BitSet(new long[]{0x0000000000000002L,0x0000000000400000L});
    public static final BitSet FOLLOW_85 = new BitSet(new long[]{0x0000000000000000L,0x0000000001000040L});
    public static final BitSet FOLLOW_86 = new BitSet(new long[]{0x0000000000000002L,0x0000000004000000L});
    public static final BitSet FOLLOW_87 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
    public static final BitSet FOLLOW_88 = new BitSet(new long[]{0x0000000000000000L,0x0000000020000000L});
    public static final BitSet FOLLOW_89 = new BitSet(new long[]{0x0000001000002000L});
    public static final BitSet FOLLOW_90 = new BitSet(new long[]{0x0000C45000002000L,0x0000000180000000L});
    public static final BitSet FOLLOW_91 = new BitSet(new long[]{0x0000C41000002000L,0x0000000180000000L});
    public static final BitSet FOLLOW_92 = new BitSet(new long[]{0x0000200440002000L});
    public static final BitSet FOLLOW_93 = new BitSet(new long[]{0x0000000000002000L,0x0000000040000000L});
    public static final BitSet FOLLOW_94 = new BitSet(new long[]{0x0000204000002000L});
    public static final BitSet FOLLOW_95 = new BitSet(new long[]{0x0000004000002000L});
    public static final BitSet FOLLOW_96 = new BitSet(new long[]{0x0000204000002000L,0x0000000600000000L});
    public static final BitSet FOLLOW_97 = new BitSet(new long[]{0x0000204000002000L,0x0000000400000000L});
    public static final BitSet FOLLOW_98 = new BitSet(new long[]{0x0000004000002000L,0x0000000400000000L});
    public static final BitSet FOLLOW_99 = new BitSet(new long[]{0x0000000000002000L,0x0000000400000000L});
    public static final BitSet FOLLOW_100 = new BitSet(new long[]{0x0000204000002000L,0x0000000200000000L});

}
