/**
 * generated by Xtext 2.27.0
 */
package fr.irisa.atsyra.absreport.aBSReport.impl;

import fr.irisa.atsyra.absreport.aBSReport.ABSReportPackage;
import fr.irisa.atsyra.absreport.aBSReport.AssetState;
import fr.irisa.atsyra.absreport.aBSReport.StepState;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Step State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absreport.aBSReport.impl.StepStateImpl#getAssetState <em>Asset State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StepStateImpl extends MinimalEObjectImpl.Container implements StepState
{
  /**
   * The cached value of the '{@link #getAssetState() <em>Asset State</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssetState()
   * @generated
   * @ordered
   */
  protected EList<AssetState> assetState;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StepStateImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ABSReportPackage.Literals.STEP_STATE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<AssetState> getAssetState()
  {
    if (assetState == null)
    {
      assetState = new EObjectContainmentEList<AssetState>(AssetState.class, this, ABSReportPackage.STEP_STATE__ASSET_STATE);
    }
    return assetState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ABSReportPackage.STEP_STATE__ASSET_STATE:
        return ((InternalEList<?>)getAssetState()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ABSReportPackage.STEP_STATE__ASSET_STATE:
        return getAssetState();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ABSReportPackage.STEP_STATE__ASSET_STATE:
        getAssetState().clear();
        getAssetState().addAll((Collection<? extends AssetState>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ABSReportPackage.STEP_STATE__ASSET_STATE:
        getAssetState().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ABSReportPackage.STEP_STATE__ASSET_STATE:
        return assetState != null && !assetState.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //StepStateImpl
