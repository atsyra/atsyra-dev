/**
 * generated by Xtext 2.27.0
 */
package fr.irisa.atsyra.absreport.aBSReport.util;

import fr.irisa.atsyra.absreport.aBSReport.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.atsyra.absreport.aBSReport.ABSReportPackage
 * @generated
 */
public class ABSReportSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static ABSReportPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ABSReportSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = ABSReportPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case ABSReportPackage.ABS_REPORT_MODEL:
      {
        ABSReportModel absReportModel = (ABSReportModel)theEObject;
        T result = caseABSReportModel(absReportModel);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ABSReportPackage.GOAL_REPORT:
      {
        GoalReport goalReport = (GoalReport)theEObject;
        T result = caseGoalReport(goalReport);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ABSReportPackage.ABS_GOAL_WITNESS:
      {
        ABSGoalWitness absGoalWitness = (ABSGoalWitness)theEObject;
        T result = caseABSGoalWitness(absGoalWitness);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ABSReportPackage.REPORT_METADATA:
      {
        ReportMetadata reportMetadata = (ReportMetadata)theEObject;
        T result = caseReportMetadata(reportMetadata);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ABSReportPackage.STEP:
      {
        Step step = (Step)theEObject;
        T result = caseStep(step);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ABSReportPackage.STEP_STATE:
      {
        StepState stepState = (StepState)theEObject;
        T result = caseStepState(stepState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ABSReportPackage.ASSET_STATE:
      {
        AssetState assetState = (AssetState)theEObject;
        T result = caseAssetState(assetState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ABSReportPackage.FEATURE_STATE:
      {
        FeatureState featureState = (FeatureState)theEObject;
        T result = caseFeatureState(featureState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ABSReportPackage.ATTRIBUTE_STATE:
      {
        AttributeState attributeState = (AttributeState)theEObject;
        T result = caseAttributeState(attributeState);
        if (result == null) result = caseFeatureState(attributeState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ABSReportPackage.REFERENCE_STATE:
      {
        ReferenceState referenceState = (ReferenceState)theEObject;
        T result = caseReferenceState(referenceState);
        if (result == null) result = caseFeatureState(referenceState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Model</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseABSReportModel(ABSReportModel object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Goal Report</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Goal Report</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGoalReport(GoalReport object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>ABS Goal Witness</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>ABS Goal Witness</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseABSGoalWitness(ABSGoalWitness object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Report Metadata</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Report Metadata</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReportMetadata(ReportMetadata object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Step</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Step</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStep(Step object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Step State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Step State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStepState(StepState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Asset State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Asset State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAssetState(AssetState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Feature State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Feature State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFeatureState(FeatureState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Attribute State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Attribute State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAttributeState(AttributeState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Reference State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Reference State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReferenceState(ReferenceState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //ABSReportSwitch
