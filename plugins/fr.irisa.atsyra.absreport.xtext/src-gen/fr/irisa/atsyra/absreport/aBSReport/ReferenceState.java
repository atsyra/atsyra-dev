/**
 * generated by Xtext 2.27.0
 */
package fr.irisa.atsyra.absreport.aBSReport;

import fr.irisa.atsyra.absystem.model.absystem.Asset;
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.atsyra.absreport.aBSReport.ReferenceState#getReference <em>Reference</em>}</li>
 *   <li>{@link fr.irisa.atsyra.absreport.aBSReport.ReferenceState#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @see fr.irisa.atsyra.absreport.aBSReport.ABSReportPackage#getReferenceState()
 * @model
 * @generated
 */
public interface ReferenceState extends FeatureState
{
  /**
   * Returns the value of the '<em><b>Reference</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reference</em>' reference.
   * @see #setReference(AssetTypeReference)
   * @see fr.irisa.atsyra.absreport.aBSReport.ABSReportPackage#getReferenceState_Reference()
   * @model
   * @generated
   */
  AssetTypeReference getReference();

  /**
   * Sets the value of the '{@link fr.irisa.atsyra.absreport.aBSReport.ReferenceState#getReference <em>Reference</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reference</em>' reference.
   * @see #getReference()
   * @generated
   */
  void setReference(AssetTypeReference value);

  /**
   * Returns the value of the '<em><b>Values</b></em>' reference list.
   * The list contents are of type {@link fr.irisa.atsyra.absystem.model.absystem.Asset}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Values</em>' reference list.
   * @see fr.irisa.atsyra.absreport.aBSReport.ABSReportPackage#getReferenceState_Values()
   * @model
   * @generated
   */
  EList<Asset> getValues();

} // ReferenceState
