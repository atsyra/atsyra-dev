package fr.irisa.atsyra.absystem.ui.tests

import fr.irisa.atsyra.absystem.gal.ui.helpers.ABS2GALWorker
import fr.irisa.atsyra.absystem.gal.ui.helpers.ABS2ScenariosWorker
import fr.irisa.atsyra.absystem.model.absystem.Goal
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils
import fr.irisa.atsyra.absystem.transfo.ui.helpers.ABS2ABSWorker
import fr.irisa.atsyra.absystem.ui.tests.helpers.WorkspaceTestHelper
import fr.irisa.atsyra.ide.ui.preferences.PreferenceConstants
import java.time.Clock
import java.time.Duration
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.PerformanceStats
import org.eclipse.emf.common.util.BasicDiagnostic
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource
import org.eclipse.ui.PlatformUI
import org.eclipse.xtext.ui.testing.util.IResourcesSetupUtil
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle
import org.junit.jupiter.api.TestMethodOrder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.hamcrest.MatcherAssert.assertThat
import static org.hamcrest.Matchers.*
import fr.irisa.atsyra.absystem.validation.EMFStructureValidator
import fr.irisa.atsyra.absystem.validation.DomainContractValidator
import fr.irisa.atsyra.absystem.validation.RequirementContractValidator

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation)
class SmallAgenceStandaloneBenchmarkTests {
	static extension Logger = LoggerFactory.getLogger(SmallAgenceStandaloneBenchmarkTests);
	static extension WorkspaceTestHelper = new WorkspaceTestHelper;
	
	static final String BASE_FOLDER_NAME = "tests-inputs-gen"
	static final String MODEL_PROJECT_NAME = "small.agence.standalone.example"
	static final String GOAL_FILE_PATH = "goals.abs"
	
	static IProject testProject
	
	Goal goal
	
	@BeforeAll
	def static void beforeClass() throws Exception {
		info("beforeClass setup")
		waitWorkbench // useful especially for the first test suite that starts the workbench
		init
		IResourcesSetupUtil::cleanWorkspace
		IResourcesSetupUtil::reallyWaitForAutoBuild
		WorkspaceTestHelper::reallyWaitForJobs(2)
		testProject = deployProject(MODEL_PROJECT_NAME,BASE_FOLDER_NAME+"/"+MODEL_PROJECT_NAME+".zip")
		IResourcesSetupUtil::reallyWaitForAutoBuild
		WorkspaceTestHelper::reallyWaitForJobs(2)
		info("beforeClass done")
	}
	
	
	@AfterAll
	def static void afterClass() {
		info("afterClass clearing")
		val IProject[] visibleProjects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
		visibleProjects.forEach[p | p.close(null)]
		WorkspaceTestHelper::reallyWaitForJobs(2)
		IResourcesSetupUtil::cleanWorkspace
		IResourcesSetupUtil::reallyWaitForAutoBuild
		WorkspaceTestHelper::reallyWaitForJobs(2)
		info("afterClass done")
	}
	
	def loadGoal() {
		val resourceSet = new ResourceSetImpl()
		val goalResource = resourceSet.getResource(URI.createPlatformResourceURI("/" + MODEL_PROJECT_NAME + "/" + GOAL_FILE_PATH, true), true)
		EcoreUtil.resolveAll(goalResource)
		goal = goalResource.allContents.filter(Goal).next
	}
	
	@BeforeEach
	def	void setUp() {
		setTargetPlatform
		IResourcesSetupUtil::reallyWaitForAutoBuild
		WorkspaceTestHelper::reallyWaitForJobs(2)
		
		loadGoal()
	}
	
	@AfterEach 
	def after() {
		testProject.findMember("gen")?.delete(true, null)
		PerformanceStats.printStats();
	}
	
	@Order(1)
	@Test
	def void abs2abs() {
		val clock = Clock.systemUTC
		val startTime = clock.instant
		
		ABS2ABSWorker.abs2abs(EMFResource.getIFile(goal),goal).join
		
		val endTime = clock.instant
		
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1uc.xmi''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1slice.xmi''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1.traceabilityMap.xmi''')
		
		assertThat(Duration.between(startTime, endTime), lessThan(Duration.ofSeconds(5)))
	}
	@Order(2)
	@Test
	def void abs2gal() {
		val clock = Clock.systemUTC
		val startTime = clock.instant
		
		ABS2GALWorker.abs2gal(EMFResource.getIFile(goal),goal).join
		
		val endTime = clock.instant
		
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1uc.xmi''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1slice.xmi''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1.traceabilityMap.xmi''')
		
		testProject.refreshLocal(IResource.DEPTH_INFINITE, null)
		
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1paramgal.gal''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1flatgal.gal''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1assetmap''')
		
		assertThat(Duration.between(startTime, endTime), lessThan(Duration.ofSeconds(10)))
	}
	
	@Order(3)
	@Test
	def void computeScenarios() {
		PlatformUI.createPreferenceStore(PreferenceConstants).setValue("maxExecTimePreference", 3600)
		val clock = Clock.systemUTC
		val startTime = clock.instant
		
		ABS2ScenariosWorker.abs2scenarios(EMFResource.getIFile(goal),goal).join
		
		val endTime = clock.instant
		
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1uc.xmi''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1slice.xmi''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1.traceabilityMap.xmi''')
		
		testProject.refreshLocal(IResource.DEPTH_INFINITE, null)
		
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1paramgal.gal''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1flatgal.gal''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1assetmap''')
		
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1workgal.gal''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1workprop.prop''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/goals_goal1scenarios''')
		WorkspaceTestHelper::assertFileExists('''«MODEL_PROJECT_NAME»/gen/report.absreport''')
		
		assertThat(Duration.between(startTime, endTime), lessThan(Duration.ofSeconds(300)))
	}
	
	@Order(4)
	@Test
	def void validateModel() {
		val clock = Clock.systemUTC
		val startTime = clock.instant
		
		val structuralValidator = new EMFStructureValidator();
		val absSet = ABSUtils.getAbsSet(goal)
		val diagnotics = new BasicDiagnostic();
		structuralValidator.validate(absSet, diagnotics)
		
		val endTime = clock.instant
		
		assertThat(diagnotics.children.size, equalTo(0))
		
		assertThat(Duration.between(startTime, endTime), lessThan(Duration.ofSeconds(5)))
	}
	
	@Order(5)
	@Test
	def void validateRequirementContracts() {
		val clock = Clock.systemUTC
		val startTime = clock.instant
		
		val contractValidator = new RequirementContractValidator();
		val absSet = ABSUtils.getAbsSet(goal)
		val diagnotics = new BasicDiagnostic();
		contractValidator.validate(absSet, diagnotics)
		
		val endTime = clock.instant
		
		assertThat(diagnotics.children.size, equalTo(6))
		
		assertThat(Duration.between(startTime, endTime), lessThan(Duration.ofSeconds(5)))
	}
	@Order(6)
	@Test
	def void validateDomainContracts() {
		val clock = Clock.systemUTC
		val startTime = clock.instant
		
		val contractValidator = new DomainContractValidator();
		val absSet = ABSUtils.getAbsSet(goal)
		val diagnotics = new BasicDiagnostic();
		contractValidator.validate(absSet, diagnotics)
		
		val endTime = clock.instant
		
		assertThat(diagnotics.children.size, equalTo(0))
		
		assertThat(Duration.between(startTime, endTime), lessThan(Duration.ofSeconds(5)))
	}
}