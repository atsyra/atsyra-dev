package fr.irisa.atsyra.absystem.ui.tests.helpers

import org.eclipse.ui.PlatformUI
import org.eclipse.swt.widgets.Display
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.jobs.Job
import org.eclipse.ui.IWindowListener
import org.eclipse.ui.IWorkbenchWindow
import java.util.ArrayList
import org.eclipse.xtext.ui.testing.util.JavaProjectSetupUtil
import java.util.zip.ZipFile
import org.eclipse.ui.dialogs.IOverwriteQuery
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.ui.internal.wizards.datatransfer.ZipLeveledStructureProvider
import org.eclipse.ui.wizards.datatransfer.ImportOperation
import org.eclipse.osgi.internal.framework.EquinoxBundle
import org.eclipse.osgi.storage.BundleInfo.Generation
import java.io.File
import java.util.Set
import java.util.List
import org.osgi.framework.Bundle
import java.util.HashSet
import org.eclipse.core.runtime.Platform
import org.eclipse.pde.core.target.ITargetPlatformService
import org.eclipse.pde.internal.core.target.TargetPlatformService
import org.eclipse.pde.core.target.ITargetDefinition
import org.eclipse.pde.core.target.ITargetLocation
import org.eclipse.pde.core.target.LoadTargetDefinitionJob
import org.eclipse.core.runtime.Path

import static org.junit.jupiter.api.Assertions.assertTrue;

class WorkspaceTestHelper {
	def void waitWorkbench() {
		// avoids  that SWT freeze the UI
		val workbenchAvailable = newArrayList(false);
		while(workbenchAvailable.get(0)) {
			Display.^default.syncExec(new Runnable(){
				override run() {
					if (PlatformUI.isWorkbenchRunning() && PlatformUI.getWorkbench().getActiveWorkbenchWindow() !== null) {
						workbenchAvailable.set(0, true);
					}
				}
			})
			println("[WorkspaceTestHelper] waiting for Workbench availability")
			Thread.sleep(1000);
		}
		WorkspaceTestHelper.reallyWaitForJobs(50)
		println("[WorkspaceTestHelper] Workbench is available")
	}
	
	def void init() {
		
		Display.^default.syncExec(new Runnable(){
				override run() {
					closeWelcomePage
				}
			})
	}
	
	def void closeWelcomePage() {
		if (PlatformUI.workbench.introManager.intro !== null) {
			PlatformUI.workbench.introManager.closeIntro(PlatformUI.workbench.introManager.intro)
		}
	}

	def IProject getProject(String projectName) {
		return ResourcesPlugin::workspace.root.getProject(projectName)
	}

	def boolean projectExists(String projectName) {
		return getProject(projectName).exists
	}
	
	/**
	 * relaunch the  waitForJobs several times in case some other background task
	 * also wait for idle time to triggers new jobs 
	 */
	static def void reallyWaitForJobs(int retry) {
		for(i : 0.. retry){
			waitForJobs
			Thread.sleep(100)
		}
		waitForJobs
	}
	
	static def void waitForJobs() {
		val delay = 1000;
		var retry = 600
		if(!Job.getJobManager().isIdle()) {
			delay(delay);
		}	
		while (!Job.getJobManager().isIdle()) {
			val Job currentJob = Job.getJobManager().currentJob
			if(retry % 10 == 0 ) {  
				val jobs = Job.getJobManager().find(null)
				for (job : jobs) {
					if(job === currentJob) {
						println("[waitForJobs](current) "+job.name+ " (rule="+job.rule+")")
					} else {
						println("[waitForJobs] "+job.name+ " (rule="+job.rule+")")
					}
					// cancel these jobs that tends to crash the tests
					if(retry <= 300 && (job.name.contains("Git Repository Change Scanner") || 
						job.name.contains("Periodic workspace save") ||
						job.name.contains("Workbench Auto-Save Job") ||
						job.name.contains("Compacting resource model")
					)) {
						println("[waitForJobs] CANCELLING job "+job.name+ " (rule="+job.rule+")")
						job.cancel
					}
				}
				val rules = jobs.filter[j | j.rule !== null].map[j | j.rule]
				rules.forEach[r1 | rules.forEach[r2 | 
					if(r1 != r2 && r1.isConflicting(r2)) {
						println("[waitForJobs] conflicting rules: "+r1+ " / "+r2)
					} 
				]]
			}
			delay(delay);
			retry = retry - 1
			if(retry <= 0) {
				throw new InterruptedException("waitForJobs timed out after " + delay * 600 + "ms")
			}
		}
	}
	
	static var closed = false;
	static def void delay(long waitTimeMillis) {
		val Display display = Display.getCurrent();

		// We try to capture when the window is closed by the tester
		PlatformUI.getWorkbench.addWindowListener(
			new IWindowListener() {

				override windowActivated(IWorkbenchWindow window) {
				}

				override windowClosed(IWorkbenchWindow window) {
					closed = true
				}

				override windowDeactivated(IWorkbenchWindow window) {
				}

				override windowOpened(IWorkbenchWindow window) {
				}

			}
		)

		// If this is the UI thread,
		// then process input.
		if (display !== null) {
			val long endTimeMillis = System.currentTimeMillis() + (waitTimeMillis/2);
			while (System.currentTimeMillis() < endTimeMillis && !closed) {
				if (!display.readAndDispatch())
					display.sleep();
			}
			display.update();
			// do a sleep on current thread too (use half of the allocated time
			try {
				Thread.sleep(waitTimeMillis/2);
			} catch (InterruptedException e) {
				// Ignored.
			}
		}
      // Otherwise, perform a simple sleep.
		else {
			try {
				Thread.sleep(waitTimeMillis);
			} catch (InterruptedException e) {
				// Ignored.
			}
		}
	}	
	
	IProject deployProjectResult = null;
	def IProject deployProject(String projectName, String zipLocation) {
		
		deployProjectResult = null
		val ArrayList<Throwable> thrownException = newArrayList()
		Display.^default.syncExec([
			try {
				val newProject = JavaProjectSetupUtil::createSimpleProject(projectName)
				
				val zip = new ZipFile(zipLocation)
				val structureProvider = new ZipLeveledStructureProvider(zip)
				val queryOverwrite = new IOverwriteQuery() {
					override queryOverwrite(String file) { return ALL }
				}
		
				new ImportOperation(
					newProject.project.fullPath,
					structureProvider.root,
					structureProvider,
					queryOverwrite
				).run(new NullProgressMonitor)
		
				zip.close
				deployProjectResult = newProject.project
				
			} catch (Exception e) { thrownException.add(e) }
		])
		thrownException.forall[e| throw new Exception(e)] // rethrown exception that was executed in the ui thread
		return deployProjectResult
	}
	
	/**
	 * Sets a target platform in the test platform to get workspace builds OK
	 * with PDE
	 * 
	 * @throws Exception
	 */
	def void setTargetPlatform() throws Exception {
		val ITargetPlatformService tpService = TargetPlatformService.getDefault();
		val ITargetDefinition targetDef = tpService.newTarget();
		targetDef.setName("Tycho platform");
		val Bundle[] bundles = Platform.getBundle("org.eclipse.core.runtime").getBundleContext().getBundles();
		val List<ITargetLocation> bundleContainers = new ArrayList<ITargetLocation>();
		val Set<File> dirs = new HashSet<File>();
		for (Bundle bundle : bundles) {
			val EquinoxBundle bundleImpl = bundle as EquinoxBundle;
			val Generation generation = bundleImpl.getModule().getCurrentRevision().getRevisionInfo() as Generation;
			val File file = generation.getBundleFile().getBaseFile();
			val File folder = file.getParentFile();
			if (!dirs.contains(folder)) {
				dirs.add(folder);
				bundleContainers.add(tpService.newDirectoryLocation(folder.getAbsolutePath()));
			}
		}
		val ITargetLocation[] bundleContainersArray = bundleContainers
		targetDef.setTargetLocations(bundleContainersArray);
		targetDef.setArch(Platform.getOSArch());
		targetDef.setOS(Platform.getOS());
		targetDef.setWS(Platform.getWS());
		targetDef.setNL(Platform.getNL());
		// targetDef.setJREContainer()
		tpService.saveTargetDefinition(targetDef);

		val Job job = new LoadTargetDefinitionJob(targetDef);
		job.schedule();
		job.join();
	}
	
	
	// some common assertions
	/**
	 * Usage : projectName/folder/file
	 */
	def static void assertFileExists(String filename) {
		val ws = ResourcesPlugin::workspace
		assertTrue(	ws.root.getFile(new Path(filename)).exists, "Cannot find file " + filename)
	}
	
	/**
	 * Usage : projectName/folder
	 */
	def static void assertFolderExists(String foldername) {
		val ws = ResourcesPlugin::workspace
		assertTrue(
			ws.root.getFolder(new Path(foldername)).exists,
			"Cannot find file " + foldername
		)
	}
	
	/**
	 * Check if {@link project} exist
	 */
	def static void assertProjectExists(String project){
		val ws = ResourcesPlugin::workspace
		assertTrue(
			ws.root.getProject(project).exists,
			"Cannot find project " + project
		)
	}
	
	
}