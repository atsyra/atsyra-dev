The `tests-inputs-gen` folder contains the tests inputs that'll be used by the tests.
It is generated from other locations using the maven script. 

Use the following command to generate it (and synchronize its content with the real sources) (run this from the root of the repository)

Tip to quickly regenerate the zip:

First do a full build of the studio and install it in your local `.m2`

'''
mvn install -f tycho.root/pom.xml -DskipTests=true
'''

then generate only the zip files
'''
mvn generate-sources -f tycho.root/pom.xml -pl :fr.irisa.atsyra.absystem.ui.tests,:2021-12 -am
'''

or launch the tests using maven

'''
mvn verify -f tycho.root/pom.xml -pl :fr.irisa.atsyra.absystem.ui.tests,:2021-12 -am --offline
'''

