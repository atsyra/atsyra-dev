package fr.irisa.atsyra.building.xtext.generator

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.building.AccessConfiguration
import fr.irisa.atsyra.building.AlarmConfiguration
import fr.irisa.atsyra.building.Zone

@Aspect (className=AccessConfiguration)
class AccessConfigurationOBPAspect{
	public def int obpAccessState(){
		return (if(_self.isLocked) 0 else 1) + if(_self.isLocked) 0 else 10;
	}
}

@Aspect (className=AlarmConfiguration)
class AlarmConfigurationOBPAspect{
	public def int obpAlarmState(){
		return (if(_self.isIsEnabled) 1 else 0);
	}
	public def int obpAlarmTriggerState(){
		return (if(_self.wasTriggered) 1 else 0);
	}
}

@Aspect (className=Zone)
class ZoneOBPAspect{
	
}