package fr.irisa.atsyra.building.xtext.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.generator.IFileSystemAccess2
import fr.irisa.atsyra.building.BuildingModel
import fr.irisa.atsyra.building.BuildingConfiguration

import static extension fr.irisa.atsyra.building.xtext.generator.AccessConfigurationOBPAspect.*
import static extension fr.irisa.atsyra.building.xtext.generator.AlarmConfigurationOBPAspect.*
import fr.irisa.atsyra.building.Virtual

class BuildingToFiacre {
		
	
	GalBuildHelper galHelper = new GalBuildHelper
	
	def doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		val buildingModel = resource.contents.get(0) as BuildingModel
		if(!buildingModel.buildingConfigurations.empty){
			galHelper.initHashmaps(buildingModel);
			for(buildingConf: buildingModel.buildingConfigurations) {
				fsa.generateFile(resource.URI.lastSegment.substring(0, resource.URI.lastSegment.lastIndexOf("."))+buildingModel.buildingConfigurations.indexOf(buildingConf)+'.fcr', 
'''process OriginalAttacksStateOn is
states s0
var
	«generateVar(buildingConf)»
from s0
	select
		«generateSelect»
	end select;
	to s0

OriginalAttacksState'''
				)
			}
		}
	}
	
	
	def String generateVar(BuildingConfiguration buildingConf){
		val result = '''
«FOR attackerConf : buildingConf.attackerConfigurations SEPARATOR ","»
	attackerPosition_«attackerConf.attacker.name» 	: int	:= «galHelper.zoneNumber(attackerConf.location)»,
	attackerLevel_«attackerConf.attacker.name» 		: int 	:= «attackerConf.attacker.level»
«ENDFOR»«IF buildingConf.accessConfigurations.exists[accessConf| !(accessConf.access instanceof Virtual)]»,«ENDIF»
«FOR accessConf : buildingConf.accessConfigurations SEPARATOR ","»
	«IF !(accessConf.access instanceof Virtual)»
	accessState_«accessConf.access.name»	: int := «accessConf.obpAccessState»«ENDIF»
«ENDFOR»«IF !buildingConf.alarmConfigurations.empty»,«ENDIF»
«FOR alarmConf : buildingConf.alarmConfigurations  SEPARATOR ","»
	alarmState_«alarmConf.alarm.name»	: int := «alarmConf.obpAlarmState»,
	triggerState_«alarmConf.alarm.name»	: int := «alarmConf.obpAlarmTriggerState»
«ENDFOR»
'''
		return result
	}
	
	def String generateSelect(){
		return ""
	}
}

