package fr.irisa.atsyra.building.xtext.generator

import fr.irisa.atsyra.building.Access
import fr.irisa.atsyra.building.Attacker
import java.util.HashMap
import fr.irisa.atsyra.building.Zone
import fr.irisa.atsyra.building.BuildingModel
import fr.irisa.atsyra.building.Alarm
import fr.irisa.atsyra.building.Item
import static extension fr.irisa.atsyra.building.xtext.modelaspects.BuildingModelAspect.*

class GalBuildHelper {
	
	public HashMap<String,Integer> zoneGalValues = new HashMap<String,Integer>()
	public HashMap<String,Integer> attackerGalValues = new HashMap<String,Integer>()

	/* Initialisation of the values of the zones and attackers of all buildings in GAL
	 * 0 is kept for the null value
	 */
	def void initHashmaps(BuildingModel buildingModel){
		//initialisation of the Hashmap
			var cptZone = 1;
			var cptAttacker = 1;
			for(building : buildingModel.allBuildings){
				for(zone : building.zones){
					zoneGalValues.put(zone.name,cptZone);
					cptZone++;
				}
				for(attacker : building.attackers){
					attackerGalValues.put(attacker.name,cptAttacker);
					cptAttacker++;
				}
			}
	}

	
	def int zoneNumber(Zone z){
		if(z == null) {
			return 0;
		}
		else {
			return zoneGalValues.get(z.name);
		}
	}

	def int attackerNumber(Attacker at){
		if(at == null) {
			return 0;
		}
		else {
			return attackerGalValues.get(at.name);
		}
	}
	
	def int bool2gal(Boolean b){
		if(b) {
			return 1;
		}
		else {
			return 0;
		}
	}
	
//	def String accessGuardFrom1To2(Access access, Attacker attacker) {
//		return '''localization_of_att_«attacker.name» == «zoneNumber(access.zone1)» && open_of_acc_«access.name» == «bool2gal(true)»'''
//	}
//	
//	def String accessActionFrom1To2(Access access, Attacker attacker) {
//		return '''
//		
//			localization_of_att_«attacker.name» = «zoneNumber(access.zone2)»;
//			«FOR alarm : access.zone2.alarms»
//				if (enabled_of_alarm_«alarm.name» == «bool2gal(true)») {
//					triggered_of_alarm_«alarm.name» = «bool2gal(true)»;
//				}
//			«ENDFOR»
//		'''
//	}
//	
//	def String accessGuardFrom2To1(Access access, Attacker attacker) {
//		return '''localization_of_att_«attacker.name» == «zoneNumber(access.zone2)» && open_of_acc_«access.name» == «bool2gal(true)»'''
//	}
//	
//	def String accessActionFrom2To1(Access access, Attacker attacker) {
//		return '''
//		
//			localization_of_att_«attacker.name» = «zoneNumber(access.zone1)»;
//			«FOR alarm : access.zone1.alarms»
//				if (enabled_of_alarm_«alarm.name» == «bool2gal(true)») {
//					triggered_of_alarm_«alarm.name» = «bool2gal(true)»;
//				}
//			«ENDFOR»
//		'''
//	}
//	
//	def String accessGuardOpens(Access access, Attacker attacker) {
//		return '''(localization_of_att_«attacker.name» == «zoneNumber(access.zone1)» || localization_of_att_«attacker.name» == «zoneNumber(access.zone2)») && open_of_acc_«access.name» == «bool2gal(false)»'''
//	}
//	
//	//open triggers the alarms on the access
//	def String accessActionOpens(Access access, Attacker attacker) {
//		return '''
//		
//			open_of_acc_«access.name» = «bool2gal(true)»;
//			«FOR alarm : access.alarms»
//				if (enabled_of_alarm_«alarm.name» == «bool2gal(true)») {
//					triggered_of_alarm_«alarm.name» = «bool2gal(true)»;
//				}
//			«ENDFOR»
//		'''
//	}
//	
//	def String accessGuardCloses(Access access, Attacker attacker) {
//		return '''(localization_of_att_«attacker.name» == «zoneNumber(access.zone1)» || localization_of_att_«attacker.name» == «zoneNumber(access.zone2)») && open_of_acc_«access.name» == «bool2gal(true)»'''
//	}
//	
//	def String accessActionCloses(Access access, Attacker attacker) {
//		return '''
//		
//			open_of_acc_«access.name» = «bool2gal(false)»;
//		'''
//	}
//	
//	//Semantics of multiple keys is assumed to be: attacker needs to have at least one key
//	def String accessGuardUnlocks(Access access, Attacker attacker) {
//		return '''(localization_of_att_«attacker.name» == «zoneNumber(access.zone1)» || localization_of_att_«attacker.name» == «zoneNumber(access.zone2)») && locked_of_acc_«access.name» == «bool2gal(true)» && ( level_of_att_«attacker.name» >= «access.level» «FOR key : access.keys BEFORE ' || ' SEPARATOR ' || '»ownedBy_of_item_«key.name» == «attackerNumber(attacker)»«ENDFOR»)'''
//	}
//	
//	def String accessActionUnlocks(Access access, Attacker attacker) {
//		return '''
//		
//			locked_of_acc_«access.name» = «bool2gal(false)»;
//		'''
//	}
//	
//	def String accessGuardLocks(Access access, Attacker attacker) {
//		return '''(localization_of_att_«attacker.name» == «zoneNumber(access.zone1)» || localization_of_att_«attacker.name» == «zoneNumber(access.zone2)») && locked_of_acc_«access.name» == «bool2gal(false)» «FOR key : access.keys BEFORE '&& (' SEPARATOR ' || ' AFTER ')'»
//				ownedBy_of_item_«key.name» == «attackerNumber(attacker)»
//				«ENDFOR»'''
//	}
//	
//	def String accessActionLocks(Access access, Attacker attacker) {
//		return '''
//		
//			locked_of_acc_«access.name» = «bool2gal(true)»;
//		'''
//	}
//	
//	def String accessGuardEnables(Alarm alarm, Attacker attacker) {
//		return '''localization_of_att_«attacker.name» == «zoneNumber(alarm.localization)» && enabled_of_alarm_«alarm.name» == «bool2gal(false)»'''
//	}
//	
//	def String accessActionEnables(Alarm alarm, Attacker attacker) {
//		return '''
//		
//			enabled_of_alarm_«alarm.name» = «bool2gal(true)»;
//		'''
//	}
//	
//	def String accessGuardDisables(Alarm alarm, Attacker attacker) {
//		return '''localization_of_att_«attacker.name» == «zoneNumber(alarm.localization)» && enabled_of_alarm_«alarm.name» == «bool2gal(true)»'''
//	}
//	
//	def String accessActionDisables(Alarm alarm, Attacker attacker) {
//		return '''
//		
//			enabled_of_alarm_«alarm.name» = «bool2gal(false)»;
//		'''
//	}
//	
//	def String accessGuardTakes(Item item, Attacker attacker) {
//		return '''localization_of_att_«attacker.name» == localization_of_item_«item.name»'''
//	}
//	
//	def String accessActionTakes(Item item, Attacker attacker) {
//		return '''
//		
//			localization_of_item_«item.name» = 0;
//			ownedBy_of_item_«item.name» = «attackerNumber(attacker)»;
//		'''
//	}
//	
//	def String accessGuardDrops(Item item, Attacker attacker) {
//		return '''ownedBy_of_item_«item.name» == «attackerNumber(attacker)»'''
//	}
//	
//	def String accessActionDrops(Item item, Attacker attacker) {
//		return '''
//		
//			ownedBy_of_item_«item.name» = 0;
//			localization_of_item_«item.name» = localization_of_att_«attacker.name»;
//		'''
//	}
	
}