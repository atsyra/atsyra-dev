package fr.irisa.atsyra.building.xtext.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.generator.IFileSystemAccess2
import fr.irisa.atsyra.building.BuildingModel
import static extension fr.irisa.atsyra.building.xtext.modelaspects.BuildingModelAspect.*

class BuildingToGal {
	
//	GalBuildHelper galHelper = new GalBuildHelper
//	
//	def doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
//		val buildingModel = resource.contents.get(0) as BuildingModel
//		if(!buildingModel.buildingConfigurations.empty){
//			galHelper.initHashmaps(buildingModel);
//			for(buildingConf: buildingModel.buildingConfigurations) {
//				fsa.generateFile(resource.URI.lastSegment.substring(0, resource.URI.lastSegment.lastIndexOf("."))+buildingModel.buildingConfigurations.indexOf(buildingConf)+'.gal', 
//'''gal OriginalAttacksState{
//	«FOR attackerConf : buildingConf.attackerConfigurations»
//		int localization_of_att_«attackerConf.attacker.name» = «galHelper.zoneNumber(attackerConf.localization)»;
//		int level_of_att_«attackerConf.attacker.name» = «attackerConf.attacker.level»;
//	«ENDFOR»
//	«FOR itemConf : buildingConf.itemConfigurations»
//		int localization_of_item_«itemConf.item.name» = «galHelper.zoneNumber(itemConf.getLocalization())»;
//		int ownedBy_of_item_«itemConf.item.name» = «galHelper.attackerNumber(itemConf.getOwnedBy())»;
//	«ENDFOR»
//	«FOR accessConf : buildingConf.accessConfigurations»
//		«IF !accessConf.access.virtual»
//			int open_of_acc_«accessConf.access.name» = «galHelper.bool2gal(accessConf.isIsOpen())»;
//			int locked_of_acc_«accessConf.access.name» = «galHelper.bool2gal(accessConf.isIsLocked())»;
//		«ENDIF»
//	«ENDFOR»
//	«FOR alarmConf : buildingConf.alarmConfigurations»
//		int triggered_of_alarm_«alarmConf.alarm.name» = «galHelper.bool2gal(alarmConf.isWasTriggered())»;
//		int enabled_of_alarm_«alarmConf.alarm.name» = «galHelper.bool2gal(alarmConf.isIsEnabled())»;
//	«ENDFOR»
//	«FOR building : buildingModel.allBuildings»
//	  	«FOR attacker : building.attackers»
//	  		«FOR access : building.accesses»
//	  			transition «attacker.name»_goes_from_«access.zone1.name»_to_«access.zone2.name»_by_«access.name» [ «galHelper.accessGuardFrom1To2(access,attacker)» ] {«galHelper.accessActionFrom1To2(access,attacker)»}
//	  			transition «attacker.name»_goes_from_«access.zone2.name»_to_«access.zone1.name»_by_«access.name» [ «galHelper.accessGuardFrom2To1(access,attacker)» ] {«galHelper.accessActionFrom2To1(access,attacker)»}
//	  			transition «attacker.name»_opens_«access.name» [ «galHelper.accessGuardOpens(access,attacker)» ] {«galHelper.accessActionOpens(access,attacker)»}
//	  			transition «attacker.name»_closes_«access.name» [ «galHelper.accessGuardCloses(access,attacker)» ] {«galHelper.accessActionCloses(access,attacker)»}
//	  			«IF !access.keys.isEmpty()»
//	  				transition «attacker.name»_unlocks_«access.name» [ «galHelper.accessGuardUnlocks(access,attacker)» ] {«galHelper.accessActionUnlocks(access,attacker)»}
//	  				transition «attacker.name»_locks_«access.name» [ «galHelper.accessGuardLocks(access,attacker)» ] {«galHelper.accessActionLocks(access,attacker)»}
//	  			«ENDIF»
//	  		«ENDFOR»
//	  		«FOR alarm : building.alarms»
//	  			transition «attacker.name»_enables_«alarm.name» [ «galHelper.accessGuardEnables(alarm,attacker)» ] {«galHelper.accessActionEnables(alarm,attacker)»}
//	  			transition «attacker.name»_disables_«alarm.name» [ «galHelper.accessGuardDisables(alarm,attacker)» ] {«galHelper.accessActionDisables(alarm,attacker)»}
//	  		«ENDFOR»
//	  		«FOR item : building.items»
//	  			transition «attacker.name»_takes_«item.name» [ «galHelper.accessGuardTakes(item,attacker)» ] {«galHelper.accessActionTakes(item,attacker)»}
//	  			transition «attacker.name»_drops_«item.name» [ «galHelper.accessGuardDrops(item,attacker)» ] {«galHelper.accessActionDrops(item,attacker)»}
//	  		«ENDFOR»
//		«ENDFOR»
//	«ENDFOR»
//}'''
//				)
//			}
//		}
//	}
}

