package fr.irisa.atsyra.building.xtext.modelaspects

import fr.irisa.atsyra.building.Import;
import fr.irisa.atsyra.building.BuildingModel

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.common.util.EList
import fr.irisa.atsyra.building.Building
import static extension fr.irisa.atsyra.building.xtext.modelaspects.ImportAspect.*
import static extension fr.irisa.atsyra.building.xtext.modelaspects.BuildingModelAspect.*
import org.eclipse.emf.common.util.BasicEList

@Aspect (className=Import)
class ImportAspect {
	def URI getEMFURI(){
		return URI.createURI(_self.getImportURI()).resolve(_self.eResource().getURI())
	}
	
	def EList<Building> getBuildings(){
		val importRes = _self.eResource.resourceSet.getResource(_self.EMFURI, true)
		val bmodel = importRes.contents.get(0) as BuildingModel
		return bmodel.allBuildings
	}
}

@Aspect (className=BuildingModel)
class BuildingModelAspect {
	EList<Building> allBuildingsCache = null
	
	def EList<Building> getImportedBuildings(){
		val EList<Building> res = new BasicEList<Building>
		for ( imp : _self.imports) {
			res.addAll(imp.buildings)
		}
		return res
	}
	def EList<Building> getAllBuildings(){
		if(_self.allBuildingsCache == null){
			_self.allBuildingsCache = new BasicEList<Building>(_self.buildings)
			_self.allBuildingsCache.addAll(_self.importedBuildings)
		}
		return _self.allBuildingsCache
	}
	
}