/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.building.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.atsyra.building.xtext.services.BuildingGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalBuildingParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'import'", "'Building'", "'{'", "'}'", "'Zone'", "'alarms'", "'('", "','", "')'", "'Alarm'", "'location'", "'Virtual'", "'access'", "'zone1'", "'zone2'", "'Door'", "'level'", "'keys'", "'Window'", "'inside'", "'outside'", "'BadgedDoor'", "'badges'", "'Item'", "'Attacker'", "'-'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalBuildingParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalBuildingParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalBuildingParser.tokenNames; }
    public String getGrammarFileName() { return "InternalBuilding.g"; }



     	private BuildingGrammarAccess grammarAccess;

        public InternalBuildingParser(TokenStream input, BuildingGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "BuildingModel";
       	}

       	@Override
       	protected BuildingGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleBuildingModel"
    // InternalBuilding.g:64:1: entryRuleBuildingModel returns [EObject current=null] : iv_ruleBuildingModel= ruleBuildingModel EOF ;
    public final EObject entryRuleBuildingModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBuildingModel = null;


        try {
            // InternalBuilding.g:64:54: (iv_ruleBuildingModel= ruleBuildingModel EOF )
            // InternalBuilding.g:65:2: iv_ruleBuildingModel= ruleBuildingModel EOF
            {
             newCompositeNode(grammarAccess.getBuildingModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBuildingModel=ruleBuildingModel();

            state._fsp--;

             current =iv_ruleBuildingModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBuildingModel"


    // $ANTLR start "ruleBuildingModel"
    // InternalBuilding.g:71:1: ruleBuildingModel returns [EObject current=null] : ( () ( (lv_imports_1_0= ruleImport ) )* ( (lv_buildings_2_0= ruleBuilding ) )* ) ;
    public final EObject ruleBuildingModel() throws RecognitionException {
        EObject current = null;

        EObject lv_imports_1_0 = null;

        EObject lv_buildings_2_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:77:2: ( ( () ( (lv_imports_1_0= ruleImport ) )* ( (lv_buildings_2_0= ruleBuilding ) )* ) )
            // InternalBuilding.g:78:2: ( () ( (lv_imports_1_0= ruleImport ) )* ( (lv_buildings_2_0= ruleBuilding ) )* )
            {
            // InternalBuilding.g:78:2: ( () ( (lv_imports_1_0= ruleImport ) )* ( (lv_buildings_2_0= ruleBuilding ) )* )
            // InternalBuilding.g:79:3: () ( (lv_imports_1_0= ruleImport ) )* ( (lv_buildings_2_0= ruleBuilding ) )*
            {
            // InternalBuilding.g:79:3: ()
            // InternalBuilding.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getBuildingModelAccess().getBuildingModelAction_0(),
            					current);
            			

            }

            // InternalBuilding.g:86:3: ( (lv_imports_1_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalBuilding.g:87:4: (lv_imports_1_0= ruleImport )
            	    {
            	    // InternalBuilding.g:87:4: (lv_imports_1_0= ruleImport )
            	    // InternalBuilding.g:88:5: lv_imports_1_0= ruleImport
            	    {

            	    					newCompositeNode(grammarAccess.getBuildingModelAccess().getImportsImportParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_imports_1_0=ruleImport();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getBuildingModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_1_0,
            	    						"fr.irisa.atsyra.building.xtext.Building.Import");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalBuilding.g:105:3: ( (lv_buildings_2_0= ruleBuilding ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalBuilding.g:106:4: (lv_buildings_2_0= ruleBuilding )
            	    {
            	    // InternalBuilding.g:106:4: (lv_buildings_2_0= ruleBuilding )
            	    // InternalBuilding.g:107:5: lv_buildings_2_0= ruleBuilding
            	    {

            	    					newCompositeNode(grammarAccess.getBuildingModelAccess().getBuildingsBuildingParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_buildings_2_0=ruleBuilding();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getBuildingModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"buildings",
            	    						lv_buildings_2_0,
            	    						"fr.irisa.atsyra.building.xtext.Building.Building");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBuildingModel"


    // $ANTLR start "entryRuleImport"
    // InternalBuilding.g:128:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalBuilding.g:128:47: (iv_ruleImport= ruleImport EOF )
            // InternalBuilding.g:129:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalBuilding.g:135:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importURI_1_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:141:2: ( (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) ) )
            // InternalBuilding.g:142:2: (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) )
            {
            // InternalBuilding.g:142:2: (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) )
            // InternalBuilding.g:143:3: otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalBuilding.g:147:3: ( (lv_importURI_1_0= ruleEString ) )
            // InternalBuilding.g:148:4: (lv_importURI_1_0= ruleEString )
            {
            // InternalBuilding.g:148:4: (lv_importURI_1_0= ruleEString )
            // InternalBuilding.g:149:5: lv_importURI_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getImportAccess().getImportURIEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_importURI_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImportRule());
            					}
            					set(
            						current,
            						"importURI",
            						lv_importURI_1_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleBuilding"
    // InternalBuilding.g:170:1: entryRuleBuilding returns [EObject current=null] : iv_ruleBuilding= ruleBuilding EOF ;
    public final EObject entryRuleBuilding() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBuilding = null;


        try {
            // InternalBuilding.g:170:49: (iv_ruleBuilding= ruleBuilding EOF )
            // InternalBuilding.g:171:2: iv_ruleBuilding= ruleBuilding EOF
            {
             newCompositeNode(grammarAccess.getBuildingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBuilding=ruleBuilding();

            state._fsp--;

             current =iv_ruleBuilding; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBuilding"


    // $ANTLR start "ruleBuilding"
    // InternalBuilding.g:177:1: ruleBuilding returns [EObject current=null] : ( () otherlv_1= 'Building' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_accesses_4_0= ruleAccess ) ) | ( (lv_alarms_5_0= ruleAlarm ) ) | ( (lv_attackers_6_0= ruleAttacker ) ) | ( (lv_items_7_0= ruleItem ) ) | ( (lv_zones_8_0= ruleZone ) ) )* otherlv_9= '}' ) ;
    public final EObject ruleBuilding() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_accesses_4_0 = null;

        EObject lv_alarms_5_0 = null;

        EObject lv_attackers_6_0 = null;

        EObject lv_items_7_0 = null;

        EObject lv_zones_8_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:183:2: ( ( () otherlv_1= 'Building' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_accesses_4_0= ruleAccess ) ) | ( (lv_alarms_5_0= ruleAlarm ) ) | ( (lv_attackers_6_0= ruleAttacker ) ) | ( (lv_items_7_0= ruleItem ) ) | ( (lv_zones_8_0= ruleZone ) ) )* otherlv_9= '}' ) )
            // InternalBuilding.g:184:2: ( () otherlv_1= 'Building' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_accesses_4_0= ruleAccess ) ) | ( (lv_alarms_5_0= ruleAlarm ) ) | ( (lv_attackers_6_0= ruleAttacker ) ) | ( (lv_items_7_0= ruleItem ) ) | ( (lv_zones_8_0= ruleZone ) ) )* otherlv_9= '}' )
            {
            // InternalBuilding.g:184:2: ( () otherlv_1= 'Building' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_accesses_4_0= ruleAccess ) ) | ( (lv_alarms_5_0= ruleAlarm ) ) | ( (lv_attackers_6_0= ruleAttacker ) ) | ( (lv_items_7_0= ruleItem ) ) | ( (lv_zones_8_0= ruleZone ) ) )* otherlv_9= '}' )
            // InternalBuilding.g:185:3: () otherlv_1= 'Building' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_accesses_4_0= ruleAccess ) ) | ( (lv_alarms_5_0= ruleAlarm ) ) | ( (lv_attackers_6_0= ruleAttacker ) ) | ( (lv_items_7_0= ruleItem ) ) | ( (lv_zones_8_0= ruleZone ) ) )* otherlv_9= '}'
            {
            // InternalBuilding.g:185:3: ()
            // InternalBuilding.g:186:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getBuildingAccess().getBuildingAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getBuildingAccess().getBuildingKeyword_1());
            		
            // InternalBuilding.g:196:3: ( (lv_name_2_0= ruleEString ) )
            // InternalBuilding.g:197:4: (lv_name_2_0= ruleEString )
            {
            // InternalBuilding.g:197:4: (lv_name_2_0= ruleEString )
            // InternalBuilding.g:198:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getBuildingAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBuildingRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_3, grammarAccess.getBuildingAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalBuilding.g:219:3: ( ( (lv_accesses_4_0= ruleAccess ) ) | ( (lv_alarms_5_0= ruleAlarm ) ) | ( (lv_attackers_6_0= ruleAttacker ) ) | ( (lv_items_7_0= ruleItem ) ) | ( (lv_zones_8_0= ruleZone ) ) )*
            loop3:
            do {
                int alt3=6;
                switch ( input.LA(1) ) {
                case 22:
                case 26:
                case 29:
                case 32:
                    {
                    alt3=1;
                    }
                    break;
                case 20:
                    {
                    alt3=2;
                    }
                    break;
                case 35:
                    {
                    alt3=3;
                    }
                    break;
                case 34:
                    {
                    alt3=4;
                    }
                    break;
                case 15:
                    {
                    alt3=5;
                    }
                    break;

                }

                switch (alt3) {
            	case 1 :
            	    // InternalBuilding.g:220:4: ( (lv_accesses_4_0= ruleAccess ) )
            	    {
            	    // InternalBuilding.g:220:4: ( (lv_accesses_4_0= ruleAccess ) )
            	    // InternalBuilding.g:221:5: (lv_accesses_4_0= ruleAccess )
            	    {
            	    // InternalBuilding.g:221:5: (lv_accesses_4_0= ruleAccess )
            	    // InternalBuilding.g:222:6: lv_accesses_4_0= ruleAccess
            	    {

            	    						newCompositeNode(grammarAccess.getBuildingAccess().getAccessesAccessParserRuleCall_4_0_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_accesses_4_0=ruleAccess();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBuildingRule());
            	    						}
            	    						add(
            	    							current,
            	    							"accesses",
            	    							lv_accesses_4_0,
            	    							"fr.irisa.atsyra.building.xtext.Building.Access");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalBuilding.g:240:4: ( (lv_alarms_5_0= ruleAlarm ) )
            	    {
            	    // InternalBuilding.g:240:4: ( (lv_alarms_5_0= ruleAlarm ) )
            	    // InternalBuilding.g:241:5: (lv_alarms_5_0= ruleAlarm )
            	    {
            	    // InternalBuilding.g:241:5: (lv_alarms_5_0= ruleAlarm )
            	    // InternalBuilding.g:242:6: lv_alarms_5_0= ruleAlarm
            	    {

            	    						newCompositeNode(grammarAccess.getBuildingAccess().getAlarmsAlarmParserRuleCall_4_1_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_alarms_5_0=ruleAlarm();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBuildingRule());
            	    						}
            	    						add(
            	    							current,
            	    							"alarms",
            	    							lv_alarms_5_0,
            	    							"fr.irisa.atsyra.building.xtext.Building.Alarm");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalBuilding.g:260:4: ( (lv_attackers_6_0= ruleAttacker ) )
            	    {
            	    // InternalBuilding.g:260:4: ( (lv_attackers_6_0= ruleAttacker ) )
            	    // InternalBuilding.g:261:5: (lv_attackers_6_0= ruleAttacker )
            	    {
            	    // InternalBuilding.g:261:5: (lv_attackers_6_0= ruleAttacker )
            	    // InternalBuilding.g:262:6: lv_attackers_6_0= ruleAttacker
            	    {

            	    						newCompositeNode(grammarAccess.getBuildingAccess().getAttackersAttackerParserRuleCall_4_2_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_attackers_6_0=ruleAttacker();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBuildingRule());
            	    						}
            	    						add(
            	    							current,
            	    							"attackers",
            	    							lv_attackers_6_0,
            	    							"fr.irisa.atsyra.building.xtext.Building.Attacker");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalBuilding.g:280:4: ( (lv_items_7_0= ruleItem ) )
            	    {
            	    // InternalBuilding.g:280:4: ( (lv_items_7_0= ruleItem ) )
            	    // InternalBuilding.g:281:5: (lv_items_7_0= ruleItem )
            	    {
            	    // InternalBuilding.g:281:5: (lv_items_7_0= ruleItem )
            	    // InternalBuilding.g:282:6: lv_items_7_0= ruleItem
            	    {

            	    						newCompositeNode(grammarAccess.getBuildingAccess().getItemsItemParserRuleCall_4_3_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_items_7_0=ruleItem();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBuildingRule());
            	    						}
            	    						add(
            	    							current,
            	    							"items",
            	    							lv_items_7_0,
            	    							"fr.irisa.atsyra.building.xtext.Building.Item");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalBuilding.g:300:4: ( (lv_zones_8_0= ruleZone ) )
            	    {
            	    // InternalBuilding.g:300:4: ( (lv_zones_8_0= ruleZone ) )
            	    // InternalBuilding.g:301:5: (lv_zones_8_0= ruleZone )
            	    {
            	    // InternalBuilding.g:301:5: (lv_zones_8_0= ruleZone )
            	    // InternalBuilding.g:302:6: lv_zones_8_0= ruleZone
            	    {

            	    						newCompositeNode(grammarAccess.getBuildingAccess().getZonesZoneParserRuleCall_4_4_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_zones_8_0=ruleZone();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBuildingRule());
            	    						}
            	    						add(
            	    							current,
            	    							"zones",
            	    							lv_zones_8_0,
            	    							"fr.irisa.atsyra.building.xtext.Building.Zone");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_9=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getBuildingAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBuilding"


    // $ANTLR start "entryRuleEString"
    // InternalBuilding.g:328:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalBuilding.g:328:47: (iv_ruleEString= ruleEString EOF )
            // InternalBuilding.g:329:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalBuilding.g:335:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalBuilding.g:341:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalBuilding.g:342:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalBuilding.g:342:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_STRING) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_ID) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalBuilding.g:343:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalBuilding.g:351:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleZone"
    // InternalBuilding.g:362:1: entryRuleZone returns [EObject current=null] : iv_ruleZone= ruleZone EOF ;
    public final EObject entryRuleZone() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleZone = null;


        try {
            // InternalBuilding.g:362:45: (iv_ruleZone= ruleZone EOF )
            // InternalBuilding.g:363:2: iv_ruleZone= ruleZone EOF
            {
             newCompositeNode(grammarAccess.getZoneRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleZone=ruleZone();

            state._fsp--;

             current =iv_ruleZone; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleZone"


    // $ANTLR start "ruleZone"
    // InternalBuilding.g:369:1: ruleZone returns [EObject current=null] : ( () otherlv_1= 'Zone' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'alarms' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? otherlv_10= '}' ) ;
    public final EObject ruleZone() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:375:2: ( ( () otherlv_1= 'Zone' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'alarms' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? otherlv_10= '}' ) )
            // InternalBuilding.g:376:2: ( () otherlv_1= 'Zone' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'alarms' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? otherlv_10= '}' )
            {
            // InternalBuilding.g:376:2: ( () otherlv_1= 'Zone' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'alarms' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? otherlv_10= '}' )
            // InternalBuilding.g:377:3: () otherlv_1= 'Zone' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'alarms' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? otherlv_10= '}'
            {
            // InternalBuilding.g:377:3: ()
            // InternalBuilding.g:378:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getZoneAccess().getZoneAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,15,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getZoneAccess().getZoneKeyword_1());
            		
            // InternalBuilding.g:388:3: ( (lv_name_2_0= ruleEString ) )
            // InternalBuilding.g:389:4: (lv_name_2_0= ruleEString )
            {
            // InternalBuilding.g:389:4: (lv_name_2_0= ruleEString )
            // InternalBuilding.g:390:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getZoneAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getZoneRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_3, grammarAccess.getZoneAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalBuilding.g:411:3: (otherlv_4= 'alarms' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==16) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalBuilding.g:412:4: otherlv_4= 'alarms' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,16,FOLLOW_9); 

                    				newLeafNode(otherlv_4, grammarAccess.getZoneAccess().getAlarmsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,17,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getZoneAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalBuilding.g:420:4: ( ( ruleEString ) )
                    // InternalBuilding.g:421:5: ( ruleEString )
                    {
                    // InternalBuilding.g:421:5: ( ruleEString )
                    // InternalBuilding.g:422:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getZoneRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getZoneAccess().getAlarmsAlarmCrossReference_4_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalBuilding.g:436:4: (otherlv_7= ',' ( ( ruleEString ) ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==18) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalBuilding.g:437:5: otherlv_7= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_7=(Token)match(input,18,FOLLOW_5); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getZoneAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalBuilding.g:441:5: ( ( ruleEString ) )
                    	    // InternalBuilding.g:442:6: ( ruleEString )
                    	    {
                    	    // InternalBuilding.g:442:6: ( ruleEString )
                    	    // InternalBuilding.g:443:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getZoneRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getZoneAccess().getAlarmsAlarmCrossReference_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,19,FOLLOW_11); 

                    				newLeafNode(otherlv_9, grammarAccess.getZoneAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            otherlv_10=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getZoneAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleZone"


    // $ANTLR start "entryRuleAlarm"
    // InternalBuilding.g:471:1: entryRuleAlarm returns [EObject current=null] : iv_ruleAlarm= ruleAlarm EOF ;
    public final EObject entryRuleAlarm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlarm = null;


        try {
            // InternalBuilding.g:471:46: (iv_ruleAlarm= ruleAlarm EOF )
            // InternalBuilding.g:472:2: iv_ruleAlarm= ruleAlarm EOF
            {
             newCompositeNode(grammarAccess.getAlarmRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAlarm=ruleAlarm();

            state._fsp--;

             current =iv_ruleAlarm; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlarm"


    // $ANTLR start "ruleAlarm"
    // InternalBuilding.g:478:1: ruleAlarm returns [EObject current=null] : ( () otherlv_1= 'Alarm' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'location' ( ( ruleEString ) ) )? otherlv_6= '}' ) ;
    public final EObject ruleAlarm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:484:2: ( ( () otherlv_1= 'Alarm' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'location' ( ( ruleEString ) ) )? otherlv_6= '}' ) )
            // InternalBuilding.g:485:2: ( () otherlv_1= 'Alarm' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'location' ( ( ruleEString ) ) )? otherlv_6= '}' )
            {
            // InternalBuilding.g:485:2: ( () otherlv_1= 'Alarm' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'location' ( ( ruleEString ) ) )? otherlv_6= '}' )
            // InternalBuilding.g:486:3: () otherlv_1= 'Alarm' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'location' ( ( ruleEString ) ) )? otherlv_6= '}'
            {
            // InternalBuilding.g:486:3: ()
            // InternalBuilding.g:487:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAlarmAccess().getAlarmAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,20,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getAlarmAccess().getAlarmKeyword_1());
            		
            // InternalBuilding.g:497:3: ( (lv_name_2_0= ruleEString ) )
            // InternalBuilding.g:498:4: (lv_name_2_0= ruleEString )
            {
            // InternalBuilding.g:498:4: (lv_name_2_0= ruleEString )
            // InternalBuilding.g:499:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAlarmAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAlarmRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_12); 

            			newLeafNode(otherlv_3, grammarAccess.getAlarmAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalBuilding.g:520:3: (otherlv_4= 'location' ( ( ruleEString ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalBuilding.g:521:4: otherlv_4= 'location' ( ( ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,21,FOLLOW_5); 

                    				newLeafNode(otherlv_4, grammarAccess.getAlarmAccess().getLocationKeyword_4_0());
                    			
                    // InternalBuilding.g:525:4: ( ( ruleEString ) )
                    // InternalBuilding.g:526:5: ( ruleEString )
                    {
                    // InternalBuilding.g:526:5: ( ruleEString )
                    // InternalBuilding.g:527:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAlarmRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getAlarmAccess().getLocationZoneCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getAlarmAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlarm"


    // $ANTLR start "entryRuleAccess"
    // InternalBuilding.g:550:1: entryRuleAccess returns [EObject current=null] : iv_ruleAccess= ruleAccess EOF ;
    public final EObject entryRuleAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAccess = null;


        try {
            // InternalBuilding.g:550:47: (iv_ruleAccess= ruleAccess EOF )
            // InternalBuilding.g:551:2: iv_ruleAccess= ruleAccess EOF
            {
             newCompositeNode(grammarAccess.getAccessRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAccess=ruleAccess();

            state._fsp--;

             current =iv_ruleAccess; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAccess"


    // $ANTLR start "ruleAccess"
    // InternalBuilding.g:557:1: ruleAccess returns [EObject current=null] : (this_Virtual_0= ruleVirtual | this_Door_1= ruleDoor | this_Window_2= ruleWindow | this_Badged_3= ruleBadged ) ;
    public final EObject ruleAccess() throws RecognitionException {
        EObject current = null;

        EObject this_Virtual_0 = null;

        EObject this_Door_1 = null;

        EObject this_Window_2 = null;

        EObject this_Badged_3 = null;



        	enterRule();

        try {
            // InternalBuilding.g:563:2: ( (this_Virtual_0= ruleVirtual | this_Door_1= ruleDoor | this_Window_2= ruleWindow | this_Badged_3= ruleBadged ) )
            // InternalBuilding.g:564:2: (this_Virtual_0= ruleVirtual | this_Door_1= ruleDoor | this_Window_2= ruleWindow | this_Badged_3= ruleBadged )
            {
            // InternalBuilding.g:564:2: (this_Virtual_0= ruleVirtual | this_Door_1= ruleDoor | this_Window_2= ruleWindow | this_Badged_3= ruleBadged )
            int alt8=4;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt8=1;
                }
                break;
            case 26:
                {
                alt8=2;
                }
                break;
            case 29:
                {
                alt8=3;
                }
                break;
            case 32:
                {
                alt8=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalBuilding.g:565:3: this_Virtual_0= ruleVirtual
                    {

                    			newCompositeNode(grammarAccess.getAccessAccess().getVirtualParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Virtual_0=ruleVirtual();

                    state._fsp--;


                    			current = this_Virtual_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalBuilding.g:574:3: this_Door_1= ruleDoor
                    {

                    			newCompositeNode(grammarAccess.getAccessAccess().getDoorParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Door_1=ruleDoor();

                    state._fsp--;


                    			current = this_Door_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalBuilding.g:583:3: this_Window_2= ruleWindow
                    {

                    			newCompositeNode(grammarAccess.getAccessAccess().getWindowParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Window_2=ruleWindow();

                    state._fsp--;


                    			current = this_Window_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalBuilding.g:592:3: this_Badged_3= ruleBadged
                    {

                    			newCompositeNode(grammarAccess.getAccessAccess().getBadgedParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Badged_3=ruleBadged();

                    state._fsp--;


                    			current = this_Badged_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAccess"


    // $ANTLR start "entryRuleVirtual"
    // InternalBuilding.g:604:1: entryRuleVirtual returns [EObject current=null] : iv_ruleVirtual= ruleVirtual EOF ;
    public final EObject entryRuleVirtual() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVirtual = null;


        try {
            // InternalBuilding.g:604:48: (iv_ruleVirtual= ruleVirtual EOF )
            // InternalBuilding.g:605:2: iv_ruleVirtual= ruleVirtual EOF
            {
             newCompositeNode(grammarAccess.getVirtualRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVirtual=ruleVirtual();

            state._fsp--;

             current =iv_ruleVirtual; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVirtual"


    // $ANTLR start "ruleVirtual"
    // InternalBuilding.g:611:1: ruleVirtual returns [EObject current=null] : (otherlv_0= 'Virtual' otherlv_1= 'access' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'zone1' ( ( ruleEString ) ) otherlv_6= 'zone2' ( ( ruleEString ) ) otherlv_8= '}' ) ;
    public final EObject ruleVirtual() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:617:2: ( (otherlv_0= 'Virtual' otherlv_1= 'access' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'zone1' ( ( ruleEString ) ) otherlv_6= 'zone2' ( ( ruleEString ) ) otherlv_8= '}' ) )
            // InternalBuilding.g:618:2: (otherlv_0= 'Virtual' otherlv_1= 'access' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'zone1' ( ( ruleEString ) ) otherlv_6= 'zone2' ( ( ruleEString ) ) otherlv_8= '}' )
            {
            // InternalBuilding.g:618:2: (otherlv_0= 'Virtual' otherlv_1= 'access' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'zone1' ( ( ruleEString ) ) otherlv_6= 'zone2' ( ( ruleEString ) ) otherlv_8= '}' )
            // InternalBuilding.g:619:3: otherlv_0= 'Virtual' otherlv_1= 'access' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'zone1' ( ( ruleEString ) ) otherlv_6= 'zone2' ( ( ruleEString ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getVirtualAccess().getVirtualKeyword_0());
            		
            otherlv_1=(Token)match(input,23,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getVirtualAccess().getAccessKeyword_1());
            		
            // InternalBuilding.g:627:3: ( (lv_name_2_0= ruleEString ) )
            // InternalBuilding.g:628:4: (lv_name_2_0= ruleEString )
            {
            // InternalBuilding.g:628:4: (lv_name_2_0= ruleEString )
            // InternalBuilding.g:629:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getVirtualAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getVirtualRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_14); 

            			newLeafNode(otherlv_3, grammarAccess.getVirtualAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,24,FOLLOW_5); 

            			newLeafNode(otherlv_4, grammarAccess.getVirtualAccess().getZone1Keyword_4());
            		
            // InternalBuilding.g:654:3: ( ( ruleEString ) )
            // InternalBuilding.g:655:4: ( ruleEString )
            {
            // InternalBuilding.g:655:4: ( ruleEString )
            // InternalBuilding.g:656:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getVirtualRule());
            					}
            				

            					newCompositeNode(grammarAccess.getVirtualAccess().getZone1ZoneCrossReference_5_0());
            				
            pushFollow(FOLLOW_15);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,25,FOLLOW_5); 

            			newLeafNode(otherlv_6, grammarAccess.getVirtualAccess().getZone2Keyword_6());
            		
            // InternalBuilding.g:674:3: ( ( ruleEString ) )
            // InternalBuilding.g:675:4: ( ruleEString )
            {
            // InternalBuilding.g:675:4: ( ruleEString )
            // InternalBuilding.g:676:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getVirtualRule());
            					}
            				

            					newCompositeNode(grammarAccess.getVirtualAccess().getZone2ZoneCrossReference_7_0());
            				
            pushFollow(FOLLOW_11);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getVirtualAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVirtual"


    // $ANTLR start "entryRuleDoor"
    // InternalBuilding.g:698:1: entryRuleDoor returns [EObject current=null] : iv_ruleDoor= ruleDoor EOF ;
    public final EObject entryRuleDoor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDoor = null;


        try {
            // InternalBuilding.g:698:45: (iv_ruleDoor= ruleDoor EOF )
            // InternalBuilding.g:699:2: iv_ruleDoor= ruleDoor EOF
            {
             newCompositeNode(grammarAccess.getDoorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDoor=ruleDoor();

            state._fsp--;

             current =iv_ruleDoor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDoor"


    // $ANTLR start "ruleDoor"
    // InternalBuilding.g:705:1: ruleDoor returns [EObject current=null] : (otherlv_0= 'Door' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'zone1' ( ( ruleEString ) ) )? (otherlv_7= 'zone2' ( ( ruleEString ) ) )? (otherlv_9= 'keys' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}' ) ;
    public final EObject ruleDoor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_level_4_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:711:2: ( (otherlv_0= 'Door' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'zone1' ( ( ruleEString ) ) )? (otherlv_7= 'zone2' ( ( ruleEString ) ) )? (otherlv_9= 'keys' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}' ) )
            // InternalBuilding.g:712:2: (otherlv_0= 'Door' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'zone1' ( ( ruleEString ) ) )? (otherlv_7= 'zone2' ( ( ruleEString ) ) )? (otherlv_9= 'keys' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}' )
            {
            // InternalBuilding.g:712:2: (otherlv_0= 'Door' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'zone1' ( ( ruleEString ) ) )? (otherlv_7= 'zone2' ( ( ruleEString ) ) )? (otherlv_9= 'keys' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}' )
            // InternalBuilding.g:713:3: otherlv_0= 'Door' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'zone1' ( ( ruleEString ) ) )? (otherlv_7= 'zone2' ( ( ruleEString ) ) )? (otherlv_9= 'keys' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getDoorAccess().getDoorKeyword_0());
            		
            // InternalBuilding.g:717:3: ( (lv_name_1_0= ruleEString ) )
            // InternalBuilding.g:718:4: (lv_name_1_0= ruleEString )
            {
            // InternalBuilding.g:718:4: (lv_name_1_0= ruleEString )
            // InternalBuilding.g:719:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getDoorAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDoorRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getDoorAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalBuilding.g:740:3: (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==27) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalBuilding.g:741:4: otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_17); 

                    				newLeafNode(otherlv_3, grammarAccess.getDoorAccess().getLevelKeyword_3_0());
                    			
                    // InternalBuilding.g:745:4: ( (lv_level_4_0= ruleEInt ) )
                    // InternalBuilding.g:746:5: (lv_level_4_0= ruleEInt )
                    {
                    // InternalBuilding.g:746:5: (lv_level_4_0= ruleEInt )
                    // InternalBuilding.g:747:6: lv_level_4_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getDoorAccess().getLevelEIntParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_level_4_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDoorRule());
                    						}
                    						set(
                    							current,
                    							"level",
                    							lv_level_4_0,
                    							"fr.irisa.atsyra.building.xtext.Building.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalBuilding.g:765:3: (otherlv_5= 'zone1' ( ( ruleEString ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==24) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalBuilding.g:766:4: otherlv_5= 'zone1' ( ( ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,24,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getDoorAccess().getZone1Keyword_4_0());
                    			
                    // InternalBuilding.g:770:4: ( ( ruleEString ) )
                    // InternalBuilding.g:771:5: ( ruleEString )
                    {
                    // InternalBuilding.g:771:5: ( ruleEString )
                    // InternalBuilding.g:772:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDoorRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getDoorAccess().getZone1ZoneCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_19);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalBuilding.g:787:3: (otherlv_7= 'zone2' ( ( ruleEString ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==25) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalBuilding.g:788:4: otherlv_7= 'zone2' ( ( ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,25,FOLLOW_5); 

                    				newLeafNode(otherlv_7, grammarAccess.getDoorAccess().getZone2Keyword_5_0());
                    			
                    // InternalBuilding.g:792:4: ( ( ruleEString ) )
                    // InternalBuilding.g:793:5: ( ruleEString )
                    {
                    // InternalBuilding.g:793:5: ( ruleEString )
                    // InternalBuilding.g:794:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDoorRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getDoorAccess().getZone2ZoneCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_20);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalBuilding.g:809:3: (otherlv_9= 'keys' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==28) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalBuilding.g:810:4: otherlv_9= 'keys' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')'
                    {
                    otherlv_9=(Token)match(input,28,FOLLOW_9); 

                    				newLeafNode(otherlv_9, grammarAccess.getDoorAccess().getKeysKeyword_6_0());
                    			
                    otherlv_10=(Token)match(input,17,FOLLOW_5); 

                    				newLeafNode(otherlv_10, grammarAccess.getDoorAccess().getLeftParenthesisKeyword_6_1());
                    			
                    // InternalBuilding.g:818:4: ( ( ruleEString ) )
                    // InternalBuilding.g:819:5: ( ruleEString )
                    {
                    // InternalBuilding.g:819:5: ( ruleEString )
                    // InternalBuilding.g:820:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDoorRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getDoorAccess().getKeysItemCrossReference_6_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalBuilding.g:834:4: (otherlv_12= ',' ( ( ruleEString ) ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==18) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalBuilding.g:835:5: otherlv_12= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_12=(Token)match(input,18,FOLLOW_5); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getDoorAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalBuilding.g:839:5: ( ( ruleEString ) )
                    	    // InternalBuilding.g:840:6: ( ruleEString )
                    	    {
                    	    // InternalBuilding.g:840:6: ( ruleEString )
                    	    // InternalBuilding.g:841:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getDoorRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getDoorAccess().getKeysItemCrossReference_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,19,FOLLOW_8); 

                    				newLeafNode(otherlv_14, grammarAccess.getDoorAccess().getRightParenthesisKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalBuilding.g:861:3: (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==16) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalBuilding.g:862:4: otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')'
                    {
                    otherlv_15=(Token)match(input,16,FOLLOW_9); 

                    				newLeafNode(otherlv_15, grammarAccess.getDoorAccess().getAlarmsKeyword_7_0());
                    			
                    otherlv_16=(Token)match(input,17,FOLLOW_5); 

                    				newLeafNode(otherlv_16, grammarAccess.getDoorAccess().getLeftParenthesisKeyword_7_1());
                    			
                    // InternalBuilding.g:870:4: ( ( ruleEString ) )
                    // InternalBuilding.g:871:5: ( ruleEString )
                    {
                    // InternalBuilding.g:871:5: ( ruleEString )
                    // InternalBuilding.g:872:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDoorRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getDoorAccess().getAlarmsAlarmCrossReference_7_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalBuilding.g:886:4: (otherlv_18= ',' ( ( ruleEString ) ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==18) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalBuilding.g:887:5: otherlv_18= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_18=(Token)match(input,18,FOLLOW_5); 

                    	    					newLeafNode(otherlv_18, grammarAccess.getDoorAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalBuilding.g:891:5: ( ( ruleEString ) )
                    	    // InternalBuilding.g:892:6: ( ruleEString )
                    	    {
                    	    // InternalBuilding.g:892:6: ( ruleEString )
                    	    // InternalBuilding.g:893:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getDoorRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getDoorAccess().getAlarmsAlarmCrossReference_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    otherlv_20=(Token)match(input,19,FOLLOW_11); 

                    				newLeafNode(otherlv_20, grammarAccess.getDoorAccess().getRightParenthesisKeyword_7_4());
                    			

                    }
                    break;

            }

            otherlv_21=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_21, grammarAccess.getDoorAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDoor"


    // $ANTLR start "entryRuleWindow"
    // InternalBuilding.g:921:1: entryRuleWindow returns [EObject current=null] : iv_ruleWindow= ruleWindow EOF ;
    public final EObject entryRuleWindow() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWindow = null;


        try {
            // InternalBuilding.g:921:47: (iv_ruleWindow= ruleWindow EOF )
            // InternalBuilding.g:922:2: iv_ruleWindow= ruleWindow EOF
            {
             newCompositeNode(grammarAccess.getWindowRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWindow=ruleWindow();

            state._fsp--;

             current =iv_ruleWindow; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWindow"


    // $ANTLR start "ruleWindow"
    // InternalBuilding.g:928:1: ruleWindow returns [EObject current=null] : (otherlv_0= 'Window' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'alarms' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? otherlv_15= '}' ) ;
    public final EObject ruleWindow() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_level_4_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:934:2: ( (otherlv_0= 'Window' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'alarms' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? otherlv_15= '}' ) )
            // InternalBuilding.g:935:2: (otherlv_0= 'Window' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'alarms' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? otherlv_15= '}' )
            {
            // InternalBuilding.g:935:2: (otherlv_0= 'Window' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'alarms' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? otherlv_15= '}' )
            // InternalBuilding.g:936:3: otherlv_0= 'Window' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'alarms' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? otherlv_15= '}'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getWindowAccess().getWindowKeyword_0());
            		
            // InternalBuilding.g:940:3: ( (lv_name_1_0= ruleEString ) )
            // InternalBuilding.g:941:4: (lv_name_1_0= ruleEString )
            {
            // InternalBuilding.g:941:4: (lv_name_1_0= ruleEString )
            // InternalBuilding.g:942:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWindowAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWindowRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_21); 

            			newLeafNode(otherlv_2, grammarAccess.getWindowAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalBuilding.g:963:3: (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==27) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalBuilding.g:964:4: otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_17); 

                    				newLeafNode(otherlv_3, grammarAccess.getWindowAccess().getLevelKeyword_3_0());
                    			
                    // InternalBuilding.g:968:4: ( (lv_level_4_0= ruleEInt ) )
                    // InternalBuilding.g:969:5: (lv_level_4_0= ruleEInt )
                    {
                    // InternalBuilding.g:969:5: (lv_level_4_0= ruleEInt )
                    // InternalBuilding.g:970:6: lv_level_4_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getWindowAccess().getLevelEIntParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_22);
                    lv_level_4_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWindowRule());
                    						}
                    						set(
                    							current,
                    							"level",
                    							lv_level_4_0,
                    							"fr.irisa.atsyra.building.xtext.Building.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalBuilding.g:988:3: (otherlv_5= 'inside' ( ( ruleEString ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==30) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalBuilding.g:989:4: otherlv_5= 'inside' ( ( ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,30,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getWindowAccess().getInsideKeyword_4_0());
                    			
                    // InternalBuilding.g:993:4: ( ( ruleEString ) )
                    // InternalBuilding.g:994:5: ( ruleEString )
                    {
                    // InternalBuilding.g:994:5: ( ruleEString )
                    // InternalBuilding.g:995:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getWindowRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getWindowAccess().getInsideZoneCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_23);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalBuilding.g:1010:3: (otherlv_7= 'outside' ( ( ruleEString ) ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==31) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalBuilding.g:1011:4: otherlv_7= 'outside' ( ( ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,31,FOLLOW_5); 

                    				newLeafNode(otherlv_7, grammarAccess.getWindowAccess().getOutsideKeyword_5_0());
                    			
                    // InternalBuilding.g:1015:4: ( ( ruleEString ) )
                    // InternalBuilding.g:1016:5: ( ruleEString )
                    {
                    // InternalBuilding.g:1016:5: ( ruleEString )
                    // InternalBuilding.g:1017:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getWindowRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getWindowAccess().getOutsideZoneCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_8);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalBuilding.g:1032:3: (otherlv_9= 'alarms' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==16) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalBuilding.g:1033:4: otherlv_9= 'alarms' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')'
                    {
                    otherlv_9=(Token)match(input,16,FOLLOW_9); 

                    				newLeafNode(otherlv_9, grammarAccess.getWindowAccess().getAlarmsKeyword_6_0());
                    			
                    otherlv_10=(Token)match(input,17,FOLLOW_5); 

                    				newLeafNode(otherlv_10, grammarAccess.getWindowAccess().getLeftParenthesisKeyword_6_1());
                    			
                    // InternalBuilding.g:1041:4: ( ( ruleEString ) )
                    // InternalBuilding.g:1042:5: ( ruleEString )
                    {
                    // InternalBuilding.g:1042:5: ( ruleEString )
                    // InternalBuilding.g:1043:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getWindowRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getWindowAccess().getAlarmsAlarmCrossReference_6_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalBuilding.g:1057:4: (otherlv_12= ',' ( ( ruleEString ) ) )*
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==18) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // InternalBuilding.g:1058:5: otherlv_12= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_12=(Token)match(input,18,FOLLOW_5); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getWindowAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalBuilding.g:1062:5: ( ( ruleEString ) )
                    	    // InternalBuilding.g:1063:6: ( ruleEString )
                    	    {
                    	    // InternalBuilding.g:1063:6: ( ruleEString )
                    	    // InternalBuilding.g:1064:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getWindowRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getWindowAccess().getAlarmsAlarmCrossReference_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop19;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,19,FOLLOW_11); 

                    				newLeafNode(otherlv_14, grammarAccess.getWindowAccess().getRightParenthesisKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_15=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getWindowAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWindow"


    // $ANTLR start "entryRuleBadged"
    // InternalBuilding.g:1092:1: entryRuleBadged returns [EObject current=null] : iv_ruleBadged= ruleBadged EOF ;
    public final EObject entryRuleBadged() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBadged = null;


        try {
            // InternalBuilding.g:1092:47: (iv_ruleBadged= ruleBadged EOF )
            // InternalBuilding.g:1093:2: iv_ruleBadged= ruleBadged EOF
            {
             newCompositeNode(grammarAccess.getBadgedRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBadged=ruleBadged();

            state._fsp--;

             current =iv_ruleBadged; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBadged"


    // $ANTLR start "ruleBadged"
    // InternalBuilding.g:1099:1: ruleBadged returns [EObject current=null] : (otherlv_0= 'BadgedDoor' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'badges' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}' ) ;
    public final EObject ruleBadged() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_level_4_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:1105:2: ( (otherlv_0= 'BadgedDoor' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'badges' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}' ) )
            // InternalBuilding.g:1106:2: (otherlv_0= 'BadgedDoor' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'badges' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}' )
            {
            // InternalBuilding.g:1106:2: (otherlv_0= 'BadgedDoor' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'badges' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}' )
            // InternalBuilding.g:1107:3: otherlv_0= 'BadgedDoor' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )? (otherlv_5= 'inside' ( ( ruleEString ) ) )? (otherlv_7= 'outside' ( ( ruleEString ) ) )? (otherlv_9= 'badges' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )? otherlv_21= '}'
            {
            otherlv_0=(Token)match(input,32,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getBadgedAccess().getBadgedDoorKeyword_0());
            		
            // InternalBuilding.g:1111:3: ( (lv_name_1_0= ruleEString ) )
            // InternalBuilding.g:1112:4: (lv_name_1_0= ruleEString )
            {
            // InternalBuilding.g:1112:4: (lv_name_1_0= ruleEString )
            // InternalBuilding.g:1113:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getBadgedAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBadgedRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_24); 

            			newLeafNode(otherlv_2, grammarAccess.getBadgedAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalBuilding.g:1134:3: (otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==27) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalBuilding.g:1135:4: otherlv_3= 'level' ( (lv_level_4_0= ruleEInt ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_17); 

                    				newLeafNode(otherlv_3, grammarAccess.getBadgedAccess().getLevelKeyword_3_0());
                    			
                    // InternalBuilding.g:1139:4: ( (lv_level_4_0= ruleEInt ) )
                    // InternalBuilding.g:1140:5: (lv_level_4_0= ruleEInt )
                    {
                    // InternalBuilding.g:1140:5: (lv_level_4_0= ruleEInt )
                    // InternalBuilding.g:1141:6: lv_level_4_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getBadgedAccess().getLevelEIntParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_25);
                    lv_level_4_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBadgedRule());
                    						}
                    						set(
                    							current,
                    							"level",
                    							lv_level_4_0,
                    							"fr.irisa.atsyra.building.xtext.Building.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalBuilding.g:1159:3: (otherlv_5= 'inside' ( ( ruleEString ) ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==30) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalBuilding.g:1160:4: otherlv_5= 'inside' ( ( ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,30,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getBadgedAccess().getInsideKeyword_4_0());
                    			
                    // InternalBuilding.g:1164:4: ( ( ruleEString ) )
                    // InternalBuilding.g:1165:5: ( ruleEString )
                    {
                    // InternalBuilding.g:1165:5: ( ruleEString )
                    // InternalBuilding.g:1166:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBadgedRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getBadgedAccess().getInsideZoneCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_26);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalBuilding.g:1181:3: (otherlv_7= 'outside' ( ( ruleEString ) ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==31) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalBuilding.g:1182:4: otherlv_7= 'outside' ( ( ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,31,FOLLOW_5); 

                    				newLeafNode(otherlv_7, grammarAccess.getBadgedAccess().getOutsideKeyword_5_0());
                    			
                    // InternalBuilding.g:1186:4: ( ( ruleEString ) )
                    // InternalBuilding.g:1187:5: ( ruleEString )
                    {
                    // InternalBuilding.g:1187:5: ( ruleEString )
                    // InternalBuilding.g:1188:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBadgedRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getBadgedAccess().getOutsideZoneCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_27);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalBuilding.g:1203:3: (otherlv_9= 'badges' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==33) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalBuilding.g:1204:4: otherlv_9= 'badges' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')'
                    {
                    otherlv_9=(Token)match(input,33,FOLLOW_9); 

                    				newLeafNode(otherlv_9, grammarAccess.getBadgedAccess().getBadgesKeyword_6_0());
                    			
                    otherlv_10=(Token)match(input,17,FOLLOW_5); 

                    				newLeafNode(otherlv_10, grammarAccess.getBadgedAccess().getLeftParenthesisKeyword_6_1());
                    			
                    // InternalBuilding.g:1212:4: ( ( ruleEString ) )
                    // InternalBuilding.g:1213:5: ( ruleEString )
                    {
                    // InternalBuilding.g:1213:5: ( ruleEString )
                    // InternalBuilding.g:1214:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBadgedRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getBadgedAccess().getBadgesItemCrossReference_6_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalBuilding.g:1228:4: (otherlv_12= ',' ( ( ruleEString ) ) )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( (LA24_0==18) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // InternalBuilding.g:1229:5: otherlv_12= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_12=(Token)match(input,18,FOLLOW_5); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getBadgedAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalBuilding.g:1233:5: ( ( ruleEString ) )
                    	    // InternalBuilding.g:1234:6: ( ruleEString )
                    	    {
                    	    // InternalBuilding.g:1234:6: ( ruleEString )
                    	    // InternalBuilding.g:1235:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getBadgedRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getBadgedAccess().getBadgesItemCrossReference_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,19,FOLLOW_8); 

                    				newLeafNode(otherlv_14, grammarAccess.getBadgedAccess().getRightParenthesisKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalBuilding.g:1255:3: (otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')' )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==16) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalBuilding.g:1256:4: otherlv_15= 'alarms' otherlv_16= '(' ( ( ruleEString ) ) (otherlv_18= ',' ( ( ruleEString ) ) )* otherlv_20= ')'
                    {
                    otherlv_15=(Token)match(input,16,FOLLOW_9); 

                    				newLeafNode(otherlv_15, grammarAccess.getBadgedAccess().getAlarmsKeyword_7_0());
                    			
                    otherlv_16=(Token)match(input,17,FOLLOW_5); 

                    				newLeafNode(otherlv_16, grammarAccess.getBadgedAccess().getLeftParenthesisKeyword_7_1());
                    			
                    // InternalBuilding.g:1264:4: ( ( ruleEString ) )
                    // InternalBuilding.g:1265:5: ( ruleEString )
                    {
                    // InternalBuilding.g:1265:5: ( ruleEString )
                    // InternalBuilding.g:1266:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBadgedRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getBadgedAccess().getAlarmsAlarmCrossReference_7_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalBuilding.g:1280:4: (otherlv_18= ',' ( ( ruleEString ) ) )*
                    loop26:
                    do {
                        int alt26=2;
                        int LA26_0 = input.LA(1);

                        if ( (LA26_0==18) ) {
                            alt26=1;
                        }


                        switch (alt26) {
                    	case 1 :
                    	    // InternalBuilding.g:1281:5: otherlv_18= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_18=(Token)match(input,18,FOLLOW_5); 

                    	    					newLeafNode(otherlv_18, grammarAccess.getBadgedAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalBuilding.g:1285:5: ( ( ruleEString ) )
                    	    // InternalBuilding.g:1286:6: ( ruleEString )
                    	    {
                    	    // InternalBuilding.g:1286:6: ( ruleEString )
                    	    // InternalBuilding.g:1287:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getBadgedRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getBadgedAccess().getAlarmsAlarmCrossReference_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop26;
                        }
                    } while (true);

                    otherlv_20=(Token)match(input,19,FOLLOW_11); 

                    				newLeafNode(otherlv_20, grammarAccess.getBadgedAccess().getRightParenthesisKeyword_7_4());
                    			

                    }
                    break;

            }

            otherlv_21=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_21, grammarAccess.getBadgedAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBadged"


    // $ANTLR start "entryRuleItem"
    // InternalBuilding.g:1315:1: entryRuleItem returns [EObject current=null] : iv_ruleItem= ruleItem EOF ;
    public final EObject entryRuleItem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleItem = null;


        try {
            // InternalBuilding.g:1315:45: (iv_ruleItem= ruleItem EOF )
            // InternalBuilding.g:1316:2: iv_ruleItem= ruleItem EOF
            {
             newCompositeNode(grammarAccess.getItemRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleItem=ruleItem();

            state._fsp--;

             current =iv_ruleItem; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleItem"


    // $ANTLR start "ruleItem"
    // InternalBuilding.g:1322:1: ruleItem returns [EObject current=null] : ( () otherlv_1= 'Item' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleItem() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:1328:2: ( ( () otherlv_1= 'Item' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalBuilding.g:1329:2: ( () otherlv_1= 'Item' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalBuilding.g:1329:2: ( () otherlv_1= 'Item' ( (lv_name_2_0= ruleEString ) ) )
            // InternalBuilding.g:1330:3: () otherlv_1= 'Item' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalBuilding.g:1330:3: ()
            // InternalBuilding.g:1331:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getItemAccess().getItemAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,34,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getItemAccess().getItemKeyword_1());
            		
            // InternalBuilding.g:1341:3: ( (lv_name_2_0= ruleEString ) )
            // InternalBuilding.g:1342:4: (lv_name_2_0= ruleEString )
            {
            // InternalBuilding.g:1342:4: (lv_name_2_0= ruleEString )
            // InternalBuilding.g:1343:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getItemAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getItemRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleItem"


    // $ANTLR start "entryRuleAttacker"
    // InternalBuilding.g:1364:1: entryRuleAttacker returns [EObject current=null] : iv_ruleAttacker= ruleAttacker EOF ;
    public final EObject entryRuleAttacker() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttacker = null;


        try {
            // InternalBuilding.g:1364:49: (iv_ruleAttacker= ruleAttacker EOF )
            // InternalBuilding.g:1365:2: iv_ruleAttacker= ruleAttacker EOF
            {
             newCompositeNode(grammarAccess.getAttackerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttacker=ruleAttacker();

            state._fsp--;

             current =iv_ruleAttacker; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttacker"


    // $ANTLR start "ruleAttacker"
    // InternalBuilding.g:1371:1: ruleAttacker returns [EObject current=null] : ( () otherlv_1= 'Attacker' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'level' ( (lv_level_5_0= ruleEInt ) ) )? otherlv_6= '}' ) ;
    public final EObject ruleAttacker() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_level_5_0 = null;



        	enterRule();

        try {
            // InternalBuilding.g:1377:2: ( ( () otherlv_1= 'Attacker' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'level' ( (lv_level_5_0= ruleEInt ) ) )? otherlv_6= '}' ) )
            // InternalBuilding.g:1378:2: ( () otherlv_1= 'Attacker' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'level' ( (lv_level_5_0= ruleEInt ) ) )? otherlv_6= '}' )
            {
            // InternalBuilding.g:1378:2: ( () otherlv_1= 'Attacker' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'level' ( (lv_level_5_0= ruleEInt ) ) )? otherlv_6= '}' )
            // InternalBuilding.g:1379:3: () otherlv_1= 'Attacker' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'level' ( (lv_level_5_0= ruleEInt ) ) )? otherlv_6= '}'
            {
            // InternalBuilding.g:1379:3: ()
            // InternalBuilding.g:1380:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAttackerAccess().getAttackerAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,35,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getAttackerAccess().getAttackerKeyword_1());
            		
            // InternalBuilding.g:1390:3: ( (lv_name_2_0= ruleEString ) )
            // InternalBuilding.g:1391:4: (lv_name_2_0= ruleEString )
            {
            // InternalBuilding.g:1391:4: (lv_name_2_0= ruleEString )
            // InternalBuilding.g:1392:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAttackerAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAttackerRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.irisa.atsyra.building.xtext.Building.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_28); 

            			newLeafNode(otherlv_3, grammarAccess.getAttackerAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalBuilding.g:1413:3: (otherlv_4= 'level' ( (lv_level_5_0= ruleEInt ) ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==27) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalBuilding.g:1414:4: otherlv_4= 'level' ( (lv_level_5_0= ruleEInt ) )
                    {
                    otherlv_4=(Token)match(input,27,FOLLOW_17); 

                    				newLeafNode(otherlv_4, grammarAccess.getAttackerAccess().getLevelKeyword_4_0());
                    			
                    // InternalBuilding.g:1418:4: ( (lv_level_5_0= ruleEInt ) )
                    // InternalBuilding.g:1419:5: (lv_level_5_0= ruleEInt )
                    {
                    // InternalBuilding.g:1419:5: (lv_level_5_0= ruleEInt )
                    // InternalBuilding.g:1420:6: lv_level_5_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getAttackerAccess().getLevelEIntParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_level_5_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttackerRule());
                    						}
                    						set(
                    							current,
                    							"level",
                    							lv_level_5_0,
                    							"fr.irisa.atsyra.building.xtext.Building.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getAttackerAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttacker"


    // $ANTLR start "entryRuleEInt"
    // InternalBuilding.g:1446:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalBuilding.g:1446:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalBuilding.g:1447:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalBuilding.g:1453:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalBuilding.g:1459:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalBuilding.g:1460:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalBuilding.g:1460:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalBuilding.g:1461:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalBuilding.g:1461:3: (kw= '-' )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==36) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalBuilding.g:1462:4: kw= '-'
                    {
                    kw=(Token)match(input,36,FOLLOW_29); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000D2450C000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000014000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000204000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000001B014000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000001000000040L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000013014000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000012014000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000010014000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x00000000C8014000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x00000000C0014000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000080014000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x00000002C8014000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x00000002C0014000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000280014000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000200014000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000008004000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000040L});

}