/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/*
 * generated by Xtext 2.12.0
 */
package fr.irisa.atsyra.building.xtext.scoping;

import org.eclipse.xtext.scoping.impl.DelegatingScopeProvider;

public abstract class AbstractBuildingScopeProvider extends DelegatingScopeProvider {
}
