/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.k3dsa;

import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystemManager;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class Activator implements BundleActivator {

	// The shared instance
	private static Activator plugin;
	
	protected MessagingSystem messagingSystem = null;
		
	@Override
	public void start(BundleContext context) throws Exception {
		plugin = this;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
	}
	
	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	public static final String baseMsgGroup = "fr.irisa.atsyra";
	public static final String msgGroup = baseMsgGroup+".ide.ui";
	
	public MessagingSystem getMessaggingSystem() {
		if (messagingSystem == null) {
			MessagingSystemManager msm = new MessagingSystemManager();
			// reuse messaging system from the Modeling Workbench Engine
			 messagingSystem = msm.createBestPlatformMessagingSystem(
					 baseMsgGroup,
	                    "ATSyRA");
		}
		return messagingSystem;
	}

	public static void debug(String msg) {
		if (getDefault() != null)
			getDefault().getMessaggingSystem().debug(msg, msgGroup);
	}
	public static void error(String msg) {
		if (getDefault() != null)
			getDefault().getMessaggingSystem().error(msg, msgGroup);
	}
	public static void error(String msg, Exception e) {
		if (getDefault() != null)
			getDefault().getMessaggingSystem().error(msg, msgGroup, e);
	}
	public static void warn(String msg) {
		if (getDefault() != null)
			getDefault().getMessaggingSystem().warn(msg, msgGroup);
	}
	public static void warn(String msg, Exception e) {
		if (getDefault() != null)
			getDefault().getMessaggingSystem().warn(msg, msgGroup, e);
	}
	
	public static String readConsoleLine(String msg) {
		if (getDefault() != null)
			return getDefault().getMessaggingSystem().readLine(msg);
		else
			return "";
	}

}
