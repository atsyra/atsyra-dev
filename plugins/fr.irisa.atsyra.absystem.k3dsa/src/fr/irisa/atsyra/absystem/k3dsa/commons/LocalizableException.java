/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.k3dsa.commons;

import java.util.Locale;

import org.eclipse.emf.common.util.ResourceLocator;

import fr.irisa.atsyra.absystem.model.absystem.util.ResourceBundleLocatorProvider;

public abstract class LocalizableException extends Exception {
	private final String messageKey;
	private final String[] substitutions;

	protected LocalizableException(String messageKey) {
		super(getString(messageKey, Locale.getDefault()));
		this.messageKey = messageKey;
		this.substitutions = null;
	}
	
	protected LocalizableException(String messageKey, String[] substitutions) {
		super(getString(messageKey, substitutions, Locale.getDefault()));
		this.messageKey = messageKey;
		this.substitutions = substitutions;
	}
	
	@Override
	public String getLocalizedMessage() {
		return getLocalizedMessage(Locale.getDefault());
	}
	
	public String getLocalizedMessage(Locale locale) {
		return substitutions == null ? getString(messageKey, locale) : getString(messageKey, substitutions, locale);
	}
	
	private static final String RESOURCES_FR_IRISA_ATSYRA_ABSYSTEM_K3DSA_COMMONS_EXCEPTIONS = "resources/Exceptions";
	
	protected static String getString(String key, Locale locale) {
		ResourceLocator resourceLocator = ResourceBundleLocatorProvider.getResourceBundleLocator(RESOURCES_FR_IRISA_ATSYRA_ABSYSTEM_K3DSA_COMMONS_EXCEPTIONS, locale, LocalizableException.class.getModule());
		return resourceLocator.getString(key);
	}
	
	protected static String getString(String key, Object[] substitutions, Locale locale) {
		ResourceLocator resourceLocator = ResourceBundleLocatorProvider.getResourceBundleLocator(RESOURCES_FR_IRISA_ATSYRA_ABSYSTEM_K3DSA_COMMONS_EXCEPTIONS, locale, LocalizableException.class.getModule());
		return resourceLocator.getString(key, substitutions);
	}
}
