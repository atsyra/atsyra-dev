package fr.irisa.atsyra.absystem.k3dsa.absystem.aspects

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.absystem.model.absystem.AssetType
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeReference
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.Asset
import fr.irisa.atsyra.absystem.model.absystem.AssetLink
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAttribute
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature
import fr.irisa.atsyra.absystem.model.absystem.PrimitiveDataType
import fr.irisa.atsyra.absystem.model.absystem.EnumDataType
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral
import fr.irisa.atsyra.absystem.model.absystem.AssetAttributeValue
import fr.irisa.atsyra.absystem.model.absystem.DefinitionGroup
import fr.irisa.atsyra.absystem.model.absystem.AssetGroup
import fr.irisa.atsyra.absystem.model.absystem.Import
import fr.irisa.atsyra.absystem.model.absystem.Tag
import fr.irisa.atsyra.absystem.model.absystem.AbstractAssetType
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeAspect
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter
import fr.irisa.atsyra.absystem.model.absystem.Member
import fr.irisa.atsyra.absystem.model.absystem.Symbol
import fr.irisa.atsyra.absystem.model.absystem.StaticMethod

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeReferenceK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetBasedSystemK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetLinkK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAttributeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeFeatureK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.PrimitiveDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumLiteralK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetAttributeValueK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.DefinitionGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ImportK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.TagK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AbstractAssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAspectK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardedActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ContractK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardParameterK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BinaryExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.OrExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AndExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.NotExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EqualityComparisonExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ConstantExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StringConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.IntConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BooleanConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberSelectionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.LambdaExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolRefK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StaticMethodK3Aspect.*

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.GuardOccurenceK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.AssetArgumentK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.AssetValueK3Aspect.*

import static extension fr.irisa.atsyra.absystem.k3dsa.ecore.aspects.EObjectAspect.*

import fr.inria.diverse.k3.al.annotationprocessor.Main
import org.eclipse.emf.common.util.EList
import fr.inria.diverse.k3.al.annotationprocessor.InitializeModel
import fr.inria.diverse.k3.al.annotationprocessor.Step
import java.util.List
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence
import java.util.HashMap
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmFactory
import java.util.ArrayList
import fr.irisa.atsyra.absystem.k3dsa.commons.NotImplementedException
import java.util.Properties
import java.io.IOException
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.CoreException
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils
import fr.irisa.atsyra.absystem.k3dsa.commons.InvalidConfigurationException
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurenceArgument
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Value
import fr.irisa.atsyra.absystem.k3dsa.commons.AssetBasedSystemException
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetValue
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ListValue
import fr.irisa.atsyra.absystem.model.absystem.Parameter
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression
import fr.irisa.atsyra.absystem.model.absystem.Multiplicity
import fr.irisa.atsyra.absystem.model.absystem.Guard
import fr.irisa.atsyra.absystem.model.absystem.Contract
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.UndefinedValue
import fr.irisa.atsyra.absystem.k3dsa.commons.UndefinedReceiverException
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils
import java.util.Set
import fr.inria.diverse.k3.al.annotationprocessor.OverrideAspectMethod
import fr.irisa.atsyra.absystem.model.absystem.AbsystemPackage
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant
import java.util.Arrays
import java.lang.reflect.Array

@Aspect(className=AssetBasedSystem)
class AssetBasedSystemK3Aspect {

	//public List<GuardedActionOccurence> possibleGuardedActions;
	//public List<Asset> allAssets; 
	//public List<AssetLink> allStaticAssetLinks
	//public List<GuardedActionOccurence> appliedActions = newArrayList
	
	/**
	 * use test and prevent multiple call to  initializeModel
	 * normally multiple calls is ok, but I'm not sure that it will not create duplicated 
	 */
	public boolean hasBeenInitialized 

	@Step 												
	@InitializeModel									
	def void initializeModel(EList<String> input) throws InvalidConfigurationException {
		_self.devInfo('-> initializeModel()');
		if(input !== null) {
			for(i : input){
				_self.devInfo('\tinput='+i);
			}
		}
		
		_self.allAssets.addAll(_self.eResource.resourceSet.allContents.filter(Asset).toList)
		val assetLinkList = _self.eResource.resourceSet.allContents.filter(AssetLink).toList
		// generate opposite static AssetLink when defined in the AssetLink
		val List<AssetLink> generatedOppositeAssetLinkList = newArrayList
		for (assetLink : assetLinkList) {
			if(assetLink.oppositeReferenceType !== null) {
				val generatedOppositeAssetLink = AbsystemPackage.eINSTANCE.absystemFactory.createAssetLink
				generatedOppositeAssetLink.sourceAsset = assetLink.targetAsset
				generatedOppositeAssetLink.targetAsset = assetLink.sourceAsset
				generatedOppositeAssetLink.referenceType = assetLink.oppositeReferenceType
				generatedOppositeAssetLink.oppositeReferenceType = assetLink.referenceType
				generatedOppositeAssetLinkList.add(generatedOppositeAssetLink)
				// INFO: this generatedOppositeAssetLink isn't added to a resource 
			}
		}
		// populate _self.allStaticAssetLinks with AssetLink and generatedOppositeAssetLinkList
		_self.allStaticAssetLinks.addAll(assetLinkList)
		_self.allStaticAssetLinks.addAll(generatedOppositeAssetLinkList)
		
		
		_self.populatePossibleGuardedActions()
		_self.computeAllSubtypes()
		//_self.printCurrentState()
		// initialize data structure according to aspect References and use default attribute values defined in the model
		_self.initRuntimeDataStructure()
		//_self.printCurrentState()
		// initialize runtime data according to initial state passed as parameter 
		if(input.size > 0){
			_self.initRuntimeData(input.get(0))	
		} 
		_self.hasBeenInitialized = true
	}
	@Main
	@Step
	def void main() {
		_self.devInfo('-> main() ');	
		
		_self.printDebug()	
		_self.printCurrentState()
	
		_self.printCurrentStateSummary()
		var List<GuardOccurence> applicableActionOccurence = _self.findApplicableGuardedActionOccurence()
		var GuardOccurence nextAction
		nextAction = _self.askNextAction(applicableActionOccurence)
		while (nextAction !== null) {
			// apply action
			_self.important('''Applying «nextAction.toUserString»...''')
			
			_self.appliedActions.add(nextAction) // store applied actions first in order to be able to get the info in the animation
			nextAction.applyAction(_self)
			
			_self.printCurrentStateSummary()
			applicableActionOccurence = _self.findApplicableGuardedActionOccurence()
			nextAction = _self.askNextAction(applicableActionOccurence)
		}
		
		_self.saveAppliedActionScenario()
		
	}
	
	def GuardOccurence askNextAction(List<GuardOccurence> applicableActionOccurence){
		_self.important('Currently applicable action occurrence(s): ('+applicableActionOccurence.size+')')
		applicableActionOccurence.forEach[action, i| 
			_self.important('''   («i+1») «action.toUserString»''')
		]
		val inputString = _self.readConsoleLine("(enter next action index)")
		try {
			val input = Integer.parseInt(inputString)-1
			if(input >= applicableActionOccurence.size || input < 0){
				_self.error('''«inputString» is out of range''')
				return null
			}
			return applicableActionOccurence.get(input)
		} catch (NumberFormatException e) {
			return null
		}
	}
	
	def void saveAppliedActionScenario() {
		_self.important("Executed action summary:")	
		for(action : _self.appliedActions) {
			_self.important('''«action.toUserString»''')	
		}
		// TODO save in the SpecificExecutionFolder cf. org.eclipse.gemoc.executionframework.engine.coreExecutionWorkspace must be an engine addon ?
		// or in another place ?
	}
	
	def void populatePossibleGuardedActions(){
		val actionList = _self.eResource.resourceSet.allContents.filter(GuardedAction).toList
		
		//_self.possibleGuardedActions = newArrayList
		
		for(action : actionList){
			// create all possible call for this action ...
			_self.possibleGuardedActions.addAll(action.generateAllPossibleGuardOccurence)
		}
	}
	
	/**
	 * Compute a list of all contract occurrences
	 * filter only static contracts or dynamic contract
	 */
	def List<GuardOccurence> getAllPossibleContractOccurences(boolean dynamic) {
		val List<GuardOccurence> possibleContractOccurences = newArrayList
		val contractList = _self.eResource.resourceSet.allContents.filter(Contract).filter [ c |
			c.guardExpression !== null && !(c.guardExpression instanceof UndefinedConstant)
		].filter[c|c.dynamic == dynamic].toList
		
		for(contract : contractList){
			// create all possible call for this contract in the model ...
			possibleContractOccurences.addAll(contract.generateAllPossibleGuardOccurence)
		}
		return possibleContractOccurences
	}
	
	def void computeAllSubtypes(){
		_self.eResource.resourceSet.allContents.filter(AssetType).forEach[
			it.computeSubtypes()
		]
	}
	
	def void initRuntimeDataStructure() {
		val allAspects = _self.eResource.resourceSet.allContents.filter(AssetTypeAspect).toList
		for(aspect : allAspects) {
			for(asset : _self.allAssets) {
				if(aspect.baseAssetType.isCompatibleAsset(asset)) {
					for(dynamicReference : aspect.assetTypeProperties){
						if(dynamicReference.multiplicity == Multiplicity.ZERO_OR_MANY || dynamicReference.multiplicity == Multiplicity.ONE_OR_MANY){
							// empty CollectionValue
							asset.featuresMapPut(dynamicReference, Interpreter_vmFactory.eINSTANCE.createListValue)
						} else {
							// put a Void/null Value
							asset.featuresMapPut(dynamicReference, null)
						}
					}
					for(dynamicAttribute : aspect.assetTypeAttributes){
						if(dynamicAttribute.multiplicity == Multiplicity.ZERO_OR_MANY || dynamicAttribute.multiplicity == Multiplicity.ONE_OR_MANY){
							// empty CollectionValue
							val l = Interpreter_vmFactory.eINSTANCE.createListValue
							asset.featuresMapPut(dynamicAttribute, l)
							// fill the collection with default values
							dynamicAttribute.defaultValues.forEach[defValue | 
								l.ownedValues.add( defValue.evalExpression(_self, null))
							]
						} else {
							if(dynamicAttribute.defaultValues.empty) {
								// put a Void/null Value
								asset.featuresMapPut(dynamicAttribute, null)
							} else {
								asset.featuresMapPut(dynamicAttribute,
									dynamicAttribute.defaultValues.get(0).evalExpression(_self, null))
							}
						}
					}
				}
			}
		}
		// TODO detect runtime ref/attribute due to usage (ie. part of an action)
		_self.devWarn("Need to implement: detect runtime ref/attribute due to usage (ie. left part of an action) ")
		//throw new NotImplementedException('AssetBasedSystemK3Aspect.initRuntimeDataStructure()')
	}
	def void initRuntimeData(String propertyFilePath) throws InvalidConfigurationException {
		try {
			if(!propertyFilePath.nullOrEmpty) {
				val IFile propertiesFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(propertyFilePath);
				if (propertiesFile.exists()) {
	
					// read property file and collect its content in a more convenient structure
					val Properties prop = new Properties();
					try {
						prop.load(propertiesFile.getContents());
						for (Object k : prop.keySet()) {
							val key = k as String
							val keyTokens = key.split('\\.')
							val String  assetName = keyTokens.get(0)
							// find asset
							val asset = _self.allAssets.findFirst[anAsset | anAsset.name == assetName]
							if(asset === null) {
								throw new InvalidConfigurationException(InvalidConfigurationException.MISSING_ASSET_FOR_INITIALIZATION, #[assetName, propertyFilePath])
							}
							// find dynamic reference or propoerty
							val String  refPropName = keyTokens.get(1)
							val featureEntry = asset.featuresMap.findFirst[kref | kref.key.name == refPropName]
							if( featureEntry !== null) {
								// assign values
								featureEntry.key.assignFromString(_self, asset, prop.getProperty(key))
							} else {	
								_self.error("cannot find dynamic attribute or dynamic reference "+refPropName+ " in asset "+assetName + " for initialization file "+propertyFilePath)
								_self.error("known dynamic features are: " +
									'''«FOR f : asset.featuresMap SEPARATOR ',' »«f.key»«ENDFOR»;'''
								)
								throw new InvalidConfigurationException(InvalidConfigurationException.MISSING_DYNAMIC_FEATURE_FOR_INITIALIZATION, #[refPropName, assetName, propertyFilePath])
							}
						}
					} catch (IOException e) {
						_self.warn("failed to load " + propertiesFile.getLocation() + " " + e.getMessage(), e);
					}
	
				} else {
					_self.warn("properties file " + propertyFilePath + " not found");
				}
			}
		} catch (CoreException e) {
			_self.warn("properties file not found, " + e.getMessage(), e);
		
		}
	}
	
	def List<GuardOccurence> findApplicableGuardedActionOccurence() {
		val res = newArrayList
		for(gao : _self.possibleGuardedActions) {
			if(gao.isApplicableAction(_self)){
				res.add(gao)
			}
		}
		return res
	}
	
	def void printCurrentState() {
		_self.info('Current state:')
		_self.allAssets.forEach[asset | _self.info(asset.runtimeDataToString)]
	}
	def void printCurrentStateSummary() {
		_self.info('Current runtime state:')
		_self.allAssets.forEach[asset | 
			if(!asset.featuresMap.empty) {
				_self.info(asset.runtimeDataToString)
			}
		]
	}
	
	def void printDebug() {
		_self.devInfo('list AssetGroup in current resource')
		for(asGrp : _self.assetGroups){
			_self.devInfo(' '+asGrp.name)
		}
		_self.devInfo('list all AssetGroup in resourceSet')
		for(asGrp : _self.eResource.resourceSet.allContents.filter(AssetGroup).toList){
			_self.devInfo(' '+asGrp.name)
		}
		_self.devInfo('list DefinitionGroup in current resource')
		for(defGrp : _self.definitionGroups){
			_self.devInfo(' '+defGrp.name)
		}
		_self.devInfo('list all DefinitionGroup in resourceSet')
		for(asGrp : _self.eResource.resourceSet.allContents.filter(DefinitionGroup).toList){
			_self.devInfo(' '+asGrp.name)
		}
		_self.devInfo('list all GuardedAction in resourceSet')
		for(gAction : _self.eResource.resourceSet.allContents.filter(GuardedAction).toList){
			_self.devInfo(' '+gAction.qualifiedName)
		}
		_self.devInfo('list all Asset in resourceSet')
		for(asset : _self.eResource.resourceSet.allContents.filter(Asset).toList){
			_self.devInfo(' '+asset.qualifiedName)
		}
		
		_self.devInfo('list all possibleGuardedActions for model assets: ('+_self.possibleGuardedActions.size+')')
		for(pga : _self.possibleGuardedActions){
			_self.devInfo(pga.toUserString)
		}
	}
	
}

@Aspect(className=AssetType)
class AssetTypeK3Aspect extends AbstractAssetTypeK3Aspect {
	Set<AssetType> subtypes = newHashSet
	
	def void computeSubtypes() {
		_self.subtypes = ABSUtils.getAllSubtypes(_self)
	}
	
	def boolean isCompatibleAsset(Asset asset){
		return _self.subtypes.contains(asset.assetType)
	}
	
	def List<Asset> allCompatibleAssets() {
		
		return ABSUtils.getAllSubtypedAssets(_self).toList
	}
}

@Aspect(className=AssetTypeReference)
class AssetTypeReferenceK3Aspect extends AssetTypeFeatureK3Aspect {
	@OverrideAspectMethod
	def void assignFromString(AssetBasedSystem mainModel, Asset receiverAsset, String rawString) throws InvalidConfigurationException {
		val featureVal = receiverAsset.featuresMapGet(_self)
		if(_self.multiplicity == Multiplicity.ZERO_OR_MANY || _self.multiplicity == Multiplicity.ONE_OR_MANY) {
			// clear the list before filling with new values
			(featureVal as ListValue).ownedValues.clear
			for(anAssetNameValue : rawString.split(",").map[s | s.trim]){
				val tartgetAsset = mainModel.allAssets.findFirst[anAsset | anAsset.name == anAssetNameValue]
				if(tartgetAsset === null){
					throw new InvalidConfigurationException("_exception__missing_asset_for_reference", #[anAssetNameValue])
				}
				(featureVal as ListValue).ownedValues.add(Interpreter_vmFactory.eINSTANCE.createAssetValue.init(tartgetAsset))
			}
		} else {
			val tartgetAsset = mainModel.allAssets.findFirst[anAsset | anAsset.name == rawString.trim]
			receiverAsset.featuresMapPut(_self, Interpreter_vmFactory.eINSTANCE.createAssetValue.init(tartgetAsset))
		}
	}
	
	@OverrideAspectMethod
	def Value evalMember(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap, Value receiverValue, List<Value> evaluatedMemberSelectionArgs) throws InvalidConfigurationException, AssetBasedSystemException,UndefinedReceiverException {
		// receiver must be an asset
		if(receiverValue instanceof AssetValue){
			// get value from featureMap
			val dynamicValue = receiverValue.assetValue.featuresMapGet(_self)
			if(dynamicValue !== null) {
				return dynamicValue
			} else {
				// check values in static reference
				if(_self.multiplicity == Multiplicity.ZERO_OR_MANY || _self.multiplicity == Multiplicity.ONE_OR_MANY) {
					// return a collection
					// TODO can we avoid creating the list on every eval ?
					val res = Interpreter_vmFactory.eINSTANCE.createListValue
					res.ownedValues.addAll(mainModel.allStaticAssetLinks.filter[link | link.sourceAsset == receiverValue.assetValue && link.referenceType == _self ].map[
						link2 | Interpreter_vmFactory.eINSTANCE.createAssetValue.init(link2.targetAsset)
					])
					return res
				} else {
					// return the first asset link
					val linkList = mainModel.allStaticAssetLinks.filter[link | link.sourceAsset == receiverValue.assetValue && link.referenceType == _self ]
					if(linkList.empty) {
						return Interpreter_vmFactory.eINSTANCE.createUndefinedValue
					} else if (linkList.size == 1){
						return Interpreter_vmFactory.eINSTANCE.createAssetValue.init(linkList.get(0).targetAsset)
					} else {
						// if we find more there is an error in the model !
						_self.error("Invalid model, multiple links for "+_self.qualifiedName+ " feature that accept only one, taking into account only the first one")
						return Interpreter_vmFactory.eINSTANCE.createAssetValue.init(linkList.get(0).targetAsset)
					}
				}
			}
		} else if(receiverValue instanceof UndefinedValue) {
			throw new UndefinedReceiverException();
		} else {
			throw new AssetBasedSystemException(AssetBasedSystemException.RECEIVER_IS_NOT_AN_ASSET, #[receiverValue.toUserString, _self.name]);
		}
	}
}

@Aspect(className=AssetTypeAttribute)
class AssetTypeAttributeK3Aspect extends AssetTypeFeatureK3Aspect {
	@OverrideAspectMethod
	def void assignFromString(AssetBasedSystem mainModel, Asset receiverAsset, String rawString) throws InvalidConfigurationException {
		val featureVal = receiverAsset.featuresMapGet(_self)
		if(_self.multiplicity == Multiplicity.ZERO_OR_MANY || _self.multiplicity == Multiplicity.ONE_OR_MANY) {
			// clear the list before filling with new values
			(featureVal as ListValue).ownedValues.clear()
			(featureVal as ListValue).ownedValues.add(_self.attributeType.createValueForRawString(rawString))
		} else {
			receiverAsset.featuresMapPut(_self, _self.attributeType.createValueForRawString(rawString))
		}
	}
	
	def void assignValue(AssetBasedSystem mainModel, Asset receiverAsset, Value newValue) {
		if(_self.multiplicity == Multiplicity.ZERO_OR_MANY || _self.multiplicity == Multiplicity.ONE_OR_MANY) {
			throw new NotImplementedException('not implemented, please implement assignValue() for '+_self );
		} else {
			receiverAsset.featuresMapPut(_self, newValue)
		}
	}
	
	@OverrideAspectMethod
	def Value evalMember(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap, Value receiverValue, List<Value> evaluatedMemberSelectionArgs) throws InvalidConfigurationException,AssetBasedSystemException,UndefinedReceiverException {
		// receiver must be an asset
		if(receiverValue instanceof AssetValue){
			// get value from featureMap
			val dynamicValue = receiverValue.assetValue.featuresMapGet(_self)
			if(dynamicValue !== null) {
				return dynamicValue
			} else {
				// check values in static Attribute
				if (_self.multiplicity == Multiplicity.ONE_OR_MANY || _self.multiplicity == Multiplicity.ZERO_OR_MANY) {
					val res = Interpreter_vmFactory.eINSTANCE.createListValue
					if (receiverValue.assetValue.assetAttributeValues.exists[it.attributeType == _self]) {
						res.ownedValues.addAll(receiverValue.assetValue.assetAttributeValues.filter[it.attributeType == _self].flatMap[it.values.map[evalExpression(mainModel, argumentMap)]])
					} else if (!_self.defaultValues.empty) {
						// use default value if defined
						for (element : _self.defaultValues) {
							res.ownedValues.add(element.evalExpression(mainModel, argumentMap))
						}
					}
					return res
				} else {
					val attributeStaticValue = receiverValue.assetValue.assetAttributeValues.findFirst [ av |
						av.attributeType == _self
					]
					_self.devInfo("" + attributeStaticValue)
					if (attributeStaticValue !== null) {
						return attributeStaticValue.values.get(0).evalExpression(mainModel, argumentMap)
					} else if (!_self.defaultValues.empty) {
						// use default value if defined
						return _self.defaultValues.get(0).evalExpression(mainModel, argumentMap)
					} else {
						throw new InvalidConfigurationException(InvalidConfigurationException.MISSING_STATIC_ATTRIBUTE_FOR_INITIALIZATION,  #[_self.name, ABSUtils.getName(_self.eContainer).orElse("unnamed EObject")])
					}
				}
			}
		} else if(receiverValue instanceof UndefinedValue) {
			throw new UndefinedReceiverException();
		} else {
			throw new AssetBasedSystemException(AssetBasedSystemException.RECEIVER_IS_NOT_AN_ASSET, #[receiverValue.toUserString, _self.name]);
		}
	}
}

@Aspect(className=AssetTypeFeature)
abstract class AssetTypeFeatureK3Aspect extends MemberK3Aspect {
	/**
	 * Assign the feature for the given asset by parsing the rawString
	 * used by properties file initialization
	 */
	def void assignFromString(AssetBasedSystem mainModel, Asset receiverAsset, String rawString) throws InvalidConfigurationException {
		throw new NotImplementedException('not implemented, please implement assignFromString() for '+_self );
	}
	

}

@Aspect(className=PrimitiveDataType)
class PrimitiveDataTypeK3Aspect {

	def Value createValueForRawString(String rawString) {
		switch (_self.name) {
			case "String": {
				val sValue =Interpreter_vmFactory.eINSTANCE.createStringValue
				sValue.stringValue = rawString
				return sValue
			}
			case "Boolean": {
				val bValue =Interpreter_vmFactory.eINSTANCE.createBooleanValue
				bValue.booleanValue = rawString.equalsIgnoreCase("true")
				return bValue
			}
			case "Integer": {
				val iValue =Interpreter_vmFactory.eINSTANCE.createIntegerValue
				iValue.intValue = Integer.parseInt(rawString)
				return iValue
			}
			default: {
				return null
			}
		}
	}
}

@Aspect(className=EnumDataType)
class EnumDataTypeK3Aspect extends PrimitiveDataTypeK3Aspect {

}

@Aspect(className=EnumLiteral)
class EnumLiteralK3Aspect {

}

@Aspect(className=AssetAttributeValue)
class AssetAttributeValueK3Aspect {

}

@Aspect(className=DefinitionGroup)
class DefinitionGroupK3Aspect {

}

@Aspect(className=AssetGroup)
class AssetGroupK3Aspect {

}

@Aspect(className=Import)
class ImportK3Aspect {

}

@Aspect(className=Tag)
class TagK3Aspect {

}

@Aspect(className=AbstractAssetType)
abstract class AbstractAssetTypeK3Aspect {

}

@Aspect(className=AssetTypeAspect)
class AssetTypeAspectK3Aspect extends AbstractAssetTypeK3Aspect {

}

@Aspect(className=Guard)
class GuardK3Aspect {
	def List<GuardOccurence> generateAllPossibleGuardOccurence() {
		var HashMap<Parameter, List<Asset>> possibleArguments = newHashMap
		for(param : _self.guardParameters){
			val assetList = param.parameterType.allCompatibleAssets
			if(assetList === null || assetList.size == 0) {
				// no solution for this action, return empty list
				return new ArrayList<GuardOccurence>()
			}
			possibleArguments.put(param, assetList)
		}
		// has at least one solution for each param
		// create all combinaison ...
		return _self.generateAllPossibleGuardOccurence(possibleArguments)
	}
	
	// recursion
	def List<GuardOccurence> generateAllPossibleGuardOccurence(HashMap<Parameter, List<Asset>> possibleArguments) {
		val List<GuardOccurence> result = newArrayList
		if(_self.hasUniquePossibleArguments(possibleArguments)) {
			// if only one asset possible for each param then generate GuardedActionOccurence
			result.add(_self.createGuardOccurence(_self.getFirstPossibleArguments(possibleArguments)))
		} else {
			// get first param with multiple choice
			val aParampWithMultiChoice = possibleArguments.keySet.findFirst[k | possibleArguments.get(k).size > 1 ]
			// split in 2 by removing 1 possible argument in a param that has more than one choice
			val leftSplit = new HashMap<Parameter, List<Asset>>()
			val rightSplit = new HashMap<Parameter, List<Asset>>()
			for(guardedActionParameter : possibleArguments.keySet){
				val listToSplit = possibleArguments.get(guardedActionParameter)
				if(guardedActionParameter == aParampWithMultiChoice) {	
					leftSplit.put(guardedActionParameter, #[listToSplit.get(0)])
					rightSplit.put(guardedActionParameter, listToSplit.subList(1, listToSplit.size))
				} else {
					leftSplit.put(guardedActionParameter, listToSplit.clone)
					rightSplit.put(guardedActionParameter, listToSplit.clone)
				}
			}	
			// recurse call for each split and collect result
			result.addAll(_self.generateAllPossibleGuardOccurence(leftSplit))
			result.addAll(_self.generateAllPossibleGuardOccurence(rightSplit))
		}
		return result
	}
	
	def boolean hasUniquePossibleArguments(HashMap<Parameter, List<Asset>> possibleArguments) {
		return ! possibleArguments.keySet.exists[k | possibleArguments.get(k).size > 1 ]
	}
	
	// return the first possible argument for each parameter
	// requires that one solution exist (
	def List<Asset> getFirstPossibleArguments(HashMap<Parameter, List<Asset>> possibleArguments) {
		return _self.guardParameters.map[p | possibleArguments.get(p).get(0)]
	}
	
	
	def Parameter getFirstParameterWithMultipleChoice(HashMap<Parameter, List<Asset>> possibleArguments) {
		return possibleArguments.keySet.findFirst[k | possibleArguments.get(k).size > 1]
	}
	
	def GuardOccurence createGuardOccurence(List<Asset> arguments) {
		val guardOccurence = Interpreter_vmFactory.eINSTANCE.createGuardOccurence()
		guardOccurence.guard = _self
		for(arg : arguments){
			val assetArg = Interpreter_vmFactory.eINSTANCE.createAssetArgument
			assetArg.asset = arg
			guardOccurence.guardOccurenceArguments.add(assetArg)
		}
		return guardOccurence
	}
}

@Aspect(className=GuardedAction)
class GuardedActionK3Aspect extends GuardK3Aspect {

}

@Aspect(className=Contract)
class ContractK3Aspect extends GuardK3Aspect {

}

@Aspect(className=GuardParameter)
class GuardParameterK3Aspect extends SymbolK3Aspect {

}



@Aspect(className=Member)
abstract class MemberK3Aspect {
	def Value evalMember(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap, Value receiverValue, List<Value> evaluatedMemberSelectionArgs) throws InvalidConfigurationException, AssetBasedSystemException, UndefinedReceiverException {
		throw new NotImplementedException('not implemented, please implement evalMember() for '+_self);
	}
	def Value evalLambdaMember(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap, Value receiverValue, LambdaExpression lambda) throws AssetBasedSystemException,UndefinedReceiverException {
		throw new NotImplementedException('not implemented, please implement evalLambdaMember() for '+_self);
	}
}
@Aspect(className=Symbol)
abstract class SymbolK3Aspect {

}

@Aspect(className=StaticMethod)
class StaticMethodK3Aspect extends MemberK3Aspect {
	@OverrideAspectMethod
	def Value evalMember(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap, Value receiverValue, List<Value> evaluatedMemberSelectionArgs) throws InvalidConfigurationException, AssetBasedSystemException, UndefinedReceiverException {
		// receiver must be an asset
		
		if(receiverValue instanceof ListValue){
			
			switch (_self.name) {
				case "contains": {
					if(evaluatedMemberSelectionArgs.size != 1) throw new AssetBasedSystemException(AssetBasedSystemException.INVALID_NUMBER_OF_ARGUMENTS, #[_self.name]);
					val result  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
					result.booleanValue = receiverValue.ownedValues.exists[oVal | oVal.bEquals(evaluatedMemberSelectionArgs.get(0))]
					return result
				} 
				case "containsAny": {
					if(evaluatedMemberSelectionArgs.size < 1) throw new AssetBasedSystemException(AssetBasedSystemException.INVALID_NUMBER_OF_ARGUMENTS, #[_self.name]);
					val result  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
					result.booleanValue = evaluatedMemberSelectionArgs.exists[arg |
						// arg can be a single element or a list
						if(arg instanceof ListValue) {
							arg.ownedValues.exists[ argListValue | 
								receiverValue.ownedValues.exists[oVal | oVal.bEquals(argListValue)]
							]
						} else {
							receiverValue.ownedValues.exists[oVal | oVal.bEquals(arg)]
						}
					]
					return result
				}
				case "containsAll": {
					val result  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
					result.booleanValue = evaluatedMemberSelectionArgs.forall[arg |
						// arg can be a single element or a list
						if(arg instanceof ListValue) {
							arg.ownedValues.forall[ argListValue | 
								receiverValue.ownedValues.exists[oVal | oVal.bEquals(argListValue)]
							]
						} else {
							receiverValue.ownedValues.exists[oVal | oVal.bEquals(arg)]
						}
					]
					return result
				}
				case "isEmpty" : {
					val result  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
					result.booleanValue = (receiverValue as ListValue).ownedValues.empty
					return result
				}
				case "filter" : {
					//, should not be used, this is tackled by evalLambdaMember()
					throw new NotImplementedException('not implemented evalMember() for '+_self);
				}
				default: {
					throw new NotImplementedException('not implemented evalMember() for '+_self);
				}
			}
		} else {
			throw new AssetBasedSystemException(AssetBasedSystemException.RECEIVER_IS_NOT_A_LIST_OF_ASSET, #[receiverValue.toUserString, _self.name]);
		}
	}
	@OverrideAspectMethod
	def Value evalLambdaMember(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap, Value receiverValue, LambdaExpression lambda) throws AssetBasedSystemException, UndefinedReceiverException {
		if(receiverValue instanceof ListValue){
			
			switch (_self.name) {
				case "filter": {
					val result = Interpreter_vmFactory.eINSTANCE.createListValue
					for(evaluatedItem : receiverValue.ownedValues){
						val HashMap<Parameter, GuardOccurenceArgument> internalArgumentMap = newHashMap
						// add param
						val assetArg = Interpreter_vmFactory.eINSTANCE.createAssetArgument
						if( evaluatedItem instanceof AssetValue){
							assetArg.asset = evaluatedItem.assetValue
							internalArgumentMap.put(lambda.lambdaParameter, assetArg)
							internalArgumentMap.putAll(argumentMap)
							val localExpression = lambda.body
							if(localExpression.evalBooleanExpression(mainModel, internalArgumentMap)){
								// clone the evaluatedItem 
								val AssetValue clonedAssetValue = Interpreter_vmFactory.eINSTANCE.createAssetValue
								clonedAssetValue.init(evaluatedItem.assetValue)
								result.ownedValues.add(clonedAssetValue)
							} // else ignored element
						} else {
							throw new NotImplementedException("value "+evaluatedItem+"in collection lambda isn't an asset "+_self);
						}
					}
					//_self.devDebug("filter result="+result.toUserString)
					return result
				}
				default: {
					throw new NotImplementedException('not implemented evalLambdaMember() for '+_self);
				}
			}
		} else {
			throw new AssetBasedSystemException(AssetBasedSystemException.RECEIVER_IS_NOT_A_LIST_OF_ASSET, #[receiverValue.toUserString, _self.name]);
		}
	}
}



