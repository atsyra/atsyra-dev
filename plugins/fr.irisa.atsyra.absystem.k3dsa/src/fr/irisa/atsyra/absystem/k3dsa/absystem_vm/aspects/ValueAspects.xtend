package fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects

import fr.inria.diverse.k3.al.annotationprocessor.Aspect


import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.AssetArgumentK3Aspect.*

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeReferenceK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetBasedSystemK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetLinkK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAttributeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeFeatureK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.PrimitiveDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumLiteralK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetAttributeValueK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.DefinitionGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ImportK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.TagK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AbstractAssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAspectK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardedActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardParameterK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BinaryExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.OrExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AndExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.NotExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EqualityComparisonExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ConstantExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StringConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.IntConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BooleanConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberSelectionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.LambdaExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolRefK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StaticMethodK3Aspect.*

import static extension fr.irisa.atsyra.absystem.k3dsa.ecore.aspects.EObjectAspect.*
import fr.irisa.atsyra.absystem.k3dsa.commons.NotImplementedException
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.BooleanValue
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetValue
import fr.irisa.atsyra.absystem.model.absystem.Asset
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ListValue
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.IntegerValue
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Value
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmFactory
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.StringValue
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.EnumValue
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral
import fr.inria.diverse.k3.al.annotationprocessor.OverrideAspectMethod
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.VersionValue
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.UndefinedValue

@Aspect(className=Value)
class ValueK3Aspect {

	def  String toUserString() {
		throw new NotImplementedException('not implemented, please implement toUserString() for '+_self);
	}
	
	def  BooleanValue bvEquals(Value rhs) {
		val bValue  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
		bValue.booleanValue = _self.bEquals(rhs)
		return bValue
	}
	def  boolean bEquals(Value rhs) {
		throw new NotImplementedException('not implemented, please implement bEquals() for '+_self);
	}
	def  BooleanValue bvLesser(Value rhs) {
		val bValue  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
		bValue.booleanValue = _self.bLesser(rhs)
		return bValue
	}
	def  boolean bLesser(Value rhs) {
		throw new NotImplementedException('not implemented, please implement bLesser() for '+_self);
	}
	
	def  BooleanValue bvGreater(Value rhs) {
		val bValue  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
		bValue.booleanValue = _self.bGreater(rhs)
		return bValue
	}
	def  boolean bGreater(Value rhs) {
		throw new NotImplementedException('not implemented, please implement bLesser() for '+_self);
	}
}

@Aspect (className=UndefinedValue)
class UndefinedValueK3Aspect  extends ValueK3Aspect{
	@OverrideAspectMethod
	def String toUserString() {
		'undefined'
	}
	
	@OverrideAspectMethod
	def boolean bEquals(Value rhs) {
		return rhs instanceof UndefinedValue
	}
}

@Aspect (className=BooleanValue)
class BooleanValueK3Aspect  extends ValueK3Aspect{
	@OverrideAspectMethod
	def  String toUserString() {
		''+_self.booleanValue
	}
	
	@OverrideAspectMethod
	def  boolean bEquals(Value rhs) {
		if(rhs instanceof BooleanValue) {
			return _self.booleanValue == rhs.booleanValue;
		} else {
			return false;
		}
	}
}

@Aspect (className=IntegerValue)
class IntegerValueK3Aspect  extends ValueK3Aspect{
	@OverrideAspectMethod
	def  String toUserString() {
		''+_self.intValue
	}
	
	@OverrideAspectMethod
	def  boolean bEquals(Value rhs) {
		if(rhs instanceof IntegerValue) {
			return _self.intValue == rhs.intValue;
		} else {
			return false;
		}
	}
	
	@OverrideAspectMethod
	def  boolean bLesser(Value rhs) {
		if(rhs instanceof IntegerValue) {
			return _self.intValue < rhs.intValue;
		} else {
			return false;
		}
	}
	
	@OverrideAspectMethod
	def  boolean bGreater(Value rhs) {
		if(rhs instanceof IntegerValue) {
			return _self.intValue > rhs.intValue;
		} else {
			return false;
		}
	}
}

@Aspect (className=StringValue)
class StringValueK3Aspect  extends ValueK3Aspect{
	@OverrideAspectMethod
	def  String toUserString() {
		''+_self.stringValue
	}
	
	@OverrideAspectMethod
	def  boolean bEquals(Value rhs) {
		if(rhs instanceof StringValue) {
			return _self.stringValue == rhs.stringValue;
		} else {
			return false;
		}
	}
}

@Aspect (className=AssetValue)
class AssetValueK3Aspect extends ValueK3Aspect {
	@OverrideAspectMethod
	def  String toUserString() {
		_self.assetValue.name
	}
	
	def AssetValue init(Asset asset){
		_self.assetValue = asset
		return _self
	}
	
	@OverrideAspectMethod
	def boolean bEquals(Value rhs) {
		if(rhs instanceof AssetValue) {
			return _self.assetValue == rhs.assetValue;
		} else {
			return false;
		}
	}
}

@Aspect (className=ListValue)
class ListValueK3Aspect extends ValueK3Aspect {
	@OverrideAspectMethod
	def String toUserString() {
		'''[«FOR ownedValue : _self.ownedValues SEPARATOR ', '»«ownedValue.toUserString»«ENDFOR»]'''
	}
	
}

@Aspect (className=EnumValue)
class EnumValueK3Aspect  extends ValueK3Aspect{
	@OverrideAspectMethod
	def String toUserString() {
		''+_self.enumliteralValue.name
	}
	
	def EnumValue init(EnumLiteral lit){
		_self.enumliteralValue = lit
		return _self
	}
	
	@OverrideAspectMethod
	def boolean bEquals(Value rhs) {
		if(rhs instanceof EnumValue) {
			return _self.enumliteralValue == rhs.enumliteralValue;
		} else {
			return false;
		}
	}
}

@Aspect (className=VersionValue)
class VersionValueK3Aspect extends ValueK3Aspect {
	@OverrideAspectMethod
	def String toUserString() {
		_self.versionValue.toString
	}

	@OverrideAspectMethod
	def boolean bEquals(Value rhs) {
		if (rhs instanceof VersionValue) {
			return _self.versionValue == rhs.versionValue;
		} else {
			return false;
		}
	}

	@OverrideAspectMethod
	def boolean bLesser(Value rhs) {
		if (rhs instanceof VersionValue) {
			return _self.versionValue.compareToAsSemVer(rhs.versionValue) === -1;
		} else {
			return false;
		}
	}

	@OverrideAspectMethod
	def boolean bGreater(Value rhs) {
		if (rhs instanceof VersionValue) {
			return _self.versionValue.compareToAsSemVer(rhs.versionValue) === 1;
		} else {
			return false;
		}
	}
}
