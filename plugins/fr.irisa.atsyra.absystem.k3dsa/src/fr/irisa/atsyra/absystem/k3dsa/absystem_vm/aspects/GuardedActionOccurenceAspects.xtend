package fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects

import fr.inria.diverse.k3.al.annotationprocessor.Aspect

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.AssetArgumentK3Aspect.*

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeReferenceK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetBasedSystemK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetLinkK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAttributeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeFeatureK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.PrimitiveDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumLiteralK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetAttributeValueK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.DefinitionGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ImportK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.TagK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AbstractAssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAspectK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardedActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardParameterK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BinaryExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.OrExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AndExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.NotExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EqualityComparisonExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ConstantExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StringConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.IntConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BooleanConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberSelectionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.LambdaExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolRefK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StaticMethodK3Aspect.*

import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurence
import java.util.HashMap
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurenceArgument
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetArgument
import fr.irisa.atsyra.absystem.k3dsa.commons.NotImplementedException
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Value
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmFactory
import fr.irisa.atsyra.absystem.model.absystem.Parameter
import fr.irisa.atsyra.absystem.model.absystem.GuardedAction
import fr.irisa.atsyra.absystem.k3dsa.commons.UndefinedReceiverException
import fr.inria.diverse.k3.al.annotationprocessor.OverrideAspectMethod
import java.util.Locale
import fr.irisa.atsyra.absystem.model.absystem.util.ABSUtils
import fr.irisa.atsyra.absystem.k3dsa.commons.AssetBasedSystemException

@Aspect(className=GuardOccurence)
class GuardOccurenceK3Aspect {

	def String toUserString() {
		return '''«_self.guard.name»(«FOR arg : _self.guardOccurenceArguments SEPARATOR ', '»«arg.toUserString»«ENDFOR»)'''
	}
	
	def String toLocalizedUserString(Locale locale) {
		ABSUtils.getLocalization(_self.guard, locale.language).map [
			'''«it.name»(«FOR arg : _self.guardOccurenceArguments SEPARATOR ', '»«arg.toUserString»«ENDFOR»)'''
		].orElseGet[_self.toUserString]
	}

	def boolean evaluateGuard(AssetBasedSystem mainModel) throws UndefinedReceiverException, AssetBasedSystemException {
		val HashMap<Parameter, GuardOccurenceArgument> argumentMap = newHashMap
		_self.guard.guardParameters.forEach [ param, index |
			argumentMap.put(param, _self.guardOccurenceArguments.get(index))
		]
		val guardEval = _self.guard.guardExpression.evalBooleanExpression(mainModel, argumentMap)
		return guardEval
	}

	/**
	 * an action is applicable if the guard is true and the actions has side effects
	 */
	def boolean isApplicableAction(AssetBasedSystem mainModel) throws AssetBasedSystemException {
		if(!(_self.guard instanceof GuardedAction)) return false
		try {
			val guardedAction = _self.guard as GuardedAction
			val HashMap<Parameter, GuardOccurenceArgument> argumentMap = newHashMap
			guardedAction.guardParameters.forEach [ param, index |
				argumentMap.put(param, _self.guardOccurenceArguments.get(index))
			]
			val guardEval = guardedAction.guardExpression.evalBooleanExpression(mainModel, argumentMap)
			// _self.devDebug("Evaluating guard expression: "+_self.guardedAction.guardExpression .xtextPrettyPrint + " -> "+ guardEval)
			return guardEval && guardedAction.guardActions.exists[action|action.hasSideEffect(mainModel, argumentMap)]
		} catch (UndefinedReceiverException e) {
			return false
		}
	}

	/**
	 * apply this action
	 * Note: this doesn't check for the guard or that the Guard is really an action (this is supposed to be verified before calling this
	 */
	def void applyAction(AssetBasedSystem mainModel) {
		if (!(_self.guard instanceof GuardedAction)) {
			throw new RuntimeException("Cannot applyAction on " + _self)
		}
		val guardedAction = _self.guard as GuardedAction
		val HashMap<Parameter, GuardOccurenceArgument> argumentMap = newHashMap
		guardedAction.guardParameters.forEach [ param, index |
			argumentMap.put(param, _self.guardOccurenceArguments.get(index))
		]
		for (action : guardedAction.guardActions) {
			action.apply(mainModel, argumentMap)
		}
	}
}

@Aspect(className=GuardOccurenceArgument)
class GuardOccurenceArgumentK3Aspect {

	def Value getValue() {
		throw new NotImplementedException('not implemented, please implement getValue() for ' + _self);
	}

	def String toUserString() {
		_self.toString
	}
}

@Aspect(className=AssetArgument)
class AssetArgumentK3Aspect extends GuardOccurenceArgumentK3Aspect {
	@OverrideAspectMethod
	def Value getValue() {
		// _self.devWarn('TODO decide if need to create many AssetValue + equals operator or use a central factory (do we need clone ?)')
		val r = Interpreter_vmFactory.eINSTANCE.createAssetValue
		r.assetValue = _self.asset
		return r
	}
	@OverrideAspectMethod
	def String toUserString() {
		_self.asset.name
	}
}
