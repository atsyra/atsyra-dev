/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.k3dsa.commons;

public class InvalidConfigurationException extends LocalizableException {
	
	public static final String MISSING_ASSET_FOR_INITIALIZATION = "_exception__missing_asset_for_initialization";
	public static final String MISSING_DYNAMIC_FEATURE_FOR_INITIALIZATION = "_exception__missing_dynamic_feature_for_initialization";
	public static final String MISSING_ASSET_FOR_REFERENCE = "_exception__missing_asset_for_reference";
	public static final String MISSING_STATIC_ATTRIBUTE_FOR_INITIALIZATION = "_exception__missing_static_attribute_for_initialization";
	
	public InvalidConfigurationException(String key, String[] substitutions) {
	  super(key, substitutions);
	}

}
