package fr.irisa.atsyra.absystem.k3dsa.absystem.aspects

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.EnumLiteral
import fr.irisa.atsyra.absystem.model.absystem.GuardParameter
import fr.irisa.atsyra.absystem.model.absystem.Expression
import fr.irisa.atsyra.absystem.model.absystem.BinaryExpression
import fr.irisa.atsyra.absystem.model.absystem.OrExpression
import fr.irisa.atsyra.absystem.model.absystem.AndExpression
import fr.irisa.atsyra.absystem.model.absystem.NotExpression
import fr.irisa.atsyra.absystem.model.absystem.EqualityComparisonExpression
import fr.irisa.atsyra.absystem.model.absystem.ConstantExpression
import fr.irisa.atsyra.absystem.model.absystem.StringConstant
import fr.irisa.atsyra.absystem.model.absystem.IntConstant
import fr.irisa.atsyra.absystem.model.absystem.BooleanConstant
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection
import fr.irisa.atsyra.absystem.model.absystem.SymbolRef

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeReferenceK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetBasedSystemK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetLinkK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAttributeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeFeatureK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.PrimitiveDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumLiteralK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetAttributeValueK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.DefinitionGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ImportK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.TagK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AbstractAssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAspectK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardedActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardParameterK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BinaryExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.OrExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AndExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.NotExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EqualityComparisonExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ConstantExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StringConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.IntConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BooleanConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberSelectionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.LambdaExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolRefK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StaticMethodK3Aspect.*

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.GuardOccurenceK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.AssetArgumentK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.AssetValueK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.ValueK3Aspect.*

import static extension fr.irisa.atsyra.absystem.k3dsa.ecore.aspects.EObjectAspect.*

import java.util.HashMap
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmFactory
import fr.irisa.atsyra.absystem.k3dsa.commons.NotImplementedException
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurenceArgument
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.BooleanValue
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Value
import fr.irisa.atsyra.absystem.k3dsa.commons.AssetBasedSystemException
import fr.irisa.atsyra.absystem.model.absystem.Parameter
import fr.irisa.atsyra.absystem.model.absystem.LambdaExpression
import fr.irisa.atsyra.absystem.model.absystem.LambdaParameter
import fr.irisa.atsyra.absystem.model.absystem.EnumConstant
import fr.irisa.atsyra.absystem.model.absystem.InequalityComparisonExpression
import fr.irisa.atsyra.absystem.model.absystem.VersionConstant
import fr.irisa.atsyra.absystem.model.absystem.ImpliesExpression
import fr.irisa.atsyra.absystem.model.absystem.UndefinedConstant
import fr.irisa.atsyra.absystem.model.absystem.Collection
import fr.inria.diverse.k3.al.annotationprocessor.OverrideAspectMethod
import fr.irisa.atsyra.absystem.k3dsa.commons.UndefinedReceiverException

@Aspect(className=Expression)
abstract class ExpressionK3Aspect {
	def boolean evalBooleanExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException, AssetBasedSystemException {
		val Value internalResult = _self.evalExpression(mainModel, argumentMap);
		var boolean result = false;
		if(internalResult instanceof BooleanValue) {
			result = internalResult.booleanValue;
		} else if (internalResult === null) {
			//some value is not defined, we evaluate to false by convention
			_self.error('BooleanExpression was null while evaluating '+_self.xtextPrettyPrint);
			return false;
		} else {
			throw new AssetBasedSystemException(AssetBasedSystemException.EXPECTED_BOOLEAN, #[internalResult.toUserString, _self.xtextPrettyPrint]);
		}
		return result;
	}
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		_self.devError('not implemented, please ask language designer to implement evalExpression() for '+_self);
		throw new NotImplementedException('not implemented, please implement evalExpression() for '+_self + ' (used in '+_self.xtextPrettyPrint+')');
	}
	
	
	
	def String toUserString() {
		_self.toString
	}
}

@Aspect(className=BinaryExpression)
abstract class BinaryExpressionK3Aspect extends ExpressionK3Aspect {

}

@Aspect(className=OrExpression)
class OrExpressionK3Aspect extends BinaryExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val result  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
		result.booleanValue = _self.lhs.evalBooleanExpression(mainModel, argumentMap) || _self.rhs.evalBooleanExpression(mainModel, argumentMap)
		return result 
	}
}

@Aspect(className=AndExpression)
class AndExpressionK3Aspect extends BinaryExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val result  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
		result.booleanValue = _self.lhs.evalBooleanExpression(mainModel, argumentMap) && _self.rhs.evalBooleanExpression(mainModel, argumentMap)
		return result 
	}
}

@Aspect(className=ImpliesExpression)
class ImpliesExpressionK3Aspect extends BinaryExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val result  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
		result.booleanValue = !_self.lhs.evalBooleanExpression(mainModel, argumentMap) || _self.rhs.evalBooleanExpression(mainModel, argumentMap)
		return result 
	}
}

@Aspect(className=NotExpression)
class NotExpressionK3Aspect extends ExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val result  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
		result.booleanValue = ! _self.expression.evalBooleanExpression(mainModel, argumentMap)
		return result 
	}
}

@Aspect(className=EqualityComparisonExpression)
class EqualityComparisonExpressionK3Aspect extends BinaryExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val lhsVal = _self.lhs.evalExpression(mainModel, argumentMap)
		val rhsVal = _self.rhs.evalExpression(mainModel, argumentMap)
		switch (_self.op) {
			case "==": {
				return lhsVal.bvEquals(rhsVal)
			}case "!=": {
				val result = lhsVal.bvEquals(rhsVal)
				if(result !== null)
					result.booleanValue = ! result.booleanValue
				return result
			}
			default: {
				throw new NotImplementedException('not implemented, please implement evalExpression() for '+_self + ' (used in '+_self.xtextPrettyPrint+')');
			}
		}
		 
	}
}

@Aspect(className=InequalityComparisonExpression)
class InequalityComparisonExpressionK3Aspect extends BinaryExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val lhsVal = _self.lhs.evalExpression(mainModel, argumentMap)
		val rhsVal = _self.rhs.evalExpression(mainModel, argumentMap)
		switch (_self.op) {
			case "<": {
				return lhsVal.bvLesser(rhsVal)
			}
			case "<=": {
				val result = lhsVal.bvGreater(rhsVal)
				if(result !== null)
					result.booleanValue = ! result.booleanValue
				return result
			}
			case ">": {
				return lhsVal.bvGreater(rhsVal)
			}
			case ">=": {
				val result = lhsVal.bvLesser(rhsVal)
				if(result !== null)
					result.booleanValue = ! result.booleanValue
				return result
			}
			default: {
				throw new NotImplementedException(
					'not implemented, please implement evalExpression() for ' + _self + ' (used in ' +
						_self.xtextPrettyPrint + ')');
			}
		}
		 
	}
}

@Aspect(className=ConstantExpression)
abstract class ConstantExpressionK3Aspect extends ExpressionK3Aspect {

}

@Aspect(className=StringConstant)
class StringConstantK3Aspect extends ConstantExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val sValue  = Interpreter_vmFactory.eINSTANCE.createStringValue
		sValue.stringValue = _self.value
		return sValue
	}
}

@Aspect(className=IntConstant)
class IntConstantK3Aspect extends ConstantExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val iValue  = Interpreter_vmFactory.eINSTANCE.createIntegerValue
		iValue.intValue = _self.value
		return iValue
	}
}

@Aspect(className=BooleanConstant)
class BooleanConstantK3Aspect extends ConstantExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val bValue  = Interpreter_vmFactory.eINSTANCE.createBooleanValue
		bValue.booleanValue = _self.value.equalsIgnoreCase("true")
		return bValue
	}
}

@Aspect(className=EnumConstant)
class EnumConstantK3Aspect extends ConstantExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val enumValue  = Interpreter_vmFactory.eINSTANCE.createEnumValue
		enumValue.enumliteralValue = _self.value
		return enumValue
	}
}

@Aspect(className=VersionConstant)
class VersionConstantK3Aspect extends ConstantExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val versionValue  = Interpreter_vmFactory.eINSTANCE.createVersionValue
		versionValue.versionValue = _self.value
		return versionValue
	}
}

@Aspect(className=UndefinedConstant)
class UndefinedConstantK3Aspect extends ConstantExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val undefinedValue  = Interpreter_vmFactory.eINSTANCE.createUndefinedValue
		return undefinedValue
	}
}

@Aspect(className=Collection)
class CollectionK3Aspect extends ExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		val listValue  = Interpreter_vmFactory.eINSTANCE.createListValue
		_self.elements.forEach[listValue.ownedValues.add(it.evalExpression(mainModel, argumentMap))]
		return listValue
	}
}

@Aspect(className=MemberSelection)
class MemberSelectionK3Aspect extends ExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		//_self.devDebug(_self.toUserString);
		val receiverVal = _self.receiver.evalExpression(mainModel, argumentMap)
		//_self.devDebug('recieverVal='+recieverVal.toUserString)
		
		if(_self.args.size == 1 && _self.args.get(0) instanceof LambdaExpression) {
			// lambda evaluation
			_self.member.evalLambdaMember(mainModel, argumentMap, receiverVal, _self.args.get(0) as LambdaExpression)
		} else {
			// standard method call
			val evaluatedArgs = _self.args.map[arg | arg.evalExpression(mainModel, argumentMap)].toList
			_self.member.evalMember(mainModel, argumentMap, receiverVal, evaluatedArgs)
		}
	}
	@OverrideAspectMethod
	def  String toUserString() {
		'''MemberSelection(
			receiver=«_self.receiver.toUserString»,
			member=«_self.member.toString», 
			args=[«FOR a : _self.args SEPARATOR ','»«a.toUserString»«ENDFOR»]
		)'''
	}
}

@Aspect(className=LambdaExpression)
class LambdaExpressionK3Aspect extends ExpressionK3Aspect {
	@OverrideAspectMethod
	def  String toUserString() {
		'''LambdaExpressionK3Aspect(
		lambdaParameter=«_self.lambdaParameter.name», 
		body=«_self.body»
	)'''
	}
}


@Aspect(className=SymbolRef)
class SymbolRefK3Aspect extends ExpressionK3Aspect {
	@OverrideAspectMethod
	def Value evalExpression(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws UndefinedReceiverException {
		switch (_self.symbol) {
			GuardParameter : {
				return argumentMap.get(_self.symbol).value 
			}
			LambdaParameter : {
				return argumentMap.get(_self.symbol).value
			}
			EnumLiteral : {
				val enumValue  = Interpreter_vmFactory.eINSTANCE.createEnumValue
				enumValue.enumliteralValue = (_self.symbol as EnumLiteral)
				return enumValue
			}
			default: {
				throw new NotImplementedException('not implemented, please implement evalExpression() for '+_self + ' ' +_self.symbol + ' (used in '+_self.xtextPrettyPrint+')');
			}
		}
	}
	@OverrideAspectMethod
	def  String toUserString() {
		'SymbolRef('+_self.symbol.name+')'
	}
}

