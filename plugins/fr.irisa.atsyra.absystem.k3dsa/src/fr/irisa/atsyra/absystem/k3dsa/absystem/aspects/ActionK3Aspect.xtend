package fr.irisa.atsyra.absystem.k3dsa.absystem.aspects

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.irisa.atsyra.absystem.model.absystem.AssetBasedSystem
import fr.irisa.atsyra.absystem.model.absystem.AssetTypeFeature
import fr.irisa.atsyra.absystem.model.absystem.Action
import fr.irisa.atsyra.absystem.model.absystem.MemberSelection

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeReferenceK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetBasedSystemK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.LambdaActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetLinkK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAttributeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeFeatureK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.PrimitiveDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumDataTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EnumLiteralK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetAttributeValueK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.DefinitionGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetGroupK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ImportK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.TagK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AbstractAssetTypeK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AssetTypeAspectK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardedActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.GuardParameterK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BinaryExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.OrExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.AndExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.NotExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.EqualityComparisonExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ActionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.ConstantExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StringConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.IntConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.BooleanConstantK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberSelectionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.LambdaExpressionK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.MemberK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolRefK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.SymbolK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem.aspects.StaticMethodK3Aspect.*

import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.GuardOccurenceK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.AssetArgumentK3Aspect.*
import static extension fr.irisa.atsyra.absystem.k3dsa.absystem_vm.aspects.AssetValueK3Aspect.*

import static extension fr.irisa.atsyra.absystem.k3dsa.ecore.aspects.EObjectAspect.*

import fr.inria.diverse.k3.al.annotationprocessor.Step
import java.util.HashMap
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.Interpreter_vmFactory
import fr.irisa.atsyra.absystem.k3dsa.commons.NotImplementedException
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.GuardOccurenceArgument
import fr.irisa.atsyra.absystem.k3dsa.commons.AssetBasedSystemException
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.AssetValue
import fr.irisa.atsyra.absystem.model.absystem.interpreter_vm.ListValue
import fr.irisa.atsyra.absystem.model.absystem.LambdaAction
import fr.irisa.atsyra.absystem.model.absystem.Parameter
import fr.irisa.atsyra.absystem.k3dsa.commons.UndefinedReceiverException
import fr.irisa.atsyra.absystem.model.absystem.ActionEnum

@Aspect(className=Action)
class ActionK3Aspect {

	def boolean hasSideEffect(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws AssetBasedSystemException {
		var boolean res = false
		try {
			switch (_self.actionType) {
				case ADD: {
					val targetVal = _self.target.evalExpression(mainModel, argumentMap)
					if (targetVal instanceof ListValue) {
						if (_self.args.size == 1) {
							val argVal = _self.args.get(0).evalExpression(mainModel, argumentMap)
							// check content
							if (argVal instanceof ListValue) {
								throw new AssetBasedSystemException(AssetBasedSystemException.ARGUMENT_VALUE_MUST_NOT_BE_A_LIST, #[_self.actionType.getName, argVal.toUserString, _self.target.xtextPrettyPrint])
							}
							// if already in target, the action has no effect
							res = !targetVal.ownedValues.exists[oVal|oVal.bEquals(argVal)]

						} else {
							throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_INVALID_NUMBER_OF_PARAMETERS, #[_self.actionType.getName]);
						}
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.TARGET_VALUE_MUST_BE_A_LIST, #[_self.actionType.getName, targetVal.toUserString, _self.target.xtextPrettyPrint]);
					}
				}
				case ADD_ALL: {
					val targetVal = _self.target.evalExpression(mainModel, argumentMap)
					if (targetVal instanceof ListValue) {
						if (_self.args.size >= 1) {
							for (arg : _self.args) {
								val argVal = arg.evalExpression(mainModel, argumentMap)
								// check content
								if (argVal instanceof ListValue) {
									// if one element is not already in target, the action has side effect
									res = argVal.ownedValues.exists [ aArgValoVal |
										!targetVal.ownedValues.exists[oVal|oVal.bEquals(aArgValoVal)]
									]
								} else {
									// if element is not already in target, the action has side effect
									res = !targetVal.ownedValues.exists[oVal|oVal.bEquals(argVal)]
								}
								if (res) {
									// _self.devDebug('''	«_self.xtextPrettyPrint» .hasSideEffect() ->  «res»''')
									return res
								}
							}
						} else {
							throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_INVALID_NUMBER_OF_PARAMETERS, #[_self.actionType.getName]);
						}
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.TARGET_VALUE_MUST_BE_A_LIST, #[_self.actionType.getName, targetVal.toUserString, _self.target.xtextPrettyPrint]);
					}
				}
				case ASSIGN: {
					val targetVal = _self.target.evalExpression(mainModel, argumentMap)
					if (targetVal instanceof ListValue) {
						throw new AssetBasedSystemException(AssetBasedSystemException.TARGET_VALUE_MUST_NOT_BE_A_LIST, #[_self.actionType.getName, targetVal.toUserString, _self.target.xtextPrettyPrint]);
					} else {
						if (_self.args.size == 1) {
							val argVal = _self.args.get(0).evalExpression(mainModel, argumentMap)
							if (argVal instanceof ListValue) {
								throw new AssetBasedSystemException(AssetBasedSystemException.ARGUMENT_VALUE_MUST_NOT_BE_A_LIST, #[_self.actionType.getName, argVal.toUserString, _self.target.xtextPrettyPrint]);
							}
							// if is already the target, the action has no effect
							res = !targetVal.bEquals(argVal)
						} else {
							throw new AssetBasedSystemException(
								AssetBasedSystemException.ACTION_INVALID_NUMBER_OF_PARAMETERS, #[_self.actionType.getName]);
						}
					}
				}
				case FOR_ALL: {
					val targetVal = _self.target.evalExpression(mainModel, argumentMap)
					if (targetVal instanceof ListValue) {
						res = _self.lambdaAction.hasSideEffect(mainModel, targetVal as ListValue, argumentMap)
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.TARGET_VALUE_MUST_BE_A_LIST, #[_self.actionType.getName, targetVal.toUserString, _self.target.xtextPrettyPrint]);
					}
				}
				case CLEAR: {
					val targetVal = _self.target.evalExpression(mainModel, argumentMap)
					if (targetVal instanceof ListValue) {
						targetVal.ownedValues.size > 0
					} else {
						val targetExp = _self.target
						if (targetExp instanceof MemberSelection) {
							val receiver = targetExp.receiver.evalExpression(mainModel, argumentMap)
							val feature = targetExp.member
							if (receiver instanceof AssetValue) {
								if (feature instanceof AssetTypeFeature) {
									receiver.assetValue.featuresMapGet(feature) === null
								} else {
									throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_MUST_BE_A_FEATURE,
										#[_self.actionType.getName, feature.name, _self.xtextPrettyPrint]);
								}
							} else {
								throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_RECEIVER_MUST_BE_AN_ASSET,
									#[_self.actionType.getName, receiver.toUserString, _self.xtextPrettyPrint]);
							}
						} else {
							throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_MUST_BE_A_MEMBERSELECTION,
								#[_self.actionType.getName, _self.xtextPrettyPrint]);
						}
						throw new NotImplementedException(
							'not implemented, please implement hasSideEffect() for ' + _self);
					}
				}
				case REMOVE: {
					val targetVal = _self.target.evalExpression(mainModel, argumentMap)
					val argVal = _self.args.get(0).evalExpression(mainModel, argumentMap)
					if (targetVal instanceof ListValue) {
						res = targetVal.ownedValues.exists[oVal|oVal.bEquals(argVal)]
					} else {
						res = !targetVal.bEquals(argVal)
					}
				}
				default: {
					throw new NotImplementedException('not implemented, please implement hasSideEffect() for ' + _self);
				}
			}
		} catch (UndefinedReceiverException e) {
			return false
		}
		// _self.devDebug('''	«_self.xtextPrettyPrint» .hasSideEffect() ->  «res»''')
		return res
	}

	@Step
	def void apply(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws AssetBasedSystemException {
		switch (_self.actionType) {
			case ADD: {
				val targetVal = _self.target.evalExpression(mainModel, argumentMap)
				if (targetVal instanceof ListValue) {
					if (_self.args.size == 1) {
						val argVal = _self.args.get(0).evalExpression(mainModel, argumentMap)
						// check content
						if (argVal instanceof ListValue) {
							throw new AssetBasedSystemException(AssetBasedSystemException.ARGUMENT_VALUE_MUST_NOT_BE_A_LIST, #[_self.actionType.getName, argVal.toUserString, _self.target.xtextPrettyPrint]);
						}
						// add
						targetVal.ownedValues.add(argVal)
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_INVALID_NUMBER_OF_PARAMETERS, #[_self.actionType.getName]);
					}
				} else {
					throw new AssetBasedSystemException(AssetBasedSystemException.TARGET_VALUE_MUST_BE_A_LIST, #[_self.actionType.getName, targetVal.toUserString, _self.target.xtextPrettyPrint]);
				}
			}
			case ADD_ALL: {
				val targetVal = _self.target.evalExpression(mainModel, argumentMap)
				if (targetVal instanceof ListValue) {
					if (_self.args.size >= 1) {
						for (arg : _self.args) {
							val argVal = arg.evalExpression(mainModel, argumentMap)
							// check content
							if (argVal instanceof ListValue) {
								// if one element is not already in target, the action has side effect
								targetVal.ownedValues.addAll(argVal.ownedValues)
							} else {
								targetVal.ownedValues.add(argVal)
							}
						}
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_INVALID_NUMBER_OF_PARAMETERS, #[_self.actionType.getName]);
					}
				} else {
					throw new AssetBasedSystemException(AssetBasedSystemException.TARGET_VALUE_MUST_BE_A_LIST, #[_self.actionType.getName, targetVal.toUserString, _self.target.xtextPrettyPrint]);
				}
			}
			case ASSIGN: {
				val targetExp = _self.target
				if (targetExp instanceof MemberSelection) {
					// throw new AssetBasedSystemException('TODO '+_self);
					val receiver = targetExp.receiver.evalExpression(mainModel, argumentMap)
					val feature = targetExp.member
					if (receiver instanceof AssetValue) {
						if (feature instanceof AssetTypeFeature) {
							val argVal = _self.args.get(0).evalExpression(mainModel, argumentMap)
							receiver.assetValue.featuresMapPut(feature, argVal)
						} else {
							throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_MUST_BE_A_FEATURE,
										#[_self.actionType.getName, feature.name, _self.xtextPrettyPrint]);
						}
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_RECEIVER_MUST_BE_AN_ASSET,
									#[_self.actionType.getName, receiver.toUserString, _self.xtextPrettyPrint]);
					}
				} else {
					throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_MUST_BE_A_MEMBERSELECTION,
								#[_self.actionType.getName, _self.xtextPrettyPrint]);
				}
			}
			case FOR_ALL: {
				val targetVal = _self.target.evalExpression(mainModel, argumentMap)
				if (targetVal instanceof ListValue) {
					if (_self.lambdaAction !== null) {
						for (targetElemVal : targetVal.ownedValues) {
							if (targetElemVal instanceof AssetValue) {
								val HashMap<Parameter, GuardOccurenceArgument> lambdaArgumentMap = newHashMap
								val assetArg = Interpreter_vmFactory.eINSTANCE.createAssetArgument
								assetArg.asset = targetElemVal.assetValue
								lambdaArgumentMap.put(_self.lambdaAction.lambdaParameter, assetArg)
								lambdaArgumentMap.putAll(argumentMap)
								_self.lambdaAction.applyLambda(mainModel, lambdaArgumentMap)
							} else {
								throw new AssetBasedSystemException(AssetBasedSystemException.TARGET_VALUE_MUST_BE_A_LIST_OF_ASSET, #[_self.actionType.getName, targetElemVal.toUserString, _self.target.xtextPrettyPrint]);
							}
						}
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_ARGUMENT_MUST_BE_A_LAMBDA, #[_self.actionType.getName, targetVal.toUserString, _self.target.xtextPrettyPrint]);
					}
				} else {
					throw new AssetBasedSystemException(AssetBasedSystemException.TARGET_VALUE_MUST_BE_A_LIST, #[_self.actionType.getName, targetVal.toUserString, _self.target.xtextPrettyPrint]);
				}
			}
			case CLEAR: {
				val targetVal = _self.target.evalExpression(mainModel, argumentMap)
				if (targetVal instanceof ListValue) {
					targetVal.ownedValues.clear
				} else {
					val targetExp = _self.target
					if (targetExp instanceof MemberSelection) {
						val receiver = targetExp.receiver.evalExpression(mainModel, argumentMap)
						val feature = targetExp.member
						if (receiver instanceof AssetValue) {
							if (feature instanceof AssetTypeFeature) {
								// val argVal = _self.args.get(0).evalExpression(mainModel, argumentMap)
								receiver.assetValue.featuresMapPut(feature, null)
							} else {
								throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_MUST_BE_A_FEATURE,
										#[_self.actionType.getName, feature.name, _self.xtextPrettyPrint]);
							}
						} else {
							throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_RECEIVER_MUST_BE_AN_ASSET,
									#[_self.actionType.getName, receiver.toUserString, _self.xtextPrettyPrint]);
						}
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_MUST_BE_A_MEMBERSELECTION,
								#[_self.actionType.getName, _self.xtextPrettyPrint]);
					}
				}
			}
			case REMOVE: {
				val targetVal = _self.target.evalExpression(mainModel, argumentMap)
				val argVal = _self.args.get(0).evalExpression(mainModel, argumentMap)
				if (targetVal instanceof ListValue) {
					val valToRemove = targetVal.ownedValues.findFirst[oVal|oVal.bEquals(argVal)]
					if (valToRemove !== null) {
						targetVal.ownedValues.remove(valToRemove)
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.CANNOT_REMOVE, #[argVal.toUserString, targetVal.toUserString]);
					}
				} else {
					if (targetVal.bEquals(argVal)) {
						val targetExp = _self.target
						if (targetExp instanceof MemberSelection) {
							val receiver = targetExp.receiver.evalExpression(mainModel, argumentMap)
							val feature = targetExp.member
							if (receiver instanceof AssetValue) {
								if (feature instanceof AssetTypeFeature) {
									// val argVal = _self.args.get(0).evalExpression(mainModel, argumentMap)
									receiver.assetValue.featuresMapPut(feature, null)
								} else {
									throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_MUST_BE_A_FEATURE,
										#[_self.actionType.getName, feature.name, _self.xtextPrettyPrint]);
								}
							} else {
								throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_RECEIVER_MUST_BE_AN_ASSET,
									#[_self.actionType.getName, receiver.toUserString, _self.xtextPrettyPrint]);
							}
						} else {
							throw new AssetBasedSystemException(AssetBasedSystemException.ACTION_TARGET_MUST_BE_A_MEMBERSELECTION,
								#[_self.actionType.getName, _self.xtextPrettyPrint]);
						}
					} else {
						throw new AssetBasedSystemException(AssetBasedSystemException.CANNOT_REMOVE, #[argVal.toUserString, targetVal.toUserString]);
					}
				}
			}
			default: {
				throw new NotImplementedException('not implemented, please implement apply() for ' + _self);
			}
		}
	}
}

@Aspect(className=LambdaAction)
class LambdaActionK3Aspect {
	def boolean hasSideEffect(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws AssetBasedSystemException {
		var res = false;
		for (action : _self.actions) {
			res = res || action.hasSideEffect(mainModel, argumentMap)
		}
		res
	}
	
	def boolean hasSideEffect(AssetBasedSystem mainModel, ListValue iteratedOn, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws AssetBasedSystemException {
		var res = false;
		for(value : iteratedOn.ownedValues) {
			if (value instanceof AssetValue) {
				res = res || _self.hasSideEffect(mainModel, value, argumentMap)
			} else {
				throw new AssetBasedSystemException(AssetBasedSystemException.TARGET_VALUE_MUST_BE_A_LIST_OF_ASSET, #[ActionEnum.FOR_ALL.getName, value.toUserString, _self.eContainer.xtextPrettyPrint]);
			}
		}
		res
	}
	
	def boolean hasSideEffect(AssetBasedSystem mainModel, AssetValue lambdaInstance, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws AssetBasedSystemException {
		val lambdaInstanceGO = Interpreter_vmFactory.eINSTANCE.createAssetArgument
		lambdaInstanceGO.asset = lambdaInstance.assetValue
		_self.hasSideEffect(mainModel, lambdaInstanceGO, argumentMap)
	}
	
	def boolean hasSideEffect(AssetBasedSystem mainModel, GuardOccurenceArgument lambdaInstance, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws AssetBasedSystemException {
		val HashMap<Parameter, GuardOccurenceArgument> updatedArgumentMap = newHashMap();
		updatedArgumentMap.putAll(argumentMap)
		updatedArgumentMap.put(_self.lambdaParameter, lambdaInstance)
		_self.hasSideEffect(mainModel, updatedArgumentMap)
	}

	def void applyLambda(AssetBasedSystem mainModel, HashMap<Parameter, GuardOccurenceArgument> argumentMap) throws AssetBasedSystemException {
		for (action : _self.actions) {
			action.apply(mainModel, argumentMap)
		}
	}
}
