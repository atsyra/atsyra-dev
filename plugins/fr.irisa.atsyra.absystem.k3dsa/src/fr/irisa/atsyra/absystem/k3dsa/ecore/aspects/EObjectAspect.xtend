package fr.irisa.atsyra.absystem.k3dsa.ecore.aspects

import org.eclipse.emf.ecore.EObject
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.EAttribute
import fr.irisa.atsyra.absystem.k3dsa.Activator
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Path
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.nodemodel.ICompositeNode

class EObjectAspect {
	
	public static final String MESSAGE_GROUP = "AssetBasedSystem_XDSML";
	
	def static void devDebug(EObject _self, String message) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().log(MessagingSystem.Kind.DevDEBUG, message, MESSAGE_GROUP);
	}
	
	def static void devDebug(EObject _self) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().log(MessagingSystem.Kind.DevDEBUG, _self.toString(), MESSAGE_GROUP);
	}
	def static void devInfo(EObject _self, String message) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().devInfo(message, MESSAGE_GROUP);
	}
	def static void devInfo(EObject _self) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().devInfo(_self.toString(), MESSAGE_GROUP);
	}
	def static void devWarn(EObject _self, String message) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().log(MessagingSystem.Kind.DevWARNING, message, MESSAGE_GROUP);
	}
	def static void devError(EObject _self, String message) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().log(MessagingSystem.Kind.DevERROR, message, MESSAGE_GROUP);
	}
	def static void info(EObject _self, String message) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().info(message, MESSAGE_GROUP);
	}
	def static void important(EObject _self, String message) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().important(message, MESSAGE_GROUP);
	}

	def static void warn(EObject _self, String message) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().warn(message, MESSAGE_GROUP);
	}
	def static void warn(EObject _self, String message, Throwable t) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().warn(message, MESSAGE_GROUP, t);
	}
	def static void error(EObject _self, String message) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().error(message, MESSAGE_GROUP);
	}
	def static void error(EObject _self, String message, Throwable t) {
		if(Activator.getDefault() !== null)
			Activator.getDefault().getMessaggingSystem().error(message, MESSAGE_GROUP, t);
	}
	
	def static String readConsoleLine(EObject _self, String message) {
		return if (Activator.getDefault() !== null)
			Activator.getDefault().messaggingSystem.readLine(message)
		else
			""
	}

	
	def static String getQualifiedName(EObject eObject) {
		val StringBuilder sb = new StringBuilder();

		if(eObject.eContainer() !== null){
			sb.append(getQualifiedName(eObject.eContainer()));
			sb.append("/");
		}
		val EAttribute feat = eObject.eClass().getEAllAttributes().findFirst[att | att.getName().equals("name")];
		if(feat !== null) {
			sb.append(eObject.eGet(feat));
		} else {
			val String lastFragmentPart = EcoreUtil.getURI(eObject).fragment()
					.substring(EcoreUtil.getURI(eObject).fragment().lastIndexOf("/")+1);
			sb.append(lastFragmentPart);
		}
		return sb.toString();
		
	}
	
	def static String xtextPrettyPrint(EObject any) {
    	if (any !== null && any.eResource() instanceof XtextResource && any.eResource().getURI() !== null) {
			val String fileURI = any.eResource().getURI().toPlatformString(true);
			val IFile workspaceFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(fileURI));
			if (workspaceFile !== null) {
				val ICompositeNode node = NodeModelUtils.findActualNodeFor(any);
				if (node !== null) {
					return node.getText().trim();
				}
			}
    	}
    	return "";
    }
	
}