/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.k3dsa.commons;

public class AssetBasedSystemException extends LocalizableException {
	
	public static final String RECEIVER_IS_NOT_AN_ASSET = "_exception__receiver_is_not_an_asset"; 
	public static final String INVALID_NUMBER_OF_ARGUMENTS = "_exception__invalid_number_of_arguments_for_method";
	public static final String RECEIVER_IS_NOT_A_LIST_OF_ASSET = "_exception__receiver_is_not_a_list_of_asset"; 
	public static final String ARGUMENT_VALUE_MUST_NOT_BE_A_LIST = "_exception__argument_value_must_not_be_a_list";
	public static final String TARGET_VALUE_MUST_BE_A_LIST  = "_exception__target_value_must_be_a_list";
	public static final String ACTION_INVALID_NUMBER_OF_PARAMETERS  = "_exception__action_invalid_number_of_parameters";
	public static final String ACTION_TARGET_MUST_BE_A_FEATURE  = "_exception__action_target_member_must_be_a_feature";
	public static final String ACTION_TARGET_RECEIVER_MUST_BE_AN_ASSET  = "_exception__action_target_receiver_must_be_an_asset";
	public static final String ACTION_TARGET_MUST_BE_A_MEMBERSELECTION  = "_exception__action_target_must_be_a_memberselection";
	public static final String TARGET_VALUE_MUST_NOT_BE_A_LIST  = "_exception__target_value_must_not_be_a_list";
	public static final String ACTION_ARGUMENT_MUST_BE_A_LAMBDA  = "_exception__action_argument_must_be_a_lambda";
	public static final String TARGET_VALUE_MUST_BE_A_LIST_OF_ASSET  = "_exception__action_target_invalid_must_be_a_list_of_assets";
	public static final String CANNOT_REMOVE  = "_exception__cannot_remove";
	public static final String EXPECTED_BOOLEAN  = "_exception__expected_boolean";

	public AssetBasedSystemException(String key, String[] substitutions) {
	  super(key, substitutions);
	}

}
