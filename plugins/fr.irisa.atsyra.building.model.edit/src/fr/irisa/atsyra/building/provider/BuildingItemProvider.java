/*******************************************************************************
 * Copyright (c) 2014, 2017 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 */
package fr.irisa.atsyra.building.provider;


import fr.irisa.atsyra.building.Building;
import fr.irisa.atsyra.building.BuildingFactory;
import fr.irisa.atsyra.building.BuildingPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.irisa.atsyra.building.Building} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BuildingItemProvider extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildingItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addZonesPropertyDescriptor(object);
			addAlarmsPropertyDescriptor(object);
			addAccessesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Zones feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addZonesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Building_zones_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Building_zones_feature", "_UI_Building_type"),
				 BuildingPackage.Literals.BUILDING__ZONES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Alarms feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAlarmsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Building_alarms_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Building_alarms_feature", "_UI_Building_type"),
				 BuildingPackage.Literals.BUILDING__ALARMS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Accesses feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAccessesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Building_accesses_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Building_accesses_feature", "_UI_Building_type"),
				 BuildingPackage.Literals.BUILDING__ACCESSES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(BuildingPackage.Literals.BUILDING__ZONES);
			childrenFeatures.add(BuildingPackage.Literals.BUILDING__ALARMS);
			childrenFeatures.add(BuildingPackage.Literals.BUILDING__ACCESSES);
			childrenFeatures.add(BuildingPackage.Literals.BUILDING__ITEMS);
			childrenFeatures.add(BuildingPackage.Literals.BUILDING__ATTACKERS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Building.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Building"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Building)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Building_type") :
			getString("_UI_Building_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Building.class)) {
			case BuildingPackage.BUILDING__ZONES:
			case BuildingPackage.BUILDING__ALARMS:
			case BuildingPackage.BUILDING__ACCESSES:
			case BuildingPackage.BUILDING__ITEMS:
			case BuildingPackage.BUILDING__ATTACKERS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(BuildingPackage.Literals.BUILDING__ZONES,
				 BuildingFactory.eINSTANCE.createZone()));

		newChildDescriptors.add
			(createChildParameter
				(BuildingPackage.Literals.BUILDING__ALARMS,
				 BuildingFactory.eINSTANCE.createAlarm()));

		newChildDescriptors.add
			(createChildParameter
				(BuildingPackage.Literals.BUILDING__ACCESSES,
				 BuildingFactory.eINSTANCE.createVirtual()));

		newChildDescriptors.add
			(createChildParameter
				(BuildingPackage.Literals.BUILDING__ACCESSES,
				 BuildingFactory.eINSTANCE.createDoor()));

		newChildDescriptors.add
			(createChildParameter
				(BuildingPackage.Literals.BUILDING__ACCESSES,
				 BuildingFactory.eINSTANCE.createWindow()));

		newChildDescriptors.add
			(createChildParameter
				(BuildingPackage.Literals.BUILDING__ACCESSES,
				 BuildingFactory.eINSTANCE.createBadgedDoor()));

		newChildDescriptors.add
			(createChildParameter
				(BuildingPackage.Literals.BUILDING__ITEMS,
				 BuildingFactory.eINSTANCE.createItem()));

		newChildDescriptors.add
			(createChildParameter
				(BuildingPackage.Literals.BUILDING__ATTACKERS,
				 BuildingFactory.eINSTANCE.createAttacker()));
	}

}
