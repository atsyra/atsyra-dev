/*******************************************************************************
 * Copyright (c) 2014, 2023 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.absystem.ide.ui.perspectives;

import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.sirius.ui.tools.api.views.modelexplorerview.IModelExplorerView;
import org.eclipse.sirius.ui.tools.internal.wizards.ModelingProjectWizard;
import org.eclipse.sirius.ui.tools.internal.wizards.NewSessionWizard;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.IConsoleConstants;



/**
 *  This class is meant to serve as an example for how various contributions 
 *  are made to a perspective. Note that some of the extension point id's are
 *  referred to as API constants while others are hardcoded and may be subject 
 *  to change. 
 */
public class ATSyRAPerspective implements IPerspectiveFactory {

	private IPageLayout factory;

	public ATSyRAPerspective() {
		super();
	}

	public void createInitialLayout(IPageLayout factory) {
		this.factory = factory;
		addViews();
		addActionSets();
		addNewWizardShortcuts();
		addPerspectiveShortcuts();
		addViewShortcuts();
	}

	private void addViews() {
		// Creates the overall folder layout. 
		// Note that each new Folder uses a percentage of the remaining EditorArea.
		/*
		IFolderLayout bottom =
			factory.createFolder(
				"bottomRight", //NON-NLS-1
				IPageLayout.BOTTOM,
				0.75f,
				factory.getEditorArea());
		bottom.addView(IPageLayout.ID_PROBLEM_VIEW);
		bottom.addView("org.eclipse.team.ui.GenericHistoryView"); //NON-NLS-1
		bottom.addView(IConsoleConstants.ID_CONSOLE_VIEW); //NON-NLS-1
		//bottom.addPlaceholder(IConsoleConstants.ID_CONSOLE_VIEW);

		IFolderLayout topLeft =
			factory.createFolder(
				"topLeft", //NON-NLS-1
				IPageLayout.LEFT,
				0.25f,
				factory.getEditorArea());
		topLeft.addView(IPageLayout.ID_RES_NAV);
		topLeft.addView("org.eclipse.jdt.junit.ResultView"); //NON-NLS-1
		
		factory.addFastView("org.eclipse.team.ccvs.ui.RepositoriesView",0.50f); //NON-NLS-1
		factory.addFastView("org.eclipse.team.sync.views.SynchronizeView", 0.50f); //NON-NLS-1
		*/
		
		// Left
        IFolderLayout topLeft = factory.createFolder("topLeft", //$NON-NLS-1$
				IPageLayout.LEFT, 0.25f, factory.getEditorArea());
		topLeft.addView(IPageLayout.ID_PROJECT_EXPLORER);
		topLeft.addPlaceholder(IModelExplorerView.ID);
		
		// Bottom
		IFolderLayout bottom = factory.createFolder("bottom", //$NON-NLS-1$
				IPageLayout.BOTTOM, 0.75f, factory.getEditorArea());
        bottom.addView(IPageLayout.ID_PROP_SHEET);
		bottom.addView(IPageLayout.ID_PROBLEM_VIEW);
		bottom.addView(IConsoleConstants.ID_CONSOLE_VIEW);
		bottom.addView("org.eclipse.pde.runtime.LogView");
		bottom.addView("net.sourceforge.plantuml.eclipse.views.PlantUmlView");
		//bottom.addPlaceholder(IConsoleConstants.ID_CONSOLE_VIEW);
        
		// Bottom Right
		//IFolderLayout bottomright = factory.createFolder("bottomRight", IPageLayout.RIGHT, (float) 0.60, "bottom"); //$NON-NLS-1$ //$NON-NLS-2$
		//bottomright.addPlaceholder("org.eclipse.emf.ecoretools.internal.views.EClassHierarchyView");
		//bottomright.addPlaceholder("org.eclipse.emf.ecoretools.internal.views.EReferencesView");
        
		// Right
		IFolderLayout outlineFolder = factory.createFolder("right", IPageLayout.RIGHT, (float)0.75, factory.getEditorArea()); //$NON-NLS-1$
		outlineFolder.addView(IPageLayout.ID_OUTLINE);

	}

	private void addActionSets() {
		factory.addActionSet("org.eclipse.debug.ui.launchActionSet"); //NON-NLS-1
		factory.addActionSet("org.eclipse.debug.ui.debugActionSet"); //NON-NLS-1
		factory.addActionSet("org.eclipse.debug.ui.profileActionSet"); //NON-NLS-1
		factory.addActionSet("org.eclipse.jdt.debug.ui.JDTDebugActionSet"); //NON-NLS-1
		factory.addActionSet("org.eclipse.team.ui.actionSet"); //NON-NLS-1
		//factory.addActionSet(JavaUI.ID_ACTION_SET);
		//factory.addActionSet(JavaUI.ID_ELEMENT_CREATION_ACTION_SET);
		factory.addActionSet(IPageLayout.ID_NAVIGATE_ACTION_SET); //NON-NLS-1
	}

	private void addPerspectiveShortcuts() {
     	factory.addPerspectiveShortcut(IDebugUIConstants.ID_DEBUG_PERSPECTIVE);
		factory.addPerspectiveShortcut("org.eclipse.team.ui.TeamSynchronizingPerspective"); //NON-NLS-1
		factory.addPerspectiveShortcut("org.eclipse.egit.ui.GitRepositoryExploring");
		factory.addPerspectiveShortcut("org.eclipse.ui.resourcePerspective"); //NON-NLS-1
        factory.addPerspectiveShortcut("org.eclipse.pde.ui.PDEPerspective");
	}

	private void addNewWizardShortcuts() {
        // project wizards
        factory.addNewWizardShortcut("fr.irisa.atsyra.absystem.xtext.ui.wizard.AssetBasedSystemDslNewProjectWizard");
        factory.addNewWizardShortcut("org.eclipse.pde.ui.NewProjectWizard");
        factory.addNewWizardShortcut(ModelingProjectWizard.ID);
        //factory.addNewWizardShortcut("fr.irisa.atsyra.topoplan.ui.importTopoPlanNewProjectWizard");
        // project content wizards
		factory.addNewWizardShortcut("org.eclipse.ui.wizards.new.folder");//NON-NLS-1
		factory.addNewWizardShortcut("fr.irisa.atsyra.absystem.xtext.ui.wizard.AssetBasedSystemDslNewFileWizard");//NON-NLS-1
		factory.addNewWizardShortcut("org.eclipse.ui.wizards.new.file");//NON-NLS-1
		factory.addNewWizardShortcut(NewSessionWizard.ID);
	}

	private void addViewShortcuts() {
		factory.addShowViewShortcut("net.sourceforge.plantuml.eclipse.views.PlantUmlView"); //NON-NLS-1
		factory.addShowViewShortcut("org.eclipse.egit.ui.StagingView"); //NON-NLS-1
		factory.addShowViewShortcut(IConsoleConstants.ID_CONSOLE_VIEW);
        factory.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);
		factory.addShowViewShortcut(IPageLayout.ID_PROJECT_EXPLORER);
		factory.addShowViewShortcut(IPageLayout.ID_PROGRESS_VIEW);
		factory.addShowViewShortcut(IPageLayout.ID_PROBLEM_VIEW);
		factory.addShowViewShortcut(IPageLayout.ID_OUTLINE);
		factory.addShowViewShortcut("org.eclipse.pde.runtime.LogView");
	}

}
