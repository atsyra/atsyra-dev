/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.witness.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.irisa.atsyra.witness.services.WitnessGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalWitnessParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_NUMBER", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'defaultValues'", "'witness'", "'{'", "'goal'", "'steps'", "'}'", "'states'", "'sequence_diagram'", "'@startuml'", "'@enduml'", "'->'", "':'", "'[*'", "'/'", "'*]'", "'['", "']'", "'='", "'::'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_NUMBER=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalWitnessParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalWitnessParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalWitnessParser.tokenNames; }
    public String getGrammarFileName() { return "InternalWitness.g"; }


    	private WitnessGrammarAccess grammarAccess;

    	public void setGrammarAccess(WitnessGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleWitnessModel"
    // InternalWitness.g:53:1: entryRuleWitnessModel : ruleWitnessModel EOF ;
    public final void entryRuleWitnessModel() throws RecognitionException {
        try {
            // InternalWitness.g:54:1: ( ruleWitnessModel EOF )
            // InternalWitness.g:55:1: ruleWitnessModel EOF
            {
             before(grammarAccess.getWitnessModelRule()); 
            pushFollow(FOLLOW_1);
            ruleWitnessModel();

            state._fsp--;

             after(grammarAccess.getWitnessModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWitnessModel"


    // $ANTLR start "ruleWitnessModel"
    // InternalWitness.g:62:1: ruleWitnessModel : ( ( rule__WitnessModel__WitnessesAssignment )* ) ;
    public final void ruleWitnessModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:66:2: ( ( ( rule__WitnessModel__WitnessesAssignment )* ) )
            // InternalWitness.g:67:2: ( ( rule__WitnessModel__WitnessesAssignment )* )
            {
            // InternalWitness.g:67:2: ( ( rule__WitnessModel__WitnessesAssignment )* )
            // InternalWitness.g:68:3: ( rule__WitnessModel__WitnessesAssignment )*
            {
             before(grammarAccess.getWitnessModelAccess().getWitnessesAssignment()); 
            // InternalWitness.g:69:3: ( rule__WitnessModel__WitnessesAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalWitness.g:69:4: rule__WitnessModel__WitnessesAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__WitnessModel__WitnessesAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getWitnessModelAccess().getWitnessesAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWitnessModel"


    // $ANTLR start "entryRuleWitness"
    // InternalWitness.g:78:1: entryRuleWitness : ruleWitness EOF ;
    public final void entryRuleWitness() throws RecognitionException {
        try {
            // InternalWitness.g:79:1: ( ruleWitness EOF )
            // InternalWitness.g:80:1: ruleWitness EOF
            {
             before(grammarAccess.getWitnessRule()); 
            pushFollow(FOLLOW_1);
            ruleWitness();

            state._fsp--;

             after(grammarAccess.getWitnessRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWitness"


    // $ANTLR start "ruleWitness"
    // InternalWitness.g:87:1: ruleWitness : ( ( rule__Witness__Group__0 ) ) ;
    public final void ruleWitness() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:91:2: ( ( ( rule__Witness__Group__0 ) ) )
            // InternalWitness.g:92:2: ( ( rule__Witness__Group__0 ) )
            {
            // InternalWitness.g:92:2: ( ( rule__Witness__Group__0 ) )
            // InternalWitness.g:93:3: ( rule__Witness__Group__0 )
            {
             before(grammarAccess.getWitnessAccess().getGroup()); 
            // InternalWitness.g:94:3: ( rule__Witness__Group__0 )
            // InternalWitness.g:94:4: rule__Witness__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Witness__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWitnessAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWitness"


    // $ANTLR start "entryRuleSequenceRule"
    // InternalWitness.g:103:1: entryRuleSequenceRule : ruleSequenceRule EOF ;
    public final void entryRuleSequenceRule() throws RecognitionException {
        try {
            // InternalWitness.g:104:1: ( ruleSequenceRule EOF )
            // InternalWitness.g:105:1: ruleSequenceRule EOF
            {
             before(grammarAccess.getSequenceRuleRule()); 
            pushFollow(FOLLOW_1);
            ruleSequenceRule();

            state._fsp--;

             after(grammarAccess.getSequenceRuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSequenceRule"


    // $ANTLR start "ruleSequenceRule"
    // InternalWitness.g:112:1: ruleSequenceRule : ( ( rule__SequenceRule__Group__0 ) ) ;
    public final void ruleSequenceRule() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:116:2: ( ( ( rule__SequenceRule__Group__0 ) ) )
            // InternalWitness.g:117:2: ( ( rule__SequenceRule__Group__0 ) )
            {
            // InternalWitness.g:117:2: ( ( rule__SequenceRule__Group__0 ) )
            // InternalWitness.g:118:3: ( rule__SequenceRule__Group__0 )
            {
             before(grammarAccess.getSequenceRuleAccess().getGroup()); 
            // InternalWitness.g:119:3: ( rule__SequenceRule__Group__0 )
            // InternalWitness.g:119:4: rule__SequenceRule__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SequenceRule__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSequenceRuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSequenceRule"


    // $ANTLR start "entryRuleStep"
    // InternalWitness.g:128:1: entryRuleStep : ruleStep EOF ;
    public final void entryRuleStep() throws RecognitionException {
        try {
            // InternalWitness.g:129:1: ( ruleStep EOF )
            // InternalWitness.g:130:1: ruleStep EOF
            {
             before(grammarAccess.getStepRule()); 
            pushFollow(FOLLOW_1);
            ruleStep();

            state._fsp--;

             after(grammarAccess.getStepRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStep"


    // $ANTLR start "ruleStep"
    // InternalWitness.g:137:1: ruleStep : ( ( rule__Step__NameAssignment ) ) ;
    public final void ruleStep() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:141:2: ( ( ( rule__Step__NameAssignment ) ) )
            // InternalWitness.g:142:2: ( ( rule__Step__NameAssignment ) )
            {
            // InternalWitness.g:142:2: ( ( rule__Step__NameAssignment ) )
            // InternalWitness.g:143:3: ( rule__Step__NameAssignment )
            {
             before(grammarAccess.getStepAccess().getNameAssignment()); 
            // InternalWitness.g:144:3: ( rule__Step__NameAssignment )
            // InternalWitness.g:144:4: rule__Step__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Step__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getStepAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStep"


    // $ANTLR start "entryRuleAbstractStepStates"
    // InternalWitness.g:153:1: entryRuleAbstractStepStates : ruleAbstractStepStates EOF ;
    public final void entryRuleAbstractStepStates() throws RecognitionException {
        try {
            // InternalWitness.g:154:1: ( ruleAbstractStepStates EOF )
            // InternalWitness.g:155:1: ruleAbstractStepStates EOF
            {
             before(grammarAccess.getAbstractStepStatesRule()); 
            pushFollow(FOLLOW_1);
            ruleAbstractStepStates();

            state._fsp--;

             after(grammarAccess.getAbstractStepStatesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractStepStates"


    // $ANTLR start "ruleAbstractStepStates"
    // InternalWitness.g:162:1: ruleAbstractStepStates : ( ( rule__AbstractStepStates__Alternatives ) ) ;
    public final void ruleAbstractStepStates() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:166:2: ( ( ( rule__AbstractStepStates__Alternatives ) ) )
            // InternalWitness.g:167:2: ( ( rule__AbstractStepStates__Alternatives ) )
            {
            // InternalWitness.g:167:2: ( ( rule__AbstractStepStates__Alternatives ) )
            // InternalWitness.g:168:3: ( rule__AbstractStepStates__Alternatives )
            {
             before(grammarAccess.getAbstractStepStatesAccess().getAlternatives()); 
            // InternalWitness.g:169:3: ( rule__AbstractStepStates__Alternatives )
            // InternalWitness.g:169:4: rule__AbstractStepStates__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractStepStates__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAbstractStepStatesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractStepStates"


    // $ANTLR start "entryRuleStepStatesCount"
    // InternalWitness.g:178:1: entryRuleStepStatesCount : ruleStepStatesCount EOF ;
    public final void entryRuleStepStatesCount() throws RecognitionException {
        try {
            // InternalWitness.g:179:1: ( ruleStepStatesCount EOF )
            // InternalWitness.g:180:1: ruleStepStatesCount EOF
            {
             before(grammarAccess.getStepStatesCountRule()); 
            pushFollow(FOLLOW_1);
            ruleStepStatesCount();

            state._fsp--;

             after(grammarAccess.getStepStatesCountRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStepStatesCount"


    // $ANTLR start "ruleStepStatesCount"
    // InternalWitness.g:187:1: ruleStepStatesCount : ( ( rule__StepStatesCount__Group__0 ) ) ;
    public final void ruleStepStatesCount() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:191:2: ( ( ( rule__StepStatesCount__Group__0 ) ) )
            // InternalWitness.g:192:2: ( ( rule__StepStatesCount__Group__0 ) )
            {
            // InternalWitness.g:192:2: ( ( rule__StepStatesCount__Group__0 ) )
            // InternalWitness.g:193:3: ( rule__StepStatesCount__Group__0 )
            {
             before(grammarAccess.getStepStatesCountAccess().getGroup()); 
            // InternalWitness.g:194:3: ( rule__StepStatesCount__Group__0 )
            // InternalWitness.g:194:4: rule__StepStatesCount__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStepStatesCountAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStepStatesCount"


    // $ANTLR start "entryRuleStepStates"
    // InternalWitness.g:203:1: entryRuleStepStates : ruleStepStates EOF ;
    public final void entryRuleStepStates() throws RecognitionException {
        try {
            // InternalWitness.g:204:1: ( ruleStepStates EOF )
            // InternalWitness.g:205:1: ruleStepStates EOF
            {
             before(grammarAccess.getStepStatesRule()); 
            pushFollow(FOLLOW_1);
            ruleStepStates();

            state._fsp--;

             after(grammarAccess.getStepStatesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStepStates"


    // $ANTLR start "ruleStepStates"
    // InternalWitness.g:212:1: ruleStepStates : ( ( rule__StepStates__Group__0 ) ) ;
    public final void ruleStepStates() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:216:2: ( ( ( rule__StepStates__Group__0 ) ) )
            // InternalWitness.g:217:2: ( ( rule__StepStates__Group__0 ) )
            {
            // InternalWitness.g:217:2: ( ( rule__StepStates__Group__0 ) )
            // InternalWitness.g:218:3: ( rule__StepStates__Group__0 )
            {
             before(grammarAccess.getStepStatesAccess().getGroup()); 
            // InternalWitness.g:219:3: ( rule__StepStates__Group__0 )
            // InternalWitness.g:219:4: rule__StepStates__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StepStates__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStepStatesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStepStates"


    // $ANTLR start "entryRuleStepState"
    // InternalWitness.g:228:1: entryRuleStepState : ruleStepState EOF ;
    public final void entryRuleStepState() throws RecognitionException {
        try {
            // InternalWitness.g:229:1: ( ruleStepState EOF )
            // InternalWitness.g:230:1: ruleStepState EOF
            {
             before(grammarAccess.getStepStateRule()); 
            pushFollow(FOLLOW_1);
            ruleStepState();

            state._fsp--;

             after(grammarAccess.getStepStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStepState"


    // $ANTLR start "ruleStepState"
    // InternalWitness.g:237:1: ruleStepState : ( ( rule__StepState__Group__0 ) ) ;
    public final void ruleStepState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:241:2: ( ( ( rule__StepState__Group__0 ) ) )
            // InternalWitness.g:242:2: ( ( rule__StepState__Group__0 ) )
            {
            // InternalWitness.g:242:2: ( ( rule__StepState__Group__0 ) )
            // InternalWitness.g:243:3: ( rule__StepState__Group__0 )
            {
             before(grammarAccess.getStepStateAccess().getGroup()); 
            // InternalWitness.g:244:3: ( rule__StepState__Group__0 )
            // InternalWitness.g:244:4: rule__StepState__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StepState__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStepStateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStepState"


    // $ANTLR start "entryRuleVarState"
    // InternalWitness.g:253:1: entryRuleVarState : ruleVarState EOF ;
    public final void entryRuleVarState() throws RecognitionException {
        try {
            // InternalWitness.g:254:1: ( ruleVarState EOF )
            // InternalWitness.g:255:1: ruleVarState EOF
            {
             before(grammarAccess.getVarStateRule()); 
            pushFollow(FOLLOW_1);
            ruleVarState();

            state._fsp--;

             after(grammarAccess.getVarStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVarState"


    // $ANTLR start "ruleVarState"
    // InternalWitness.g:262:1: ruleVarState : ( ( rule__VarState__Group__0 ) ) ;
    public final void ruleVarState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:266:2: ( ( ( rule__VarState__Group__0 ) ) )
            // InternalWitness.g:267:2: ( ( rule__VarState__Group__0 ) )
            {
            // InternalWitness.g:267:2: ( ( rule__VarState__Group__0 ) )
            // InternalWitness.g:268:3: ( rule__VarState__Group__0 )
            {
             before(grammarAccess.getVarStateAccess().getGroup()); 
            // InternalWitness.g:269:3: ( rule__VarState__Group__0 )
            // InternalWitness.g:269:4: rule__VarState__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__VarState__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVarStateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVarState"


    // $ANTLR start "entryRuleLong"
    // InternalWitness.g:278:1: entryRuleLong : ruleLong EOF ;
    public final void entryRuleLong() throws RecognitionException {
        try {
            // InternalWitness.g:279:1: ( ruleLong EOF )
            // InternalWitness.g:280:1: ruleLong EOF
            {
             before(grammarAccess.getLongRule()); 
            pushFollow(FOLLOW_1);
            ruleLong();

            state._fsp--;

             after(grammarAccess.getLongRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLong"


    // $ANTLR start "ruleLong"
    // InternalWitness.g:287:1: ruleLong : ( RULE_NUMBER ) ;
    public final void ruleLong() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:291:2: ( ( RULE_NUMBER ) )
            // InternalWitness.g:292:2: ( RULE_NUMBER )
            {
            // InternalWitness.g:292:2: ( RULE_NUMBER )
            // InternalWitness.g:293:3: RULE_NUMBER
            {
             before(grammarAccess.getLongAccess().getNUMBERTerminalRuleCall()); 
            match(input,RULE_NUMBER,FOLLOW_2); 
             after(grammarAccess.getLongAccess().getNUMBERTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLong"


    // $ANTLR start "entryRuleInt"
    // InternalWitness.g:303:1: entryRuleInt : ruleInt EOF ;
    public final void entryRuleInt() throws RecognitionException {
        try {
            // InternalWitness.g:304:1: ( ruleInt EOF )
            // InternalWitness.g:305:1: ruleInt EOF
            {
             before(grammarAccess.getIntRule()); 
            pushFollow(FOLLOW_1);
            ruleInt();

            state._fsp--;

             after(grammarAccess.getIntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInt"


    // $ANTLR start "ruleInt"
    // InternalWitness.g:312:1: ruleInt : ( RULE_NUMBER ) ;
    public final void ruleInt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:316:2: ( ( RULE_NUMBER ) )
            // InternalWitness.g:317:2: ( RULE_NUMBER )
            {
            // InternalWitness.g:317:2: ( RULE_NUMBER )
            // InternalWitness.g:318:3: RULE_NUMBER
            {
             before(grammarAccess.getIntAccess().getNUMBERTerminalRuleCall()); 
            match(input,RULE_NUMBER,FOLLOW_2); 
             after(grammarAccess.getIntAccess().getNUMBERTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInt"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalWitness.g:328:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalWitness.g:329:1: ( ruleQualifiedName EOF )
            // InternalWitness.g:330:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalWitness.g:337:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:341:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalWitness.g:342:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalWitness.g:342:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalWitness.g:343:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalWitness.g:344:3: ( rule__QualifiedName__Group__0 )
            // InternalWitness.g:344:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "rule__AbstractStepStates__Alternatives"
    // InternalWitness.g:352:1: rule__AbstractStepStates__Alternatives : ( ( ruleStepStatesCount ) | ( ruleStepStates ) );
    public final void rule__AbstractStepStates__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:356:1: ( ( ruleStepStatesCount ) | ( ruleStepStates ) )
            int alt2=2;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // InternalWitness.g:357:2: ( ruleStepStatesCount )
                    {
                    // InternalWitness.g:357:2: ( ruleStepStatesCount )
                    // InternalWitness.g:358:3: ruleStepStatesCount
                    {
                     before(grammarAccess.getAbstractStepStatesAccess().getStepStatesCountParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleStepStatesCount();

                    state._fsp--;

                     after(grammarAccess.getAbstractStepStatesAccess().getStepStatesCountParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalWitness.g:363:2: ( ruleStepStates )
                    {
                    // InternalWitness.g:363:2: ( ruleStepStates )
                    // InternalWitness.g:364:3: ruleStepStates
                    {
                     before(grammarAccess.getAbstractStepStatesAccess().getStepStatesParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleStepStates();

                    state._fsp--;

                     after(grammarAccess.getAbstractStepStatesAccess().getStepStatesParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractStepStates__Alternatives"


    // $ANTLR start "rule__StepStatesCount__Alternatives_2"
    // InternalWitness.g:373:1: rule__StepStatesCount__Alternatives_2 : ( ( ( rule__StepStatesCount__ReferencedStepAssignment_2_0 ) ) | ( 'defaultValues' ) );
    public final void rule__StepStatesCount__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:377:1: ( ( ( rule__StepStatesCount__ReferencedStepAssignment_2_0 ) ) | ( 'defaultValues' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            else if ( (LA3_0==11) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalWitness.g:378:2: ( ( rule__StepStatesCount__ReferencedStepAssignment_2_0 ) )
                    {
                    // InternalWitness.g:378:2: ( ( rule__StepStatesCount__ReferencedStepAssignment_2_0 ) )
                    // InternalWitness.g:379:3: ( rule__StepStatesCount__ReferencedStepAssignment_2_0 )
                    {
                     before(grammarAccess.getStepStatesCountAccess().getReferencedStepAssignment_2_0()); 
                    // InternalWitness.g:380:3: ( rule__StepStatesCount__ReferencedStepAssignment_2_0 )
                    // InternalWitness.g:380:4: rule__StepStatesCount__ReferencedStepAssignment_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StepStatesCount__ReferencedStepAssignment_2_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStepStatesCountAccess().getReferencedStepAssignment_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalWitness.g:384:2: ( 'defaultValues' )
                    {
                    // InternalWitness.g:384:2: ( 'defaultValues' )
                    // InternalWitness.g:385:3: 'defaultValues'
                    {
                     before(grammarAccess.getStepStatesCountAccess().getDefaultValuesKeyword_2_1()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getStepStatesCountAccess().getDefaultValuesKeyword_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Alternatives_2"


    // $ANTLR start "rule__StepStates__Alternatives_1"
    // InternalWitness.g:394:1: rule__StepStates__Alternatives_1 : ( ( ( rule__StepStates__ReferencedStepAssignment_1_0 ) ) | ( 'defaultValues' ) );
    public final void rule__StepStates__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:398:1: ( ( ( rule__StepStates__ReferencedStepAssignment_1_0 ) ) | ( 'defaultValues' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            else if ( (LA4_0==11) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalWitness.g:399:2: ( ( rule__StepStates__ReferencedStepAssignment_1_0 ) )
                    {
                    // InternalWitness.g:399:2: ( ( rule__StepStates__ReferencedStepAssignment_1_0 ) )
                    // InternalWitness.g:400:3: ( rule__StepStates__ReferencedStepAssignment_1_0 )
                    {
                     before(grammarAccess.getStepStatesAccess().getReferencedStepAssignment_1_0()); 
                    // InternalWitness.g:401:3: ( rule__StepStates__ReferencedStepAssignment_1_0 )
                    // InternalWitness.g:401:4: rule__StepStates__ReferencedStepAssignment_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StepStates__ReferencedStepAssignment_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStepStatesAccess().getReferencedStepAssignment_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalWitness.g:405:2: ( 'defaultValues' )
                    {
                    // InternalWitness.g:405:2: ( 'defaultValues' )
                    // InternalWitness.g:406:3: 'defaultValues'
                    {
                     before(grammarAccess.getStepStatesAccess().getDefaultValuesKeyword_1_1()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getStepStatesAccess().getDefaultValuesKeyword_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__Alternatives_1"


    // $ANTLR start "rule__Witness__Group__0"
    // InternalWitness.g:415:1: rule__Witness__Group__0 : rule__Witness__Group__0__Impl rule__Witness__Group__1 ;
    public final void rule__Witness__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:419:1: ( rule__Witness__Group__0__Impl rule__Witness__Group__1 )
            // InternalWitness.g:420:2: rule__Witness__Group__0__Impl rule__Witness__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Witness__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__0"


    // $ANTLR start "rule__Witness__Group__0__Impl"
    // InternalWitness.g:427:1: rule__Witness__Group__0__Impl : ( 'witness' ) ;
    public final void rule__Witness__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:431:1: ( ( 'witness' ) )
            // InternalWitness.g:432:1: ( 'witness' )
            {
            // InternalWitness.g:432:1: ( 'witness' )
            // InternalWitness.g:433:2: 'witness'
            {
             before(grammarAccess.getWitnessAccess().getWitnessKeyword_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getWitnessKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__0__Impl"


    // $ANTLR start "rule__Witness__Group__1"
    // InternalWitness.g:442:1: rule__Witness__Group__1 : rule__Witness__Group__1__Impl rule__Witness__Group__2 ;
    public final void rule__Witness__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:446:1: ( rule__Witness__Group__1__Impl rule__Witness__Group__2 )
            // InternalWitness.g:447:2: rule__Witness__Group__1__Impl rule__Witness__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Witness__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__1"


    // $ANTLR start "rule__Witness__Group__1__Impl"
    // InternalWitness.g:454:1: rule__Witness__Group__1__Impl : ( ( rule__Witness__NameAssignment_1 ) ) ;
    public final void rule__Witness__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:458:1: ( ( ( rule__Witness__NameAssignment_1 ) ) )
            // InternalWitness.g:459:1: ( ( rule__Witness__NameAssignment_1 ) )
            {
            // InternalWitness.g:459:1: ( ( rule__Witness__NameAssignment_1 ) )
            // InternalWitness.g:460:2: ( rule__Witness__NameAssignment_1 )
            {
             before(grammarAccess.getWitnessAccess().getNameAssignment_1()); 
            // InternalWitness.g:461:2: ( rule__Witness__NameAssignment_1 )
            // InternalWitness.g:461:3: rule__Witness__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Witness__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWitnessAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__1__Impl"


    // $ANTLR start "rule__Witness__Group__2"
    // InternalWitness.g:469:1: rule__Witness__Group__2 : rule__Witness__Group__2__Impl rule__Witness__Group__3 ;
    public final void rule__Witness__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:473:1: ( rule__Witness__Group__2__Impl rule__Witness__Group__3 )
            // InternalWitness.g:474:2: rule__Witness__Group__2__Impl rule__Witness__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Witness__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__2"


    // $ANTLR start "rule__Witness__Group__2__Impl"
    // InternalWitness.g:481:1: rule__Witness__Group__2__Impl : ( '{' ) ;
    public final void rule__Witness__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:485:1: ( ( '{' ) )
            // InternalWitness.g:486:1: ( '{' )
            {
            // InternalWitness.g:486:1: ( '{' )
            // InternalWitness.g:487:2: '{'
            {
             before(grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__2__Impl"


    // $ANTLR start "rule__Witness__Group__3"
    // InternalWitness.g:496:1: rule__Witness__Group__3 : rule__Witness__Group__3__Impl rule__Witness__Group__4 ;
    public final void rule__Witness__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:500:1: ( rule__Witness__Group__3__Impl rule__Witness__Group__4 )
            // InternalWitness.g:501:2: rule__Witness__Group__3__Impl rule__Witness__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__Witness__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__3"


    // $ANTLR start "rule__Witness__Group__3__Impl"
    // InternalWitness.g:508:1: rule__Witness__Group__3__Impl : ( 'goal' ) ;
    public final void rule__Witness__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:512:1: ( ( 'goal' ) )
            // InternalWitness.g:513:1: ( 'goal' )
            {
            // InternalWitness.g:513:1: ( 'goal' )
            // InternalWitness.g:514:2: 'goal'
            {
             before(grammarAccess.getWitnessAccess().getGoalKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getGoalKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__3__Impl"


    // $ANTLR start "rule__Witness__Group__4"
    // InternalWitness.g:523:1: rule__Witness__Group__4 : rule__Witness__Group__4__Impl rule__Witness__Group__5 ;
    public final void rule__Witness__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:527:1: ( rule__Witness__Group__4__Impl rule__Witness__Group__5 )
            // InternalWitness.g:528:2: rule__Witness__Group__4__Impl rule__Witness__Group__5
            {
            pushFollow(FOLLOW_8);
            rule__Witness__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__4"


    // $ANTLR start "rule__Witness__Group__4__Impl"
    // InternalWitness.g:535:1: rule__Witness__Group__4__Impl : ( ( rule__Witness__GoalNameAssignment_4 ) ) ;
    public final void rule__Witness__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:539:1: ( ( ( rule__Witness__GoalNameAssignment_4 ) ) )
            // InternalWitness.g:540:1: ( ( rule__Witness__GoalNameAssignment_4 ) )
            {
            // InternalWitness.g:540:1: ( ( rule__Witness__GoalNameAssignment_4 ) )
            // InternalWitness.g:541:2: ( rule__Witness__GoalNameAssignment_4 )
            {
             before(grammarAccess.getWitnessAccess().getGoalNameAssignment_4()); 
            // InternalWitness.g:542:2: ( rule__Witness__GoalNameAssignment_4 )
            // InternalWitness.g:542:3: rule__Witness__GoalNameAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Witness__GoalNameAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getWitnessAccess().getGoalNameAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__4__Impl"


    // $ANTLR start "rule__Witness__Group__5"
    // InternalWitness.g:550:1: rule__Witness__Group__5 : rule__Witness__Group__5__Impl rule__Witness__Group__6 ;
    public final void rule__Witness__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:554:1: ( rule__Witness__Group__5__Impl rule__Witness__Group__6 )
            // InternalWitness.g:555:2: rule__Witness__Group__5__Impl rule__Witness__Group__6
            {
            pushFollow(FOLLOW_5);
            rule__Witness__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__5"


    // $ANTLR start "rule__Witness__Group__5__Impl"
    // InternalWitness.g:562:1: rule__Witness__Group__5__Impl : ( 'steps' ) ;
    public final void rule__Witness__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:566:1: ( ( 'steps' ) )
            // InternalWitness.g:567:1: ( 'steps' )
            {
            // InternalWitness.g:567:1: ( 'steps' )
            // InternalWitness.g:568:2: 'steps'
            {
             before(grammarAccess.getWitnessAccess().getStepsKeyword_5()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getStepsKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__5__Impl"


    // $ANTLR start "rule__Witness__Group__6"
    // InternalWitness.g:577:1: rule__Witness__Group__6 : rule__Witness__Group__6__Impl rule__Witness__Group__7 ;
    public final void rule__Witness__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:581:1: ( rule__Witness__Group__6__Impl rule__Witness__Group__7 )
            // InternalWitness.g:582:2: rule__Witness__Group__6__Impl rule__Witness__Group__7
            {
            pushFollow(FOLLOW_9);
            rule__Witness__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__6"


    // $ANTLR start "rule__Witness__Group__6__Impl"
    // InternalWitness.g:589:1: rule__Witness__Group__6__Impl : ( '{' ) ;
    public final void rule__Witness__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:593:1: ( ( '{' ) )
            // InternalWitness.g:594:1: ( '{' )
            {
            // InternalWitness.g:594:1: ( '{' )
            // InternalWitness.g:595:2: '{'
            {
             before(grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_6()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__6__Impl"


    // $ANTLR start "rule__Witness__Group__7"
    // InternalWitness.g:604:1: rule__Witness__Group__7 : rule__Witness__Group__7__Impl rule__Witness__Group__8 ;
    public final void rule__Witness__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:608:1: ( rule__Witness__Group__7__Impl rule__Witness__Group__8 )
            // InternalWitness.g:609:2: rule__Witness__Group__7__Impl rule__Witness__Group__8
            {
            pushFollow(FOLLOW_9);
            rule__Witness__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__7"


    // $ANTLR start "rule__Witness__Group__7__Impl"
    // InternalWitness.g:616:1: rule__Witness__Group__7__Impl : ( ( rule__Witness__StepsAssignment_7 )* ) ;
    public final void rule__Witness__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:620:1: ( ( ( rule__Witness__StepsAssignment_7 )* ) )
            // InternalWitness.g:621:1: ( ( rule__Witness__StepsAssignment_7 )* )
            {
            // InternalWitness.g:621:1: ( ( rule__Witness__StepsAssignment_7 )* )
            // InternalWitness.g:622:2: ( rule__Witness__StepsAssignment_7 )*
            {
             before(grammarAccess.getWitnessAccess().getStepsAssignment_7()); 
            // InternalWitness.g:623:2: ( rule__Witness__StepsAssignment_7 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalWitness.g:623:3: rule__Witness__StepsAssignment_7
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Witness__StepsAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getWitnessAccess().getStepsAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__7__Impl"


    // $ANTLR start "rule__Witness__Group__8"
    // InternalWitness.g:631:1: rule__Witness__Group__8 : rule__Witness__Group__8__Impl rule__Witness__Group__9 ;
    public final void rule__Witness__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:635:1: ( rule__Witness__Group__8__Impl rule__Witness__Group__9 )
            // InternalWitness.g:636:2: rule__Witness__Group__8__Impl rule__Witness__Group__9
            {
            pushFollow(FOLLOW_11);
            rule__Witness__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__8"


    // $ANTLR start "rule__Witness__Group__8__Impl"
    // InternalWitness.g:643:1: rule__Witness__Group__8__Impl : ( '}' ) ;
    public final void rule__Witness__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:647:1: ( ( '}' ) )
            // InternalWitness.g:648:1: ( '}' )
            {
            // InternalWitness.g:648:1: ( '}' )
            // InternalWitness.g:649:2: '}'
            {
             before(grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_8()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__8__Impl"


    // $ANTLR start "rule__Witness__Group__9"
    // InternalWitness.g:658:1: rule__Witness__Group__9 : rule__Witness__Group__9__Impl rule__Witness__Group__10 ;
    public final void rule__Witness__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:662:1: ( rule__Witness__Group__9__Impl rule__Witness__Group__10 )
            // InternalWitness.g:663:2: rule__Witness__Group__9__Impl rule__Witness__Group__10
            {
            pushFollow(FOLLOW_5);
            rule__Witness__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__9"


    // $ANTLR start "rule__Witness__Group__9__Impl"
    // InternalWitness.g:670:1: rule__Witness__Group__9__Impl : ( 'states' ) ;
    public final void rule__Witness__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:674:1: ( ( 'states' ) )
            // InternalWitness.g:675:1: ( 'states' )
            {
            // InternalWitness.g:675:1: ( 'states' )
            // InternalWitness.g:676:2: 'states'
            {
             before(grammarAccess.getWitnessAccess().getStatesKeyword_9()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getStatesKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__9__Impl"


    // $ANTLR start "rule__Witness__Group__10"
    // InternalWitness.g:685:1: rule__Witness__Group__10 : rule__Witness__Group__10__Impl rule__Witness__Group__11 ;
    public final void rule__Witness__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:689:1: ( rule__Witness__Group__10__Impl rule__Witness__Group__11 )
            // InternalWitness.g:690:2: rule__Witness__Group__10__Impl rule__Witness__Group__11
            {
            pushFollow(FOLLOW_12);
            rule__Witness__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__10"


    // $ANTLR start "rule__Witness__Group__10__Impl"
    // InternalWitness.g:697:1: rule__Witness__Group__10__Impl : ( '{' ) ;
    public final void rule__Witness__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:701:1: ( ( '{' ) )
            // InternalWitness.g:702:1: ( '{' )
            {
            // InternalWitness.g:702:1: ( '{' )
            // InternalWitness.g:703:2: '{'
            {
             before(grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_10()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__10__Impl"


    // $ANTLR start "rule__Witness__Group__11"
    // InternalWitness.g:712:1: rule__Witness__Group__11 : rule__Witness__Group__11__Impl rule__Witness__Group__12 ;
    public final void rule__Witness__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:716:1: ( rule__Witness__Group__11__Impl rule__Witness__Group__12 )
            // InternalWitness.g:717:2: rule__Witness__Group__11__Impl rule__Witness__Group__12
            {
            pushFollow(FOLLOW_12);
            rule__Witness__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__11"


    // $ANTLR start "rule__Witness__Group__11__Impl"
    // InternalWitness.g:724:1: rule__Witness__Group__11__Impl : ( ( rule__Witness__StepStatesAssignment_11 )* ) ;
    public final void rule__Witness__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:728:1: ( ( ( rule__Witness__StepStatesAssignment_11 )* ) )
            // InternalWitness.g:729:1: ( ( rule__Witness__StepStatesAssignment_11 )* )
            {
            // InternalWitness.g:729:1: ( ( rule__Witness__StepStatesAssignment_11 )* )
            // InternalWitness.g:730:2: ( rule__Witness__StepStatesAssignment_11 )*
            {
             before(grammarAccess.getWitnessAccess().getStepStatesAssignment_11()); 
            // InternalWitness.g:731:2: ( rule__Witness__StepStatesAssignment_11 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_NUMBER) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalWitness.g:731:3: rule__Witness__StepStatesAssignment_11
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Witness__StepStatesAssignment_11();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getWitnessAccess().getStepStatesAssignment_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__11__Impl"


    // $ANTLR start "rule__Witness__Group__12"
    // InternalWitness.g:739:1: rule__Witness__Group__12 : rule__Witness__Group__12__Impl rule__Witness__Group__13 ;
    public final void rule__Witness__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:743:1: ( rule__Witness__Group__12__Impl rule__Witness__Group__13 )
            // InternalWitness.g:744:2: rule__Witness__Group__12__Impl rule__Witness__Group__13
            {
            pushFollow(FOLLOW_14);
            rule__Witness__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__12"


    // $ANTLR start "rule__Witness__Group__12__Impl"
    // InternalWitness.g:751:1: rule__Witness__Group__12__Impl : ( '}' ) ;
    public final void rule__Witness__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:755:1: ( ( '}' ) )
            // InternalWitness.g:756:1: ( '}' )
            {
            // InternalWitness.g:756:1: ( '}' )
            // InternalWitness.g:757:2: '}'
            {
             before(grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_12()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__12__Impl"


    // $ANTLR start "rule__Witness__Group__13"
    // InternalWitness.g:766:1: rule__Witness__Group__13 : rule__Witness__Group__13__Impl rule__Witness__Group__14 ;
    public final void rule__Witness__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:770:1: ( rule__Witness__Group__13__Impl rule__Witness__Group__14 )
            // InternalWitness.g:771:2: rule__Witness__Group__13__Impl rule__Witness__Group__14
            {
            pushFollow(FOLLOW_14);
            rule__Witness__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__13"


    // $ANTLR start "rule__Witness__Group__13__Impl"
    // InternalWitness.g:778:1: rule__Witness__Group__13__Impl : ( ( rule__Witness__Group_13__0 )? ) ;
    public final void rule__Witness__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:782:1: ( ( ( rule__Witness__Group_13__0 )? ) )
            // InternalWitness.g:783:1: ( ( rule__Witness__Group_13__0 )? )
            {
            // InternalWitness.g:783:1: ( ( rule__Witness__Group_13__0 )? )
            // InternalWitness.g:784:2: ( rule__Witness__Group_13__0 )?
            {
             before(grammarAccess.getWitnessAccess().getGroup_13()); 
            // InternalWitness.g:785:2: ( rule__Witness__Group_13__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==18) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalWitness.g:785:3: rule__Witness__Group_13__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Witness__Group_13__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getWitnessAccess().getGroup_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__13__Impl"


    // $ANTLR start "rule__Witness__Group__14"
    // InternalWitness.g:793:1: rule__Witness__Group__14 : rule__Witness__Group__14__Impl ;
    public final void rule__Witness__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:797:1: ( rule__Witness__Group__14__Impl )
            // InternalWitness.g:798:2: rule__Witness__Group__14__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Witness__Group__14__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__14"


    // $ANTLR start "rule__Witness__Group__14__Impl"
    // InternalWitness.g:804:1: rule__Witness__Group__14__Impl : ( '}' ) ;
    public final void rule__Witness__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:808:1: ( ( '}' ) )
            // InternalWitness.g:809:1: ( '}' )
            {
            // InternalWitness.g:809:1: ( '}' )
            // InternalWitness.g:810:2: '}'
            {
             before(grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_14()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group__14__Impl"


    // $ANTLR start "rule__Witness__Group_13__0"
    // InternalWitness.g:820:1: rule__Witness__Group_13__0 : rule__Witness__Group_13__0__Impl rule__Witness__Group_13__1 ;
    public final void rule__Witness__Group_13__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:824:1: ( rule__Witness__Group_13__0__Impl rule__Witness__Group_13__1 )
            // InternalWitness.g:825:2: rule__Witness__Group_13__0__Impl rule__Witness__Group_13__1
            {
            pushFollow(FOLLOW_5);
            rule__Witness__Group_13__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group_13__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__0"


    // $ANTLR start "rule__Witness__Group_13__0__Impl"
    // InternalWitness.g:832:1: rule__Witness__Group_13__0__Impl : ( 'sequence_diagram' ) ;
    public final void rule__Witness__Group_13__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:836:1: ( ( 'sequence_diagram' ) )
            // InternalWitness.g:837:1: ( 'sequence_diagram' )
            {
            // InternalWitness.g:837:1: ( 'sequence_diagram' )
            // InternalWitness.g:838:2: 'sequence_diagram'
            {
             before(grammarAccess.getWitnessAccess().getSequence_diagramKeyword_13_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getSequence_diagramKeyword_13_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__0__Impl"


    // $ANTLR start "rule__Witness__Group_13__1"
    // InternalWitness.g:847:1: rule__Witness__Group_13__1 : rule__Witness__Group_13__1__Impl rule__Witness__Group_13__2 ;
    public final void rule__Witness__Group_13__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:851:1: ( rule__Witness__Group_13__1__Impl rule__Witness__Group_13__2 )
            // InternalWitness.g:852:2: rule__Witness__Group_13__1__Impl rule__Witness__Group_13__2
            {
            pushFollow(FOLLOW_15);
            rule__Witness__Group_13__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group_13__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__1"


    // $ANTLR start "rule__Witness__Group_13__1__Impl"
    // InternalWitness.g:859:1: rule__Witness__Group_13__1__Impl : ( '{' ) ;
    public final void rule__Witness__Group_13__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:863:1: ( ( '{' ) )
            // InternalWitness.g:864:1: ( '{' )
            {
            // InternalWitness.g:864:1: ( '{' )
            // InternalWitness.g:865:2: '{'
            {
             before(grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_13_1()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_13_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__1__Impl"


    // $ANTLR start "rule__Witness__Group_13__2"
    // InternalWitness.g:874:1: rule__Witness__Group_13__2 : rule__Witness__Group_13__2__Impl rule__Witness__Group_13__3 ;
    public final void rule__Witness__Group_13__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:878:1: ( rule__Witness__Group_13__2__Impl rule__Witness__Group_13__3 )
            // InternalWitness.g:879:2: rule__Witness__Group_13__2__Impl rule__Witness__Group_13__3
            {
            pushFollow(FOLLOW_16);
            rule__Witness__Group_13__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group_13__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__2"


    // $ANTLR start "rule__Witness__Group_13__2__Impl"
    // InternalWitness.g:886:1: rule__Witness__Group_13__2__Impl : ( '@startuml' ) ;
    public final void rule__Witness__Group_13__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:890:1: ( ( '@startuml' ) )
            // InternalWitness.g:891:1: ( '@startuml' )
            {
            // InternalWitness.g:891:1: ( '@startuml' )
            // InternalWitness.g:892:2: '@startuml'
            {
             before(grammarAccess.getWitnessAccess().getStartumlKeyword_13_2()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getStartumlKeyword_13_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__2__Impl"


    // $ANTLR start "rule__Witness__Group_13__3"
    // InternalWitness.g:901:1: rule__Witness__Group_13__3 : rule__Witness__Group_13__3__Impl rule__Witness__Group_13__4 ;
    public final void rule__Witness__Group_13__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:905:1: ( rule__Witness__Group_13__3__Impl rule__Witness__Group_13__4 )
            // InternalWitness.g:906:2: rule__Witness__Group_13__3__Impl rule__Witness__Group_13__4
            {
            pushFollow(FOLLOW_16);
            rule__Witness__Group_13__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group_13__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__3"


    // $ANTLR start "rule__Witness__Group_13__3__Impl"
    // InternalWitness.g:913:1: rule__Witness__Group_13__3__Impl : ( ( rule__Witness__SequenceRulesAssignment_13_3 )* ) ;
    public final void rule__Witness__Group_13__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:917:1: ( ( ( rule__Witness__SequenceRulesAssignment_13_3 )* ) )
            // InternalWitness.g:918:1: ( ( rule__Witness__SequenceRulesAssignment_13_3 )* )
            {
            // InternalWitness.g:918:1: ( ( rule__Witness__SequenceRulesAssignment_13_3 )* )
            // InternalWitness.g:919:2: ( rule__Witness__SequenceRulesAssignment_13_3 )*
            {
             before(grammarAccess.getWitnessAccess().getSequenceRulesAssignment_13_3()); 
            // InternalWitness.g:920:2: ( rule__Witness__SequenceRulesAssignment_13_3 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_NUMBER) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalWitness.g:920:3: rule__Witness__SequenceRulesAssignment_13_3
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Witness__SequenceRulesAssignment_13_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getWitnessAccess().getSequenceRulesAssignment_13_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__3__Impl"


    // $ANTLR start "rule__Witness__Group_13__4"
    // InternalWitness.g:928:1: rule__Witness__Group_13__4 : rule__Witness__Group_13__4__Impl rule__Witness__Group_13__5 ;
    public final void rule__Witness__Group_13__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:932:1: ( rule__Witness__Group_13__4__Impl rule__Witness__Group_13__5 )
            // InternalWitness.g:933:2: rule__Witness__Group_13__4__Impl rule__Witness__Group_13__5
            {
            pushFollow(FOLLOW_17);
            rule__Witness__Group_13__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Witness__Group_13__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__4"


    // $ANTLR start "rule__Witness__Group_13__4__Impl"
    // InternalWitness.g:940:1: rule__Witness__Group_13__4__Impl : ( '@enduml' ) ;
    public final void rule__Witness__Group_13__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:944:1: ( ( '@enduml' ) )
            // InternalWitness.g:945:1: ( '@enduml' )
            {
            // InternalWitness.g:945:1: ( '@enduml' )
            // InternalWitness.g:946:2: '@enduml'
            {
             before(grammarAccess.getWitnessAccess().getEndumlKeyword_13_4()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getEndumlKeyword_13_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__4__Impl"


    // $ANTLR start "rule__Witness__Group_13__5"
    // InternalWitness.g:955:1: rule__Witness__Group_13__5 : rule__Witness__Group_13__5__Impl ;
    public final void rule__Witness__Group_13__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:959:1: ( rule__Witness__Group_13__5__Impl )
            // InternalWitness.g:960:2: rule__Witness__Group_13__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Witness__Group_13__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__5"


    // $ANTLR start "rule__Witness__Group_13__5__Impl"
    // InternalWitness.g:966:1: rule__Witness__Group_13__5__Impl : ( '}' ) ;
    public final void rule__Witness__Group_13__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:970:1: ( ( '}' ) )
            // InternalWitness.g:971:1: ( '}' )
            {
            // InternalWitness.g:971:1: ( '}' )
            // InternalWitness.g:972:2: '}'
            {
             before(grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_13_5()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_13_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__Group_13__5__Impl"


    // $ANTLR start "rule__SequenceRule__Group__0"
    // InternalWitness.g:982:1: rule__SequenceRule__Group__0 : rule__SequenceRule__Group__0__Impl rule__SequenceRule__Group__1 ;
    public final void rule__SequenceRule__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:986:1: ( rule__SequenceRule__Group__0__Impl rule__SequenceRule__Group__1 )
            // InternalWitness.g:987:2: rule__SequenceRule__Group__0__Impl rule__SequenceRule__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__SequenceRule__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SequenceRule__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__0"


    // $ANTLR start "rule__SequenceRule__Group__0__Impl"
    // InternalWitness.g:994:1: rule__SequenceRule__Group__0__Impl : ( ( rule__SequenceRule__FromAssignment_0 ) ) ;
    public final void rule__SequenceRule__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:998:1: ( ( ( rule__SequenceRule__FromAssignment_0 ) ) )
            // InternalWitness.g:999:1: ( ( rule__SequenceRule__FromAssignment_0 ) )
            {
            // InternalWitness.g:999:1: ( ( rule__SequenceRule__FromAssignment_0 ) )
            // InternalWitness.g:1000:2: ( rule__SequenceRule__FromAssignment_0 )
            {
             before(grammarAccess.getSequenceRuleAccess().getFromAssignment_0()); 
            // InternalWitness.g:1001:2: ( rule__SequenceRule__FromAssignment_0 )
            // InternalWitness.g:1001:3: rule__SequenceRule__FromAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__SequenceRule__FromAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getSequenceRuleAccess().getFromAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__0__Impl"


    // $ANTLR start "rule__SequenceRule__Group__1"
    // InternalWitness.g:1009:1: rule__SequenceRule__Group__1 : rule__SequenceRule__Group__1__Impl rule__SequenceRule__Group__2 ;
    public final void rule__SequenceRule__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1013:1: ( rule__SequenceRule__Group__1__Impl rule__SequenceRule__Group__2 )
            // InternalWitness.g:1014:2: rule__SequenceRule__Group__1__Impl rule__SequenceRule__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__SequenceRule__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SequenceRule__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__1"


    // $ANTLR start "rule__SequenceRule__Group__1__Impl"
    // InternalWitness.g:1021:1: rule__SequenceRule__Group__1__Impl : ( '->' ) ;
    public final void rule__SequenceRule__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1025:1: ( ( '->' ) )
            // InternalWitness.g:1026:1: ( '->' )
            {
            // InternalWitness.g:1026:1: ( '->' )
            // InternalWitness.g:1027:2: '->'
            {
             before(grammarAccess.getSequenceRuleAccess().getHyphenMinusGreaterThanSignKeyword_1()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getSequenceRuleAccess().getHyphenMinusGreaterThanSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__1__Impl"


    // $ANTLR start "rule__SequenceRule__Group__2"
    // InternalWitness.g:1036:1: rule__SequenceRule__Group__2 : rule__SequenceRule__Group__2__Impl rule__SequenceRule__Group__3 ;
    public final void rule__SequenceRule__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1040:1: ( rule__SequenceRule__Group__2__Impl rule__SequenceRule__Group__3 )
            // InternalWitness.g:1041:2: rule__SequenceRule__Group__2__Impl rule__SequenceRule__Group__3
            {
            pushFollow(FOLLOW_20);
            rule__SequenceRule__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SequenceRule__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__2"


    // $ANTLR start "rule__SequenceRule__Group__2__Impl"
    // InternalWitness.g:1048:1: rule__SequenceRule__Group__2__Impl : ( ( rule__SequenceRule__ToAssignment_2 ) ) ;
    public final void rule__SequenceRule__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1052:1: ( ( ( rule__SequenceRule__ToAssignment_2 ) ) )
            // InternalWitness.g:1053:1: ( ( rule__SequenceRule__ToAssignment_2 ) )
            {
            // InternalWitness.g:1053:1: ( ( rule__SequenceRule__ToAssignment_2 ) )
            // InternalWitness.g:1054:2: ( rule__SequenceRule__ToAssignment_2 )
            {
             before(grammarAccess.getSequenceRuleAccess().getToAssignment_2()); 
            // InternalWitness.g:1055:2: ( rule__SequenceRule__ToAssignment_2 )
            // InternalWitness.g:1055:3: rule__SequenceRule__ToAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SequenceRule__ToAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSequenceRuleAccess().getToAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__2__Impl"


    // $ANTLR start "rule__SequenceRule__Group__3"
    // InternalWitness.g:1063:1: rule__SequenceRule__Group__3 : rule__SequenceRule__Group__3__Impl rule__SequenceRule__Group__4 ;
    public final void rule__SequenceRule__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1067:1: ( rule__SequenceRule__Group__3__Impl rule__SequenceRule__Group__4 )
            // InternalWitness.g:1068:2: rule__SequenceRule__Group__3__Impl rule__SequenceRule__Group__4
            {
            pushFollow(FOLLOW_4);
            rule__SequenceRule__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SequenceRule__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__3"


    // $ANTLR start "rule__SequenceRule__Group__3__Impl"
    // InternalWitness.g:1075:1: rule__SequenceRule__Group__3__Impl : ( ':' ) ;
    public final void rule__SequenceRule__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1079:1: ( ( ':' ) )
            // InternalWitness.g:1080:1: ( ':' )
            {
            // InternalWitness.g:1080:1: ( ':' )
            // InternalWitness.g:1081:2: ':'
            {
             before(grammarAccess.getSequenceRuleAccess().getColonKeyword_3()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getSequenceRuleAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__3__Impl"


    // $ANTLR start "rule__SequenceRule__Group__4"
    // InternalWitness.g:1090:1: rule__SequenceRule__Group__4 : rule__SequenceRule__Group__4__Impl ;
    public final void rule__SequenceRule__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1094:1: ( rule__SequenceRule__Group__4__Impl )
            // InternalWitness.g:1095:2: rule__SequenceRule__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SequenceRule__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__4"


    // $ANTLR start "rule__SequenceRule__Group__4__Impl"
    // InternalWitness.g:1101:1: rule__SequenceRule__Group__4__Impl : ( ( rule__SequenceRule__ReferencedStepAssignment_4 ) ) ;
    public final void rule__SequenceRule__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1105:1: ( ( ( rule__SequenceRule__ReferencedStepAssignment_4 ) ) )
            // InternalWitness.g:1106:1: ( ( rule__SequenceRule__ReferencedStepAssignment_4 ) )
            {
            // InternalWitness.g:1106:1: ( ( rule__SequenceRule__ReferencedStepAssignment_4 ) )
            // InternalWitness.g:1107:2: ( rule__SequenceRule__ReferencedStepAssignment_4 )
            {
             before(grammarAccess.getSequenceRuleAccess().getReferencedStepAssignment_4()); 
            // InternalWitness.g:1108:2: ( rule__SequenceRule__ReferencedStepAssignment_4 )
            // InternalWitness.g:1108:3: rule__SequenceRule__ReferencedStepAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__SequenceRule__ReferencedStepAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getSequenceRuleAccess().getReferencedStepAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__Group__4__Impl"


    // $ANTLR start "rule__StepStatesCount__Group__0"
    // InternalWitness.g:1117:1: rule__StepStatesCount__Group__0 : rule__StepStatesCount__Group__0__Impl rule__StepStatesCount__Group__1 ;
    public final void rule__StepStatesCount__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1121:1: ( rule__StepStatesCount__Group__0__Impl rule__StepStatesCount__Group__1 )
            // InternalWitness.g:1122:2: rule__StepStatesCount__Group__0__Impl rule__StepStatesCount__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__StepStatesCount__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__0"


    // $ANTLR start "rule__StepStatesCount__Group__0__Impl"
    // InternalWitness.g:1129:1: rule__StepStatesCount__Group__0__Impl : ( () ) ;
    public final void rule__StepStatesCount__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1133:1: ( ( () ) )
            // InternalWitness.g:1134:1: ( () )
            {
            // InternalWitness.g:1134:1: ( () )
            // InternalWitness.g:1135:2: ()
            {
             before(grammarAccess.getStepStatesCountAccess().getStepStatesCountAction_0()); 
            // InternalWitness.g:1136:2: ()
            // InternalWitness.g:1136:3: 
            {
            }

             after(grammarAccess.getStepStatesCountAccess().getStepStatesCountAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__0__Impl"


    // $ANTLR start "rule__StepStatesCount__Group__1"
    // InternalWitness.g:1144:1: rule__StepStatesCount__Group__1 : rule__StepStatesCount__Group__1__Impl rule__StepStatesCount__Group__2 ;
    public final void rule__StepStatesCount__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1148:1: ( rule__StepStatesCount__Group__1__Impl rule__StepStatesCount__Group__2 )
            // InternalWitness.g:1149:2: rule__StepStatesCount__Group__1__Impl rule__StepStatesCount__Group__2
            {
            pushFollow(FOLLOW_21);
            rule__StepStatesCount__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__1"


    // $ANTLR start "rule__StepStatesCount__Group__1__Impl"
    // InternalWitness.g:1156:1: rule__StepStatesCount__Group__1__Impl : ( ( rule__StepStatesCount__NameAssignment_1 ) ) ;
    public final void rule__StepStatesCount__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1160:1: ( ( ( rule__StepStatesCount__NameAssignment_1 ) ) )
            // InternalWitness.g:1161:1: ( ( rule__StepStatesCount__NameAssignment_1 ) )
            {
            // InternalWitness.g:1161:1: ( ( rule__StepStatesCount__NameAssignment_1 ) )
            // InternalWitness.g:1162:2: ( rule__StepStatesCount__NameAssignment_1 )
            {
             before(grammarAccess.getStepStatesCountAccess().getNameAssignment_1()); 
            // InternalWitness.g:1163:2: ( rule__StepStatesCount__NameAssignment_1 )
            // InternalWitness.g:1163:3: rule__StepStatesCount__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__StepStatesCount__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStepStatesCountAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__1__Impl"


    // $ANTLR start "rule__StepStatesCount__Group__2"
    // InternalWitness.g:1171:1: rule__StepStatesCount__Group__2 : rule__StepStatesCount__Group__2__Impl rule__StepStatesCount__Group__3 ;
    public final void rule__StepStatesCount__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1175:1: ( rule__StepStatesCount__Group__2__Impl rule__StepStatesCount__Group__3 )
            // InternalWitness.g:1176:2: rule__StepStatesCount__Group__2__Impl rule__StepStatesCount__Group__3
            {
            pushFollow(FOLLOW_22);
            rule__StepStatesCount__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__2"


    // $ANTLR start "rule__StepStatesCount__Group__2__Impl"
    // InternalWitness.g:1183:1: rule__StepStatesCount__Group__2__Impl : ( ( rule__StepStatesCount__Alternatives_2 ) ) ;
    public final void rule__StepStatesCount__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1187:1: ( ( ( rule__StepStatesCount__Alternatives_2 ) ) )
            // InternalWitness.g:1188:1: ( ( rule__StepStatesCount__Alternatives_2 ) )
            {
            // InternalWitness.g:1188:1: ( ( rule__StepStatesCount__Alternatives_2 ) )
            // InternalWitness.g:1189:2: ( rule__StepStatesCount__Alternatives_2 )
            {
             before(grammarAccess.getStepStatesCountAccess().getAlternatives_2()); 
            // InternalWitness.g:1190:2: ( rule__StepStatesCount__Alternatives_2 )
            // InternalWitness.g:1190:3: rule__StepStatesCount__Alternatives_2
            {
            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Alternatives_2();

            state._fsp--;


            }

             after(grammarAccess.getStepStatesCountAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__2__Impl"


    // $ANTLR start "rule__StepStatesCount__Group__3"
    // InternalWitness.g:1198:1: rule__StepStatesCount__Group__3 : rule__StepStatesCount__Group__3__Impl rule__StepStatesCount__Group__4 ;
    public final void rule__StepStatesCount__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1202:1: ( rule__StepStatesCount__Group__3__Impl rule__StepStatesCount__Group__4 )
            // InternalWitness.g:1203:2: rule__StepStatesCount__Group__3__Impl rule__StepStatesCount__Group__4
            {
            pushFollow(FOLLOW_19);
            rule__StepStatesCount__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__3"


    // $ANTLR start "rule__StepStatesCount__Group__3__Impl"
    // InternalWitness.g:1210:1: rule__StepStatesCount__Group__3__Impl : ( '[*' ) ;
    public final void rule__StepStatesCount__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1214:1: ( ( '[*' ) )
            // InternalWitness.g:1215:1: ( '[*' )
            {
            // InternalWitness.g:1215:1: ( '[*' )
            // InternalWitness.g:1216:2: '[*'
            {
             before(grammarAccess.getStepStatesCountAccess().getLeftSquareBracketAsteriskKeyword_3()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getStepStatesCountAccess().getLeftSquareBracketAsteriskKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__3__Impl"


    // $ANTLR start "rule__StepStatesCount__Group__4"
    // InternalWitness.g:1225:1: rule__StepStatesCount__Group__4 : rule__StepStatesCount__Group__4__Impl rule__StepStatesCount__Group__5 ;
    public final void rule__StepStatesCount__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1229:1: ( rule__StepStatesCount__Group__4__Impl rule__StepStatesCount__Group__5 )
            // InternalWitness.g:1230:2: rule__StepStatesCount__Group__4__Impl rule__StepStatesCount__Group__5
            {
            pushFollow(FOLLOW_23);
            rule__StepStatesCount__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__4"


    // $ANTLR start "rule__StepStatesCount__Group__4__Impl"
    // InternalWitness.g:1237:1: rule__StepStatesCount__Group__4__Impl : ( ( rule__StepStatesCount__MaxCountAssignment_4 ) ) ;
    public final void rule__StepStatesCount__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1241:1: ( ( ( rule__StepStatesCount__MaxCountAssignment_4 ) ) )
            // InternalWitness.g:1242:1: ( ( rule__StepStatesCount__MaxCountAssignment_4 ) )
            {
            // InternalWitness.g:1242:1: ( ( rule__StepStatesCount__MaxCountAssignment_4 ) )
            // InternalWitness.g:1243:2: ( rule__StepStatesCount__MaxCountAssignment_4 )
            {
             before(grammarAccess.getStepStatesCountAccess().getMaxCountAssignment_4()); 
            // InternalWitness.g:1244:2: ( rule__StepStatesCount__MaxCountAssignment_4 )
            // InternalWitness.g:1244:3: rule__StepStatesCount__MaxCountAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__StepStatesCount__MaxCountAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getStepStatesCountAccess().getMaxCountAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__4__Impl"


    // $ANTLR start "rule__StepStatesCount__Group__5"
    // InternalWitness.g:1252:1: rule__StepStatesCount__Group__5 : rule__StepStatesCount__Group__5__Impl rule__StepStatesCount__Group__6 ;
    public final void rule__StepStatesCount__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1256:1: ( rule__StepStatesCount__Group__5__Impl rule__StepStatesCount__Group__6 )
            // InternalWitness.g:1257:2: rule__StepStatesCount__Group__5__Impl rule__StepStatesCount__Group__6
            {
            pushFollow(FOLLOW_19);
            rule__StepStatesCount__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__5"


    // $ANTLR start "rule__StepStatesCount__Group__5__Impl"
    // InternalWitness.g:1264:1: rule__StepStatesCount__Group__5__Impl : ( '/' ) ;
    public final void rule__StepStatesCount__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1268:1: ( ( '/' ) )
            // InternalWitness.g:1269:1: ( '/' )
            {
            // InternalWitness.g:1269:1: ( '/' )
            // InternalWitness.g:1270:2: '/'
            {
             before(grammarAccess.getStepStatesCountAccess().getSolidusKeyword_5()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getStepStatesCountAccess().getSolidusKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__5__Impl"


    // $ANTLR start "rule__StepStatesCount__Group__6"
    // InternalWitness.g:1279:1: rule__StepStatesCount__Group__6 : rule__StepStatesCount__Group__6__Impl rule__StepStatesCount__Group__7 ;
    public final void rule__StepStatesCount__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1283:1: ( rule__StepStatesCount__Group__6__Impl rule__StepStatesCount__Group__7 )
            // InternalWitness.g:1284:2: rule__StepStatesCount__Group__6__Impl rule__StepStatesCount__Group__7
            {
            pushFollow(FOLLOW_24);
            rule__StepStatesCount__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__6"


    // $ANTLR start "rule__StepStatesCount__Group__6__Impl"
    // InternalWitness.g:1291:1: rule__StepStatesCount__Group__6__Impl : ( ( rule__StepStatesCount__CountAssignment_6 ) ) ;
    public final void rule__StepStatesCount__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1295:1: ( ( ( rule__StepStatesCount__CountAssignment_6 ) ) )
            // InternalWitness.g:1296:1: ( ( rule__StepStatesCount__CountAssignment_6 ) )
            {
            // InternalWitness.g:1296:1: ( ( rule__StepStatesCount__CountAssignment_6 ) )
            // InternalWitness.g:1297:2: ( rule__StepStatesCount__CountAssignment_6 )
            {
             before(grammarAccess.getStepStatesCountAccess().getCountAssignment_6()); 
            // InternalWitness.g:1298:2: ( rule__StepStatesCount__CountAssignment_6 )
            // InternalWitness.g:1298:3: rule__StepStatesCount__CountAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__StepStatesCount__CountAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getStepStatesCountAccess().getCountAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__6__Impl"


    // $ANTLR start "rule__StepStatesCount__Group__7"
    // InternalWitness.g:1306:1: rule__StepStatesCount__Group__7 : rule__StepStatesCount__Group__7__Impl rule__StepStatesCount__Group__8 ;
    public final void rule__StepStatesCount__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1310:1: ( rule__StepStatesCount__Group__7__Impl rule__StepStatesCount__Group__8 )
            // InternalWitness.g:1311:2: rule__StepStatesCount__Group__7__Impl rule__StepStatesCount__Group__8
            {
            pushFollow(FOLLOW_25);
            rule__StepStatesCount__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__7"


    // $ANTLR start "rule__StepStatesCount__Group__7__Impl"
    // InternalWitness.g:1318:1: rule__StepStatesCount__Group__7__Impl : ( '*]' ) ;
    public final void rule__StepStatesCount__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1322:1: ( ( '*]' ) )
            // InternalWitness.g:1323:1: ( '*]' )
            {
            // InternalWitness.g:1323:1: ( '*]' )
            // InternalWitness.g:1324:2: '*]'
            {
             before(grammarAccess.getStepStatesCountAccess().getAsteriskRightSquareBracketKeyword_7()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getStepStatesCountAccess().getAsteriskRightSquareBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__7__Impl"


    // $ANTLR start "rule__StepStatesCount__Group__8"
    // InternalWitness.g:1333:1: rule__StepStatesCount__Group__8 : rule__StepStatesCount__Group__8__Impl ;
    public final void rule__StepStatesCount__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1337:1: ( rule__StepStatesCount__Group__8__Impl )
            // InternalWitness.g:1338:2: rule__StepStatesCount__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StepStatesCount__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__8"


    // $ANTLR start "rule__StepStatesCount__Group__8__Impl"
    // InternalWitness.g:1344:1: rule__StepStatesCount__Group__8__Impl : ( ( rule__StepStatesCount__StepStateAssignment_8 )* ) ;
    public final void rule__StepStatesCount__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1348:1: ( ( ( rule__StepStatesCount__StepStateAssignment_8 )* ) )
            // InternalWitness.g:1349:1: ( ( rule__StepStatesCount__StepStateAssignment_8 )* )
            {
            // InternalWitness.g:1349:1: ( ( rule__StepStatesCount__StepStateAssignment_8 )* )
            // InternalWitness.g:1350:2: ( rule__StepStatesCount__StepStateAssignment_8 )*
            {
             before(grammarAccess.getStepStatesCountAccess().getStepStateAssignment_8()); 
            // InternalWitness.g:1351:2: ( rule__StepStatesCount__StepStateAssignment_8 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==26) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalWitness.g:1351:3: rule__StepStatesCount__StepStateAssignment_8
            	    {
            	    pushFollow(FOLLOW_26);
            	    rule__StepStatesCount__StepStateAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getStepStatesCountAccess().getStepStateAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__Group__8__Impl"


    // $ANTLR start "rule__StepStates__Group__0"
    // InternalWitness.g:1360:1: rule__StepStates__Group__0 : rule__StepStates__Group__0__Impl rule__StepStates__Group__1 ;
    public final void rule__StepStates__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1364:1: ( rule__StepStates__Group__0__Impl rule__StepStates__Group__1 )
            // InternalWitness.g:1365:2: rule__StepStates__Group__0__Impl rule__StepStates__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__StepStates__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStates__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__Group__0"


    // $ANTLR start "rule__StepStates__Group__0__Impl"
    // InternalWitness.g:1372:1: rule__StepStates__Group__0__Impl : ( ( rule__StepStates__NameAssignment_0 ) ) ;
    public final void rule__StepStates__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1376:1: ( ( ( rule__StepStates__NameAssignment_0 ) ) )
            // InternalWitness.g:1377:1: ( ( rule__StepStates__NameAssignment_0 ) )
            {
            // InternalWitness.g:1377:1: ( ( rule__StepStates__NameAssignment_0 ) )
            // InternalWitness.g:1378:2: ( rule__StepStates__NameAssignment_0 )
            {
             before(grammarAccess.getStepStatesAccess().getNameAssignment_0()); 
            // InternalWitness.g:1379:2: ( rule__StepStates__NameAssignment_0 )
            // InternalWitness.g:1379:3: rule__StepStates__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__StepStates__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getStepStatesAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__Group__0__Impl"


    // $ANTLR start "rule__StepStates__Group__1"
    // InternalWitness.g:1387:1: rule__StepStates__Group__1 : rule__StepStates__Group__1__Impl rule__StepStates__Group__2 ;
    public final void rule__StepStates__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1391:1: ( rule__StepStates__Group__1__Impl rule__StepStates__Group__2 )
            // InternalWitness.g:1392:2: rule__StepStates__Group__1__Impl rule__StepStates__Group__2
            {
            pushFollow(FOLLOW_25);
            rule__StepStates__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepStates__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__Group__1"


    // $ANTLR start "rule__StepStates__Group__1__Impl"
    // InternalWitness.g:1399:1: rule__StepStates__Group__1__Impl : ( ( rule__StepStates__Alternatives_1 ) ) ;
    public final void rule__StepStates__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1403:1: ( ( ( rule__StepStates__Alternatives_1 ) ) )
            // InternalWitness.g:1404:1: ( ( rule__StepStates__Alternatives_1 ) )
            {
            // InternalWitness.g:1404:1: ( ( rule__StepStates__Alternatives_1 ) )
            // InternalWitness.g:1405:2: ( rule__StepStates__Alternatives_1 )
            {
             before(grammarAccess.getStepStatesAccess().getAlternatives_1()); 
            // InternalWitness.g:1406:2: ( rule__StepStates__Alternatives_1 )
            // InternalWitness.g:1406:3: rule__StepStates__Alternatives_1
            {
            pushFollow(FOLLOW_2);
            rule__StepStates__Alternatives_1();

            state._fsp--;


            }

             after(grammarAccess.getStepStatesAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__Group__1__Impl"


    // $ANTLR start "rule__StepStates__Group__2"
    // InternalWitness.g:1414:1: rule__StepStates__Group__2 : rule__StepStates__Group__2__Impl ;
    public final void rule__StepStates__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1418:1: ( rule__StepStates__Group__2__Impl )
            // InternalWitness.g:1419:2: rule__StepStates__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StepStates__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__Group__2"


    // $ANTLR start "rule__StepStates__Group__2__Impl"
    // InternalWitness.g:1425:1: rule__StepStates__Group__2__Impl : ( ( rule__StepStates__StepStateAssignment_2 )* ) ;
    public final void rule__StepStates__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1429:1: ( ( ( rule__StepStates__StepStateAssignment_2 )* ) )
            // InternalWitness.g:1430:1: ( ( rule__StepStates__StepStateAssignment_2 )* )
            {
            // InternalWitness.g:1430:1: ( ( rule__StepStates__StepStateAssignment_2 )* )
            // InternalWitness.g:1431:2: ( rule__StepStates__StepStateAssignment_2 )*
            {
             before(grammarAccess.getStepStatesAccess().getStepStateAssignment_2()); 
            // InternalWitness.g:1432:2: ( rule__StepStates__StepStateAssignment_2 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==26) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalWitness.g:1432:3: rule__StepStates__StepStateAssignment_2
            	    {
            	    pushFollow(FOLLOW_26);
            	    rule__StepStates__StepStateAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getStepStatesAccess().getStepStateAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__Group__2__Impl"


    // $ANTLR start "rule__StepState__Group__0"
    // InternalWitness.g:1441:1: rule__StepState__Group__0 : rule__StepState__Group__0__Impl rule__StepState__Group__1 ;
    public final void rule__StepState__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1445:1: ( rule__StepState__Group__0__Impl rule__StepState__Group__1 )
            // InternalWitness.g:1446:2: rule__StepState__Group__0__Impl rule__StepState__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__StepState__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepState__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepState__Group__0"


    // $ANTLR start "rule__StepState__Group__0__Impl"
    // InternalWitness.g:1453:1: rule__StepState__Group__0__Impl : ( () ) ;
    public final void rule__StepState__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1457:1: ( ( () ) )
            // InternalWitness.g:1458:1: ( () )
            {
            // InternalWitness.g:1458:1: ( () )
            // InternalWitness.g:1459:2: ()
            {
             before(grammarAccess.getStepStateAccess().getStepStateAction_0()); 
            // InternalWitness.g:1460:2: ()
            // InternalWitness.g:1460:3: 
            {
            }

             after(grammarAccess.getStepStateAccess().getStepStateAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepState__Group__0__Impl"


    // $ANTLR start "rule__StepState__Group__1"
    // InternalWitness.g:1468:1: rule__StepState__Group__1 : rule__StepState__Group__1__Impl rule__StepState__Group__2 ;
    public final void rule__StepState__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1472:1: ( rule__StepState__Group__1__Impl rule__StepState__Group__2 )
            // InternalWitness.g:1473:2: rule__StepState__Group__1__Impl rule__StepState__Group__2
            {
            pushFollow(FOLLOW_27);
            rule__StepState__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepState__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepState__Group__1"


    // $ANTLR start "rule__StepState__Group__1__Impl"
    // InternalWitness.g:1480:1: rule__StepState__Group__1__Impl : ( '[' ) ;
    public final void rule__StepState__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1484:1: ( ( '[' ) )
            // InternalWitness.g:1485:1: ( '[' )
            {
            // InternalWitness.g:1485:1: ( '[' )
            // InternalWitness.g:1486:2: '['
            {
             before(grammarAccess.getStepStateAccess().getLeftSquareBracketKeyword_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getStepStateAccess().getLeftSquareBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepState__Group__1__Impl"


    // $ANTLR start "rule__StepState__Group__2"
    // InternalWitness.g:1495:1: rule__StepState__Group__2 : rule__StepState__Group__2__Impl rule__StepState__Group__3 ;
    public final void rule__StepState__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1499:1: ( rule__StepState__Group__2__Impl rule__StepState__Group__3 )
            // InternalWitness.g:1500:2: rule__StepState__Group__2__Impl rule__StepState__Group__3
            {
            pushFollow(FOLLOW_27);
            rule__StepState__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StepState__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepState__Group__2"


    // $ANTLR start "rule__StepState__Group__2__Impl"
    // InternalWitness.g:1507:1: rule__StepState__Group__2__Impl : ( ( rule__StepState__VarStateAssignment_2 )* ) ;
    public final void rule__StepState__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1511:1: ( ( ( rule__StepState__VarStateAssignment_2 )* ) )
            // InternalWitness.g:1512:1: ( ( rule__StepState__VarStateAssignment_2 )* )
            {
            // InternalWitness.g:1512:1: ( ( rule__StepState__VarStateAssignment_2 )* )
            // InternalWitness.g:1513:2: ( rule__StepState__VarStateAssignment_2 )*
            {
             before(grammarAccess.getStepStateAccess().getVarStateAssignment_2()); 
            // InternalWitness.g:1514:2: ( rule__StepState__VarStateAssignment_2 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalWitness.g:1514:3: rule__StepState__VarStateAssignment_2
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__StepState__VarStateAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getStepStateAccess().getVarStateAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepState__Group__2__Impl"


    // $ANTLR start "rule__StepState__Group__3"
    // InternalWitness.g:1522:1: rule__StepState__Group__3 : rule__StepState__Group__3__Impl ;
    public final void rule__StepState__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1526:1: ( rule__StepState__Group__3__Impl )
            // InternalWitness.g:1527:2: rule__StepState__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StepState__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepState__Group__3"


    // $ANTLR start "rule__StepState__Group__3__Impl"
    // InternalWitness.g:1533:1: rule__StepState__Group__3__Impl : ( ']' ) ;
    public final void rule__StepState__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1537:1: ( ( ']' ) )
            // InternalWitness.g:1538:1: ( ']' )
            {
            // InternalWitness.g:1538:1: ( ']' )
            // InternalWitness.g:1539:2: ']'
            {
             before(grammarAccess.getStepStateAccess().getRightSquareBracketKeyword_3()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getStepStateAccess().getRightSquareBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepState__Group__3__Impl"


    // $ANTLR start "rule__VarState__Group__0"
    // InternalWitness.g:1549:1: rule__VarState__Group__0 : rule__VarState__Group__0__Impl rule__VarState__Group__1 ;
    public final void rule__VarState__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1553:1: ( rule__VarState__Group__0__Impl rule__VarState__Group__1 )
            // InternalWitness.g:1554:2: rule__VarState__Group__0__Impl rule__VarState__Group__1
            {
            pushFollow(FOLLOW_28);
            rule__VarState__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VarState__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VarState__Group__0"


    // $ANTLR start "rule__VarState__Group__0__Impl"
    // InternalWitness.g:1561:1: rule__VarState__Group__0__Impl : ( ( rule__VarState__NameAssignment_0 ) ) ;
    public final void rule__VarState__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1565:1: ( ( ( rule__VarState__NameAssignment_0 ) ) )
            // InternalWitness.g:1566:1: ( ( rule__VarState__NameAssignment_0 ) )
            {
            // InternalWitness.g:1566:1: ( ( rule__VarState__NameAssignment_0 ) )
            // InternalWitness.g:1567:2: ( rule__VarState__NameAssignment_0 )
            {
             before(grammarAccess.getVarStateAccess().getNameAssignment_0()); 
            // InternalWitness.g:1568:2: ( rule__VarState__NameAssignment_0 )
            // InternalWitness.g:1568:3: rule__VarState__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__VarState__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getVarStateAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VarState__Group__0__Impl"


    // $ANTLR start "rule__VarState__Group__1"
    // InternalWitness.g:1576:1: rule__VarState__Group__1 : rule__VarState__Group__1__Impl rule__VarState__Group__2 ;
    public final void rule__VarState__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1580:1: ( rule__VarState__Group__1__Impl rule__VarState__Group__2 )
            // InternalWitness.g:1581:2: rule__VarState__Group__1__Impl rule__VarState__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__VarState__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VarState__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VarState__Group__1"


    // $ANTLR start "rule__VarState__Group__1__Impl"
    // InternalWitness.g:1588:1: rule__VarState__Group__1__Impl : ( '=' ) ;
    public final void rule__VarState__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1592:1: ( ( '=' ) )
            // InternalWitness.g:1593:1: ( '=' )
            {
            // InternalWitness.g:1593:1: ( '=' )
            // InternalWitness.g:1594:2: '='
            {
             before(grammarAccess.getVarStateAccess().getEqualsSignKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getVarStateAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VarState__Group__1__Impl"


    // $ANTLR start "rule__VarState__Group__2"
    // InternalWitness.g:1603:1: rule__VarState__Group__2 : rule__VarState__Group__2__Impl ;
    public final void rule__VarState__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1607:1: ( rule__VarState__Group__2__Impl )
            // InternalWitness.g:1608:2: rule__VarState__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__VarState__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VarState__Group__2"


    // $ANTLR start "rule__VarState__Group__2__Impl"
    // InternalWitness.g:1614:1: rule__VarState__Group__2__Impl : ( ( rule__VarState__ValueAssignment_2 ) ) ;
    public final void rule__VarState__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1618:1: ( ( ( rule__VarState__ValueAssignment_2 ) ) )
            // InternalWitness.g:1619:1: ( ( rule__VarState__ValueAssignment_2 ) )
            {
            // InternalWitness.g:1619:1: ( ( rule__VarState__ValueAssignment_2 ) )
            // InternalWitness.g:1620:2: ( rule__VarState__ValueAssignment_2 )
            {
             before(grammarAccess.getVarStateAccess().getValueAssignment_2()); 
            // InternalWitness.g:1621:2: ( rule__VarState__ValueAssignment_2 )
            // InternalWitness.g:1621:3: rule__VarState__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__VarState__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVarStateAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VarState__Group__2__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalWitness.g:1630:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1634:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalWitness.g:1635:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalWitness.g:1642:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1646:1: ( ( RULE_ID ) )
            // InternalWitness.g:1647:1: ( RULE_ID )
            {
            // InternalWitness.g:1647:1: ( RULE_ID )
            // InternalWitness.g:1648:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalWitness.g:1657:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1661:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalWitness.g:1662:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalWitness.g:1668:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1672:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalWitness.g:1673:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalWitness.g:1673:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalWitness.g:1674:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalWitness.g:1675:2: ( rule__QualifiedName__Group_1__0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==29) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalWitness.g:1675:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_30);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalWitness.g:1684:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1688:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalWitness.g:1689:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_4);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalWitness.g:1696:1: rule__QualifiedName__Group_1__0__Impl : ( '::' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1700:1: ( ( '::' ) )
            // InternalWitness.g:1701:1: ( '::' )
            {
            // InternalWitness.g:1701:1: ( '::' )
            // InternalWitness.g:1702:2: '::'
            {
             before(grammarAccess.getQualifiedNameAccess().getColonColonKeyword_1_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getColonColonKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalWitness.g:1711:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1715:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalWitness.g:1716:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalWitness.g:1722:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1726:1: ( ( RULE_ID ) )
            // InternalWitness.g:1727:1: ( RULE_ID )
            {
            // InternalWitness.g:1727:1: ( RULE_ID )
            // InternalWitness.g:1728:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__WitnessModel__WitnessesAssignment"
    // InternalWitness.g:1738:1: rule__WitnessModel__WitnessesAssignment : ( ruleWitness ) ;
    public final void rule__WitnessModel__WitnessesAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1742:1: ( ( ruleWitness ) )
            // InternalWitness.g:1743:2: ( ruleWitness )
            {
            // InternalWitness.g:1743:2: ( ruleWitness )
            // InternalWitness.g:1744:3: ruleWitness
            {
             before(grammarAccess.getWitnessModelAccess().getWitnessesWitnessParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleWitness();

            state._fsp--;

             after(grammarAccess.getWitnessModelAccess().getWitnessesWitnessParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WitnessModel__WitnessesAssignment"


    // $ANTLR start "rule__Witness__NameAssignment_1"
    // InternalWitness.g:1753:1: rule__Witness__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Witness__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1757:1: ( ( RULE_ID ) )
            // InternalWitness.g:1758:2: ( RULE_ID )
            {
            // InternalWitness.g:1758:2: ( RULE_ID )
            // InternalWitness.g:1759:3: RULE_ID
            {
             before(grammarAccess.getWitnessAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__NameAssignment_1"


    // $ANTLR start "rule__Witness__GoalNameAssignment_4"
    // InternalWitness.g:1768:1: rule__Witness__GoalNameAssignment_4 : ( RULE_STRING ) ;
    public final void rule__Witness__GoalNameAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1772:1: ( ( RULE_STRING ) )
            // InternalWitness.g:1773:2: ( RULE_STRING )
            {
            // InternalWitness.g:1773:2: ( RULE_STRING )
            // InternalWitness.g:1774:3: RULE_STRING
            {
             before(grammarAccess.getWitnessAccess().getGoalNameSTRINGTerminalRuleCall_4_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getWitnessAccess().getGoalNameSTRINGTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__GoalNameAssignment_4"


    // $ANTLR start "rule__Witness__StepsAssignment_7"
    // InternalWitness.g:1783:1: rule__Witness__StepsAssignment_7 : ( ruleStep ) ;
    public final void rule__Witness__StepsAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1787:1: ( ( ruleStep ) )
            // InternalWitness.g:1788:2: ( ruleStep )
            {
            // InternalWitness.g:1788:2: ( ruleStep )
            // InternalWitness.g:1789:3: ruleStep
            {
             before(grammarAccess.getWitnessAccess().getStepsStepParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleStep();

            state._fsp--;

             after(grammarAccess.getWitnessAccess().getStepsStepParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__StepsAssignment_7"


    // $ANTLR start "rule__Witness__StepStatesAssignment_11"
    // InternalWitness.g:1798:1: rule__Witness__StepStatesAssignment_11 : ( ruleAbstractStepStates ) ;
    public final void rule__Witness__StepStatesAssignment_11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1802:1: ( ( ruleAbstractStepStates ) )
            // InternalWitness.g:1803:2: ( ruleAbstractStepStates )
            {
            // InternalWitness.g:1803:2: ( ruleAbstractStepStates )
            // InternalWitness.g:1804:3: ruleAbstractStepStates
            {
             before(grammarAccess.getWitnessAccess().getStepStatesAbstractStepStatesParserRuleCall_11_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractStepStates();

            state._fsp--;

             after(grammarAccess.getWitnessAccess().getStepStatesAbstractStepStatesParserRuleCall_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__StepStatesAssignment_11"


    // $ANTLR start "rule__Witness__SequenceRulesAssignment_13_3"
    // InternalWitness.g:1813:1: rule__Witness__SequenceRulesAssignment_13_3 : ( ruleSequenceRule ) ;
    public final void rule__Witness__SequenceRulesAssignment_13_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1817:1: ( ( ruleSequenceRule ) )
            // InternalWitness.g:1818:2: ( ruleSequenceRule )
            {
            // InternalWitness.g:1818:2: ( ruleSequenceRule )
            // InternalWitness.g:1819:3: ruleSequenceRule
            {
             before(grammarAccess.getWitnessAccess().getSequenceRulesSequenceRuleParserRuleCall_13_3_0()); 
            pushFollow(FOLLOW_2);
            ruleSequenceRule();

            state._fsp--;

             after(grammarAccess.getWitnessAccess().getSequenceRulesSequenceRuleParserRuleCall_13_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Witness__SequenceRulesAssignment_13_3"


    // $ANTLR start "rule__SequenceRule__FromAssignment_0"
    // InternalWitness.g:1828:1: rule__SequenceRule__FromAssignment_0 : ( ruleInt ) ;
    public final void rule__SequenceRule__FromAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1832:1: ( ( ruleInt ) )
            // InternalWitness.g:1833:2: ( ruleInt )
            {
            // InternalWitness.g:1833:2: ( ruleInt )
            // InternalWitness.g:1834:3: ruleInt
            {
             before(grammarAccess.getSequenceRuleAccess().getFromIntParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleInt();

            state._fsp--;

             after(grammarAccess.getSequenceRuleAccess().getFromIntParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__FromAssignment_0"


    // $ANTLR start "rule__SequenceRule__ToAssignment_2"
    // InternalWitness.g:1843:1: rule__SequenceRule__ToAssignment_2 : ( ruleInt ) ;
    public final void rule__SequenceRule__ToAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1847:1: ( ( ruleInt ) )
            // InternalWitness.g:1848:2: ( ruleInt )
            {
            // InternalWitness.g:1848:2: ( ruleInt )
            // InternalWitness.g:1849:3: ruleInt
            {
             before(grammarAccess.getSequenceRuleAccess().getToIntParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInt();

            state._fsp--;

             after(grammarAccess.getSequenceRuleAccess().getToIntParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__ToAssignment_2"


    // $ANTLR start "rule__SequenceRule__ReferencedStepAssignment_4"
    // InternalWitness.g:1858:1: rule__SequenceRule__ReferencedStepAssignment_4 : ( ( ruleQualifiedName ) ) ;
    public final void rule__SequenceRule__ReferencedStepAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1862:1: ( ( ( ruleQualifiedName ) ) )
            // InternalWitness.g:1863:2: ( ( ruleQualifiedName ) )
            {
            // InternalWitness.g:1863:2: ( ( ruleQualifiedName ) )
            // InternalWitness.g:1864:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getSequenceRuleAccess().getReferencedStepStepCrossReference_4_0()); 
            // InternalWitness.g:1865:3: ( ruleQualifiedName )
            // InternalWitness.g:1866:4: ruleQualifiedName
            {
             before(grammarAccess.getSequenceRuleAccess().getReferencedStepStepQualifiedNameParserRuleCall_4_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getSequenceRuleAccess().getReferencedStepStepQualifiedNameParserRuleCall_4_0_1()); 

            }

             after(grammarAccess.getSequenceRuleAccess().getReferencedStepStepCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SequenceRule__ReferencedStepAssignment_4"


    // $ANTLR start "rule__Step__NameAssignment"
    // InternalWitness.g:1877:1: rule__Step__NameAssignment : ( RULE_ID ) ;
    public final void rule__Step__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1881:1: ( ( RULE_ID ) )
            // InternalWitness.g:1882:2: ( RULE_ID )
            {
            // InternalWitness.g:1882:2: ( RULE_ID )
            // InternalWitness.g:1883:3: RULE_ID
            {
             before(grammarAccess.getStepAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStepAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__NameAssignment"


    // $ANTLR start "rule__StepStatesCount__NameAssignment_1"
    // InternalWitness.g:1892:1: rule__StepStatesCount__NameAssignment_1 : ( ruleInt ) ;
    public final void rule__StepStatesCount__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1896:1: ( ( ruleInt ) )
            // InternalWitness.g:1897:2: ( ruleInt )
            {
            // InternalWitness.g:1897:2: ( ruleInt )
            // InternalWitness.g:1898:3: ruleInt
            {
             before(grammarAccess.getStepStatesCountAccess().getNameIntParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInt();

            state._fsp--;

             after(grammarAccess.getStepStatesCountAccess().getNameIntParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__NameAssignment_1"


    // $ANTLR start "rule__StepStatesCount__ReferencedStepAssignment_2_0"
    // InternalWitness.g:1907:1: rule__StepStatesCount__ReferencedStepAssignment_2_0 : ( ( ruleQualifiedName ) ) ;
    public final void rule__StepStatesCount__ReferencedStepAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1911:1: ( ( ( ruleQualifiedName ) ) )
            // InternalWitness.g:1912:2: ( ( ruleQualifiedName ) )
            {
            // InternalWitness.g:1912:2: ( ( ruleQualifiedName ) )
            // InternalWitness.g:1913:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getStepStatesCountAccess().getReferencedStepStepCrossReference_2_0_0()); 
            // InternalWitness.g:1914:3: ( ruleQualifiedName )
            // InternalWitness.g:1915:4: ruleQualifiedName
            {
             before(grammarAccess.getStepStatesCountAccess().getReferencedStepStepQualifiedNameParserRuleCall_2_0_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getStepStatesCountAccess().getReferencedStepStepQualifiedNameParserRuleCall_2_0_0_1()); 

            }

             after(grammarAccess.getStepStatesCountAccess().getReferencedStepStepCrossReference_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__ReferencedStepAssignment_2_0"


    // $ANTLR start "rule__StepStatesCount__MaxCountAssignment_4"
    // InternalWitness.g:1926:1: rule__StepStatesCount__MaxCountAssignment_4 : ( ruleLong ) ;
    public final void rule__StepStatesCount__MaxCountAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1930:1: ( ( ruleLong ) )
            // InternalWitness.g:1931:2: ( ruleLong )
            {
            // InternalWitness.g:1931:2: ( ruleLong )
            // InternalWitness.g:1932:3: ruleLong
            {
             before(grammarAccess.getStepStatesCountAccess().getMaxCountLongParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleLong();

            state._fsp--;

             after(grammarAccess.getStepStatesCountAccess().getMaxCountLongParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__MaxCountAssignment_4"


    // $ANTLR start "rule__StepStatesCount__CountAssignment_6"
    // InternalWitness.g:1941:1: rule__StepStatesCount__CountAssignment_6 : ( ruleLong ) ;
    public final void rule__StepStatesCount__CountAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1945:1: ( ( ruleLong ) )
            // InternalWitness.g:1946:2: ( ruleLong )
            {
            // InternalWitness.g:1946:2: ( ruleLong )
            // InternalWitness.g:1947:3: ruleLong
            {
             before(grammarAccess.getStepStatesCountAccess().getCountLongParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleLong();

            state._fsp--;

             after(grammarAccess.getStepStatesCountAccess().getCountLongParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__CountAssignment_6"


    // $ANTLR start "rule__StepStatesCount__StepStateAssignment_8"
    // InternalWitness.g:1956:1: rule__StepStatesCount__StepStateAssignment_8 : ( ruleStepState ) ;
    public final void rule__StepStatesCount__StepStateAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1960:1: ( ( ruleStepState ) )
            // InternalWitness.g:1961:2: ( ruleStepState )
            {
            // InternalWitness.g:1961:2: ( ruleStepState )
            // InternalWitness.g:1962:3: ruleStepState
            {
             before(grammarAccess.getStepStatesCountAccess().getStepStateStepStateParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleStepState();

            state._fsp--;

             after(grammarAccess.getStepStatesCountAccess().getStepStateStepStateParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStatesCount__StepStateAssignment_8"


    // $ANTLR start "rule__StepStates__NameAssignment_0"
    // InternalWitness.g:1971:1: rule__StepStates__NameAssignment_0 : ( ruleInt ) ;
    public final void rule__StepStates__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1975:1: ( ( ruleInt ) )
            // InternalWitness.g:1976:2: ( ruleInt )
            {
            // InternalWitness.g:1976:2: ( ruleInt )
            // InternalWitness.g:1977:3: ruleInt
            {
             before(grammarAccess.getStepStatesAccess().getNameIntParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleInt();

            state._fsp--;

             after(grammarAccess.getStepStatesAccess().getNameIntParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__NameAssignment_0"


    // $ANTLR start "rule__StepStates__ReferencedStepAssignment_1_0"
    // InternalWitness.g:1986:1: rule__StepStates__ReferencedStepAssignment_1_0 : ( ( ruleQualifiedName ) ) ;
    public final void rule__StepStates__ReferencedStepAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:1990:1: ( ( ( ruleQualifiedName ) ) )
            // InternalWitness.g:1991:2: ( ( ruleQualifiedName ) )
            {
            // InternalWitness.g:1991:2: ( ( ruleQualifiedName ) )
            // InternalWitness.g:1992:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getStepStatesAccess().getReferencedStepStepCrossReference_1_0_0()); 
            // InternalWitness.g:1993:3: ( ruleQualifiedName )
            // InternalWitness.g:1994:4: ruleQualifiedName
            {
             before(grammarAccess.getStepStatesAccess().getReferencedStepStepQualifiedNameParserRuleCall_1_0_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getStepStatesAccess().getReferencedStepStepQualifiedNameParserRuleCall_1_0_0_1()); 

            }

             after(grammarAccess.getStepStatesAccess().getReferencedStepStepCrossReference_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__ReferencedStepAssignment_1_0"


    // $ANTLR start "rule__StepStates__StepStateAssignment_2"
    // InternalWitness.g:2005:1: rule__StepStates__StepStateAssignment_2 : ( ruleStepState ) ;
    public final void rule__StepStates__StepStateAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:2009:1: ( ( ruleStepState ) )
            // InternalWitness.g:2010:2: ( ruleStepState )
            {
            // InternalWitness.g:2010:2: ( ruleStepState )
            // InternalWitness.g:2011:3: ruleStepState
            {
             before(grammarAccess.getStepStatesAccess().getStepStateStepStateParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleStepState();

            state._fsp--;

             after(grammarAccess.getStepStatesAccess().getStepStateStepStateParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepStates__StepStateAssignment_2"


    // $ANTLR start "rule__StepState__VarStateAssignment_2"
    // InternalWitness.g:2020:1: rule__StepState__VarStateAssignment_2 : ( ruleVarState ) ;
    public final void rule__StepState__VarStateAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:2024:1: ( ( ruleVarState ) )
            // InternalWitness.g:2025:2: ( ruleVarState )
            {
            // InternalWitness.g:2025:2: ( ruleVarState )
            // InternalWitness.g:2026:3: ruleVarState
            {
             before(grammarAccess.getStepStateAccess().getVarStateVarStateParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleVarState();

            state._fsp--;

             after(grammarAccess.getStepStateAccess().getVarStateVarStateParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepState__VarStateAssignment_2"


    // $ANTLR start "rule__VarState__NameAssignment_0"
    // InternalWitness.g:2035:1: rule__VarState__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__VarState__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:2039:1: ( ( RULE_ID ) )
            // InternalWitness.g:2040:2: ( RULE_ID )
            {
            // InternalWitness.g:2040:2: ( RULE_ID )
            // InternalWitness.g:2041:3: RULE_ID
            {
             before(grammarAccess.getVarStateAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVarStateAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VarState__NameAssignment_0"


    // $ANTLR start "rule__VarState__ValueAssignment_2"
    // InternalWitness.g:2050:1: rule__VarState__ValueAssignment_2 : ( ruleInt ) ;
    public final void rule__VarState__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWitness.g:2054:1: ( ( ruleInt ) )
            // InternalWitness.g:2055:2: ( ruleInt )
            {
            // InternalWitness.g:2055:2: ( ruleInt )
            // InternalWitness.g:2056:3: ruleInt
            {
             before(grammarAccess.getVarStateAccess().getValueIntParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInt();

            state._fsp--;

             after(grammarAccess.getVarStateAccess().getValueIntParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VarState__ValueAssignment_2"

    // Delegated rules


    protected DFA2 dfa2 = new DFA2(this);
    static final String dfa_1s = "\10\uffff";
    static final String dfa_2s = "\2\uffff\2\5\3\uffff\1\5";
    static final String dfa_3s = "\1\4\1\5\2\4\1\5\2\uffff\1\4";
    static final String dfa_4s = "\1\4\1\13\1\35\1\32\1\5\2\uffff\1\35";
    static final String dfa_5s = "\5\uffff\1\2\1\1\1\uffff";
    static final String dfa_6s = "\10\uffff}>";
    static final String[] dfa_7s = {
            "\1\1",
            "\1\2\5\uffff\1\3",
            "\1\5\13\uffff\1\5\6\uffff\1\6\2\uffff\1\5\2\uffff\1\4",
            "\1\5\13\uffff\1\5\6\uffff\1\6\2\uffff\1\5",
            "\1\7",
            "",
            "",
            "\1\5\13\uffff\1\5\6\uffff\1\6\2\uffff\1\5\2\uffff\1\4"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "352:1: rule__AbstractStepStates__Alternatives : ( ( ruleStepStatesCount ) | ( ruleStepStates ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000050000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000820L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000008000020L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000020000002L});

}