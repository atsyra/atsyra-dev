/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/*
 * generated by Xtext 2.12.0
 */
package fr.irisa.atsyra.witness.ide.contentassist.antlr;

import com.google.inject.Inject;
import fr.irisa.atsyra.witness.ide.contentassist.antlr.internal.InternalWitnessParser;
import fr.irisa.atsyra.witness.services.WitnessGrammarAccess;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;

public class WitnessParser extends AbstractContentAssistParser {

	@Inject
	private WitnessGrammarAccess grammarAccess;

	private Map<AbstractElement, String> nameMappings;

	@Override
	protected InternalWitnessParser createParser() {
		InternalWitnessParser result = new InternalWitnessParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getAbstractStepStatesAccess().getAlternatives(), "rule__AbstractStepStates__Alternatives");
					put(grammarAccess.getStepStatesCountAccess().getAlternatives_2(), "rule__StepStatesCount__Alternatives_2");
					put(grammarAccess.getStepStatesAccess().getAlternatives_1(), "rule__StepStates__Alternatives_1");
					put(grammarAccess.getWitnessAccess().getGroup(), "rule__Witness__Group__0");
					put(grammarAccess.getWitnessAccess().getGroup_13(), "rule__Witness__Group_13__0");
					put(grammarAccess.getSequenceRuleAccess().getGroup(), "rule__SequenceRule__Group__0");
					put(grammarAccess.getStepStatesCountAccess().getGroup(), "rule__StepStatesCount__Group__0");
					put(grammarAccess.getStepStatesAccess().getGroup(), "rule__StepStates__Group__0");
					put(grammarAccess.getStepStateAccess().getGroup(), "rule__StepState__Group__0");
					put(grammarAccess.getVarStateAccess().getGroup(), "rule__VarState__Group__0");
					put(grammarAccess.getQualifiedNameAccess().getGroup(), "rule__QualifiedName__Group__0");
					put(grammarAccess.getQualifiedNameAccess().getGroup_1(), "rule__QualifiedName__Group_1__0");
					put(grammarAccess.getWitnessModelAccess().getWitnessesAssignment(), "rule__WitnessModel__WitnessesAssignment");
					put(grammarAccess.getWitnessAccess().getNameAssignment_1(), "rule__Witness__NameAssignment_1");
					put(grammarAccess.getWitnessAccess().getGoalNameAssignment_4(), "rule__Witness__GoalNameAssignment_4");
					put(grammarAccess.getWitnessAccess().getStepsAssignment_7(), "rule__Witness__StepsAssignment_7");
					put(grammarAccess.getWitnessAccess().getStepStatesAssignment_11(), "rule__Witness__StepStatesAssignment_11");
					put(grammarAccess.getWitnessAccess().getSequenceRulesAssignment_13_3(), "rule__Witness__SequenceRulesAssignment_13_3");
					put(grammarAccess.getSequenceRuleAccess().getFromAssignment_0(), "rule__SequenceRule__FromAssignment_0");
					put(grammarAccess.getSequenceRuleAccess().getToAssignment_2(), "rule__SequenceRule__ToAssignment_2");
					put(grammarAccess.getSequenceRuleAccess().getReferencedStepAssignment_4(), "rule__SequenceRule__ReferencedStepAssignment_4");
					put(grammarAccess.getStepAccess().getNameAssignment(), "rule__Step__NameAssignment");
					put(grammarAccess.getStepStatesCountAccess().getNameAssignment_1(), "rule__StepStatesCount__NameAssignment_1");
					put(grammarAccess.getStepStatesCountAccess().getReferencedStepAssignment_2_0(), "rule__StepStatesCount__ReferencedStepAssignment_2_0");
					put(grammarAccess.getStepStatesCountAccess().getMaxCountAssignment_4(), "rule__StepStatesCount__MaxCountAssignment_4");
					put(grammarAccess.getStepStatesCountAccess().getCountAssignment_6(), "rule__StepStatesCount__CountAssignment_6");
					put(grammarAccess.getStepStatesCountAccess().getStepStateAssignment_8(), "rule__StepStatesCount__StepStateAssignment_8");
					put(grammarAccess.getStepStatesAccess().getNameAssignment_0(), "rule__StepStates__NameAssignment_0");
					put(grammarAccess.getStepStatesAccess().getReferencedStepAssignment_1_0(), "rule__StepStates__ReferencedStepAssignment_1_0");
					put(grammarAccess.getStepStatesAccess().getStepStateAssignment_2(), "rule__StepStates__StepStateAssignment_2");
					put(grammarAccess.getStepStateAccess().getVarStateAssignment_2(), "rule__StepState__VarStateAssignment_2");
					put(grammarAccess.getVarStateAccess().getNameAssignment_0(), "rule__VarState__NameAssignment_0");
					put(grammarAccess.getVarStateAccess().getValueAssignment_2(), "rule__VarState__ValueAssignment_2");
				}
			};
		}
		return nameMappings.get(element);
	}
			
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public WitnessGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(WitnessGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
