package fr.irisa.atsyra.witness

import com.google.inject.Inject

import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider
import org.eclipse.xtext.naming.IQualifiedNameConverter
import org.eclipse.xtext.naming.QualifiedName
import fr.irisa.atsyra.witness.witness.StepStatesCount

class WitnessQualifiedNameProvider extends DefaultDeclarativeQualifiedNameProvider {
	@Inject
	private IQualifiedNameConverter converter = new IQualifiedNameConverter.DefaultImpl();

	override def QualifiedName getFullyQualifiedName(EObject obj) {
		if (obj instanceof StepStatesCount) {
			val text = obj.count.toString;
			if (text !== null) {
				return converter.toQualifiedName(text.toUpperCase);
			}
		}
		return super.getFullyQualifiedName(obj);
	}
}
