/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.witness.conversion;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.impl.AbstractDeclarativeValueConverterService;
import org.eclipse.xtext.conversion.impl.AbstractIDValueConverter;
import org.eclipse.xtext.conversion.impl.STRINGValueConverter;

import com.google.inject.Inject;

public class WitnessValueConverters extends AbstractDeclarativeValueConverterService {
	@Inject
	private AbstractIDValueConverter idValueConverter;

	@ValueConverter(rule = "ID")
	public IValueConverter<String> ID() {
		return idValueConverter;
	}

	@Inject
	private AbstractIDValueConverter terminalsIdValueConverter;

	@ValueConverter(rule = "org.eclipse.xtext.common.Terminals.ID")
	public IValueConverter<String> TerminalsID() {
		return terminalsIdValueConverter;
	}
	
	@Inject
	private IntValueConverter intValueConverter;
	
	@ValueConverter(rule = "Int")
	public IValueConverter<Integer> INT() {
		return intValueConverter;
	}
	
	@Inject
	private LongValueConverter longValueConverter;
	
	@ValueConverter(rule = "Long")
	public IValueConverter<Long> LONG() {
		return longValueConverter;
	}
	
	@Inject
	private IntValueConverter terminalsIntValueConverter;
	
	@ValueConverter(rule = "org.eclipse.xtext.common.Terminals.INT")
	public IValueConverter<Integer> TerminalsINT() {
		return terminalsIntValueConverter;
	}

	@Inject
	private STRINGValueConverter stringValueConverter;
	
	@ValueConverter(rule = "STRING")
	public IValueConverter<String> STRING() {
		return stringValueConverter;
	}

	@Inject
	private STRINGValueConverter terminalsStringValueConverter;
	
	@ValueConverter(rule = "org.eclipse.xtext.common.Terminals.STRING")
	public IValueConverter<String> TerminalsSTRING() {
		return terminalsStringValueConverter;
	}
}