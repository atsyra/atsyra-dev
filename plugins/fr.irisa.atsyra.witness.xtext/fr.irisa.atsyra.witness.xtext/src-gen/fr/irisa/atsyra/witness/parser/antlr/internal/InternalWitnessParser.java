/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package fr.irisa.atsyra.witness.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.atsyra.witness.services.WitnessGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalWitnessParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_NUMBER", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'witness'", "'{'", "'goal'", "'steps'", "'}'", "'states'", "'sequence_diagram'", "'@startuml'", "'@enduml'", "'->'", "':'", "'defaultValues'", "'[*'", "'/'", "'*]'", "'['", "']'", "'='", "'::'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_NUMBER=6;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalWitnessParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalWitnessParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalWitnessParser.tokenNames; }
    public String getGrammarFileName() { return "InternalWitness.g"; }



     	private WitnessGrammarAccess grammarAccess;

        public InternalWitnessParser(TokenStream input, WitnessGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "WitnessModel";
       	}

       	@Override
       	protected WitnessGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleWitnessModel"
    // InternalWitness.g:64:1: entryRuleWitnessModel returns [EObject current=null] : iv_ruleWitnessModel= ruleWitnessModel EOF ;
    public final EObject entryRuleWitnessModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWitnessModel = null;


        try {
            // InternalWitness.g:64:53: (iv_ruleWitnessModel= ruleWitnessModel EOF )
            // InternalWitness.g:65:2: iv_ruleWitnessModel= ruleWitnessModel EOF
            {
             newCompositeNode(grammarAccess.getWitnessModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWitnessModel=ruleWitnessModel();

            state._fsp--;

             current =iv_ruleWitnessModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWitnessModel"


    // $ANTLR start "ruleWitnessModel"
    // InternalWitness.g:71:1: ruleWitnessModel returns [EObject current=null] : ( (lv_witnesses_0_0= ruleWitness ) )* ;
    public final EObject ruleWitnessModel() throws RecognitionException {
        EObject current = null;

        EObject lv_witnesses_0_0 = null;



        	enterRule();

        try {
            // InternalWitness.g:77:2: ( ( (lv_witnesses_0_0= ruleWitness ) )* )
            // InternalWitness.g:78:2: ( (lv_witnesses_0_0= ruleWitness ) )*
            {
            // InternalWitness.g:78:2: ( (lv_witnesses_0_0= ruleWitness ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalWitness.g:79:3: (lv_witnesses_0_0= ruleWitness )
            	    {
            	    // InternalWitness.g:79:3: (lv_witnesses_0_0= ruleWitness )
            	    // InternalWitness.g:80:4: lv_witnesses_0_0= ruleWitness
            	    {

            	    				newCompositeNode(grammarAccess.getWitnessModelAccess().getWitnessesWitnessParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_witnesses_0_0=ruleWitness();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getWitnessModelRule());
            	    				}
            	    				add(
            	    					current,
            	    					"witnesses",
            	    					lv_witnesses_0_0,
            	    					"fr.irisa.atsyra.witness.Witness.Witness");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWitnessModel"


    // $ANTLR start "entryRuleWitness"
    // InternalWitness.g:100:1: entryRuleWitness returns [EObject current=null] : iv_ruleWitness= ruleWitness EOF ;
    public final EObject entryRuleWitness() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWitness = null;


        try {
            // InternalWitness.g:100:48: (iv_ruleWitness= ruleWitness EOF )
            // InternalWitness.g:101:2: iv_ruleWitness= ruleWitness EOF
            {
             newCompositeNode(grammarAccess.getWitnessRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWitness=ruleWitness();

            state._fsp--;

             current =iv_ruleWitness; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWitness"


    // $ANTLR start "ruleWitness"
    // InternalWitness.g:107:1: ruleWitness returns [EObject current=null] : (otherlv_0= 'witness' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'goal' ( (lv_goalName_4_0= RULE_STRING ) ) otherlv_5= 'steps' otherlv_6= '{' ( (lv_steps_7_0= ruleStep ) )* otherlv_8= '}' otherlv_9= 'states' otherlv_10= '{' ( (lv_stepStates_11_0= ruleAbstractStepStates ) )* otherlv_12= '}' (otherlv_13= 'sequence_diagram' otherlv_14= '{' otherlv_15= '@startuml' ( (lv_sequenceRules_16_0= ruleSequenceRule ) )* otherlv_17= '@enduml' otherlv_18= '}' )? otherlv_19= '}' ) ;
    public final EObject ruleWitness() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_goalName_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        EObject lv_steps_7_0 = null;

        EObject lv_stepStates_11_0 = null;

        EObject lv_sequenceRules_16_0 = null;



        	enterRule();

        try {
            // InternalWitness.g:113:2: ( (otherlv_0= 'witness' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'goal' ( (lv_goalName_4_0= RULE_STRING ) ) otherlv_5= 'steps' otherlv_6= '{' ( (lv_steps_7_0= ruleStep ) )* otherlv_8= '}' otherlv_9= 'states' otherlv_10= '{' ( (lv_stepStates_11_0= ruleAbstractStepStates ) )* otherlv_12= '}' (otherlv_13= 'sequence_diagram' otherlv_14= '{' otherlv_15= '@startuml' ( (lv_sequenceRules_16_0= ruleSequenceRule ) )* otherlv_17= '@enduml' otherlv_18= '}' )? otherlv_19= '}' ) )
            // InternalWitness.g:114:2: (otherlv_0= 'witness' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'goal' ( (lv_goalName_4_0= RULE_STRING ) ) otherlv_5= 'steps' otherlv_6= '{' ( (lv_steps_7_0= ruleStep ) )* otherlv_8= '}' otherlv_9= 'states' otherlv_10= '{' ( (lv_stepStates_11_0= ruleAbstractStepStates ) )* otherlv_12= '}' (otherlv_13= 'sequence_diagram' otherlv_14= '{' otherlv_15= '@startuml' ( (lv_sequenceRules_16_0= ruleSequenceRule ) )* otherlv_17= '@enduml' otherlv_18= '}' )? otherlv_19= '}' )
            {
            // InternalWitness.g:114:2: (otherlv_0= 'witness' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'goal' ( (lv_goalName_4_0= RULE_STRING ) ) otherlv_5= 'steps' otherlv_6= '{' ( (lv_steps_7_0= ruleStep ) )* otherlv_8= '}' otherlv_9= 'states' otherlv_10= '{' ( (lv_stepStates_11_0= ruleAbstractStepStates ) )* otherlv_12= '}' (otherlv_13= 'sequence_diagram' otherlv_14= '{' otherlv_15= '@startuml' ( (lv_sequenceRules_16_0= ruleSequenceRule ) )* otherlv_17= '@enduml' otherlv_18= '}' )? otherlv_19= '}' )
            // InternalWitness.g:115:3: otherlv_0= 'witness' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'goal' ( (lv_goalName_4_0= RULE_STRING ) ) otherlv_5= 'steps' otherlv_6= '{' ( (lv_steps_7_0= ruleStep ) )* otherlv_8= '}' otherlv_9= 'states' otherlv_10= '{' ( (lv_stepStates_11_0= ruleAbstractStepStates ) )* otherlv_12= '}' (otherlv_13= 'sequence_diagram' otherlv_14= '{' otherlv_15= '@startuml' ( (lv_sequenceRules_16_0= ruleSequenceRule ) )* otherlv_17= '@enduml' otherlv_18= '}' )? otherlv_19= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getWitnessAccess().getWitnessKeyword_0());
            		
            // InternalWitness.g:119:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalWitness.g:120:4: (lv_name_1_0= RULE_ID )
            {
            // InternalWitness.g:120:4: (lv_name_1_0= RULE_ID )
            // InternalWitness.g:121:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_1_0, grammarAccess.getWitnessAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWitnessRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.witness.Witness.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_3, grammarAccess.getWitnessAccess().getGoalKeyword_3());
            		
            // InternalWitness.g:145:3: ( (lv_goalName_4_0= RULE_STRING ) )
            // InternalWitness.g:146:4: (lv_goalName_4_0= RULE_STRING )
            {
            // InternalWitness.g:146:4: (lv_goalName_4_0= RULE_STRING )
            // InternalWitness.g:147:5: lv_goalName_4_0= RULE_STRING
            {
            lv_goalName_4_0=(Token)match(input,RULE_STRING,FOLLOW_8); 

            					newLeafNode(lv_goalName_4_0, grammarAccess.getWitnessAccess().getGoalNameSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWitnessRule());
            					}
            					setWithLastConsumed(
            						current,
            						"goalName",
            						lv_goalName_4_0,
            						"fr.irisa.atsyra.witness.Witness.STRING");
            				

            }


            }

            otherlv_5=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_5, grammarAccess.getWitnessAccess().getStepsKeyword_5());
            		
            otherlv_6=(Token)match(input,12,FOLLOW_9); 

            			newLeafNode(otherlv_6, grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_6());
            		
            // InternalWitness.g:171:3: ( (lv_steps_7_0= ruleStep ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalWitness.g:172:4: (lv_steps_7_0= ruleStep )
            	    {
            	    // InternalWitness.g:172:4: (lv_steps_7_0= ruleStep )
            	    // InternalWitness.g:173:5: lv_steps_7_0= ruleStep
            	    {

            	    					newCompositeNode(grammarAccess.getWitnessAccess().getStepsStepParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_steps_7_0=ruleStep();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getWitnessRule());
            	    					}
            	    					add(
            	    						current,
            	    						"steps",
            	    						lv_steps_7_0,
            	    						"fr.irisa.atsyra.witness.Witness.Step");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_8=(Token)match(input,15,FOLLOW_10); 

            			newLeafNode(otherlv_8, grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_8());
            		
            otherlv_9=(Token)match(input,16,FOLLOW_5); 

            			newLeafNode(otherlv_9, grammarAccess.getWitnessAccess().getStatesKeyword_9());
            		
            otherlv_10=(Token)match(input,12,FOLLOW_11); 

            			newLeafNode(otherlv_10, grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_10());
            		
            // InternalWitness.g:202:3: ( (lv_stepStates_11_0= ruleAbstractStepStates ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_NUMBER) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalWitness.g:203:4: (lv_stepStates_11_0= ruleAbstractStepStates )
            	    {
            	    // InternalWitness.g:203:4: (lv_stepStates_11_0= ruleAbstractStepStates )
            	    // InternalWitness.g:204:5: lv_stepStates_11_0= ruleAbstractStepStates
            	    {

            	    					newCompositeNode(grammarAccess.getWitnessAccess().getStepStatesAbstractStepStatesParserRuleCall_11_0());
            	    				
            	    pushFollow(FOLLOW_11);
            	    lv_stepStates_11_0=ruleAbstractStepStates();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getWitnessRule());
            	    					}
            	    					add(
            	    						current,
            	    						"stepStates",
            	    						lv_stepStates_11_0,
            	    						"fr.irisa.atsyra.witness.Witness.AbstractStepStates");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_12=(Token)match(input,15,FOLLOW_12); 

            			newLeafNode(otherlv_12, grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_12());
            		
            // InternalWitness.g:225:3: (otherlv_13= 'sequence_diagram' otherlv_14= '{' otherlv_15= '@startuml' ( (lv_sequenceRules_16_0= ruleSequenceRule ) )* otherlv_17= '@enduml' otherlv_18= '}' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==17) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalWitness.g:226:4: otherlv_13= 'sequence_diagram' otherlv_14= '{' otherlv_15= '@startuml' ( (lv_sequenceRules_16_0= ruleSequenceRule ) )* otherlv_17= '@enduml' otherlv_18= '}'
                    {
                    otherlv_13=(Token)match(input,17,FOLLOW_5); 

                    				newLeafNode(otherlv_13, grammarAccess.getWitnessAccess().getSequence_diagramKeyword_13_0());
                    			
                    otherlv_14=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_14, grammarAccess.getWitnessAccess().getLeftCurlyBracketKeyword_13_1());
                    			
                    otherlv_15=(Token)match(input,18,FOLLOW_14); 

                    				newLeafNode(otherlv_15, grammarAccess.getWitnessAccess().getStartumlKeyword_13_2());
                    			
                    // InternalWitness.g:238:4: ( (lv_sequenceRules_16_0= ruleSequenceRule ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==RULE_NUMBER) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalWitness.g:239:5: (lv_sequenceRules_16_0= ruleSequenceRule )
                    	    {
                    	    // InternalWitness.g:239:5: (lv_sequenceRules_16_0= ruleSequenceRule )
                    	    // InternalWitness.g:240:6: lv_sequenceRules_16_0= ruleSequenceRule
                    	    {

                    	    						newCompositeNode(grammarAccess.getWitnessAccess().getSequenceRulesSequenceRuleParserRuleCall_13_3_0());
                    	    					
                    	    pushFollow(FOLLOW_14);
                    	    lv_sequenceRules_16_0=ruleSequenceRule();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getWitnessRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"sequenceRules",
                    	    							lv_sequenceRules_16_0,
                    	    							"fr.irisa.atsyra.witness.Witness.SequenceRule");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,19,FOLLOW_15); 

                    				newLeafNode(otherlv_17, grammarAccess.getWitnessAccess().getEndumlKeyword_13_4());
                    			
                    otherlv_18=(Token)match(input,15,FOLLOW_15); 

                    				newLeafNode(otherlv_18, grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_13_5());
                    			

                    }
                    break;

            }

            otherlv_19=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_19, grammarAccess.getWitnessAccess().getRightCurlyBracketKeyword_14());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWitness"


    // $ANTLR start "entryRuleSequenceRule"
    // InternalWitness.g:274:1: entryRuleSequenceRule returns [EObject current=null] : iv_ruleSequenceRule= ruleSequenceRule EOF ;
    public final EObject entryRuleSequenceRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSequenceRule = null;


        try {
            // InternalWitness.g:274:53: (iv_ruleSequenceRule= ruleSequenceRule EOF )
            // InternalWitness.g:275:2: iv_ruleSequenceRule= ruleSequenceRule EOF
            {
             newCompositeNode(grammarAccess.getSequenceRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSequenceRule=ruleSequenceRule();

            state._fsp--;

             current =iv_ruleSequenceRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSequenceRule"


    // $ANTLR start "ruleSequenceRule"
    // InternalWitness.g:281:1: ruleSequenceRule returns [EObject current=null] : ( ( (lv_from_0_0= ruleInt ) ) otherlv_1= '->' ( (lv_to_2_0= ruleInt ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleSequenceRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_from_0_0 = null;

        AntlrDatatypeRuleToken lv_to_2_0 = null;



        	enterRule();

        try {
            // InternalWitness.g:287:2: ( ( ( (lv_from_0_0= ruleInt ) ) otherlv_1= '->' ( (lv_to_2_0= ruleInt ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) ) )
            // InternalWitness.g:288:2: ( ( (lv_from_0_0= ruleInt ) ) otherlv_1= '->' ( (lv_to_2_0= ruleInt ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) )
            {
            // InternalWitness.g:288:2: ( ( (lv_from_0_0= ruleInt ) ) otherlv_1= '->' ( (lv_to_2_0= ruleInt ) ) otherlv_3= ':' ( ( ruleQualifiedName ) ) )
            // InternalWitness.g:289:3: ( (lv_from_0_0= ruleInt ) ) otherlv_1= '->' ( (lv_to_2_0= ruleInt ) ) otherlv_3= ':' ( ( ruleQualifiedName ) )
            {
            // InternalWitness.g:289:3: ( (lv_from_0_0= ruleInt ) )
            // InternalWitness.g:290:4: (lv_from_0_0= ruleInt )
            {
            // InternalWitness.g:290:4: (lv_from_0_0= ruleInt )
            // InternalWitness.g:291:5: lv_from_0_0= ruleInt
            {

            					newCompositeNode(grammarAccess.getSequenceRuleAccess().getFromIntParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_16);
            lv_from_0_0=ruleInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSequenceRuleRule());
            					}
            					set(
            						current,
            						"from",
            						lv_from_0_0,
            						"fr.irisa.atsyra.witness.Witness.Int");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,20,FOLLOW_17); 

            			newLeafNode(otherlv_1, grammarAccess.getSequenceRuleAccess().getHyphenMinusGreaterThanSignKeyword_1());
            		
            // InternalWitness.g:312:3: ( (lv_to_2_0= ruleInt ) )
            // InternalWitness.g:313:4: (lv_to_2_0= ruleInt )
            {
            // InternalWitness.g:313:4: (lv_to_2_0= ruleInt )
            // InternalWitness.g:314:5: lv_to_2_0= ruleInt
            {

            					newCompositeNode(grammarAccess.getSequenceRuleAccess().getToIntParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_18);
            lv_to_2_0=ruleInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSequenceRuleRule());
            					}
            					set(
            						current,
            						"to",
            						lv_to_2_0,
            						"fr.irisa.atsyra.witness.Witness.Int");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,21,FOLLOW_4); 

            			newLeafNode(otherlv_3, grammarAccess.getSequenceRuleAccess().getColonKeyword_3());
            		
            // InternalWitness.g:335:3: ( ( ruleQualifiedName ) )
            // InternalWitness.g:336:4: ( ruleQualifiedName )
            {
            // InternalWitness.g:336:4: ( ruleQualifiedName )
            // InternalWitness.g:337:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSequenceRuleRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSequenceRuleAccess().getReferencedStepStepCrossReference_4_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSequenceRule"


    // $ANTLR start "entryRuleStep"
    // InternalWitness.g:355:1: entryRuleStep returns [EObject current=null] : iv_ruleStep= ruleStep EOF ;
    public final EObject entryRuleStep() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStep = null;


        try {
            // InternalWitness.g:355:45: (iv_ruleStep= ruleStep EOF )
            // InternalWitness.g:356:2: iv_ruleStep= ruleStep EOF
            {
             newCompositeNode(grammarAccess.getStepRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStep=ruleStep();

            state._fsp--;

             current =iv_ruleStep; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStep"


    // $ANTLR start "ruleStep"
    // InternalWitness.g:362:1: ruleStep returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleStep() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalWitness.g:368:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalWitness.g:369:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalWitness.g:369:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalWitness.g:370:3: (lv_name_0_0= RULE_ID )
            {
            // InternalWitness.g:370:3: (lv_name_0_0= RULE_ID )
            // InternalWitness.g:371:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getStepAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getStepRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"fr.irisa.atsyra.witness.Witness.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStep"


    // $ANTLR start "entryRuleAbstractStepStates"
    // InternalWitness.g:390:1: entryRuleAbstractStepStates returns [EObject current=null] : iv_ruleAbstractStepStates= ruleAbstractStepStates EOF ;
    public final EObject entryRuleAbstractStepStates() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractStepStates = null;


        try {
            // InternalWitness.g:390:59: (iv_ruleAbstractStepStates= ruleAbstractStepStates EOF )
            // InternalWitness.g:391:2: iv_ruleAbstractStepStates= ruleAbstractStepStates EOF
            {
             newCompositeNode(grammarAccess.getAbstractStepStatesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractStepStates=ruleAbstractStepStates();

            state._fsp--;

             current =iv_ruleAbstractStepStates; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractStepStates"


    // $ANTLR start "ruleAbstractStepStates"
    // InternalWitness.g:397:1: ruleAbstractStepStates returns [EObject current=null] : (this_StepStatesCount_0= ruleStepStatesCount | this_StepStates_1= ruleStepStates ) ;
    public final EObject ruleAbstractStepStates() throws RecognitionException {
        EObject current = null;

        EObject this_StepStatesCount_0 = null;

        EObject this_StepStates_1 = null;



        	enterRule();

        try {
            // InternalWitness.g:403:2: ( (this_StepStatesCount_0= ruleStepStatesCount | this_StepStates_1= ruleStepStates ) )
            // InternalWitness.g:404:2: (this_StepStatesCount_0= ruleStepStatesCount | this_StepStates_1= ruleStepStates )
            {
            // InternalWitness.g:404:2: (this_StepStatesCount_0= ruleStepStatesCount | this_StepStates_1= ruleStepStates )
            int alt6=2;
            alt6 = dfa6.predict(input);
            switch (alt6) {
                case 1 :
                    // InternalWitness.g:405:3: this_StepStatesCount_0= ruleStepStatesCount
                    {

                    			newCompositeNode(grammarAccess.getAbstractStepStatesAccess().getStepStatesCountParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_StepStatesCount_0=ruleStepStatesCount();

                    state._fsp--;


                    			current = this_StepStatesCount_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalWitness.g:414:3: this_StepStates_1= ruleStepStates
                    {

                    			newCompositeNode(grammarAccess.getAbstractStepStatesAccess().getStepStatesParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_StepStates_1=ruleStepStates();

                    state._fsp--;


                    			current = this_StepStates_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractStepStates"


    // $ANTLR start "entryRuleStepStatesCount"
    // InternalWitness.g:426:1: entryRuleStepStatesCount returns [EObject current=null] : iv_ruleStepStatesCount= ruleStepStatesCount EOF ;
    public final EObject entryRuleStepStatesCount() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStepStatesCount = null;


        try {
            // InternalWitness.g:426:56: (iv_ruleStepStatesCount= ruleStepStatesCount EOF )
            // InternalWitness.g:427:2: iv_ruleStepStatesCount= ruleStepStatesCount EOF
            {
             newCompositeNode(grammarAccess.getStepStatesCountRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStepStatesCount=ruleStepStatesCount();

            state._fsp--;

             current =iv_ruleStepStatesCount; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStepStatesCount"


    // $ANTLR start "ruleStepStatesCount"
    // InternalWitness.g:433:1: ruleStepStatesCount returns [EObject current=null] : ( () ( (lv_name_1_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_3= 'defaultValues' ) otherlv_4= '[*' ( (lv_maxCount_5_0= ruleLong ) ) otherlv_6= '/' ( (lv_count_7_0= ruleLong ) ) otherlv_8= '*]' ( (lv_stepState_9_0= ruleStepState ) )* ) ;
    public final EObject ruleStepStatesCount() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_maxCount_5_0 = null;

        AntlrDatatypeRuleToken lv_count_7_0 = null;

        EObject lv_stepState_9_0 = null;



        	enterRule();

        try {
            // InternalWitness.g:439:2: ( ( () ( (lv_name_1_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_3= 'defaultValues' ) otherlv_4= '[*' ( (lv_maxCount_5_0= ruleLong ) ) otherlv_6= '/' ( (lv_count_7_0= ruleLong ) ) otherlv_8= '*]' ( (lv_stepState_9_0= ruleStepState ) )* ) )
            // InternalWitness.g:440:2: ( () ( (lv_name_1_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_3= 'defaultValues' ) otherlv_4= '[*' ( (lv_maxCount_5_0= ruleLong ) ) otherlv_6= '/' ( (lv_count_7_0= ruleLong ) ) otherlv_8= '*]' ( (lv_stepState_9_0= ruleStepState ) )* )
            {
            // InternalWitness.g:440:2: ( () ( (lv_name_1_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_3= 'defaultValues' ) otherlv_4= '[*' ( (lv_maxCount_5_0= ruleLong ) ) otherlv_6= '/' ( (lv_count_7_0= ruleLong ) ) otherlv_8= '*]' ( (lv_stepState_9_0= ruleStepState ) )* )
            // InternalWitness.g:441:3: () ( (lv_name_1_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_3= 'defaultValues' ) otherlv_4= '[*' ( (lv_maxCount_5_0= ruleLong ) ) otherlv_6= '/' ( (lv_count_7_0= ruleLong ) ) otherlv_8= '*]' ( (lv_stepState_9_0= ruleStepState ) )*
            {
            // InternalWitness.g:441:3: ()
            // InternalWitness.g:442:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStepStatesCountAccess().getStepStatesCountAction_0(),
            					current);
            			

            }

            // InternalWitness.g:448:3: ( (lv_name_1_0= ruleInt ) )
            // InternalWitness.g:449:4: (lv_name_1_0= ruleInt )
            {
            // InternalWitness.g:449:4: (lv_name_1_0= ruleInt )
            // InternalWitness.g:450:5: lv_name_1_0= ruleInt
            {

            					newCompositeNode(grammarAccess.getStepStatesCountAccess().getNameIntParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_19);
            lv_name_1_0=ruleInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStepStatesCountRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.irisa.atsyra.witness.Witness.Int");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalWitness.g:467:3: ( ( ( ruleQualifiedName ) ) | otherlv_3= 'defaultValues' )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            else if ( (LA7_0==22) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalWitness.g:468:4: ( ( ruleQualifiedName ) )
                    {
                    // InternalWitness.g:468:4: ( ( ruleQualifiedName ) )
                    // InternalWitness.g:469:5: ( ruleQualifiedName )
                    {
                    // InternalWitness.g:469:5: ( ruleQualifiedName )
                    // InternalWitness.g:470:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getStepStatesCountRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getStepStatesCountAccess().getReferencedStepStepCrossReference_2_0_0());
                    					
                    pushFollow(FOLLOW_20);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalWitness.g:485:4: otherlv_3= 'defaultValues'
                    {
                    otherlv_3=(Token)match(input,22,FOLLOW_20); 

                    				newLeafNode(otherlv_3, grammarAccess.getStepStatesCountAccess().getDefaultValuesKeyword_2_1());
                    			

                    }
                    break;

            }

            otherlv_4=(Token)match(input,23,FOLLOW_17); 

            			newLeafNode(otherlv_4, grammarAccess.getStepStatesCountAccess().getLeftSquareBracketAsteriskKeyword_3());
            		
            // InternalWitness.g:494:3: ( (lv_maxCount_5_0= ruleLong ) )
            // InternalWitness.g:495:4: (lv_maxCount_5_0= ruleLong )
            {
            // InternalWitness.g:495:4: (lv_maxCount_5_0= ruleLong )
            // InternalWitness.g:496:5: lv_maxCount_5_0= ruleLong
            {

            					newCompositeNode(grammarAccess.getStepStatesCountAccess().getMaxCountLongParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_21);
            lv_maxCount_5_0=ruleLong();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStepStatesCountRule());
            					}
            					set(
            						current,
            						"maxCount",
            						lv_maxCount_5_0,
            						"fr.irisa.atsyra.witness.Witness.Long");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,24,FOLLOW_17); 

            			newLeafNode(otherlv_6, grammarAccess.getStepStatesCountAccess().getSolidusKeyword_5());
            		
            // InternalWitness.g:517:3: ( (lv_count_7_0= ruleLong ) )
            // InternalWitness.g:518:4: (lv_count_7_0= ruleLong )
            {
            // InternalWitness.g:518:4: (lv_count_7_0= ruleLong )
            // InternalWitness.g:519:5: lv_count_7_0= ruleLong
            {

            					newCompositeNode(grammarAccess.getStepStatesCountAccess().getCountLongParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_22);
            lv_count_7_0=ruleLong();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStepStatesCountRule());
            					}
            					set(
            						current,
            						"count",
            						lv_count_7_0,
            						"fr.irisa.atsyra.witness.Witness.Long");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,25,FOLLOW_23); 

            			newLeafNode(otherlv_8, grammarAccess.getStepStatesCountAccess().getAsteriskRightSquareBracketKeyword_7());
            		
            // InternalWitness.g:540:3: ( (lv_stepState_9_0= ruleStepState ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==26) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalWitness.g:541:4: (lv_stepState_9_0= ruleStepState )
            	    {
            	    // InternalWitness.g:541:4: (lv_stepState_9_0= ruleStepState )
            	    // InternalWitness.g:542:5: lv_stepState_9_0= ruleStepState
            	    {

            	    					newCompositeNode(grammarAccess.getStepStatesCountAccess().getStepStateStepStateParserRuleCall_8_0());
            	    				
            	    pushFollow(FOLLOW_23);
            	    lv_stepState_9_0=ruleStepState();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStepStatesCountRule());
            	    					}
            	    					add(
            	    						current,
            	    						"stepState",
            	    						lv_stepState_9_0,
            	    						"fr.irisa.atsyra.witness.Witness.StepState");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStepStatesCount"


    // $ANTLR start "entryRuleStepStates"
    // InternalWitness.g:563:1: entryRuleStepStates returns [EObject current=null] : iv_ruleStepStates= ruleStepStates EOF ;
    public final EObject entryRuleStepStates() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStepStates = null;


        try {
            // InternalWitness.g:563:51: (iv_ruleStepStates= ruleStepStates EOF )
            // InternalWitness.g:564:2: iv_ruleStepStates= ruleStepStates EOF
            {
             newCompositeNode(grammarAccess.getStepStatesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStepStates=ruleStepStates();

            state._fsp--;

             current =iv_ruleStepStates; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStepStates"


    // $ANTLR start "ruleStepStates"
    // InternalWitness.g:570:1: ruleStepStates returns [EObject current=null] : ( ( (lv_name_0_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_2= 'defaultValues' ) ( (lv_stepState_3_0= ruleStepState ) )* ) ;
    public final EObject ruleStepStates() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        EObject lv_stepState_3_0 = null;



        	enterRule();

        try {
            // InternalWitness.g:576:2: ( ( ( (lv_name_0_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_2= 'defaultValues' ) ( (lv_stepState_3_0= ruleStepState ) )* ) )
            // InternalWitness.g:577:2: ( ( (lv_name_0_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_2= 'defaultValues' ) ( (lv_stepState_3_0= ruleStepState ) )* )
            {
            // InternalWitness.g:577:2: ( ( (lv_name_0_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_2= 'defaultValues' ) ( (lv_stepState_3_0= ruleStepState ) )* )
            // InternalWitness.g:578:3: ( (lv_name_0_0= ruleInt ) ) ( ( ( ruleQualifiedName ) ) | otherlv_2= 'defaultValues' ) ( (lv_stepState_3_0= ruleStepState ) )*
            {
            // InternalWitness.g:578:3: ( (lv_name_0_0= ruleInt ) )
            // InternalWitness.g:579:4: (lv_name_0_0= ruleInt )
            {
            // InternalWitness.g:579:4: (lv_name_0_0= ruleInt )
            // InternalWitness.g:580:5: lv_name_0_0= ruleInt
            {

            					newCompositeNode(grammarAccess.getStepStatesAccess().getNameIntParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_19);
            lv_name_0_0=ruleInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStepStatesRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"fr.irisa.atsyra.witness.Witness.Int");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalWitness.g:597:3: ( ( ( ruleQualifiedName ) ) | otherlv_2= 'defaultValues' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID) ) {
                alt9=1;
            }
            else if ( (LA9_0==22) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalWitness.g:598:4: ( ( ruleQualifiedName ) )
                    {
                    // InternalWitness.g:598:4: ( ( ruleQualifiedName ) )
                    // InternalWitness.g:599:5: ( ruleQualifiedName )
                    {
                    // InternalWitness.g:599:5: ( ruleQualifiedName )
                    // InternalWitness.g:600:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getStepStatesRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getStepStatesAccess().getReferencedStepStepCrossReference_1_0_0());
                    					
                    pushFollow(FOLLOW_23);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalWitness.g:615:4: otherlv_2= 'defaultValues'
                    {
                    otherlv_2=(Token)match(input,22,FOLLOW_23); 

                    				newLeafNode(otherlv_2, grammarAccess.getStepStatesAccess().getDefaultValuesKeyword_1_1());
                    			

                    }
                    break;

            }

            // InternalWitness.g:620:3: ( (lv_stepState_3_0= ruleStepState ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==26) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalWitness.g:621:4: (lv_stepState_3_0= ruleStepState )
            	    {
            	    // InternalWitness.g:621:4: (lv_stepState_3_0= ruleStepState )
            	    // InternalWitness.g:622:5: lv_stepState_3_0= ruleStepState
            	    {

            	    					newCompositeNode(grammarAccess.getStepStatesAccess().getStepStateStepStateParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_23);
            	    lv_stepState_3_0=ruleStepState();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStepStatesRule());
            	    					}
            	    					add(
            	    						current,
            	    						"stepState",
            	    						lv_stepState_3_0,
            	    						"fr.irisa.atsyra.witness.Witness.StepState");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStepStates"


    // $ANTLR start "entryRuleStepState"
    // InternalWitness.g:643:1: entryRuleStepState returns [EObject current=null] : iv_ruleStepState= ruleStepState EOF ;
    public final EObject entryRuleStepState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStepState = null;


        try {
            // InternalWitness.g:643:50: (iv_ruleStepState= ruleStepState EOF )
            // InternalWitness.g:644:2: iv_ruleStepState= ruleStepState EOF
            {
             newCompositeNode(grammarAccess.getStepStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStepState=ruleStepState();

            state._fsp--;

             current =iv_ruleStepState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStepState"


    // $ANTLR start "ruleStepState"
    // InternalWitness.g:650:1: ruleStepState returns [EObject current=null] : ( () otherlv_1= '[' ( (lv_varState_2_0= ruleVarState ) )* otherlv_3= ']' ) ;
    public final EObject ruleStepState() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_varState_2_0 = null;



        	enterRule();

        try {
            // InternalWitness.g:656:2: ( ( () otherlv_1= '[' ( (lv_varState_2_0= ruleVarState ) )* otherlv_3= ']' ) )
            // InternalWitness.g:657:2: ( () otherlv_1= '[' ( (lv_varState_2_0= ruleVarState ) )* otherlv_3= ']' )
            {
            // InternalWitness.g:657:2: ( () otherlv_1= '[' ( (lv_varState_2_0= ruleVarState ) )* otherlv_3= ']' )
            // InternalWitness.g:658:3: () otherlv_1= '[' ( (lv_varState_2_0= ruleVarState ) )* otherlv_3= ']'
            {
            // InternalWitness.g:658:3: ()
            // InternalWitness.g:659:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStepStateAccess().getStepStateAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,26,FOLLOW_24); 

            			newLeafNode(otherlv_1, grammarAccess.getStepStateAccess().getLeftSquareBracketKeyword_1());
            		
            // InternalWitness.g:669:3: ( (lv_varState_2_0= ruleVarState ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalWitness.g:670:4: (lv_varState_2_0= ruleVarState )
            	    {
            	    // InternalWitness.g:670:4: (lv_varState_2_0= ruleVarState )
            	    // InternalWitness.g:671:5: lv_varState_2_0= ruleVarState
            	    {

            	    					newCompositeNode(grammarAccess.getStepStateAccess().getVarStateVarStateParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_24);
            	    lv_varState_2_0=ruleVarState();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStepStateRule());
            	    					}
            	    					add(
            	    						current,
            	    						"varState",
            	    						lv_varState_2_0,
            	    						"fr.irisa.atsyra.witness.Witness.VarState");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            otherlv_3=(Token)match(input,27,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getStepStateAccess().getRightSquareBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStepState"


    // $ANTLR start "entryRuleVarState"
    // InternalWitness.g:696:1: entryRuleVarState returns [EObject current=null] : iv_ruleVarState= ruleVarState EOF ;
    public final EObject entryRuleVarState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVarState = null;


        try {
            // InternalWitness.g:696:49: (iv_ruleVarState= ruleVarState EOF )
            // InternalWitness.g:697:2: iv_ruleVarState= ruleVarState EOF
            {
             newCompositeNode(grammarAccess.getVarStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVarState=ruleVarState();

            state._fsp--;

             current =iv_ruleVarState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVarState"


    // $ANTLR start "ruleVarState"
    // InternalWitness.g:703:1: ruleVarState returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleInt ) ) ) ;
    public final EObject ruleVarState() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalWitness.g:709:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleInt ) ) ) )
            // InternalWitness.g:710:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleInt ) ) )
            {
            // InternalWitness.g:710:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleInt ) ) )
            // InternalWitness.g:711:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleInt ) )
            {
            // InternalWitness.g:711:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalWitness.g:712:4: (lv_name_0_0= RULE_ID )
            {
            // InternalWitness.g:712:4: (lv_name_0_0= RULE_ID )
            // InternalWitness.g:713:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_25); 

            					newLeafNode(lv_name_0_0, grammarAccess.getVarStateAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getVarStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"fr.irisa.atsyra.witness.Witness.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,28,FOLLOW_17); 

            			newLeafNode(otherlv_1, grammarAccess.getVarStateAccess().getEqualsSignKeyword_1());
            		
            // InternalWitness.g:733:3: ( (lv_value_2_0= ruleInt ) )
            // InternalWitness.g:734:4: (lv_value_2_0= ruleInt )
            {
            // InternalWitness.g:734:4: (lv_value_2_0= ruleInt )
            // InternalWitness.g:735:5: lv_value_2_0= ruleInt
            {

            					newCompositeNode(grammarAccess.getVarStateAccess().getValueIntParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_2_0=ruleInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getVarStateRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_2_0,
            						"fr.irisa.atsyra.witness.Witness.Int");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVarState"


    // $ANTLR start "entryRuleLong"
    // InternalWitness.g:756:1: entryRuleLong returns [String current=null] : iv_ruleLong= ruleLong EOF ;
    public final String entryRuleLong() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleLong = null;


        try {
            // InternalWitness.g:756:44: (iv_ruleLong= ruleLong EOF )
            // InternalWitness.g:757:2: iv_ruleLong= ruleLong EOF
            {
             newCompositeNode(grammarAccess.getLongRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLong=ruleLong();

            state._fsp--;

             current =iv_ruleLong.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLong"


    // $ANTLR start "ruleLong"
    // InternalWitness.g:763:1: ruleLong returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_NUMBER_0= RULE_NUMBER ;
    public final AntlrDatatypeRuleToken ruleLong() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_NUMBER_0=null;


        	enterRule();

        try {
            // InternalWitness.g:769:2: (this_NUMBER_0= RULE_NUMBER )
            // InternalWitness.g:770:2: this_NUMBER_0= RULE_NUMBER
            {
            this_NUMBER_0=(Token)match(input,RULE_NUMBER,FOLLOW_2); 

            		current.merge(this_NUMBER_0);
            	

            		newLeafNode(this_NUMBER_0, grammarAccess.getLongAccess().getNUMBERTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLong"


    // $ANTLR start "entryRuleInt"
    // InternalWitness.g:780:1: entryRuleInt returns [String current=null] : iv_ruleInt= ruleInt EOF ;
    public final String entryRuleInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleInt = null;


        try {
            // InternalWitness.g:780:43: (iv_ruleInt= ruleInt EOF )
            // InternalWitness.g:781:2: iv_ruleInt= ruleInt EOF
            {
             newCompositeNode(grammarAccess.getIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInt=ruleInt();

            state._fsp--;

             current =iv_ruleInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInt"


    // $ANTLR start "ruleInt"
    // InternalWitness.g:787:1: ruleInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_NUMBER_0= RULE_NUMBER ;
    public final AntlrDatatypeRuleToken ruleInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_NUMBER_0=null;


        	enterRule();

        try {
            // InternalWitness.g:793:2: (this_NUMBER_0= RULE_NUMBER )
            // InternalWitness.g:794:2: this_NUMBER_0= RULE_NUMBER
            {
            this_NUMBER_0=(Token)match(input,RULE_NUMBER,FOLLOW_2); 

            		current.merge(this_NUMBER_0);
            	

            		newLeafNode(this_NUMBER_0, grammarAccess.getIntAccess().getNUMBERTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInt"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalWitness.g:804:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalWitness.g:804:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalWitness.g:805:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalWitness.g:811:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalWitness.g:817:2: ( (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* ) )
            // InternalWitness.g:818:2: (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* )
            {
            // InternalWitness.g:818:2: (this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )* )
            // InternalWitness.g:819:3: this_ID_0= RULE_ID (kw= '::' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_26); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalWitness.g:826:3: (kw= '::' this_ID_2= RULE_ID )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==29) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalWitness.g:827:4: kw= '::' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,29,FOLLOW_4); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getColonColonKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_26); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"

    // Delegated rules


    protected DFA6 dfa6 = new DFA6(this);
    static final String dfa_1s = "\10\uffff";
    static final String dfa_2s = "\2\uffff\2\6\3\uffff\1\6";
    static final String dfa_3s = "\1\6\1\4\2\6\1\4\2\uffff\1\6";
    static final String dfa_4s = "\1\6\1\26\1\35\1\32\1\4\2\uffff\1\35";
    static final String dfa_5s = "\5\uffff\1\1\1\2\1\uffff";
    static final String dfa_6s = "\10\uffff}>";
    static final String[] dfa_7s = {
            "\1\1",
            "\1\2\21\uffff\1\3",
            "\1\6\10\uffff\1\6\7\uffff\1\5\2\uffff\1\6\2\uffff\1\4",
            "\1\6\10\uffff\1\6\7\uffff\1\5\2\uffff\1\6",
            "\1\7",
            "",
            "",
            "\1\6\10\uffff\1\6\7\uffff\1\5\2\uffff\1\6\2\uffff\1\4"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "404:2: (this_StepStatesCount_0= ruleStepStatesCount | this_StepStates_1= ruleStepStates )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000008040L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000080040L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000008000010L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000020000002L});

}