/*******************************************************************************
 * Copyright (c) 2014, 2018 IRISA/Inria  and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/*
 * generated by Xtext 2.12.0
 */
package fr.irisa.atsyra.witness.serializer;

import com.google.inject.Inject;
import fr.irisa.atsyra.witness.services.WitnessGrammarAccess;
import fr.irisa.atsyra.witness.witness.SequenceRule;
import fr.irisa.atsyra.witness.witness.Step;
import fr.irisa.atsyra.witness.witness.StepState;
import fr.irisa.atsyra.witness.witness.StepStates;
import fr.irisa.atsyra.witness.witness.StepStatesCount;
import fr.irisa.atsyra.witness.witness.VarState;
import fr.irisa.atsyra.witness.witness.Witness;
import fr.irisa.atsyra.witness.witness.WitnessModel;
import fr.irisa.atsyra.witness.witness.WitnessPackage;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class WitnessSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private WitnessGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == WitnessPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case WitnessPackage.SEQUENCE_RULE:
				sequence_SequenceRule(context, (SequenceRule) semanticObject); 
				return; 
			case WitnessPackage.STEP:
				sequence_Step(context, (Step) semanticObject); 
				return; 
			case WitnessPackage.STEP_STATE:
				sequence_StepState(context, (StepState) semanticObject); 
				return; 
			case WitnessPackage.STEP_STATES:
				sequence_StepStates(context, (StepStates) semanticObject); 
				return; 
			case WitnessPackage.STEP_STATES_COUNT:
				sequence_StepStatesCount(context, (StepStatesCount) semanticObject); 
				return; 
			case WitnessPackage.VAR_STATE:
				sequence_VarState(context, (VarState) semanticObject); 
				return; 
			case WitnessPackage.WITNESS:
				sequence_Witness(context, (Witness) semanticObject); 
				return; 
			case WitnessPackage.WITNESS_MODEL:
				sequence_WitnessModel(context, (WitnessModel) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     SequenceRule returns SequenceRule
	 *
	 * Constraint:
	 *     (from=Int to=Int referencedStep=[Step|QualifiedName])
	 */
	protected void sequence_SequenceRule(ISerializationContext context, SequenceRule semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, WitnessPackage.Literals.SEQUENCE_RULE__FROM) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, WitnessPackage.Literals.SEQUENCE_RULE__FROM));
			if (transientValues.isValueTransient(semanticObject, WitnessPackage.Literals.SEQUENCE_RULE__TO) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, WitnessPackage.Literals.SEQUENCE_RULE__TO));
			if (transientValues.isValueTransient(semanticObject, WitnessPackage.Literals.SEQUENCE_RULE__REFERENCED_STEP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, WitnessPackage.Literals.SEQUENCE_RULE__REFERENCED_STEP));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getSequenceRuleAccess().getFromIntParserRuleCall_0_0(), semanticObject.getFrom());
		feeder.accept(grammarAccess.getSequenceRuleAccess().getToIntParserRuleCall_2_0(), semanticObject.getTo());
		feeder.accept(grammarAccess.getSequenceRuleAccess().getReferencedStepStepQualifiedNameParserRuleCall_4_0_1(), semanticObject.eGet(WitnessPackage.Literals.SEQUENCE_RULE__REFERENCED_STEP, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     StepState returns StepState
	 *
	 * Constraint:
	 *     varState+=VarState*
	 */
	protected void sequence_StepState(ISerializationContext context, StepState semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AbstractStepStates returns StepStatesCount
	 *     StepStatesCount returns StepStatesCount
	 *
	 * Constraint:
	 *     (name=Int referencedStep=[Step|QualifiedName]? maxCount=Long count=Long stepState+=StepState*)
	 */
	protected void sequence_StepStatesCount(ISerializationContext context, StepStatesCount semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AbstractStepStates returns StepStates
	 *     StepStates returns StepStates
	 *
	 * Constraint:
	 *     (name=Int referencedStep=[Step|QualifiedName]? stepState+=StepState*)
	 */
	protected void sequence_StepStates(ISerializationContext context, StepStates semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Step returns Step
	 *
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_Step(ISerializationContext context, Step semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, WitnessPackage.Literals.STEP__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, WitnessPackage.Literals.STEP__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getStepAccess().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     VarState returns VarState
	 *
	 * Constraint:
	 *     (name=ID value=Int)
	 */
	protected void sequence_VarState(ISerializationContext context, VarState semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, WitnessPackage.Literals.VAR_STATE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, WitnessPackage.Literals.VAR_STATE__NAME));
			if (transientValues.isValueTransient(semanticObject, WitnessPackage.Literals.VAR_STATE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, WitnessPackage.Literals.VAR_STATE__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getVarStateAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getVarStateAccess().getValueIntParserRuleCall_2_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     WitnessModel returns WitnessModel
	 *
	 * Constraint:
	 *     witnesses+=Witness+
	 */
	protected void sequence_WitnessModel(ISerializationContext context, WitnessModel semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Witness returns Witness
	 *
	 * Constraint:
	 *     (name=ID goalName=STRING steps+=Step* stepStates+=AbstractStepStates* sequenceRules+=SequenceRule*)
	 */
	protected void sequence_Witness(ISerializationContext context, Witness semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
}
