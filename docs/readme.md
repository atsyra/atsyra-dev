### Quick Setup & Commands

Prerequisites: 

**[AsciiDoctor](http://asciidoctor.org/)** RubyGem can be installed with the following command:

```
gem install asciidoctor
gem install asciidoctor-diagram

```

-----

Generate Eclipse Help:

```
mvn  clean verify -pl .,releng/neon,docs/fr.irisa.atsyra.building.doc

```

HTML for Web:

```
cd docs/fr.irisa.atsyra.building.doc
./build.sh

```

The result is in the `generated-docs` folder