AssetBasedSystem 
with core::*;
with base::*;
with Building_def::*;
with physical_IS_def::*;
with Building_behavior_def::*;
import "physical_is_definitions.abs";
import "logical_is_behaviordefinitions.abs";
import "platform:/resource/mini.building.abs.model.lib/models/building_behaviordefinitions.abs";

DefinitionGroup physical_IS_behavior_def {
	AssetTypeAspect Attacker {
		// @runtime
		//reference location : Zone
		//reference ownedItems : SecurityItem [*] default []
		reference knownCredentials : Credential[*] default []
		reference knownSharedFolders : FileSharing[*] default []
		reference knownMachines : Machine[*] default []
		reference stolenData : Data[*] default []
		reference stolenStorage : Storage[*] default []
		reference stolenMachine : Machine[*] default []
		reference compromizedMachine : Machine[*] default []
	}
	GuardedAction stealMachine(attacker : Attacker, machine : Machine) {
		guard = attacker.location == machine.location && machine.cableLocked == false
		action {
			attacker.stolenMachine.add(machine);
			attacker.stolenStorage.addAll(machine.storages);
		// machine.storages.forAll({storage -> attacker.stolenData.addAll(storage.data)});
		}
	}
	GuardedAction stealStorage(attacker : Attacker, storage : Storage) {
		guard = storage.hostedBy.location == attacker.location
		action {
			attacker.stolenStorage.add(storage);
			attacker.stolenData.addAll(storage.data);
		}
	}

	GuardedAction stealData(attacker : Attacker, data : Data, accountUsed : Account) {
		guard = attacker.location == data.storedOn.hostedBy.location && data.storedOn.hostedBy.exploitedBy.accounts.contains(accountUsed) && attacker.knownCredentials.contains(accountUsed.credential)
		action {
			attacker.stolenData.add(data);
		}
	}

	GuardedAction bruteForceOSAuthentication(attacker : Attacker, account : Account) {
		guard = attacker.location == account.validFor.runsOn.location && account.authenticationType == EAuthenticationType::PASSWORD
		action {
			attacker.knownCredentials.add(account.credential);
			attacker.compromizedMachine.add(account.validFor.runsOn);
		}
	}

	GuardedAction discoverNetworkFromCompromizedMachine(attacker : Attacker, compromizedMachine : Machine) {
		guard = attacker.compromizedMachine.contains(compromizedMachine) && !compromizedMachine.connectedTo.isEmpty()
		action {
			compromizedMachine.connectedTo.forAll({network -> attacker.knownMachines.addAll(network.machines)});
			attacker.knownMachines.forAll({machine -> attacker.knownSharedFolders.addAll(machine.shares)});
		}
	}

	GuardedAction stealDataOnShareForlderViaCompromizedMachine(attacker : Attacker, sharedFolder : FileSharing, sharedFolderAccount : Account, compromizedMachine : Machine) {
		guard = attacker.compromizedMachine.contains(compromizedMachine) &&
			  attacker.knownSharedFolders.contains(sharedFolder) &&
			  sharedFolder.authorizedAccount.contains(sharedFolderAccount) &&
			  (attacker.knownCredentials.contains(sharedFolderAccount.credential) || sharedFolderAccount.authenticationType == EAuthenticationType::NONE)
		action {
			attacker.stolenData.addAll(sharedFolder.dataShared);
		}
	}
}
