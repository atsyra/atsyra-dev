AssetBasedSystem
with core::*;
import "platform:/plugin/core.abs.model.lib/models/core.abs";
import "platform:/resource/mini.base.abs.model.lib/models/base_definitions.abs";

DefinitionGroup Building_def {
	annotation "icon.name"
	annotation "security.repository.name"
	annotation "security.repository.version"
	annotation "security.repository.section.title"
	
	AssetType Zone extends (Any, core::ContainableByAny){ // TODO inheritance to Any should be implicit cf. https://gitlab.inria.fr/atsyra/atsyra-dev/-/issues/137
		tags (Building)
		reference alarms : Alarm [*] opposite zoneControlled
		//reference hasMachines : Machine[*] opposite location
		reference entrances : Access[*] opposite outside
		reference exits : Access[*] opposite inside
	}

	abstract AssetType Access extends (core::ContainableByAny){
		tags (Building)
		reference inside : Zone[1] opposite exits
		reference outside : Zone[1] opposite entrances
		reference alarms : Alarm[*] opposite accessControlled
		attribute securityLevel : base::SecurityLevel default base::SecurityLevel::UNDEFINED
	}

	AssetType FreeAccess extends (Access) {
		tags (Building)
	}
	abstract AssetType CloseableAccess extends (Access) {
		tags (Building)
	}

	AssetType Door  extends (CloseableAccess) {
		tags (Building)
		reference keys : Key[1..*] opposite doors
	}
	AssetType BadgeDoor extends (CloseableAccess) {
		tags (Building)
		reference badges : Badge[1..*] opposite doors
	}
	AssetType Window  extends (CloseableAccess) {
		tags (Building)
	}
	AssetType Alarm {
		tags (Building)
		reference zoneControlled : Zone[0..1] opposite alarms
		reference accessControlled : Access[0..1] opposite alarms
	}

	abstract AssetType SecurityItem {
		tags (Building)
	}

	AssetType Key extends (SecurityItem) {
		tags (Building)
		reference doors : Door[*] opposite keys
	}
	AssetType Badge extends (SecurityItem) {
		tags (Building)
		reference doors : BadgeDoor[*] opposite badges
	}
	AssetType GenericItem extends (SecurityItem) {
		tags (Building)
	}
	
	/* define some structural contracts applying to the static structure of Building models */
	static contract insideIsDifferentFromOutSide (access : Access) {
		severity = ERROR
		description "Inside part of an access must be distinct from outside part"
		guard = access.inside != access.outside
	}
	
	static contract BadgeDoorMustHaveBadge( badgeDoor : BadgeDoor) {
		severity = WARNING
		description "A badged door must have at least one associated badge"
		guard = ! badgeDoor.badges.isEmpty()
	}

	Tag Building
}
