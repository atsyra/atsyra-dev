AssetBasedSystem
with default_def::* from "default_definitions.abs";
DefinitionGroup Building_def {
	AssetType Zone extends (default_def::Any) {
		level "1"
		tags (Building)
		// reference nestedInformation : AbstractInformation [*]
		reference alarms : Alarm [*]
		reference hasMachines : Machine[*]
	}

	AssetType Access extends (default_def::Any) {
		level "2"
		tags (Building)
		reference inside : Zone
		reference outside : Zone
		reference alarms : Alarm [*]

	}
	AssetType FreeAccess extends (Access) {
		level "2"
		tags (Building)
	}
	AssetType CloseableAccess extends (Access) {
		level "2"
		tags (Building)
	}

	AssetType Door  extends (CloseableAccess) {
		level "2"
		tags (Building)
		reference keys : Item[*]
	}
	AssetType BadgeDoor extends (CloseableAccess) {
		level "2"
		tags (Building)
		reference badges : Item[*]
	}
	AssetType Windows  extends (CloseableAccess) {
		level "2"
		tags (Building)

	}
	AssetType Alarm extends (default_def::Any) {
		level "3"
		tags (Building)
		reference control : Zone
	}

	AssetType Item extends (default_def::Any) {
		level "3"
	}

	AssetType Attacker extends (default_def::Any) {
		tags (Human)
	// reference testRef : Information [*]
	}
	AssetType Machine extends (default_def::Any) {
		attribute name : String default undefined
		attribute cableLocked : Boolean default false
		reference storages : Storage[*] default []
		reference location : Zone[1]
		reference exploitedBy : OS[1]
		reference connectedTo : Network[*] default []
		reference shares : FileSharing[*] default []
	}
	AssetType Workstation extends (Machine) {

	}
	AssetType Server extends (Machine) {

	}
	AssetType Storage extends (default_def::Any) {
		attribute encrypted : Boolean default false
		reference hostedBy : Machine[1]
		reference encryptedWith : Credential[0..1]
		reference data : Data[*] default []
	}
	AssetType Data extends (default_def::Any) {
		attribute type : EDataSensibility default EDataSensibility::STANDARD
		attribute encrypted : Boolean default false
		reference encryptedWith : Credential[0..1] default undefined
		reference storedOn : Storage[1]
	}
	AssetType OS extends (default_def::Any) {
		attribute plateform : EPlateform default undefined
		attribute version : Version default undefined
		reference runsOn : Machine[1]
		reference accounts : Account[*] default []
	}
	AssetType Account extends (default_def::Any) {
		attribute accountType : EAccountType default undefined
		attribute authenticationType : EAuthenticationType default EAuthenticationType::PASSWORD
		attribute permission : EPermission default EPermission::USER
		reference credential : Credential[0..1] default undefined
		reference validFor : OS[0..1] default undefined
	}
	AssetType Credential extends (default_def::Any) {
		reference worksWith : Account[*] default []
	}
	AssetType Network extends (default_def::Any) {
		reference machines : Machine[1..*]
	}
	AssetType FileSharing extends (default_def::Any) {
		reference sharedBy : Machine[1]
		reference dataShared : Data[*] default []
		reference authorizedAccount : Account[*] default []
	}
	/* define some contracts applying to the static structure of Building models */
	static contract insideIsDifferentFromOutSide (access : Access) {
		description "Inside part of an access must be distinct from outside part"
		guard = access.inside != access.outside
	}
	static contract BadgeDoorMustHaveAkey( badgeDoor : BadgeDoor) {
		severity = WARNING
		annotations {
			"ISO.xxx" = "ISO Rule N° yyy"
		}
		description "According to ISO.xxx a badged door must have at least one associated badge"
		guard = ! badgeDoor.badges.isEmpty()
	}

	annotation "ISO.xxx"
}
