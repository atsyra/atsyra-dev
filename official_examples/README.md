Content of the examples:


- `abs.lib` : definition of AssetType, their behavior, contracts, etc, they are grouped in by usage
-- `abs.lib/core.abs.model.lib` : the common concept that can be used by all abs.
-- `abs.lib/mini.*.model.lib` :  this is the `mini` abs domain, the definitions are grouped by sub domain in separate files/libraries.
`mini.base`, `mini.building`, `mini.is`. An example model may use on or all of these sub domains.
- `abs.model` : contains example models (Asset)  relying on definition from `abs.lib`,  the name of the example must indicate the domain. ex: `mini` if definitions come from the `mini`domain.
- `abs.standalone` : contains example models containing both definitions (AssetType, behavior, contract) and user model (Asset).