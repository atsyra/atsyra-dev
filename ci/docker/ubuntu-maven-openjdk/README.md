# how to manage the docker images for use by gitlab-ci


cf. https://gitlab.inria.fr/atsyra/atsyra-dev/container_registry

from this directory

```
docker login registry.gitlab.inria.fr
docker build -t registry.gitlab.inria.fr/atsyra/atsyra-dev/ubuntu-maven-openjdk:latest .
docker push registry.gitlab.inria.fr/atsyra/atsyra-dev/ubuntu-maven-openjdk:latest
```



## Some notes about using this docker locally

### Direct use of docker
use docker to compile atsyra from this directory (or adapt the volume mount)

```
docker run -it -v $PWD/../../..:/builds/atsyra --user 1000 registry.gitlab.inria.fr/atsyra/atsyra-dev/ubuntu-maven-openjdk:latest /bin/bash
```


in the docker shell:

Adapt MAVEN_OPTS to use your local .m2
```
export MAVEN_OPTS='-Dmaven.repo.local=/builds/atsyra/.m2/repository  -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Djava.awt.headless=true -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true'
```

Launch virtual display
```
Xvfb :1 -ac -screen 0 1024x768x8 & export DISPLAY=:1
```

and launch maven command. For example
```
mvn --projects  '!releng/fr.irisa.atsyra.studio.product,!releng/fr.irisa.atsyra.studio.sdk.product'    clean integration-test      fr.jcgay.maven.plugins:buildplan-maven-plugin:list-phase
```


### Use of gitlab-runner

in .gitlab-ci.yml, change MAVEN_OPT  to remove `-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository` so the `/root.m2` will be used and mapped

`test_only` is the name of the job you want to start in the .gitlab-ci.yml

```
gitlab-runner exec docker --cache-type --cache-dir=/tmp/gitlab-cache --docker-cache-dir=/tmp/gitlab-cache --docker-volumes=/tmp/gitlab-cache  --docker-image registry.gitlab.inria.fr/atsyra/atsyra-dev/ubuntu-maven-openjdk:latest --docker-pull-policy="if-not-present" --docker-volumes $PWD/.m2:/root/.m2 --docker-volumes $PWD/cache:/cache  test_only
```