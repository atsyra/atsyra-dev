(define (problem problem1)

    (:domain building)

    (:objects
        Exterior Lobby WelcomeDesk Corridor1 Corridor2 LuggageStorage - zone
        Alarm1 - alarm
        FrontDoor WelcomeDeskGate SideDoor1 StorageSideDoor1 StorageSideDoor2 - door
        Window1 Window2 - windows
        SideAccess - freeAccess
        welcomeDeskToStorageAccess - badgeDoor
        ArseneLupin - attacker
        receptionistBadge key1 document - item
    )

    (:init
        ; from references
        (inside StorageSideDoor2 Corridor2)
        (inside StorageSideDoor1 Corridor1)
        (inside Window2 Corridor2)
        (inside Window1 Corridor1)
        (inside SideAccess2 Lobby)
        (inside SideDoor1 Lobby)
        (inside WelcomeDeskGate WelcomeDesk)
        (inside welcomeDeskToStorageAccess WelcomeDesk)
        (inside FrontDoor Lobby)

        (outside StorageSideDoor2 LuggageStorage)
        (outside StorageSideDoor1 LuggageStorage)
        (outside Window2 Exterior)
        (outside Window1 Exterior)
        (outside SideAccess2 Corridor2)
        (outside SideDoor1 Corridor1)
        (outside WelcomeDeskGate Lobby)
        (outside welcomeDeskToStorageAccess LuggageStorage)
        (outside FrontDoor Exterior)

        (keys StorageSideDoor2 key1)
        (keys SideDoor1 key1)

        (alarms Corridor1 Alarm1)

        (badges welcomeDeskToStorageAccess receptionistBadge)

        ; from attributes
        (location ArseneLupin Exterior)
        (itemsInZone WelcomeDesk receptionistBadge)
        (itemsInZone WelcomeDesk key1)
        (itemsInZone LuggageStorage document)
        (isActive Alarm1)
        (isLocked welcomeDeskToStorageAccess)
        (isLocked SideDoor1)
        (isLocked StorageSideDoor2)

        ; relaxing problem (for testing)
        (isOpen FrontDoor)
    )

    (:goal (location ArseneLupin Lobby))
)