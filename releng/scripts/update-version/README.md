This script can be used to automate the release process of the project.

Important:
Note that due to OSGI/maven version semantics differences and use of tycho (where 1.0.0.qualifier <=> 1.0.0-SNAPSHOT ) 
removing -SNAPSHOT should be done only on pure maven projects (ie. pomfirst)  the .qualifier must remain when 

# Creating a Release

From the root of the repository:

Change pomfirst pom into non snapshot 
```
python releng/scripts/update-version/update-version.py --old_version 2.0.0-SNAPSHOT --new_version 2.0.0 pomfirst/
```

check changed files : Take care of versions of dependancies which may have the same version as the source or target version

Commit, verify, tag and push (push both the commit and the tag)


Revert pomfirst to snapshot
```
python releng/scripts/update-version/update-version.py --old_version 2.0.0 --new_version 2.0.0-SNAPSHOT pomfirst/
```

bump version for all the projets (including tycho)

```
python releng/scripts/update-version/update-version.py --old_version 2.0.0-SNAPSHOT --new_version 2.0.1-SNAPSHOT .
```

Commit, verify, push

