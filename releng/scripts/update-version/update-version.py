import argparse
import fileinput
import os
import sys


def replace_version(directory, filename, old_version, new_version):
    """
    Recursively replaces a version number in files with a given name
    in a directory tree.

    Args:
        directory (str): The root directory of the directory tree.
        filename (str): The filename to filter files.
        old_version (str): The version number to replace.
        new_version (str): The new version number to replace with.
    """
    nbModifiedFiles = 0
    nbFiles = 0
    # Walk through the directory tree
    for root, dirs, files in os.walk(directory):
        for file in files:
            base_name = os.path.basename(file)
            # if file.endswith(extension):
            if base_name == filename:
                file_path = os.path.join(root, file)
                hasOccurences = False
                nbFiles = nbFiles + 1
                # Open the file for reading and writing
                with fileinput.FileInput(file_path, inplace=True) as f:
                    for line in f:
                        # Replace the old version with the new version
                        if old_version in line:
                            hasOccurences = True
                        new_line = line.replace(old_version, new_version)
                        sys.stdout.write(new_line)
                if hasOccurences:
                    nbModifiedFiles = nbModifiedFiles + 1
    
    print(f"Replaced version in {nbModifiedFiles} files out of {nbFiles} files with name  '{filename}' .")




if __name__ == '__main__':
    # Create an ArgumentParser object
    parser = argparse.ArgumentParser()
    parser.add_argument('folder_name', nargs='?', default='.')
    parser.add_argument('--old_version', required=True, help='old version number (using maven syntax for SNAPSHOT if required)')
    parser.add_argument('--new_version', required=True, help='new version number (using maven syntax for SNAPSHOT if required)')
    args = parser.parse_args()
    
    replace_version(args.folder_name, 'pom.xml', args.old_version, args.new_version)
    old_version_eclipse = args.old_version.replace('-SNAPSHOT','.qualifier')
    new_version_eclipse = args.new_version.replace('-SNAPSHOT','.qualifier')
    replace_version(args.folder_name, 'MANIFEST.MF', old_version_eclipse, new_version_eclipse)
    replace_version(args.folder_name, 'feature.xml', old_version_eclipse, new_version_eclipse)
